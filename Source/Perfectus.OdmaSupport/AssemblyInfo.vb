Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Perfectus ODMA Support")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("DPA Technologies Ltd")> 
<Assembly: AssemblyProduct("Perfectus")> 
<Assembly: AssemblyCopyright("Copyright (c) 1999-2015 DPA Technologies Ltd.  All rights reserved.")> 
<Assembly: AssemblyTrademark("USA Patent Pending No. 76/083067.\nAustralian Patent No. 82716/01.\nNew Zealand Patent No. 506004.")> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("CD59F720-5F06-406D-BE58-5DBAD7B2D551")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("5.4.0.0")>
'<Assembly: AssemblyKeyFile("..\..\..\..\..\..\..\REFERENCES\Keys\Perfectus\Perfectus.snk")> 