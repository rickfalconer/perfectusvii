
Imports System.Runtime.InteropServices
Public Module OdmaImports

    'alternative code:
    '    <DllImport("odma32.dll")> Public Function ODMRegisterApp( _
    'ByRef pOdmHandle As Byte() _
    ', ByVal version As Short _
    ', ByVal lpszAppId As String _
    ', ByVal dwEnvData As Integer _
    ', ByRef pReserved As Byte()) As Integer
    '    End Function
    '=======================================================================
    'These are the declarations for the functions used from ODMA32.DLL
    '=======================================================================
    'Note: The ODMA C API is much more fussy with regard to Int types than VB (which wasn't strongly typed). Int32 must be used, not Integer
    Public ODMHandle As Int32

    Declare Function ODMRegisterApp Lib "odma32.dll" (ByRef pOdmHandle As Int32, ByVal version As Short, ByVal lpszAppId As String, ByVal dwEnvData As Integer, ByRef pReserved As Byte()) As Integer
    Declare Sub ODMUnRegisterApp Lib "odma32.dll" (ByVal ODMHandle As Int32)
    Declare Function ODMSelectDoc Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByRef flags As Int32) As Int32
    Declare Function ODMOpenDoc Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal flags As Int32, ByVal docID As String, ByVal docLocation As String) As Int32

    Declare Function ODMSaveDoc Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal newDocID As String) As Integer

    Declare Function ODMSaveAs Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal newDocID As String, ByVal format As String, ByVal callBack As Long, ByVal instanceData As Long) As Integer
    Declare Function ODMCloseDoc Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal activeTime As Int32, ByVal pagesPrinted As Int32, ByVal sessionData As Int32, ByVal dataLen As Int32) As Integer
    Declare Function ODMNewDoc Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal flags As Int32, ByVal format As String, ByVal docLocation As String) As Int32
    Declare Function ODMActivate Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal action As Int32, ByVal docID As String) As Integer
    Declare Function ODMGetDocInfo Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal item As Integer, ByVal data As String, ByVal dataLen As Integer) As Integer
    Declare Function ODMSetDocInfo Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal docID As String, ByVal item As Integer, ByVal data As String) As Integer
    Declare Function ODMGetDMSInfo Lib "odma32.dll" (ByVal ODMHandle As Int32, ByVal DMSID As String, ByVal verNo As Integer, ByVal extensions As Long) As Integer
    Declare Function ODMGetDMSList Lib "odma32.dll" (ByVal buffer As String, ByVal buffer_size As Integer) As Integer
    Declare Function ODMGetDMSCount Lib "odma32.dll" () As Integer
    Declare Function ODMSetDMS Lib "odma32.dll" (ByVal appId As String, ByVal DMSID As String) As Integer


    Public Const ODM_API_VERSION = 100        ' Version of the API.
    Public Const ODM_DOCID_MAX = 255
    Public Const ODM_DMSID_MAX = 9            ' Max length of a DMS ID including the terminating NULL character.
    Public Const ODM_APPID_MAX = 9            ' Max length of a application ID including the terminating NULL character.
    Public Const ODM_FILENAME_MAX = 255       ' Maximum length of a filename returned by ODMA including the terminating NULL character.


    'Common format type names
    Public Const ODM_FORMAT_TEXT = "Text"
    Public Const ODM_FORMAT_RTF = "Rich text format"
    Public Const ODM_FORMAT_DCA = "DCA RFT"   ' IBM DCA Rich Format Text
    Public Const ODM_FORMAT_TIFF = "Tiff"
    Public Const ODM_FORMAT_GIF = "Gif"       ' Compuserve Graphics Interchange Format
    Public Const ODM_FORMAT_BMP = "Windows bitmap"
    Public Const ODM_FORMAT_PCX = "PCX"
    Public Const ODM_FORMAT_CGM = "CGM"       ' Computer Graphics Metafile
    Public Const ODM_FORMAT_EXE = "Executable file"
    Public Const ODM_FORMAT_PCL = "PCL"       ' HP Printer Control Language */
    Public Const ODM_FORMAT_PS = "PostScript"

    'Error returns
    Public Const ODM_NOERROR = 0              ' Success!
    Public Const ODM_E_FAIL = 1               ' Unspecified failure
    Public Const ODM_E_CANCEL = 2             ' Action was cancelled at user's request
    Public Const ODM_E_NODMS = 3              ' DMS not registered
    Public Const ODM_E_CANTINIT = 4           ' DMS failed to initalize
    Public Const ODM_E_VERSION = 5            ' DMS doesn't support the requested version of ODMA
    Public Const ODM_E_APPSELECT = 6          ' User has indicated that he wants to use
    '     the application 's file selection
    '     capabilities rather than those of the
    '     DMS.
    Public Const ODM_E_USERINT = 7            ' Requested action cannot be performed
    '     without user interaction, but silent
    '     mode was specified.
    Public Const ODM_E_HANDLE = 8             ' The DMHANDLE argument was invalid.
    Public Const ODM_E_ACCESS = 9             ' User does not have requested access rights
    '     to specified document.
    Public Const ODM_E_INUSE = 10             ' Document is currently in use and cannot
    '     be accessed in specified mode.
    Public Const ODM_E_DOCID = 11             ' Invalid document ID
    Public Const ODM_E_OPENMODE = 12          ' The specified action is incompatible
    '     with the mode in which the document was
    '     opened.
    Public Const ODM_E_NOOPEN = 13            ' The specified document is not open.
    Public Const ODM_E_ITEM = 14              ' Invalid item specifier.
    Public Const ODM_E_OTHERAPP = 15          ' Selected document was for another app.
    Public Const ODM_E_NOMOREDATA = 16        ' No more data is available.
    Public Const ODM_E_PARTIALSUCCESS = 17    '

    ' New Error Codes for ODMA 2.0
    Public Const ODM_E_REQARG = 18
    Public Const ODM_E_NOSUPPORT = 19
    Public Const ODM_E_TRUNCATED = 20
    Public Const ODM_E_INVARG = 21
    Public Const ODM_E_OFFLINE = 22

    'ODMOpenDoc modes
    Public Const ODM_MODIFYMODE = 1           ' Open document in a modifiable mode.
    Public Const ODM_VIEWMODE = 2             ' Open document in non-modifiable mode.

    'Actions for ODMActivate
    Public Const ODM_NONE = 0                 ' No specific action is requested.
    Public Const ODM_DELETE = 1               ' Delete the specified document.
    Public Const ODM_SHOWATTRIBUTES = 2       ' Display the specified document's profile or attributes.
    Public Const ODM_EDITATTRIBUTES = 3       ' Edit the specified document's profile or attributes.
    Public Const ODM_VIEWDOC = 4              ' Display the specified document in a viewer window.
    Public Const ODM_OPENDOC = 5              ' Open the specified document in its native application.

    'Item selectors for ODMGetDocInfo and ODMSetDocInfo
    Public Const ODM_AUTHOR = 1               ' Author of the document.
    Public Const ODM_NAME = 2                 ' Descriptive name of the document.
    Public Const ODM_TYPE = 3                 ' Type of the document.
    Public Const ODM_TITLETEXT = 4            ' Suggested text to display in the
    '     document window 's title bar.
    Public Const ODM_DMS_DEFINED = 5          ' DMS defined data.
    Public Const ODM_CONTENTFORMAT = 6        ' String describing document's format

    'ODMA 2.0
    Public Const ODM_ALTERNATE_RENDERINGS = 7
    Public Const ODM_CHECKEDOUTBY = 8
    Public Const ODM_CHECKOUTCOMMENT = 9
    Public Const ODM_CHECKOUTDATE = 10
    Public Const ODM_CREATEDBY = 11
    Public Const ODM_CREATEDDATE = 12
    Public Const ODM_DOCID_LATEST = 13
    Public Const ODM_DOCID_RELEASED = 14
    Public Const ODM_DOCVERSION = 15
    Public Const ODM_DOCVERSION_LATEST = 16
    Public Const ODM_DOCVERSION_RELEASED = 17
    Public Const ODM_LOCATION = 18
    Public Const ODM_KEYWORDS = 19
    Public Const ODM_LASTCHECKINBY = 20
    Public Const ODM_LASTCHECKINDATE = 21
    Public Const ODM_MODIFYDATE = 22
    Public Const ODM_MODIFYDATE_LATEST = 23
    Public Const ODM_MODIFYDATE_RELEASED = 24
    Public Const ODM_OWNER = 25
    Public Const ODM_SUBJECT = 26
    Public Const ODM_TITLETEXT_RO = 27
    Public Const ODM_URL = 28

    'Misc. modes, flags
    Public Const ODM_SILENT = 16              ' Don't interact with the user while fulfilling this request.

    'Flags for Query Interface
    Public Const ODM_ALL = 1                  ' All DMS's should be searched
    Public Const ODM_SPECIFIC = 2             ' Only specific DMS's should be searched


    'app specific variables
    Dim gDMSRunning As Boolean
    Public ThisApp As String = "IPMANAGER"  ' Registry App constant for the Application ID, e.g. MS WORD, WORDPERFECT

    Function RegisterDMS(ByVal WindowHandle As Integer, ByRef ErrCode As String, ByVal AppId As String, ByRef handle As Int32) As Integer
        Dim intRet As Integer 'Version
        Dim rc As Integer
        Dim hand As Int32
        RegisterDMS = False    'Initialize routine in case of failure

        Try
            intRet = ODMRegisterApp(ODMHandle, ODM_API_VERSION, AppId, WindowHandle, Nothing)
            handle = ODMHandle
            Select Case rc
                Case 0
                    'is returned if successful.
                    ErrCode = "Successful"
                    gDMSRunning = True
                    RegisterDMS = True
                Case ODM_E_NODMS
                    'is returned if no Document Management System has been
                    'registered for the calling application.
                    ErrCode = "There are no DMS systems configured for this Application."
                Case ODM_E_CANTINIT
                    'is returned if a DMS is registered for the
                    'calling application, but it fails to initialize itself.
                    ErrCode = "DMS cannot initialize properly"
                Case ODM_E_VERSION
                    'is returned if the DMS does not support
                    'the requested version of the API.
                    ErrCode = "DMS does not support this version of ODMA API."
                Case Else
                    ErrCode = "Unknown error code against ODMA: " & Str(rc)

            End Select

        Catch ex As Exception
            Throw
            'System.Windows.Forms.MessageBox.Show(ex.Message.ToString)
            '    Case 429
            '        ErrCode = "Cannot find or start DMS system. Integration is unavailable. Error code:" & Err.Number

            '    Case Else
            '        ErrCode = Err.Description & " " & Err.Number
            'End Select
        End Try
    End Function

    Function Unregister_DMS() As Integer
        'On Error Resume Next
        ODMUnRegisterApp(ODMHandle)
        ODMHandle = 0
        Debug.WriteLine("DMS is unregistered.")
    End Function

End Module
