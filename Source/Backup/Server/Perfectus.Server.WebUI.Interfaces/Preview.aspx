<%@ Register TagPrefix="cc1" Namespace="Perfectus.Server.WebUI.Library.Preview" Assembly="Perfectus.Server.WebUI.Library" %>
<%@ Page language="c#" Inherits="Perfectus.Server.WebUI.Interfaces.Preview" Trace="true" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Preview</title>
		<meta name="vs_snapToGrid" content="False">
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles/default.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<p>
				<table height="100%" cellSpacing="1" cellPadding="1" width="100%" border="0">
					<tr>
						<td><cc1:Preview id="Preview1" runat="server" CssClass="preview" 
						JavascriptPath="Script" 
						RenderPagePath="RenderImage.aspx?" 
						CssPath = "~/styles/default.css" EnableViewState="false"/></td>
					</tr>
				</table>
			</p>
		</form>
	</body>
</HTML>
