using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Interfaces
{
    /// <summary>
    /// Summary description for InterviewPreview.
    /// </summary>
    public partial class InterviewPreview : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            // We tell the interview ctrl object this is a preview page immediately, and DON'T wait until later on in the 
            // processing of the ctrl based on whether the PreviewPage object is set or not.
            iui.IsPagePreview = true;
            
            if (!IsPostBack)
            {
                loading.Visible = true;
                loaded.Visible = false;
            }
            else
            {
                Package package = new Package();
                BinaryFormatter bf = new BinaryFormatter();
                string pageData = Request.Form["PAGEDATA"];
                if (pageData != null && pageData.Trim().Length > 0)
                {
                    byte[] pageBytes = Convert.FromBase64String(pageData);
                    object o;
                    using (MemoryStream ms = new MemoryStream(pageBytes))
                    {
                        bf.Binder = new PackageSerialisationBinder();
                        bf.Context = new StreamingContext(StreamingContextStates.Persistence);
                        o = bf.Deserialize(ms);
                    }
                    if (o != null && o is InterviewPage)
                    {
                        InterviewPage ip = (InterviewPage)o;
                        package.Name = ip.ParentPackage.Name;
                        
                        foreach(QuestionDisplayType qdt in ip.ParentPackage.ExtensionMapping.Keys)
                        {
                            package.ExtensionMapping.Add(qdt, ip.ParentPackage.ExtensionMapping[qdt]);
                        }
                        ip.ParentPackage = package;
                        iui.PreviewPage = ip;
                        iui.DataBind();
                    }
                }
                loading.Visible = false;
                loaded.Visible = true;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Load += new EventHandler(Page_Load);
        }
        #endregion
    }
}
