using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class GetPreviewFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        byte[] data = null;
		string filename = Request.QueryString["file"];

        if (File.Exists(filename))
        {
            // Read PDF into byte array
            FileStream fStream = null;
            BinaryReader br = null;

            try
            {
                FileInfo fInfo = new FileInfo(filename);
                long numBytes = fInfo.Length;
                fStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                br = new BinaryReader(fStream);
                data = br.ReadBytes((int)numBytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (br != null)
                {
                    br.Close();
                }
                if (fStream != null)
                {
                    fStream.Close();
                }
                File.Delete(filename);
            }

            // Write the PDF into the Page
            Response.ClearContent();
            Response.AppendHeader("content-length", data.Length.ToString());
            Response.AppendHeader("content-disposition", "inline:filename=preview.pdf");
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(data);
            Response.Flush();
            Response.Close();
        }
    }
}
