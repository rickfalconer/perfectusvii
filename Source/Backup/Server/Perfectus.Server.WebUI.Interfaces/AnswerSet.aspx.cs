using System;
using System.IO;

namespace Perfectus.Server.WebUI.Interfaces
{
	/// <summary>
	/// Summary description for AnswerSet.
	/// </summary>
	public partial class AnswerSet : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			string qstr = Request.QueryString.ToString();
			try
			{
				Response.Clear();
				Response.AddHeader("content-disposition", "attachment; filename=answers.xml");
				Response.ContentType = "text/xml";			

				Guid instanceId = new Guid(qstr);
				MemoryStream ms = AnswerAcquirer.Xml.InstanceToAnswerSetXml.GetAnswerSetXml(instanceId);
				ms.Seek(0, SeekOrigin.Begin);
				ms.WriteTo(Response.OutputStream);
			}
			catch
			{}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
