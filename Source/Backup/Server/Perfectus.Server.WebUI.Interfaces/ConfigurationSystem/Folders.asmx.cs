using System;
using System.ComponentModel;
using System.Web.Services;
using Perfectus.Server.Common;

namespace Perfectus.Server.WebAPI.ConfigurationSystem
{
	/// <summary>
	///		Exposes the ConfigurationSystem.Folders API as a web service.
	/// </summary>
	public class Folders : WebService
	{
		public Folders()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

//		/// <summary>
//		///		Exposes the <see cref="Perfectus.Server.API.ConfigurationSystem.Folders.GetFolders()">API's Acquire method</see> as a web service.
//		/// </summary>
//		/// <returns>Returns a string containing an XML view of the visible folder hierarchy for the current user.  Only folders for which the user has PUBLISH permission are returned.</returns>
//		[WebMethod]
//		public string GetFolders()
//		{
//			try
//			{
//				return API.ConfigurationSystem.Folders.GetFolders();
//			}
//			catch (Exception ex)
//			{
//				Logging.LogMessage("The server was unable to retrieve the list of folders.", ex);
//				throw;
//			}
//		}

	}
}