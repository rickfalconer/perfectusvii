﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Perfectus.Server.WebUI.Interfaces.PackageManager
{
    // NOTE: If you change the interface name "IPublishStreaming" here, you must also update the reference to "IPublishStreaming" in Web.config.
    [ServiceContract]
    public interface IPublishStreaming
    {
        [OperationContract]
        PublishResult Publish_Wse_MTOM(System.IO.Stream zippedPackageBytes);

        [OperationContract]
        PublishResult Publish_Wse_MTOM_NewVersion(System.IO.Stream zippedPackageBytes);
    }
    [DataContract]
    public struct PublishResult
    {
        [DataMember]
        public int VersionNumber;
        [DataMember]
        public int RevisionNumber;
    }
}
