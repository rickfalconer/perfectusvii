/*using System;
using System.ComponentModel;
using System.Web.Services;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebAPI.PackageManager
{
	/// <summary>
	///		Exposes the API.PackageManagement.Retrieval API as a web service.
	/// </summary>
	public class Retrieval : WebService
	{
		/// <summary>
		///		Default constructor.
		/// </summary>
		public Retrieval()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

		/// <summary>
		///		Retrieves a Package from the server's database.  The returned package does <b>not</b> contain its templates' WordML.
		/// </summary>
		/// <param name="packageId">The <see cref="Guid"></see> that uniquely identifies the package to retrieve.</param>
		/// <param name="versionNumber">The version number of the package to retrieve.</param>
		/// <param name="revisionNumber">The revision number of the package to retrieve.</param>
		/// <returns></returns>
		public Package GetPackageWithoutTemplateData(Guid packageId, int versionNumber, int revisionNumber)
		{
			return API.PackageManagement.Retrieval.GetPackageWithoutTemplateData(packageId, versionNumber, revisionNumber);
		}


	}
}*/