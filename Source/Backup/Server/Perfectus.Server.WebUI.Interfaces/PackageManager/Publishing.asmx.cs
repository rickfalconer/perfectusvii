using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Web.Services;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Common;

namespace Perfectus.Server.WebAPI.PackageManager
{
	/// <summary>
	///		Allows client applications (for example, IPManager) to publish a serialised Package via a web service interface.
	/// </summary>
	/// <remarks>
	///		This service makes use of the MTOM attachment features of Microsoft's Web Service Enhancements (WSE) 3.0.  
    ///     It also requires a gzip compression library, such as SharpZipLib.
	/// </remarks>
	public class Publishing : WebService
	{
		/// <summary>
		///		Default constructor.
		/// </summary>
		public Publishing()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

		/// <summary>
		///		Accepts a serialised Package (typically with Templates' WordML serialised in-line) as a gzip-compressed MTOM attachment.
		/// </summary>
		/// <param name="newVersion">If true, the server will increment the package's version number.</param>
        /// <param name="zippedPackageBytes">A byte array containing a zipped package.</param>
		/// <example>
		/// <code>
		/// string serverWebApiBaseUrl = "http://localhost/perfectus.server.webApi";
		/// 
		/// // Publish the package
		/// pkg.PublishedBy = WindowsIdentity.GetCurrent().Name;
		///
		/// // Compress the package attachemt using SharpZipLib's gzip (v0.80)
		/// GZipOutputStream gzip = new GZipOutputStream(da.Stream);
		/// 
		/// // By specifying 'CrossMachine' any Templates will also be serialsed.
		/// Package.SaveToStream(pkg, gzip, StreamingContextStates.CrossMachine);
		/// gzip.Finish();
		/// PublishWebApi.PublishingWse publisher = new PublishWebApi.PublishingWse();
		/// 
		/// publisher.Url = string.Format("{0}/PackageManager/publishing.asmx", serverWebApiBaseUrl);
		/// 
		/// publisher.Credentials = CredentialCache.DefaultCredentials;
		/// 
		/// // Publish it to folder '1', and increment the version number if it's already present
		/// publisher.Publish_MTOM_Compressed(new int[] {1}, byte[] zippedPackageBytes);
		/// </code>
		/// </example>
		/// <remarks>
		/// When called, the client must also have the WSE 3.0 runtime installed.  
        /// To generate the appropriate WSE proxy objects from this service's WSDL, the WSE 3.0 developer tools must be installed.
		/// </remarks>
		[WebMethod]
		public PublishResult Publish_MTOM_Compressed(bool newVersion, byte[] zippedPackageBytes)
		{
			int versionNumber;
			int revisionNumber;

			Publish_MTOM_Compressed_Out(newVersion, out versionNumber, out revisionNumber, zippedPackageBytes);
			PublishResult retVal = new PublishResult();
			retVal.VersionNumber = versionNumber;
			retVal.RevisionNumber = revisionNumber;

			return retVal;
		}
		/// <summary>
		///		Accepts a serialised Package (typically with Templates' WordML serialised in-line) as a gzip-compressed MTOM attachment.
		/// </summary>
		/// <param name="newVersion">If true, the server will increment the package's version number.</param>
		/// <param name="versionNumber">[out] The version number assigned to the newly published package.</param>
		/// <param name="revisionNumber">[out] The revision number assigned to the newly published package.</param>
        /// <param name="zippedPackageBytes">A byte array containing a zipped package.</param>
		/// <example>
		/// <code>
		/// string serverWebApiBaseUrl = "http://localhost/perfectus.server.webApi";
		/// 
		/// // Publish the package
		/// pkg.PublishedBy = WindowsIdentity.GetCurrent().Name;
		///
		/// // Compress the package attachemt using SharpZipLib's gzip (v0.80)
        /// MemoryStream memoryStream = new MemoryStream();
        /// GZipOutputStream gzip = new GZipOutputStream(memoryStream);
		/// 
		/// // By specifying 'CrossMachine' any Templates will also be serialsed.
		/// Package.SaveToStream(pkg, gzip, StreamingContextStates.CrossMachine);
		/// gzip.Finish();
		/// PublishWebApi.PublishingWse publisher = new PublishWebApi.PublishingWse();
		/// 
		/// publisher.Url = string.Format("{0}/PackageManager/publishing.asmx", serverWebApiBaseUrl);
		/// 
		/// publisher.Credentials = CredentialCache.DefaultCredentials;
		/// publisher.RequestSoapContext.Attachments.Add(da);
		/// 
		/// // Publish it to folder '1', and increment the version number if it's already present
		/// publisher.Publish_MTOM_Compressed(new int[] {1}, true, zippedPackageBytes);
		/// </code>
		/// </example>
		/// <remarks>
		/// When called, the client must also have the WSE 3.0 runtime installed.  
        /// To generate the appropriate WSE proxy objects from this service's WSDL, the WSE 3.0 developer tools must be installed.
		/// </remarks>
		[WebMethod]
		public void Publish_MTOM_Compressed_Out(bool newVersion, out int versionNumber, out int revisionNumber, byte[] packageBytes)
		{
			Package p = null;
			try
			{
				// CrossMachine = deserialise all the templates as in-line byte[], rather than out-of-band in the temp folder
                p = Package.OpenFromStream(new MemoryStream(packageBytes), StreamingContextStates.CrossMachine);
				API.PackageManagement.Publishing.Publish(p, newVersion, out versionNumber, out revisionNumber);
			}
			catch (InvalidOperationException ioex)
			{
				Guid packageId = Guid.Empty;
				if (p != null)
				{
					packageId = p.UniqueIdentifier;
				}

				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageError"), ioex, Guid.Empty, packageId, int.MinValue, int.MinValue, int.MinValue, Logging.ExceptionLogSeverityCode.ERROr, true);
				throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageCannotBePublished"));

			}
			catch (Exception ex)
			{
				Guid packageId = Guid.Empty;
				if (p != null)
				{
					packageId = p.UniqueIdentifier;
				}

				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageError"), ex, Guid.Empty, packageId, int.MinValue, int.MinValue, int.MinValue, Logging.ExceptionLogSeverityCode.ERROr, true);
				throw;
			}
		}

       	public struct PublishResult
		{
			public int VersionNumber;
			public int RevisionNumber;
		}
	}
}
