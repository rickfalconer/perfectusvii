<%@ Page validateRequest="false"  EnableEventValidation="false" language="c#" Inherits="Perfectus.Server.WebUI.Interfaces.Interview" Trace="false" MaintainScrollPositionOnPostback="true"  %>
<%@ Register TagPrefix="cc1" Namespace="Perfectus.Server.WebUI.Library.Interview" Assembly="Perfectus.Server.WebUI.Library, culture=neutral" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head runat="server">
		<title>Interview</title>
		<link href="Styles/default2.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<form id="Form1" method="post" runat="server" enableviewstate="false">
            <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
                EnableScriptLocalization="true" ID="ScriptManager1" CombineScripts="false">
		        <Services>
		            <asp:ServiceReference Path="~/scriptServices.asmx" />
		        </Services>
            </ajaxToolkit:ToolkitScriptManager>
            <%-- pf-3096
		    <asp:ScriptManager runat="server" ID="ScriptManager1" EnableScriptLocalization="true" EnableScriptGlobalization="true">
		        <Services>
		            <asp:ServiceReference Path="~/scriptServices.asmx" />
		        </Services>
		    </asp:ScriptManager>
            --%>
		    <cc1:interview id="iui" runat="server" CssClass="interview" JavascriptPath="Script" ImagesPath="Images"
       			           PreviewRenderPagePath="RenderImage.aspx?" AnswerSetDownloadPath="AnswerSet.aspx" 
       			           PreviewCssPath="styles/default.css" EnableViewState="false" ShowFooter="true" ShowHeader="true" />
  			<%--
  			    <!-- To use customised navigiation, remove the <cc1:interview ...> control above and use the definition below, and then 
  			    uncomment the javascript also below. -->
  			
  			    <cc1:interview id="iui" runat="server" CssClass="interview" JavascriptPath="Script" ImagesPath="Images"
       			           PreviewRenderPagePath="RenderImage.aspx?" AnswerSetDownloadPath="AnswerSet.aspx" 
       			           PreviewCssPath="styles/default.css" EnableViewState="false" ShowFooter="false" ShowHeader="false" />
       	    --%>
            <!--
            <asp:PlaceHolder runat="server" ID="Buttons">
                <div id="NavButtons">
                    <asp:PlaceHolder runat="server" ID="PreviousHolder">
                        <button onclick="document.getElementById('<%= iui.PreviousButtonClientId %>').click();"><< Back</button>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="NextHolder">
                        <button onclick="document.getElementById('<%= iui.NextOrFinishButtonClientId %>').click();">Next >></button>
                    </asp:PlaceHolder>        
                    <asp:PlaceHolder runat="server" ID="FastForwardHolder">
                        <button onclick="document.getElementById('<%= iui.FastForwardButtonClientId %>').click();">Fast Forward</button>
                    </asp:PlaceHolder>                
                    <asp:PlaceHolder runat="server" ID="HistoryHolder">
                        <button onclick="document.getElementById('<%= iui.HistoryAnchorClientId %>').click();">History</button>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="PreviewHolder">
                        <button onclick="document.getElementById('<%= iui.PreviewAnchorClientId %>').click();">Preview</button>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="SuspendHolder">
                        <button onclick="document.getElementById('<%= iui.SuspendButtonClientId %>').click();">Suspend</button>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="SaveToHolder">
                        <button onclick="document.getElementById('<%= iui.SaveToButtonClientId %>').click();">Save Interview</button>
                    </asp:PlaceHolder>
                </div>
            </asp:PlaceHolder> 
            -->
		</form>
	</body>
</html>