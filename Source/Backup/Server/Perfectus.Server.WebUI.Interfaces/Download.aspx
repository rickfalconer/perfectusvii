<%@ Page language="c#" AutoEventWireup="false"%>
<%@ Import namespace="Perfectus.Common.PackageObjects" %>
<%@ Import namespace="Perfectus.Server.InterviewSystem" %>
<%@ Import namespace="Perfectus.Server.PackageManager" %>
<%@ Import namespace="System.Runtime.Serialization.Formatters.Binary" %>
<%@ Import namespace="System.Runtime.Serialization" %>
<%@ Import namespace="System.IO" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Download</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
<table>
<tr>
<td>
Package ID (GUID)
</td>
<td>
<asp:TextBox id=txtPackageId runat="server"></asp:TextBox>
</td>
</tr>
<tr>
<td>
Version Number
</td>
<td>
<asp:TextBox id=txtVersion runat="server"></asp:TextBox>
</td>
</tr>
<tr>
<td>
Revision Number
</td>
<td>
<asp:TextBox id=txtRevision runat="server"></asp:TextBox>
</td>
</tr>
</table>
<asp:Button id=Button1 runat="server" Text="Download" OnClick="Button1_Click"></asp:Button>



		</form>
	</body>
</HTML>

<script runat=server>

		private void Button1_Click(object sender, System.EventArgs e)
		{
			Guid packageId = new Guid(txtPackageId.Text);
			int version = Convert.ToInt32(txtVersion.Text);
			int revision = Convert.ToInt32(txtRevision.Text);
			
			// Make an instance - 5.1 has no getpackage procedures.
			Guid instanceId = InstanceManagement.CreateInstance(packageId, version, revision, "perfectus");

			// Retrieve the instance
			Package p = InstanceManagement.GetInstance(instanceId);

			// Put the outcome actions back in.

			foreach(Outcome o in p.Outcomes)
			{
				for (int i = 0; i < o.Definition.AllPossibleActions.Length; i++)
				{
					OutcomeAction oa = (OutcomeAction)(o.Definition.AllPossibleActions[i]);
					byte[] outcomeActionBytes;
					outcomeActionBytes = Perfectus.Server.PackageManager.Retrieval.GetOutcomeActionBytes(p.UniqueIdentifier, version, revision, o.UniqueIdentifier, oa.UniqueIdentifier);

					// Turn the bytes back into an OutcomeAction
					using (MemoryStream msOa = new MemoryStream())
					{
						BinaryFormatter bf = new BinaryFormatter();
						msOa.Write(outcomeActionBytes, 0, outcomeActionBytes.Length);
						msOa.Seek(0, SeekOrigin.Begin);
						object deserialised = bf.Deserialize(msOa);
						oa = (OutcomeAction)deserialised;
					}

					// Replace the stub in the package object with the one retreived from the db.
					o.Definition.AllPossibleActions[i] = oa;
				}
			}

/*			// Put the templates back in.
			foreach(TemplateDocument t in p.Templates)
			{
				if (t is WordTemplateDocument2)
				{
					byte[] templateBytes = Retrieval.GetTemplateBytes(p.UniqueIdentifier, version, revision, t.UniqueIdentifier);
					//((WordTemplateDocument2)t).WordMLBytes = templateBytes;
					((WordTemplateDocument2)t).CreateBlankWordMLFile();
					string tempFile = ((WordTemplateDocument2)t).FilePath;
					using(FileStream fs = File.Create(tempFile))
					{
						fs.Write(templateBytes, 0, templateBytes.Length);
					}
				}
			}*/

			// Delete all the templates as they don't survive serialisation this way for now.
			for (int i = p.Templates.Count - 1; i >= 0; i--)
			{
				p.Templates.RemoveAt(i);
			}


			// Write it out.
			Response.Clear();
			Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.ip", p.Name));
			Package.SaveToStream(p, Response.OutputStream, StreamingContextStates.CrossMachine);
			Response.Flush();
			Response.End();
		}

</script>
