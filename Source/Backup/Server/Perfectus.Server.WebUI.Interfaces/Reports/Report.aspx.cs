using System;
using Perfectus.Server.WebUI.Library;
using Perfectus.Server.WebUI.Library.Reporting;


namespace Perfectus.Server.WebUI.Interfaces.Reports
{
	/// <summary>
	/// Summary description for Report.
	/// </summary>
	public partial class Report : System.Web.UI.Page
	{
		private void Page_Load(object sender, System.EventArgs e)
		{	
			// report=library&itemId=95f6887d-341d-41bb-a60d-40b78c066342&itemType=Question
            if (Request.QueryString["report"] != null)
			{
				string strReportType = Request.QueryString["report"].ToLower();
				reportControl.ReportType = ReportTypes.PerfectusReportType.None;

                switch (strReportType)
				{
                    case "clauses":
						reportControl.ReportType = ReportTypes.PerfectusReportType.AllClauses;
						reportControl.ClauseId = Request.QueryString["clauseId"];
						reportControl.ClauseName = Request.QueryString["clauseName"];
						break;

                    case "packagesbyclause":
						reportControl.ReportType = ReportTypes.PerfectusReportType.PackagesUsingClause;
						reportControl.ClauseId = Request.QueryString["clauseId"];
						reportControl.ClauseName = Request.QueryString["clauseName"];
						break;

                    case "instancesusingclause":
						reportControl.ReportType = ReportTypes.PerfectusReportType.InstancesUsingClause;
						reportControl.ClauseId = Request.QueryString["clauseId"];
						reportControl.ClauseName = Request.QueryString["clauseName"];
						reportControl.PackageId = new Guid(Request.QueryString["packageId"]);
						reportControl.PackageName = Request.QueryString["packageName"];
						break;

                    case "library":
						reportControl.ReportType = ReportTypes.PerfectusReportType.PackagesUsingLibraryItem;
						reportControl.LibraryItemId = new Guid(Request.QueryString["itemId"]);
						reportControl.LibraryItemType = Request.QueryString["itemType"];
						reportControl.LibraryItemName = Request.QueryString["itemName"];
					break;

                    case "instancesusinglibraryitem":
						reportControl.ReportType = ReportTypes.PerfectusReportType.InstancesUsingLibraryItem;
						reportControl.LibraryItemId = new Guid(Request.QueryString["itemId"]);
						reportControl.LibraryItemType = Request.QueryString["itemType"];
						reportControl.LibraryItemName = Request.QueryString["itemName"];
						reportControl.PackageId = new Guid(Request.QueryString["packageId"]);
						reportControl.PackageName = Request.QueryString["packageName"];
						break;
				}
			}					
			
            if (reportControl.ReportType != ReportTypes.PerfectusReportType.None)
			{
				reportControl.DataBind();
			}
			else
			{
				System.Web.HttpContext.Current.Response.Write("The url is not a valid format");
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
	}
}
