<%@ Page language="c#"  AutoEventWireup="false" Inherits="Perfectus.Server.WebUI.Interfaces.Reports.Report" trace="False"%>
<%@ Register TagPrefix="cc1" Namespace="Perfectus.Server.WebUI.Library.Reporting" Assembly="Perfectus.Server.WebUI.Library, version=5.4.0.0, publicKeyToken=cba912c898d07724, culture=neutral" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Reports</title>
		<link href="../Styles/Reports.css" type="text/css" rel="stylesheet">
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<cc1:ReportControl id="reportControl" runat="server" JavascriptPath="Script" ImagesPath="../Images" CssClass="report"></cc1:ReportControl></form>
	</body>
</html>
