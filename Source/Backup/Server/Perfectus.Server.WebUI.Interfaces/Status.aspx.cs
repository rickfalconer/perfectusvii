using System;
using Perfectus.Server.WebUI.Library.Status;

namespace Perfectus.Server.WebUI.Interfaces
{
    /// <summary>
    /// Summary description for Status.
    /// </summary>
    public partial class Status : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Status1.DataBind();
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Status1.StartInterview += new EventHandler<StartInterviewEventArgs>(this.Status1_StartInterview);
            this.Status1.ResumeInterview += new EventHandler<ResumeInterviewEventArgs>(this.Status1_ResumeInterview);

        }
        #endregion

        private void Status1_StartInterview(object sender, StartInterviewEventArgs e)
        {
            string destinationUrl = string.Format("Interview.aspx?EasyStart/{0}/{1}/{2}", e.PackageId, e.PackageVersionId, e.PackageRevisionId);
            Response.Redirect(destinationUrl);
        }
        private void Status1_ResumeInterview(object sender, ResumeInterviewEventArgs e)
        {
            Guid newGuid = this.Status1.CreateInterviewFromExisting(e);

            string destinationUrl = string.Format("Interview.aspx?r/{0}", newGuid);
            Response.Redirect(destinationUrl);
        }

    }
}