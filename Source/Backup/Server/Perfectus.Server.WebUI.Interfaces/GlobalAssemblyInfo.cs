using System.Reflection;

[assembly: AssemblyVersion("18.2.0.0")]
[assembly: AssemblyFileVersion("18.2.0.0")]
[assembly: AssemblyInformationalVersion("18.2.0.0")]
[assembly: AssemblyCopyright("Copyright � 1999-2018 DPA Technologies Ltd.  All rights reserved.")]
[assembly: AssemblyCompany("DPA Technologies Ltd.")]
[assembly: AssemblyProduct("Perfectus")]
[assembly: AssemblyTrademark("USA Patent Pending No. 76/083067.\nAustralian Patent No. 82716/01.\nNew Zealand Patent No. 506004.")]