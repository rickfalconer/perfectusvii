<%@ Page language="c#" Inherits="Perfectus.Server.WebUI.Interfaces.AllPackages"  %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>All Packages</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table>
			<asp:Repeater id="Repeater1" runat="server">
				<ItemTemplate>
					<tr>
						<td><%# ((System.Data.DataRowView)Container.DataItem)["Name"] %></td>
						<td><%# ((System.Data.DataRowView)Container.DataItem)["PackageVersionNumber"] %>.<%# ((System.Data.DataRowView)Container.DataItem)["PackageRevisionNumber"] %></td>
						<td><%# ((System.Data.DataRowView)Container.DataItem)["CreatedBy"] %></td>
						<td><a href='interview.aspx?EasyStart/<%# ((System.Data.DataRowView)Container.DataItem)["PackageId"] %>/<%# ((System.Data.DataRowView)Container.DataItem)["PackageVersionNumber"] %>/<%# ((System.Data.DataRowView)Container.DataItem)["PackageRevisionNumber"] %>'>Start</a></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
			</table>
		</form>
	</body>
</HTML>