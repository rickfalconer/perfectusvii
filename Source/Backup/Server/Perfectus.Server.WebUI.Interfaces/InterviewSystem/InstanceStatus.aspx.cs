using System;
using System.Web.UI;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.WebUI.Interfaces.InterviewSystem
{
    public partial class InstanceStatus : Page
    {
        // Class level variables
        private Guid instanceIDGuid = Guid.Empty;
        private string instanceID = string.Empty;
        private string statusXml = string.Empty;

        // Constants
        private const string INSTANCE_ID = "InstanceID";

        /// <summary>
        ///     Accessor for the instances status represented as xml
        /// </summary>
        /// 


        /// <summary>
        ///     The instance id of the package instance to be handled
        /// </summary>
        public string InstanceID
        {
            set { instanceID = value; }
        }

        /// <summary>
        ///     Page_Load Event Handler. Directly passes all processing to the HandleLoad() method.
        /// </summary>
        /// <remarks>
        ///     All processing passed to HandleLoad() Method to allow for unit testing
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                InstanceID = Request.Params.Get(INSTANCE_ID);
                HandleOnload();
            }
        }

        /// <summary>
        ///     Handles the Onload event.
        ///     Separated into it's own method to allow for a unit test method to call this.
        /// </summary>
        private void HandleOnload()
        {
            try
            {
                // Get the post variables
                ValidateInstanceID();

                // Get the status information for the given instance
                SetStatusXml();
            }
            catch (Exception ex)
            {
                HandleProcessingError(ex);
            }
        }

        /// <summary>
        ///     Sets the Instance Id which is provided as a parameter in the POST method request.
        ///     Validation of the Instance Id is performed, with an appropiate exception being thrown
        ///     when validation fails
        /// </summary>
        private void ValidateInstanceID()
        {
            // Validate the InstanceID
            if (instanceID == null || instanceID == string.Empty) // Has been provided?
            {
                string message =
                    ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString(
                        "Perfectus.Server.WebUI.Interfaces.InstanceStatus.NullInstanceID");
                throw new ArgumentNullException(INSTANCE_ID, message);
            }

            try
            {
                instanceIDGuid = new Guid(instanceID);
            }
            catch (Exception ex)
            {
                string message =
                    ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString(
                        "Perfectus.Server.WebUI.Interfaces.InstanceStatus.InvalidInstanceID");
                throw new InvalidCastException(string.Format(message, instanceID), ex);
            }
        }

        private void SetStatusXml()
        {
            try
            {
                statusXml = InstanceManagement.GetInstanceStatusXml(instanceIDGuid);
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                if (rethrow)
                {
                    throw;
                }                
            }
        }

        private void HandleProcessingError(Exception ex)
        {
            //<InstanceStatus>
            XmlDocument xmlDocument = new XmlDocument();

            // Ensure the xml root node is present
            if (statusXml == null || statusXml == string.Empty)
            {
                xmlDocument.LoadXml("<InstanceStatus></InstanceStatus>");
            }

            // Create the error element and populate
            XmlElement errorNode = xmlDocument.CreateElement("ProcessingError");
            errorNode.InnerXml = ex.Message;
            xmlDocument.DocumentElement.AppendChild(errorNode);

            //TODO - need to set the config correctly - throwing errors currently
            /*bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
            if (rethrow)
            {
                throw ex;
            }*/
             
            // Write friendly error response xml
            statusXml = xmlDocument.InnerXml;
        }

        /// <summary>
        ///     Renders the contents of the status for the given instance id.
        /// </summary>
        /// <remarks>
        ///     Any exceptions handled during processing will be included in the
        ///     status xml instead of the actual status data.
        /// </remarks>
        /// <param name="writer">
        ///     The html to be written in the response stream for the control. 
        ///     In this case Xml of the given instance's status.
        /// </param>
        protected override void Render(HtmlTextWriter writer)
        {
            writer.Write(statusXml);
            base.Render(writer);
        }

//#if UNIT_TESTS
        
//        /* 
//         * This section has been added solely for the use of unit tesing and should not
//         * be included in release builds 
//         * 
//         */

//        [Test]
//        [Ignore]
//        public void OnLoadNullInstanceIDTest()
//        {
//            // Do not preset the instance id
//            HandleOnload();

//            // Asserts
//            Assert.AreEqual("The Instance ID has not been provided.\r\nParameter name: InstanceID", statusXml);
//        }

//        [Test]
//        [Ignore]
//        public void OnloadInvalidLengthInstanceIDTest()
//        {
//            InstanceID = "893dac1b-";
//            HandleOnload();

//            // Asserts
//            Assert.AreEqual("The Instance ID length is not a valid length of 32 characters.", statusXml);
//        }

//        [Test]
//        [Ignore]
//        public void OnloadCastExceptionInstanceIDTest()
//        {
//            InstanceID = "893dac1b*4610*4cd2*8018*dbb339c9cc33";
//            HandleOnload();

//            // Asserts
//            Assert.AreEqual(string.Format("Could not cast the InstanceId {0} to a valid GUID.", instanceID), statusXml);
//        }

//        [Test]
//        public void OnloadValidInstanceIDTest()
//        { 
//            string testInstanceId = "893dac1b-4610-4cd2-8018-dbb339c9cc33";
//            InstanceID = testInstanceId;
//            HandleOnload();

//            // Asserts
//            Assert.AreEqual(testInstanceId, instanceID);
//            Assert.IsNotEmpty(statusXml);
//        }

//#endif
    }
}
