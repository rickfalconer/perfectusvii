﻿using System.Reflection;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus.Server.WebUI.Interfaces")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs