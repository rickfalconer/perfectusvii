﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Perfectus.Server.Abstractions
{
    internal class A_Package
    {
        public Guid uniqueIdentifier;
        public DateTime creationDateTime;
        public DateTime modificationDateTime;
        public string createdBy;
        public string modifiedBy;
        public string description;
        public string name;
    }

    internal class A_Question
    {
        public Guid uniqueIdentifier;
        public DateTime creationDateTime;
        public DateTime modificationDateTime;
        public string createdBy;
        public string modifiedBy;
        public string displayType;
        public string name;
        public string prompt;
        public string defaultAnswer;
        public string notes;
        public int displayHeight;
        public int displayWidth;
        public string direction;
        public string prefix;
        public string suffix;
        public string interviewFormatString;
        public string dataType;
        public int dataSize;
        public string buttonText;
        public string dataBindings;
        public string parameter;
        public string validation;
        public string validationMessage;
        public bool mandatory;
        public bool visible;
        public bool enabled;
        public string hint;
        public string example;
        public string help;
        public string previewHoverText;
    }
}