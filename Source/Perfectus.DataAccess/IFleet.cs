﻿using System;
namespace Perfectus.DataAccess
{
    public interface IFleet :IDisposable
    {
        void AddCursorOutParameter(string CursorName);
        void AddCursorOutParameter(string CursorName, String param_cmd_name);
        

        void AddInParameterInt32(string name, Int32 value,String param_cmd_name);
        void AddInParameterInt32(string name, Int32 value);
        void AddInParameterInt32(string name, String param_cmd_name);
        void AddInParameterInt32(string name);
        void AddInParameterInt64(string name, Int64 value, String param_cmd_name);
        void AddInParameterInt64(string name, Int64 value);
        void AddInParameterInt64(string name, String param_cmd_name);
        void AddInParameterInt64(string name);
        void AddInParameterBoolean(string name, Boolean value, String param_cmd_name);
        void AddInParameterBoolean(string name, Boolean value);
        void AddInParameterBoolean(string name);
        void AddInParameterString(string name, String value, String param_cmd_name);
        void AddInParameterString(string name, String value);
        void AddInParameterString(string name);
        void AddInParameterDateTime(string name, DateTime value, String param_cmd_name);
        void AddInParameterDateTime(string name, DateTime value);
        void AddInParameterDateTime(string name);
        void AddInParameterGuid(string name, Guid value, String param_cmd_name);
        void AddInParameterGuid(string name, Guid value);
        void AddInParameterGuid(string name);
        void AddInParameterGuid(string name, String param_cmd_name);
        void AddInParameterBinary(string name, byte[] value, String param_cmd_name);
        void AddInParameterBinary(string name, byte[] value);
        void AddInParameterBinary(string name);

        void AddInParameterColumn(String Name, String SourceColumn, System.Data.DbType type, String ID);
        
        
        /*
         * These are now private:
        void AddInParameter(string name, System.Data.DbType dbType);
        void AddInParameter(string name, System.Data.DbType dbType, object value);
        void AddInParameter(string name, System.Data.DbType dbType, object value, String param_cmd_name);
        */

        void AddOutParameter(string name, System.Data.DbType dbType, int size);
        void AddOutParameter(string name, System.Data.DbType dbType, int size, String param_cmd_name);

        void BeginTransaction();
        void Commit();
        new void Dispose();

        System.Data.DataSet ExecuteDataSet(string param_sql);
        System.Data.DataSet ExecuteDataSetSP(string param_sp);

        void LoadDataSet(String param_sql, System.Data.DataSet dataSet, string[] tableNames);
        void LoadDataSet(String param_sql, System.Data.DataSet dataSet, string[] tableNames, String param_cmd_name);

        int UpdateDataSet(System.Data.DataSet dataSet, string TableName, String insertID, String updateID, String deleteID);

        int ExecuteNonQuery(string param_sql);
        int ExecuteNonQuerySP(string param_sp);
        int ExecuteNonQuery(string param_sql, string param_command_name);
        int ExecuteNonQuerySP(string param_sp, string param_command_name);
        
        System.Data.IDataReader ExecuteReader(string param_sql);
        System.Data.IDataReader ExecuteReaderSP(string param_sp);
        System.Data.IDataReader ExecuteReader(string param_sql, string param_command_name);
        System.Data.IDataReader ExecuteReaderSP(string param_sp, string param_command_name);
        
        object ExecuteScalar(string param_sql);
        object ExecuteScalarSP(string param_sp);
        object ExecuteScalar(string param_sql, string param_command_name);
        object ExecuteScalarSP(string param_sp, string param_command_name);

        void SetParameterValue(String param_name, object value, String param_cmd_name);
        void SetParameterValue(String param_name, object value);

        Int32 GetParameterInt32(String param_name, String param_cmd_name);
        Int32 GetParameterInt32(String param_name);
        Int64 GetParameterInt64(String param_name, String param_cmd_name);
        Int64 GetParameterInt64(String param_name);
        Guid GetParameterGuid(String param_name, String param_cmd_name);
        Guid GetParameterGuid(String param_name);
        String GetParameterString(String param_name, String param_cmd_name);
        String GetParameterString(String param_name);
        DateTime GetParameterDateTime(String param_name, String param_cmd_name);
        DateTime GetParameterDateTime(String param_name);

        // no longer public:
        //object GetParameterValue(String param_name, String param_cmd_name);
        //object GetParameterValue(String param_name);
        string fleetname { get; }
        bool InTransaction();
        void Rollback();
        int timeout { get; set; }

        Boolean IsOracle { get; }
    }
}
