using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

//using Microsoft.Practices.EnterpriseLibrary.Data.Oracle;
//using Microsoft.Practices.EnterpriseLibrary.Data.Oracle.Configuration;
//using System.Data.Common;

namespace Perfectus.DataAccess
{
    internal class ConfigfileFleet : Fleet
    {
        // Note: 'param_connect_name' represents the name of the connection string in the configuration file.
        // Note: 'param_fleet_name' represents the unique name we give each fleet instance.
        internal protected ConfigfileFleet( IConfigurationSource configsource, String param_connect_name, String param_fleet_name )
            : base( param_fleet_name )
        {
            CreateDatabase( new DatabaseProviderFactory( configsource ), param_connect_name );
        }

        // Note: 'param_connect_name' represents the name of the connection string in the configuration file.
        // Note: 'param_fleet_name' represents the unique name we give each fleet instance.
        internal protected ConfigfileFleet( FileConfigurationSource filesource, String param_connect_name, String param_fleet_name )
            : base ( param_fleet_name)
        {
            CreateDatabase( new DatabaseProviderFactory( filesource ), param_connect_name );
        }

        private void CreateDatabase( DatabaseProviderFactory dbfactory, String param_connect_name )
        {
            if( !String.IsNullOrEmpty( param_connect_name ) )
                base.m_database = dbfactory.Create( param_connect_name );
            else
                base.m_database = dbfactory.CreateDefault( );
        }
    }

    internal class FactoryFleet : Fleet
    {
        internal protected FactoryFleet(String param_connection_string, DbProviderFactory param_factory)
            : this(param_connection_string, param_factory, Guid.NewGuid().ToString()) {}
        internal protected FactoryFleet(String param_connection_string, DbProviderFactory param_factory,String param_fleet_name )
            : base ( param_fleet_name)
        {
            base.m_database = new GenericDatabase(param_connection_string, param_factory);
        }
    }
    internal class DefaultFleet : Fleet
    {
        internal protected DefaultFleet(String param_db_name,String param_fleet_name )
            : base(param_fleet_name)
        {
            // Note: 'param_db_name' represents the name of the connection string in the configuration file.
            if ( param_db_name == null )
                base.m_database = DatabaseFactory.CreateDatabase();
            else
                base.m_database = DatabaseFactory.CreateDatabase(param_db_name);
        }
    }
    internal class TypedFleet : Fleet
    {
        internal protected TypedFleet(String param_connection_string, Type param_type)
            : this(param_connection_string, param_type, Guid.NewGuid().ToString()) { }
        internal protected TypedFleet(String param_connection_string, Type param_type,String param_fleet_name )
            : base(param_fleet_name)
        {
            Type type = param_type;
            while (true) // Basic test
            {
                if (type == typeof(Database))
                    break;
                type = type.BaseType;
                if (type == null)
                    throw new ExceptionDataLayer("Type or base type must be 'Database'");
            }
            Object[] arguments = new Object[1];
            arguments[0] = param_connection_string;
            base.m_database = (Database)System.Activator.CreateInstance(param_type, arguments);
        }
    }

    //-------------------------------------------------------------------------
    internal abstract class Fleet : Perfectus.DataAccess.IFleet
    {
        protected class dbCommand : IDisposable
        {
            private String m_name;
            public String name
            {
                get { return m_name; }
            }
            private DbCommand m_command;
            public DbCommand Command
            {
                get { return m_command; }
            }
            protected internal dbCommand(String param_name, Database param_database)
            {
                m_name = param_name;
                m_command = param_database.DbProviderFactory.CreateCommand();
                if (m_command == null)
                    throw new ExceptionDataLayer("Failed to create command object");
            }

            #region IDisposable Members

            public void Dispose()
            {
                m_command.Dispose();
            }

            #endregion
        }
        private DbTransaction m_transaction;
        private DbConnection m_connection;
        private List<dbCommand> m_commands;
        protected Database m_database;

        //----
        private int m_timeout = 30;
        public int timeout
        {
            get { return m_timeout; }
            set { if (value >= 0) m_timeout = value; }
        }

        private String m_fleet_name;
        public String fleetname
        {
            get { return m_fleet_name; }
        }

        //-----

        private DbCommand RetrieveOrCreateCommand(String param_name)
        {
            if (param_name == null || param_name.Length == 0)
                return RetrieveOrCreateCommand();

            foreach (dbCommand cmd in m_commands)
                if (param_name == cmd.name)
                    return cmd.Command;

            m_commands.Add(new dbCommand(param_name, m_database));
            return RetrieveOrCreateCommand(param_name);
        }        
        private DbCommand RetrieveOrCreateCommand()
        {
            if (m_commands.Count == 0)
            {
                m_commands.Add(new dbCommand("Primary", m_database));
            }
            return RetrieveOrCreateCommand("Primary");
        }

        
        //-------------------------------------------------------------------
        protected Fleet(String param_fleet_name)
        {
            m_commands = new List<dbCommand>();
            m_fleet_name = param_fleet_name;
        }
        //-------------------------------------------------------------------
        

        internal void Remove()
        {
            if (InTransaction())
                Rollback();

            if ( null != m_commands )
            foreach (dbCommand cmd in m_commands)
                cmd.Dispose();

            if (m_database != null && m_connection != null)
                m_connection.Dispose();

            FleetContainer.RemoveFleet(this);

            m_commands = null;
            m_database = null;
            m_connection = null;
        }

        #region transaction handling
        public bool InTransaction()
        {
            return m_transaction != null;
        }
        public void BeginTransaction()
        {
            if (m_transaction != null)
                throw new ExceptionDataLayer("Transaction already active");
            if (m_connection == null)
                m_connection = m_database.CreateConnection();
            if (m_connection.State == ConnectionState.Closed)
                m_connection.Open();
            m_transaction = m_connection.BeginTransaction();
        }

        public void Rollback()
        {
            if (m_transaction == null)
                throw new ExceptionDataLayer("No active transaction");
            try
            {
                m_transaction.Rollback();
                if (m_connection.State == ConnectionState.Open)
                    m_connection.Close();
            }
            catch { }
            finally { m_transaction = null; }
        }

        public void Commit()
        {
            if (m_transaction == null)
                throw new ExceptionDataLayer("No active transaction");
            m_transaction.Commit();
            if (m_connection.State == ConnectionState.Open)
                m_connection.Close();
            m_transaction = null;
        }
        #endregion

        #region Database system

        public Boolean IsOracle
        {
            get { return m_database != null && m_database.ToString() == "Microsoft.Practices.EnterpriseLibrary.Data.Odp10.Odp10Database"; }
        }

        #endregion


        #region Execute statement

        // Object 
        // Looks a bit messy, but compiler can check type...
        /////////////
        // Int32
        public void AddInParameterInt32(string name, Int32 value, String param_cmd_name)
        {
            AddInParameter(name, DbType.Int32, value, param_cmd_name);
        }
        public void AddInParameterInt32(string name, Int32 value)
        {
            AddInParameterInt32(name, value, null);
        }
        public void AddInParameterInt32(string name)
        {
            AddInParameter(name, DbType.Int32, null);
        }
        public void AddInParameterInt32(string name, String param_cmd_name)
        {
            AddInParameter(name, DbType.Int32, null, param_cmd_name);
        }
        /////////////
        // Int64
        public void AddInParameterInt64(string name, Int64 value, String param_cmd_name)
        {
            AddInParameter(name, DbType.Int64, value, param_cmd_name);
        }
        public void AddInParameterInt64(string name, Int64 value)
        {
            AddInParameterInt64(name, value, null);
        }
        public void AddInParameterInt64(string name)
        {
            AddInParameter(name, DbType.Int64, null);
        }
        public void AddInParameterInt64(string name, String param_cmd_name)
        {
            AddInParameter(name, DbType.Int64, null, param_cmd_name);
        }

        /////////////
        // String
        public void AddInParameterString(string name, String value, String param_cmd_name)
        {
            AddInParameter(name, DbType.String, value, param_cmd_name);
        }
        public void AddInParameterString(string name, String value)
        {
            AddInParameterString(name, value, null);
        }
        public void AddInParameterString(string name)
        {
            AddInParameterString(name, null);
        }
        /////////////
        // Guid
        public void AddInParameterGuid(string name, Guid value, String param_cmd_name)
        {
            if( value == Guid.Empty )
                AddInParameter( name, DbType.Guid, DBNull.Value, param_cmd_name );
            else
                AddInParameter( name, DbType.Guid, value, param_cmd_name );
        }
        public void AddInParameterGuid(string name, Guid value)
        {
            AddInParameterGuid(name, value, null);
        }
        public void AddInParameterGuid(string name)
        {
            AddInParameter(name, DbType.Guid, null);
        }
        public void AddInParameterGuid(string name, String param_cmd_name)
        {
            AddInParameter(name, DbType.Guid, null, param_cmd_name);
        }
        /////////////
        // Boolean
        public void AddInParameterBoolean(string name, Boolean value, String param_cmd_name)
        {
            AddInParameter(name, DbType.Boolean, value, param_cmd_name);
        }
        public void AddInParameterBoolean(string name, Boolean value)
        {
            AddInParameterBoolean(name, value, null);
        }
        public void AddInParameterBoolean(string name)
        {
            AddInParameter(name, DbType.Boolean, null);
        }
        /////////////
        // DateTime
        public void AddInParameterDateTime(string name, DateTime value, String param_cmd_name)
        {
            AddInParameter(name, DbType.DateTime, value, param_cmd_name);
        }
        public void AddInParameterDateTime(string name, DateTime value)
        {
            AddInParameterDateTime(name, value, null);
        }
        public void AddInParameterDateTime(string name)
        {
            AddInParameter(name, DbType.DateTime, null);
        }
        /////////////
        // Binary
        public void AddInParameterBinary(string name, byte[] value, String param_cmd_name)
        {
            AddInParameter(name, DbType.Binary, value, param_cmd_name);
        }
        public void AddInParameterBinary(string name, byte[] value)
        {
            AddInParameterBinary(name, value, null);
        }
        public void AddInParameterBinary(string name)
        {
            AddInParameterBinary(name, null);
        }

        // Data Adapter
        public void AddInParameterColumn(String Name, String SourceColumn, DbType type, String ID)
        {
            m_database.AddInParameter(RetrieveOrCreateCommand(ID), Name, type, SourceColumn, DataRowVersion.Default /* ::= DataRowVersion.Current */);
        }

        // Now private...

        private void AddInParameter(string name, DbType dbType, object value, String param_cmd_name)
        {
            m_database.AddInParameter(RetrieveOrCreateCommand(param_cmd_name), name, dbType, value);
        }
        private void AddInParameter(string name, DbType dbType, object value)
        {
            AddInParameter(name, dbType, value, null);
        }
        private void AddInParameter(string name, System.Data.DbType dbType)
        {
            AddInParameter(name, dbType, null);
        }
        
        ///////////////////////////////
        ///////////////////////////////
        
        public void AddOutParameter(string name, DbType dbType, int size, String param_cmd_name)
        {
            m_database.AddOutParameter(RetrieveOrCreateCommand(param_cmd_name), name, dbType, size);
        }
        public void AddOutParameter(string name, DbType dbType, int size)
        {
            AddOutParameter(name, dbType, size, null);
        }

        //dn4
        public void AddCursorOutParameter(string CursorName, String param_cmd_name)
        {
            //m_database.AddCursorOutParameter(RetrieveOrCreateCommand(), CursorName);
        }
        public void AddCursorOutParameter(string CursorName)
        {
            AddCursorOutParameter(CursorName, null);
        }
        //-----------------

        public Int32 GetParameterInt32(String param_name, String param_cmd_name)
        {
            Int32 result;
            Object obj = GetParameterValue(param_name, param_cmd_name);
            if (!Int32.TryParse(obj.ToString(), out result))
                throw new InvalidCastException(String.Format("Datatype of parameter {0} cannot be converted to Int32", param_name));

            return result;
        }
        public Int32 GetParameterInt32(String param_name)
        {
            return GetParameterInt32(param_name, null);
        }
        // long (64 - Bit)
        public Int64 GetParameterInt64(String param_name, String param_cmd_name)
        {
            Int64 result;
            Object obj = GetParameterValue(param_name, param_cmd_name);
            if (!Int64.TryParse(obj.ToString(), out result))
                throw new ExceptionDataLayer(String.Format("Datatype of parameter {0} cannot be converted to Int64", param_name));
            return result;
        }
        public Int64 GetParameterInt64(String param_name)
        {
            return GetParameterInt64(param_name, null);
        }
        // Guid
        public Guid GetParameterGuid(String param_name, String param_cmd_name)
        {
            Guid result;
            Object obj = GetParameterValue(param_name, param_cmd_name);
            try
            {
                if (obj == DBNull.Value)
                    return Guid.Empty;
                result = new Guid(obj.ToString());
            }
            catch (Exception)
            {
                throw new ExceptionDataLayer(String.Format("Datatype of parameter {0} cannot be converted to Guid", param_name));
            }
            return result;
        }
        public Guid GetParameterGuid(String param_name)
        {
            return GetParameterGuid(param_name, null);
        }
        // DateTime
        public DateTime GetParameterDateTime(String param_name, String param_cmd_name)
        {
            DateTime result;
            Object obj = GetParameterValue(param_name, param_cmd_name);
            try
            {
                if (obj == DBNull.Value)
                    return DateTime.MinValue;

                if (!DateTime.TryParse(obj.ToString(), out result))
                    throw new ExceptionDataLayer(String.Format("Datatype of parameter {0} cannot be converted to DateTime", param_name)); ;
                return result;
            }
            catch (Exception)
            {
                throw new ExceptionDataLayer(String.Format("Datatype of parameter {0} cannot be converted to DateTime", param_name));
            }
        }
        public DateTime GetParameterDateTime(String param_name)
        {
            return GetParameterDateTime(param_name, null);
        }
        // String
        public String GetParameterString(String param_name, String param_cmd_name)
        {
            String result;
            Object obj = GetParameterValue(param_name, param_cmd_name);

            try
            {
                if (obj == DBNull.Value)
                    return null;
                result = obj.ToString();
            }
            catch (Exception)
            {
                throw new ExceptionDataLayer(String.Format("Datatype of parameter {0} cannot be converted to String", param_name));
            }
            return result;
        }
        public String GetParameterString(String param_name)
        {
            return GetParameterString(param_name, null);
        }
        // private...

        


        private object GetParameterValue(String param_name, DbCommand param_command)
        {
            return m_database.GetParameterValue(param_command, param_name);
        }

        private object GetParameterValue(String param_name, String param_cmd_name)
        {
            return GetParameterValue(param_name, RetrieveOrCreateCommand(param_cmd_name));
        }
        private object GetParameterValue(String param_name)
        {
            return GetParameterValue(param_name, System.String.Empty);
        }
        //-----------------

        public void SetParameterValue(String param_name, object value, String param_cmd_name)
        {
            m_database.SetParameterValue(RetrieveOrCreateCommand(param_cmd_name), param_name, value);
        }
        public void SetParameterValue(String param_name, object value)
        {
            m_database.SetParameterValue(RetrieveOrCreateCommand(), param_name, value);
        }



        private DbCommand GetCommand(String param_sql, String param_cmd_name, CommandType param_commandType)
        {
            DbCommand command = RetrieveOrCreateCommand(param_cmd_name);

            if (param_commandType != CommandType.StoredProcedure &&
                param_commandType != CommandType.Text)
                throw new ExceptionDataLayer("Command type not supported");

            command.CommandType = param_commandType;
            command.CommandText = param_sql;
            command.CommandTimeout = timeout;
            return command;
        }
        
        private void CheckSQLStatement(String param_sql)
        {
            if (string.IsNullOrEmpty(param_sql))
                throw new ExceptionDataLayer("SQL statement must not be empty");
        }

        private void RemoveInParameter()
        {
        }
        private void RemoveOutParameter()
        {
        }

        //ParameterDirection

        public int ExecuteNonQuerySP(String param_sp, String param_command_name)
        {
            return ExecuteNonQuery(param_sp, CommandType.StoredProcedure, param_command_name);
        }
        public int ExecuteNonQuerySP(String param_sp)
        {
            return ExecuteNonQuerySP(param_sp, null);
        }
        public int ExecuteNonQuery(String param_sp, String param_command_name)
        {
            return ExecuteNonQuery(param_sp, CommandType.Text, param_command_name);
        }
        public int ExecuteNonQuery(String param_sql)
        {
            return ExecuteNonQuery(param_sql, null);
        }
        private int ExecuteNonQuery(String param_sql, CommandType param_commandType, String param_command_name)
        {
            CheckSQLStatement(param_sql);
            DbCommand command = GetCommand(param_sql, param_command_name, param_commandType);

            int retVal = 0;
            if (InTransaction())
                retVal = m_database.ExecuteNonQuery(command, m_transaction);
            else
            retVal = m_database.ExecuteNonQuery(command);

            return retVal;
        }


        public Object ExecuteScalarSP(String param_sp)
        {
            return ExecuteScalarSP(param_sp,null);
        }
        public Object ExecuteScalarSP(String param_sp, String param_cmd_name)
        {
            return ExecuteScalar(param_sp, CommandType.StoredProcedure, param_cmd_name);
        }
        public Object ExecuteScalar(String param_sp)
        {
            return ExecuteScalar(param_sp, null);
        }
        public Object ExecuteScalar(String param_sql, String param_command_name)
        {
            return ExecuteScalar(param_sql, CommandType.Text, param_command_name);
        }
        private Object ExecuteScalar(String param_sql, CommandType param_commandType, String param_command_name)
        {
            CheckSQLStatement(param_sql);
            DbCommand command = GetCommand(param_sql, param_command_name, param_commandType);
            
            Object obj = 0;
            if (InTransaction())
                obj = m_database.ExecuteScalar(command, m_transaction);
            else
                obj = m_database.ExecuteScalar(command);

            return obj;
        }


        public IDataReader ExecuteReaderSP(String param_sp)
        {
            return ExecuteReaderSP(param_sp, null);
        }
        public IDataReader ExecuteReaderSP(String param_sp, String param_cmd_name)
        {
            return ExecuteReader(param_sp, CommandType.StoredProcedure, param_cmd_name);
        }
        public IDataReader ExecuteReader(String param_sql)
        {
            return ExecuteReader(param_sql, null);
        }
        public IDataReader ExecuteReader(String param_sql, String param_cmd_name)
        {
            return ExecuteReader(param_sql, CommandType.Text, param_cmd_name);
        }
        private IDataReader ExecuteReader(String param_sql, CommandType param_commandType, String param_cmd_name)
        {
            CheckSQLStatement(param_sql);
            DbCommand command = GetCommand(param_sql, param_cmd_name, param_commandType);

            IDataReader reader;
            if (InTransaction())
                reader = m_database.ExecuteReader(command, m_transaction);
            else
                reader = m_database.ExecuteReader(command);

            return reader;
        }



        public DataSet ExecuteDataSetSP(String param_sp, String param_cmd_name)
        {
            return ExecuteDataSet(param_sp, param_cmd_name, CommandType.StoredProcedure);
        }
        public DataSet ExecuteDataSetSP(String param_sp)
        {
            return ExecuteDataSet(param_sp, null, CommandType.StoredProcedure);
        }
        public DataSet ExecuteDataSet(String param_sp, String param_cmd_name)
        {
            return ExecuteDataSet(param_sp, param_cmd_name, CommandType.Text);
        }
        public DataSet ExecuteDataSet(String param_sql)
        {
            return ExecuteDataSet(param_sql, null, CommandType.Text);
        }
        private DataSet ExecuteDataSet(String param_sql, String param_cmd_name, CommandType param_commandType)
        {
            CheckSQLStatement(param_sql);
            DbCommand command = GetCommand(param_sql, param_cmd_name, param_commandType);

            DataSet dataSet = null;
            if (InTransaction())
                dataSet = m_database.ExecuteDataSet(command, m_transaction);
            else
                dataSet = m_database.ExecuteDataSet(command);
            return dataSet;
        }

        public void LoadDataSet(String param_sql, DataSet dataSet, string[] tableNames)
        {
            LoadDataSet(param_sql, dataSet, tableNames, null);
        }
        public void LoadDataSet(String param_sql, DataSet dataSet, string[] tableNames, String param_cmd_name)
        {
            CheckSQLStatement(param_sql);
            DbCommand command = GetCommand(param_sql, param_cmd_name, CommandType.StoredProcedure);

            if ( null == dataSet )
                    throw new ExceptionDataLayer("DataSet parameter must not be empty");
            if (InTransaction())
                m_database.LoadDataSet(command, dataSet, tableNames,m_transaction);
            else
                m_database.LoadDataSet(command, dataSet, tableNames);
        }

        public int UpdateDataSet(DataSet dataSet, string TableName, String insertID, String updateID, String deleteID)
        {
            if (!InTransaction())
                throw new ExceptionDataLayer("Expect active transaction");

            CheckSQLStatement(TableName);
            if (dataSet == null)
                throw new ExceptionDataLayer("DataSet parameter must not be empty");

            DbCommand insertCommand = GetCommand(insertID, insertID, CommandType.StoredProcedure);
            DbCommand updateCommand = GetCommand(updateID, updateID, CommandType.StoredProcedure);
            DbCommand deleteCommand = GetCommand(deleteID, deleteID, CommandType.StoredProcedure);

            return m_database.UpdateDataSet(dataSet, TableName, insertCommand, updateCommand, deleteCommand, m_transaction);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Remove();
        }
        ~Fleet() {
            Remove();
        }
        #endregion
    }


    public static class FleetContainer
    {
        private static List<IFleet> m_fleet_container = new List<IFleet>();

        private static IFleet AddFleet(IFleet param_fleet)
        {
            lock (m_fleet_container)
            {
                m_fleet_container.Add(param_fleet);
            }
            return param_fleet;
        }
        internal static void RemoveFleet(Fleet param_fleet)
        {
            lock (m_fleet_container)
            {
                foreach (IFleet fleet in m_fleet_container)
                    if (fleet == param_fleet)
                    {
                        m_fleet_container.Remove(fleet);
                        break;
                    }
            }
        }
        static public IFleet FindFleet(String param_fleet_name)
        {
            lock (m_fleet_container)
            {
                foreach (IFleet fleet in m_fleet_container)
                    if (param_fleet_name == fleet.fleetname)
                        return fleet;
            }
            return null;
        }
        static public void Shutdown()
        {
            while (m_fleet_container.Count > 0)
                m_fleet_container[0].Dispose();
        }

        //-------------------------------------------------------------------------------
        private static String getUniqueFleetName()
        {
            return String.Format("Fleet-{0}", Guid.NewGuid().ToString());
        }

        private static void checkUniqueFleetName(String param_fleet_name)
        {
            if (null != FindFleet(param_fleet_name))
                throw new ExceptionDataLayer("Fleet already in-use");
        }
        
        
        //-------------------------------------------------------------------------------
        // Creates fleet based on default enterprise library configuration settings.
        static public IFleet CreateDefault(String param_fleet_name, String param_db_name)
        {
            checkUniqueFleetName(param_fleet_name);
            IFleet fleet = new DefaultFleet(param_db_name == String.Empty ? null : param_db_name, param_fleet_name);
            return AddFleet(fleet);
        }
        static public IFleet CreateDefault(String param_fleet_name)
        {
            return CreateDefault(param_fleet_name, String.Empty);
        }
        static public IFleet CreateDefault()
        {
            return CreateDefault(getUniqueFleetName(), String.Empty);
        }
        
        //-------------------------------------------------------------------------------
        // Create fleet based on the standard perfectus data configuration file. Namely the "PerfectusDataSource".
        // Hence ignores the default selected source for the enterprise library.
        static public IFleet Create(String param_fleet_name, String param_connect_name )
        {
            checkUniqueFleetName( param_fleet_name );

            IConfigurationSource configsource = ConfigurationSourceFactory.Create( "PerfectusDataSource" );
           
            IFleet fleet = new ConfigfileFleet( configsource, param_connect_name, param_fleet_name );
            return AddFleet(fleet);
        }
        static public IFleet Create(String param_fleet_name)
        {
            return Create(param_fleet_name, String.Empty);
        }
        static public IFleet Create()
        {
            return Create(getUniqueFleetName(), String.Empty);
        }
        
        //-------------------------------------------------------------------------------
        static public IFleet Create(String param_fleet_name, String param_connection_string, Type param_type)
        {
            checkUniqueFleetName(param_fleet_name);
            IFleet fleet = new TypedFleet(param_connection_string, param_type);
            return AddFleet(fleet);
        }
        static public IFleet Create(String param_connection_string, Type param_type)
        {
            return Create(getUniqueFleetName(), param_connection_string, param_type);
        }
        //-------------------------------------------------------------------------------
        static public IFleet Create(String param_fleet_name, String param_connection_string, DbProviderFactory param_factory)
        {
            checkUniqueFleetName(param_fleet_name);
            IFleet fleet = new FactoryFleet(param_connection_string, param_factory, param_fleet_name);
            return AddFleet(fleet);
        }
        static public IFleet Create(String param_connection_string, DbProviderFactory param_factory)
        {
            return Create(getUniqueFleetName(), param_connection_string, param_factory);
        }
        //-------------------------------------------------------------------------------
        // Note: 'param_config_file' represents the path to the configuration file.
        // Note: 'param_database_name' represents the name of the connection string in the configuration file.
        static public IFleet CreateWithConfigFile( String param_config_file, String param_database_name )
        {
            FileConfigurationSource filesource = new FileConfigurationSource( param_config_file );
            
            IFleet fleet = new ConfigfileFleet( filesource, param_database_name, getUniqueFleetName( ) );
            return AddFleet( fleet );
        }
        static public IFleet CreateWithConfigFile(String param_config_file)
        {
            return CreateWithConfigFile(param_config_file, null);
        }
        //-------------------------------------------------------------------------------
    }
}


