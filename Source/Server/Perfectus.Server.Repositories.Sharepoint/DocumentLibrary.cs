using System;
using Microsoft.SharePoint;

namespace Perfectus.Sharepoint
{
    /// <summary>
    /// An object that holds information about a Sharepoint DocumentLibrary. Lots of the static methods in the
    /// documentSharing class expect to be passed a 'DocumentLibrary' object.
    /// </summary>
    public class DocumentLibrary
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of a DocumentLibrary Class.
        /// </summary>
        /// <param name="serverAddress">The address of the Sharepoint server the DocumentLibrary belongs to.</param>
        /// <param name="siteName">The name of the Sharepoint site the DocumentLibrary belongs to.</param>
        /// <param name="documentRepositoryName">The name of the Document Library.</param>
        public DocumentLibrary(string spServerAddress, string spSiteName, string spDocumentRepositoryName)
        {
            serverAddress = spServerAddress;
            siteName = spSiteName;
            documentRepositoryName = spDocumentRepositoryName;
        }

        /// <summary>
        /// Creates a new instance of a DocumentLibrary Class.
        /// </summary>
        public DocumentLibrary()
        {
        }

        #endregion

        #region Private properties

        private string serverAddress;
        private string siteName;
        private string documentRepositoryName;

        #endregion

        #region Public properties

        /// <summary>
        /// The address of the Sharepoint server the DocumentLibrary belongs to.
        /// </summary>
        public string ServerAddress
        {
            get { return serverAddress; }
            set { serverAddress = value; }
        }

        /// <summary>
        /// The name of the Sharepoint site the DocumentLibrary belongs to.
        /// </summary>
        public string SiteName
        {
            get { return siteName; }
            set { siteName = value; }
        }

        /// <summary>
        /// The name of the Document Library.
        /// </summary>
        public string DocumentRepositoryName
        {
            get { return documentRepositoryName; }
            set { documentRepositoryName = value; }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// The full path to the DocumentLibrary. Including server, Site and Document repository information.
        /// </summary>
        public string FullPath
        {
            get { return GetFullPath(); }
        }

        /// <summary>
        /// Gets the root Document Library folder.
        /// </summary>
        /// <returns>The root folder in the Document Library</returns>
        public SPFolder GetFolder()
        {
            SPSite siteCollection = null;
            SPWeb site = null;

            siteCollection = new SPSite(serverAddress);
            site = siteCollection.AllWebs[siteName];
            SPFolder folder = site.Folders[documentRepositoryName];

            return folder;
        }

        /// <summary>
        /// Gets the SPWeb that the current DocumentLibrary belongs to.
        /// </summary>
        /// <returns>The SPWeb that the current DocumentLibrary belongs to.</returns>
        public SPWeb GetWeb()
        {
            SPSite siteCollection = null;
            SPWeb site = null;
            siteCollection = new SPSite(serverAddress);
            site = siteCollection.AllWebs[siteName];
            return site;
        }

        /// <summary>
        /// Gets the SPSite that the current DocumentLibrary belongs to.
        /// </summary>
        /// <returns>The SPSite that the current DocumentLibrary belongs to.</returns>
        public SPSite GetSite()
        {
            SPSite siteCollection = null;
            siteCollection = new SPSite(serverAddress);
            return siteCollection;
        }

        /// <summary>
        /// Gets the full path to the DocumentLibrary. Including server, Site and Document repository information.
        /// </summary>
        /// <returns>The full path to the DocumentLibrary. Including server, Site and Document repository information.</returns>
        public string GetFullPath()
        {
            SPWeb web = GetWeb();
            SPFolder folder = GetFolder();
            return web.Url + "/" + folder.Url;
        }

        #endregion
    }
}