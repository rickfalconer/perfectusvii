using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common;

namespace Perfectus.Sharepoint
{
    public static class Utilities
    {
        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        public static string GetResource(string key)
        {
            return GetResource("Perfectus.Sharepoint.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        public static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }
    }
}