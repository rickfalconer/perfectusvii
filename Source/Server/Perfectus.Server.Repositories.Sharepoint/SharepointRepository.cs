using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml.XPath;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.SharePoint;
using Perfectus.Common;
using Perfectus.Common.SharedLibrary;
using Perfectus.Common.SharedLibrary.Configuration;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Sharepoint
{
    public class SharepointRepository : IRepository
    {
        #region Private Member Variables

        // Class level variables
        internal SPWeb mSite = null;
        internal bool isActive = false;
        internal string m_sharedLibraryLoadMessage = string.Empty;
        internal const string csPERFECTUS_TYPE = "PerfectusType";
        internal const string csLIBRARY_UNIQUE_IDENTIFIER = "LibraryUniqueIdentifier";

        // Sharepoint connection setting variables
        internal string msLocation, msSublocation, msPrefectusSharedLibrary;
        internal const string csREPOSITORY_LOCATION = "RepositoryLocation";
        internal const string csREPOSITORY_SUB_LOCATION = "RepositorySubLocation";
        internal const string csREPOSITORY_NAME = "RepositoryName";

        // Sharepoint folder variables
        internal string msSimpleOutcomeFolder, msInterviewPageFolder, msFunctionFolder;
        internal string msOutcomeFolder, msQuestionFolder, msTemplateFolder;
        internal const string csSIMPLE_OUTCOME_FOLDER = "SimpleOutcomeFolder";
        internal const string csINTERVIEW_PAGE_FOLDER = "InterviewPageFolder";
        internal const string csFUNCTION_FOLDER = "FunctionFolder";
        internal const string csOUTCOME_FOLDER = "OutcomeFolder";
        internal const string csQUESTION_FOLDER = "QuestionFolder";
        internal const string csTEMPLATE_FOLDER = "TemplateFolder";
        internal const string csEXTENSION = ".xml";

        // Serialized Xml variables
        internal const string msDEPENDENCY = "Dependancy";
        internal string csFILE_NOT_FOUND = Utilities.GetResource("FNotFound");
        internal string csCHECKED_IN = Utilities.GetResource("None");

        #endregion

        #region Property Methods

        // Properties

        /// <summary>
        ///     Indicates whether the instance is active or not.
        /// </summary>
        /// <returns>Boolean flag indicating the active status of the instance.</returns>
        public bool IsSharedLibraryActivated()
        {
            return isActive;
        }

        /// <summary>
        ///     Indicates whether the instance is active or not.
        /// </summary>
        /// <param name="sharedLibraryLoadMessage">
        /// Provides a feedback mechanism through a string message. Allows some expansion on true or false, (e.g. Why it is false etc).</param>
        /// <returns>Boolean flag indicating the active status of the instance.</returns>
        public bool IsSharedLibraryActivated(out string sharedLibraryLoadMessage)
        {
            sharedLibraryLoadMessage = m_sharedLibraryLoadMessage;
            return isActive;
        }

        /// <summary>
        ///     The Sharepoint web site being used (targeted).
        /// </summary>
        private SPWeb TargetWebSite
        {
            get
            {
                if (mSite == null)
                {
                    SPSite siteCollection = null;

                    try
                    {
                        siteCollection = new SPSite(msLocation);
                        mSite = siteCollection.AllWebs[msSublocation];
                    }
                    finally
                    {
                        if (siteCollection != null)
                        {
                            siteCollection.Dispose();
                        }
                    }
                }

                return mSite;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialises the class, populating the Sharepoint connection settings and repository folder values
        /// </summary>
        public SharepointRepository()
        {
            RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();
            ConnectionSetting connectionSetting = repositoryConfigurationView.GetActiveConnectionSettings();

            // Populate the Sharepoint configuration settings
            msLocation = connectionSetting.ConnectionParameters[csREPOSITORY_LOCATION].Value;
            msSublocation = connectionSetting.ConnectionParameters[csREPOSITORY_SUB_LOCATION].Value;
            msPrefectusSharedLibrary = connectionSetting.ConnectionParameters[csREPOSITORY_NAME].Value;

            // Populate the Sharepoint folder setings
            msSimpleOutcomeFolder = connectionSetting.ConnectionParameters[csSIMPLE_OUTCOME_FOLDER].Value;
            msInterviewPageFolder = connectionSetting.ConnectionParameters[csINTERVIEW_PAGE_FOLDER].Value;
            msFunctionFolder = connectionSetting.ConnectionParameters[csFUNCTION_FOLDER].Value;
            msOutcomeFolder = connectionSetting.ConnectionParameters[csOUTCOME_FOLDER].Value;
            msQuestionFolder = connectionSetting.ConnectionParameters[csQUESTION_FOLDER].Value;
            msTemplateFolder = connectionSetting.ConnectionParameters[csTEMPLATE_FOLDER].Value;

            isActive = repositoryConfigurationView.RepositoryActivated;
        }

        #endregion

        #region Public Members

        #region Upload Methods

        /// <summary>
        ///     Adds Repository items to sharepoint.
        ///     First ensures the given locations exist in sharepoint and then uploads the items.
        ///     The latest version of the item is then obtained and returned to the client. The client can then ensure that
        ///     it has the lastest version as reflected in the library.
        /// </summary>
        /// <param name="repositoryItem">The root repositoryItem that needs to be added to the library.</param>
        /// <remarks>
        ///     On Exception, the original exception will be wrapped as an inner exception within a SharedLibraryException
        ///     and the repositoryItems list being processed will be included as a property.
        /// </remarks>
        /// <returns>A repositoryItem containing the latest version of the items from the Shared Library.</returns>
        public void AddRepositoryItems(RepositoryItem repositoryItem)
        {
            if (repositoryItem.DependentRepositoryItems != null)
            {
                // Iterate through the repository items collection adding them to the library
                foreach (RepositoryItem dependentRepositoryItem in repositoryItem.DependentRepositoryItems)
                {
                    AddRepositoryItems(dependentRepositoryItem);
                }
            }

            AddRepositoryItem(repositoryItem);
        }

        private void AddRepositoryItem(RepositoryItem repositoryItem)   
        {
            bool newItem;
            
            ValidateRepositoryItem(repositoryItem);

            // Ensure the paths are set
            EnsureRepositoryItemPaths(repositoryItem);

            // Add the item to sharepoint
            UploadRepositoryItem(repositoryItem, false, out newItem);

            // Straight away grab the library item from the repository to ensure all SL settings are set and correct
            GetLatestVersion(repositoryItem);
            repositoryItem.IsNewLibraryItem = newItem;
        }

        private void ValidateRepositoryItem(RepositoryItem repositoryItem)
        {
            string message;

            if (repositoryItem.Name == string.Empty || repositoryItem.Name.Length == 0)
            {
                message = Utilities.GetResource("RepositoryNameNotSet");
                throw new SharedLibraryException(message, null);
            }

            //TODO - Other validation tests need to be implemented.
        }

        #endregion

        #region Download Methods

        /// <summary>
        ///		Gets the root folder for the given library item type that contains a reference to the items within the folder
        ///		and subfolders with their respective children being referenced within each object.
        /// </summary>
        /// <param name="type">The type of library item to get.</param>
        /// <returns>
        ///		A root repository folder of the given library type, containing the items at the root level, 
        ///		and subfolders which contain references to their contained items.
        /// </returns>
        public RepositoryFolder GetAllItemsByType(LibraryItemType type)
        {
            RepositoryFolder rootFolder = new RepositoryFolder();

            // Get a reference to the specific shared library type root folder
            SPFolder rootSPFolder;
            rootSPFolder = GetFolder(type);

            // Populate a folder collection of folders (including their child folders etc)
            ProcessFolder(rootSPFolder, rootFolder, type);
            
            return rootFolder;
        }

        /// <summary>
        ///     Gets the latest version of the given repositoryItem from the library along with any dependents.
        /// </summary>
        /// <param name="repositoryItem">The repository Item to be retrieved.</param>
        /// <returns>A repository item including dependencys.</returns>
        public void GetLatestVersion(RepositoryItem repositoryItem)
        {
            List<RepositoryItem> handledRepositoryItems = new List<RepositoryItem>();
            repositoryItem.DependentRepositoryItems = null;
            GetRepositoryItemAndDependents(repositoryItem, handledRepositoryItems);
        }

        #endregion

        #region Check In Methods

        public void CheckIn(RepositoryItem repositoryItem)
        {
            bool newItem;

            if (repositoryItem.DependentRepositoryItems != null)
            {
                // Upload the checkin item and dependents to the library
                foreach (RepositoryItem dependentRepositoryItem in repositoryItem.DependentRepositoryItems)
                {
                    CheckIn(dependentRepositoryItem);
                }
            }

            if (repositoryItem.RelativePath == null)
            {
                EnsureRepositoryItemPaths(repositoryItem);
            }

            if (repositoryItem.DependentObject)
            {
                UploadRepositoryItem(repositoryItem, true, out newItem);

                if (newItem)
                {
                    GetLatestVersion(repositoryItem);
                }

                repositoryItem.IsNewLibraryItem = newItem;
            }
            else
            {
                // All dependent items are now in the shared library - now perform the check in
                string path = GetFullRelativePath(repositoryItem.RelativePath);
                SPFile file = GetFile(path);
                file.SaveBinary(repositoryItem.SerialisedContentsByteArray);
                file.CheckIn(repositoryItem.CheckInComment);

                // Ensure the latest version is in the collection
                GetLatestVersion(repositoryItem);

                // Ensure the title is updated - if the name of the item has changed
                if (file.Item["Title"].ToString() != repositoryItem.Title)
                {
                    file.Item["Title"] = GetNameFromPath(repositoryItem.Title);
                    file.Item.Update();
                }
            }
        }

        public void GetCheckOutStatus(RepositoryItem repositoryItem)
        {
            string path = GetFullRelativePath(repositoryItem.RelativePath);
            SPFile file = GetFile(path);
                
            // Only set the meta data and not the files contents
            SetItemContents(file, repositoryItem, true);
        }

        public void CheckOut(RepositoryItem repositoryItem)
        {
            string path = GetFullRelativePath(repositoryItem.RelativePath);
            SPFile file = GetFile(path);
            file.CheckOut();

            // Get the given repository item
            GetRepositoryItem(repositoryItem);

            // Get the items dependents
            List<RepositoryItem> handledrepositoryItems = new List<RepositoryItem>();
            GetRepositoryItemAndDependents(repositoryItem, handledrepositoryItems);

            // Update the checked out library item
            repositoryItem.CheckOutStatus = CheckOutStatus.CheckedOut;
            repositoryItem.CheckedOutBy = file.CheckedOutBy.Name;
            repositoryItem.CheckedOutByLoginName = file.CheckedOutBy.LoginName;
            repositoryItem.CheckedOutDateTime = file.CheckedOutDate;
            repositoryItem.Version = file.UIVersion;
        }

        public void UndoCheckout(RepositoryItem repositoryItem)
        {
            string path = GetFullRelativePath(repositoryItem.RelativePath);
            SPFile file = GetFile(path);
            file.UndoCheckOut();
        }

        /// <summary>
        ///     Gets the latest version index and then retores the version directly previous.
        ///     This method is usually called after a checkin has thrown an exception and the latest version
        ///     needs to be rolled back.
        /// </summary>
        /// <param name="repositoryItem">The repositoryItem whose latest version needs to be rolled back</param>
        public void RollBackLatestVersion(RepositoryItem repositoryItem)
        {
            string path = GetFullRelativePath(repositoryItem.RelativePath);
            SPFile file = GetFile(path);
            int latestVersionIndex = file.Versions.Count - 1;

            // Rollback a version
            file.Versions.Restore(latestVersionIndex - 1);
        }

        #endregion

        #region Misc Methods

        /// <summary>
        ///		Indicates whether a repositoryItem is currently present in the Shared Library or not.
        ///		Note 
        /// </summary>
        /// <param name="repositoryItem"></param>
        /// <returns>The found repository item</returns>
        /// <remarks>
        ///		An assumption is made that you cannot move items around the shared library, and therefore a check is only
        ///		made on the original location (as given by repositoryItem.RelativePath).
        ///		Although an administrator can manually manipulate the SharedPoint repository from within SharePoint, this is
        ///		not catered for in the functionality of the shared library.	 
        /// </remarks>
        public RepositoryItem DoesRepositoryItemExist(RepositoryItem repositoryItem)
        {
            RepositoryItem foundItem = null;

            if (repositoryItem.DependentRepositoryItems != null)
            {
                foreach (RepositoryItem dependentRepositoryItem in repositoryItem.DependentRepositoryItems)
                {
                    foundItem = DoesRepositoryItemExist(dependentRepositoryItem);

                    if (foundItem != null)
                    {
                        return foundItem;
                    }
                }
            }
            
            if (foundItem == null)
            {
                // Has the item been in the library already & is not linked in the package
                if (repositoryItem.RelativePath != null && !repositoryItem.Linked)
                {
                    // Item has been in the Shared Library already

                    // Attempt to retrieve the file & check that it exists
                    string path = GetFullRelativePath(repositoryItem.RelativePath);
                    SPFile file = TargetWebSite.GetFile(path);

                    if (file.Exists)
                    {
                        return repositoryItem; // repositoryItem is in the shared library
                    }
                    else
                    {
                        //TODO - Assumption: You cannot move items in the library
                        return null; // repositoryItem is not in the shared library 
                    }
                }
            }

            return foundItem;
        }

        /// <summary>
        ///		Creates a new folder at the root or below depending whether the relative path of teh new folder has been set
        /// </summary>
        /// <param name="repositoryFolder">The folder to be created.</param>
        /// <param name="LibraryItemType">The type of folder to create.</param>
        /// <returns>A flag indicating whether the folder was successfully created or not.</returns>
        public bool CreateNewFolder(RepositoryFolder repositoryFolder, LibraryItemType LibraryItemType)
        {
            try
            {
                // Set the true relative path for the shared library in sharepoint
                string rootFolderName = GetFolderTypeName(LibraryItemType);
                repositoryFolder.RelativePath = rootFolderName + "/" + repositoryFolder.RelativePath + "/";
                repositoryFolder.RelativePath = SetRelativePath("", null, repositoryFolder);

                // Create the new folder at relative path location
                EnsureParentFolder(TargetWebSite, repositoryFolder.RelativePath);
                return true; // folder created
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                return false; // folder not created
            }
        }

        #endregion

        #region Additional Admin Functional Methods

        public void Rename(RepositoryItem repositoryItem, string newFileName)
        {
            /*
            try
            {
                /*SPFile file = GetFile(folder + "/" + currentFileName);
                file.CopyTo(folder + "/" + newFileName, true);
			
                SPWeb site = this.TargetWebSite;

                SPList srcList = site.Lists[folder];
                SPListItemCollection listItems = srcList.Items;

                for (int i = listItems.Count - 1; i < 0 ; i--)
                {
                    if (listItems[i].File.Name == repositoryItem.Filename)
                    {
                        listItems.Delete(i);
                    }
                }
            }
            catch (Exception ex)
            {
                /*
				bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				if (rethrow)
					throw ex;
                 
                throw;
            }
            */
        }

        /// <summary>
        /// Deletes a given Library Item file from the shared library.
        /// </summary>
        /// <param name="repositoryItem">The library item to be deleted from the shared library.</param>
        public void Delete(RepositoryItem repositoryItem)
        {
            if (repositoryItem == null)
            {
                return;
            }

            SPWeb site = TargetWebSite;
            string foldername = GetFolderTypeName(repositoryItem.LibraryItemType);

            // Get the folder to delete the file from and delete it
            SPFileCollection files = site.Folders[msPrefectusSharedLibrary].SubFolders[foldername].Files;
            string path = GetFullRelativePath(repositoryItem.RelativePath);

            SPFile file;
            file = TargetWebSite.GetFile(path);
            if ( file.Exists )
            files.Delete(path);
        }

        /// <summary>
        ///     Deletes all the items from the shared library.
        /// </summary>
        public void DeleteAll()
        {
            SPWeb site = TargetWebSite;
            string url;

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msQuestionFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msOutcomeFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msSimpleOutcomeFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msTemplateFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msInterviewPageFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);

            url = site.Folders[msPrefectusSharedLibrary].SubFolders[msFunctionFolder].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);
        }

        /// <summary>
        ///     Deletes all the items from the shared library for the given folder type.
        /// </summary>
        public void DeleteByType(LibraryItemType type)
        {
            SPWeb site = TargetWebSite;
            string foldername, url;

            foldername = GetFolderTypeName(type);
            url = site.Folders[msPrefectusSharedLibrary].SubFolders[foldername].Url;
            site.Folders[msPrefectusSharedLibrary].SubFolders.Delete(url);
        }

        public string Search(string searchText)
        {
            string returnValue = "";

            // Search the target web site for documents containing the search term
            SPSearchResultCollection results = TargetWebSite.SearchDocuments(searchText);
            Int32 count = results.Count - 1;

            // Bring up a string of found values                
            for (int i = 0; i <= count; i++)
            {
                if (i == 0)
                {
                    returnValue = results[i].Title;
                }
                else
                {
                    returnValue += "~" + results[i].Title;
                }
            }

            return returnValue;
        }

        #endregion

        #endregion

        #region Private Members

        private void GetRepositoryItemAndDependents(RepositoryItem repositoryItem, List<RepositoryItem> handledRepositoryItems)
        {
            SPFile file;
            MemoryStream stream = new MemoryStream();
            XPathNodeIterator dependency;

            // Get the xml file from sharepoint
            string path = GetFullRelativePath(repositoryItem.RelativePath);
            file = TargetWebSite.GetFile(path);

            // Set the repository item propertys for the given file
            SetItemContents(file, repositoryItem);
            handledRepositoryItems.Add(repositoryItem);
            
            // Get the dependent items using an Xpath query
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(repositoryItem.SerialisedContents);
            writer.Flush();
            dependency = GetRepositoryItemDependents(stream);

            // Iterate through each dependent item getting their dependent items (using recursion)
            if (dependency != null)
            {
                while (dependency.MoveNext())
                {
                    // Translate the element into a library item and recursively get it's dependents
                    RepositoryItem dependentRepositoryItem = Translate.ToRepositoryItem(dependency.Current);

                    // Get the dependent's dependents (recurive), if a valid item and not already handled & not a package constant
                    if (dependentRepositoryItem != null && !RepositoryItemHasBeenHandled(handledRepositoryItems, dependentRepositoryItem) &&
                        dependentRepositoryItem.PerfectusType != "Perfectus.Common.PackageObjects.PackageConstant")
                    {
                        GetRepositoryItemAndDependents(dependentRepositoryItem, handledRepositoryItems);

                        if (repositoryItem.DependentRepositoryItems == null)
                        {
                            repositoryItem.DependentRepositoryItems = new List<RepositoryItem>();
                        }

                        repositoryItem.DependentRepositoryItems.Add(dependentRepositoryItem);
                    }
                }
            }
        }

        private bool RepositoryItemHasBeenHandled(List<RepositoryItem> handledRepositoryItems, RepositoryItem repositoryItem)
        {
            if (handledRepositoryItems.Contains(repositoryItem))
            {
                return true;
            }

            // Don't reprocess if the item has already been processed
            foreach (RepositoryItem handledRepositoryItem in handledRepositoryItems)
            {
                if (handledRepositoryItem.RelativePath == repositoryItem.RelativePath)
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        ///     Builds up the full relative path and ensures spaces are replaced with + chars
        /// </summary>
        /// <param name="relativePath">the basic relative of the Repository Item / Folder.</param>
        /// <returns>The full relative path including the document library location.</returns>
        private string GetFullRelativePath(string relativePath)
        {
            string path = string.Format("{0}/{1}", msPrefectusSharedLibrary, relativePath);
            return path;
        }

        private XPathNodeIterator GetRepositoryItemDependents(MemoryStream stream)
        {
            stream.Position = 0; // Ensure the document is read from the starting position
            XPathDocument xPathDocument = new XPathDocument(stream);
            XPathNavigator xPathNavigator = xPathDocument.CreateNavigator();

            // Select all the <Dependency> elements and return the iterator
            XPathNodeIterator dependency = xPathNavigator.Select("//" + msDEPENDENCY);
            return dependency;
        }

        private void GetRepositoryItem(RepositoryItem repositoryItem)
        {
            // Get the latest version of the file
            string path = string.Format("{0}/{1}", msPrefectusSharedLibrary, repositoryItem.RelativePath);
            SPFile file = TargetWebSite.GetFile(path);

            if (file.Exists)
            {
                SetItemContents(file, repositoryItem);
            }
            else
            {
                throw new FileNotFoundException(string.Format(csFILE_NOT_FOUND + "{0}", repositoryItem.Filename));
            }
        }
        
        private SPFolder GetFolder(LibraryItemType type)
        {
            SPFolder folder = null;

            // Get a reference to the Shared Library root folder
            SPFolder sharedLibraryFolder = TargetWebSite.GetFolder(msPrefectusSharedLibrary);

            // Get the specific folder type name required
            string folderTypeName = GetFolderTypeName(type);

            // Ensure the folder exists - create it if required (file name needed, but not used)
            EnsureParentFolder(TargetWebSite, SetRelativePath(folderTypeName, "", null));

            // Get the root folder for the library item type
            folder = sharedLibraryFolder.SubFolders[folderTypeName];
            
            return folder;
        }

        private void ProcessFolder(SPFolder folder, RepositoryFolder repositoryFolder, LibraryItemType type)
        {
            // Add all the repository items (files) in the folder into an repository items collection
            foreach (SPFile file in folder.Files)
            {
                // set the files properties into a repository item
                RepositoryItem item;
                item = CreateRepositoryItem(file, type, false);
                repositoryFolder.RepositoryItems.Add(item);
            }

            // Recursively call the process folder to drill down into the directory structure
            foreach (SPFolder subfolder in folder.SubFolders)
            {
                RepositoryFolder repositorySubfolder;
                string relativePath = GetRelativePathFromFilePath(subfolder.ServerRelativeUrl);
                repositorySubfolder = new RepositoryFolder(subfolder.Name, relativePath);

                // Add the sub folder to the folder collection and drill down into it to process
                repositoryFolder.Subfolders.Add(repositorySubfolder);
                ProcessFolder(subfolder, repositorySubfolder, type);
            }
        }

        private RepositoryItem CreateRepositoryItem(SPFile file, LibraryItemType type, bool dependentItem)
        {
            // Create the base item
            string relativePath = GetRelativePathFromFilePath(file.ServerRelativeUrl);
            RepositoryItem item = new RepositoryItem(file.Name, type, relativePath, file.Author.Name, 
                                                     ConvertSPCheckOutStatus(file.CheckedOutBy), file.UIVersion, dependentItem);

            // Set the additional properties
            item.Title = file.Title;
            item.Version = file.UIVersion;
            item.Linked = false; // not linked from the library side - only linked from the IP Explorer side
            item.CreatedDateTime = file.TimeCreated;
            item.CreatedBy = file.Author.Name;
            item.ModifiedBy = file.ModifiedBy.Name;
            item.ModifiedDateTime = file.TimeLastModified;
            item.CheckInComment = file.CheckInComment;

            try
            {
                item.PerfectusType = file.Item[csPERFECTUS_TYPE].ToString();
            }
            catch
            {
                throw new Exception(Utilities.GetResource("PerfectusTypeColumnDoesNotExist"));
            }

            try
            {
                Guid LUID;
                Globals.IsGuid(file.Item[csLIBRARY_UNIQUE_IDENTIFIER].ToString(), out LUID);
                item.LibraryUniqueIdentifier = LUID;
            }
            catch
            {
                throw new Exception(Utilities.GetResource("LUIDColumnDoesNotExist"));
            }

            if (file.CheckedOutBy != null)
            {
                item.CheckedOutByLoginName = file.CheckedOutBy.LoginName;
                item.CheckedOutBy = file.CheckedOutBy.Name;
                item.CheckedOutDateTime = file.CheckedOutDate;
            }

            return item;
        }

        private CheckOutStatus ConvertSPCheckOutStatus(SPUser user)
        {
            // User will be null if the item is not checked out
            if (user != null)
                return CheckOutStatus.CheckedOut;
            else
                return CheckOutStatus.CheckedIn;
        }

        /// <summary>
        ///     Ensures the filename and paths are set for the given repositoryItem.
        /// </summary>
        /// <param name="repositoryItem">The repositoryItem whose filename and paths are to be set (if required).</param>
        internal void EnsureRepositoryItemPaths(RepositoryItem repositoryItem)
        {
            if (repositoryItem.Filename == null)
            {
                repositoryItem.Filename = SetFilename(repositoryItem);
            }

            repositoryItem.RelativePath = SetRelativePath(repositoryItem, repositoryItem.Folder);
        }

        /// <summary>
        /// Returns the filename of the given RepositoryItem.
        /// </summary>
        /// <param name="repositoryItem">A repositoryItem whose filename is to be set.</param>
        /// <returns>The filename of the given repositoryItem.</returns>
        internal string SetFilename(RepositoryItem repositoryItem)
        {
            // Remove chars that are not allowed
            repositoryItem.Name = repositoryItem.Name.Replace("\"", "");
            repositoryItem.Name = repositoryItem.Name.Replace("'", "");

            repositoryItem.Name = repositoryItem.Name.Replace("/", "@");
            repositoryItem.Name = repositoryItem.Name.Replace("\\", "@");

            repositoryItem.Name = repositoryItem.Name.Replace("~", "@");
            repositoryItem.Name = repositoryItem.Name.Replace("<", "@");
            repositoryItem.Name = repositoryItem.Name.Replace(">", "@");
            repositoryItem.Name = repositoryItem.Name.Replace(":", "@");
            repositoryItem.Name = repositoryItem.Name.Replace("!", "@");

            return HttpUtility.UrlEncode(repositoryItem.Name) + csEXTENSION;
        }

        /// <summary>
        /// Returns the relative path of the given library item in sharepoint
        /// </summary>
        /// <param name="repositoryItem">A library item whose relative path is to be discovered.</param>
        /// <param name="repositoryFolder">The folder that the item is in.</param>
        /// <returns>The relative path of the given library item in sharepoint.</returns>
        private string SetRelativePath(RepositoryItem repositoryItem, RepositoryFolder repositoryFolder)
        {
            return SetRelativePath(GetFolderTypeName(repositoryItem.LibraryItemType), repositoryItem.Name, repositoryFolder);
        }

        /// <summary>
        /// Returns the relative path of the given library item in sharepoint. 
        /// In doing so the path is encoded and any % are removed (sharedpoint doesn't handle them).
        /// </summary>
        /// <param name="foldername">The name of the items folder.</param>
        /// <param name="filename">The name of the </param>
        /// <param name="repositoryItem">The repositoryItem that is being set.</param>
        /// <returns>The relative path of the given repositoryItem in sharepoint.</returns>
        private string SetRelativePath(string foldername, string filename, RepositoryFolder repositoryFolder)
        {
            StringBuilder sb = new StringBuilder();

            filename = HttpUtility.UrlEncode(filename);
            
            if (repositoryFolder == null)
            {
                // Root level
                sb.Append(foldername);
                                
                if (filename != null)
                {
                    sb.Append("/");
                    sb.Append(filename);
                    sb.Append(csEXTENSION);
                }
            }
            else
            {
                // Below root level
                sb.Append(repositoryFolder.RelativePath);

                if (filename != null)
                {
                    sb.Append("/");
                    sb.Append(filename);
                    sb.Append(csEXTENSION);
                }
            }

            return sb.ToString();
        }

        private string GetFolderTypeName(LibraryItemType type)
        {
            switch (type)
            {
                case LibraryItemType.SimpleOutcome:
                    return msSimpleOutcomeFolder;
                case LibraryItemType.InterviewPage:
                    return msInterviewPageFolder;
                case LibraryItemType.Function:
                    return msFunctionFolder;
                case LibraryItemType.Outcome:
                    return msOutcomeFolder;
                case LibraryItemType.Question:
                    return msQuestionFolder;
                case LibraryItemType.Template:
                    return msTemplateFolder;
                default:
                    throw new InvalidOperationException(Utilities.GetResource("InvalidFolderType"));
            }
        }

        /// <summary>
        ///		Uploads a given repositoryItem to the shared library
        /// </summary>
        /// <param name="repositoryItem">The repositoryItem to be uploaded.</param>
        /// <param name="isCheckingIn">
        ///		Indicates where the item is being checked in or added. If a root
        ///     item already exists, and is not being checked in a new instance will be created,
        ///     otherwise it will not be readded.
        /// </param>
        /// <returns>A string indicating the author of the repositoryItem.</returns>
        /// <param name="newItem"></param>
        private string UploadRepositoryItem(RepositoryItem repositoryItem, bool isCheckingIn, out bool newItem)
        {
            newItem = false;
            string relativePath = string.Empty;
            string path;

            try
            {
                // Ensure the starting space is removed
                if (repositoryItem.RelativePath.StartsWith(" "))
                {
                    repositoryItem.RelativePath = repositoryItem.RelativePath.Substring(1);
                }

                path = string.Format("{0}/{1}", msPrefectusSharedLibrary, repositoryItem.RelativePath);
                SPFile file = TargetWebSite.GetFile(path);

                // Ensure the file does not already exist and then add it to the given destination
                if (!file.Exists)
                {
                    EnsureParentFolder(TargetWebSite, repositoryItem.RelativePath);

                    // Ensure the item is defaulted to be linked to the shared library
                    repositoryItem.Linked = true;
                    repositoryItem.CheckOutStatus = CheckOutStatus.CheckedIn;

                    try
                    {
                        file = TargetWebSite.Files.Add(path, repositoryItem.SerialisedContentsByteArray);
                        file.Item["Title"] = repositoryItem.Title;
                        file.Item[csPERFECTUS_TYPE] = repositoryItem.PerfectusType;
                        file.Item[csLIBRARY_UNIQUE_IDENTIFIER] = repositoryItem.LibraryUniqueIdentifier.ToString();
                        file.Item.Update();
                    }
                    catch (Exception ex)
                    {
                        if (ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1))
                        {
                            throw new Exception(String.Format(Utilities.GetResource("ErrorUpdatingFileItemProperties"), repositoryItem.Name), ex);
                        }
                    }

                    // Mark the item as new
                    newItem = true;
                    repositoryItem.IsNewLibraryItem = newItem;

                    return file.Author.Name;
                }
                else if (file.Exists && (repositoryItem.DependentObject || isCheckingIn) && 
                         (file.Item[csLIBRARY_UNIQUE_IDENTIFIER].ToString() == repositoryItem.LibraryUniqueIdentifier.ToString()))
                {
                    // Already in the shared library - do not re-add
                    return file.Author.Name;
                }
                else
                {
                    throw new InvalidOperationException(String.Format(Utilities.GetResource("AlreadyExists"), repositoryItem.Name));
                }
            }
            catch (Exception ex)
            {
                // Revert the library item to it's original state
                repositoryItem.Linked = false;
                repositoryItem.CheckOutStatus = CheckOutStatus.CheckedIn;
                repositoryItem.LibraryUniqueIdentifier = Guid.Empty;

                throw ex;
            }
        }

        private string GetNameFromPath(string path)
        {
            int nameStartIndex, nameEndIndex;
            string name;

            nameStartIndex = path.LastIndexOf("/") + 1; // Move to the start position of the name (+1)
            nameEndIndex = path.LastIndexOf(".");

            try
            {
                name = path.Substring(nameStartIndex, (path.Length - (nameStartIndex + (path.Length - nameEndIndex))));
            }
            catch
            {
                name = path;
            }

            return name;
        }

        private string EnsureParentFolder(SPWeb parentSite, string destinationFile)
        {
            string path = string.Format("{0}/{1}", msPrefectusSharedLibrary, destinationFile);
            destinationFile = parentSite.GetFile(path).Url;

            int index = destinationFile.LastIndexOf("/");
            string parentFolderUrl = string.Empty;

            // Ensure the parent folder exists - if not, create it
            if (index > -1)
            {
                parentFolderUrl = destinationFile.Substring(0, index);

                SPFolder parentFolder = parentSite.GetFolder(parentFolderUrl);

                if (!parentFolder.Exists)
                {
                    SPFolder currentFolder = parentSite.RootFolder;

                    foreach (string folder in parentFolderUrl.Split('/'))
                    {
                        currentFolder = currentFolder.SubFolders.Add(folder);
                    }
                }
            }

            return parentFolderUrl;
        }

        /// <summary>
        ///		Sets a repositoryItem from xml and returns it as a repositoryItem object.
        /// </summary>
        /// <param name="file">The Repository Item as a serialized SPFile.</param>
        /// <param name="repositoryItem">The given repositoryItem to set.</param>
        /// <param name="perfectusType">The specific perfectus type (e.g. not just function, but specifically DatePFunction etc.)</param>
        private void SetItemContents(SPFile file, RepositoryItem repositoryItem)
        {
            SetItemContents(file, repositoryItem, false);
        }

        /// <summary>
        ///		Sets a repositoryItem from xml and returns it as a repositoryItem object.
        /// </summary>
        /// <param name="file">The Repository Item as a serialized SPFile.</param>
        /// <param name="repositoryItem">The given repositoryItem to set.</param>
        /// <param name="fileStatusOnly">Flag to indicate whether to populate only the files meta data, or it's contents as well.</param>
        private void SetItemContents(SPFile file, RepositoryItem repositoryItem, bool fileStatusOnly)
        {
            if (!file.Exists)
            {
                throw new FileNotFoundException(String.Format(Utilities.GetResource("FileNotFound"), file.Url));
            }

            // Ensure the library items file properties are update to date
            repositoryItem.Name = file.Name;
            repositoryItem.RelativePath = GetRelativePathFromFilePath(file.Url);
            repositoryItem.CreatedBy = file.Author.Name;
            repositoryItem.Version = file.UIVersion;
            repositoryItem.Linked = true;
            repositoryItem.CreatedDateTime = file.TimeCreated;
            repositoryItem.ModifiedBy = file.ModifiedBy.Name;
            repositoryItem.ModifiedDateTime = file.TimeLastModified;
            repositoryItem.CheckInComment = file.CheckInComment;

            if (repositoryItem.LibraryItemType == LibraryItemType.None)
            {
                repositoryItem.LibraryItemType = DecipherLibraryItemType(repositoryItem.RelativePath);
            }

            // Set the checked in / out status
            if (file.CheckOutStatus.ToString() == csCHECKED_IN)
            {
                repositoryItem.CheckOutStatus = CheckOutStatus.CheckedIn;
            }
            else
            {
                repositoryItem.CheckOutStatus = CheckOutStatus.CheckedOut;
                repositoryItem.CheckedOutDateTime = file.CheckedOutDate;
                repositoryItem.CheckedOutBy = file.CheckedOutBy.Name;
                repositoryItem.CheckedOutByLoginName = file.CheckedOutBy.LoginName;
            }

            if (!fileStatusOnly)
            {
                repositoryItem.SerialisedContentsByteArray = file.OpenBinary();
            }
        }

        private LibraryItemType DecipherLibraryItemType(string relativePath)
        {
            int typeIndex = relativePath.IndexOf("/");
            string libraryType = relativePath.Substring(0, typeIndex);

            if (msQuestionFolder == libraryType)
            {
                return LibraryItemType.Question;
            }
            else if (msOutcomeFolder == libraryType)
            {
                return LibraryItemType.Outcome;
            }
            else if (msTemplateFolder == libraryType)
            {
                return LibraryItemType.Template;
            }
            else if (msSimpleOutcomeFolder == libraryType)
            {
                return LibraryItemType.SimpleOutcome;
            }
            else if (msInterviewPageFolder == libraryType)
            {
                return LibraryItemType.InterviewPage;
            }
            else if (msFunctionFolder == libraryType)
            {
                return LibraryItemType.Function;
            }
            else
            {
                throw new Exception(Utilities.GetResource("RelativePathError"));
            }
        }

        private string GetRelativePathFromFilePath(string path)
        {
            int docLibIndex;
            int startingIndex = 1;
            bool leadingSlash = (path.IndexOf("/", 0) == 0) ? true : false;

            if (leadingSlash)
            {
                path = path.Substring(1, path.Length - 1);
            }

            docLibIndex = path.IndexOf("/") + startingIndex;
            path = path.Substring(docLibIndex, path.Length - docLibIndex);

            return path;
        }
        
        /// <summary>
        ///     Returns a sharepoint file, using the given (sharepoint) file path.
        ///     A fileNotFoundException is thrown if the file does not exist.
        /// </summary>
        /// <param name="filePath">The sharepoint file path of the file.</param>
        /// <returns>A populated SPFile representing the file in sharepoint.</returns>
        private SPFile GetFile(string filePath)
        {
            if (filePath == null)
            {
                throw new NullReferenceException(Utilities.GetResource("FilePathIsNull"));
            }

            SPFile file = TargetWebSite.GetFile(filePath);

            if (!file.Exists)
            {
                throw new FileNotFoundException(String.Format(Utilities.GetResource("FileNotFound"), filePath));
            }

            return file;
        }

        /// <summary>
        /// Wraps a given exception with the library items being processed and sets the original exception's message
        /// to be it's message.
        /// </summary>
        /// <param name="ex">The exception being wrapped.</param>
        /// <param name="repositoryItem">
        ///     The LibraryItem which was being processed when the original exception was thrown.
        /// </param>
        /// <returns>A populated ShardLibraryException</returns>
        private SharedLibraryException SetSharedLibraryException(Exception ex, RepositoryItem repositoryItem)
        {
            SharedLibraryException sharedLibraryException = new SharedLibraryException(ex.Message, ex);
            sharedLibraryException.RepositoryItem = repositoryItem;
            return sharedLibraryException;
        }

        #endregion
    }
}
