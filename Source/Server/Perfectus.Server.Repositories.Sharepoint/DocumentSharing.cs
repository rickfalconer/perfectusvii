using System;
using System.IO;
using Microsoft.SharePoint;
using Perfectus.Common;

namespace Perfectus.Sharepoint
{
    /// <summary>
    /// Contains static methods for saving and retireving files from sharepoint.
    /// </summary>
    public abstract class DocumentSharing
    {
        public DocumentSharing()
        {
        }

        #region Retrieve and Save files from Sharepoint.

        /// <summary>
        /// Gets a file from Sharepoint.
        /// </summary>
        /// <param name="fileRelPath">The relative path to the sharepoint file.</param>
        /// <param name="siteInfo">A SiteInfo object containing information about the sharepoint server, site, document repository, etc.</param>
        /// <param name="fileName">The name of the retrieved file.</param>
        /// <returns>A MemoryStream containing the retrieved file.</returns>
        public static MemoryStream GetFile(string fileRelPath, DocumentLibrary docLib, out string fileName)
        {
            //			SPFolder folder = docLib.GetFolder();
            //
            //			// Get the file from sharepoint.
            //			SPFile file = folder.Files[fileRelPath];
            //			
            //			fileName = file.Name;
            //			
            //			// Turn SPFile into a stream.			
            //			Byte[] fileBytes = file.OpenBinary();				
            //			MemoryStream fileStream = new MemoryStream(fileBytes);
            //	
            //			return fileStream;

            SPWeb web = docLib.GetWeb();

            // Get the file from sharepoint.
            SPFile file = web.GetFile(docLib.DocumentRepositoryName + @"\" + fileRelPath);

            fileName = file.Name;

            // Turn SPFile into a stream.			
            Byte[] fileBytes = file.OpenBinary();
            MemoryStream fileStream = new MemoryStream(fileBytes);

            return fileStream;
        }

        /// <summary>
        /// Saves a file to sharepoint.
        /// </summary>
        /// <param name="relPathToSave">The relative file path to save the file to.</param>
        /// <param name="siteInfo">A SiteInfo object containing information about the sharepoint server, site, document repository, etc.</param>
        /// <param name="stream">A Stream containing the file to save.</param>
        public static void SaveFile(string relPathToSave, DocumentLibrary docLib, Stream stream)
        {
            //			// Get sharepoint folder.
            //			SPFolder folder = docLib.GetFolder();
            //
            //			// turn attachment into byte array
            //			byte[] fileBytes = new byte[stream.Length];
            //						
            //			// saves our stream to array.
            //			ReadWholeArray(stream, fileBytes);
            //
            //			// add file to sharepoint.
            //			folder.Files.Add(relPathToSave, fileBytes);

            // Get sharepoint web
            SPWeb web = docLib.GetWeb();

            // turn attachment into byte array
            byte[] fileBytes = new byte[stream.Length];

            // saves our stream to array.
            ReadWholeArray(stream, fileBytes);

            // add file to sharepoint.
            web.Files.Add(docLib.DocumentRepositoryName + @"\" + relPathToSave, fileBytes);

        }

        #endregion

        #region miscl

        /// <summary>
        /// Reads data into a complete array, throwing an EndOfStreamException
        /// if the stream runs out of data first, or if an IOException
        /// naturally occurs.
        /// </summary>
        /// <param name="stream">The stream to read data from</param>
        /// <param name="data">The array to read bytes into. The array
        /// will be completely filled from the stream, so an appropriate
        /// size must be given.</param>
        private static void ReadWholeArray(Stream stream, byte[] data)
        {
            int offset = 0;
            int remaining = data.Length;
            while (remaining > 0)
            {
                int read = stream.Read(data, offset, remaining);
                if (read <= 0)
                    throw new EndOfStreamException
                        (String.Format(ResourceLoader.GetResourceManager("Perfectus.Sharepoint.Localisation").GetString("EndOfStream"), remaining));
                remaining -= read;
                offset += read;
            }
        }

        #endregion

    }
}