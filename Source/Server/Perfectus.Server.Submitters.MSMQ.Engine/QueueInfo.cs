using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.Submitters.MSMQ.Engine
{
	/// <summary>
	/// Summary description for QueueInfo.
	/// </summary>
	public class QueueInfo
	{
		private string queuePath;
		private string xsltPath;
		private bool isBatch;
		private bool isHigh;
		private string iterateXPath;
		private Submitter.ApplyModeOption applyMode;
		private Submitter.QuestionMatchModeOption matchMode;

		public string QueuePath
		{
			get { return queuePath; }
		}

		public string XsltPath
		{
			get { return xsltPath; }
		}

		public bool IsBatch
		{
			get { return isBatch; }
		}

		public bool IsHigh
		{
			get { return isHigh; }
		}

		public string IterateXPath
		{
			get { return iterateXPath; }
		}

		public Submitter.ApplyModeOption ApplyMode
		{
			get { return applyMode; }
		}

		public Submitter.QuestionMatchModeOption MatchMode
		{
			get { return matchMode; }
		}

		private QueueInfo()
		{
		}
		
		public QueueInfo(string queuePath, string xsltPath, bool isBatch, bool isHigh, string iterateXPath, Submitter.QuestionMatchModeOption matchMode, Submitter.ApplyModeOption applyMode)
		{
			this.queuePath = queuePath;
			this.xsltPath = xsltPath;
			this.isBatch = isBatch;
			this.isHigh = isHigh;
			this.iterateXPath = iterateXPath;
			this.matchMode = matchMode;
			this.applyMode = applyMode;
		}
	}
}
