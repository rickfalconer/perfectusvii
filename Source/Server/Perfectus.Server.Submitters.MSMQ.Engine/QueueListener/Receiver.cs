using System;
using System.Diagnostics;
using System.IO;
using System.Messaging;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Perfectus.Server.API;

namespace Perfectus.Server.Submitters.MSMQ.Engine.QueueListener
{
	/// <summary>
	/// Summary description for Receiver.
	/// </summary>
	public class Receiver
	{
		private static Receiver instance;


		// This is a sub-class (rather than just a ThreadStart delegate) to allow some state (the queue to listen to) to be passed to the thread.
		private class Listener
		{
			private MessageQueue queue;
			private MessageHandlerDelegate handler;
			private ThreadPool threadPool;
			private QueueInfo info;

			public Listener(MessageQueue queue, QueueInfo info, MessageHandlerDelegate handler, ThreadPool threadPool)
			{
				Trace.WriteLine(string.Format("Listener instantiated for queue: {0}", queue.QueueName));
				this.queue = queue;
				this.handler = handler;
				this.threadPool = threadPool;
				this.info = info;
			}


			public Listener(MessageQueue queue, QueueInfo info, MessageHandlerDelegate handler) : this (queue, info, handler, null)
			{
			}

			public void Listen()
			{
				try
				{
					while(true)
					{
						Common.Logging.TraceWrite(string.Format("Waiting for message on queue: {0}", queue.QueueName), "QueueWatcher");
						queue.Formatter = new BinaryMessageFormatter();
				
						// Block this thread until there's a message in the queue.
						queue.Peek();
						Common.Logging.TraceWrite(string.Format("Message detected on queue: {0}", queue.QueueName), "QueueWatcher");

						Common.Logging.TraceWrite("Waiting for thread pool to signal.", "QueueWatcher");
						// Block this thread until the threadpool signals that it's queue has room.
						if (threadPool != null)
							threadPool.autoResetEvent.WaitOne();
						Common.Logging.TraceWrite("Signal received from thread pool.", "QueueWatcher");
					
						Message m = queue.Receive();
						Common.Logging.TraceWrite(string.Format("Message received from queue: {0}", queue.QueueName), "QueueWatcher");
						if (handler != null)
						{
							Common.Logging.TraceWrite(string.Format("Calling Handler delegate: {0}", handler.Method.Name), "QueueWatcher");
					
							if (threadPool != null)
								threadPool.PostRequest(handler, new object[]{m});
							else
								handler(m, info);
						}
						else
						{
							Trace.WriteLine("Handler is null!");
						}
					}
				}
				catch (Exception ex)
				{
					Perfectus.Server.Common.Logging.LogMessage(ex.Message, ex);
				
				}
			}
		}


		private delegate void MessageHandlerDelegate(Message m, QueueInfo info);
		private Config config;
		private ThreadPool threadPool;

		// Prevent instantion with new by making the ctor protected so only this type can make an instance.
		protected Receiver()
		{
			try
			{
				config = (Config) System.Configuration.ConfigurationSettings.GetConfig("queueWatcher");
				
				threadPool = new ThreadPool(config.ThreadPoolSize, config.ThreadPoolSize, "Perfectus Queue Watcher thread pool.");
				threadPool.RequestQueueLimit = config.ThreadPoolSize;
				threadPool.Start();
			}
			catch (Exception ex)
			{
				Perfectus.Server.Common.Logging.LogMessage(ex.Message, ex);
				throw;
			}
		}

		public void Start()
		{
			// Launch a thread to monitor the queue.
			// These are long-running, so we can create some new threads to do it, rather than the threadpool.

			try
			{
				for (int i = 0; i < config.QueuesToWatch.Length; ++i)
				{
					MessageQueue queue = new MessageQueue(config.QueuesToWatch[i].QueuePath);
					Listener listenerIn = new Listener(queue, config.QueuesToWatch[i], new MessageHandlerDelegate(HandleRequestMessage));

					ThreadStart startIn = new ThreadStart(listenerIn.Listen);
					Thread tIn = new Thread(startIn);

					tIn.Name = "Perfectus Queue Watcher.";

					tIn.Start();
				}
			}
			catch (Exception ex)
			{
				Perfectus.Server.Common.Logging.LogMessage(ex.Message, ex);
				throw;
			}
		}


		public void Stop()
		{
			if (threadPool != null)
			{
				threadPool.Stop();
			}
		}

		private void HandleRequestMessage(Message m, QueueInfo info)
		{
			try
			{			
				Common.Logging.TraceWrite(string.Format("Handling a message with the label: {0}", m.Label), "QueueWatcher Message Handler");

				// Submit to Perfectus
				if (info.IsBatch)
				{
					XPathDocument receivedXpath;
					try
					{
						// Get the message body into an XmlDocument
						receivedXpath = new XPathDocument(m.BodyStream);
					}
					catch (Exception ex)
					{
						throw new XmlException("Received message body may not be valid XML. See inner exception for details.", ex);
					}
					XslTransform transform = new XslTransform();
					transform.Load(info.XsltPath);
					
					Submit.SubmitXPathDocumentBatch(receivedXpath, transform, info.IterateXPath, info.MatchMode, info.ApplyMode,
					                                Guid.Empty, true, info.IsHigh);
				}
				else
				{
					XmlDocument receivedXml = new XmlDocument();
					try
					{
						// Get the message body into an XmlDocument
						receivedXml.Load(m.BodyStream);
					}
					catch (Exception ex)
					{
						throw new XmlException("Received message body may not be valid XML. See inner exception for details.", ex);
					}
					XmlDocument answerSetXml;
					// Transform if required
					if (info.XsltPath.Trim().Length == 0)
					{
						answerSetXml = receivedXml;
					}
					else
					{
						// Transform
						XslCompiledTransform transform = new XslCompiledTransform();
						transform.Load(info.XsltPath);

                        MemoryStream memoryStream = new MemoryStream();
                        transform.Transform(receivedXml.CreateNavigator(), null, memoryStream);
                        memoryStream.Position = 0;

                        answerSetXml = new XmlDocument();
                        answerSetXml.Load(memoryStream);

						//XmlReader rdr = transform.Transform(receivedXml.CreateNavigator(), null, new XmlUrlResolver());
						//answerSetXml = new XmlDocument();
						//answerSetXml.Load(rdr);					
                    }

					Submit.SubmitAnswersetDocument(answerSetXml, info.MatchMode, info.ApplyMode, Guid.Empty, true, info.IsHigh);
				}				
			}
			catch (Exception ex)
			{
				Perfectus.Server.Common.Logging.LogMessage(ex.Message, ex);		
			}
			finally
			{
				m.Dispose();			
			}
		}


		public static Receiver GetInstance()
		{
			if (null == instance)
			{
				instance = new Receiver();
			}
			return instance;
		}
	}
}
