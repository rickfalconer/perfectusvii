using System;
using System.Configuration;
using System.Diagnostics;
using System.Xml;

namespace Skanska.HandbookGenerator
{
	/// <summary>
	/// Summary description for Engine.
	/// </summary>
	public class Engine
	{
		public Engine()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public static void HandleJob(XmlDocument inputAnswerSet)
		{
			Trace.WriteLine("Entering HandleJob");
			// Get the names of our special nodes.
			string definitionQuestionName = ConfigurationSettings.AppSettings["handbookDefinitionQuestionName"];
			if (definitionQuestionName == null)
			{
				Trace.WriteLine(" *** handbookDefinitionQuestionName not specified in config file.");
				return;
			}
			string clauseQuestionName = ConfigurationSettings.AppSettings["clauseToInsertQuestionName"];
			if (clauseQuestionName == null)
			{
				Trace.WriteLine(" *** clauseToInsertQuestionName not specified in config file.");
				return;
			}
			
			string docTypeQuestionName = ConfigurationSettings.AppSettings["documentTypeToCreateQuestionName"];
			if (docTypeQuestionName == null)
			{
				Trace.WriteLine(" *** documentTypeToCreateQuestionName not specified in config file.");
				return;
			}
			
			
			Trace.WriteLine(string.Format(" Will look for data in the question: {0}", definitionQuestionName));
			Trace.WriteLine(string.Format(" Will pass the clause in the question: {0}", clauseQuestionName));
			Trace.WriteLine(string.Format(" Will pass the doc type in the question: {0}", docTypeQuestionName));
			// Find the node in our answerSet - this is the root of all our data
			XmlNode questionNode = inputAnswerSet.SelectSingleNode(string.Format("//answer[@name='{0}']", definitionQuestionName));
			if (questionNode == null)
			{
				Trace.WriteLine(" *** Definition question not found in AnswerSet.");
				return;
			}
			
			Trace.Write(" Creating base output answerSet");
			XmlDocument outputAnswerSet = (XmlDocument)inputAnswerSet.Clone();
			// Remove our special answer from the output answerset - it could be huge and it's not used by the assembly side of things.
			XmlNode outputQuestionNode =
				outputAnswerSet.SelectSingleNode(string.Format("//answer[@name='{0}']", definitionQuestionName));
			outputAnswerSet.RemoveChild(outputQuestionNode);
			
			// If our other special questions don't exist already in the output, create them.
			XmlNode clauseQuestion =
				outputAnswerSet.SelectSingleNode(string.Format("//answer[@name='{0}']", definitionQuestionName));
			if (clauseQuestion == null)
			{
				Trace.WriteLine(" Clause question not present in answerset, adding it...");
				//clauseQuestion.AppendChild(new XmlAttribute(string.Empty, "name", string.Empty, outputAnswerSet));
				clauseQuestion = outputAnswerSet.CreateElement("answer");
				((XmlElement)clauseQuestion).SetAttribute("name", clauseQuestionName);
					
			}
			
			Trace.WriteLine("Leaving HandleJob");
			
		}
	}
}
