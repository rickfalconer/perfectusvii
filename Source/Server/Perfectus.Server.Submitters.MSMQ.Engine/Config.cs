using System;

namespace Perfectus.Server.Submitters.MSMQ.Engine
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		private QueueInfo[] queuesToWatch;
		private int threadPoolSize;

		public QueueInfo[] QueuesToWatch
		{
			get { return queuesToWatch; }
		}

		public int ThreadPoolSize
		{
			get { return threadPoolSize; }
		}

		private Config()
		{
		}
		
		public Config(QueueInfo[] queuesToWatch, int threadPoolSize)
		{
			this.queuesToWatch = queuesToWatch;
			this.threadPoolSize = threadPoolSize;
		}
	}
}
