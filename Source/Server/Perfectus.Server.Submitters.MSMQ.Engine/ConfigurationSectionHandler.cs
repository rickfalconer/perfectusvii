using System;
using System.Xml;
using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.Submitters.MSMQ.Engine
{
	/// <summary>
	/// Summary description for ConfigurationSectionHandler.
	/// </summary>
	public class ConfigurationSectionHandler : System.Configuration.IConfigurationSectionHandler
	{
		public ConfigurationSectionHandler()
		{
		}


		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			int poolSize = Convert.ToInt32(section.SelectSingleNode("threadPoolSize").InnerText);
			
			XmlNodeList nodes = section.SelectNodes("queuesToWatch/queue");
			QueueInfo[] queues = new QueueInfo[nodes.Count];
			for(int i = 0; i < nodes.Count; ++i)
			{
				string path = nodes[i].Attributes["path"].InnerText;
				string xslt = nodes[i].Attributes["xsltPath"].InnerText;
				bool isBatch = bool.Parse(nodes[i].Attributes["isBatch"].InnerText);
				bool isHigh = bool.Parse(nodes[i].Attributes["submitHighPriority"].InnerText);
				string iterateXPath = nodes[i].Attributes["iterateXPath"].InnerText;
				string matchModeString = nodes[i].Attributes["questionMatchMode"].InnerText;
				Submitter.QuestionMatchModeOption matchMode = matchModeString == "Name"
				                                              	? Submitter.QuestionMatchModeOption.Name
				                                              	: Submitter.QuestionMatchModeOption.Guid;
				string applyModeString = nodes[i].Attributes["applyMode"].InnerText;
				Submitter.ApplyModeOption applyMode = applyModeString == "Raw"
				                                      	? Submitter.ApplyModeOption.Raw
				                                      	: Submitter.ApplyModeOption.SimulateInterview;
				QueueInfo info = new QueueInfo(path, xslt, isBatch, isHigh, iterateXPath, matchMode, applyMode);
				queues[i] = info;
			}
			
			return new Config(queues, poolSize);
			
			//return Config.LoadFrom(sharedConfigPath);
		}
	}
}
