using System;
using System.Diagnostics;
using System.Xml;
using Perfectus.Server.Submitters.MSMQ.Engine.QueueListener;

namespace Perfectus.Server.Submitters.MSMQ.Engine.Console
{
	/// <summary>
	/// Summary description for EntryPoint.
	/// </summary>
	class EntryPoint
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			System.Console.WriteLine(Perfectus.Common.About.ConsoleString);

			Trace.Listeners.Add(new TextWriterTraceListener(System.Console.Out));
			Receiver receiver = Receiver.GetInstance();
			receiver.Start();
		}
	}
}
