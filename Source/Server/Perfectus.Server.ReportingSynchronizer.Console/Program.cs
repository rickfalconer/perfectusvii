using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.ReportingSynchronizerEngine;

namespace Perfectus.Server.ReportingSynchronizerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dateToGetPackagesFrom;
            bool getIncompletePackages;

            try
            {               
                dateToGetPackagesFrom = DateTime.Now.AddDays(- Settings1.Default.DaysPreviousToGetPackagesBy);
                getIncompletePackages = Settings1.Default.GetIncompletePackages;
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error loading settings from configuration file. The following exception was returned: " + ex.ToString());
                return;
            }

            AnswerSynchronizer answerSynchronizer = new AnswerSynchronizer(dateToGetPackagesFrom, getIncompletePackages);
            answerSynchronizer.Error += new ErrorHandler(answerSynchronizer_Error);
            answerSynchronizer.Feedback += new FeedbackHandler(answerSynchronizer_Feedback);
            answerSynchronizer.Synchronize();            
        }

        static void answerSynchronizer_Feedback(FeedbackHandlerArgs e)
        {
            Console.WriteLine(e.Message);
        }

        static void answerSynchronizer_Error(ErrorHandlerArgs e)
        {
            Console.WriteLine(e.Message + " The following exception was returned: " + e.Exception.ToString());
            //TODO: Log using enterprise library logging block.
        }
    }
}
