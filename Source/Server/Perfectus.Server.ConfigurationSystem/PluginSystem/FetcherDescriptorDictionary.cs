using System;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// A dictionary with keys of type string and values of type FetcherDescriptor
	/// </summary>
	public class FetcherDescriptorDictionary: System.Collections.DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FetcherDescriptorDictionary class
		/// </summary>
		public FetcherDescriptorDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the FetcherDescriptor associated with the given string
		/// </summary>
		/// <param name="key">
		/// The string whose value to get or set.
		/// </param>
		public virtual FetcherDescriptor this[string key]
		{
			get
			{
				return (FetcherDescriptor) this.Dictionary[key];
			}
			set
			{
				this.Dictionary[key] = value;
			}
		}

		/// <summary>
		/// Adds an element with the specified key and value to this FetcherDescriptorDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to add.
		/// </param>
		/// <param name="value">
		/// The FetcherDescriptor value of the element to add.
		/// </param>
		public virtual void Add(string key, FetcherDescriptor value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this FetcherDescriptorDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this FetcherDescriptorDictionary.
		/// </param>
		/// <returns>
		/// true if this FetcherDescriptorDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this FetcherDescriptorDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this FetcherDescriptorDictionary.
		/// </param>
		/// <returns>
		/// true if this FetcherDescriptorDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this FetcherDescriptorDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The FetcherDescriptor value to locate in this FetcherDescriptorDictionary.
		/// </param>
		/// <returns>
		/// true if this FetcherDescriptorDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(FetcherDescriptor value)
		{
			foreach (FetcherDescriptor item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this FetcherDescriptorDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to remove.
		/// </param>
		public virtual void Remove(string key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this FetcherDescriptorDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Keys
		{
			get
			{
				return this.Dictionary.Keys;
			}
		}

		/// <summary>
		/// Gets a collection containing the values in this FetcherDescriptorDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Values
		{
			get
			{
				return this.Dictionary.Values;
			}
		}
	}
}
