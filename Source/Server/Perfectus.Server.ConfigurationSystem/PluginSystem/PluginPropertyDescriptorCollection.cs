using System;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// A collection of elements of type PluginPropertyDescriptor
	/// </summary>
	public class PluginPropertyDescriptorCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the PluginPropertyDescriptorCollection class.
		/// </summary>
		public PluginPropertyDescriptorCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PluginPropertyDescriptorCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PluginPropertyDescriptorCollection.
		/// </param>
		public PluginPropertyDescriptorCollection(PluginPropertyDescriptor[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PluginPropertyDescriptorCollection class, containing elements
		/// copied from another instance of PluginPropertyDescriptorCollection
		/// </summary>
		/// <param name="items">
		/// The PluginPropertyDescriptorCollection whose elements are to be added to the new PluginPropertyDescriptorCollection.
		/// </param>
		public PluginPropertyDescriptorCollection(PluginPropertyDescriptorCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PluginPropertyDescriptorCollection.
		/// </param>
		public virtual void AddRange(PluginPropertyDescriptor[] items)
		{
			foreach (PluginPropertyDescriptor item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PluginPropertyDescriptorCollection to the end of this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <param name="items">
		/// The PluginPropertyDescriptorCollection whose elements are to be added to the end of this PluginPropertyDescriptorCollection.
		/// </param>
		public virtual void AddRange(PluginPropertyDescriptorCollection items)
		{
			foreach (PluginPropertyDescriptor item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type PluginPropertyDescriptor to the end of this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginPropertyDescriptor to be added to the end of this PluginPropertyDescriptorCollection.
		/// </param>
		public virtual void Add(PluginPropertyDescriptor value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic PluginPropertyDescriptor value is in this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginPropertyDescriptor value to locate in this PluginPropertyDescriptorCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PluginPropertyDescriptorCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(PluginPropertyDescriptor value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PluginPropertyDescriptorCollection
		/// </summary>
		/// <param name="value">
		/// The PluginPropertyDescriptor value to locate in the PluginPropertyDescriptorCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(PluginPropertyDescriptor value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PluginPropertyDescriptorCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the PluginPropertyDescriptor is to be inserted.
		/// </param>
		/// <param name="value">
		/// The PluginPropertyDescriptor to insert.
		/// </param>
		public virtual void Insert(int index, PluginPropertyDescriptor value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the PluginPropertyDescriptor at the given index in this PluginPropertyDescriptorCollection.
		/// </summary>
		public virtual PluginPropertyDescriptor this[int index]
		{
			get
			{
				return (PluginPropertyDescriptor) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific PluginPropertyDescriptor from this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginPropertyDescriptor value to remove from this PluginPropertyDescriptorCollection.
		/// </param>
		public virtual void Remove(PluginPropertyDescriptor value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PluginPropertyDescriptorCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(PluginPropertyDescriptorCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public PluginPropertyDescriptor Current
			{
				get
				{
					return (PluginPropertyDescriptor) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (PluginPropertyDescriptor) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PluginPropertyDescriptorCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual PluginPropertyDescriptorCollection.Enumerator GetEnumerator()
		{
			return new PluginPropertyDescriptorCollection.Enumerator(this);
		}
	}
}
