using System;
using System.Xml.Serialization;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// A collection of elements of type PluginDescriptor
	/// </summary>
		 public class PluginDescriptorCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the PluginDescriptorCollection class.
		/// </summary>
		public PluginDescriptorCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PluginDescriptorCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PluginDescriptorCollection.
		/// </param>
		public PluginDescriptorCollection(PluginDescriptor[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PluginDescriptorCollection class, containing elements
		/// copied from another instance of PluginDescriptorCollection
		/// </summary>
		/// <param name="items">
		/// The PluginDescriptorCollection whose elements are to be added to the new PluginDescriptorCollection.
		/// </param>
		public PluginDescriptorCollection(PluginDescriptorCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this PluginDescriptorCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PluginDescriptorCollection.
		/// </param>
		public virtual void AddRange(PluginDescriptor[] items)
		{
			foreach (PluginDescriptor item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PluginDescriptorCollection to the end of this PluginDescriptorCollection.
		/// </summary>
		/// <param name="items">
		/// The PluginDescriptorCollection whose elements are to be added to the end of this PluginDescriptorCollection.
		/// </param>
		public virtual void AddRange(PluginDescriptorCollection items)
		{
			foreach (PluginDescriptor item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type PluginDescriptor to the end of this PluginDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginDescriptor to be added to the end of this PluginDescriptorCollection.
		/// </param>
		public virtual void Add(PluginDescriptor value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic PluginDescriptor value is in this PluginDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginDescriptor value to locate in this PluginDescriptorCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PluginDescriptorCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(PluginDescriptor value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PluginDescriptorCollection
		/// </summary>
		/// <param name="value">
		/// The PluginDescriptor value to locate in the PluginDescriptorCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(PluginDescriptor value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PluginDescriptorCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the PluginDescriptor is to be inserted.
		/// </param>
		/// <param name="value">
		/// The PluginDescriptor to insert.
		/// </param>
		public virtual void Insert(int index, PluginDescriptor value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the PluginDescriptor at the given index in this PluginDescriptorCollection.
		/// </summary>
		public virtual PluginDescriptor this[int index]
		{
			get
			{
				return (PluginDescriptor) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific PluginDescriptor from this PluginDescriptorCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginDescriptor value to remove from this PluginDescriptorCollection.
		/// </param>
		public virtual void Remove(PluginDescriptor value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PluginDescriptorCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(PluginDescriptorCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public PluginDescriptor Current
			{
				get
				{
					return (PluginDescriptor) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (PluginDescriptor) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PluginDescriptorCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual PluginDescriptorCollection.Enumerator GetEnumerator()
		{
			return new PluginDescriptorCollection.Enumerator(this);
		}
	}

}
