using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	public enum PluginBehaviour
	{
		Unknown,
		Converter,
		Distributor,
        InstanceComplete
	};

	/// <summary>
	/// Summary description for PluginDescriptor.
	/// </summary>
	
	[XmlType(Namespace="http://www.perfectus.net/serviceDefinitions/Perfectus.Server.WebAPI.Configuration" +
		 "System")]
	public sealed class PluginDescriptor
	{
		private object pluginInstance;
		private PluginPropertyDescriptorCollection propertyDescriptors = new PluginPropertyDescriptorCollection();
		private perfectusServerConfigurationPluginsPlugin pluginConfigInfo;
		private PluginBehaviour pluginKind = PluginBehaviour.Unknown;
		private string typeName;
		private string friendlyName;
		private string fullName;
		private string preferredDoc;
		private Guid uniqueIdentifier;

		public PluginBehaviour PluginKind
		{
			get { return pluginKind; }
			set { pluginKind = value; }
		}

		public string PreferredDoc
		{
			get { return preferredDoc; }
			set { preferredDoc = value; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
			set { uniqueIdentifier = value; }
		}

		private string path;

		public string FullName
		{
			get { return fullName; }
			set { fullName = value; }
		}

		public string Path
		{
			get { return path; }
			set { path = value; }
		}

		public PluginPropertyDescriptorCollection PropertyDescriptors
		{
			get { return propertyDescriptors; }
		}

		public string TypeName
		{
			get { return typeName; }
			set { typeName = value; }
		}

		public string FriendlyName
		{
			get { return friendlyName; }
			set { friendlyName = value; }
		}

		public PluginDescriptor()
		{
		}

		public PluginDescriptor(object pluginInstance, perfectusServerConfigurationPluginsPlugin pConfig)
		{
			this.pluginInstance = pluginInstance;
			pluginConfigInfo = pConfig;
			Reset();
		}

		private void Reset()
		{
			
			Type pluginType = pluginInstance.GetType();
			typeName = pluginType.FullName;
			fullName = pluginType.AssemblyQualifiedName;
			friendlyName = (pluginConfigInfo.GetLocalisedName());

			if (pluginInstance is IConverter)
			{
				pluginKind = PluginBehaviour.Converter;
				uniqueIdentifier = ((IConverter)pluginInstance).UniqueIdentifier;
				preferredDoc = Enum.GetName(typeof (SourceDocument), ((IConverter) pluginInstance).DocumentTypeConverted);
			}
            else if (pluginInstance is IDistributor)
            {
                pluginKind = PluginBehaviour.Distributor;
                uniqueIdentifier = ((IDistributor) pluginInstance).UniqueIdentifier;
            }
            else if (pluginInstance is IInstanceCompletionNotifier)
            {
                pluginKind = PluginBehaviour.InstanceComplete;
                uniqueIdentifier = Guid.NewGuid();
            }


		    foreach (PropertyDescriptor pd in TypeDescriptor.GetProperties(pluginInstance))
			{
				if (pd.IsBrowsable)
				{
					propertyDescriptors.Add(new PluginPropertyDescriptor(pd, this, pluginConfigInfo));
				}
			}
		}
	}
}