using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// Summary description for RelativePath.
	/// </summary>
	public class RelativePath
	{
		[DllImport("shlwapi.dll", 
			 EntryPoint="PathRelativePathTo", 
			 ExactSpelling=false, 
			 CharSet=System.Runtime.InteropServices.CharSet.Auto, 
			 SetLastError=true, 
			 ThrowOnUnmappableChar = true)]
		private static extern long PathRelativePathTo(StringBuilder pszPath, string pszFrom, FileAttributes dwAttrFrom, string pszTo, FileAttributes dwAttrTo);

		public static string Calculate(string from, string to)
		{
			//string tmp = new string('\0', 260);
			StringBuilder tmp = new StringBuilder(260);
			
			from = from.Replace("/", "\\");
			to = to.Trim('\\');
			from = from.Trim('\\');

			long ret = PathRelativePathTo(tmp, from, FileAttributes.Directory, to, FileAttributes.Directory);
			string retStr = tmp.ToString();
			return retStr.Trim();
		}
	}
}
