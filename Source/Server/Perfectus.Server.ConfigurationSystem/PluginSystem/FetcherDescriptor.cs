using System;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// Summary description for FetcherDescriptor.
	/// </summary>
	public class FetcherDescriptor
	{
		private string key;
		private string type;
		private string path;

		private FetcherDescriptor()
		{
		}

		public string Key
		{
			get { return key; }
		}

		public string Type
		{
			get { return type; }
		}

		public string Path
		{
			get { return path; }
		}

		public FetcherDescriptor(string key, string type, string path)
		{
			this.key = key;
			this.type = type;
			this.path = path;
		}
	}
}
