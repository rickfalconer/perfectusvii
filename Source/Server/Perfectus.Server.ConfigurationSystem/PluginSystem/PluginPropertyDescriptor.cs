using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace Perfectus.Server.ConfigurationSystem.PluginSystem
{
	/// <summary>
	/// Summary description for PluginPropertyDescriptor.
	/// </summary>
	
	//TODO: Localisation approach for the plugin interface.  Localised attributes for friendly names, property names etc.?
	public class PluginPropertyDescriptor
	{
	//	private PropertyInfo pi;
		private perfectusServerConfigurationPluginsPlugin pluginConfig;
		private string editor;
		private string converter;
		private string typeName;
		private string name;
		private string displayName;
		private PropertyDescriptor pd;

		public string Name
		{
			get {return name; }
			set {name = value;}
		}

		public string DisplayName
		{
			get { return displayName; }
			set { displayName = value; }
		}

		public string TypeName
		{
			get {return typeName;}
			set {typeName = value;}
		}

		public string Editor
		{
			get {return editor;}
			set { editor = value;}
		}

		public string Converter
		{
			get { return converter; }
			set { converter = value; }
		}

		private object defaultValue = null;
        private String description = null;
        private bool _readOnly = false;
		private bool visible = true;

        public String Description
        {
            get { return description; }
            set { description = value; }
        }

		public object DefaultValue
		{
			get {return defaultValue;}
			set {defaultValue = value;}
		}

		public bool ReadOnly
		{
			get { return _readOnly;}
			set {_readOnly = value;}
		}

		public bool Visible
		{
			get { return visible;}
			set { visible = value;}
		}

		public PluginPropertyDescriptor()
		{
		}


		private PluginDescriptor parentPlugin;

//		public PluginPropertyDescriptor(PropertyInfo pi, PluginDescriptor parentPlugin)
//		{
//			if (pi == null)
//			{
//				throw new ArgumentNullException("pi");
//			}
//			else
//			{
//				this.parentPlugin = parentPlugin;
//				name = pi.Name;
//				typeName = pi.PropertyType.AssemblyQualifiedName;
//				Reset();
//			}
//		}

		public PluginPropertyDescriptor(PropertyDescriptor pd, PluginDescriptor parentPlugin, perfectusServerConfigurationPluginsPlugin pluginConfig)
		{
			if (pd == null)
			{
				throw new ArgumentNullException("pd");
			}
			else
			{
				this.pluginConfig = pluginConfig;
				this.parentPlugin = parentPlugin;
				this.pd = pd;
				name = pd.Name;
				typeName = pd.PropertyType.AssemblyQualifiedName;
				
				Reset();
			}
		}


		private void Reset()
		{
            Attribute cAttr = pd.Attributes[typeof(TypeConverterAttribute)];//Attribute.GetCustomAttribute(pi, typeof(TypeConverterAttribute), true);
			if (cAttr != null)
			{
				TypeConverterAttribute tca = cAttr as TypeConverterAttribute;
				if (tca != null)
				{
					converter = tca.ConverterTypeName;
				}
			}

			Attribute eAttr = pd.Attributes[typeof(EditorAttribute)];//Attribute.GetCustomAttribute(pi, typeof(EditorAttribute), true);
			if (eAttr != null)
			{
				EditorAttribute ea = eAttr as EditorAttribute;
				if (ea != null)
				{
					editor = ea.EditorTypeName;
				}
			}

            Attribute dAttr = pd.Attributes[typeof(DefaultValueAttribute)];
            if (dAttr != null)
            {
                DefaultValueAttribute da = dAttr as DefaultValueAttribute;
                if (da != null)
                {
                    defaultValue = da.Value;
                }
            }

            Attribute rAttr = pd.Attributes[typeof(ReadOnlyAttribute)];
            if (rAttr != null)
            {
                ReadOnlyAttribute ra = rAttr as ReadOnlyAttribute;
                if (ra != null)
                {
                    _readOnly = ra.IsReadOnly;
                }
            }
            
            dAttr = pd.Attributes[typeof(DescriptionAttribute)];
            if (dAttr != null)
            {
                DescriptionAttribute da = dAttr as DescriptionAttribute;
                if (da != null)
                {
                    description = da.Description;
                }
            }

			displayName = pd.DisplayName;

			// Find any overrides in the configuration file
			
			if (pluginConfig != null && pluginConfig.propertyOverrides != null && pluginConfig.propertyOverrides.property != null)
			{
				foreach(perfectusServerConfigurationPluginsPluginPropertyOverridesProperty propOverride in pluginConfig.propertyOverrides.property)
				{
					if (propOverride.name.ToLower(CultureInfo.InvariantCulture) == this.name.ToLower(CultureInfo.InvariantCulture) ||
                        propOverride.name.ToLower(CultureInfo.InvariantCulture) == this.DisplayName.ToLower(CultureInfo.InvariantCulture))
					{
						this._readOnly = propOverride.readOnly;
						this.visible = propOverride.visible;
						this.defaultValue = propOverride.defaultValue;
					}
				}
			}
            // Bug - deserialisation in interview fails if property is marked read-only
            // no idea why
			this._readOnly = false;
		}
	}
}
