using System;

namespace Perfectus.Server.ConfigurationSystem.ServerSystem
{
	[Serializable]
	public class ServerPublishInfo
	{
		public string AssemblyName = Config.Shared.ipmanager.publishControl.assemblyName;
		public string TypeName = Config.Shared.ipmanager.publishControl.typeName;
	}
}
