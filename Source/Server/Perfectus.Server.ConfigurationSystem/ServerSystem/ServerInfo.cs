using System;
using System.Reflection;
using System.Xml.Serialization;
using Perfectus.Common;

namespace Perfectus.Server.ConfigurationSystem.ServerSystem
{
	/// <summary>
	/// Summary description for ServerInfo.
	/// </summary>
	[Serializable]
	[XmlType(Namespace="http://tempuri.org/")]
	public class ServerInfo
	{
		public string Name = Config.Shared.identity.serverName;
		public string Message = Config.Shared.identity.serverMessage;
        public Guid UniqueIdentifier = Config.Shared.identity.uniqueIdentifier; 
        public int VersionMajor = Assembly.GetExecutingAssembly().GetName().Version.Major;
		public int VersionMinor = Assembly.GetExecutingAssembly().GetName().Version.Minor;
		public int VersionBuild = Assembly.GetExecutingAssembly().GetName().Version.Build;
		public int VersionRevision = Assembly.GetExecutingAssembly().GetName().Version.Revision;
		public string InterviewAddress = null; // Not required in 4.2
		public string MyDocumentsURL = Config.Shared.identity.myDocumentsURL;
        public string InformationalVersion = ((AssemblyInformationalVersionAttribute)AssemblyInformationalVersionAttribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyInformationalVersionAttribute))).InformationalVersion;	
	}
}
