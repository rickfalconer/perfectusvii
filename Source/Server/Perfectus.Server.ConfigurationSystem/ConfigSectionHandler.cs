using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Perfectus.Server.ConfigurationSystem
{
	/// <summary>
	/// Summary description for SectionHandler.
	/// </summary>
	public class ConfigSectionHandler : System.Configuration.IConfigurationSectionHandler
	{
		public ConfigSectionHandler()
		{
		}

		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			string sharedConfigPath = section.SelectSingleNode("sharedConfigPath").InnerText;
			return Config.LoadFrom(sharedConfigPath);
		}

		#endregion
	}
}
