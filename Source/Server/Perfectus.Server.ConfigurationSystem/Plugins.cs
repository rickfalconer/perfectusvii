using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using Perfectus.Server.ConfigurationSystem.PluginSystem;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.ConfigurationSystem
{
	/// <summary>
	/// Summary description for Plugins.
	/// </summary>
	public sealed class Plugins
	{
//		private static readonly string cacheKey = "Perfectus.Server.PackageManager.Plugins";
		private static readonly string pluginFolder = Config.Shared.plugins.basePath;

		public static PluginDescriptorCollection PluginDescriptors
		{
			get
			{
				return GetPluginDescriptors();
			} 
		}

		private static PluginDescriptorCollection GetPluginDescriptors()
		{
			PluginDescriptorCollection coll = new PluginDescriptorCollection();
			foreach(perfectusServerConfigurationPluginsPlugin pConf in Config.Shared.plugins.plugin)
			{
				if (pConf.type == null)
				{
					continue;
				}

				if (pConf.type.ToLower(CultureInfo.InvariantCulture) == "converter" 
					|| pConf.type.ToLower(CultureInfo.InvariantCulture) == "distributor" 
                    || pConf.type.ToLower(CultureInfo.InvariantCulture) == "instancecomplete")
				{
					if (pConf.path != null)
					{	
						Assembly a = null;
						try
						{						
							a = Assembly.LoadFrom(pConf.path);
							object o = a.CreateInstance(pConf.typeName, true);
							if (o is IDistributor || o is IConverter || o is IInstanceCompletionNotifier)
							{
								PluginDescriptor pd = new PluginDescriptor(o, pConf); 
								pd.Path = pConf.path;
															
								bool alreadyFound = false;
								foreach(PluginDescriptor test in coll)
								{
									if (test.FullName == pConf.assemblyName)
									{
										alreadyFound = true;
										break;
									}
								}
								if (!alreadyFound)
								{
									coll.Add(pd);
								}
							}
						}
						catch
						{
							throw;
						}
						finally
						{
							if (a != null)
							{
								a = null;
							}
						}
					}
				}
			}
			return coll;
		}
	}
}
