using System;
using System.Threading;
using System.Xml.Serialization;

namespace Perfectus.Server.ConfigurationSystem
{
    // 
    // This source code was auto-generated by xsd, Version=1.1.4322.2032.
    // Do not re-generate. Extensively modified, and XSD can no longer cope with the schema.


    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    [XmlRoot(Namespace = "http://tempuri.org/SharedConfigSet.xsd", IsNullable = false)]
    public class perfectusServerConfiguration
    {
        /// <remarks/>
        [XmlElement("coordinator", typeof (perfectusServerConfigurationCoordinator))] public
            perfectusServerConfigurationCoordinator coordinator;

        [XmlElement("queue", typeof (perfectusServerConfigurationQueue))] public 
            perfectusServerConfigurationQueue queue;

        [XmlElement("database", typeof (perfectusServerConfigurationDatabase))] public
            perfectusServerConfigurationDatabase database;

        [XmlElement("identity", typeof (perfectusServerConfigurationIdentity))] public
            perfectusServerConfigurationIdentity identity;

        [XmlElement("assembly", typeof (perfectusServerConfigurationAssembly))] public
            perfectusServerConfigurationAssembly assembly;

        [XmlElement("plugins", typeof (perfectusServerConfigurationPlugins))] public perfectusServerConfigurationPlugins
            plugins;

        [XmlElement("interview", typeof (perfectusServerConfigurationInterview))] public
            perfectusServerConfigurationInterview interview;

        [XmlElement("status", typeof (perfectusServerConfigurationStatus))] public perfectusServerConfigurationStatus
            status;

        [XmlElement("reporting", typeof (perfectusServerConfigurationReporting))] public
            perfectusServerConfigurationReporting reporting;

        [XmlElement("ipManager", typeof (perfectusServerConfigurationIpManager))] public
            perfectusServerConfigurationIpManager ipmanager;

        [XmlIgnore] public string path;

        //[System.Xml.Serialization.XmlElementAttribute("MyDocuments", typeof(perfectusServerConfigurationMyDocumsnts))]
        //public perfectusServerConfigurationIpManager ipmanager;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationCoordinator
    {
        /// <remarks/>
        public int threadsAtOnce;

        /// <remarks/>
        public string tempDirectory;

        /// <remarks/>
        public int stopOnCriticalCondition;

        /// <remarks/>
        public int maxcachedocumentsize;
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationIpManager
    {
        [XmlElement("publishControl")] public perfectusServerConfigurationIpManagerPublishControl publishControl;
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationIpManagerPublishControl
    {
        [XmlElement("assemblyName")] public string assemblyName;

        [XmlElement("typeName")] public string typeName;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationPluginsPluginPropertyOverridesProperty
    {
        /// <remarks/>
        [XmlAttribute()] public string name;

        /// <remarks/>
        [XmlAttribute()] public string defaultValue;

        /// <remarks/>
        [XmlAttribute()] public bool readOnly = false;

        /// <remarks/>
        [XmlAttribute()] public bool visible = true;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationPluginsPlugin
    {
        /// <remarks/>
        [XmlElement("propertyOverrides")] public perfectusServerConfigurationPluginsPluginPropertyOverrides
            propertyOverrides;

        /// <remarks/>
        [XmlElement("name")] public perfectusServerConfigurationPluginsPluginName[] name;

        /// <remarks/>
        [XmlAttribute()] public string type;

        /// <remarks/>
        [XmlAttribute()] public string typeName;

        /// <remarks/>
        [XmlAttribute()] public string assemblyName;

        /// <remarks/>
        [XmlAttribute()] public string key;

        /// <remarks/>
        [XmlAttribute()] public string iconFileName;

        [XmlIgnore] public string path;

        [XmlIgnore] public Guid uniqueIdentifier;

        public string GetLocalisedName()
        {
            string specificCulture = Thread.CurrentThread.CurrentUICulture.Name;
            string languageCulture = null;
            string defaultName = null;
            string specificName = null;
            string languageName = null;

            string[] cultureParts = specificCulture.Split('-');
            if (cultureParts.Length == 2)
            {
                languageCulture = cultureParts[0];
            }

            string localisedName = null;
            foreach (perfectusServerConfigurationPluginsPluginName n in name)
            {
                string cultureAttributeValue = null;
                if (n.uiCulture != null)
                {
                    cultureAttributeValue = n.uiCulture;
                }
                if (cultureAttributeValue == null)
                {
                    defaultName = n.value;
                }
                else if (languageCulture != null && cultureAttributeValue == languageCulture)
                {
                    languageName = n.value;
                }
                else if (cultureAttributeValue == specificCulture)
                {
                    specificName = n.value;
                }

                localisedName = defaultName;
                if (specificName != null)
                {
                    localisedName = specificName;
                }
                else if (languageName != null)
                {
                    localisedName = languageName;
                }
            }
            return localisedName;
        }
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationPluginsPluginPropertyOverrides
    {
        [XmlElement("property")] public perfectusServerConfigurationPluginsPluginPropertyOverridesProperty[] property;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationPlugins
    {
        /// <remarks/>
        [XmlElement("plugin")] public perfectusServerConfigurationPluginsPlugin[] plugin;

        /// <remarks/>
        [XmlAttribute()] public string basePath;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationAssembly
    {
        /// <remarks/>
        public int templateCacheTtl;

        /// <remarks/>
        public string mergeXsltPath;

        /// <remarks/>
        public string htmlPreviewXsltPath;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationInterview
    {
        /// <remarks/>
        public int serviceResultCacheTtl;

        public string resumeURL;
        public bool enableLocalSaveAnswerSet;
        public bool enableSuspendInterview;

        //used for defining nav button and progress layout on EI
        public string prevNavButton;
        public string progressBar;
        public string nextOrFinishNavButton;
        public string interviewHeight;
        public string interviewResponsive;

        // Property for ActivePDF Preview
        public string activePDFServerIP;
        public string activePDFPort;
        public string activePDFTimeOut;
        public string activePDFSharedFolder;

        public string activePDFuserPassword;
        public string activePDFownerPassword;
        public int activePDFallowPrinting;
        public int activePDFallowEdit;
        public int activePDFallowCopy;
        public int activePDFallowModify;
        public int activePDFallowFillIn;
        public int activePDFallowMakeAccessible;
        public int activePDFallowAssembly;
        public int activePDFallowReproduce;

        public bool EnableEmailLogging;
        public string SMTPServer;
        public string Subject;
        public string To;
        public string From;
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationStatus
    {
        /// <remarks/>
        public bool enableLocalOpenAnswerSet;
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationReporting
    {
        /// <remarks/>
        public bool enableServerReporting;

        public bool enableAnswerReporting;
        public int maximumSearchResults;
    }

    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationMyDocuments
    {
        /// <remarks/>
        public string address;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationQueue
    {
        /// <remarks/>
        public string server;

        /// <remarks/>
        public string name;
        public string transactionQueueName;

        /// <remarks/>
        public bool isPrivate;

        /// <remarks/>
        public bool databaseQueue;

        /// <remarks/>
        public int maxRetry;

        /// <remarks/>
        public int delayTime;

        /// <remarks/>
        public int timeout;

        public perfectusServerConfigurationQueue()
        {
            databaseQueue = true;
        }
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationDatabase
    {
        /// <remarks/>
        //public string connectionString;
        public string myDocumentsConnectionString;
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/SharedConfigSet.xsd")]
    [XmlType("System.Guid")]
    public class perfectusServerConfigurationIdentity
    {
        /// <remarks/>
        public Guid uniqueIdentifier;

        /// <remarks/>
        public string serverName;

        /// <remarks/>
        public string serverMessage;

        public string myDocumentsURL;
    }

    /// <remarks/>
    [XmlType(Namespace = "http://tempuri.org/SharedConfigSet.xsd")]
    public class perfectusServerConfigurationPluginsPluginName
    {
        /// <remarks/>
        [XmlAttribute("uiCulture")] public string uiCulture;

        [XmlText] public string value;
    }
}
