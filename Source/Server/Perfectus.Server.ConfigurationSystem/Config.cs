using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Server.PluginKit;

using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;

namespace Perfectus.Server.ConfigurationSystem
{

	public sealed class Config
	{
		private static FileSystemWatcher fsw;


		private Config()
		{}


		static Config()
		{
            Shared = (perfectusServerConfiguration)System.Configuration.ConfigurationSettings.GetConfig( "perfectus" );
			fsw = new FileSystemWatcher(Path.GetDirectoryName(Shared.path));
			fsw.Filter = Path.GetFileName(Shared.path);
			fsw.Changed += new FileSystemEventHandler(fsw_Changed); 
			fsw.EnableRaisingEvents = true;
		}

		public static perfectusServerConfiguration Shared;

		private static void fsw_Changed(object sender, FileSystemEventArgs e)
		{
			
			if (e.ChangeType == WatcherChangeTypes.Changed && e.Name.ToLower() == Path.GetFileName(Shared.path).ToLower())
			{
				
				Shared = LoadFrom(Shared.path);
			}
		}

		private static void FindAssembly(string assemblyName, string parentFolder, ref string pathToAssembly)
		{
			if (pathToAssembly != null)
			{
				return;
			}
			else
			{
				string probe = string.Format("{0}{1}{2}", parentFolder, Path.DirectorySeparatorChar, assemblyName);
				if (File.Exists(probe))
				{
					pathToAssembly = probe;
					return;
				}
				else
				{
					foreach (string dir in Directory.GetDirectories(parentFolder))
					{
						if (pathToAssembly == null)
						{
							FindAssembly(assemblyName, dir, ref pathToAssembly);
						}
					}
				}
			}
		}

		internal static perfectusServerConfiguration LoadFrom(string sharedConfigPath)
		{
			XmlSerializer ser = new XmlSerializer(typeof(perfectusServerConfiguration));
			XmlTextReader xr = null;
			try
			{
				perfectusServerConfiguration retval = null;
				using(FileStream fs = File.OpenRead(sharedConfigPath))
				{
					xr = new XmlTextReader(fs);

                    try
                    {
                        retval = (perfectusServerConfiguration)( ser.Deserialize( xr ) );
                    }
                    catch( Exception ex )
                    {
                        // Not being able to deserialise the (corrupted) shared config file is a serious error.
                        Log.Critical( Log.Category.ConfigurationSystem, ex, new ID( "ProcessCoordinator.HealthTestFailed" ), null );

                        // Throw new exception 
                        throw ex;
                    }
					retval.path = sharedConfigPath;
				}

                Log.Trace(Log.Category.InterviewSystem,Log.Priority.MID, String.Format("Loading plugins from {0}", retval.plugins.basePath));
				// Find each plugin and remember the path to its assembly.
				foreach(perfectusServerConfigurationPluginsPlugin pConf in retval.plugins.plugin)
				{
					string pathToPlugin = null;
					Guid pluginId = Guid.Empty;

                    // Try and instantiate the plugin to get its id
                    Assembly a = null;
                    object o = null;

                    String message = "";

                    try
                    {
                        FindAssembly( pConf.assemblyName, retval.plugins.basePath, ref pathToPlugin );
					    pConf.path = pathToPlugin;

                        message = String.Format("plugin '{0}', type '{1}', path '{2}'",
                            pConf.assemblyName,
                            pConf.typeName,
                            pathToPlugin);
                        
						a = Assembly.LoadFrom(pConf.path);
						o = a .CreateInstance(pConf.typeName, true);
						if (o is IDistributor)
						{
							pluginId = ((IDistributor)o).UniqueIdentifier;
                            Log.Trace(Log.Category.InterviewSystem, Log.Priority.MID, String.Format("Loaded distributor {0}", message));
						}
						else if (o is IConverter)
						{
							pluginId = ((IConverter)o).UniqueIdentifier;
                            Log.Trace(Log.Category.InterviewSystem, Log.Priority.MID, String.Format("Loaded converter {0}", message));
                        }
                        else if (o is IStringFormatter)
						{
                            Log.Trace(Log.Category.InterviewSystem, Log.Priority.MID, String.Format("Loaded formatter {0}", message));
                        }
                        else if (o is IFetcher)
						{
                            Log.Trace(Log.Category.InterviewSystem, Log.Priority.MID, String.Format("Loaded fetcher {0}", message));
                        }
                        else
                            Log.Trace(Log.Category.InterviewSystem, Log.Priority.MID, String.Format("Loaded UNKNOWN {0}", message));
					}
					catch (Exception ex)
					{
                        Log.Trace(Log.Category.InterviewSystem, Log.Priority.LOW, String.Format("Failed to load {0}\n{1}", message, ex.Message));
                        // We're not worried about dud plug-ins at this point. The coordinator will report problems with instantiation.
					}
					finally
					{
						a = null;
						o = null;
					}
					
					pConf.uniqueIdentifier = pluginId;
				}
				return retval;
			}

			finally
			{
				if (xr != null && xr.ReadState != ReadState.Closed)
				{
					xr.Close();
				}
			}
		}
	}
}