using System.Diagnostics;
using Perfectus.Server.Submitters.MSMQ.Engine.QueueListener;

namespace Perfectus.Server.Submitters.MSMQ.ServiceHost
{
	public class QueueWatcherService : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private static Receiver receiver;

		public QueueWatcherService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new QueueWatcherService() };

			Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// QueueWatcherService
			// 
			ServiceName = "PerfectusQueueWatcher";

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			receiver = Receiver.GetInstance();
			receiver.Start();
			
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			Trace.WriteLine("Service stopping...");
			if (receiver != null)
			{
				receiver.Stop();
			}

			foreach(TraceListener tl in Trace.Listeners)
			{
				try
				{
					Trace.WriteLine(string.Format(" Closing trace listener: {0} ", tl.Name));
					tl.Flush();
					tl.Close();
				}
				catch
				{
					Trace.WriteLine(" couldn't close listener.");
				}
			}
		}
	}
}
