using System.Reflection;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server Plug-in Base")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs