using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginBase
{
	/// <summary>
	///		The base class from which Distributor Plug-ins can inherit.
	/// </summary>
	public abstract class DistributorBase
	{
		/// <summary>
		///		The ID of the package instance.
		/// </summary>
		protected Guid instanceId;
		protected string user;
        protected StringBindingMetaData metaData;

        /// <summary>
        /// The MetaData structure specific this the current package instance.
        /// </summary>
        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return metaData; }
            set { metaData = value; }
        }

		/// <summary>
		///		The ID of the package instance within the database.
		/// </summary>
		[Browsable(false)]
		public Guid PackageInstanceId
		{
			get { return instanceId; }
			set { instanceId = value; }
		}

		[Browsable(false)]
		public string UserId
		{
			get { return user; }
			set { user = value; }
		}

		/// <summary>
		///		Default constructor.
		/// </summary>
		protected DistributorBase()
		{
		}


		/// <summary>
		///		The StreamReaders containing the files to distribute.
		/// </summary>
		protected ArrayList readers = new ArrayList();

		/// <summary>
		///		The filenames of the files to distribute.
		/// </summary>
		protected ArrayList fileNames = new ArrayList();

		protected ArrayList templateIds = new ArrayList();
		protected ArrayList templateNames = new ArrayList();


		/// <summary>
		///		Appends a <see cref="StreamReader"/> to the distributor plug-in.
		/// </summary>
		/// <remarks>
		///		<para>For each document that should be distributed by this plug-in, append a StreamReader whose backing stream contains the document.</para>
		///		<para>All StreamReaders should be appended prior to calling the Distribute() method.</para>
		/// </remarks>
		/// <param name="sr">The <see cref="StreamReader"/> containing the file to distribute.</param>
		/// <param name="fileName">The filename (e.g. "AnswerSummary.xml") that the StreamReader represents.</param>
		public void AppendStreamReader(StreamReader sr, string fileName, Guid templateId, string templateName)
		{
			readers.Add(sr);
			fileNames.Add(fileName);
			templateIds.Add(templateId);
			templateNames.Add(templateName);
		}
	}
}