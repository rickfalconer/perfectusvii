using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Drawing.Design;
using System.Drawing.Printing;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Distributors.Printer
{
	/// <summary>
	/// Summary description for Printer Distributor.
	/// </summary>
	public class Distributor : DistributorBase, IDistributor
	{
		private TextQuestionQueryValue tqvPrinterPath = new TextQuestionQueryValue();
		private IntQuestionValue tqvPaperSource = new IntQuestionValue();
		
		private Config conf;
		private int sleeptimeval;
		private int timeoutval;
		private int paperSource;
		//private Microsoft.Office.Interop.Word.Application ObjWord = null;
		//private bool wordObjectCreated = false;
        private StringBindingMetaData instanceMetaData;

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("A28959F8-8D22-4cc2-9178-C8D9E084A6E4"); }
		}

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		[DisplayName("Printer Path")]
        [Description("The pathname to the printer that is available to the Perfectus server.")]
        [DefaultValue("{default}")]
        public TextQuestionQueryValue PrinterPath
		{
			get { return tqvPrinterPath; }
			set { tqvPrinterPath = value; }
		}	
		
		[Browsable(true)]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.IntQuestionConverter, Studio")]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.IntQuestionEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Paper Source")]
        [Description("An integer value representing the printer tray to use.")]
        [DefaultValue(0)]
        public IntQuestionValue PaperSource
		{
			get { return tqvPaperSource; }
			set { tqvPaperSource = value; }
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return instanceMetaData; }
            set { instanceMetaData = value; }
        }

		public Distributor()
		{
			conf = Config.Load();
			
			timeoutval=Convert.ToInt32(conf.TimeOut);
			sleeptimeval=Convert.ToInt32(conf.SleepTime);
		}

		[DllImport("winspool.drv", CharSet=CharSet.Auto, SetLastError=true)]
		public static extern bool SetDefaultPrinter(string Name);

        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool GetDefaultPrinter(StringBuilder pszBuffer, ref int size);

        [DllImport("kernel32.dll", EntryPoint = "GetLastError", SetLastError = false,
ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        private static extern Int32 GetLastError();


		public DistributionResult[] Distribute()
		{
            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# printer Distribute() called.");
            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, String.Format ("Timeout='{0}',  Interval='{1}'", timeoutval, sleeptimeval));

			DistributionResult[] result = new DistributionResult[readers.Count];

			if (readers.Count == 0)
			{
                Log.Trace(Log.Category.Distributors, Log.Priority.MID, " # Nothing to do. (No readers were appeneded to this distributor.)");
				return null;
			}

			try
			{
				string printerPath = null;
                
                // get the property name
				if (tqvPrinterPath != null)
				{
                    printerPath = tqvPrinterPath.Evaluate(AnswerProvider.Caller.System, instanceMetaData);
                    Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format(" # Will try to print to: [{0}]", printerPath));
				}
				else
					throw new Exception("No printer name supplied.");
				
				
				if (tqvPaperSource != null)
				{
					paperSource = tqvPaperSource.Evaluate(AnswerProvider.Caller.System);
				}
				else
					paperSource = 0;
                Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Papersource: {0}", paperSource));

                StringBuilder dp = new StringBuilder(256);
                int size = dp.Capacity;
                if (!GetDefaultPrinter(dp, ref size))
                    size = 0;

                if (String.Compare("{default}", printerPath, true) == 0)
                {
                    if (size == 0)
                        throw new Exception("Cannot determine the default printer.");
                    printerPath = dp.ToString();
                }

				//set printer
                if (!SetDefaultPrinter(printerPath))
                {
                    int rc = GetLastError();
                    String message = String.Format("Cannot find the printer." + System.Environment.NewLine +
                        "The error code from printer manager: {0}" + System.Environment.NewLine, rc);

                    String all_printer_names = System.String.Empty + "Available printers are:" + System.String.Empty;
                    foreach (String printer_name in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        all_printer_names += System.Environment.NewLine + printer_name;
                        if (String.Compare(printer_name, dp.ToString(), true) == 0)
                            all_printer_names += " {default}";
                    }
                    message += all_printer_names + System.Environment.NewLine;

                    throw new Exception(message);
                }
                Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Printer succesfully set to: [{0}]", printerPath));

				// get templates need to print
				string tempFileName = Path.GetTempFileName();
				File.Delete(tempFileName);
				DirectoryInfo di = Directory.CreateDirectory(tempFileName);
				string directory = di.FullName;
			
				for (int i = 0; i < readers.Count; i++)
				{
					// Write each stream to disc.
					string fileName = (string) fileNames[i];
					string filetype= Path.GetExtension(fileName);
					
					result[i] = new DistributionResult(this.UniqueIdentifier, null, fileName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), null); 
					string fullPath = string.Format("{0}{1}{2}", directory, Path.DirectorySeparatorChar, fileName);
					StreamReader sr = (StreamReader) readers[i];
					sr.BaseStream.Seek(0, SeekOrigin.Begin);

					byte[] buffer = new byte[1024];
					using (FileStream fs = new FileStream(fullPath, FileMode.Create))
					{
						int bytesRead = 0;
						do
					{
							bytesRead = sr.BaseStream.Read(buffer, 0, buffer.Length);
							fs.Write(buffer, 0, bytesRead);
						} while (bytesRead > 0);
					}

					DoPrint(fullPath, fullPath);
                    Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, String.Format("Printing Document '{0}'",fullPath));
				
					// OK at this point the file is still being printed so there will be a lock on it.
					// Wait until lock has gone and delete file.  (bit of a hack)
					DateTime timeOutValue = DateTime.Now.AddMilliseconds(timeoutval);  
					while(File.Exists(fullPath))
					{
						try
						{
							File.Delete(fullPath);
						}
						catch(Exception)
						{
							if (DateTime.Now > timeOutValue)
							{
								throw new Exception("Timeout occured when attempting to delete print file '" + fullPath + "'");
							}
							else
							{
								Thread.Sleep(10);
							}							
						}
					}

                    Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "Process exited.");
				}
				
				//if(wordObjectCreated)
				//{
				//	// If we have used Word we now need to destory it.
				//	object saveChanges = false;
				//	object M=Type.Missing;
				//	ObjWord.Quit(ref saveChanges,ref M,ref M);
				//	wordObjectCreated = false;
				//}
			}
			catch 
			{
				throw;
			}
					
			return result;
		}

		/// <summary>
		/// Do the actual printing.
		/// </summary>
		/// <param name="StrFileName"></param>
		/// <param name="fullPath"></param>
		private void DoPrint(string StrFileName, string fullPath)
		{
			object M = Type.Missing;    // M is missing...hi..hi
			object V = StrFileName;     // Convert to Object..
			object I = false;           // Convert to Object
			object saveChanges = false;

            try
            {
                if (GetUseword(fullPath)) // Ends with .doc. itz Word document....
                {
                    //pf-3266 remove word automation office12
                    //if (!wordObjectCreated)
                    //{
                    //    ObjWord = new Microsoft.Office.Interop.Word.Application(); // Create Word Application
                    //    wordObjectCreated = true;
                    //}

                    //ObjWord.Documents.Open(ref V, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M);

                    //// Set up paper trays for printing
                    //ObjWord.ActiveDocument.PageSetup.OtherPagesTray = (Microsoft.Office.Interop.Word.WdPaperTray)paperSource;
                    //ObjWord.ActiveDocument.PageSetup.FirstPageTray = (Microsoft.Office.Interop.Word.WdPaperTray)paperSource;

                    //// Update any fields in the document.
                    //ObjWord.ActiveDocument.Fields.Update();
                    //foreach (TableOfContents toc in ObjWord.ActiveDocument.TablesOfContents)
                    //{
                    //    toc.Update();
                    //}

                    //ObjWord.PrintOut(ref I, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M, ref M);
                    //ObjWord.ActiveDocument.Close(ref saveChanges, ref M, ref M);

                    // use gembox to print
                    Perfectus.Common.SharedLibrary.GemBoxAPI.PrintDocument(StrFileName);

                }

                else // For all other types..I did check for .txt,.pdf and .Msg
                {
                    System.Diagnostics.Process P = new Process();           // Create a process
                    P.StartInfo.FileName = StrFileName.ToString();          // Convert ....> Readable
                    P.StartInfo.CreateNoWindow = true;                      // Perfect...Create No Window
                    P.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;    // WindowStyle---->HIDDEN
                    P.StartInfo.Verb = "print";
                    bool isstart = P.Start();                               // Start the process

                    if (isstart)
                    {
                        try
                        {
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, String.Format("Process name: {0}", P.ProcessName));
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, String.Format("File name:    {0}", P.StartInfo.FileName));
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, String.Format("Argument:     {0}", P.StartInfo.Arguments));
                        }
                        catch { }
                    }

                    int ticks = 0;

                    if (P != null && isstart)
                    {
                        //check timeout 						
                        Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "check timeout");

                        while (!P.WaitForExit(timeoutval) && ticks < 3)
                        {
                            ++ticks;
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, " still waiting.");
                        }

                        if (!P.HasExited)
                        {
                            P.Kill();
                            throw new Exception(String.Format("Printer process did not response withing timeout, process name: '{0}', filename: '{1}', Argument: '{2}'", P.ProcessName, P.StartInfo.FileName, P.StartInfo.Arguments));
                        }
                    }
                }
            }

            catch (Exception ex) //Err Handler.....
            {
                throw;
            }
		}
		/// <summary>
		/// Fuction to descide if the input document is a MS Word document.
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		private static bool GetUseword(string fullPath)
		{
			bool useword=false;
				
			// Check if need print with word
			string fileExten = Path.GetExtension(fullPath);
            fileExten = fileExten.ToLower();

            if (fileExten == ".doc")
				useword = true;

            if (fileExten == ".docx")
                useword = true;

            if (fileExten == ".docm")
                useword = true;

            if (fileExten == ".html")
                useword = true;

            if (fileExten == ".htm")
                useword = true;

            if (fileExten == ".xml")
			{
				// Check if its wordML 
				FileStream fsIn = new FileStream(fullPath,FileMode.Open, FileAccess.Read); 
				XmlTextReader xtr = new XmlTextReader(fsIn); 
				string tagstr = "";

				while(!xtr.EOF)
				{ 
					xtr.Read();
					tagstr += xtr.ReadOuterXml();
				}
						
				xtr.Close();						
				fsIn.Close();
						
				if (tagstr.IndexOf("wordDocument")>0)
				{	
					useword = true;
				}
			}

			return useword;
		}
	}
}