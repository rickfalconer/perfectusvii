using System;
using System.IO;
using System.Xml.Serialization;


namespace Perfectus.Server.Plugins.Distributors.Printer
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		public Config()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private string timeout;
		private string sleeptime;
		private string printmethod;

		[XmlElement]
		public string TimeOut
		{
			get { return timeout; }
			set { timeout = value; }
		}

		[XmlElement]
		public string SleepTime
		{
			get { return sleeptime; }
			set { sleeptime = value; }
		}
		
		

		public static Config Load()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamReader sr = new StreamReader(configPath))
			{
				return (Config)(serialiser.Deserialize(sr));
			}
		}

		public void Save()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamWriter sw = new StreamWriter(configPath, false))
			{
				serialiser.Serialize(sw, this);
			}			
		}
	}
}
