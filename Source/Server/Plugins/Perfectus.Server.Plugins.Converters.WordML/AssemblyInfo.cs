using System.Reflection;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server WordML Converter")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs