using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;

namespace Perfectus.Server.Plugins.Converters.WordML
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Converter : IConverter
	{
	    private StringBindingMetaData instanceMetaData;

	    public Converter()
		{
		}

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("748FC501-5131-4a26-890C-FE9118890DA1"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

		public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
		{
            //pf-3266 remove word automation office12 replace with error in log
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Legacy WordML 2003 converter not supported");
            convertedFileName = string.Empty;
            return null;

            //Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# WordML 2003 converter started");

            //convertedFileName = string.Format("{0}.xml", fileNameWithoutExtension);
            //object oConvertedFileName = null;
            //source.BaseStream.Seek(0, SeekOrigin.Begin);

            //string emptyTempFile = Path.GetTempFileName();

            //string wordMlPath = emptyTempFile + ".xml";

            //object oWordMlPath = wordMlPath;

            //byte[] writeBuffer = new byte[1024];
            //using (FileStream fs = new FileStream(wordMlPath, FileMode.Create))
            //{
            //    int bytesRead = 0;
            //    do
            //    {
            //        bytesRead = source.BaseStream.Read(writeBuffer, 0, writeBuffer.Length);
            //        fs.Write(writeBuffer, 0, bytesRead);
            //    } while (bytesRead > 0);
            //}

            ////dn4
            ////ApplicationClass app = new ApplicationClass();
            //Application app = new Application();

            //try
            //{
            //    try
            //    {
            //        object oMissing = Missing.Value;

            //        Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Opening '{0}'", oWordMlPath.ToString()));

            //        Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //        app.Selection.WholeStory();
            //        app.Selection.Fields.Update();

            //        foreach (TableOfContents toc in doc.TablesOfContents)
            //        {
            //            toc.Update();
            //        }


            //        object oWmlFormat = WdSaveFormat.wdFormatXML;

            //        oConvertedFileName = Path.GetTempFileName();

            //        Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Saving '{0}'", oConvertedFileName.ToString()));

            //        doc.SaveAs(ref oConvertedFileName, ref oWmlFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //        object oFalse = false;
            //        doc.Close(ref oFalse, ref oMissing, ref oMissing);
            //        app.Quit(ref oFalse, ref oMissing, ref oMissing);
            //    }
            //    finally
            //    {
            //        try
            //        {
            //            if (File.Exists(wordMlPath))
            //            {
            //                File.Delete(wordMlPath);
            //            }
            //            if (File.Exists(emptyTempFile))
            //            {
            //                File.Delete(emptyTempFile);
            //            }

            //        }

            //        catch
            //        {
            //        }
            //    }

            //    MemoryStream ms = new MemoryStream();

            //    byte[] buffer = new byte[1024];
            //    using (FileStream fs = File.OpenRead(oConvertedFileName.ToString()))
            //    {
            //        int bytesRead = 0;
            //        do
            //        {
            //            bytesRead = fs.Read(buffer, 0, buffer.Length);
            //            ms.Write(buffer, 0, bytesRead);
            //        } while (bytesRead > 0);
            //    }

            //    ms.Seek(0, SeekOrigin.Begin);

            //    Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# WordML 2003 end");
            //    return ms;
            //}
            //finally
            //{
            //    if (oConvertedFileName != null && File.Exists(oConvertedFileName.ToString()))
            //    {
            //        File.Delete(oConvertedFileName.ToString());
            //    }
            //}
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }
	}
}