using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.FormatStringExtensions.FormatStringSample
{
    /// <summary>
    /// Sample Formatter Plugin
    /// </summary>

    /* 
		
    CONFIGURATION SAMPLES FOR GETTING THIS PLUGIN TO WORK:
		
    Sample Server shared configuration entry:
			
    <plugin type="formatStringExtension" assemblyName="Perfectus.Server.Plugins.FormatStringExtensions.FormatStringSample.dll"
    typeName="Perfectus.Server.Plugins.FormatStringExtensions.FormatStringSample.Formatter" key="SampleFormatStringPlugin">
    <name>Format Sring Extension Sample</name>
    </plugin>
			
		
    Sample IPManager configuration entry:
		
    <formatStringExtensions>	
    <formatString forDataType="TextFormats" serverSidePluginKey="SampleFormatStringPlugin">
    <name>Sample Format String Plugin</name>	
    </formatString>		
    </formatStringExtensions>	
			
    Note - 	The serverSidePluginKey property has the value of the key specified in the server configuration file. 
    Available "forDataType" formats are: TextFormats, NumberFormats and DateFormats			
		
    */

    /// A Formatter Plugin must Implement IStringFormatter.
    public class Formatter : IStringFormatter
    {
        public Formatter()
        {
        }

        /// <summary>
        /// Formats a string
        /// </summary>
        /// <param name="inputString">The string to format</param>
        /// <param name="inputParams">Parameters</param>
        /// <returns>The formatted string</returns>
        public string FormatString(string inputString, string inputParams)
        {
            // This sample removes character sequences as specified by the input parameters, then finally it makes the string uppercase.

            string retVal = inputString;

            string[] switches = inputParams.Split('-');

            foreach (string s in switches)
            {
                if (s.Length > 0)
                {
                    retVal = retVal.Replace(s, "");
                }
            }

            retVal = retVal.ToUpper();
            return retVal;
        }
    }
}
