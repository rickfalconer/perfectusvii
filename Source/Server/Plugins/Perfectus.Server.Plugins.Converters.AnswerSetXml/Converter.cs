using System;
using System.ComponentModel;
using System.IO;
using System.Xml.XPath;
using System.Xml.Xsl;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Server.Plugins.Converters.AnswerSetXml
{
    /// <summary>
    /// Summary description for Converter.
    /// </summary>
    public class Converter : IConverter
    {
        private TextQuestionQueryValue fileName = null;
        private TextQuestionQueryValue xsltFilePath = null;
        private StringBindingMetaData instanceMetaData;

        public Converter()
        {
        }

        [Browsable(false)]
        public Guid UniqueIdentifier
        {
            get { return new Guid("8AA3BC8A-6196-4526-95BA-FB45DD7813BF"); }

        }

        [Browsable(false)]
        public SourceDocument DocumentTypeConverted
        {
            get { return SourceDocument.AnswerSetXml; }
        }

        /// <summary>
        /// Suggested output file name.
        /// </summary>
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [System.ComponentModel.DisplayName("File Name")]
        public TextQuestionQueryValue FileNameValue
        {
            get { return fileName; }
            set { fileName = value; }
        }

        [Browsable(false)]
        public string FileName
        {
            set
            {
                fileName = new TextQuestionQueryValue();
                fileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                fileName.TextValue = value;
            }
        }

        /// <summary>
        /// Fully pathed file name of the transformation to use.
        /// </summary>
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [System.ComponentModel.DisplayName("XSLT File Path")]
        public TextQuestionQueryValue XSLTFilePath
        {
            get { return xsltFilePath; }
            set { xsltFilePath = value; }
        }

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return instanceMetaData; }
            set { instanceMetaData = value; }
        }

        /// <summary>
        /// Convert the input memory stream using the supplied XSLT.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="fileNameWithoutExtension"></param>
        /// <param name="convertedFileName"></param>
        /// <returns></returns>
        public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string returnedFilename)
        {
            // Check arguments.
            string lXsltFilePath = string.Empty;

            if (xsltFilePath != null && (xsltFilePath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || xsltFilePath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            {
                lXsltFilePath = xsltFilePath.Evaluate(AnswerProvider.Caller.System, instanceMetaData);
            }

            if (!File.Exists(lXsltFilePath))//if we have not specified a transform to use, just output the answerset
            {
                returnedFilename = ProcessAnswerset(source, fileNameWithoutExtension);
                return source.BaseStream;
            }
            else
            {
                MemoryStream ms;
                Transform(source, lXsltFilePath, out returnedFilename, out ms);
                return ms;
            }
        }

        private string ProcessAnswerset(StreamReader source, string fileNameWithoutExtension)
        {
            string returnedFilename;

            if (fileName != null && (fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            {
                returnedFilename = fileName.Evaluate(AnswerProvider.Caller.Assembly, instanceMetaData);
            }
            else
            {
                returnedFilename = fileNameWithoutExtension;
            }

            if (returnedFilename == null || returnedFilename.Length == 0)
                returnedFilename = string.Format("{0}.xml", "Answerset");

            if (!returnedFilename.ToLower().EndsWith(".xml"))
                returnedFilename = string.Format("{0}.xml", returnedFilename);

            source.BaseStream.Seek(0, SeekOrigin.Begin);
            return returnedFilename;
        }

        private void Transform(StreamReader source, string lXsltFilePath, out string convertedFileName, out MemoryStream ms)
        {
            // Set the output file name.
            convertedFileName = GetOutputFileName();

            ms = new MemoryStream();

            using (MemoryStream newStream = new MemoryStream())
            {
                // Copy the input stream data to a new memory stream because the Transform below manages to 
                // close the original stream after finishing with it!
                source.BaseStream.Seek(0, SeekOrigin.Begin);
                byte[] newBytes = new byte[4096];
                int bytesRead;
                while ((bytesRead = source.BaseStream.Read(newBytes, 0, 1)) > 0)
                {
                    newStream.Write(newBytes, 0, bytesRead);
                }
                newStream.Seek(0, SeekOrigin.Begin);

                // Load the XSLT and transform the stream.
                XslTransform transform = new XslTransform();
                transform.Load(lXsltFilePath);
                XPathDocument xPathDocument = new XPathDocument(newStream);
                transform.Transform(xPathDocument, null, ms, null);

                // Reset both the input and output streams.
                source.BaseStream.Seek(0, SeekOrigin.Begin);
                ms.Seek(0, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Get the suggested output file name.
        /// </summary>
        /// <returns></returns>
        private string GetOutputFileName()
        {
            if (fileName != null)
            {
                string outputFileName = fileName.Evaluate(AnswerProvider.Caller.System, instanceMetaData);

                if (outputFileName != null)
                {
                    if (Path.HasExtension(outputFileName))
                    {
                        return outputFileName;
                    }
                    else
                    {
                        return string.Format("{0}.xml", outputFileName);
                    }
                }
            }
            return "transformed.xml";
        }
    }
}