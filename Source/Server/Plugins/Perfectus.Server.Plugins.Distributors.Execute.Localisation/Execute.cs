using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Localisation
{
    public class Execute : MessageBase, IMessageEnumaration
    {
        public enum MEnum
        {
            // Error
            InvalidWorkingFolder = 32000,
            FailedCreateConfiguration,
            ExecuteableNameEmpty,
            WorkingFolderInvalid,
            HangingDetected,
            NoneZeroResult,
            
            // Warning
            ConfigurationFileCreated = 33000,

            // Info
        }

        internal Dictionary<int, string> links = new Dictionary<int, string>();
        public Execute()
        {
            links.Add((int)MEnum.InvalidWorkingFolder, 
                "Please review your configuration node <WorkingDirectory> in 'config.xml'." );
            links.Add((int)MEnum.ExecuteableNameEmpty,
                "Please add a valid application name to the distributer.");
        }
        override protected Dictionary<int, string> link { get { return links; } }

        protected override Type EnumType()
        {
            return typeof(MEnum);
        }
    }
}
