using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using Perfectus.Common.Property.Class;
using Microsoft.Win32;

namespace Perfectus.Server.Plugins.Converters.Word2007
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public partial class Converter : IConverter
    {
#if DEBUG
        // Create the XML string from the classes
        private String GenerateDefaultXML()
        {
            //pf-3266 remove word automation office12
            //try
            //{
            //    PropertyList x = new PropertyList(typeof(WdSaveFormat).ToString());

            //    PropertyListEntry listEntry = new PropertyListEntry("Printable data format (*.PDF)", WdSaveFormat.wdFormatPDF.ToString());

            //    Property extendedproperty = new Property("Enable Encryption", "EncryptionEnabled", typeof(bool).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.DefaultValue = false;
            //    extendedproperty.Value = false;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("User Password", "UserPassword", typeof(String).ToString());
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Owner Password", "OwnerPassword", typeof(String).ToString());
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("allow printing", "allowprinting", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Printing", "allowDegradedPrinting", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Modify Content", "allowModifyContents", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Copying", "allowCopy", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Modify Annotations", "allowModifyAnnotations", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Fill In", "allowFillIn", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Screen Readers", "allowScreenReaders", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    extendedproperty = new Property("Allow Assembly", "allowAssembly", typeof(Boolean).ToString());
            //    extendedproperty.PerfectusType = Perfectus.Common.PerfectusDataType.YesNo;
            //    extendedproperty.ConverterTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio";
            //    extendedproperty.EditorTypeName = "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio";
            //    listEntry.properties.Add(extendedproperty);

            //    x.ListElements.Add(listEntry);

            //    listEntry = new PropertyListEntry("RitchText (*.rtf)", WdSaveFormat.wdFormatRTF.ToString());
            //    x.ListElements.Add(listEntry);

            //    listEntry = new PropertyListEntry("Document (*.doc)", WdSaveFormat.wdFormatDocument.ToString());
            //    x.ListElements.Add(listEntry);

            //    listEntry = new PropertyListEntry("Text (*.txt)", WdSaveFormat.wdFormatDOSText.ToString());
            //    x.ListElements.Add(listEntry);

            //    listEntry = new PropertyListEntry("Word", WdSaveFormat.wdFormatXMLDocument.ToString());
            //    x.ListElements.Add(x.CurrentElement = listEntry);

            //    listEntry = new PropertyListEntry("XML (*.xml)", WdSaveFormat.wdFormatXML.ToString());
            //    x.ListElements.Add(listEntry);

            //    XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
            //    using (MemoryStream m = new MemoryStream())
            //    {
            //        ser.Serialize(m, x);
            //        return Encoding.ASCII.GetString(m.ToArray(), 0, (int)m.Length);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    String msg = ex.Message;
            //}
            return string.Empty;
        }

#endif
    }
    internal static class OfficeApp
    {
        public enum eOfficeVersion
        {
            eOfficeVersion_Unknown, // error return value
            eOfficeVersion_95,
            eOfficeVersion_97,
            eOfficeVersion_2000,
            eOfficeVersion_XP,   // XP = 2002 + marketing
            eOfficeVersion_2003,
            eOfficeVersion_2007,
        };

        public enum eOfficeApp // in case you are looking for a particular app
        {
            eOfficeApp_Word,
            eOfficeApp_Excel,
            eOfficeApp_Outlook,
            eOfficeApp_Access,
            eOfficeApp_PowerPoint,
        };


        private static int GetMajorVersion(string _path)
        {
            int toReturn = 0;
            if (File.Exists(_path))
            {
                try
                {
                    FileVersionInfo _fileVersion = FileVersionInfo.GetVersionInfo(_path);
                    toReturn = _fileVersion.FileMajorPart;
                }
                catch
                { }
            }
            return toReturn;
        }
        private static string GetComponentPath(eOfficeApp _component)
        {
            const string RegKey = @"Software\Microsoft\Windows\CurrentVersion\App Paths";
            string toReturn = string.Empty;
            string _key = string.Empty;
            _key = "winword.exe";

            //looks inside CURRENT_USER:
            RegistryKey _mainKey = Registry.CurrentUser;
            try
            {
                _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                if (_mainKey != null)
                {
                    toReturn = _mainKey.GetValue(string.Empty).ToString();
                }
            }
            catch
            { }

            //if not found, looks inside LOCAL_MACHINE:
            _mainKey = Registry.LocalMachine;
            if (string.IsNullOrEmpty(toReturn))
            {
                try
                {
                    _mainKey = _mainKey.OpenSubKey(RegKey + "\\" + _key, false);
                    if (_mainKey != null)
                    {
                        toReturn = _mainKey.GetValue(string.Empty).ToString();
                    }
                }
                catch
                { }
            }

            //closing the handle:
            if (_mainKey != null)
                _mainKey.Close();

            return toReturn;
        }

        public static int GetOfficeVersion()
        {
            String path = GetComponentPath(eOfficeApp.eOfficeApp_Word);
            return GetMajorVersion(path);
        }
    }
}