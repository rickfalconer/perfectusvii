using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
//using WordDocument = Microsoft.Office.Interop.Word.Document;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using System.Xml;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Text;
using Perfectus.Common.Property.Class;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Converters.Word2007
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public partial class Converter : IConverter
	{
	    private StringBindingMetaData instanceMetaData;
        private String outputFormatXML;
        private Config cfg = null;
        private int OfficeVersion = -1;

        public Converter()
        {
            OfficeVersion = OfficeApp.GetOfficeVersion();
        }

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
            get { return new Guid("80B3FCB4-00B3-4a17-BE08-709B1A7A48BB"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

        
        [Browsable(true)]
        [TypeConverter("Perfectus.Common.Property.TypeConverters.XmlEncodedConverter, Perfectus.Common.Property")]
        [Editor("Perfectus.Common.Property.Editors.XmlEncodedEditor, Perfectus.Common.Property", typeof(UITypeEditor))]
        [Description("Property Field XML")]
        [DefaultValue("<?xml version=\"1.0\"?> " +
"<PropertyList xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"> " +
"  <CurrentElement> " +
"    <properties /> " +
"      <FriendlyName>Office Open XML (*.docx)</FriendlyName> " +
"      <InternalName>wdFormatXMLDocument</InternalName> " +
"  </CurrentElement> " +
"  <ListElements> " +
"    <PropertyListEntry> " +
"      <properties />  " +
"      <FriendlyName>Office Open XML (*.docx)</FriendlyName> " +
"      <InternalName>wdFormatXMLDocument</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties /> " +
"      <FriendlyName>Office Open XML Macro Enabled (*.docx)</FriendlyName> " +
"      <InternalName>wdFormatXMLDocumentMacroEnabled</InternalName> " +
"    </PropertyListEntry> " +
//"    <PropertyListEntry> " +
//"      <properties /> " +
//"      <FriendlyName>Document (*.doc)</FriendlyName> " +
//"      <InternalName>wdFormatDocument97</InternalName> " +
//"    </PropertyListEntry> " + 
//"    <PropertyListEntry> " +
//"      <properties /> " +
//"      <FriendlyName>Word 2003 XML (*.xml)</FriendlyName> " +
//"      <InternalName>wdFormatXML</InternalName> " +
//"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties /> " +
"      <FriendlyName>Rich Text (*.rtf)</FriendlyName> " +
"      <InternalName>wdFormatRTF</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties />  " +
"      <FriendlyName>HyperText Markup (*.html)</FriendlyName> " +
"      <InternalName>wdFormatHTML</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties />  " +
"      <FriendlyName>Hypertext Markup Filtered (*.html)</FriendlyName> " +
"      <InternalName>wdFormatFilteredHTML</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties>  " +
"        <Property> " +
"          <FriendlyName>Line break</FriendlyName> " +
"          <InternalName>LineBreak</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"      </properties> " +
"      <FriendlyName>Text (*.txt)</FriendlyName> " +
"      <InternalName>wdFormatText</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties /> " +
"      <FriendlyName>XML Paper Specification (*.xps)</FriendlyName> " +
"      <InternalName>wdFormatXPS</InternalName> " +
"    </PropertyListEntry> " +
"    <PropertyListEntry> " +
"      <properties> " +
"        <Property> " +
"          <Value xsi:type=\"xsd:boolean\">false</Value> " +
"          <DefaultValue xsi:type=\"xsd:boolean\">false</DefaultValue> " +
"          <FriendlyName>Enable Encryption</FriendlyName> " +
"          <InternalName>EncryptionEnabled</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>User Password</FriendlyName> " +
"          <InternalName>UserPassword</InternalName> " +
"          <TypeName>System.String</TypeName> " +
"          <PerfectusType>Text</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Owner Password</FriendlyName> " +
"          <InternalName>OwnerPassword</InternalName> " +
"          <TypeName>System.String</TypeName> " +
"          <PerfectusType>Text</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Printing</FriendlyName> " +
"          <InternalName>allowprinting</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Degraded Printing</FriendlyName> " +
"          <InternalName>allowDegradedPrinting</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Modify Content</FriendlyName> " +
"          <InternalName>allowModifyContents</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Copying</FriendlyName> " +
"          <InternalName>allowCopy</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Modify Annotations</FriendlyName> " +
"          <InternalName>allowModifyAnnotations</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Fill In</FriendlyName> " +
"          <InternalName>allowFillIn</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Screen Readers</FriendlyName> " +
"          <InternalName>allowScreenReaders</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"        <Property> " +
"          <FriendlyName>Allow Assembly</FriendlyName> " +
"          <InternalName>allowAssembly</InternalName> " +
"          <TypeName>System.Boolean</TypeName> " +
"          <PerfectusType>YesNo</PerfectusType> " +
"          <EditorTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio</EditorTypeName> " +
"          <ConverterTypeName>Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio</ConverterTypeName> " +
"        </Property> " +
"      </properties> " +
"      <FriendlyName>Portable Document Format (*.pdf)</FriendlyName> " +
"      <InternalName>wdFormatPDF</InternalName> " +
"    </PropertyListEntry> " +
"  </ListElements> " +
"  <TypeName>Microsoft.Office.Interop.Word.WdSaveFormat</TypeName> " +
"  <PerfectusType>Text</PerfectusType> " +
"</PropertyList> ")]
        [ReadOnly (false)]
        public String Format
        {
            get { return outputFormatXML; }
            set { outputFormatXML = value; }
        }

		public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
		{
            PropertyList selection = PropertyList.CreateFromXML(Format);
            PropertyListEntry selectedFormatEntry = selection.CurrentElement;

            //pf-3266 remove word automation office12
            //WdSaveFormat selectedFormat = (WdSaveFormat)Enum.Parse(typeof(WdSaveFormat), selectedFormatEntry.InternalName);
            string selectedFormat = selectedFormatEntry.InternalName;

            //pf-3266 remove word automation office12
            // sanity check
            //int optionAsInt = (int) selectedFormat;

            //// earlier than Office 2007 does not support wdFormatXMLDocument (=12) or higher 
            //if (OfficeVersion < 12 && optionAsInt > 11) 
            //{
            //    String errorMessage = String.Format("Installed Office package is of version {0}" + System.Environment.NewLine +
            //        "This version does not support the file format {1}.", OfficeVersion, selectedFormat.ToString());
            //    throw new Exception(errorMessage);
            //}

            String filenameExtension = "docx";
            object oInsertLineBreaks = false;
            switch (selectedFormat)
            {
                case "wdFormatPDF":
                    filenameExtension = "pdf";
                    break;
                case "wdFormatRTF":
                    filenameExtension = "rtf";
                    break;
                case "wdFormatText":
                case "wdFormatTextLineBreaks":
                case "wdFormatDOSText":
                case "wdFormatDOSTextLineBreaks":
                    // FB2304 : Inserts line breaks using the same functionality as provided by Word when clicking 
                    // the 'Insert Line Breaks' checkbox in the file conversion dialogue window when saving to a .txt file.
                    if ( System.Convert.ToBoolean( selectedFormatEntry[ "LineBreak" ] ) )
                        oInsertLineBreaks = true;
                    filenameExtension = "txt";
                    break;
                case "wdFormatXPS":
                    filenameExtension = "xps";
                    break;
                case "wdFormatXMLDocument":
                    if (System.Convert.ToBoolean(selectedFormatEntry["EnableMacros"]))
                    {
                        selectedFormat = "wdFormatXMLDocumentMacroEnabled";
                        filenameExtension = "docm";
                        break;
                    }
                    filenameExtension = "docx";
                    break;
                case "wdFormatXMLDocumentMacroEnabled":
                    filenameExtension = "docm";
                    break;
                case "wdFormatHTML":
                    if (System.Convert.ToBoolean(selectedFormatEntry["Filterred"]))
                        selectedFormat = "wdFormatFilteredHTML";
                    filenameExtension = "html";
                    break;
                case "wdFormatFilteredHTML":
                    filenameExtension = "html";
                    break;
                case "wdFormatXML":
                case "wdFormatXMLTemplate":
                case "wdFormatXMLTemplateMacroEnabled":
                case "wdFormatFlatXML":
                    filenameExtension = "xml";
                    break;
            }
            convertedFileName = string.Format("{0}.{1}", fileNameWithoutExtension, filenameExtension);
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Output type '{0}', file '{1}'", selectedFormat, convertedFileName));

            // This is the file name we will use when creating temporary files between each stage.
            string word2007Path = Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) );
            //string wordMlPath = word2007Path + ".xml";
            string encryptedPath = word2007Path + ".pdf";
            Log.Trace( Log.Category.Converters, Log.Priority.HIGH, String.Format( "Using temporary path '{0}'", word2007Path ) );

            try
            {
                //pf-3266 remove word automation office12
                //// Create the WordML file.
                //object oWordMlPath = wordMlPath;
                //source.BaseStream.Seek( 0, SeekOrigin.Begin );
                //byte[ ] writeBuffer = new byte[ 1024 ];
                //using( FileStream fs = new FileStream( wordMlPath, FileMode.Create ) )
                //{
                //    int bytesRead = 0;
                //    do
                //    {
                //        bytesRead = source.BaseStream.Read( writeBuffer, 0, writeBuffer.Length );
                //        fs.Write( writeBuffer, 0, bytesRead );
                //    } while( bytesRead > 0 );
                //}

                //// Create the Word2007 document Saved As....
                ////dn4
                ////ApplicationClass app = new ApplicationClass( );
                ////Application app = new Application();
                ////Microsoft.Office.Interop.Word.Document doc = null;
                //Microsoft.Office.Interop.Word.Application app = null;
                //WordDocument doc = null;
                //object oFalse = false;
                //object oMissing = Missing.Value;

                //try
                //{
                //    Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Opening document" );
                //    app = new Microsoft.Office.Interop.Word.Application();


                //    doc = app.Documents.Open( ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );

                //    app.Selection.WholeStory( );
                //    app.Selection.Fields.Update( );

                //    Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Updating TOC" );
                //    foreach (TableOfContents toc in doc.TablesOfContents)
                //    {
                //        toc.Update( );
                //    }

                //    Object oword2007Path = word2007Path;
                //    Object oSelectedFormat = selectedFormat;

                //    if (selectedFormat == WdSaveFormat.wdFormatPDF ||
                //         selectedFormat == WdSaveFormat.wdFormatXPS)
                //    {
                //        Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Calling v12 export function" );

                //        WdExportFormat exportFormat = (selectedFormat == WdSaveFormat.wdFormatPDF ? WdExportFormat.wdExportFormatPDF : WdExportFormat.wdExportFormatXPS);
                //        doc.ExportAsFixedFormat(word2007Path, exportFormat, false,
                //            WdExportOptimizeFor.wdExportOptimizeForPrint,
                //            WdExportRange.wdExportAllDocument,
                //            0, 0,
                //            WdExportItem.wdExportDocumentContent, true, true,
                //            WdExportCreateBookmarks.wdExportCreateWordBookmarks, true, false, false, ref oMissing);
                //    }
                //    else
                //    {
                //        Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Calling standard save function" );
                //        doc.SaveAs( ref oword2007Path, ref oSelectedFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oInsertLineBreaks, ref oMissing, ref oMissing, ref oMissing );
                //    }
                //}
                //catch( Exception ex )
                //{
                //    throw ex;
                //}
                //finally
                //{
                //    if( doc != null )
                //        doc.Close( ref oFalse, ref oMissing, ref oMissing );
                //    if( app != null )
                //        app.Quit( ref oFalse, ref oMissing, ref oMissing );

                //    doc = null;
                //    app = null;

                //    GC.Collect( );
                //}

                //pf-3266 remove word automation office12
                source.BaseStream.Seek(0, SeekOrigin.Begin);
                word2007Path = word2007Path + "." + filenameExtension;
                Perfectus.Common.SharedLibrary.GemBoxAPI.ConvertWord(source.BaseStream, word2007Path);
                //pf-3266

                string finalTempFile = word2007Path;

                // Maybe encode PDF documents
                if (selectedFormat == "wdFormatPDF")
                {
                    String permissionStr;

                    bool encryptPdf = System.Convert.ToBoolean( selectedFormatEntry[ "EncryptionEnabled" ] );
                    if( encryptPdf )
                    {
                        Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Encrypting PDF" );
                    
                        String sUserPassword = System.Convert.ToString( selectedFormatEntry[ "UserPassword" ] );
                        String sOwnerPassword = System.Convert.ToString( selectedFormatEntry[ "OwnerPassword" ] );
                        bool allowprinting = System.Convert.ToBoolean( selectedFormatEntry[ "allowprinting" ] );
                        bool allowDegradedPrinting = System.Convert.ToBoolean( selectedFormatEntry[ "allowDegradedPrinting" ] );
                        bool allowModifyContents = System.Convert.ToBoolean( selectedFormatEntry[ "allowModifyContents" ] );
                        bool allowCopy = System.Convert.ToBoolean( selectedFormatEntry[ "allowCopy" ] );
                        bool allowModifyAnnotations = System.Convert.ToBoolean( selectedFormatEntry[ "allowModifyAnnotations" ] );
                        bool allowFillIn = System.Convert.ToBoolean( selectedFormatEntry[ "allowFillIn" ] );
                        bool allowScreenReaders = System.Convert.ToBoolean( selectedFormatEntry[ "allowScreenReaders" ] );
                        bool allowAssembly = System.Convert.ToBoolean( selectedFormatEntry[ "allowAssembly" ] );

                        cfg = Config.Load( );

                        permissionStr =
                            GetNumberStringFromYNQ( allowprinting ) + GetNumberStringFromYNQ( allowModifyContents ) +
                            GetNumberStringFromYNQ( allowCopy ) + GetNumberStringFromYNQ( allowModifyAnnotations ) +
                            GetNumberStringFromYNQ( allowFillIn ) + GetNumberStringFromYNQ( allowScreenReaders ) +
                            GetNumberStringFromYNQ( allowAssembly ) + GetNumberStringFromYNQ( allowDegradedPrinting );

                        Log.Trace( Log.Category.Converters, Log.Priority.HIGH, String.Format( "Permission: {0}, program: '{1}' '{2}'", permissionStr, cfg.Encrypt995Folder, cfg.Encrypt995ExecutableName ) );
                    
                        EncryptDocument( 
                            word2007Path,
                            encryptedPath,
                            cfg.Encrypt995Folder,
                            cfg.Encrypt995ExecutableName,
                            sUserPassword,
                            sOwnerPassword,
                            permissionStr,
                            cfg.EncryptLevel );

                        Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "PDF file successfully encrypted" );

                        // Swap file names around so that we return the encrypted file.
                        finalTempFile = encryptedPath;
                    }
                }

                // Finally save to memory stream and return.
                MemoryStream ms = new MemoryStream( );
                byte[ ] buffer = new byte[ 1024 ];
                using( FileStream fs = File.OpenRead( finalTempFile.ToString( ) ) )
                {
                    int bytesRead = 0;
                    do
                    {
                        bytesRead = fs.Read( buffer, 0, buffer.Length );
                        ms.Write( buffer, 0, bytesRead );
                    } while( bytesRead > 0 );
                }
                ms.Seek( 0, SeekOrigin.Begin );
                return ms;
            }
            catch
            {
                throw ;
            }
            finally
            {
                try
                {
                    // Delete all files that might or might not have been created. Not necessary to check as exception
                    // is not thrown if file does not exist.
                    //File.Delete( wordMlPath );
                    File.Delete( word2007Path );
                    File.Delete( encryptedPath );
                }
                catch
                {
                    Log.Trace( Log.Category.Converters, Log.Priority.HIGH, "Word2007 converter could not delete the temporary files" );
                }
            }
		}

        private void EncryptDocument( string inPath, string outPath, string appWorkingDirectory, string executableName, string userPassword, string ownerPassword, string mask, string encryptLevel )
        {
            Process p = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = executableName;
            startInfo.WorkingDirectory = appWorkingDirectory;
            
            if (userPassword == null )
                userPassword = String.Empty;
            if (ownerPassword == null )
                ownerPassword = String.Empty;
            
            FileInfo _File = new FileInfo(inPath);
            String fi = _File.DirectoryName + "\\" + _File.Name;

            _File = new FileInfo(outPath);
            String fo = _File.DirectoryName + "\\" + _File.Name;

            startInfo.Arguments = "\"" + fi + "\" \"" + fo + "\" \"" + userPassword + "\" \"" + ownerPassword + "\" " +
                mask + " " + encryptLevel;
            startInfo.CreateNoWindow = true;

            Log.Trace( Log.Category.Converters, Log.Priority.HIGH, String.Format( "Program '{0}', directory: '{1}', Argument: '{2}'", executableName, appWorkingDirectory, startInfo.Arguments ) );

            p.StartInfo = startInfo;

            p.Start();
            if (!p.WaitForExit(30000))
                throw new Exception("Encryption timed out");
        }

        private string GetNumberStringFromYNQ(bool valueToConvert)
        {
            if (valueToConvert)
            {
                return "1";
            }
            else
            {
                return "0";
            }
        }
        
        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }
	}
}
