using System;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace Perfectus.Server.Plugins.Converters.ActivePdf
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		public Config()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private string serverIPAddress;
		private string portNumber;
		private string timeout;
		private string folderPath;


		[XmlElement]
		public string ServerIPAddress
		{
			get { return serverIPAddress; }
			set { serverIPAddress = value; }
		}

		[XmlElement]
		public string PortNumber
		{
			get { return portNumber; }
			set { portNumber = value; }
		}
		
		[XmlElement]
		public string TimeOut
		{
			get { return timeout; }
			set { timeout = value; }
		}

		[XmlElement]
		public string FolderPath
		{
			get { return folderPath; }
			set { folderPath = value; }
		}

        public static Config Load()
        {
            System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));
            // Get the current location.
            string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
            using (StreamReader sr = new StreamReader(configPath))
            {
                return (Config)(serialiser.Deserialize(sr));
            }
        }

        public void Save()
        {
            System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));
            // Get the current location.
            string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
            using (StreamWriter sw = new StreamWriter(configPath, false))
            {
                serialiser.Serialize(sw, this);
            }
        }

	}
}
