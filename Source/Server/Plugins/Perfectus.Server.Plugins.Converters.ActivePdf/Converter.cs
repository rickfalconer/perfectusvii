using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Drawing.Design;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using Perfectus.Server.PluginKit;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.Plugins.Converters.ActivePdf
{
	/// <summary>
	/// Summary description for Converter.
	/// </summary>
	public class Converter:IConverter
	{
		const int SUCCESS_SUBMIT = 0;
		const int SUCCESS_CONVERT = 1;

		public Converter()
		{
		}
	
		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get
			{
                return new Guid("616D759A-BA78-437c-B8BA-9538A2DBC436");
            }
		}                     

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

		private YesNoQueryValue encryptionEnabled = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue EncryptionEnabled
		{
			get { return encryptionEnabled; }
			set { encryptionEnabled = value; }
		}
        
		private TextQuestionQueryValue userPassword = null;

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue UserPassword
		{
			get { return userPassword; }
			set { userPassword = value; }
		}

		private TextQuestionQueryValue ownerPassword = null;
		
		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue OwnerPassword
		{
			get { return ownerPassword; }
			set { ownerPassword = value; }
		}
		private YesNoQueryValue allowPrinting = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowPrinting
		{
			get { return allowPrinting; }
			set { allowPrinting = value; }
		}

		//--------------------------------------------------------
		private YesNoQueryValue allowEdit = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowEdit
		{
			get { return allowEdit; }
			set { allowEdit = value; }
		}
		//--------------------------------------------------------

		private YesNoQueryValue allowCopy = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowCopy
		{
			get { return allowCopy; }
			set { allowCopy = value; }
		}

		private YesNoQueryValue allowModify = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowModify
		{
			get { return allowModify; }
			set { allowModify = value; }
		}


		private YesNoQueryValue allowFillIn	 = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowFillIn
		{
			get { return allowFillIn; }
			set { allowFillIn = value; }
		}

		private YesNoQueryValue allowMakeAccessible = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowMakeAccessible
		{
			get { return allowMakeAccessible; }
			set { allowMakeAccessible = value; }
		}

		private YesNoQueryValue allowAssembly = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowAssembly
		{
			get { return allowAssembly; }
			set { allowAssembly = value; }
		}

		private YesNoQueryValue allowReproduce = null;
	    private StringBindingMetaData instanceMetaData;

	    [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowReproduce
		{
			get { return allowReproduce; }
			set { allowReproduce = value; }
		}


		public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
		{
            convertedFileName = string.Format("{0}.pdf", fileNameWithoutExtension);
            //pf-3266 remove word automation office12
            //string wordmlFileName = string.Format("{0}.xml", fileNameWithoutExtension);
            //string wordFileName = string.Format("{0}.doc", fileNameWithoutExtension);
            string wordFileName = string.Format("{0}.docx", fileNameWithoutExtension);

            Config cfg = Config.Load();

			string folderPath = cfg.FolderPath;
            folderPath = folderPath.TrimEnd('\\').Trim();

            //object oWordMlPath = null;
            //oWordMlPath = folderPath + @"\" + wordmlFileName;
            object oConvertedWordFile = null;
            oConvertedWordFile = folderPath + @"\" + wordFileName;
			string oConvertedPDFFile = null;
            oConvertedPDFFile = folderPath + @"\" + convertedFileName;

			// Clear old temp files
			try
			{
				if (File.Exists(oConvertedWordFile.ToString()))
				{
					File.Delete(oConvertedWordFile.ToString());
				}
				if (File.Exists(oConvertedPDFFile.ToString()))
				{
					File.Delete(oConvertedPDFFile.ToString());
				}
			}
			catch
			{
				Trace.WriteLine("Old files still in use.");
			}

            //pf-3266 remove word automation office12
            //         // Get source stream into a WordML file
            //         source.BaseStream.Seek(0, SeekOrigin.Begin);
            //byte[] writeBuffer = new byte[1024];
            //using (FileStream fs = new FileStream(oWordMlPath.ToString(), FileMode.Create))
            //{
            //	int bytesRead = 0;
            //	do
            //	{
            //		bytesRead = source.BaseStream.Read(writeBuffer, 0, writeBuffer.Length);
            //		fs.Write(writeBuffer, 0, bytesRead);
            //	} while (bytesRead > 0);
            //}

            //         // Convert WordML to Word
            //         //dn4
            //         //ApplicationClass app = new ApplicationClass();
            //         Application app = new Application();

            //try
            //{
            //	object oMissing = Missing.Value;
            //	Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //	app.Selection.WholeStory();
            //	app.Selection.Fields.Update();

            //	foreach (TableOfContents toc in doc.TablesOfContents)
            //	{
            //		toc.Update();
            //	}

            //	object oDocFormat = WdSaveFormat.wdFormatDocument;
            //	doc.SaveAs(ref oConvertedWordFile, ref oDocFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //	object oFalse = false;
            //	doc.Close(ref oFalse, ref oMissing, ref oMissing);
            //	app.Quit(ref oFalse, ref oMissing, ref oMissing);
            //         }
            //catch (Exception ex)
            //{
            //	Trace.WriteLine(ex.Message);
            //}
            //finally
            //{
            //	// Delete temp wordml file
            //	try
            //	{
            //		if (File.Exists(oWordMlPath.ToString()))
            //		{
            //			File.Delete(oWordMlPath.ToString());
            //		}
            //	}
            //	catch
            //	{
            //		Trace.WriteLine(string.Format("ActivePDF converter could not delete the temporary file {0}", oWordMlPath.ToString()));
            //	}
            //}

            //pf-3266 remove word automation office12
            source.BaseStream.Seek(0, SeekOrigin.Begin);
            Perfectus.Common.SharedLibrary.GemBoxAPI.ConvertWord(source.BaseStream, (string)oConvertedWordFile);
            //pf-3266

            // Convert Word to PDF and return the PDF file to a stream
            try
            {
				int convertStatus = ConvertToPDF(oConvertedWordFile.ToString(),folderPath,cfg);

				if (convertStatus == SUCCESS_CONVERT && File.Exists(oConvertedPDFFile))
				{
					// Read the converted PDF file to stream 
					MemoryStream ms = new MemoryStream();

					byte[] buffer = new byte[1024];
					using (FileStream fs = File.OpenRead(oConvertedPDFFile.ToString()))
					{
						int bytesRead = 0;
						do
						{
							bytesRead = fs.Read(buffer, 0, buffer.Length);
							ms.Write(buffer, 0, bytesRead);
						} while (bytesRead > 0);
					}

					ms.Seek(0, SeekOrigin.Begin);
					return ms;
				}
				else
				{
					Trace.WriteLine(string.Format("ActivePDF converter has error on ConvertPDF {0} (Check /windows/apconv.log for details)",convertStatus.ToString()));
					return null;
				}
			}
			finally
			{
				// Delete converted word and pdf file
				try
				{
					Trace.WriteLine("Delete doc and pdf file.");

					if (File.Exists(oConvertedWordFile.ToString()))
					{
						File.Delete(oConvertedWordFile.ToString());
					}
					if (File.Exists(oConvertedPDFFile))
					{
						File.Delete(oConvertedPDFFile);
					}
				}
				catch
				{
					Trace.WriteLine("ActivePDF converter could not delete the temporary files");
				}
			}
		}

	    [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }

	    /// <summary>
        /// Invokes the ActivePDF server and DocConverter service to convert the Doc document to PDF
        /// </summary>
        /// <param name="file"></param>
        /// <param name="folder"></param>
        /// <param name="cfg"></param>
        /// <returns></returns>
		private int ConvertToPDF(string file,string folder,Config cfg)
		{
			int convertStatus = 0;
			APDocConvNET.APDocConverter docConverter = new APDocConvNET.APDocConverter();
			
            // set security option
			AnswerProvider.Caller caller = AnswerProvider.Caller.Assembly;
			bool encryptPdf=false;
			if(encryptionEnabled != null)
			{
				encryptPdf = encryptionEnabled.Evaluate(false, caller);	
			}			
			if(encryptPdf)
			{
				string sUserPassword = "";
				string sOwnerPassword = "";

				if(userPassword != null)
				{
					sUserPassword = userPassword.Evaluate(caller, instanceMetaData);
				}
				if(ownerPassword != null)
				{
					sOwnerPassword = ownerPassword.Evaluate(caller, instanceMetaData);
				}

				if(sUserPassword == null || sUserPassword.Trim().Length == 0)
				{
					sUserPassword = "";
				}

				if(sOwnerPassword == null || sOwnerPassword.Trim().Length == 0)
				{
					sOwnerPassword = "";
				}

				docConverter.SetSecurityOptions128(sUserPassword,sOwnerPassword,
					GetEncryptionPermission(allowPrinting,caller),GetEncryptionPermission(allowEdit,caller),
					GetEncryptionPermission(allowCopy,caller),GetEncryptionPermission(allowModify,caller),
					GetEncryptionPermission(allowFillIn,caller),GetEncryptionPermission(allowMakeAccessible,caller),
					GetEncryptionPermission(allowAssembly,caller),GetEncryptionPermission(allowReproduce,caller));
			}

			// Get Submission properties from config file
			string serverIP = cfg.ServerIPAddress;
			int port = Convert.ToInt32(cfg.PortNumber);
			short timeout = Convert.ToInt16(cfg.TimeOut);

			// Submit file to ActivePDF server and do the conversion
			docConverter.StatusTimeout=timeout;
			int intReturnSubmit=-1;
			intReturnSubmit = docConverter.Submit(serverIP, port, file, folder, folder, folder, "", "", 0, "");

            if (intReturnSubmit == SUCCESS_SUBMIT)
			{
				convertStatus =  docConverter.CheckStatus(serverIP, port, file, folder, folder);
			}

			// Release Object
			docConverter = null;
			return convertStatus; 
		}

		/// <summary>
		/// Get the encryption setting from the answers to property 
		/// and convert the boolean value to integer value.
		///  Yes - 1
		///  No - 0 
		/// </summary>
		/// <param name="valueToConvert"></param>
		/// <param name="caller"></param>
		/// <returns></returns>
		private int GetEncryptionPermission(YesNoQueryValue valueToConvert,AnswerProvider.Caller caller)
		{
			if (valueToConvert != null)
			{
				bool permissionVal = valueToConvert.Evaluate(true,caller);
				if(permissionVal)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
			else
			{
				// set the security permission to be Yes if no answer
				return 1;
			}
			
		}
	}
}
