using Perfectus.Server.PluginKit;
using System;
using System.Globalization;
using System.Threading;

namespace Perfectus.Server.Plugins.FormatStringExtensions.NameFormatter
{
    public class Formatter : IStringFormatter
    {

        /// <summary>
        /// Summary description for Formatter.
        /// </summary>
        /* 
		
        CONFIGURATION SAMPLES FOR GETTING THIS PLUGIN TO WORK:
		
        Sample Server shared configuration entry:
			
        <plugin type="formatStringExtension" assemblyName="Perfectus.Server.Plugins.FormatStringExtensions.NameFormatter.dll"
        typeName="Perfectus.Server.Plugins.FormatStringExtensions.NameFormatter.Formatter" key="NameFormatter">
        <name>Name Formatter</name>
        </plugin>
			
		
        Sample IPManager configuration entry:
		
        <formatStringExtensions>	
        <formatString forDataType="TextFormats" serverSidePluginKey="NameFormatter">
        <name>Name Formatter</name>	
        </formatString>		
        </formatStringExtensions>	
			
        Note - 	The serverSidePluginKey property has the value of the key specified in the server configuration file. 
        Available "forDataType" formats are: TextFormats, NumberFormats and DateFormats			
		
        */

        private const string LOWERCASE_FORMAT_KEY = "lower";
        private const string UPPERCASE_FORMAT_KEY = "upper";
        private const string TITLECASE_FORMAT_KEY = "title";
        private const string NAMEINFIRSTCAP_FORMAT_KEY = "namefirstcap";
        private const string CASENAMEINUPPERCASE_FORMART_KEY = "casenameuppercap";

        public Formatter()
        {
        }

        public string FormatString(string input, string parameter)
        {
            try
            {

                string formattedString = "";

                if (parameter.Length > 0)
                {
                    input = input.ToLower();
                    parameter = parameter.ToLower();

                    if (parameter.IndexOf(NAMEINFIRSTCAP_FORMAT_KEY) >= 0)
                    {
                        formattedString = NameInFirstCap(input);
                    }
                    if (parameter.IndexOf(CASENAMEINUPPERCASE_FORMART_KEY) >= 0)
                    {
                        formattedString = CaseNameUpperCap(input);
                    }
                }
                else
                {
                    // if no format paramter defined, return original input string
                    formattedString = input;
                }
                return formattedString;
            }
            catch
            {
                return input;
            }
        }

        /// <summary>
        /// This is added to all first and last names fields as well as case title fields when in title case (e.g. In the case of Bloggs v. the United Kingdom, Elena d'Amico, ...)
        /// leaves "de ", "d'", "di ", "van ", "von ", "the ", "of ", "and ", "et ", "autres ", "c. ", "v. ", "i " in lower case
        /// But keep anonymous names in the uppercase (e.g. N.M. v. Italy)
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        private string NameInFirstCap(string input)
        {
            string formattedString = "";

            input = input.Trim();
            string[] parts = input.Split(' ');

            int wordIndex = 0;
            foreach (string part in parts)
            {
                // Handle special char "İ", if contains this character then convert it to "i"
                string formattedPart = part.Replace(Convert.ToChar(304), 'i');

                if (!IsSpecialChars(formattedPart))
                {
                    formattedPart = ApplyFormat(formattedPart, TITLECASE_FORMAT_KEY);
                    formattedPart = NameWithApostrophe(formattedPart, wordIndex == 0);
                }
                else
                {
                    formattedPart = ApplyFormat(formattedPart, LOWERCASE_FORMAT_KEY);
                }

                formattedString = formattedString + formattedPart + " ";
                wordIndex++;
            }

            formattedString = formattedString.Trim();

            // Trim the space after Mac and Mc
            if (formattedString.IndexOf("Mac ") >= 0)
            {
                formattedString = formattedString.Replace("Mac ", "Mac");
            }
            if (formattedString.IndexOf("Mc ") >= 0)
            {
                formattedString = formattedString.Replace("Mc ", "Mc");
            }

            return formattedString;
        }

        /// <summary>
        /// If name beginning with an article and apostrophe,like "d'", "l'", and the word is the first word in the words, appear with the first letter in the upper case,
        /// the straight apostrophe becomes a smart one and the next letter is in the upper case.
        /// e.g. D'Amico v. Italy
        /// But if the word is not the first word in the name then 
        /// e.g. Elena d'Amico
        /// This function is integrated into NameInFirstCap
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        private string NameWithApostrophe(string word, bool isFirstWord)
        {
            string formattedWord = "";

            if (word.Length > 1 && word.Substring(1, 1) == "'")
            {
                string firstChar = word.Substring(0, 1);
                string thirdsChar = word.Substring(2, 1);
                string restChars = "";
                if (word.Length > 2)
                {
                    restChars = word.Substring(3);
                }
                if (isFirstWord)
                {
                    formattedWord = firstChar.ToUpper() + "'" + thirdsChar.ToUpper() + restChars;
                }
                else
                {
                    formattedWord = firstChar.ToLower() + "'" + thirdsChar.ToUpper() + restChars;
                }
            }
            else
            {
                formattedWord = word;
            }
            return formattedWord;
        }

        /// <summary>
        /// Convert the input name into upper case, but leave "v." and "c." are to remain in the lower case 
        /// e.g. BLOGGS v. THE UNITED KINGDOM
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        private string CaseNameUpperCap(string input)
        {
            // Handle special char "ı", if contains this character convert it to "I" 
            input = input.Replace(Convert.ToChar(305), 'I');

            string formattedString = ApplyFormat(input, UPPERCASE_FORMAT_KEY);

            formattedString = formattedString.Replace(" V. ", " v.");
            formattedString = formattedString.Replace(" C. ", " c.");

            return formattedString.Trim();

        }

        /// <summary>
        /// Format input string to lower/upper/title case based on the format key
        /// </summary>
        /// <param name="input"></param>
        /// <param name="formatKey"></param>
        /// <returns></returns>
        private string ApplyFormat(string input, string formatKey)
        {
            string formattedString = input;

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            if (formatKey == LOWERCASE_FORMAT_KEY)
            {
                formattedString = textInfo.ToLower(input);
            }
            else if (formatKey == UPPERCASE_FORMAT_KEY)
            {
                formattedString = textInfo.ToUpper(input);
            }
            else if (formatKey == TITLECASE_FORMAT_KEY)
            {
                formattedString = textInfo.ToTitleCase(input);
            }

            return formattedString.Trim();
        }

        /// <summary>
        /// Check if the string is one of the special strings 
        /// which needs to remain lower case when format as "NameInFirstCap".
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        private bool IsSpecialChars(string chars)
        {
            chars = chars.ToLower();
            switch (chars)
            {
                case "de":
                    return true;
                case "di":
                    return true;
                case "van":
                    return true;
                case "von":
                    return true;
                case "the":
                    return true;
                case "of":
                    return true;
                case "and":
                    return true;
                case "et":
                    return true;
                case "autres":
                    return true;
                case "c.":
                    return true;
                case "v.":
                    return true;
                case "i":
                    return true;
                default:
                    return false;
            }
        }
    }
}
