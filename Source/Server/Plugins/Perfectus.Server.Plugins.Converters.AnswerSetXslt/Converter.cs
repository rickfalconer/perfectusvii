using System;
using System.ComponentModel;
using System.IO;
using System.Xml.XPath;
using System.Xml.Xsl;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using System.Drawing.Design;

namespace Perfectus.Server.Plugins.Converters.AnswerSetXslt
{
	/// <summary>
	/// Transform an AnswerSet using an eXtensible Stylesheet Language Transformation (XSLT).
	/// </summary>
	public class Converter : IConverter
	{
		private TextQuestionQueryValue xsltFilePath = null;
		private TextQuestionQueryValue outputFileName = null;
	    private StringBindingMetaData instanceMetaData;

	    /// <summary>
		/// Document type being converted.
		/// </summary>
		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.AnswerSetXml; }
		}

		/// <summary>
		/// Unique identifier.
		/// </summary>
		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("A27F476D-2214-4640-9897-F99D369AA331"); }
		}

		/// <summary>
		/// Convert the input memory stream using the supplied XSLT.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="fileNameWithoutExtension"></param>
		/// <param name="convertedFileName"></param>
		/// <returns></returns>
		public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
		{
				
			// Check arguments.
            string lXsltFilePath = xsltFilePath.Evaluate(AnswerProvider.Caller.System, instanceMetaData);
			if (!File.Exists(lXsltFilePath))
			{
				throw new ArgumentException(string.Format("XSLT file '{0}' not found", xsltFilePath));
			}
				
			// Set the output file name.
            convertedFileName = GetOutputFileName(fileNameWithoutExtension);

			MemoryStream ms = new MemoryStream();
			
			using (MemoryStream newStream = new MemoryStream())
			{
				// Copy the input stream data to a new memory stream because the Transform below manages to 
				// close the original stream after finishing with it!
				source.BaseStream.Seek(0, SeekOrigin.Begin);
				byte[] newBytes = new byte[4096];
				int bytesRead;
				while((bytesRead = source.BaseStream.Read(newBytes, 0, 1))>0)
				{
					newStream.Write(newBytes, 0, bytesRead);
				}
				newStream.Seek(0, SeekOrigin.Begin);

				// Load the XSLT and transform the stream.
				XslTransform transform = new XslTransform();
				transform.Load(lXsltFilePath);
				XPathDocument xPathDocument = new XPathDocument(newStream);
				transform.Transform(xPathDocument, null, ms, null);

				// Reset both the input and output streams.
				source.BaseStream.Seek(0, SeekOrigin.Begin);
				ms.Seek(0, SeekOrigin.Begin);			
			}
			return ms;
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }

	    /// <summary>
		/// Suggested output file name.
		/// </summary>
		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue OutputFileName
		{
			get { return outputFileName; }
			set { outputFileName = value; }
		}

		/// <summary>
		/// Fully pathed file name of the transformation to use.
		/// </summary>
		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue XsltFilePath
		{
			get { return xsltFilePath; }
			set { xsltFilePath = value; }
		}
		
		/// <summary>
		/// Get the suggested output file name.
		/// </summary>
		/// <returns></returns>
        private string GetOutputFileName(string suggestedFilename)
		{
            string returnedFilename;

            if (outputFileName != null && (outputFileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || outputFileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            {
                returnedFilename = outputFileName.Evaluate(AnswerProvider.Caller.Assembly, instanceMetaData);
            }
            else
            {
                returnedFilename = suggestedFilename;
            }

            if (returnedFilename == null || returnedFilename.Length == 0)
                returnedFilename = string.Format("{0}.xml", "transformed");

            if (!returnedFilename.ToLower().EndsWith(".xml"))
                returnedFilename = string.Format("{0}.xml", returnedFilename);

            return returnedFilename;
		}
	}
}
