using System;
using System.Text.RegularExpressions;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.FormatStringExtensions.DateFormatter
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Formatter : IStringFormatter
	{
		/*
		CONFIGURATION SAMPLES FOR GETTING THIS PLUGIN TO WORK:

		Sample Server shared configuration entry:

		<plugin type="formatStringExtension" assemblyName="Perfectus.Server.Plugins.FormatStringExtensions.DateFormatter.dll"
		typeName="Perfectus.Server.Plugins.FormatStringExtensions.DateFormatter.Formatter" key="DateFormatter">
		<name>Date Formatter</name>
		</plugin>

		Sample IPManager configuration entry:

		<formatString forDataType="DateFormats" serverSidePluginKey="DateFormatter">
		<name>Date Formatter</name>	
		</formatString>

		Note - 	The serverSidePluginKey property has the value of the key specified in the server configuration file. 
		Available "forDataType" formats are: TextFormats, NumberFormats and DateFormats		
		*/

		public Formatter()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public string FormatString(string inputString, string inputParams)
		{
			//catch empty input 
			if (inputString=="")
			{
				return "";
			}

			DateTime inputDate;
			string day;
			
			inputDate = Convert.ToDateTime(inputString);
			day = inputDate.Day.ToString();
			
			if((day == "1")||(day == "21")||(day == "31"))
			{
				day = day + "st";
			}
			else if ((day == "2")||(day == "22"))
			{
				day = day + "nd";
			}
			else if ((day == "3")||(day == "23"))
			{
				day = day + "rd";
			}
			else
			{
				day = day + "th";
			}
			
			string finalReturnDate;
			if(inputParams=="")
			{
				finalReturnDate = day + " of " + inputDate.ToString("MMMM") + " " + inputDate.ToString("yyyy");
			}
			else
			{
				finalReturnDate = Regex.Replace(inputParams, "{d}", day);
				finalReturnDate = Regex.Replace(finalReturnDate, "{m}", inputDate.ToString("MMMM"));
				finalReturnDate = Regex.Replace(finalReturnDate, "{y}", inputDate.ToString("yyyy"));
			}
			
            return finalReturnDate;		
		}
	
		//apply uppercase/lowercase to the input string
		private string ApplyParams(DateTime input, string inputParams)
		{
			return "";
		}
	}
}