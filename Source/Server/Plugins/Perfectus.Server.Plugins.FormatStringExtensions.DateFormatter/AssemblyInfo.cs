using System.Reflection;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus Server Date Format String Extension")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs