using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Converters.Doc.Word
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Converter : IConverter
	{
	    private StringBindingMetaData instanceMetaData;

	    public Converter()
		{
		}

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get 
		{
			return new Guid("FC2EA62D-ED2C-45d9-BB54-30AFEA689E57"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

        public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
        {
            //pf-3266 remove word automation office12 replace with error in log
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Legacy Word Doc converter not supported");
            convertedFileName = string.Empty;
            return null;

            //convertedFileName = string.Format("{0}.doc", fileNameWithoutExtension);

            //Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("# Word DOC Converter processing file '{0}'", convertedFileName));

            //         object oConvertedFileName = null;
            //source.BaseStream.Seek(0, SeekOrigin.Begin);

            //string emptyTempFile = Path.GetTempFileName();

            //string wordMlPath = emptyTempFile + ".xml";

            //object oWordMlPath = wordMlPath;

            //byte[] writeBuffer = new byte[1024];
            //using (FileStream fs = new FileStream(wordMlPath, FileMode.Create))
            //{
            //	int bytesRead = 0;
            //	do
            //	{
            //		bytesRead = source.BaseStream.Read(writeBuffer, 0, writeBuffer.Length);
            //		fs.Write(writeBuffer, 0, bytesRead);
            //	} while (bytesRead > 0);
            //}


            ////dn4
            //         //ApplicationClass app = new ApplicationClass();
            //         Application app = new Application();
            //try
            //{
            //	try
            //	{
            //                 Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Word is opening '{0}'", oWordMlPath.ToString()));

            //		object oMissing = Missing.Value;
            //		Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //		app.Selection.WholeStory();
            //		app.Selection.Fields.Update();

            //		foreach (TableOfContents toc in doc.TablesOfContents)
            //		{
            //			toc.Update();
            //		}

            //		object oDocFormat = WdSaveFormat.wdFormatDocument;

            //		oConvertedFileName = Path.GetTempFileName();

            //                 Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Word is saving '{0}'", oConvertedFileName.ToString()));
            //                 doc.SaveAs(ref oConvertedFileName, ref oDocFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

            //		object oFalse = false;
            //		doc.Close(ref oFalse, ref oMissing, ref oMissing);
            //		app.Quit(ref oFalse, ref oMissing, ref oMissing);
            //	}
            //	finally
            //	{
            //		try
            //		{
            //			if (File.Exists(wordMlPath))
            //			{
            //				File.Delete(wordMlPath);
            //			}
            //			if (File.Exists(emptyTempFile))
            //			{
            //				File.Delete(emptyTempFile);
            //			}

            //		}

            //		catch
            //		{
            //                     Log.Trace(Log.Category.Converters, Log.Priority.HIGH, string.Format("Word DOC converter could not delete the temporary file {0}", wordMlPath));
            //		}
            //	}

            //	MemoryStream ms = new MemoryStream();

            //	byte[] buffer = new byte[1024];
            //	using (FileStream fs = File.OpenRead(oConvertedFileName.ToString()))
            //	{
            //		int bytesRead = 0;
            //		do
            //		{
            //			bytesRead = fs.Read(buffer, 0, buffer.Length);
            //			ms.Write(buffer, 0, bytesRead);
            //		} while (bytesRead > 0);
            //	}

            //	ms.Seek(0, SeekOrigin.Begin);

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Word DOC Converter ended");
            //	return ms;
            //}
            //finally
            //{
            //	if (oConvertedFileName != null && File.Exists(oConvertedFileName.ToString()))
            //	{
            //		File.Delete(oConvertedFileName.ToString());
            //	}
            //}
        }

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }
	}
}