using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;
using System.Text;
using Perfectus.Library.Logging;
using Perfectus.Library.Exception;
using Perfectus.Library.Localisation;

namespace Perfectus.Server.Plugins.Distributors.Execute
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class ExecuteDistributor : DistributorBase, IDistributor
    {
        private Config config;

        /// <summary>
        ///		The ID of the package instance.
        /// </summary>
        protected Guid instanceId;
        protected string user;
        protected StringBindingMetaData metaData;

        // Assume the file is located in 'c:\windows\temp\the document.pdf'
        // 'the document'
        private string FileBaseName;
        private const string FileBaseNameIdent = "{#filebasename#}";
        // 'the document.pdf' 
        private string FileName;
        private const string FileNameIdent = "{#filename#}";
        // 'c:\windows\temp\the document.pdf'
        private string FilePathName;
        private const string FilePathNameIdent = "{#filepathname#}";
        // 'pdf'
        private string FileExtension;
        private const string FileExtensionIdent = "{#fileextension#}";
        // 'c:\windows\temp\'
        private string WorkingDirectory;
        private const string WorkingDiretoryIdent = "{#workingdiretory#}";
        // placeholder that will be replaced with nothing
        // (use this if you want to run a program that doesn't require any filename on the command line...)
        private const string IgnoreIdent = "{#ignore#}";

        /// <summary>
        /// The MetaData structure specific this the current package instance.
        /// </summary>
        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return metaData; }
            set { metaData = value; }
        }

        /// <summary>
        ///		The ID of the package instance within the database.
        /// </summary>
        [Browsable(false)]
        public Guid PackageInstanceId
        {
            get { return instanceId; }
            set { instanceId = value; }
        }

        [Browsable(false)]
        public string UserId
        {
            get { return user; }
            set { user = value; }
        }

        // The folder into which the document should be distributed
        private TextQuestionQueryValue executableName = new TextQuestionQueryValue();
        private TextQuestionQueryValue paraPrefix = new TextQuestionQueryValue();
        private TextQuestionQueryValue paraPostfix = new TextQuestionQueryValue();

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Application Pathname")]
        [Description("The complete path and filename of the external application.  The application is called with '<Application Pathname> <Prefix Argument> Document <Suffix Argument>'.")]
        public TextQuestionQueryValue ExecutableName
        {
            get { return executableName; }
            set { executableName = value; }
        }
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Prefix Argument")]
        [Description("Additional arguments to be inserted before the document name.")]
        public TextQuestionQueryValue PrefixArgument
        {
            get { return paraPrefix; }
            set { paraPrefix = value; }
        }
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Suffix Argument")]
        [Description("Additional arguments to be inserted after the document name.")]
        public TextQuestionQueryValue SuffixArgument
        {
            get { return paraPostfix; }
            set { paraPostfix = value; }
        }

        public ExecuteDistributor()
        {
            FileExtension = string.Empty;
            FileBaseName = string.Empty;
            FilePathName = string.Empty;
            FileName = string.Empty;
            FileExtension = string.Empty;
            WorkingDirectory = string.Empty;

            try
            {
                config = Config.Load();
            }
            catch
            {
                // If the config cannot be found then create one with default credentials
                config = new Config();
                try
                {
                    config.Save();
                    Log.Warn(new ID("Execute.ConfigurationFileCreated"));
                }
                catch (Exception ex2)
                {
                    throw new PerfectusException(new ID("Execute.FailedCreateConfiguration"), ex2);
                }
            }
        }

        public DistributionResult[] Distribute()
        {
            DistributionResult[] result = new DistributionResult[readers.Count];
            Log.Trace(Log.Category.ServerPlugins,
                "# Execute Distribute() called.");

            if (readers.Count == 0)
            {
                Log.Trace(" # Nothing to do. (No readers were appeneded to this distributor.)");
                return result;
            }

            // Get the folder into which the file(s) should be streamed.

            if (config.ExecutableName != null &&
                config.ExecutableName != string.Empty)
                Log.Trace(" #Executeable name enforced by configuration");
            else
                config.ExecutableName =
                    executableName.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
            if (config.ExecutableName == null)
                throw new PerfectusException(new ID("Execute.ExecuteableNameEmpty"));

            // Get the folder into which the file(s) should be streamed.
            string paramPrefix = String.Empty;
            string paramPostfix = String.Empty;

            if (paraPrefix != null &&
                paraPrefix.ValueType != Perfectus.Common.PackageObjects.TextQuestionQueryValue.TextQuestionQueryValueType.NoValue)
                paramPrefix = paraPrefix.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);

            if (paraPostfix != null &&
                paraPostfix.ValueType != Perfectus.Common.PackageObjects.TextQuestionQueryValue.TextQuestionQueryValueType.NoValue)
                paramPostfix = paraPostfix.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);

            // Write each stream to disc.
            for (int i = 0; i < readers.Count; i++)
            {
                // Set working folder
                WorkingDirectory = config.WorkingDirectory;
                if (WorkingDirectory.Length == 0)
                    WorkingDirectory = Path.GetTempPath();
                else
                    if (!Directory.Exists(WorkingDirectory))
                        throw new PerfectusException(new ID("Execute.WorkingFolderInvalid"), WorkingDirectory);

                if (!WorkingDirectory.EndsWith("\\"))
                    WorkingDirectory += "\\";

                // Set filename
                FileName = FileBaseName = (string)fileNames[i];
                int j;
                if ((j = FileName.LastIndexOf('.')) > 0)
                {
                    FileExtension = FileName.Substring(j + 1);
                    FileBaseName = FileName.Substring(0, j);
                }

                if (config.CreateTempFilename)
                {
                    if (FileExtension.Length > 0)
                        FileName = String.Format("{0}.{1}", FileBaseName = new FileInfo(Path.GetTempFileName()).Name, FileExtension);
                    else
                        FileName = FileBaseName = new FileInfo(Path.GetTempFileName()).Name;
                }
                // Here comes the complete name
                FilePathName = string.Format("{0}{1}", WorkingDirectory, FileName);

                result[i] = new DistributionResult(this.UniqueIdentifier, null, FilePathName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), FilePathName);

                StreamReader sr = (StreamReader)readers[i];
                sr.BaseStream.Seek(0, SeekOrigin.Begin);

                byte[] buffer = new byte[1024];
                using (FileStream fs = new FileStream(FilePathName, FileMode.Create))
                {
                    int bytesRead = 0;
                    do
                    {
                        bytesRead = sr.BaseStream.Read(buffer, 0, buffer.Length);
                        fs.Write(buffer, 0, bytesRead);
                    } while (bytesRead > 0);
                }

                StringBuilder argument = new StringBuilder();

                // Argument from the config file
                argument.Append(config.PrefixParameter);

                // Argument from the package, if not overriden
                if (!config.ReplaceParameter)
                    argument.Append(paramPrefix);

                // Yet not sure how to add the filename of the document
                argument.Append("@@FilenamePlaceholder@@");

                // Argument from the package, if not overriden
                if (!config.ReplaceParameter)
                    argument.Append(paramPostfix);

                // Argument from the config file
                argument.Append(config.PostfixParameter);

                if (Log.ShallTrace())
                {
                    Log.Trace("# FileExtension\t" + FileExtension);
                    Log.Trace("# FileBaseName\t" + FileBaseName);
                    Log.Trace("# FilePathName\t" + FilePathName);
                    Log.Trace("# FileName\t" + FileName);
                    Log.Trace("# FileExtension\t" + FileExtension);
                    Log.Trace("# WorkingDirectory\t" + WorkingDirectory);
                    Log.Trace("# Paremeter Prefix\t" + paramPrefix);
                    Log.Trace("# Paremeter Suffix\t" + paramPostfix);

                    Log.Trace("# Timeout\t" + config.TimeOut.ToString());
                    Log.Trace("# Shell\t" + config.UseShall.ToString());
                    Log.Trace("# ErrorDialog\t" + config.ErrorDialog.ToString());
                    Log.Trace("# CreateNoWindow\t" + config.CreateNoWindow.ToString());
                    Log.Trace("# WorkingDirectory\t" + config.WorkingDirectory);
                    Log.Trace("# ErrorIfExitCodeNotZero\t" + config.ErrorIfExitCodeNotZero.ToString());
                    Log.Trace("# ExecuteableName\t" + config.ExecutableName);
                    Log.Trace("# Prefix\t" + config.PrefixParameter);
                    Log.Trace("# Postfix\t" + config.PostfixParameter);
                    Log.Trace("# Override\t" + config.ReplaceParameter.ToString());
                    Log.Trace("# Create TMP file\t" + config.CreateTempFilename);
                    Log.Trace("# Cleanup time\t" + config.CleanUpTimeOut.ToString());
                }

                /// Maybe the user wants to specifiy the filename placeholders
                if (!argument.ToString().Contains("{#"))
                {
                    argument.Replace("@@FilenamePlaceholder@@", String.Format(" \"{0}\" ", FilePathName));
                }
                else
                {
                    argument.Replace("@@FilenamePlaceholder@@", " ");
                    argument.Replace(IgnoreIdent, String.Empty);
                    int k;
                    while (0 < (k = argument.ToString().ToLower().IndexOf(FileBaseNameIdent)))
                        argument = argument.Replace(argument.ToString().Substring(k, FileBaseNameIdent.Length), FileBaseName);

                    while (0 < (k = argument.ToString().ToLower().IndexOf(FileNameIdent)))
                        argument = argument.Replace(argument.ToString().Substring(k, FileNameIdent.Length), FileName);

                    while (0 < (k = argument.ToString().ToLower().IndexOf(FilePathNameIdent)))
                        argument = argument.Replace(argument.ToString().Substring(k, FilePathNameIdent.Length), FilePathName);

                    while (0 < (k = argument.ToString().ToLower().IndexOf(FileExtensionIdent)))
                        argument = argument.Replace(argument.ToString().Substring(k, FileExtensionIdent.Length), FileExtension);

                    while (0 < (k = argument.ToString().ToLower().IndexOf(WorkingDiretoryIdent)))
                        argument = argument.Replace(argument.ToString().Substring(k, WorkingDiretoryIdent.Length), WorkingDirectory);
                }
                ///
                ProcessStartInfo startInfo = new ProcessStartInfo(config.ExecutableName, argument.ToString().Trim());

                startInfo.WorkingDirectory = WorkingDirectory;
                startInfo.ErrorDialog = config.ErrorDialog;
                startInfo.CreateNoWindow = config.CreateNoWindow;
                startInfo.UseShellExecute = config.UseShall;
                Process p = new Process();
                p.StartInfo = startInfo;

                try
                {
                    Log.Trace("Calling '" + startInfo.FileName + " " + startInfo.Arguments);
                    p.Start();
                    if (config.TimeOut > 0)
                    {
                        if (!p.WaitForExit(config.TimeOut))
                        {
                            p.Kill();
                            throw new PerfectusException(new ID("Execute.HangingDetected"), config.TimeOut);
                        }
                        int exitCode = p.ExitCode;
                        p.Close();
                        if (config.ErrorIfExitCodeNotZero && exitCode != 0)
                        {
                            throw new PerfectusException(new ID("Execute.NoneZeroResult"), exitCode);
                        }
                        Log.Trace(string.Format("Success '" + startInfo.FileName, "' returned code {0}", exitCode));
                    }
                }

                finally
                {
                    int myCounter = 0;
                    if (config.TimeOut <= 0)
                        myCounter = 10;

                    do
                    {
                        try
                        {
                            if (myCounter > 0 && config.CleanUpTimeOut > 0)
                                System.Threading.Thread.Sleep(config.CleanUpTimeOut);

                            if (config.CleanUpTimeOut > 0)
                            {
                                File.Delete(FilePathName);
                                Log.Trace("Temporary file released");
                            }
                            else
                                Log.Trace("Temporary file remained");

                            myCounter = 0;
                        }
                        catch (Exception ex)
                        {
                            Log.Trace("Waiting for document to be freed");
                            myCounter--;
                        }
                    } while (myCounter > 0);
                }
            }
            Log.Trace("Execute distributor exiting");
            return result;
        }
        [Browsable(false)]
        public Guid UniqueIdentifier
        {
            get { return new Guid("5E61D36A-D52A-45ef-9B8C-30C60F5A20D0"); }
        }
    }
}

