using System;
using System.IO;
using System.Xml.Serialization;


namespace Perfectus.Server.Plugins.Distributors.Execute
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		public Config()
		{
			timeout =  60000;
			useShell = false;
			ErrorDialog = false;
			CreateNoWindow = true;
			WorkingDirectory = String.Empty;
			ErrorIfExitCodeNotZero = true;;
			executableName = String.Empty;
			prefixParameter= String.Empty;
			postfixParameter= String.Empty;
			overrideOtherParameter = false;
			createTempFilename = false;
			cleanUptimeout = 5000;
		}

		private bool useShell;
		private bool 	errorDialog;
		private bool createNoWindow ;
		private string workingDirectory;
		private string executableName ;
		private bool errorIfExitCodeNotZero;
		private int timeout;
		private string prefixParameter;
		private string postfixParameter;
		private bool overrideOtherParameter;
		private bool createTempFilename;
		private int cleanUptimeout;
		
		[XmlElement]
		public string WorkingDirectory
		{
			get { return workingDirectory; }
			set { workingDirectory = value.Trim(); }
		}
		
		[XmlElement]
		public string ExecutableName
		{
			get { return executableName; }
            set { executableName = value.Trim();  }
		}
		
		[XmlElement]
		public bool ErrorIfExitCodeNotZero
		{
			get { return errorIfExitCodeNotZero; }
			set { errorIfExitCodeNotZero = value; }
		}


		[XmlElement]
		public bool ReplaceParameter
		{
			get { return overrideOtherParameter; }
			set { overrideOtherParameter = value; }
		}
		
		[XmlElement]
		public string PrefixParameter
		{
			get { return prefixParameter; }
			set { prefixParameter = value; }
		}
		
		[XmlElement]
		public string PostfixParameter
		{
			get { return postfixParameter; }
			set { postfixParameter = value; }
		}
		
		[XmlElement]
		public int TimeOut
		{
			get { return timeout; }
			set { timeout = value; }
		}

		[XmlElement]
		public int CleanUpTimeOut
		{
			get { return cleanUptimeout; }
			set { cleanUptimeout = value; }
		}
		
		
		[XmlElement]
		public bool CreateTempFilename
		{
			get { return createTempFilename; }
			set { createTempFilename = value; }
		}

		[XmlElement]
		public bool UseShall
		{
			get { return useShell ; }
			set { useShell = value; }
		}

		[XmlElement]
		public bool ErrorDialog
		{
			get { return errorDialog; }
			set { errorDialog = value; }
		}

		[XmlElement]
		public bool CreateNoWindow
		{
			get { return createNoWindow; }
			set { createNoWindow = value; }
		}

		

		public static Config Load()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamReader sr = new StreamReader(configPath))
			{
				return (Config)(serialiser.Deserialize(sr));
			}
		}

		public void Save()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamWriter sw = new StreamWriter(configPath, false))
			{
				serialiser.Serialize(sw, this);
			}			
		}
	}
}
