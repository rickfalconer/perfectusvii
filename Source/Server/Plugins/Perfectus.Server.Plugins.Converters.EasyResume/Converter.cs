using System;
using System.ComponentModel;
using System.IO;
using System.Xml;
using Perfectus.Server.PluginKit;
using Perfectus.Common.PackageObjects;
using System.Drawing.Design;

namespace Perfectus.Server.Plugins.Converters.EasyResume
{
	/// <summary>
	/// Summary description for Converter.
	/// </summary>
	public class Converter : IConverter
	{
		private Config conf;

		public Converter()
		{
			conf = Config.Load();
		}

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get 
			{
				return new Guid("39AF6962-C47B-4b04-A093-A73246CD9053");
			}
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.AnswerSetXml; }
		}

		private TextQuestionQueryValue fileName = null;

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue FileName
		{
			get { return fileName; }
			set { fileName = value; }
		}


		private InterviewPage resumePage = null;
	    private StringBindingMetaData instanceMetaData;

	    [Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.PageListEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.PageListConverter, Studio")]
		public InterviewPage ResumePage
		{
			get { return resumePage; }
			set
			{
				resumePage = value;
			}
		}

		public Stream ConvertMemoryStream(StreamReader source, string filenameWithoutExtension, out string returnedFilename)
		{
            returnedFilename = GetFileName(filenameWithoutExtension);
			XmlDocument sourceXml = GetDocFromSource(source);
			string instanceId = GetInstanceId(sourceXml);
			//string version = GetVersion(sourceXml);
			//string revision = GetRevision(sourceXml);
			string pageId = GetPageId();
			string linkPath = GetLinkPath(instanceId,pageId);
			return GetFile(linkPath);
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }

	    private XmlDocument GetDocFromSource(StreamReader source)
		{
			XmlDocument sourceXml = new XmlDocument();
			source.BaseStream.Seek(0, SeekOrigin.Begin);
			sourceXml.Load(source.BaseStream);
			source.BaseStream.Seek(0, SeekOrigin.Begin);

			return sourceXml;
		}

		private string GetInstanceId(XmlDocument doc)
		{
			try 
			{
				return doc.SelectSingleNode("/answerSet/@packageInstanceId").Value.ToString();
			}
			catch
			{
				throw new Exception("Could not retrieve package instance from answerset.");
			}
		}

		private string GetPageId()
		{
			try 
			{
				return resumePage.UniqueIdentifier.ToString();
			}
			catch
			{
				throw new Exception("The Resume Page property was not specified.");
			}
		}

        private string GetFileName(string suggestedFilename)
		{
            string returnedFilename;

            if (fileName != null && (fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            {
                returnedFilename = fileName.Evaluate(AnswerProvider.Caller.Assembly, instanceMetaData);
            }
            else
            {
                returnedFilename = suggestedFilename;
            }

            if (returnedFilename == null || returnedFilename.Length == 0)
                returnedFilename = string.Format("{0}.html", "easyResume");

            if (!returnedFilename.ToLower().EndsWith(".html"))
                returnedFilename = string.Format("{0}.html", returnedFilename);

            return returnedFilename;
		}

		private string GetLinkPath(string instanceId, string pageId)
		{
			string retVal = conf.EasyResumeFormatString;
			retVal = String.Format(retVal,instanceId, pageId);
			return retVal;
		}

		private MemoryStream GetFile(string linkPath)
		{
			MemoryStream ms = new MemoryStream();
			StreamWriter sw = new StreamWriter(ms);
			sw.WriteLine("<html><head>");
			sw.WriteLine("<meta http-equiv=\"refresh\" content=\"0;url={0}\">",linkPath);
			sw.WriteLine("</head><body></body></html>");
			sw.Flush();

			ms.Position = 0;
			ms.Seek(0, SeekOrigin.Begin);	
			return ms;
		}
	}
}