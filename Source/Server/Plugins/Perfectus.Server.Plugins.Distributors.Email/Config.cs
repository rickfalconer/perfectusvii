using System;
using System.IO;
using System.Xml.Serialization;
using System.Net.Mail;


namespace Perfectus.Server.Plugins.Distributors.Email
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		public Config()
		{
            SmtpClient client = new SmtpClient();
            timeout = client.Timeout;
			deliveryMethod = client.DeliveryMethod;
            uploadDirectory = String.Empty;
            host = String.Empty;
            port = 25;
        }

		private int timeout;
        private string uploadDirectory;
        private SmtpDeliveryMethod deliveryMethod;
        private string host;
        private int port;
		
		[XmlElement]
		public string UploadDirectory
		{
            get { return uploadDirectory; }
            set { uploadDirectory = value.Trim(); }
		}
		
		[XmlElement]
		public int TimeOut
		{
			get { return timeout; }
			set { timeout = value; }
		}

        [XmlElement]
        public string Host
        {
            get { return host; }
            set { host = value.Trim(); }
        }

        [XmlElement]
        public int Port
        {
            get { return port; }
            set { port = value; }
        }

		[XmlElement]
		public SmtpDeliveryMethod DeliveryMethod
		{
            get { return deliveryMethod; }
            set { deliveryMethod = value; }
		}
        

		public static Config Load()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamReader sr = new StreamReader(configPath))
			{
				return (Config)(serialiser.Deserialize(sr));
			}
		}

		public void Save()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamWriter sw = new StreamWriter(configPath, false))
			{
				serialiser.Serialize(sw, this);
			}			
		}
	}
}
