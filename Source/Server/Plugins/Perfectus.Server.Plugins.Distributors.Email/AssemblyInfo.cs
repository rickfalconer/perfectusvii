using System;
using System.Reflection;
using System.Runtime.InteropServices;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server Email Distributor")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs