using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Xml;
using System.Net;
using System.Net.Mail;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Server.Plugins.Distributors.Email
{
    /// <summary>
    /// Summary description for Distributor.
    /// </summary>
    public class Distributor : DistributorBase, IDistributor
    {
        private Config config;

        private TextQuestionQueryValue toAddress = new TextQuestionQueryValue();
        private TextQuestionQueryValue fromAddress = new TextQuestionQueryValue();
        private TextQuestionQueryValue ccAddress = new TextQuestionQueryValue();
        private TextQuestionQueryValue bccAddress = new TextQuestionQueryValue();
        private TextQuestionQueryValue subject = new TextQuestionQueryValue();
        private TextQuestionQueryValue body = new TextQuestionQueryValue();

        private TextQuestionQueryValue insertInBodyFileTypes = new TextQuestionQueryValue();
        private TextQuestionQueryValue bodyEncoding = new TextQuestionQueryValue();

        private string server = string.Empty;

        [Browsable(false)]
        public Guid UniqueIdentifier
        {
            get { return new Guid("DBD5FC52-4B0C-4ed5-8987-A4CE7A04C82A"); }
        }

        [Browsable(true)]
        [DisplayName ("SMTP Server")]
        [Description("The location of the SMTP server that will be used to send the email.")]
        public string Server
        {
            get { return server; }
            set { server = value; }
        }

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [Description("The email address of the recipient of the mail. Separate multiple email addresses with a comma.")]
        [DisplayName ("To Address")]
        public TextQuestionQueryValue ToAddress
        {
            get { return toAddress; }
            set { toAddress = value; }
        }

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayName("From Address")]
        [Description("The email address of the sender of the mail. This does not have to be a real address.")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        public TextQuestionQueryValue From
        {
            get { return fromAddress; }
            set { fromAddress = value; }
        }


        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("CC Address")]
        [Description("The email address of the recipient that will receive a carbon copy of the mail. Separate multiple email addresses with a comma.")]
        public TextQuestionQueryValue CcAddress
        {
            get { return ccAddress; }

            set { ccAddress = value; }
        }

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayName("BCC Address")]
        [Description("The email address of the recipient that will receive a blind carbon copy of the mail. Separate multiple email addresses with a comma.")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        public TextQuestionQueryValue BccAddress
        {
            get { return bccAddress; }
            set { bccAddress = value; }
        }


        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Email Subject")]
        [Description("The subject of the email.")]
        public TextQuestionQueryValue Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Email Body")]
        [Description("The body of the email, unless files are specified in the property 'File Types for Body'.")]
        public TextQuestionQueryValue Body
        {
            get { return body; }
            set { body = value; }
        }
        
        public Distributor()
        {
            try
            {
                config = Config.Load();
            }
            catch
            {
                // If the config cannot be found then create one with default credentials
                config = new Config();
                try
                {
                    config.Save();
                    Log.Trace(" #Email - configuration file created automatically");
                }
                catch (Exception ex2)
                {
                    Log.Trace(" #Email - failed to create configuration file", ex2.Message);
                }
            }
        }

        
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName ("File Types for Body")]
        [Description("A document of the specified type will be inserted into the body. For example: *.html;*.htm")]
        public TextQuestionQueryValue InsertInBodyFileTypes
        {
            get { return insertInBodyFileTypes; }
            set { insertInBodyFileTypes = value; }
        }

        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Body Encoding")]
        [Description("The type of encoding to be used to format the body of an email. For example: utf-8")]
        public TextQuestionQueryValue BodyEncoding
        {
            get { return bodyEncoding; }
            set { bodyEncoding = value; }
        }

        public DistributionResult[] Distribute()
        {
            DistributionResult[] result = new DistributionResult[readers.Count];
            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# Email Distribute() called.");

            try
            {
                string to = default(String);
                string from = default(String);
                string cc = default(String);
                string bcc = default(String);
                string subj = default(String);
                string bod = default(String);
                string bodyFileTypes = default(String);
                Encoding bodyEncodingType = System.Text.Encoding.UTF8;
                bool bodyIsHTML = false;


                List<Attachment> attachments = new List<Attachment>();

                if (toAddress != null)
                {
                    to = toAddress.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (fromAddress != null)
                {
                    from = fromAddress.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (ccAddress != null)
                {
                    cc = ccAddress.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (bccAddress != null)
                {
                    bcc = bccAddress.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (subject != null)
                {
                    subj = subject.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (body != null)
                {
                    bod = body.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
                }

                if (insertInBodyFileTypes != null)
                {
                    bodyFileTypes = insertInBodyFileTypes.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);

                    if (bodyFileTypes == null)
                    {
                        bodyFileTypes = "";
                    }
                    else if (!bodyFileTypes.EndsWith(";"))
                    {
                        bodyFileTypes += ";";
                    }
                }

                if (bodyEncoding != null)
                {
                    string encodingName = bodyEncoding.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);

                    if (encodingName != null && encodingName.Length > 0)
                    {
                        try
                        {
                            bodyEncodingType = System.Text.Encoding.GetEncoding(encodingName);
                        }
                        catch (Exception ex)
                        {
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Convert body encoding throws error: {0}. Use default encoding: UTF-8.", ex.Message));
                        }
                    }
                }

                if (to == default(string) || to.Length == 0)
                    throw new Exception(" #Email# The TO address must not be empty");
                if (from == default(string) || from.Length == 0)
                    throw new Exception(" #Email# The FROM address must not be empty");


                if (readers.Count == 0 &&
                    (bod == null || bod.Length == 0) &&
                    (subj == null || subj.Length == 0))
                {
                    Log.Trace(Log.Category.Distributors, Log.Priority.MID, " # Nothing to do. (No readers, body or subject were appeneded to this distributor.)");
                    return null;
                }


                Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Email distributor is setting {0} as message body encoding", bodyEncodingType.ToString()));

                for (int i = 0; i < readers.Count; i++)
                {
                    // Write each stream to disc.
                    string fileName = (string)fileNames[i];
                    result[i] = new DistributionResult(this.UniqueIdentifier, null, fileName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), null);
                    string fileType = "*" + Path.GetExtension(fileName);

                    Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Email distributor processing file {0} with file type of {1}", fileName, fileType));

                    StreamReader sr = (StreamReader)readers[i];

                    sr.BaseStream.Seek(0, SeekOrigin.Begin);

                    Encoding encoding = System.Text.Encoding.ASCII;


                    if (bodyFileTypes != null && bodyFileTypes.IndexOf(fileType + ";") > -1)
                    {
                        Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Email distributor processing file {0} is attempting to put message in body", fileName));
                        string mailBody = sr.ReadToEnd();
                        
                        if (fileType.ToLower().StartsWith("*.htm"))
                        {
                            bodyEncodingType = System.Text.Encoding.ASCII;
                            bodyIsHTML = true;
                            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Email distributor processing file {0} is attempting to set the mail type to HTML", fileName));
                        }

                        SendTheMail(bcc, mailBody, cc, from, null, subj, to, bodyIsHTML, bodyEncodingType);
                    }
                    else
                    {
                        Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("Email distributor processing file {0} is attempting attach message to email", fileName));
                        Attachment attachment = new Attachment((Stream)sr.BaseStream, fileName);
                        attachments.Add(attachment);
                    }

                }
                // If we have some stuff to send then send it now
                if (attachments.Count > 0)
                {
                    Log.Trace(Log.Category.Distributors, Log.Priority.MID, "Sending attachment mail to", to, cc, bcc);
                    SendTheMail(bcc, bod, cc, from, attachments, subj, to, bodyIsHTML, bodyEncodingType);
                }
                if (readers.Count == 0)
                {
                    Log.Trace(Log.Category.Distributors, Log.Priority.MID, "Sending no document mail to", to, cc, bcc);
                    SendTheMail(bcc, bod, cc, from, null, subj, to, bodyIsHTML, bodyEncodingType);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "Email distributor exiting");
            return result;
        }

        private void SendTheMail(string bcc, string bod, string cc, string from, List<Attachment> attachments, string subj, string to, bool bodyIsHTML, System.Text.Encoding encodingType)
        {
            MailMessage msg = new MailMessage();

            foreach (string address in GetValidAdresses(to))
                msg.To.Add(new MailAddress(address));

            msg.From = new MailAddress(GetValidAdresses(from)[0]);

            if (cc != default(string) && cc.Length > 0)
                foreach (string address in GetValidAdresses(cc))
                    msg.CC.Add(new MailAddress(address));

            if (bcc != default(string) && bcc.Length > 0)
                foreach (string address in GetValidAdresses(bcc))
                    msg.Bcc.Add(new MailAddress(address));

            msg.Subject = subj;
            msg.Body = bod;
            msg.IsBodyHtml = bodyIsHTML;

            // Set encoding for the mail body message
            msg.BodyEncoding = encodingType;

            if (attachments != null)
                foreach (Attachment attach in attachments)
                {
                    msg.Attachments.Add(attach);
                }
            SmtpClient client;
            
            
            if (config.Host != String.Empty)
            {
                client = new System.Net.Mail.SmtpClient(config.Host);
                client.Port = config.Port;
            }
            else
                client = new System.Net.Mail.SmtpClient(server);

            if (config.TimeOut > client.Timeout)
                client.Timeout = config.TimeOut;

            client.DeliveryMethod = config.DeliveryMethod;
            if (config.DeliveryMethod == SmtpDeliveryMethod.SpecifiedPickupDirectory)
            {
                client.PickupDirectoryLocation = config.UploadDirectory;
                Log.Trace (String .Format ( " #Email using 'SpecifiedPickupDirectory' to {0}" ,  client.PickupDirectoryLocation ));
            }
            else
            Log.Trace (String .Format ( " #Email using '{0}'" ,  client.DeliveryMethod));
            client.Send(msg);
        }

        private string[] GetValidAdresses(string adr)
        {
            StringBuilder builder = new StringBuilder (adr);
            // "Piere@here""Frank@there" to "Piere@here" "Frank@there"
            builder.Replace ("\"\"", "\" \"");
            // "Piere@here" "Frank@there" to Piere@here Frank@there
            builder.Replace ( "\"", String.Empty );

            return builder.ToString().Split(new char[] { ',', ';' , ':','\t',' ','|'});
        }
    }
}