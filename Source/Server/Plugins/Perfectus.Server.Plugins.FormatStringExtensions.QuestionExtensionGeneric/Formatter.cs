/* 
 * Quick fix for SharePoint integration
 * Needed a plugin that could return the Display value of an item list
 * rather than the Item Value.
 * New Interface added,
 * needs modified Answerset DLL from Core and plugin dll
 */ 

namespace FormatQuestionExtension.ItemListDisplay
{
    using Perfectus.Server.PluginKit;
    using System;
    using System.Globalization;
    using System.Threading;
    using Perfectus.Common.PackageObjects;

    public class Formatter : IStringFormatter2
    {
        public Formatter()
        {   
        }

        public string FormatString(string input, string parameter)
        {
            return input;
        }

        public string FormatQuestion(Question inputQuestion, string input, string parameter)
        {
            parameter = parameter.Trim().ToLower();

            if (inputQuestion.Items != null && inputQuestion.Items.Count > 0)
            {
                if (String.Compare(parameter, "value", true) == 0)
                    return input;
                else
                {
                    foreach (ItemsItem item in inputQuestion.Items)
                        if (item.Value == input)
                            return item.Display;
                }
                return "Input string malformed";
            }
            return "Question must be item list to be used with 'FormatQuestionExtension.ItemListDisplay'";
        }
    }
}
