using System;
using System.ComponentModel;
using System.IO;
using Perfectus.Server.PluginKit;
using System.Xml;
using Perfectus.Common.PackageObjects;
using System.Drawing.Design;

namespace Perfectus.Server.Plugins.Converters.MirrorXml
{
	/// <summary>
	/// Summary description for Converter.
	/// </summary>
	public class Converter : IConverter
	{
		public Converter()
		{
		}

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("3542E8D6-F18E-442e-8683-8C87CDF0A666");
			}
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.AnswerSetXml; }
		}
//
//		[Browsable(true)]
//		public string FileName
//		{
//			get { return fileName; }
//			set { fileName = value; }
//		}

		private TextQuestionQueryValue fileName = null;
	    private StringBindingMetaData instanceMetaData;

	    [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue FileName
		{
			get { return fileName; }
			set { fileName = value; }
		}


		public Stream ConvertMemoryStream(StreamReader source, string filenameWithoutExtension, out string returnedFilename)
		{
            returnedFilename = GetFileName(filenameWithoutExtension);
			
			// Load the input XML into a document. Make sure the input stream is rewound prior to loading it into the doc.
			XmlDocument sourceXml = new XmlDocument();
			source.BaseStream.Seek(0, SeekOrigin.Begin);
			sourceXml.Load(source.BaseStream);
			source.BaseStream.Seek(0, SeekOrigin.Begin);
			
			// create a new XML document, load the original into it, then perform mirror swaps on it.
			XmlDocument mirrorDoc = new XmlDocument();		
			mirrorDoc.LoadXml(sourceXml.DocumentElement.OuterXml);													
			AnswerAcquirer.Xml.XmlToMirrorAnswerSet.CreateMirror(mirrorDoc);
			AnswerAcquirer.Xml.XmlToMirrorAnswerSet.CleanUpMirrorDocument(mirrorDoc);
						
			// Set up a memory stream to contain the output of mirror answerset and save the mirror answerSet to it
			MemoryStream ms = new MemoryStream();
			mirrorDoc.Save(ms);

			// Rewind and return the stream containing the mirror answerset.
			ms.Seek(0, SeekOrigin.Begin);			
			return ms;
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }

        private string GetFileName(string suggestedFilename)
        {
            string returnedFilename;

            if (fileName != null && (fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            {
                returnedFilename = fileName.Evaluate(AnswerProvider.Caller.Assembly, instanceMetaData);
            }
            else
            {
                returnedFilename = suggestedFilename;
            }

            if (returnedFilename == null || returnedFilename.Length == 0)
                returnedFilename = string.Format("{0}.xml", "mirrorAnswers");

            if (!returnedFilename.ToLower().EndsWith(".xml"))
                returnedFilename = string.Format("{0}.xml", returnedFilename);

            return returnedFilename;
        }
	}
}