using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Distributors.FileSystem
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Distributor : DistributorBase, IDistributor
	{
		// The folder into which the document should be distributed
		private TextQuestionQueryValue fileSystemFolder = new TextQuestionQueryValue();

		[Browsable(true)]
        [Description("The path to the folder where the assembled documents will be placed.")]
        [DisplayName("File System Folder")]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue FileSystemFolder
		{
			get { return fileSystemFolder; }
			set { fileSystemFolder = value; }
		}

		public Distributor()
		{
		}

		public DistributionResult[] Distribute()
		{
			DistributionResult[] result = new DistributionResult[readers.Count];

            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# File System Distribute() called.");

			if (readers.Count == 0)
			{
                Log.Trace(Log.Category.Distributors, Log.Priority.MID, "# Nothing to do. (No readers were appeneded to this distributor.)");
				return result;
			}

			// Get the folder into which the file(s) should be streamed.
            if (fileSystemFolder == null)
            {
                Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# FileSystemFolder must not be null.");
                throw new ArgumentNullException("FileSystemFolder", "FileSystemFolder must not be null.");
            }

			string folder = fileSystemFolder.Evaluate(AnswerProvider.Caller.System, this.InstanceMetaData);
			if (folder == null)
			{
				Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# FileSystemFolder evaluated to null.");
				throw new ArgumentNullException("FileSystemFolder", "FileSystemFolder evaluated to null.");
			}

            DirectoryInfo dirInfo = new DirectoryInfo(folder);
            if (!dirInfo.Exists)
            {
                throw new DirectoryNotFoundException(string.Format("# The requested directory '{0}' does not exist.", dirInfo.FullName));
            }

			// Write each stream to disc.
			for (int i=0; i < readers.Count; i++)
			{

				string fileName = (string)fileNames[i];
				string fullPath = string.Format("{0}{1}{2}", folder, Path.DirectorySeparatorChar, fileName);

				result[i] = new DistributionResult(this.UniqueIdentifier, null, fileName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), fullPath);

                Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, string.Format("# Writing file '{0}'.", fullPath));

				StreamReader sr = (StreamReader)readers[i];
				sr.BaseStream.Seek(0, SeekOrigin.Begin);

				byte[] buffer = new byte[1024];
				using (FileStream fs = new FileStream(fullPath, FileMode.Create))
				{
					int bytesRead = 0;
					do
					{
						bytesRead = sr.BaseStream.Read(buffer, 0, buffer.Length);
						fs.Write(buffer, 0, bytesRead);
					} while (bytesRead > 0);
				}	
			}
            Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "File distributor exiting");

			return result;
		}
		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("E525CA4C-770E-48b7-B5D3-D32BEBE00B84"); }
		}
	}
}
