using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Messaging;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Distributors.MSMQ
{
	/// <summary>
	/// Summary description for Class1. 
	/// </summary>
	public class Distributor : DistributorBase, IDistributor
	{
		// The folder into which the document should be distributed
		private TextQuestionQueryValue queuePath = new TextQuestionQueryValue();

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayName("Queue Path")]
        [Description ("The pathname of the message queue available to the Perfectus server where the documents will be placed.")]
		public TextQuestionQueryValue QueuePath
		{
			get { return queuePath; }
			set { queuePath = value; }
		}

		public DistributionResult[] Distribute()
		{
			DistributionResult[] result = new DistributionResult[readers.Count];

			Log.Trace(Log.Category.Distributors, Log.Priority.HIGH, "# MSMQ Distribute() called.");

			if (readers.Count == 0)
			{
                Log.Trace( Log.Category.Distributors, Log.Priority.MID, " # Nothing to do. (No readers were appeneded to this distributor.)" );
				return result;
			}

			// Get the queue onto which the file(s) should be placed.
			string queueToWrite = null;
			try
			{
				queueToWrite = queuePath.Evaluate(AnswerProvider.Caller.System, InstanceMetaData);
			}
			catch
			{
			} // Leave queueToWrite null.
			if (queueToWrite == null)
			{
				throw new ArgumentNullException("QueuePath", "QueuePath evaluated to null.");
			}

			// Submit each stream to the queue.
			MessageQueue queue = new MessageQueue(queueToWrite);		
			
			for (int i=0; i < readers.Count; i++)
			{
				Message msg = new Message();
				
				
				// Copy from the inputstream to the body's stream.
				byte[] buffer = new byte[1024];
				int bytesRead = 0;
				StreamReader sr = (StreamReader)readers[i];
				// Rewind, in case it's not pointing at pos 0.
				sr.BaseStream.Seek(0, SeekOrigin.Begin);
				while((bytesRead = sr.BaseStream.Read(buffer, 0, buffer.Length)) > 0)
				{
					msg.BodyStream.Write(buffer, 0, bytesRead);
				}

				// Submit
				string templateName = templateNames[i] == null ? string.Empty : templateNames[i].ToString();
				string fileName = fileNames[i] == null ? string.Empty : fileNames[i].ToString();
				string label = string.Format("Perfectus|{0}|{1}", instanceId, fileName);
				msg.Label = label;
				msg.Formatter = queue.Formatter;
				queue.Send(msg);
                Log.Trace( Log.Category.Distributors, Log.Priority.HIGH, String.Format( "# Sent file #{0} to queue: {2} with label: {1}", i, label, queueToWrite ) );
				
				result[i] = new DistributionResult(UniqueIdentifier, null, null, (Guid)templateIds[i], templateName, queueToWrite); 

				// Rewind our input streams
				sr.BaseStream.Seek(0, SeekOrigin.Begin);
			}
            Log.Trace( Log.Category.Distributors, Log.Priority.HIGH, "# MSMQ exited" );

			return result;
		}
		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("FA1A0340-DDE3-465B-81C3-850703EB9291"); }
		}
	}
}
