using System;
using System.IO;
using System.Xml.Serialization;

namespace Perfectus.Server.Plugins.Converters.Pdf.Word
{
	public class Config
	{
		public Config()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private string printerName;
		private string ghostScriptFolder;
		private string ghostScriptParams;
		private string encrypt995Folder;
		private string encrypt995ExecutableName;
		private string encryptLevel;

		[XmlElement]
		public string PrinterName
		{
			get { return printerName; }
			set { printerName = value; }
		}

		[XmlElement]
		public string GhostScriptFolder
		{
			get { return ghostScriptFolder; }
			set { ghostScriptFolder = value; }
		}

		[XmlElement]
		public string GhostScriptParams
		{
			get { return ghostScriptParams; }
			set { ghostScriptParams = value; }
		}

		[XmlElement]
		public string Encrypt995Folder
		{
			get { return encrypt995Folder; }
			set { encrypt995Folder = value; }
		}

		[XmlElement]
		public string Encrypt995ExecutableName
		{
			get { return encrypt995ExecutableName; }
			set { encrypt995ExecutableName = value; }
		}

		[XmlElement]
		public string EncryptLevel
		{
			get { return encryptLevel; }
			set { encryptLevel = value; }
		}

		public static Config Load()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamReader sr = new StreamReader(configPath))
			{
				return (Config)(serialiser.Deserialize(sr));
			}
		}

		public void Save()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamWriter sw = new StreamWriter(configPath, false))
			{
				serialiser.Serialize(sw, this);
			}			
		}
	}
}
