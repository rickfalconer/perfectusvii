using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using System.Runtime.InteropServices;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Converters.Pdf.Word
{


	/// <summary>
	/// 
	/// </summary>
	public class Converter : IConverter
	{
        [DllImport("Kernel32.dll")]
        private static extern uint GetShortPathName(string lpszLongPath, [Out] StringBuilder lpszShortPath, uint cchBuffer);


		private const string printerName = "PDF995";
	    private StringBindingMetaData instanceMetaData;

		public Converter()
		{
		}

		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("A1E8B11B-A41C-4bf1-BDF6-2B1E34758B13"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

		private YesNoQueryValue encryptionEnabled = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue EncryptionEnabled
		{
			get { return encryptionEnabled; }
			set { encryptionEnabled = value; }
		}
        
		private TextQuestionQueryValue userPassword = null;

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue UserPassword
		{
			get { return userPassword; }
			set { userPassword = value; }
		}

		private TextQuestionQueryValue ownerPassword = null;
		
		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue OwnerPassword
		{
			get { return ownerPassword; }
			set { ownerPassword = value; }
		}
		private YesNoQueryValue allowPrinting = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowPrinting
		{
			get { return allowPrinting; }
			set { allowPrinting = value; }
		}

		private YesNoQueryValue allowModifyContents = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowModifyContents
		{
			get { return allowModifyContents; }
			set { allowModifyContents = value; }
		}

		private YesNoQueryValue allowCopy = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowCopy
		{
			get { return allowCopy; }
			set { allowCopy = value; }
		}

		private YesNoQueryValue allowModifyAnnotations = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowModifyAnnotations
		{
			get { return allowModifyAnnotations; }
			set { allowModifyAnnotations = value; }
		}

		private YesNoQueryValue allowFillIn	 = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowFillIn
		{
			get { return allowFillIn; }
			set { allowFillIn = value; }
		}

		private YesNoQueryValue allowScreenReaders = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowScreenReaders
		{
			get { return allowScreenReaders; }
			set { allowScreenReaders = value; }
		}

		private YesNoQueryValue allowAssembly = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowAssembly
		{
			get { return allowAssembly; }
			set { allowAssembly = value; }
		}

		private YesNoQueryValue allowDegradedPrinting = null;

		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue AllowDegradedPrinting
		{
			get { return allowDegradedPrinting; }
			set { allowDegradedPrinting = value; }
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return instanceMetaData; }
            set { instanceMetaData = value; }
        }

        public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
        {
            //pf-3266 remove word automation office12 replace with error in log
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Legacy Word Pdf converter not supported");
            convertedFileName = string.Empty;
            return null;

            //         Log.Trace(Log.Category.Converters, Log.Priority.HIGH, " # Word PDF converter started");

            //         Config cfg = Config.Load();
            //string printerName = cfg.PrinterName;

            //convertedFileName = string.Format("{0}.pdf", fileNameWithoutExtension);
            //source.BaseStream.Seek(0, SeekOrigin.Begin);

            //         // This is the file name we will use when creating temporary files between each stage.
            //         string tempFileName = Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) );
            //         string wordMlPath = tempFileName + ".xml";
            //         string tempOutPSPath = tempFileName + ".ps";
            //         string tempOutPdfPath = tempOutPSPath + ".pdf";
            //         string encryptedPath = tempFileName + ".pdf";

            //         try
            //         {
            //             // **** Step 1: Create the WordML document.  ****
            //             byte[ ] writeBuffer = new byte[ 1024 ];
            //             using( FileStream fs = new FileStream( wordMlPath, FileMode.Create ) )
            //             {
            //                 int bytesRead = 0;
            //                 do
            //                 {
            //                     bytesRead = source.BaseStream.Read( writeBuffer, 0, writeBuffer.Length );
            //                     fs.Write( writeBuffer, 0, bytesRead );
            //                 } while( bytesRead > 0 );
            //             }

            //             // **** Step 2: Create the Post Script file.  *****
            //             //db4
            //             //ApplicationClass app = new ApplicationClass( );
            //             Application app = new Application();
            //             object oWordMlPath = wordMlPath;

            //             object oMissing = Missing.Value;

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "Word opens '{0}'", oWordMlPath.ToString());

            //             Microsoft.Office.Interop.Word.Document doc = app.Documents.Open( ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );

            //             app.Selection.WholeStory( );
            //             app.Selection.Fields.Update( );

            //             foreach( TableOfContents toc in doc.TablesOfContents )
            //                 toc.Update( );

            //             app.ActivePrinter = printerName;
            //             doc.Activate( );

            //             object oTempOutPath = tempOutPSPath;
            //             object oFalse = false;

            //             // Print to the printer, specifying an output filename
            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "Printing to '{0}'", oTempOutPath.ToString());

            //             doc.PrintOut( ref oFalse, ref oMissing, ref oMissing, ref oTempOutPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );
            //             // FB1120 (testing - need feedback from customer)
            //             object doNotSaveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
            //             doc.Save( );
            //             // <- FB1120
            //             doc.Close( ref doNotSaveChanges, ref oMissing, ref oMissing );
            //             app.Quit( ref oFalse, ref oMissing, ref oMissing );

            //             // **** Step 3: Create the PDF file. ****
            //	// PDF995 converts happily to .PS, but gets mucky with INI files to turn that into PDF.
            //	// So, lets do that here instead.
            //	Process p = new Process();
            //	ProcessStartInfo startInfo = new ProcessStartInfo();
            //	startInfo.FileName = "gswin32c.exe";

            //	startInfo.WorkingDirectory = cfg.GhostScriptFolder;
            //	startInfo.Arguments = string.Format(cfg.GhostScriptParams, tempOutPSPath, tempOutPdfPath);
            //	startInfo.CreateNoWindow = true;
            //	p.StartInfo = startInfo;

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Converting '{0}' with '{1}' in '{2}'", 
            //                 startInfo.FileName,startInfo.Arguments,startInfo.WorkingDirectory));

            //	p.Start();
            //	p.WaitForExit();

            //             // **** Step 4: Encrypt the file ****
            //	// Optionally Add encryption using Encrypt995.
            //	bool encryptPdf = false;
            //	string finalPath = tempOutPdfPath;

            //	if(encryptionEnabled != null)
            //	{
            //		encryptPdf = encryptionEnabled.Evaluate(false, AnswerProvider.Caller.Assembly);	
            //	}			
            //	if(encryptPdf)
            //	{
            //	    Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "encrypting...");
            //             	EncryptDocument(tempOutPdfPath, encryptedPath, cfg.Encrypt995Folder, cfg.Encrypt995ExecutableName,cfg.EncryptLevel);
            //		finalPath = encryptedPath;
            //	}

            //             // **** Step 5: Finally pass the final file into the memory stream which we return.
            //	MemoryStream ms = new MemoryStream();

            //	byte[] buffer = new byte[1024];
            //	using (FileStream fs = File.OpenRead(finalPath))
            //	{
            //		int bytesRead = 0;
            //		do
            //		{
            //			bytesRead = fs.Read(buffer, 0, buffer.Length);
            //			ms.Write(buffer, 0, bytesRead);
            //		} while (bytesRead > 0);
            //	}

            //	ms.Seek(0, SeekOrigin.Begin);

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, " # Word PDF converter done");

            //	return ms;
            //         }
            //         catch( Exception ex )
            //         {
            //	throw ;
            //         }
            //         finally
            //         {
            //             try
            //             {
            //                 // Delete all files that might or might not have been created. Not necessary to check as exception
            //                 // is not thrown if file does not exist.
            //                 File.Delete( wordMlPath );
            //                 File.Delete( tempOutPSPath );
            //                 File.Delete( tempOutPdfPath );
            //                 File.Delete( encryptedPath );
            //             }
            //             catch
            //             {
            //                 Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "Word PDF converter could not delete the temporary files" );
            //             }
            //         }
        }

        // Return short path format of a file name
        private static string ToShortPathName(string longName)
        {
            StringBuilder s = new StringBuilder(1000);
            uint iSize = (uint)s.Capacity;
            uint iRet = GetShortPathName(longName, s, iSize);
            return s.ToString();
        }

		private void EncryptDocument(string inPath, string outPath, string appWorkingDirectory, string executableName, string encryptLevel)
		{
			Process p = new Process();
			ProcessStartInfo startInfo = new ProcessStartInfo();
			startInfo.FileName = executableName;
			startInfo.WorkingDirectory = appWorkingDirectory;
			AnswerProvider.Caller caller = AnswerProvider.Caller.Assembly;
			string sUserPassword = "";
			string sOwnerPassword = "";

			if(userPassword != null)
			{
				sUserPassword = userPassword.Evaluate(caller, instanceMetaData);
			}
			if(ownerPassword != null)
			{
				sOwnerPassword = ownerPassword.Evaluate(caller, instanceMetaData);
			}

			if(sUserPassword == null || sUserPassword.Trim().Length == 0)
			{
				sUserPassword = "\"\"";
			}

			if(sOwnerPassword == null || sOwnerPassword.Trim().Length == 0)
			{
				sOwnerPassword = "\"\"";
			}

            FileInfo _File = new FileInfo(inPath);
            String fi = ToShortPathName(_File.DirectoryName) + "\\" + _File.Name;

            _File = new FileInfo(outPath);
            String fo = ToShortPathName(_File.DirectoryName) + "\\"+_File.Name;

            //startInfo.Arguments = ToShortPathName(inPath) + " " + ToShortPathName(outPath) + " " + sUserPassword + " " + sOwnerPassword + " " + GetEncryptionPermissions(caller) + " " + encryptLevel;
            startInfo.Arguments = fi + " " + fo + " " + sUserPassword + " " + sOwnerPassword + " " + GetEncryptionPermissions(caller) + " " + encryptLevel;
            startInfo.CreateNoWindow = true;
			p.StartInfo = startInfo;
			p.Start();
			p.WaitForExit();
		}

		private string GetEncryptionPermissions(AnswerProvider.Caller caller)
		{
            return GetNumberStringFromYNQ(allowPrinting,caller) + GetNumberStringFromYNQ(allowModifyContents, caller) +
			GetNumberStringFromYNQ(allowCopy, caller) + GetNumberStringFromYNQ(allowModifyAnnotations, caller) + 
			GetNumberStringFromYNQ(allowFillIn, caller) + GetNumberStringFromYNQ(allowScreenReaders, caller) + 
			GetNumberStringFromYNQ(allowAssembly, caller) +  GetNumberStringFromYNQ(allowDegradedPrinting, caller);
		}

		private string GetNumberStringFromYNQ(YesNoQueryValue valueToConvert, AnswerProvider.Caller caller)
		{
			if(valueToConvert == null)
			{
				return "1";
			}

			bool boolVal = valueToConvert.Evaluate(true,caller);
			if(boolVal)
			{
				return "1";
			}
			else
			{
				return "0";
			}
		}
	}
}
