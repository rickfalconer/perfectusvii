using System.Reflection;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server PDF Converter")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs