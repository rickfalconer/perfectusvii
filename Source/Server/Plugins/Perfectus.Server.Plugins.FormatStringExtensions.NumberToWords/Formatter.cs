using Perfectus.Server.PluginKit;
using System;

namespace Perfectus.Server.Plugins.FormatStringExtensions.NumberToWords
{
    /// <summary>
    /// Summary description for Formatter.
    /// </summary>
    public class Formatter : IStringFormatter
    {
        /* 
	
        CONFIGURATION SAMPLES FOR GETTING THIS PLUGIN TO WORK:
			
        Sample Server shared configuration entry:
				
        <plugin type="formatStringExtension" assemblyName="Perfectus.Server.Plugins.FormatStringExtensions.NumberToWords.dll"
        typeName="Perfectus.Server.Plugins.FormatStringExtensions.FormatNumberSample.Formatter" key="NumberToWords">
        <name>Number To Words</name>
        </plugin>
				
			
        Sample IPManager configuration entry:
			
        <formatStringExtensions>	
        <formatString forDataType="NumberFormats" serverSidePluginKey="NumberToWords">
        <name>Number To Words</name>	
        </formatString>		
        </formatStringExtensions>	
				
        Note - 	The serverSidePluginKey property has the value of the key specified in the server configuration file. 
        Available "forDataType" formats are: TextFormats, NumberFormats and DateFormats			
			
        */


        private string dollar = "";
        private string cent = "";
        private string fontStyle = "";
        private int decimalPlaces = 0;
        private bool needCurrencyStyle = false;
        private bool needRoundDecimalPlaces = false;
        private bool isNegative = false;

        const string DOLLAR_US = "Dollar";
        const string CENT_US = "Cent";

        const string DOLLAR_UK = "Pound";
        const string CENT_UK = "Pence";

        const string CURRENCY_US = "us";
        const string CURRENCY_UK = "uk";

        const string UPPERCASE = "u";
        const string TITLECASE = "t";

        const string DECIMALPLACE = "dp";

        const string HUNDRED = "Hundred";
        const string THOUSAND = "Thousand";
        const string MILLION = "Million";
        const string BILLION = "Billion";

        const string ZERO_KEY = "Zero";
        const string SPACE_KEY = " ";
        const string HYPHEN_KEY = "-";
        const string POINT_KEY = "Point";
        const string PLURAL_KEY = "s";
        const string AND_KEY = "And";
        const string COMMA_KEY = ",";

        /// <summary>
        /// Convert Numbers to Words
        /// </summary>
        /// <param name="inputString">The string to format</param>
        /// <param name="inputParams">Parameters</param>
        /// <returns>Formatted Words</returns>
        public string FormatString(string input, string parameter)
        {
            string formattedNumber = "";
            if (input != null && input.Length > 0)
            {
                formattedNumber = input;

                CheckIfNegative(input);

                CheckParameter(parameter.ToLower());

                if (needCurrencyStyle)
                {
                    formattedNumber = ConvertToMoney(input);
                }
                else
                {
                    formattedNumber = ConvertToWords(input);
                }

                // Added "minus" to negative numbers
                if (isNegative && formattedNumber != input && formattedNumber.Length > 0)
                {
                    formattedNumber = "minus" + SPACE_KEY + formattedNumber;
                }

                formattedNumber = ApplyFontStyle(formattedNumber, fontStyle);

            }
            return formattedNumber;
        }

        /// <summary>
        /// Check if input number is negatvie
        /// </summary>
        /// <param name="input"></param>
        private void CheckIfNegative(string input)
        {
            if (input.StartsWith("-"))
            {
                isNegative = true;
            }
        }

        /// <summary>
        /// Check paramter setting
        /// </summary>
        /// <param name="parameter"></param>
        private void CheckParameter(string parameter)
        {
            if (parameter != null & parameter.IndexOf("-") > 0)
            {
                string[] parameters = parameter.Split('-');
                for (int i = 0; i < parameters.Length; i++)
                {
                    CheckCurrencyStyle(parameters[i]);
                    CheckFontStyle(parameters[i]);
                    CheckRoundDecimalPlaces(parameters[i]);
                }
            }
            else
            {
                CheckCurrencyStyle(parameter);
                CheckFontStyle(parameter);
                CheckRoundDecimalPlaces(parameter);
            }
        }

        /// <summary>
        /// Check if the parameter set to require money format
        /// </summary>
        /// <param name="inputParameter"></param>
        private void CheckCurrencyStyle(string parameter)
        {
            if (parameter != null && parameter.IndexOf(CURRENCY_US) >= 0)
            {
                dollar = DOLLAR_US;
                cent = CENT_US;
                needCurrencyStyle = true;
            }
            if (parameter != null && parameter.IndexOf(CURRENCY_UK) >= 0)
            {
                dollar = DOLLAR_UK;
                cent = CENT_UK;
                needCurrencyStyle = true;
            }
        }

        /// <summary>
        /// Check if the parameter set to require font style
        /// </summary>
        /// <param name="inputParameter"></param>
        private void CheckFontStyle(string parameter)
        {
            if (parameter != null && parameter.IndexOf(UPPERCASE) >= 0 && parameter.Length == 1)
            {
                fontStyle = UPPERCASE;
            }
            if (parameter != null && parameter.IndexOf(TITLECASE) >= 0)
            {
                fontStyle = TITLECASE;
            }
        }

        /// <summary>
        /// Check if the parameter set to require round to decimal places
        /// </summary>
        /// <param name="inputParameter"></param>
        private void CheckRoundDecimalPlaces(string parameter)
        {
            if (parameter != null && parameter.IndexOf(DECIMALPLACE) >= 0)
            {
                parameter = parameter.Trim();
                decimalPlaces = Convert.ToInt32(parameter.Substring(0, parameter.Length - DECIMALPLACE.Length));
                needRoundDecimalPlaces = true;
            }
        }

        /// <summary>
        /// Convert numbers to money words
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string ConvertToMoney(string input)
        {
            // Trim & Format the number to 2dp number by using "Two decimal places" format string
            input = Convert.ToDecimal(input).ToString("F2");

            string formattedWords = "";
            int dotIndex = input.IndexOf(".");

            string integerPart = input;
            string decimalPart = "0";


            if (dotIndex > 0)
            {
                integerPart = input.Substring(0, dotIndex);
                decimalPart = input.Substring(dotIndex + 1);
            }

            formattedWords += FormatIntegers(integerPart);
            formattedWords += SPACE_KEY + AppendedUnitWords(Convert.ToInt64(integerPart), dollar) + SPACE_KEY + AND_KEY + SPACE_KEY;


            if (Convert.ToInt32(decimalPart) > 0)
            {
                // Has cents part
                string formattedDecimal = "";
                formattedDecimal += FormatDecimals(decimalPart);
                formattedDecimal += SPACE_KEY + AppendedUnitWords(Convert.ToInt64(decimalPart), cent);

                if (Convert.ToInt64(integerPart) == 0)
                {
                    // Only has cents part
                    formattedWords = formattedDecimal;
                }
                else
                {
                    formattedWords += formattedDecimal;
                }
            }

            return TrimFromattedNumber(formattedWords, AND_KEY + SPACE_KEY);
        }

        /// <summary>
        /// Convert numbers to word
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string ConvertToWords(string input)
        {
            // Trim & Format the number to required decimal places 
            if (needRoundDecimalPlaces)
            {
                string format = "";
                if (decimalPlaces == 0)
                {
                    // If parameter set as 0dp then use no decimal places format
                    format = "F0";
                }
                else
                {
                    format = ".";
                    for (int i = 0; i < decimalPlaces; i++)
                    {
                        format += "0";
                    }
                }
                input = Convert.ToDecimal(input).ToString(format);
            }

            string formattedWords = "";
            bool hasDecimal = false;
            int dotIndex = input.IndexOf(".");

            string integerPart = input;
            string decimalPart = "0";

            if (dotIndex >= 0)
            {
                // For numbers like 0.1/-0.1 will be formatted to .1/-.1 so need to add the 0 back
                if (dotIndex == 0 || (isNegative && dotIndex == 1))
                {
                    integerPart = "0";
                }
                else
                {
                    integerPart = input.Substring(0, dotIndex);
                }
                decimalPart = input.Substring(dotIndex + 1);
                hasDecimal = true;
            }

            formattedWords += FormatIntegers(integerPart);
            if (hasDecimal || (needRoundDecimalPlaces && decimalPlaces > 0))
            {
                formattedWords += SPACE_KEY + POINT_KEY + SPACE_KEY + FormatDecimals(decimalPart);
            }

            return formattedWords.Trim();
        }

        /// <summary>
        /// Format integer part of the input number
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        private string FormatIntegers(string input)
        {
            //Trim the number with space and negative sign
            long inputNumber = Convert.ToInt64(input);
            if (inputNumber < 0)
            {
                inputNumber = inputNumber * -1;
            }
            input = Convert.ToString(inputNumber);

            int digitsNum = input.Length;

            //Catch 0 
            if (IsZero(input))
            {
                return ZERO_KEY;
            }

            if (digitsNum <= 3)
            {
                return FormatThreeOrLessDigits(input);
            }
            else
            {
                switch (digitsNum)
                {
                    case 4:
                    case 5:
                    case 6:
                        return FormatThousand(input);
                    case 7:
                    case 8:
                    case 9:
                        return FormatMillion(input);
                    case 10:
                    case 11:
                    case 12:
                        return FormatBillion(input);
                    default:
                        return input;
                }
            }
        }

        /// <summary>
        /// Format the decimal part of the input number
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string FormatDecimals(string input)
        {
            string formattedDecimals = "";

            if (needCurrencyStyle)
            {
                formattedDecimals = FormatTwoDigits(input);
            }
            else
            {
                for (int i = 0; i < input.Length; i++)
                {
                    string digit = input.Substring(i, 1);
                    if (IsZero(digit))
                    {
                        formattedDecimals += ZERO_KEY;
                    }
                    else
                    {
                        formattedDecimals += FormatSingleDigit(digit);
                    }
                    formattedDecimals += SPACE_KEY;
                }
            }
            return formattedDecimals.Trim();
        }

        /// <summary>
        /// Check if the number is equal to 0
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private bool IsZero(string input)
        {
            if (input.Length == 1 && input == "0")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Format numbers from 0 - 9
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        private string FormatSingleDigit(string input)
        {
            switch (Convert.ToInt32(input))
            {
                case 1:
                    return "One";
                case 2:
                    return "Two";
                case 3:
                    return "Three";
                case 4:
                    return "Four";
                case 5:
                    return "Five";
                case 6:
                    return "Six";
                case 7:
                    return "Seven";
                case 8:
                    return "Eight";
                case 9:
                    return "Nine";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Format number from 10- 19 
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private string FormatTwoDigitsLessThanTwenty(int input)
        {
            switch (input)
            {
                case 10:
                    return "Ten";
                case 11:
                    return "Eleven";
                case 12:
                    return "Twelve";
                case 13:
                    return "Thirteen";
                case 14:
                    return "Fourteen";
                case 15:
                    return "Fifteen";
                case 16:
                    return "Sixteen";
                case 17:
                    return "Seventeen";
                case 18:
                    return "Eighteen";
                case 19:
                    return "Nineteen";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Format number which are 20,30,40...90
        /// </summary>
        /// <param name="number">first digital number</param>
        /// <returns></returns>
        private string FormatTwoDigitsEndWithZero(int input)
        {
            switch (input)
            {
                case 2:
                    return "Twenty";
                case 3:
                    return "Thirty";
                case 4:
                    return "Forty";
                case 5:
                    return "Fifty";
                case 6:
                    return "Sixty";
                case 7:
                    return "Seventy";
                case 8:
                    return "Eighty";
                case 9:
                    return "Ninety";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Format numbers from 00 - 99
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string FormatTwoDigits(string input)
        {
            // Trim input number make sure NOT start with 0
            // - 00 -> 0
            // - 01 -> 1
            // ...
            int inputNumber = Convert.ToInt32(input);
            input = Convert.ToString(inputNumber);
            string formattedDigits = "";

            if (inputNumber < 10)
            {
                // Handle numbers from 0 - 9
                formattedDigits = FormatSingleDigit(input);
            }
            else if (inputNumber >= 10 && inputNumber < 20)
            {
                // Handle numbers from 10 - 19
                formattedDigits = FormatTwoDigitsLessThanTwenty(inputNumber);
            }
            else
            {
                // Handle numbers from 20 - 99
                int firstDigit = inputNumber / 10;
                int secondDigit = inputNumber - firstDigit * 10;

                formattedDigits += FormatTwoDigitsEndWithZero(firstDigit);

                if (secondDigit > 0)
                {
                    formattedDigits += HYPHEN_KEY + FormatSingleDigit(Convert.ToString(secondDigit));
                }
            }

            return formattedDigits.Trim();
        }


        /// <summary>
        /// Select format method base on length of input
        /// 1 - 3 digits
        /// 000 - 999
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string FormatThreeOrLessDigits(string input)
        {
            // Trim input number make sure NOT start with 0
            // - 000 -> 0
            // - 011 -> 11
            // ...
            input = Convert.ToString(Convert.ToInt32(input));

            switch (input.Length)
            {
                case 1:
                    return FormatSingleDigit(input);
                case 2:
                    return FormatTwoDigits(input);
                case 3:
                    return FormatHundred(input);
                default:
                    return "";
            }
        }

        /// <summary>
        /// 100 - 999
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string FormatHundred(string input)
        {
            string formattedHundred = "";

            string hundredDigit = input.Substring(0, 1);
            string restDigits = input.Substring(1);

            formattedHundred += FormatSingleDigit(hundredDigit);
            if (formattedHundred.Length > 0)
            {
                formattedHundred += SPACE_KEY + HUNDRED + SPACE_KEY + AND_KEY + SPACE_KEY;
            }
            formattedHundred += FormatTwoDigits(restDigits);

            return TrimFromattedNumber(formattedHundred, AND_KEY + SPACE_KEY);
        }

        /// <summary>
        /// Format 4 - 6 digits number
        /// 0000 - 999999
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string FormatThousand(string input)
        {
            string formattedThousand = "";
            string thousandDigits = input.Substring(0, input.Length - 3);
            string restDigits = input.Substring(input.Length - 3);

            formattedThousand += FormatThreeOrLessDigits(thousandDigits);
            if (formattedThousand.Length > 0)
            {
                formattedThousand += SPACE_KEY + THOUSAND;

                int restNumber = int.Parse(restDigits);

                if (restNumber > 0 && restNumber < 100)
                {
                    formattedThousand += SPACE_KEY + AND_KEY + SPACE_KEY;
                }
                else if (restNumber >= 100)
                {
                    formattedThousand += COMMA_KEY + SPACE_KEY;
                }
            } 
            formattedThousand += FormatThreeOrLessDigits(restDigits);

            return TrimFromattedNumber(formattedThousand, COMMA_KEY + SPACE_KEY);
        }

        /// <summary>
        /// Format 7 - 9 digits number
        /// 0000000 - 999999999
        /// </summary>
        /// <returns></returns>
        private string FormatMillion(string input)
        {
            string formattedMillion = "";
            string millionDigits = input.Substring(0, input.Length - 6);
            string restDigits = input.Substring(input.Length - 6);

            int restNumber = Int32.Parse(restDigits);

            formattedMillion += FormatThreeOrLessDigits(millionDigits);
            if (formattedMillion.Length > 0)
            {
                formattedMillion += SPACE_KEY + MILLION;

                if (restNumber >= 100)
                    formattedMillion += COMMA_KEY + SPACE_KEY;
                else if (restNumber < 100 && restNumber > 0)
                    formattedMillion += SPACE_KEY + AND_KEY + SPACE_KEY;
            }
            formattedMillion += FormatThousand(restDigits);

            return TrimFromattedNumber(formattedMillion, COMMA_KEY + SPACE_KEY);
        }

        /// <summary>
        /// Format 10 - 12 digits number
        /// 0000000000 - 999999999999
        /// </summary>
        /// <returns></returns>
        private string FormatBillion(string input)
        {
            string formattedBillion = "";
            string billionDigits = input.Substring(0, input.Length - 9);
            string restDigits = input.Substring(input.Length - 9);

            formattedBillion += FormatThreeOrLessDigits(billionDigits);
            if (formattedBillion.Length > 0)
            {
                formattedBillion += SPACE_KEY + BILLION + COMMA_KEY + SPACE_KEY;
            }
            formattedBillion += FormatMillion(restDigits);

            return TrimFromattedNumber(formattedBillion, COMMA_KEY + SPACE_KEY);
        }

        /// <summary>
        /// Check if need to add plural for the unit word
        /// </summary>
        private string AppendedUnitWords(long amount, string unit)
        {
            if (unit == dollar)
            {
                // if its 1 or -1 just append with dollar or pound
                if (amount == 1 || amount == -1)
                {
                    return dollar;
                }
                else
                {
                    return dollar + PLURAL_KEY;
                }
            }
            if (unit == cent)
            {
                if (amount == 1 || cent == CENT_UK)
                {
                    return cent;
                }
                else
                {
                    return cent + PLURAL_KEY;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Trim end of the formatted word 
        /// - and 
        /// - , 
        /// </summary>
        /// <param name="formattedWords"></param>
        /// <param name="trimKey"></param>
        /// <returns></returns>
        private string TrimFromattedNumber(string formattedWords, string trimKey)
        {
            if (formattedWords.EndsWith(trimKey))
            {
                formattedWords = formattedWords.Remove(formattedWords.Length - trimKey.Length, trimKey.Length);
            }

            return formattedWords.Trim();
        }

        /// <summary>
        /// Apply font style to the formatted words
        /// </summary>
        /// <param name="input"></param>
        /// <param name="style"></param>
        /// <returns></returns>
        private string ApplyFontStyle(string input, string style)
        {
            switch (style)
            {
                case UPPERCASE:
                    return input.ToUpper();
                case TITLECASE:
                    return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input);
                default:
                    return input.ToLower();
            }

        }
    }
}
