using System;
using System.Diagnostics;
using System.IO;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.Distributors.OpenOnServer
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Distributor : DistributorBase, IDistributor
	{
		public Guid UniqueIdentifier
		{
			get 
			{
				return new Guid("{823D1DC0-4235-45ee-8E69-98D2D202CB4B}");
			}

		}

		public Distributor() 
		{
		}

		public DistributionResult[] Distribute()
		{
			DistributionResult[] result = new DistributionResult[readers.Count];
			Trace.WriteLine("# OpenOnServer Distribute() called.");

			if (readers.Count == 0)
			{
				Trace.WriteLine(" # Nothing to do. (No readers were appeneded to this distributor.)");
				return null;
			}

			string tempFileName = Path.GetTempFileName();
			File.Delete(tempFileName);
			DirectoryInfo di = Directory.CreateDirectory(tempFileName);
			string directory = di.FullName;


			for (int i = 0; i < readers.Count; i++)
			{
				// Write each stream to disc.
				string fileName = (string) fileNames[i];
				result[i] = new DistributionResult(this.UniqueIdentifier, null, fileName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), null); 
				string fullPath = string.Format("{0}{1}{2}", directory, Path.DirectorySeparatorChar, fileName);

				StreamReader sr = (StreamReader) readers[i];

				sr.BaseStream.Seek(0, SeekOrigin.Begin);

				byte[] buffer = new byte[1024];
				using (FileStream fs = new FileStream(fullPath, FileMode.Create))
				{
					int bytesRead = 0;
					do
					{
						bytesRead = sr.BaseStream.Read(buffer, 0, buffer.Length);
						fs.Write(buffer, 0, bytesRead);
					} while (bytesRead > 0);
				}

				LaunchFile(fullPath);

			}

			return result;

		}

		private void LaunchFile(string filePath)
		{
			if (filePath == null)
			{
				throw new ArgumentNullException("filePath", "Path cannot be null");
			}

			ProcessStartInfo startInfo = new ProcessStartInfo(filePath);
			startInfo.WorkingDirectory = Path.GetDirectoryName(filePath);
			startInfo.UseShellExecute = true;
			Process.Start(startInfo);
		}

	}
}
