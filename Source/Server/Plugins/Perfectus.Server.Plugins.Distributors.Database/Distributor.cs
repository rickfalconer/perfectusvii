using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using Perfectus.Server.PluginBase;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.Distributors.Database
{
	public class Distributor : DistributorBase, IDistributor
	{
		private string connectionString;
		private Config conf;


		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("7C964D10-74D9-484b-AF96-1600FA7E1EB7"); }
		}

		public Distributor()
		{
			conf = Config.Load();
		}

		public DistributionResult[] Distribute()
		{
			Trace.WriteLine("# Perfectus My Documents Distribute() called.");
			DistributionResult[] result = new DistributionResult[readers.Count];
			connectionString = conf.ConnectionString;

			if (readers.Count == 0)
			{
				Trace.WriteLine(" # Nothing to do. (No readers were appeneded to this distributor.)");
				return null;
			}

			for (int i = 0; i < readers.Count; i++)
			{
				// Write each stream to disc.
				string fileName = (string) fileNames[i];

				StreamReader sr = (StreamReader) readers[i];

				sr.BaseStream.Seek(0, SeekOrigin.Begin);

				byte[] bytes = new byte[sr.BaseStream.Length];
				sr.BaseStream.Read(bytes, 0, (int) sr.BaseStream.Length);

				using (SqlConnection conn = new SqlConnection(connectionString))
				{
					conn.Open();
					SqlCommand cmd = new SqlCommand("MyDocumentsPlugin_InsertDocument", conn);
					cmd.CommandType = CommandType.StoredProcedure;
					SqlParameter param = cmd.Parameters.Add("RETURN_VALUE", SqlDbType.Int);
					param.Direction = ParameterDirection.ReturnValue;
					cmd.Parameters.Add("@InstanceId", SqlDbType.UniqueIdentifier).Value = base.instanceId;
					cmd.Parameters.Add("@FileName", SqlDbType.NVarChar, 255).Value = fileName;
					cmd.Parameters.Add("@DocumentBytes", SqlDbType.Image).Value = bytes;
					
					cmd.ExecuteNonQuery();
					int docID = (int)cmd.Parameters["RETURN_VALUE"].Value;
					string dLink = "getdocument.aspx?docID=" + docID;
					result[i] = new DistributionResult(this.UniqueIdentifier, dLink, fileName, (Guid)templateIds[i], templateNames[i] == null ? string.Empty : templateNames[i].ToString(), null); 
				}
			}

				
			return result;
		}

	}
}
