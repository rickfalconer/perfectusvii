using System;
using System.ComponentModel;
using System.IO;
using System.Resources;
using System.Xml;
using System.Xml.Xsl;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.Converters.AnswerSummaryWordML
{
	/// <summary>
	///		This class is a sample Perfectus Converter that accepts the AnswerSetXML from an Interview and returns a simple summary WordML document.
	/// </summary>
	/// <remarks>
	///		<list type="table">
	///			<listheader>
	///				<term>
	///					The following steps need to be completed to create a new Perfectus Converter plug-in
	///				</term>
	///			</listheader>
	///			<item>
	///				<term>
	///					Referenced assemblies
	///				</term>
	///				<description>
	///					The project must reference the Perfectus.Server.PluginKit assembly (from the GAC).
	///				</description>
	///			</item>
	///			<item>
	///				<term>
	///					Base class
	///				</term>
	///				<description>
	///					The class must be derived from Perfectus.Server.PluginKit.ConverterBase.
	///				</description>
	///			</item>
	///			<item>
	///				<term>
	///					Class-level attributes
	///				</term>
	///				<description>
	///					The class must be decorated with the following attributes: <b>[ConverterSourceDocument(SourceDocument.AnswerSetXml)]</b> or <b>[ConverterSourceDocument(SourceDocument.WordML)]</b>; <b>[PluginFriendlyName("The name that will appear in the IPManager application")]</b> and <b>[Guid("{A unique identifier for this plugin}"]</b>.
	///				</description>
	///			</item>
	///			<item>
	///				<term>
	///					Interface
	///				</term>
	///				<description>
	///					The class must implement the abstract method <b>Stream ConvertMemoryStream(StreamReader, String, out String)</b>.
	///				</description>
	///			</item>
	///		</list>
	/// </remarks>
	/// <example>
	///		The following example can be found in the Samples/Converter project in the Perfectus SDK.
	///		<code>
	/// DOCTODO:
	///		</code>
	/// </example>
	public class Converter : IConverter
	{
	    private StringBindingMetaData instanceMetaData;

	    [Browsable(false)]
		public Guid UniqueIdentifier
		{
			get { return new Guid("6BFCCA31-21A2-4ef5-8AB9-8F7873626A7D"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.AnswerSetXml; }
		}

		/// <summary>
		///		This method converts incoming AnswerSetXML into a simple WordML summary document.
		/// </summary>
		/// <remarks>
		///		In this example, an XSLT transform is performed on the incoming AnswerSetXML, and the resulting WordML is returned.
		/// </remarks>
		/// <param name="source">A StreamReader containing the incoming data.  Depending on the ConverterSourceDocument attribute, this can be AnswerSetXml or the WordMl output of the assembly system.</param>
		/// <param name="fileNameWithoutExtension">The filename that the data should be given - without an extension.</param>
		/// <param name="convertedFileName">The name of the file that the converted data should be given.  E.g. 'answerSummary.xml'.</param>
		/// <returns>Returns a Stream (usually a MemoryStream) containing the converted data.</returns>
		public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
		{
			// Regardless of the input filename, specify that the output file is called 'InterviewSummary.xml'
            convertedFileName = GetReturnFilename(fileNameWithoutExtension);

			// Get the XSLT from our resource
			ResourceManager rm = new ResourceManager(typeof (Converter));

			string xslt = rm.GetString("xslt");

			// Set up the transform engine
			XslTransform transform = new XslTransform();
			XmlDocument transformDoc = new XmlDocument();
			transformDoc.LoadXml(xslt);
			transform.Load(transformDoc.CreateNavigator());
			transformDoc = null;

			// Load the input XML into a document.  Make sure the input stream is rewound prior to loading it into the doc.
			XmlDocument sourceXml = new XmlDocument();
			source.BaseStream.Seek(0, SeekOrigin.Begin);
			sourceXml.Load(source.BaseStream);

			// Set up a memory stream to contain the output of the transform
			MemoryStream ms = new MemoryStream();

			// Perform the transform
			transform.Transform(sourceXml.CreateNavigator(), null, ms);

			// Rewind and return the stream containing the WordML summary document.
			ms.Seek(0, SeekOrigin.Begin);
			return ms;
		}

        private string GetReturnFilename(string suggestedFilename)
        {
            string returnedFilename = suggestedFilename;

            if (returnedFilename == null || returnedFilename.Length == 0)
                returnedFilename = "AnswerSummary.xml";

            if (!returnedFilename.ToLower().EndsWith(".xml"))
                returnedFilename = string.Format("{0}.xml", returnedFilename);

            return returnedFilename;
        }

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
	    {
	        get { return instanceMetaData; }
	        set { instanceMetaData = value; }
	    }
	}
}