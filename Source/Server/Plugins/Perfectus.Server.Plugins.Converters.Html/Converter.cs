using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Drawing.Design;
using System.Threading;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using Perfectus.Server.PluginKit;
using Perfectus.Common.PackageObjects;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Converters.Html
{
	/// <summary>
	/// The converter process convrts WordML documents into HTML
	/// by using the HTMLConverter.xslt script.
	/// This script is the same as the one used in the preview process except is does
	/// not do the popup stuff (really!) and handles the images differently.
	/// The images are extracted from the xml, copied into a folder with the same name as 
	/// </summary>
	public class Converter : IConverter
	{
		const int SUCCESS_SUBMIT = 0;
		const int SUCCESS_CONVERT = 1;
		private Config cfg = null;
		private string _ImgPath = "";
		string _URLPath = "";
		string _CssPath = "";
		bool _UpdateFields = false;
//		private Microsoft.Office.Interop.Word.Application ObjWord = null;
		private bool wordObjectCreated = false;
        private StringBindingMetaData instanceMetaData;
		
		public Converter()
		{
		}
	
		[Browsable(false)]
		public Guid UniqueIdentifier
		{
			get 
			{
				return new Guid("90064ABA-46F7-4aba-8448-3175EA8B10FE"); }
		}

		[Browsable(false)]
		public SourceDocument DocumentTypeConverted
		{
			get { return SourceDocument.WordML; }
		}

		private TextQuestionQueryValue imgPath = null;
		
		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue ImagePath
		{
			get { return imgPath; }
			set { imgPath = value; }
		}
        
		private TextQuestionQueryValue urlPath = null;

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue URLPath
		{
			get { return urlPath; }
			set { urlPath = value; }
		}
		
		private TextQuestionQueryValue cssPath = null;

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
		public TextQuestionQueryValue StyleSheetPath
		{
			get { return cssPath; }
			set { cssPath = value; }
		}
		
		private YesNoQueryValue updateFields = null;

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
		public YesNoQueryValue UpdateWordFields
		{
			get { return updateFields; }
			set { updateFields = value; }
		}

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return instanceMetaData; }
            set { instanceMetaData = value; }
        }

        public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
        {
            //pf-3266 remove word automation office12 replace with error in log
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Legacy Word Html converter not supported");
            convertedFileName = string.Empty;
            return null;

            //         try
            //         {
            //	const string backslash = @"\";

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("# HTML Converter processing file '{0}'", fileNameWithoutExtension));
            //	convertedFileName = string.Format("{0}.html", fileNameWithoutExtension); 

            //	string wordmlFileName = string.Format("{0}.xml", fileNameWithoutExtension); 

            //	AnswerProvider.Caller caller = AnswerProvider.Caller.Assembly;

            //             if (imgPath != null && (imgPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || imgPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            //	{
            //		_ImgPath = imgPath.Evaluate(caller, instanceMetaData);
            //	}
            //	else
            //	{
            //                 throw new Exception("HTML Converter - No image Path was found");
            //	}

            //             if (_ImgPath != null && !_ImgPath.EndsWith(backslash))
            //             {
            //                 _ImgPath += backslash;
            //             }

            //             if (urlPath != null && (urlPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || urlPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            //	{
            //		_URLPath = urlPath.Evaluate(caller, instanceMetaData);
            //	}	
            //	else
            //	{
            //                 throw new Exception("HTML Converter - No URL was found");
            //	}

            //             if (_URLPath != null && !_URLPath.EndsWith("/"))
            //             {
            //                 _URLPath += "/";
            //             }

            //             if (cssPath != null && (cssPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question || cssPath.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text))
            //	{
            //                 _CssPath = cssPath.Evaluate(caller, instanceMetaData);
            //	}	
            //	else
            //	{
            //                 throw new Exception("HTML Converter - No css Path was found");
            //	}

            //	if (updateFields != null)
            //	{
            //		_UpdateFields = updateFields.YesNoValue; 
            //	}

            //	cfg = Config.Load();

            //	XmlDocument wordMLDoc = new XmlDocument();


            //	if (_UpdateFields)
            //	{
            //		wordMLDoc = UpdateWordDoc(source);

            //	}
            //	else
            //	{
            //		// Get source stream into a WordML file
            //		source.BaseStream.Seek(0, SeekOrigin.Begin);					
            //		wordMLDoc.Load(source);
            //	}

            //	// Save the converted html file to stream
            //	MemoryStream ms = ConvertToHtml(wordMLDoc, fileNameWithoutExtension);
            //	ms.Seek(0, SeekOrigin.Begin);

            //	if(wordObjectCreated)
            //	{
            //		// If we have used Word we now need to destory it.
            //		object saveChanges = false;
            //		object M=Type.Missing;
            //		ObjWord.Quit(ref saveChanges,ref M,ref M);
            //		wordObjectCreated = false;
            //	}

            //             Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "HTML Converter exiting process");
            //	return ms;
            //}
            //catch
            //{
            //	throw;

            //}
        }

		//private MemoryStream ConvertToHtml(XmlDocument wordMLDoc, string fileName)
		//{
						
		//	XslTransform HTMLTransform = new XslTransform();
		//	HTMLTransform.Load(cfg.HtmlConverterXsltPath);
		//	XsltArgumentList argList = new XsltArgumentList();
		//	argList.AddParam("docName", string.Empty, fileName);
		//	argList.AddParam("imagePath", string.Empty, _ImgPath);
		//	argList.AddParam("urlPath", string.Empty, _URLPath);
		//	argList.AddParam("cssPath", string.Empty, _CssPath);
		//	ConverterSupport converterSupport = new ConverterSupport();
		//	argList.AddExtensionObject("urn:ConverterSupport", converterSupport);

		//	MemoryStream ms = new MemoryStream();

  //          Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "about to transform");
		//	HTMLTransform.Transform(wordMLDoc, argList, ms, new XmlUrlResolver());
			
		//	return ms;
			
		//}
		
		/// <summary>
		/// Method takes the current stream and loads into Word which updates the fields
		/// </summary>
		/// <param name="source"></param>
		/// <returns></returns>
		//private XmlDocument UpdateWordDoc(StreamReader source)
		//{
		//	string folderPath = cfg.TempFolderPath;
		//	if (!folderPath.EndsWith(@"\"))
		//		folderPath += @"\";
			
		//	string fileName = folderPath + "TempWordFile.xml";			
			
		//	object M=Type.Missing; //M is missing...hi..hi
		//	object objWordFilePath= fileName; //Convert to Object..
		//	object saveChanges = true;
			
		//	DeleteTempFile(fileName);
		//	source.BaseStream.Seek(0, SeekOrigin.Begin);

		//	byte[] buffer = new byte[1024];
		//	using (FileStream fs = new FileStream(fileName, FileMode.Create))
		//	{
		//		int bytesRead = 0;
		//		do
		//		{
		//			bytesRead = source.BaseStream.Read(buffer, 0, buffer.Length);
		//			fs.Write(buffer, 0, bytesRead);
		//		} while (bytesRead > 0);
		//	}
			
		//	if (!wordObjectCreated)
		//	{
		//		ObjWord =new Microsoft.Office.Interop.Word.Application(); //Create Word Application
		//		wordObjectCreated = true;
		//	}

  //          Log.Trace(Log.Category.Converters, Log.Priority.HIGH, String.Format("Word opening '{0}'", objWordFilePath));
		//	ObjWord.Documents.Open(ref objWordFilePath,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M,ref M);
					
		//	/// Update any fields in the document.
		//	ObjWord.ActiveDocument.Fields.Update();
		//	foreach (TableOfContents toc in ObjWord.ActiveDocument.TablesOfContents)
		//	{
		//		toc.Update();
		//	}
   
		//	ObjWord.ActiveDocument.Close(ref saveChanges,ref M,ref M);
			
		//	XmlDocument wordMLDoc = new XmlDocument();
		//	wordMLDoc.Load(fileName);

		//	return wordMLDoc;
	
		//}
		
		//private void DeleteTempFile(string fileName)
		//{
		//	DateTime timeOutValue = DateTime.Now.AddMilliseconds(Convert.ToInt32(cfg.TimeOut));  
		//	while(File.Exists(fileName))
		//	{
		//		// Clear old temp files
		//		try
		//		{
		//			File.Delete(fileName);
		//		}
		//		catch
		//		{
		//			if (DateTime.Now > timeOutValue)
		//			{
		//				throw new Exception(string.Format("HTML Converter. Old temp file '{0}' still in use.", fileName));
		//			}
		//			else
		//			{
		//				Thread.Sleep(10);
		//			}			
		//		}				

		//	}
		//}		
	}
}