using System;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace Perfectus.Server.Plugins.Converters.Html
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		public Config()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		private string htmlConverterXsltPath;
		private string tempFolderPath;
		private string timeout;

		[XmlElement]
		public string TimeOut
		{
			get { return timeout; }
			set { timeout = value; }
		}


		[XmlElement]
		public string HtmlConverterXsltPath
		{
			get { return htmlConverterXsltPath; }
			set { htmlConverterXsltPath = value; }
		}
		
		[XmlElement]
		public string TempFolderPath
		{
			get { return tempFolderPath; }
			set { tempFolderPath = value; }
		}


		public static Config Load()
		{
			XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamReader sr = new StreamReader(configPath))
			{
				return (Config)(serialiser.Deserialize(sr));
			}
		}

		public void Save()
		{
			System.Xml.Serialization.XmlSerializer serialiser = new XmlSerializer(typeof(Config));	
			// Get the current location.
			string configPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
			configPath = Path.GetDirectoryName(configPath) + Path.DirectorySeparatorChar + "config.xml";
			using (StreamWriter sw = new StreamWriter(configPath, false))
			{
				serialiser.Serialize(sw, this);
			}			
		}

	}
}