using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.XPath;

namespace Perfectus.Server.Plugins.Converters.Html
{
	/// <summary>
	/// Summary description for ConverterSupport.
	/// </summary>
	public class ConverterSupport
	{
		public ConverterSupport()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		public string GenerateImageURL(XPathNodeIterator bindata, string docName, string imageName, string urlPagePath, string imagePath)
		{
	
			byte[] data;
			try
			{
				data = Convert.FromBase64String(bindata.Current.Value);
			}
			catch
			{
				data = new byte[0];
			}
			
			if (data.Length == 0)
			{
				return null;
			}
			else
			{
				string fileSuffix = Path.GetExtension(imageName);
				string fileName = GenerateMD5Hash(data) + fileSuffix;
				string filePath = imagePath + docName + Path.DirectorySeparatorChar;
				string filePathName = filePath + fileName;
				
				if (!File.Exists(filePathName))
				{
					if (!Directory.Exists(filePath))
					{
						Directory.CreateDirectory(filePath);
					}
					
					using (FileStream fs = new FileStream(filePathName, FileMode.Create))
					{
						fs.Write(data, 0, data.Length);
					}
				}
				
				string imageSrc = urlPagePath + docName + "/" + fileName;
				return imageSrc;
			}
		}
		
		private string GenerateMD5Hash(byte[] inData)
		{
			byte[] tmpHash;
			
			//Compute hash based on source data
			tmpHash = new MD5CryptoServiceProvider().ComputeHash(inData);
			

			int i;
			StringBuilder sOutput = new StringBuilder(tmpHash.Length);
			for (i=0;i < tmpHash.Length -1; i++) 
			{
				sOutput.Append(tmpHash[i].ToString("X2"));
			}
			return sOutput.ToString();
		}
	}
}