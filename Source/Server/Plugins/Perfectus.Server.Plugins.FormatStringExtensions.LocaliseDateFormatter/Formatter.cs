using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Globalization;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.Plugins.FormatStringExtensions.LocaliseDateFormatter
{
    public class Formatter : IStringFormatter
    {
        /*
		CONFIGURATION SAMPLES FOR GETTING THIS PLUGIN TO WORK:

		Sample Server shared configuration entry:

		<plugin type="formatStringExtension" assemblyName="Perfectus.Server.Plugins.FormatStringExtensions.DateFormatter.dll"
		typeName="Perfectus.Server.Plugins.FormatStringExtensions.DateFormatter.Formatter" key="DateFormatter">
		<name>Date Formatter</name>
		</plugin>

		Sample IPManager configuration entry:

		<formatString forDataType="DateFormats" serverSidePluginKey="DateFormatter">
		<name>Date Formatter</name>	
		</formatString>

		Note - 	The serverSidePluginKey property has the value of the key specified in the server configuration file. 
		Available "forDataType" formats are: TextFormats, NumberFormats and DateFormats		
		*/

        public Formatter()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        
        /// <summary>
        /// Parse the parameter string. 
        /// This plug-in accepts a pipe (|) delimited string.
        /// 1. A .NET date format string. e.g. "d" for a short date, "f" for a full date and time. See http://blog.stevex.net/index.php/string-formatting-in-csharp/ for a list of available date format strings.
        /// 2. An optional two-part culture identifier. E.g. fr-FR, en-US, en-NZ. 
        /// E.g. "d|en-NZ" would return the date formatted as a short date, according to New Zealand English rules.
        ///      "s" would return a sortable date/time string.
        ///      "Y|fr-FR" would return the Year/Month according to French French rules.
        /// </summary>
        /// <param name="inputString"></param>
        /// <param name="inputParams"></param>
        /// <returns></returns>
        public string FormatString(string inputString, string inputParams)
        {
         
            // Default to the running thread's culture.
            CultureInfo ci = Thread.CurrentThread.CurrentCulture;
            string[] inputParamArray = inputParams.Split('|');

            // If no param passed, return the string unchanged.
            if (inputParamArray.Length == 0)
            {
                return inputString;
            }

            if (inputParamArray.Length > 1)
            {
                try
                {
                    ci = new CultureInfo(inputParamArray[1].Trim());
                }
                catch
                {
                    return inputString;
                }
            }

            string formatString = inputParamArray[0];

            // Cast the input string as a date. If that fails, return the string unchanged.
            DateTime dateTime;
            try
            {
                dateTime = DateTime.Parse(inputString);
            }
            catch
            {
                return inputString;
            }

            // Return the input date formatted according to the formatString and using the specified culture's DateTimeFormat rules.
            return dateTime.ToString(formatString, ci.DateTimeFormat);

        }
    }
}
