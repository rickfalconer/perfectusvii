﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Microsoft.Office.Interop.Word;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.PluginKit;
using Perfectus.Library.Logging;

namespace Perfectus.Server.Plugins.Converters.Docx
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class Converter : IConverter
    {
        private StringBindingMetaData instanceMetaData;

        public Converter()
        {
        }

        [Browsable(false)]
        public Guid UniqueIdentifier
        {
            get { return new Guid("C16B3C39-508A-40CC-B402-1F02B2BDCFED"); }
        }

        [Browsable(false)]
        public SourceDocument DocumentTypeConverted
        {
            get { return SourceDocument.WordML; }
        }

        public Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName)
        {
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Docx converter started");
            convertedFileName = string.Format("{0}.docx", fileNameWithoutExtension);
            source.BaseStream.Seek(0, SeekOrigin.Begin);
            Log.Trace(Log.Category.Converters, Log.Priority.HIGH, "# Docx converter started");
            return source.BaseStream;
        }

        [Browsable(false)]
        public StringBindingMetaData InstanceMetaData
        {
            get { return instanceMetaData; }
            set { instanceMetaData = value; }
        }
    }
}