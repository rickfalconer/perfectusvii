#if UNIT_TESTS
using System;
using System.Data;
using NUnit.Framework;
using Perfectus.Server.DataAccess.InterviewSystem;
using Perfectus.DataAccess;

namespace Perfectus.Server.DataAccess.Tests
{

	[TestFixture]
	[CategoryAttribute("DataAccess Layer")]
	public class LoggingFixture : PublishBaseTest
	{
		private Guid instanceId;
		[TestFixtureSetUp]
		public override void FixtureSetUp()
		{
			base.FixtureSetUp();
			instanceId = InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");
		}

		[Test]
		public void LogAllParams()
		{
			int exId = Logging.LogException(DateTime.UtcNow, "FAIL", 0, package.UniqueIdentifier, versionNumber,  revisionNumber, instanceId, "test", "test", "test", "test", "test", "tt", int.MinValue);
			Logging.LogException(DateTime.UtcNow, "FAIL", 0, package.UniqueIdentifier, versionNumber,  revisionNumber, instanceId, "test", "test child", "test child", "test child", "test child", "tt", exId);
		}

		[Test]
		public void LogSomeParams()
		{
			int exId = Logging.LogException(DateTime.UtcNow, "FAIL", int.MinValue,  Guid.Empty, int.MinValue, int.MinValue, Guid.Empty, "test", null, null, null, null, null, int.MinValue);
			Logging.LogException(DateTime.UtcNow, "FAIL", int.MinValue,  Guid.Empty, int.MinValue, int.MinValue, Guid.Empty, "test child", null, null, null, null, null, exId);
		}

        [TestFixtureTearDown]
        public override void FixtureTearDown()
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "Tests_DeleteTestInstances";
                fleet.ExecuteNonQuerySP(commandName);
            }
            base.FixtureTearDown();
        }
	}
}
#endif