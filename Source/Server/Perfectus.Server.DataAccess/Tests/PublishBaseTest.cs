#if UNIT_TESTS
using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using NUnit.Framework;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;
using Perfectus.DataAccess;
using System.Collections.Generic;

namespace Perfectus.Server.DataAccess.Tests
{
	public class PublishBaseTest
	{
		protected Package package;
		protected byte[] packageBytes; 
		protected TemplateDocumentCollection templates;
		protected OutcomeCollection outcomes;
        protected List<AttachmentBase> attachments;
		protected int versionNumber;
		protected int revisionNumber;
		protected Guid templateId;
		protected byte[] originalTemplateBytes;
		protected Guid outcomeId;
		protected Guid outcomeActionId;

		[TestFixtureSetUp]
		public virtual void FixtureSetUp()
		{
			// Small
			string path = @"C:\PROJECTS\REFERENCES\TESTS\Test Package.ip";
			// Large
			//string path = @"C:\PROJECTS\REFERENCES\TESTS\Power of Attorney Suite as at 7 Oct05.ip";

			if (! File.Exists(path))
			{
                throw new FileNotFoundException(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("MustExist"), path));
			}
			
			using (FileStream fs = new FileStream(path, FileMode.Open))
			using (MemoryStream ms = new MemoryStream())
			{
				package = Package.OpenFromStream(fs, StreamingContextStates.CrossMachine);
				fs.Flush();				
				templates = package.Templates;
				templateId = package.Templates[0].UniqueIdentifier;
				originalTemplateBytes = ((WordTemplateDocument2)(templates[0])).WordMLBytes;
				outcomes = package.Outcomes;
                attachments = package.Attachments;
				outcomeId = outcomes[0].UniqueIdentifier;
				outcomeActionId = outcomes[0].Definition.AllPossibleActions[0].UniqueIdentifier;

				Package.SaveToStream(package, ms, StreamingContextStates.Persistence);
				ms.Seek(0, SeekOrigin.Begin);
				packageBytes = ms.ToArray();
				ms.Close();
			}
			Assert.AreNotEqual(0, packageBytes.Length);
			DoPublish();
		}

		[TestFixtureTearDown]
		public virtual void FixtureTearDown()
		{
			DeleteTestPackages();
		}

		protected void DeleteTestPackages()
		{
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "Tests_DeleteTestPackages";
                fleet.ExecuteNonQuerySP(commandName);
            }
		}

		protected void DoPublish()
		{
			InterviewPage startPage = package.Interviews[0].StartPage;
            PackageManager.Publishing.Publish(package.UniqueIdentifier, packageBytes, int.MinValue, true, templates, outcomes, attachments, "nUnit test package", startPage.Name, startPage.UniqueIdentifier, "test", "test", "test", DateTime.UtcNow, DateTime.UtcNow, DateTime.UtcNow, "test", out versionNumber, out revisionNumber, true , string.Empty);
			Assert.AreNotEqual(versionNumber, int.MinValue);
			Assert.AreNotEqual(revisionNumber, int.MinValue);
		}
	}
}
#endif