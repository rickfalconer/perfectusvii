#if UNIT_TESTS
using NUnit.Framework;
using Perfectus.Server.Common.Data;

namespace Perfectus.Server.DataAccess.Tests
{
	/// <summary>
	/// Summary description for RetrievalFixture.
	/// </summary>
	[TestFixture]
	[Category("DataAccess Layer")]
	public class RetrievalFixture : PublishBaseTest
	{

		[Test]
		public void RetrieveTemplateBytesFromTestPackage()
		{
			byte[] templateBytes = PackageManager.Retrieval.GetTemplateBytes(package.UniqueIdentifier, versionNumber, revisionNumber, templateId);
			Assert.AreNotEqual(0, templateBytes.Length);
			Assert.AreEqual(templateBytes, originalTemplateBytes);
		}

		[Test]
		public void RetrieveOutcomeActionBytesFromTestPackage()
		{
			byte[] outcomeActionBytes = PackageManager.Retrieval.GetOutcomeActionBytes(package.UniqueIdentifier, versionNumber,  revisionNumber, outcomeId, outcomeActionId);
			
			Assert.AreNotEqual(0, outcomeActionBytes.Length);
			//TODO: Check against original bytes in package object prior to publication, a la templates above.
		}

		[Test]
		public void GetAllPackages()
		{
			PackageListSet pls = PackageManager.Retrieval.GetPackageList(null);
			Assert.IsTrue(pls.PackageManagement_GetPackages.Rows.Count > 0);
		}

		[Test]
		public void GetAllPackagesForUser()
		{
			PackageListSet pls = PackageManager.Retrieval.GetPackageList("test");
			Assert.AreEqual(1, pls.PackageManagement_GetPackages.Rows.Count);
		}

		[Test]
		public void GetPackageStatus()
		{
			int inProgressRecordCount;
			int historyRecordCount;
			PackageManager.Retrieval.GetPackageStatus("test", 5, 0, 5, 0, 5, out inProgressRecordCount, out historyRecordCount);
		}
	}
}
#endif