#if UNIT_TESTS
using System;
using System.Data;
using System.IO;
using System.Runtime.Serialization;
using NUnit.Framework;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common.Data;
using Perfectus.Server.DataAccess.InterviewSystem;
using Category = NUnit.Framework.CategoryAttribute;
using Perfectus.Common;
using Perfectus.DataAccess;

namespace Perfectus.Server.DataAccess.Tests
{
	/// <summary>
	/// Summary description for InstanceManagementFixture.
	/// </summary>
	[TestFixture]
	[CategoryAttribute("DataAccess Layer")]
	public class InstanceManagementFixture : PublishBaseTest
	{
		private Guid instanceId;
		private Package instance;
		private string filePath;
		private byte[] imageBytes = new byte[]{0xff, 0x00, 0xff, 0x00};
		private Guid imageId1 = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
		private Guid imageId2 = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2);

		[TestFixtureSetUp]
		public override void FixtureSetUp()
		{
			// Publish a test package
			base.FixtureSetUp();

			// Create an instance
			instanceId = InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");
			instance = DoGetInstance(instanceId);

			filePath = @"C:\PROJECTS\REFERENCES\TESTS\Test Upload.doc";
			if (! File.Exists(filePath))
			{
                throw new FileNotFoundException(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("MustExist"), filePath));
			}
		}

		[Test]
		public void CreateInstance()
		{
			Guid returnedInstanceId = InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");
			Assert.AreNotEqual(Guid.Empty, returnedInstanceId);
		}

		[Test]
		[ExpectedException(typeof(Exception))]
		public void CreateInstanceNonexistentPackageId()
		{
			try
			{
				InstanceManagement.CreateInstance(Guid.NewGuid(), 1, 0, "test");
			}
			catch (Exception ex)
			{
				if (ex.Message.IndexOf(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("InstanceNotCreated")) >= 0)
				{
					throw new Exception();
				}
			}
		}

		[TestFixtureTearDown]
		public override void FixtureTearDown()
		{
			DoDeleteTestInstances();
			
			// Delete the test published package
			base.FixtureTearDown ();
		}

		private void DoDeleteTestInstances()
		{
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "Tests_DeleteTestInstances";
                fleet.ExecuteNonQuerySP(commandName);
            }

		}

		[Test]
		public void GetInstance()
		{
			DoGetInstance(instanceId);
		}

		[Test]
		[ExpectedException(typeof(SerializationException))]
		public void GetNonexistentInstance()
		{
			Package p = DoGetInstance(Guid.NewGuid());
			Assert.IsNull(p);
		}

		[Test]
		public void SaveInstance()
		{
			instance.Name = "saved";
			byte[] instanceBytes;

			// Serialise the package 

			using(MemoryStream ms = new MemoryStream())
			{
				// Persistence = do not serialise wordml
				Package.SaveToStream(instance, ms, StreamingContextStates.Persistence);
				// Get the bytes of the package from the stream
				ms.Seek(0, SeekOrigin.Begin);
				instanceBytes = ms.ToArray();
			}
			Assert.AreNotEqual(0, instanceBytes.Length);

			InstanceManagement.SaveInstance(instanceId, instanceBytes);

			Package instanceAfterSave = DoGetInstance(instanceId);
			Assert.AreEqual("saved", instanceAfterSave.Name);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void SaveInstanceEmptyByteArray()
		{
			InstanceManagement.SaveInstance(instanceId, new byte[0]);
		}

		[Test]
		public void RenameInstance()
		{
			InstanceManagement.RenameInstance(instanceId, "test renamed");
		}

		[Test]
		[ExpectedException(typeof(Exception))]
		public void RenameNonexistentInstance()
		{
			try
			{
                InstanceManagement.RenameInstance(Guid.NewGuid(), ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("TestRenamedNonexistent"));
			}
			catch (Exception ex)
			{
                if (ex.Message.IndexOf(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("InstanceNotRenamed")) >= 0)
				{
					throw new Exception();
				}
			}

		}

		[Test]
		public void DeleteInstance()
		{
			Guid instanceToDeleteId = InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");
			InstanceManagement.DeleteInstance(instanceToDeleteId, "test");
			
			Package p = null;
			try
			{
				Guid currentPageId;
				p = InstanceManagement.GetInstance(instanceToDeleteId, out currentPageId);
			}
			catch(SerializationException)
			{
			}

			Assert.IsNull(p);
		}

		// Note: The fixture means the stack is the result of the original createInstance, so this only really tests the data access.
		[Test]
		public void NavigationStackPeek()
		{
			Guid pageId = Guid.Empty;
			InstanceManagement.NavigationStackPeek(instanceId, out pageId);
			Assert.AreNotEqual(Guid.Empty, pageId);
		}

		[Test]
		[ExpectedException(typeof(DataException))]
		public void NavigationStackPeekInvalidInstance()
		{
			Guid pageId;
			InstanceManagement.NavigationStackPeek(Guid.NewGuid(), out pageId);
		}

		[Test]
		public void NavigationStackPopOffNewInstance()
		{
			Guid instanceToPopId = InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");
			try
			{
				InstanceManagement.NavigationStackPop(instanceToPopId, false);
			}
			catch(Exception ex)
			{
                if (ex.Message.IndexOf(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("CannotPopLastPage")) == -1)
				{
					throw;
				}
				else
				{
					return;
				}
			}
            Assert.Fail(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("ExpectedException"));	
		}

		[Test]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void NavigationStackPopBackToBeforeZero()
		{
			InstanceManagement.NavigationStackPopBackToPoint(instanceId, -1);
		}

		[Test]
		public void NavigationStackPopBackToZero()
		{
			InstanceManagement.NavigationStackPopBackToPoint(instanceId, 0);
		}

		[Test]
		public void NavigationStackFastForward()
		{
			InstanceManagement.NavigationStackFastForward(instanceId);
		}

		[Test]
		public void NavigationStackPush()
		{
			bool isSufficientPages = instance.Interviews[0].Pages.Count > 1;
			Assert.IsTrue(isSufficientPages, "Interview zero must have at least two pages for this test to run.");

			InterviewPage pageToPush = instance.Interviews[0].Pages[1];
			InstanceManagement.NavigationStackPush(instanceId, pageToPush.UniqueIdentifier, pageToPush.Name);
		}

		[Test]
		public void NavigationStackGet()
		{
			StackEntry[] stack = InstanceManagement.NavigationStackGet(instanceId);
			Assert.IsTrue(stack.Length > 0, "Stack had a length of less that one.");
		}

		[Test]
		public void NavigationStackGetPendingDeletion()
		{
			StackEntry[] stack = InstanceManagement.NavigationStackGetPendingDeletion(instanceId);
			Assert.IsTrue(stack.Length == 0, "Stack has items pending deletion (a fast forward is avaliable).");
		}

		[Test]
		public void NavigationStackEndToEnd()
		{
			bool isSufficientPages = instance.Interviews[0].Pages.Count > 2;
			Assert.IsTrue(isSufficientPages, "Interview zero must have at least three pages for this test to run.");

			// Make sure the stack has only one page on it.
			StackEntry[] stack = InstanceManagement.NavigationStackGet(instanceId);
			Assert.IsTrue(stack.Length == 1, "Stack must have one page one it for this test to run.");

			InterviewPageCollection pages = instance.Interviews[0].Pages;
			InterviewPage startPage = instance.Interviews[0].StartPage;

			Guid peekId;

			// See if the stack has the first page of the Interview on it for the start of the test.
			InstanceManagement.NavigationStackPeek(instanceId, out peekId);
			Assert.AreEqual(peekId, startPage.UniqueIdentifier, "Init: Top of stack should be the Interview's start page, but isn't.");

			// Push pages[1]
			InstanceManagement.NavigationStackPush(instanceId, pages[1].UniqueIdentifier, pages[1].Name);

			// See if the stack has page 2 on top now.
			InstanceManagement.NavigationStackPeek(instanceId, out peekId);
			Assert.AreEqual(peekId, pages[1].UniqueIdentifier, string.Format("Push 1: Top of stack should be {0}, but isn't.", pages[1].Name));

			// Push pages[2]
			InstanceManagement.NavigationStackPush(instanceId, pages[2].UniqueIdentifier, pages[2].Name);

			// See if the stack has page 2 on top now.
			InstanceManagement.NavigationStackPeek(instanceId, out peekId);
			Assert.AreEqual(peekId, pages[2].UniqueIdentifier, string.Format("Push 2: Top of stack should be {0}, but isn't.", pages[2].Name));

			// Make sure the stack has three pages now.
			stack = InstanceManagement.NavigationStackGet(instanceId);
			Assert.IsTrue(stack.Length == 3, "Push 2: Stack should have three pages, but doesn't.");

			// Pop back to start
			InstanceManagement.NavigationStackPopBackToPoint(instanceId, 0);

			// Make sure the stack has one page again.
			stack = InstanceManagement.NavigationStackGet(instanceId);
			Assert.IsTrue(stack.Length == 1, "Pop Back: Stack should have one page, but doesn't.");

			// Make sure the pending deletion stack contains the remaining pages
			stack = InstanceManagement.NavigationStackGetPendingDeletion(instanceId);
			Assert.IsTrue(stack.Length == 2, "Pop Back: Ghost stack should have two pages, but doesn't.");


			// See if the stack has the start page of the Interview on it again.
			InstanceManagement.NavigationStackPeek(instanceId, out peekId);
			Assert.AreEqual(peekId, startPage.UniqueIdentifier, "Pop Back: Top of stack should be the Interview's start page, but isn't.");

			// Fast forward
			InstanceManagement.NavigationStackFastForward(instanceId);



			// See if the stack has page 2 on top again.
			InstanceManagement.NavigationStackPeek(instanceId, out peekId);
			Assert.AreEqual(peekId, pages[2].UniqueIdentifier, string.Format("Ffwd: Top of stack should be {0}, but isn't.", pages[2].Name));
		}

		[Test]
		public void FileAdd()
		{
			Assert.IsTrue(instance.Questions.Count > 0, "Instance must have at least one question.");
			Question fileQuestion = instance.Questions[0];

			DoFileAddUpdate(fileQuestion);
		}

		[Test]
		public void FileUpdate()
		{
			Assert.IsTrue(instance.Questions.Count > 0, "Instance must have at least two questions.");
			Question fileQuestion = instance.Questions[1];
			
			// Add/Update twice to force an update
			DoFileAddUpdate(fileQuestion);
			DoFileAddUpdate(fileQuestion);

			
		}

		[Test]
		public void FileDelete()
		{
			Assert.IsTrue(instance.Questions.Count > 0, "Instance must have at least one question.");
			Question fileQuestion = instance.Questions[0];
			
			DoFileAddUpdate(fileQuestion);

			InstanceManagement.File_Delete(instanceId, 0, fileQuestion.UniqueIdentifier);
		}

		[Test]
		public void FileGet()
		{
			Assert.IsTrue(instance.Questions.Count > 0, "Instance must have at least one question.");
			Question fileQuestion = instance.Questions[0];
			
			byte[] uploadedBytes = DoFileAddUpdate(fileQuestion);
			string originalFilePath = null;
			string contentType = null;

			byte[] gotBytes = InstanceManagement.File_GetBytes(instanceId, 0, fileQuestion.UniqueIdentifier, out originalFilePath, out contentType);

			Assert.IsTrue(gotBytes.Length > 0, "Get returned zero-byte array.");
			Assert.AreEqual(uploadedBytes, gotBytes, "Uploaded bytes differ from got bytes.");
			Assert.AreEqual(filePath, originalFilePath, "Got file path does not match that passed to add.");
			Assert.AreEqual("application/msword", contentType, "Got file path does not match that passed to add.");
		}

		[Test]
		public void SavePreviewImage()
		{		
			InstanceManagement.SavePreviewImage(imageId1, imageBytes);
		}

		[Test]
		public void GetPreviewImage()
		{
			InstanceManagement.SavePreviewImage(imageId2, imageBytes);
			byte[] returnedBytes;
			using (MemoryStream ms = InstanceManagement.GetPreviewImage(imageId2))
			{
				returnedBytes = new byte[ms.Length];
				ms.Seek(0, SeekOrigin.Begin);
				ms.Read(returnedBytes, 0, (int)ms.Length);
			}
			Assert.AreEqual(imageBytes, returnedBytes);
		}

		[Test]
		public void GetVersion()
		{
			int returnedVersionNumber;
			int returnedRevisionNumber;
			InstanceManagement.GetVersion(instanceId, out returnedVersionNumber, out returnedRevisionNumber);
			Assert.AreEqual(versionNumber, returnedVersionNumber);
			Assert.AreEqual(revisionNumber, returnedRevisionNumber);
		}

		[Test]
		public void SetInstanceStatus()
		{
			InstanceManagement.SetInstanceStatus(instanceId, "FAIL");
		}

		[Test]
		public void SetDistributionResult()
		{
			InstanceManagement.SetDistributionResult(instanceId, new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1), new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1), "test", "test", "test", "test" );
		}

		[Test]
		public void  CopyMirrorFiles()
		{
			// Add a file to the instance
			Assert.IsTrue(instance.Questions.Count > 0, "Instance must have at least one question.");
			Question fileQuestion = instance.Questions[0];

			DoFileAddUpdate(fileQuestion);

			// Make a new instance
			Guid newInstanceId = InterviewSystem.InstanceManagement.CreateInstance(package.UniqueIdentifier, versionNumber, revisionNumber, "test");

			InterviewSystem.InstanceManagement.CopyMirrorFiles(newInstanceId, instanceId);
	
			string originalFilePath;
			string contentType;
			byte[] gotBytes = InstanceManagement.File_GetBytes(instanceId, 0, fileQuestion.UniqueIdentifier, out originalFilePath, out contentType);

			Assert.IsTrue(gotBytes.Length > 0, "Get returned zero-byte array.");
		}


		[Test]
		public void CopyPackageInstance()
		{
			Guid newId = Guid.NewGuid();
			Guid pageId;
			InstanceManagement.CopyPackageInstance(newId, instanceId, "test" );
			Package copiedInstance = InstanceManagement.GetInstance(newId, out pageId);
			Assert.AreEqual(instance.UniqueIdentifier, copiedInstance.UniqueIdentifier);
		}

		[Test]
		public void GetTemplateList()
		{
			TemplateData[] templates = InstanceManagement.GetTemplateList(instanceId);
			Assert.AreNotEqual(0, templates.Length);
		}

		private byte[] DoFileAddUpdate(Question fileQuestion)
		{
			byte[] fileBytes;
			// Get file bytes
			using(FileStream fs = new FileStream(filePath, FileMode.Open))
			{
				fileBytes = new byte[fs.Length];
				fs.Read(fileBytes, 0, fileBytes.Length);
			}
	
			Assert.IsFalse(fileBytes.Length == 0, "File for upload is zero bytes.");
	
			// Do it twice to test the update part of the stored proc.
			InstanceManagement.File_AddUpdate(instanceId, 0, fileQuestion.UniqueIdentifier, filePath, "application/msword", fileBytes);

			return fileBytes;
		}



		private Package DoGetInstance(Guid instanceIdToGet)
		{
			Guid currentPageId;
			return InstanceManagement.GetInstance(instanceIdToGet, out currentPageId);
		}
	}
}
#endif
