using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Reflection;

using Perfectus.DataAccess;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using Perfectus.Library.Exception;

namespace Perfectus.Server.DataAccess.Administration
{
    /// <summary>
    /// Class to query and maintain package instance and transactional information from the data base
    /// for administration purposes.
    /// </summary>
    public class PackageTransaction
    {
        /// <summary>
        /// Returns DataSet of PackageInstance information configured into a transaction like hierarchical structure 
        /// </summary>
        /// <param name="MaxRows">Max number of rows to return.</param>
        /// <param name="MinDate">The minimum date, used to restrict execution time and memory demands</param>
        /// <param name="sortByColumn">Column to sort by (Nullable)</param>
        /// <param name="sortOrder">Sort Order, must be 'DESC' or 'ASC'</param>
        /// <param name="filterTextColumn">Column to do like searches (Nullable)</param>
        /// <param name="filterText">Text search string  (Nullable)</param>
        /// <param name="filterDateColumn">Column to do date filtering  (Nullable)</param>
        /// <param name="filterFromDate">From date  (Nullable)</param>
        /// <param name="filterToDate">To date  (Nullable)</param>
        /// <param name="exStatusValues">List of status values to exclude.  (Nullable)</param>
        /// <returns>The DataSet with single table :'PackageInstances'  </returns>
        public DataSet GetPackageInstanceData(
            int MaxRows,
            DateTime MinDate,
            string sortByColumn,
            System.ComponentModel.ListSortDirection sortOrder,
            string filterTextColumn,
            string filterText,
            string filterDateColumn,
            DateTime? filterFromDate,
            DateTime? filterToDate,
            string exStatusValues )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            // This is what we return.
            DataSet ds = new DataSet( );

            // We must have a minimum date
            if( MinDate.Year < 1900 )
                MinDate = new DateTime( 1900, 1, 1 );

            // Change filter text for 'like' searching
            string formattedFilterText = FormatTextFilter( filterTextColumn, filterText );

            try
            {
                using( IFleet fleet = FleetContainer.Create( ) )
                {
                    // Our SP name.
                    string commandName = "Instance_GetTransactions";

                    // Input parameters.
                    fleet.AddInParameterInt32( "@MaxRows", MaxRows );
                    fleet.AddInParameterDateTime( "@MinDate", MinDate );
                    fleet.AddInParameterString( "@SortBy", String.IsNullOrEmpty( sortByColumn ) ? null : sortByColumn );
                    fleet.AddInParameterString( "@SortOrder", sortOrder == System.ComponentModel.ListSortDirection.Ascending ? "ASC" : "DESC" );
                    fleet.AddInParameterString( "@FilterTextColumn", String.IsNullOrEmpty( filterTextColumn ) ? null : filterTextColumn );
                    fleet.AddInParameterString( "@FilterText", String.IsNullOrEmpty( formattedFilterText ) ? null : formattedFilterText );
                    fleet.AddInParameterString( "@FilterDateColumn", String.IsNullOrEmpty( filterDateColumn ) ? null : filterDateColumn );
                    fleet.AddInParameterDateTime( "@FilterFromDate" );
                    if( filterFromDate.HasValue )
                        fleet.SetParameterValue( "@FilterFromDate", (DateTime)filterFromDate );
                    fleet.AddInParameterDateTime( "@FilterToDate" );
                    if( filterToDate.HasValue )
                        fleet.SetParameterValue( "@FilterToDate", (DateTime)filterToDate );
                    fleet.AddInParameterString( "@ExStatusValue", String.IsNullOrEmpty( exStatusValues ) ? null : exStatusValues );

                    // Output parameter. (Oracle only)
                    fleet.AddCursorOutParameter( "rc1" );

                    fleet.LoadDataSet( commandName, ds, new string[ ] { "PackageInstances" } );
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return ds;
            }
            catch( System.Data.SqlClient.SqlException ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.SQLException" ), "Instance_GetTransactions", ex.Message );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.GeneralException" ), ex, "GetPackageInstanceData", ex.Message );
            }
        }

        /// <summary>
        /// Returns distribution data for a given package instance id.
        /// </summary>
        /// <param name="PackageInstanceId">The package instance ID</param>
        /// <returns>The DataSet of information</returns>
        public DataSet GetPackageDistributions( Guid PackageInstanceId )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            // This is what we return.
            DataSet ds = new DataSet( );

            try
            {
                using( IFleet fleet = FleetContainer.Create( ) )
                {
                    // Our SP name.
                    string commandName = "Instance_GetDistributions";

                    // Input parameters.
                    fleet.AddInParameterGuid( "@PackageInstanceId", PackageInstanceId );

                    // These are the tables that should be returned.
                    fleet.AddCursorOutParameter( "Distributions" );
                    fleet.AddCursorOutParameter( "Exceptions" );

                    fleet.LoadDataSet( commandName, ds, new string[ ] { "Distributions", "Exceptions" } );
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return ds;
            }
            catch( System.Data.SqlClient.SqlException ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.SQLException" ), "Instance_GetDistributions", ex.Message );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.GeneralException" ), ex, "GetPackageDistributions", ex.Message );
            }
        }


        /// <summary>
        /// Returns DataSet of PackageInstance information configured into a transaction like hierarchical structure 
        /// </summary>
        /// <param name="MaxRows">Max number of rows to return.</param>
        /// <param name="MinDate">The minimum date, used to restrict execution time and memory demands</param>
        /// <param name="sortByColumn">Column to sort by (Nullable)</param>
        /// <param name="sortOrder">Sort Order, must be 'DESC' or 'ASC'</param>
        /// <param name="filterTextColumn">Column to do like searches (Nullable)</param>
        /// <param name="filterText">Text search string  (Nullable)</param>
        /// <param name="filterDateColumn">Column to do date filtering  (Nullable)</param>
        /// <param name="filterFromDate">From date  (Nullable)</param>
        /// <param name="filterToDate">To date  (Nullable)</param>
        /// <param name="exStatusValues">List of status values to exclude.  (Nullable)</param>
        /// <returns>The DataSet with single table :'Transactions'   </returns>
        public DataSet GetTransactionData(
            int MaxRows,
            DateTime MinDate,
            string sortByColumn,
            System.ComponentModel.ListSortDirection sortOrder,
            string filterTextColumn,
            string filterText,
            string filterDateColumn,
            DateTime? filterFromDate,
            DateTime? filterToDate,
            string exStatusValues )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            // This is what we return.
            DataSet ds = new DataSet( );

            // We must have a minimum date
            if( MinDate.Year < 1900 )
                MinDate = new DateTime( 1900, 1, 1 );

            // Change filter text for 'like' searching
            string formattedFilterText = FormatTextFilter(filterTextColumn, filterText);

            try
            {
                using( IFleet fleet = FleetContainer.Create( ) )
                {
                    // Our SP name.
                    string commandName = "Job_GetTransactions";

                    // Input parameters.
                    fleet.AddInParameterInt32( "@MaxRows", MaxRows );
                    fleet.AddInParameterDateTime( "@MinDate", MinDate );
                    fleet.AddInParameterString( "@SortBy", String.IsNullOrEmpty( sortByColumn ) ? null : sortByColumn );
                    fleet.AddInParameterString( "@SortOrder", sortOrder == System.ComponentModel.ListSortDirection.Ascending ? "ASC" : "DESC" );
                    fleet.AddInParameterString( "@FilterTextColumn", String.IsNullOrEmpty( filterTextColumn ) ? null : filterTextColumn );
                    fleet.AddInParameterString( "@FilterText", String.IsNullOrEmpty( formattedFilterText ) ? null : formattedFilterText );
                    fleet.AddInParameterString( "@FilterDateColumn", String.IsNullOrEmpty( filterDateColumn ) ? null : filterDateColumn );
                    fleet.AddInParameterDateTime( "@FilterFromDate" );
                    if( filterFromDate.HasValue )
                        fleet.SetParameterValue( "@FilterFromDate", (DateTime)filterFromDate );
                    fleet.AddInParameterDateTime( "@FilterToDate" );
                    if( filterToDate.HasValue )
                        fleet.SetParameterValue( "@FilterToDate", (DateTime)filterToDate );
                    fleet.AddInParameterString( "@ExStatusValue", String.IsNullOrEmpty( exStatusValues ) ? null : exStatusValues );

                    // Output parameter. (Oracle only)
                    fleet.AddCursorOutParameter( "rc1" );

                    fleet.LoadDataSet( commandName, ds, new string[ ] { "Transactions" } );
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return ds;
            }
            catch( System.Data.SqlClient.SqlException ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.SQLException" ), "Job_GetTransactions", ex.Message );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.GeneralException" ), ex, "GetTransactionData", ex.Message );
            }
        }

        /// <summary>
        /// Returns distribution data for a given job queue id.
        /// </summary>
        /// <param name="JobQueueID">The Job Queue ID</param>
        /// <returns>The DataSet of information</returns>
        public DataSet GetJobDistributions( int JobQueueID )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            // This is what we return.
            DataSet ds = new DataSet( );

            try
            {
                using( IFleet fleet = FleetContainer.Create( ) )
                {
                    // Our SP name.
                    string commandName = "Job_GetDistributions";

                    // Input parameters.
                    fleet.AddInParameterInt32( "@JobQueueId", JobQueueID );

                    // These are the tables that should be returned.
                    fleet.AddCursorOutParameter( "Distributions" );
                    fleet.AddCursorOutParameter( "Exceptions" );

                    fleet.LoadDataSet( commandName, ds, new string[ ] { "Distributions", "Exceptions" } );
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return ds;
            }
            catch( System.Data.SqlClient.SqlException ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.SQLException" ), "Job_GetDistributions", ex.Message );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.GeneralException" ), ex, "GetJobDistributions", ex.Message );
            }
        }

        /// <summary>
        /// Returns dictionary of Package status values.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetPackageStatus( )
        {
            return GetStatus( "Instance_StatusList" );
        }

        /// <summary>
        /// Returns dictionary of Job status values.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetJobStatus( )
        {
            return  GetStatus( "Job_StatusList" );
        }

        /// <summary>
        /// Formats the user entered filter to support wild card searching, and column specific requirements.
        /// </summary>
        /// <param name="thecolumn">the text field to filter by </param>
        /// <param name="thefilter">the filter value</param>
        /// <returns>the formated filter text</returns>
        private string FormatTextFilter(string thecolumn, string thefilter)
        {
            if (String.IsNullOrEmpty(thefilter))
                return thefilter;

            // First remove any '*' characters at the beginning and end. The user might or might not have added them.
            // If they didn't, we intend to add them otherselves.
            string filtertext = thefilter.Trim( '*' );

            // Add DB wild card characters.
            filtertext = String.Format("%{0}%", filtertext);

            // Replace any '?' characters.
            filtertext = filtertext.Replace('?', '_');

            return filtertext;
        }

        // Call stored procedure to extract list of key value pairs of job or package status's
        private Dictionary<string, string> GetStatus( string sp_command )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            // This is what we return
            Dictionary<string, string> statusList = new Dictionary<string, string>( );

            try
            {
                using( IFleet fleet = FleetContainer.Create( ) )
                {
                    // Call stored procedure.
                    using( IDataReader rdr = fleet.ExecuteReaderSP( sp_command ) )
                    {
                        while( rdr.Read( ) )
                        {
                            try
                            {
                                // Insert into our list ( No need to check for nulls )
                                statusList.Add( rdr.GetString( 0 ), rdr.GetString( 1 ) );
                            }
                            catch
                            {
                                // Ignore duplicates. (Should never happen)
                            }
                        }
                    }
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return statusList;
            }
            catch( System.Data.SqlClient.SqlException ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.SQLException" ), sp_command, ex.Message );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( Log.Category.DataAccess, new ID( "Administrator.GeneralException" ), ex, "GetStatus", ex.Message );
            }
        }
    }
}
