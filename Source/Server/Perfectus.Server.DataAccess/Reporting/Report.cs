using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Messaging;
using System.Runtime.Serialization;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.DataAccess;
//using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Perfectus.Server.DataAccess.Reporting
{
	/// <summary>
	/// Summary description for Report.
	/// </summary>
	public sealed class Report
	{
		public Report()
		{
		}
        #region Reporting Package Retrieval Methods

        public static ReportSet GetPackagesBySharedLibraryElement(Guid itemId, string packageName, DateTime startDate, DateTime endDate)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "GET_PACKAGES_BY_LIBITEM";
                fleet.AddInParameterGuid("itemId", itemId);
                fleet.AddInParameterString("packageName", packageName);
                fleet.AddCursorOutParameter("PackagesUsingLibraryItem");

                if (startDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("endDate", endDate);
                }

                ReportSet rs = new ReportSet();
                fleet.LoadDataSet(commandName, rs, new string[] { "PackagesUsingLibraryItem" });
                return rs;
            }
        }

		public static ReportSet GetPackagesByClause(string clauseId, string clauseName, string packageName, DateTime startDate, DateTime endDate)
		{
                        using (IFleet fleet = FleetContainer.Create("report","SQLReporting"))
            {
                string commandName = "GET_PACKAGES_BY_CLAUSE";
                fleet.AddInParameterString("clauseId", clauseId);
                fleet.AddInParameterString("clauseName", clauseName);
                fleet.AddInParameterString("packageName", packageName);
                fleet.AddCursorOutParameter("PackagesUsingClause");

                if (startDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("endDate", endDate);
                }

                ReportSet rs = new ReportSet();
                fleet.LoadDataSet(commandName, rs, new string[] { "PackagesUsingClause" });
                return rs;
            }
		}

        public static ReportSet GetClauses(string clauseId, string clauseName, string clauseLibrary)
		{
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "GET_CLAUSES";
                fleet.AddInParameterString("clauseId", clauseId);
                fleet.AddInParameterString("clauseName", clauseName);
                fleet.AddInParameterString("clauseLibrary", clauseLibrary);
                fleet.AddCursorOutParameter("Clauses");
                ReportSet ss = new ReportSet();
                fleet.LoadDataSet(commandName, ss, new string[] { "Clauses" });
                return ss;
            }
		}

		public static int OfflineGetVersionByTempOfflineId(Guid tempOfflineId)
		{
			int version = int.MinValue;

            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "VERSION_GET_BY_MODID";

                fleet.AddInParameterGuid("tempOfflineId", tempOfflineId);
                fleet.AddOutParameter( "VersionNumber", DbType.Int32, 4);

                fleet.ExecuteNonQuerySP(commandName);

                try 
                {
                    version = fleet.GetParameterInt32("versionNumber");
                }
                catch
                {
                    throw new Exception("The package contains no information in the reporting database.");
                }
            }
			return version;
		}

		#endregion
		#region Reporting Package Publish Methods
        public static void PublishReportingPackageInfo(Guid id, int personId, string name, string createdBy, string modifiedBy, string publishedBy, DateTime createdTime, DateTime modifiedTime, DateTime publishedTime, string description, int versionNumber, int revisionNumber, string serverName, Guid tempOfflineId)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string publishCommandName = "PACKAGE_INSERT";

                fleet.AddInParameterGuid("packageId", id);
                fleet.AddInParameterString("Name", name);
                fleet.AddInParameterString("createdBy", createdBy);
                fleet.AddInParameterString("modifiedBy", modifiedBy);
                fleet.AddInParameterString("publishedBy", publishedBy);
                fleet.AddInParameterDateTime("whenCreated", createdTime);
                fleet.AddInParameterDateTime("whenModified", modifiedTime);
                fleet.AddInParameterDateTime("whenPublished", publishedTime);
                fleet.AddInParameterString("description", description);
                fleet.AddInParameterInt32("revisionNumber", revisionNumber);
                fleet.AddInParameterInt32("versionNumber", versionNumber);
                fleet.AddInParameterString("serverName", serverName);
                fleet.AddInParameterGuid("serverId", Guid.Empty);
                fleet.AddInParameterGuid("tempOfflineId", tempOfflineId);

                fleet.ExecuteNonQuerySP(publishCommandName);
            }
        }

        public static void PublishReportingLibItemInfo(Guid packageId, Package package, int versionNumber, int revisionNumber)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string publishCommandName = "LIBITEM_INSERT";
                fleet.AddInParameterGuid("packageId");
                fleet.AddInParameterInt32("versionNumber");
                fleet.AddInParameterInt32("revisionNumber");
                fleet.AddInParameterGuid("itemId");
                fleet.AddInParameterString("itemName");
                fleet.AddInParameterString("itemType");

                foreach (Question q in package.Questions)
                {
                    DoPublishReportingLibraryItemInfo(fleet, publishCommandName, q, packageId, versionNumber, revisionNumber);
                }
                foreach (Outcome o in package.Outcomes)
                {
                    DoPublishReportingLibraryItemInfo(fleet, publishCommandName, o, packageId, versionNumber, revisionNumber);
                }
                foreach (SimpleOutcome so in package.SimpleOutcomes)
                {
                    DoPublishReportingLibraryItemInfo(fleet, publishCommandName, so, packageId, versionNumber, revisionNumber);
                }
                if (package.Interviews.Count > 0)
                {
                    foreach (InterviewPage pg in package.Interviews[0].Pages)
                    {
                        DoPublishReportingLibraryItemInfo(fleet, publishCommandName, pg, packageId, versionNumber, revisionNumber);
                    }
                }
                foreach (PFunction f in package.Functions)
                {
                    DoPublishReportingLibraryItemInfo(fleet, publishCommandName, f, packageId, versionNumber, revisionNumber);
                }
                foreach (TemplateDocument t in package.Templates)
                {
                    DoPublishReportingLibraryItemInfo(fleet, publishCommandName, t, packageId, versionNumber, revisionNumber);
                }
            }
        }

		private static void DoPublishReportingLibraryItemInfo(IFleet fleet, String storedProcName, PackageItem item, Guid packageId, int versionNumber, int revisionNumber)
		{
			if(item is LibraryItem)
			{
				LibraryItem libItem = (LibraryItem) item;
				if(libItem.LibraryUniqueIdentifier != Guid.Empty)
				{	
					fleet.SetParameterValue( "packageId", packageId);
                    fleet.SetParameterValue( "versionNumber", versionNumber);
                    fleet.SetParameterValue( "revisionNumber", revisionNumber);
                    fleet.SetParameterValue( "itemId", libItem.LibraryUniqueIdentifier);
                    fleet.SetParameterValue( "itemName", libItem.Name);
                    fleet.SetParameterValue( "itemType", libItem.LibraryType);	

					try
					{
                        fleet.ExecuteNonQuerySP(storedProcName);
					}
					catch
					{
                        
					}					
				}
			}
		}


        public static void PublishReportingClauseInfo(Guid packageId, OutcomeCollection outcomes, int version, int revision)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string clauseCommandName = "CLAUSE_INSERT";

                fleet.AddInParameterGuid("packageId");
                fleet.AddInParameterInt32("versionNumber");
                fleet.AddInParameterInt32("revisionNumber");
                fleet.AddInParameterString("clauseName");
                fleet.AddInParameterString("clauseId");
                fleet.AddInParameterString("clauseLibrary");
                fleet.AddInParameterString("clauseId");

                foreach (Outcome o in outcomes)
                {
                    foreach (OutcomeAction action in o.Definition.AllPossibleActions)
                    {
                        if (action  != null && action.SubQuery == null && action.LibraryKey != null)
                        {
                            fleet.SetParameterValue("packageId", packageId);
                            fleet.SetParameterValue("versionNumber", version);
                            fleet.SetParameterValue("revisionNumber", revision);
                            fleet.SetParameterValue("clauseName", action.DisplayName);
                            fleet.SetParameterValue("clauseLibrary", action.LibraryName);
                            fleet.SetParameterValue("clauseId", action.ReportingId);

                            try
                            {
                                fleet.ExecuteNonQuerySP(clauseCommandName);
                            }
                            catch
                            {
                                throw;
                            }
                        }
                    }
                }
            }
        }



        public static bool IsPackagePublished(Guid packageId, out DateTime prevModifiedUTC, out int nextVersionNumber)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "PACKAGE_PEEK";

                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddOutParameter("isPreviouslyPublished", DbType.Int32, 4);
                fleet.AddOutParameter("nextVersionNumber", DbType.Int32, 4);
                fleet.AddOutParameter("previousDateModified", DbType.DateTime, 8);

                fleet.ExecuteNonQuerySP(commandName);

                int isPreviouslyPublished = fleet.GetParameterInt32("isPreviouslyPublished");
                nextVersionNumber = fleet.GetParameterInt32("nextVersionNumber");
                prevModifiedUTC = fleet.GetParameterDateTime("previousDateModified");

                return (isPreviouslyPublished == 1 ? true : false);
            }
        }
		#endregion		
		#region Reporting Instance Management Methods

        public static int OfflineGetVersionByDate(Guid packageId, DateTime dateModified)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "VERSION_GET_BYDATE";
                int version = int.MinValue;

                fleet.AddInParameterGuid("PackageId", packageId);
                fleet.AddInParameterInt64("dateModifiedTicks", dateModified.Ticks);
                fleet.AddOutParameter("VersionNumber", DbType.Int32, 4);

                fleet.ExecuteNonQuerySP(commandName);

                try
                {
                    version = fleet.GetParameterInt32("versionNumber");
                }
                catch (Exception)
                {
                    throw new Exception("The package contains no information in the reporting database.");
                }
                return version;
            }
        }

        public static void CreateReportingInstance(Guid packageId, Guid instanceId, int version, int revision, DateTime whenCreatedUtc, string createdBy)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "INSTANCE_CREATE";

                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddInParameterInt32("version", version);
                fleet.AddInParameterInt32("revision", revision);
                fleet.AddInParameterDateTime("whenMadeUtc", whenCreatedUtc);
                fleet.AddInParameterString("createdBy", createdBy);
                fleet.AddInParameterGuid("instanceId", instanceId);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void SetReportingInstanceComplete(Guid instanceId)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "STATUS_SETCOMPLETE";
                fleet.AddInParameterGuid("instanceId", instanceId);
                fleet.ExecuteNonQuerySP(commandName);
            }
        }


        public static void SetReportingClauseUsed(Guid packageInstanceId, string clauseId, string clauseName)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "CLAUSE_USED_INSERT";

                fleet.AddInParameterGuid("packageInstanceId", packageInstanceId);
                fleet.AddInParameterString("clauseId", clauseId);
                fleet.AddInParameterString("clauseName", clauseName);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static ReportSet GetInstancesByClauseUsed(string clauseId, string clauseName, Guid packageId, string referenceName, DateTime startDate, DateTime endDate)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "GET_INSTANCES_BY_CLAUSEUSED";
                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddInParameterString("referenceName", referenceName);
                fleet.AddInParameterString("clauseId", clauseId);
                fleet.AddInParameterString("clauseName", clauseName);

                if (startDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("endDate", endDate);
                }
                fleet.AddCursorOutParameter("InstancesUsingClause");

                ReportSet rs = new ReportSet();
                fleet.LoadDataSet(commandName, rs, new string[] { "InstancesUsingClause" });
                return rs;
            }
        }

        public static ReportSet GetInstancesByPackage(Guid packageId, string referenceName, DateTime startDate, DateTime endDate)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "GET_INSTANCES_BY_PACKAGE";
                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddInParameterString("referenceName", referenceName);

                if (startDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    fleet.AddInParameterDateTime("endDate", endDate);
                }

                ReportSet rs = new ReportSet();
                fleet.LoadDataSet(commandName, rs, new string[] { "InstancesUsingPackage" });
                return rs;
            }
        }

        public static InstanceInfoSet GetInstanceInfoFromCoreDB(DateTime dateToGetPackagesFrom)
        {
            using (IFleet fleet = FleetContainer.Create()) 
            {
                string commandName = "INSTANCE_INFOSET_GET";
                fleet.AddInParameterDateTime("dateToGetPackagesFrom", dateToGetPackagesFrom);
                fleet.AddCursorOutParameter("rc1");
                fleet.AddCursorOutParameter("rc2");
                InstanceInfoSet ds = new InstanceInfoSet();
                fleet.LoadDataSet(commandName, ds, new string[] { "Package", "PackageInstance" });
                return ds;
            }
        }

        public static bool DoesPackageExist(Guid packageId, int version, int revision)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "PACKAGE_COUNT";
                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddInParameterInt32("version", version);
                fleet.AddInParameterInt32("revision", revision);
                fleet.AddOutParameter("PackageCount", DbType.Int32, 4); 

                fleet.ExecuteNonQuerySP(commandName);
                int packageCount = fleet.GetParameterInt32("PackageCount");
                return (packageCount > 0 ? true : false );
            }
        }

        public static bool DoesInstanceExist(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "INSTANCE_COUNT";
                fleet.AddInParameterGuid("instanceId", packageInstanceId);
                fleet.AddOutParameter("InstanceCount", DbType.Int32, 4);
                fleet.ExecuteNonQuerySP(commandName);
                int instanceCount = fleet.GetParameterInt32("InstanceCount");
                return (instanceCount > 0 ? true : false);
            }
        }

        #endregion
        #region Reporting Store Instance Answers Methods

        public static void SaveAnswers(Guid packageInstanceId, ReportAnswerSet newSet)
        {
            using (IFleet fleet = FleetContainer.Create("report", "SQLReporting"))
            {
                string commandName = "GetAnswers";
                // We need to load what is currently in the database for the instance into a dataset so that after merging the new dataset ADO
                // knows how do it's Insert/Update/Delete magic.

                fleet.AddInParameterGuid("packageUniqueIdentifier", packageInstanceId);

                fleet.AddCursorOutParameter ("PackageInstanceCur");
                fleet.AddCursorOutParameter ("Question");
                fleet.AddCursorOutParameter ("DataType");
                fleet.AddCursorOutParameter ("AnswerRow");
                fleet.AddCursorOutParameter("AnswerCell");

                ReportAnswerSet ds = new ReportAnswerSet();
                fleet.LoadDataSet(commandName, ds, new string[] { "PackageInstance", "Question", "DataType", "AnswerRow", "AnswerCell" });

                fleet.BeginTransaction();
                // Flag each answer records as deleted by clearing rows, if a primary key in the new set matches a 'deleted' record in the original set 
                // then ADO will be smart enough to change the rowState to modified and do updates (as long as we set PreserveChanges to false) or
                // if there is something in the old set but not the new set then ADO will see there have a row state of deleted and delete them.

                foreach (ReportAnswerSet.AnswerCellRow acr in ds.AnswerCell.Rows)
                {
                    acr.Delete();
                }
                foreach (ReportAnswerSet.AnswerRowRow arr in ds.AnswerRow.Rows)
                {
                    arr.Delete();
                }

                // Merge our new set with the erlier retrieved one.
                ds.Merge(newSet, false);

                String questionInsert = "QUESTION_INSERT";
                String questionUpdate = "QUESTION_UPDATE";
                String questionDelete = "QUESTION_DELETE";
                SetQuestionCommands(fleet, questionInsert, questionUpdate,questionDelete);
                
                String answerRowInsert = "ANSWERROW_INSERT";
                String answerRowUpdate = "ANSWERROW_UPDATE";
                String answerRowDelete = "ANSWERROW_DELETE";
                SetAnswerRowCommands(fleet, answerRowInsert, answerRowUpdate, answerRowDelete);

                String answerCellInsert= "ANSWERCELL_INSERT";
                String answerCellUpdate= "ANSWERCELL_UPDATE";
                String answerCellDelete= "ANSWERCELL_DELETE";
                SetAnswerCellCommands(fleet, answerCellInsert, answerCellUpdate, answerCellDelete);
                
                DataSet dsDeletes = ds.GetChanges(DataRowState.Deleted);
                DataSet dsInsertUpdates = ds.GetChanges(DataRowState.Modified | DataRowState.Added);

                // To ensure referential integrity we need to do things in the right order...

                // do deletes, from child tables up.
                if (dsDeletes != null)
                {
                    fleet.UpdateDataSet(dsDeletes, "AnswerCell", answerCellInsert, answerCellUpdate, answerCellDelete);
                    fleet.UpdateDataSet(dsDeletes, "AnswerRow", answerRowInsert, answerRowUpdate, answerRowDelete);
                }

                // do inserts/updates from, parent table down.
                if (dsInsertUpdates != null)
                {
                    fleet.UpdateDataSet(dsInsertUpdates, "Question", questionInsert, questionUpdate, questionDelete);
                    fleet.UpdateDataSet(dsInsertUpdates, "AnswerRow", answerRowInsert, answerRowUpdate, answerRowDelete);
                    fleet.UpdateDataSet(dsInsertUpdates, "AnswerCell", answerCellInsert, answerCellUpdate, answerCellDelete);
                }

                fleet.Commit();
            }
        }

        private static void SetQuestionCommands(IFleet fleet, String insertID, String updateID, String deleteID)
        {
            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("name", "name", DbType.String, insertID);
            fleet.AddInParameterColumn("dataTypeCode", "dataTypeCode", DbType.String, insertID);
            fleet.AddInParameterColumn("prompt", "prompt", DbType.String, insertID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("name", "name", DbType.String, updateID);
            fleet.AddInParameterColumn("dataTypeCode", "dataTypeCode", DbType.String, updateID);
            fleet.AddInParameterColumn("prompt", "prompt", DbType.String, updateID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, deleteID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, deleteID);
        }

        private static void SetAnswerRowCommands(IFleet fleet, String insertID, String updateID, String deleteID)
        {
            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, insertID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, updateID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, deleteID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, deleteID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, deleteID);
        }

        private static void SetAnswerCellCommands(IFleet fleet, String insertID, String updateID, String deleteID)
        {
            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, insertID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, insertID);
            fleet.AddInParameterColumn("cellNumber", "cellNumber", DbType.Int32, insertID);
            fleet.AddInParameterColumn("textValue", "textValue", DbType.String, insertID);
            fleet.AddInParameterColumn("numberValue", "numberValue", DbType.Decimal, insertID);
            fleet.AddInParameterColumn("yesNoValue", "yesNoValue", DbType.Boolean, insertID);
            fleet.AddInParameterColumn("dateValue", "dateValue", DbType.DateTime, insertID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, updateID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, updateID);
            fleet.AddInParameterColumn("cellNumber", "cellNumber", DbType.Int32, updateID);
            fleet.AddInParameterColumn("textValue", "textValue", DbType.String, updateID);
            fleet.AddInParameterColumn("numberValue", "numberValue", DbType.Decimal, updateID);
            fleet.AddInParameterColumn("yesNoValue", "yesNoValue", DbType.Boolean, updateID);
            fleet.AddInParameterColumn("dateValue", "dateValue", DbType.DateTime, updateID);

            fleet.AddInParameterColumn("packageInstanceId", "packageInstanceId", DbType.Guid, deleteID);
            fleet.AddInParameterColumn("questionId", "questionId", DbType.Guid, deleteID);
            fleet.AddInParameterColumn("rowNumber", "rowNumber", DbType.Int32, deleteID);
            fleet.AddInParameterColumn("cellNumber", "cellNumber", DbType.Int32, deleteID);
        }

        #endregion
    }
}
