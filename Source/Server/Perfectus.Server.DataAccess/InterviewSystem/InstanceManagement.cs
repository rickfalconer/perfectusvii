using System;
using System.Collections;
using System.Data;
//using System.Data.Common;
//using System.Data.SqlTypes;
using System.IO;
using System.Messaging;
using System.Runtime.Serialization;
//using Microsoft.Practices.EnterpriseLibrary.Common;
//using Microsoft.Practices.EnterpriseLibrary.Data;
//using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;

using Perfectus.DataAccess;

namespace Perfectus.Server.DataAccess.InterviewSystem
{
    /// <summary>
    /// Summary description for InstanceManagement.
    /// </summary>
    public class InstanceManagement
    {
        private InstanceManagement()
        {
        }

        public static Package GetInstance(Guid packageInstanceId, out Guid currentPageId)
        {
            Package p = null;

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_GET";

                fleet.AddInParameterGuid("packageInstanceId", packageInstanceId);
                fleet.AddOutParameter("currentPageId", DbType.Guid, 16);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        p = GetPackageFromData(rdr);
                        rdr.Close();
                    }
                }
                catch (SerializationException sEx)
                {
                    throw new SerializationException(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("InstanceNotRetrived."), sEx);
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }

                }
                currentPageId = fleet.GetParameterGuid("currentPageId");
            }
            return p;
        }

        [Obsolete("This should no longer be called - serialising a package instance is too expensive for large packages. Use SaveInstanceAnswerState() instead to just save interview-relevant data (answers, seen, bound items, etc.)")]
        public static void SaveInstance(Guid packageInstanceId, byte[] instanceBytes)
        {
            throw new Exception("Obsolete");
        }

        public static void SaveInstanceAnswerState(Guid packageInstanceId, byte[] stateBytes)
        {
            if (stateBytes == null || stateBytes.Length == 0)
            {
                throw new ArgumentNullException("stateBytes", ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("StateBytesNull"));
            }

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_SAVEANSWERSTATE";
                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterBinary("State", stateBytes);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static MemoryStream GetInstanceAnswerState(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_GETANSWERSTATE";
                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);

                fleet.AddCursorOutParameter("cur_OUT");

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        MemoryStream ms = GetStreamFromData(rdr);
                        rdr.Close();
                        return ms;
                    }
                }
                finally
                {
                    // Close the reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
        }

        public static string GetInstanceReference(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_GETREFERENCE";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddOutParameter("Reference", DbType.StringFixedLength, 50);

                fleet.ExecuteNonQuerySP(commandName);

                object reference = fleet.GetParameterString("Reference");

                if (Convert.IsDBNull(reference))
                {
                    return null;
                }
                else
                {
                    return reference.ToString().Trim();
                }
            }
        }

        public static void RenameInstance(Guid packageInstanceId, string newName)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_RENAME";

                fleet.AddInParameterGuid("instanceId", packageInstanceId);
                // Server will trow error if we pass in a name that exceeds the column width
                fleet.AddInParameterString("NewName", newName.Substring(0,Math.Min ( newName.Length,50)));

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void DeleteInstance(Guid packageInstanceId, string deletedBy)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_DELETE";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterString("DeletedBy", deletedBy);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static Guid CreateInstance(Guid packageId, int version, int revision, string createdBy)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_CREATE";

                // Create this here above the DB, as Oracle has no 'NewId()' equivalent.
                Guid newInstanceId = Guid.NewGuid();

                fleet.AddInParameterGuid("packageId", packageId);
                fleet.AddInParameterInt32("version", version);
                fleet.AddInParameterInt32("revision", revision);
                fleet.AddInParameterString("createdBy", createdBy);
                fleet.AddInParameterGuid("instanceId", newInstanceId);

                fleet.ExecuteNonQuerySP(commandName);

                return newInstanceId;
            }
        }

        public static void NavigationStackPeek(Guid packageInstanceId, out Guid pageId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_PEEK";
                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddOutParameter("PageId", DbType.Guid, 16);

                fleet.ExecuteNonQuerySP(commandName);

                Guid guid = fleet.GetParameterGuid("PageId");
                if (guid==Guid.Empty)
                {
                    throw new DataException(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("PageIdNotReturned"));
                }
                else
                {
                    pageId = guid;
                }
            }
        }

        public static void NavigationStackPop(Guid packageInstanceId, bool forcePop)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_POP";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterBoolean("ForcePop", forcePop);
                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void NavigationStackPopBackToPoint(Guid packageInstanceId, int positionInStack)
        {
            if (positionInStack < 0)
            {
                // NavigationStack is zero-based
                throw new ArgumentOutOfRangeException("positionInStack", ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("OutOfBounds"));
            }

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_POPBACKTO";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterInt32("PointInStack", positionInStack);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void NavigationStackFastForward(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_FASTFORWARD";
                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void NavigationStackPush(Guid packageInstanceId, Guid pageId, string pageName)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_PUSH";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterGuid("PageId", pageId);
                fleet.AddInParameterString("PageName", pageName);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static StackEntry[] NavigationStackGet(Guid packageInstanceId)
        {
            ArrayList al = new ArrayList();
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_GET";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        while (rdr.Read())
                        {
                            StackEntry se = new StackEntry();
                            se.PageId = rdr.GetGuid(0);
                            se.PositionInStack = rdr.GetInt32(1);
                            se.PageName = rdr.GetString(2);
                            al.Add(se);
                        }
                    }
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }

            return (StackEntry[])(al.ToArray(typeof(StackEntry)));
        }

        public static StackEntry[] NavigationStackGetPendingDeletion(Guid packageInstanceId)
        {
            ArrayList al = new ArrayList();
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "NAVSTACK_GETGHOST";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        while (rdr.Read())
                        {
                            StackEntry se = new StackEntry();
                            se.PageId = rdr.GetGuid(0);
                            se.PositionInStack = rdr.GetInt32(1);
                            se.PageName = rdr.GetString(2);
                            al.Add(se);
                        }
                    }
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
            return (StackEntry[])(al.ToArray(typeof(StackEntry)));
        }

        public static void File_AddUpdate(Guid packageInstanceId, int sequenceNumber, Guid questionId, string originalFilePath, string contentType, byte[] fileData)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_FILE_INSERTUPDATE";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterInt32("SequenceNumber", sequenceNumber);
                fleet.AddInParameterGuid("QuestionUID", questionId);
                fleet.AddInParameterString("OriginalFilePath", originalFilePath);
                fleet.AddInParameterString("ContentType", contentType);
                fleet.AddInParameterBinary("FileData", fileData);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void File_Delete(Guid packageInstanceId, int sequenceNumber, Guid questionId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_FILE_DELETE";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterInt32("SequenceNumber", sequenceNumber);
                fleet.AddInParameterGuid("QuestionUID", questionId);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static byte[] File_GetBytes(Guid packageInstanceId, int sequenceNumber, Guid questionId, out string originalFilePath, out string contentType)
        {
            byte[] bytes = null;
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_FILE_GET";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);
                fleet.AddInParameterInt32("SequenceNumber", sequenceNumber);
                fleet.AddInParameterGuid("QuestionUID", questionId);

                //TODO: Should these lengths be halved? Does the library understand SQL and Oracle's unicode types?
                fleet.AddOutParameter("OriginalFilePath", DbType.String, 1000);
                fleet.AddOutParameter("ContentType", DbType.String, 100);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        bytes = PackageManager.Retrieval.GetBytesFromDataReader(rdr);
                    }
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }

                originalFilePath = fleet.GetParameterString("OriginalFilePath");
                contentType = fleet.GetParameterString("ContentType");
            }
            return bytes;
        }

        public static void SavePreviewImage(Guid imageId, byte[] imageData)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "DOCPREVIEW_INSERTIMAGE";

                fleet.AddInParameterGuid("ImageId", imageId);
                fleet.AddInParameterBinary("ImageData", imageData);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static MemoryStream GetPreviewImage(Guid imageId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "DOCPREVIEW_GETIMAGE";

                fleet.AddInParameterGuid("ImageId", imageId);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);
                try
                {
                    using (rdr)
                    {
                        MemoryStream ms = GetStreamFromData(rdr);
                        return ms;
                    }
                }
                finally
                {
                    DeletePreviewImage(imageId);

                    // Close the reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
        }

        private static void DeletePreviewImage(Guid imageId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "DOCPREVIEW_DELETEIMAGE";

                fleet.AddInParameterGuid("ImageId", imageId);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void GetVersion(Guid packageInstanceId, out int versionNumber, out int revisionNumber)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_VERSION_GET";

                fleet.AddInParameterGuid("PackageInstanceId", packageInstanceId);

                fleet.AddOutParameter("VersionNumber", DbType.Int32, 4);
                fleet.AddOutParameter("RevisionNumber", DbType.Int32, 4);

                fleet.ExecuteNonQuerySP(commandName);

                versionNumber = fleet.GetParameterInt32("VersionNumber");
                revisionNumber = fleet.GetParameterInt32("RevisionNumber");
            }
        }

        public static void SetInstanceStatus(Guid instanceId, string statusCode)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "STATUS_UPDATE";

                fleet.AddInParameterGuid("instanceId", instanceId);
                fleet.AddInParameterString("statusCode", statusCode);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void SetDistributionResult(Guid instanceId, Guid distributorId, Guid templateId, string templateName, string link, string linkText, string internalReference,  
            string distributorName, string mIMEFormat, int id, Guid pageId, string pageName)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "DISTRIBUTIONRESULT_INSERT";

                fleet.AddInParameterGuid("instanceId", instanceId);
                fleet.AddInParameterGuid("distributorId", distributorId);
                fleet.AddInParameterGuid("templateId", templateId);
                fleet.AddInParameterString("templateName", templateName);
                fleet.AddInParameterString("link", link);
                fleet.AddInParameterString("linkText", linkText);
                fleet.AddInParameterString("internalReference", internalReference);

                // need this for Oracle....
                if ( distributorName != null )
                    fleet.AddInParameterString("DistributorName", distributorName.Substring ( 0, Math.Min (distributorName.Length, 250)) );
                else
                    fleet.AddInParameterString("DistributorName", null);

                if ( mIMEFormat != null )
                    fleet.AddInParameterString("MIMEFormat", mIMEFormat.Substring ( 0, Math.Min (mIMEFormat.Length, 250)) );
                else
                    fleet.AddInParameterString("MIMEFormat", null);
                
                if (-1 != id)
                    fleet.AddInParameterInt32("JobQueueID", id);
                else
                    fleet.AddInParameterString("JobQueueID", null);
                
                fleet.AddInParameterGuid("PageId", pageId);
                
                if ( pageName != null )
                    fleet.AddInParameterString("PageName", pageName.Substring ( 0, Math.Min (pageName.Length, 50)) );

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void SetDistributionResult(Guid instanceId, Guid distributorId, Guid templateId, string templateName, string link, string linkText, string internalReference)
        {
            SetDistributionResult(instanceId, distributorId, templateId, templateName, link, linkText, internalReference,
                null, null, -1, Guid.Empty, null);
        }

        public static void CopyMirrorFiles(Guid newPackageInstanceId, Guid oldPackageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_FILE_INSERTCOPY";

                fleet.AddInParameterGuid("newPackageInstanceId", newPackageInstanceId);
                fleet.AddInParameterGuid("oldPackageInstanceId", oldPackageInstanceId);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void CopyPackageInstance(Guid newPackageInstanceId, Guid oldPackageInstanceId, string userIdent)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_INSERTCOPY";

                fleet.AddInParameterGuid("newPackageInstanceId", newPackageInstanceId);
                fleet.AddInParameterGuid("oldPackageInstanceId", oldPackageInstanceId);
                fleet.AddInParameterString("userIdent", userIdent);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static TemplateData[] GetTemplateList(Guid instanceId)
        {
            ArrayList al = new ArrayList();

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_TEMPLATELIST_GET";

                fleet.AddInParameterGuid("InstanceId", instanceId);

                IDataReader rdr = fleet.ExecuteReaderSP(commandName);

                try
                {
                    using (rdr)
                    {
                        while (rdr.Read())
                        {
                            TemplateData td = new TemplateData();
                            td.UniqueIdentifier = rdr.GetGuid(0);
                            td.Name = rdr.GetString(1);
                            al.Add(td);
                        }
                    }
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
            return (TemplateData[])(al.ToArray(typeof(TemplateData)));
        }

        public static void EnQueuePackageInstance(Job job, bool isHighPriority)
        {
            bool ShallUseDatabaseQueues = Config.Shared.queue.databaseQueue;
            if (ShallUseDatabaseQueues)
            {
                DatabaseJobQueue.DatabaseJobQueue.Create(job);
                return;
            }

            string queueName = Config.Shared.queue.name;
            string machineName = Config.Shared.queue.server;
            bool isPrivate = Config.Shared.queue.isPrivate;

            MessageQueue queue;
            MessageQueue.EnableConnectionCache = false;
            Message msg = new Message();
            msg.Body = job;
            msg.Priority = (isHighPriority) ? MessagePriority.High : MessagePriority.Normal;

            using (queue = MSMQ.Initialise(machineName, queueName, isPrivate))
            {
                msg.Formatter = queue.Formatter;
                // Enqueue the package instance		
                string label = string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.DataAccess.Localisation").GetString("InstanceOfPackage"), job.InstanceId, job.VersionNumber, job.RevisionNumber);

                if (queue.Transactional)
                {
                    MessageQueueTransaction transaction = new MessageQueueTransaction();
                    transaction.Begin();
                    queue.Send(msg, label, transaction);
                    transaction.Commit();
                }
                else
                    queue.Send(msg, label);
            }
        }

        private static Package GetPackageFromData(IDataReader rdr)
        {
            using (rdr)
            using (MemoryStream ms = new MemoryStream())
            {
                const int chunkSize = 32768;
                if (rdr.Read())
                {
                    byte[] buffer = new byte[chunkSize];
                    long idx = 0;
                    long size = 0;
                    //Write the BLOB chunk by chunk to the stream.
                    while ((size = rdr.GetBytes(0, idx, buffer, 0, chunkSize)) == chunkSize)
                    {
                        ms.Write(buffer, 0, chunkSize);
                        idx += chunkSize;
                    }
                    //Write the last bytes.
                    byte[] remaining = new byte[size];
                    Array.Copy(buffer, 0, remaining, 0, size);
                    ms.Write(remaining, 0, remaining.Length);
                }

                // Deserialise a package from it
                Package P = Package.OpenFromStream(ms, StreamingContextStates.Persistence);
                return P;
            }
        }

        private static MemoryStream GetStreamFromData(IDataReader rdr)
        {
            MemoryStream ms;
            ms = new MemoryStream();
            using (rdr)
            {
                const int chunkSize = 32768;
                if (rdr.Read())
                {
                    byte[] buffer = new byte[chunkSize];
                    long idx = 0;
                    long size = 0;
                    //Write the BLOB chunk by chunk to the stream.

                    try
                    {
                        while ((size = rdr.GetBytes(0, idx, buffer, 0, chunkSize)) == chunkSize)
                        {
                            ms.Write(buffer, 0, chunkSize);
                            idx += chunkSize;
                        }
                        //Write the last bytes.
                        byte[] remaining = new byte[size];
                        Array.Copy(buffer, 0, remaining, 0, size);
                        ms.Write(remaining, 0, remaining.Length);
                    }
                    catch (Exception ex)
                    {
                        //TODO:HACK
                        // After porting to .Net V2 rdr.GetBtytes() throws this exception when the AnswerState column contains null data.
                    }
                }
                // Rewind the stream
                ms.Seek(0, SeekOrigin.Begin);
                return ms;
            }
        }

        public static void SetCompleted(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_SETCOMPLETE";

                fleet.AddInParameterGuid("packageInstanceId", packageInstanceId);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void SetSynchronized(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_SETSYNCHED";

                fleet.AddInParameterGuid("packageInstanceId", packageInstanceId);
                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        public static void ResumeInstance(Guid packageInstanceId)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_RESUMED";

                fleet.AddInParameterGuid("packageInstanceId", packageInstanceId);
                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        /// <summary>
        ///     Using the given instance id, the status information is obtained from
        ///     the database.
        /// </summary>
        /// <param name="instanceId">The instance id to be retireved.</param>
        /// <returns>The instances status information as an xml string.</returns>
        public static string GetInstanceStatusXml(Guid instanceId)
        {
            DataSet instanceStatusDataSet = new DataSet("InstanceStatus");
            
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "INSTANCE_STATUS_GET";

                // Define the stored procedure logic
                fleet.AddInParameterGuid("instanceId", instanceId);
                fleet.AddCursorOutParameter("rc1");
                fleet.AddCursorOutParameter("rc2"); 
                fleet.AddCursorOutParameter("rc3");

                // Populate a DataSet - will contain one package instance record (if found), with related tables
                fleet.LoadDataSet(commandName, instanceStatusDataSet, new string[] { "PackageInstance", "DistributionResult", "Errors" });
            }
            return instanceStatusDataSet.GetXml();
        }
    }
}
