using System;
using System.Data;
using System.Data.Common;
using System.IO;

using Perfectus.Server.Common.Data;
using Perfectus.DataAccess;
//using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Perfectus.Server.DataAccess.PackageManager
{
    /// <summary>
    /// Summary description for Retrieval.
    /// </summary>
    public sealed class Retrieval
    {
        //		private static readonly string connStr = Config.Shared.database.connectionString;//ConfigurationSystem.Configuration.Perfectus_Server_DataAccess_ConnectionString;

        private Retrieval()
        {

        }

        public static byte[] GetTemplateBytes(Guid packageId, int versionNumber, int revisionNumber, Guid templateId)
        {
            byte[] outBytes = new byte[0];

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGE_TEMPLATE_GET";

                fleet.AddInParameterGuid("PackageId", packageId);
                fleet.AddInParameterInt32("VersionNumber", versionNumber);
                fleet.AddInParameterInt32("RevisionNumber", revisionNumber);
                fleet.AddInParameterGuid("TemplateId", templateId);

                IDataReader rdr = null;

                try
                {
                    rdr = fleet.ExecuteReaderSP(commandName);
                    outBytes = GetBytesFromDataReader(rdr);
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
            return outBytes;
        }

        public static byte[] GetAttachmentBytes(Guid packageId, int versionNumber, int revisionNumber, Guid attachmentId)
        {
            byte[] outBytes = new byte[0];

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGE_ATTACHMENT_GET";

                fleet.AddInParameterGuid("PackageId", packageId);
                fleet.AddInParameterInt32("VersionNumber", versionNumber);
                fleet.AddInParameterInt32("RevisionNumber", revisionNumber);
                fleet.AddInParameterGuid("AttachmentId", attachmentId);

                IDataReader rdr = null;

                try
                {
                    rdr = fleet.ExecuteReaderSP(commandName);
                    outBytes = GetBytesFromDataReader(rdr);
                }
                finally
                {
                    // Close the Reader.
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
            return outBytes;
        }


        public static byte[] GetOutcomeActionBytes(Guid packageId, int versionNumber, int revisionNumber, Guid outcomeId, Guid outcomeActionId)
        {
            byte[] OutcomeActionBytes;
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGE_OUTCOMEACTION_GET";

                fleet.AddInParameterGuid("PackageId", packageId);
                fleet.AddInParameterInt32("VersionNumber", versionNumber);
                fleet.AddInParameterInt32("RevisionNumber", revisionNumber);
                fleet.AddInParameterGuid("outcomeId", outcomeId);
                fleet.AddInParameterGuid("outcomeActionId",  outcomeActionId);

                IDataReader outcomeActionReader = null;

                try
                {
                    outcomeActionReader = fleet.ExecuteReaderSP(commandName);
                    OutcomeActionBytes = GetBytesFromDataReader(outcomeActionReader);
                }
                finally
                {
                    // Close the Reader.
                    if (outcomeActionReader != null)
                    {
                        outcomeActionReader.Close();
                    }
                }
            }
            return OutcomeActionBytes;
        }

        public static PackageListSet GetPackageList(string createdBy)
        {
            PackageListSet pls = new PackageListSet();
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGE_GETALLBY";
                fleet.AddInParameterString("createdBy", createdBy);
                fleet.LoadDataSet(commandName, pls, new string[] { "PackageManagement_GetPackages" });
            }

            return pls;
        }

        public static StatusSet GetPackageStatus(string createdBy, int recentTransactionCount, int inProgressStart, int inProgressEnd, int myHistoryStart, int myHistoryEnd, out int inProgressRecordCount, out int historyRecordCount)
        {
            StatusSet ss = new StatusSet();
            /*

            Logging.
            LogException(
            DateTime.Now, "BCD", 1, Guid.NewGuid(), 1,
            1, Guid.NewGuid(), "s1", "s2", "s3", "s4",
            "s5", "s6", 1);

            */
            // NLS_COMP=LINGUISTIC
            // NLS_SORT=BINARY_CI

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "STATUS_GETBY";

                fleet.AddInParameterString("CreatedByPersonId");
                if (createdBy != null)
                {
                    fleet.SetParameterValue("CreatedByPersonId", createdBy);
                }
                fleet.AddInParameterInt32("RecentTransactionCount", recentTransactionCount);
                fleet.AddInParameterInt32("InProgressStart", inProgressStart);
                fleet.AddInParameterInt32("InProgressEnd", inProgressEnd);
                fleet.AddInParameterInt32("MyHistoryStart", myHistoryStart);
                fleet.AddInParameterInt32("MyHistoryEnd", myHistoryEnd);

                fleet.AddOutParameter("InProgressRecordCount", DbType.Int32, 4);
                fleet.AddOutParameter("HistoryRecordCount", DbType.Int32, 4);

                fleet.AddCursorOutParameter("MostRecent");
                fleet.AddCursorOutParameter("InProgress");
                fleet.AddCursorOutParameter("MyHistory");
                fleet.AddCursorOutParameter("InterviewStatus");
                fleet.AddCursorOutParameter("DistributionResult");
                fleet.AddCursorOutParameter("Errors");

                fleet.BeginTransaction();
                
                fleet.LoadDataSet(commandName, ss, new string[] { "MostRecent", "InProgress", "MyHistory", "InterviewStatus", "DistributionResult", "Errors" });

                inProgressRecordCount = fleet.GetParameterInt32("InProgressRecordCount");
                historyRecordCount = fleet.GetParameterInt32("HistoryRecordCount");
                fleet.Commit();
            }
            return ss;
        }

        internal static byte[] GetBytesFromDataReader(IDataReader rdr)
        {
            using (rdr)
            using (MemoryStream ms = new MemoryStream())
            {
                const int chunkSize = 1024;
                if (rdr.Read())
                {
                    byte[] buffer = new byte[chunkSize];
                    long idx = 0;
                    long size = 0;
                    //Write the BLOB chunk by chunk to the stream.
                    while ((size = rdr.GetBytes(0, idx, buffer, 0, chunkSize)) == chunkSize)
                    {
                        ms.Write(buffer, 0, chunkSize);
                        idx += chunkSize;
                    }
                    //Write the last bytes.
                    byte[] remaining = new byte[size];
                    Array.Copy(buffer, 0, remaining, 0, size);
                    ms.Write(remaining, 0, remaining.Length);
                }

                // Rewind the stream
                ms.Seek(0, SeekOrigin.Begin);

                // Deserialise a package from it
                byte[] outBytes = new byte[ms.Length];
                ms.Read(outBytes, 0, (int)ms.Length);
                return outBytes;
            }
        }
    }
}
