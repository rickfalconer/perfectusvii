using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Perfectus.Common.PackageObjects;
using Perfectus.DataAccess;
using System.Collections.Generic;

namespace Perfectus.Server.DataAccess.PackageManager
{
    /// <summary>
    /// Summary description for Publisher.
    /// </summary>
    public sealed class Publishing
    {

        //		private static readonly string connStr = Config.Shared.database.connectionString;

        private Publishing()
        {
        }

        public static void InsertVersionXml(Guid id, Int32 version, Int32 revision, String versionXml)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                String sql = String.Format("insert into PackageVersion (PackageGUID,PackageVersion,PackageRevision,Xml) values ('{0}',{1},{2},'{3}')", id, version, revision, versionXml);
                fleet.ExecuteNonQuery(sql);
            }
        }

        public static void Publish(Guid id, byte[] packageData, int personId, bool isNewVersion, TemplateDocumentCollection templates, OutcomeCollection outcomes, List<AttachmentBase> attachments, string name, string startPageName, Guid startPageId, string createdBy, string modifiedBy, string publishedBy, DateTime createdTime, DateTime modifiedTime, DateTime publishedTime, string description, string versionXML, out int versionNumber, out int revisionNumber)
        {
            Publish(id, packageData, personId, isNewVersion, templates, outcomes, attachments, name, startPageName, startPageId, createdBy, modifiedBy, publishedBy, createdTime, modifiedTime, publishedTime, description, out versionNumber, out revisionNumber, true, String.Empty);
        }

        public static void Publish(Guid id, byte[] packageData, int personId, bool isNewVersion, TemplateDocumentCollection templates, OutcomeCollection outcomes, List<AttachmentBase> attachments, string name, string startPageName, Guid startPageId, string createdBy, string modifiedBy, string publishedBy, DateTime createdTime, DateTime modifiedTime, DateTime publishedTime, string description, out int versionNumber, out int revisionNumber, bool disposeTemplatesAndOutcomes, String packageVersionXML)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string publishCommandName = "PACKAGE_INSERT";
                string templatesCommandName = "PACKAGE_TEMPLATE_INSERT";
                string outcomesCommandName = "PACKAGE_OUTCOMEACTION_INSERT";
                string attachmentsCommandName = "PACKAGE_ATTACHMENT_INSERT";

                fleet.AddInParameterBoolean("isNewVersion", isNewVersion, publishCommandName);
                fleet.AddInParameterGuid("packageId", id, publishCommandName);
                fleet.AddInParameterString("Name", name, publishCommandName);
                if (startPageName != null && startPageName.Length > 0)
                {
                    fleet.AddInParameterString("startPageName", startPageName, publishCommandName);
                }
                else
                {
                    fleet.AddInParameterString("startPageName", null, publishCommandName);
                }

                if (startPageId != Guid.Empty)
                {
                    fleet.AddInParameterGuid("startPageId", startPageId, publishCommandName);
                }
                else
                {
                    fleet.AddInParameterGuid("startPageId", publishCommandName);
                }

                fleet.AddInParameterString("createdBy", createdBy, publishCommandName);
                fleet.AddInParameterString("modifiedBy", modifiedBy, publishCommandName);
                fleet.AddInParameterString("publishedBy", publishedBy, publishCommandName);
                fleet.AddInParameterDateTime("whenCreated", createdTime, publishCommandName);
                fleet.AddInParameterDateTime("whenModified", modifiedTime, publishCommandName);
                fleet.AddInParameterDateTime("whenPublished", publishedTime, publishCommandName);
                fleet.AddInParameterString("description", description, publishCommandName);
                fleet.AddInParameterBinary("packageBytesNoTemplates", packageData, publishCommandName);
                fleet.AddInParameterString("versionXML", packageVersionXML.Substring(0, Math.Min(packageVersionXML.Length,2000)), publishCommandName);

                fleet.AddOutParameter("newRevisionNumber", DbType.Int32, 4, publishCommandName);
                fleet.AddOutParameter("newVersionNumber", DbType.Int32, 4, publishCommandName);

                fleet.AddInParameterGuid("PackageId", templatesCommandName);
                fleet.AddInParameterInt32("PackageVersionNumber", templatesCommandName);
                fleet.AddInParameterInt32("PackageRevisionNumber", templatesCommandName);
                fleet.AddInParameterString("Name", null, templatesCommandName);
                fleet.AddInParameterGuid("TemplateId", templatesCommandName);
                fleet.AddInParameterBinary("TemplateBytes", null, templatesCommandName);

                fleet.AddInParameterGuid("PackageId", attachmentsCommandName);
                fleet.AddInParameterInt32("PackageVersionNumber", attachmentsCommandName);
                fleet.AddInParameterInt32("PackageRevisionNumber", attachmentsCommandName);
                fleet.AddInParameterString("AttachmentName", null, attachmentsCommandName);
                fleet.AddInParameterGuid("AttachmentId", attachmentsCommandName);
                fleet.AddInParameterBinary("AttachmentBytes", null, attachmentsCommandName);

                fleet.AddInParameterGuid("PackageId",  outcomesCommandName);
                fleet.AddInParameterInt32("PackageVersionNumber", outcomesCommandName);
                fleet.AddInParameterInt32("PackageRevisionNumber",  outcomesCommandName);
                fleet.AddInParameterString("Name", null, outcomesCommandName);
                fleet.AddInParameterGuid("OutcomeId", outcomesCommandName);
                fleet.AddInParameterGuid("OutcomeActionId", outcomesCommandName);
                fleet.AddInParameterBinary("OutcomeActionBytes", null, outcomesCommandName);

                versionNumber = int.MinValue;
                revisionNumber = int.MinValue;

                fleet.BeginTransaction();

                try
                {
                    fleet.ExecuteNonQuerySP(publishCommandName, publishCommandName);

                    int newRevisionNumber = fleet.GetParameterInt32("newRevisionNumber", publishCommandName);
                    int newVersionNumber = fleet.GetParameterInt32("newVersionNumber", publishCommandName);

                    versionNumber = newVersionNumber;
                    revisionNumber = newRevisionNumber;

                    //Store each template, setting it to null as soon as possible.
                    for (int i = templates.Count - 1; i >= 0; --i)
                    {
                        if (templates[i] is WordTemplateDocument2)
                        {
                            WordTemplateDocument2 t = (WordTemplateDocument2)templates[i];

                            fleet.SetParameterValue("PackageId", id, templatesCommandName);
                            fleet.SetParameterValue("PackageVersionNumber", newVersionNumber, templatesCommandName);
                            fleet.SetParameterValue("PackageRevisionNumber", newRevisionNumber, templatesCommandName);
                            fleet.SetParameterValue("Name", t.Name, templatesCommandName);
                            fleet.SetParameterValue("TemplateId", t.UniqueIdentifier, templatesCommandName);
                            fleet.SetParameterValue("TemplateBytes", t.OfficeOpenXMLBytes, templatesCommandName);
                            
                            fleet.ExecuteNonQuerySP(templatesCommandName, templatesCommandName);
                        }

                        if (disposeTemplatesAndOutcomes)
                        {
                            TemplateDocument d = templates[i];
                            templates.RemoveAt(i);
                            d = null;
                        }
                    }


                    //Store each attachment, setting it to null as soon as possible.
                    for (int i = attachments.Count - 1; i >= 0; --i)
                    {
                        if (attachments[i] is AttachmentStatic)
                        {
                            AttachmentStatic a = (AttachmentStatic)attachments[i];

                            fleet.SetParameterValue("PackageId", id, attachmentsCommandName);
                            fleet.SetParameterValue("PackageVersionNumber", newVersionNumber, attachmentsCommandName);
                            fleet.SetParameterValue("PackageRevisionNumber", newRevisionNumber, attachmentsCommandName);
                            fleet.SetParameterValue("AttachmentName", a.Name, attachmentsCommandName);
                            fleet.SetParameterValue("AttachmentId", a.UniqueIdentifier, attachmentsCommandName);
                            // FB2328: Changed class to consistently have a null payload value if content length is zero. Required for serialisation. 
                            // However the DB can not have a null value hence we need to ensure this.
                            fleet.SetParameterValue("AttachmentBytes", a.GetPayload() == null ? new byte[0] : a.GetPayload( ), attachmentsCommandName);

                            fleet.ExecuteNonQuerySP(attachmentsCommandName, attachmentsCommandName);
                        }

                        if (disposeTemplatesAndOutcomes)
                        {
                            AttachmentBase a = attachments[i];
                            attachments.RemoveAt(i);
                            a= null;
                        }
                    }


                    // Store each outcome, setting all actions' WordML to null as soon as possible.
                    for (int i = outcomes.Count - 1; i >= 0; i--)
                    {
                        Outcome o = outcomes[i];
                        foreach (ActionBase ab in o.Definition.AllPossibleActions)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                if (ab is OutcomeAction)
                                {
                                    BinaryFormatter bf = new BinaryFormatter();
                                    bf.Serialize(ms, ab);
                                    byte[] outcomeActionBytes = ms.ToArray();

                                    fleet.SetParameterValue("PackageId", id, outcomesCommandName);
                                    fleet.SetParameterValue("PackageVersionNumber", newVersionNumber, outcomesCommandName);
                                    fleet.SetParameterValue("PackageRevisionNumber", newRevisionNumber, outcomesCommandName);
                                    fleet.SetParameterValue("Name", o.Name, outcomesCommandName);
                                    fleet.SetParameterValue("OutcomeId", o.UniqueIdentifier, outcomesCommandName);
                                    fleet.SetParameterValue("OutcomeActionId", ab.UniqueIdentifier, outcomesCommandName);
                                    fleet.SetParameterValue("OutcomeActionBytes", outcomeActionBytes, outcomesCommandName);

                                    fleet.ExecuteNonQuerySP(outcomesCommandName, outcomesCommandName);

                                    if (disposeTemplatesAndOutcomes)
                                    {
                                        ((OutcomeAction)ab).OfficeOpenXML = null;
                                    }
                                }
                            }
                        }
                    }
                    fleet.Commit();
                }
                catch (Exception ex)
                {
                    fleet.Rollback();
                    throw;
                }
            }
        }
    }
}

