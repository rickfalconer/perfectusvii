using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.DataAccess;
using Perfectus.Library.Logging;
using Perfectus.Server.Common.Data;
using System.Data;
using Perfectus.Library.Localisation;
using Perfectus.Library.Exception;
using System.Reflection;

namespace Perfectus.Server.DataAccess.DatabaseJobQueue
{
    /// <summary>
    /// Class to craete, read and update Jobs in the database
    /// The job database is a full replacement for jobs in the MSMQ
    /// </summary>
    public class DatabaseJobQueue
    {
        const int DEFAULT_PRIORITY = 100;

        // Time to wait until the last status change of a job
        // before it shall be rescheduled
        const int DEFAULT_DELAY = 30;
        static private int _delayTillReRead = DEFAULT_DELAY;

        #region public static methods
        public static void SetConfig(int delayedReadInSeconds)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            if (delayedReadInSeconds <= 0)
                delayedReadInSeconds = 0;

            if (delayedReadInSeconds >= 1440 * 60)
                delayedReadInSeconds = 1440 * 60;

            // time in seconds before a failed job should be picked again
            _delayTillReRead = delayedReadInSeconds;
            Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("Setting delay to {0} seconds", _delayTillReRead));
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        /// <summary>
        /// Create a new job
        /// Used at the end of an interview or by the fileaccepter etc.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public static bool Create(Job job)
        {
            return Create(job, DEFAULT_PRIORITY);
        }
        public static bool Create(Job job, int priority)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);
            bool success = true;

            if (priority > 500)
                priority = 500;
            else if (priority <= 1)
                priority = 1;
            using (IFleet fleet = FleetContainer.Create())
            {
                try
                {
                    fleet.timeout = 60; // 60 seconds timeout

                    // Status := READY
                    fleet.AddInParameterString("@Status", JobStatus.QUEUE.ToString());
                    fleet.AddInParameterGuid("@PackageInstanceId", job.InstanceId);
                    fleet.AddInParameterString("@TaskSubmittedBy", job.TaskSubmittedBy);
                    fleet.AddInParameterGuid("@PageToProcess", job.PageToProcessId);
                    fleet.AddInParameterInt32("@Priority", priority);

                    if ( job.SourceName.Length > 0 )
                        fleet.AddInParameterString("@PageName", job.SourceName.Substring(0, Math.Min(job.SourceName.Length, 50)));

                    // Create one job record
                    fleet.ExecuteNonQuerySP("Job_Create");
                    Log.Trace( Log.Category.DataAccess, Log.Priority.LOW, String.Format( "Created job '{0}'", job.Label ) );
                }
                catch (Exception ex)
                {
                    Log.Error( Log.Category.DataAccess, ex, new ID( "ProcessCoordinator.CreatingJobFailed" ), job.Label );
                    success = false;
                }

                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                return success;
            }
        }
        /// <summary>
        /// Peek available jobs in the database
        /// (note that this call doesn't timeout, if another reader selected with 
        ///  HOLDLOCK, but hansn't made any modifications yet).
        /// </summary>
        /// <returns>Job available</returns>
        public static int Peek()
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);
            int nrJobs = 0;

            using (IFleet fleet = FleetContainer.Create())
            {
                //fleet.timeout = 2; // in seconds
                //pf-3136
                fleet.timeout = 10; // in seconds
                try
                {
                    fleet.AddInParameterInt32("@Delay", _delayTillReRead);
                    fleet.AddOutParameter("AvailableJobs", DbType.Int32, 4);
                    fleet.ExecuteNonQuerySP("Job_peek");

                    nrJobs = fleet.GetParameterInt32("AvailableJobs");

                    if (nrJobs > 0)
                    {
                        Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("Nr. of jobs available := {0}", nrJobs));
                    }
                    else
                        Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, "No jobs found");
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    if (!ex.Message.Contains(new ID("ProcessCoordinator.TIMEOUT_MESSAGE_PART").ToString()))
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.PeekFromDatabaseFailed"));
                    else
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.PeekFromDatabaseTimeout"), fleet.timeout.ToString());
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("ORA-01013"))
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.PeekFromDatabaseTimeout"), fleet.timeout.ToString());
                    else
                        // don't want to shout this error, as next peek might succeed 
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.PeekFromDatabaseFailed"));
                }
                Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
                return nrJobs;
            }
        }

        /// <summary>
        /// Read - static public
        /// </summary>
        /// <param name="callerId"></param>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public static Job Read(String callerId, out Object messageId)
        {
            return ReadTaskAnyAvailable(callerId, out messageId);
        }
        public static void Complete(Object messageId)
        {
            UpdateStatus(messageId, JobStatus.COMPLETE);
        }
        public static void Fail(object messageId)
        {
            UpdateStatus(messageId, JobStatus.ERROR);
        }
        public static void Queue(object messageId)
        {
            UpdateStatus(messageId, JobStatus.QUEUE);
        }
        public static void Distribute(object messageId)
        {
            UpdateStatus(messageId, JobStatus.DISTRIBUTE);
        }
        public static void Assemble(object messageId)
        {
            UpdateStatus(messageId, JobStatus.ASSEMBLE);
        }
        public static void Convert(object messageId)
        {
            UpdateStatus(messageId, JobStatus.CONVERT);
        }
        public static Job ReadNoWait(object messageId)
        {
            return ReadTaskNoneBlocking(messageId);
        }

        /// <summary>
        /// Resets the retry counter for all failed jobs in the queue belonging to the package id.
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        public static int ReTry( Guid packageId )
        {
            Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.In );

            int nrJobs = 0;

            using( IFleet fleet = FleetContainer.Create( ) )
            {
                try
                {
                    fleet.AddInParameterGuid( "@PackageInstanceId", packageId );
                    fleet.AddOutParameter( "@JobReactivated", DbType.Int32, 4 );
                    fleet.ExecuteNonQuerySP( "PACKAGEINSTANCE_RESUBMIT" );

                    nrJobs = fleet.GetParameterInt32( "@JobReactivated" );

                    if( nrJobs > 0 )
                        Log.Trace( Log.Category.DataAccess, Log.Priority.HIGH, String.Format( "Retry Jobs = {0}", nrJobs ) );
                }
                catch( Exception ex )
                {
                    throw new PerfectusException( Log.Category.DataAccess, new ID( "ProcessCoordinator.RetryFromDatabaseFailed" ), packageId.ToString( ), ex.Message );
                }
                Log.Trace( Log.Category.DataAccess, MethodBase.GetCurrentMethod( ), Log.InOut.Out );
                
                return nrJobs;
            }
        }
        #endregion

        #region private  static methods
        /// <summary>
        /// Read job using the unique job identifier
        /// </summary>
        /// <param name="messageId">unique id</param>
        /// <returns>job</returns>
        private static Job ReadTaskByMessageID(Object messageId)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            Job job = null;
            try
            {
                // We assume that we are ruinning in a transactional context.
                // This read shall be used just prior an update operation
                IFleet fleet = FleetContainer.FindFleet(messageId.ToString());
                fleet.AddInParameterInt32("@Id", (int)messageId);
                fleet.AddCursorOutParameter("rc1");

                // This call will cause blocking
                using (IDataReader rdr = fleet.ExecuteReaderSP("Job_Read"))
                    if (rdr.Read())
                        job = GetJobByReader(rdr);
                Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("Reading id {0} with job '{1}'", messageId, job.ToString()));
            }
            catch (Exception ex)
            {
                job = null;
                Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingFromDatabaseFailed"));
                throw ex;
            }
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);

            return job;
        }



        /// <summary>
        /// Dirty read of SQL
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns>job</returns>
        private static Job ReadTaskNoneBlocking(Object messageId)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            Job job = null;
            using (IFleet fleet = FleetContainer.Create())
            {
                try
                {
                    fleet.AddInParameterInt32("@Id", (int)messageId);
                    fleet.AddCursorOutParameter("rc1");

                    using (IDataReader rdr = fleet.ExecuteReaderSP("Job_ReadNoneBlocking"))
                        if (rdr.Read())
                            job = GetJobByReader(rdr);
                    Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("None blocking read of id {0} returns '{1}'", messageId, job.ToString()));
                }
                catch (Exception ex)
                {
                    job = null;
                    Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingDirtyFromDatabaseFailed"));
                }
            }
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return job;
        }

        /// <summary>
        /// Get job from database reader
        /// </summary>
        private static Job GetJobByReader(IDataReader rdr)
        {
            Job job = new Job(
                rdr.GetGuid(0),                         // PackageId
                rdr.GetGuid(3),                         // PackageInstanceId
                System.Convert.ToInt32(rdr.GetValue(1)),// Version
                System.Convert.ToInt32(rdr.GetValue(2)),// Revision
                rdr.IsDBNull(4) ?                       //Submitted by
                                String.Empty :
                                rdr.GetString(4),
                rdr.IsDBNull(5) ?                       // Page to process
                                Guid.Empty :
                                rdr.GetGuid(5));

            job.Retry = System.Convert.ToInt32(rdr.GetValue(6)); // Retry
            return job;
        }

        /// <summary>
        /// Read any available job.
        /// On success set the field AssignedProcess in the database to the 
        /// identifier of the coordinator and return the unique key
        /// 
        /// The method might not return data, if another coordinator picked up the 
        /// job earlier.
        /// 
        /// The method might fail, which should not be critical for the overall coordinator
        /// (e.g. deadlock on update)
        /// </summary>
        /// <param name="callerId">identifier of the coordinator</param>
        /// <param name="messageId">unique key</param>
        /// <returns></returns>
        private static Job ReadTaskAnyAvailable(String callerId, out Object messageId)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            Job job = null;
            messageId = null;
            using (IFleet fleet = FleetContainer.Create())
            {
                fleet.BeginTransaction();
                try
                {
                    fleet.timeout = 5;          // wait max. 5 seconds
                    fleet.AddInParameterInt32("@Delay", _delayTillReRead);
                    fleet.AddInParameterString("@AssignedProcess", callerId);
                    fleet.AddCursorOutParameter("rc1");

                    using( IDataReader rdr = fleet.ExecuteReaderSP( "Job_ReadAny" ) )
                    {
                        if( rdr.Read( ) )
                        {
                            job = GetJobByReader( rdr );

                            // ID columns under Oracle are of type Int64,
                            // hence do not use GetIn32(...)
                            messageId = System.Convert.ToInt32( rdr.GetValue( 7 ) );
                            rdr.Close( );

                            Log.Trace( Log.Category.DataAccess, Log.Priority.HIGH, String.Format( "Coordinator '{0}' is reading record {1} with task '{2}'", callerId, messageId, job.ToString( ) ) );

                            try
                            {
                                fleet.Commit( );
                            }
                            catch( Exception ex )
                            {
                                Log.Trace( Log.Category.DataAccess, Log.Priority.LOW, String.Format( "ReadTaskAnyAvailable - Commit failed {0} / {0}", job.ToString( ), ex.Message ) );
                                job = null;
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    try
                    {
                        fleet.Rollback();
                    }
                    catch (Exception ex2){
                        Log.Trace(Log.Category.DataAccess, Log.Priority.LOW, String.Format("ReadTaskAnyAvailable - Rollback failed {0}", ex2.Message));
                    }
                    job = null;

                    if (!ex.Message.Contains(new ID("ProcessCoordinator.TIMEOUT_MESSAGE_PART").ToString()))
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingFromDatabaseFailed"));
                    else
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.ReadingFromDatabaseTimeout"), fleet.timeout.ToString());
                }
                catch (Exception ex)
                {
                    try
                    {
                        fleet.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Log.Trace(Log.Category.DataAccess, Log.Priority.LOW, String.Format("ReadTaskAnyAvailable - Rollback failed {0}", ex2.Message));
                    }
                    job = null;

                    if (ex.Message.Contains("ORA-01013"))
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.ReadingFromDatabaseTimeout"), fleet.timeout.ToString());
                    else
                        // don't want to shout this error, as next peek might succeed 
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingFromDatabaseFailed"));
                }

            }
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return job;
        }

        /// <summary>
        /// Set status of job, maybe increase retry counter
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="status"></param>
        private static void UpdateStatus(Object messageId, JobStatus status)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            using (IFleet fleet = FleetContainer.Create(messageId.ToString()))
            {
                Job job = null;
                try
                {
                    fleet.BeginTransaction();
                    fleet.timeout = 120; // 2 minutes

                    job = ReadTaskByMessageID(messageId);

                    switch (status)
                    {
                        case JobStatus.QUEUE:
                            job.IncrementRetry();
                            break;
                    }
                    // need a new command, as default was used for reader
                    fleet.AddInParameterString("@Status", status.ToString(), "NewCMD");
                    fleet.AddInParameterInt32("@Retry", job.Retry, "NewCMD");
                    fleet.AddInParameterInt32("@ID", (int)messageId, "NewCMD");

                    fleet.ExecuteNonQuerySP("Job_Update", "NewCMD");

                    fleet.Commit();
                    Log.Trace(Log.Category.DataAccess, Log.Priority.LOW, String.Format(String.Format("Updating status '{0}' Id {1} for job '{2}'",
                        status.ToString(), messageId, job.Label)));
                }
                catch (Exception ex)
                {
                    Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.UpdateStatusDatabaseFailed"),
                        messageId.ToString(), status.ToString(), job != null ? job.Label : "job data unavailable");
                }
                Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            }
        }
        #endregion

        #region Distributor Queue
        /// <summary>
        /// Set status of job, maybe increase retry counter
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="status"></param>
        public static int DistributionPeek(int jobId)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);
            int nrJobs = 0;

            using (IFleet fleet = FleetContainer.Create())
            {
                fleet.timeout = 2; // in seconds
                try
                {
                    fleet.AddInParameterInt32("@JobQueueId", jobId);
                    fleet.AddOutParameter("AvailableDistribution", DbType.Int32, 4);
                    fleet.ExecuteNonQuerySP("Distribute_Peek");

                    nrJobs = fleet.GetParameterInt32("AvailableDistribution");

                    if (nrJobs > 0)
                    {
                        Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("Nr. of distribution available := {0}", nrJobs));
                    }
                    else
                        Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, "No distribution found");
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    if (!ex.Message.Contains(new ID("ProcessCoordinator.TIMEOUT_MESSAGE_PART").ToString()))
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.PeekFromDatabaseFailed"));
                    else
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.PeekFromDatabaseTimeout"), fleet.timeout.ToString());
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("ORA-01013"))
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.PeekFromDatabaseTimeout"), fleet.timeout.ToString());
                    else
                        // don't want to shout this error, as next peek might succeed 
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.PeekFromDatabaseFailed"));
                }
                Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
                return nrJobs;
            }
        }

        public struct DistributorDocumentPair
        {
            internal DistributorDocumentPair(Guid param_distributorGuid, Guid param_documentGuid)
            {
                distributorGuid=param_distributorGuid;
                documentGuid = param_documentGuid;
            }
            public Guid distributorGuid;
            public Guid documentGuid;
        }
        
        // need to modify once new delivery manager is defined
        public static List<DistributorDocumentPair> DistributionReadNoneBlocking(int jobQueueId)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            List<DistributorDocumentPair> distributions = new List<DistributorDocumentPair>();
            using (IFleet fleet = FleetContainer.Create())
            {
                try
                {
                    fleet.AddInParameterInt32("@JobQueueId", (int)jobQueueId);
                    fleet.AddCursorOutParameter("rc1");

                    using (IDataReader rdr = fleet.ExecuteReaderSP("Distribute_ReadNoneBlocking"))
                        while (rdr.Read())
                        {
                            distributions.Add(new DistributorDocumentPair ( rdr.GetGuid(2), rdr.GetGuid(1) ));
                        }
                    Log.Trace(Log.Category.DataAccess, Log.Priority.HIGH, String.Format("None blocking read of JobQueueId {0}", jobQueueId));
                }
                catch (Exception ex)
                {
                    distributions = null;
                    Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingDirtyFromDatabaseFailed"));
                }
            }
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return distributions;
        }

        public static void DistributionCreate(int jobQueueId, Guid documentGuid, Guid distributorGuid, String documentName, String distributorName)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);
            using (IFleet fleet = FleetContainer.Create())
            {
                try
                {
                    fleet.timeout = 60; // 60 seconds timeout

                    // Status := READY
                    fleet.AddInParameterInt32("@JobQueueId", jobQueueId);
                    fleet.AddInParameterGuid("@DocumentGuid", documentGuid);
                    fleet.AddInParameterGuid("@DistributorGuid", distributorGuid);

                    if (documentName != null && documentName.Length > 0)
                        fleet.AddInParameterString("@DocumentName", documentName);
                    if (distributorName != null && distributorName.Length > 0)
                        fleet.AddInParameterString("@DistributorName", distributorName);

                    // Create one job record
                    fleet.ExecuteNonQuerySP("Distribute_Create");
                    Log.Trace(Log.Category.DataAccess, Log.Priority.LOW, String.Format("Created distribution '{0}' {1} {2}", jobQueueId, documentGuid, distributorGuid));
                }
                catch (Exception ex)
                {
                    Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.CreatingJobFailed"), jobQueueId);
                }

                Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            }
        }


        public static void DistributionRemove(int jobQueueId, Guid distributor)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);
            using (IFleet fleet = FleetContainer.Create())
            {
                try
                {
                    fleet.timeout = 60; // 60 seconds timeout

                    fleet.AddInParameterInt32("@JobQueueId", jobQueueId);
                    fleet.AddInParameterGuid("@DistributorGuid", distributor);
                    
                    // Create one job record
                    fleet.ExecuteNonQuerySP("Distribute_Remove");
                    Log.Trace(Log.Category.DataAccess, Log.Priority.LOW, String.Format("Deleted distribution '{0}' {1}", jobQueueId, distributor));
                }
                catch (Exception ex)
                {
                    Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.CreatingJobFailed"), jobQueueId);
                }

                Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            }
        }
        #endregion

        

        #region Test Code
        public static bool PerformConnectivityCheck(out string reason)
        {
            try
            {
                reason = null;
                using (IFleet fleet = FleetContainer.Create())
                {
                    fleet.ExecuteScalar ("select SequenceNumber from VersionInformation");
                    return true;
                }
            }
            catch (Exception ex)
            {
                reason = ex.Message;
                return false;
            }
        }
        #endregion

        public static void ResetOverdueJobs(int timeoutInSecond)
        {
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.In);

            using (IFleet fleet = FleetContainer.Create())
            {
                fleet.BeginTransaction();
                try
                {
                    fleet.timeout = 30;          // wait max. 5 seconds
                    fleet.AddInParameterInt32("@MaxDwellTime", timeoutInSecond * 3);
                    fleet.AddCursorOutParameter("rc1");

                    using (IDataReader rdr = fleet.ExecuteReaderSP("Job_ResetOverdue"))
                    {
                        while (rdr.Read())
                        {
                            Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.OrphantJobFound"),
                                System.Convert.ToInt32(rdr.GetValue(0)),
                                rdr.GetString(1),
                                rdr.GetValue(2).ToString(),
                                rdr.GetGuid(3).ToString());
                        }
                        rdr.Close();
                    }
                    fleet.Commit();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    if (!ex.Message.Contains(new ID("ProcessCoordinator.TIMEOUT_MESSAGE_PART").ToString()))
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingFromDatabaseFailed"));
                    else
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.ReadingFromDatabaseTimeout"), fleet.timeout.ToString());
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("ORA-01013"))
                        Log.Warn(Log.Category.DataAccess, new ID("ProcessCoordinator.ReadingFromDatabaseTimeout"), fleet.timeout.ToString());
                    else
                        // don't want to shout this error, as next peek might succeed 
                        Log.Error(Log.Category.DataAccess, ex, new ID("ProcessCoordinator.ReadingFromDatabaseFailed"));
                }
            }
            Log.Trace(Log.Category.DataAccess, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }
    }
}
