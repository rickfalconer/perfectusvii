using System;
using System.Data;
using System.Data.Common;
using Perfectus.DataAccess;

namespace Perfectus.Server.DataAccess
{
	/// <summary>
	/// Summary description for Logging.
	/// </summary>
	public class Logging
	{
		//private static string ConnectionString = Config.Shared.database.connectionString;

		private Logging()
		{
		}

        public static int LogException(
            DateTime whenLoggedUtc, string severityCode, int personId, Guid relatedPackageId, int relatedPackageVersionNumber,
            int relatedPackageRevisionNumber, Guid relatedPackageInstanceId, string friendlyMessage, string message, string stackTrace, string raisingAssemblyName,
            string raisingAssemblyPath, string raisingAssemblyVersion, int parentExceptionId)
        {
            return LogException(
            whenLoggedUtc, severityCode, personId, relatedPackageId, relatedPackageVersionNumber,
            relatedPackageRevisionNumber, relatedPackageInstanceId, friendlyMessage, message, stackTrace, raisingAssemblyName,
            raisingAssemblyPath, raisingAssemblyVersion, parentExceptionId, -1, null, null, null);
        }

        public static int LogException(
            DateTime whenLoggedUtc, string severityCode, int personId, Guid relatedPackageId, int relatedPackageVersionNumber,
            int relatedPackageRevisionNumber, Guid relatedPackageInstanceId, string friendlyMessage, string message, string stackTrace, string raisingAssemblyName,
            string raisingAssemblyPath, string raisingAssemblyVersion, int parentExceptionId,
            int jobqueueid, String errorNumber, String hint, String category)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "EXCEPTION_INSERT";

                fleet.AddInParameterString("SeverityCode", severityCode);
                fleet.AddInParameterDateTime("WhenLoggedUtc", whenLoggedUtc);
                if (personId > int.MinValue)
                    fleet.AddInParameterInt32("PersonId", personId);
                else
                    fleet.AddInParameterInt32("PersonId");

                if (relatedPackageId != Guid.Empty)
                    fleet.AddInParameterGuid("RelatedPackageId", relatedPackageId);
                else
                    fleet.AddInParameterGuid("RelatedPackageId");

                if (relatedPackageVersionNumber > int.MinValue)
                    fleet.AddInParameterInt32("RelatedPackageVersionNumber", relatedPackageVersionNumber);
                else
                    fleet.AddInParameterInt32("RelatedPackageVersionNumber");

                if (relatedPackageRevisionNumber > int.MinValue)
                    fleet.AddInParameterInt32("RelatedPackageRevisionNumber", relatedPackageRevisionNumber);
                else
                    fleet.AddInParameterInt32("RelatedPackageRevisionNumber");

                if (relatedPackageInstanceId != Guid.Empty)
                    fleet.AddInParameterGuid("RelatedPackageInstanceId", relatedPackageInstanceId);
                else
                    fleet.AddInParameterGuid("RelatedPackageInstanceId");

                if (friendlyMessage != null)
                    fleet.AddInParameterString("FriendlyMessage", friendlyMessage.Replace( "\r\n", " " ) );
                else
                    fleet.AddInParameterString("FriendlyMessage");

                if (message != null)
                    fleet.AddInParameterString("message", message);
                else
                    fleet.AddInParameterString("message");

                if (stackTrace != null)
                    fleet.AddInParameterString("StackTrace", stackTrace);
                else fleet.AddInParameterString("StackTrace");

                if (raisingAssemblyName != null)
                    fleet.AddInParameterString("RaisingAssemblyName", raisingAssemblyName);
                else
                    fleet.AddInParameterString("RaisingAssemblyName");

                if (raisingAssemblyPath != null)
                    fleet.AddInParameterString("RaisingAssemblyPath", raisingAssemblyPath);
                else
                    fleet.AddInParameterString("RaisingAssemblyPath");

                if (raisingAssemblyVersion != null)
                    fleet.AddInParameterString("RaisingAssemblyVersion", raisingAssemblyVersion);
                else fleet.AddInParameterString("RaisingAssemblyVersion");

                if (parentExceptionId > int.MinValue)
                    fleet.AddInParameterInt32("ParentExceptionId", parentExceptionId);
                else
                    fleet.AddInParameterInt32("ParentExceptionId");

                if (jobqueueid != -1)
                    fleet.AddInParameterInt32("JOBQUEUEID", jobqueueid);
                else
                    fleet.AddInParameterInt32("JOBQUEUEID");

                if (errorNumber != null)
                    fleet.AddInParameterString("ERRORNUMBER", errorNumber);
                else
                    fleet.AddInParameterString("ERRORNUMBER");

                if (hint != null)
                    fleet.AddInParameterString("HINT", hint);
                else
                    fleet.AddInParameterString("HINT");

                if (category != null)
                    fleet.AddInParameterString("CATEGORY", category);
                else
                    fleet.AddInParameterString("CATEGORY");

                fleet.AddOutParameter("ExceptionId", DbType.Int32, 4);

                fleet.ExecuteNonQuerySP(commandName);

                return fleet.GetParameterInt32("ExceptionId");
            }
        }

/*		public static ExceptionLogSet ExceptionLog_GetByPackageInstanceId(int PackageInstanceId)
		{
			ExceptionLogSet els = new ExceptionLogSet();
			SqlHelper.FillDataset(ConnectionString,"ExceptionLog_GetByPackageInstanceId",els,new string[]{"ExceptionLog", "SeverityCode"}, PackageInstanceId);
			return els;
		}*/
	}
}
