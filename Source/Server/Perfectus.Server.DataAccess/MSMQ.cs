using System.Messaging;

namespace Perfectus.Server.DataAccess
{
	public sealed class MSMQ
	{
		private MSMQ()
		{
		}

		public static MessageQueue Initialise(string machineName, string queueName, bool isPrivate)
		{
			string queuePath;
			MessageQueue queue;
			// Construct the queue path
			if (isPrivate)
			{
				queuePath = string.Format(@"{0}\Private$\{1}", machineName, queueName);
			} 
			else
			{
				queuePath = string.Format(@"{0}\{1}", machineName, queueName);
			}


			// Ensure the queue exists.  Create it if necessary
            try
            {
                if (!MessageQueue.Exists(queuePath))
                {
                    queue = MessageQueue.Create(queuePath);
                    queue.MaximumQueueSize = MessageQueue.InfiniteQueueSize;
                }
                else
                {
                    queue = new MessageQueue(queuePath, false);
                }
            }
            catch 
            {
                // Assume the queue exists and enqueue (ignore the active directory error - try again (FB-1224)
                queue = new MessageQueue(queuePath, false);
            }

			queue.Formatter = new BinaryMessageFormatter();

			return queue;
		}
	}
}
