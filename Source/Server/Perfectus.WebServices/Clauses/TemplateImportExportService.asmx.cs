using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
//using Microsoft.Web.Services2;
//using Microsoft.Web.Services2.Dime;
using System.Net;
using System.IO;
//TODO - commented out due to compile port errors
//using Perfectus.Sharepoint;

namespace Perfectus.WebServices.Sharepoint.Services
{
    /// <summary>
    /// Summary description for Service1.
    /// </summary>
    public class TemplateImportExportService : System.Web.Services.WebService
    {
        /*		#region Constructor
                /// <summary>
                /// Creates a new instance of the webservice.
                /// </summary>		
                public TemplateImportExportService()
                {	
			
                }

                #endregion
		
                #region Public web methods
		
                /// <summary>
                /// Gets a file from sharepoint
                /// </summary>
                /// <param name="relativePath">The relative sharepoint path of the file to get</param>
                [WebMethod]		
                public void GetFileFromSP(string relativeFilePath) 
                {
                    SoapContext ctx = ResponseSoapContext.Current;
		
                    if (ctx == null)
                    {
                        throw new DimeFormatException("No context.");
                    }
                    else
                    {
                        DocumentLibrary docLib = GetDocumentLibrary();

                        string fileName = null;
                        MemoryStream fileStream = DocumentSharing.GetFile(relativeFilePath, docLib, out fileName);
				
                        // Set up the stream into a DimeAttachment		
                        DimeAttachment dimeAttach = new DimeAttachment("application/msword", TypeFormat.MediaType, fileStream);

                        // Might be nice to know for the caller of the web method to know this.			
                        dimeAttach.Id = fileName;

                        // Add the attachment.			
                        ctx.Attachments.Add(dimeAttach);
                    }
                }	

                /// <summary>
                /// Saves a file to sharepoint.
                /// </summary>
                /// <param name="Id">The Id of the DIME attachment to save</param>
                /// <param name="relativePath">The relative sharepoint path to save the file to</param>
                [WebMethod]
                public void SaveFileToSP(string id, string relFilePath) 
                {
                    SoapContext ctx = RequestSoapContext.Current;
			
                    if (ctx == null)
                    {
                        throw new DimeFormatException("No context.");
                    }
                    else
                    {
                        if (ctx.Attachments[id] == null)
                        {
                            throw new DimeFormatException(string.Format("Attachment: {0} not found.", id));
                        }
                        else
                        {
                            DimeAttachment attachment = (DimeAttachment) ctx.Attachments[id];	
                            DocumentLibrary docLib = GetDocumentLibrary();
                            DocumentSharing.SaveFile(relFilePath, docLib, attachment.Stream);
                        }
                    }					
                }	
		
                /// <summary>
                /// Gets the full path to Document Library the service will use for Sharepoint operations.
                /// </summary>
                /// <returns>A string with the full path to Document Library the service will use for Sharepoint operations.</returns>
                [WebMethod]
                public string GetFullPath() 
                {	
                    DocumentLibrary docLib = GetDocumentLibrary();
                    return docLib.FullPath;			
                }

                /// Gets the name of the Network Map that should be set up on all web service consumers.
                /// </summary>
                /// <returns>A string with the the name of the Network Map that should be set up on all web service consumers.</returns>
                [WebMethod]
                public string GetNetworkMapName() 
                {			
                    return this.GetNetworkMap();			
                }
                /// <summary>
                /// Gets the name of the Sharepoint Document Library that the service will use for Sharepoint operations.
                /// </summary>	
                [WebMethod]
                public string GetDocumentRepositoryName()
                {
                    return this.GetDocRepositoryName();
                }

                /// <summary>
                /// Gets the name of the Sharepoint site that the service will use for Sharepoint operations.
                /// </summary>
                [WebMethod]
                public string GetSiteName()
                {
                    return this.GetFullSiteName();
                }

                #endregion

                #region Site information methods		
	
                /// <summary>
                /// Gets the address of the Sharepoint server that the service will use for Sharepoint operations.
                /// </summary>		
                private string GetServerAddress()
                {
                    return System.Configuration.ConfigurationSettings.AppSettings["templateSharepointServer"];
                }		

                /// <summary>
                /// Gets the name of the Sharepoint site that the service will use for Sharepoint operations.
                /// </summary>	
                private string GetFullSiteName()
                {
                    return System.Configuration.ConfigurationSettings.AppSettings["templateSharepointSite"];
                }
		
                /// <summary>
                /// Gets the name of the Network Map that should be set up on all web service consumers.
                /// </summary>
                private string GetNetworkMap()
                {
                    return System.Configuration.ConfigurationSettings.AppSettings["templateSharepointNetworkMapName"];
                }

                /// <summary>
                /// Gets the name of the Sharepoint Document Library that the service will use for Sharepoint operations.
                /// </summary>	
                private string GetDocRepositoryName()
                {
                    return System.Configuration.ConfigurationSettings.AppSettings["templateSharepointDocRepository"];
                }

                /// <summary>
                /// Gets an instance of a DocumentLibrary class based on the services server, site and document library information.
                /// </summary>
                private DocumentLibrary GetDocumentLibrary()
                {
                    DocumentLibrary docLib = new DocumentLibrary();

                    docLib.ServerAddress = this.GetServerAddress();
                    docLib.SiteName = this.GetSiteName();
                    docLib.DocumentRepositoryName = this.GetDocRepositoryName();

                    return docLib;
                }

                #endregion
            */
    }
}