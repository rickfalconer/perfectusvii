<?xml version="1.0"?>
<configuration>
	<configSections>
		<section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=3.1.0.0, Culture=neutral" />
		<section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=3.1.0.0, Culture=neutral" />
		<section name="repositoryConfiguration" type="Perfectus.Common.SharedLibrary.Configuration.RepositoryConfiguration, Perfectus.Common"/>
		<section name="microsoft.web.services3" type="Microsoft.Web.Services3.Configuration.WebServicesConfiguration, Microsoft.Web.Services3, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
	</configSections>
	<!-- Exception Handling Configuration -->
	<exceptionHandling>
		<exceptionPolicies>
			<add name="Critical Priority Exception">
				<exceptionTypes>
					<add name="Exception" type="System.Exception, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" postHandlingAction="NotifyRethrow">
						<exceptionHandlers>
							<add logCategory="EventLog" eventId="100" severity="Error" title="Shared Library Exception" priority="1" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" name="Logging Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging" />
						</exceptionHandlers>
					</add>
				</exceptionTypes>
			</add>
			<add name="Noncritical Priority Exception">
				<exceptionTypes>
					<add name="Exception" type="System.Exception, mscorlib" postHandlingAction="NotifyRethrow">
						<exceptionHandlers>
							<add logCategory="EventLog" eventId="101" severity="Error" title="Shared Library Exception" priority="2" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" name="Logging Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging" />
						</exceptionHandlers>
					</add>
				</exceptionTypes>
			</add>
			<add name="Low Priority Exception">
				<exceptionTypes>
					<add name="Exception" type="System.Exception, mscorlib" postHandlingAction="None">
						<exceptionHandlers>
							<add logCategory="EventLog" eventId="102" severity="Error" title="Shared Library Exception" priority="3" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" name="Logging Handler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging" />
						</exceptionHandlers>
					</add>
				</exceptionTypes>
			</add>
		</exceptionPolicies>
	</exceptionHandling>
	<!-- Logging Configuration -->
	<loggingConfiguration defaultCategory="EventLog" tracingEnabled="false">
		<logFilters>
			<add name="Category" type="Microsoft.Practices.EnterpriseLibrary.Logging.Filters.CategoryFilter, Microsoft.Practices.EnterpriseLibrary.Logging" categoryFilterMode="AllowAllExceptDenied">
				<categoryFilters />
			</add>
			<add name="Priority" type="Microsoft.Practices.EnterpriseLibrary.Logging.Filters.PriorityFilter, Microsoft.Practices.EnterpriseLibrary.Logging" minimumPriority="0" />
		</logFilters>
		<categorySources>
			<add name="EventLog" switchValue="All">
				<listeners>
					<add name="Event Log Destination" />
				</listeners>
			</add>
			<add name="Tracing" switchValue="All">
				<listeners>
					<add name="Flat File Destination" />
				</listeners>
			</add>
		</categorySources>
		<specialSources>
			<errors name="errors" switchValue="All">
				<listeners>
					<add name="Event Log Destination" />
				</listeners>
			</errors>
		</specialSources>
		<listeners>
			<add name="Event Log Destination" source="Perfectus Unit Testing" formatter="Default Formatter" log="Perfectus"
				 type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" />
			<add name="Flat File Destination" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" fileName="C:\trace.log" />
		</listeners>
		<formatters>
			<add name="Default Formatter" type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="Timestamp: {timestamp}&#xD;&#xA;Message: {message}&#xD;&#xA;Category: {category}&#xD;&#xA;Priority: {priority}&#xD;&#xA;EventId: {eventid}&#xD;&#xA;Severity: {severity}&#xD;&#xA;Title:{title}&#xD;&#xA;Machine: {machine}&#xD;&#xA;Application Domain: {appDomain}&#xD;&#xA;Process Id: {processId}&#xD;&#xA;Process Name: {processName}&#xD;&#xA;Win32 Thread Id: {win32ThreadId}&#xD;&#xA;Thread Name: {threadName}&#xD;&#xA;Extended Properties: {dictionary({key} - {value}&#xD;&#xA;)}" />
		</formatters>
	</loggingConfiguration>
	<system.web>
		<compilation defaultLanguage="c#" debug="true">
			<assemblies>
				<add assembly="*"/>
			</assemblies>
		</compilation>
		<customErrors mode="Off"/>
		<authentication mode="Windows"/>
		<identity impersonate="true"/>
		<authorization>
			<deny users="?"/>
			<allow users="*"/>
		</authorization>
		<trace enabled="false" requestLimit="10" pageOutput="false" traceMode="SortByTime" localOnly="true"/>
		<sessionState mode="InProc" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;Trusted_Connection=yes" cookieless="false" timeout="20"/>
		<globalization requestEncoding="utf-8" responseEncoding="utf-8"/>
		<webServices>
			<soapExtensionImporterTypes>
				<add type="Microsoft.Web.Services3.Description.WseExtensionImporter, Microsoft.Web.Services3, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"/>
			</soapExtensionImporterTypes>
		</webServices>
		<httpRuntime maxRequestLength="2097151" />
	</system.web>
	<appSettings>
    <add key="ApplicationName" value="ProcessManager" />

    <!-- sharepoint clause plugin and template import/export webservices. 
			 Do not put trailing '/' on any values. 
		-->
		<add key="clauseSharepointServer" value="http://localhost:8081"/>
		<add key="clauseSharepointSite" value=""/>
		<add key="clauseSharepointDocRepository" value="Clause Library"/>
		<add key="clauseSharepointNetworkMapName" value="Sharepoint"/>
		<add key="templateSharepointServer" value="http://localhost:8081"/>
		<add key="templateSharepointSite" value=""/>
		<add key="templateSharepointDocRepository" value="Clause Library"/>
		<add key="templateSharepointNetworkMapName" value="Sharepoint"/>
	</appSettings>
	<!-- WSE 3.0 & MTOM Settings -->
	<microsoft.web.services3>
		<security>
			<defaultTtlInSeconds value="86400">
			</defaultTtlInSeconds>
			<timeToleranceInSeconds value="86400">
			</timeToleranceInSeconds>
		</security>
		<messaging>
			<mtom clientMode="On"/>
		</messaging>
	</microsoft.web.services3>
	<!-- Shared Library Settings -->
	<repositoryConfiguration activeInstance="SharepointRepository" activated="true">
		<repositoryTypes>
			<!-- Sharepoint respository type -->
			<repositoryType name="Sharepoint" type="Perfectus.Sharepoint.SharepointRepository, Perfectus.Sharepoint"/>
		</repositoryTypes>
		<instances>
			<!-- Instance settings that will be set to the type Sharepoint when dynamically loaded -->
			<instance name="SharepointRepository" type="Sharepoint" connectionSetting="SharepointRepository"/>
		</instances>
		<connectionSettings>
			<!-- Connection Settings for the SharepointRepository Instance -->
			<connectionSetting name="SharepointRepository">
				<parameters>
					<!-- Sharepoint repository connection settings -->
					<parameter name="RepositoryType" value="sharepoint"/>
					<parameter name="RepositoryName" value="Perfectus%20Shared%20Library"/>
					<parameter name="RepositoryLocation" value="http://localhost:8081/"/>
					<parameter name="RepositorySubLocation" value=""/>

					<!-- Repository folder names - only change the value settings -->
					<parameter name="FunctionFolder" value="Functions"/>
					<parameter name="InterviewPageFolder" value="Interview Pages"/>
					<parameter name="OutcomeFolder" value="Outcomes"/>
					<parameter name="QuestionFolder" value="Questions"/>
					<parameter name="SimpleOutcomeFolder" value="Simple Outcomes"/>
					<parameter name="TemplateFolder" value="Templates"/>
				</parameters>
			</connectionSetting>
		</connectionSettings>
	</repositoryConfiguration>
</configuration>