using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;

using System.Web;
using System.Web.Services;
using Perfectus.Common;
using Perfectus.Common.SharedLibrary;
using Perfectus.Common.PackageObjects;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.WebServices.SharedLibrary
{
    /// <summary>
    /// Summary description for SharedLibrary
    /// </summary>
    [WebService(Namespace = "http://www.perfectussolutions.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class SharedLibrary : System.Web.Services.WebService
    {
        #region Variables & Constructor

        // Shared library repostiory variables
        private IRepository mRepository;
        private RepositoryProviderFactory mRepositoryFactory;

        public SharedLibrary()
        {
            InitializeComponent();
            mRepositoryFactory = new RepositoryProviderFactory();
            mRepository = mRepositoryFactory.CreateActiveRepository();
        }

        #region Component Designer generated code

        //Required by the Web Services Designer 
        private IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() { }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #endregion
        #endregion
        #region Property Methods

        [WebMethod]
        public bool IsSharedLibraryActivated(out string sharedLibraryLoadMessage)
        {
            try
            {
                return mRepository.IsSharedLibraryActivated(out sharedLibraryLoadMessage);
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);

                if (rethrow)
                {
                    throw ex;
                }
                else
                {
                    sharedLibraryLoadMessage = ex.Message;
                    return false;
                }
            }
        }

        #endregion
        #region Upload Methods

        /// <summary>
        ///     Adds a given list of RepositoryItems to the library and returns the instances as 
        ///     they are reprsented in the Shared Library.
        /// </summary>
        /// <param name="request">A byte array containing a list of RepositoryItems.</param>
        /// <returns>A byte array containing the added RepositoryItems list as represented in the shared library.</returns>
        [WebMethod]
        public byte[] AddRepositoryItems(byte[] request)
        {
            RepositoryItem repositoryItem = null;
            
            try
            {
                repositoryItem = Translate.ToUncompressedRepositoryItem(request);
                mRepository.AddRepositoryItems(repositoryItem);

                return Translate.ToCompressedByteArray(repositoryItem);
            }
            catch (Exception ex)
            {
                try
                {
                    DeleteNewlyAddedRepositoryItems(repositoryItem);
                }
                finally
                {
                    throw ex;
                }
            }
        }

        #endregion
        #region  Download Methods

        [WebMethod]
        public RepositoryFolder GetAllItemsByType(LibraryItemType type)
        {
            try
            {
                return mRepository.GetAllItemsByType(type);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Get the latest version of the provided libtaty item
        /// </summary>
        /// <param name="request">Byte array representing a given library item.</param>
        /// <returns>A byte array representing the latest version of the LibraryItem instance.</returns>
        [WebMethod]
        public byte[] GetLatestVersion(byte[] request)
        {
            RepositoryItem repositoryItem;

            try
            {
                repositoryItem = Translate.ToUncompressedRepositoryItem(request);
                mRepository.GetLatestVersion(repositoryItem);

                return Translate.ToCompressedByteArray(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        #endregion
        #region Check In / Out Methods

        /// <summary>
        /// Gets the CheckOut status for the given LibraryItem, which is provided via a byte array.
        /// </summary>
        /// <param name="request">A byte array representing an instance of the LibraryItem whose CheckOut status is sort after.</param>
        /// <param name="loginName">The login name of the user who has the item checked out, otherwise an empty string.</param>
        /// <returns>The CheckOut status of the givne LibraryItem.</returns>
        [WebMethod]
        public byte[] GetCheckOutStatus(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.GetCheckOutStatus(repositoryItem);

                return Translate.ToByteArray(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Checks a collection of LibraryItem's into the repository.
        /// </summary>
        /// <param name="request">The LibraryItem to be checked in represented by a byte array.</param>
        /// <returns>A byte array representing the collection of LibraryItems that have just been checked into the library.</returns>
        [WebMethod]
        public byte[] CheckIn(byte[] request)
        {
            RepositoryItem repositoryItem = null; ;

            try
            {
                repositoryItem = Translate.ToUncompressedRepositoryItem(request);
                mRepository.CheckIn(repositoryItem);

                return Translate.ToCompressedByteArray(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);

                try
                {
                    DeleteNewlyAddedRepositoryItems(repositoryItem);
                }
                finally
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Checks out a LibraryItem from the repository.
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to check out.</param>
        /// <returns>Byte array reprsenting the latest version of checked out LibraryItem.</returns>
        [WebMethod]
        public byte[] CheckOut(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.CheckOut(repositoryItem);

                return Translate.ToCompressedByteArray(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Performs an undo checkout of the given LibraryItem.
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to perform an undo check on.</param>
        [WebMethod]
        public void UndoCheckout(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.UndoCheckout(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Rolls the latest version back of the given LibraryItem.
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to perform the rollback on.</param>
        [WebMethod]
        public void RollBackLatestVersion(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.RollBackLatestVersion(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        #endregion
        #region Misc Methods

        /// <summary>
        /// Discovers whether a given LibraryItem exists in the Repository or not
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem whose existance is being checked.</param>
        /// <returns>A boolean flag indicating whether the LibraryItem exists or not.</returns>
        [WebMethod]
        public byte[] DoesRepositoryItemExist(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                RepositoryItem foundRepositoryItem = mRepository.DoesRepositoryItemExist(repositoryItem);

                if (foundRepositoryItem != null)
                {
                    return Translate.ToByteArray(foundRepositoryItem);
                }
                else
                { 
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Creates a new folder in the repository using the given repositoryFolder as a template.
        /// </summary>
        /// <param name="repositoryFolder">Represents the respository folder to be created.</param>
        /// <param name="libraryItemType">The type of LibraryItem that the folder should be created for.</param>
        /// <returns>A boolean indicating whether the folder was successfully created or not.</returns>
        [WebMethod]
        public bool CreateNewFolder(RepositoryFolder repositoryFolder, LibraryItemType libraryItemType)
        {
            try
            {
                return mRepository.CreateNewFolder(repositoryFolder, libraryItemType);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        #endregion
        #region Additional Admin Functional Methods

        /// <summary>
        /// Renames a LibraryItem in the repository.
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to be renamed.</param>
        /// <param name="newFileName">The new file name of the item in the repository.</param>
        [WebMethod]
        public void Rename(byte[] request, string newFileName)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.Rename(repositoryItem, newFileName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes an item from the repository
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to be deleted.</param>
        [WebMethod]
        public void Delete(byte[] request)
        {
            try
            {
                RepositoryItem repositoryItem = Translate.ToRepositoryItem(request);
                mRepository.Delete(repositoryItem);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes an item from the repository
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to be deleted.</param>
        [WebMethod]
        public void DeleteAll()
        {
            try
            {
                mRepository.DeleteAll();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes an item from the repository
        /// </summary>
        /// <param name="request">Byte array representing the LibraryItem to be deleted.</param>
        [WebMethod]
        public void DeleteAllByType(LibraryItemType type)
        {
            try
            {
                mRepository.DeleteByType(type);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        /// <summary>
        /// Searches the repository for LibraryItems using the given search text.
        /// </summary>
        /// <param name="searchText">The text to search on.</param>
        /// <returns>Results of found LibraryItems.</returns>
        [WebMethod]
        public string Search(string searchText)
        {
            try
            {
                return mRepository.Search(searchText);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);
                throw ex;
            }
        }

        #endregion
        #region Private Methods
        
        /// <summary>
        ///     Deletes the RepositoryItems contained within the list, only if the given item is marked as newly added to the library.
        /// </summary>
        /// <remarks>
        ///     This is usually called after an error when new items have been added to the shared library.
        /// </remarks>
        /// <param name="updatedRepositoryItems">A collection of RepositoryItems which have been updated in the repository.</param>
        private void DeleteNewlyAddedRepositoryItems(RepositoryItem repositoryItem)
        {
            if (repositoryItem == null) { return; }

            try
            {                
                if ( repositoryItem.DependentRepositoryItems != null )
                foreach (RepositoryItem dependentRepositoryItem in repositoryItem.DependentRepositoryItems)
                {
                    DeleteNewlyAddedRepositoryItems(dependentRepositoryItem);
                }

                if (repositoryItem.IsNewLibraryItem)
                {
                    Delete(repositoryItem);
                }
            }
            catch (Exception ex)
            {
                // Only log the exception, don't rethrow - as this is called by functions adding new library items and is handled there
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw new Exception(string.Format("{0} Shared Library Deletion Error", ex.Message)); //TODO - SL - ERROR RESOURCE
            }
        }

        /// <summary>
        ///     Deletes a given RepositoryItem from the shared library
        /// </summary>
        /// <param name="repositoryItem">The instance of the RepositoryItem to be deleted.</param>
        private void Delete(RepositoryItem repositoryItem)
        {
            try
            {
                mRepository.Delete(repositoryItem);
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL1);

                if (rethrow)
                {
                    throw ex;
                }
            }
        }

        #endregion
    }
}
