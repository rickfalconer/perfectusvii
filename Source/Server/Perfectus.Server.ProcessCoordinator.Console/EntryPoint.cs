using System;
using System.Diagnostics;
//using Perfectus.Server.ProcessCoordinator.Engine.QueueListener;
using Perfectus.Server.ProcessCoordinator.Engine2;

namespace Perfectus.Server.ProcessCoordinator.ConsoleHost
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    class EntryPoint
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Process Coordinator starting...");
            Ctrl.ptr().Start();
            Console.WriteLine("Process Coordinator running...");
            Console.WriteLine("Press Ctrl-A to shutdown, press Ctrl-C to abort");
            
            while (true)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.A && keyInfo.Modifiers == ConsoleModifiers.Control)
                {
                    Console.WriteLine("Process Coordinator shuting down...");
                    Ctrl.ptr().Stop();
                    break;
                }
            }
        }
    }
}