 
-- Job_StatusList
CREATE OR REPLACE PROCEDURE APPMR.Job_StatusList (
     	 RC1 OUT APPMR.PERFECTUS_GLOBAL.REF_CUR  
     ) 
     AS 
	  BEGIN
 	 		 OPEN RC1 FOR 
	 		 SELECT Status AS StatusValue, StatusFriendly AS StatusName FROM  APPMR.JobStatus ORDER BY Rank; 
      END Job_StatusList; 

-- Instance_StatusList
CREATE OR REPLACE PROCEDURE APPMR.Instance_StatusList (
     	 RC1 OUT  APPMR.PERFECTUS_GLOBAL.REF_CUR  
     ) 
     AS 
	  BEGIN
 	 		 OPEN RC1 FOR 
	 		 SELECT InterviewState AS StatusValue, Name AS StatusName FROM  APPMR.InterviewStatus ORDER BY Rank; 
      END Instance_StatusList;


-- UDF_MAXDATE
CREATE OR REPLACE FUNCTION  APPMR.UDF_MAXDATE ( param1 date, param2 date )  return date IS    
	   v_value date;    
	   v_param1 date;    
	   v_param2 date;  
	   BEGIN  	
			v_param1 := param1;  	
			v_param2 := param2;  	
			IF( param1 IS NULL ) THEN  		
				v_param1 := param2;  	
			END IF;  	
			IF( param2 IS NULL ) THEN  		
				v_param2 := param1;  	
			END IF;  	
			IF( v_param1 > v_param2) THEN  	 	 
				v_value := v_param1;  	
			ELSE  		 
				v_value := v_param2;  	
			END IF;  	
			 	
			RETURN v_value;  
		END; 	 

-- UDF_LOCALTOUTC
CREATE OR REPLACE FUNCTION  APPMR.UDF_LOCALTOUTC ( lcldate date )  return timestamp IS 
	   BEGIN  	
	   	  return CAST(lcldate AS TIMESTAMP) + (SYS_EXTRACT_UTC(SYSTIMESTAMP)-SYSTIMESTAMP);  
	   END;  	 

-- UDF_UTCTOLOCAL
CREATE OR REPLACE FUNCTION  APPMR.UDF_UTCTOLOCAL ( utcdate date )  return timestamp IS 
	   BEGIN  	
	   	  return CAST(utcdate AS TIMESTAMP) + (SYSTIMESTAMP-SYS_EXTRACT_UTC(SYSTIMESTAMP));  
	   END;  	 


	 