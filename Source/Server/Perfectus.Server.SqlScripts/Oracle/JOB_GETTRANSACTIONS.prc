CREATE OR REPLACE PROCEDURE APP_PERFECTUS.Job_GetTransactions (
	MaxRows		  	 IN number,
 	MinDate 	  	 IN date,
 	SortBy 		  	 IN varchar2 DEFAULT 'JobQueueID',
 	SortOrder 	  	 IN varchar2 DEFAULT 'DESC',
 	FilterTextColumn IN varchar2 DEFAULT NULL,
  	FilterText 		 IN varchar2 DEFAULT NULL,
  	FilterDateColumn IN varchar2 DEFAULT NULL,
  	FilterFromDate	 IN date 	 DEFAULT NULL,
  	FilterToDate	 IN date 	 DEFAULT NULL,
  	ExStatusValue 	 IN varchar  DEFAULT NULL,
	RC1   		  	 OUT APP_PERFECTUS.PERFECTUS_GLOBAL.REF_CUR
 ) AS

   sql_cmd varchar2(32767);
   mindateutc date;
   filtercol varchar2(100);
   datecol varchar2(100);
   sortcol varchar2(100);

   param1 varchar2(100) := 'p1';
   param2 varchar2(100) := 'p2';
   param3 varchar2(100) := 'p3';
   
   Quote constant varchar2(1) := '''';
   Doubled_Quote constant varchar2(2) := Quote||Quote;	
   CompleteStatusName varchar(100);

BEGIN 

	-- Check for a valid sort 
	IF( SortOrder <> 'DESC' AND SortOrder <> 'ASC' ) THEN 
		RETURN;
	END IF;

	-- Convert Min date to Utc as WhenCreate column is Utc
	mindateutc := APP_PERFECTUS.UDF_LOCALTOUTC( MinDate );	

	-- Commonly used value.
	SELECT StatusFriendly INTO CompleteStatusName FROM APP_PERFECTUS.JobStatus WHERE (Status = 'COMPLETE');

	-- Remove all existing records.
	DELETE FROM APP_PERFECTUS.TP_PackageJobQueue;
	
	-- Insert distinct records.
	INSERT INTO APP_PERFECTUS.TP_PackageJobQueue (PackageInstanceId, JobQueueID)
		SELECT
			p.PackageInstanceId,
			jq.JobQueueID AS JobQueueID
		FROM APP_PERFECTUS.PackageInstance p
		LEFT OUTER JOIN APP_PERFECTUS.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId)
		WHERE (jq.JobQueueID IS NOT NULL) AND (p.WhenCreated >= mindateutc) 
		UNION 
		SELECT 
			p.PackageInstanceId,
			d.JobQueueID AS JobQueueID
		FROM APP_PERFECTUS.PackageInstance p
		LEFT OUTER JOIN APP_PERFECTUS.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId)
		WHERE (d.JobQueueID IS NOT NULL) AND (p.WhenCreated >= mindateutc) 
		ORDER BY JobQueueID DESC;
		
	-- Update remaining records.
	UPDATE APP_PERFECTUS.TP_PackageJobQueue pt1 
	    SET (PageId,PageName,StatusValue,StatusName,LastStatusChange,PackageVersion,WhenCreatedLcl,FriendlyMessage) =	
	   ( SELECT 
	   	 		NVL(jq.PageToProcess, d.PageId), 
				NVL(jq.PageName, d.PageName),
				NVL(jq.Status, 'COMPLETE'),
				NVL( (SELECT js.StatusFriendly FROM APP_PERFECTUS.JobStatus js WHERE (js.Status = jq.Status)), CompleteStatusName ),
				APP_PERFECTUS.udf_MaxDate( pt2.LastStatusChange,APP_PERFECTUS.udf_MaxDate( APP_PERFECTUS.UDF_UTCTOLOCAL( d.WhenLoggedUtc), jq.LastStatusChange)),
				(p.PackageVersionNumber + (p.PackageRevisionNumber*0.001)),
				APP_PERFECTUS.UDF_UTCTOLOCAL(p.WhenCreated ),
 				NVL2(jq.JobQueueID, (SELECT FriendlyMessage FROM (SELECT e.RelatedPackageInstanceId, e.JOBQUEUEID, e.FriendlyMessage FROM APP_PERFECTUS.ExceptionLog e 
 						WHERE e.ParentExceptionId IS NULL ORDER BY ExceptionId DESC ) 
 						WHERE (RelatedPackageInstanceId=p.PackageInstanceId)AND(JOBQUEUEID=pt2.JobQueueID) AND ROWNUM=1), NULL ) 	 
		FROM APP_PERFECTUS.TP_PackageJobQueue pt2 
 		INNER JOIN APP_PERFECTUS.PackageInstance p ON (p.PackageInstanceId = pt2.PackageInstanceId)
 		LEFT OUTER JOIN APP_PERFECTUS.JobQueue jq ON (jq.PackageInstanceId = pt2.PackageInstanceId) AND jq.JobQueueID = pt2.JobQueueID
 		LEFT OUTER JOIN APP_PERFECTUS.DistributionResult d ON (d.PackageInstanceId = pt2.PackageInstanceId) AND d.JobQueueID = pt2.JobQueueID
		WHERE (ROWNUM = 1) AND (pt2.PackageInstanceId = pt1.PackageInstanceId) AND (pt2.JobQueueID = pt1.JobQueueID) );
			
	-- Build sql
	sql_cmd := 'SELECT * FROM (SELECT pt.JobQueueID,pt.PageId,pt.PageName,pt.StatusValue,pt.StatusName,pt.LastStatusChange,p.Reference,pt.PackageVersion,p.CreatedByPersonId,pt.WhenCreatedLcl,pt.FriendlyMessage,pt.PackageInstanceId ' ||
		'FROM APP_PERFECTUS.TP_PackageJobQueue pt ' ||
		'INNER JOIN APP_PERFECTUS.PackageInstance p ON (p.PackageInstanceId = pt.PackageInstanceId) '; 

	-- Order by clause
	IF( SortBy IS NOT NULL ) THEN
	 	sortcol:=Replace(SortBy, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||' ORDER BY '||sortcol||' '||SortOrder||' ';
	END IF;

	sql_cmd:=sql_cmd||') WHERE ROWNUM <= :1 ';

	-- Filter clauses
	IF( FilterTextColumn IS NOT NULL AND FilterText IS NOT NULL ) THEN 
	 	filtercol:=Replace(FilterTextColumn, Quote, Doubled_Quote); 
		sql_cmd:=sql_cmd||' AND '||filtercol||' LIKE :p1 '; 
		param1:=FilterText; 
	ELSE 
		sql_cmd:=sql_cmd||' AND ''p1'' = :p1 '; 
	END IF; 
	IF(FilterDateColumn IS NOT NULL AND FilterFromDate IS NOT NULL ) THEN
	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||' AND '||datecol||' >= :p1 ';
		param2:=FilterFromDate;
	ELSE
		sql_cmd:=sql_cmd||' AND ''p2'' = :p2 ';
	END IF;
	IF( FilterDateColumn IS NOT NULL AND FilterToDate IS NOT NULL ) THEN
	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||' AND '||datecol||' <= :p3 ';
		param3 := FilterToDate;
	ELSE
		sql_cmd:=sql_cmd||' AND ''p3'' = :p3 ';
	END IF;

	-- Status Options
 	IF( ExStatusValue IS NOT NULL ) THEN
 		sql_cmd := sql_cmd || '	AND pt.StatusValue NOT IN (' || ExStatusValue || ') ';
 	END IF;

	OPEN RC1 FOR sql_cmd USING MaxRows, param1, param2, param3;

END Job_GetTransactions; 
