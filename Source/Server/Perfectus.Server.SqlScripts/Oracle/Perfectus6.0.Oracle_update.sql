 PROMPT *-------------------------------------*
 PROMPT * Upgrade script FOR 5.6.0 TO 6.0.0   *
 PROMPT *-------------------------------------*
 
 PROMPT * Please enter the TABLESPACE name (e.g. "PerfectusTS") *
 ACCEPT askTabelspace PROMPT 'Enter the tablespace name: ' 
 
 PROMPT * Please enter the perfectus username (e.g. App_Perfectus") *
 ACCEPT askOWNER_USER PROMPT 'Enter the perfectus standard username: ' 
 
 
 DECLARE 
          Tabelspace VARCHAR2 ( 120) := '&askTabelspace';
          OWNER_USER VARCHAR2 ( 120):= '&askOWNER_USER';

--         Tabelspace VARCHAR2 ( 120) := 'PMR';
--         OWNER_USER VARCHAR2 ( 120):= 'APP_PERFECTUS';


----------------------------------------------
PROCEDURE MaybeDropTable ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2 ) 
IS
  counter INT;      
BEGIN
	SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND UPPER (table_name)=UPPER(tablename);
    IF counter > 0 THEN
       EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;
END;            

PROCEDURE CreateTable ( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
BEGIN
    EXECUTE IMMEDIATE 'CREATE TABLE ' || lookupuser || '.' || tablename || tabledefinition || 'TABLESPACE ' || tablespacename;
END;            

----------------------------------------------

PROCEDURE CreatePrimaryKey ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  keyName VARCHAR2 (200 );
BEGIN
    keyName := 'PK_' || UPPER(tablename);
	SELECT COUNT(*) INTO counter FROM dba_indexes WHERE UPPER(index_name) = keyName AND owner=UPPER(lookupuser) AND table_name=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' drop CONSTRAINT ' || keyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' ADD (CONSTRAINT ' || keyName || ' PRIMARY KEY ( ' || fieldname || ' ) ) ';
END;                    

----------------------------------------------

PROCEDURE CreateIndex ( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  keyName VARCHAR2 (200 );
  v_index varchar2(200);
BEGIN
    keyName := 'IDX_' || UPPER(SUBSTR(tablename,1,4)) || '_' || UPPER(SUBSTR(fieldname,1,4)) || '_' || UPPER(SUBSTR(lookupuser,1,4));

	BEGIN 
		SELECT INDEX_NAME INTO v_index FROM DBA_INDEXES WHERE OWNER=UPPER(lookupuser) AND INDEX_NAME = UPPER(keyName);

    	EXECUTE IMMEDIATE 'DROP INDEX '||lookupuser||'.'||keyName;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN GOTO createindex;
	
	END;
	
	<<createindex>>
    EXECUTE IMMEDIATE 'CREATE INDEX '||lookupuser||'.'||keyName||' ON '||lookupuser||'.'||tablename||' ( '||fieldname||' ) TABLESPACE '||tablespacename;
END;                    

----------------------------------------------

PROCEDURE CreateForeignKey (
          sourceuser VARCHAR2,
         sourcetable VARCHAR2,
         sourcefields VARCHAR2, 
          targetuser VARCHAR2,
         targettable VARCHAR2,
         targetfields VARCHAR2)
IS
  counter INT;      
  foreignKeyName VARCHAR2 (200 );
BEGIN
     foreignKeyName := 'FK_' || UPPER(SUBSTR(sourcetable,1,10)) || '_' || UPPER 

(SUBSTR(targettable,1,10));
     SELECT COUNT (*) INTO counter FROM dba_constraints WHERE owner=UPPER(sourceuser) 

AND constraint_type='R' AND table_name=UPPER(sourcetable) AND 

foreignKeyName=constraint_name;
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' drop 

CONSTRAINT ' || foreignKeyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' ADD ( 

CONSTRAINT ' || 
						foreignKeyName || ' FOREIGN KEY ( ' || 

sourcefields || ' ) ' ||
                      'REFERENCES ' || targetuser ||'.'|| targettable || ' ( ' ||
                      targetfields || ' ) ON DELETE CASCADE ) ';
END;                    
    
 
----------------------------------------------
PROCEDURE CreateSequenceAndTrigger ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  seqName VARCHAR2 (200 );
  triggerName   VARCHAR2 (200 );
BEGIN
     seqName := UPPER(lookupuser) || '.SEQ' || UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2));
       SELECT COUNT(*) INTO counter FROM dba_sequences WHERE UPPER(sequence_name) = 'SEQ' || 

UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2)) AND sequence_owner=UPPER(lookupuser);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'DROP SEQUENCE '||seqName ;
    END IF;
     EXECUTE IMMEDIATE 'CREATE SEQUENCE ' || seqName || ' MINVALUE 0 START  WITH 1 ';

     triggerName := UPPER(lookupuser) || '.TRG' || UPPER(tablename) || '_' || 

UPPER(SUBSTR(fieldname,1,2));
     EXECUTE IMMEDIATE ' CREATE or replace TRIGGER ' || triggerName || ' BEFORE INSERT ON ' || 

UPPER(lookupuser) || '.' || UPPER(tablename) || 
                         ' FOR EACH ROW ' || 
                       ' BEGIN ' ||  
                       ' SELECT ' || seqName ||'.nextval INTO :new.' || fieldname 

|| ' FROM dual; END; ';
END;

----------------------------------------------
PROCEDURE MaybeDropTrigger ( lookupuser VARCHAR2,tablename  VARCHAR2, fieldname VARCHAR2) 
IS
  v_index varchar2(200);
  name   VARCHAR2 (200 );
BEGIN
    name := 'TRG' || UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2));
	
	BEGIN 
		SELECT TRIGGER_NAME INTO v_index FROM DBA_TRIGGERS WHERE OWNER=UPPER(lookupuser) AND TRIGGER_NAME=UPPER(name);

    	EXECUTE IMMEDIATE 'DROP TRIGGER '||lookupuser||'.'||name;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN RETURN;
	
	END;
END;            


----------------------------------------------
PROCEDURE CreateTemporaryTable ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
  counter INT;      
BEGIN
	SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND UPPER (table_name)=UPPER(tablename);
    IF counter > 0 THEN
       EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;

    EXECUTE IMMEDIATE 'CREATE GLOBAL TEMPORARY TABLE ' || lookupuser || '.' || tablename || tabledefinition || 'ON COMMIT PRESERVE ROWS';
END;            


----------------------------------------------

PROCEDURE CreateAllTables 
IS
BEGIN
	 MaybeDropTable ( OWNER_USER, 'ATTACHMENT' ); 
	 MaybeDropTable ( OWNER_USER, 'DISTRIBUTEQUEUE' ); 
	 MaybeDropTable ( OWNER_USER, 'JOBQUEUE' ); 
	 MaybeDropTable ( OWNER_USER, 'JOBSTATUS' ); 

     CreateTable  (Tabelspace, OWNER_USER, 'JOBQUEUE',  
     '(JOBQUEUEID 		NUMBER (10,0) NOT NULL,' || 
	 'PACKAGEINSTANCEID VARCHAR2(50)  NOT NULL,' ||
     'PAGETOPROCESS     VARCHAR2(50)  NULL,' ||
	 'LASTSTATUSCHANGE  DATE			NOT NULL,' || 
     'TASKSUBMITTEDBY   VARCHAR2 (50)  NOT NULL,' || 
	 'STATUS			VARCHAR2 (50)  NOT NULL,' || 
	 'PRIORITY			NUMBER (4)  NOT NULL,' || 
	 'RETRY				NUMBER (4)  NOT NULL,' || 
	 'ASSIGNEDPROCESS	VARCHAR2 (500),' ||
	 'PAGENAME          VARCHAR2(50))' );

     CreateTable  (Tabelspace, OWNER_USER, 'JOBSTATUS',  
     '(STATUS			VARCHAR2 (50)  NOT NULL,' || 
     'STATUSFRIENDLY	VARCHAR2 (100) NOT NULL,' || 
	 'RANK				NUMBER (4)  NOT NULL)' );
	 
	 CreateTable  (Tabelspace, OWNER_USER, 'ATTACHMENT', '( ' ||
     'PACKAGEID              VARCHAR2(50 BYTE)      NOT NULL,' ||
     'PACKAGEVERSIONNUMBER   NUMBER(10)             NOT NULL,' ||
     'PACKAGEREVISIONNUMBER  NUMBER(10)             NOT NULL,' ||
     'ATTACHMENTID          VARCHAR2(50 BYTE)      NOT NULL,' ||
     'ATTACHMENTNAME        VARCHAR2(100 BYTE)     NOT NULL,' ||
     'ATTACHMENTBYTES       BLOB                   NOT NULL' ||
       '  )');

	 CreateTable  (Tabelspace, OWNER_USER, 'DISTRIBUTEQUEUE', '( ' ||
     'JOBQUEUEID 		NUMBER (10,0) NOT NULL,' ||
     'DOCUMENTGUID      VARCHAR2(50 BYTE)      NOT NULL,' ||
	 'DISTRIBUTORGUID	VARCHAR2(50 BYTE)      NOT NULL,' ||
     'DOCUMENTNAME      VARCHAR2(255 BYTE)     NOT NULL,' ||
	 'DISTRIBUTORNAME	VARCHAR2(255 BYTE)     NOT NULL'  ||
       '  )');
END;

PROCEDURE CreateSequnces
IS
BEGIN
     CreateSequenceAndTrigger ( OWNER_USER, 'JOBQUEUE', 'JOBQUEUEID'     );
END;


PROCEDURE CreatePrimaryKeys
IS
BEGIN
     CreatePrimaryKey ( OWNER_USER, 'JOBQUEUE', 'JOBQUEUEID' );
     CreatePrimaryKey ( OWNER_USER, 'JOBSTATUS', 'STATUS' );
	 CreatePrimaryKey ( OWNER_USER, 'ATTACHMENT', 'PACKAGEID, PACKAGEVERSIONNUMBER, PACKAGEREVISIONNUMBER, ATTACHMENTID' );
	 CreatePrimaryKey ( OWNER_USER, 'DISTRIBUTEQUEUE', 'JOBQUEUEID, DOCUMENTGUID, DISTRIBUTORGUID' );
END;


----------------------------------------------


 PROCEDURE CreateForeignKeys
 IS
 BEGIN
       CreateForeignKey (OWNER_USER,'JOBQUEUE','STATUS', OWNER_USER,'JOBSTATUS','STATUS');
       CreateForeignKey (OWNER_USER,'JOBQUEUE','STATUS', OWNER_USER,'JOBSTATUS','STATUS');
	   
       CreateForeignKey (OWNER_USER,'ATTACHMENT','PACKAGEID, PACKAGEVERSIONNUMBER, PACKAGEREVISIONNUMBER',
                         OWNER_USER,'PACKAGE_','PackageId,PackageVersionNumber,PackageRevisionNumber');

       CreateForeignKey (OWNER_USER,'DISTRIBUTEQUEUE','JOBQUEUEID', OWNER_USER,'JOBQUEUE','JOBQUEUEID');
 END;

 


PROCEDURE CreateProcedure (
          sourceuser VARCHAR2,
         procedurename VARCHAR2,
         pbody VARCHAR2)
IS
BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE PROCEDURE ' || sourceuser ||'.'|| procedurename || pbody; 
END;                    




PROCEDURE CreateFunctions
IS
BEGIN

-- UDF_MAXDATE
    EXECUTE IMMEDIATE 'CREATE OR REPLACE FUNCTION ' || OWNER_USER || '.UDF_MAXDATE ( param1 date, param2 date )  return date IS ' ||   
	   'v_value date; ' ||   
	   'v_param1 date; ' ||   
	   'v_param2 date; ' || 
	   'BEGIN ' || 	
			'v_param1 := param1; ' || 	
			'v_param2 := param2; ' || 	
			'IF( param1 IS NULL ) THEN ' || 		
			'	v_param1 := param2; ' || 	
			'END IF; ' || 	
			'IF( param2 IS NULL ) THEN ' || 		
			'	v_param2 := param1; ' || 	
			'END IF; ' || 	
			'IF( v_param1 > v_param2) THEN ' || 	 	 
			'	v_value := v_param1; ' || 	
			'ELSE ' || 		 
			'	v_value := v_param2; ' || 	
			'END IF; ' || 	
			' ' ||	
			'RETURN v_value; ' || 
		'END; '; 	 

-- UDF_LOCALTOUTC
    EXECUTE IMMEDIATE 'CREATE OR REPLACE FUNCTION ' || OWNER_USER || '.UDF_LOCALTOUTC ( lcldate date )  return timestamp IS ' ||
	   'BEGIN ' || 	
	   '	  return CAST(lcldate AS TIMESTAMP) + (SYS_EXTRACT_UTC(SYSTIMESTAMP)-SYSTIMESTAMP); ' || 
	   'END; '; 	 

-- UDF_UTCTOLOCAL
    EXECUTE IMMEDIATE 'CREATE OR REPLACE FUNCTION ' || OWNER_USER || '.UDF_UTCTOLOCAL ( utcdate date )  return timestamp IS ' ||
	   'BEGIN ' || 	
	   '	  return CAST(utcdate AS TIMESTAMP) + (SYSTIMESTAMP-SYS_EXTRACT_UTC(SYSTIMESTAMP)); ' || 
	   'END; '; 	 

END;


PROCEDURE CreateProcedures
 IS
 BEGIN


-- JOB_CREATE
         CreateProcedure ( OWNER_USER,'JOB_CREATE', '(' ||
     '   Status				IN VARCHAR2,' ||
     '   PackageInstanceId	IN VARCHAR2,' ||
     '   TaskSubmittedBy	IN VARCHAR2,' ||
     '   PageToProcess		IN VARCHAR2,' ||
     '   Priority			IN NUMBER,' ||
     '   PageName 			IN VARCHAR2 default NULL ' ||
     ') ' ||
     'AS ' ||
	 ' v_InterviewStatus varchar (12); ' ||
	 ' v_PackageInstanceId varchar (50); ' ||
	 '  BEGIN' ||
	 '    insert into ' || OWNER_USER || '.JobQUEUE (Status,PackageInstanceId,TaskSubmittedBy,PageToProcess,LASTSTATUSCHANGE, PRIORITY, Retry, PageName)  '||
	 '      values (Status,PackageInstanceId,TaskSubmittedBy,PageToProcess,sysdate, Priority, 0, PageName); '||
     '  END "JOB_CREATE"; ');
	 
-- JOB_PEEK

         CreateProcedure ( OWNER_USER,'JOB_PEEK', '(' ||
     '   Delay IN NUMBER, ' ||
     '   AvailableJobs OUT NUMBER ' ||
     ') ' ||
     'AS ' ||
	 ' v_delay number; ' ||
	 '  BEGIN' ||
	 ' 		  IF  Delay < 0 THEN ' ||
     '            v_delay := 0; ' ||
	 ' 		  ELSIF  Delay > 86400 THEN ' ||
     '            v_delay := 86400 ; ' ||
	 ' 		  ELSE ' ||
     '            v_delay := DELAY ; ' ||
     '        END IF; ' ||
	 '		   select count(1) into AvailableJobs from ' || OWNER_USER || '.JobQUEUE join PackageInstance on JobQUEUE.PackageInstanceID=PackageInstance.PackageInstanceID WHERE Status=''QUEUE'' AND AssignedProcess is null ' || 
	 '		   		  AND (Retry = 0 or Delay < 86400*(SYSDATE-laststatuschange)) ' ||
	 '				  AND PackageInstance.InterviewState!=''FAIL'' and PackageInstance.InterviewState!=''DONE''; ' ||
	 '  END "JOB_PEEK"; ');

-- Job_ResetOverdue

         CreateProcedure ( OWNER_USER,'JOB_RESETOVERDUE', '(' ||
     '   MaxDwellTime  IN NUMBER, ' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 '  BEGIN' ||
	 
 	 '   	  Open rc1 for ' ||
	 '		   select JobQueueID, TaskSubmittedBy, LastStatusChange,  pi.PackageInstanceId' || 
	 '		   from ' || OWNER_USER || '.JobQUEUE join ' || OWNER_USER || '.PackageInstance  pi on JobQUEUE.PackageInstanceId=pi.PackageInstanceId ' ||
	 '		   where AssignedProcess is not null and Status != ''COMPLETE'' and pi.InterviewState!=''FAIL'' and pi.InterviewState!=''DONE'' and (MaxDwellTime < 86400*(SYSDATE-laststatuschange));' ||
	 
     '	      update ' || OWNER_USER || '.JobQUEUE set JobQUEUE.LastStatusChange=sysdate, AssignedProcess=null, Status=''QUEUE'' ' ||
	 '		   where AssignedProcess is not null and Status != ''COMPLETE'' and ' ||
	 '		   MaxDwellTime < 86400*(SYSDATE-laststatuschange) and ' ||
	 '		    jobqueue.PackageInstanceId in ' ||
	 '		   ( select PackageInstance.PackageInstanceId from ' || OWNER_USER || '.PackageInstance where InterviewState!=''DONE'' and InterviewState!=''FAIL''); ' ||  

	 
	 
	 '  END "JOB_RESETOVERDUE"; ');


-- JOB_UPDATE	 
	          CreateProcedure ( OWNER_USER,'JOB_UPDATE', '(' ||
     '	 Status IN VARCHAR2,' ||
     '	 Retry IN NUMBER,' ||
	 '   ID IN NUMBER ' ||
     ') ' ||
     'AS ' ||
	 ' v_id number; '||
	 ' v_Retry number; '||
	 ' v_status varchar(25); '||
	 ' v_AssignedProcess varchar(500); '||
	 ' v_PackageInstanceId varchar(50); '||

	 ' v_lastJobSuccess number; '||
	 ' v_pageJobNotSuccess number; '||

	 '  BEGIN' ||
	 '   v_id := ID; '||
	 '   v_Retry := Retry; '||
	 '   v_status := status; '||
	 '   v_AssignedProcess := null; '||

     '	 update ' || OWNER_USER || '.JobQUEUE set JobQUEUE.Status=v_Status, JobQUEUE.Retry=v_Retry, LastStatusChange=sysdate where JobQUEUE.JobQueueid=v_id; '||
	 
     '  SELECT PackageInstanceId INTO v_PackageInstanceId FROM ' || OWNER_USER || '.JobQUEUE WHERE JobQueueid=v_id; '||

	 
 	 '   if  v_Status = ''QUEUE'' THEN '||
     '	     update ' || OWNER_USER || '.JobQUEUE set JobQUEUE.AssignedProcess=null where JobQUEUE.JobQueueid=v_id; '||

     '   ELSIF  v_Status = ''ERROR'' THEN ' ||
     '	     update packageinstance set InterviewState=''FAIL'', WhenCompleted = SYS_EXTRACT_UTC(SYSTIMESTAMP) where PackageInstanceId=v_PackageInstanceId; '||		

     '   ELSIF  v_Status = ''COMPLETE'' THEN ' ||
	 '	 		SELECT count(1) into v_lastJobSuccess from ' || OWNER_USER || '.JobQueue ' || 
	 '	 			   where PackageInstanceId=v_PackageInstanceId  ' || 
	 '	 			   		 and status=''COMPLETE'' ' ||
	 '	 					 and PageToProcess IS NULL; ' ||

	 '   		IF v_lastJobSuccess = 1 THEN ' ||
	 '   		   SELECT count(1) into v_pageJobNotSuccess from ' || OWNER_USER || '.JobQueue ' || 
	 '   		   		  where PackageInstanceId=v_PackageInstanceId ' ||
	 '   				  		and status !=''COMPLETE'' ' ||
	 '   						and PageToProcess IS NOT NULL; ' ||
	 '   			IF v_pageJobNotSuccess = 0 THEN ' ||
	 '   		   	   delete from ' || OWNER_USER || '.jobQueue where PackageInstanceId=v_PackageInstanceId; ' ||
	 '   		   	   update ' || OWNER_USER || '.PackageInstance set InterviewState=''DONE'', WhenCompleted = SYS_EXTRACT_UTC(SYSTIMESTAMP) where PackageInstanceId=v_PackageInstanceId; ' ||
	 '				END IF; ' ||
	 '	        END IF; ' ||

     '   END IF; ' ||
	 
     '  END "JOB_UPDATE"; ');

--JOB_READ
         CreateProcedure ( OWNER_USER,'JOB_READ', '(' ||
     '   ID IN NUMBER,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 ' v_id number; '||
	 '  BEGIN' ||
	 '   v_id := ID; '||
 	 '   Open rc1 for ' ||
	 '		   select pi.PackageId,pi.PackageVersionNumber,pi.PackageRevisionNumber,pi.PackageInstanceId,TaskSubmittedBy,PageToProcess,Retry' || 
	 '		   from ' || OWNER_USER || '.JobQUEUE join ' || OWNER_USER || '.PackageInstance  pi on JobQUEUE.PackageInstanceId=pi.PackageInstanceId ' ||
	 '		   where JobQUEUE.JobQueueid = v_id for update;' ||
     '  END "JOB_READ"; ');
	 

         CreateProcedure ( OWNER_USER,'JOB_READANY', '(' ||
     '   Delay IN NUMBER, ' ||
     '	 AssignedProcess IN VARCHAR2,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 ' v_delay number; ' ||
	 ' v_id number; ' ||
     ' v_AssignedProcess varchar2(500);' ||
	 '  BEGIN' ||
	 '   v_AssignedProcess := AssignedProcess; '|| 
	 ' 		  IF  Delay < 0 THEN ' ||
     '            v_delay := 0; ' ||
	 ' 		  ELSIF  Delay > 86400 THEN ' ||
     '            v_delay := 86400 ; ' ||
	 ' 		  ELSE ' ||
     '            v_delay := DELAY ; ' ||
     '        END IF; ' ||

	 '	 	  select JobQueueid into v_id from ' || OWNER_USER || '.JobQUEUE  where Status = ''QUEUE'' AND AssignedProcess is null AND priority > 0  AND (JobQUEUE.Retry = 0 OR 1 < 86400*(SYSDATE-laststatuschange)) AND rownum <= 1 for update; ' ||
	 '        IF v_id >= 0 THEN	 ' ||
     '	 	  update ' || OWNER_USER || '.JobQUEUE set JobQUEUE.AssignedProcess=v_AssignedProcess, LastStatusChange=sysdate where JobQUEUE.JobQueueid=v_id;' ||

 	 '   	  Open rc1 for ' ||
	 '		   select pi.PackageId,pi.PackageVersionNumber,pi.PackageRevisionNumber,pi.PackageInstanceId,TaskSubmittedBy,PageToProcess,Retry,JobQueueid' || 
	 '		   from ' || OWNER_USER || '.JobQUEUE join ' || OWNER_USER || '.PackageInstance  pi on JobQUEUE.PackageInstanceId=pi.PackageInstanceId ' ||
	 '		   where JobQueueid=v_id;' ||
	 '        END IF;	 ' ||
	 
	 
/*
 	 '   	  Open rc1 for ' ||
	 '		   select pi.PackageId,pi.PackageVersionNumber,pi.PackageRevisionNumber,pi.PackageInstanceId,TaskSubmittedBy,PageToProcess,Retry,JobQueueid' || 
	 '		   from ' || OWNER_USER || '.JobQUEUE join ' || OWNER_USER || '.PackageInstance  pi on JobQUEUE.PackageInstanceId=pi.PackageInstanceId ' ||
	 '		   where Status = ''QUEUE'' AND AssignedProcess is null AND priority > 0 AND (JobQUEUE.Retry = 0 OR v_delay < 86400*(SYSDATE-laststatuschange)) AND rownum <= 1 for update order by LASTSTATUSCHANGE, priority desc;' ||
*/	 
     '  END "JOB_READANY"; ');

	 
         CreateProcedure ( OWNER_USER,'JOB_READNONEBLOCKING', '(' ||
     '   ID IN NUMBER,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 ' v_id number; '||
	 '  BEGIN' ||
	 '   v_id := ID; '||
	 	 '   Open rc1 for ' ||
	 '		   select pi.PackageId,pi.PackageVersionNumber,pi.PackageRevisionNumber,pi.PackageInstanceId,TaskSubmittedBy,PageToProcess,Retry' || 
	 '		   from ' || OWNER_USER || '.JobQUEUE join ' || OWNER_USER || '.PackageInstance  pi on JobQUEUE.PackageInstanceId=pi.PackageInstanceId ' ||
	 '		   where JobQUEUE.JobQueueid = v_id;' ||
     '  END "JOB_READNONEBLOCKING"; ');
	 
	 
-- JOB_GETINFO
         CreateProcedure ( OWNER_USER,'JOB_GETINFO', '(' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC4     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 '  BEGIN' ||
     '   Open rc1 for ' ||
	 '		   select PACKAGEINSTANCEID,PACKAGEID,PACKAGEVERSIONNUMBER,PACKAGEREVISIONNUMBER,REFERENCE,CREATEDBYPERSONID,WHENCREATED,WHENCOMPLETED,WHENDELETED,DELETEDBYPERSONID,InterviewState,ISSYNCHRONIZED from ' || OWNER_USER || '.PackageInstance;  ' ||  		 
 	 '   Open rc2 for ' ||
	 '		   select * from ' || OWNER_USER || '.JobQueue;  ' ||  		 
  	 '   Open rc3 for ' ||
	 '		   select * from ' || OWNER_USER || '.ExceptionLog;  ' ||  		 
  	 '   Open rc4 for ' ||
	 '		   select * from ' || OWNER_USER || '.DistributionResult;  ' ||  		 
     '  END "JOB_GETINFO"; ');
	
	
	CreateProcedure ( OWNER_USER,'PACKAGEINSTANCE_RESUBMIT', '(' ||
     '   PackageInstanceId     IN VARCHAR2, ' ||
     '   JobReactivated OUT NUMBER ' ||
     ') ' ||
     'AS ' ||
	 ' v_InterviewState    varchar2(50); ' ||
	 ' v_PackageInstanceId    varchar2(50); ' ||
	 ' v_error_jobs NUMBER(10,0);  ' ||
     'BEGIN ' ||
	 ' v_PackageInstanceId := PackageInstanceId; ' ||
	 ' JobReactivated := 0; ' ||
	 
     '  SELECT PackageInstance.InterviewState INTO v_InterviewState FROM ' || OWNER_USER || '.PACKAGEINSTANCE WHERE PACKAGEINSTANCEID=v_PackageInstanceId; ' ||
     '  SELECT count(*) INTO v_error_jobs FROM ' || OWNER_USER || '.JobQUEUE WHERE PACKAGEINSTANCEID=v_PackageInstanceId and STATUS=''ERROR''; '||

	 '  IF  v_InterviewState = ''FAIL'' AND v_error_jobs > 0 THEN ' ||
	 '      update ' || OWNER_USER || '.JobQUEUE SET Status = ''QUEUE'', retry=0, AssignedProcess=null WHERE PACKAGEINSTANCEID=v_PackageInstanceId; ' ||
	 '      update ' || OWNER_USER || '.PACKAGEINSTANCE SET InterviewState = ''FINI'' WHERE PACKAGEINSTANCEID=v_PackageInstanceId; ' ||
	 '      JobReactivated := v_error_jobs; ' ||
     '  END IF; ' ||

     'END PACKAGEINSTANCE_RESUBMIT;');
    
	

    CreateProcedure ( OWNER_USER,'PACKAGE_ATTACHMENT_INSERT', '(' || 
     'PackageId     IN VARCHAR2, ' ||
     'PackageVersionNumber     IN NUMBER, ' ||
     'PackageRevisionNumber     IN NUMBER, ' ||
     'ATTACHMENTName     IN VARCHAR2, ' ||
     'ATTACHMENTId     IN VARCHAR2, ' ||
     'ATTACHMENTBytes     IN BLOB) ' ||
     'AS ' ||
     'v_PackageId     VARCHAR2(50); ' ||
     'v_ATTACHMENTname varchar2(100);' ||
     'v_PackageVersionNumber      NUMBER; ' ||
     'v_PackageRevisionNumber      NUMBER; ' ||
     'v_ATTACHMENTId      VARCHAR2(50); ' ||
     'v_ATTACHMENTBytes      BLOB; ' ||
     'BEGIN ' ||
     '  v_packageid := packageid; ' ||
     '	v_ATTACHMENTname := ATTACHMENTName;' ||
     '	v_PackageVersionNumber :=PackageVersionNumber; ' ||
     '	v_PackageRevisionNumber := PackageRevisionNumber;' ||
     '	v_ATTACHMENTId := ATTACHMENTId;' ||
     '	v_ATTACHMENTBytes := ATTACHMENTBytes; ' ||
     '        INSERT INTO ' || OWNER_USER || '.ATTACHMENT(PackageId, PackageVersionNumber, PackageRevisionNumber, ATTACHMENTId, ATTACHMENTName, ATTACHMENTBytes) ' ||
     '            VALUES (v_PackageId, v_PackageVersionNumber, v_PackageRevisionNumber, v_ATTACHMENTId, v_ATTACHMENTname, v_ATTACHMENTBytes); ' ||
     'END PACKAGE_ATTACHMENT_INSERT;');

     CreateProcedure ( OWNER_USER,'PACKAGE_ATTACHMENT_GET', '(' ||
     'PackageId         IN VARCHAR2, ' ||
     'VersionNumber     IN NUMBER, ' ||
     'RevisionNumber    IN NUMBER, ' ||
     'ATTACHMENTId     IN VARCHAR2, ' ||
      'cur_result       OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'v_packageid    varchar2(50); ' ||
     'v_ATTACHMENTid    varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageid := PackageId; ' ||
     'v_ATTACHMENTid := ATTACHMENTId; ' ||
     '    BEGIN ' ||
     '        OPEN cur_result FOR ' ||
     '        SELECT ATTACHMENTBytes ' ||
     '         FROM ' || OWNER_USER || '.ATTACHMENT ' ||  
     '            WHERE PackageId = v_packageid ' ||  
     '             AND PackageVersionNumber = VersionNumber ' ||  
     '             AND PackageRevisionNumber = RevisionNumber ' ||  
     '             AND ATTACHMENTId = v_ATTACHMENTid; ' ||
     '    END; ' ||
     'END PACKAGE_ATTACHMENT_GET; ');    

	 CreateProcedure ( OWNER_USER,'Distribute_Remove', '(' ||
     'JobQueueID        IN NUMBER, ' ||
     'DistributerGuid   IN VARCHAR2 ) ' ||
     'AS ' ||
     'v_JobQueueId  NUMBER;   ' ||
     'v_DistributerGuid varchar2(50); ' ||
     'BEGIN ' ||
     '		v_JobQueueId := JobQueueID; ' ||
     '		v_DistributerGuid := DistributerGuid; ' ||
     '		delete from  ' || OWNER_USER || '.DistributeQueue ' || 
     '			   where JobQueueID = v_JobQueueId and DistributorGuid = v_DistributerGuid; ' ||
     'END Distribute_Remove; ');    

	 CreateProcedure ( OWNER_USER,'Distribute_Create', '(' ||
     'JobQueueID        IN NUMBER,   ' ||
     'DocumentGuid      IN VARCHAR2, ' ||
     'DistributorGuid   IN VARCHAR2, ' ||
     'DocumentName      IN VARCHAR2, ' ||
     'DistributorName   IN VARCHAR2 DEFAULT NULL ) ' ||
     'AS ' ||
   'BEGIN ' ||
  	 '		Insert into ' || OWNER_USER || '.DistributeQueue (JobQueueId,DocumentGuid,DistributorGuid, DocumentName, DistributorName) ' || 
     '			   values (JobQueueID,DocumentGuid,DistributorGuid,DocumentName,DistributorName ); ' ||
     'END Distribute_Create; ');    
	 

	 CreateProcedure ( OWNER_USER,'Distribute_ReadNoneBlocking', '(' ||
     'JobQueueID        IN NUMBER,   ' ||
     'RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR )' || 
     'AS ' ||
     'v_JobQueueId  NUMBER;   ' ||
     'BEGIN ' ||
     '		v_JobQueueId := JobQueueID; ' ||
 	 '   Open rc1 for ' ||
	 '		   select JobQueueId,DocumentGuid,DistributorGuid ' || 
	 '		   from ' || OWNER_USER || '.DistributeQueue ' ||
	 '		   where JobQueueID = v_JobQueueId;' ||
     'END Distribute_ReadNoneBlocking; ');    
	 	 
		 
	  CreateProcedure ( OWNER_USER,'Distribute_Peek', '(' ||
     'JobQueueID        IN NUMBER, ' ||
     'AvailableDistribution	OUT NUMBER )' ||
     'AS ' ||
     'v_JobQueueId  NUMBER;   ' ||
     'BEGIN ' ||
     '		v_JobQueueId := JobQueueID; ' ||
	 '		select count(1) into AvailableDistribution from ' || OWNER_USER || '.DistributeQueue where JobQueueID = v_JobQueueId;' || 
     'END Distribute_Peek; ');	

END;



PROCEDURE UpdateProcedures
 IS
 BEGIN

-- PACKAGE_INSERT
    CreateProcedure ( OWNER_USER,'package_insert', '(' ||
     '   isnewversion              IN       NUMBER,' ||
     '   packageid                 IN       VARCHAR2,' ||
     '   NAME                      IN       VARCHAR2,' ||
     '   startpagename             IN       VARCHAR2,' ||
     '   startpageid               IN       VARCHAR2,' ||
     '   createdby                 IN       VARCHAR2,' ||
     '   modifiedby                IN       VARCHAR2,' ||
     '   publishedby               IN       VARCHAR2,' ||
     '   whencreated               IN       DATE,' ||
     '   whenmodified              IN       DATE,' ||
     '   whenpublished             IN       DATE,' ||
     '   description               IN       VARCHAR2,' ||
     '   packagebytesnotemplates   IN       BLOB,' ||
     '   versionXML                IN       VARCHAR2,' ||
     '   newrevisionnumber         IN OUT   NUMBER,' ||
     '   newversionnumber          IN OUT   NUMBER' ||
     ') ' ||
     'AS ' ||
     '   revisionnumber   NUMBER (10, 0); ' ||
     '   versionnumber    NUMBER (10, 0); ' ||
     '   v_packageid        VARCHAR2(50); ' ||
     '   existing_count    number(10, 0); ' ||
     'BEGIN ' ||
     '   v_packageid := packageid; ' ||

     '	SELECT MAX(packageversionnumber) INTO versionnumber FROM ' || OWNER_USER || '.PACKAGE_ ' ||
     '  WHERE packageid = v_packageid; ' ||
	 '  IF versionnumber IS NULL THEN ' ||
     '		versionnumber := 1; ' ||
     '  ELSE ' || 
	 '		BEGIN ' ||
	 '		    IF isnewversion = 1 THEN ' ||
     '			  versionnumber := versionnumber + 1; ' ||
     '			END IF; ' ||
     '		END; ' ||
     '  END IF; ' ||

     '   SELECT   1 + CASE WHEN (MAX(PackageRevisionNumber) IS NOT NULL) THEN ' ||
     '                        MAX(PackageRevisionNumber) ' ||
     '                     ELSE ' ||
     '                        -1 ' ||
     '                     END ' ||
     '   INTO     revisionnumber ' ||
     '   FROM     ' || OWNER_USER || '.package_ ' ||
     '   WHERE    packageid = v_packageid ' ||
     '   AND      packageversionnumber = versionnumber; ' ||
     '   BEGIN ' ||
     '      INSERT INTO package_ ' ||
     '                  (packageid, packageversionnumber, packagerevisionnumber, ' ||
     '                   packagebytes, NAME, description, startpagename, ' ||
     '                   startpageid, createdby, modifiedby, publishedby, ' ||
     '                   whencreated, whenmodified, whenpublished, versionXML ' ||
     '                  ) ' ||
     '           VALUES (v_packageid, versionnumber, revisionnumber, ' ||
     '                   packagebytesnotemplates, substr(NAME,1,50), substr(description,1,1000), startpagename, ' ||
     '                   startpageid, upper(substr(createdby,1,510)), substr(modifiedby,1,510), substr(publishedby,1,510), ' ||
     '                   whencreated, whenmodified, whenpublished, substr(versionXML,1,2000) ' ||
     '                  ); ' ||
     '   END; ' ||
     '   newversionnumber := versionnumber; ' ||
     '   newrevisionnumber := revisionnumber; ' ||
     'END package_insert; ');
     
-- INSTANCE_CREATE
	          CreateProcedure ( OWNER_USER,'INSTANCE_CREATE', '(' ||      
     'packageId     IN VARCHAR2, ' ||
     'version     IN NUMBER, ' ||
     'revision     IN NUMBER, ' ||
     'createdBy     IN VARCHAR2, ' ||
     'Instanceid     IN VARCHAR2) ' ||
     'AS ' ||
     'v_packageid    varchar2(50); ' ||
     'v_createdBy varchar2(255); ' ||
     'BEGIN ' ||
     'v_packageid := packageid; ' ||
     'v_createdBy := upper(createdby); ' ||
     'BEGIN ' ||
     'INSERT INTO ' || OWNER_USER || '.PackageInstance (PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, WhenCreated, CreatedByPersonId, reference, InterviewState) ' ||
     '    SELECT  instanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, ' ||
     '            SYS_EXTRACT_UTC(SYSTIMESTAMP), v_createdBy, name, ''INTE'' ' ||
     '                 FROM ' || OWNER_USER || '.Package_ ' ||
     '                        WHERE PackageId = v_packageid ' ||
     '                         AND PackageVersionNumber = version ' ||
     '                         AND PackageRevisionNumber = revision; ' ||
     '        IF  SQL%ROWCOUNT <> 1 THEN ' ||
     '            GOTO Err; ' ||
     '        END IF; ' ||
     '        INSERT INTO ' || OWNER_USER || '.NavigationStack (PackageInstanceId, PositionInStack, PageId, PageName) ' ||
     '            SELECT  instanceId, 0, startPageId, startPageName ' ||
     '                 FROM ' || OWNER_USER || '.Package_ ' ||
     '                        WHERE PackageId = v_packageid ' ||
     '                         AND PackageVersionNumber = version ' ||
     '                         AND PackageRevisionNumber = revision; ' ||
     '        IF  SQL%ROWCOUNT <> 1 THEN ' ||
     '            GOTO Err; ' ||
     '        END IF; ' ||
     '        GOTO Finished; ' ||
     '    <<Err>> NULL; ' ||
     '    raise_application_error(-20999, ''Instance was not created.''); ' ||
     '    <<Finished>> NULL;' ||
     '    END; ' ||
     'END INSTANCE_CREATE;');
	 
-- Package_VersionXML_Get
         CreateProcedure ( OWNER_USER,'PACKAGE_VERSIONXML_GET', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||   
     '  VersionXML     IN OUT varchar2 ) ' ||
     '  AS ' ||
     '  v_packageinstanceid        varchar2(50); ' ||
     '  BEGIN ' ||
     '  v_packageinstanceid := PackageInstanceId; ' ||
     '          FOR rec IN ( SELECT  VersionXML FROM ' || OWNER_USER || '.PackageInstance   WHERE PackageInstanceId = v_packageinstanceid) ' ||
     '          LOOP ' ||
     '  VersionXML := rec.VersionXML ;  ' ||
     '          END LOOP; ' ||
     '  END PACKAGE_VERSIONXML_GET;');


	 
-- STATUS_UPDATE
     CreateProcedure ( OWNER_USER,'STATUS_UPDATE', '(' ||
      'instanceId     IN VARCHAR2, ' ||
      'statusCode     IN CHAR) ' ||
      'AS ' ||
      'currentStatusRank     NUMBER(10,0); ' ||
      'proposedStatusRank     NUMBER(10,0); ' ||
      'currentStatusCode     CHAR; ' ||
      'BEGIN ' ||
      '    BEGIN ' ||
      '        FOR rec IN ( SELECT   Rank ' ||
      '                                 FROM ' || OWNER_USER || '.InterviewStatus c INNER JOIN ' || OWNER_USER || '.PackageInstance pin ' || 
      '                                ON c.InterviewState = pin.InterviewState  ' ||
      '                                    WHERE PackageInstanceId = instanceId) ' ||
      '        LOOP ' ||
      '           CurrentStatusRank := rec.Rank ; ' || 
      '        END LOOP; ' ||
      '        FOR rec IN ( SELECT   InterviewState ' ||
      '                                 FROM ' || OWNER_USER || '.PackageInstance ' || 
      '                                    WHERE PackageInstanceId = instanceId) ' ||
      '        LOOP ' ||
      '           currentStatusCode := rec.InterviewState ; ' || 
      '        END LOOP; ' ||
      '        FOR rec IN ( SELECT   RANK ' ||
      '                                     FROM ' || OWNER_USER || '.InterviewStatus ' ||  
      '                                        WHERE InterviewState = statusCode) ' ||
      '        LOOP ' ||
      '           proposedStatusRank := rec.RANK ; ' || 
      '        END LOOP; ' ||
      '        IF  ( CurrentStatusRank < proposedStatusRank OR   CurrentStatusRank IS NULL OR   CurrentStatusRank = 0 OR (statusCode = ''FINI'' AND currentStatusCode = ''DONE'' )) THEN ' ||
      '        BEGIN ' ||
      '            UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '            SET                 InterviewState = statusCode ' ||
      '                            WHERE PackageInstanceId = instanceId; ' ||
      '        END; ' ||
      '        END IF; ' ||
      '    END; ' ||
      'END STATUS_UPDATE; ' );
     
-- INSTANCE_STATUS_GET
     CreateProcedure ( OWNER_USER,'INSTANCE_STATUS_GET', '(' ||
      'PACKAGEINSTANCEID IN VARCHAR2, ' ||
      'RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
      'RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
       'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
      'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     '  OPEN RC1 FOR ' ||
     '	SELECT  PackageInstance.PackageInstanceId,  ' ||
     '			PackageInstance.PackageId,  ' ||
     '			PackageInstance.PackageVersionNumber,  ' ||
     '			PackageInstance.PackageRevisionNumber,  ' ||
     '			PackageInstance.Reference,  ' ||
     '			PackageInstance.WhenCreated,  ' ||
     '			PackageInstance.WhenCompleted,  ' ||
     '			PackageInstance.WhenDeleted,  ' ||
     '			PackageInstance.DeletedByPersonId,  ' ||
     '			Package_.Name AS ParentPackageName,  ' ||
     '			PackageInstance.CreatedByPersonId,  ' ||
	 '			InterviewStatus.Name AS InterviewStatusName,  ' ||
	 '			InterviewStatus.Rank AS InterviewStatusRank,  ' ||
	 '			InterviewStatus.InterviewState ' ||
	 '			FROM ' || OWNER_USER || '.PackageInstance ' ||
	 '			INNER JOIN ' || OWNER_USER || '.Package_ ON PackageInstance.PackageId = Package_.PackageId ' ||
	 '			AND PackageInstance.PackageVersionNumber = Package_.PackageVersionNumber ' ||
	 '						   AND PackageInstance.PackageRevisionNumber = Package_.PackageRevisionNumber ' ||
	 '			LEFT OUTER JOIN ' || OWNER_USER || '.InterviewStatus ON PackageInstance.InterviewState = InterviewStatus.InterviewState ' ||
	 '			WHERE PackageInstance.PackageInstanceId = v_packageinstanceid ;' ||
     '  OPEN RC2 FOR ' ||
	 '			SELECT  TemplateName, ' ||
	 '					Link, ' ||
	 '					LinkText, ' ||
	 '					InternalReference,' || 
	 '					WhenLoggedUtc,' || 
	 '					TemplateId' ||
	 '			FROM  ' || OWNER_USER || '.DistributionResult ' ||
	 '			WHERE PackageInstanceId = v_packageinstanceid ;' ||
     '  OPEN RC3 FOR ' ||
	 '			SELECT  RelatedPackageInstanceId,  ' ||
	 '					FriendlyMessage,  ' ||
	 '					Message,  ' ||
	 '					RaisingAssemblyName, ' ||
	 '					SeverityCode ' ||
	 '			FROM ' || OWNER_USER || '.ExceptionLog ' ||
	 '			WHERE ExceptionLog.RelatedPackageInstanceId = v_packageinstanceid ;' ||
     'END INSTANCE_STATUS_GET; ');
     
-- INSTANCE_INFOSET_GET
      CreateProcedure ( OWNER_USER,'INSTANCE_INFOSET_GET', '(' ||
     '   DateToGetPackagesFrom   IN  DATE,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
	 ' Open rc1 for  ' ||
	 ' SELECT distinct Package_.PackageId, Package_.PackageVersionNumber, Package_.PackageRevisionNumber, ' ||	 
     '   Package_.Name, Package_.Description, Package_.StartPageName, Package_.StartPageId, ' ||
     '   Package_.CreatedBy, Package_.ModifiedBy, Package_.PublishedBy, ' ||
     '   Package_.WhenCreated, Package_.WhenModified, Package_.WhenPublished, Package_.WhenDeleted, Package_.DeletedByPersonId ' ||
     '   	  FROM ' || OWNER_USER || '.PACKAGE_' ||	
     ' join ' || OWNER_USER || '.PackageInstance on PackageInstance.PackageId = Package_.PackageID ' ||	
     ' AND PackageInstance.PackageVersionNumber = Package_.PackageVersionNumber AND PackageInstance.PackageRevisionNumber = Package_.PackageRevisionNumber ' ||	
     ' where COALESCE(isSynchronized, 0) <> 1 AND PackageInstance.WhenCreated > DateToGetPackagesFrom; ' ||	
 
	 ' Open rc2 for SELECT  ' ||	
	 ' PackageInstance.PackageInstanceId, PackageInstance.PackageId, PackageInstance.PackageVersionNumber,   ' ||	
	 ' PackageInstance.PackageRevisionNumber, PackageInstance.Reference, PackageInstance.CreatedByPersonId,  ' ||	
	 ' PackageInstance.WhenCreated, PackageInstance.WhenCompleted, PackageInstance.WhenDeleted,  ' ||	
	 ' PackageInstance.DeletedByPersonId, PackageInstance.InterviewState, PackageInstance.IsSynchronized  ' ||	
	 '   	  FROM ' || OWNER_USER || '.PackageInstance ' ||	
	 ' WHERE COALESCE(isSynchronized,0) <> 1 AND PackageInstance.WhenCreated > DateToGetPackagesFrom;	 ' ||	
 
 	 'END INSTANCE_INFOSET_GET; ');	 
	 
	 
-- STATUS_GETBY
CreateProcedure ( OWNER_USER,'STATUS_GETBY', '(' ||
      'CreatedByPersonId       IN VARCHAR2, ' ||
      'RecentTransactionCount  IN NUMBER, ' ||
      'InProgressStart         IN NUMBER, ' ||
      'InProgressEnd   IN NUMBER, ' ||
      'MyHistoryStart  IN NUMBER, ' ||
      'MyHistoryEnd    IN NUMBER, ' ||
      'InProgressRecordCount   IN OUT NUMBER, ' ||
      'HistoryRecordCount      IN OUT NUMBER, ' ||
      'RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
      'RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC4     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC5     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC6     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_createdByPersonId VARCHAR2(255); ' ||
      'v_tmpSeq number; ' ||
      ' BEGIN ' ||
      '        v_createdByPersonId := CreatedByPersonId; ' ||
      
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPINSTANCE; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPHISTORY; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT; ' ||
      
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA increment by 1 '';' ||

      ' SELECT ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA increment by 1 '';' ||

      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA increment by 1 '';' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPINSTANCE( ' ||
      '                    packageinstanceid ' ||
      '                   ,packageid ' ||
      '                   ,packageversionnumber ' ||
      '                   ,packagerevisionnumber ' ||
      '                   ,reference ' ||
      '                   ,createdbypersonid ' ||
      '                   ,whencreated ' ||
      '                   ,whencompleted ' ||
      '                   ,whendeleted ' ||
      '                   ,deletedbypersonid ' ||
      '                   ,InterviewState ' ||
      '                   ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                                ,pin.PackageId ' ||
      '                                ,pin.PackageVersionNumber ' ||
      '                                ,pin.PackageRevisionNumber ' ||
      '                                ,pin.Reference ' ||
      '                                ,pin.CreatedByPersonId ' ||
      '                                ,pin.WhenCreated ' ||
      '                                ,pin.WhenCompleted ' ||
      '                                ,pin.WhenDeleted ' ||
      '                                ,pin.DeletedByPersonId ' ||
      '                                ,pin.InterviewState ' ||
      '                                ,p.Name ParentPackageName ' || 
      '                                  FROM ' || OWNER_USER || '.PackageInstance pin ' || 
      '                                         INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                                         ON pin.PackageId = p.PackageId ' || 
      '                                         AND pin.PackageVersionNumber = p.PackageVersionNumber ' ||
      '                                         AND pin.PackageRevisionNumber = p.PackageRevisionNumber ' ||
            '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
      '                                  AND pin.WhenDeleted IS NULL ' ||   
      '                          ORDER BY pin.WhenCreated DESC ; ' ||

      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS( ' ||
      '                            packageinstanceid ' ||
      '                           ,packageid ' ||
      '                           ,packageversionnumber ' ||
      '                           ,packagerevisionnumber ' ||
      '                           ,reference ' ||
      '                           ,createdbypersonid ' ||
      '                           ,whencreated ' ||
      '                           ,whencompleted ' ||
      '                           ,whendeleted ' ||
      '                           ,deletedbypersonid ' ||
      '                           ,InterviewState ' ||
      '                           ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                               , pin.PackageId ' ||
      '                               , pin.PackageVersionNumber ' ||
      '                               , pin.PackageRevisionNumber ' ||
      '                               , pin.Reference ' ||
      '                               , pin.CreatedByPersonId ' ||
      '                               , pin.WhenCreated ' ||
      '                               , pin.WhenCompleted ' ||
      '                               , pin.WhenDeleted ' ||
      '                               , pin.DeletedByPersonId ' ||
      '                               , pin.InterviewState ' ||
      '                               , p.Name ParentPackageName ' || 
      '                          FROM ' || OWNER_USER || '.PackageInstance pin ' || 
      '                          INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                             ON pin.PackageId = p.PackageId ' || 
      '                             AND pin.PackageVersionNumber = p.PackageVersionNumber ' ||
      '                             AND pin.PackageRevisionNumber = p.PackageRevisionNumber ' || 
      '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
      '                          and pin.WhenDeleted IS NULL ' ||  
      '                          and pin.InterviewState!=''FAIL'' and pin.InterviewState!=''DONE''   ' ||  
      '                          ORDER BY pin.WhenCreated DESC ; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPHISTORY( ' ||
      '                    packageinstanceid ' ||
      '                   ,packageid ' ||
      '                   ,packageversionnumber ' ||
      '                   ,packagerevisionnumber ' ||
      '                   ,reference ' ||
      '                   ,createdbypersonid ' ||
      '                   ,whencreated ' ||
      '                   ,whencompleted ' ||
      '                   ,whendeleted ' ||
      '                   ,deletedbypersonid ' ||
      '                   ,InterviewState ' ||
      '                   ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                                , pin.PackageId ' ||
      '                                , pin.PackageVersionNumber ' ||
      '                                , pin.PackageRevisionNumber ' ||
      '                                , pin.Reference ' ||
      '                                , pin.CreatedByPersonId ' ||
      '                                , pin.WhenCreated ' ||
      '                                , pin.WhenCompleted ' ||
      '                                , pin.WhenDeleted ' ||
      '                                , pin.DeletedByPersonId ' ||
      '                                , pin.InterviewState ' ||
      '                                , p.Name ParentPackageName ' ||
      '                                  FROM ' || OWNER_USER || '.PackageInstance pin INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                                         ON pin.PackageId = p.PackageId and pin.PackageVersionNumber ' || 
      '                         = p.PackageVersionNumber and pin.PackageRevisionNumber = p.PackageRevisionNumber  ' ||
              '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
    '                                  AND pin.WhenDeleted IS NULL ' ||   
      '                                         ORDER BY pin.WhenCreated DESC; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPINSTANCE t INNER JOIN ' || OWNER_USER || '.DistributionResult d ' || 
      '                                         ON t.PackageInstanceId = d.PackageInstanceId   ' ||
      '                                                 WHERE rownum <= RecentTransactionCount   ' ||
      '                                         ORDER BY t.Rank; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS t INNER JOIN ' || OWNER_USER || '.DistributionResult ' || 
      '                         d  ' ||
      '                                         ON t.PackageInstanceId = d.PackageInstanceId left JOIN ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         td  ' ||
      '                                         ON d.DistributionResultId = td.DistributionResultId ' ||  
      '                                                 WHERE Rank  BETWEEN InProgressStart AND InProgressEnd ' || 
      '                                                  and td.DistributionResultId IS NULL ' ||  
      '                                         ORDER BY t.Rank; ' ||
   
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPHISTORY t INNER JOIN ' || OWNER_USER || '.DistributionResult d ' || 
      '                                         ON t.PackageInstanceId = d.PackageInstanceId left JOIN ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         td  ' ||
      '                                         ON d.DistributionResultId = td.DistributionResultId ' ||  
      '                                                 WHERE Rank  BETWEEN MyHistoryStart AND MyHistoryEnd ' ||  
      '                                                  and td.DistributionResultId IS NULL ' ||   
      '                                         ORDER BY t.Rank ; ' ||
  
      '                 OPEN RC1 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  InterviewState ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCE t ' ||  
      '                                         WHERE Rank <= RecentTransactionCount ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      ' 				OPEN RC2 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  InterviewState ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS t ' ||  
      '                                         WHERE Rank  BETWEEN InProgressStart AND InProgressEnd ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      '  				OPEN RC3 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  InterviewState ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPHISTORY t ' ||  
      '                                         WHERE Rank  BETWEEN MyHistoryStart AND MyHistoryEnd ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      '  				OPEN RC4 FOR ' ||
      '                 SELECT * ' ||
      '                          FROM ' || OWNER_USER || '.InterviewStatus; ' ||
      ' 				OPEN RC5 FOR ' ||
      '                 SELECT * ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' ||  
      '                                 ORDER BY packageInstanceId , templateName ; ' ||
      '                 SELECT   count(*) ' ||
      '                 INTO InProgressRecordCount ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS; ' || 
      '                 SELECT   count(*) ' ||
      '                 INTO HistoryRecordCount ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPHISTORY; ' ||
      ' 				OPEN RC6 FOR ' ||
      '                 SELECT RelatedPackageInstanceId ' ||
      '                 ,  FriendlyMessage ' ||
      '                 ,  Message ' ||
      '                 ,  raisingAssemblyName ' ||
      '                 ,  SeverityCode ' ||
      '                          FROM ' || OWNER_USER || '.exceptionLog ' ||  
      '                                         WHERE EXISTS ( ' || 
      '                                 SELECT  *  ' ||
      '                                          FROM ' || OWNER_USER || '.TP_TMPHISTORY t ' ||  
      '                                         WHERE t.packageInstanceId = exceptionLog.relatedPackageInstanceId ' || 
      '                                          AND Rank  BETWEEN MyHistoryStart AND MyHistoryEnd             ) ' || 
      '                                          OR EXISTS ( ' || 
      '                                 SELECT  *  ' ||
      '                                          FROM ' || OWNER_USER || '.TP_TMPINSTANCE ti ' ||  
      '                                         WHERE ti.packageInstanceId = exceptionLog.relatedPackageInstanceId ' || 
      '                                          AND rownum <= RecentTransactionCount           ); ' || 
	 
      '       END status_getby; ' );
	  
-- INSTANCE_GET
     CreateProcedure ( OWNER_USER,'INSTANCE_GET', '(' ||
     'packageInstanceId     IN VARCHAR2, ' ||
     'currentPageId     OUT VARCHAR2, ' ||
     'cur_result     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
          'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     '        OPEN cur_result FOR ' ||
     '        SELECT Package_.PackageBytes ' ||
     '         FROM ' || OWNER_USER || '.PackageInstance join ' || OWNER_USER || '.Package_  on ' ||
     '         PackageInstance.PackageId = Package_.PackageId AND ' ||
     '         PackageInstance.PackageVersionNumber = Package_.PackageVersionNumber AND ' ||
     '         PackageInstance.PackageRevisionNumber = Package_.PackageRevisionNumber ' ||
     '            WHERE PackageInstance.PackageInstanceId = v_packageinstanceid ' ||  
     '             AND PackageInstance.WhenDeleted IS NULL; ' ||
     '        FOR rec IN ( SELECT   PageId ' ||
     '             FROM ' || OWNER_USER || '.NavigationStack ' || 
     '                                            WHERE PackageInstanceId = v_packageinstanceid ' ||  
     '                                        ORDER BY PositionInStack DESC) ' ||
     '        LOOP ' ||
     '           currentPageId := rec.PageId ; ' || 
     '        END LOOP; ' ||
     'END INSTANCE_GET; ');
	  
-- INSTANCE_INSERTCOPY
     CreateProcedure ( OWNER_USER,'INSTANCE_INSERTCOPY', '(' ||
      'newPackageInstanceId     IN VARCHAR2, ' ||
      'oldPackageInstanceId     IN VARCHAR2, ' ||
      'userIdent     IN VARCHAR2 ) ' ||
      'AS ' ||
      'BEGIN ' ||
      '        INSERT INTO ' || OWNER_USER || '.PackageInstance (PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, CreatedByPersonId, WhenCreated, AnswerState) ' ||
      '            SELECT  newPackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, upper(userIdent), SYS_EXTRACT_UTC(SYSTIMESTAMP), AnswerState ' || 
      '             FROM ' || OWNER_USER || '.PackageInstance   ' ||
      '                WHERE packageInstanceId = oldPackageInstanceId; ' ||
      '        INSERT INTO ' || OWNER_USER || '.PackageInstance_File (PackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData) ' ||
      '            SELECT  newPackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData ' || 
      '                 FROM ' || OWNER_USER || '.PackageInstance_File   ' ||
      '                        WHERE packageInstanceId = oldPackageInstanceId; ' ||
      '        INSERT INTO ' || OWNER_USER || '.NavigationStack (PackageInstanceId, PositionInStack, PageId, PageName, PendingDeletion) ' ||
      '            SELECT  newPackageInstanceId, PositionInStack, PageId, PageName, PendingDeletion ' || 
      '                 FROM ' || OWNER_USER || '.NavigationStack   ' ||
      '                        WHERE packageInstanceId = oldPackageInstanceId; ' ||
      'END INSTANCE_INSERTCOPY; ' );
	  


     CreateProcedure ( OWNER_USER,'INSTANCE_SETCOMPLETE', '(' ||
      'PACKAGEINSTANCEID IN VARCHAR2) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'UPDATE ' || OWNER_USER || '.PACKAGEINSTANCE SET WhenCompleted = SYS_EXTRACT_UTC(SYSTIMESTAMP) WHERE PackageInstanceId = v_packageinstanceid; ' ||
      'END INSTANCE_SETCOMPLETE; ');
     
    CreateProcedure ( OWNER_USER,'INSTANCE_DELETE', '(' ||
      'PackageInstanceId     IN VARCHAR2, ' ||
      'DeletedBy     IN VARCHAR2) ' ||
      'AS ' ||
      'v_packageinstanceid    varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      '    BEGIN ' ||
      '        UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '        SET     WhenDeleted = SYS_EXTRACT_UTC(SYSTIMESTAMP),  ' ||
      '            DeletedByPersonId = DeletedBy ' ||
      '            WHERE PackageInstanceId = v_packageinstanceid; ' ||
      '    END; ' ||
      'END INSTANCE_DELETE; ' );


     CreateProcedure ( OWNER_USER,'DISTRIBUTIONRESULT_INSERT', '(' ||
      'instanceId     IN VARCHAR2, ' ||
      'distributorId     IN VARCHAR2, ' ||
      'templateid     IN VARCHAR2, ' ||
      'templateName     IN VARCHAR2, ' ||
      'link     IN VARCHAR2, ' ||
      'linkText     IN VARCHAR2, ' ||
      'internalReference     IN VARCHAR2, ' ||
      'DistributorName IN VARCHAR2  DEFAULT NULL, ' ||
      'MIMEFormat IN VARCHAR2  DEFAULT NULL, ' ||
      'JobQueueID IN NUMBER  DEFAULT NULL, ' ||
      'PageId IN VARCHAR2  DEFAULT NULL, ' ||
	  'PageName IN VARCHAR2  DEFAULT NULL) ' ||
      'AS ' ||
      'BEGIN ' ||
      '        INSERT INTO  ' || OWNER_USER || '.DistributionResult (PackageInstanceId, distributorId, templateId, templateName, link, linkText, internalReference, whenLoggedUtc,DistributorName,MIMEFormat,JobQueueID,PageId,PageName) ' ||
      '            VALUES (instanceId,distributorId,templateId,templateName,link,linkText,internalReference,SYS_EXTRACT_UTC(SYSTIMESTAMP),DistributorName,MIMEFormat,JobQueueID,PageId,PageName); ' ||
      'END DISTRIBUTIONRESULT_INSERT; ' );

	
     CreateProcedure ( OWNER_USER,'EXCEPTION_INSERT', '(' ||
      'SeverityCode     IN CHAR, ' ||
      'WhenLoggedUtc     IN DATE, ' ||
      'PersonId     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageId     IN VARCHAR2  DEFAULT NULL, ' ||
      'RelatedPackageVersionNumber     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageRevisionNumber     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageInstanceId     IN VARCHAR2  DEFAULT NULL, ' ||
      'FriendlyMessage     IN VARCHAR2, ' ||
      'message     IN VARCHAR2  DEFAULT NULL, ' ||
      'StackTrace     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyName     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyPath     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyVersion     IN VARCHAR2  DEFAULT NULL, ' ||
      'ParentExceptionId     IN NUMBER  DEFAULT NULL, ' ||
      'JOBQUEUEID IN NUMBER  DEFAULT NULL, ' ||
      'ERRORNUMBER IN VARCHAR2  DEFAULT NULL, ' ||
      'HINT IN VARCHAR2  DEFAULT NULL, ' ||
      'CATEGORY IN VARCHAR2  DEFAULT NULL, ' ||
      'ExceptionId    OUT NUMBER) ' ||
      'AS ' ||
     'v_ExceptionId NUMBER (10,0);' ||  
     'v_SEVERITYCODE CHAR (4);' || 
     'v_WHENLOGGEDUTC TIMESTAMP;' ||
     'v_PERSONID NUMBER (10,0);' ||  
     'v_RELATEDPACKAGEID VARCHAR2(50) ;' || 
     'v_RELATEDPACKAGEVERSIONNUMBER NUMBER (10,0);' || 
     'v_RELATEDPACKAGEREVISIONNUMBER NUMBER (10,0);' || 
     'v_RELATEDPACKAGEINSTANCEID VARCHAR2(50);' || 
     'v_FRIENDLYMESSAGE VARCHAR2 (510);' || 
     'v_MESSAGE VARCHAR2 (510);' || 
     'v_STACKTRACE VARCHAR2(2500)  ;' || 
     'v_RAISINGASSEMBLYNAME VARCHAR2 (50)  ;' || 
     'v_RAISINGASSEMBLYPATH VARCHAR2 (500)  ;' || 
     'v_RAISINGASSEMBLYVERSION VARCHAR2 (20)  ;' || 
     'v_PARENTEXCEPTIONID NUMBER (10,0) ;' || 
     'v_JOBQUEUEID NUMBER (10,0);' || 
     'v_ERRORNUMBER VARCHAR2 (255);' || 
     'v_HINT VARCHAR2 (1000);' || 
     'v_CATEGORY VARCHAR2 (50);' || 
      'BEGIN ' ||
     'v_SEVERITYCODE := SEVERITYCODE;' || 
     'v_WHENLOGGEDUTC  := WHENLOGGEDUTC;' ||
     'v_PERSONID := PERSONID;' ||  
     'v_RELATEDPACKAGEID := RELATEDPACKAGEID;' || 
     'v_RELATEDPACKAGEVERSIONNUMBER := RELATEDPACKAGEVERSIONNUMBER;' || 
     'v_RELATEDPACKAGEREVISIONNUMBER := RELATEDPACKAGEREVISIONNUMBER;' || 
     'v_RELATEDPACKAGEINSTANCEID := RELATEDPACKAGEINSTANCEID;' || 
     'v_FRIENDLYMESSAGE := substr ( FRIENDLYMESSAGE, 1, 508);' || 
     'v_MESSAGE := substr ( MESSAGE, 1, 508);' ||
     'v_STACKTRACE := substr ( STACKTRACE, 1, 2498);' ||
     'v_RAISINGASSEMBLYNAME := substr ( RAISINGASSEMBLYNAME, 1, 49);' || 
     'v_RAISINGASSEMBLYPATH := substr ( RAISINGASSEMBLYPATH, 1, 498);' || 
     'v_RAISINGASSEMBLYVERSION := RAISINGASSEMBLYVERSION;' || 
     'v_PARENTEXCEPTIONID := PARENTEXCEPTIONID;' ||      
	 'v_JOBQUEUEID := JOBQUEUEID;' || 
     'v_ERRORNUMBER := ERRORNUMBER;' || 
     'v_HINT := HINT;' || 
     'v_CATEGORY := CATEGORY;' || 
      '        INSERT INTO ' || OWNER_USER || '.ExceptionLog (SeverityCode, PersonId, WhenLoggedUtc, RelatedPackageId, RelatedPackageVersionNumber, RelatedPackageRevisionNumber, RelatedPackageInstanceId, FriendlyMessage, Message, StackTrace, RaisingAssemblyName, RaisingAssemblyPath, RaisingAssemblyVersion, parentExceptionId,JOBQUEUEID,ERRORNUMBER,HINT,CATEGORY) ' ||
      '            VALUES (v_SeverityCode,v_PersonId,v_WhenLoggedUtc,v_RelatedPackageId,v_RelatedPackageVersionNumber,v_RelatedPackageRevisionNumber,v_RelatedPackageInstanceId,v_FriendlyMessage,v_Message,v_StackTrace, ' ||
      '            v_RaisingAssemblyName,v_RaisingAssemblyPath,v_RaisingAssemblyVersion,v_ParentExceptionId, v_JOBQUEUEID,v_ERRORNUMBER,v_HINT,v_CATEGORY); ' ||
    ' select max (ExceptionId)  into v_ExceptionId from ' || OWNER_USER || '.ExceptionLog;' ||
      ' ExceptionId :=  v_ExceptionId; ' ||
      'END EXCEPTION_INSERT; ' );
	
END; 

	

PROCEDURE CreateAdminProcedures
 IS
 BEGIN


-- Temp table for Job_GetTransactions
 CreateTemporaryTable  (OWNER_USER, 'TP_PackageJobQueue',  
 	  '( PackageInstanceId varchar2(50) NOT NULL, ' ||
      'JobQueueID number NOT NULL, ' ||  
      'PageId varchar2(50) NULL, ' ||
      'PageName nvarchar2(50) NULL, ' ||
      'StatusValue varchar2(25) NULL, ' ||
      'StatusName varchar2(100) NULL, ' ||
      'LastStatusChange date NULL, ' ||
      'PackageVersion number NULL, ' ||
      'WhenCreatedLcl date NULL, ' ||
      'FriendlyMessage nvarchar2(255) NULL ) ' );

-- Job_StatusList
CreateProcedure ( OWNER_USER,'Job_StatusList', '(' ||
     '	 RC1 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 ' BEGIN' ||
 	 '		 OPEN RC1 FOR ' ||
	 '		 SELECT Status AS StatusValue, StatusFriendly AS StatusName FROM ' || OWNER_USER || '.JobStatus ORDER BY Rank; ' ||
     ' END Job_StatusList; ');

-- Instance_StatusList
CreateProcedure ( OWNER_USER,'Instance_StatusList', '(' ||
     '	 RC1 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     ') ' ||
     'AS ' ||
	 ' BEGIN' ||
 	 '		 OPEN RC1 FOR ' ||
	 '		 SELECT InterviewState AS StatusValue, Name AS StatusName FROM ' || OWNER_USER || '.InterviewStatus ORDER BY Rank; ' ||
     ' END Instance_StatusList; ');


-- Instance_GetTransactions
CreateProcedure ( OWNER_USER,'Instance_GetTransactions', '(' ||
	'	MaxRows		  	 IN number, ' ||
	' 	MinDate 	  	 IN date, ' ||
	' 	SortBy 		  	 IN varchar2 DEFAULT ''WhenCreatedLcl'', ' ||
	' 	SortOrder 	  	 IN varchar2 DEFAULT ''DESC'', ' ||
	' 	FilterTextColumn IN varchar2 DEFAULT NULL, ' ||
	'  	FilterText 		 IN varchar2 DEFAULT NULL, ' ||
	'  	FilterDateColumn IN varchar2 DEFAULT NULL, ' ||
	'  	FilterFromDate	 IN date 	 DEFAULT NULL, ' ||
	'  	FilterToDate	 IN date 	 DEFAULT NULL, ' ||
	'  	ExStatusValue 	 IN varchar  DEFAULT NULL, ' ||
	'	RC1   		  	 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' ||
	' ) AS ' ||
	' ' ||	
	'   sql_cmd varchar2(32767); ' ||
	'   mindateutc date; ' ||
	'   filtercol varchar2(100); ' ||
	'   datecol varchar2(100); ' ||
	'   sortcol varchar2(100); ' ||
	' ' ||
	'   param1 varchar2(100) := ''p1''; ' ||
	'   param2 varchar2(100) := ''p2''; ' ||
	'  param3 varchar2(100) := ''p3''; ' ||
	' ' ||   
	'   Quote constant varchar2(1) := ''''''''; ' ||
	'   Doubled_Quote constant varchar2(2) := Quote||Quote; ' ||	
	' ' ||	
	'BEGIN ' || 
	'	IF( SortOrder <> ''DESC'' AND SortOrder <> ''ASC'' ) THEN ' || 
	'		RETURN; ' ||
	'	END IF; ' ||
	' ' ||	
	'	mindateutc := ' || OWNER_USER || '.UDF_LOCALTOUTC( MinDate ); ' ||	
	' ' ||	
	'	sql_cmd := ''SELECT	Reference,PackageVersion,CreatedByPersonId,WhenCreatedLcl,StatusName,LastStatusChange,StatusValue,FriendlyMessage,WhenDeleted,PackageInstanceId '' || ' ||
	'	''FROM (	'' || ' ||
	'	''	 SELECT 	'' || ' || 
	'	''		p.PackageInstanceId,	'' || ' ||
	'	''		p.Reference,	'' || ' ||
	'	''		( p.PackageVersionNumber + (p.PackageRevisionNumber*0.001)) AS PackageVersion,	'' || ' ||
	'	''		CreatedByPersonId,	'' || ' ||
	'	''		' || OWNER_USER || '.UDF_UTCTOLOCAL(WhenCreated) WhenCreatedLcl,	'' || ' || 
	'	''		i.Name AS StatusName,	'' || ' ||
	'	''		NVL(' || OWNER_USER || '.UDF_UTCTOLOCAL(p.WhenDeleted),NVL(' || OWNER_USER || '.UDF_UTCTOLOCAL(p.WhenCompleted),' || OWNER_USER || '.UDF_UTCTOLOCAL(p.WhenCreated))) AS LastStatusChange, '' || ' ||  
	'	''		p.InterviewState AS StatusValue,	'' || ' ||
	'	''		p.WhenDeleted,	'' || ' ||
	'	''		(	'' ||   ' ||
	'	''		CASE	'' ||  ' ||
	'	''			 WHEN (p.InterviewState = ''''DONE'''' ) THEN	'' || ' ||
	'	''			    NULL	'' || ' ||
	'	''			 ELSE	'' || ' || 
	'	''			 	(SELECT FriendlyMessage FROM (SELECT e.RelatedPackageInstanceId, e.FriendlyMessage FROM ' || OWNER_USER || '.ExceptionLog e '' || ' || 
	'	''					WHERE e.ParentExceptionId IS NULL ORDER BY e.ExceptionId DESC ) WHERE RelatedPackageInstanceId = p.PackageInstanceId AND ROWNUM=1) '' || ' ||	 
	'	''		END )  AS FriendlyMessage	'' || ' ||   
	'	''	FROM ' || OWNER_USER || '.PackageInstance p	'' || ' || 
	'	''	INNER JOIN ' || OWNER_USER || '.InterviewStatus i ON i.InterviewState = p.InterviewState	'' || ' ||  
	'	''	WHERE p.WhenCreated > = :1 ''; ' ||
	' ' ||
	' 	IF( ExStatusValue IS NOT NULL ) THEN ' ||
	' 		sql_cmd := sql_cmd || ''	AND p.InterviewState NOT IN ('' || ExStatusValue || '') ''; ' ||
	'	END IF; ' ||
	' ' ||
	'	sql_cmd:=sql_cmd||'' ORDER BY ''; ' || 
	'	IF( SortBy IS NOT NULL AND SortBy <> ''WhenCreatedLcl'' ) THEN ' ||
	'	 	sortcol:=Replace(SortBy, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||sortcol||'' ''||SortOrder||'', WhenCreatedLcl DESC''; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||''WhenCreatedLcl ''||SortOrder||'' ''; ' ||
	'	END IF; ' ||
	' ' ||	
	'	sql_cmd := sql_cmd || '') InstanceTable WHERE ROWNUM <= :2 ''; ' || 
	' ' ||	
	' ' ||	
	' 	IF( FilterTextColumn IS NOT NULL AND FilterText IS NOT NULL ) THEN ' ||
	'	 	filtercol:=Replace(FilterTextColumn, Quote, Doubled_Quote); ' ||
	' 		sql_cmd:=sql_cmd||'' AND ''||filtercol||'' LIKE :p1 ''; ' ||
	'		param1:=FilterText; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p1'''' = :p1 ''; ' ||
	' 	END IF; ' ||
	'	IF(FilterDateColumn IS NOT NULL AND FilterFromDate IS NOT NULL ) THEN ' ||
	'	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||'' AND ''||datecol||'' >= :p1 ''; ' ||
	'		param2:=FilterFromDate; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p2'''' = :p2 ''; ' ||
	'	END IF; ' ||
	'	IF( FilterDateColumn IS NOT NULL AND FilterToDate IS NOT NULL ) THEN ' ||
	'	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||'' AND ''||datecol||'' <= :p3 ''; ' ||
	'		param3 := FilterToDate; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p3'''' = :p3 ''; ' ||
	'	END IF; ' ||
	' ' ||	
	'	OPEN RC1 FOR sql_cmd USING mindateutc, MaxRows, param1, param2, param3; ' ||
	' ' ||	
	'END Instance_GetTransactions; ');

-- Instance_GetDistributions
CreateProcedure ( OWNER_USER,'Instance_GetDistributions', '(' ||
	'	   p_PackageInstanceId varchar, ' || 
	'	   RC1 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||  
	'	   RC2 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ) ' || 
	'AS ' ||
	'  v_CompleteStatusName varchar(100); ' ||
	' ' ||	
	'BEGIN ' ||
	'	SELECT StatusFriendly INTO v_CompleteStatusName FROM ' || OWNER_USER || '.JobStatus WHERE (Status = ''COMPLETE''); ' ||
	' ' ||	
	' 	OPEN RC1 FOR SELECT ' || 
	' 		p.PackageInstanceId, ' ||
	' 		jq.JobQueueID AS JobQueueID, ' ||
	' 		jq.PageToProcess AS PageId, ' ||
	' 		jq.PageName, ' ||
	' 		dq.DistributorGuid AS DistributorId, ' ||
	' 		dq.DistributorName, ' ||
	' 		dq.DocumentGuid AS DocumentId, ' ||
	' 		dq.DocumentName, ' ||
	' 		jq.Status AS StatusValue, ' ||
	' 		(SELECT js.StatusFriendly FROM ' || OWNER_USER || '.JobStatus js WHERE (js.Status = jq.Status)) AS StatusName, ' ||
	' 		jq.LastStatusChange, ' || 
	' 		NULL AS InternalReference, ' ||
	' 		NULL AS LinkText ' ||
	' 	FROM ' || OWNER_USER || '.PackageInstance p ' ||
	' 	INNER JOIN ' || OWNER_USER || '.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId) ' ||
	' 	LEFT OUTER JOIN ' || OWNER_USER || '.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueID) ' ||
	' 	WHERE p.PackageInstanceId = p_PackageInstanceId ' ||
	' 	UNION ALL ' || 
	' 	SELECT ' || 
	' 		p.PackageInstanceId, ' ||
	' 		d.JobQueueID AS JobQueueID, ' ||
	' 		d.PageId, ' ||
	' 		d.PageName, ' ||
	' 		d.DistributorId, ' ||
	' 		d.DistributorName, ' ||
	' 		d.TemplateId AS DocumentId, ' ||
	' 		d.TemplateName AS DocumentName, ' ||
	' 		''COMPLETE'', ' ||
	' 		v_CompleteStatusName AS StatusName, ' ||
	' 		' || OWNER_USER || '.UDF_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange, ' ||  
	' 		d.InternalReference, ' ||
	' 		d.LinkText ' ||
	' 	FROM ' || OWNER_USER || '.PackageInstance p ' ||
	' 	INNER JOIN ' || OWNER_USER || '.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId) ' ||
	' 	WHERE p.PackageInstanceId = p_PackageInstanceId ' ||
	' 	ORDER BY JobQueueID; ' || 
	' ' || 
	' 	OPEN RC2 FOR SELECT * FROM (SELECT ' || 
	' 		 RelatedPackageInstanceId, JOBQUEUEID, Message AS ExceptionMessage ' ||
	' 	FROM ' || OWNER_USER || '.ExceptionLog ' ||
	' 	WHERE (RelatedPackageInstanceId = p_PackageInstanceId)AND(ParentExceptionId IS NULL) ORDER BY ExceptionId DESC ) WHERE ROWNUM=1; ' ||
	'END Instance_GetDistributions; ');

-- Job_GetTransactions
CreateProcedure ( OWNER_USER,'Job_GetTransactions', '(' ||
	'	MaxRows		  	 IN number, ' ||
	' 	MinDate 	  	 IN date, ' ||
	' 	SortBy 		  	 IN varchar2 DEFAULT ''JobQueueID'', ' ||
	' 	SortOrder 	  	 IN varchar2 DEFAULT ''DESC'', ' ||
	' 	FilterTextColumn IN varchar2 DEFAULT NULL, ' ||
	'  	FilterText 		 IN varchar2 DEFAULT NULL, ' ||
	'  	FilterDateColumn IN varchar2 DEFAULT NULL, ' ||
	'  	FilterFromDate	 IN date 	 DEFAULT NULL, ' ||
	'  	FilterToDate	 IN date 	 DEFAULT NULL, ' ||
	'  	ExStatusValue 	 IN varchar  DEFAULT NULL, ' ||
	'	RC1   		  	 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' ||
	' ) AS ' ||
	' ' ||	
	'   sql_cmd varchar2(32767); ' ||
	'   mindateutc date; ' ||
	'   filtercol varchar2(100); ' ||
	'   datecol varchar2(100); ' ||
	'   sortcol varchar2(100); ' ||
	' ' ||	
	'   param1 varchar2(100) := ''p1''; ' ||
	'   param2 varchar2(100) := ''p2''; ' ||
	'   param3 varchar2(100) := ''p3''; ' ||
	' ' ||   
	'   Quote constant varchar2(1) := ''''''''; ' ||
	'   Doubled_Quote constant varchar2(2) := Quote||Quote; ' ||	
	'   CompleteStatusName varchar(100); ' ||
	' ' ||	
	'BEGIN ' || 
	' ' ||	
	'	IF( SortOrder <> ''DESC'' AND SortOrder <> ''ASC'' ) THEN ' || 
	'		RETURN; ' ||
	'	END IF; ' ||
	' ' ||	
	'	mindateutc := ' || OWNER_USER || '.UDF_LOCALTOUTC( MinDate ); ' ||	
	' ' ||	
	'	SELECT StatusFriendly INTO CompleteStatusName FROM ' || OWNER_USER || '.JobStatus WHERE (Status = ''COMPLETE''); ' ||
	' ' ||	
	'	DELETE FROM ' || OWNER_USER || '.TP_PackageJobQueue; ' ||
	' ' ||	
	'	INSERT INTO ' || OWNER_USER || '.TP_PackageJobQueue (PackageInstanceId, JobQueueID) ' ||
	'		SELECT  ' || 
	'			p.PackageInstanceId, ' ||
	'			jq.JobQueueID AS JobQueueID ' ||
	'		FROM ' || OWNER_USER || '.PackageInstance p ' ||
	'		LEFT OUTER JOIN ' || OWNER_USER || '.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId) ' ||
	'		WHERE (jq.JobQueueID IS NOT NULL) AND (p.WhenCreated >= mindateutc) ' || 
	'		UNION ' || 
	'		SELECT ' || 
	'			p.PackageInstanceId, ' ||
	'			d.JobQueueID AS JobQueueID ' ||
	'		FROM ' || OWNER_USER || '.PackageInstance p ' ||
	'		LEFT OUTER JOIN ' || OWNER_USER || '.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId) ' ||
	'		WHERE (d.JobQueueID IS NOT NULL) AND (p.WhenCreated >= mindateutc) ' || 
	'		ORDER BY JobQueueID DESC; ' ||
	' ' ||		
	'	UPDATE ' || OWNER_USER || '.TP_PackageJobQueue pt1 ' || 
	'	    SET (PageId,PageName,StatusValue,StatusName,LastStatusChange,PackageVersion,WhenCreatedLcl,FriendlyMessage) = ' ||	
	'	   ( SELECT ' || 
	'	   	 		NVL(jq.PageToProcess, d.PageId), ' || 
	'				NVL(jq.PageName, d.PageName), ' ||
	'				NVL(jq.Status, ''COMPLETE''), ' ||
	'				NVL( (SELECT js.StatusFriendly FROM ' || OWNER_USER || '.JobStatus js WHERE (js.Status = jq.Status)), CompleteStatusName ), ' ||
	'				' || OWNER_USER || '.udf_MaxDate( pt2.LastStatusChange,' || OWNER_USER || '.udf_MaxDate( ' || OWNER_USER || '.UDF_UTCTOLOCAL( d.WhenLoggedUtc), jq.LastStatusChange)), ' ||
	'				(p.PackageVersionNumber + (p.PackageRevisionNumber*0.001)), ' ||
	'				' || OWNER_USER || '.UDF_UTCTOLOCAL(p.WhenCreated ), ' ||
	' 				NVL2(jq.JobQueueID, (SELECT FriendlyMessage FROM (SELECT e.RelatedPackageInstanceId, e.JOBQUEUEID, e.FriendlyMessage FROM ' || OWNER_USER || '.ExceptionLog e ' || 
	'					WHERE e.ParentExceptionId IS NULL ORDER BY ExceptionId DESC ) ' || 
	'					WHERE (RelatedPackageInstanceId=p.PackageInstanceId)AND(JOBQUEUEID=pt2.JobQueueID) AND ROWNUM=1), NULL ) ' || 	 
	'		FROM ' || OWNER_USER || '.TP_PackageJobQueue pt2 ' || 
	' 		INNER JOIN ' || OWNER_USER || '.PackageInstance p ON (p.PackageInstanceId = pt2.PackageInstanceId) ' ||
	' 		LEFT OUTER JOIN ' || OWNER_USER || '.JobQueue jq ON (jq.PackageInstanceId = pt2.PackageInstanceId) AND jq.JobQueueID = pt2.JobQueueID ' ||
	' 		LEFT OUTER JOIN ' || OWNER_USER || '.DistributionResult d ON (d.PackageInstanceId = pt2.PackageInstanceId) AND d.JobQueueID = pt2.JobQueueID ' ||
	'		WHERE (ROWNUM = 1) AND (pt2.PackageInstanceId = pt1.PackageInstanceId) AND (pt2.JobQueueID = pt1.JobQueueID) ); ' ||
	' ' ||			
	'	sql_cmd := ''SELECT	* FROM (SELECT pt.JobQueueID,pt.PageId,pt.PageName,pt.StatusValue,pt.StatusName,pt.LastStatusChange,p.Reference,pt.PackageVersion,p.CreatedByPersonId,pt.WhenCreatedLcl,pt.FriendlyMessage,pt.PackageInstanceId '' || ' ||
	'		''FROM ' || OWNER_USER || '.TP_PackageJobQueue pt '' || ' ||
	'		''INNER JOIN ' || OWNER_USER || '.PackageInstance p ON (p.PackageInstanceId = pt.PackageInstanceId) ''; ' ||
	-- Order by clause
	'	IF( SortBy IS NOT NULL ) THEN ' ||
	'	 	sortcol:=Replace(SortBy, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||'' ORDER BY ''||sortcol||'' ''||SortOrder||'' ''; ' ||
	'	END IF; ' ||
	' ' ||	
	'	sql_cmd:=sql_cmd||'') WHERE ROWNUM <= :1 ''; ' ||
	' ' ||	
	' 	IF( FilterTextColumn IS NOT NULL AND FilterText IS NOT NULL ) THEN ' ||
	'	 	filtercol:=Replace(FilterTextColumn, Quote, Doubled_Quote);  ' ||
	' 		sql_cmd:=sql_cmd||'' AND ''||filtercol||'' LIKE :p1 ''; ' ||
	'		param1:=FilterText; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p1'''' = :p1 ''; ' ||
	' 	END IF; ' ||
	'	IF(FilterDateColumn IS NOT NULL AND FilterFromDate IS NOT NULL ) THEN ' ||
	'	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||'' AND ''||datecol||'' >= :p1 ''; ' ||
	'		param2:=FilterFromDate; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p2'''' = :p2 ''; ' ||
	'	END IF; ' ||
	'	IF( FilterDateColumn IS NOT NULL AND FilterToDate IS NOT NULL ) THEN ' ||
	'	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote); ' ||
	'		sql_cmd:=sql_cmd||'' AND ''||datecol||'' <= :p3 ''; ' ||
	'		param3 := FilterToDate; ' ||
	'	ELSE ' ||
	'		sql_cmd:=sql_cmd||'' AND ''''p3'''' = :p3 ''; ' ||
	'	END IF; ' ||
	' ' ||	
	' 	IF( ExStatusValue IS NOT NULL ) THEN ' ||
	' 		sql_cmd := sql_cmd || ''	AND pt.StatusValue NOT IN ('' || ExStatusValue || '') ''; ' ||
	' 	END IF; ' ||
	' ' ||	
	'	OPEN RC1 FOR sql_cmd USING MaxRows, param1, param2, param3; ' ||
	'END Job_GetTransactions; ');


-- Job_GetDistributions
CreateProcedure ( OWNER_USER,'Job_GetDistributions', '(' ||
	'	   p_JobQueueID number, ' ||
	'	   RC1 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||  
	'	   RC2 OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ) ' || 
	'AS ' ||
	'  v_CompleteStatusName varchar(100); ' ||
	' ' ||	
	'BEGIN ' ||
	'	SELECT StatusFriendly INTO v_CompleteStatusName FROM ' || OWNER_USER || '.JobStatus WHERE (Status = ''COMPLETE''); ' ||
	' ' ||	
	' 	OPEN RC1 FOR SELECT ' || 
	'		jq.PackageInstanceId AS PackageInstanceId, ' ||
	'		jq.JobQueueID AS JobQueueID, ' ||
	'		dq.DistributorGuid AS DistributorId, ' ||
	'		dq.DistributorName, ' ||
	'		dq.DocumentGuid AS DocumentId, ' ||
	'		dq.DocumentName, ' ||
	'		jq.Status AS StatusValue, ' ||
	'		(SELECT js.StatusFriendly FROM ' || OWNER_USER || '.JobStatus js WHERE (js.Status = jq.Status)) AS StatusName, ' ||
	'		jq.LastStatusChange, ' || 
	'		NULL AS InternalReference, ' ||
	'		NULL AS LinkText ' ||
	'	FROM ' || OWNER_USER || '.JobQueue jq ' ||
	'	LEFT OUTER JOIN ' || OWNER_USER || '.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueID) ' ||
	'	WHERE jq.JobQueueID = p_JobQueueID ' ||
	'	UNION ALL ' ||
	'	SELECT ' || 
	'		d.PackageInstanceId AS PackageInstanceId, ' ||
	'		d.JobQueueID AS JobQueueID, ' ||
	'		d.DistributorId AS DistributorId, ' ||
	'		d.DistributorName, ' ||
	'		d.TemplateId AS DocumentId, ' ||
	'		d.TemplateName AS DocumentName, ' ||
	'		''COMPLETE'', ' ||
	'		v_CompleteStatusName AS StatusName, ' ||
	' 		' || OWNER_USER || '.UDF_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange, ' ||  
	'		d.InternalReference, ' ||
	'		d.LinkText ' ||
	'	FROM ' || OWNER_USER || '.DistributionResult d ' || 
	'	WHERE d.JobQueueID = p_JobQueueID ' ||
	'	ORDER BY PackageInstanceId, JobQueueID, DistributorId, DocumentId; ' ||
	' ' ||	
	' 	OPEN RC2 FOR SELECT * FROM (SELECT ' || 
	' 		 RelatedPackageInstanceId, JOBQUEUEID, Message AS ExceptionMessage ' ||
	' 	FROM ' || OWNER_USER || '.ExceptionLog ' ||
	' 	WHERE (JOBQUEUEID = p_JobQueueID)AND(ParentExceptionId IS NULL) ORDER BY ExceptionId DESC) WHERE ROWNUM=1; ' ||
	'END Job_GetDistributions; ');
END;
	 
        
PROCEDURE AlterTable ( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2,
		 fieldtype VARCHAR2)
IS
  v_column varchar2(30);
BEGIN
	SELECT COLUMN_NAME INTO v_column FROM DBA_TAB_COLS WHERE OWNER=UPPER(lookupuser) AND TABLE_NAME=UPPER(tablename) AND COLUMN_NAME = UPPER(fieldname);
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN 
		    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser || '.' || tablename || ' Add (' || fieldname || ' ' || fieldtype || ')';
END;


PROCEDURE DeleteColumn( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2)
IS
  v_column varchar2(30);
BEGIN
	SELECT COLUMN_NAME INTO v_column FROM DBA_TAB_COLS WHERE OWNER=UPPER(lookupuser) AND TABLE_NAME=UPPER(tablename) AND COLUMN_NAME = UPPER(fieldname);

    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser || '.' || tablename || ' drop column ' || fieldname || '';
	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN RETURN;
END;

PROCEDURE RenameColumn( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldnameOld VARCHAR2,
		 fieldnameNew VARCHAR2)
IS
  v_column varchar2(30);
BEGIN
	SELECT COLUMN_NAME INTO v_column FROM DBA_TAB_COLS WHERE OWNER=UPPER(lookupuser) AND TABLE_NAME=UPPER(tablename) AND COLUMN_NAME = UPPER(fieldnameOld);
	
    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser || '.' || tablename || ' rename column ' || fieldnameOld || ' to ' || fieldnameNew || '';

	EXCEPTION
		WHEN NO_DATA_FOUND THEN RETURN;
END;

PROCEDURE RenameTable( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablenameOld  VARCHAR2,
		 tablenameNew VARCHAR2)
IS
  v_table varchar2(30);
BEGIN
	SELECT TABLE_NAME INTO v_table FROM DBA_TABLES WHERE OWNER=UPPER(lookupuser) AND TABLE_NAME = UPPER(tablenameOld);

    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser || '.' || tablenameOld || ' rename to ' || tablenameNew || '';

	EXCEPTION
		WHEN NO_DATA_FOUND THEN RETURN;
END;


PROCEDURE AlterTables 
IS
BEGIN
	 DeleteColumn ( Tabelspace, OWNER_USER, 'PACKAGEINSTANCE', 'State' );
	 RenameColumn ( Tabelspace, OWNER_USER, 'PACKAGEINSTANCE', 'CoordinatorStatusCode', 'InterviewState' );
	 RenameColumn ( Tabelspace, OWNER_USER, 'COORDINATORSTATUS', 'CoordinatorStatusCode', 'InterviewState' );
	 RenameTable  ( Tabelspace, OWNER_USER, 'COORDINATORSTATUS', 'INTERVIEWSTATUS' );
	 
 	 AlterTable ( Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'DistributorName', 'VARCHAR2(255)' ); 
 	 AlterTable ( Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'MIMEFormat', 'VARCHAR2(255)' );
 	 AlterTable ( Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'JOBQUEUEID', 'number(10)' );
 	 AlterTable ( Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'PageName', 'VARCHAR2(50)' );
 	 AlterTable ( Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'PageId', 'VARCHAR2(50)' );
	 
	 AlterTable ( Tabelspace, OWNER_USER, 'PACKAGE_', 'VersionXML', 'VARCHAR2(2048)' ); 
	 
	 AlterTable ( Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'JOBQUEUEID', 'number(10)' ); 
	 AlterTable ( Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'ERRORNUMBER', 'VARCHAR2(255)' ); 
	 AlterTable ( Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'HINT', 'VARCHAR2(1000)' ); 
	 AlterTable ( Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'CATEGORY', 'VARCHAR2(50)' );
	 
 	 CreateIndex (  Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'JOBQUEUEID' );
 	 CreateIndex (  Tabelspace, OWNER_USER, 'EXCEPTIONLOG', 'RELATEDPACKAGEINSTANCEID' );
 	 CreateIndex (  Tabelspace, OWNER_USER, 'JOBQUEUE', 'PACKAGEINSTANCEID, JOBQUEUEID' );
 	 CreateIndex (  Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'PACKAGEINSTANCEID' );
 	 CreateIndex (  Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT', 'JOBQUEUEID' );
	  
	 -- Invalid trigger and sequence in 5.6.0
     MaybeDropTrigger( OWNER_USER, 'TP_TMPDISTRIBUTIONRESULT', 'DISTRIBUTIONRESULTID'     );
End;


PROCEDURE CreateStatusPSupport 
IS
BEGIN
     CreateTemporaryTable  (OWNER_USER, 'TP_TMPINSTANCEINPROGRESS',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'INTERVIEWSTATE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );
     
     
     CreateTemporaryTable  (OWNER_USER, 'TP_TMPINSTANCE',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'INTERVIEWSTATE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );

     CreateTemporaryTable  (OWNER_USER, 'TP_TMPHISTORY',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'INTERVIEWSTATE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );

     
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPINSTANCEINPROGRESS', 'RANK'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPINSTANCE', 'RANK'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPHISTORY', 'RANK'     );
END;


PROCEDURE CreateData 
IS
  counter INT;      
BEGIN

 EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || OWNER_USER || '.JOBSTATUS WHERE STATUS = ''QUEUE'' ' INTO counter;
 IF( counter < 1 ) THEN
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''QUEUE'',''Queued'',0) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''ASSEMBLE'',''Assembling'',1) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''CONVERT'',''Converting'',2) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''DISTRIBUTE'',''Distributing'',3) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''COMPLETE'',''Completed'',5) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''ERROR'',''Failed'',6) ';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.JOBSTATUS(STATUS,STATUSFRIENDLY,RANK) values (''INVALID'',''Invalid'',100) ';
 END IF;
 
 EXECUTE IMMEDIATE 'SELECT COUNT(*) FROM ' || OWNER_USER || '.INTERVIEWSTATUS WHERE INTERVIEWSTATE = ''INTE'' ' INTO counter;
 IF( counter < 1 ) THEN
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.InterviewStatus ( InterviewState,Name,Rank ) values (''INTE'', ''Interview'', 1 )';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.InterviewStatus ( InterviewState,Name,Rank ) values (''SUSP'', ''Suspended'', 1 )';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.InterviewStatus ( InterviewState,Name,Rank ) values (''FINI'', ''Interview Finished'', 2 )';
	 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.InterviewStatus ( InterviewState,Name,Rank ) values (''FAIL'', ''Failed'', 4 )';
 END IF;
 
 EXECUTE IMMEDIATE 'update ' || OWNER_USER || '.PackageInstance set InterviewState = ''INTE'' where InterviewState is null ';
 EXECUTE IMMEDIATE 'update ' || OWNER_USER || '.PackageInstance set InterviewState = ''FINI'' where InterviewState = ''QUEU'' or InterviewState = ''PROG'' ';
 EXECUTE IMMEDIATE 'update ' || OWNER_USER || '.PackageInstance set InterviewState = ''FAIL'' where InterviewState = ''ERRO'' or InterviewState = ''WARN'' ';

 EXECUTE IMMEDIATE 'update ' || OWNER_USER || '.InterviewStatus set Name=''Jobs Completed'', Rank=3 where InterviewState=''DONE'' ';

 EXECUTE IMMEDIATE 'delete from ' || OWNER_USER || '.InterviewStatus where InterviewState in (''ERRO'',''QUEU'',''PROG'',''WARN'' )';


 EXECUTE IMMEDIATE 'delete from ' || OWNER_USER || '.versioninformation';
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.versioninformation (Major,Minor,Build,Revision,SequenceNumber) Values (6,0,0,0,25)';
END;

----------------------------------------------
----------------------------------------------
        
BEGIN
	CreateAllTables; 
   	CreateSequnces;
   	CreatePrimaryKeys;
   	CreateForeignKeys;
 	AlterTables;
 	CreateFunctions;
 	CreateProcedures;
 	CreateAdminProcedures;
 	CreateData;
 	CreateStatusPSupport;
  	UpdateProcedures;
END;
/
