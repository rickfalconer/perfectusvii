CREATE OR REPLACE PROCEDURE APP_PERFECTUS.Instance_GetDistributions ( 
	   p_PackageInstanceId varchar, 
	   RC1 OUT APP_PERFECTUS.PERFECTUS_GLOBAL.REF_CUR,  
	   RC2 OUT APP_PERFECTUS.PERFECTUS_GLOBAL.REF_CUR ) 
AS
  v_CompleteStatusName varchar(100);

BEGIN 
	SELECT StatusFriendly INTO v_CompleteStatusName FROM APP_PERFECTUS.JobStatus WHERE (Status = 'COMPLETE'); 
 	
 	OPEN RC1 FOR SELECT  
 		p.PackageInstanceId, 
 		jq.JobQueueID AS JobQueueID, 
 		jq.PageToProcess AS PageId, 
 		jq.PageName, 
 		dq.DistributorGuid AS DistributorId, 
 		dq.DistributorName, 
 		dq.DocumentGuid AS DocumentId, 
 		dq.DocumentName, 
 		jq.Status AS StatusValue, 
 		(SELECT js.StatusFriendly FROM APP_PERFECTUS.JobStatus js WHERE (js.Status = jq.Status)) AS StatusName, 
 		jq.LastStatusChange,  
 		NULL AS InternalReference, 
 		NULL AS LinkText 
 	FROM APP_PERFECTUS.PackageInstance p 
 	INNER JOIN APP_PERFECTUS.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId) 
 	LEFT OUTER JOIN APP_PERFECTUS.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueID) 
 	WHERE p.PackageInstanceId = p_PackageInstanceId 
 	UNION ALL  
 	SELECT  
 		p.PackageInstanceId, 
 		d.JobQueueID AS JobQueueID, 
 		d.PageId, 
 		d.PageName, 
 		d.DistributorId, 
 		d.DistributorName, 
 		d.TemplateId AS DocumentId, 
 		d.TemplateName AS DocumentName, 
 		'COMPLETE', 
 		v_CompleteStatusName AS StatusName, 
 		APP_PERFECTUS.UDF_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange,   
 		d.InternalReference, 
 		d.LinkText 
 	FROM APP_PERFECTUS.PackageInstance p 
 	INNER JOIN APP_PERFECTUS.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId) 
 	WHERE p.PackageInstanceId = p_PackageInstanceId 
 	ORDER BY JobQueueID;  
  
 	OPEN RC2 FOR SELECT * FROM (SELECT  
 		 RelatedPackageInstanceId, JOBQUEUEID, Message AS ExceptionMessage 
 	FROM APP_PERFECTUS.ExceptionLog 
 	WHERE (RelatedPackageInstanceId = p_PackageInstanceId)AND(ParentExceptionId IS NULL) ORDER BY ExceptionId DESC ) WHERE ROWNUM=1; 
END Instance_GetDistributions; 

