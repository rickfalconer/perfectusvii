PROMPT *-------------------------------------*
PROMPT * Installation script FOR             *
PROMPT *     Perfectus 5.6.0.0               *
PROMPT *     Standard                        *
PROMPT *-------------------------------------*

PROMPT * Please enter the DATABASE path ON the server (e.g. "c:/oracle/oradata/") *
ACCEPT askTabelspacePathName PROMPT 'Enter the database path on the server: ' 

PROMPT * Please enter the TABLESPACE name (e.g. "PerfectusTS") *
ACCEPT askTabelspace PROMPT 'Enter the tablespace name: ' 

PROMPT * Please enter the perfectus username (e.g. App_Perfectus") *
ACCEPT askOWNER_USER PROMPT 'Enter the perfectus standard username: ' 

DECLARE 
        TabelspacePathName VARCHAR2 (255) := '&askTabelspacePathName';
        Tabelspace VARCHAR2 ( 120) := '&askTabelspace';
        OWNER_USER VARCHAR2 ( 120):= '&askOWNER_USER';
        Success BOOLEAN := TRUE;
----------------------------------------------
    
FUNCTION ReCreateTablespace (
         TablespaceName VARCHAR2,
         DataFilePathname VARCHAR2
) 
RETURN
      BOOLEAN
IS
  counter INT;      
  dbFilePathname VARCHAR2 (1000);
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tablespaces WHERE tablespace_name = 

UPPER(trim(TablespaceName));
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop tablespace ' || Tabelspace || ' including contents and 

datafiles';
    END IF;
    
    dbFilePathname := DataFilePathname || TablespaceName || '.dbf'; 
    EXECUTE IMMEDIATE 'CREATE TABLESPACE ' || Tabelspace || ' LOGGING  DATAFILE ''' || 

dbFilePathname || ''' SIZE 200M REUSE  AUTOEXTEND ON NEXT 5M MAXSIZE UNLIMITED';    
    RETURN TRUE;
END;

----------------------------------------------

FUNCTION ReCreateUser (
         lookupuser VARCHAR2,
         isOwner BOOLEAN,
         tablespacename VARCHAR2 )
RETURN
      BOOLEAN
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_users WHERE username = UPPER(trim(lookupuser));
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop user ' || lookupuser || ' cascade';
    END IF;
    EXECUTE IMMEDIATE 'CREATE USER ' || lookupuser || ' IDENTIFIED BY ' || lookupuser || ' default TABLESPACE ' || tablespacename;
    
    IF isOwner THEN
       EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, CREATE PUBLIC SYNONYM, CREATE VIEW TO ' || 

lookupuser;
    ELSE
        EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, CREATE VIEW TO ' || lookupuser;
    END IF;
    RETURN TRUE;
END;     

----------------------------------------------

FUNCTION ReCreatePackages (
         lookupuser VARCHAR2 )
RETURN
      BOOLEAN
IS
  command VARCHAR2 (1000);
BEGIN
     command := 'CREATE OR REPLACE PACKAGE ' || lookupuser || '.PERFECTUS_GLOBAL AUTHID CURRENT_USER AS TYPE REF_CUR IS REF CURSOR; END PERFECTUS_GLOBAL;';
     EXECUTE IMMEDIATE command;
RETURN TRUE;
END;     

PROCEDURE CreateTable ( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND 

UPPER (table_name)=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;

    EXECUTE IMMEDIATE 'CREATE TABLE ' || lookupuser || '.' || tablename || tabledefinition || ' 

TABLESPACE ' || tablespacename;
END;            


PROCEDURE CreateTemporaryTable ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND 

UPPER (table_name)=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;

    EXECUTE IMMEDIATE 'CREATE GLOBAL TEMPORARY TABLE ' || lookupuser || '.' || tablename || tabledefinition ||
    'ON COMMIT PRESERVE ROWS';
END;            


----------------------------------------------

PROCEDURE CreateSequenceAndTrigger ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  seqName VARCHAR2 (200 );
  triggerName   VARCHAR2 (200 );
BEGIN
     seqName := UPPER(lookupuser) || '.SEQ' || UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2));
       SELECT COUNT(*) INTO counter FROM dba_sequences WHERE UPPER(sequence_name) = 'SEQ' || 

UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2)) AND sequence_owner=UPPER(lookupuser);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'DROP SEQUENCE '||seqName ;
    END IF;
     EXECUTE IMMEDIATE 'CREATE SEQUENCE ' || seqName || ' MINVALUE 0 START  WITH 1 ';

     triggerName := UPPER(lookupuser) || '.TRG' || UPPER(tablename) || '_' || 

UPPER(SUBSTR(fieldname,1,2));
     EXECUTE IMMEDIATE ' CREATE or replace TRIGGER ' || triggerName || ' BEFORE INSERT ON ' || 

UPPER(lookupuser) || '.' || UPPER(tablename) || 
                         ' FOR EACH ROW ' || 
                       ' BEGIN ' ||  
                       ' SELECT ' || seqName ||'.nextval INTO :new.' || fieldname 

|| ' FROM dual; END; ';
END;

----------------------------------------------

PROCEDURE CreatePrimaryKey ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  keyName VARCHAR2 (200 );
BEGIN
     keyName := 'PK_' || UPPER(tablename);

       SELECT COUNT(*) INTO counter FROM dba_indexes WHERE UPPER(index_name) = keyName AND 

owner=UPPER(lookupuser) AND table_name=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' drop CONSTRAINT 

' || keyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' ADD ( CONSTRAINT ' || 

keyName || ' PRIMARY KEY ( ' || fieldname || ' ) ) ';
END;                    

----------------------------------------------

PROCEDURE CreateForeignKey (
          sourceuser VARCHAR2,
         sourcetable VARCHAR2,
         sourcefields VARCHAR2, 
          targetuser VARCHAR2,
         targettable VARCHAR2,
         targetfields VARCHAR2)
IS
  counter INT;      
  foreignKeyName VARCHAR2 (200 );
BEGIN
     foreignKeyName := 'FK_' || UPPER(SUBSTR(sourcetable,1,10)) || '_' || UPPER 

(SUBSTR(targettable,1,10));
     SELECT COUNT (*) INTO counter FROM dba_constraints WHERE owner=UPPER(sourceuser) AND 

constraint_type='R' 
                                AND table_name=UPPER(sourcetable) AND 

foreignKeyName=constraint_name;
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' drop 

CONSTRAINT ' || foreignKeyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' ADD ( CONSTRAINT ' || 
                       foreignKeyName || ' FOREIGN KEY ( ' || sourcefields || ' ) 

' ||
                      'REFERENCES ' || targetuser ||'.'|| targettable || ' ( ' ||
                      targetfields || ' ) ON DELETE CASCADE ) ';
END;                    

----------------------------------------------
----------------------------------------------

PROCEDURE CreateAllTables 
IS
BEGIN
     CreateTable  (Tabelspace, OWNER_USER, 'COORDINATORSTATUS',  
     '(COORDINATORSTATUSCODE CHAR (4) NOT NULL,' ||
     ' NAME VARCHAR2 (100) NOT NULL, ' ||
     ' RANK NUMBER (10,0) NOT NULL)'); 
     
     CreateTable  (Tabelspace, OWNER_USER, 'DISTRIBUTIONRESULT',
     '(DISTRIBUTIONRESULTID NUMBER (10,0) NOT NULL,' ||
     ' DISTRIBUTORID VARCHAR2(50) NOT NULL,' ||
     ' PACKAGEINSTANCEID VARCHAR2(50) NOT NULL,' ||
     ' TEMPLATEID VARCHAR2(50),' ||
     ' TEMPLATENAME VARCHAR2 (1000),' || 
     ' LINK VARCHAR2 (1000),' ||
     ' LINKTEXT VARCHAR2 (1000),' ||
     ' INTERNALREFERENCE VARCHAR2 (1000),' ||
     ' WHENLOGGEDUTC DATE NOT NULL)' );
     
     CreateTable  (Tabelspace, OWNER_USER, 'EXCEPTIONLOG',
     '(EXCEPTIONID NUMBER (10,0) NOT NULL,' || 
     'SEVERITYCODE CHAR (4) NOT NULL,' || 
     'WHENLOGGEDUTC TIMESTAMP NOT NULL,PERSONID NUMBER (10,0),' ||  
     'RELATEDPACKAGEID VARCHAR2(50)  ,' || 
     'RELATEDPACKAGEVERSIONNUMBER NUMBER (10,0)  ,' || 
     'RELATEDPACKAGEREVISIONNUMBER NUMBER (10,0)  ,' || 
     'RELATEDPACKAGEINSTANCEID VARCHAR2(50)  ,' || 
     'FRIENDLYMESSAGE VARCHAR2 (510) NOT NULL,' || 
     'MESSAGE VARCHAR2 (510),' || 
     'STACKTRACE VARCHAR2(2500)  ,' || 
     'RAISINGASSEMBLYNAME VARCHAR2 (50)  ,' || 
     'RAISINGASSEMBLYPATH VARCHAR2 (500)  ,' || 
     'RAISINGASSEMBLYVERSION VARCHAR2 (20)  ,' || 
     'PARENTEXCEPTIONID NUMBER (10,0)  )');

     CreateTable  (Tabelspace, OWNER_USER, 'NAVIGATIONSTACK', '( ' ||
     'PACKAGEINSTANCEID  VARCHAR2(50)          NOT NULL,' ||
     'POSITIONINSTACK    NUMBER(9)                  NOT NULL,' ||
     'PAGEID             VARCHAR2(50), ' ||
     'PAGENAME           VARCHAR2(1000),' ||
     'PENDINGDELETION    NUMBER(1)                  DEFAULT 0                     NOT NULL' ||
     '  )');

     CreateTable  (Tabelspace, OWNER_USER, 'OUTCOMEACTION', '( ' ||
     'PACKAGEID            VARCHAR2(50)          NOT NULL,' ||
     'PACKAGEVERSIONNUMBER   NUMBER(10)                  NOT NULL,' ||
     'PACKAGEREVISIONNUMBER  NUMBER(10)                  NOT NULL,' ||
     'OUTCOMEID          VARCHAR2(50), ' ||
     'OUTCOMEACTIONID     VARCHAR2(50), ' ||
     'NAME                VARCHAR2(1000),' ||
     'OUTCOMEACTIONBYTES BLOB' ||
     '  )');
     
     CreateTable  (Tabelspace, OWNER_USER, 'PACKAGE_', '( ' ||
     'PACKAGEID              VARCHAR2(50 BYTE)      NOT NULL,' ||
     'PACKAGEVERSIONNUMBER   NUMBER(10)             NOT NULL,' ||
     'PACKAGEREVISIONNUMBER  NUMBER(10)             NOT NULL,' ||
     'PACKAGEBYTES           BLOB                   NOT NULL,' ||
     'NAME                   VARCHAR2(50 BYTE)      NOT NULL,' ||
     'DESCRIPTION            VARCHAR2(1000 BYTE),' ||
     'STARTPAGENAME          VARCHAR2(100 BYTE),' ||
     'STARTPAGEID            VARCHAR2(50 BYTE),' ||
     'CREATEDBY              VARCHAR2(510 BYTE),' ||
     'MODIFIEDBY             VARCHAR2(510 BYTE),' ||
     'PUBLISHEDBY            VARCHAR2(510 BYTE),' ||
     'WHENCREATED            DATE,' ||
     'WHENMODIFIED           DATE,' ||
     'WHENPUBLISHED          DATE,' ||
     'WHENDELETED            DATE,' ||
     'DELETEDBYPERSONID      VARCHAR2(510 BYTE)' ||
     '  )');

     
     CreateTable  (Tabelspace, OWNER_USER, 'PACKAGEINSTANCE', '( ' ||
     'PACKAGEINSTANCEID      VARCHAR2(50 BYTE)      NOT NULL,' ||
     'PACKAGEID              VARCHAR2(50 BYTE)      NOT NULL,' ||
     'PACKAGEVERSIONNUMBER   NUMBER(10)             NOT NULL,' ||
     'PACKAGEREVISIONNUMBER  NUMBER(10)             NOT NULL,' ||
     'REFERENCE              VARCHAR2(50 BYTE)     NOT NULL,' ||
     'STATE                  BLOB,' ||
     'CREATEDBYPERSONID      VARCHAR2(510 BYTE)     NOT NULL,' ||
     'WHENCREATED            DATE                   NOT NULL,' ||
     'WHENCOMPLETED          DATE,' ||
     'WHENDELETED            DATE,' ||
     'DELETEDBYPERSONID      VARCHAR2(510 BYTE),' ||
     'COORDINATORSTATUSCODE  CHAR(4 BYTE),' ||
     'ANSWERSTATE            BLOB,' ||
     'ISSYNCHRONIZED         NUMBER(4) DEFAULT 0' ||
      '  )');

     CreateTable  (Tabelspace, OWNER_USER, 'PACKAGEINSTANCE_FILE', '( ' ||
     'PACKAGEINSTANCEID  VARCHAR2(50 BYTE)          NOT NULL,' ||
     'SEQUENCENUMBER     NUMBER(10)                 NOT NULL,' ||
     'QUESTIONUID        VARCHAR2(50 BYTE)          NOT NULL,' ||
     'ORIGINALFILEPATH   VARCHAR2(1000 BYTE)        NOT NULL,' ||
     'CONTENTTYPE        VARCHAR2(100 BYTE)         NOT NULL,' ||
     'FILEDATA           BLOB                       NOT NULL' ||
       '  )');

     CreateTable  (Tabelspace, OWNER_USER, 'PREVIEWIMAGE', '( ' ||
     'IMAGEID    VARCHAR2(50 BYTE)                  NOT NULL,' ||
     'IMAGEDATA  BLOB                               NOT NULL' ||
       '  )');

     CreateTable  (Tabelspace, OWNER_USER, 'SEVERITYCODE', '( ' ||
     'SEVERITYCODE  CHAR(4 BYTE)                    NOT NULL,' ||
     'NAME          VARCHAR2(100 BYTE)' ||
       '  )');

      CreateTable  (Tabelspace, OWNER_USER, 'TEMPLATE', '( ' ||
     'PACKAGEID              VARCHAR2(50 BYTE)      NOT NULL,' ||
     'PACKAGEVERSIONNUMBER   NUMBER(10)             NOT NULL,' ||
     'PACKAGEREVISIONNUMBER  NUMBER(10)             NOT NULL,' ||
     'TEMPLATEID             VARCHAR2(50 BYTE)      NOT NULL,' ||
     'NAME                   VARCHAR2(100 BYTE)     NOT NULL,' ||
     'TEMPLATEBYTES          BLOB                   NOT NULL' ||
       '  )');

      CreateTable  (Tabelspace, OWNER_USER, 'VERSIONINFORMATION', '( ' ||
     'MAJOR           NUMBER(10)                    NOT NULL,' ||
     'MINOR           NUMBER(10)                    NOT NULL,' ||
     'BUILD           NUMBER(10)                    NOT NULL,' ||
     'REVISION        NUMBER(10)                    NOT NULL,' ||
     'SEQUENCENUMBER  NUMBER(10)                    NOT NULL' ||
       '  )');

END;

PROCEDURE CreateSequnces
IS
BEGIN
     CreateSequenceAndTrigger ( OWNER_USER, 'EXCEPTIONLOG', 'EXCEPTIONID'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'DISTRIBUTIONRESULT', 'DISTRIBUTIONRESULTID' );
END;


PROCEDURE CreatePrimaryKeys
IS
BEGIN
     CreatePrimaryKey ( OWNER_USER, 'COORDINATORSTATUS', 'COORDINATORSTATUSCODE' );
     CreatePrimaryKey ( OWNER_USER, 'DISTRIBUTIONRESULT', 'DISTRIBUTIONRESULTID' );
     CreatePrimaryKey ( OWNER_USER, 'EXCEPTIONLOG', 'EXCEPTIONID' );
     CreatePrimaryKey ( OWNER_USER, 'NAVIGATIONSTACK', 'PACKAGEINSTANCEID, POSITIONINSTACK' );
     CreatePrimaryKey ( OWNER_USER, 'OUTCOMEACTION', 'PACKAGEID, PACKAGEVERSIONNUMBER, 

PACKAGEREVISIONNUMBER, OUTCOMEID, OUTCOMEACTIONID' );
     CreatePrimaryKey ( OWNER_USER, 'PACKAGE_', 'PACKAGEID, PACKAGEVERSIONNUMBER, 

PACKAGEREVISIONNUMBER' );
     CreatePrimaryKey ( OWNER_USER, 'PACKAGEINSTANCE', 'PACKAGEINSTANCEID' );
     CreatePrimaryKey ( OWNER_USER, 'PACKAGEINSTANCE_FILE', 'PACKAGEINSTANCEID, SEQUENCENUMBER, 

QUESTIONUID' );
     CreatePrimaryKey ( OWNER_USER, 'PREVIEWIMAGE', 'IMAGEID' );
     CreatePrimaryKey ( OWNER_USER, 'SEVERITYCODE', 'SEVERITYCODE' );
     CreatePrimaryKey ( OWNER_USER, 'TEMPLATE', 'PACKAGEID, PACKAGEVERSIONNUMBER, 

PACKAGEREVISIONNUMBER, TEMPLATEID' );
END;


----------------------------------------------


 PROCEDURE CreateForeignKeys
 IS
 BEGIN
                        
       CreateForeignKey (OWNER_USER,'DISTRIBUTIONRESULT','PackageInstanceId',
                             OWNER_USER,'PACKAGEINSTANCE','PackageInstanceId');

       CreateForeignKey (OWNER_USER,'EXCEPTIONLOG','ParentExceptionId',
                             OWNER_USER,'EXCEPTIONLOG','ExceptionId');

       CreateForeignKey (OWNER_USER,'EXCEPTIONLOG','RelatedPackageInstanceId',
                           OWNER_USER,'PACKAGEINSTANCE','PackageInstanceId');

       CreateForeignKey (OWNER_USER,'EXCEPTIONLOG','SeverityCode',
                           OWNER_USER,'SEVERITYCODE','SeverityCode');

       CreateForeignKey (OWNER_USER,'NAVIGATIONSTACK','PackageInstanceId',
                           OWNER_USER,'PACKAGEINSTANCE','PackageInstanceId');

       CreateForeignKey (OWNER_USER,'OUTCOMEACTION','PackageId,PackageVersionNumber,PackageRevisionNumber', 
                        OWNER_USER,'PACKAGE_','PackageId,PackageVersionNumber,PackageRevisionNumber');

       CreateForeignKey (OWNER_USER,'PACKAGEINSTANCE','CoordinatorStatusCode',
                           OWNER_USER,'COORDINATORSTATUS','CoordinatorStatusCode');

       CreateForeignKey (OWNER_USER,'PACKAGEINSTANCE_FILE','PackageInstanceId',
                           OWNER_USER,'PACKAGEINSTANCE','PackageInstanceId');

       CreateForeignKey 

(OWNER_USER,'PACKAGEINSTANCE','PackageId,PackageVersionNumber,PackageRevisionNumber', 
                           

OWNER_USER,'PACKAGE_','PackageId,PackageVersionNumber,PackageRevisionNumber');

       CreateForeignKey 

(OWNER_USER,'TEMPLATE','PackageId,PackageVersionNumber,PackageRevisionNumber',
                          

OWNER_USER,'PACKAGE_','PackageId,PackageVersionNumber,PackageRevisionNumber');
                        
END;

 


PROCEDURE CreateProcedure (
          sourceuser VARCHAR2,
         procedurename VARCHAR2,
         pbody VARCHAR2)
IS
BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE PROCEDURE ' || sourceuser ||'.'|| procedurename || pbody; 
END;                    






 PROCEDURE CreateProcedures
 IS
 BEGIN
    CreateProcedure ( OWNER_USER,'package_insert', '(' ||
     '   isnewversion              IN       NUMBER,' ||
     '   packageid                 IN       VARCHAR2,' ||
     '   NAME                      IN       VARCHAR2,' ||
     '   startpagename             IN       VARCHAR2,' ||
     '   startpageid               IN       VARCHAR2,' ||
     '   createdby                 IN       VARCHAR2,' ||
     '   modifiedby                IN       VARCHAR2,' ||
     '   publishedby               IN       VARCHAR2,' ||
     '   whencreated               IN       DATE,' ||
     '   whenmodified              IN       DATE,' ||
     '   whenpublished             IN       DATE,' ||
     '   description               IN       VARCHAR2,' ||
     '   packagebytesnotemplates   IN       BLOB,' ||
     '   newrevisionnumber         IN OUT   NUMBER,' ||
     '   newversionnumber          IN OUT   NUMBER' ||
     ') ' ||
     'AS ' ||
     '   revisionnumber   NUMBER (10, 0); ' ||
     '   versionnumber    NUMBER (10, 0); ' ||
     '   v_packageid        VARCHAR2(50); ' ||
     '   existing_count    number(10, 0); ' ||
     'BEGIN ' ||
     '   v_packageid := packageid; ' ||

     '	SELECT MAX(packageversionnumber) INTO versionnumber FROM ' || OWNER_USER || '.PACKAGE_ ' ||
     '  WHERE packageid = v_packageid; ' ||
	 '  IF versionnumber IS NULL THEN ' ||
     '		versionnumber := 1; ' ||
     '  ELSE ' || 
	 '		BEGIN ' ||
	 '		    IF isnewversion = 1 THEN ' ||
     '			  versionnumber := versionnumber + 1; ' ||
     '			END IF; ' ||
     '		END; ' ||
     '  END IF; ' ||

     '   SELECT   1 + CASE WHEN (MAX(PackageRevisionNumber) IS NOT NULL) THEN ' ||
     '                        MAX(PackageRevisionNumber) ' ||
     '                     ELSE ' ||
     '                        -1 ' ||
     '                     END ' ||
     '   INTO     revisionnumber ' ||
     '   FROM     ' || OWNER_USER || '.package_ ' ||
     '   WHERE    packageid = v_packageid ' ||
     '   AND      packageversionnumber = versionnumber; ' ||
     '   BEGIN ' ||
     '      INSERT INTO package_ ' ||
     '                  (packageid, packageversionnumber, packagerevisionnumber, ' ||
     '                   packagebytes, NAME, description, startpagename, ' ||
     '                   startpageid, createdby, modifiedby, publishedby, ' ||
     '                   whencreated, whenmodified, whenpublished ' ||
     '                  ) ' ||
     '           VALUES (v_packageid, versionnumber, revisionnumber, ' ||
     '                   packagebytesnotemplates, substr(NAME,1,50), substr(description,1,1000), startpagename, ' ||
     '                   startpageid, upper(substr(createdby,1,510)), substr(modifiedby,1,510), substr(publishedby,1,510), ' ||
     '                   whencreated, whenmodified, whenpublished ' ||
     '                  ); ' ||
     '   END; ' ||
     '   newversionnumber := versionnumber; ' ||
     '   newrevisionnumber := revisionnumber; ' ||
     'END package_insert; ');
     
     CreateProcedure ( OWNER_USER,'INSTANCE_GET', '(' ||
     'packageInstanceId     IN VARCHAR2, ' ||
     'currentPageId     OUT VARCHAR2, ' ||
     'cur_result     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
          'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     '        OPEN cur_result FOR ' ||
     '        SELECT State ' ||
     '         FROM ' || OWNER_USER || '.PackageInstance ' || 
     '            WHERE PackageInstanceId = v_packageinstanceid ' ||  
     '             AND WhenDeleted IS NULL; ' ||
     '        FOR rec IN ( SELECT   PageId ' ||
     '             FROM ' || OWNER_USER || '.NavigationStack ' || 
     '                                            WHERE PackageInstanceId = v_packageinstanceid ' ||  
     '                                        ORDER BY PositionInStack DESC) ' ||
     '        LOOP ' ||
     '           currentPageId := rec.PageId ; ' || 
     '        END LOOP; ' ||
     'END INSTANCE_GET; ');
     

     
    CreateProcedure ( OWNER_USER,'package_template_insert', '(' || 
     'PackageId     IN VARCHAR2, ' ||
     'PackageVersionNumber     IN NUMBER, ' ||
     'PackageRevisionNumber     IN NUMBER, ' ||
     'Name     IN VARCHAR2, ' ||
     'TemplateId     IN VARCHAR2, ' ||
     'TemplateBytes     IN BLOB) ' ||
     'AS ' ||
     'v_PackageId     VARCHAR2(50); ' ||
     'v_name varchar2(100);' ||
     'v_PackageVersionNumber      NUMBER; ' ||
     'v_PackageRevisionNumber      NUMBER; ' ||
     'v_TemplateId      VARCHAR2(50); ' ||
     'v_TemplateBytes      BLOB; ' ||
     'BEGIN ' ||
     '  v_packageid := packageid; ' ||
     '	v_name := name;' ||
     '	v_PackageVersionNumber :=PackageVersionNumber; ' ||
     '	v_PackageRevisionNumber := PackageRevisionNumber;' ||
     '	v_TemplateId := TemplateId;' ||
     '	v_TemplateBytes := TemplateBytes; ' ||
     '        INSERT INTO ' || OWNER_USER || '.Template (PackageId, PackageVersionNumber, PackageRevisionNumber, TemplateId, Name, TemplateBytes) ' ||
     '            VALUES (v_PackageId, v_PackageVersionNumber, v_PackageRevisionNumber, v_TemplateId, v_name, v_TemplateBytes); ' ||
     'END PACKAGE_TEMPLATE_INSERT;');

     
         CreateProcedure ( OWNER_USER,'PACKAGE_OUTCOMEACTION_INSERT', '(' || 
     'PackageId     IN VARCHAR2, ' ||
     'PackageVersionNumber     IN NUMBER, ' ||
     'PackageRevisionNumber     IN NUMBER, ' ||
     'Name     IN VARCHAR2, ' ||
     'OutcomeId     IN VARCHAR2, ' ||
     'OutcomeActionId     IN VARCHAR2, ' ||
     'OutcomeActionBytes     IN BLOB) ' ||
     'AS ' ||
     'BEGIN ' ||
     '    BEGIN ' ||
     '        INSERT INTO ' || OWNER_USER || '.OutcomeAction (PackageId, PackageVersionNumber, PackageRevisionNumber, OutcomeId, OutcomeActionId, Name, OutcomeActionBytes) ' ||
     '            VALUES (PackageId,     PackageVersionNumber,         PackageRevisionNumber,  ' ||
     '                        OutcomeId,                 OutcomeActionId,                     Name,                         OutcomeActionBytes); ' ||
     '    END; ' ||
     'END PACKAGE_OUTCOMEACTION_INSERT; ');


         CreateProcedure ( OWNER_USER,'INSTANCE_CREATE', '(' ||      
     'packageId     IN VARCHAR2, ' ||
     'version     IN NUMBER, ' ||
     'revision     IN NUMBER, ' ||
     'whenMadeUtc     IN DATE, ' ||
     'createdBy     IN VARCHAR2, ' ||
     'Instanceid     IN VARCHAR2) ' ||
     'AS ' ||
     'v_packageid    varchar2(50); ' ||
     'v_createdBy varchar2(255); ' ||
     'BEGIN ' ||
     'v_packageid := packageid; ' ||
     'v_createdBy := upper(createdby); ' ||
     'BEGIN ' ||
     'INSERT INTO ' || OWNER_USER || '.PackageInstance (PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, State, WhenCreated, CreatedByPersonId, reference) ' ||
     '    SELECT  instanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, ' ||
     '            PackageBytes, whenMadeUtc, v_createdBy, name ' ||
     '                 FROM ' || OWNER_USER || '.Package_ ' ||
     '                        WHERE PackageId = v_packageid ' ||
     '                         AND PackageVersionNumber = version ' ||
     '                         AND PackageRevisionNumber = revision; ' ||
     '        IF  SQL%ROWCOUNT <> 1 THEN ' ||
     '            GOTO Err; ' ||
     '        END IF; ' ||
     '        INSERT INTO ' || OWNER_USER || '.NavigationStack (PackageInstanceId, PositionInStack, PageId, PageName) ' ||
     '            SELECT  instanceId, 0, startPageId, startPageName ' ||
     '                 FROM ' || OWNER_USER || '.Package_ ' ||
     '                        WHERE PackageId = v_packageid ' ||
     '                         AND PackageVersionNumber = version ' ||
     '                         AND PackageRevisionNumber = revision; ' ||
     '        IF  SQL%ROWCOUNT <> 1 THEN ' ||
     '            GOTO Err; ' ||
     '        END IF; ' ||
     '        GOTO Finished; ' ||
     '    <<Err>> NULL; ' ||
     '    raise_application_error(-20999, ''Instance was not created.''); ' ||
     '    <<Finished>> NULL;' ||
     '    END; ' ||
     'END INSTANCE_CREATE;');
 
 

         CreateProcedure ( OWNER_USER,'NAVSTACK_PEEK', '(' ||      
     'PackageInstanceId     IN VARCHAR2, ' ||
     'PageId     OUT VARCHAR2) ' ||
     'AS ' ||
     'v_packageinstanceid        varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid :=    PackageInstanceId; ' ||
     '    BEGIN ' ||
     '        FOR rec IN ( SELECT   PageId FROM ' || OWNER_USER || '.NavigationStack WHERE PackageInstanceId = v_packageinstanceid   ' ||
     '            AND PositionInStack = ( SELECT  MAX(PositionInStack) FROM ' || OWNER_USER || '.NavigationStack   ' ||
     '                WHERE PackageInstanceId = v_packageinstanceid  AND PendingDeletion = 0  )) ' ||
     '        LOOP ' ||
     '           PageId := rec.PageId ;  ' ||
     '        END LOOP; ' ||
     '    END; ' ||
     'END NAVSTACK_PEEK;');


         CreateProcedure ( OWNER_USER,'INSTANCE_GETANSWERSTATE', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  cur_OUT OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'OPEN cur_OUT FOR ' ||
     '    SELECT AnswerState FROM ' || OWNER_USER || '.PACKAGEINSTANCE WHERE PackageInstanceId = v_packageinstanceid AND WhenDeleted IS NULL; ' ||
     'END "INSTANCE_GETANSWERSTATE";');


         CreateProcedure ( OWNER_USER,'INSTANCE_RESUMED', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'UPDATE ' || OWNER_USER || '.PACKAGEINSTANCE SET WhenCompleted = null, isSynchronized = 0 WHERE PackageInstanceId = v_packageinstanceid; ' ||
     'END "INSTANCE_RESUMED";');


         CreateProcedure ( OWNER_USER,'INSTANCE_VERSION_GET', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  VersionNumber     IN OUT NUMBER, ' ||
     '  RevisionNumber     IN OUT NUMBER) ' ||
     '  AS ' ||
     '  v_packageinstanceid        varchar2(50); ' ||
     '  BEGIN ' ||
     '  v_packageinstanceid := PackageInstanceId; ' ||
     '          FOR rec IN ( SELECT   PackageVersionNumber,  PackageRevisionNumber ' ||
     '                                   FROM ' || OWNER_USER || '.PackageInstance   ' ||
     '                                      WHERE PackageInstanceId = v_packageinstanceid) ' ||
     '          LOOP ' ||
     '  VersionNumber := rec.PackageVersionNumber ;  ' ||
     '  RevisionNumber := rec.PackageRevisionNumber ;  ' ||
     '          END LOOP; ' ||
     '  END "INSTANCE_VERSION_GET";');
    
    
    
         CreateProcedure ( OWNER_USER,'INSTANCE_GETREFERENCE', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||   
     '  Reference     IN OUT varchar2 ) ' ||
     '  AS ' ||
     '  v_packageinstanceid        varchar2(50); ' ||
     '  BEGIN ' ||
     '  v_packageinstanceid := PackageInstanceId; ' ||
     '          FOR rec IN ( SELECT  Reference FROM ' || OWNER_USER || '.PackageInstance   WHERE PackageInstanceId = v_packageinstanceid) ' ||
     '          LOOP ' ||
     '  Reference := rec.Reference ;  ' ||
     '          END LOOP; ' ||
     '  END "INSTANCE_GETREFERENCE";');



         CreateProcedure ( OWNER_USER,'NAVSTACK_GET', '(' ||      
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  cur_OUT OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'OPEN cur_OUT FOR ' ||
     '  SELECT PageId, PositionInStack, PageName FROM ' || OWNER_USER || '.NavigationStack WHERE PackageInstanceId = v_packageinstanceid AND PendingDeletion = 0 ' ||
     '  ORDER BY PositionInStack ASC; ' ||
     'END "NAVSTACK_GET";');


         CreateProcedure ( OWNER_USER,'NAVSTACK_GETGHOST', '(' ||
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  cur_OUT OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'OPEN cur_OUT FOR ' ||
     '        SELECT PageId ,  PositionInStack ,  PageName FROM ' || OWNER_USER || '.NavigationStack  WHERE PackageInstanceId = v_packageinstanceid  AND PendingDeletion = 1   ' ||
     '  ORDER BY PositionInStack ASC; ' ||
     'END "NAVSTACK_GETGHOST";');


         CreateProcedure ( OWNER_USER,'INSTANCE_TEMPLATELIST_GET', '(' ||
     '  INSTANCEID IN VARCHAR2, ' ||
     '  cur_OUT OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'BEGIN ' ||
     '    OPEN cur_OUT FOR SELECT t.TemplateId, t.Name FROM ' || OWNER_USER || '.Template t ' ||
     '    INNER JOIN ' || OWNER_USER || '.Package_ p ON p.PackageId = t.PackageId AND p.PackageVersionNumber = t.PackageVersionNumber AND p.PackageRevisionNumber = t.PackageRevisionNumber ' ||
     '    INNER JOIN ' || OWNER_USER || '.PackageInstance pi ON p.PackageId = pi.PackageId AND p.PackageVersionNumber = pi.PackageVersionNumber AND p.PackageRevisionNumber = pi.PackageRevisionNumber ' ||
     'WHERE pi.PackageInstanceId = INSTANCEID; ' ||
     'END "INSTANCE_TEMPLATELIST_GET";');
     
     
         CreateProcedure ( OWNER_USER,'INSTANCE_SAVEANSWERSTATE', '(' ||
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  STATE   IN       BLOB)' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'v_state BLOB; ' ||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     'v_state := STATE; ' ||
     'UPDATE ' || OWNER_USER || '.PackageInstance SET AnswerState = v_state WHERE PackageInstanceId = v_packageinstanceid; ' ||
     'END "INSTANCE_SAVEANSWERSTATE";');

     
         CreateProcedure ( OWNER_USER,'NAVSTACK_PUSH', '(' ||
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '    PageId     IN VARCHAR2, ' ||
     '    PageName     IN VARCHAR2) ' ||
     '    AS ' ||
     '    stackHeight     NUMBER(10,0); ' ||
     '    v_packageinstanceid        varchar2(50); ' ||
     '    BEGIN ' ||
     '    v_packageinstanceid := PackageInstanceId; ' ||
     '        DELETE FROM ' || OWNER_USER || '.NavigationStack WHERE PackageInstanceId = v_packageinstanceid  AND PendingDeletion = 1; ' ||
     '    SELECT   DECODE(NULL,MAX(PositionInStack),-1,MAX(PositionInStack)) ' ||
     '            INTO stackHeight ' ||
     '                 FROM ' || OWNER_USER || '.NavigationStack   ' ||
     '                        WHERE PackageInstanceId = v_packageinstanceid; ' ||
     '            INSERT INTO ' || OWNER_USER || '.NavigationStack (PackageInstanceId, PageId, PageName, PositionInStack) ' ||
     '                VALUES (v_packageinstanceid, PageId, PageName, stackHeight + 1); ' ||
     'END "NAVSTACK_PUSH";');
     
     
     
     CreateProcedure ( OWNER_USER,'NAVSTACK_POP', '(' ||
     '  PACKAGEINSTANCEID IN VARCHAR2, ' ||
     '  ForcePop NUMBER ) ' ||
      'AS ' ||
      'stackHeight     NUMBER(10,0); ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      '    BEGIN ' ||
      '        SELECT   COUNT(1) ' ||
      '        INTO stackHeight ' ||
      '         FROM ' || OWNER_USER || '.NavigationStack ' ||  
      '            WHERE PackageInstanceId = v_packageinstanceid ' ||  
      '             AND PendingDeletion = 0; ' ||
      '        IF  stackHeight > 1 OR  ForcePop = 1 THEN ' ||
      '        BEGIN ' ||
      '            DELETE FROM ' || OWNER_USER || '.NavigationStack ' || 
      '                WHERE PackageInstanceId = v_packageinstanceid ' || 
      '                 AND PositionInStack >= ( ' ||
      '        SELECT  MAX(PositionInStack) ' ||
      '                 FROM ' || OWNER_USER || '.NavigationStack ' || 
      '                WHERE PackageInstanceId = v_packageinstanceid ' || 
      '                 AND PendingDeletion = 0              ); ' ||
      '        END; ' ||
      '        ELSE ' ||
      '        BEGIN ' ||
      '            raise_application_error(-20999, ''Cannot pop the last page on the stack.''); ' ||
      '        END; ' ||
      '        END IF; ' ||
      '    END; ' ||
      'END NAVSTACK_POP; ' );
      
      
     CreateProcedure ( OWNER_USER,'PACKAGE_TEMPLATE_GET', '(' ||
     'PackageId         IN VARCHAR2, ' ||
     'VersionNumber     IN NUMBER, ' ||
     'RevisionNumber     IN NUMBER, ' ||
     'templateId         IN VARCHAR2, ' ||
      'cur_result         OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
     'AS ' ||
     'v_packageid    varchar2(50); ' ||
     'v_templateid    varchar2(50); ' ||
     'BEGIN ' ||
     'v_packageid := PackageId; ' ||
     'v_templateid := templateId; ' ||
     '    BEGIN ' ||
     '        OPEN cur_result FOR ' ||
     '        SELECT TemplateBytes ' ||
     '         FROM ' || OWNER_USER || '.Template ' ||  
     '            WHERE PackageId = v_packageid ' ||  
     '             AND PackageVersionNumber = VersionNumber ' ||  
     '             AND PackageRevisionNumber = RevisionNumber ' ||  
     '             AND templateId = v_templateid; ' ||
     '    END; ' ||
     'END PACKAGE_TEMPLATE_GET; ');    
  
     CreateProcedure ( OWNER_USER,'PACKAGE_OUTCOMEACTION_GET', '(' ||
     'PackageId         IN VARCHAR2, ' ||
     'VersionNumber     IN NUMBER, ' ||
     'RevisionNumber     IN NUMBER, ' ||
     'outcomeId         IN VARCHAR2, ' ||
     'outcomeActionId     IN VARCHAR2, ' ||
      'cur_result         OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_packageid            varchar2(50); ' ||
      'v_outcomeid            varchar2(50); ' ||
      'v_outcomeactionid    varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageid := PackageId; ' ||
      'v_outcomeid := outcomeId; ' ||
      'v_outcomeactionid := outcomeActionId; ' ||
      '    BEGIN ' ||
     '        OPEN cur_result FOR ' ||
      '        SELECT OutcomeActionBytes ' ||
      '         FROM ' || OWNER_USER || '.OutcomeAction ' ||  
      '            WHERE PackageId = v_packageid ' ||  
      '             AND PackageVersionNumber = VersionNumber ' ||  
      '             AND PackageRevisionNumber = RevisionNumber ' ||
      '             AND outcomeId = v_outcomeid   ' ||
      '             AND outcomeActionId = v_outcomeactionid; ' ||
      '    END; ' ||
      'END PACKAGE_OUTCOMEACTION_GET; ' );


     CreateProcedure ( OWNER_USER,'PACKAGE_GETALLBY', '(' ||
      'createdBy     IN VARCHAR2  DEFAULT NULL, ' ||
      'cur_result         OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_createdby        varchar2(510); ' ||
      'BEGIN ' ||
      'v_createdby := createdBy; ' ||
      '    BEGIN ' ||
      '        OPEN cur_result FOR ' ||
      '        SELECT PackageId ' ||
      '        ,  PackageVersionNumber ' ||
      '        ,  PackageRevisionNumber ' ||
      '        ,  Name ' ||
      '        ,  Description ' ||
      '        ,  CreatedBy ' ||
      '        ,  PublishedBy ' ||
      '        ,  WhenPublished ' ||
      '         FROM  ' || OWNER_USER || '.Package_ ' ||  
      '            WHERE WhenDeleted IS NULL ' ||  
      '             AND   ' ||
      '            (v_createdby IS NULL ' ||  
      '             OR CreatedBy = upper(v_createdby)) ' ||  
      '        ORDER BY WhenPublished DESC; ' ||
      '    END; ' ||
      'END PACKAGE_GETALLBY; ' );

    CreateProcedure ( OWNER_USER,'INSTANCE_UPDATE', '(' ||
      ' PackageInstanceId     IN VARCHAR2, ' || 
      ' State                 IN BLOB) ' ||
      ' AS ' ||
      ' v_packageinstanceid        varchar2(50); ' ||
      ' v_state                    blob; ' ||
      ' BEGIN ' ||
      ' v_packageinstanceid := PackageInstanceId; ' ||
      ' v_state := State; ' ||
      '     BEGIN ' ||
      '         UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '         SET             State = v_state ' ||
      '                     WHERE PackageInstanceId = v_packageinstanceid; ' ||
      '     END; ' ||
      ' END INSTANCE_UPDATE; ');


        CreateProcedure ( OWNER_USER,'INSTANCE_RENAME', '(' ||
      'instanceId     IN VARCHAR2, ' ||
      'NewName     IN VARCHAR2) ' ||
      'AS ' ||
      'BEGIN ' ||
      '    BEGIN ' ||
      '        UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '        SET     Reference = substr(NewName,1,50) ' ||
      '        WHERE PackageInstanceId = instanceId; ' ||
      '        IF  SQL%ROWCOUNT = 0 THEN ' ||
      '            raise_application_error(-20999, ''Instance was not renamed.''); ' ||
      '        END IF; ' ||
      '    END; ' ||
      'END instance_rename; ' );

    CreateProcedure ( OWNER_USER,'INSTANCE_DELETE', '(' ||
      'PackageInstanceId     IN VARCHAR2, ' ||
      'WhenDeleted     IN DATE, ' ||
      'DeletedBy     IN VARCHAR2) ' ||
      'AS ' ||
      'v_packageinstanceid    varchar2(50); ' ||
      'v_whendeleted            date; ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      'v_whendeleted := WhenDeleted; ' ||
      '    BEGIN ' ||
      '        UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '        SET     WhenDeleted = v_whendeleted,  ' ||
      '            DeletedByPersonId = DeletedBy ' ||
      '            WHERE PackageInstanceId = v_packageinstanceid; ' ||
      '    END; ' ||
      'END INSTANCE_DELETE; ' );


    CreateProcedure ( OWNER_USER,'NAVSTACK_POPBACKTO', '(' ||
      'PackageInstanceId     IN VARCHAR2, ' ||
      'PointInStack     IN NUMBER) ' ||
      'AS ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      '    BEGIN ' ||
      '        DELETE FROM ' || OWNER_USER || '.NavigationStack ' || 
      '    WHERE PendingDeletion = 1  ' ||
      '     AND PackageInstanceId = v_packageinstanceid; ' ||
      '        UPDATE ' || OWNER_USER || '.NavigationStack ' ||
      '        SET             PendingDeletion = 1 ' ||
      '                    WHERE PackageInstanceId = v_packageinstanceid ' ||  
      '                     AND PositionInStack > PointInStack; ' ||
      '    END; ' ||
      'END NAVSTACK_POPBACKTO; ' );


     CreateProcedure ( OWNER_USER,'NAVSTACK_FASTFORWARD', '(' ||
      'PackageInstanceId     IN VARCHAR2) ' ||
      'AS ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      '    BEGIN ' ||
      '        UPDATE ' || OWNER_USER || '.NavigationStack ' ||
      '        SET     PendingDeletion = 0 ' ||
      '            WHERE PackageInstanceId = v_packageinstanceid; ' ||
      '    END; ' ||
      'END NAVSTACK_FASTFORWARD; ' );


     CreateProcedure ( OWNER_USER,'INSTANCE_FILE_INSERTUPDATE', '(' ||
      'PackageInstanceId IN VARCHAR2, ' ||
      'SequenceNumber     IN NUMBER, ' ||
      'QuestionUID         IN VARCHAR2, ' ||
      'OriginalFilePath     IN VARCHAR2, ' ||
      'ContentType         IN VARCHAR2, ' ||
      'FileData             IN BLOB) ' ||
      'AS ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'v_sequencenumber        number(10,0); ' ||
      'v_questionuid            varchar2(50); ' ||
      'v_originalfilepath        varchar2(1000); ' ||
      'v_contenttype            varchar2(100); ' ||
      'v_filedata                blob; ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      'v_sequencenumber := SequenceNumber; ' ||
      'v_questionuid := QuestionUID; ' ||
      'v_originalfilepath := OriginalFilePath; ' ||
      'v_contenttype := ContentType; ' ||
      'v_filedata := FileData; ' ||
      '   UPDATE ' || OWNER_USER || '.PackageInstance_File ' ||
      '   SET       OriginalFilePath = v_originalfilepath, ' || 
      '      ContentType = v_contenttype, ' || 
      '      FileData = v_filedata ' ||
      '   WHERE PackageInstanceId = v_packageinstanceid ' ||  
      '   AND SequenceNumber = v_sequencenumber ' ||  
      '   AND QuestionUID = v_questionuid; ' ||
      '   IF SQL%ROWCOUNT = 0 THEN ' ||
      '      INSERT INTO ' || OWNER_USER || '.PackageInstance_File ' || 
      '      VALUES (v_packageinstanceid, ' ||
      '              v_sequencenumber,  ' ||                        
      '              v_questionuid, ' || 
      '                v_originalfilepath, ' ||                                                             
      '                v_contenttype,  ' ||
      '                v_filedata); ' ||
      '   END IF; ' ||
      '      END instance_file_insertupdate; ' );

     

     CreateProcedure ( OWNER_USER,'INSTANCE_FILE_DELETE', '(' ||
      'PackageInstanceId IN VARCHAR2, ' ||
      'SequenceNumber     IN NUMBER, ' ||
      'QuestionUID         IN VARCHAR2) ' ||
      'AS ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'v_sequencenumber        number(10,0); ' ||
      'v_questionuid            varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      'v_sequencenumber := SequenceNumber; ' ||
      'v_questionuid := QuestionUID; ' ||
      '    BEGIN ' ||
      '        DELETE FROM ' || OWNER_USER || '.PackageInstance_File ' || 
      '    WHERE PackageInstanceId = v_packageinstanceid ' || 
      '         and SequenceNumber = v_sequencenumber ' || 
      '         and QuestionUID = v_questionuid; ' ||
      '    update ' || OWNER_USER || '.PackageInstance_File set SequenceNumber = SequenceNumber - 1 ' ||
      '    where ' ||
      '        PackageInstanceId = v_packageinstanceid ' || 
      '        and QuestionUID = v_questionuid ' || 
      '        and SequenceNumber > v_sequencenumber; ' ||
      '    END; ' ||
      'END INSTANCE_FILE_DELETE; ' );
     

     CreateProcedure ( OWNER_USER,'INSTANCE_FILE_GET', '(' ||
      'PackageInstanceId     IN VARCHAR2, ' ||
      'SequenceNumber     IN NUMBER, ' ||
      'QuestionUID     IN VARCHAR2, ' ||
      'OriginalFilePath     IN OUT VARCHAR2, ' ||
      'ContentType     IN OUT VARCHAR2, ' ||
      'cur_result         OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_packageinstanceid        varchar2(50); ' ||
      'v_sequencenumber        number(10,0); ' ||
      'v_questionuid            varchar2(50); ' ||
      'BEGIN ' ||
      'v_packageinstanceid := PackageInstanceId; ' ||
      'v_sequencenumber := SequenceNumber; ' ||
      'v_questionuid := QuestionUID; ' ||
      '    BEGIN ' ||
      '        FOR rec IN ( SELECT   OriginalFilePath,  ContentType ' ||
      '                                 FROM ' || OWNER_USER || '.PackageInstance_File   ' ||
      '                                    WHERE PackageInstanceId = v_packageinstanceid ' ||  
      '                                     and SequenceNumber = v_sequencenumber   ' ||
      '                                     and QuestionUID = v_questionuid) ' ||
      '        LOOP ' ||
      '           OriginalFilePath := rec.OriginalFilePath ; ' || 
      '         ContentType := rec.ContentType ;  ' ||
      '        END LOOP; ' ||
      '        OPEN cur_result FOR ' ||
      '        SELECT FileData ' ||
      '             FROM ' || OWNER_USER || '.PackageInstance_File ' ||  
      '                    WHERE PackageInstanceId = v_packageinstanceid ' ||  
      '                     and SequenceNumber = v_sequencenumber   ' ||
      '                     and QuestionUID = v_questionuid; ' ||
      '    END; ' ||
      'END INSTANCE_FILE_GET; ' );

     CreateProcedure ( OWNER_USER,'DOCPREVIEW_INSERTIMAGE', '(' ||
      'ImageId     IN VARCHAR2, ' ||
      'ImageData     IN BLOB) ' ||
      'AS ' ||
      'imageCount     NUMBER(10,0); ' ||
      'v_imageid    varchar2(50); ' ||
      'BEGIN ' ||
      'v_imageid := ImageId; ' ||
      '    BEGIN ' ||
      '        SELECT   count(*) ' ||
      '        INTO imageCount ' ||
      '         FROM ' || OWNER_USER || '.PreviewImage ' ||  
      '            WHERE ImageId = v_imageid; ' ||
      '        IF  imageCount = 0 THEN ' ||
      '        BEGIN ' ||
      '            INSERT INTO ' || OWNER_USER || '.PreviewImage (ImageId, ImageData) ' ||
      '                VALUES (v_imageid,                 ImageData); ' ||
      '        END; ' ||
      '        END IF; ' ||
      '    END; ' ||
      'END DOCPREVIEW_INSERTIMAGE; ' );
     
     
     CreateProcedure ( OWNER_USER,'DOCPREVIEW_GETIMAGE', '(' ||
      'ImageId     IN VARCHAR2, ' ||
      'cur_result         OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_imageid    varchar2(50); ' ||
      'BEGIN ' ||
      'v_imageid := ImageId; ' ||
      '    BEGIN ' ||
      '        OPEN cur_result FOR ' ||
      '        SELECT imageData ' ||
      '         FROM ' || OWNER_USER || '.PreviewImage ' ||  
      '        WHERE imageId = v_imageid; ' ||
      '    END; ' ||
      'END DOCPREVIEW_GETIMAGE; ' );


     CreateProcedure ( OWNER_USER,'DOCPREVIEW_DELETEIMAGE', '(' ||
      'ImageId     IN VARCHAR2) ' ||
      'AS ' ||
      'v_imageid    varchar2(50); ' ||
      'BEGIN ' ||
      'v_imageid := ImageId; ' ||
      '    BEGIN ' ||
      '        DELETE FROM ' || OWNER_USER || '.PreviewImage ' || 
      '    WHERE imageId = v_imageid; ' ||
      '    END; ' ||
      'END DOCPREVIEW_DELETEIMAGE; ' );

     
     CreateProcedure ( OWNER_USER,'STATUS_UPDATE', '(' ||
      'instanceId     IN VARCHAR2, ' ||
      'statusCode     IN CHAR) ' ||
      'AS ' ||
      'currentStatusRank     NUMBER(10,0); ' ||
      'proposedStatusRank     NUMBER(10,0); ' ||
      'BEGIN ' ||
      '    BEGIN ' ||
      '        FOR rec IN ( SELECT   Rank ' ||
      '                                 FROM ' || OWNER_USER || '.coordinatorStatus c INNER JOIN ' || OWNER_USER || '.PackageInstance pin ' || 
      '                                ON c.CoordinatorStatusCode = pin.CoordinatorStatusCode  ' ||
      '                                    WHERE PackageInstanceId = instanceId) ' ||
      '        LOOP ' ||
      '           CurrentStatusRank := rec.Rank ; ' || 
      '        END LOOP; ' ||
      '        FOR rec IN ( SELECT   RANK ' ||
      '                                     FROM ' || OWNER_USER || '.coordinatorStatus ' ||  
      '                                        WHERE CoordinatorStatusCode = statusCode) ' ||
      '        LOOP ' ||
      '           proposedStatusRank := rec.RANK ; ' || 
      '        END LOOP; ' ||
      '        IF  ( CurrentStatusRank < proposedStatusRank OR   CurrentStatusRank IS NULL OR   CurrentStatusRank = 0) THEN ' ||
      '        BEGIN ' ||
      '            UPDATE ' || OWNER_USER || '.PackageInstance ' ||
      '            SET                 CoordinatorStatusCode = statusCode ' ||
      '                            WHERE PackageInstanceId = instanceId; ' ||
      '        END; ' ||
      '        END IF; ' ||
      '    END; ' ||
      'END STATUS_UPDATE; ' );
     
     
     CreateProcedure ( OWNER_USER,'DISTRIBUTIONRESULT_INSERT', '(' ||
      'instanceId     IN VARCHAR2, ' ||
      'distributorId     IN VARCHAR2, ' ||
      'templateid     IN VARCHAR2, ' ||
      'templateName     IN VARCHAR2, ' ||
      'link     IN VARCHAR2, ' ||
      'linkText     IN VARCHAR2, ' ||
      'internalReference     IN VARCHAR2, ' ||
      'whenLoggedUtc     IN DATE) ' ||
      'AS ' ||
      'BEGIN ' ||
      '    BEGIN ' ||
      '        INSERT INTO  ' || OWNER_USER || '.DistributionResult (PackageInstanceId, distributorId, templateId, templateName, link, linkText, internalReference, whenLoggedUtc) ' ||
      '            VALUES (instanceId,distributorId,templateId,templateName,link,linkText,internalReference,whenLoggedUtc); ' ||
      '    END; ' ||
      'END DISTRIBUTIONRESULT_INSERT; ' );

     
     
     CreateProcedure ( OWNER_USER,'INSTANCE_FILE_INSERTCOPY', '(' ||
      'newPackageInstanceId     IN VARCHAR2, ' ||
      'oldPackageInstanceId     IN VARCHAR2) ' ||
      'AS ' ||
      'BEGIN ' ||
      'BEGIN ' ||
      '        INSERT INTO ' || OWNER_USER || '.PackageInstance_File (PackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData) ' ||
      '            SELECT  newPackageInstanceId, SequenceNumber, QuestionUID,  ' ||
      '            OriginalFilePath, ContentType, FileData  ' ||
      '             FROM ' || OWNER_USER || '.PackageInstance_File   ' ||
      '                WHERE packageInstanceId = oldPackageInstanceId; ' ||
      '    END; ' ||
      'END INSTANCE_FILE_INSERTCOPY; ' );


     CreateProcedure ( OWNER_USER,'INSTANCE_INSERTCOPY', '(' ||
      'newPackageInstanceId     IN VARCHAR2, ' ||
      'oldPackageInstanceId     IN VARCHAR2, ' ||
      'userIdent     IN VARCHAR2, ' ||
      'whenCreated     IN DATE) ' ||
      'AS ' ||
      'BEGIN ' ||
      '        INSERT INTO ' || OWNER_USER || '.PackageInstance (PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, State, CreatedByPersonId, WhenCreated, AnswerState) ' ||
      '            SELECT  newPackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, State, upper(userIdent), whenCreated, AnswerState ' || 
      '             FROM ' || OWNER_USER || '.PackageInstance   ' ||
      '                WHERE packageInstanceId = oldPackageInstanceId; ' ||
      '        INSERT INTO ' || OWNER_USER || '.PackageInstance_File (PackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData) ' ||
      '            SELECT  newPackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData ' || 
      '                 FROM ' || OWNER_USER || '.PackageInstance_File   ' ||
      '                        WHERE packageInstanceId = oldPackageInstanceId; ' ||
      '        INSERT INTO ' || OWNER_USER || '.NavigationStack (PackageInstanceId, PositionInStack, PageId, PageName, PendingDeletion) ' ||
      '            SELECT  newPackageInstanceId, PositionInStack, PageId, PageName, PendingDeletion ' || 
      '                 FROM ' || OWNER_USER || '.NavigationStack   ' ||
      '                        WHERE packageInstanceId = oldPackageInstanceId; ' ||
      'END INSTANCE_INSERTCOPY; ' );
     
     CreateProcedure ( OWNER_USER,'INSTANCE_SETCOMPLETE', '(' ||
      'PACKAGEINSTANCEID IN VARCHAR2, ' ||
      'WhenCompleted IN DATE) ' ||
     'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
     'v_WhenCompleted DATE; '||
     'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
      'v_WhenCompleted := WhenCompleted; '||
     'UPDATE ' || OWNER_USER || '.PACKAGEINSTANCE SET WhenCompleted = v_WhenCompleted WHERE PackageInstanceId = v_packageinstanceid; ' ||
      'END INSTANCE_SETCOMPLETE; ');
     
     CreateProcedure ( OWNER_USER,'INSTANCE_SETSYNCHED', '(' ||
      'PACKAGEINSTANCEID IN VARCHAR2) ' ||
       'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
      'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
		'UPDATE ' || OWNER_USER || '.PACKAGEINSTANCE SET isSynchronized = 1 WHERE PackageInstanceId = v_packageinstanceid; ' ||
      'END INSTANCE_SETSYNCHED; ');

     CreateProcedure ( OWNER_USER,'INSTANCE_STATUS_GET', '(' ||
      'PACKAGEINSTANCEID IN VARCHAR2, ' ||
      'RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
      'RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
       'AS ' ||
     'v_packageinstanceid varchar2(50); ' ||
      'BEGIN ' ||
     'v_packageinstanceid := packageinstanceid; ' ||
     '  OPEN RC1 FOR ' ||
     '	SELECT  PackageInstance.PackageInstanceId,  ' ||
     '			PackageInstance.PackageId,  ' ||
     '			PackageInstance.PackageVersionNumber,  ' ||
     '			PackageInstance.PackageRevisionNumber,  ' ||
     '			PackageInstance.Reference,  ' ||
     '			PackageInstance.WhenCreated,  ' ||
     '			PackageInstance.WhenCompleted,  ' ||
     '			PackageInstance.WhenDeleted,  ' ||
     '			PackageInstance.DeletedByPersonId,  ' ||
     '			Package_.Name AS ParentPackageName,  ' ||
     '			PackageInstance.CreatedByPersonId,  ' ||
	 '			CoordinatorStatus.Name AS CoordinatorStatusName,  ' ||
	 '			CoordinatorStatus.Rank AS CoordinatorStatusRank,  ' ||
	 '			CoordinatorStatus.CoordinatorStatusCode ' ||
	 '			FROM ' || OWNER_USER || '.PackageInstance ' ||
	 '			INNER JOIN ' || OWNER_USER || '.Package_ ON PackageInstance.PackageId = Package_.PackageId ' ||
	 '			AND PackageInstance.PackageVersionNumber = Package_.PackageVersionNumber ' ||
	 '						   AND PackageInstance.PackageRevisionNumber = Package_.PackageRevisionNumber ' ||
	 '			LEFT OUTER JOIN ' || OWNER_USER || '.CoordinatorStatus ON PackageInstance.CoordinatorStatusCode = CoordinatorStatus.CoordinatorStatusCode ' ||
	 '			WHERE PackageInstance.PackageInstanceId = v_packageinstanceid ;' ||
     '  OPEN RC2 FOR ' ||
	 '			SELECT  TemplateName, ' ||
	 '					Link, ' ||
	 '					LinkText, ' ||
	 '					InternalReference,' || 
	 '					WhenLoggedUtc,' || 
	 '					TemplateId' ||
	 '			FROM  ' || OWNER_USER || '.DistributionResult ' ||
	 '			WHERE PackageInstanceId = v_packageinstanceid ;' ||
     '  OPEN RC3 FOR ' ||
	 '			SELECT  RelatedPackageInstanceId,  ' ||
	 '					FriendlyMessage,  ' ||
	 '					Message,  ' ||
	 '					RaisingAssemblyName, ' ||
	 '					SeverityCode ' ||
	 '			FROM ' || OWNER_USER || '.ExceptionLog ' ||
	 '			WHERE ExceptionLog.RelatedPackageInstanceId = v_packageinstanceid ;' ||
     'END INSTANCE_STATUS_GET; ');
     

     CreateProcedure ( OWNER_USER,'EXCEPTION_INSERT', '(' ||
      'SeverityCode     IN CHAR, ' ||
      'WhenLoggedUtc     IN DATE, ' ||
      'PersonId     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageId     IN VARCHAR2  DEFAULT NULL, ' ||
      'RelatedPackageVersionNumber     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageRevisionNumber     IN NUMBER  DEFAULT NULL, ' ||
      'RelatedPackageInstanceId     IN VARCHAR2  DEFAULT NULL, ' ||
      'FriendlyMessage     IN VARCHAR2, ' ||
      'message     IN VARCHAR2  DEFAULT NULL, ' ||
      'StackTrace     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyName     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyPath     IN VARCHAR2  DEFAULT NULL, ' ||
      'RaisingAssemblyVersion     IN VARCHAR2  DEFAULT NULL, ' ||
      'ParentExceptionId     IN NUMBER  DEFAULT NULL, ' ||
      'ExceptionId    OUT NUMBER) ' ||
      'AS ' ||
     'v_ExceptionId NUMBER (10,0);' ||  
     'v_SEVERITYCODE CHAR (4);' || 
     'v_WHENLOGGEDUTC TIMESTAMP;' ||
     'v_PERSONID NUMBER (10,0);' ||  
     'v_RELATEDPACKAGEID VARCHAR2(50) ;' || 
     'v_RELATEDPACKAGEVERSIONNUMBER NUMBER (10,0);' || 
     'v_RELATEDPACKAGEREVISIONNUMBER NUMBER (10,0);' || 
     'v_RELATEDPACKAGEINSTANCEID VARCHAR2(50);' || 
     'v_FRIENDLYMESSAGE VARCHAR2 (510);' || 
     'v_MESSAGE VARCHAR2 (510);' || 
     'v_STACKTRACE VARCHAR2(2500)  ;' || 
     'v_RAISINGASSEMBLYNAME VARCHAR2 (50)  ;' || 
     'v_RAISINGASSEMBLYPATH VARCHAR2 (500)  ;' || 
     'v_RAISINGASSEMBLYVERSION VARCHAR2 (20)  ;' || 
     'v_PARENTEXCEPTIONID NUMBER (10,0) ;' || 
      'BEGIN ' ||
     'v_SEVERITYCODE := SEVERITYCODE;' || 
     'v_WHENLOGGEDUTC  := WHENLOGGEDUTC;' ||
     'v_PERSONID := PERSONID;' ||  
     'v_RELATEDPACKAGEID := RELATEDPACKAGEID;' || 
     'v_RELATEDPACKAGEVERSIONNUMBER := RELATEDPACKAGEVERSIONNUMBER;' || 
     'v_RELATEDPACKAGEREVISIONNUMBER := RELATEDPACKAGEREVISIONNUMBER;' || 
     'v_RELATEDPACKAGEINSTANCEID := RELATEDPACKAGEINSTANCEID;' || 
     'v_FRIENDLYMESSAGE := substr ( FRIENDLYMESSAGE, 1, 508);' || 
     'v_MESSAGE := substr ( MESSAGE, 1, 508);' ||
     'v_STACKTRACE := substr ( STACKTRACE, 1, 2498);' ||
     'v_RAISINGASSEMBLYNAME := substr ( RAISINGASSEMBLYNAME, 1, 49);' || 
     'v_RAISINGASSEMBLYPATH := substr ( RAISINGASSEMBLYPATH, 1, 498);' || 
     'v_RAISINGASSEMBLYVERSION := RAISINGASSEMBLYVERSION;' || 
     'v_PARENTEXCEPTIONID := PARENTEXCEPTIONID;' ||      
      '        INSERT INTO ' || OWNER_USER || '.ExceptionLog (SeverityCode, PersonId, WhenLoggedUtc, RelatedPackageId, RelatedPackageVersionNumber, RelatedPackageRevisionNumber, RelatedPackageInstanceId, FriendlyMessage, Message, StackTrace, RaisingAssemblyName, RaisingAssemblyPath, RaisingAssemblyVersion, parentExceptionId) ' ||
      '            VALUES (v_SeverityCode,v_PersonId,v_WhenLoggedUtc,v_RelatedPackageId,v_RelatedPackageVersionNumber,v_RelatedPackageRevisionNumber,v_RelatedPackageInstanceId,v_FriendlyMessage,v_Message,v_StackTrace, ' ||
      '            v_RaisingAssemblyName,v_RaisingAssemblyPath,v_RaisingAssemblyVersion,v_ParentExceptionId); ' ||
    ' select max (ExceptionId)  into v_ExceptionId from ' || OWNER_USER || '.ExceptionLog;' ||
      ' ExceptionId :=  v_ExceptionId; ' ||
      'END EXCEPTION_INSERT; ' );

      
    CreateProcedure ( OWNER_USER,'STATUS_GETBY', '(' ||
      'CreatedByPersonId       IN VARCHAR2, ' ||
      'RecentTransactionCount  IN NUMBER, ' ||
      'InProgressStart         IN NUMBER, ' ||
      'InProgressEnd   IN NUMBER, ' ||
      'MyHistoryStart  IN NUMBER, ' ||
      'MyHistoryEnd    IN NUMBER, ' ||
      'InProgressRecordCount   IN OUT NUMBER, ' ||
      'HistoryRecordCount      IN OUT NUMBER, ' ||
      'RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
      'RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC4     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC5     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
      'RC6     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR) ' ||
      'AS ' ||
      'v_createdByPersonId VARCHAR2(255); ' ||
      'v_tmpSeq number; ' ||
      ' BEGIN ' ||
      '        v_createdByPersonId := CreatedByPersonId; ' ||
      
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPINSTANCE; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPHISTORY; ' ||
      '                 DELETE FROM ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT; ' ||
      
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCE_RA increment by 1 '';' ||

      ' SELECT ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPHISTORY_RA increment by 1 '';' ||

      ' SELECT ' || OWNER_USER || '.SEQTP_TMPDISTRIBUTIONRESULT_DI.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPDISTRIBUTIONRESULT_DI increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPDISTRIBUTIONRESULT_DI.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPDISTRIBUTIONRESULT_DI increment by 1 '';' ||

      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA.nextval into v_tmpSeq from dual;' ||  
      ' v_tmpSeq := 0 - v_tmpSeq;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA increment by '' || v_tmpSeq;' || 
      ' SELECT ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA.nextval into v_tmpSeq from dual ;  ' ||
      ' execute immediate ''alter sequence ' || OWNER_USER || '.SEQTP_TMPINSTANCEINPROGRESS_RA increment by 1 '';' ||

      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPINSTANCE( ' ||
      '                    packageinstanceid ' ||
      '                   ,packageid ' ||
      '                   ,packageversionnumber ' ||
      '                   ,packagerevisionnumber ' ||
      '                   ,reference ' ||
      '                   ,createdbypersonid ' ||
      '                   ,whencreated ' ||
      '                   ,whencompleted ' ||
      '                   ,whendeleted ' ||
      '                   ,deletedbypersonid ' ||
      '                   ,coordinatorstatuscode ' ||
      '                   ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                                ,pin.PackageId ' ||
      '                                ,pin.PackageVersionNumber ' ||
      '                                ,pin.PackageRevisionNumber ' ||
      '                                ,pin.Reference ' ||
      '                                ,pin.CreatedByPersonId ' ||
      '                                ,pin.WhenCreated ' ||
      '                                ,pin.WhenCompleted ' ||
      '                                ,pin.WhenDeleted ' ||
      '                                ,pin.DeletedByPersonId ' ||
      '                                ,pin.CoordinatorStatusCode ' ||
      '                                ,p.Name ParentPackageName ' || 
      '                                  FROM ' || OWNER_USER || '.PackageInstance pin ' || 
      '                                         INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                                         ON pin.PackageId = p.PackageId ' || 
      '                                         AND pin.PackageVersionNumber = p.PackageVersionNumber ' ||
      '                                         AND pin.PackageRevisionNumber = p.PackageRevisionNumber ' ||
            '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
      '                                  AND pin.WhenDeleted IS NULL ' ||   
      '                          ORDER BY pin.WhenCreated DESC ; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS( ' ||
      '                            packageinstanceid ' ||
      '                           ,packageid ' ||
      '                           ,packageversionnumber ' ||
      '                           ,packagerevisionnumber ' ||
      '                           ,reference ' ||
      '                           ,createdbypersonid ' ||
      '                           ,whencreated ' ||
      '                           ,whencompleted ' ||
      '                           ,whendeleted ' ||
      '                           ,deletedbypersonid ' ||
      '                           ,coordinatorstatuscode ' ||
      '                           ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                               , pin.PackageId ' ||
      '                               , pin.PackageVersionNumber ' ||
      '                               , pin.PackageRevisionNumber ' ||
      '                               , pin.Reference ' ||
      '                               , pin.CreatedByPersonId ' ||
      '                               , pin.WhenCreated ' ||
      '                               , pin.WhenCompleted ' ||
      '                               , pin.WhenDeleted ' ||
      '                               , pin.DeletedByPersonId ' ||
      '                               , pin.CoordinatorStatusCode ' ||
      '                               , p.Name ParentPackageName ' || 
      '                          FROM ' || OWNER_USER || '.PackageInstance pin ' || 
      '                          INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                             ON pin.PackageId = p.PackageId ' || 
      '                             AND pin.PackageVersionNumber = p.PackageVersionNumber ' ||
      '                             AND pin.PackageRevisionNumber = p.PackageRevisionNumber ' || 
      '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
      '                          and pin.WhenDeleted IS NULL ' ||  
      '                          and pin.CoordinatorStatusCode IS NULL  ' ||  
      '                          ORDER BY pin.WhenCreated DESC ; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPHISTORY( ' ||
      '                    packageinstanceid ' ||
      '                   ,packageid ' ||
      '                   ,packageversionnumber ' ||
      '                   ,packagerevisionnumber ' ||
      '                   ,reference ' ||
      '                   ,createdbypersonid ' ||
      '                   ,whencreated ' ||
      '                   ,whencompleted ' ||
      '                   ,whendeleted ' ||
      '                   ,deletedbypersonid ' ||
      '                   ,coordinatorstatuscode ' ||
      '                   ,parentpackagename) ' ||
      '                         SELECT  pin.PackageInstanceId ' ||
      '                                , pin.PackageId ' ||
      '                                , pin.PackageVersionNumber ' ||
      '                                , pin.PackageRevisionNumber ' ||
      '                                , pin.Reference ' ||
      '                                , pin.CreatedByPersonId ' ||
      '                                , pin.WhenCreated ' ||
      '                                , pin.WhenCompleted ' ||
      '                                , pin.WhenDeleted ' ||
      '                                , pin.DeletedByPersonId ' ||
      '                                , pin.CoordinatorStatusCode ' ||
      '                                , p.Name ParentPackageName ' ||
      '                                  FROM ' || OWNER_USER || '.PackageInstance pin INNER JOIN ' || OWNER_USER || '.Package_ p ' || 
      '                                         ON pin.PackageId = p.PackageId and pin.PackageVersionNumber ' || 
      '                         = p.PackageVersionNumber and pin.PackageRevisionNumber = p.PackageRevisionNumber  ' ||
              '                                  WHERE pin.CreatedByPersonId = upper(v_CreatedByPersonId) ' ||  
    '                                  AND pin.WhenDeleted IS NULL ' ||   
      '                                         ORDER BY pin.WhenCreated DESC; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPINSTANCE t INNER JOIN ' || OWNER_USER || '.DistributionResult d ' || 
      '                                         ON t.PackageInstanceId = d.PackageInstanceId   ' ||
      '                                                 WHERE rownum <= RecentTransactionCount   ' ||
      '                                         ORDER BY t.Rank; ' ||
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS t INNER JOIN ' || OWNER_USER || '.DistributionResult ' || 
      '                         d  ' ||
      '                                         ON t.PackageInstanceId = d.PackageInstanceId left JOIN ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         td  ' ||
      '                                         ON d.DistributionResultId = td.DistributionResultId ' ||  
      '                                                 WHERE Rank  BETWEEN InProgressStart AND InProgressEnd ' || 
      '                                                  and td.DistributionResultId IS NULL ' ||  
      '                                         ORDER BY t.Rank; ' ||
   
      '                 INSERT INTO ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         SELECT  d.DistributionResultId, d.DistributorId, d.PackageInstanceId, ' || 
      '                         d.TemplateId, d.TemplateName, d.Link, d.LinkText, d.InternalReference,  ' ||
      '                         d.WhenLoggedUtc  ' ||
      '                                  FROM ' || OWNER_USER || '.TP_TMPHISTORY t INNER JOIN ' || OWNER_USER || '.DistributionResult d ' || 
      '                                         ON t.PackageInstanceId = d.PackageInstanceId left JOIN ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' || 
      '                         td  ' ||
      '                                         ON d.DistributionResultId = td.DistributionResultId ' ||  
      '                                                 WHERE Rank  BETWEEN MyHistoryStart AND MyHistoryEnd ' ||  
      '                                                  and td.DistributionResultId IS NULL ' ||   
      '                                         ORDER BY t.Rank ; ' ||
  
      '                 OPEN RC1 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  CoordinatorStatusCode ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCE t ' ||  
      '                                         WHERE Rank <= RecentTransactionCount ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      ' 				OPEN RC2 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  CoordinatorStatusCode ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS t ' ||  
      '                                         WHERE Rank  BETWEEN InProgressStart AND InProgressEnd ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      '  				OPEN RC3 FOR ' ||
      '                 SELECT PackageInstanceId ' ||
      '                 ,  PackageId ' ||
      '                 ,  PackageVersionNumber ' ||
      '                 ,  PackageRevisionNumber ' ||
      '                 ,  Reference ' ||
      '                 ,  CreatedByPersonId ' ||
      '                 ,  WhenCreated ' ||
      '                 ,  WhenCompleted ' ||
      '                 ,  WhenDeleted ' ||
      '                 ,  DeletedByPersonId ' ||
      '                 ,  CoordinatorStatusCode ' ||
      '                 ,  ParentPackageName ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPHISTORY t ' ||  
      '                                         WHERE Rank  BETWEEN MyHistoryStart AND MyHistoryEnd ' ||  
      '                                 ORDER BY t.Rank ; ' ||
      '  				OPEN RC4 FOR ' ||
      '                 SELECT * ' ||
      '                          FROM ' || OWNER_USER || '.CoordinatorStatus; ' ||
      ' 				OPEN RC5 FOR ' ||
      '                 SELECT * ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPDISTRIBUTIONRESULT ' ||  
      '                                 ORDER BY packageInstanceId , templateName ; ' ||
      '                 SELECT   count(*) ' ||
      '                 INTO InProgressRecordCount ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPINSTANCEINPROGRESS; ' || 
      '                 SELECT   count(*) ' ||
      '                 INTO HistoryRecordCount ' ||
      '                          FROM ' || OWNER_USER || '.TP_TMPHISTORY; ' ||
      ' 				OPEN RC6 FOR ' ||
      '                 SELECT RelatedPackageInstanceId ' ||
      '                 ,  FriendlyMessage ' ||
      '                 ,  Message ' ||
      '                 ,  raisingAssemblyName ' ||
      '                 ,  SeverityCode ' ||
      '                          FROM ' || OWNER_USER || '.exceptionLog ' ||  
      '                                         WHERE EXISTS ( ' || 
      '                                 SELECT  *  ' ||
      '                                          FROM ' || OWNER_USER || '.TP_TMPHISTORY t ' ||  
      '                                         WHERE t.packageInstanceId = exceptionLog.relatedPackageInstanceId ' || 
      '                                          AND Rank  BETWEEN MyHistoryStart AND MyHistoryEnd             ) ' || 
      '                                          OR EXISTS ( ' || 
      '                                 SELECT  *  ' ||
      '                                          FROM ' || OWNER_USER || '.TP_TMPINSTANCE ti ' ||  
      '                                         WHERE ti.packageInstanceId = exceptionLog.relatedPackageInstanceId ' || 
      '                                          AND rownum <= RecentTransactionCount           ); ' || 
      '       END status_getby; ' );


    

      CreateProcedure ( OWNER_USER,'INSTANCE_INFOSET_GET', '(' ||
     '   DateToGetPackagesFrom   IN  DATE,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
	 ' Open rc1 for  ' ||
	 ' SELECT distinct Package_.PackageId, Package_.PackageVersionNumber, Package_.PackageRevisionNumber, ' ||	 
     '   Package_.Name, Package_.Description, Package_.StartPageName, Package_.StartPageId, ' ||
     '   Package_.CreatedBy, Package_.ModifiedBy, Package_.PublishedBy, ' ||
     '   Package_.WhenCreated, Package_.WhenModified, Package_.WhenPublished, Package_.WhenDeleted, Package_.DeletedByPersonId ' ||
     '   	  FROM ' || OWNER_USER || '.PACKAGE_' ||	
     ' join ' || OWNER_USER || '.PackageInstance on PackageInstance.PackageId = Package_.PackageID ' ||	
     ' AND PackageInstance.PackageVersionNumber = Package_.PackageVersionNumber AND PackageInstance.PackageRevisionNumber = Package_.PackageRevisionNumber ' ||	
     ' where COALESCE(isSynchronized, 0) <> 1 AND PackageInstance.WhenCreated > DateToGetPackagesFrom; ' ||	
 
	 ' Open rc2 for SELECT  ' ||	
	 ' PackageInstance.PackageInstanceId, PackageInstance.PackageId, PackageInstance.PackageVersionNumber,   ' ||	
	 ' PackageInstance.PackageRevisionNumber, PackageInstance.Reference, PackageInstance.CreatedByPersonId,  ' ||	
	 ' PackageInstance.WhenCreated, PackageInstance.WhenCompleted, PackageInstance.WhenDeleted,  ' ||	
	 ' PackageInstance.DeletedByPersonId, PackageInstance.CoordinatorStatusCode, PackageInstance.IsSynchronized  ' ||	
	 '   	  FROM ' || OWNER_USER || '.PackageInstance ' ||	
	 ' WHERE COALESCE(isSynchronized,0) <> 1 AND PackageInstance.WhenCreated > DateToGetPackagesFrom;	 ' ||	
 
 	 'END "INSTANCE_INFOSET_GET"; ');
/*
     INSTANCE_STATUS_GET
     INSTANCE_SETSYNCHED
*/



END;


PROCEDURE CreateStatusPSupport 
IS
BEGIN
     CreateTemporaryTable  (OWNER_USER, 'TP_TMPINSTANCEINPROGRESS',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'COORDINATORSTATUSCODE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );
     
     
     CreateTemporaryTable  (OWNER_USER, 'TP_TMPINSTANCE',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'COORDINATORSTATUSCODE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );

     CreateTemporaryTable  (OWNER_USER, 'TP_TMPHISTORY',  
     '(RANK NUMBER, ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' ||
     'PACKAGEID VARCHAR2(50), ' ||  
     'PACKAGEVERSIONNUMBER NUMBER(10, 0), ' ||
     'PACKAGEREVISIONNUMBER NUMBER(10, 0), ' ||
     'REFERENCE VARCHAR2(100), ' ||
     'CREATEDBYPERSONID VARCHAR2(510), ' ||
     'WHENCREATED DATE, ' ||
     'WHENCOMPLETED DATE, ' ||
     'WHENDELETED DATE, ' ||
     'DELETEDBYPERSONID VARCHAR2(510), ' ||
     'COORDINATORSTATUSCODE CHAR(4), ' ||
     'PARENTPACKAGENAME VARCHAR2(100)) ' );

     CreateTemporaryTable  (OWNER_USER, 'TP_TMPDISTRIBUTIONRESULT',  
     '(DISTRIBUTIONRESULTID NUMBER(10, 0), ' || 
     'DISTRIBUTORID VARCHAR2(50), ' ||
     'PACKAGEINSTANCEID VARCHAR2(50), ' || 
     'TEMPLATEID VARCHAR2(50), ' || 
     'TEMPLATENAME VARCHAR2(1000), ' ||
     'LINK VARCHAR2(1000), ' ||
     'LINKTEXT VARCHAR2(1000), ' ||
     'INTERNALREFERENCE VARCHAR2(1000), ' || 
     'WHENLOGGEDUTC DATE ) ' );
     
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPINSTANCEINPROGRESS', 'RANK'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPINSTANCE', 'RANK'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPHISTORY', 'RANK'     );
-- This is not needed as it causes duplicate entries in the status page.
     CreateSequenceAndTrigger ( OWNER_USER, 'TP_TMPDISTRIBUTIONRESULT', 'DISTRIBUTIONRESULTID'     );
 

END;

PROCEDURE CreateData 
IS
BEGIN
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.Coordinatorstatus(Coordinatorstatuscode,Name,Rank) values (''DONE'',''Complete'',3) ';
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.Coordinatorstatus(Coordinatorstatuscode,Name,Rank) values (''ERRO'',''Errors'',5)';
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.Coordinatorstatus(Coordinatorstatuscode,Name,Rank) values (''PROG'',''In progress'',2)';
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.Coordinatorstatus(Coordinatorstatuscode,Name,Rank) values (''WARN'',''Warnings'',4)';
 EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.Coordinatorstatus(Coordinatorstatuscode,Name,Rank) values (''QUEU'',''Queued'',0)';

EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.severitycode values(''ERRO'',''Error'')';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.severitycode values(''INFO'',''Information'')';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.severitycode values(''WARN'',''Warning'')';

EXECUTE IMMEDIATE 'delete from ' || OWNER_USER || '.versioninformation';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.versioninformation (Major,Minor,Build,Revision,SequenceNumber) Values (5,1,4,0,6)';
END;

----------------------------------------------
----------------------------------------------
        
BEGIN
     success := success AND ReCreateTablespace ( Tabelspace, TabelspacePathName );
     success := success AND ReCreateUser ( OWNER_USER, TRUE, Tabelspace );
     success := success AND ReCreatePackages (OWNER_USER);
    
    CreateAllTables; 
   CreateSequnces;
    CreatePrimaryKeys;
    CreateForeignKeys;

    CreateStatusPSupport;
   
    CreateProcedures;
    
    CreateData;

COMMIT;

END;
/
