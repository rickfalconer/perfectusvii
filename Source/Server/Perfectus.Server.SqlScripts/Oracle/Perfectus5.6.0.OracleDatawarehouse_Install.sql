PROMPT *-------------------------------------*
PROMPT * Installation script for             *
PROMPT *     Perfectus 5.6.0.0               *
PROMPT *     Data Warehousing                *
PROMPT *-------------------------------------*

PROMPT * Please enter the database path on the server (e.g. "c:/oracle/oradata/") *
ACCEPT askTabelspacePathName PROMPT 'Enter the database path on the server: ' 

PROMPT * Please enter the tablespace name (e.g. "PerfectusTSReporting") *
ACCEPT askTabelspace PROMPT 'Enter the tablespace name: ' 

PROMPT * Please enter the perfectus username (e.g. "App_Perfectus_DW") *
ACCEPT askOWNER_USER PROMPT 'Enter the perfectus standard username: '

DECLARE 
        TabelspacePathName VARCHAR2 (255) := '&askTabelspacePathName';
        Tabelspace VARCHAR2 ( 120) := '&askTabelspace';
        OWNER_USER VARCHAR2 ( 120):= '&askOWNER_USER';
        Success BOOLEAN := TRUE;

----------------------------------------------
    
FUNCTION ReCreateTablespace (
         TablespaceName VARCHAR2,
         DataFilePathname VARCHAR2
) 
RETURN
      BOOLEAN
IS
  counter INT;      
  dbFilePathname VARCHAR2 (1000);
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tablespaces WHERE tablespace_name = 

UPPER(trim(TablespaceName));
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop tablespace ' || Tabelspace || ' including contents and 

datafiles';
    END IF;
    
    dbFilePathname := DataFilePathname || TablespaceName || '.dbf'; 
    EXECUTE IMMEDIATE 'CREATE TABLESPACE ' || Tabelspace || ' LOGGING  DATAFILE ''' || 

dbFilePathname || ''' SIZE 200M REUSE  AUTOEXTEND ON NEXT 5M MAXSIZE UNLIMITED';    
    RETURN TRUE;
END;

----------------------------------------------

FUNCTION ReCreateUser (
         lookupuser VARCHAR2,
         isOwner BOOLEAN,
         tablespacename VARCHAR2 )
RETURN
      BOOLEAN
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_users WHERE username = UPPER(trim(lookupuser));
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop user ' || lookupuser || ' cascade';
    END IF;
    EXECUTE IMMEDIATE 'CREATE USER ' || lookupuser || ' IDENTIFIED BY ' || lookupuser || ' default 

TABLESPACE ' || tablespacename;
    
    IF isOwner THEN
       EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, CREATE PUBLIC SYNONYM, CREATE VIEW TO ' || 

lookupuser;
    ELSE
        EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, CREATE VIEW TO ' || lookupuser;
    END IF;
    RETURN TRUE;
END;     

----------------------------------------------

FUNCTION ReCreatePackages (
         lookupuser VARCHAR2 )
RETURN
      BOOLEAN
IS
  command VARCHAR2 (1000);
BEGIN
     command := 'CREATE OR REPLACE PACKAGE ' || lookupuser || '.PERFECTUS_GLOBAL AUTHID 

CURRENT_USER AS TYPE REF_CUR IS REF CURSOR; END PERFECTUS_GLOBAL;';
     EXECUTE IMMEDIATE command;
RETURN TRUE;
END;     

PROCEDURE CreateTable ( 
          tablespacename VARCHAR2,         
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND 

UPPER (table_name)=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;

    EXECUTE IMMEDIATE 'CREATE TABLE ' || lookupuser || '.' || tablename || tabledefinition || ' 

TABLESPACE ' || tablespacename;
END;            


PROCEDURE CreateTemporaryTable ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         tabledefinition VARCHAR2) 
IS
  counter INT;      
BEGIN
       SELECT COUNT(*) INTO counter FROM dba_tables WHERE UPPER(owner) = UPPER(trim(lookupuser)) AND 

UPPER (table_name)=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'drop table ' || lookupuser || '.' || tablename;
    END IF;

    EXECUTE IMMEDIATE 'CREATE GLOBAL TEMPORARY TABLE ' || lookupuser || '.' || tablename || tabledefinition ||
    'ON COMMIT PRESERVE ROWS';
END;            


----------------------------------------------

PROCEDURE CreateSequenceAndTrigger ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  seqName VARCHAR2 (200 );
  triggerName   VARCHAR2 (200 );
BEGIN
     seqName := UPPER(lookupuser) || '.SEQ' || UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2));
       SELECT COUNT(*) INTO counter FROM dba_sequences WHERE UPPER(sequence_name) = 'SEQ' || 

UPPER(tablename) || '_' || UPPER(SUBSTR(fieldname,1,2)) AND sequence_owner=UPPER(lookupuser);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'DROP SEQUENCE '||seqName ;
    END IF;
     EXECUTE IMMEDIATE 'CREATE SEQUENCE ' || seqName || ' MINVALUE 0 START  WITH 1 ';

     triggerName := UPPER(lookupuser) || '.TRG' || UPPER(tablename) || '_' || 

UPPER(SUBSTR(fieldname,1,2));
     EXECUTE IMMEDIATE ' CREATE or replace TRIGGER ' || triggerName || ' BEFORE INSERT ON ' || 

UPPER(lookupuser) || '.' || UPPER(tablename) || 
                         ' FOR EACH ROW ' || 
                       ' BEGIN ' ||  
                       ' SELECT ' || seqName ||'.nextval INTO :new.' || fieldname 

|| ' FROM dual; END; ';
END;

----------------------------------------------

PROCEDURE CreatePrimaryKey ( 
          lookupuser VARCHAR2,
         tablename  VARCHAR2,
         fieldname VARCHAR2) 
IS
  counter INT;      
  keyName VARCHAR2 (200 );
BEGIN
     keyName := 'PK_' || UPPER(tablename);

       SELECT COUNT(*) INTO counter FROM dba_indexes WHERE UPPER(index_name) = keyName AND 

owner=UPPER(lookupuser) AND table_name=UPPER(tablename);
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' drop CONSTRAINT 

' || keyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || lookupuser ||'.'|| tablename || ' ADD ( CONSTRAINT ' || 

keyName || ' PRIMARY KEY ( ' || fieldname || ' ) ) ';
END;                    

----------------------------------------------

PROCEDURE CreateForeignKey (
          sourceuser VARCHAR2,
         sourcetable VARCHAR2,
         sourcefields VARCHAR2, 
          targetuser VARCHAR2,
         targettable VARCHAR2,
         targetfields VARCHAR2)
IS
  counter INT;      
  foreignKeyName VARCHAR2 (200 );
BEGIN
     foreignKeyName := 'FK_' || UPPER(SUBSTR(sourcetable,1,10)) || '_' || UPPER 

(SUBSTR(targettable,1,10));
     SELECT COUNT (*) INTO counter FROM dba_constraints WHERE owner=UPPER(sourceuser) AND 

constraint_type='R' 
                                AND table_name=UPPER(sourcetable) AND 

foreignKeyName=constraint_name;
     IF counter > 0 THEN
         EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' drop 

CONSTRAINT ' || foreignKeyName;
    END IF;
    EXECUTE IMMEDIATE 'ALTER TABLE ' || sourceuser ||'.'|| sourcetable || ' ADD ( CONSTRAINT ' || 
                       foreignKeyName || ' FOREIGN KEY ( ' || sourcefields || ' ) 

' ||
                      'REFERENCES ' || targetuser ||'.'|| targettable || ' ( ' ||
                      targetfields || ' ) ON DELETE CASCADE ) ';
END;                    

----------------------------------------------
----------------------------------------------

PROCEDURE CreateAllTables 
IS
BEGIN
     CreateTable  (Tabelspace, OWNER_USER, 'AnswerCell', '( ' ||
     'PACKAGEINSTANCEID     VARCHAR2(50 BYTE)      NOT NULL,' ||
     'QUESTIONID            VARCHAR2(50 BYTE)      NOT NULL,' ||
     'RowNumber				NUMBER(10)             NOT NULL,' ||
     'CellNumber			NUMBER(10)             NOT NULL,' ||
     'TextValue             VARCHAR2(4000 BYTE),' ||
     'NumberValue			NUMBER,' ||
     'YesNoValue			Number(4) default 0,' ||
     'DateValue				DATE' ||
     '  )');
	 
     CreateTable  (Tabelspace, OWNER_USER, 'AnswerRow', '( ' ||
     'PackageInstanceId		VARCHAR2(50 BYTE)      NOT NULL,' ||
     'QuestionId			VARCHAR2(50 BYTE)      NOT NULL,' ||
     'RowNumber				NUMBER(10)             NOT NULL' ||
     '  )');
     
     CreateTable  (Tabelspace, OWNER_USER, 'Clause', '( ' ||
     'clauseName			VARCHAR2(255 BYTE),' ||
     'clauseId				VARCHAR2(255 BYTE) NOT NULL,' ||
     'clauseLibrary			VARCHAR2(100 BYTE) NOT NULL' ||
     '  )');
     
     
     CreateTable  (Tabelspace, OWNER_USER, 'ClauseInstanceUsedTrack', '( ' ||
     'trackId				NUMBER NOT NULL, ' ||
     'clauseId				VARCHAR2(255 BYTE) NOT NULL,' ||
     'PackageInstanceId		VARCHAR2(50 BYTE)  NOT NULL,' ||
     'clauseLibrary			VARCHAR2(100 BYTE) NOT NULL' ||
     '  )');

	 
     CreateTable  (Tabelspace, OWNER_USER, 'ClausePackageUsedTrack', '( ' ||
     'trackId				NUMBER NOT NULL, ' ||
     'clauseId				VARCHAR2(255 BYTE) ,' ||
     'clauseLibrary			VARCHAR2(100 BYTE), ' ||
     'PACKAGEVERSION	NUMBER (10,0)  ,' || 
     'PACKAGEREVISION	NUMBER (10,0)  ,' || 
     'PackageId				VARCHAR2(50 BYTE)' ||
     '  )');
     
     
     CreateTable  (Tabelspace, OWNER_USER, 'DataType', '( ' ||
     'DataTypeCode			CHAR (4) NOT NULL,' ||
     'Name					VARCHAR2(50 BYTE)' ||
     '  )');
	 

     CreateTable  (Tabelspace, OWNER_USER, 'LibraryItem', '( ' ||
	 'itemId		VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'packageId		VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'packageVersion	NUMBER (10,0)  ,' || 
     'packageRevision	NUMBER (10,0)  ,' || 
     'itemName				VARCHAR2(255 BYTE) ,' ||
     'itemType				VARCHAR2(255 BYTE) ' ||
     '  )');
	
	
     CreateTable  (Tabelspace, OWNER_USER, 'Package', '( ' ||
	 'packageId				VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'PACKAGEVERSIONNUMBER	NUMBER (10,0) NOT NULL ,' || 
     'PACKAGEREVISIONNUMBER	NUMBER (10,0) NOT NULL ,' || 
	 'Name					VARCHAR2(50 BYTE),' ||
	 'Description			VARCHAR2(500 BYTE),' ||
	 'CreatedBy 			VARCHAR2(255 BYTE) ,' ||
	 'ModifiedBy 			VARCHAR2(255 BYTE) ,' ||
	 'PublishedBy 			VARCHAR2(255 BYTE) ,' ||
	 'WhenCreated			DATE,' ||
	 'WhenModified			DATE,' ||
	 'WhenModifiedTicks		NUMBER,' ||
	 'WhenPublished			DATE,' ||
	 'WhenDeleted			DATE,' ||
	 'IsDesktopEdition		NUMBER(10) DEFAULT (0),' ||
	 'ServerName 			VARCHAR2(500 BYTE) ,' ||
	 'ServerId				VARCHAR2(50 BYTE) ,' ||
	 'TempOfflineId			VARCHAR2(50 BYTE) ' ||
     '  )');
     
     
     CreateTable  (Tabelspace, OWNER_USER, 'PackageInstance', '( ' ||
	 'PackageInstanceId     VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'packageId				VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'PACKAGEVERSIONNUMBER	NUMBER (10,0) NOT NULL ,' || 
     'PACKAGEREVISIONNUMBER	NUMBER (10,0) NOT NULL ,' || 
	 'Reference 			VARCHAR2(50 BYTE)  NOT NULL,' || 
	 'CreatedByPersonId 	VARCHAR2(255 BYTE)  NOT NULL,' || 
	 'WhenCreated 			DATE NOT NULL,' || 
	 'IsComplete 			Number(4)' || 
     '  )');
     
     
     CreateTable  (Tabelspace, OWNER_USER, 'Question', '( ' ||
	 'PackageInstanceId     VARCHAR2(50 BYTE)  NOT NULL,' ||
	 'QuestionId			VARCHAR2(50 BYTE)  NOT NULL,' ||
     'Name					VARCHAR2(255 BYTE) NOT NULL,' ||
     'DataTypeCode			CHAR (4) NOT NULL,' ||
     'Prompt				VARCHAR2(500 BYTE)' ||
     '  )');

     
     CreateTable  (Tabelspace, OWNER_USER, 'VersionInformation', '( ' ||
	 'Major	NUMBER (10,0) NOT NULL ,' || 
	 'Minor	NUMBER (10,0) NOT NULL ,' || 
	 'Build	NUMBER (10,0) NOT NULL ,' || 
	 'Revision	NUMBER (10,0) NOT NULL ,' || 
	 'SequenceNumber	NUMBER (10,0) NOT NULL ' || 
     '  )');

END;

PROCEDURE CreateSequnces
IS
BEGIN
     CreateSequenceAndTrigger ( OWNER_USER, 'ClauseInstanceUsedTrack', 'trackId'     );
     CreateSequenceAndTrigger ( OWNER_USER, 'ClausePackageUsedTrack', 'trackId'     );
END;


PROCEDURE CreatePrimaryKeys
IS
BEGIN
     CreatePrimaryKey ( OWNER_USER, 'AnswerCell', 'PACKAGEINSTANCEID,QUESTIONID,RowNumber,CellNumber');
     CreatePrimaryKey ( OWNER_USER, 'AnswerRow', 'PackageInstanceId,QuestionId,RowNumber');
     CreatePrimaryKey ( OWNER_USER, 'Clause', 'clauseId');
	 CreatePrimaryKey ( OWNER_USER, 'ClauseInstanceUsedTrack', 'trackId,clauseId');
	 CreatePrimaryKey ( OWNER_USER, 'ClausePackageUsedTrack', 'trackId');
	 CreatePrimaryKey ( OWNER_USER, 'DataType', 'DataTypeCode');
	 CreatePrimaryKey ( OWNER_USER, 'LibraryItem', 'itemId');
	 CreatePrimaryKey ( OWNER_USER, 'Package', 'packageId,PACKAGEVERSIONNUMBER,PACKAGEREVISIONNUMBER');
	 CreatePrimaryKey ( OWNER_USER, 'PackageInstance', 'PackageInstanceId');
	 CreatePrimaryKey ( OWNER_USER, 'Question', 'PackageInstanceId,QuestionId');
END;


----------------------------------------------


 PROCEDURE CreateForeignKeys
 IS
 BEGIN
                        
       CreateForeignKey (OWNER_USER,'AnswerCell','PackageInstanceId,QuestionId,RowNumber',
                             OWNER_USER,'AnswerRow','PackageInstanceId,QuestionId,RowNumber');
       CreateForeignKey (OWNER_USER,'AnswerRow','PackageInstanceId,QuestionId',
							OWNER_USER,'Question','PackageInstanceId,QuestionId');
       CreateForeignKey (OWNER_USER,'ClauseInstanceUsedTrack','clauseId',
							OWNER_USER,'Clause','clauseId');
       CreateForeignKey (OWNER_USER,'ClauseInstanceUsedTrack','trackId',
							OWNER_USER,'ClausePackageUsedTrack','trackId');
       CreateForeignKey (OWNER_USER,'ClausePackageUsedTrack','packageId,PackageVersion,PackageRevision',
							OWNER_USER,'Package','PackageId,PackageVersionNumber,PackageRevisionNumber');
       CreateForeignKey (OWNER_USER,'LibraryItem','packageId,packageVersion,packageRevision',
							OWNER_USER,'Package','PackageId,PackageVersionNumber,PackageRevisionNumber');
       CreateForeignKey (OWNER_USER,'PackageInstance','PackageId,PackageVersionNumber,PackageRevisionNumber',
							OWNER_USER,'Package','PackageId,PackageVersionNumber,PackageRevisionNumber');
       CreateForeignKey (OWNER_USER,'Question','DataTypeCode',
							OWNER_USER,'DataType','DataTypeCode');

END;

 


PROCEDURE CreateProcedure (
          sourceuser VARCHAR2,
         procedurename VARCHAR2,
         pbody VARCHAR2)
IS
BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE PROCEDURE ' || sourceuser ||'.'|| procedurename || pbody; 
END;                    



PROCEDURE CreateFunction (
          sourceuser VARCHAR2,
         procedurename VARCHAR2,
         pbody VARCHAR2)
IS
BEGIN
    EXECUTE IMMEDIATE 'CREATE OR REPLACE FUNCTION ' || sourceuser ||'.'|| procedurename || pbody; 
END;                    




 PROCEDURE CreateProcedures
 IS
 BEGIN
    CreateProcedure ( OWNER_USER,'PACKAGE_PEEK', '(' ||
     '   packageid               IN       VARCHAR2,' ||
     '   IsPreviouslyPublished   IN OUT     NUMBER,' ||
     '   nextVersionNumber       IN OUT   NUMBER,' ||
     '   PreviousDateModified    IN OUT   DATE' ||
     ') ' ||
     'AS ' ||
     '   PackageRowCount   NUMBER (10, 0); ' ||
     '   VersionNumber    NUMBER (10, 0); ' ||
     '   v_packageid        VARCHAR2(50); ' ||
     '   PreviousDate	Date; ' ||
     'BEGIN ' ||
     '   v_packageid := packageid; ' ||
     '    SELECT   Count(1)  ' ||
     '            INTO PackageRowCount ' ||
     '                 FROM ' || OWNER_USER || '.Package where packageid = v_packageid; ' ||
     '   IF PackageRowCount = 0 THEN ' ||
	 ' begin ' ||
	 '			IsPreviouslyPublished := 0; ' ||
	 '		  VersionNumber := 1; ' ||
	 ' end; ' ||
	 '		ELSE ' ||
	 ' begin ' ||
	 '			IsPreviouslyPublished := 1; ' ||
	 '        select COALESCE ( max(PackageVersionNumber), 1) into VersionNumber from ' || OWNER_USER || '.Package where packageid = v_packageid; '||
	 '      SELECT WhenModified into PreviousDate  from ' || OWNER_USER || '.Package WHERE PackageVersionNumber = VersionNumber - 1 AND packageid = v_packageid; '||
	 ' end; ' ||
	 '  END IF; ' ||
	 '	PreviousDateModified := PreviousDate; ' ||
	 '	nextVersionNumber := VersionNumber; ' ||
     'END "PACKAGE_PEEK"; ');
     
     
         CreateProcedure ( OWNER_USER,'PACKAGE_INSERT', '(' ||
     '   packageid               IN       VARCHAR2,' ||
     '   Name IN VARCHAR2,' ||
     '   CreatedBy IN VARCHAR2,' ||
     '   ModifiedBy IN VARCHAR2,' ||
     '   PublishedBy IN VARCHAR2,' ||
     '   WhenCreated IN DATE,' ||
     '   WhenModified IN DATE,' ||
     '   WhenPublished IN DATE,' || 
     '   Description IN VARCHAR2,' ||
     '   RevisionNumber IN NUMBER,' ||
     '   VersionNumber IN NUMBER,' ||
     '   ServerName IN VARCHAR2,' ||
     '   ServerId IN       VARCHAR2,' ||
     '   TempOfflineId IN       VARCHAR2' ||
     ') ' ||
     'AS ' ||
     '		v_PackageId VARCHAR2(50);' ||
     '		v_RevisionNumber NUMBER(10,0);' ||
     '		v_VersionNumber NUMBER(10,0);' ||
     '      v_exist NUMBER (4,0); '||
	 '  BEGIN' ||
	 '		v_PackageId := PackageId; ' ||
     '		v_RevisionNumber := RevisionNumber;' ||
     '		v_VersionNumber := VersionNumber;' ||
     '		SELECT   Count(1)  ' ||
     '            INTO v_exist ' ||
     '                 FROM ' || OWNER_USER || '.Package where packageid = v_packageid ' ||
     '							and PackageVersionNumber = v_VersionNumber and PackageRevisionNumber = v_RevisionNumber; ' ||
     '	    IF v_exist = 0 then  ' ||
     '	    		INSERT INTO Package(PackageId, PackageVersionNumber, PackageRevisionNumber, Name, Description, CreatedBy, ModifiedBy, PublishedBy, WhenCreated,  ' ||
     '	    				WhenModified, WhenPublished, serverName, serverId, tempOfflineId)  ' ||
     '	    		VALUES  ' ||
     '	    			(v_PackageId, v_VersionNumber, v_RevisionNumber, substr(NAME,1,50), Description, CreatedBy, ModifiedBy, PublishedBy, WhenCreated, WhenModified, WhenPublished, ServerName, ServerId, TempOfflineId);  ' ||
     '	    	END IF; ' ||
     'END "PACKAGE_INSERT"; ');


         CreateProcedure ( OWNER_USER,'PACKAGE_COUNT', '(' ||
     '   packageid               IN       VARCHAR2,' ||
     '   Version IN NUMBER,' ||
     '   Revision IN NUMBER, ' ||
     '   PackageCount OUT NUMBER ' ||
     ') ' ||
     '   AS ' ||
     '      v_packageid    varchar2(50); ' ||
     'BEGIN ' ||
     '  v_packageid := PackageId; ' ||
	 '	select count(1) into PackageCount from ' || OWNER_USER || '.package WHERE packageId = v_packageid AND packageVersionNumber = Version AND PackageRevisionNumber = Revision; ' ||
     'END "PACKAGE_COUNT"; '); 
	 
     
         CreateProcedure ( OWNER_USER,'INSTANCE_COUNT', '(' ||
     '   InstanceId  IN       VARCHAR2,' ||
     '   InstanceCount OUT NUMBER ' ||
     ') ' ||
     '   AS ' ||
     'BEGIN ' ||
	 '	select count(1) into InstanceCount from ' || OWNER_USER || '.packageInstance WHERE packageInstanceId = InstanceId; ' ||
     'END "INSTANCE_COUNT"; '); 
	 
	 
	 
         CreateProcedure ( OWNER_USER,'LIBITEM_INSERT', '(' ||
     '   PackageId IN VARCHAR2,' ||
     '   RevisionNumber IN NUMBER,' ||
     '   VersionNumber IN NUMBER,' ||
     '   ItemID IN VARCHAR2,' ||
     '   ItemName IN VARCHAR2,' ||
     '   itemType IN VARCHAR2' ||
     ') ' ||
     '   AS ' ||
     '   BEGIN ' ||
     '   INSERT INTO LibraryItem(PackageId, packageVersion, packageRevision, itemId, itemName, itemType) ' ||
     '   			VALUES ' ||
     '   			(PackageId, VersionNumber, RevisionNumber, ItemID, ItemName, itemType); ' ||
     'END "LIBITEM_INSERT"; '); 


         CreateProcedure ( OWNER_USER,'CLAUSE_INSERT', '(' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseName IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2' ||
     ') ' ||
     '   AS ' ||
     '      v_exist NUMBER (4,0); '||
     '      v_ClauseID VARCHAR2(255);' ||
     '      v_ClauseLibrary VARCHAR2(255);' ||
     '   BEGIN ' ||
     '      v_ClauseID := ClauseID; ' ||
     '		v_ClauseLibrary := ClauseLibrary; ' ||
     '		SELECT   Count(1)  ' ||
     '            INTO v_exist ' ||
     '                 FROM ' || OWNER_USER || '.Clause WHERE clauseId = v_ClauseID AND clauseLibrary = v_ClauseLibrary; ' ||
     '	    IF v_exist = 0 then  ' ||
     '			INSERT INTO ' || OWNER_USER || '.Clause(clauseId, clauseName,clauseLibrary) ' ||
     '					VALUES ' ||
     '						(v_ClauseID, ClauseName, v_ClauseLibrary); ' ||
     '	    	END IF; ' ||
     'END "CLAUSE_INSERT"; ');
	 
	 
	 CreateProcedure ( OWNER_USER,'INSTANCE_CREATE', '(' ||
     '   PackageId IN VARCHAR2,' ||
     '   Version IN NUMBER,' ||
     '   Revision IN NUMBER,' ||
     '   WhenMadeUTC IN DATE,' ||
     '   CreatedBy IN VARCHAR2,' ||
     '   InstanceId IN VARCHAR2' ||
     ') ' ||
     '   AS ' ||
     '      v_packageid    varchar2(50); ' ||
     'BEGIN ' ||
     '  v_packageid := PackageId; ' ||
	 '	INSERT INTO  ' || OWNER_USER || '.PackageInstance(PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, WhenCreated, CreatedByPersonId, reference) ' ||
	 '	SELECT InstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, WhenMadeUTC, CreatedBy, name ' ||
	 '		   FROM ' || OWNER_USER || '.PACKAGE WHERE PackageId = v_PackageId AND PackageVersionNumber = Version AND PackageRevisionNumber = Revision; ' ||
     'END "INSTANCE_CREATE"; ');

	
	 CreateProcedure ( OWNER_USER,'GETANSWERS', '(' ||
     '   PackageUniqueIdentifier IN VARCHAR2, ' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC2     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
     '	 RC3     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' ||
     '	 RC4     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR, ' || 
     '	 RC5     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR  ' ||
     ') ' ||
     '   AS ' ||
     'BEGIN ' ||
	 '   Open rc1 for ' ||
     '   SELECT PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, CreatedByPersonId, WhenCreated, IsComplete FROM ' || OWNER_USER || '.PackageInstance WHERE packageInstanceId = PackageUniqueIdentifier; '||
	 '   Open rc2 for ' ||
     '   SELECT PackageInstanceId, QuestionId, Name, DataTypeCode, Prompt FROM ' || OWNER_USER || '.Question WHERE packageInstanceId = PackageUniqueIdentifier; '||
	 '   Open rc3 for ' ||
     '   SELECT DataTypeCode, Name FROM ' || OWNER_USER || '.DataType; '||
	 '   Open rc4 for ' ||
     '   SELECT PackageInstanceId, QuestionId, RowNumber FROM ' || OWNER_USER || '.AnswerRow WHERE packageInstanceId = PackageUniqueIdentifier; '||
	 '   Open rc5 for ' ||
     '   SELECT PackageInstanceId, QuestionId, RowNumber, CellNumber, TextValue, NumberValue, YesNoValue, DateValue FROM ' || OWNER_USER || '.AnswerCell WHERE packageInstanceId = PackageUniqueIdentifier; '||
     'END "GETANSWERS"; ');
     			

		 CreateProcedure ( OWNER_USER,'QUESTION_INSERT', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   Name IN VARCHAR2, ' ||
     '   DataTypeCode IN CHAR, ' ||
     '   Prompt IN VARCHAR2 ' ||
	     ') ' ||
     '   AS ' ||
     '   v_Name  VARCHAR2(255); ' ||
     'BEGIN ' ||
     '   	v_Name := Name;' ||
	 '		INSERT INTO ' || OWNER_USER || '.Question (packageInstanceId, questionId, name, dataTypeCode, prompt)' ||
	 '		VALUES (PackageInstanceId, QuestionId, v_Name, DataTypeCode, Prompt);' ||
     'END "QUESTION_INSERT"; ');


		 CreateProcedure ( OWNER_USER,'QUESTION_UPDATE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   Name IN VARCHAR2, ' ||
     '   DataTypeCode IN CHAR, ' ||
     '   Prompt IN VARCHAR2 ' ||
	     ') ' ||
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     '   v_Name  VARCHAR2(255); ' ||
     '   v_DataTypeCode CHAR(4); ' ||
     '   v_Prompt VARCHAR2(500); ' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
     '   	v_Name := Name;' ||
     '      v_DataTypeCode := DataTypeCode;' ||
     '      v_Prompt := Prompt;' ||
	 '			UPDATE ' || OWNER_USER || '.Question ' ||
	 '				   SET Name = v_Name, DataTypeCode = v_DataTypeCode, Prompt = v_Prompt ' ||
	 '				   WHERE PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId; ' || 
     'END "QUESTION_UPDATE"; ');
	 
	 CreateProcedure ( OWNER_USER,'QUESTION_DELETE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2 ' ||	
	     ') ' || 
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
	 ' 				DELETE FROM ' || OWNER_USER || '.QUESTION' ||
	 '				   WHERE PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId; ' || 
     'END "QUESTION_DELETE"; ');



		 CreateProcedure ( OWNER_USER,'ANSWERROW_INSERT', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER ' ||
	     ') ' ||
     '   AS ' ||
     'BEGIN ' ||
	 '		INSERT INTO ' || OWNER_USER || '.AnswerRow (packageInstanceId, questionId, RowNumber)' ||
	 '		VALUES (PackageInstanceId, QuestionId, RowNumber);' ||
     'END "ANSWERROW_INSERT"; ');


		 CreateProcedure ( OWNER_USER,'ANSWERROW_UPDATE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER ' ||
	     ') ' ||
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     '   v_RowNumber NUMBER; ' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
     '   	v_RowNumber := RowNumber;' ||
	 '			UPDATE ' || OWNER_USER || '.AnswerRow ' ||
	 '				   SET PackageInstanceId = v_PackageInstanceId, QuestionId = v_QuestionId, RowNumber = v_RowNumber ' ||
	 '				   WHERE PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId and rowNumber = v_RowNumber; ' || 
     'END "ANSWERROW_UPDATE"; ');
	 
	 CreateProcedure ( OWNER_USER,'ANSWERROW_DELETE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER ' ||
	     ') ' || 
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     '   v_RowNumber NUMBER; ' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
     '   	v_RowNumber := RowNumber;' ||
	 ' 				DELETE FROM ' || OWNER_USER || '.AnswerRow' ||
	 '				   WHERE PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId and rowNumber = v_RowNumber; ' || 
     'END "ANSWERROW_DELETE"; ');

	 
		 CreateProcedure ( OWNER_USER,'ANSWERCELL_INSERT', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER, ' ||
     '   CellNumber IN NUMBER, ' ||
     '   TextValue IN VARCHAR2, ' ||
     '   NumberValue NUMBER, ' ||
     '   YesNoValue NUMBER, ' ||
     '   DateValue Date ' ||
	     ') ' ||
     '   AS ' ||
     'BEGIN ' ||
	 '		INSERT INTO ' || OWNER_USER || '.ANSWERCELL (PackageInstanceId, QuestionId, RowNumber, CellNumber, TextValue, NumberValue, YesNoValue, DateValue)' ||
	 '		VALUES (PackageInstanceId, QuestionId, RowNumber, CellNumber, TextValue, NumberValue, YesNoValue, DateValue);' ||
     'END "ANSWERCELL_INSERT"; ');


		 CreateProcedure ( OWNER_USER,'ANSWERCELL_UPDATE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER, ' ||
     '   CellNumber IN NUMBER, ' ||
     '   TextValue IN VARCHAR2, ' ||
     '   NumberValue NUMBER, ' ||
     '   YesNoValue NUMBER, ' ||
     '   DateValue Date ' ||
	     ') ' ||
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     '   v_RowNumber NUMBER; ' ||
     '   v_CellNumber NUMBER; ' ||
     '   v_TextValue VARCHAR2(4000); ' ||
     '   v_NumberValue NUMBER; ' ||
     '   v_YesNoValue NUMBER(4); ' ||
     '   v_DateValue Date; ' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
     '   	v_RowNumber := RowNumber;' ||
     '   	v_CellNumber := CellNumber;' ||
     '   	v_TextValue := TextValue;' ||
     '   	v_NumberValue := NumberValue;' ||
     '   	v_YesNoValue := YesNoValue;' ||
     '   	v_DateValue := DateValue;' ||
	 '			UPDATE ' || OWNER_USER || '.ANSWERCELL ' ||
	 '				SET  PackageInstanceId = v_PackageInstanceId, QuestionId = v_QuestionId, RowNumber = v_RowNumber, CellNumber = v_CellNumber,' || 
	 '				TextValue = v_TextValue, NumberValue = v_NumberValue, YesNoValue = v_YesNoValue, DateValue = v_DateValue ' || 
	 '				WHERE   PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId AND RowNumber = v_RowNumber AND CellNumber = v_CellNumber; ' || 
     'END "ANSWERCELL_UPDATE"; ');
	 
	 CreateProcedure ( OWNER_USER,'ANSWERCELL_DELETE', '(' ||
     '   PackageInstanceId IN VARCHAR2, ' ||
     '   QuestionId IN VARCHAR2, ' ||	
     '   RowNumber IN NUMBER, ' ||
     '   CellNumber IN NUMBER ' ||
	     ') ' || 
     '   AS ' ||
	 '   v_PackageInstanceId VARCHAR2(50);' ||
	 '   v_QuestionId VARCHAR2(50);' ||
     '   v_RowNumber NUMBER; ' ||
     '   v_CellNumber NUMBER; ' ||
     'BEGIN ' ||
	 ' 		v_PackageInstanceId := PackageInstanceId;' ||
	 '		v_QuestionId := QuestionId;' ||
     '   	v_RowNumber := RowNumber;' ||
	 '		v_CellNumber := CellNumber;'||
	 ' 				DELETE FROM ' || OWNER_USER || '.ANSWERCELL' ||
	 '				   WHERE PackageInstanceId = v_PackageInstanceId AND QuestionId = v_QuestionId and rowNumber = v_RowNumber and cellNumber = v_CellNumber; ' || 
     'END "ANSWERCELL_DELETE"; ');

	  CreateProcedure ( OWNER_USER,'CLAUSE_INSTANCE_USED_INSERT', '(' ||
     '   PackageInstanceId IN VARCHAR2,' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2' ||
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
	 ' 				INSERT INTO ' || OWNER_USER || '.ClauseInstanceUsedTrack(PackageInstanceId, clauseId, clauseLibrary)' ||
	 ' 					VALUES' ||
	 ' 					(PackageInstanceId, ClauseID, ClauseLibrary);' ||
     'END "CLAUSE_INSTANCE_USED_INSERT"; ');


	  CreateProcedure ( OWNER_USER,'CLAUSE_PACKAGE_USED_INSERT', '(' ||
     '   PackageId IN VARCHAR2,' ||
     '   PackageVersion IN NUMBER,' ||
     '   PackageRevision IN NUMBER,' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2' ||
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
	 ' 				INSERT INTO ' || OWNER_USER || '.ClausePackageUsedTrack(PackageId, packageVersion, packageRevision, clauseId, clauseLibrary)' ||
	 ' 						VALUES' ||
	 ' 						(PackageId, PackageVersion, PackageRevision, ClauseID, ClauseLibrary);' ||
     'END "CLAUSE_PACKAGE_USED_INSERT"; ');


	  CreateProcedure ( OWNER_USER,'GET_CLAUSES', '(' ||
     '   ClauseName IN VARCHAR2,' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     '   v_ClauseName VARCHAR2(255);' ||
     '   v_ClauseID VARCHAR2(255);' ||
     '   v_ClauseLibrary VARCHAR2(255);' ||
     'BEGIN ' ||
	 '   v_ClauseName := ClauseName;' ||
	 '   v_ClauseID := ClauseID;' ||
	 '   v_ClauseLibrary := ClauseLibrary;' ||
     '   Open RC1 for SELECT DISTINCT clauseName, clauseId, clauseLibrary' ||
     '   		FROM  ' || OWNER_USER || '.Clause ' ||
     '   		WHERE clauseName LIKE ''%'' + COALESCE(v_ClauseName, clauseName)  + ''%''' ||
     '   		AND clauseId  LIKE ''%'' + COALESCE(v_ClauseID, clauseId) + ''%''' ||
     '   		AND clauseLibrary LIKE ''%'' +  COALESCE(v_ClauseLibrary, clauseLibrary) + ''%''' ||
     '   				ORDER BY clauseName;' ||
     'END "GET_CLAUSES"; ');
	 
	 
	  CreateProcedure ( OWNER_USER,'GET_PACKAGES_BY_LIBITEM', '(' ||
     '   ItemID IN VARCHAR2,' ||
     '   PackageName IN VARCHAR2,' ||
     '   StartDate IN DATE,' ||
     '   EndDate IN DATE,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     '   v_ItemID VARCHAR2(50);' ||
     '   v_EndDate Date;' ||
     'BEGIN ' ||
	 '  v_ItemID := ItemID;' ||
	 '  /*v_EndDate := DateADD( ''dd'', 1 , EndDate);*/ v_EndDate := EndDate; ' ||
     '   Open RC1 for SELECT DISTINCT P.PackageId, P.Name, P.WhenCreated, P.PublishedBy, P.ServerName FROM ' || OWNER_USER || '.PACKAGE P' ||
	 '			INNER JOIN ' || OWNER_USER || '.LibraryItem L ON P.PackageId = L.packageId' ||
	 '			WHERE L.itemId = COALESCE(v_ItemID, L.itemId) ' ||
	 '			AND P.Name LIKE ''%'' + COALESCE(PackageName, Name) + ''%''' ||  
	 '			AND P.WhenCreated BETWEEN COALESCE(StartDate, whenCreated) AND COALESCE(v_EndDate, P.whenCreated)' ||
	 '			ORDER BY P.WhenCreated DESC;' ||
	 'END "GET_PACKAGES_BY_LIBITEM"; ');


	 /*
	  CreateProcedure ( OWNER_USER,'CLAUSE_USED_INSERT', '(' ||
     '   PackageInstanceId IN VARCHAR2,' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseName IN VARCHAR2' ||
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
     'INSERT INTO ' || OWNER_USER || '.ClauseUsedTrack(PackageInstanceId, clauseId, clauseName)' ||
     '		 VALUES (PackageInstanceId, ClauseID, ClauseName);' ||
	 'END "CLAUSE_USED_INSERT"; ');
	 */
	 
	  CreateProcedure ( OWNER_USER,'GET_PACKAGES_BY_CLAUSE', '(' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2,' ||
     '   PackageName IN VARCHAR2,' ||
     '   StartDate IN DATE,' ||
     '   EndDate IN DATE,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     '   v_ClauseID VARCHAR2(255);' ||
     '   v_ClauseLibrary VARCHAR2(255);' ||	
     '   v_PackageName VARCHAR2(255);' ||	
     '   v_StartDate Date;' ||
     '   v_EndDate Date;' ||
     'BEGIN ' ||
     '   v_ClauseID := ClauseID;' ||
     '   v_ClauseLibrary := ClauseLibrary;' ||	
     '   v_PackageName := PackageName ;' ||	
     '   v_StartDate := StartDate;' ||
     '   v_EndDate := EndDate + 1;' ||
     '   Open rc1 for SELECT DISTINCT PACKAGE.PackageId, PACKAGE.Name, PACKAGE.WhenCreated, PACKAGE.PublishedBy, PACKAGE.ServerName' ||	 
     '   	  FROM ' || OWNER_USER || '.PACKAGE' ||	
     '   	  		INNER JOIN ' || OWNER_USER || '.ClausePackageUsedTrack ON ClausePackageUsedTrack.packageId = PACKAGE.packageId' ||	  
     '   	  			AND ClausePackageUsedTrack.packageVersion = PACKAGE.packageVersionNumber' ||	
     '   	  			AND ClausePackageUsedTrack.packageRevision = PACKAGE.packageRevisionNumber' ||	
     '   	  		INNER JOIN ' || OWNER_USER || '.Clause ON Clause.clauseID = ClausePackageUsedTrack.ClauseId' ||	
     '   	  		WHERE Clause.clauseId = COALESCE(v_ClauseID, Clause.clauseId)' ||	
     '   	  		AND Clause.clauseLibrary = COALESCE(v_ClauseLibrary, clause.clauseLibrary)' ||	
     '   	  		AND PACKAGE.Name LIKE ''%'' + COALESCE(v_PackageName, Name) + ''%''  ' ||	
     '   	  			AND PACKAGE.WhenCreated BETWEEN COALESCE(v_StartDate, whenCreated) AND COALESCE(v_EndDate, whenCreated)' ||	
     '   	  		ORDER BY PACKAGE.WhenCreated DESC;' ||	
	 'END "GET_PACKAGES_BY_CLAUSE"; ');

	  CreateProcedure ( OWNER_USER,'VERSION_GET_BY_MODID', '(' ||
     '   TempOfflineId IN VARCHAR2,' ||
     '   VersionNumber OUT NUMBER' ||
     '	 ) ' || 
     '   AS ' ||
     '   v_TempOfflineId VARCHAR2(50);' ||
     '   v_ClauseLibrary VARCHAR2(255);' ||	
     '   v_PackageName VARCHAR2(255);' ||	
     '   v_StartDate Date;' ||
     '   v_EndDate Date;' ||
     'BEGIN ' ||
	 ' v_TempOfflineId := TempOfflineId;' ||
	 ' SELECT packageVersionNumber into VersionNumber FROM  ' || OWNER_USER || '.PACKAGE WHERE tempOfflineId = v_TempOfflineId;' ||
	 'END "VERSION_GET_BY_MODID"; ');
	 

	  CreateProcedure ( OWNER_USER,'GET_INSTANCES_BY_CLAUSE', '(' ||
     '   PackageId IN VARCHAR2,' ||
     '   ReferenceName IN VARCHAR2,' ||
     '   StartDate IN DATE,' ||
     '   EndDate IN DATE,' ||
     '   ClauseID IN VARCHAR2,' ||
     '   ClauseLibrary IN VARCHAR2,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     '   v_ClauseID VARCHAR2(255);' ||
     '   v_ClauseLibrary VARCHAR2(255);' ||	
     '   v_ReferenceName VARCHAR2(255);' ||
     '   v_PackageId VARCHAR2(50);' ||
     '   v_StartDate Date;' ||
     '   v_EndDate Date;' ||
     'BEGIN ' ||
     '   v_ClauseID := ClauseID;' ||
     '   v_ClauseLibrary := ClauseLibrary;' ||	
     '   v_ReferenceName := ReferenceName;' ||	
     '   v_PackageId := PackageId;' ||	
     '   v_StartDate := StartDate;' ||
     '   v_EndDate := EndDate + 1;' ||
     '   Open rc1 for SELECT  PackageVersionNumber, PackageRevisionNumber, WhenCreated, Reference, CreatedByPersonId, IsComplete,' ||
     '   			 (SELECT COUNT(*) FROM  ' || OWNER_USER || '.ClauseInstanceUsedTrack ' ||
     '   				 WHERE clauseId = v_ClauseID AND clauseLibrary = v_ClauseLibrary AND ClauseInstanceUsedTrack.PackageInstanceId = PackageInstance.PackageInstanceId)' ||
     '   			FROM   ' || OWNER_USER || '.PackageInstance ' ||
     '   			WHERE packageId = COALESCE(v_PackageId, packageId) ' || 
     '   			AND Reference LIKE ''%'' + COALESCE(v_ReferenceName, reference) + ''%''' ||  
     '   			AND PackageInstance.whenCreated BETWEEN COALESCE(v_StartDate, whenCreated) AND COALESCE(v_EndDate, whenCreated)' ||
     '   			ORDER BY WhenCreated DESC;' ||
	 'END "GET_INSTANCES_BY_CLAUSE"; ');
	 
				

	  CreateProcedure ( OWNER_USER,'GET_INSTANCES_BY_PACKAGE', '(' ||
     '   PackageId IN VARCHAR2,' ||
     '   ReferenceName IN VARCHAR2,' ||
     '   StartDate IN DATE,' ||
     '   EndDate IN DATE,' ||
     '	 RC1     OUT ' || OWNER_USER || '.PERFECTUS_GLOBAL.REF_CUR ' || 
     '	 ) ' || 
     '   AS ' ||
     '   v_ClauseID VARCHAR2(255);' ||
     '   v_ClauseLibrary VARCHAR2(255);' ||	
     '   v_ReferenceName VARCHAR2(255);' ||
     '   v_PackageId VARCHAR2(50);' ||
     '   v_StartDate Date;' ||
     '   v_EndDate Date;' ||
     'BEGIN ' ||
     '   v_ReferenceName := ReferenceName;' ||	
     '   v_PackageId := PackageId;' ||	
     '   v_StartDate := StartDate;' ||
     '   v_EndDate := EndDate + 1;' ||
     '   Open rc1 for ' ||
     '   	 SELECT  PackageVersionNumber, PackageRevisionNumber, WhenCreated, Reference, CreatedByPersonId, IsComplete' ||
     '   		FROM   ' || OWNER_USER || '.PackageInstance' ||
     '   		WHERE packageId = COALESCE(v_PackageId, packageId)' || 
     '   				AND Reference LIKE ''%'' + COALESCE(v_ReferenceName, reference) + ''%''' ||  
     '   				AND PackageInstance.whenCreated BETWEEN COALESCE(v_StartDate, whenCreated) AND COALESCE(v_EndDate, whenCreated)' ||
     '   				ORDER BY WhenCreated DESC;' ||
	 'END "GET_INSTANCES_BY_PACKAGE"; ');

	 
	  CreateProcedure ( OWNER_USER,'STATUS_SETCOMPLETE', '(' ||
     '   InstanceId IN VARCHAR2 ' || 
     '	 ) ' || 
     '   AS ' ||
     'BEGIN ' ||
	 ' UPDATE  ' || OWNER_USER || '.PackageInstance SET IsComplete = 1 WHERE PackageInstanceId = InstanceId;' ||
	 'END "STATUS_SETCOMPLETE"; ');
	 
END;

PROCEDURE CreateData 
IS
BEGIN
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.versioninformation (Major,Minor,Build,Revision,SequenceNumber) Values (5,5,5,0,23)';

EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.DataType values(''text'', ''Text'')';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.DataType values(''numb'', ''Number'')';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.DataType values(''bool'', ''Bool'')';
EXECUTE IMMEDIATE 'insert into ' || OWNER_USER || '.DataType values(''date'', ''Date'')';

END;


----------------------------------------------
----------------------------------------------
        
BEGIN
     success := success AND ReCreateTablespace ( Tabelspace, TabelspacePathName );
     success := success AND ReCreateUser ( OWNER_USER, TRUE, Tabelspace );
     success := success AND ReCreatePackages (OWNER_USER);
    
    CreateAllTables; 
    CreateSequnces;
    CreatePrimaryKeys;
    CreateForeignKeys;
    CreateProcedures;
    CreateData;
    
COMMIT;

END;
/
