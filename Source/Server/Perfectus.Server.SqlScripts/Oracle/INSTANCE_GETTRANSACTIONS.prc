CREATE OR REPLACE PROCEDURE APP_PERFECTUS.Instance_GetTransactions (
	MaxRows		  	 IN number,
 	MinDate 	  	 IN date,
 	SortBy 		  	 IN varchar2 DEFAULT 'WhenCreatedLcl',
 	SortOrder 	  	 IN varchar2 DEFAULT 'DESC',
 	FilterTextColumn IN varchar2 DEFAULT NULL,
  	FilterText 		 IN varchar2 DEFAULT NULL,
  	FilterDateColumn IN varchar2 DEFAULT NULL,
  	FilterFromDate	 IN date 	 DEFAULT NULL,
  	FilterToDate	 IN date 	 DEFAULT NULL,
  	ExStatusValue 	 IN varchar  DEFAULT NULL,
	RC1   		  	 OUT APP_PERFECTUS.PERFECTUS_GLOBAL.REF_CUR
 ) AS

   sql_cmd varchar2(32767);
   mindateutc date;
   filtercol varchar2(100);
   datecol varchar2(100);
   sortcol varchar2(100);

   param1 varchar2(100) := 'p1';
   param2 varchar2(100) := 'p2';
   param3 varchar2(100) := 'p3';
   
   Quote constant varchar2(1) := '''';
   Doubled_Quote constant varchar2(2) := Quote||Quote;	

BEGIN 
	-- Check for a valid sort 
	IF( SortOrder <> 'DESC' AND SortOrder <> 'ASC' ) THEN 
		RETURN;
	END IF;

	-- Convert Min date to Utc and WhenCreate column is Utc
	mindateutc := APP_PERFECTUS.UDF_LOCALTOUTC( MinDate );	
	
	-- Build sql
	sql_cmd := 'SELECT	Reference,PackageVersion,CreatedByPersonId,WhenCreatedLcl,StatusName,LastStatusChange,StatusValue,FriendlyMessage,WhenDeleted,PackageInstanceId ' ||
	'FROM (	' ||
	'	 SELECT 	' || 
	'		p.PackageInstanceId,	' ||
	'		p.Reference,	' ||
	'		( p.PackageVersionNumber + (p.PackageRevisionNumber*0.001)) AS PackageVersion,	' ||
	'		CreatedByPersonId,	' ||
	'		APP_PERFECTUS.UDF_UTCTOLOCAL(WhenCreated) WhenCreatedLcl,	' || 
	'		i.Name AS StatusName,	' ||
	'		NVL(APP_PERFECTUS.UDF_UTCTOLOCAL(p.WhenDeleted),NVL(APP_PERFECTUS.UDF_UTCTOLOCAL(p.WhenCompleted),APP_PERFECTUS.UDF_UTCTOLOCAL(p.WhenCreated))) AS LastStatusChange, ' ||  
	'		p.InterviewState AS StatusValue,	' ||
	'		p.WhenDeleted,	' ||
	'		(	' ||  
	'		CASE	' || 
	'			 WHEN (p.InterviewState = ''DONE'' ) THEN	' ||
	'			    NULL	' ||
	'			 ELSE	' || 
	'			 	(SELECT FriendlyMessage FROM (SELECT e.RelatedPackageInstanceId, e.FriendlyMessage FROM APP_PERFECTUS.ExceptionLog e ' || 
	'					WHERE e.ParentExceptionId IS NULL ORDER BY e.ExceptionId DESC ) WHERE RelatedPackageInstanceId = p.PackageInstanceId AND ROWNUM=1) ' ||	 
	'		END )  AS FriendlyMessage	' ||   
	'	FROM APP_PERFECTUS.PackageInstance p	' || 
	'	INNER JOIN APP_PERFECTUS.InterviewStatus i ON i.InterviewState = p.InterviewState	' ||  
	'	WHERE p.WhenCreated > = :1 ';
	
	-- Status Options
 	IF( ExStatusValue IS NOT NULL ) THEN
 		sql_cmd := sql_cmd || '	AND p.InterviewState NOT IN (' || ExStatusValue || ') ';
 	END IF;

	-- Order by clause
	sql_cmd:=sql_cmd||' ORDER BY '; 
	IF( SortBy IS NOT NULL AND SortBy <> 'WhenCreatedLcl' ) THEN
	 	sortcol:=Replace(SortBy, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||sortcol||' '||SortOrder||', WhenCreatedLcl DESC';
	ELSE
		sql_cmd:=sql_cmd||'WhenCreatedLcl '||SortOrder||' ';
	END IF;

	sql_cmd := sql_cmd || ') InstanceTable WHERE ROWNUM <= :2 '; 
	
	
	-- Filter clauses
 	IF( FilterTextColumn IS NOT NULL AND FilterText IS NOT NULL ) THEN
	 	filtercol:=Replace(FilterTextColumn, Quote, Doubled_Quote);
 		sql_cmd:=sql_cmd||' AND '||filtercol||' LIKE :p1 ';
		param1:=FilterText;
	ELSE
		sql_cmd:=sql_cmd||' AND ''p1'' = :p1 ';
 	END IF;
	IF(FilterDateColumn IS NOT NULL AND FilterFromDate IS NOT NULL ) THEN
	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||' AND '||datecol||' >= :p1 ';
		param2:=FilterFromDate;
	ELSE
		sql_cmd:=sql_cmd||' AND ''p2'' = :p2 ';
	END IF;
	IF( FilterDateColumn IS NOT NULL AND FilterToDate IS NOT NULL ) THEN
	 	datecol:=Replace(FilterDateColumn, Quote, Doubled_Quote);
		sql_cmd:=sql_cmd||' AND '||datecol||' <= :p3 ';
		param3 := FilterToDate;
	ELSE
		sql_cmd:=sql_cmd||' AND ''p3'' = :p3 ';
	END IF;
	

	OPEN RC1 FOR sql_cmd USING mindateutc, MaxRows, param1, param2, param3;

END Instance_GetTransactions; 
