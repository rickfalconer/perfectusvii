CREATE OR REPLACE PROCEDURE APPMR.Job_GetDistributions ( 
	   p_JobQueueID number,
	   RC1 OUT APPMR.PERFECTUS_GLOBAL.REF_CUR,  
	   RC2 OUT APPMR.PERFECTUS_GLOBAL.REF_CUR ) 
AS
  v_CompleteStatusName varchar(100);

BEGIN
	SELECT StatusFriendly INTO v_CompleteStatusName FROM APPMR.JobStatus WHERE (Status = 'COMPLETE');

 	OPEN RC1 FOR SELECT 
		jq.PackageInstanceId AS PackageInstanceId,
		jq.JobQueueID AS JobQueueID,
		dq.DistributorGuid AS DistributorId,
		dq.DistributorName,
		dq.DocumentGuid AS DocumentId,
		dq.DocumentName,
		jq.Status AS StatusValue,
		(SELECT js.StatusFriendly FROM APPMR.JobStatus js WHERE (js.Status = jq.Status)) AS StatusName,
		jq.LastStatusChange, 
		NULL AS InternalReference,
		NULL AS LinkText
	FROM APPMR.JobQueue jq
	LEFT OUTER JOIN APPMR.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueID)
	WHERE jq.JobQueueID = p_JobQueueID
	UNION ALL
	SELECT 
		d.PackageInstanceId AS PackageInstanceId,
		d.JobQueueID AS JobQueueID,
		d.DistributorId AS DistributorId,
		d.DistributorName,
		d.TemplateId AS DocumentId,
		d.TemplateName AS DocumentName,
		'COMPLETE',
		v_CompleteStatusName AS StatusName,
 		APPMR.UDF_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange,  
		d.InternalReference,
		d.LinkText
	FROM APPMR.DistributionResult d 
	WHERE d.JobQueueID = p_JobQueueID
	ORDER BY PackageInstanceId, JobQueueID, DistributorId, DocumentId;

 	OPEN RC2 FOR SELECT 
 		 RelatedPackageInstanceId, JOBQUEUEID, Message AS ExceptionMessage
 	FROM APPMR.ExceptionLog
 	WHERE (ROWNUM = 1)AND(JOBQUEUEID = p_JobQueueID)AND(ParentExceptionId IS NULL);

END Job_GetDistributions; 

