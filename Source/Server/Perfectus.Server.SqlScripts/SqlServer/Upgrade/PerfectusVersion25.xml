﻿<?xml version="1.0" encoding="utf-8" ?>
<SqlScripts>
  <SqlServer>
    <Upgrade From="24" Transaction="true" Note="Upgrading to version 6.0.0.x">
      <![CDATA[
			USE {0}
			GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Instance_GetTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Instance_GetTransactions]
GO

CREATE PROCEDURE [dbo].[Instance_GetTransactions] 
  @MaxRows int,
  @MinDate datetime,
  @SortBy varchar(50) = 'WhenCreatedLcl',
  @SortOrder varchar(4) = 'DESC',
  @FilterTextColumn varchar(50) = NULL,
  @FilterText varchar(100) = NULL,
  @FilterDateColumn varchar(50) = NULL,
  @FilterFromDate datetime = NULL,
  @FilterToDate datetime = NULL,
  @ExStatusValue varchar(100) = NULL

  AS
  BEGIN
  -- Check for a valid sort 
  IF( @SortOrder <> 'DESC' AND @SortOrder <> 'ASC' ) 
  RETURN

  DECLARE @SQLSTRING nvarchar(max)
  DECLARE @PARAMLIST nvarchar(max)                                 

  -- The list of parameters.
  SELECT @PARAMLIST =	'
  @xMaxRows int,
  @xMinDateUtc datetime,
  @xFilterText varchar(100),
  @xFilterFromDate datetime,
  @xFilterToDate datetime'

  -- Convert Min date to Utc and WhenCreate column is Utc
  DECLARE @MinDateUtc datetime
  SET @MinDateUtc = dbo.udf_LOCALTOUTC(@MinDate)

  -- Create the sql
  SET @SQLSTRING = '
  SELECT TOP(@xMaxRows)
	  Reference, 
	  PackageVersion,
	  CreatedByPersonId, 
	  WhenCreatedLcl,
	  StatusName,
	  LastStatusChange,
	  StatusValue,
	  FriendlyMessage,
	  WhenDeleted,
	  PackageInstanceId 
  FROM (
	  SELECT
		  p.PackageInstanceId,
		  p.Reference,
		   ( SELECT (p.PackageVersionNumber + (p.PackageRevisionNumber*0.001))) AS PackageVersion,
		  CreatedByPersonId,
		  dbo.udf_UTCTOLOCAL(p.WhenCreated ) AS WhenCreatedLcl, 
		  i.Name AS StatusName,
		  ( SELECT ISNULL( dbo.udf_UTCTOLOCAL(p.WhenDeleted ), 
			  ISNULL( dbo.udf_UTCTOLOCAL(p.WhenCompleted ), 
			  dbo.udf_UTCTOLOCAL(p.WhenCreated ) ) ) ) AS LastStatusChange,
		  p.InterviewState AS StatusValue,
		  p.WhenDeleted,
		  ( CASE WHEN p.InterviewState = ''DONE'' THEN	NULL
				ELSE ( SELECT TOP(1) e.FriendlyMessage FROM dbo.ExceptionLog e 
			  WHERE (e.RelatedPackageInstanceId = p.PackageInstanceId)AND(e.ParentExceptionId IS NULL) ORDER BY e.ExceptionId DESC ) 
			END ) AS FriendlyMessage
	  FROM dbo.PackageInstance p 
	  INNER JOIN dbo.InterviewStatus i ON i.InterviewState = p.InterviewState 
	  WHERE (p.WhenCreated >= @xMinDateUtc) '

  -- Status Options
  IF( @ExStatusValue IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND p.InterviewState NOT IN (' + @ExStatusValue + ') '

  -- Remaining sql
  SET @SQLSTRING = @SQLSTRING + '
	  ) InstanceTable
  WHERE 1=1 '

  -- Filter clauses
  IF( @FilterTextColumn IS NOT NULL AND @FilterText IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND ' + quotename(@FilterTextColumn) + ' LIKE @xFilterText '
  IF( @FilterDateColumn IS NOT NULL AND @FilterFromDate IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND ' + quotename(@FilterDateColumn) + ' >= @xFilterFromDate '
  IF( @FilterDateColumn IS NOT NULL AND @FilterToDate IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND ' + quotename(@FilterDateColumn) + ' <= @xFilterToDate '

  -- Order by clause
  SET @SQLSTRING = @SQLSTRING + '
  ORDER BY '
  IF( @SortBy IS NOT NULL AND @SortBy <> 'WhenCreatedLcl' )
  SET @SQLSTRING = @SQLSTRING + quotename(@SortBy ) + ' ' + @SortOrder + ', '
  SET @SQLSTRING = @SQLSTRING + 'WhenCreatedLcl ' + @SortOrder + ' '

  EXEC sp_executesql	@SQLSTRING, 
				  @PARAMLIST,
				  @MaxRows,
				  @MinDateUtc,
				  @FilterText,
				  @FilterFromDate,
				  @FilterToDate
END
GO

GRANT EXECUTE ON [dbo].[Instance_GetTransactions] TO [WebAppRole]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Instance_GetDistributions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Instance_GetDistributions]
GO

CREATE PROCEDURE [dbo].[Instance_GetDistributions] 
  @PackageInstanceId uniqueidentifier
  AS
  BEGIN
  DECLARE @CompleteStatusName varchar(100)
  SET @CompleteStatusName = (SELECT StatusFriendly FROM dbo.JobStatus WHERE (Status = 'COMPLETE'))

  SELECT 
  p.PackageInstanceId,
  jq.JobQueueId AS JobQueueId,
  jq.PageToProcess AS PageId,
  jq.PageName,
  dq.DistributorGuid AS DistributorId,
  dq.DistributorName,
  dq.DocumentGuid AS DocumentId,
  dq.DocumentName,
  jq.Status AS StatusValue,
  (SELECT js.StatusFriendly FROM dbo.JobStatus js WHERE (js.Status = jq.Status)) AS StatusName,
  jq.LastStatusChange, 
  NULL AS InternalReference,
  NULL AS LinkText
  FROM dbo.PackageInstance p
  INNER JOIN dbo.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId)
  LEFT OUTER JOIN dbo.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueId)
  WHERE p.PackageInstanceId = @PackageInstanceId
  UNION ALL 
  SELECT 
  p.PackageInstanceId,
  d.JobQueueId AS JobQueueId,
  d.PageId,
  d.PageName,
  d.DistributorId,
  d.DistributorName,
  d.TemplateId AS DocumentId,
  d.TemplateName AS DocumentName,
  'COMPLETE',
  @CompleteStatusName AS StatusName,
  dbo.udf_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange, 
  d.InternalReference,
  d.LinkText
  FROM dbo.PackageInstance p
  INNER JOIN dbo.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId)
  WHERE p.PackageInstanceId = @PackageInstanceId
  ORDER BY JobQueueId 

  SELECT TOP(1) RelatedPackageInstanceId, JobQueueId, Message AS ExceptionMessage
  FROM dbo.ExceptionLog
  WHERE (RelatedPackageInstanceId = @PackageInstanceId)AND(ParentExceptionId IS NULL)
  ORDER BY ExceptionId DESC 
END
GO

GRANT EXECUTE ON [dbo].[Instance_GetDistributions] TO [WebAppRole]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Job_GetTransactions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Job_GetTransactions]
GO

CREATE PROCEDURE [dbo].[Job_GetTransactions] 
  @MaxRows int,
  @MinDate datetime,
  @SortBy varchar(50) = 'JobQueueId',
  @SortOrder varchar(4) = 'DESC',
  @FilterTextColumn varchar(50) = NULL,
  @FilterText varchar(100) = NULL,
  @FilterDateColumn varchar(50) = NULL,
  @FilterFromDate datetime = NULL,
  @FilterToDate datetime = NULL,
  @ExStatusValue varchar(100) = NULL

  AS
  BEGIN
  -- Check for a valid sort 
  IF( @SortOrder <> 'DESC' AND @SortOrder <> 'ASC' ) 
  RETURN

  DECLARE @SQLSTRING nvarchar(max)
  DECLARE @PARAMLIST nvarchar(max)                                 

  -- The list of parameters.
  SELECT @PARAMLIST =	'
  @xMaxRows int,
  @xMinDateUtc datetime,
  @xFilterText varchar(100),
  @xFilterFromDate datetime,
  @xFilterToDate datetime'

  -- Convert Min date to Utc and WhenCreate column is Utc
  DECLARE @MinDateUtc datetime
  SET @MinDateUtc = dbo.udf_LOCALTOUTC(@MinDate)

  -- Create the sql
  SET @SQLSTRING = '
  DECLARE @CompleteStatusName varchar(100)
  SET @CompleteStatusName = (SELECT StatusFriendly FROM dbo.JobStatus WHERE (Status = ''COMPLETE''))

  DECLARE @PackageJobQueueTable TABLE
	  ( PackageInstanceId uniqueidentifier NOT NULL,
	    JobQueueId int NOT NULL,
	    PageId uniqueidentifier NULL,
	    PageName nvarchar(50) NULL,
	    StatusValue varchar(25) NULL,
	    StatusName varchar(100) null,
	    LastStatusChange datetime NULL,
	    PackageVersion float NULL,
	    WhenCreatedLcl datetime NULL,
	    FriendlyMessage nvarchar(255) NULL )

  INSERT @PackageJobQueueTable
	  ( PackageInstanceId, JobQueueId )
      SELECT 
	      p.PackageInstanceId,
	      jq.JobQueueId AS JobQueueId
      FROM dbo.PackageInstance p
      INNER JOIN dbo.JobQueue jq ON (jq.PackageInstanceId = p.PackageInstanceId)
      WHERE (jq.JobQueueId IS NOT NULL) AND (p.WhenCreated >= @xMinDateUtc) 
      UNION 
      SELECT 
	      p.PackageInstanceId,
	      d.JobQueueId AS JobQueueId
      FROM dbo.PackageInstance p
      INNER JOIN dbo.DistributionResult d ON (d.PackageInstanceId = p.PackageInstanceId)
      WHERE (d.JobQueueId IS NOT NULL) AND (p.WhenCreated >= @xMinDateUtc) 
      ORDER BY JobQueueId DESC

  UPDATE @PackageJobQueueTable SET	
	  PageId = ISNULL(jq.PageToProcess, d.PageId),
	  PageName = ISNULL(jq.PageName, d.PageName),
	  StatusValue = ISNULL(jq.Status, ''COMPLETE''),
	  StatusName = ISNULL((SELECT js.StatusFriendly FROM dbo.JobStatus AS js WHERE (js.Status = jq.Status)), @CompleteStatusName),
	  LastStatusChange = dbo.udf_MaxDate( pt.LastStatusChange,
		  dbo.udf_MaxDate(dbo.udf_UTCTOLOCAL(d.WhenLoggedUtc), 
		  jq.LastStatusChange)),
	  PackageVersion = ( SELECT (p.PackageVersionNumber + (p.PackageRevisionNumber*0.001))),
	  WhenCreatedLcl = dbo.udf_UTCTOLOCAL(p.WhenCreated ), 
	  FriendlyMessage = ( CASE WHEN jq.JobQueueId IS NULL THEN NULL
		ELSE (SELECT TOP(1) e.FriendlyMessage FROM dbo.ExceptionLog e WHERE (e.JobQueueId = pt.JobQueueId)AND(e.ParentExceptionId IS NULL) ORDER BY e.ExceptionId DESC)
		END ) 
  FROM @PackageJobQueueTable AS pt
  INNER JOIN dbo.PackageInstance p ON (p.PackageInstanceId = pt.PackageInstanceId)
  LEFT OUTER JOIN dbo.JobQueue jq ON (jq.PackageInstanceId = pt.PackageInstanceId) AND jq.JobQueueId = pt.JobQueueId
  LEFT OUTER JOIN dbo.DistributionResult d ON (d.PackageInstanceId = pt.PackageInstanceId) AND d.JobQueueId = pt.JobQueueId

  SELECT TOP(@xMaxRows)
	  pt.JobQueueId, 
	  pt.PageId,
	  pt.PageName,
	  pt.StatusValue,
	  pt.StatusName,
	  pt.LastStatusChange,
	  p.Reference, 
	  pt.PackageVersion,
	  p.CreatedByPersonId, 
	  pt.WhenCreatedLcl, 
	  pt.FriendlyMessage,
	  pt.PackageInstanceId
  FROM @PackageJobQueueTable pt
  INNER JOIN dbo.PackageInstance p ON (p.PackageInstanceId = pt.PackageInstanceId)
  WHERE 1=1 '

  -- Filter clauses
  IF( @FilterTextColumn IS NOT NULL AND @FilterText IS NOT NULL )
  BEGIN
	  IF( @FilterTextColumn='PackageInstanceId')
		  SET @SQLSTRING = @SQLSTRING + '
			  AND pt.' + quotename(@FilterTextColumn) + ' LIKE @xFilterText '
	  ELSE
		  SET @SQLSTRING = @SQLSTRING + '
			  AND ' + quotename(@FilterTextColumn) + ' LIKE @xFilterText '
  END
  IF( @FilterDateColumn IS NOT NULL AND @FilterFromDate IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND ' + quotename(@FilterDateColumn) + ' >= @xFilterFromDate '
  IF( @FilterDateColumn IS NOT NULL AND @FilterToDate IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND ' + quotename(@FilterDateColumn) + ' <= @xFilterToDate '

  IF( @ExStatusValue IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  AND StatusValue NOT IN (' + @ExStatusValue + ') '

  -- Order by clause
  IF( @SortBy IS NOT NULL )
  SET @SQLSTRING = @SQLSTRING + '
	  ORDER BY ' + quotename(@SortBy) + ' ' + @SortOrder + ' '

  EXEC sp_executesql	@SQLSTRING, 
				  @PARAMLIST,
				  @MaxRows,
				  @MinDateUtc,
				  @FilterText,
				  @FilterFromDate,
				  @FilterToDate
END
GO

GRANT EXECUTE ON [dbo].[Job_GetTransactions] TO [WebAppRole]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Job_GetDistributions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Job_GetDistributions]
GO

CREATE PROCEDURE [dbo].[Job_GetDistributions] 
  @JobQueueId int
  AS
  BEGIN
  DECLARE @CompleteStatusName varchar(100)
  SET @CompleteStatusName = (SELECT StatusFriendly FROM dbo.JobStatus WHERE (Status = 'COMPLETE'))

  SELECT 
  jq.PackageInstanceId AS PackageInstanceId,
  jq.JobQueueId AS JobQueueId,
  dq.DistributorGuid AS DistributorId,
  dq.DistributorName,
  dq.DocumentGuid AS DocumentId,
  dq.DocumentName,
  jq.Status AS StatusValue,
  (SELECT js.StatusFriendly FROM JobStatus js WHERE (js.Status = jq.Status)) AS StatusName,
  jq.LastStatusChange, 
  NULL AS InternalReference,
  NULL AS LinkText
  FROM dbo.JobQueue jq
  LEFT OUTER JOIN dbo.DistributeQueue dq ON (dq.JobQueueId = jq.JobQueueId)
  WHERE jq.JobQueueId = @JobQueueId
  UNION ALL
  SELECT 
  d.PackageInstanceId AS PackageInstanceId,
  d.JobQueueId AS JobQueueId,
  d.DistributorId AS DistributorId,
  d.DistributorName,
  d.TemplateId AS DocumentId,
  d.TemplateName AS DocumentName,
  'COMPLETE',
  @CompleteStatusName AS StatusName,
  dbo.udf_UTCTOLOCAL(d.WhenLoggedUtc) AS LastStatusChange, 
  d.InternalReference,
  d.LinkText
  FROM dbo.DistributionResult d 
  WHERE d.JobQueueId = @JobQueueId
  ORDER BY PackageInstanceId, JobQueueId, DistributorId, DocumentId

  SELECT TOP(1) RelatedPackageInstanceId, JobQueueId, Message AS ExceptionMessage
  FROM dbo.ExceptionLog
  WHERE (JobQueueId = @JobQueueId)AND(ParentExceptionId IS NULL)
  ORDER BY ExceptionId DESC 
END
GO

GRANT EXECUTE ON [dbo].[Job_GetDistributions] TO [WebAppRole]
GO

UPDATE VersionInformation SET Major = 6, 
						      Minor = 0, 
						      Build = 0, 
						      Revision = 0, 
						      SequenceNumber = 25				

                          ]]>
    </Upgrade>
  </SqlServer>
</SqlScripts>
