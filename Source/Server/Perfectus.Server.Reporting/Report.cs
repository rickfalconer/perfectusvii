using System;
using System.Net;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.DataAccess;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Reporting.WebApi.Reporting;

namespace Perfectus.Server.Reporting
{

	public sealed class Report
	{
        // All the isReportingEnabled stuff in this class should probably be moved out to the calling application. for example: check if reporting is enabled in the web interface, if so then call the reporting method, rather than always calling the reporting method and letting that do the check.

		#region Reporting Package Retrieval

		public static ReportSet GetPackagesBySharedLibraryElement(Guid libraryItemId, string packageName, DateTime startDate, DateTime endDate)
		{
			if(packageName != null && packageName.Length == 0)
			{
				packageName = null;
			}

			return DataAccess.Reporting.Report.GetPackagesBySharedLibraryElement(libraryItemId, packageName, startDate, endDate);
		}

		public static ReportSet GetPackagesByClause(string clauseId, string clauseName, string packageName, DateTime startDate, DateTime endDate)
		{
			if(clauseId != null && clauseId.Length == 0)
			{
				clauseId = null;
			}

			if(clauseName != null && clauseName.Length == 0)
			{
				clauseName = null;
			}

			if(packageName != null && packageName.Length == 0)
			{
				packageName = null;
			}

			return DataAccess.Reporting.Report.GetPackagesByClause(clauseId, clauseName, packageName, startDate, endDate);
		}

		public static int OfflineGetVersionByTempOfflineId(Guid tempOfflineId)
		{
			return DataAccess.Reporting.Report.OfflineGetVersionByTempOfflineId(tempOfflineId);
		}

		#endregion

		#region Reporting Publishing

        public static void PublishReportingPackageInfo(Guid packageId, int personId, string name, string createdBy, string modifiedBy, string publishedBy, DateTime createdTime, DateTime modifiedTime, DateTime publishedTime, string description, int versionNumber, int revisionNumber, string serverName)
        {
            DataAccess.Reporting.Report.PublishReportingPackageInfo(packageId, personId, name, createdBy, modifiedBy, publishedBy, createdTime, modifiedTime, publishedTime, description, versionNumber, revisionNumber, serverName, Guid.Empty);
        }

        /// <summary>
        /// Used by the desktop edition for 'publishing' when the core database is not available. 
        /// Nothing is published as such, however it means that package information will get into the reporting database. 
        /// 
        /// If the package has not been published previously the version will start at 1.
        /// The version will increment by 1 if the last modified date/time on the package has changed.
        /// If the package is already in the reporting database and the modified/datetime is the same then nothing will happen.
        /// </summary>
        /// <param name="package"></param>
        /// <param name="versionNumber"></param>
        public static void PublishOffline(Package package, out int versionNumber)
        {
            versionNumber = int.MinValue;
            versionNumber = PublishReportingPackageInfo(package, versionNumber, true); // true - desktop edition
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="package"></param>
        /// <param name="versionNumber"></param>
        /// <param name="publishIsOffline">Indicates whether the publish is from Desktop (true) or the web interview.</param>
        private static int PublishReportingPackageInfo(Package package, int versionNumber, bool publishIsOffline)
        {
            Guid id = package.UniqueIdentifier;
            string name = package.Name;
            string createdBy = package.CreatedBy;
            string modifiedBy = package.ModifiedBy;
            string publishedBy = package.PublishedBy;
            DateTime createdUTC = package.CreationDateTime;
            DateTime modifiedUTC = package.ModificationDateTime;
            DateTime publishedUTC = package.PublishedDateTime;
            string description = package.Description;
            Guid tempOfflineId = package.TempOfflineId;
            string serverName;
            Guid serverId;

            if (publishIsOffline)
            {
                //TODO: Resource/localize
                serverName = "Desktop edition";
                serverId = Guid.Empty;
            }
            else
            {
                serverName = Perfectus.Server.ConfigurationSystem.Config.Shared.identity.serverName;
            }

            int personId = int.MinValue;
            DateTime prevModifiedUTC;
            int nextVersionNumber;

            package.PublishedDateTime = DateTime.UtcNow;
            bool previouslyPublished = DataAccess.Reporting.Report.IsPackagePublished(id, out prevModifiedUTC, out nextVersionNumber);

            // If it has been previously published to reporting database we need to increment version number by 1.
            if (!previouslyPublished || modifiedUTC != prevModifiedUTC)
            {
                if (publishIsOffline)
                {
                    // Handle desktop versioning
                    versionNumber = int.MinValue;

                    if (!previouslyPublished)
                    {
                        versionNumber = 1;
                    }
                    else
                    {
                        versionNumber = nextVersionNumber;
                    }
                }
                else
                {
                    // Ensure the version number is set
                    if (versionNumber <= 0)
                    {
                        versionNumber = 1;
                    }
                }

                DataAccess.Reporting.Report.PublishReportingPackageInfo(id, personId, name, createdBy, modifiedBy, publishedBy, createdUTC, modifiedUTC, publishedUTC, description, versionNumber, 0, serverName, tempOfflineId);
                DataAccess.Reporting.Report.PublishReportingLibItemInfo(id, package, versionNumber, 0);
                DataAccess.Reporting.Report.PublishReportingClauseInfo(id, package.Outcomes, versionNumber, 0);
            }

            return versionNumber;
        }        

        public static bool DoesPackageExist(Guid packageId, int version, int revision)
        {
            return DataAccess.Reporting.Report.DoesPackageExist(packageId, version, revision);
        }

		#endregion

		#region Reporting Instance Management

        public static void CreateReportingInstance(Guid packageId, Guid instanceId, int version, int revision, DateTime dateTime, string userIden)
        {
            CreateReportingInstance(packageId, instanceId, version, revision, dateTime, userIden, false);
        }

        public static void CreateReportingInstance(Guid packageId, Guid instanceId, int version, int revision, DateTime dateTime, string userIden, bool ignoreConfigIsAllowed)
        {
            bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;

            if (ignoreConfigIsAllowed || isReportingEnabled)
            {
                DataAccess.Reporting.Report.CreateReportingInstance(packageId, instanceId, version, revision, dateTime, userIden);
            }
        }

		public static void CreateOfflineInstance(Guid packageId, Guid instanceId, int version, int revision)
		{
			string userId = Identity.GetRunningUserId();
			DateTime dateTime = DateTime.UtcNow;			
			DataAccess.Reporting.Report.CreateReportingInstance(packageId, instanceId, version, revision, dateTime, userId);
		}

        public static ReportSet GetInstancesByClauseUsed(string clauseId, string clauseName, Guid packageId, string referenceName, DateTime startDate, DateTime endDate)
        {
            if (clauseId != null && clauseId.Length == 0)
            {
                clauseId = null;
            }

            if (clauseName != null && clauseName.Length == 0)
            {
                clauseName = null;
            }

            if (referenceName != null && referenceName.Length == 0)
            {
                referenceName = null;
            }

            return DataAccess.Reporting.Report.GetInstancesByClauseUsed(clauseId, clauseName, packageId, referenceName, startDate, endDate);
        }

        public static ReportSet GetInstancesByPackage(Guid packageId, string referenceName, DateTime startDate, DateTime endDate)
        {
            return DataAccess.Reporting.Report.GetInstancesByPackage(packageId, referenceName, startDate, endDate);
        }

        public static ReportSet GetClauses(string clauseId, string clauseName, string clauseLibrary)
        {
            if (clauseId != null && clauseId.Length == 0)
            {
                clauseId = null;
            }

            if (clauseName != null && clauseName.Length == 0)
            {
                clauseName = null;
            }

            if (clauseLibrary != null && clauseLibrary.Length == 0)
            {
                clauseLibrary = null;
            }

            return DataAccess.Reporting.Report.GetClauses(clauseId, clauseName, clauseLibrary);
        }

        public static bool DoesInstanceExist(Guid packageInstanceId)
        {            
           return DataAccess.Reporting.Report.DoesInstanceExist(packageInstanceId);
        }

		#endregion

		#region Reporting Instance Status Methods

		public static void SetReportingInstanceComplete(Guid instanceId)
		{				
			SetReportingInstanceComplete(instanceId, false);
		}

		public static void SetReportingInstanceComplete(Guid instanceId, bool isOffline)
		{	
			bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;

			if(isReportingEnabled || isOffline)
			{
				DataAccess.Reporting.Report.SetReportingInstanceComplete(instanceId);
			}
		}

		public static void SetReportingClauseUsed(Guid instanceId, string clauseId, string clauseName)
		{		
			bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;

			if(isReportingEnabled)
			{
				DataAccess.Reporting.Report.SetReportingClauseUsed(instanceId, clauseId, clauseName);
			}
		}

		public static void SetReportingClauseUsed(Guid instanceId, string uniqueLibraryKey, string clauseName, bool isOffline, string offlineReportingUrl)
		{
			if(! isOffline)
			{
				SetReportingClauseUsed(instanceId, uniqueLibraryKey, clauseName);	
			}
			else
			{
				// We have to do things via a web service, as the calling application wont know about the reporting database.
				using(OfflineInstanceManagement srv = new OfflineInstanceManagement())
				{
					string url = offlineReportingUrl;

					if (url[url.Length - 1] != '/')
					{
						url += "/";
					}
                
					srv.Url = url + "reports/desktopInstanceManagement.asmx";

					srv.Credentials = CredentialCache.DefaultCredentials;
					srv.SetOfflineClauseUsed(instanceId, uniqueLibraryKey, clauseName);
				}
			}
		}

        public static InstanceInfoSet GetInstanceInfoFromCoreDB(DateTime dateToGetPackagesFrom)
        {
            return DataAccess.Reporting.Report.GetInstanceInfoFromCoreDB(dateToGetPackagesFrom);
        }

		#endregion

        #region Reporting Store Instance Answers Methods

        public static void SaveAnswers(Guid packageInstanceId, ReportAnswerSet ds, AnswerProvider.Caller caller)
        {
            bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;
            bool isAnswerReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableAnswerReporting;

            if (isReportingEnabled && isAnswerReportingEnabled)
            {
                DataAccess.Reporting.Report.SaveAnswers(packageInstanceId, ds);
                DataAccess.InterviewSystem.InstanceManagement.SetSynchronized(packageInstanceId);
            }
        }

        public static void SaveAnswers(Package instance, Guid packageInstanceId, int version, int revision, AnswerProvider.Caller caller)
        {
            SaveAnswers(instance, packageInstanceId, version, revision, caller, false);
        }

        public static void SaveAnswers(Package instance, Guid packageInstanceId, int version, int revision, AnswerProvider.Caller caller, 
                                       bool ignoreConfigIsAllowed)
        {
            bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;
            bool isAnswerReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableAnswerReporting;

            if (ignoreConfigIsAllowed == true || (isReportingEnabled && isAnswerReportingEnabled))
            {
                // Ensure the package & instance exists in the reporting db
                EnsureReportingPackageExists(instance, packageInstanceId, version, revision);
                
                ReportAnswerSet ds = new ReportAnswerSet();
                BuildReportAnswerSet(packageInstanceId, instance, caller, ds);
                DataAccess.Reporting.Report.SaveAnswers(packageInstanceId, ds);
                DataAccess.InterviewSystem.InstanceManagement.SetSynchronized(packageInstanceId);
            }
        }

        /// <summary>
        ///     Ensures the given package instance exists in the reporting database, if not existing the instance information is added
        ///     to the reporting database.
        /// </summary>
        /// <param name="instance">The given package instance reference.</param>
        /// <param name="packageInstanceId">The Guid of the package instance.</param>
        /// <param name="version">The version of the package.</param>
        /// <param name="revision">The revision number of the package.</param>
        private static void EnsureReportingPackageExists(Package instance, Guid packageInstanceId, int version, int revision)
        { 
            // Ensure the package exists in the reporting db
            if (!DoesPackageExist(instance.UniqueIdentifier, version, revision))
            {
                PublishReportingPackageInfo(instance, version, false); // false - not desktop
            }

            // Ensure the instance exists in the reporting db
            if (!DoesInstanceExist(packageInstanceId))
            {
                CreateReportingInstance(instance.UniqueIdentifier, packageInstanceId, version, revision, DateTime.Now, Identity.GetRunningUserId());
            }
        }

        private static void BuildReportAnswerSet(Guid packageInstanceId, Package instance, AnswerProvider.Caller caller, ReportAnswerSet ds)
        {
            foreach (Question q in instance.Questions)
            {
                if (q.DataType == Perfectus.Common.PerfectusDataType.Number || q.DataType == Perfectus.Common.PerfectusDataType.Text || q.DataType == Perfectus.Common.PerfectusDataType.YesNo || q.DataType == Perfectus.Common.PerfectusDataType.Date)
                {
                    InsertQuestionRows(packageInstanceId, q, true, caller, ds);
                    InsertAnswers(q, packageInstanceId, caller, ds);
                }
            }
        }

        private static void InsertQuestionRows(Guid packageInstanceId, Question q, bool deleteExisting, AnswerProvider.Caller caller, ReportAnswerSet ds)
        {
            ReportAnswerSet.QuestionRow qr = ds.Question.NewQuestionRow();
            qr.PackageInstanceId = packageInstanceId;
            qr.QuestionId = q.UniqueIdentifier;
            qr.DataTypeCode = GetDataTypeCode(q.DataType);
            qr.Name = q.Name;
            qr.Prompt = q.Prompt.Evaluate(caller, q.StringBindingMetaData);
            ds.Question.Rows.Add(qr);
        }

        private static void InsertAnswers(Question q, Guid packageInstanceId, AnswerProvider.Caller caller, ReportAnswerSet ds)
        {
            AnswerCollectionCollection collColl = q.GetAnswerCollectionCollection(caller);

            if (collColl != null)
            {
                for (int j = 0; j < collColl.Count; j++)
                {
                    AnswerCollection coll = collColl[j];

                    ReportAnswerSet.AnswerRowRow answerRowRow = ds.AnswerRow.NewAnswerRowRow();
                    answerRowRow.PackageInstanceId = packageInstanceId;
                    answerRowRow.QuestionId = q.UniqueIdentifier;
                    answerRowRow.RowNumber = j + 1;

                    ds.AnswerRow.Rows.Add(answerRowRow);

                    for (int k = 0; k < coll.Count; k++)
                    {
                        object answer = AnswerProvider.ConvertToBestType(coll[k], q.DataType, caller, q.StringBindingMetaData);

                        ReportAnswerSet.AnswerCellRow cellRow = ds.AnswerCell.NewAnswerCellRow();
                        cellRow.PackageInstanceId = packageInstanceId;
                        cellRow.QuestionId = q.UniqueIdentifier;
                        cellRow.RowNumber = j + 1;
                        cellRow.CellNumber = k + 1;

                        if (answer is decimal)
                        {
                            cellRow.NumberValue = Convert.ToDecimal(answer);
                        }
                        else if (answer is DateTime)
                        {
                            cellRow.DateValue = (DateTime)answer;
                        }
                        else if (answer is bool)
                        {
                            cellRow.YesNoValue = (bool)answer;
                        }
                        else
                        {
                            cellRow.TextValue = answer.ToString();
                        }

                        ds.AnswerCell.Rows.Add(cellRow);
                    }
                }
            }
        }

        private static string GetDataTypeCode(Perfectus.Common.PerfectusDataType dataType)
        {
            switch (dataType)
            {
                case Perfectus.Common.PerfectusDataType.Text:
                    return "text";
                case Perfectus.Common.PerfectusDataType.Date:
                    return "date";
                case Perfectus.Common.PerfectusDataType.Number:
                    return "numb";
                case Perfectus.Common.PerfectusDataType.YesNo:
                    return "bool";
                default:
                    return "text";
            }
        }

        #endregion 
    }
}
