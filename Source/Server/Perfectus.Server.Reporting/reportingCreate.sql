IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'PerfectusReporting')
	DROP DATABASE [PerfectusReporting]
GO

CREATE DATABASE [PerfectusReporting]  ON (NAME = N'PerfectusReporting_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL$SIMON\Data\PerfectusReporting_Data.MDF' , SIZE = 2, FILEGROWTH = 10%) LOG ON (NAME = N'PerfectusReporting_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL$SIMON\Data\PerfectusReporting_Log.LDF' , SIZE = 1, FILEGROWTH = 10%)
 COLLATE Latin1_General_CI_AS
GO

exec sp_dboption N'PerfectusReporting', N'autoclose', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'bulkcopy', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'trunc. log', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'torn page detection', N'true'
GO

exec sp_dboption N'PerfectusReporting', N'read only', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'dbo use', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'single', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'autoshrink', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'ANSI null default', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'recursive triggers', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'ANSI nulls', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'concat null yields null', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'cursor close on commit', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'default to local cursor', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'quoted identifier', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'ANSI warnings', N'false'
GO

exec sp_dboption N'PerfectusReporting', N'auto create statistics', N'true'
GO

exec sp_dboption N'PerfectusReporting', N'auto update statistics', N'true'
GO

if( (@@microsoftversion / power(2, 24) = 8) and (@@microsoftversion & 0xffff >= 724) )
	exec sp_dboption N'PerfectusReporting', N'db chaining', N'false'
GO

use [PerfectusReporting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ClauseInstanceUsedTrack_Clause]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ClauseInstanceUsedTrack] DROP CONSTRAINT FK_ClauseInstanceUsedTrack_Clause
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ClausePackageUsedTrack_Clause]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ClausePackageUsedTrack] DROP CONSTRAINT FK_ClausePackageUsedTrack_Clause
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ClausePackageUsedTrack_Package]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ClausePackageUsedTrack] DROP CONSTRAINT FK_ClausePackageUsedTrack_Package
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LibraryItem_Package]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LibraryItem] DROP CONSTRAINT FK_LibraryItem_Package
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_PackageInstance_Package]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[PackageInstance] DROP CONSTRAINT FK_PackageInstance_Package
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ClauseInstanceUsedTrack_PackageInstance]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ClauseInstanceUsedTrack] DROP CONSTRAINT FK_ClauseInstanceUsedTrack_PackageInstance
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CLAUSE_INSTANCE_USED_INSERT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CLAUSE_INSTANCE_USED_INSERT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CLAUSE_PACKAGE_USED_INSERT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[CLAUSE_PACKAGE_USED_INSERT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Clause_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Clause_Insert]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GET_CLAUSES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GET_CLAUSES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GET_INSTANCES_BY_CLAUSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GET_INSTANCES_BY_CLAUSE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GET_INSTANCES_BY_PACKAGE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GET_INSTANCES_BY_PACKAGE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GET_PACKAGES_BY_CLAUSE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GET_PACKAGES_BY_CLAUSE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GET_PACKAGES_BY_LIBITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GET_PACKAGES_BY_LIBITEM]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[INSTANCE_CREATE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[INSTANCE_CREATE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LIBITEM_INSERT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LIBITEM_INSERT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PACKAGE_INSERT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PACKAGE_INSERT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PACKAGE_PEEK]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PACKAGE_PEEK]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[STATUS_SETCOMPLETE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[STATUS_SETCOMPLETE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VERSION_GET_BY_MODID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[VERSION_GET_BY_MODID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Clause]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Clause]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClauseInstanceUsedTrack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClauseInstanceUsedTrack]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClausePackageUsedTrack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClausePackageUsedTrack]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LibraryItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[LibraryItem]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Package]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Package]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PackageInstance]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PackageInstance]
GO

if not exists (select * from master.dbo.syslogins where loginname = N'APP_Perfectus')
BEGIN
	declare @logindb nvarchar(132), @loginlang nvarchar(132) select @logindb = N'Perfectus514', @loginlang = N'us_english'
	if @logindb is null or not exists (select * from master.dbo.sysdatabases where name = @logindb)
		select @logindb = N'master'
	if @loginlang is null or (not exists (select * from master.dbo.syslanguages where name = @loginlang) and @loginlang <> N'us_english')
		select @loginlang = @@language
	exec sp_addlogin N'APP_Perfectus', null, @logindb, @loginlang
END
GO

if not exists (select * from master.dbo.syslogins where loginname = N'humanusblue\Simon')
	exec sp_grantlogin N'humanusblue\Simon'
	exec sp_defaultdb N'humanusblue\Simon', N'master'
	exec sp_defaultlanguage N'humanusblue\Simon', N'us_english'
GO

exec sp_addsrvrolemember N'humanusblue\Simon', sysadmin
GO

if not exists (select * from dbo.sysusers where name = N'APP_Perfectus')
	EXEC sp_grantdbaccess N'APP_Perfectus', N'APP_Perfectus'
GO

exec sp_addrolemember N'db_owner', N'APP_Perfectus'
GO

CREATE TABLE [dbo].[Clause] (
	[clauseName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[clauseId] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,
	[clauseLibrary] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ClauseInstanceUsedTrack] (
	[trackId] [int] IDENTITY (1, 1) NOT NULL ,
	[clauseId] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL ,
	[packageInstanceId] [uniqueidentifier] NULL ,
	[clauseLibrary] [nvarchar] (100) COLLATE Latin1_General_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ClausePackageUsedTrack] (
	[trackId] [int] IDENTITY (1, 1) NOT NULL ,
	[clauseId] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL ,
	[clauseLibrary] [nvarchar] (100) COLLATE Latin1_General_CI_AS NULL ,
	[packageVersion] [int] NULL ,
	[packageRevision] [int] NULL ,
	[packageId] [uniqueidentifier] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[LibraryItem] (
	[itemId] [uniqueidentifier] NOT NULL ,
	[packageId] [uniqueidentifier] NOT NULL ,
	[packageVersion] [int] NOT NULL ,
	[packageRevision] [int] NOT NULL ,
	[itemName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[itemType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Package] (
	[PackageId] [uniqueidentifier] NOT NULL ,
	[PackageVersionNumber] [int] NOT NULL ,
	[PackageRevisionNumber] [int] NOT NULL ,
	[Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PublishedBy] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WhenCreated] [datetime] NULL ,
	[WhenModified] [datetime] NULL ,
	[WhenModifiedTicks] [bigint] NULL ,
	[WhenPublished] [datetime] NULL ,
	[WhenDeleted] [datetime] NULL ,
	[IsDesktopEdition] [int] NOT NULL ,
	[ServerName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ServerId] [uniqueidentifier] NULL ,
	[TempOfflineId] [uniqueidentifier] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[PackageInstance] (
	[PackageInstanceId] [uniqueidentifier] NOT NULL ,
	[PackageId] [uniqueidentifier] NOT NULL ,
	[PackageVersionNumber] [int] NOT NULL ,
	[PackageRevisionNumber] [int] NOT NULL ,
	[Reference] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CreatedByPersonId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[WhenCreated] [datetime] NOT NULL ,
	[IsComplete] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Clause] WITH NOCHECK ADD 
	CONSTRAINT [PK_Clause] PRIMARY KEY  CLUSTERED 
	(
		[clauseId],
		[clauseLibrary]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ClauseInstanceUsedTrack] WITH NOCHECK ADD 
	CONSTRAINT [PK_ClauseUsedTrack] PRIMARY KEY  CLUSTERED 
	(
		[trackId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ClausePackageUsedTrack] WITH NOCHECK ADD 
	CONSTRAINT [PK_ClausePackageUsedTrack] PRIMARY KEY  CLUSTERED 
	(
		[trackId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LibraryItem] WITH NOCHECK ADD 
	CONSTRAINT [PK_PackageItem] PRIMARY KEY  CLUSTERED 
	(
		[itemId],
		[packageId],
		[packageVersion],
		[packageRevision]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Package] WITH NOCHECK ADD 
	CONSTRAINT [PK_Package] PRIMARY KEY  CLUSTERED 
	(
		[PackageId],
		[PackageVersionNumber],
		[PackageRevisionNumber]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[PackageInstance] WITH NOCHECK ADD 
	CONSTRAINT [PK_PackageInstance] PRIMARY KEY  CLUSTERED 
	(
		[PackageInstanceId]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Package] ADD 
	CONSTRAINT [DF_Package_IsDesktopEdition] DEFAULT (0) FOR [IsDesktopEdition]
GO

ALTER TABLE [dbo].[PackageInstance] ADD 
	CONSTRAINT [DF_PackageInstance_IsComplete] DEFAULT (0) FOR [IsComplete]
GO

ALTER TABLE [dbo].[ClauseInstanceUsedTrack] ADD 
	CONSTRAINT [FK_ClauseInstanceUsedTrack_Clause] FOREIGN KEY 
	(
		[clauseId],
		[clauseLibrary]
	) REFERENCES [dbo].[Clause] (
		[clauseId],
		[clauseLibrary]
	),
	CONSTRAINT [FK_ClauseInstanceUsedTrack_PackageInstance] FOREIGN KEY 
	(
		[packageInstanceId]
	) REFERENCES [dbo].[PackageInstance] (
		[PackageInstanceId]
	)
GO

ALTER TABLE [dbo].[ClausePackageUsedTrack] ADD 
	CONSTRAINT [FK_ClausePackageUsedTrack_Clause] FOREIGN KEY 
	(
		[clauseId],
		[clauseLibrary]
	) REFERENCES [dbo].[Clause] (
		[clauseId],
		[clauseLibrary]
	),
	CONSTRAINT [FK_ClausePackageUsedTrack_Package] FOREIGN KEY 
	(
		[packageId],
		[packageVersion],
		[packageRevision]
	) REFERENCES [dbo].[Package] (
		[PackageId],
		[PackageVersionNumber],
		[PackageRevisionNumber]
	)
GO

ALTER TABLE [dbo].[LibraryItem] ADD 
	CONSTRAINT [FK_LibraryItem_Package] FOREIGN KEY 
	(
		[packageId],
		[packageVersion],
		[packageRevision]
	) REFERENCES [dbo].[Package] (
		[PackageId],
		[PackageVersionNumber],
		[PackageRevisionNumber]
	)
GO

ALTER TABLE [dbo].[PackageInstance] ADD 
	CONSTRAINT [FK_PackageInstance_Package] FOREIGN KEY 
	(
		[PackageId],
		[PackageVersionNumber],
		[PackageRevisionNumber]
	) REFERENCES [dbo].[Package] (
		[PackageId],
		[PackageVersionNumber],
		[PackageRevisionNumber]
	)
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.CLAUSE_INSTANCE_USED_INSERT    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE Procedure CLAUSE_INSTANCE_USED_INSERT
(	
	@PackageInstanceId uniqueidentifier,	
	@ClauseId nvarchar(255),
	@clauseLibrary nvarchar(255)	
)
AS
BEGIN 
	INSERT INTO ClauseInstanceUsedTrack(PackageInstanceId, clauseId, clauseLibrary)
		VALUES
		(@PackageInstanceId, @ClauseId, @clauseLibrary)
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.CLAUSE_PACKAGE_USED_INSERT    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE Procedure CLAUSE_PACKAGE_USED_INSERT
(	
	@PackageId uniqueidentifier,	
	@packageVersion int,
	@packageRevision int,
	@ClauseId nvarchar(255),
	@clauseLibrary nvarchar(100)	
)
AS
	BEGIN 
		INSERT INTO ClausePackageUsedTrack(PackageId, packageVersion, packageRevision, clauseId, clauseLibrary)
			VALUES
			(@PackageId, @packageVersion, @packageRevision, @ClauseId, @clauseLibrary)
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.CLAUSE_INSERT    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE Procedure Clause_Insert
(	
	@ClauseId nvarchar(255),
	@ClauseName nvarchar(255),
	@ClauseLibrary nvarchar(255)
)
AS
BEGIN 

	DECLARE @count int
	
	SET @count = (SELECT COUNT(*) FROM Clause WHERE clauseId = @ClauseId AND clauseLibrary = @clauseLibrary)

	IF @count = 0
	BEGIN
	INSERT INTO Clause(clauseId, clauseName,clauseLibrary)
		VALUES
			(@clauseId, @clauseName, @clauseLibrary)
	END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.GET_CLAUSES    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE PROCEDURE GET_CLAUSES
(
	@clauseName nvarchar(255) = null,
	@clauseId nvarchar(255) = null,
	@clauseLibrary nvarchar(255) = null
)
AS
BEGIN
	SELECT DISTINCT clauseName, clauseId, clauseLibrary
	FROM  Clause
	WHERE clauseName like '%' + COALESCE(@clauseName, clauseName)  + '%'
	AND clauseId  like '%' + COALESCE(@clauseId, clauseId) + '%'  
	AND clauseLibrary like '%' +  COALESCE(@clauseLibrary, clauseLibrary) + '%'
	ORDER BY clauseName
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.GET_INSTANCES_BY_CLAUSE    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE PROCEDURE GET_INSTANCES_BY_CLAUSE /* and packageId */
(
	@packageID uniqueidentifier,
	@referenceName nvarchar(255) = null,
	@startDate datetime = null,
	@endDate dateTime = null,
	@clauseId nvarchar(255),
	@clauseLibrary nvarchar(255)
)
AS
	BEGIN		
		SET @endDate = DATEADD(day,1,@endDate)
		
		SELECT  PackageVersionNumber, PackageRevisionNumber, WhenCreated, Reference, CreatedByPersonId, IsComplete,
			 (SELECT count(*) FROM ClauseInstanceUsedTrack 
			 WHERE clauseId = @clauseId AND clauseLibrary = @clauseLibrary AND ClauseInstanceUsedTrack.PackageInstanceId = PackageInstance.PackageInstanceId)
		FROM  PackageInstance
		WHERE packageId = COALESCE(@packageId, packageId) 
		AND Reference like '%' + COALESCE(@referenceName, reference) + '%'  
		AND PackageInstance.whenCreated BETWEEN COALESCE(@startDate, whenCreated) AND COALESCE(@endDate, whenCreated)
		ORDER BY WhenCreated DESC
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.GET_INSTANCES_BY_PACKAGE    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE PROCEDURE GET_INSTANCES_BY_PACKAGE
(
	@packageID uniqueidentifier,
	@referenceName nvarchar(255) = null,
	@startDate datetime = null,
	@endDate dateTime = null
)
AS
	BEGIN		
		SET @endDate = DATEADD(day,1,@endDate)
		
		SELECT  PackageVersionNumber, PackageRevisionNumber, WhenCreated, Reference, CreatedByPersonId, IsComplete
		FROM  PackageInstance
		WHERE packageId = COALESCE(@packageId, packageId) 
		AND Reference like '%' + COALESCE(@referenceName, reference) + '%'  
		AND PackageInstance.whenCreated BETWEEN COALESCE(@startDate, whenCreated) AND COALESCE(@endDate, whenCreated)
		ORDER BY WhenCreated DESC
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.GET_PACKAGES_BY_CLAUSE    Script Date: 4/07/2006 3:52:22 p.m. ******/
CREATE PROCEDURE GET_PACKAGES_BY_CLAUSE
(
	@clauseID nvarchar(255) = null,
	@clauseLibrary nvarchar(255) = null,	
	@packageName nvarchar(255) = null,	
	@startDate datetime = null,
	@endDate dateTime = null
)
AS
	BEGIN
	
		SET @endDate = DATEADD(day,1,@endDate)
	
		/* We return a distinct record, because the report requires that only one result is displayed  if the package has multiple versions containing the clause. */
		SELECT DISTINCT Package.PackageId, Package.[Name], Package.WhenCreated, Package.PublishedBy, Package.ServerName 
		FROM Package
		INNER JOIN ClausePackageUsedTrack ON ClausePackageUsedTrack.packageId = Package.packageId  
			AND ClausePackageUsedTrack.packageVersion = Package.packageVersionNumber
			AND ClausePackageUsedTrack.packageRevision = Package.packageRevisionNumber
		INNER JOIN Clause ON Clause.clauseID = ClausePackageUsedTrack.ClauseId		
		WHERE Clause.clauseId = COALESCE(@clauseID, Clause.clauseId) 
		AND Clause.clauseLibrary = COALESCE(@clauseLibrary, clause.clauseLibrary)
		AND Package.[Name] like '%' + COALESCE(@packageName, [Name]) + '%'  
		AND Package.WhenCreated BETWEEN COALESCE(@startDate, whenCreated) AND COALESCE(@endDate, whenCreated)
		ORDER BY Package.WhenCreated DESC	
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.GET_PACKAGES_BY_LIBITEM    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE PROCEDURE GET_PACKAGES_BY_LIBITEM
(
	@itemID uniqueidentifier,
	@packageName nvarchar(255) = null,
	@startDate datetime = null,
	@endDate dateTime = null
)
AS

BEGIN
	SET @endDate = DATEADD(day,1,@endDate)

	SELECT DISTINCT package.PackageId, Package.[Name], Package.WhenCreated, Package.PublishedBy, Package.ServerName FROM Package
	INNER JOIN LibraryItem on package.PackageId = LibraryItem.packageId
	WHERE LibraryItem.itemId = COALESCE(@itemID, itemId) 
	AND Package.[Name] like '%' + COALESCE(@packageName, [Name]) + '%'  
	AND Package.WhenCreated BETWEEN COALESCE(@startDate, whenCreated) AND COALESCE(@endDate, whenCreated)
	ORDER BY package.WhenCreated DESC
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.INSTANCE_CREATE    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE  PROCEDURE INSTANCE_CREATE
(
	@packageId uniqueidentifier,
	@version int,
	@revision int,
	@whenMadeUtc datetime,
	@createdBy nvarchar(255),
	@Instanceid uniqueidentifier
)
AS
BEGIN
	INSERT INTO PackageInstance(PackageInstanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, WhenCreated, CreatedByPersonId, reference)
	SELECT @instanceId, PackageId, PackageVersionNumber, PackageRevisionNumber, @whenMadeUtc, @createdBy, [name]
	FROM Package WHERE PackageId = @packageId AND PackageVersionNumber = @version AND PackageRevisionNumber = @revision
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.LIBITEM_INSERT    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE Procedure LIBITEM_INSERT
(	
	@PackageId uniqueidentifier,
	@RevisionNumber int,
	@VersionNumber int,
	@itemId uniqueIdentifier,
	@itemName nvarchar(255),
	@itemType nvarchar(255)
)
AS
BEGIN 
SET NOCOUNT ON

	INSERT INTO LibraryItem(PackageId, packageVersion, packageRevision, itemId, itemName, itemType)
	VALUES
	(@PackageId, @VersionNumber, @RevisionNumber, @itemId, @itemName, @itemType)
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.PACKAGE_INSERT    Script Date: 4/07/2006 3:52:22 p.m. ******/

CREATE Procedure PACKAGE_INSERT
(	
	@PackageId uniqueidentifier,
	@Name nvarchar(50),
	@CreatedBy nvarchar(255),
	@ModifiedBy nvarchar(255),
	@PublishedBy nvarchar(255),
	@WhenCreated datetime,
	@WhenModified datetime,
	@WhenPublished datetime,
	@Description nvarchar(500),
	@RevisionNumber int,
	@VersionNumber int,
	@serverName nvarchar(500),
	@serverId uniqueidentifier,
	@tempOfflineId uniqueidentifier
)
AS
BEGIN 
	INSERT INTO Package(PackageId, PackageVersionNumber, PackageRevisionNumber, [Name], [Description], CreatedBy, ModifiedBy, PublishedBy, WhenCreated,
			WhenModified, WhenPublished, serverName, serverId, tempOfflineId)
	VALUES
		(@PackageId, @VersionNumber, @RevisionNumber, @Name, @Description, @CreatedBy, @ModifiedBy, @PublishedBy, @WhenCreated, @WhenModified, @WhenPublished, @serverName, @serverId, @tempOfflineId)

END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.PACKAGE_PEEK    Script Date: 4/07/2006 3:52:22 p.m. ******/

/* used by desktop edition only not the core product  */
CREATE Procedure PACKAGE_PEEK
(
	@packageId uniqueIdentifier,
	@isPreviouslyPublished int output,
	@nextVersionNumber int output,
	@previousDateModified datetime output
)
AS
BEGIN 

	DECLARE @packageRowCount  int
	DECLARE @versionNumber int
	DECLARE @previouslyPublished  int
	DECLARE @previousDate  datetime
	
	SELECT @packageRowCount = Count(*) FROM Package WHERE packageId = @packageId
/*
Work out the new version number
*/
	
	IF @packageRowCount = 0
	BEGIN
		SET @previouslyPublished = 0
	END
	ELSE
	BEGIN
		SET @previouslyPublished = 1
	END

	IF @PreviouslyPublished = 1
	BEGIN
		SELECT @VersionNumber = 1 + COALESCE(MAX(PackageVersionNumber), 0) FROM Package WHERE PackageId = @packageId	
		SELECT @previousDate = WhenModified FROM Package WHERE PackageVersionNumber = @VersionNumber - 1 AND PackageID = @packageId
	END
	ELSE
	BEGIN
		SET @versionNumber = 1	
	END
/*
Return things
*/
	BEGIN
	
		SELECT @nextVersionNumber = @versionNumber
		SELECT @isPreviouslyPublished = @previouslyPublished
		SELECT @previousDateModified = @previousDate
		RETURN 0 -- 0 = no errors
	END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.STATUS_SETCOMPLETE    Script Date: 4/07/2006 3:52:22 p.m. ******/
CREATE PROC STATUS_SETCOMPLETE
(
@instanceId uniqueidentifier
)
AS
BEGIN
	UPDATE PackageInstance SET IsComplete = 1 WHERE PackageInstanceId = @instanceId
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


/****** Object:  Stored Procedure dbo.VERSION_GET_BY_MODID    Script Date: 4/07/2006 3:52:22 p.m. ******/

/* used by desktop edition only not the core product  */

CREATE Procedure VERSION_GET_BY_MODID
(	
	@tempOfflineId uniqueIdentifier,
	@versionNumber int output
)
AS
BEGIN 
	DECLARE @version int
	SELECT @version = packageVersionNumber FROM Package WHERE tempOfflineId = @tempOfflineId	
	SELECT @versionNumber = @version
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

