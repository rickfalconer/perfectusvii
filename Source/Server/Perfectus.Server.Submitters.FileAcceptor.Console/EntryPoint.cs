using System;
using System.Diagnostics;

namespace Perfectus.Server.Submitters.FileAcceptor.Console
{
	/// <summary>
	/// File Acceptor test console entry point class.
	/// </summary>
	class EntryPoint
	{
        private static Perfectus.Server.Submitters.FileAcceptor.Engine.FileAcceptor _FileAcceptor;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			Trace.Listeners.Add(new System.Diagnostics.TextWriterTraceListener(System.Console.Out));

			System.Console.WriteLine("FileAcceptor.Console starting...");
			if (args != null && args.Length > 0 )
				System.Console.WriteLine(string.Format("Command line arguments: {0}", args));
			
			_FileAcceptor = FileAcceptor.Engine.FileAcceptor.GetInstance();
			_FileAcceptor.Start();
			System.Console.WriteLine("Hit enter to exit...");
			System.Console.ReadLine();

			TidyUp();
		}

		private static void TidyUp()
		{
			System.Console.WriteLine("FileAcceptor.Console stopping...");
			if (_FileAcceptor != null)
			{
				_FileAcceptor.Stop();
			}

			foreach(TraceListener tl in Trace.Listeners)
			{
				try
				{
					System.Console.WriteLine(string.Format("Closing trace listener: {0}", tl.Name));
					tl.Flush();
					tl.Close();
				}
				catch
				{
					Trace.WriteLine("Couldn't close listener.");
				}
			}
		}
	
	}
}
