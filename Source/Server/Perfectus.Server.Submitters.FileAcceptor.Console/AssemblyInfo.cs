using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus File Acceptor Console")]

// Assembly Copyright
[assembly: AssemblyCompany("Perfectus Technology")]
[assembly: AssemblyProduct("Perfectus")]
[assembly: AssemblyCopyright("Copyright � 1999-2010 Perfectus Technology Limited.  All rights reserved.")]
[assembly: AssemblyTrademark("USA Patent Pending No. 76/083067.\nAustralian Patent No. 82716/01.\nNew Zealand Patent No. 506004.")]

// Assembly Versioning
[assembly: AssemblyVersion("7.0.0.0")]                  // Binary Compatibility Version
[assembly: AssemblyFileVersion("7.0.0.0")]              // Marketing Version
[assembly: AssemblyInformationalVersion("7.0.0.0")]     // Marketing Version

// Assembly Misc settings
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: AssemblyCulture("")]
[assembly: Guid("3d5900ae-111a-45be-96b3-d9e4606ca793")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif