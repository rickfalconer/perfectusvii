using System;

namespace Perfectus.Server.WebUI.Library.Interview
{
	/// <summary>
	/// Summary description for InterviewFinishedEventArgs.
	/// </summary>
	public class InterviewFinishedEventArgs : EventArgs
	{
		private string destinationUrl;

		public string DestinationUrl
		{
			get { return destinationUrl; }
		}

		public InterviewFinishedEventArgs(string destinationUrl)
		{
			this.destinationUrl = destinationUrl;
		}
	}
}
