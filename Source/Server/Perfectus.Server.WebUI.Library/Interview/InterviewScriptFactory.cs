using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Globalization;
using System.Threading;

namespace Perfectus.Server.WebUI.Library.Interview
{
	/// <summary>
	/// Summary description for InterviewScriptFactory.
	/// </summary>
	internal class InterviewScriptFactory
	{
        private enum BindingTarget { DefaultAnswer, Prompt, Hint, Example }

		private InterviewScriptFactory()
		{
		}

        public static string GetVars( Package ins, InterviewPage p, string interviewClientID, 
                string validationDHTMLStateId, StringBindingMetaData metaData ) 
		{
			if (p == null || p is InternalPage == false)
				return string.Empty;
	
			Debug.Assert(ins != null);
			Debug.Assert(p != null);
			StringBuilder sb = new StringBuilder();

			sb.Append("<script language='javascript' type='text/javascript'>");
			
            // Add script to create a reference to the hidden control that keeps a list of disabled validation controls....
            sb.Append( GetHiddenControlID( interviewClientID, validationDHTMLStateId ) );
            
			QuestionCollection qsOnPage = ((InternalPage)p).GetQuestionsOnPage();

			foreach (Question q in ins.Questions)
			{
				if ((!qsOnPage.Contains(q)) || q.Visible.IsDefinitelyNo())
				{
                    // FB1163 - {PackageName} needs stringbinding meta data, but was not set corrcetly
                    if (q.StringBindingMetaData.PackageInstanceId == Guid.Empty)
                        q.StringBindingMetaData =metaData;
                    // <- FB1163
					sb.Append(GetVarLine(q));
				}
			}
			foreach(PFunction pf in ins.Functions)
			{
				sb.Append(GetVarLine(pf));
			}

			sb.Append(GenerateDhtmlFunctions(p));

			sb.Append("</script>");
			return sb.ToString();
		}

		private static string GetVarLine(AnswerProvider ap)
		{
			StringBuilder sb = new StringBuilder();
			string varName = string.Format("answerTo{0:N}", ap.UniqueIdentifier);
			sb.Append("var ");
			sb.Append(varName);
			sb.Append(" = ");
			sb.Append(GetJavaScriptAnswers(ap, varName));
			sb.Append("\n");
			return sb.ToString();
		}

        private static string GetHiddenControlID(string interviewClientID, string hiddenControlID )
        {
            StringBuilder sb = new StringBuilder();
            sb.Append( "var validationCtrl = document.getElementById( '" );
            sb.Append( interviewClientID );
            sb.Append( "_" );
            sb.Append( hiddenControlID );
            sb.Append( "' ); \n" );
            return sb.ToString();
        }

/*		private static bool IsQuestionReferencedOnPage(Question q, PageItemCollection coll)
		{
			foreach (IPageItem ipi in coll)
			{
				if (ipi.Visible.ContainsReferenceTo(q))
				{
					return true;
				}
				if (ipi is Question)
				{
					Question qi = (Question) ipi;
					if (qi.Enabled.ContainsReferenceTo(q))
					{
						return true;
					}
					if (qi.Mandatory != null && qi.Mandatory.ContainsReferenceTo(q))
					{
						return true;
					}
				}
				if (ipi is HorizontalLayoutZone)
				{
					bool isInHz = (IsQuestionReferencedOnPage(q, ((HorizontalLayoutZone) ipi).Items));
					if (isInHz)
					{
						return true;
					}
				}
				if (ipi is RepeaterZone)
				{
					bool isInRz = (IsQuestionReferencedOnPage(q, ((RepeaterZone) ipi).Items));
					if (isInRz)
					{
						return true;
					}
				}
			}
			return false;
		}*/

		private static string GenerateDhtmlFunctions(InterviewPage page)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("\tfunction UpdateDhtml()\n");
			sb.Append("\t{\n");
            // This creates the JS to dynamically change the visibility and enable/disable validation. As part of FB355 it is 
            // important that the mandatory checks are performed BEFORE the visibility as the JS is written based on this fact.
            // #1 : Mandatory checks (must be before visibility)
			GenerateMandatoryQueryForItems(page.Items, sb);
            // #2 : Visibility checks.
			GenerateVisibleQueryForItems(page.Items, sb);
            sb.Append("\t}\n\n");


            sb.Append("\tfunction UpdateQueryDhtml(src, qf)\n");
            sb.Append("\t{\n");
            GenerateQueryTextAnswer(page.Items, sb);
			sb.Append("\t}\n");

			return sb.ToString();
		}

        private static string EncodeXml(string xml)
        {
            xml = xml.Replace("\"", "\\\"");
            xml = xml.Replace(System.Environment.NewLine, " " /*"<br />"*/);
            return xml;
        }


        private static String BindingTargetToJavaFunctionName(BindingTarget bindingTarget)
        {
            switch (bindingTarget)
            {
                case BindingTarget.DefaultAnswer:
                    return "ChangeDefaultAnswerRef";
                case BindingTarget.Prompt:
                    return "ChangePromptRef";
                default:
                    throw new Exception("Not implemented");
            }
        }

        private static String BuildQuery(StringBuilder sb, Query query, String id, BindingTarget bindingTarget)
        {
            if (query.QueryExpression != null)
                sb.Append(String.Format("\tif({0})", query.QueryExpression.ToJavaScriptForBrowserString()));

            sb.Append("\n\t{");

            if (query.ActionIfTrue != null && query.ActionIfTrue.SubQuery != null)
            {
                sb.Append(BuildQuery(new StringBuilder(), query.ActionIfTrue.SubQuery, id, bindingTarget));
            }
            else
            {
                string defaultAnswerXml = String.Empty;
                if (query.ActionIfTrue != null)
                {
                    defaultAnswerXml = ((HtmlStringBindingAction)query.ActionIfTrue).Payload;
                    if (defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
                        defaultAnswerXml = EncodeXml(defaultAnswerXml.Replace("-", ""));
                }
                sb.Append(String.Format("{0}( src, qf, \"{1}\", \"{2}\" );\n",
                    BindingTargetToJavaFunctionName(bindingTarget), id, defaultAnswerXml));
            }
            
            sb.Append("\n\t}");
            sb.Append("\n\telse");
            sb.Append("\n\t{\n\t");


            // We have to deal with two specific condition that could blow up 
            // 1. There is no ELSE
            // 2. There is an ELSE, but the referred text is empty (= NULL)
            // In both cases we have to detect and use an empty string instead.
            if (query.ActionIfFalse != null && query.ActionIfFalse.SubQuery != null)
            {
                sb.Append(BuildQuery(new StringBuilder(), query.ActionIfFalse.SubQuery, id, bindingTarget));
            }
            else
            {
                string defaultAnswerXml= String.Empty;
                if (query.ActionIfFalse != null)
                {
                    defaultAnswerXml = ((HtmlStringBindingAction)query.ActionIfFalse).Payload;
                    if (defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
                        defaultAnswerXml = EncodeXml(defaultAnswerXml.Replace("-", ""));
                }
                sb.Append(String.Format("{0}( src, qf, \"{1}\", \"{2}\" );\n",
                    BindingTargetToJavaFunctionName(bindingTarget), id, defaultAnswerXml));
            }
            sb.Append("\n\t}\n");

			return sb.ToString();
		}

        private static void GenerateQueryTextAnswer(PageItemCollection picoll, StringBuilder sb)
        {
            foreach (IPageItem ipi in picoll)
            {
                if (ipi is RepeaterZone)
                    GenerateQueryTextAnswer(((RepeaterZone)ipi).Items, sb);
                else if (ipi is HorizontalLayoutZone)
                    GenerateQueryTextAnswer(((HorizontalLayoutZone)ipi).Items, sb);
                else
                    if (ipi is Question)
                    {
                        Question q = ipi as Question;
                        String id = q.UniqueIdentifier.ToString();
                        id = id.Replace("-", "");

                        if (q.DefaultAnswer != null && q.DefaultAnswer.QuestionValue == null)
                        {
                            Query query = q.DefaultAnswer.QueryValue;
                            if (query != null)
                                sb.Append(BuildQuery(new StringBuilder(), query, id, BindingTarget.DefaultAnswer));
                        }

                        if (q.Prompt != null && q.Prompt.QuestionValue == null)
                        {
                            Query query = q.Prompt.QueryValue;
                            if (query != null)
                                sb.Append(BuildQuery(new StringBuilder(), query, id, BindingTarget.Prompt));
                        }
                    }
            }
        }

		private static void GenerateVisibleQueryForItems(PageItemCollection picoll, StringBuilder sb)
		{
			foreach (IPageItem ipi in picoll)
			{
				if( ipi.Visible != null && ipi.Visible.ValueType == YesNoQueryValue.YesNoQueryValueType.Query )
				{
					Query query = ipi.Visible.QueryValue;
					if (query != null)
					{
						sb.Append(string.Format("\t\tif({0})\n\t\t\t{{Show('{1}');}}else{{Hide('{1}');}}\n", 
                            query.QueryExpression.ToJavaScriptForBrowserString(), ipi.UniqueIdentifier.ToString("N")));
					}
				}

				if (ipi is RepeaterZone)
				{
					GenerateVisibleQueryForItems(((RepeaterZone) ipi).Items, sb);
				}
				else if (ipi is HorizontalLayoutZone)
				{
					GenerateVisibleQueryForItems(((HorizontalLayoutZone) ipi).Items, sb);
				}
			}
		}

		private static void GenerateMandatoryQueryForItems(PageItemCollection picoll, StringBuilder sb)
		{
			foreach (IPageItem ipi in picoll)
			{
				if (ipi is Question)
				{
                    Question q = (Question)ipi;

                    if( q.Mandatory != null && q.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query )
					{
                        Query query = q.Mandatory.QueryValue;
						if (query != null)
						{
							sb.Append(string.Format("\t\tif({0})\n\t\t\t{{Required('{1}');}}else{{NotRequired('{1}');}}\n", 
                                query.QueryExpression.ToJavaScriptForBrowserString(), q.UniqueIdentifier.ToString("N")));
						}
					}
				}

				if (ipi is RepeaterZone)
				{
					GenerateMandatoryQueryForItems(((RepeaterZone) ipi).Items, sb);
				}
				else if (ipi is HorizontalLayoutZone)
				{
					GenerateMandatoryQueryForItems(((HorizontalLayoutZone) ipi).Items, sb);
				}
			}
		}

		private static string GetJavaScriptAnswers(AnswerProvider ap, string scriptVariableName)
		{
		    if (ap is Question)
			{
			    AnswerCollectionCollection collColl = ((Question)ap).GetAnswerCollectionCollection(AnswerProvider.Caller.Interview);
				if (collColl != null)
				{
					StringBuilder sb = new StringBuilder();
					sb.Append(string.Format("new Array({0});", collColl.Count));
					for (int i = 0; i < collColl.Count; i++)
					{
						string comma = "";
						sb.Append(string.Format("\n\t{0}[{1}] = new Array(", scriptVariableName, i));
						for (int j = 0; j < collColl[i].Count; j++)
						{
							sb.Append(comma);
							sb.Append(SingleAnswerToJavaScriptVar(ap, collColl[i][j], AnswerProvider.Caller.Interview));
							comma = ",";
						}
						sb.Append(");");
					}

					return sb.ToString();
				}
				else
				{
					return "null;";
				}
			}
            else if (ap is ArithmeticPFunction)
			{
                object roundedFuncValue = ((ArithmeticPFunction)ap).GetRoundedValueForQuery(AnswerProvider.Caller.Interview);
                return string.Format("new Array(new Array({0}));", SingleAnswerToJavaScriptVar(ap, roundedFuncValue, AnswerProvider.Caller.Interview));
			}
            else
			{
                return string.Format("new Array(new Array({0}));", SingleAnswerToJavaScriptVar(ap, ap.GetSingleAnswer(AnswerProvider.Caller.Interview), AnswerProvider.Caller.Interview));
			}
		}

		private static string SingleAnswerToJavaScriptVar(AnswerProvider ap, object o, AnswerProvider.Caller caller)
		{
		    bool success;
			o = AnswerProvider.ConvertToBestType(o, ap.DataType, out success, caller, ap.StringBindingMetaData);
			
			if (! success ||  (o == null))
			{
				return "";
			}
			else
			{
				switch (ap.DataType)
				{
					case PerfectusDataType.Date:
						return string.Format("[new Date('{0} 00:00:00').getTime()]", ((DateTime)o).ToShortDateString());
					case PerfectusDataType.YesNo:
						return (bool) o ? "true" : "false";
					case PerfectusDataType.Number:

                        //we need to change the current culture to US english to ensure that the JavaScript friendly variables are written out
                        CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;//store our current culture for later
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");//set US
                        string numberValue = string.Format("[{0}]", o);
                        Thread.CurrentThread.CurrentCulture = originalCulture;//set original culure back

						return numberValue;
                    default:
                    case PerfectusDataType.Text:
                        {
                            // Escape all characters that we cannot have in the script.
                            return string.Format( "'{0}'", EscapeJavascript( o.ToString( ) ) ); 
                        }
				}
			}
		}

        public static string GenerateCulturesNumberValidScript()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script language='javascript' type='text/javascript'>");
            sb.Append(@"function getEvalFriendlyFloat(value)
                        {
                            value = value.replace(/[" + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator + @"]/g,'');
                            value = value.replace(/[" + Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator + @"]/g,'.');
                            return value.replace(/[^0-9.-]/g, '');
                        }");
            sb.Append("</script>");
            return sb.ToString();
        }

        /// <summary>
        /// Used to escape certain characters out of the answer text so that the text can be savely used within javascript 
        /// renderred to the browser. At the moment the predefined entities in XML are also escaped, although this might 
        /// not be 100% necessary as these characters are valid within the javascript. Unsure exactly where and how this 
        /// script is used and if pasted into attributes within html elements (as was the case before see FB386), the characters
        /// would need to be escaped.
        /// </summary>
        /// <param name="script">answer text</param>
        /// <returns>formatted text.</returns>
        public static string EscapeJavascript( string script )
        {
            return System.Web.HttpUtility.HtmlEncode( script )
                            .Replace( "\r", "\\r" )
                            .Replace( "\n", "\\n" )
                            .Replace( @"'", @"\'" );
        }
    }
}
