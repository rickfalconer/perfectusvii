using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;

namespace Perfectus.Server.WebUI.Library.Interview
{
	/// <summary>
	/// Summary description for ProgressBar.
	/// </summary>
	internal class ProgressBar : WebControl
	{
		private double percent = -1;

		public double Percent
		{
			get { return percent; }
			set { percent = value; }
		}

		protected override void CreateChildControls()
		{
			CreateBar();
		}

		private void CreateBar()
		{
			if (percent >= 0)
			{
				Table table = new Table();
				TableRow tr = new TableRow();
				TableCell tc = new TableCell();
				TableCell tc2 = new TableCell();
				table.CellSpacing = 0;

				tc.CssClass = "progressBg";
				tc2.CssClass = "progressText";
				tc.Controls.Add(new LiteralControl(string.Format("<div style=\"width:{0}%\" class=\"progressFg\">&nbsp;</div>", Math.Round(percent))));
				table.Rows.Add(tr);
				tr.Cells.Add(tc);
				tr.Cells.Add(tc2);
				Controls.Add(table);

				string progressInfoFormat = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("ProgressInfo");
				string progressInfo = string.Format(progressInfoFormat, percent/100);
				tc2.Controls.Add(new LiteralControl(string.Format("<span class=\"progressInfo\">{0}</span>", progressInfo)));

			}

		}

		public override void DataBind()
		{
			ClearChildViewState();
			CreateBar();
			ChildControlsCreated = true;
		}
	}
}
