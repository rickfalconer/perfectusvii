using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;
using System.Text.RegularExpressions;
using System.Xml;

namespace Perfectus.Server.WebUI.Library.Interview
{
    public abstract class InterviewStringBinding
    {
        /// <summary>
        /// Takes a string that may contain xml with string binding info, and convert it to html with string binding info
        /// </summary>
        /// <param name="xmlBody">The string to perform string binding operations on</param>
        ///  <param name="package">Package to get answers from</param>
        /// <returns>The modified string</returns>
        public static string GetClientHtml(string xmlBody, Package package, StringBindingMetaData stringBindingMetaData, int repeatNumber, bool inRepeater)
        {
            if (xmlBody == null || (xmlBody.IndexOf("<text>") == -1 && xmlBody.IndexOf("{") == -1))
            {
                return xmlBody;
            }

            AnswerProvider.Caller caller = AnswerProvider.Caller.Interview;
            Regex itemReg = new Regex("<item.*</item>");
            Match itemMatch = itemReg.Match(xmlBody, 0, xmlBody.IndexOf("</item>") + 8); // we only want to get one at a time.
            StringBindingItemDictionary itemDict = package.StringBindingItemByGuid;

            while (itemMatch.Success)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(itemMatch.ToString());

                string strUId = doc.SelectSingleNode("/item/@uId").Value;
                Guid uId = new Guid(strUId);
                string fieldId = null;
                XmlNode fieldNode = doc.SelectSingleNode("/item/@fieldId");
                if (fieldNode != null)
                {
                    fieldId = fieldNode.InnerText;
                }
                string strDisplay = doc.SelectSingleNode("/item").InnerXml;

                AnswerProvider item = (AnswerProvider)itemDict[uId];

                object answer = null;
                if (item != null)
                {
                    answer = item.GetSingleAnswer(caller);
                }
                string strAnswer = null;

                if (answer != null)
                {
                    strAnswer = item.GetSingleAnswer(caller).ToString();

                    if (item is PFunction)
                    {
                        PFunction pf = (PFunction)item;
                        strAnswer = InterviewSystem.Format.FormatForDisplay(strAnswer, pf.InterviewFormatString, pf.InterviewFormatStringParams, pf.DataType, package);
                    }
                    else if (item is Question)
                    {
                        Question q = (Question)item;
                        strAnswer = InterviewSystem.Format.FormatForDisplay(strAnswer, q.InterviewFormatString, q.InterviewFormatStringParams, q.DataType, package);
                    }
                }

                if (strAnswer == null || strAnswer.Length == 0)
                {
                    strAnswer = strDisplay;
                }
                else
                {
                    if (fieldId != null && answer.ToString().Trim().StartsWith("<multiValue>"))
                    {
                        XmlDocument multiDoc = new XmlDocument();
                        try
                        {
                            multiDoc.LoadXml(strAnswer);
                            XmlNode valueNode = multiDoc.SelectSingleNode(string.Format("/multiValue/field[@id='{0}']", fieldId));
                            if (valueNode != null)
                            {
                                strAnswer = valueNode.InnerXml;
                            }
                        }
                        catch
                        {
                            // Do nothing, leave answer alone and bind normally ignoring the field stuff.
                        }
                    }

                }

                strUId = strUId.Replace("-", "");
                string inRepeaterAttr = inRepeater ? "inRepeater='true'" : string.Empty;
                string repeatNumAttr = inRepeater ? string.Format("repeat='{0}'", repeatNumber) : string.Empty;
                string strHtml = String.Format("<span dAnswer=\"{0}\" uId=\"uId{1}\" {3} {4}>{2}</span>", strDisplay, strUId, strAnswer, inRepeaterAttr, repeatNumAttr);

                // start ie space issue fix. if the charcter imdediately following the span is a " ", then replace it with a "&nbsp;" or else if the character before and after span is " " we only get one space in some versions of Internet Explorer.
                int poToReplace = itemMatch.Index + itemMatch.Length;
                if (xmlBody.IndexOf(" ", poToReplace) == poToReplace)
                {
                    xmlBody = xmlBody.Remove(poToReplace, 1);
                    xmlBody = xmlBody.Insert(poToReplace, "&nbsp;");
                }
                // end ie space issue fix.

                xmlBody = xmlBody.Remove(itemMatch.Index, itemMatch.Length);
                xmlBody = xmlBody.Insert(itemMatch.Index, strHtml);

                itemMatch = itemReg.Match(xmlBody, 0, xmlBody.IndexOf("</item>") + 8);
            }

            StringBinding.ReplaceMetadataSections(ref xmlBody, ref stringBindingMetaData);

            xmlBody = xmlBody.Replace("<text>", "");
            xmlBody = xmlBody.Replace("</text>", "");

            return xmlBody;
        }
    }
}
