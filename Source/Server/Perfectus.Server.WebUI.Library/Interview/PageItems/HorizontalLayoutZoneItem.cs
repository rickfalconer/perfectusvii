using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.WebUI.Library.Interview.PageItems.Fields;
using System.Xml;
using System.IO;
using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
    internal class HorizontalLayoutZoneItem : PageItemBase, INamingContainer
    {
        private PageItemCollection items;
        private Table table;
        private string scriptPath;
        private QuestionFieldCollection questionFieldControls;
        private EventHandler callButtonClickHandler;
        private EventHandler loadAnswerSetHandler;
        private Guid instanceId;
        private string imagesPath;
        private DisplayTypeExtensionInfoDictionary extensionMappings;
        private QuestionField qfToFocus = null;
        private Package parentPackage = null;
        // FB158: We want this flag to indicate that any single component on the row is visible. 
        private bool repeaterVisible = false;
        private bool isPagePreview;
        private bool createButton = false;
        private int maxRepeats = 1; // This is forced to 1, as it is no longer collected in IPManager (if it ever was)
        private int minRepeats = 1;
        private int numRepeats = 1;
        private string[] itemTypes = null;
        private Question[] questionStubs = new Question[0];
        private int[] rowIds = null;
        private string[] itemIds = null;
        private bool hasBound = false;
        private Guid pageId;

        // responsive
        private bool responsive = false;

        private HorizontalLayoutZoneItem()
        {
        }

        public HorizontalLayoutZoneItem(PageItemCollection items, string scriptPath, string imagesPath,
                                        QuestionFieldCollection questionFieldControls,
                                        EventHandler callButtonEventHandler, Guid instanceId,
                                        DisplayTypeExtensionInfoDictionary extensionMappings,
                                        EventHandler loadAnswerSetHandler, Package package, int maxRepeats,
                                        bool isPreview, Guid pageId, bool responsive)
        {
            this.items = items;
            this.scriptPath = scriptPath;
            this.questionFieldControls = questionFieldControls;
            callButtonClickHandler = callButtonEventHandler;
            this.loadAnswerSetHandler = loadAnswerSetHandler;
            this.instanceId = instanceId;
            this.imagesPath = imagesPath;
            this.extensionMappings = extensionMappings;
            parentPackage = package;
            this.maxRepeats = maxRepeats;
            isPagePreview = isPreview;
            this.pageId = pageId;
            this.responsive = responsive;
        }

        protected override void CreateChildControls()
        {
            CreateTable();
        }

        private void CreateTable()
        {
            if (!hasBound)
            {
                DataBind();
            }

            Controls.Clear();

            if (rowIds == null || rowIds.Length == 0)
            {
                rowIds = new int[numRepeats];
                for (int i = 0; i < numRepeats; i++)
                {
                    rowIds[i] = i;
                }
            }

            //Debug.Assert(rowIds.Length == numRepeats - 1);
            table = new Table();

            // responsive prevent white gaps around rows
            if (responsive) { table.Style.Add("border-spacing", "inherit"); }

            Controls.Add(table);
            TableRow promptRow = new TableRow();
            TableRow hintRow = new TableRow();
            TableRow[] fieldRows = new TableRow[numRepeats];
            // responsive - row header to help identify repeater qs for same row
            TableRow headerRow = new TableRow();
            string lastFieldRowID = string.Empty;
            string rowBackgroundcolour = "";

            int packageVer = 0;
            int packageRev = 0;

            try
            {
                InstanceManagement.GetVersion(instanceId, out packageVer, out packageRev);
            }
            catch (InvalidCastException) // GetVersion fails in page preview mode
            {
            }

            string metaDataReference = string.Empty;
            if (packageVer > 0)
            {
                metaDataReference = InstanceManagement.GetInstanceReference(instanceId);
                parentPackage = InstanceManagement.GetInstance(instanceId);
            }
            StringBindingMetaData stringBindingMetaData = new StringBindingMetaData();

            if (parentPackage != null)
            {
                stringBindingMetaData =
                    new StringBindingMetaData(parentPackage.Name, metaDataReference, instanceId, packageVer, packageRev);
            }

            if (responsive && maxRepeats > 1)
                rowBackgroundcolour = "aliceBlue";
            else
                rowBackgroundcolour = "aliceBlue";


            // responsive - add a header row if repeating 
            if (responsive && maxRepeats > 1)
            {
                headerRow.Style.Add("background-color", rowBackgroundcolour);
                //headerRow.Style.Add("border-color", rowBackgroundcolour);
                table.Rows.Add(headerRow);
                TableCell headerCell = new TableCell();
                HtmlBlock b = new HtmlBlock();
                b.Html = string.Format("Row {0}", 1);
                HtmlBlockItem hbi = new HtmlBlockItem(b, parentPackage, stringBindingMetaData, 1, (maxRepeats > 1));
                //TODO new style
                hbi.CssClass = "pageLabel";
                headerCell.Controls.Add(hbi);
                headerRow.Cells.Add(headerCell);
            }

            // add styles for first prompt and hint
            if (responsive)
            {
                promptRow.Style.Add("background-color", rowBackgroundcolour);
                hintRow.Style.Add("background-color", rowBackgroundcolour);
            }

            table.Rows.Add(promptRow);
            table.Rows.Add(hintRow);

            hintRow.Visible = false;
            // Make the prompt, eg, hint rows/cells
            for (int i = 0; i < itemTypes.Length; i++)
            {
                // responsive - add new rows for each col
                if (responsive && i > 0)
                {
                    promptRow = new TableRow();
                    table.Rows.Add(promptRow);
                    hintRow = new TableRow();
                    table.Rows.Add(hintRow);
                    //fieldRows = new TableRow[numRepeats];

                    // add style for these
                    if (responsive)
                    {
                        promptRow.Style.Add("background-color", rowBackgroundcolour);
                        hintRow.Style.Add("background-color", rowBackgroundcolour);
                    }
                }

                PopulatePromptHint(ref promptRow, ref hintRow, ref fieldRows, i);

                // responsive - add the first field row now, make it id unique with row and col
                if (responsive)
                {
                    int irep = 0;
                    TableRow fieldRow = new TableRow();
                    fieldRow.VerticalAlign = VerticalAlign.Top;
                    fieldRows[irep] = fieldRow;
                    fieldRow.ID = string.Format("r{0}{1}", rowIds[irep], i);
                    lastFieldRowID = fieldRow.ID;
                    // note last param - identifies this field as the one to populate, instead of all the field in the 'row'
                    PopulateRow(fieldRow, itemTypes, itemIds, irep, false, stringBindingMetaData, i);
                    fieldRow.Style.Add("background-color", rowBackgroundcolour);
                    //fieldRow.Style.Add("border-color", rowBackgroundcolour);
                    table.Rows.Add(fieldRow);
                }
            }

            // not responsive? - populate the fields and reps here
            if (!responsive)
            {
                for (int i = 0; i < numRepeats; i++)
                {
                    TableRow fieldRow = new TableRow();
                    fieldRow.VerticalAlign = VerticalAlign.Top;
                    fieldRows[i] = fieldRow;
                    fieldRow.ID = string.Format("r{0}", rowIds[i]);
                    lastFieldRowID = fieldRow.ID;
                    PopulateRow(fieldRow, itemTypes, itemIds, i, false, stringBindingMetaData);
                    //fieldRow.Style.Add("background-color", rowBackgroundcolour);
                    table.Rows.Add(fieldRow);
                }
            }

            // responsive with more than 1 rep? add a row for each field for 2nd rep +
            if (responsive && maxRepeats > 1)
            {
                // firstly add a remove button for the current row
                CreateRemoveButtton(rowBackgroundcolour, lastFieldRowID, 0);

                // now process the 2nd + rows
                for (int irep = 1; irep < numRepeats; irep++)
                {
                    // Add a row header and stlye to identify the row
                    headerRow = new TableRow();
                    if (rowBackgroundcolour == "aliceBlue")
                        rowBackgroundcolour = "lightGoldenrodYellow";
                    else
                        rowBackgroundcolour = "aliceBlue";

                    headerRow.Style.Add("background-color", rowBackgroundcolour);
                    //headerRow.Style.Add("border-color", rowBackgroundcolour);
                    table.Rows.Add(headerRow);
                    TableCell headerCell = new TableCell();
                    HtmlBlock b = new HtmlBlock();
                    b.Html = string.Format("Row {0}", irep + 1);
                    HtmlBlockItem hbi = new HtmlBlockItem(b, parentPackage, stringBindingMetaData, irep, (maxRepeats > 1));
                    //TODO new style
                    hbi.CssClass = "pageLabel";
                    headerCell.Controls.Add(hbi);
                    headerRow.Cells.Add(headerCell);

                    // Make the prompt, eg, hint rows/cells for each rep
                    for (int i = 0; i < itemTypes.Length; i++)
                    {
                        // add new rows for each col, not the hint - its already been seen on the first rep
                        promptRow = new TableRow();
                        promptRow.Style.Add("background-color", rowBackgroundcolour);
                        table.Rows.Add(promptRow);
                        hintRow = new TableRow();
                        hintRow.Style.Add("background-color", rowBackgroundcolour);

                        PopulatePromptHint(ref promptRow, ref hintRow, ref fieldRows, i);

                        //populate the field right after each prompt/hint
                        TableRow fieldRow = new TableRow();
                        fieldRow.VerticalAlign = VerticalAlign.Top;
                        fieldRows[irep] = fieldRow;
                        fieldRow.ID = string.Format("r{0}{1}", rowIds[irep], i);
                        lastFieldRowID = fieldRow.ID;
                        // note last param - identifies this field as the one to populate, instead of all the field in the 'row'
                        PopulateRow(fieldRow, itemTypes, itemIds, irep, false, stringBindingMetaData, i);
                        fieldRow.Style.Add("background-color", rowBackgroundcolour);
                        table.Rows.Add(fieldRow);

                        // new row for Remove button, only after last field
                        if (i == itemTypes.Length - 1)
                        {
                            CreateRemoveButtton(rowBackgroundcolour, lastFieldRowID, irep);
                        }

                        // new row the for add button
                    }
                }
            }

            Controls.Add(table);

            if (numRepeats < maxRepeats)
            {
                createButton = true;
                createAddButton(itemTypes.Length);
                createButton = false;
            }

            ChildControlsCreated = true;
        }

        private void CreateRemoveButtton(string rowBackgroundcolour, string lastFieldRowID, int irep)
        {
            TableCell buttonCell = new TableCell();
            Button removeButton = new Button();
            removeButton.CausesValidation = false;
            removeButton.Text =
                ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString(
                    "RepeaterRemoveButtonText");
            buttonCell.Controls.Add(removeButton);
            removeButton.Enabled = (numRepeats > minRepeats) && !isPagePreview;
            // ID is from last row, ie fieldRow, but without the cell part
            removeButton.ID = string.Format("{0}rmv", rowIds[irep]);
            removeButton.CommandArgument = lastFieldRowID;
            removeButton.Command += new CommandEventHandler(removeButton_Command);

            buttonCell.CssClass = "repeaterRemove";
            TableRow buttonRow = new TableRow();
            buttonRow.Cells.Add(buttonCell);
            buttonRow.Style.Add("background-color", rowBackgroundcolour);
            //buttonRow.Style.Add("border-color", rowBackgroundcolour);
            table.Rows.Add(buttonRow);
        }

        private void PopulatePromptHint(ref TableRow promptRow, ref TableRow hintRow, ref TableRow[] fieldRows, int i)
        {
            TableCell promptCell = new TableCell();
            promptCell.Attributes.Add("item", itemIds[i]);
            promptCell.CssClass = "promptCellHorizontal";
            //promptCell.ColumnSpan = 2;
            promptRow.Cells.Add(promptCell);
            TableCell hintCell = new TableCell();
            hintCell.Attributes.Add("item", itemIds[i]);
            hintCell.CssClass = "hintCellHorizontal";
            //hintCell.ColumnSpan = 2;
            hintRow.Cells.Add(hintCell);
            switch (itemTypes[i])
            {
                case "Q":
                    Question q = null;
                    if (items != null)
                    {
                        q = (Question)items[i];

                        if (q.Visible.IsDefinitelyNo() && q.ParentPackage != null)
                        {
                            promptCell.Visible = false;
                            hintCell.Visible = false;
                        }
                        else
                        {
                            //  FB158: We have a visible cell, hence set the appropriate flag so that the Add button will be
                            // drawn. This fixes the case. This does not imply the row will still have content as the visibility 
                            // of some cells might depend on queries which are evaluated browser side. In my opinion this 
                            // flag is unnessary, and all we need is to always draw the Add button and complement that with
                            // some javascript which dynamically sets the visibilty of the button based on the fact that all 
                            // the cells in the row are visible.
                            repeaterVisible = true;

                            bool currentlyVisible = q.Visible.Evaluate(true, AnswerProvider.Caller.Interview);

                            promptCell.Style.Add("display", currentlyVisible ? "" : "none");
                            hintCell.Style.Add("display", currentlyVisible ? "" : "none");
                        }
                    }

                    QuestionPrompt qp = new QuestionPrompt(q);
                    QuestionExample qe = new QuestionExample(q);
                    QuestionHint qh = new QuestionHint(q);

                    qp.DataBind();
                    qe.DataBind();
                    qh.DataBind();

                    promptCell.Controls.Add(qp);

                    bool haveExample = q != null && q.Example != null && q.Example.EvaluateAndKeepXml().Length > 0;
                    bool haveHint = q != null && q.Hint != null && q.Hint.EvaluateAndKeepXml().Trim().Length > 0;
                    bool questionStubHasExample = questionStubs[i].Example != null &&
                                                  questionStubs[i].Example.EvaluateAndKeepXml().Trim().Length > 0;
                    bool questionStubHasHint = questionStubs[i].Hint != null &&
                                               questionStubs[i].Hint.EvaluateAndKeepXml().Trim().Length > 0;

                    if (haveHint || questionStubHasHint)
                    {
                        hintCell.Controls.Add(qh);
                        hintRow.Visible = true;
                    }
                    if ((haveHint && haveExample) || questionStubHasHint && questionStubHasExample)
                    {
                        hintCell.Controls.Add(new LiteralControl("<br />"));
                    }
                    if (haveExample || questionStubHasExample)
                    {
                        hintCell.Controls.Add(qe);
                        hintRow.Visible = true;
                    }

                    break;
            }
        }

        protected override void OnDataBinding(EventArgs e)
        {
            hasBound = true;
            if (items != null)
            {
                itemTypes = new string[items.Count];
                itemIds = new string[items.Count];
                questionStubs = new Question[items.Count];
                int[] displayTypes = new int[items.Count];
                int mostAnswers = 0;
                for (int i = 0; i < items.Count; i++)
                {
                    IPageItem ipi = items[i];
                    itemIds[i] = ipi.UniqueIdentifier.ToString("N");
                    if (ipi is Question)
                    {
                        itemTypes[i] = "Q";
                        Question q = (Question)ipi;
                        displayTypes[i] = (int)q.DisplayType;
                        questionStubs[i] = q;
                        AnswerCollectionCollection collColl =
                            q.GetAnswerCollectionCollection(AnswerProvider.Caller.Interview);
                        if (collColl != null && collColl.Count > mostAnswers)
                        {
                            mostAnswers = collColl.Count;
                        }
                    }
                    else if (ipi is HorizontalLayoutZone)
                    {
                        itemTypes[i] = "H";
                    }
                    else if (ipi is HtmlBlock)
                    {
                        itemTypes[i] = "T";
                    }
                    else if (ipi is RepeaterZone)
                    {
                        itemTypes[i] = "R";
                    }
                    else
                        itemTypes[i] = "U";
                }

                if (mostAnswers == 0)
                {
                    numRepeats = mostAnswers + 1;
                }
                else
                {
                    numRepeats = mostAnswers;
                }
            }
        }

        private void createAddButton(int colSpan)
        {
            //if (NumRepeats < MaxRepeats && repeaterVisible == true || createButton == true)
            if (repeaterVisible && createButton)
            {
                TableRow buttonRow = new TableRow();
                // responsive dont need this spanage
                if (!responsive)
                {
                    TableCell paddingCell = new TableCell();
                    buttonRow.Cells.Add(paddingCell);
                    paddingCell.ColumnSpan = colSpan;
                }
                TableCell buttonCell = new TableCell();
                buttonCell.CssClass = "repeaterAdd";
                buttonRow.Cells.Add(buttonCell);
                Button addButton = new Button();
                addButton.Text =
                    ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString(
                        "RepeaterAddButtonText");
                addButton.Click += new EventHandler(addButton_Click);
                buttonCell.Controls.Add(addButton);
                addButton.CausesValidation = false;
                addButton.Enabled = (numRepeats <= maxRepeats) && !isPagePreview;
                addButton.ID = "addbtn";
                table.Rows.Add(buttonRow);
            }
        }

        private void PopulateRow(TableRow row, string[] _itemTypes, string[] _itemIds, int repeatNumber, bool setFocus,
                                 StringBindingMetaData stringBindingMetaData)
        {
            PopulateRow(row, _itemTypes, _itemIds, repeatNumber, setFocus, stringBindingMetaData, -1);
        }

        // responsive - only process one item if cell number is supplied
        private void PopulateRow(TableRow row, string[] _itemTypes, string[] _itemIds, int repeatNumber, bool setFocus,
                                 StringBindingMetaData stringBindingMetaData, int cellNumber)
        {
            //string scr = null;
            bool isFirstQuestion = true;

            for (int i = 0; i < _itemTypes.Length; i++)
            {
                // responsive is for one item only, normall all are done (its -1 by default)
                if (cellNumber == -1 || cellNumber == i)
                {
                    TableCell fieldCell = new TableCell();
                    fieldCell.Attributes.Add("nowrap", "true");
                    fieldCell.CssClass = "fieldCellHorizontal";
                    row.Cells.Add(fieldCell);

                    switch (_itemTypes[i])
                    {
                        case "Q":
                            Question q;
                            if (items != null)
                            {
                                q = (Question)items[i];
                                if (q.Visible.IsDefinitelyNo() && q.ParentPackage != null)
                                {
                                    fieldCell.Visible = false;
                                }
                                else
                                {
                                    fieldCell.Style.Add("display",
                                                        q.Visible.Evaluate(true, AnswerProvider.Caller.Interview)
                                                            ? ""
                                                            : "none");
                                }
                            }
                            else
                            {
                                q = questionStubs[i];
                            }

                            q.StringBindingMetaData = stringBindingMetaData;

                            QuestionField qf =
                                new QuestionField(q, scriptPath, imagesPath, repeatNumber, callButtonClickHandler,
                                                  instanceId, extensionMappings, loadAnswerSetHandler, isPagePreview, (maxRepeats > 1));
                            questionFieldControls.Add(qf);
                            fieldCell.Controls.Add(qf);
                            qf.ID = string.Format("{0}f{1}_on_{2:N}", row.ID, i, pageId);
                            if (isFirstQuestion && setFocus)
                            {
                                qfToFocus = qf;
                                isFirstQuestion = false;
                            }

                            // Nov 2015 removed all this default in favour of fix within the answersetupload itself.
                            ////Rick Mar 2015 pf-3126 defaulting the answerset is not compatible with livelink integration, 
                            ////              'Unable to cast object of type 'Perfectus.Livelink.ServerIntegration.DisplayTypeExtensions.DocumentPicker' to type 'Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.AnswerSetUpload'.'
                            ////              GetDefaultUpload needs to be implemented without changing the interface
                            //////Rick Dec 2012 pf-375/379
                            //if (q.DataType == Perfectus.Common.PerfectusDataType.AnswerSet)
                            //{
                            //    if (q.DefaultAnswer != null)
                            //    {
                            //        //get the replacement question id, tidy up the xml and replace it with the instance id
                            //        string defaultAnswerXml = q.DefaultAnswer.EvaluateAndKeepXml();
                            //        string newDefault = defaultAnswerXml;

                            //        // looks like <text>c:\rick\<item uId="b5706466-f5c6-4f38-8ef8-8ea56dab3de6"></item>answers.xml</text>
                            //        if (defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
                            //        {
                            //            defaultAnswerXml = defaultAnswerXml.Replace("<text>", "");
                            //            defaultAnswerXml = defaultAnswerXml.Replace("</text>", "");
                            //            newDefault = defaultAnswerXml.Substring(0, defaultAnswerXml.IndexOf("<item"));
                            //            newDefault += instanceId.ToString();
                            //            newDefault += defaultAnswerXml.Substring(defaultAnswerXml.IndexOf("></item>") + 8);
                            //        }
                            //        //success = ((AnswerSetUpload)qf.FieldControl).GetDefaultUpload(parentPackage, newDefault);
                            //        //Rick 22 July 2015 - update: In order to resolve the type error I duplicated this logic to Interview.cs and HorizontalLayoutZoneItem.cs
                            //        if (newDefault != null && newDefault.Length > 0)
                            //        {
                            //            XmlDocument doc = new XmlDocument();
                            //            if (File.Exists(newDefault))
                            //            {
                            //                doc.Load(newDefault);
                            //                Submitter.ApplyAnswerSet(parentPackage, doc, Submitter.QuestionMatchModeOption.Name, false);
                            //            }
                            //        }
                            //    }
                            //}

                            break;
                        case "T":
                            HtmlBlock b = null;
                            if (items != null)
                            {
                                b = (HtmlBlock)items[i];
                                if (b.Visible.IsDefinitelyNo())
                                {
                                    fieldCell.Visible = false;
                                }
                                else
                                {
                                    fieldCell.Style.Add("display",
                                                        b.Visible.Evaluate(true, AnswerProvider.Caller.Interview)
                                                            ? ""
                                                            : "none");
                                }
                            }

                            HtmlBlockItem hbi = new HtmlBlockItem(b, parentPackage, stringBindingMetaData, repeatNumber, (maxRepeats > 1));
                            hbi.DataBind();
                            fieldCell.Controls.Add(hbi);
                            break;
                    }
                    fieldCell.Attributes.Add("item", _itemIds[i]);
                }   // responsive
            }
            if (maxRepeats > 1)
            {
                // responsive takes care of the button, normally its always done (its -1 by default)
                if (cellNumber == -1)
                {
                    TableCell buttonCell = new TableCell();
                    Button removeButton = new Button();
                    removeButton.CausesValidation = false;
                    removeButton.Text =
                        ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString(
                            "RepeaterRemoveButtonText");
                    buttonCell.Controls.Add(removeButton);
                    removeButton.Enabled = (numRepeats > minRepeats) && !isPagePreview;
                    removeButton.ID = string.Format("{0}rmv", row.ID);
                    removeButton.CommandArgument = row.ID;
                    removeButton.Command += new CommandEventHandler(removeButton_Command);
                    buttonCell.CssClass = "repeaterRemove";
                    row.Cells.Add(buttonCell);
                }
            }
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            AddNewRow();
            OnSaveOnPostbackRequested();
        }

        private void AddNewRow()
        {
            EnsureChildControls();
            numRepeats++;

            int packageVer = 0;
            int packageRev = 0;

            try
            {
                InstanceManagement.GetVersion(instanceId, out packageVer, out packageRev);
            }
            catch (InvalidCastException) // GetVersion fails in page preview mode
            {
            }

            string reference = InstanceManagement.GetInstanceReference(instanceId);
            StringBindingMetaData stringBindingMetaData = new StringBindingMetaData();

            if (parentPackage != null)
            {
                stringBindingMetaData =
                    new StringBindingMetaData(parentPackage.Name, reference, instanceId, packageVer, packageRev);
            }

            if (table != null)
            {
                // Give the new row the same ID it will get on postback in CreateChildControls() - so its viewstate, etc. can get applied
                string newRowID;
                if (rowIds.Length > 0)
                {
                    int[] newRowIds = new int[rowIds.Length + 1];
                    rowIds.CopyTo(newRowIds, 0);
                    int oldMax = rowIds[rowIds.Length - 1];
                    newRowIds[newRowIds.Length - 1] = oldMax + 1;
                    rowIds = newRowIds;
                    newRowID = string.Format("r{0}", newRowIds[newRowIds.Length - 1]);
                    // Our IDs are zero based, but NumRepeats starts at one.
                }
                else
                {
                    newRowID = "r0";
                    rowIds = new int[] { 0 };
                }

                ResetAddRemoveButtons();
                TableRow newRow = new TableRow();
                newRow.ID = newRowID;
                PopulateRow(newRow, itemTypes, itemIds, numRepeats - 1, true, stringBindingMetaData);
                table.Rows.AddAt(table.Rows.Count - 1, newRow);
            }
        }


        private void removeButton_Command(object sender, CommandEventArgs e)
        {
            EnsureChildControls();
            int cItemTypes;
            string rowId = e.CommandArgument.ToString();
            try
            {
                cItemTypes = items.Count;
            }
            catch
            {
                cItemTypes = 1;
            }
            Control r = table.FindControl(rowId);
            if (r != null && r is TableRow)
            {
                TableRow tr = (TableRow)r;

                // Remove all controls in the table from the controlsCollection
                foreach (TableCell tc in tr.Cells)
                {
                    Control c = tc.Controls[0];
                    if (c is QuestionField)
                    {
                        QuestionField qf = (QuestionField)c;
                        questionFieldControls.Remove(qf);
                    }
                }

                table.Rows.Remove(tr);

                string ids = tr.ID.Replace("r", string.Empty);

                // responsive - this removed the last cell, e.g. rXn, now remove rXn-1, etc until n=0
                if (responsive)
                {

                    string rowIDLeftPart = rowId.Substring(0, rowId.Length - 1);
                    int rowIDRightPart = Convert.ToInt32(rowId.Substring(rowId.Length - 1, 1));

                    for (int i = 0; i < rowIDRightPart; i++)
                    {
                        string rowIDPart = rowIDLeftPart + i.ToString();
                        r = table.FindControl(rowIDPart);
                        if (r != null && r is TableRow)
                        {
                            tr = (TableRow)r;

                            // Remove all controls in the table from the controlsCollection
                            foreach (TableCell tc in tr.Cells)
                            {
                                Control c = tc.Controls[0];
                                if (c is QuestionField)
                                {
                                    QuestionField qf = (QuestionField)c;
                                    questionFieldControls.Remove(qf);
                                }
                            }

                            table.Rows.Remove(tr);
                        }
                    }
                    // strip last 0 from  tr.ID eg rX0 becomes rX
                    ids = rowIDLeftPart.Replace("r", string.Empty);
                }

                int id = Convert.ToInt32(ids);
                int[] newRowIds = new int[rowIds.Length - 1];
                int newIndex = 0;
                // Remove the row's ID from the array of IDs so it is never used again (would cause postback problems if it was)
                for (int i = 0; i < rowIds.Length; i++)
                {
                    if (rowIds[i] != id)
                    {
                        newRowIds[newIndex++] = rowIds[i];
                    }
                }

                rowIds = newRowIds;

                ResetAddRemoveButtons();
                if (numRepeats == maxRepeats)
                {
                    createButton = true;
                    createAddButton(cItemTypes);
                    createButton = false;
                }

                numRepeats--;
            }
            OnSaveOnPostbackRequested();
        }

        private void ResetAddRemoveButtons()
        {
            if (table != null)
            {
                bool enableButton = (numRepeats > minRepeats);
                for (int i = 0; i < rowIds.Length; i++)
                {
                    Control c = table.FindControl(string.Format("r{0}rmv", rowIds[i]));
                    if (c != null && c is Button)
                    {
                        Button b = (Button)c;
                        b.Enabled = enableButton;
                    }
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            base.Render(writer);
            if (qfToFocus != null)
            {
                writer.Write(
                    string.Format("<script type='text/javascript'>document.getElementById('{0}').focus();</script>",
                                  qfToFocus.FieldControl.InputClientId));
            }
        }
    }
}
