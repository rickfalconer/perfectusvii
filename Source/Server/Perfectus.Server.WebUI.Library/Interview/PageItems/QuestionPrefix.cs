using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
    /// <summary>
    /// Summary description for QuestionPrefix.
    /// </summary>
    internal class QuestionPrefix : Label
    {
        private Question question = null;

        private QuestionPrefix()
        {
        }

        public QuestionPrefix(Question q)
        {
            question = q;
            base.CssClass = "questionPrefix";
        }

        public override void DataBind()
        {
            if (question != null)
            {
                string strPrefix = question.Prefix.EvaluateAndKeepXml();
                Text = InterviewStringBinding.GetClientHtml(strPrefix, question.ParentPackage, question.StringBindingMetaData, 0, false);
            }
        }
    }
}
