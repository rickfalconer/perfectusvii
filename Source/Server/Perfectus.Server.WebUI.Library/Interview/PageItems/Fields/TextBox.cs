using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Globalization;
using System.Threading;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	internal class TextBox : FieldBase, INamingContainer
	{
		private System.Web.UI.WebControls.TextBox textbox;

        public TextBox(Question q, int repeatNumber, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
		}

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return textbox.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return textbox.ID;
            }
        }
		protected override void CreateField()
		{
			ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
			textbox = new System.Web.UI.WebControls.TextBox();
			
			CompareValidator cv = new CompareValidator();
			cv.Display = ValidatorDisplay.Dynamic;
			cv.CssClass = "validator";
			textbox.Enabled = Enabled;

			if (question != null)
			{
/*
				//Javascipt addition for Dynamic text. Adds onblur so that any items on the page bound to this control get updated.
				string uID = string.Format("uId{0}", question.UniqueIdentifier);
				uID = uID.Replace("-", "");
				textbox.Attributes["onBlur"] = string.Format("changeControlText('{0}',{1})",uID, "this.value");
				// End of Javascipt
*/

				if(question.ParentPackage != null) // first trip as full question is been used, not one made from the question stub.
				{
					// write out answer. either answered or default.
					AnswerCollection answer = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
				   
					if (answer != null  && answer.Count > 0)
					{
                        object oAnswer = AnswerProvider.ConvertToBestType(answer[0], question.DataType, AnswerProvider.Caller.Interview, question.StringBindingMetaData).ToString();
					    textbox.Text = InterviewSystem.Format.FormatForDisplay(oAnswer, question.InterviewFormatString, question.InterviewFormatStringParams, question.DataType, question.ParentPackage);
					}
				}
				else if(! Enabled && question.ParentPackage == null) // postback has happened. need to manually restore any enabled=false values.
				{
					if(question.Tag != null)
					{
						textbox.Text = question.Tag;
					}
					textbox.ReadOnly = true;
				}

				textbox.ID = "tb";

				cv.ControlToValidate = textbox.ID;
				cv.Operator = ValidationCompareOperator.DataTypeCheck;
				switch (question.DataType)
				{
					case PerfectusDataType.Number:
						cv.Type = ValidationDataType.Currency;
						cv.ErrorMessage = string.Format("<br />{0}", rm.GetString("ValidationNumber"));

                        //Change the current cultures currency format to allow 8dp instead of the default 2dp
                        NumberFormatInfo nfi = new NumberFormatInfo();
                        nfi = (NumberFormatInfo)Thread.CurrentThread.CurrentCulture.NumberFormat.Clone();
                        nfi.CurrencyDecimalDigits = 8;
                        CultureInfo ci = new CultureInfo(Thread.CurrentThread.CurrentCulture.ToString());
                        ci.NumberFormat = nfi;
                        Thread.CurrentThread.CurrentCulture = ci;

						break;
					case PerfectusDataType.Date:
						cv.Type = ValidationDataType.Date;
						cv.ErrorMessage = string.Format("<br />{0}", rm.GetString("ValidationDate"));
						break;
					default:
					case PerfectusDataType.Text:
						cv.Type = ValidationDataType.String;
						cv.ErrorMessage = string.Format("<br />{0}", rm.GetString("ValidationText"));
						break;
				}

				switch (question.DisplayType)
				{
					case QuestionDisplayType.Password:
						textbox.TextMode = TextBoxMode.Password;
						break;
					case QuestionDisplayType.TextArea:
						textbox.TextMode = TextBoxMode.MultiLine;
						textbox.Rows = question.DisplayHeight;
						break;
					default:
						textbox.TextMode = TextBoxMode.SingleLine;
						break;
				}

				textbox.Columns = question.DisplayWidth;

			}
			cv.Text = cv.ErrorMessage;
			cv.Visible = true;
            // Add question identifier so that we can track validator client side.
            cv.Attributes.Add( "quid", question.UniqueIdentifier.ToString( "N" ) );
            // Data type validations must always be enabled by default. This might change if the visibility changes, but have nothing to
            // do on whether the field is mandatory/visible or not! (FB 355)
            cv.Enabled = true;
            Controls.Add( textbox );


            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
			Controls.Add(cv);

			if (question.DataSize > 0)
			{
				textbox.MaxLength = question.DataSize;
				if (question.DisplayType == QuestionDisplayType.TextArea)
				{
					textbox.Attributes["maxlength"] = question.DataSize.ToString();
				}

				CustomValidator lv = new CustomValidator();
				lv.ServerValidate += new ServerValidateEventHandler(lv_ServerValidate);
				lv.ClientValidationFunction = "validateLength";
				lv.ControlToValidate = textbox.ID;
				lv.Display = ValidatorDisplay.Dynamic;
				lv.CssClass = "validator";
                // Add question identifier so that we can track validator client side.
                lv.Attributes.Add( "quid", question.UniqueIdentifier.ToString( "N" ) );

				string lvMsg = string.Format("<br />{0}", rm.GetString("ValidationLength"));
				lv.ErrorMessage = string.Format(lvMsg, question.DataSize);
				lv.Text = lv.ErrorMessage;
                Controls.Add( lv );
			}

			if (question.Validation != null && question.Validation.ValueType == RegexUrlValue.RegexUrlValueType.Regex && question.Validation.RegexValue != null && question.Validation.RegexValue.Trim().Length > 0)
			{
				RegularExpressionValidator rev = new RegularExpressionValidator();
				rev.ControlToValidate = textbox.ID;
				rev.ValidationExpression = question.Validation.RegexValue;
				rev.Display = ValidatorDisplay.Dynamic;
				if (question.ValidationErrorMessage != null && question.ValidationErrorMessage.EvaluateAndKeepXml().Trim().Length > 0)
				{
					rev.ErrorMessage = string.Format("<br />{0}", question.ValidationErrorMessage);
				}
				else
				{
					rev.ErrorMessage = string.Format("<br />{0}", rm.GetString("RegexValidatorDefaultMessage"));
				}

				rev.Text = rev.ErrorMessage;
				rev.CssClass = "validator";
                // Add question identifier so that we can track validator client side.
                rev.Attributes.Add( "quid", question.UniqueIdentifier.ToString( "N" ) );
                // Regular expression validations must always be enabled by default. This might change if the visibility changes, but have nothing to
                // do on whether the field is mandatory/visible or not! (FB 355)
                rev.Enabled = true;
                Controls.Add( rev );
			}

			base.CreateField();

			ChildControlsCreated = true;
		}

		protected override void CreateChildControls()
		{
			CreateField();
		}

        public override AnswerCollection GetAnswers()
        {
			EnsureChildControls();
			object o = GetBestType(textbox.Text);
            return new AnswerCollection(new object[] { o });
		}

		private void lv_ServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = (textbox.Text.Length <= question.DataSize);
		}
	}
}
