using System.Globalization;
using System.Resources;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
    /// <summary>
    /// Summary description for TextBox.
    /// </summary>
    internal class CheckBox : FieldBase, INamingContainer
    {
        private System.Web.UI.WebControls.CheckBox checkBox;
        private System.Web.UI.WebControls.TextBox hidden = null;

        public CheckBox(Question q, int repeatNumber, string imagesPath)
            : base(q, repeatNumber, imagesPath)
        {
        }
        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return checkBox.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return checkBox.ID;
            }
        }

        protected override void CreateField()
        {
            checkBox = new System.Web.UI.WebControls.CheckBox();
            checkBox.ID = "cb";
            checkBox.Enabled = Enabled;

            if (question != null)
            {
                if (question.ParentPackage != null) // first trip, because full question not one made from a stub is been used.
                {
                    AnswerCollection coll = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
                    if (coll != null)
                    {
                        object cbVal = coll[0];
                        object bestType = AnswerProvider.ConvertToBestType(cbVal, PerfectusDataType.YesNo, AnswerProvider.Caller.Interview, question.StringBindingMetaData);
                        if (coll.Count > 0)//&& coll[0] is bool)
                        {
                            if (bestType != null && bestType.ToString().ToLower() == "true")
                            {
                                checkBox.Checked = true;
                            }
                            else
                            {
                                checkBox.Checked = false;
                            }
                        }
                    }
                }
                else if (!Enabled && question.Tag != null && question.Tag.Length > 0) // postback has happened. need to manually restore any enabled=false values.
                {
                    checkBox.Checked = bool.Parse(question.Tag);
                }

            }
            Controls.Add(checkBox);

            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
            CreateHelp();

            // If the parentPackage property isn't null, we are a full question, not a serialised stub. Therefore, it's the first time we're rendered.
            bool shouldRenderRequiredValidator = false;
            if( ( question.ParentPackage != null ) && ( question.Mandatory != null ) &&
                !( question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo && question.Mandatory.YesNoValue == false ) )
                // Add required validator if we have a mandatory setting.
                shouldRenderRequiredValidator = true;

            //Is there a mandatory query?
            bool hasMandatoryQuery = question.Mandatory != null && question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query;

            // Work out the initial state. This will be set to no if there is a mandatory query but will change 
            // dynamically on script enabled browsers 
            bool mandatory = ( hasMandatoryQuery ) ? false : question.Mandatory.YesNoValue;

            if( shouldRenderRequiredValidator )
            {
                ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
                hidden = new System.Web.UI.WebControls.TextBox();
                hidden.Style.Add("display", "none");
                hidden.ID = "hdn";
                Controls.Add(hidden);
                checkBox.Attributes.Add("onclick", string.Format("document.getElementById('{0}').value = this.checked;", hidden.ClientID));
                CompareValidator cv = new CompareValidator();
                cv.ControlToValidate = hidden.ID;
                cv.Operator = ValidationCompareOperator.Equal;
                cv.ValueToCompare = "true";
                cv.Text = string.Format("<br />{0}", rm.GetString("RequiredFieldValidatorMessage"));
                //cv.Text = cv.ErrorMessage;
                cv.Display = ValidatorDisplay.Dynamic;
                cv.CssClass = "validator";
                // Add question identifier so that we can track validator client side.
                cv.Attributes.Add( "quid", question.UniqueIdentifier.ToString( "N" ) );
                // Flag that this is a required field validator for use in JS
                cv.Attributes.Add( "required", "yes" );
                cv.Enabled = mandatory;
                Controls.Add(cv);
            }
        }

        protected override void CreateChildControls()
        {
            CreateField();
        }

        public override AnswerCollection GetAnswers()
        {
            EnsureChildControls();
            bool b = checkBox.Checked;
            return new AnswerCollection(new object[] { b });
        }

        protected override void OnPreRender(System.EventArgs e)
        {
            if (hidden != null)
            {
                hidden.Text = checkBox.Checked.ToString().ToLower(CultureInfo.InvariantCulture);
            }
            base.OnPreRender(e);
        }

    }
}
