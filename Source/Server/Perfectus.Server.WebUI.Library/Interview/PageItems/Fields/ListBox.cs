using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	internal class ListBox : FieldBase, INamingContainer
	{
		private System.Web.UI.WebControls.ListBox listbox;
		private AnswerCollection toSelect = null;
        private int listBoxRows = 4;

        public ListBox(Question q, int repeatNumber, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
            var rows = System.Configuration.ConfigurationManager.AppSettings["ListBoxRows"];
            if (rows != null)
                listBoxRows = int.Parse(rows);
        }

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return listbox.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return listbox.ID;
            }
        }
		protected override void CreateField()
		{
			listbox = new System.Web.UI.WebControls.ListBox();
			listbox.Enabled = Enabled;
            listbox.Rows = listBoxRows;
            listbox.ID = "lb";
			if (question != null)
			{
				if (question.Items != null)
				{
                    switch (question.DisplayType)
                    {
                        case QuestionDisplayType.MultiSelectListBox:
                            listbox.SelectionMode = ListSelectionMode.Multiple;
                            break;
                        default:
                            listbox.SelectionMode = ListSelectionMode.Single;
                            break;
                    }

/*
					//Javascipt addition for Dynamic text
				    string controlID = string.Format("uId{0}", question.UniqueIdentifier);
					controlID = controlID.Replace("-", "");
					listbox.Attributes["onBlur"] = string.Format("changeControlText('{0}',{1})",controlID, "this.value");
					// End of Javascipt
*/

					listbox.DataSource = question.Items;
					listbox.DataTextField = "Display";
					listbox.DataValueField = "Value";
					listbox.DataBind();
				}
				if (question.GetAnswerCount(AnswerProvider.Caller.Interview) > 0)
				{
					object oAnswer = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
					if (oAnswer != null && oAnswer is AnswerCollection)
					{
						toSelect = (AnswerCollection) oAnswer;
					}		
				}
			}
			Controls.Add(listbox);
            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
			base.CreateField();
		    ChildControlsCreated = true;
		}

		protected override void CreateChildControls()
		{
			CreateField();
		}

        public override AnswerCollection GetAnswers()
        {
            EnsureChildControls();
			AnswerCollection coll = new AnswerCollection();
			foreach (ListItem li in listbox.Items)
			{
				if (li.Selected)
				{
					coll.Add(GetBestType(li.Value));
				}
			}
			return coll;
		}

		protected override void OnDataBinding(EventArgs e)
		{
			if (question != null)
			{
				Controls.Clear();
				ClearChildViewState();

				CreateField();


				listbox.Style.Add("z-index", "-1");
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

            if( listbox != null &&  toSelect == null || toSelect.Count <= 0 )
                return;

            // Remove all current selections such as the default maybe?
            listbox.ClearSelection( );

            // Scan through list of selection objects.
			foreach( object o in toSelect )
			{
			    string answer = string.Empty;

                // Determine the answer in a string format.
                if( o is TextQuestionQueryValue)
                    answer = ((TextQuestionQueryValue)o).Evaluate(AnswerProvider.Caller.Interview, question.StringBindingMetaData);
                else
                    answer = o.ToString( );

                // Ignore any select objects that have no valid answer.
                if( String.IsNullOrEmpty( answer ) )
                    continue;

                ListItem itemToSelect = null;

                // Scan through all items in the list to find a match
                foreach( ListItem item in listbox.Items )
                {
                    // Is this item already selected?
                    if( item.Selected )
                        continue;

                    if( this.question != null )
                    {
                        // Perform specific comparisons based on data type of the question...
                        switch( this.question.DataType )
                        {
                            // FB 1610: Date type questions need to perform date type comparisons.
                            case PerfectusDataType.Date:
                                try
                                {
                                    if( DateTime.Compare( Convert.ToDateTime( item.Value ), Convert.ToDateTime( answer ) ) == 0 )
                                        itemToSelect = item;
                                }
                                // Ignore any conversion exceptions.
                                catch { }
                                break;
                            // At the moment there are no other required type specific comparisons. If we later identify some
                            // they should be placed in the switch statement. For example numbers are currently mapped exactly by their 
                            // string representation on the list. ie (1.00 != 1.0)
                            default:
                                break;
                        }
                    }

                    // Perform a straight string comparison if nothing so far.
                    if( itemToSelect == null ) 
                    {
                        // Perform a case sensitive and culture insensitive comparison. As part of case FB1345 we also trim spaces off the strings...
                        if( String.Compare( item.Value.Trim( ), answer.Trim( ), false, System.Globalization.CultureInfo.InvariantCulture ) == 0 )
                            itemToSelect = item;
                    }

                    // If we've found one, no need to continue.
                    if( itemToSelect != null )
                        break;
                }

                // Finally flag the item in the listbox
                if( itemToSelect != null )
                    itemToSelect.Selected = true;
			}
        }
	}
}
