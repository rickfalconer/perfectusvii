using System;
using System.Web.UI;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Resources;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
    /// <summary>
    /// Summary description for TextBox.
    /// </summary>
    internal class Calendar : FieldBase, INamingContainer
    {
        System.Web.UI.WebControls.TextBox dateTextBox;
        CalendarExtender calendarExtender;
        HtmlImage popupImage;
        CompareValidator compareValidator;

        public Calendar(Question question, string scriptPath, int repeatNumber, string imagesPath) : base(question, repeatNumber, imagesPath)
        {
            this.imagesPath = imagesPath;
        }

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return dateTextBox.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return dateTextBox.ID;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            calendarExtender.TargetControlID = dateTextBox.ID;
            base.OnPreRender(e);
        }

        protected override void CreateField()
        {
            ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
            dateTextBox = new System.Web.UI.WebControls.TextBox();
            calendarExtender = new CalendarExtender();

            if (question != null)
            {
                if (question.ParentPackage != null) // first trip, as full question is been used, not one made from the stub.
                {
                    AnswerCollection coll = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
                    
                    if (coll != null && coll.Count > 0)
                    {
                        object o = AnswerProvider.ConvertToBestType(coll[0], PerfectusDataType.Date, AnswerProvider.Caller.Interview, question.StringBindingMetaData);
                        
                        if (o != null && o is DateTime)
                        {
                            dateTextBox.Text = ((DateTime)o).ToShortDateString();
                        }
                    }
                }
                else if (!Enabled && question.Tag != null && question.Tag.Length > 0) // postback has happened. need to manually restore any enabled=false values.
                {
                    dateTextBox.Text = DateTime.Parse(question.Tag).ToShortDateString();
                }
            }

            popupImage = new HtmlImage();
            popupImage.Attributes.Add("Type", "image");
            popupImage.Attributes.Add("src", imagesPath + "/calendar.gif");
            popupImage.ID = "calbtn";
            popupImage.Style.Add("vertical-align", "middle");

            calendarExtender.ID = "cal";                      //this is what holds our actual date
            calendarExtender.PopupButtonID = popupImage.ID;
            calendarExtender.Animated = true;

            dateTextBox.ID = "caltxt";
            dateTextBox.Style.Add("vertical-align", "middle");

            calendarExtender.TargetControlID = dateTextBox.ID;

            compareValidator = new CompareValidator();
            compareValidator.ID = "calVal";
            compareValidator.Operator = ValidationCompareOperator.DataTypeCheck;
            compareValidator.Type = ValidationDataType.Date;
            compareValidator.ControlToValidate = dateTextBox.ID;
            compareValidator.Display = ValidatorDisplay.Dynamic;
            compareValidator.Text = string.Format("<br />{0}", rm.GetString("ValidationDate"));
            
            QuestionSuffix questionSuffix = new QuestionSuffix(question);
            questionSuffix.DataBind();

            Controls.Add(calendarExtender);
            Controls.Add(dateTextBox);
            Controls.Add(popupImage);
            Controls.Add(questionSuffix);
            CreateHelp();
            Controls.Add(compareValidator);
    
            base.CreateField();
        }

        public override AnswerCollection GetAnswers()
        {
            EnsureChildControls();
            DateTime dateTime;

            try
            {
                dateTime = Convert.ToDateTime(dateTextBox.Text);
            }
            catch
            {
                return new AnswerCollection();
            }

            return new AnswerCollection(new object[] { dateTime });
        }


        protected override void CreateChildControls()
        {
            CreateField();
        }
    }
}
