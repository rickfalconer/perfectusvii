using System;
using System.Collections.Generic;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.AnswerAcquirer.Xml;
using System.IO;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for FileUpload.
	/// </summary>
	internal class AnswerSetUpload : FieldBase, INamingContainer
	{
		private HtmlInputFile fileUp;
		private Label label;
		private Button deleteBtn;
		private Guid instanceId;
		private bool haveSaved = false;
		private CustomValidator fileTypeValidator;

		public AnswerSetUpload(Question q, int repeatNumber, Guid instanceId, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
			this.instanceId = instanceId;
		}

	    private string filePath;

		public override string InputClientId
		{
			get
			{
				EnsureChildControls();
                return fileUp.ClientID;
			}
		}

		public override string InputId
		{
			get
			{
                EnsureChildControls();
                return fileUp.ID;
            }
		}

        public override AnswerCollection GetAnswers()
		{
            if (label.Text != null && label.Text.Trim().Length > 0 && label.Visible)
			{
				// Not the actual answer, but this is all that we put into the answerset.
				return new AnswerCollection(new object[] {label.Text});                
			}

			else
			{
                return new AnswerCollection();
			}
		}

		protected override void CreateChildControls()
		{
			Controls.Clear();
			CreateField();
			ChildControlsCreated = true;
		}

		protected override void OnLoad(EventArgs e)
		{
			EnsureChildControls();
			fileTypeValidator.Validate();
			base.OnLoad(e);
		}

		protected override void CreateField()
		{
			fileUp = new HtmlInputFile();
			if (!Enabled)
			{
				fileUp.Attributes.Add("disabled", "true");
			}
			label = new Label();
			deleteBtn = new Button();
			fileTypeValidator = new CustomValidator();
			HtmlInputHidden hidden = new HtmlInputHidden();


			ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
			fileUp.ID = "fu";
			label.ID = "fl";
			deleteBtn.ID = "fb";
			deleteBtn.Click += new EventHandler(deleteBtn_Click);
			hidden.Value = "v";
			deleteBtn.Text = rm.GetString("FileDeleteButtonText");

			fileTypeValidator.ErrorMessage = string.Format("{0}", rm.GetString("AnswerSetUploadTypeValidatorMessage"));
			fileTypeValidator.ServerValidate += new ServerValidateEventHandler(fileTypeValidator_ServerValidate);
			fileTypeValidator.Text = fileTypeValidator.ErrorMessage;
			fileTypeValidator.ControlToValidate = fileUp.ID;

			fileTypeValidator.Enabled = true;
			fileTypeValidator.ClientValidationFunction = "validateUploadXmlExtension";
			fileTypeValidator.CssClass = "validator";
            fileTypeValidator.Display = ValidatorDisplay.Dynamic;

            bool autoload = false;
            AnswerCollection coll;

            if (question != null && question.ParentPackage != null) // If ParentPackage is populated, we have a proper question, rather than just a stub.
            {
                //pf-3168 autoload answerset
                string defaultFilename = string.Empty;
                if (question.DefaultAnswer != null)
                {
                    if (question.DefaultAnswer.TextValue != null)
                    {
                        if (question.DefaultAnswer.TextValue.Length > 0)
                        {
                            coll = question.GetAnswer(repeatNumber, AnswerProvider.Caller.System);
                            if (coll != null && coll.Count > 0)
                            {
                                object oAnswer = AnswerProvider.ConvertToBestType(coll[0], question.DataType, AnswerProvider.Caller.System, question.StringBindingMetaData).ToString();
                                defaultFilename = InterviewSystem.Format.FormatForDisplay(oAnswer, question.InterviewFormatString, question.InterviewFormatStringParams, question.DataType, question.ParentPackage);
                            }
                            if (defaultFilename == string.Empty)
                            {
                                // fall back to old evaluate function
                                defaultFilename = question.DefaultAnswer.Evaluate(question.StringBindingMetaData);
                            }
                        }
                    }
                }

                coll = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
                if (coll != null && coll.Count > 0)
                {
                    if (coll[0] != null)
                    {
                        filePath = coll[0].ToString();

                        //pf-3168 if answer is not picked autoload default answerset filename with q in bound string, or plain q
                        if (defaultFilename.Length > 0 && fileUp.PostedFile == null)
                        {
                            XmlDocument doc = new XmlDocument();
                            if (File.Exists(defaultFilename))
                            {
                                doc.Load(defaultFilename);
                                Submitter.ApplyAnswerSet(question.ParentPackage, doc, Submitter.QuestionMatchModeOption.Name, false);
                                haveSaved = true;
                                autoload = true;
                            }
                        }
                    }
                }
            }

            if (autoload)
            {
                fileUp.Visible = false;
                label.Visible = true;
                label.Text = filePath;
                deleteBtn.Visible = false;
                fileUp.Attributes.Add("disabled", "true");
            }
            else
            {
                if (filePath != null && filePath.Length > 0)
                {
                    fileUp.Visible = false;
                    label.Visible = true;
                    label.Text = filePath;
                    deleteBtn.Visible = true;
                }
                else
                {
                    fileUp.Visible = true;
                    label.Visible = false;
                    deleteBtn.Visible = false;
                }
            }

			Controls.Add(hidden);
			Controls.Add(fileUp);
			Controls.Add(label);
			QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
            Controls.Add(deleteBtn);
            Controls.Add(fileTypeValidator);			
			base.CreateField();
		}


		public string getFileName()
		{
			string theFile = "";
			if (fileTypeValidator.IsValid && fileUp.PostedFile != null && fileUp.PostedFile.ContentLength > 0)
			{
				HttpPostedFile file = fileUp.PostedFile;
				if (file != null && file.ContentLength > 0)
				{
					theFile = file.FileName.ToString();
				}
			}
			else
			{
				theFile = "";
			}
			return theFile;
		}
		
        /// <summary>
        ///     Loads the file (AnswerSet) and applies the answers to the given package instance.
        /// </summary>
        /// <param name="instance">A package instance whose questions are going to have the answerSet's answers applied to it.</param>
        /// <returns>Success indicator.</returns>
		public bool LoadFile(Package instance)
		{
			if (fileTypeValidator.IsValid && !haveSaved && fileUp.PostedFile != null && fileUp.PostedFile.ContentLength > 0)
			{
				HttpPostedFile file = fileUp.PostedFile;
				if (file != null && file.ContentLength > 0)
				{
					XmlDocument doc = new XmlDocument();
					doc.Load(fileUp.PostedFile.InputStream);
					Submitter.ApplyAnswerSet(instance, doc, Submitter.QuestionMatchModeOption.Name, false);
					filePath = fileUp.PostedFile.FileName;
					haveSaved = true;
					return true;
				}
			}

			return false;
		}

        //Rick 22 July 2015 - update: In order to resolve the type error I duplicated this logic to Interview.cs and HorizontalLayoutZoneItem.cs
        //Rick Mar 2015 pf-3126 defaulting the answerset is not compatible with livelink integration, 
        //              'Unable to cast object of type 'Perfectus.Livelink.ServerIntegration.DisplayTypeExtensions.DocumentPicker' to type 'Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.AnswerSetUpload'.'
        //              GetDefaultUpload needs to be implemented without changing the interface
        ////Rick Dec 2012 PF-375 Answerset with a default filename can auto load
        //public bool GetDefaultUpload(Package instance, string filename)
        //{
        //    if (filename != null && filename.Length > 0)
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        if (File.Exists(filename))
        //        {
        //            doc.Load(filename);
        //            Submitter.ApplyAnswerSet(instance, doc, Submitter.QuestionMatchModeOption.Name, false);
        //            filePath = filename;
        //            haveSaved = true;
        //            return true;
        //        }
        //    }

        //    return false;
        //}

		private void deleteBtn_Click(object sender, EventArgs e)
		{
			InstanceManagement.File_Delete(instanceId, repeatNumber, question.UniqueIdentifier);
			filePath = null;
			deleteBtn.Visible = false;
			label.Text = string.Empty;
			label.Visible = false;
			fileUp.Visible = true;
		}

		private void fileTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
		{
            AnswerSet answerSet;

			args.IsValid = true;
			if (fileUp != null && fileUp.Visible && fileUp.PostedFile != null)
			{
				string t = fileUp.PostedFile.ContentType;
				switch (t)
				{
					case "text/xml":
						args.IsValid = true;
						break;
					case "application/x-xml":
						args.IsValid = true;
						break;
					default:
						args.IsValid = false;
						break;
				}

                // Validate that the file is a valid XmlDocument 
                try
                {
                    // Validate that the file is a valid Perfectus AnswerSet
                    XmlSerializer serializer = new XmlSerializer(typeof(AnswerSet));
                    fileUp.PostedFile.InputStream.Position = 0;
                    answerSet = (AnswerSet)serializer.Deserialize(fileUp.PostedFile.InputStream);
                    fileUp.PostedFile.InputStream.Position = 0; // Ensure we leave the position at the start

                    if (answerSet != null && !answerSet.IsValid)
                    {
                        args.IsValid = false;
                    }
                }
                catch (Exception ex)
                {
                    args.IsValid = false;
                    return;
                }
			}
		}
	}
}
