using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	internal class RadioList : FieldBase, INamingContainer
	{
		private RadioButtonList radioList;
		private int itemToSelect = -2;

        public RadioList(Question q, int repeatNumber, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
            base.CssClass = "radioList";
		}

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return radioList.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return radioList.ID;
            }
        }
		protected override void CreateField()
		{
			radioList = new RadioButtonList();
			radioList.Enabled = Enabled;
            itemToSelect = -2;

			if (question != null)
			{
				radioList.RepeatDirection = (question.Direction == Question.ListDirection.Horizontal) ? RepeatDirection.Horizontal : RepeatDirection.Vertical;
				if (question.Items != null)
				{
//					if(radioList.RepeatDirection == RepeatDirection.Horizontal)
//					{
//						radioList.Attributes.CssStyle.Add("display","inline");
//					}
				
					radioList.DataSource = question.Items;
					radioList.DataTextField = "Display";
					radioList.DataValueField = "Value";
					radioList.ID = "rl";
					radioList.DataBind();
				}
				if (question.GetAnswerCount(AnswerProvider.Caller.Interview) > 0)
				{
					object oAnswer = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
					if (oAnswer != null && oAnswer is AnswerCollection)
					{
						AnswerCollection coll = (AnswerCollection) oAnswer;
						if (coll.Count > 0)
						{
							string answer = String.Empty;

                            if (coll[0] is TextQuestionQueryValue)
                                answer = ((TextQuestionQueryValue)coll[0]).Evaluate(AnswerProvider.Caller.Interview, question.StringBindingMetaData);
							else
								answer = coll[0].ToString();

                            // Ignore any select objects that have no valid answer.
                            if( !String.IsNullOrEmpty( answer ) )
                            {
							    for (int i = 0; i < radioList.Items.Count; i++)
							    {
                                    ListItem item = radioList.Items[ i ];

                                    // Perform specific comparisons based on data type of the question...
                                    switch( this.question.DataType )
                                    {
                                        // FB 1610: Date type questions need to perform date type comparisons.
                                        case Perfectus.Common.PerfectusDataType.Date:
                                            try
                                            {
                                                if( DateTime.Compare( Convert.ToDateTime( item.Value ), Convert.ToDateTime( answer ) ) == 0 )
                                                    itemToSelect = i;
                                            }
                                            // Ignore any conversion exceptions.
                                            catch { }
                                            break;
                                        // At the moment there are no other required type specific comparisons. If we later identify some
                                        // they should be placed in the switch statement. For example numbers are currently mapped exactly by their 
                                        // string representation on the list. ie (1.00 != 1.0)
                                        default:
                                            break;
                                    }

                                    // Perform a straight string comparison if nothing so far.
                                    if( itemToSelect == -2 ) 
                                    {
                                        // Perform a case sensitive and culture insensitive comparison. As part of case FB1345 we also trim spaces off the strings...
                                        if( String.Compare( item.Value.Trim( ), answer.Trim( ), false, System.Globalization.CultureInfo.InvariantCulture ) == 0 )
                                            itemToSelect = i;
                                    }

                                    if( itemToSelect > -2 ) 
                                        break;
                                }
                            }
						}
					}
				}
			}
			Controls.Add(radioList);
            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
			base.CreateField();
		}

		protected override void CreateChildControls()
		{
			CreateField();
		}

        public override AnswerCollection GetAnswers()
        {
			EnsureChildControls();
			object v = GetBestType(radioList.SelectedValue);
            return new AnswerCollection(new object[] {v});
		}

		protected override void OnDataBinding(EventArgs e)
		{
			if (question != null)
			{
				Controls.Clear();
				ClearChildViewState();

				CreateField();
				switch (question.Direction)
				{
					case Question.ListDirection.Horizontal:
						radioList.RepeatDirection = RepeatDirection.Horizontal;
						break;
					default:
						radioList.RepeatDirection = RepeatDirection.Vertical;
						break;
				}

				ChildControlsCreated = true;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (itemToSelect > -2)
			{
				radioList.ClearSelection();
				radioList.SelectedIndex = itemToSelect;
			}
		}

	}
}
