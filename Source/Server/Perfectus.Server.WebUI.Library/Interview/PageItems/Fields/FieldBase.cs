using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Resources;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
    /// <summary>
    /// Summary description for FieldBase.
    /// </summary>
    public abstract class FieldBase : WebControl
    {
        protected Question question;
        protected int repeatNumber;
        protected string imagesPath;
        protected object parameter;

        public abstract string InputClientId { get; }

        public abstract string InputId { get; }

		public FieldBase(Question question, int repeatNumber, string imagesPath)
		{
			this.imagesPath = imagesPath;
			this.question = question;
			this.repeatNumber = repeatNumber;
			this.imagesPath = imagesPath;

		}

		protected virtual void CreateField()
		{
            //Is there a mandatory query?
            bool hasMandatoryQuery = question.Mandatory != null && question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query;

			// If the parentPackage property isn't null, we are a full question, not a serialised stub. Therefore, it's the first time we're rendered.
            bool shouldRenderRequiredValidator = false;
            if( ( question.ParentPackage != null ) && ( question.Mandatory != null ) && 
                !( question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo && question.Mandatory.YesNoValue == false))
                // Add required validator if we have a mandatory setting.
				shouldRenderRequiredValidator = true;

			// Work out the initial state. This will be set to no if there is a mandatory query but will change 
            // dynamically on script enabled browsers 
            bool mandatory = ( hasMandatoryQuery ) ? false : question.Mandatory.YesNoValue;

			if (shouldRenderRequiredValidator)
			{
				ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
				RequiredFieldValidator rfv = new RequiredFieldValidator();
				//TODO: Do we have validators on multi field pickers?
			    rfv.ControlToValidate = InputId;
				rfv.ErrorMessage = string.Format("<br />{0}", rm.GetString("RequiredFieldValidatorMessage"));
				rfv.Text = rfv.ErrorMessage;
				rfv.Display = ValidatorDisplay.Dynamic;
				rfv.CssClass = "validator";
				rfv.ID = "rfv";
                // The initial state based on whether query or not
                rfv.Enabled = mandatory;
                // Add question identifier so that we can track validator client side.
                rfv.Attributes.Add( "quid", question.UniqueIdentifier.ToString( "N" ) );
                // Flag that this is a required field validator for use in JS
                rfv.Attributes.Add( "required", "yes" );
                Controls.Add( rfv );
			}
		}

		protected void CreateHelp()
		{
		    ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");

			if (question.Help != null && question.Help.Length > 0)
			{
				string lcaseHelp = question.Help.Trim();
				bool isUrl = (lcaseHelp.Length >= 7 && lcaseHelp.IndexOf("http://") == 0) || (lcaseHelp.Length >= 8 && lcaseHelp.IndexOf("https://") == 0);
                // PF-3167 embed both link and text description in the help text property. 
                // The format is an html HREF e.g. <a href="http://my.site.com/newsupplier.aspx?param=xyz">Click to load new supplier</a> 
                bool isHREF = (lcaseHelp.Length >= 16 && lcaseHelp.IndexOf("<a href=") == 0);
			    
                Control c;
			    if (imagesPath != null && imagesPath.Trim().Length > 0)
				{
					HtmlImage img = new HtmlImage();
					img.Src = string.Format("{0}/help.gif", imagesPath);
					img.Attributes.Add("class", "help");
					if (isUrl == false && isHREF == false)
					{
						img.Attributes.Add("title", question.Help);
					}
					c = img;
				}
				else
				{
					Label lbl = new Label();
					lbl.Text = "?";
					lbl.CssClass = "help";
					if (! isUrl)
					{
						lbl.ToolTip = question.Help;
					}
					c = lbl;
				}


				if (isUrl)
				{
					HtmlAnchor a = new HtmlAnchor();
					a.Controls.Add(c);
					a.Attributes.Add("class", "help");
					a.HRef = lcaseHelp;
					a.Target = "_blank";
					a.Title = rm.GetString("HelpClickHere");
					Controls.Add(a);
				}
                else if(isHREF)
                {
                    HtmlAnchor a = new HtmlAnchor();
                    a.Controls.Add(c);
                    a.Attributes.Add("class", "help");
                    a.HRef = lcaseHelp.Substring(lcaseHelp.IndexOf("http"), lcaseHelp.IndexOf(">") - lcaseHelp.IndexOf("http")-1);
                    a.Target = "_blank";
                    a.Title = lcaseHelp.Substring(lcaseHelp.IndexOf(">") + 1, lcaseHelp.IndexOf("</a>") - lcaseHelp.IndexOf(">") - 1);
                    Controls.Add(a);
                }
                else
				{
					Controls.Add(c);
				}
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			if (question != null)
			{
				Controls.Clear();
				ClearChildViewState();

				CreateField();

				ChildControlsCreated = true;
			}
		}

		protected object GetBestType(object answerFromForm)
		{
			return AnswerProvider.ConvertToBestType(answerFromForm, question.DataType,AnswerProvider.Caller.Interview, question.StringBindingMetaData);
		}

        public abstract AnswerCollection GetAnswers();
	}
}
