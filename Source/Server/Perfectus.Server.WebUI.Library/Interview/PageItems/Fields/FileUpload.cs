using System;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for FileUpload.
	/// </summary>
	internal class FileUpload : FieldBase, INamingContainer
	{
		private HtmlInputFile fileUp;
		private Label label;
		private Button deleteBtn;
		private Guid instanceId;
		private bool haveSaved = false;
		private CustomValidator fileTypeValidator;

		public FileUpload(Question q, int repeatNumber, Guid instanceId, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
			this.instanceId = instanceId;
		}

	    private string filePath = null;

		public override string InputClientId
		{
			get
			{
				EnsureChildControls();
				return fileUp.ClientID;
			}
		}

		public override string InputId
		{
			get
			{
				EnsureChildControls();
				return fileUp.ID;
			}
		}
		public override AnswerCollection GetAnswers()
		{
			if (label.Text != null && label.Text.Trim().Length > 0 && label.Visible)
			{
				// Not the actual answer, but this is all that we put into the answerset.
				return new AnswerCollection(new object[] {label.Text});
			    
			}
            else if (fileUp.Value != null && fileUp.Value.Trim().Length > 0 && fileUp.Visible)
            {
                return new AnswerCollection(new object[] {fileUp.Value});
            }
			else
			{
				return new AnswerCollection();
				
			}
		}

		protected override void CreateChildControls()
		{
			Controls.Clear();
			CreateField();
			ChildControlsCreated = true;
		}

		protected override void OnLoad(EventArgs e)
		{
			EnsureChildControls();
			fileTypeValidator.Validate();
			base.OnLoad(e);
		}

		protected override void CreateField()
		{
			fileUp = new HtmlInputFile();
			if (!Enabled)
			{
				fileUp.Attributes.Add("disabled", "true");
			}
			label = new Label();
			deleteBtn = new Button();
			fileTypeValidator = new CustomValidator();
			HtmlInputHidden hidden = new HtmlInputHidden();


			ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
			fileUp.ID = "fu";
			label.ID = "fl";
			deleteBtn.ID = "fb";
			deleteBtn.Click += new EventHandler(deleteBtn_Click);
			hidden.Value = "v";
			deleteBtn.Text = rm.GetString("FileDeleteButtonText");

			fileTypeValidator.ErrorMessage = string.Format("<br />{0}", rm.GetString("FileUploadTypeValidatorMessage"));
			fileTypeValidator.ServerValidate += new ServerValidateEventHandler(fileTypeValidator_ServerValidate);
			fileTypeValidator.Text = fileTypeValidator.ErrorMessage;
			fileTypeValidator.ControlToValidate = fileUp.ID;

			fileTypeValidator.Enabled = true;
			fileTypeValidator.ClientValidationFunction = "validateUploadExtension";
			fileTypeValidator.CssClass = "validator";
			fileTypeValidator.Display = ValidatorDisplay.Dynamic;

            //Rick Dec 2012 pf- default plus instance id metadata in the filename
            //TODO test this with file_upload.ip
            if (question.DefaultAnswer != null)
            {
                //get the replacement question id, tidy up the xml and replace it with the instance id
                string defaultAnswerXml = question.DefaultAnswer.EvaluateAndKeepXml();
                string newDefault = defaultAnswerXml;

                // looks like <text>c:\rick\<item uId="b5706466-f5c6-4f38-8ef8-8ea56dab3de6"></item>answers.xml</text>
                if (defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
                {
                    defaultAnswerXml = defaultAnswerXml.Replace("<text>", "");
                    defaultAnswerXml = defaultAnswerXml.Replace("</text>", "");
                    newDefault = defaultAnswerXml.Substring(0, defaultAnswerXml.IndexOf("<item"));
                    newDefault += instanceId.ToString();
                    newDefault += defaultAnswerXml.Substring(defaultAnswerXml.IndexOf("></item>") + 8);
                    question.DefaultAnswer.TextValue = newDefault;
                }
            }

			if (question != null && question.ParentPackage != null) // If ParentPackage is populated, we have a proper question, rather than just a stub.
			{
				AnswerCollection coll = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
				if (coll != null && coll.Count > 0)
				{
					if (coll[0] != null)
					{
						filePath = coll[0].ToString();
					}
				}
			}

			if (filePath != null && filePath.Length > 0)
			{
				fileUp.Visible = false;
				label.Visible = true;
				label.Text = filePath;
				deleteBtn.Visible = true;
                deleteBtn.CausesValidation = false;
			}
			else
			{
				fileUp.Visible = true;
				label.Visible = false;
				deleteBtn.Visible = false;
			}

			Controls.Add(hidden);
			Controls.Add(fileUp);
			Controls.Add(label);
            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
			Controls.Add(fileTypeValidator);
			Controls.Add(deleteBtn);
			base.CreateField();
		}


		public bool WriteFileIfPresent()
		{
			if (fileTypeValidator.IsValid && !haveSaved && fileUp.PostedFile != null && fileUp.PostedFile.ContentLength > 0)
			{
				HttpPostedFile file = fileUp.PostedFile;
				if (file != null && file.ContentLength > 0)
				{
					byte[] bytes = new byte[file.ContentLength];
					file.InputStream.Read(bytes, 0, file.ContentLength);
					InstanceManagement.File_AddUpdate(instanceId, repeatNumber, question.UniqueIdentifier, file.FileName, file.ContentType, bytes);
					fileUp.Visible = false;
					filePath = fileUp.PostedFile.FileName;
					label.Text = filePath;
					label.Visible = true;
					deleteBtn.Visible = true;
					haveSaved = true;
					return true;
				}
			}

			return false;
		}


		private void deleteBtn_Click(object sender, EventArgs e)
		{
			InstanceManagement.File_Delete(instanceId, repeatNumber, question.UniqueIdentifier);
			filePath = null;
			deleteBtn.Visible = false;
			label.Text = string.Empty;
			label.Visible = false;
			fileUp.Visible = true;
		}

		private void fileTypeValidator_ServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = true;
            if (fileUp != null && fileUp.Visible && fileUp.PostedFile != null)
            {
                //pf-3086 office 2003 no longer compatible for upload, ok for attachment though
                if (this.question.DataType != PerfectusDataType.Attachment)
                {
                    string t = fileUp.PostedFile.ContentType;
                    switch (t)
                    {
                        //pf-3086 office 2003 no longer compatible for upload
                        //case "application/msword":    
                        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                        case "text/xml":
                        case "text/richtext":
                        case "application/rtf":
                        case "image/gif":
                        case "image/jpeg":
                        case "image/pjpeg":
                        case "image/x-ms-bmp":
                        case "image/x-png":
                        case "image/png":
                        case "image/bmp":
                        case "image/x-xbitmap":
                        case "image/tiff":
                            args.IsValid = true;
                            break;
                        default:
                            args.IsValid = false;
                            break;
                    }
                }
            }
		}

        protected override void OnInit(EventArgs e)
        {

            Page.RegisterRequiresControlState(this);

            base.OnInit(e);

        }



        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            filePath = (string)rgState[1];
        }



        protected override object SaveControlState()
        {
            object[] rgState = new object[2];
            rgState[0] = base.SaveControlState();
            rgState[1] = filePath;
            return rgState;
        }
	}
}
