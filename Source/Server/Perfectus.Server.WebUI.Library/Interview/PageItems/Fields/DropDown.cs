using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
	/// <summary>
	/// Summary description for TextBox.
	/// </summary>
	internal class DropDown : FieldBase, INamingContainer
	{
		private DropDownList dropDown;
		private int itemToSelect = -2;

        public DropDown(Question q, int repeatNumber, string imagesPath) : base(q, repeatNumber, imagesPath)
		{
		}

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return dropDown.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return dropDown.ID;
            }
        }

		protected override void CreateField()
		{
			dropDown = new DropDownList();
			dropDown.Enabled = Enabled;
			dropDown.ID = "dd";
			if (question != null)
			{
				if (question.Items != null)
				{
/*
					//Javascipt addition for Dynamic text
				    string controlID = string.Format("uId{0}", question.UniqueIdentifier);
					controlID = controlID.Replace("-", "");
					dropDown.Attributes["onBlur"] = string.Format("changeControlText('{0}',{1})",controlID, "this.value");
					// End of Javascipt
*/

					dropDown.DataSource = question.Items;
					dropDown.DataTextField = "Display";
					dropDown.DataValueField = "Value";
					dropDown.DataBind();
				}
				if (question.GetAnswerCount(AnswerProvider.Caller.Interview) > 0)
				{
					object oAnswer = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
					if (oAnswer != null && oAnswer is AnswerCollection)
					{
						AnswerCollection coll = (AnswerCollection) oAnswer;
						if (coll.Count > 0)
						{
							string answer;

                            if (coll[0] is TextQuestionQueryValue)
							{
                                answer = ((TextQuestionQueryValue)coll[0]).Evaluate(AnswerProvider.Caller.Interview, question.StringBindingMetaData);
							}
							else
							{
								answer = coll[0].ToString();
							}

                            //add empty dropdown value case #2702
                            CheckForEmptyValue();

                            //pf-3264 dropdowns in horizontal layout don't have blank value. rebind to fix
                            dropDown.DataBind();

                            ListItem selectThis = dropDown.Items.FindByValue(answer);
                            // FB 1345 
                            if (null == selectThis)
                                foreach (ListItem item in dropDown.Items)
                                    if (item.Value.Trim() == answer)
                                    {
                                        selectThis = item;
                                        break;
                                    }

							for (int i = 0; i < dropDown.Items.Count; i++)
							{
								if (dropDown.Items[i] == selectThis)
								{
									itemToSelect = i;
								}
							}
						}
					}
				}
			}
			Controls.Add(dropDown);
            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
			CreateHelp();
			base.CreateField();
		}

        //checks that we have an empty value, if not add one. If empty value is not valid question needs to be made mandatory.
        private void CheckForEmptyValue()
        {
            bool nullValueFound = false;

            //pf-3102 allow for list to be null, i.e. no items - create the collection and flag the need for a blank row
            if (question.Items == null)
            {
                ItemsItemCollection newCol = new ItemsItemCollection();
                ItemsItem ii = new ItemsItem() { Display = string.Empty, Value = string.Empty };
                newCol.Add(ii);
                question.Items = newCol;
            }

            //add empty dropdown value case #2702
            foreach (ItemsItem itemsItems in question.Items)
            {
                if (itemsItems.Value == string.Empty)
                {
                    nullValueFound = true;
                }
            }
            if (!nullValueFound)
            {
                ItemsItem ii = new ItemsItem();
                ii.Display = string.Empty;
                ii.Value = string.Empty;
                question.Items.Insert(0, ii);
            }

        }

		protected override void CreateChildControls()
		{
			CreateField();
		}


        public override AnswerCollection GetAnswers()
        {
            EnsureChildControls();
			object v = GetBestType(dropDown.SelectedValue);
            return new AnswerCollection(new object[] { v });
        }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (itemToSelect > -2)
			{
				dropDown.ClearSelection();
				dropDown.SelectedIndex = itemToSelect;
			}
		}

	}
}
