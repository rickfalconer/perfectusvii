using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems.Fields
{
    /// <summary>
    /// Summary description for TextBox.
    /// </summary>
    internal class CheckBoxList : FieldBase, INamingContainer
    {
        private System.Web.UI.WebControls.CheckBoxList checkboxList;
        private AnswerCollection toSelect = null;

        public CheckBoxList(Question q, int repeatNumber, string imagesPath)
            : base(q, repeatNumber, imagesPath)
        {
            base.CssClass = "checkboxList";
        }

        public override string InputClientId
        {
            get
            {
                EnsureChildControls();
                return checkboxList.ClientID;
            }
        }

        public override string InputId
        {
            get
            {
                EnsureChildControls();
                return checkboxList.ID;
            }
        }
        protected override void CreateField()
        {

            checkboxList = new System.Web.UI.WebControls.CheckBoxList();
            checkboxList.Enabled = Enabled;
            checkboxList.ID = "cbl";
            if (question != null)
            {
                if (question.Items != null)
                {
/*
                    //Javascipt addition for Dynamic text
                    string controlID = string.Format("uId{0}", question.UniqueIdentifier);
                    controlID = controlID.Replace("-", "");
                    checkboxList.Attributes["onBlur"] = string.Format("changeControlText('{0}',{1})", controlID, "this.value");
                    // End of Javascipt
*/

                    checkboxList.DataSource = question.Items;
                    checkboxList.DataTextField = "Display";
                    checkboxList.DataValueField = "Value";
                    checkboxList.RepeatDirection = (question.Direction == Question.ListDirection.Horizontal) ? RepeatDirection.Horizontal : RepeatDirection.Vertical;
                    checkboxList.DataBind();
                }
                if (question.GetAnswerCount(AnswerProvider.Caller.Interview) > 0)
                {
                    object oAnswer = question.GetAnswer(repeatNumber, AnswerProvider.Caller.Interview);
                    if (oAnswer != null && oAnswer is AnswerCollection)
                    {
                        toSelect = (AnswerCollection)oAnswer;
                    }
                }
            }

            Controls.Add(checkboxList);

            QuestionSuffix qs = new QuestionSuffix(question);
            Controls.Add(qs);
            qs.DataBind();
            CreateHelp();

            StringBuilder sb = new StringBuilder();

            // This javascript updates the value attribute for all input elements in the list, which are used for DHTML changes
            // such as visibility during interview. (See FB 2403)  However in future this would be far better done if the html was 
            // altered in the render stage. (TBDL)
            sb.Append( "<script type='text/javascript'>" );
            for( int i = 0; i < checkboxList.Items.Count; i++ )
            {
                string id = string.Format( "{0}_{1}", checkboxList.ClientID, i );
                sb.Append( "document.getElementById('" );
                sb.Append( id );
                sb.Append( "').value = '" );
                sb.Append( InterviewScriptFactory.EscapeJavascript( checkboxList.Items[ i ].Value ) );
                sb.Append( "';" );
            }
            sb.Append( "</script>" );

            // If the parentPackage property isn't null, we are a full question, not a serialised stub. Therefore, it's the first time we're rendered.
            bool shouldRenderRequiredValidator = false;
            if( ( question.ParentPackage != null ) && ( question.Mandatory != null ) &&
                !( question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo && question.Mandatory.YesNoValue == false ) )
                // Add required validator if we have a mandatory setting.
                shouldRenderRequiredValidator = true;

            //Is there a mandatory query?
            bool hasMandatoryQuery = question.Mandatory != null && question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query;

            // Work out the initial state. This will be set to no if there is a mandatory query but will change 
            // dynamically on script enabled browsers 
            bool mandatory = ( hasMandatoryQuery ) ? false : question.Mandatory.YesNoValue;

            if (shouldRenderRequiredValidator)
            {
                ResourceManager rm = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation");
                string functionId = string.Format("valManCBL_{0}", Guid.NewGuid().ToString().Replace("-", string.Empty));
                string key = checkboxList.ClientID;

                sb.Append("<script language='javascript'>\n");
                sb.Append("function " + functionId + "(oSrc, args)\n{\n");
                sb.Append(string.Format("for(i=0;i<{0};i++)", checkboxList.Items.Count));
                sb.Append("\n	{\n");
                sb.Append("		if(document.getElementById('" + key + "_' + i + '').checked)");
                sb.Append("\n		{args.IsValid = true;return}");
                sb.Append("\n	}\n args.IsValid = false}\n</script>");

                CustomValidator vC = new CustomValidator();
                vC.Enabled = mandatory;
                vC.Visible = true;
                vC.ClientValidationFunction = functionId;
                vC.ErrorMessage = string.Format("<br />{0}", rm.GetString("RequiredFieldValidatorMessage"));
                vC.CssClass = "validator";
                // Add question identifier so that we can track validator client side.
                vC.Attributes.Add( "quid", question.UniqueIdentifier.ToString("N"));
                // Flag that this is a required field validator for use in JS
                vC.Attributes.Add( "required", "yes" );
                Controls.Add( vC );
            }

            Controls.Add(new LiteralControl(sb.ToString()));
        }

        protected override void CreateChildControls()
        {
            CreateField();
        }

        public override AnswerCollection GetAnswers()
        {
            EnsureChildControls();
            AnswerCollection coll = new AnswerCollection();
            foreach (ListItem li in checkboxList.Items)
            {
                if (li.Selected)
                {
                    coll.Add(GetBestType(li.Value));
                }
            }
            return coll;
        }

        protected override void OnDataBinding(EventArgs e)
        {
            if (question != null)
            {
                Controls.Clear();
                ClearChildViewState();

                CreateField();
                switch (question.Direction)
                {
                    case Question.ListDirection.Horizontal:
                        checkboxList.RepeatDirection = RepeatDirection.Horizontal;
                        break;
                    default:
                        checkboxList.RepeatDirection = RepeatDirection.Vertical;
                        break;
                }

                ChildControlsCreated = true;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
			base.OnPreRender(e);

            if( checkboxList != null && toSelect == null || toSelect.Count <= 0 )
                return;

            // Remove all current selections such as the default maybe?
            checkboxList.ClearSelection( );

            // Scan through list of selection objects.
			foreach( object o in toSelect )
			{
			    string answer = string.Empty;

                // Determine the answer in a string format.
                if( o is TextQuestionQueryValue)
                    answer = ((TextQuestionQueryValue)o).Evaluate(AnswerProvider.Caller.Interview, question.StringBindingMetaData);
                else
                    answer = o.ToString( );

                // Ignore any select objects that have no valid answer.
                if( String.IsNullOrEmpty( answer ) )
                    continue;

                ListItem itemToSelect = null;

                // Scan through all items in the list to find a match
                foreach( ListItem item in checkboxList.Items )
                {
                    // Is this item already selected?
                    if( item.Selected )
                        continue;

                    if( this.question != null )
                    {
                        // Perform specific comparisons based on data type of the question...
                        switch( this.question.DataType )
                        {
                            // FB 1610: Date type questions need to perform date type comparisons.
                            case PerfectusDataType.Date:
                                try
                                {
                                    if( DateTime.Compare( Convert.ToDateTime( item.Value ), Convert.ToDateTime( answer ) ) == 0 )
                                        itemToSelect = item;
                                }
                                // Ignore any conversion exceptions.
                                catch { }
                                break;
                            // At the moment there are no other required type specific comparisons. If we later identify some
                            // they should be placed in the switch statement. For example numbers are currently mapped exactly by their 
                            // string representation on the list. ie (1.00 != 1.0)
                            default:
                                break;
                        }
                    }

                    // Perform a straight string comparison if nothing so far.
                    if( itemToSelect == null ) 
                    {
                        // Perform a case sensitive and culture insensitive comparison. As part of case FB1345 we also trim spaces off the strings...
                        if( String.Compare( item.Value.Trim( ), answer.Trim( ), false, System.Globalization.CultureInfo.InvariantCulture ) == 0 )
                            itemToSelect = item;
                    }

                    // If we've found one, no need to continue.
                    if( itemToSelect != null )
                        break;
                }

                // Finally flag the item in the listbox
                if( itemToSelect != null )
                    itemToSelect.Selected = true;
			}
        }
    }
}
