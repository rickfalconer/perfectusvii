using System.Collections;
using Perfectus.Server.WebUI.Library.Interview.PageItems.Fields;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// A collection of elements of type FieldBase
	/// </summary>
	internal class FieldBaseCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FieldBaseCollection class.
		/// </summary>
		public FieldBaseCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the FieldBaseCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new FieldBaseCollection.
		/// </param>
		public FieldBaseCollection(FieldBase[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the FieldBaseCollection class, containing elements
		/// copied from another instance of FieldBaseCollection
		/// </summary>
		/// <param name="items">
		/// The FieldBaseCollection whose elements are to be added to the new FieldBaseCollection.
		/// </param>
		public FieldBaseCollection(FieldBaseCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this FieldBaseCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this FieldBaseCollection.
		/// </param>
		public virtual void AddRange(FieldBase[] items)
		{
			foreach (FieldBase item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another FieldBaseCollection to the end of this FieldBaseCollection.
		/// </summary>
		/// <param name="items">
		/// The FieldBaseCollection whose elements are to be added to the end of this FieldBaseCollection.
		/// </param>
		public virtual void AddRange(FieldBaseCollection items)
		{
			foreach (FieldBase item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type FieldBase to the end of this FieldBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The FieldBase to be added to the end of this FieldBaseCollection.
		/// </param>
		public virtual void Add(FieldBase value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic FieldBase value is in this FieldBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The FieldBase value to locate in this FieldBaseCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this FieldBaseCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(FieldBase value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this FieldBaseCollection
		/// </summary>
		/// <param name="value">
		/// The FieldBase value to locate in the FieldBaseCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(FieldBase value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the FieldBaseCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the FieldBase is to be inserted.
		/// </param>
		/// <param name="value">
		/// The FieldBase to insert.
		/// </param>
		public virtual void Insert(int index, FieldBase value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the FieldBase at the given index in this FieldBaseCollection.
		/// </summary>
		public virtual FieldBase this[int index]
		{
			get { return (FieldBase) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific FieldBase from this FieldBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The FieldBase value to remove from this FieldBaseCollection.
		/// </param>
		public virtual void Remove(FieldBase value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by FieldBaseCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(FieldBaseCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public FieldBase Current
			{
				get { return (FieldBase) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (FieldBase) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this FieldBaseCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}