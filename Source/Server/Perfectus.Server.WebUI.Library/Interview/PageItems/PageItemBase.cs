using System;
using System.Web.UI.WebControls;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// Summary description for PageItemBase.
	/// </summary>
	internal abstract class PageItemBase : WebControl
	{
	    public event EventHandler SaveOnPostbackRequested;

        protected void OnSaveOnPostbackRequested()
		{
            if (SaveOnPostbackRequested != null)
            {                
                SaveOnPostbackRequested(this, EventArgs.Empty);
		}
        }		
	}
}
