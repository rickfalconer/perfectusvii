using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.Remoting;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.WebUI.Library.Interview.PageItems.Fields;
using Calendar = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.Calendar;
using CheckBox = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.CheckBox;
using CheckBoxList = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.CheckBoxList;
using ListBox = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.ListBox;
using TextBox = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.TextBox;
using AnswerSetUpload = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.AnswerSetUpload;
using FileUpload = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.FileUpload;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// Summary description for QuestionItem.
	/// </summary>
	internal class QuestionField : PageItemBase, INamingContainer
	{
		private Question question;
		private string scriptPath;
		private FieldBase fieldControl;
		private int repeatNumber;
		private EventHandler callButtonClickHandler;
		private EventHandler loadAnswerSetHandler;
		private string imagesPath;
		private Guid instanceId;
		private DisplayTypeExtensionInfoDictionary extensionMappings;
		private bool enabled;
		private bool isPagePreview;
	    private bool inRepeater;

		public Guid QuestionId
		{
			get
			{
				if (question != null)
				{
					return question.UniqueIdentifier;
				}
				else
				{
					return Guid.Empty;
				}
			}
		}

		public FieldBase FieldControl
		{
			get
			{
				EnsureChildControls();
				return fieldControl;
			}
		}

		private QuestionField()
		{
		}

        public QuestionField(Question question, string scriptPath, string imagesPath, int repeatNumber, EventHandler callButtonClickHandler, Guid instanceId, DisplayTypeExtensionInfoDictionary extensionMappings, EventHandler loadAnswerSetHandler, bool isPreview, bool inRepeater)
		{
            this.inRepeater = inRepeater;
			this.question = question;
			this.scriptPath = scriptPath;
			this.repeatNumber = repeatNumber;
            this.callButtonClickHandler = callButtonClickHandler;
            this.loadAnswerSetHandler = loadAnswerSetHandler;
			this.instanceId = instanceId;
			this.imagesPath = imagesPath;
			isPagePreview = isPreview;

			if (question.ParentPackage != null)
			{
				enabled = question.Enabled.Evaluate(true, AnswerProvider.Caller.System);
			}
			else
			{
				enabled = question.Enabled.YesNoValue;
			}
		
			this.extensionMappings = extensionMappings;
		}

		public string GetChildId()
		{
			EnsureChildControls();
			if (fieldControl != null)
			{
				return fieldControl.UniqueID;
			}
			else
			{
				return null;
			}
		}

        public AnswerCollection GetAnswers()
		{
			EnsureChildControls();
			return fieldControl.GetAnswers();
		}

		protected override void CreateChildControls()
		{
			CreateField();
		}

		private void CreateField()
		{
            QuestionPrefix qp = new QuestionPrefix(question);
            Controls.Add(qp);
		    qp.DataBind();
			
			fieldControl = CreateField(question.DisplayType, question, imagesPath);
			fieldControl.ID = "f";
			fieldControl.Enabled = enabled;
			Controls.Add(fieldControl);

			if (question.ServiceCallButtonText != null && question.ServiceCallButtonText.Trim().Length > 0)
			{
				Button callButton = new Button();

                callButton.CausesValidation = false;

				callButton.Text = question.ServiceCallButtonText;

				if(! isPagePreview)
				{
					callButton.Click += new EventHandler(callButton_Click);
				}
                else 
                    callButton.Enabled = false;

				Controls.Add(callButton);

			}
			if (question.DataType == PerfectusDataType.AnswerSet)
			{
				Button answerSetButton = new Button();

                answerSetButton.CausesValidation = false;

				answerSetButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("AnswerSetUploadButton");

                if (!isPagePreview)
                {
                    answerSetButton.Click += new EventHandler(loadAnswerSet_Click);
                }
                else
                    answerSetButton.Enabled = false;

				Controls.Add(answerSetButton);

			}
			Attributes.Add("answers", question.UniqueIdentifier.ToString("N"));
            Attributes.Add("fieldid", fieldControl.InputClientId);
            if (inRepeater)
            {
                Attributes.Add("inRepeater", "true");
                Attributes.Add("repeat", repeatNumber.ToString());
            }

            if ((question.DisplayType == QuestionDisplayType.TextBox || question.DisplayType == QuestionDisplayType.TextArea) && question.InterviewFormatString != null && question.InterviewFormatString.Trim().Length > 0)
            {
                Attributes.Add("fs", question.InterviewFormatString);
                if (question.InterviewFormatStringParams != null && question.InterviewFormatStringParams.Trim().Length > 0)
                {
                    Attributes.Add("fp", question.InterviewFormatStringParams);
                }
            }

			if(question.DefaultAnswer != null)
			{
				string defaultAnswerXml = question.DefaultAnswer.EvaluateAndKeepXml();
				if(defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
				{
					// javascript is wanting no "-" in the guids.
					defaultAnswerXml = defaultAnswerXml.Replace("-","");
					Attributes.Add("defaultAnswersXml", defaultAnswerXml);
				}
			}

		    string pdt;
			switch (question.DataType)
			{
				case PerfectusDataType.Date:
					pdt = "date";
					break;
				case PerfectusDataType.YesNo:
					pdt = "yesno";
					break;
				case PerfectusDataType.File:
					pdt = "file";
					break;
				case PerfectusDataType.Number:
					pdt = "number";
					break;
				case PerfectusDataType.AnswerSet:
					pdt = "file";
					break;
				default:
				case PerfectusDataType.Text:
					pdt = "text";
					break;
			}
			Attributes.Add("pdt", pdt);
		}

		protected override void OnDataBinding(EventArgs e)
		{
			if (question != null)
			{
				Controls.Clear();
				ClearChildViewState();

				CreateField();

				ChildControlsCreated = true;
			}
		}


        private FieldBase CreateField(QuestionDisplayType displayType, Question q, string sImagesPath)
        {
            // WorkAround for SharePoint choice picker

            // The old implementation created either a standard field (type FieldBase), or a plugin
            // returned such an object. Therefore by design a plugin does provide the 'answer'.
            // THe actual problem: A SharePoint choice column can have different values. 
            // Other than that it is just a standard (let's say) radio button question. The old design requires 
            // that a plugin implements the entire question. 
            // Solution:
            // Call the plugin and pass the question. If the type returns unmodified, then use the old
            // behavior. Otherwise (maybe) create a standard question control using the modfied question.
            
            if ((int)displayType >= 1000)
            {
                string key = null;
                if (extensionMappings != null)
                {
                    DisplayTypeExtensionInfo dtei;
                    dtei = extensionMappings[displayType];
                    if (dtei != null)
                    {
                        key = dtei.ServerSideDisplayPluginId.ToLower(CultureInfo.InvariantCulture);

                        if (key.Contains("#;"))
                            key = key.Substring(0, key.LastIndexOf("#;"));
                    }
                }

                bool found = false;
                if (key != null)
                {
                    foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
                    {
                        if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "displaytypeextension" && plugin.path != null && plugin.path.Trim().Length > 0 && plugin.key.ToLower(CultureInfo.InvariantCulture) == key)
                        {
                            ObjectHandle h = Activator.CreateInstanceFrom(plugin.path, plugin.typeName, true, BindingFlags.CreateInstance, null, new object[] { q, repeatNumber, sImagesPath }, System.Threading.Thread.CurrentThread.CurrentUICulture, null, null);
                            object o = h.Unwrap();


                            if (o != null && o is FieldBase &&
                                q.DisplayType == displayType ||
                                (int)q.DisplayType >= 1000)
                            {
                                return (FieldBase)o;
                            }
                            else
                            {
                                found = true;
                                displayType = q.DisplayType;
                                break;
                            }
                        }
                    }
                }
                if ( ! found)
                throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("PluginCouldNotBeLoaded"), (int)displayType));
            }

            // NO ELSE - Siedeeffect is wanted

            switch (displayType)
            {
                case QuestionDisplayType.Password:
                case QuestionDisplayType.TextArea:
                case QuestionDisplayType.TextBox:
                    return new TextBox(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.CheckBox:
                    return new CheckBox(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.Calendar:
                    return new Calendar(q, scriptPath, repeatNumber, sImagesPath);
                case QuestionDisplayType.DropDownList:
                    return new DropDown(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.RadioList:
                    return new RadioList(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.CheckBoxList:
                    return new CheckBoxList(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.ListBox:
                case QuestionDisplayType.MultiSelectListBox:
                    return new ListBox(q, repeatNumber, sImagesPath);
                case QuestionDisplayType.FilePicker:
                    return new FileUpload(q, repeatNumber, instanceId, sImagesPath);
                case QuestionDisplayType.AnswerSetPicker:
                    return new AnswerSetUpload(q, repeatNumber, instanceId, sImagesPath);
                default:
                    return new TextBox(q, repeatNumber, sImagesPath);
            }
        }

		private void callButton_Click(object sender, EventArgs e)
		{
			OnCallButtonClick(e);
		}
		private void loadAnswerSet_Click(object sender, EventArgs e)
		{
			OnLoadAnswerSetClick(e);

		}

		protected void OnCallButtonClick(EventArgs e)
		{
			if (callButtonClickHandler != null)
			{
				callButtonClickHandler(this, e);
			}
		}
		protected void OnLoadAnswerSetClick(EventArgs e)
		{
			if (loadAnswerSetHandler != null)
			{
				loadAnswerSetHandler(this, e);
			}
		}
	}
}
