using System;
using System.Web.UI;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// Summary description for HtmlBlock.
	/// </summary>
	internal class HtmlBlockItem : PageItemBase
	{
		private HtmlBlock block = null;
		private Package parentPackage = null;
        private StringBindingMetaData stringBindingMetaData;
	    private bool inRepeater;
	    private int repeatNumber;
		private HtmlBlockItem()
		{
		}

        public HtmlBlockItem(HtmlBlock block, Package package, StringBindingMetaData stringBindingMetaData, int repeatNumber, bool inRepeater)
		{
			this.block = block;
			parentPackage = package;
            this.stringBindingMetaData = stringBindingMetaData;
            this.inRepeater = inRepeater;
            this.repeatNumber = repeatNumber;
		}

        protected override void OnPreRender(EventArgs e)
        {
            DataBind();
            base.OnPreRender(e);
        }

		protected override void OnDataBinding(EventArgs e)
		{
			if (block != null)
			{
				Controls.Clear();
				ClearChildViewState();
                string strHtml = InterviewStringBinding.GetClientHtml(block.Html, parentPackage, stringBindingMetaData, repeatNumber, inRepeater);
                LiteralControl htmlArea = new LiteralControl(strHtml);
                Controls.Add(htmlArea);
				ChildControlsCreated = true;
			}
		}
	}
}
