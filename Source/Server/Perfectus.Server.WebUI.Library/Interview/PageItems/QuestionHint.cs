using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
    /// <summary>
    /// Summary description for QuestionPrompt.
    /// </summary>
    internal class QuestionHint : Label
    {
        private Question question = null;

        private QuestionHint()
        {
        }

        public QuestionHint(Question q)
        {
            question = q;
            base.CssClass = "questionHint";

            if (question != null)
            {
                string strHint = question.Hint.EvaluateAndKeepXml();
                Text = InterviewStringBinding.GetClientHtml(strHint, question.ParentPackage, question.StringBindingMetaData, 0, false);
            }
        }
    }
}
