using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
    /// <summary>
    /// Summary description for QuestionPrompt.
    /// </summary>
    internal class QuestionPrompt : Label
    {
        private Question question = null;

        private QuestionPrompt()
        {
        }

        public QuestionPrompt(Question q)
        {
            question = q;
            base.CssClass = "questionPrompt";

            if (question != null)
            {
                string strPrompt = question.Prompt.EvaluateAndKeepXml();
                Text = InterviewStringBinding.GetClientHtml(strPrompt, question.ParentPackage, question.StringBindingMetaData, 0, false);
                Attributes.Add("promptofquestion", q.UniqueIdentifier.ToString().Replace("-",""));
            }
        }
    }
}
