using System.Collections;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// A collection of elements of type QuestionItem
	/// </summary>
	internal class QuestionFieldCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the QuestionItemCollection class.
		/// </summary>
		public QuestionFieldCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the QuestionItemCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new QuestionItemCollection.
		/// </param>
		public QuestionFieldCollection(QuestionField[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the QuestionItemCollection class, containing elements
		/// copied from another instance of QuestionItemCollection
		/// </summary>
		/// <param name="items">
		/// The QuestionItemCollection whose elements are to be added to the new QuestionItemCollection.
		/// </param>
		public QuestionFieldCollection(QuestionFieldCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this QuestionItemCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this QuestionItemCollection.
		/// </param>
		public virtual void AddRange(QuestionField[] items)
		{
			foreach (QuestionField item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another QuestionItemCollection to the end of this QuestionItemCollection.
		/// </summary>
		/// <param name="items">
		/// The QuestionItemCollection whose elements are to be added to the end of this QuestionItemCollection.
		/// </param>
		public virtual void AddRange(QuestionFieldCollection items)
		{
			foreach (QuestionField item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type QuestionItem to the end of this QuestionItemCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionItem to be added to the end of this QuestionItemCollection.
		/// </param>
		public virtual void Add(QuestionField value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic QuestionItem value is in this QuestionItemCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionItem value to locate in this QuestionItemCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this QuestionItemCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(QuestionField value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this QuestionItemCollection
		/// </summary>
		/// <param name="value">
		/// The QuestionItem value to locate in the QuestionItemCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(QuestionField value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the QuestionItemCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the QuestionItem is to be inserted.
		/// </param>
		/// <param name="value">
		/// The QuestionItem to insert.
		/// </param>
		public virtual void Insert(int index, QuestionField value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the QuestionItem at the given index in this QuestionItemCollection.
		/// </summary>
		public virtual QuestionField this[int index]
		{
			get { return (QuestionField) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific QuestionItem from this QuestionItemCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionItem value to remove from this QuestionItemCollection.
		/// </param>
		public virtual void Remove(QuestionField value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by QuestionItemCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(QuestionFieldCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public QuestionField Current
			{
				get { return (QuestionField) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (QuestionField) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this QuestionItemCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}