using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
    /// <summary>
    /// Summary description for QuestionExample.
    /// </summary>
    internal class QuestionExample : Label
    {
        private Question question = null;

        private QuestionExample()
        {
        }

        public QuestionExample(Question q)
        {
            question = q;
            base.CssClass = "questionExample";

            if (question != null)
            {
                string strExample = question.Example.EvaluateAndKeepXml();
                Text = InterviewStringBinding.GetClientHtml(strExample, question.ParentPackage, question.StringBindingMetaData, 0, false);
            }
        }
    }
}
