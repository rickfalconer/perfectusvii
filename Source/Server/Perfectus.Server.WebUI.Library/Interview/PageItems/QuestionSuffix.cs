using System.Web.UI.WebControls;
using Perfectus.Common.PackageObjects;


namespace Perfectus.Server.WebUI.Library.Interview.PageItems
{
	/// <summary>
	/// Summary description for QuestionSuffix.
	/// </summary>
	public class QuestionSuffix : Label
	{
		private Question question = null;

		private QuestionSuffix()
		{
		}

        public QuestionSuffix(Question q)
        {
            question = q;
			base.CssClass = "questionSuffix";
		}

		public override void DataBind()
		{
			if (question != null)
			{
				string strSuffix = question.Suffix.EvaluateAndKeepXml();
                Text = InterviewStringBinding.GetClientHtml(strSuffix, question.ParentPackage, question.StringBindingMetaData, 0, false);	
			}
		}
	}
}

