using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Perfectus.Server.AnswerAcquirer.AnswerInstance;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.PluginKit;
using Perfectus.Server.WebUI.Library.Interview.PageItems;
using Perfectus.Server.WebUI.Library.Interview.PageItems.Fields;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Server.WebUI.Library.Preview;
using FileUpload = Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.FileUpload;

namespace Perfectus.Server.WebUI.Library.Interview
{
    [DefaultProperty("packageInstanceId")]
    [ToolboxData("<{0}:Interview runat=server></{0}:Interview>")]
    public class Interview : WebControl, INamingContainer
    {
        #region Class Variables Properties and Contructor

        public enum Direction
        {
            Next,
            Previous,
            Unknown
        }

        // Core child controls
        WebControl historyControl;
        WebControl interviewPageControl;
        WebControl topMenu;
        WebControl ErrorMessage;
        Label ErrorTitle;
        Label ErrorText;
        WebControl buttonBar;

        // The hidden control that contains disabled validations client side.
        HiddenField validationDHTMLState;

        // The hidden control that contains the view mode responsive state - Desktop / Device
        HiddenField viewModeState;
        
        LiteralControl pageTitleLabel;
        Table interviewPageTable;
        Table historyPanelTable;
        TableCell historyCell;

        private bool showHeader = true;
        private bool showFooter = true;
        private bool showProgress = true;
        private string answerSetDownloadPath;
        private Guid pageId = Guid.Empty;
        private Package instance = null;
        private InterviewPage page = null;
        private InterviewPage previewPage = null;
        private QuestionFieldCollection questionFieldControls = new QuestionFieldCollection();
        private EventHandler callButtonEventHandler = null;
        private EventHandler loadAnswerSetHandler = null;
        private bool answersApplied = false;
        private string imagesPath = null;
        private string javascriptPath = null;
        private string previewRenderPagePath = null;
        private string previewCssPath = null;
        private string interviewPageClientId;
        private string previewId;
        private bool willCallService = false;
        private bool showMessageBar = false;
        private IInterviewSaver saverPlugin = null;
        private IInterviewSuspender suspenderPlugin = null;
        private MessageBar messageBar;
        private int numRepeaters;
        private bool suspendExternalRedirect = false;
        private const string LEFTNAVCELL = "LEFTNAVCELL";
        private const string CENTRENAVCELL = "CENTRENAVCELL";
        private const string RIGHTNAVCELL = "RIGHTNAVCELL";
        private int savesRequested = 0;
        private SharedLibraryProxy mRepository = new SharedLibraryProxy();
        private Guid packageInstanceId;
        private string[] itemTypes = new string[0];
        private string[] itemIds = new string[0];
        private bool showSaveButtons = false;
        private bool showSuspendButton = false;
        private bool showHistoryButton = false;
        private bool isPagePreview = false;
        private bool showPreviewButton = false;
        // responsive
        private bool showViewModeButton = false;
        private string responsive = string.Empty;

        private string pageTitle = string.Empty;
        private bool isDefinitelyLastPage = false;
        private Question[] questionStubs = new Question[0];
        private InternalPage.ListPreviewStyle previewStyle = InternalPage.ListPreviewStyle.HTML;
        private DisplayTypeExtensionInfoDictionary extensionMappings = new DisplayTypeExtensionInfoDictionary();
        private string vars = string.Empty;
        private string instanceName = string.Empty;
        private StackEntry[] stack = new StackEntry[0];
        private bool showFF = false;

        private string interviewClientId;
        private string validationDHTMLStateId;
        private string viewModeStateId;

        // Binding & creation flags
        private bool InterviewPageCreated = false;
        private bool hasBound = false;
        private bool forceRefresh = false;

        private LinkButton saveToButton;
        private LinkButton suspendButton;
        private LinkButton fastForwardButton;
        private LinkButton nextOrFinishButton;
        private LinkButton previousButton;
        private HtmlAnchor saveToLocalAnchor;
        private HtmlAnchor historyAnchor;
        private HtmlAnchor previewAnchor;
        private HtmlAnchor viewModeAnchor;
        private LinkButton viewModeButton;

        public event EventHandler<InterviewFinishedEventArgs> Finished;

        [Browsable(false)]
        public string SaveToButtonClientId
        {
            get { return saveToButton == null ? null : saveToButton.ClientID; }
        }

        [Browsable(false)]
        public string SuspendButtonClientId
        {
            get { return suspendButton == null ? null : suspendButton.ClientID; }
        }

        [Browsable(false)]
        public string SaveToLocalAnchorClientId
        {
            get { return saveToLocalAnchor == null ? null : saveToLocalAnchor.ClientID; }
        }

        [Browsable(false)]
        public string ViewModeAnchorClientId
        {
            get { return viewModeAnchor == null ? null : viewModeAnchor.ClientID; }
        }

        [Browsable(false)]
        public string ViewModeButtonClientId
        {
            get { return viewModeButton == null ? null : viewModeButton.ClientID; }
        }

        [Browsable(false)]
        public string HistoryAnchorClientId
        {
            get { return historyAnchor == null ? null : historyAnchor.ClientID; }
        }

        [Browsable(false)]
        public string PreviewAnchorClientId
        {
            get { return previewAnchor == null ? null : previewAnchor.ClientID; }
        }

        [Browsable(false)]
        public string FastForwardButtonClientId
        {
            get { return fastForwardButton == null ? null : fastForwardButton.ClientID; }
        }

        [Browsable(false)]
        public string NextOrFinishButtonClientId
        {
            get { return nextOrFinishButton == null ? null : nextOrFinishButton.ClientID; }
        }

        [Browsable(false)]
        public string PreviousButtonClientId
        {
            get { return previousButton == null ? null : previousButton.ClientID; }
        }

        [Browsable(true)]
        public bool ShowHeader
        {
            get { return showHeader; }
            set { showHeader = value; }
        }

        [Browsable(true)]
        public bool ShowFooter
        {
            get { return showFooter; }
            set { showFooter = value; }
        }

        public bool ShowProgress
        {
            get { return showProgress; }
            set { showProgress = value; }
        }

        [Browsable(false)]
        public Package Instance
        {
            get { return instance; }
        }

        [Browsable(true)]
        public string PreviewRenderPagePath
        {
            get { return previewRenderPagePath; }
            set { previewRenderPagePath = value; }
        }

        [Browsable(true)]
        public string JavascriptPath
        {
            get { return javascriptPath; }
            set { javascriptPath = value; }
        }

        [Browsable(true)]
        public string ImagesPath
        {
            get { return imagesPath; }
            set { imagesPath = value; }
        }

        [Browsable(true)]
        public string PreviewCssPath
        {
            get { return previewCssPath; }
            set { previewCssPath = value; }
        }

        [Browsable(true)]
        public string AnswerSetDownloadPath
        {
            get { return answerSetDownloadPath; }
            set { answerSetDownloadPath = value; }
        }

        // This represents the page to be display as passed to us from the IPManager client.
        public InterviewPage PreviewPage
        {
            get { return previewPage; }
            set { previewPage = value; }
        }

        // Is this control hosted by a IPManager client preview page? FB2169
        public bool IsPagePreview
        {
            get { return isPagePreview; }
            set { isPagePreview = value; }
        }

        // The hidden control that contains disabled validations client side.
        public HiddenField ValidationDHTMLState
        {
            get { return validationDHTMLState; }
        }

        // The hidden control that contains responsive view mode state from client side.
        public HiddenField ViewModeState
        {
            get { return viewModeState; }
        }
        // Interview page uses cookie to track these
        public bool ShowViewModeButton
        {
            get { return showViewModeButton; }
            set { showViewModeButton = value; }
        }
        public string Responsive
        {
            get { return responsive; }
            set { responsive = value; }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        public Guid PackageInstanceId
        {
            get
            {
                return packageInstanceId;
            }
            set
            {
                packageInstanceId = value;
            }
        }

        public double ProgressPercent
        {
            get
            {
                if (page != null)
                {
                    isDefinitelyLastPage = (page.NextPage == null || page.NextPage.PageValue == null && page.NextPage.QueryValue == null);
                    if (page.ParentInterview.ProgressMax == -1)
                    {
                        page.ParentInterview.CalculatePageRanks();
                    }
                    double progress = page.Progress;
                    double total = page.ParentInterview.ProgressMax;
                    return (progress / total) * 100;
                }
                else
                {
                    return 0;
                }
            }
        }

        // Set before DataBind() if an External Redirect should not occur.
        public bool SuspendExternalRedirect
        {
            get
            {
                return suspendExternalRedirect;
            }
            set
            {
                suspendExternalRedirect = value;
            }
        }

        [Browsable(false)]
        public bool ForceRefresh
        {
            get { return forceRefresh; }
            set { forceRefresh = value; }
        }

        public Interview()
            : base(HtmlTextWriterTag.Div)
        {
            this.EnableViewState = false;
            callButtonEventHandler = new EventHandler(CallButton_Click);
            loadAnswerSetHandler = new EventHandler(LoadAnswerSet_Click);
        }

        #endregion
        #region ASP.Net Control LifeCycle Methods

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // If an IPManager client preview page, we can only load the control data once we have the preview page from the client. FB2169
            if (isPagePreview && PreviewPage == null)
                return;

            if (!hasBound && Visible)
            {
                SetInterviewPageData();
            }

            if (!InterviewPageCreated)
            {
                EnsureChildControls();
                DataBind();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            // Hidden control to track the dhtml state of view mode
            viewModeState = new HiddenField();
            viewModeState.ID = "viewModeState";
            viewModeStateId = viewModeState.ClientID;

            GetUserConfigAndPrefs();

            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        protected override void CreateChildControls()
        {
            if (ChildControlsCreated) // Exit if already created
            {
                return;
            }

            Page.Trace.Write("Perfectus", "Begin Interview.CreateChildControls");
            CreateSelf();
            Page.Trace.Write("Perfectus", "End Interview.CreateChildControls");
        }

        protected override void LoadControlState(object savedState)
        {
            object[] rgState = (object[])savedState;
            base.LoadControlState(rgState[0]);
            pageId = (Guid)rgState[1];
            packageInstanceId = (Guid)rgState[2];
            showFF = (bool)rgState[3];
            // only load if user actually choose it
            if (showViewModeButton)
                this.responsive = (string)rgState[4];
        }

        protected override object SaveControlState()
        {
            object[] rgState = new object[5];
            rgState[0] = base.SaveControlState();
            rgState[1] = pageId;
            rgState[2] = packageInstanceId;
            rgState[3] = showFF;
            rgState[4] = this.responsive;
            return rgState;
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            EnsureChildControls();

            // TODO. Sort out Server validation issues. This is temp fix to stop validation firing before next hit. 
            // Validation not working on firefox
            if( Page.IsPostBack && !answersApplied )
            {
                Page.Validate( );
            }

            // If the postback didn't trigger a save, and we have file upload controls, we have to do a save, as they do not survive a second postback.
            if (!isPagePreview && Page.IsPostBack && !answersApplied && Page.IsValid)
            {
                foreach (QuestionField qf in questionFieldControls)
                {
                    if (qf.FieldControl is FileUpload)
                    {
                        if (instance == null)
                        {
                            // Get instance
                            Page.Trace.Write("Perfectus", "Begin retrieve instance in OnPreRender");
                            instance = InstanceManagement.GetInstance(packageInstanceId);
                            Page.Trace.Write("Perfectus", "End retrieve instance in OnPreRender");
                        }

                        // Get this page from the instance
                        InterviewPage thisPage = GetThisPage(instance);

                        // Apply answers
                        if (!answersApplied)
                        {
                            ApplyAnswersFromControls(thisPage);
                        }

                        // Save instance
                        Page.Trace.Write("Perfectus", "Begin save instance in OnPreRender");
                        InstanceManagement.SaveInstance(packageInstanceId, instance);
                        Page.Trace.Write("Perfectus", "End save instance in OnPreRender");

                        break;
                    }
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //this script needs to be registered first as the scripts below utilise it
            Page.ClientScript.RegisterStartupScript(GetType(), "CultureSpecificNumberScript", InterviewScriptFactory.GenerateCulturesNumberValidScript());

            string questionArray = GetQuestionArrayScript();

            StringBuilder sb = new StringBuilder();
            sb.Append(questionArray);

#if DEBUG
            sb.Append(string.Format(@"<script type=""text/javascript"" src=""{0}/perfectus.js?{1:N}""></script>", javascriptPath,
                  Guid.NewGuid()));
#else
			    sb.Append(string.Format(@"<script type=""text/javascript"" src=""{0}/perfectus.js""></script>", javascriptPath));
#endif

            sb.Append(@"<script type=""text/javascript"">");
            sb.AppendFormat("var perfectus_instanceId = '{0:N}';", packageInstanceId);
            sb.Append("var dateOrder = '");
            sb.Append(GetDateOrder());
            sb.Append("';\n");
            sb.Append(string.Format("var timeNow=new Date('{0} 00:00:00').getTime();\n", DateTime.Now.ToShortDateString()));
            sb.Append(string.Format("var pageIsPostBack = {0};\n", this.Page.IsPostBack.ToString().ToLower()));
            sb.Append("BindChangeEvents(questionControls);");
            sb.Append("</script>");

            if (!isPagePreview)
            {
                sb.Append(vars);
            }

            Page.ClientScript.RegisterStartupScript(GetType(), "qevents", sb.ToString());

            base.Render(writer);
        }

        #endregion
        #region Interview Page Creation Methods

        private void GetUserConfigAndPrefs()
        {
            // get defaults from config
            string configViewMode = ConfigurationSystem.Config.Shared.interview.interviewResponsive;
            if (configViewMode == null || configViewMode == string.Empty || configViewMode.ToLower() == "false")
            {
                ShowViewModeButton = false;
                Responsive = "Desktop";
            }
            else if (configViewMode.ToLower() == "true")
            {
                ShowViewModeButton = false;
                Responsive = "Device";
            }
            else if (configViewMode.ToLower() == "user")
            {
                ShowViewModeButton = true;
                Responsive = "Desktop";
            }
        }

        private void CreateSelf()
        {
            Page.Trace.Write("Perfectus", "Begin Interview.CreateSelf");

            interviewClientId = this.ClientID;

            // Hidden control to track the dhtml status of validation controls
            validationDHTMLState = new HiddenField( );
            validationDHTMLState.ID = "validationDHTMLState";
            validationDHTMLStateId = validationDHTMLState.ClientID;

            SetInterviewPageData();
            CreateInterviewPage();
            CreateTopMenu(instanceName, out historyAnchor, out previewAnchor, out saveToButton, out suspendButton, out saveToLocalAnchor, out viewModeAnchor);
            Controls.Add(topMenu);

            CreateNavigationButtonBar();

            Preview.Preview preview = new Preview.Preview();
            preview.ShowTemplateDropDown = true;
            preview.HistoryJump += new EventHandler<HistoryJumpEventArgs>(preview_HistoryJump);

            ErrorMessage = CreateErrorMessage();
            ErrorMessage.Visible = false;
            WebControl webServiceMsg = CreateServiceMessage();
            webServiceMsg.Visible = false;

            messageBar = new MessageBar();
            messageBar.Visible = showMessageBar;

            if (page != null)
            {
                BindingSystem binder = new BindingSystem(instance);
                willCallService = binder.WillCallServices(page);
            }

            if (page != null && page.Seen && willCallService)
            {
                //pf-3175 refresh services - yes, no, ask

                //pf-3269 external page must not be affected by the internal page refresh logic
                bool bAskRefreshServices = false;
                if (page.GetType() == typeof(InternalPage))
                {
                    bAskRefreshServices = (((InternalPage)page).RefreshServices == InternalPage.ListRefreshServices.Ask);
                }
                if (bAskRefreshServices)
                {
                    webServiceMsg.Visible = true;
                }
            }

            preview.PackageInstanceId = packageInstanceId;
            preview.JavascriptPath = javascriptPath;
            preview.RenderPagePath = PreviewRenderPagePath;
            preview.CssPath = previewCssPath;
            preview.CssClass = "preview";
            preview.PreviewStyle = previewStyle;    // Sign previewstyle value 
            preview.ID = "pv";
            preview.Style.Add("display", "none");
            preview.PreviewRequested += new EventHandler(preview_PreviewRequested);

            LibraryControl verControl = new LibraryControl("p");
            string ver = ((AssemblyInformationalVersionAttribute)(AssemblyInformationalVersionAttribute.GetCustomAttribute(Assembly.GetExecutingAssembly(), typeof(AssemblyInformationalVersionAttribute)))).InformationalVersion;
            verControl.Controls.Add(new LiteralControl(ver));
            verControl.CssClass = "versionNumber";

            Controls.Add(ErrorMessage);
            Controls.Add(webServiceMsg);
            Controls.Add(messageBar);
            Controls.Add(interviewPageControl);
            Controls.Add(preview);
            Controls.Add(buttonBar);
            Controls.Add(verControl);
            Controls.Add(validationDHTMLState);
            Controls.Add(viewModeState);

            previewId = preview.ClientID;
            interviewPageClientId = interviewPageControl.ClientID;

            // Ensure the ID's are set to the client id
            validationDHTMLStateId = validationDHTMLState.ClientID;
            viewModeStateId = ViewModeState.ClientID;

            if (historyAnchor != null)
            {
                string onClickValue = string.Format("toggleDisplay(document.getElementById('{0}')); hideDisplay(document.getElementById('{1}'));showDisplay(document.getElementById('{2}'));", historyControl.ClientID, preview.ClientID, interviewPageControl.ClientID);
                historyAnchor.Attributes.Add("onclick", onClickValue);
            }

            if (previewAnchor != null)
            {
                // Preview link text to be shown depending on current toggle state 
                string previewText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Preview");
                string interviewText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Interview");
                string onClickValue = string.Format("toggleDisplay(document.getElementById('{0}')); toggleDisplay(document.getElementById('{1}')); togglePreviewAnchorText(document.getElementById('{2}'), '{3}', '{4}');", previewId, interviewPageControl.ClientID, previewAnchor.ClientID, previewText, interviewText);
                previewAnchor.Attributes.Add("onclick", onClickValue);
            }

            if (viewModeAnchor != null)
            {
                string onClickValue = string.Format("togglePreviewAnchorText(document.getElementById('{0}'), '{1}', '{2}'); document.getElementById('{3}').value = document.getElementById('{0}').innerHTML; __doPostBack('iui$viewModeButton',document.getElementById('iui_viewModeState').value); ", viewModeAnchor.ClientID, "Desktop", "Device", ViewModeState.ClientID);
                viewModeAnchor.Attributes.Add("onclick", onClickValue);
            }

            ChildControlsCreated = true;
            Page.Trace.Write("Perfectus", "End Interview.CreateSelf");
        }

        private void CreateInterviewPage()
        {
            interviewPageControl = new WebControl(HtmlTextWriterTag.Div);
            interviewPageControl.ID = "pg";
            interviewPageControl.CssClass = "interviewPage";

            // Set the interview height from a config, default to 500px for compatibility with legacy
            string sHeight = ConfigurationSystem.Config.Shared.interview.interviewHeight;
            if (sHeight == null || sHeight == string.Empty)
                sHeight = "500px";
            interviewPageControl.Style.Add("height", sHeight);

            Table interviewTable = new Table();
            interviewTable.ID = "mt";
            interviewTable.CellPadding = 0;
            interviewTable.CellSpacing = 0;
            interviewTable.Attributes.Add("class", "main");

            TableRow titleRow = new TableRow();
            titleRow.ID = "title";
            historyCell = new TableCell();
            historyCell.ID = "hist";
            historyCell.CssClass = "historyCell";
            titleRow.Cells.Add(historyCell);

            CreateHistory(historyCell);

            TableCell titleCell = new TableCell();
            titleCell.ColumnSpan = 2;
            titleRow.Cells.Add(titleCell);
            pageTitleLabel = new LiteralControl();
            titleCell.Controls.Add(pageTitleLabel);
            titleCell.CssClass = "title";
            interviewTable.Rows.Add(titleRow);

            // Padding Row
            TableRow paddingRow = new TableRow();
            TableCell paddingCell = new TableCell();
            paddingCell.Controls.Add(new LiteralControl("&nbsp;"));
            paddingCell.ColumnSpan = 2;
            paddingRow.CssClass = "paddingRow";
            interviewTable.Rows.Add(paddingRow);
            paddingRow.Cells.Add(paddingCell);

            // Interview data row (will contain a nested table)
            interviewPageTable = new Table();
            interviewPageTable.ID = "interviewPageTable";
            interviewPageTable.CellPadding = 0;
            interviewPageTable.CellSpacing = 0;
            TableRow interviewPageRow = new TableRow();
            interviewPageRow.CssClass = "paddingRow";
            TableCell interviewPageCell = new TableCell();
            interviewPageRow.Cells.Add(interviewPageCell);
            interviewPageCell.Controls.Add(interviewPageTable);
            interviewTable.Rows.Add(interviewPageRow);

            interviewPageControl.Controls.Add(interviewTable);
        }

        private void CreateHistory(TableCell historyCell)
        {
            historyControl = new Panel();
            historyControl.Style.Add("display", "none");
            historyControl.CssClass = "crumbs";
            historyControl.ID = "crumbs";
            historyPanelTable = new Table();
            historyPanelTable.ID = "HistoryPanelTable";
            historyPanelTable.CellSpacing = 0;
            historyPanelTable.CellPadding = 0;
            TableRow headerRow = new TableRow();
            TableCell headerCell = new TableCell();
            TableCell headerRight = new TableCell();

            headerRow.CssClass = "header";
            historyPanelTable.Rows.Add(headerRow);
            headerRow.Cells.Add(headerCell);
            headerRow.Cells.Add(headerRight);

            headerCell.CssClass = "title";
            headerRight.CssClass = "rightIcon";
            historyPanelTable.Rows.Add(headerRow);
            historyControl.Controls.Add(historyPanelTable);

            string historyText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("History");
            headerCell.Controls.Add(new LiteralControl(string.Format("<span>{0}</span>", historyText)));

            historyCell.Controls.Add(historyControl);
        }

        private void CreateNavigationButtonBar()
        {
            buttonBar = new WebControl(HtmlTextWriterTag.Div);
            buttonBar.CssClass = "botmenu";
            ProgressBar prog = new ProgressBar();

            if (page != null)
            {
                isDefinitelyLastPage = (page.NextPage == null || page.NextPage.PageValue == null && page.NextPage.QueryValue == null);
                if (page.ParentInterview.ProgressMax == -1)
                {
                    page.ParentInterview.CalculatePageRanks();
                }
                double progress = page.Progress;
                double total = page.ParentInterview.ProgressMax;
                prog.Percent = (progress / total) * 100;
            }

            prog.Visible = showProgress;

            Table table = new Table();

            if (!showFooter)
            {
                table.Style.Add("display", "none");
            }

            table.CellSpacing = 0;
            table.CellPadding = 0;
            table.CssClass = "botMenuTable";
            TableRow tr = new TableRow();
            table.Rows.Add(tr);
            TableCell leftCorner = new TableCell();
            tr.Cells.Add(leftCorner);
            TableCell leftCell = new TableCell();
            tr.Cells.Add(leftCell);
            TableCell middleCell = new TableCell();
            tr.Cells.Add(middleCell);
            TableCell rightCell = new TableCell();
            tr.Cells.Add(rightCell);
            TableCell rightCorner = new TableCell();
            tr.Cells.Add(rightCorner);
            rightCorner.Controls.Add(new LiteralControl("&nbsp;"));

            WebControl prevNavButton = CreatePrevNavButton();
            WebControl nextOrFinishNavButton = CreateNextOrFinishNavButton();

            //hash table created that stores the three nav controls 
            Hashtable navControlsHash = new Hashtable();
            navControlsHash.Add(Config.Shared.interview.prevNavButton, prevNavButton);
            if (showProgress)
            {
                navControlsHash.Add(Config.Shared.interview.progressBar, prog);
            }
            navControlsHash.Add(Config.Shared.interview.nextOrFinishNavButton, nextOrFinishNavButton);

            AssignControlToNavCells(navControlsHash, leftCell, middleCell, rightCell);

            leftCorner.CssClass = "leftCorner";
            leftCell.CssClass = "leftNavCell";
            rightCell.CssClass = "rightNavCell";
            rightCorner.CssClass = "rightCorner";
            middleCell.CssClass = "centreNavCell";

            buttonBar.Controls.Add(table);
        }

        private static void AssignControlToNavCells(Hashtable navControlsHash, TableCell leftCell, TableCell middleCell, TableCell rightCell)
        {

            leftCell.Style.Add("display", "none");
            middleCell.Style.Add("display", "none");
            rightCell.Style.Add("display", "none");

            foreach (DictionaryEntry entry in navControlsHash)
            {
                WebControl wc = (WebControl)entry.Value;

                switch (entry.Key.ToString().ToUpper())
                {
                    case LEFTNAVCELL:
                        leftCell.Controls.Add(wc);
                        leftCell.Style.Remove("display");
                        break;
                    case CENTRENAVCELL:
                        middleCell.Controls.Add(wc);
                        middleCell.Style.Remove("display");
                        break;
                    case RIGHTNAVCELL:
                        rightCell.Controls.Add(wc);
                        rightCell.Style.Remove("display");
                        break;
                }
            }
        }

        private void CreateTopMenu(string sInstanceName, out HtmlAnchor historyAnchor, out HtmlAnchor previewAnchor,
                                   out LinkButton saveToDMButton, out LinkButton suspendButton, out HtmlAnchor saveToLocalAnchor, out HtmlAnchor viewModeAnchor)
        {
            topMenu = new LibraryControl("div");
            topMenu.CssClass = "topMenu";
            Table headerTable = new Table();

            if (!showHeader)
            {
                headerTable.Style.Add("display", "none");
            }

            headerTable.ID = "hdr";
            TableRow headerTableRow = new TableRow();
            TableCell leftIconCell = new TableCell();
            TableCell packageTitleCell = new TableCell();
            TableCell rightOptions = new TableCell();
            headerTableRow.Cells.Add(leftIconCell);
            headerTableRow.Cells.Add(packageTitleCell);
            headerTableRow.Cells.Add(rightOptions);
            headerTable.Rows.Add(headerTableRow);

            leftIconCell.CssClass = "leftIcon";
            packageTitleCell.CssClass = "packageTitle";
            rightOptions.CssClass = "rightOptions";

            Label headerLabel = new Label();
            headerLabel.Text = sInstanceName;
            packageTitleCell.Controls.Add(headerLabel);

            historyAnchor = null;

            if (showHistoryButton)
            {
                historyAnchor = new HtmlAnchor();
                historyAnchor.ID = "historyAnchor";
                historyAnchor.HRef = "#";
                historyAnchor.InnerText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("History");
                historyAnchor.Attributes.Add("class", "history");
                rightOptions.Controls.Add(historyAnchor);
                historyAnchor.Attributes.Add("id", historyAnchor.ClientID);
            }

            previewAnchor = null;

            if (showPreviewButton)
            {
                previewAnchor = new HtmlAnchor();
                previewAnchor.ID = "previewAnchor";
                previewAnchor.HRef = "#";
                previewAnchor.InnerText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Preview");
                previewAnchor.Attributes.Add("class", "preview");
                rightOptions.Controls.Add(previewAnchor);
                previewAnchor.Attributes.Add("id", previewAnchor.ClientID);
            }

            viewModeAnchor = null;

            if (showViewModeButton)
            {
                viewModeAnchor = new HtmlAnchor();
                viewModeAnchor.ID = "viewModeAnchor";
                viewModeAnchor.HRef = "#";
                viewModeAnchor.InnerText = responsive == "Desktop" ? "Device" : "Desktop"; // ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Preview");
                viewModeAnchor.Attributes.Add("class", "viewMode");
                rightOptions.Controls.Add(viewModeAnchor);
                viewModeAnchor.Attributes.Add("id", viewModeAnchor.ClientID);
                //viewModeAnchor.Click += new EventHandler(viewModeAnchor_Click);
                // hidden control to hold viewmode value - button text doesnt survive postback
                viewModeButton = new LinkButton();
                viewModeButton.ID = "viewModeButton";
                rightOptions.Controls.Add(viewModeButton);
                viewModeButton.Attributes.Add("style", "display: none");
                viewModeButton.Attributes.Add("id", viewModeButton.ClientID);
                viewModeButton.Click += new EventHandler(viewModeButton_Click);
            }

            saveToDMButton = null;
            suspendButton = null;
            saveToLocalAnchor = null;

            if (showSuspendButton)
            {
                // Is there an IInterviewOpener plugin registered?			
                perfectusServerConfigurationPluginsPlugin suspenderPluginInfo = null;
                foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
                {
                    if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "interviewsuspender")
                    {
                        suspenderPluginInfo = plugin;
                        break;
                    }
                }

                if (suspenderPluginInfo != null)
                {
                    ObjectHandle h = Activator.CreateInstanceFrom(suspenderPluginInfo.path, suspenderPluginInfo.typeName);
                    if (h != null)
                    {
                        object o = h.Unwrap();

                        if (o is IInterviewSuspender)
                        {
                            suspenderPlugin = (IInterviewSuspender)o;
                            suspendButton = new LinkButton();
                            suspendButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").
                                    GetString("Suspend");
                            suspendButton.CssClass = "suspend";
                            suspendButton.ID = "suspend";
                            suspendButton.Click += new EventHandler(suspendButton_Click);
                            rightOptions.Controls.Add(suspendButton);
                            suspendButton.Attributes.Add("id", suspendButton.ClientID);
                        }
                    }
                }

            }

            if (showSaveButtons)
            {
                // Local save
                if (Config.Shared.interview.enableLocalSaveAnswerSet && answerSetDownloadPath != null)
                {
                    saveToLocalAnchor = new HtmlAnchor();
                    saveToLocalAnchor.ID = "saveToLocalAnchor";
                    saveToLocalAnchor.InnerText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Save");
                    saveToLocalAnchor.Attributes.Add("class", "saveToLocal");
                    saveToLocalAnchor.HRef = string.Format("{0}?{1}", AnswerSetDownloadPath, packageInstanceId);
                    rightOptions.Controls.Add(saveToLocalAnchor);
                    saveToLocalAnchor.Attributes.Add("id", saveToLocalAnchor.ClientID);
                }

                // Plugin (DMS) save
                // Is there an IInterviewOpener plugin registered?			
                perfectusServerConfigurationPluginsPlugin saverPluginInfo = null;
                foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
                {
                    if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "interviewsaver")
                    {
                        saverPluginInfo = plugin;
                        break;
                    }
                }

                if (saverPluginInfo != null)
                {
                    object o = Activator.CreateInstanceFrom(saverPluginInfo.path, saverPluginInfo.typeName, false, 0, null, new object[] { instance }, null, null, null).Unwrap();

                    if (o is IInterviewSaver && o is WebControl)
                    {
                        saverPlugin = (IInterviewSaver)o;
                        saveToDMButton = new LinkButton();

                        string btnText = saverPluginInfo.GetLocalisedName();

                        if (btnText == null || btnText.Length == 0)
                        {
                            btnText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("DMS Save");
                        }

                        saveToDMButton.Text = btnText;
                        saveToDMButton.CssClass = "saveTo";
                        saveToDMButton.ID = "dmsSaver";

                        saveToDMButton.Click += new EventHandler(saveToButton_Click);
                        saveToDMButton.Attributes.Add("onclick", string.Format("return {0};", saverPlugin.ClientSideFunctionName));

                        topMenu.Controls.Add((WebControl)saverPlugin);
                        rightOptions.Controls.Add(saveToDMButton);
                        saveToDMButton.Attributes.Add("id", saveToDMButton.ClientID);
                    }
                }
            }

            topMenu.Controls.Add(headerTable);
        }

        private WebControl CreateServiceMessage()
        {

            LibraryControl c = new LibraryControl("div");
            c.CssClass = "webServiceMsg";
            Table headerTable = new Table();
            headerTable.ID = "msg";
            TableRow headerTableRow = new TableRow();
            TableCell leftIconCell = new TableCell();
            TableCell packageTitleCell = new TableCell();
            headerTableRow.Cells.Add(leftIconCell);
            headerTableRow.Cells.Add(packageTitleCell);
            headerTable.Rows.Add(headerTableRow);
            leftIconCell.CssClass = "leftIcon";
            packageTitleCell.CssClass = "webServiceMessage";
            LinkButton servBtn = new LinkButton();
            servBtn.Click += new EventHandler(CallButton_Click);
            servBtn.CausesValidation = false;
            servBtn.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("BindWebService");
            packageTitleCell.Controls.Add(servBtn);
            c.Controls.Add(headerTable);
            return c;
        }

        private WebControl CreateErrorMessage()
        {
            LibraryControl errorDiv = new LibraryControl("div");
            errorDiv.CssClass = "errorMessage";

            Table errorTable = new Table();
            errorTable.ID = "errorMessage";

            // Error Title Row
            TableRow tableRow = new TableRow();
            errorTable.Rows.Add(tableRow);

            TableCell iconCell = new TableCell();
            iconCell.CssClass = "leftIcon";
            tableRow.Cells.Add(iconCell);

            TableCell textCell = new TableCell();
            textCell.CssClass = "errorMessage";
            tableRow.Cells.Add(textCell);

            ErrorTitle = new Label();
            ErrorTitle.ID = "errorTitle";
            ErrorTitle.Visible = false;
            textCell.Controls.Add(ErrorTitle);

            // Error Text Row
            tableRow = new TableRow();
            errorTable.Rows.Add(tableRow);

            iconCell = new TableCell();
            iconCell.Text = "&nbsp;";
            tableRow.Cells.Add(iconCell);

            textCell = new TableCell();
            textCell.CssClass = "errorMessage";
            tableRow.Cells.Add(textCell);

            ErrorText = new Label();
            ErrorText.ID = "errorText";
            ErrorText.Visible = false;
            textCell.Controls.Add(ErrorText);

            errorDiv.Controls.Add(errorTable);

            return errorDiv;
        }

        private WebControl CreatePrevNavButton()
        {
            WebControl c = new WebControl(HtmlTextWriterTag.Span);

            // Only make the 'previous' button if there is more than one stack entry.
            if (stack.Length > 1)
            {
                previousButton = new LinkButton();
                previousButton.CssClass = "prevButton";
                previousButton.ID = string.Format("prevButtonOnPage{0:N}", pageId);
                previousButton.Click += new EventHandler(prevBtn_Click);
                previousButton.CausesValidation = false;
                previousButton.Attributes.Add("onclick", "DisableButton()");
                previousButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("PrevButtonText");

                if (isPagePreview)
                {
                    previousButton.Enabled = false;
                }
                c.Controls.Add(previousButton);
                previousButton.Attributes.Add("id", previousButton.ClientID);
            }
            else
            {
                previousButton = null;
            }
            return c;
        }

        private WebControl CreateNextOrFinishNavButton()
        {
            WebControl c = new WebControl(HtmlTextWriterTag.Span);

            nextOrFinishButton = new LinkButton();
            if (isDefinitelyLastPage)
            {
                nextOrFinishButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("FinishButtonText");
                nextOrFinishButton.CssClass = "finishButton";
            }
            else
            {
                nextOrFinishButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("NextButtonText");
                nextOrFinishButton.CssClass = "nextButton";
            }
            nextOrFinishButton.Click += new EventHandler(nextBtn_Click);
            //nextBtn.Attributes.Add("OnClick", "this.enabled=false;");
            nextOrFinishButton.CausesValidation = true;
            if (isPagePreview)
            {
                nextOrFinishButton.Enabled = false;
            }

            nextOrFinishButton.ID = string.Format("nextOrFinishButtonOnPage{0:N}", pageId);

            c.Controls.Add(nextOrFinishButton);
            nextOrFinishButton.Attributes.Add("id", nextOrFinishButton.ClientID);

            //Fast forward button

            //			int position;
            //			InstanceManagement.GetPointer(packageInstanceId, out position);

            bool isViewingBelowTopOfStack = InstanceManagement.NavigationStackGetPendingDeletion(packageInstanceId).Length > 0;

            if (!isDefinitelyLastPage && showFF && isViewingBelowTopOfStack)
            {
                fastForwardButton = new LinkButton();
                fastForwardButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("FFButtonText");
                fastForwardButton.CssClass = "ffButton";
                fastForwardButton.Click += new EventHandler(ffBtn_Click);
                fastForwardButton.CausesValidation = true;
                //ffBtn.Attributes.Add("OnClick", "javascript: return disableControl(this)");
                if (isPagePreview)
                {
                    fastForwardButton.Enabled = false;
                }
                fastForwardButton.ID = string.Format("fastForwardButtonOnPage{0:N}", pageId);
                c.Controls.Add(fastForwardButton);
                fastForwardButton.Attributes.Add("id", fastForwardButton.ClientID);
            }

            return c;
        }

        #endregion
        #region Interview Page Data Population Methods

        protected override void OnDataBinding(EventArgs e)
        {
            if ((hasBound && InterviewPageCreated) || forceRefresh)
            {
                this.Controls.Clear();
                ChildControlsCreated = false;
                forceRefresh = false;
            }

            EnsureChildControls();
            PopulateInterviewPage();
            PopulateHistoryPanel();
        }

        private void SetInterviewPageData()
        {
            SetInterviewPageData(Direction.Unknown);
        }
        private void SetInterviewPageData(Direction direction)
        {
            hasBound = true;
            Page.Trace.Write("Perfectus", "Begin Interview.OnDataBinding");
            StackEntry[] loadedStack = new StackEntry[0];

            //setup StringBinding MetaData
            int packageVer = 0;
            int packageRev = 0;
            StringBindingMetaData stringBindingMetaData = new StringBindingMetaData();
            string metaDataPackageReference = string.Empty;

            // Note: We cannot use the existence of the previewPage object to determine whether this ctrl belongs to a preview
            // page. Basically it is or isn't irrespective of whether the previewPage object has been instanciated already. The problem
            // is that the object only gets instanciated during postback, hence we should never be here in the first place during preview
            // if we don't already have a previewPage object. See FB2169 Hence have removed the code below that relied on an exception
            // being throw, as the exception is not thrown if an Oracle db is used.

            // Get the Page to render
            if (previewPage != null)
            {
                page = previewPage;
                instance = previewPage.ParentPackage;
            }
            else
            {
                InstanceManagement.GetVersion(packageInstanceId, out packageVer, out packageRev);
                metaDataPackageReference = InstanceManagement.GetInstanceReference(packageInstanceId);

                // Get the InterviewInstance from the DB
                if (instance == null)
                {
                    Page.Trace.Write("Perfectus", "Begin retrieve instance");
                    instance = InstanceManagement.GetInstance(packageInstanceId);
                    Page.Trace.Write("Perfectus", "End retrieve instance");
                }
                // Get the page ID that's on top of the stack.
                InstanceManagement.NavigationStackPeek(packageInstanceId, out pageId);

                if (pageId != Guid.Empty)
                {
                    loadedStack = InstanceManagement.NavigationStackGet(packageInstanceId);

                    // dynamic page title
                    page = instance.InterviewPageByGuid[loadedStack[0].PageId];
                    if (page != null)
                    {
                        string evaluatedPageTitleProp = page.PageTitle.Evaluate(AnswerProvider.Caller.Interview, stringBindingMetaData);
                        if (evaluatedPageTitleProp != null && evaluatedPageTitleProp != string.Empty && evaluatedPageTitleProp.Length > 0)
                        {
                            loadedStack[0].PageName = evaluatedPageTitleProp;
                        }
                    }
                    //
                    page = instance.InterviewPageByGuid[pageId];

                    // Apply seen to this page
                    SeeQuestionsOnPage(page);
                }
            }

            stringBindingMetaData = new StringBindingMetaData(instance.Name, metaDataPackageReference, packageInstanceId,
                                                                      packageVer, packageRev);
            if (page != null && page is ExternalPage && suspendExternalRedirect != true)
            {

                // FB361: The address needs to be url encoded.
                string strAddress = ((ExternalPage)page).Address.Evaluate(AnswerProvider.Caller.Interview, true, stringBindingMetaData);

                // External page authors may want the pageId and instanceId passed to them for use in an easyresumenext.
                if (strAddress != null)
                {
                    strAddress = strAddress.Replace("{pageId}", page.UniqueIdentifier.ToString());
                    strAddress = strAddress.Replace("{instanceId}", packageInstanceId.ToString());
                    strAddress = strAddress.Replace("{direction}", direction.ToString().ToLower());
                }
                else
                {
                    strAddress = "";
                }

                HttpContext.Current.Response.Redirect(strAddress);
            }

            //pf-3175 refresh services - yes, no, ask

            //pf-3269 external page must not be affected by the internal page refresh logic
            bool bRefreshServices = false;
            if (page.GetType() == typeof(InternalPage))
            {
                bRefreshServices = (((InternalPage)page).RefreshServices == InternalPage.ListRefreshServices.Yes);
            }

            if (previewPage == null && packageInstanceId != Guid.Empty && page != null &&
                (page.Seen == false || bRefreshServices))
            {
                Page.Trace.Write("Perfectus", "Begin page service bind");
                BindingSystem binder = new BindingSystem(instance);

                willCallService = binder.WillCallServices(page);
                if (willCallService)
                {
                    binder.Bind(page);
                    if (page.ParentPackage != null)
                    {
                        InstanceManagement.SaveInstance(packageInstanceId, page.ParentPackage);
                    }
                }
                Page.Trace.Write("Perfectus", "End page service bind");
            }


            if (page != null)
            {
                if (page is InternalPage)
                {
                    showPreviewButton = previewPage == null && instance != null && instance.Templates != null && instance.Templates.Count > 0 && ((InternalPage)page).ShowPreviewButton.Evaluate(true, AnswerProvider.Caller.Interview);
                    showSaveButtons = previewPage == null && ((InternalPage)page).AllowSaveAnswerSet.Evaluate(true, AnswerProvider.Caller.Interview);
                    showSuspendButton = previewPage == null && ((InternalPage)page).AllowSuspend.Evaluate(true, AnswerProvider.Caller.Interview);
                    showHistoryButton = previewPage == null && ((InternalPage)page).ShowHistory.Evaluate(true, AnswerProvider.Caller.Interview);

                    // Get the preview style value set in IPManager
                    previewStyle = ((InternalPage)page).PreviewStyle;
                }

                string evaluatedPageTitleProp = page.PageTitle.Evaluate(AnswerProvider.Caller.Interview, stringBindingMetaData);

                if (evaluatedPageTitleProp != null && evaluatedPageTitleProp != string.Empty && evaluatedPageTitleProp.Length > 0)
                {
                    pageTitle = evaluatedPageTitleProp;
                }
                else
                {
                    pageTitle = page.Name;
                }

                stack = loadedStack;

                string[] itemTypesToBind;
                itemTypesToBind = new string[page.Items.Count];
                Question[] questionStubsToBind;
                questionStubsToBind = new Question[page.Items.Count];
                string[] itemIdsToBind;
                itemIdsToBind = new string[page.Items.Count];

                for (int i = 0; i < page.Items.Count; i++)
                {
                    IPageItem ipi = page.Items[i];
                    itemIdsToBind[i] = ipi.UniqueIdentifier.ToString("N");
                    if (ipi is Question)
                    {
                        itemTypesToBind[i] = "Q";
                        questionStubsToBind[i] = (Question)ipi;

                        if (previewPage == null)
                        {
                            stringBindingMetaData = new StringBindingMetaData(instance.Name, metaDataPackageReference, packageInstanceId,
                                                                              packageVer, packageRev);
                        }

                        questionStubsToBind[i].StringBindingMetaData = stringBindingMetaData;
                    }
                    else if (ipi is HorizontalLayoutZone)
                    {
                        itemTypesToBind[i] = "H";
                    }
                    else if (ipi is HtmlBlock)
                    {
                        itemTypesToBind[i] = "T";
                    }
                    else if (ipi is RepeaterZone)
                    {
                        itemTypesToBind[i] = "R";
                    }
                    else
                        itemTypesToBind[i] = "U";
                }

                extensionMappings = instance.ExtensionMapping;
                itemTypes = itemTypesToBind;
                itemIds = itemIdsToBind;
                questionStubs = questionStubsToBind;
                instanceName = instance.Name;
                vars = InterviewScriptFactory.GetVars( instance, page, interviewClientId, validationDHTMLStateId, stringBindingMetaData );
            }

            Page.Trace.Write("Perfectus", "End Interview.OnDataBinding");
        }

        private void PopulateInterviewPage()
        {
            StringBindingMetaData stringBindingMetaData = new StringBindingMetaData();

            if (previewPage == null)
            {
                int packageVer = 0;
                int packageRev = 0;

                try
                {
                    InstanceManagement.GetVersion(packageInstanceId, out packageVer, out packageRev);
                }
                catch (InvalidCastException)
                {
                    // leave at 0.0
                }

                string metaDataPackageReference = string.Empty;

                if (packageVer > 0)
                {
                    metaDataPackageReference = InstanceManagement.GetInstanceReference(packageInstanceId);
                }

                if (instance != null)
                {
                    stringBindingMetaData = new StringBindingMetaData(instance.Name, metaDataPackageReference, packageInstanceId,
                                                                      packageVer, packageRev);
                }
            }

            // Set the title of the page
            pageTitleLabel.Text = string.Format("<span class=\"pageLabel\">{0}</span>", pageTitle);

            // Add the questions / htmlTextArea's, Repeaters etc - one row for each item
            for (int i = 0; i < itemTypes.Length; i++)
            {
                TableRow row = new TableRow();
                interviewPageTable.Rows.Add(row);
                row.Attributes.Add("item", itemIds[i]);

                switch (itemTypes[i])
                {
                    case "Q":
                        CreateQuestionRow(row, i, stringBindingMetaData);
                        break;
                    case "T":
                        CreateHtmlTextAreaRow(row, i, stringBindingMetaData);
                        break;
                    case "H":
                        CreateHorizontalLayoutRow(row, i);
                        break;
                    case "R":
                        CreateRepeaterRow(row, i);
                        break;
                    default:
                        Controls.Add(new LiteralControl("Unknown type: " + itemTypes[i]));
                        break;
                }
            }

            InterviewPageCreated = true;
        }

        private void CreateQuestionRow(TableRow row, int itemIndex, StringBindingMetaData stringBindingMetaData)
        {
            // Set the question
            Question question = (page != null) ? question = (Question)page.Items[itemIndex] : question = questionStubs[itemIndex];
            question.StringBindingMetaData = stringBindingMetaData;

            // Question visibility
            if (question.Visible.IsDefinitelyNo() && question.ParentPackage != null)
            {
                row.Visible = false;
            }
            else
            {
                row.Style.Add("display", question.Visible.Evaluate(true, AnswerProvider.Caller.Interview) ? "" : "none");
            }

            // Question prompt field
            TableCell promptCell = new TableCell();
            promptCell.CssClass = "promptCellVertical";
            QuestionPrompt questionPrompt = new QuestionPrompt(question);
            promptCell.Controls.Add(questionPrompt);
            row.Cells.Add(promptCell);

            // Question input field (textbox, checkbox etc)
            TableCell fieldCell = new TableCell();
            fieldCell.CssClass = "fieldCellVertical";
            row.Cells.Add(fieldCell);

            QuestionField questionField = new QuestionField(question, javascriptPath, imagesPath, 0, callButtonEventHandler,
                                                            packageInstanceId, extensionMappings, loadAnswerSetHandler,
                                                            isPagePreview, false);
            questionField.ID = string.Format("qf{0}_on_{1:N}", itemIndex, pageId);
            fieldCell.Controls.Add(questionField);
            questionFieldControls.Add(questionField);

            // Hint text
            QuestionHint questionHint = new QuestionHint(question);
            if (question.Hint != null && question.Hint.EvaluateAndKeepXml().Trim().Length > 0)
            {
                fieldCell.Controls.Add(new LiteralControl("<br />"));
                fieldCell.Controls.Add(questionHint);
            }

            // Example text
            QuestionExample questionExample = new QuestionExample(question);
            if (question.Example != null && question.Example.EvaluateAndKeepXml().Trim().Length > 0)
            {
                fieldCell.Controls.Add(new LiteralControl("<br />"));
                fieldCell.Controls.Add(questionExample);
            }

            // Nov 2015 removed all this default in favour of fix within the answersetupload itself.
            ////Rick Mar 2015 pf-` defaulting the answerset is not compatible with livelink integration, 
            ////              'Unable to cast object of type 'Perfectus.Livelink.ServerIntegration.DisplayTypeExtensions.DocumentPicker' to type 'Perfectus.Server.WebUI.Library.Interview.PageItems.Fields.AnswerSetUpload'.'
            ////              GetDefaultUpload needs to be implemented without changing the interface
            ////Document Picker
            //////Rick Dec 2012 pf-375

            //if (question.DataType == Perfectus.Common.PerfectusDataType.AnswerSet)
            //{
            //    if (question.DefaultAnswer.TextValue != string.Empty)
            //    {
            //        Page.Trace.Write("Perfectus", "Begin populate question from AnswerSet DefaultAnswer");

            //        //get the replacement question id, tidy up the xml and replace it with the instance id
            //        string defaultAnswerXml = question.DefaultAnswer.EvaluateAndKeepXml();
            //        string newDefault = defaultAnswerXml;

            //        // looks like <text>c:\rick\<item uId="b5706466-f5c6-4f38-8ef8-8ea56dab3de6"></item>answers.xml</text>
            //        if (defaultAnswerXml != null && defaultAnswerXml.IndexOf("<item") > -1)
            //        {
            //            defaultAnswerXml = defaultAnswerXml.Replace("<text>", "");
            //            defaultAnswerXml = defaultAnswerXml.Replace("</text>", "");
            //            newDefault = defaultAnswerXml.Substring(0, defaultAnswerXml.IndexOf("<item"));
            //            newDefault += packageInstanceId.ToString();
            //            newDefault += defaultAnswerXml.Substring(defaultAnswerXml.IndexOf("></item>") + 8);
            //        }
            //        bool success = false;
            //        //success = ((AnswerSetUpload)questionField.FieldControl).GetDefaultUpload(instance, newDefault);
            //        //Rick 22 July 2015 - update: In order to resolve the type error I duplicated this logic to Interview.cs and HorizontalLayoutZoneItem.cs
            //        if (newDefault != null && newDefault.Length > 0)
            //        {
            //            XmlDocument doc = new XmlDocument();
            //            if (File.Exists(newDefault))
            //            {
            //                doc.Load(newDefault);
            //                Submitter.ApplyAnswerSet(instance, doc, Submitter.QuestionMatchModeOption.Name, false);
            //                success = true;
            //            }
            //        }


            //        //cause new answers to be saved into the interview, replacing previous values
            //        InstanceManagement.SaveInstance(packageInstanceId, instance);

            //        //status bar info
            //        if (success)
            //        {
            //            string message = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").
            //                                GetString("AnswerSetApplied");
            //            messageBar.Text = message.ToString();
            //            messageBar.Visible = true;
            //        }

            //        //pf-393 prevent this from running again and reseting other controls in the process 
            //        //note for version 6: uncomment the following line to make the answer persist across page changes.
            //        //also make the defaulting of each answer item configurable in IPManager.
            //        //question.DefaultAnswer = new TextQuestionQueryValue() { TextValue = string.Empty };

            //        Page.Trace.Write("Perfectus", "End populate question from AnswerSet DefaultAnswer");
            //    }
            //}
        }

        private void CreateHtmlTextAreaRow(TableRow row, int itemIndex, StringBindingMetaData stringBindingMetaData)
        {
            HtmlBlock htmlBlock = null;

            // Set the htmlBlock and visibility
            if (page != null)
            {
                htmlBlock = (HtmlBlock)page.Items[itemIndex];

                if (htmlBlock.Visible.IsDefinitelyNo())
                {
                    row.Visible = false;
                }
                else
                {
                    row.Style.Add("display", htmlBlock.Visible.Evaluate(true, AnswerProvider.Caller.Interview) ? "" : "none");
                }
            }

            // Styles
            TableCell htmlCell = new TableCell();
            htmlCell.CssClass = "htmlCell";
            htmlCell.ColumnSpan = 2;
            row.Cells.Add(htmlCell);

            // Add the html text area item
            HtmlBlockItem htmlBlockItem = new HtmlBlockItem(htmlBlock, instance, stringBindingMetaData, 0, false);
            htmlCell.Controls.Add(htmlBlockItem);
        }

        private void CreateHorizontalLayoutRow(TableRow row, int itemIndex)
        {
            PageItemCollection items = null;

            //  Set the rowsvisibility
            if (page != null)
            {
                items = ((HorizontalLayoutZone)(page.Items[itemIndex])).Items;
                if (((HorizontalLayoutZone)(page.Items[itemIndex])).Visible.IsDefinitelyNo())
                {
                    row.Visible = false;
                }
                else
                {
                    bool isVisible = ((HorizontalLayoutZone)(page.Items[itemIndex])).Visible.Evaluate(true, AnswerProvider.Caller.Interview);
                    string visibility = isVisible ? "" : "none";
                    row.Style.Add("display", visibility);
                }
            }

            // Define the horizontal zone contained in the row
            TableCell horizontalZoneCell = new TableCell();
            row.Cells.Add(horizontalZoneCell);
            horizontalZoneCell.CssClass = "hzoneContainerCell";
            horizontalZoneCell.ColumnSpan = 2;
            HorizontalLayoutZoneItem horizontalLayoutZoneItem = new HorizontalLayoutZoneItem(items, javascriptPath, imagesPath,
                                                                                             questionFieldControls, callButtonEventHandler,
                                                                                             packageInstanceId, extensionMappings,
                                                                                             loadAnswerSetHandler, instance, 1,
                                                                                             isPagePreview, pageId, responsive == "Device");
            horizontalZoneCell.Controls.Add(horizontalLayoutZoneItem);
        }

        private void CreateRepeaterRow(TableRow row, int itemIndex)
        {
            TableCell repeaterZoneCell = new TableCell();
            row.Cells.Add(repeaterZoneCell);
            repeaterZoneCell.CssClass = "repeaterZoneContainerCell";
            repeaterZoneCell.ColumnSpan = 2;

            PageItemCollection pageItems = null;

            // Set the properties for the repeater zone, including visibility
            if (page != null)
            {
                pageItems = ((RepeaterZone)(page.Items[itemIndex])).Items;

                if (((RepeaterZone)(page.Items[itemIndex])).Visible.IsDefinitelyNo())
                {
                    row.Visible = false;
                }
                else
                {
                    bool isVisible = ((RepeaterZone)(page.Items[itemIndex])).Visible.Evaluate(true, AnswerProvider.Caller.Interview);
                    string visibility = isVisible ? "" : "none";
                    row.Style.Add("display", visibility);
                    numRepeaters = ((RepeaterZone)(page.Items[itemIndex])).MaxRows.Evaluate(AnswerProvider.Caller.Interview);

                    if (numRepeaters == 0)
                    {
                        numRepeaters = int.MaxValue;
                    }
                }
            }

            HorizontalLayoutZoneItem horizontalLayoutZoneItem = new HorizontalLayoutZoneItem(pageItems, javascriptPath, imagesPath,
                                                                                             questionFieldControls, callButtonEventHandler,
                                                                                             packageInstanceId, extensionMappings,
                                                                                             loadAnswerSetHandler, instance, numRepeaters,
                                                                                             isPagePreview, pageId, responsive == "Device");
            horizontalLayoutZoneItem.SaveOnPostbackRequested += new EventHandler(hzi2_SaveOnPostbackRequested);
            repeaterZoneCell.Controls.Add(horizontalLayoutZoneItem);
        }

        private void PopulateHistoryPanel()
        {
            historyCell.RowSpan = itemTypes.Length + 2;

            for (int i = 0; i < stack.Length; i++)
            {
                StackEntry stackEntry = stack[i];
                InterviewPage InterviewPage = (InterviewPage)instance.InterviewPageByGuid[stackEntry.PageId];

                if (InterviewPage.ShowPageInHistory.Evaluate(true, AnswerProvider.Caller.Interview))
                {
                    TableRow tableRow = new TableRow();
                    TableCell tableCell = new TableCell();
                    tableRow.Cells.Add(tableCell);
                    tableCell.ColumnSpan = 2;
                    tableRow.CssClass = "historyItem";
                    historyPanelTable.Rows.Add(tableRow);

                    LinkButton linkButton = new LinkButton();
                    linkButton.EnableViewState = false;
                    linkButton.Text = stackEntry.PageName;
                    linkButton.CommandName = "PageSelect";
                    linkButton.CommandArgument = stackEntry.PositionInStack.ToString();
                    tableCell.Controls.Add(linkButton);
                    linkButton.Command += new CommandEventHandler(lb_Command);
                    linkButton.CausesValidation = false;
                }
            }
        }

        #endregion
        #region Postback Event Handlers

        void hzi2_SaveOnPostbackRequested(object sender, EventArgs e)
        {
            SaveOnPostback();
        }

        private void prevBtn_Click(object sender, EventArgs e)
        {
            DoPrevious();
        }

        private void ffBtn_Click(object sender, EventArgs e)
        {

            //int position;
            //InstanceManagement.GetPointer(packageInstanceId, out position);

            bool okToFF = IsOkayToFfwd();
            if (okToFF)
            {
                DoFastForward();
                //doFastForward(position);

                DataBind();

                /*
                string message = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("FFComplete");
                messageBar.Text = message.ToString();
                messageBar.Visible = true;
                 */
            }
            else
            {
                SaveOnPostback();
                showFF = false;
                if (fastForwardButton != null)
                {
                    fastForwardButton.Visible = false;
                }
                string message = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("FFComplete");
                messageBar.Text = message.ToString();
                messageBar.Visible = true;
            }
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            EnsureChildControls();

            // By this stage page validation should already have taken place.
            if( Page.IsValid )
                DoNext( );
        }

        private void CallButton_Click(object sender, EventArgs e)
        {
            //if (Page.IsValid)
            {
                // Get instance
                if (instance == null)
                {
                    Page.Trace.Write("Perfectus", "Begin retrieve instance in callButton_Click");
                    instance = InstanceManagement.GetInstance(packageInstanceId);
                    Page.Trace.Write("Perfectus", "End retrieve instance in callButton_Click");
                }

                // Get this page from the instance
                InterviewPage thisPage = GetThisPage(instance);

                // Apply answers
                if (!answersApplied)
                {
                    ApplyAnswersFromControls(thisPage);
                }

                // Apply seen to this page
                SeeQuestionsOnPage(thisPage);

                // Save instance
                Page.Trace.Write("Perfectus", "Begin save instance in callButton_Click");
                InstanceManagement.SaveInstance(packageInstanceId, instance);
                Page.Trace.Write("Perfectus", "End save instance in callButton_Click");
                thisPage.Seen = false;
                showFF = false;
                DataBind();

            }
            Page.Validate();
        }

        /// <summary>
        ///     Event Handler to load an AnswerSet File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadAnswerSet_Click(object sender, EventArgs e)
        {
            if (instance == null)
            {
                instance = InstanceManagement.GetInstance(packageInstanceId);
            }

            if (previewPage == null)
            {
                bool success = false;
                foreach (QuestionField qf in questionFieldControls)
                {
                    if (qf.FieldControl is AnswerSetUpload)
                    {
                        success = ((AnswerSetUpload)qf.FieldControl).LoadFile(instance);
                    }
                    else
                    {
                        AnswerCollection qfAnswer = qf.GetAnswers();

                        // If there's no answer, ignore it
                        if (qfAnswer.Count == 0 || qfAnswer[0].ToString().Trim().Length == 0)
                        {
                            continue;
                        }

                        Question q = instance.QuestionByGuid[qf.QuestionId];
                        if (q == null || q.DataType != PerfectusDataType.AnswerSet)
                        {
                            continue;
                        }

                        if (q.DisplayType >= QuestionDisplayType.Extension1 &&
                            q.DisplayType <= QuestionDisplayType.Extension50)
                        {
                            if (instance.ExtensionMapping.ContainsKey(q.DisplayType))
                            {
                                DisplayTypeExtensionInfo dtei = instance.ExtensionMapping[q.DisplayType];

                                // find the plugin's Fetcher
                                foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
                                {
                                    if (plugin.type.ToLowerInvariant() == "fetcher" &&
                                        plugin.path != null && plugin.path.Trim().Length > 0 &&
                                        plugin.key.ToLowerInvariant() == dtei.ServerSideFetcherPluginId.ToLowerInvariant())
                                    {
                                        ObjectHandle h = Activator.CreateInstanceFrom(plugin.path, plugin.typeName, true,
                                                                                      BindingFlags.CreateInstance, null,
                                                                                      null,
                                                                                      System.Threading.Thread.CurrentThread.
                                                                                      CurrentUICulture, null, null);
                                        object o = h.Unwrap();

                                        if (o != null && o is IFetcher)
                                        {
                                            IFetcher fetcher = (IFetcher)o;
                                            string contentType;
                                            using (Stream s = fetcher.Fetch(qfAnswer[0].ToString(), instance, packageInstanceId, out contentType))
                                            {
                                                if (contentType == "text/xml" || contentType == "application/x-xml")
                                                {
                                                    XmlDocument doc = new XmlDocument();
                                                    s.Seek(0, SeekOrigin.Begin);
                                                    doc.Load(s);
                                                    Submitter.ApplyAnswerSet(instance, doc, Submitter.QuestionMatchModeOption.Name, false);
                                                    success = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                InstanceManagement.SaveInstance(packageInstanceId, instance);
                DataBind();
                if (success)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").
                                        GetString("AnswerSetApplied");
                    messageBar.Text = message.ToString();
                    messageBar.Visible = true;
                }
            }

            // previously:
            // if ( Page.IsValid ) { ... }
            // now: testing after loading the data
            Page.Validate();
        }

        private void preview_PreviewRequested(object sender, EventArgs e)
        {
            previewAnchor.InnerText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Interview");

            if (previewId != null && interviewPageClientId != null)
            {
                // Passing controls' clientID to the javascript
                Page.ClientScript.RegisterStartupScript(GetType(), "previewOn", string.Format("<script type='text/javascript'>document.getElementById('{0}').style.display='';document.getElementById('{1}').style.display='none';</script>", previewId, interviewPageClientId));
            }
        }

        private void viewModeButton_Click(object sender, EventArgs e)
        {
            this.responsive = this.viewModeState.Value == "Desktop" ? "Device" : "Desktop";
            //postback
            EnsureChildControls();
            DataBind();
        }

        private void suspendButton_Click(object sender, EventArgs e)
        {
            if (suspenderPlugin != null)
            {
                EnsureChildControls();
                Page.Validate();
                if (Page.IsValid)
                {
                    // Get instance
                    if (instance != null)
                    {
                        Page.Trace.Write("Perfectus", "Begin retrieve instance in suspendButton_Click");
                        instance = InstanceManagement.GetInstance(packageInstanceId);
                        Page.Trace.Write("Perfectus", "End retrieve instance in suspendButton_Click");
                    }

                    // Get this page from the instance
                    InterviewPage thisPage = GetThisPage(instance);

                    // Apply answers
                    if (!answersApplied)
                    {
                        ApplyAnswersFromControls(thisPage);
                    }

                    // Apply seen to this page
                    SeeQuestionsOnPage(thisPage);

                    // Save instance
                    Page.Trace.Write("Perfectus", "Begin save instance in suspendButton_Click");
                    thisPage.Seen = true;
                    InstanceManagement.SaveInstance(packageInstanceId, instance);
                    Page.Trace.Write("Perfectus", "End save instance in suspendButton_Click");

                    suspenderPlugin.Suspend(instance, packageInstanceId);
                }
            }
        }

        private void saveToButton_Click(object sender, EventArgs e)
        {
            if (saverPlugin != null)
            {
                EnsureChildControls();
                Page.Validate();
                if (Page.IsValid)
                {
                    // Get instance
                    if (instance == null)
                    {
                        Page.Trace.Write("Perfectus", "Begin retrieve instance in saveToBtn_Click");
                        instance = InstanceManagement.GetInstance(packageInstanceId);
                        Page.Trace.Write("Perfectus", "End retrieve instance in saveToBtn_Click");
                    }

                    // Get this page from the instance
                    InterviewPage thisPage = GetThisPage(instance);

                    // Apply answers
                    if (!answersApplied)
                    {
                        ApplyAnswersFromControls(thisPage);
                    }

                    // Apply seen to this page
                    SeeQuestionsOnPage(thisPage);

                    // Save instance
                    Page.Trace.Write("Perfectus", "Begin save instance in saveToBtn_Click");
                    thisPage.Seen = true;
                    InstanceManagement.SaveInstance(packageInstanceId, instance);
                    Page.Trace.Write("Perfectus", "End save instance in saveToBtn_Click");

                    XmlDocument answerSetXml = new XmlDocument();
                    using (MemoryStream ms = InstanceToAnswerSetXml.GetAnswerSetXml(instance, packageInstanceId, false))
                    {
                        answerSetXml.Load(ms);
                    }
                    string docRef = Page.Request.Form[saverPlugin.DocRefFieldId];
                    string msg = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("AnswerSetSavedSuccessfully");

                    try
                    {
                        saverPlugin.SaveAnswerSet(answerSetXml, docRef);
                    }
                    catch (Exception ex)
                    {
                        msg = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("AnswerSetNotSaved") + "\n" + ex.Message;
                    }

                    msg = msg.Replace("'", @"\'");
                    msg = msg.Replace("\n", @"\n");
                    Page.ClientScript.RegisterStartupScript(GetType(), "SaveAnswerSetToMessage", "alert ('" + msg + "');", true);
                    DataBind();
                }
            }
            else
            {
                SaveOnPostback();
            }
        }

        private void preview_HistoryJump(object sender, HistoryJumpEventArgs e)
        {
            PopBackToPointInStack(e.PositionInStack);
        }

        #endregion
        #region SharedLibrary

        /// <summary>
        ///		Updates the shared library items in the package if the shared library is activated
        /// </summary>
        public void GetLatestSharedLibraryVersions()
        {
            // Handle Shared Library - Is it turnned on?
            try
            {
                if (mRepository.IsSharedLibraryActivated())
                {
                    instance.UpdateAllLinkedSharedLibraryItems(mRepository);
                }
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL3);
                if (rethrow)
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("SharedLibraryConnectionError"));
            }
        }

        #endregion
        #region Navigation Methods

        public void DoPrevious()
        {
            if (instance == null)
            {
                Page.Trace.Write("Perfectus", "Begin retrieve instance in prevBtn_Click");
                instance = InstanceManagement.GetInstance(packageInstanceId);
                Page.Trace.Write("Perfectus", "End retrieve instance in prevBtn_Click");
            }

            // Get this page from the instance
            InterviewPage thisPage = GetThisPage(instance);

            // Apply answers
            if (!answersApplied)
            {
                ApplyAnswersFromControls(thisPage);
            }

            // Unsee all on this page
            UnseePage(thisPage);

            // Save instance
            Page.Trace.Write("Perfectus", "Begin save instance in prevBtn_Click");
            thisPage.Seen = true;
            InstanceManagement.SaveInstance(packageInstanceId, instance);
            Page.Trace.Write("Perfectus", "End save instance in prevBtn_Click");

            // Pop
            InstanceManagement.NavigationStackPop(packageInstanceId);
            showFF = true;

            // Rebind self
            SetInterviewPageData(Direction.Previous);
            DataBind();
        }

        private bool IsOkayToFfwd()
        {
            Package instanceFromDb = InstanceManagement.GetInstance(packageInstanceId);
            Package instanceToCheck = (Package)instanceFromDb.Clone();//InstanceManagement.GetInstance(packageInstanceId);

            if (instanceFromDb != null && instanceToCheck != null)
            {
                InterviewPage pageToCheck = GetThisPage(instanceToCheck);
                InterviewPage pageFromDb = instanceFromDb.InterviewPageByGuid[pageToCheck.UniqueIdentifier];
                if (pageToCheck != null && pageFromDb != null && pageToCheck is InternalPage)
                {
                    // case 1716 -- bad side effect -- Answers applied to Clone, but status set for the page
                    bool saveAnswersApplied = this.answersApplied;
                    ApplyAnswersFromControls(pageToCheck);
                    this.answersApplied = saveAnswersApplied;

                    foreach (Question qToCheck in ((InternalPage)pageToCheck).GetQuestionsOnPage())
                    {
                        Question qFromDb = instanceFromDb.QuestionByGuid[qToCheck.UniqueIdentifier];
                        if (qFromDb != null)
                        {
                            AnswerCollectionCollection collCollToCheck = qToCheck.AnswersByPerson;
                            AnswerCollectionCollection collCollFromDb = qFromDb.AnswersByPerson;
                            bool hasChanged = !collCollToCheck.IsEquivalentTo(collCollFromDb);
                            if (hasChanged && IsImportant(qFromDb))
                            {
                                return false;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void DoFastForward()
        {
            StackEntry[] ghostStack = InstanceManagement.NavigationStackGetPendingDeletion(packageInstanceId);
            if (ghostStack.Length > 0)
            {
                instance = InstanceManagement.GetInstance(packageInstanceId);
                InterviewPage thisPage = GetThisPage(instance);
                // Apply answers
                if (!answersApplied)
                {
                    ApplyAnswersFromControls(thisPage);
                }
                SeeQuestionsOnPage(thisPage);

                foreach (StackEntry se in ghostStack)
                {
                    if (instance != null && instance.InterviewPageByGuid.Contains(se.PageId))
                    {
                        InterviewPage ghostPage = instance.InterviewPageByGuid[se.PageId];
                        if (ghostPage != null)
                        {
                            ghostPage.Seen = true;
                            SeeQuestionsOnPage(ghostPage);
                        }
                    }
                }

                InstanceManagement.SaveInstance(packageInstanceId, instance);
                InstanceManagement.NavigationStackFastForward(packageInstanceId);
            }
        }

        public void DoNext()
        {
            try
            {
                // Get instance
                if (instance == null)
                {
                    Page.Trace.Write("Perfectus", "Begin retrieve instance in nextBtn_Click");
                    instance = InstanceManagement.GetInstance(packageInstanceId);
                    Page.Trace.Write("Perfectus", "End retrieve instance in nextBtn_Click");
                }

                // Get this page from the instance
                InterviewPage thisPage = GetThisPage(instance);
                QuestionFieldCollection clientSideQuestionFields = questionFieldControls;

                // Apply answers
                if (!answersApplied)
                {
                    ApplyAnswersFromControls(thisPage);
                }

                ValidateInterviewPageQuestions(thisPage, clientSideQuestionFields);

                // Apply seen to this page
                SeeQuestionsOnPage(thisPage);

                // Save instance
                Page.Trace.Write("Perfectus", "Begin save instance in nextBtn_Click");
                thisPage.Seen = true;
                InstanceManagement.SaveInstance(packageInstanceId, instance);
                Page.Trace.Write("Perfectus", "End save instance in nextBtn_Click");

                // Process any rules the Page has
                Page.Trace.Write("Perfectus", "Begining Page Rule processing in nextBtn_Click");
                if (thisPage is InternalPage)
                {
                    ProcessPageRules((InternalPage)thisPage);
                }
                Page.Trace.Write("Perfectus", "End Page Rule processing in nextBtn_Click");

                // Calculate next page
                InterviewPage candidatePage = thisPage;

                do
                {
                    candidatePage = candidatePage.NextPage.Evaluate(AnswerProvider.Caller.System);
                }
                while (candidatePage != null && !(candidatePage.Visible.Evaluate(true, AnswerProvider.Caller.System)));

                InterviewPage nextPage = candidatePage;
                if (nextPage != null)
                {
                    // Make sure we aren't doubling the top of the stack.
                    Guid topPageId;
                    InstanceManagement.NavigationStackPeek(packageInstanceId, out topPageId);
                    if (topPageId != nextPage.UniqueIdentifier)
                    {
                        int packageVer = int.MinValue;
                        int packageRev = int.MinValue;
                        InstanceManagement.GetVersion(packageInstanceId, out packageVer, out packageRev);
                        string metaDataPackageReference = InstanceManagement.GetInstanceReference(packageInstanceId);
                        StringBindingMetaData stringBindingMetaData = new StringBindingMetaData(instance.Name, metaDataPackageReference, packageInstanceId, packageVer, packageRev);

                        string evaluatedNextPageTitleProp = nextPage.PageTitle.Evaluate(AnswerProvider.Caller.Interview, stringBindingMetaData);
                        string nextPageName = null;

                        if (evaluatedNextPageTitleProp != null && evaluatedNextPageTitleProp != string.Empty && evaluatedNextPageTitleProp.Length > 0)
                        {
                            nextPageName = evaluatedNextPageTitleProp;
                        }
                        else
                        {
                            nextPageName = nextPage.Name;
                        }

                        // Push next page on to top of stack
                        InstanceManagement.NavigationStackPush(packageInstanceId, nextPage.UniqueIdentifier, nextPageName);
                    }
                    pageId = nextPage.UniqueIdentifier;

                    // Databind self
                    SetInterviewPageData(Direction.Next);
                    DataBind();
                }
                else
                {
                    DoFinish(thisPage);
                }
            }
            catch (InterviewException ex)
            {
                ShowError(ex);
            }
        }

        private void ShowError(InterviewException ex)
        {
            ErrorTitle.Text = ex.Title;
            ErrorTitle.Visible = true;
            ErrorText.Text = ex.UIMessage;
            ErrorText.Visible = true;
            ErrorMessage.Visible = true;

            // FB355: Move the check to here, otherwise the interface would not display server side validation errors....
            if( Config.Shared.interview.EnableEmailLogging )
            {
                string to = Config.Shared.interview.To;
                string from = Config.Shared.interview.From;
                string subject = Config.Shared.interview.Subject;
                string smtpServer = Config.Shared.interview.SMTPServer;

                // Send an email to raise the error
                InterviewEmailLogger interviewEmailLogger = new InterviewEmailLogger( ex, to, from, smtpServer );
                interviewEmailLogger.SendEmail( subject, ex.Title, "100", Identity.GetRunningUserId( ), this.Page.Request.RawUrl );
            }
        }

        /// <summary>
        ///		Returns true if a question (altered and detected as altered in 'IsOkayToFfwd') has any bearing on navigation, visibility etc. Anything except a plain old question in a template basically.
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        private static bool IsImportant(Question q)
        {
            Package p = q.ParentPackage;
            if (p != null)
            {
                // Navigation
                foreach (InterviewPage pg in p.InterviewPageByGuid.Values)
                {
                    if (pg.NextPage != null && pg.NextPage.ContainsReferenceTo(q))
                    {
                        return true;
                    }

                    if (pg.Visible != null && pg.Visible.ContainsReferenceTo(q))
                    {
                        return true;
                    }

                    if (pg is InternalPage)
                    {
                        if (((InternalPage)pg).ShowPreviewButton != null && ((InternalPage)pg).ShowPreviewButton.ContainsReferenceTo(q))
                        {
                            return true;
                        }

                        if (InternalPage.ColContainsRefToImportantQuestion(pg.Items, q))
                        {
                            return true;
                        }

                        if (((InternalPage)pg).AllowSaveAnswerSet != null && ((InternalPage)pg).AllowSaveAnswerSet.ContainsReferenceTo(q))
                        {
                            return true;
                        }
                    }
                }

                // Visiblity, enabled etc.
                foreach (Question qn in p.Questions)
                {
                    if (qn.Visible.ContainsReferenceTo(q))
                    {
                        return true;
                    }

                    if (qn.Mandatory.ContainsReferenceTo(q))
                    {
                        return true;
                    }

                    if (qn.Enabled.ContainsReferenceTo(q))
                    {
                        return true;
                    }

                    if (qn.DataBindings != null)
                    {
                        return true;
                    }
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        private void lb_Command(object sender, CommandEventArgs e)
        {
            int popBackTo = Convert.ToInt32(e.CommandArgument);
            PopBackToPointInStack(popBackTo);
        }

        private void PopBackToPointInStack(int popBackTo)
        {
            // Get instance
            if (instance == null)
            {
                Page.Trace.Write("Perfectus", "Begin retrieve instance in lb_Command");
                instance = InstanceManagement.GetInstance(packageInstanceId);
                Page.Trace.Write("Perfectus", "End retrieve instance in lb_Command");
            }

            // Get this page from the instance
            InterviewPage thisPage = GetThisPage(instance);

            // Apply answers
            if (!answersApplied)
            {
                ApplyAnswersFromControls(thisPage);
            }

            // Unsee all on this page, and everything back to the selected one.
            for (int i = popBackTo; i < stack.Length; i++)
            {
                StackEntry entry = stack[i];
                InterviewPage stackPage = instance.InterviewPageByGuid[entry.PageId];
                UnseePage(stackPage);
            }

            // Save instance
            Page.Trace.Write("Perfectus", "Begin save instance in lb_Command");
            InstanceManagement.SaveInstance(packageInstanceId, instance);
            Page.Trace.Write("Perfectus", "End save instance in lb_Command");

            // Pop back to pageId
            InstanceManagement.NavigationStackPopBackToPoint(packageInstanceId, popBackTo);
            // Databind self?
            showFF = true;
            DataBind();
        }

        #endregion
        #region Miscelaneous Methods

        private void ApplyAnswersFromControls(InterviewPage thisPage)
        {
            if (!isPagePreview && thisPage is InternalPage)
            {
                EnsureChildControls();

                //Debug.Assert(questionFieldControls.Count == ((InternalPage)thisPage).GetQuestionsOnPage().Count);
                Page.Trace.Write("Perfectus", string.Format("Saving answers from {0} questions on {1}.", questionFieldControls.Count, thisPage.Name));
                if (instance == null)
                {
                    instance = thisPage.ParentPackage;
                }
                ArrayList enabledQfs = new ArrayList();
                // Set all answers to new empty collections
                foreach (QuestionField qi in questionFieldControls)
                {
                    Question q = instance.QuestionByGuid[qi.QuestionId];
                    if (q != null)
                    {
                        if (q.Enabled == null || (q.Enabled != null && q.Enabled.Evaluate(true, AnswerProvider.Caller.System)))
                        {
                            q.AnswersByPerson.Clear();
                            if (!enabledQfs.Contains(qi))
                            {
                                enabledQfs.Add(qi);
                            }
                        }
                    }
                }
                // If there are any file controls on the page, ask them to write their files into the db
                SaveUploadedFiles();

                // Add all answers from the controls to the new empty collections
                foreach (QuestionField qi in enabledQfs)
                {
                    Question q = instance.QuestionByGuid[qi.QuestionId];
                    if (q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
                    //if (qi.FieldControl.Enabled)
                    {
                        //q.AnswersByPerson.Clear();
                        Debug.Assert(qi.QuestionId != Guid.Empty);


                        AnswerCollection coll = qi.GetAnswers();
                        AnswerCollectionCollection collColl = q.AnswersByPerson;
                        if (collColl == null)
                        {
                            collColl = new AnswerCollectionCollection();
                            q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
                        }
                        collColl.Add(coll);
                        Page.Trace.Write("Perfectus", string.Format("Answering {0}", q.Name));
                    }
                }

                answersApplied = true;
            }
        }

        private void ValidateInterviewPageQuestions(InterviewPage page, QuestionFieldCollection clientSideQuestionFields)
        {
            if (page is InternalPage)
            {
                foreach (QuestionField questionField in clientSideQuestionFields)
                {
                    Question question = instance.QuestionByGuid[questionField.QuestionId];
                    ValidateQuestion( questionField, question );
                }
            }
        }

        /// <summary>
        ///     Validates a given question against it's coresponding questionField, and ensures the client side validation
        ///     is in sync with what the interivew's answers are.
        /// </summary>
        /// <param name="questionField"></param>
        /// <param name="question"></param>
        /// <param name="internalPage"></param>
        /// <returns></returns>
        private bool ValidateQuestion( QuestionField questionField, Question question )
        {
            if (!ShouldQuestionBeVisibile( question ))
                // Question not visible (not answered) and hence valid
                return true; 

            // FB355: The manatory query is the only validation we need to do, as all other
            // validations would have been automatically evaluated during page validation.
            // Manatory query validations are disabled by default and hence we need to do it ourselfs here.
            if( question.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query )
                ValidateQuestionMandatory( questionField, question );

            return true;
        }

        /// <summary>
        ///     Assess the visibility of a given question.
        /// </summary>
        /// <param name="questionField">The client side question field.</param>
        /// <param name="question">The question being assessed.</param>
        /// <returns>Whether the question should be visible or not.</returns>
        private bool ShouldQuestionBeVisibile( Question question )
        {
            // Note FB355: It did not make much sense testing the visibility against that at client side.
            // Firstly it did not work as questions contained within a container would lose visibility,
            // yet this is not recorded. Also what is the user supposed to do?
            return question.Visible.Evaluate( true, AnswerProvider.Caller.Interview );
        }

        private void ValidateQuestionMandatory( QuestionField questionField, Question question )
        {
            if( question.Mandatory.Evaluate( true, AnswerProvider.Caller.Interview ) )
            {
                // List of validations that have been disabled client side.
                String disabledValidations = validationDHTMLState.Value;
                // The query states that this needs to be mandatory. 
                RequiredFieldValidator reqval = null;

                // Find the required validator and use it to determine whether the answer (if any) is valid.
                foreach( Control ctrl in questionField.FieldControl.Controls )
                {
                    if( ctrl.GetType( ) != typeof( RequiredFieldValidator ) )
                        continue;

                    // We have a required field validator. If it was disabled, we check its valid and act appropriately.
                    reqval = (RequiredFieldValidator)ctrl;

                    // If this validator is listed, we ignore it
                    if( disabledValidations.Contains( reqval.ClientID ) )
                        continue;
                    
                    // We only concerned with validators that be default are disable.
                    if( reqval.Enabled == false )
                    {

                        // Enable the validator and validate!
                        reqval.Enabled = true;
                        reqval.Validate( );

                        if( !reqval.IsValid )
                        {
                            string errorText = ResourceLoader.GetResourceManager( "Perfectus.Server.WebUI.Library.Localisation" ). GetString( "DHTMLError" );
                            throw new InterviewException( question.UniqueIdentifier.ToString( ) + " failed mandatory query validation.", errorText );
                        }
                    }

                }
            }
        }

        private void SaveOnPostback()
        {
            EnsureChildControls();
            if (instance == null)
            {
                Page.Trace.Write("Perfectus", "Begin retrieve instance in SaveOnPostback");
                instance = InstanceManagement.GetInstance(packageInstanceId);
                Page.Trace.Write("Perfectus", "End retrieve instance in SaveOnPostback");
            }

            // Get this page from the instance
            InterviewPage thisPage = GetThisPage(instance);

            // Apply answers
            if (!answersApplied)
            {
                ApplyAnswersFromControls(thisPage);
            }

            // Save instance
            Page.Trace.Write("Perfectus", "Begin save instance in SaveOnPostback");
            thisPage.Seen = true;
            InstanceManagement.SaveInstance(packageInstanceId, instance);
            Page.Trace.Write("Perfectus", "End save instance in SaveOnPostback");
            DataBind();
        }

        private static string GetDateOrder()
        {
            string shortDatePattern = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
            if (shortDatePattern.IndexOf('y') < shortDatePattern.IndexOf('M'))
            {
                return "ymd";
            }
            if (shortDatePattern.IndexOf('M') < shortDatePattern.IndexOf('d'))
            {
                return "mdy";
            }
            return "dmy";

        }

        private string GetQuestionArrayScript()
        {
            EnsureChildControls();
            StringBuilder sb = new StringBuilder();
            string comma = string.Empty;
            sb.Append("<script type='text/javascript'>");
            sb.Append("var questionControls = new Array(");
            foreach (QuestionField qf in questionFieldControls)
            {
                if (qf != null)
                {
                    sb.Append(comma);
                    sb.Append(string.Format("document.getElementById('{0}')", qf.ClientID));
                    comma = ",";
                }
            }
            sb.Append(");");
            sb.Append("</script>");
            return sb.ToString();
        }

        private void SaveUploadedFiles()
        {
            if (!isPagePreview)
            {
                foreach (QuestionField qf in questionFieldControls)
                {
                    if (qf.FieldControl is FileUpload)
                    {
                        ((FileUpload)qf.FieldControl).WriteFileIfPresent();
                    }
                }
            }
        }

        public void ResumeInstance()
        {
            EnsureChildControls();
            bool visibleStartPage = true;

            // Get the page ID that's on top of the stack.
            InstanceManagement.NavigationStackPeek(packageInstanceId, out pageId);

            instance = InstanceManagement.GetInstance(packageInstanceId);

            // Get this page from the instance
            InterviewPage candidatePage = GetThisPage(instance);

            // Forward to the next visible page if the current is invisible
            if (!candidatePage.Visible.Evaluate(true, AnswerProvider.Caller.System))
            {
                if (candidatePage.UniqueIdentifier == instance.Interviews[0].StartPage.UniqueIdentifier)
                {
                    visibleStartPage = false;
                }

                // Work out the next page
                do
                {
                    candidatePage = candidatePage.NextPage.Evaluate(AnswerProvider.Caller.System);
                }
                while (candidatePage != null && !(candidatePage.Visible.Evaluate(true, AnswerProvider.Caller.System)));

                InterviewPage nextPage = candidatePage;
                if (nextPage != null)
                {
                    // Make sure we aren't doubling the top of the stack.
                    Guid topPageId = Guid.Empty;

                    if (visibleStartPage)
                    {
                        InstanceManagement.NavigationStackPeek(packageInstanceId, out topPageId);
                    }
                    else
                    {
                        // Invisible start pages must not be on the stack
                        InstanceManagement.NavigationStackPop(packageInstanceId, true);
                    }

                    if (topPageId != nextPage.UniqueIdentifier)
                    {
                        // Push next page on to top of stack
                        InstanceManagement.NavigationStackPush(packageInstanceId, nextPage.UniqueIdentifier, nextPage.Name);
                    }
                    pageId = nextPage.UniqueIdentifier;
                }
                else
                {
                    // If the start page is invisible and there are no other pages in the interview you arrive here
                    // (or the next page query evaluates to null)
                    // Note the start page is left on the stack (to allow interview resumes)

                    // Invisible start page & no other pages - end the interview
                    DoFinish(instance.Interviews[0].StartPage);
                }
            }
        }

        private InterviewPage GetThisPage(Package p)
        {
            return p.InterviewPageByGuid[pageId];
        }

        private void UnseePage(InterviewPage pageToUnSee)
        {
            if (!isPagePreview && pageToUnSee != null && pageToUnSee is InternalPage)
            {
                Page.Trace.Write("Perfectus", string.Format("Unseeing page {0}", pageToUnSee.Name));
                foreach (Question q in ((InternalPage)pageToUnSee).GetQuestionsOnPage())
                {
                    q.Seen = false;
                }
            }
        }

        private void SeeQuestionsOnPage(InterviewPage pageToSee)
        {
            if (!isPagePreview && pageToSee != null && pageToSee is InternalPage)
            {
                SeeCollection(pageToSee.Items, false);
                //				QuestionCollection qCol = pageToSee.GetQuestionsOnPage();
                //				foreach(Question q in qCol)
                //				{
                //					// If the question is still visible (server-side) after all answers have been applied, mark it as seen.
                //					// Otherwise, mark it as unseen.
                //					q.Seen = (q.Visible != null && q.Visible.Evaluate(true, AnswerProvider.Caller.System));
                //				}
            }
        }

        private static void SeeCollection(PageItemCollection coll, bool forceInvisible)
        {
            foreach (IPageItem item in coll)
            {
                if (item is HorizontalLayoutZone)
                {
                    SeeCollection(((HorizontalLayoutZone)item).Items, !(item.Visible.Evaluate(true, AnswerProvider.Caller.System)));
                }
                if (item is RepeaterZone)
                {
                    SeeCollection(((RepeaterZone)item).Items, !(item.Visible.Evaluate(true, AnswerProvider.Caller.System)));
                }
                if (item is Question)
                {
                    Question q = (Question)item;
                    if (forceInvisible)
                    {
                        q.Seen = false;
                    }
                    else
                    {
                        q.Seen = (q.Visible != null && q.Visible.Evaluate(true, AnswerProvider.Caller.System));
                    }
                }
            }
        }

        /// <summary>
        ///		Places the current (answered) instance on the queue for processing.
        /// </summary>
        private void DoFinish(InterviewPage finalPage)
        {
            Debug.Assert(instance != null);
            if (instance != null)
            {
                int ver;
                int rev;
                InstanceManagement.GetVersion(packageInstanceId, out ver, out rev);
                Job j = new Job(new ProcessingTask(instance.UniqueIdentifier, packageInstanceId, ver, rev, Identity.GetRunningUserId()));
                j.SourceName = String.Empty;
                //				ProcessingTask t = new ProcessingTask(instance.UniqueIdentifier, packageInstanceId, ver, rev, Identity.GetRunningUserId());
                InstanceManagement.EnQueuePackageInstance(j);
                string destinationUrl = null;
                if (finalPage != null && finalPage.ParentInterview != null)
                {
                    destinationUrl = finalPage.ParentInterview.DestinationUrl;
                }

                Perfectus.Server.Reporting.Report.SaveAnswers(instance, packageInstanceId, ver, rev, AnswerProvider.Caller.Assembly);
                InstanceManagement.SetCompleted(packageInstanceId);

                OnFinished(destinationUrl);
            }
        }

        /// <summary>
        ///	Places the current (answered) instance on the queue for per Page processing.
        /// </summary>
        private void ProcessPageRules(InternalPage pageToProcess)
        {
            Debug.Assert(instance != null);
            Debug.Assert(pageToProcess != null);

            if (pageToProcess.PageRules2 != null && pageToProcess.PageRules2.Count > 0)
            {
                if (instance != null)
                {
                    int ver;
                    int rev;
                    InstanceManagement.GetVersion(packageInstanceId, out ver, out rev);

                    Job j = new Job(new ProcessingTask(instance.UniqueIdentifier, packageInstanceId, ver, rev, Identity.GetRunningUserId(), pageToProcess.UniqueIdentifier));
                    j.SourceName = pageToProcess.Name;

                    //ProcessingTask t = new ProcessingTask(instance.UniqueIdentifier, packageInstanceId, ver, rev, Identity.GetRunningUserId(), pageToProcess.UniqueIdentifier);
                    InstanceManagement.EnQueuePackageInstance(j);
                }
            }
        }

        protected void OnFinished(string destinationUrl)
        {
            if (Finished != null)
            {
                Finished(this, new InterviewFinishedEventArgs(destinationUrl));
            }
        }

        #endregion
    }
}
