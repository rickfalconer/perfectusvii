using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.PluginKit;
using System.Collections;
using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.WebUI.Library.Status
{
	/// <summary>
	/// Summary description for Status.
	/// </summary>
	[ToolboxData("<{0}:Status runat=server></{0}:Status>")]

	public class Status : WebControl, INamingContainer
	{
		public event EventHandler<StartInterviewEventArgs> StartInterview;
		public event EventHandler<ResumeInterviewEventArgs> ResumeInterview;

		private string createdBy = string.Empty;
		private StatusSet status = null;
		private Table table = null;
		private int inProgressRecordCount; 
		private int historyRecordCount;
		private System.Web.UI.HtmlControls.HtmlInputHidden recentHidden = null;
		private System.Web.UI.HtmlControls.HtmlInputHidden inProgressHidden = null;
		private System.Web.UI.HtmlControls.HtmlInputHidden historyHidden = null;
		private IInterviewOpener openerPlugin = null;
		private ArrayList _statusItemArray = new ArrayList();
		private HtmlInputFile fileUp;
		//public delegate void packageDeletedHandler(Guid packageId);
		//public event packageDeletedHandler packageDeleted;
		
		private enum sectionMode
		{
			MyRecentTransactions,
			MyTransactionsInProgress,
			MyHistory
		} ;

		[Browsable(true)]
		public string JavascriptPath
		{
			get { return javascriptPath; }
			set { javascriptPath = value; }
		}

		private string javascriptPath = null;

		[Browsable(true)]
		public int RecentTransactionItemCount
		{
			get { return recentTransactionItemCount; }
			set { recentTransactionItemCount = value; }
		}

		private int recentTransactionItemCount = 0;

		[Browsable(true)]
		public int InProgressPagingSize
		{
			get { return inProgressPagingSize; }
			set { inProgressPagingSize = value; }
		}

		private int inProgressPagingSize = 0;

		[Browsable(true)]
		public int HistoryPagingSize
		{
			get { return historyPagingSize; }
			set { historyPagingSize = value; }
		}

		private int historyPagingSize = 0;

		[Browsable(true)]
		public string ImagesPath
		{
			get { return imagesPath; }
			set { imagesPath = value; }
		}

		private string imagesPath = string.Empty;

        // Section Titles are configurable

        [Browsable(true)]
        public string RecentTransactionTitle
        {
            get { return recentTransactionTitle; }
            set { recentTransactionTitle = value; }
        }

        private string recentTransactionTitle = "";
        
        [Browsable(true)]
        public string InProgressTitle
        {
            get { return inProgressTitle; }
            set { inProgressTitle = value; }
        }

        private string inProgressTitle = "";
        
        [Browsable(true)]
        public string HistoryTitle
        {
            get { return historyTitle; }
            set { historyTitle = value; }
        }

        private string historyTitle = "";

        // Sections display are configurable
        // All sections will display by default

        [Browsable(true)]
        public bool TopMenuVisible
        {
            get { return topMenuVisible; }
            set { topMenuVisible = value; }
        }

        private bool topMenuVisible = true;

        [Browsable(true)]
        public bool RecentTransactionVisible
        {
            get { return recentTransactionVisible; }
            set { recentTransactionVisible = value; }
        }
        
        private bool recentTransactionVisible = true;

        [Browsable(true)]
        public bool InProgressVisible
        {
            get { return inProgressVisible; }
            set { inProgressVisible = value; }
        }

        private bool inProgressVisible = true;

        [Browsable(true)]
        public bool HistoryVisible
        {
            get { return historyVisible; }
            set { historyVisible = value; }
        }

        private bool historyVisible = true;

        // If expend instances in each section when page first load is configurable
        // By default, instances in Recent Transaction section will get expended on page first load
        //             instances in In Progress and History section will be hidden on page first load
        [Browsable(true)]
        public bool RecentTransactionExpanded
        {
            get { return recentTransactionExpanded; }
            set { recentTransactionExpanded = value; }
        }

        private bool recentTransactionExpanded = true;
        
        [Browsable(true)]
        public bool InProgressExpanded
        {
            get { return inProgressExpanded; }
            set { inProgressExpanded = value; }
        }

        private bool inProgressExpanded = false;
        
        [Browsable(true)]
        public bool HistoryExpanded
        {
            get { return historyExpanded; }
            set { historyExpanded = value; }
        }

        private bool historyExpanded = false;

        // If include PackageInformation and Distributor columns is configurable,
        // these two columns will be included by default

        [Browsable(true)]
        public bool IncludePackageInfo
        {
            get { return includePackageInfo; }
            set { includePackageInfo = value; }
        }
        
        private bool includePackageInfo = true;

        [Browsable(true)]
        public bool IncludeDistributorInfo
        {
            get { return includeDistributorInfo; }
            set { includeDistributorInfo = value; }
        }
       
        private bool includeDistributorInfo = true;


		private int currentMyHistoryPage
		{
			get
			{
				object oCurrentMyHistoryPage = ViewState["currentMyHistoryPage"];
				if (oCurrentMyHistoryPage != null)
				{
					return Convert.ToInt32(oCurrentMyHistoryPage.ToString());
				}
				else
				{
					return 1;
				}
			}
			set
			{
				ViewState["currentMyHistoryPage"] = value.ToString();
			}
		}

		private int currentInProgressPage
		{
			get
			{
				object oCurrentInProgressPage = ViewState["currentInProgressPage"];
				if (oCurrentInProgressPage != null)
				{
					return Convert.ToInt32(oCurrentInProgressPage.ToString());
				}
				else
				{
					return 1;
				}
			}
			set
			{
				ViewState["currentInProgressPage"] = value.ToString();
			}
		}

		protected override void CreateChildControls()
		{
			CreateControls();
		}

		private WebControl CreateTopMenu(out LinkButton openDmsButton, out HtmlAnchor openLocalAnchor, out Button uploadButton)
		{
			LibraryControl c = new LibraryControl("div");
			c.CssClass = "topMenu";
			Table headerTable = new Table();
			headerTable.ID = "hdr";
			TableRow headerTableRow = new TableRow();
			TableCell leftIconCell = new TableCell();
			TableCell packageTitleCell = new TableCell();
			TableCell rightOptions = new TableCell();
			headerTableRow.Cells.Add(leftIconCell);
			headerTableRow.Cells.Add(packageTitleCell);
			headerTableRow.Cells.Add(rightOptions);
			headerTable.Rows.Add(headerTableRow);

			leftIconCell.CssClass = "leftIcon";
			packageTitleCell.CssClass = "packageTitle";
			rightOptions.CssClass = "rightOptions";

			Label headerLabel = new Label();
			headerLabel.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Status");
			packageTitleCell.Controls.Add(headerLabel);

			// Is there an IInterviewOpener plugin registered?			
			perfectusServerConfigurationPluginsPlugin openerPluginInfo = null;
			foreach(perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
			{
				if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "interviewopener")
				{
					openerPluginInfo = plugin;
					break;
				}
			}
			
			openDmsButton = null;
			openLocalAnchor = null;
			fileUp = null;
			uploadButton = null;
			
			// Local save
			if (Config.Shared.status.enableLocalOpenAnswerSet)
			{
				openLocalAnchor = new HtmlAnchor();
				openLocalAnchor.InnerText = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Open");
				openLocalAnchor.Attributes.Add("class", "openFromLocal");
				openLocalAnchor.HRef = "#";

				fileUp = new HtmlInputFile();
				uploadButton = new Button();
				fileUp.Style.Add("display", "none");
				uploadButton.Style.Add("display", "none");
				
				uploadButton.Text =ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Upload");
				uploadButton.Click += new EventHandler(uploadButton_Click);			


				rightOptions.Controls.Add(fileUp);
				rightOptions.Controls.Add(uploadButton);
				rightOptions.Controls.Add(openLocalAnchor);

				fileUp.ID = "openlocal";
				uploadButton.ID = "uploadlocal";

			}
			
			
			if ( openerPluginInfo != null)
			{
				object o = Activator.CreateInstanceFrom(openerPluginInfo.path, openerPluginInfo.typeName).Unwrap();
				
				if (o is IInterviewOpener && o is WebControl)
				{
					openerPlugin = (IInterviewOpener)o;
					openDmsButton = new LinkButton();
					openDmsButton.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("DMS Open");
					openDmsButton.CssClass = "openFrom";
					openDmsButton.ID="dmsOpener";

					openDmsButton.Click += new EventHandler(openFromButton_Click);
					openDmsButton.Attributes.Add("onclick", string.Format("return {0};", openerPlugin.ClientSideFunctionName));



					c.Controls.Add((WebControl)openerPlugin);
					rightOptions.Controls.Add(openDmsButton);
				}
			}
			
			c.Controls.Add(headerTable);
			return c;
		}


		private void CreateControls()
		{
			LinkButton openFromButton = null;
			Button uploadButton = null;
			HtmlAnchor openLocalAnchor = null;

			table = new Table();
			table.ID = "pt";
			table.CssClass = "statusTable";
			table.CellSpacing = 0;

			GetData(currentInProgressPage, currentMyHistoryPage);

			recentHidden = new System.Web.UI.HtmlControls.HtmlInputHidden();
			recentHidden.ID = "recentHidden";

            // Open or hide sections depends on the values set in the status user control

            if (Page.Request.Form[this.ClientID + ":" + recentHidden.UniqueID] == null)
            {
                if (recentTransactionExpanded)
                {
                    recentHidden.Value = "false";
                }
                else 
                {
                    recentHidden.Value = "true";
                }
            }
            else
                recentHidden.Value = Page.Request.Form[this.ClientID + ":" + recentHidden.UniqueID];
            Controls.Add(recentHidden);
            

			inProgressHidden = new System.Web.UI.HtmlControls.HtmlInputHidden();
			inProgressHidden.ID = "inProgressHidden";
            if (Page.Request.Form[this.ClientID + ":" + inProgressHidden.UniqueID] == null)
            {
                inProgressHidden.Value = "true"; 
                if (inProgressExpanded)
                {
                    inProgressHidden.Value = "false";
                }
            }
            else
                inProgressHidden.Value = Page.Request.Form[this.ClientID + ":" + inProgressHidden.UniqueID];
			Controls.Add(inProgressHidden);

			historyHidden = new System.Web.UI.HtmlControls.HtmlInputHidden();
			historyHidden.ID = "historyHidden";
            if (Page.Request.Form[this.ClientID + ":" + historyHidden.UniqueID] == null)
            {
                historyHidden.Value = "true"; 
                if (historyExpanded)
                {
                    historyHidden.Value = "false";
                }
            }
            else
                historyHidden.Value = Page.Request.Form[this.ClientID + ":" + historyHidden.UniqueID];
			Controls.Add(historyHidden);

            // Display sections depends on the values set in the status user control

            if (topMenuVisible)
            {
                WebControl topMenu = CreateTopMenu(out openFromButton, out openLocalAnchor, out uploadButton);
                Controls.Add(topMenu);

                if (openLocalAnchor != null && fileUp != null && uploadButton != null)
                {
                    openLocalAnchor.Attributes.Add("onclick", string.Format("ToggleUpload('{0}', '{1}')", fileUp.ClientID, uploadButton.ClientID));
                }
            }

            if (recentTransactionVisible)
            {
                CreateMyRecentTransactions(sectionMode.MyRecentTransactions, recentHidden);
            }

            if (inProgressVisible)
            {
                CreateMyTransactionsInProgress(sectionMode.MyTransactionsInProgress, inProgressHidden);
            }

            if (historyVisible)
            {
                CreateMyHistory(sectionMode.MyHistory, historyHidden);
            }

			Controls.Add(table);

            if (status != null)
            {
                CreateErrorDivs(status.Errors);
            }
		}

		private void GetData(int inProgressPage, int myHistoryPage)
		{
			createdBy = Identity.GetRunningUserId();
			int inProgressStart = ((inProgressPage - 1) * inProgressPagingSize) + 1;
			int inProgressEnd = inProgressPage * inProgressPagingSize;
			int myHistoryStart = ((myHistoryPage - 1) * historyPagingSize) + 1;
			int myHistoryEnd = myHistoryPage * historyPagingSize;;
			if (createdBy != string.Empty)
			{
				status = PackageManager.Retrieval.GetPackageStatus(createdBy, recentTransactionItemCount, inProgressStart, inProgressEnd, myHistoryStart, myHistoryEnd, out inProgressRecordCount, out historyRecordCount);
			}		
		}

		private void CreateErrorDivs(StatusSet.ErrorsDataTable errors)
		{	

			Panel errorPanel = new Panel();	
			Table errorTable = new Table();
			Guid lastPackageInstanceId = Guid.Empty;
			bool stillPanelAndTableToAdd = false;

            // String collection to prevent duplicate messages being shown.
            System.Collections.Specialized.StringCollection messages = new System.Collections.Specialized.StringCollection( );

			foreach(StatusSet.ErrorsRow er in errors)
			{
                if( new Guid( er.RelatedPackageInstanceId ) != lastPackageInstanceId && CheckUniqueErrorControl( "error" + er.RelatedPackageInstanceId ) == true ) // new div to make
                {
                    // Clear our history of error messages
                    messages.Clear( );

                    // if not first time
                    if( lastPackageInstanceId != Guid.Empty )
                    {
                        //add previous Table and Div
                        errorPanel.Controls.Add( errorTable );
                        Controls.Add( errorPanel );
                    }

                    errorPanel = new Panel( );
                    errorTable = new Table( );

                    stillPanelAndTableToAdd = true;

                    TableRow errorTitleRow = new TableRow( );
                    TableCell errorTitleCellLeft = new TableCell( );
                    TableCell errorTitleCell = new TableCell( );
                    TableCell errorTitleCellRight = new TableCell( );
                    Label errorTitleLabel = new Label( );
                    Label errorTitleLabelRight = new Label( );

                    errorTable.CssClass = "errorTable";
                    errorTable.CellPadding = 0;
                    errorTable.CellSpacing = 0;
                    errorTitleCell.CssClass = "errorHeader";
                    errorTitleCellLeft.CssClass = "errorLeftMainIcon";
                    errorTitleCellRight.CssClass = "closeBoxCell";
                    errorTitleCellLeft.Text = "&nbsp;";
                    errorPanel.ID = "error" + er.RelatedPackageInstanceId;
                    errorPanel.CssClass = "errorPopup";
                    errorTitleLabel.Text = ResourceLoader.GetResourceManager( "Perfectus.Server.WebUI.Library.Localisation" ).GetString( "ErrorsHeading" );
                    errorTitleLabelRight.Text = ResourceLoader.GetResourceManager( "Perfectus.Server.WebUI.Library.Localisation" ).GetString( "Close" );
                    errorTitleCellRight.Attributes.Add( "onMouseOver", "body.style.cursor = 'pointer'" );
                    errorTitleCellRight.Attributes.Add( "onMouseOut", "body.style.cursor = '';" );
                    errorTitleCellRight.Attributes.Add( "onClick", "toggleErrorBox('" + this.ClientID + "_error" + er.RelatedPackageInstanceId + "',false, event);body.style.cursor = '';" );
                    errorTitleCell.Controls.Add( errorTitleLabel );
                    errorTitleCellRight.Controls.Add( errorTitleLabelRight );
                    errorTitleRow.Controls.Add( errorTitleCellLeft );
                    errorTitleRow.Controls.Add( errorTitleCell );
                    errorTitleRow.Controls.Add( errorTitleCellRight );
                    errorTable.Rows.Add( errorTitleRow );
                }

                // Check whether the error message is already recorded... (This is such terrible code)
				else if( (er["Message"] !=System.DBNull.Value) && messages.Contains( er.Message ) )
                    continue;
			
				// severity info.
				TableRow errorRowSeverity = new TableRow();
				TableCell errorCellSeverityLeft = new TableCell();
				TableCell errorCellSeverity = new TableCell();
				errorCellSeverityLeft.CssClass = "errorLeftSubIcon";
				errorCellSeverityLeft.Text = "&nbsp;";
				errorCellSeverity.CssClass = "errorTitle";
				errorCellSeverity.ColumnSpan = 2;
				Label errorLabelSeverity = new Label();
				//errorLabelSeverity.Text = er.seSeverityInfo;
				errorLabelSeverity.Text = WriteSeverityCode(er.SeverityCode);
				errorCellSeverity.Controls.Add(errorLabelSeverity);
				errorRowSeverity.Controls.Add(errorCellSeverityLeft);
				errorRowSeverity.Controls.Add(errorCellSeverity);
				errorTable.Controls.Add(errorRowSeverity);

				// message
				TableRow errorRowMessage = new TableRow();
				TableCell errorCellMessageLeft = new TableCell();
				TableCell errorCellMessage = new TableCell();
				errorCellMessageLeft.CssClass = "errorLeftCell";
				errorCellMessageLeft.Text = "&nbsp;";
				errorCellMessage.CssClass = "errorFriendlyMessage";
				errorCellMessage.ColumnSpan = 2;
				Label errorLabelMessage = new Label();

				if(er["Message"] !=System.DBNull.Value)
				{				
					errorLabelMessage.Text = er.Message;

                    // Add message to our string collection
                    messages.Add( er.Message );
				}				

				errorCellMessage.Controls.Add(errorLabelMessage);
				errorRowMessage.Controls.Add(errorCellMessageLeft);
				errorRowMessage.Controls.Add(errorCellMessage);
				errorTable.Controls.Add(errorRowMessage);

				// friendly message
				TableRow errorRowfriendlyMessage = new TableRow();
				TableCell errorCellfriendlyMessage = new TableCell();
				TableCell errorCellfriendlyMessageLeft = new TableCell();
				errorCellfriendlyMessage.CssClass = "errorDetailMessage";
				errorCellfriendlyMessageLeft.CssClass = "errorLeftCell";
				errorCellfriendlyMessage.ColumnSpan = 2;
				errorCellfriendlyMessageLeft.Text = "&nbsp;";
				Label errorLabelfriendlyMessage = new Label();

				if(er["FriendlyMessage"] !=System.DBNull.Value)
				{				
					errorLabelfriendlyMessage.Text = er.FriendlyMessage;
				}	
					
				errorCellfriendlyMessage.Controls.Add(errorLabelfriendlyMessage);
				errorRowfriendlyMessage.Controls.Add(errorCellfriendlyMessageLeft);
				errorRowfriendlyMessage.Controls.Add(errorCellfriendlyMessage);
				errorTable.Controls.Add(errorRowfriendlyMessage);

				// module info
				TableRow errorRowAssembly = new TableRow();
				TableCell errorCellAssembly = new TableCell();
				TableCell errorCellAssemblyLeft = new TableCell();
				errorCellAssembly.CssClass = "errorModule";
				errorCellAssembly.ColumnSpan = 2;
				errorCellAssemblyLeft.CssClass = "errorLeftCell";
				errorCellAssemblyLeft.Text = "&nbsp;";
				Label errorLabelAssembly = new Label();
				
				if(er["raisingAssemblyName"] !=System.DBNull.Value)
				{				
					errorLabelAssembly.Text = er.raisingAssemblyName;
				}	
				
				errorCellAssembly.Controls.Add(errorLabelAssembly);
				errorRowAssembly.Controls.Add(errorCellAssemblyLeft);
				errorRowAssembly.Controls.Add(errorCellAssembly);
				errorTable.Controls.Add(errorRowAssembly);			
			
				lastPackageInstanceId = new Guid(er.RelatedPackageInstanceId);
			}
			// still have last table to add
			if(stillPanelAndTableToAdd)
			{
				errorPanel.Controls.Add(errorTable);				
				Controls.Add(errorPanel);
			}
		}

		private bool CheckUniqueErrorControl(string errorControlName)
		{
			foreach(Control x in this.Controls)
			{
				if(x.ID == errorControlName)
				{
					return false;
				}
			}

			return true;
		}

		private string WriteSeverityCode(string code)
		{
			if(code == "WARN")
			{
				return ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Warning");
			}
			else
			{
				return ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Error");
			}
		}

		private TableRow CreateSectionRow(sectionMode mode, HtmlInputHidden hiddenControl)
		{
			string section = string.Empty;
			string sectionImageName= string.Empty;
			string sectionExpandedImageName = string.Empty;
			string sectionExpandedNoImage = string.Empty;

			if (hiddenControl.Value == "true")
			{
				sectionExpandedImageName = imagesPath + "/down.gif";
				sectionExpandedNoImage = "+";
			}
			else
			{
				sectionExpandedImageName = imagesPath + "/up.gif";
				sectionExpandedNoImage = "-";
			}

			TableRow sectionRow = new TableRow();
			table.Rows.Add(sectionRow);

			TableCell tcSection = new TableCell();

			if(imagesPath == string.Empty)
				tcSection.CssClass = "sectionLeft";
			else
				tcSection.CssClass = "sectionLeftImages";

			tcSection.ColumnSpan = 7;
			sectionRow.Cells.Add(tcSection);

			Label lSection = new Label();
			lSection.CssClass = "sectionLeftText";
			switch(mode)
			{
				case sectionMode.MyRecentTransactions:
                    if (recentTransactionTitle.Length > 0)
                    {
                        lSection.Text = recentTransactionTitle;
                    }
                    else
                    {
                        lSection.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("RecentTransactions");
                    }
                    section = "Recent";
					sectionImageName = "/trans1.gif";
					break;
				case sectionMode.MyTransactionsInProgress:
                    if (inProgressTitle.Length > 0)
                    {
                        lSection.Text = inProgressTitle;
                    }
                    else 
                    {
                        lSection.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("InProgressTransactions");
                    }
                    section = "InProgress";
					sectionImageName = "/trans2.gif";
					break;
				case sectionMode.MyHistory:
					 if (historyTitle.Length > 0)
                    {
                        lSection.Text = historyTitle;
                    }
                    else
                    {
                        lSection.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("MyHistory");
                    }
                    section = "History";
					sectionImageName = "/history.gif";
					break;
			}
			if(imagesPath != string.Empty)
			{
				Image iSection = new Image();
				iSection.ImageUrl = imagesPath + sectionImageName;
				tcSection.Controls.Add(iSection);
			}
            tcSection.Controls.Add(new LiteralControl("&nbsp;"));
			tcSection.Controls.Add(lSection);

			TableCell tcShowHide = new TableCell();
			if(imagesPath == string.Empty)
				tcShowHide.CssClass = "sectionRight";
			else
				tcShowHide.CssClass = "sectionRightImages";
			sectionRow.Cells.Add(tcShowHide);

			if(imagesPath == string.Empty)
			{
				Label lShowHide = new Label();
				lShowHide.Text = sectionExpandedNoImage;
				lShowHide.ID = "sec" + section;
				lShowHide.Attributes.Add("onclick", string.Format("SectionShowHideChildrenNoImage(document.getElementById('{0}'), '{1}', 'section', document.getElementById('{2}_{3}'));", hiddenControl.ClientID, section, this.ClientID, lShowHide.ClientID));
				tcShowHide.Controls.Add(lShowHide);
			}
			else
			{
				string imgPlusPath = imagesPath + "/down.gif";
				string imgMinusPath = imagesPath + "/up.gif";

				Image iShowHide = new Image();
				iShowHide.ID = "sec" + section;
				iShowHide.ImageUrl = sectionExpandedImageName;
				iShowHide.Attributes.Add("onclick", string.Format("SectionShowHideChildren(document.getElementById('{0}'), '{1}', 'section', '{2}', '{3}', document.getElementById('{4}_{5}'));", hiddenControl.ClientID, section, imgPlusPath, imgMinusPath, this.ClientID, iShowHide.ClientID));
				tcShowHide.Controls.Add(iShowHide);
			}
			return sectionRow;
		}

		private TableRow CreateHeaderRow(sectionMode mode, HtmlInputHidden hiddenControl)
		{
			string section = string.Empty;
			string cssClass = string.Empty;
			switch(mode)
			{
				case sectionMode.MyRecentTransactions:
					cssClass = "header";
					break;
				case sectionMode.MyTransactionsInProgress:
					cssClass = "header";
					break;
				case sectionMode.MyHistory:
					cssClass = "headerNoUnderline";
					break;
			}

			TableRow hdrRow = new TableRow();
			hdrRow.CssClass = cssClass;

			if(hiddenControl.Value == "true")
				hdrRow.Style.Add("display", "none");
			else
				hdrRow.Style.Add("display", "");

			table.Rows.Add(hdrRow);

			TableCell tchdrReference = new TableCell();
			tchdrReference.CssClass = cssClass;
			tchdrReference.ColumnSpan = 3;
			hdrRow.Cells.Add(tchdrReference);

			Label lhdrReference = new Label();
			lhdrReference.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Reference");
			tchdrReference.Controls.Add(lhdrReference);

			TableCell tchdrTransactions = new TableCell();
			tchdrTransactions.CssClass = cssClass;
            hdrRow.Cells.Add(tchdrTransactions);

			Label lhdrTransactions = new Label();

			switch(mode)
			{
				case sectionMode.MyRecentTransactions:
					lhdrTransactions.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Package");
					section = "Recent";
					break;
				case sectionMode.MyTransactionsInProgress:
					lhdrTransactions.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Package");
					section = "InProgress";
					break;
				case sectionMode.MyHistory:
					lhdrTransactions.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Package");
					section = "History";
					break;
			}

            // if set includePackageInfo to be true, add the Package header label
            if (includePackageInfo)
            {
                tchdrTransactions.Controls.Add(lhdrTransactions);
            }

			TableCell tchdrCreated = new TableCell();
			tchdrCreated.CssClass = cssClass;
			hdrRow.Cells.Add(tchdrCreated);

			Label lhdrCreated = new Label();
			lhdrCreated.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Date");
			tchdrCreated.Controls.Add(lhdrCreated);
					
			TableCell tchdrCompleted = new TableCell();
			tchdrCompleted.CssClass = cssClass;
			hdrRow.Cells.Add(tchdrCompleted);

			Label lhdrCompleted = new Label();
			lhdrCompleted.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Status");
			tchdrCompleted.Controls.Add(lhdrCompleted);

			TableCell tchdrDistibutor = new TableCell();
			tchdrDistibutor.CssClass = cssClass;
            hdrRow.Cells.Add(tchdrDistibutor);

            //if set includeDistributorInfo to be true, add the distributor header label in the header row
            if (includeDistributorInfo)
            {
                Label lhdrDistibutor = new Label();
                lhdrDistibutor.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Distributor");
                tchdrDistibutor.Controls.Add(lhdrDistibutor);
            }

			TableCell tchdrDelete = new TableCell();
			tchdrDelete.CssClass = cssClass;
			hdrRow.Cells.Add(tchdrDelete);

			Label lhdrDelete = new Label();
			lhdrDelete.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Delete");
			tchdrDelete.Controls.Add(lhdrDelete);

			hdrRow.Attributes.Add("section", section);

			return hdrRow;
		}

		private TableRow CreatePagingRow(sectionMode mode, HtmlInputHidden hiddenControl)
		{ 
			int pageCount = 0;
			int currentPage = 0;
			int previousPage = 0;
			int nextPage = 0;

			string section= string.Empty;

			switch(mode)
			{
				case sectionMode.MyTransactionsInProgress:
					if (inProgressRecordCount > 0 && inProgressPagingSize > 0)
					{
						pageCount = Convert.ToInt32(System.Math.Ceiling(Convert.ToDouble(inProgressRecordCount)/Convert.ToDouble(inProgressPagingSize)));
					}
					section = "InProgress";
					break;
				case sectionMode.MyHistory:
					if (historyRecordCount > 0 && historyPagingSize > 0)
					{
						pageCount = Convert.ToInt32(System.Math.Ceiling(Convert.ToDouble(historyRecordCount)/Convert.ToDouble(historyPagingSize)));
					}
					section = "History";
					break;
			}

			TableRow pgnRow = new TableRow();
			pgnRow.CssClass = "paging";

			if(hiddenControl.Value == "true")
				pgnRow.Style.Add("display", "none");
			else
				pgnRow.Style.Add("display", "");

			table.Rows.Add(pgnRow);

			TableCell tcPage = new TableCell();			
			tcPage.CssClass = "paging";
			tcPage.ColumnSpan = 8;
			pgnRow.Cells.Add(tcPage);

			LinkButton lbPrevious = new LinkButton();
			lbPrevious.EnableViewState = false;
			lbPrevious.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Previous")+"&nbsp;";

			LinkButton lbNext = new LinkButton();
			lbNext.EnableViewState = false;
			lbNext.Text = "&nbsp;" + ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Next");

			switch(mode)
			{
				case sectionMode.MyTransactionsInProgress:
					currentPage = currentInProgressPage;
					lbPrevious.CommandName = "InProgress";
					previousPage = currentInProgressPage - 1;
					lbPrevious.CommandArgument = previousPage.ToString();
					lbNext.CommandName = "InProgress";
					nextPage = currentInProgressPage + 1;
					lbNext.CommandArgument = nextPage.ToString();
					break;
				case sectionMode.MyHistory:
					currentPage = currentMyHistoryPage;
					lbPrevious.CommandName = "MyHistory";
					previousPage = currentMyHistoryPage - 1;
					lbPrevious.CommandArgument = previousPage.ToString();
					lbNext.CommandName = "MyHistory";
					nextPage = currentMyHistoryPage + 1;
					lbNext.CommandArgument = nextPage.ToString();
					break;
			}

			if (previousPage > 0)
			{
				tcPage.Controls.Add(lbPrevious);
				lbPrevious.Command += new CommandEventHandler(lbPage_Command);
			}

			// set what number to start at.
			int iStart = 1;

			if(currentPage > 10) // show 20 below the current page only.
			{
				iStart = currentPage - 10;
			}

			if(pageCount > 1)
			{
				for (int i = iStart;i <= pageCount && i < (iStart + 20); i++)
				{
					LinkButton lbPage = new LinkButton();
					lbPage.EnableViewState = false;
					lbPage.Text = i.ToString() + "&nbsp;";

					if (i == currentPage)
						lbPage.CssClass = "currentPage";
					else
						lbPage.CssClass = "notCurrentPage";

					switch(mode)
					{
						case sectionMode.MyTransactionsInProgress:
							lbPage.CommandName = "InProgress";
							break;
						case sectionMode.MyHistory:
							lbPage.CommandName = "MyHistory";
							break;
					}
					lbPage.CommandArgument = i.ToString();
					lbPage.ID = string.Format("{0}{1}", lbPage.CommandName, i);
					tcPage.Controls.Add(lbPage);
					lbPage.Command += new CommandEventHandler(lbPage_Command);				
				}
			}

			if (nextPage <= pageCount)
			{
				tcPage.Controls.Add(lbNext);
				lbNext.Command += new CommandEventHandler(lbPage_Command);
			}
			pgnRow.Attributes.Add("section", section);
			return pgnRow;
		}

		private TableRow CreateFooterRow(sectionMode mode)
		{
			bool displayFooterImage = false;

			TableRow ftrRow = new TableRow();
			ftrRow.CssClass = "footer";
			table.Rows.Add(ftrRow);

			switch(mode)
			{
				case sectionMode.MyRecentTransactions:
					displayFooterImage = false;
					break;
				case sectionMode.MyTransactionsInProgress:
					displayFooterImage = false;
					break;
				case sectionMode.MyHistory:
					displayFooterImage = true;
					break;
			}

			TableCell tcFooter = new TableCell();
			tcFooter.ColumnSpan = 8;
			ftrRow.Cells.Add(tcFooter);

			if(imagesPath == string.Empty || !displayFooterImage)
			{
				Label lfooter = new Label();
				lfooter.Text = "&nbsp;";
				tcFooter.Controls.Add(lfooter);
			}
			else
			{
				Image iFooter = new Image();
				iFooter.ImageUrl = imagesPath + "/Poweredbyperfectus.gif";
				tcFooter.Controls.Add(iFooter);
			}
			return ftrRow;
		}

		private void CreateMyRecentTransactions(sectionMode mode, HtmlInputHidden hiddenControl)
		{
			table.Rows.Add(CreateSectionRow(mode, hiddenControl));
			table.Rows.Add(CreateHeaderRow(mode, hiddenControl));
            if (status != null)
            {
                foreach (StatusSet.MostRecentRow mrr in status.MostRecent)
                {
                    string rowId = GetSectionString(mode) + mrr.PackageInstanceId.ToString() + mrr.PackageVersionNumber.ToString() + mrr.PackageRevisionNumber.ToString();
                    table.Rows.Add(CreateInstanceRow(mrr, mode, hiddenControl, rowId));
                    AddDistributorRows(new Guid(mrr.PackageInstanceId), mode, table, rowId);
                }
            }
			table.Rows.Add(CreateFooterRow(mode));
		}

		private void CreateMyTransactionsInProgress(sectionMode mode, HtmlInputHidden hiddenControl)
		{
			table.Rows.Add(CreateSectionRow(mode, hiddenControl));
			table.Rows.Add(CreateHeaderRow(mode, hiddenControl));
            if (status != null)
            {
                foreach (StatusSet.InProgressRow ipr in status.InProgress)
                {
                    table.Rows.Add(CreateInstanceRow(ipr, mode, hiddenControl, string.Empty));
                    //AddDistributorRows(ipr.PackageInstanceId,table,hiddenControl);
                }
            }
            table.Rows.Add(CreatePagingRow(mode, hiddenControl));
			table.Rows.Add(CreateFooterRow(mode));
		}

		private void CreateMyHistory(sectionMode mode, HtmlInputHidden hiddenControl)
		{
			table.Rows.Add(CreateSectionRow(mode,hiddenControl));
			table.Rows.Add(CreateHeaderRow(mode, hiddenControl));
            if (status != null)
            {
                foreach (StatusSet.MyHistoryRow pr in status.MyHistory)
                {
                    string rowId = GetSectionString(mode) + pr.PackageInstanceId.ToString() + pr.PackageVersionNumber.ToString() + pr.PackageRevisionNumber.ToString();
                    table.Rows.Add(CreateInstanceRow(pr, mode, hiddenControl, rowId));
                    AddDistributorRows(new Guid(pr.PackageInstanceId), mode, table, rowId);
                }
            }
			table.Rows.Add(CreatePagingRow(mode, hiddenControl));
			table.Rows.Add(CreateFooterRow(mode));							
		}

		private TableRow CreateInstanceRow(DataRow pir, sectionMode mode, HtmlInputHidden hiddenControl, string rowId)
		{			
			string section = GetSectionString(mode);	

			// Row
			TableRow pirRow = new TableRow();
			pirRow.CssClass = "instanceInfo";

			/// main show hide.
			if(hiddenControl.Value == "true")
				pirRow.Style.Add("display", "none");
			else
				pirRow.Style.Add("display", "");
			
			// child show hide	
			TableCell tcShowHide = new TableCell();
			pirRow.Cells.Add(tcShowHide);
			tcShowHide.CssClass = "instanceInfoPlusMinus";

			if(pir["InterviewState"].ToString() == "DONE" || pir["InterviewState"].ToString() == "FAIL" )
			{
				HtmlInputHidden hdnExpanded = new HtmlInputHidden();
				hdnExpanded.ID = "hdn" + rowId;
				hdnExpanded.Value = Page.Request.Form[this.ClientID + ":" + hdnExpanded.ID];
				Controls.Add(hdnExpanded);

				Label lShowHide = new Label();
				lShowHide.Text = "&nbsp;";
				tcShowHide.Controls.Add(lShowHide);				

				string imgPlusPath = imagesPath + "/plus.gif";
				string imgMinusPath = imagesPath + "/minus.gif";

				Image iShowHide = new Image();
				iShowHide.ID = "lbl" + rowId;
				if (hdnExpanded.Value == "true")
					iShowHide.ImageUrl = imagesPath + "/minus.gif";
				else
					iShowHide.ImageUrl = imagesPath + "/plus.gif";
				iShowHide.CssClass="showHidePointer";
				iShowHide.Attributes.Add("onclick", string.Format("PackageShowHideChildren(document.getElementById('{0}'), '{1}', 'item', '{2}', '{3}', document.getElementById('{4}_{5}'));", hdnExpanded.ClientID, "hdn" + rowId, imgPlusPath, imgMinusPath, this.ClientID, iShowHide.ClientID));
				tcShowHide.Controls.Add(iShowHide);				
			}
			

			//Image
			TableCell tcImage = new TableCell();
			tcImage.CssClass = "instanceInfoTransact";			
			if(imagesPath != string.Empty)
			{
				Image iInstance = new Image();
				iInstance.ImageUrl = imagesPath + "/transaction.gif";
				tcImage.Controls.Add(iInstance);
			}
			pirRow.Cells.Add(tcImage);

			//Package Instance Reference
			TableCell tcReference = new TableCell();
			tcReference.CssClass = "instanceInfoRef";
			pirRow.Cells.Add(tcReference);

            bool canRerun = 
                pir["InterviewState"].ToString() == "DONE" || 
                pir["InterviewState"].ToString() == "INTE" || 
                (pir["InterviewState"].ToString() == "FINI" ||
                Convert.IsDBNull(pir["InterviewState"]));

            bool wantClone = canRerun &&
                pir["InterviewState"].ToString() == "DONE" ||
                pir["InterviewState"].ToString() == "FINI";

			LinkButton lbReference = new LinkButton();
			lbReference.EnableViewState = false;
			lbReference.Text = pir["Reference"].ToString();

            if (canRerun && wantClone)
                lbReference.CssClass = "interviewClone";
            else if (canRerun )
                lbReference.CssClass = "interviewReference";
            else
                lbReference.CssClass = "interviewFailed";

			lbReference.CommandName = mode.ToString();
			lbReference.CommandArgument = String.Format ("{0}#;{1}",
                pir["PackageInstanceId"].ToString(),
                pir["InterviewState"].ToString());
			lbReference.ID = string.Format("{0}{1}", lbReference.CommandName, pir["PackageInstanceId"].ToString());
            lbReference.Enabled = canRerun;
			
			tcReference.Controls.Add(lbReference);
			lbReference.Command += new CommandEventHandler(lbInstanceReference_Command);

			Label lDate = new Label();
			lDate.Text = "<br>" + ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Created") +" : " + Convert.ToDateTime(pir["WhenCreated"]).ToLocalTime().ToString();
			lDate.CssClass = "packageInfo";
			tcReference.Controls.Add(lDate);

			//Parent Package
			TableCell tcParent = new TableCell();
			tcParent.CssClass = "instanceInfoPackage";
			pirRow.Cells.Add(tcParent);

			Label lParent = new Label();
			lParent.Text = pir["ParentPackageName"].ToString();
			lParent.CssClass = "parentPackageName";

            // if set includePackageInfo to be true, add the Package label in the table cell
            if (includePackageInfo)
            {
                tcParent.Controls.Add(lParent);
            }
				
			Label lVersion = new Label();
			lVersion.CssClass = "packageInfo";
			lVersion.Text = "<Br />" + ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Version") + " " + pir["PackageVersionNumber"].ToString();
            
            // if set includePackageInfo to be true, add the Package version label in the table cell
            if (includePackageInfo)
            {
                tcParent.Controls.Add(lVersion);
            }

			//Instance Created
			TableCell tcCreated = new TableCell();
			tcCreated.CssClass = "instanceInfoCreated";
			pirRow.Cells.Add(tcCreated);

			Label lCreated = new Label();
			lCreated.Text = Convert.ToDateTime(pir["WhenCreated"]).ToLocalTime().ToShortDateString();
			tcCreated.Controls.Add(lCreated);

			//Instance Status
			TableCell tcStatus = new TableCell();
			tcStatus.CssClass = "instanceInfoStatus";
			pirRow.Cells.Add(tcStatus);

			string statusLabel = string.Empty;
			string imageName = string.Empty;

			if(imagesPath != string.Empty)
			{
				switch (pir["InterviewState"].ToString())
				{
					case "DONE":
						imageName = "/complete.gif";
						break;
					case "FAIL":
						imageName = "/error.gif";
						tcStatus.Attributes.Add("onMouseOver","body.style.cursor = 'pointer';");
						tcStatus.Attributes.Add("onMouseOut","body.style.cursor = '';");
						tcStatus.Attributes.Add("onClick","toggleErrorBox('" + this.ClientID + "_error" + pir["PackageInstanceId"] + "', true, event)");	
						break;
                    case "FINI":
						imageName = "/queued.gif";
						break;
					default:
						//imageName = "/spacer.gif";
						imageName = "/progress.gif";
						break;
				}						
				
				Image iStatus = new Image();
				iStatus.ImageUrl = imagesPath + imageName;
				iStatus.ID = string.Format("{0}{1}{2}","image", lbReference.CommandName, pir["PackageInstanceId"].ToString());
				
				if(mode == sectionMode.MyRecentTransactions) // if we want to add to xmlHttpRequest dynamic status updates array.
				{
					_statusItemArray.Add(iStatus.ClientID);					
				}
				
				tcStatus.Controls.Add(iStatus);
				statusLabel = "<br/>";
			}
				
			Label lStatus = new Label();

			string strStatusCode = string.Empty;

			if(!Convert.IsDBNull(pir["InterviewState"]))
			{
				strStatusCode = pir["InterviewState"].ToString();				
			}
			else 
			{
                strStatusCode = "INTE";
			}

            //RDF 2/5/2013 removed as type InterviewStatus does not exist
            Perfectus.Server.Common.Data.StatusSet.InterviewStatusRow sRow = status.InterviewStatus.FindByInterviewState(strStatusCode);

            if (sRow != null) 
            statusLabel += status.InterviewStatus.FindByInterviewState(strStatusCode).Name.ToString();

			lStatus.Text = statusLabel;
			lStatus.ID = string.Format("{0}{1}{2}","label", lbReference.CommandName, pir["PackageInstanceId"].ToString());

			if(imageName != string.Empty)
			{
				lStatus.CssClass = "packageInfo";
			}
			
			tcStatus.Controls.Add(lStatus);

			//Instance Distributor
			//pirRow.Cells.Add(GetDistributorList(new Guid(pir["PackageInstanceId"].ToString())));
			TableCell placeHolderCell = new TableCell();
			placeHolderCell.Text = "&nbsp;";
			placeHolderCell.CssClass = "instanceInfo";
			pirRow.Cells.Add(placeHolderCell);

			//Instance Delete
			TableCell tcDelete = new TableCell();
			tcDelete.CssClass = "instanceInfoDelete";
			pirRow.Cells.Add(tcDelete);

			LinkButton lbDelete = new LinkButton();
            lbDelete.EnableViewState = false;
            
			if(imagesPath == string.Empty)
			{
				lbDelete.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("DeleteAbbreviation");
				lbDelete.CssClass = "instanceDel";
			}
			else
				lbDelete.Text = string.Format("<img src='{0}/delete.gif'border='0'/>", imagesPath);
			
			lbDelete.CommandName = "Delete";
			lbDelete.CommandArgument = pir["PackageInstanceId"].ToString();
			lbDelete.Command += new CommandEventHandler(lbDelete_Command);
            //Only perform the deletion when End User confirms that they really want to delete the instance.
            lbDelete.Attributes.Add("onClick", string.Format("javascript:return confirm('{0}')", ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("DeleteWarning")));
            
            tcDelete.Controls.Add(lbDelete);

			pirRow.Attributes.Add("section", section);

			return pirRow;
		}


		private void AddDistributorRows(Guid packageInstanceId, sectionMode mode, Table table, string rowId)
		{			
			string distributorName = string.Empty;
			string iconFileName = string.Empty;	
			int docCount = 0;
//			Guid lastTemplateId = Guid.Empty;
			
			string section = GetSectionString(mode);		

		
			foreach (StatusSet.DistributionResultRow dr in status.DistributionResult)
			{
				if (dr.PackageInstanceId.ToString() == packageInstanceId.ToString())
				{
					docCount += 1;
					// Row
					TableRow disResultRow = new TableRow();
					disResultRow.Attributes.Add("item", "hdn" + rowId);					

					disResultRow.CssClass = "disRow";
					disResultRow.Attributes.Add("section", section);

					//if(hiddenControl.Value == "true")
						disResultRow.Style.Add("display", "none");
					//else
					//	disResultRow.Style.Add("display", "visible");


					TableCell placeHolderCell = new TableCell();
					placeHolderCell.Text = "&nbsp;";
					placeHolderCell.ColumnSpan = 2;
					placeHolderCell.CssClass = "instanceInfo";

					TableCell docNameCell = new TableCell();
					docNameCell.ColumnSpan = 4;
					docNameCell.CssClass = "disDocName";

										
					if(dr["LinkText"] != System.DBNull.Value)
					{
						docNameCell.Text = "<img src=\"" + WriteImageByTemplateName(dr.LinkText) + "\" align=\"absMiddle\" /> ";

						if(dr["link"] != System.DBNull.Value && dr.Link.Length > 0)
						{
							docNameCell.Text += "<a href=\"" + dr.Link + "\" class=\"docNameLink\">";
						}

						docNameCell.Text += dr.LinkText;

						if(dr["link"] != System.DBNull.Value && dr.Link.Length > 0)
						{
							docNameCell.Text += "</a>";
						}
					}
					else if(dr["TemplateName"] != System.DBNull.Value)
					{
						docNameCell.Text = "<img src=\"" + WriteImageByTemplateName(dr.TemplateName) + "\" align=\"absMiddle\" /> ";
						docNameCell.Text += dr.TemplateName;
					}
				
					docNameCell.Text += "<br><span class=\"disCreated\">" + ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Distributed") +" : " + dr.WhenLoggedUtc.ToLocalTime().ToString() + "</span>";
					
					TableCell distListCell = new TableCell();
					distListCell.ColumnSpan = 2;
					distListCell.CssClass = "disInfo";

                    //if set includeDistributorInfo to be true, add the distributor image and name label in the table cell
                    if (includeDistributorInfo)
                    {
                        InterviewSystem.Status.GetDistributorDetails(new Guid(dr.DistributorId.ToString()), out distributorName, out iconFileName);
                        if (imagesPath != string.Empty)
                        {
                            Image i = new Image();
                            if (iconFileName == null)
                                iconFileName = "/spacer.gif";
                            i.ImageUrl = imagesPath + "/" + iconFileName;
                            i.Attributes.Add("align", "absmiddle");
                            distListCell.Controls.Add(i);
                        }
                        Label l = new Label();
                        l.Text = "&nbsp;" + distributorName + "<br/>";
                        l.CssClass = "packageInfo";
                        distListCell.Controls.Add(l);
                    }

					disResultRow.Cells.Add(placeHolderCell);
					disResultRow.Cells.Add(docNameCell);				
					disResultRow.Cells.Add(distListCell);
					
					table.Rows.Add(disResultRow);

//					lastTemplateId = dr.TemplateId;
				}
			}

			if(docCount == 0) // add empty row.
			{
				// Row
				TableRow disResultRow = new TableRow();
				disResultRow.Attributes.Add("item", "hdn" + rowId);					

				disResultRow.CssClass = "disRow";
				disResultRow.Attributes.Add("section", section);
				disResultRow.Style.Add("display", "none");

				TableCell placeHolderCell = new TableCell();
				placeHolderCell.Text = "&nbsp;";
				placeHolderCell.ColumnSpan = 2;
				placeHolderCell.CssClass = "instanceInfo";

				TableCell noDocCell = new TableCell();
				noDocCell.ColumnSpan = 6;
				noDocCell.CssClass = "disDocName";
				noDocCell.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("NoDocuments");

				disResultRow.Cells.Add(placeHolderCell);
				disResultRow.Cells.Add(noDocCell);

				table.Rows.Add(disResultRow);

			}
		}	
	
		private string GetSectionString(sectionMode mode)
		{
			string section = string.Empty;

			switch(mode)
			{
				case sectionMode.MyRecentTransactions:					
					section = "Recent";
					break;
				case sectionMode.MyTransactionsInProgress:					
					section = "InProgress";
					break;
				case sectionMode.MyHistory:
					section = "History";					
					break;
			}	

			return section;
		}

		private string WriteImageByTemplateName(string fileName)
		{
			if(fileName.ToUpper().IndexOf(".XML") > 0)
			{
				return imagesPath + "/xml.gif";
			}
			else if(fileName.ToUpper().IndexOf(".PDF") > 0)
			{
				return imagesPath + "/pdf.gif";
			}
			if(fileName.ToUpper().IndexOf(".DOC") > 0)
			{
				return imagesPath + "/word.gif";
			}
			else
			{
				return imagesPath + "/xml.gif";
			}
						
	}

		private void lbDelete_Command(object sender, CommandEventArgs e)
		{
            String state = String.Empty;
			System.Guid packageInstanceId = GuidFromEventArg(e, out state);
			InstanceManagement.DeleteInstance(packageInstanceId);

            // We tell the browser to redirect back to this url. This clears out the browser post buffer FB1526
            // Any state changes made in the page are not preserved for the new request, however this does appear
            // to be a problem because the current implementation does not entirely preserve the page state either.
            // (apart from the page number)
            Context.Response.Redirect( Context.Request.Url.PathAndQuery );

            //this.DataBind( );
		}

/*
		private void lbPackageName_Command(object sender, CommandEventArgs e)
		{
			string[] packageParts;
			Guid packageId;
			int packageVersionNumber;
			int packageRevisionNumber;
			packageParts = e.CommandArgument.ToString().Split('|');
			packageId = new Guid(packageParts[0]);
			packageVersionNumber = Convert.ToInt32(packageParts[1]);
			packageRevisionNumber = Convert.ToInt32(packageParts[2]);

			if (StartInterview != null)
			{
				StartInterview(this, new StartInterviewEventArgs(packageId,packageVersionNumber, packageRevisionNumber));
			}
		}

*/
        private Guid GuidFromEventArg(CommandEventArgs e, out String Status)
        {
            Status = String.Empty;
            int index = e.CommandArgument.ToString().IndexOf("#;");
            if (index > 0)
            {
                Status = e.CommandArgument.ToString().Substring(index + 2);
                return new Guid(e.CommandArgument.ToString().Substring(0, index));
            }
            return new Guid(e.CommandArgument.ToString());
        }

        private void lbInstanceReference_Command(object sender, CommandEventArgs e)
        {
            Guid packageInstanceId;

            String state;
            packageInstanceId = GuidFromEventArg(e, out state);
            if (ResumeInterview != null)
            {
                ResumeInterview(this, new ResumeInterviewEventArgs(packageInstanceId, state));
            }
        }

		private void lbPage_Command(object sender, CommandEventArgs e)
		{
			EnsureChildControls();
			switch(e.CommandName.ToString().ToUpper())
			{
				case "INPROGRESS":
					currentInProgressPage = Convert.ToInt32(e.CommandArgument);
					Controls.Clear();
                    //Keep the section open when click the page buttons
                    inProgressExpanded = true;
					CreateControls();
					break;
				case "MYHISTORY":
					currentMyHistoryPage = Convert.ToInt32(e.CommandArgument);
					Controls.Clear();
                    //Keep the section open when click the page buttons
                    historyExpanded = true;
					CreateControls();
					break;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
            Page.RegisterClientScriptBlock("lib", string.Format(@"<script type=""text/javascript"" src=""{0}/Perfectus_Status.js""></script>", javascriptPath));
			Page.RegisterClientScriptBlock("dynamicGlobals", string.Format(@"<script type=""text/javascript"">var statusControlId='{0}';</script>", this.ClientID));
			
			base.OnPreRender(e);
		}

		protected override void OnDataBinding(EventArgs e)
		{
			Page.Trace.Write("Perfectus", "DataBind called");

			Controls.Clear();
			ClearChildViewState();
			CreateControls();
			ChildControlsCreated = true;
		}

		private void openFromButton_Click(object sender, EventArgs e)
		{
			if (openerPlugin != null)
			{
//				Guid stopAtPageId = Guid.Empty;
				string docRef = Page.Request.Form[openerPlugin.DocRefFieldId];
				XmlDocument answerSetXml = openerPlugin.GetAnswerSet(docRef);

				LaunchFromAnswerSet(answerSetXml);
			}
		}

		private void LaunchFromAnswerSet(XmlDocument answerSetXml)
		{
            string packageIdString = answerSetXml.SelectSingleNode("/answerSet/@packageId").Value;
			string packageVersionString = answerSetXml.SelectSingleNode("/answerSet/@packageVersion").Value;
			string packageRevisionString = answerSetXml.SelectSingleNode("/answerSet/@packageRevision").Value;

            Guid packageId = new Guid(packageIdString);
			int version = Convert.ToInt32(packageVersionString);
			int revision = Convert.ToInt32(packageRevisionString);
	
			Guid instanceId = InstanceManagement.CreateInstance(packageId, version, revision);
			Package instance = InstanceManagement.GetInstance(instanceId);

            // Call routine to apply answerset (FB1403: Changed to use non obsolete class and same functionality as AnswerSetUpload control)
            Submitter.ApplyAnswerSet( instance, answerSetXml, Submitter.QuestionMatchModeOption.Name, false );

			InstanceManagement.SaveInstance(instanceId, instance);
	
			if (ResumeInterview != null)
                ResumeInterview(this, new ResumeInterviewEventArgs(instanceId));
		}

        public Guid CreateInterviewFromExisting(ResumeInterviewEventArgs e)
        {
            if ( e.State.Length == 0 || e.State == "INTE" )
                return e.PackageInstanceId;
            
            String reference = InstanceManagement.GetInstanceReference (e.PackageInstanceId);
            Guid newGuid = Guid.NewGuid();
            InstanceManagement.CopyPackageInstance(newGuid, e.PackageInstanceId, Identity.GetRunningUserId());

            return newGuid;
        }

        private void uploadButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (fileUp.PostedFile.InputStream.Length <= 0)
                    MessageBox(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Status.FailedNoAnswersetFile"));
                else
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(fileUp.PostedFile.InputStream);
                    LaunchFromAnswerSet(doc);
                }
            }
            catch
            {
                MessageBox(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Status.FailedXMLAnswerset"));
            }
        }

        private void MessageBox(string msg)
        {
            Label lbl = new Label();
            lbl.Text = "<script language='javascript'>" + Environment.NewLine + "window.alert('" + msg + "')</script>";
            Page.Controls.Add(lbl);
        }
    }
}
