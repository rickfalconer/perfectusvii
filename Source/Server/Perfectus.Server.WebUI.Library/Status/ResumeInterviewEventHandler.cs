using System;

namespace Perfectus.Server.WebUI.Library.Status
{
	/// <summary>
	/// Summary description for ResumeInterviewEventHandler.
	/// </summary>
	public delegate void ResumeInterviewEventHandler(object sender, ResumeInterviewEventArgs e);
}
