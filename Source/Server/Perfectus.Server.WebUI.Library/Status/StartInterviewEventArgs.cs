using System;

namespace Perfectus.Server.WebUI.Library.Status
{
	/// <summary>
	/// Summary description for StartInterviewEventArgs.
	/// </summary>
	public class StartInterviewEventArgs : EventArgs
	{
		private Guid packageId;
		private int packageVersionNumber;
		private int packageRevisionNumber;

		public Guid PackageId
		{
			get { return packageId; }
		}

		public int PackageVersionId
		{
			get { return packageVersionNumber; }
		}

		public int PackageRevisionId
		{
			get { return packageRevisionNumber; }
		}

		public StartInterviewEventArgs(Guid packageId, int packageVersionNumber, int packageRevisionNumber)
		{
			this.packageId = packageId;
			this.packageVersionNumber = packageVersionNumber;
			this.packageRevisionNumber = packageRevisionNumber;
		}
	}
}
