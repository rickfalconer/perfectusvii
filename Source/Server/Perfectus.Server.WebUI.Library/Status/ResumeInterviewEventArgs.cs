using System;

namespace Perfectus.Server.WebUI.Library.Status
{
	/// <summary>
	/// Summary description for ResumeInterviewEventArgs.
	/// </summary>
	public class ResumeInterviewEventArgs : EventArgs
	{
		private Guid packageInstanceId;
        private String state;

		public Guid PackageInstanceId
		{
			get { return packageInstanceId; }
		}

        public String State
        {
            get { return state; }
        }


        public ResumeInterviewEventArgs(Guid packageInstanceId)
            :this ( packageInstanceId, String.Empty)
		{
		}

        public ResumeInterviewEventArgs(Guid packageInstanceId, String state)
        {
            this.packageInstanceId = packageInstanceId;
            this.state = state;
        }
	}
}
