using System;

namespace Perfectus.Server.WebUI.Library.Status
{
	/// <summary>
	/// Summary description for StartInterviewEventHandler.
	/// </summary>
	public delegate void StartInterviewEventHandler(object sender, StartInterviewEventArgs e);
}
