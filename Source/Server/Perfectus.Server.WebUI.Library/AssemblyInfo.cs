using System;
using System.Reflection;
using System.Runtime.InteropServices;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server Web Library")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs