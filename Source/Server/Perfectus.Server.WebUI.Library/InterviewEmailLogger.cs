using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using Perfectus.Server.ConfigurationSystem;

namespace Perfectus.Server.WebUI.Library
{
    public class InterviewEmailLogger
    {
        private InterviewException interviewException;
        private string to;
        private string from;
        private string subject;
        private string smtpServer;

        public InterviewException InterviewException
        {
            get { return interviewException; }
            set { interviewException = value; }
        }

        public string To
        {
            get { return to;  }
            set { to = value; }
        }

        public string From
        {
            get { return from; }
            set { from = value; }
        }

        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public string SMTPServer
        {
            get { return smtpServer; }
            set { smtpServer = value; }
        }

        /// <summary>
        ///     Contructor - ensures the instance is in a valid state ready for sending.
        /// </summary>
        /// <param name="interviewException"></param>
        /// <param name="type"></param>
        /// <param name="eventID"></param>
        /// <param name="user"></param>
        /// <param name="computer"></param>
        /// <param name="stack"></param>
        public InterviewEmailLogger(InterviewException interviewException, string to, string from, string smtpServer)
        {
            if (interviewException == null || interviewException.Message.Length == 0 || interviewException.UIMessage.Length == 0)
            {
                throw new InvalidOperationException("The Interview Exception is invalid.");
            }

            if (to == null || to.Length == 0 || from == null || from.Length == 0 || smtpServer == null || smtpServer.Length == 0)
            {
                throw new InvalidOperationException("The Email settings (To | From | SMTPServer) are invalid.");
            }

            this.interviewException = interviewException;
            this.to = to;
            this.from = from;
            this.smtpServer = smtpServer;            
        }

        /// <summary>
        ///     Sends an email detailing the given InterviewException
        /// </summary>
        public void SendEmail(string subject, string title, string eventID, string user, string url)
        {
            // Define the email
            MailMessage mail = new MailMessage(new MailAddress(from), new MailAddress(to));
            mail.Subject = subject;
            mail.Body = GetMessageBody(title, eventID, user, url);

            // Send the email
            SmtpClient SmtpClient = new SmtpClient();
            SmtpClient.Host = smtpServer;
            SmtpClient.Send(mail);
        }

        /// <summary>
        ///     Formats the message body for the email
        /// </summary>
        /// <returns></returns>
        private string GetMessageBody(string title, string eventID, string user, string url)
        { 
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(interviewException.Title);
            sb.AppendLine("");
            sb.AppendLine("Error Details");
            sb.AppendLine("");
            sb.AppendLine(GetErrorDetail("Title", title));
            sb.AppendLine(GetErrorDetail("Date", DateTime.Now.ToLongDateString()));
            sb.AppendLine(GetErrorDetail("Time", DateTime.Now.ToLongTimeString()));
            sb.AppendLine(GetErrorDetail("Event ID", eventID));
            sb.AppendLine(GetErrorDetail("User", user));
            sb.AppendLine(GetErrorDetail("URL", url));
            sb.AppendLine("Description");
            sb.AppendLine(interviewException.UIMessage);
            sb.AppendLine(interviewException.Message);
            sb.AppendLine(GetErrorDetail("Exception Stack", interviewException.StackTrace));
            return sb.ToString();
        }

        /// <summary>
        ///     Formats the key / value detail line message
        /// </summary>
        /// <param name="key">The key of the error detail.</param>
        /// <param name="value">The value of the error detail.</param>
        /// <returns>The formated error message detail.</returns>
        private static string GetErrorDetail(string key, string value)
        {
            return string.Format("{0}: {1}", key, value);
        }
    }
}