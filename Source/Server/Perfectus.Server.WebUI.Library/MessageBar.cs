using System.Web.UI.WebControls;

namespace Perfectus.Server.WebUI.Library
{
	/// <summary>
	/// Summary description for MessageBar.
	/// </summary>
	public class MessageBar : WebControl
	{
	    private string text;
		public string Text
		{
			get
			{
			    return text;
			}
			set
			{
			    text = value;
			}
		}

		public MessageBar() : base("div")
		{
		}

		protected override void CreateChildControls()
		{
			base.CreateChildControls ();
			
			CssClass = "messageBar";
			Table headerTable = new Table();
			headerTable.ID = "msgBar";
			TableRow headerTableRow = new TableRow();
			TableCell leftIconCell = new TableCell();
			TableCell packageTitleCell = new TableCell();
			headerTableRow.Cells.Add(leftIconCell);
			headerTableRow.Cells.Add(packageTitleCell);
			headerTable.Rows.Add(headerTableRow);
			leftIconCell.CssClass = "leftIcon";
			packageTitleCell.CssClass = "webServiceMessage";
			Literal messageString = new Literal();
			messageString.Text = text;
			packageTitleCell.Controls.Add(messageString);
			Controls.Add(headerTable);

			ChildControlsCreated = true;
		}

	}
}
