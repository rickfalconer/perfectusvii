using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace Perfectus.Server.WebUI.Library
{
    // After porting to .Net 2 there were some access errors on existing code when trying to make a new instance of a WebControl.
    // Code that previously made new instances of a WebControl now make new instances of this class, which has access to call the constructor on Web Control.
    public class LibraryControl : WebControl
    {
        public LibraryControl(string name) : base(name)
        { 
        }
    }
}
