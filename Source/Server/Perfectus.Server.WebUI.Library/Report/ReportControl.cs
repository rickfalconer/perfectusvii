using System;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Server.Common.Data;
using Image = System.Web.UI.WebControls.Image;


namespace Perfectus.Server.WebUI.Library.Reporting
{
	/// <summary>
	/// Summary description for Report.
	/// </summary>
	/// 

	[ToolboxData("<{0}:Report runat=server></{0}:Report>")]
	public class ReportControl : WebControl
	{		
		private string imagesPath = string.Empty;
		private Guid libraryItemId = Guid.Empty;
		private string libraryItemType = string.Empty;
		private string libraryItemName = string.Empty;
		private ReportTypes.PerfectusReportType reportType = ReportTypes.PerfectusReportType.None;
		private string packageName = string.Empty;
		private Guid packageId = Guid.Empty;
		private string clauseId = string.Empty;
		private string clauseName = string.Empty;
		ReportBase report;
				
		[Browsable(true)]
		public string ImagesPath
		{
			get { return imagesPath; }
			set { imagesPath = value; }
		}	

		public Guid LibraryItemId
		{
			get { return libraryItemId; }
			set { libraryItemId = value; }
		}

		public string LibraryItemName
		{
			get { return libraryItemName; }
			set { libraryItemName = value; }
		}	

		public string LibraryItemType
		{
			get { return libraryItemType; }
			set { libraryItemType = value; }
		}

		public string PackageName
		{
			get { return packageName; }
			set { packageName = value; }
		}

		public string ClauseId
		{
			get { return clauseId; }
			set { clauseId = value; }
		}

		public string ClauseName
		{
			get { return clauseName; }
			set { clauseName = value; }
		}

		public Guid PackageId
		{
			get { return packageId; }
			set { packageId = value; }
		}
	
		public ReportTypes.PerfectusReportType ReportType
		{
			get 
			{ 
				return reportType; 
			}
			set 
			{
				reportType = value;
			}
		}

		public ReportControl() : base("div")
		{
			
		}
	
		protected override void OnDataBinding(EventArgs e)
		{		
			Controls.Clear();
			ClearChildViewState();
			CreateControls();
			ChildControlsCreated = true;
		}
				
		private void CreateControls()
		{	
			switch(reportType)
			{
				case ReportTypes.PerfectusReportType.AllClauses:
					//TODO:
					report = new AllClauses(imagesPath);
					this.Controls.Add(report);
					break;
				case ReportTypes.PerfectusReportType.PackagesUsingClause:				
					report = new PackagesByClause(libraryItemId, imagesPath, clauseName, clauseId);					
					this.Controls.Add(report);
					break;
				case ReportTypes.PerfectusReportType.InstancesUsingClause:				
					report = new InstancesUsingClauseReport(clauseId, clauseName,imagesPath, packageId, packageName);					
					this.Controls.Add(report);
					break;
				case ReportTypes.PerfectusReportType.PackagesUsingLibraryItem:
					report = new PackagesUsingLibraryItemReport(libraryItemId, imagesPath, libraryItemName, libraryItemType);					
					this.Controls.Add(report);
					break;
				case ReportTypes.PerfectusReportType.InstancesUsingLibraryItem:				
					report = new InstancesUsingLibraryItemReport(libraryItemId, imagesPath, libraryItemName, libraryItemType, packageId, packageName);					
					this.Controls.Add(report);
					break;	
			
			}			
		}	

	}

	public class ReportTypes 
	{		
		public enum PerfectusReportType
		{
			AllClauses,
			PackagesUsingClause,
			InstancesUsingClause,
			PackagesUsingLibraryItem,
			InstancesUsingLibraryItem,
			None
		}
	}
}
