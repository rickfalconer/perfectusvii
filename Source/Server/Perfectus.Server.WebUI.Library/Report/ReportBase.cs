using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using eWorld.UI;
using Perfectus.Common;

namespace Perfectus.Server.WebUI.Library.Reporting
{
	/// <summary>
	/// Summary description for ReportBase.
	/// </summary>
	public abstract class ReportBase : WebControl
	{
		protected Table table = null;
		protected string m_imagesPath = string.Empty;
		protected string m_scriptPath = "../script"; //TODO:
		protected int maxResults = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.maximumSearchResults;

		public string ImagesPath 
		{
			get { return m_imagesPath;}
			set {m_imagesPath = value;}
		}
		
		public ReportBase()
		{
		}
	
		protected virtual void CreateTopBar(string title, bool createBackButton)
		{
            LibraryControl c = new LibraryControl("div");
			c.CssClass = "reportsTop";
			Table headerTable = new Table();
			headerTable.ID = "hdr";
			TableRow headerTableRow = new TableRow();
			TableCell packageTitleCell = new TableCell();

			Image iReport = new Image();
			iReport.CssClass = "iconLeft";
			iReport.ImageUrl = m_imagesPath + "/reports/report.gif";
			packageTitleCell.Controls.Add(iReport);		
	
			Label headerLabel = new Label();
			headerLabel.Text = "Report: " + title;
			packageTitleCell.Controls.Add(headerLabel);
			
			headerTableRow.Cells.Add(packageTitleCell);		
						
			packageTitleCell.CssClass = "packageTitle";

			// create refersh button
			TableCell refreshCell = new TableCell();
			refreshCell.CssClass = "refreshBack";
			refreshCell.Attributes.Add("onMouseOver","body.style.cursor = 'pointer';");
			refreshCell.Attributes.Add("onMouseOut","body.style.cursor = '';");
			refreshCell.Attributes.Add("onClick","location.reload()");
			headerTableRow.Cells.Add(refreshCell);
			
			// create back button
			if(createBackButton)
			{
				TableCell rightOptions = new TableCell();
				rightOptions.CssClass = "rightBack";
				rightOptions.Attributes.Add("onMouseOver","body.style.cursor = 'pointer';");
				rightOptions.Attributes.Add("onMouseOut","body.style.cursor = '';");
				rightOptions.Attributes.Add("onClick","history.back()");
				headerTableRow.Cells.Add(rightOptions);				
			}

			headerTable.Rows.Add(headerTableRow);
			c.Controls.Add(headerTable);
			
			this.Controls.Add(c);
		}

		protected string GetLibItemTypeImage(string itemType)
		{
			//TODO: Names from enum
			switch(itemType.ToLower())
			{
				case "question":
					return "question.gif";
				
				case "template":
					return "text.gif";
				
				case "outcome":
					return "outcome.gif";
				
				case "function":
					return "function.gif";

				case "simpleoutcome":
					return "soutcome.gif";

				case "interviewpage":
				return "ipage.gif";
								
				default :
					return "spacer.gif";				
			}
			
		}

		protected CalendarPopup GetCalendarControl()
		{
			CalendarPopup calendar = new CalendarPopup();
			calendar.Enabled = Enabled;			
			calendar.ShowClearDate = true;
			calendar.ShowGoToToday = true;
			calendar.UseExternalResource = true;
			calendar.SelectedDateStyle = null;
			calendar.ButtonStyle = null;
			calendar.ClearDateStyle = null;
			calendar.DayHeaderStyle = null;
			calendar.GoToTodayStyle = null;
			calendar.HolidayStyle = null;
			calendar.MonthHeaderStyle = null;
			calendar.OffMonthStyle = null;
			calendar.SelectedDateStyle = null;
			calendar.TextboxLabelStyle = null;
			calendar.TodayDayStyle = null;
			calendar.WeekdayStyle = null;
			calendar.WeekendStyle = null;

			this.CssClass = "calendar";
			calendar.CssClass = "calendarNotpopped";
			calendar.ButtonStyle.CssClass = "button";
			calendar.SelectedDateStyle.CssClass = "selectedDate";
			calendar.ClearDateStyle.CssClass = "clearDate";
			calendar.DayHeaderStyle.CssClass = "dayHeader";
			calendar.GoToTodayStyle.CssClass = "goToToday";
			calendar.HolidayStyle.CssClass = "holiday";
			calendar.MonthHeaderStyle.CssClass = "monthHeader";
			calendar.OffMonthStyle.CssClass = "offMonth";
			calendar.SelectedDateStyle.CssClass = "selectedDate";
			calendar.TextboxLabelStyle.CssClass = "textboxLabel";
			calendar.TodayDayStyle.CssClass = "todayDate";
			calendar.WeekdayStyle.CssClass = "weekday";
			calendar.WeekendStyle.CssClass = "weekend";
		
			calendar.Nullable = true;

			calendar.ExternalResourcePath = string.Format("{0}/eWorld_UI_CalendarPopup.js", m_scriptPath);

			return calendar;		
		}

		protected TableRow GetNoRecordsFoundRow(int colSpan)
		{
			Label lmessage = new Label();
			lmessage.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("NoMatchingData");
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.CssClass = "noRecordCell";
			cell.ColumnSpan = colSpan;

			cell.Controls.Add(lmessage);
			row.Cells.Add(cell);

			return row;
		}

		protected TableRow GetMaxResultsFoundRow(int colSpan)
		{
			Label lmessage = new Label();
			lmessage.Text = maxResults + ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("MaxAllowed") + " ";
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.CssClass = "noRecordCell";
			cell.ColumnSpan = colSpan;

			cell.Controls.Add(lmessage);
			row.Cells.Add(cell);

			return row;
		}

		protected TableRow GetPrintRow(int colSpan)
		{		

			//print button

			Image image = new Image();
			
			image.Attributes.Add("onMouseOver","body.style.cursor = 'pointer';");
			image.Attributes.Add("onMouseOut","body.style.cursor = '';");
			image.Attributes.Add("onClick","window.print()");
			image.ImageUrl = m_imagesPath + "/reports/print.gif";
				
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.CssClass = "printCell";
			cell.ColumnSpan = colSpan;
			cell.HorizontalAlign = HorizontalAlign.Right;
			
			cell.Controls.Add(image);
			row.Cells.Add(cell);

			return row;
		}

		protected TableRow GetAdvancedSearchOptionsRow(int colSpan)
		{
			// max results

			Label lblMax = new Label();
			lblMax.Text = "Max Results: ";

			DropDownList ddl = new DropDownList();
			ddl.Items.Add("20");
			ddl.Items.Add("50");
			ddl.Items.Add("100");
			ddl.Items.Add("200");
			ddl.Items.Add("All");

			ddl.SelectedIndex = 1;	

			// Order By

			Label lblOrder = new Label();
			lblOrder.Text = " Order by: ";

			DropDownList ddl2 = new DropDownList();
			ddl2.Items.Add("Descending");
			ddl2.Items.Add("Ascending");
	
			ddl2.SelectedIndex = 0;	

				
			TableRow row = new TableRow();
			TableCell cell = new TableCell();
			cell.CssClass = "printCell";
			cell.ColumnSpan = colSpan;
			cell.HorizontalAlign = HorizontalAlign.Right;
			
			// add controls
			cell.Controls.Add(lblMax);
			cell.Controls.Add(ddl);
			cell.Controls.Add(lblOrder);
			cell.Controls.Add(ddl2);
	
			row.Cells.Add(cell);

			return row;
		}

	}
}
