using System;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Server.Common.Data;
using Image = System.Web.UI.WebControls.Image;
using eWorld.UI;

namespace Perfectus.Server.WebUI.Library.Reporting
{
	/// <summary>
	/// Summary description for PackagesUsingLibraryItemReport.
	/// </summary>
	public class AllClauses : ReportBase
	{
		private ReportSet reportSet;
		private TextBox txtClauseName;
		private TextBox txtClauseId;
		private TextBox txtClauseLibrary;
        
		public AllClauses(string imagesPath)
		{		
			m_imagesPath = imagesPath;
			table = new Table();
			table.CssClass = "reportTable";			
			CreateTopBar("Clauses", false);
			GetData(null, null, null);
			table.Rows.Add(GetItemNameAndSearchRow());			
			BuildReportTable();
			Controls.Add(table);
		}

		private void GetData(string clauseName, string clauseId, string clauseLibrary)
		{
			reportSet = Perfectus.Server.Reporting.Report.GetClauses(clauseId, clauseName, clauseLibrary);
		}

		private TableRow GetItemNameAndSearchRow()
		{
			TableRow tr = new TableRow();			

			//Search controls
			TableCell tcSearch = new TableCell();
			tcSearch.ColumnSpan = 1;
			tcSearch.CssClass = "searchCell";
			tcSearch.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;
		
			Label lClauseName = new Label();
			lClauseName.Text = "Clause: ";
			tcSearch.Controls.Add(lClauseName);
			
			txtClauseName = new TextBox();
			tcSearch.Controls.Add(txtClauseName);

			Label lClauseID = new Label();
			lClauseID.Text = " Id: ";
			tcSearch.Controls.Add(lClauseID);

			txtClauseId = new TextBox();
			tcSearch.Controls.Add(txtClauseId);

			Label lClauseLibrary = new Label();
			lClauseLibrary.Text = "Library: ";
			tcSearch.Controls.Add(lClauseLibrary);

			txtClauseLibrary = new TextBox();
			tcSearch.Controls.Add(txtClauseLibrary);

			Button btnSearch = new Button();
			btnSearch.Text = "Go";
			btnSearch.ID = "btnSearch";
			btnSearch.CssClass = "btnGo";
			btnSearch.Click +=new EventHandler(btnSearch_Click);
			tcSearch.Controls.Add(btnSearch);

			// end search controls
			tr.Cells.Add(tcSearch);
		
			return tr;
		}
	

		private void BuildReportTable()
		{
			// remove any table already called reportTable as on postback BuildReportTable() will be called again.
			for(int i = 0; i < table.Controls.Count; i ++)
			{
				Control c = table.Controls[i];
				if(c.ID == "reportTable" || c.ID == "reportHolderRow" || c.ID == "reportHolderCell")
				{
					table.Controls.Remove(c);
					i --;
				}
			}

			Table reportTable = new Table();
			reportTable.ID = "reportTable"; 
			reportTable.CssClass = "reportInnerTable";
           	
			reportTable.Rows.Add(GetHeaderRow());
            			

			if(reportSet.Clauses.Count == 0)
			{
				reportTable.Rows.Add(GetNoRecordsFoundRow(3));
			}
			else
			{
				int count = 0;
				foreach(ReportSet.ClausesRow rr in reportSet.Clauses)	
				{
					count = count + 1;
					if(count > maxResults)
					{
						reportTable.Rows.Add(GetMaxResultsFoundRow(3));
						break;
					}

					TableRow row = GetRowFromDataRow(rr);
					if(row != null)
					{
						reportTable.Rows.Add(row);
					}
				}
			}

			reportTable.Rows.Add(GetPrintRow(3));

			// add the table to the global table, as a nested table.
			TableRow tableHolderRow = new TableRow();
			tableHolderRow.ID = "reportHolderRow";
			TableCell tableHolderCell = new TableCell();
			tableHolderCell.ID = "reportHolderCell";
			tableHolderCell.ColumnSpan = 2;

			tableHolderRow.Controls.Add(tableHolderCell);
			tableHolderCell.Controls.Add(reportTable);

			table.Controls.Add(tableHolderRow);
		}

		private TableRow GetRowFromDataRow(ReportSet.ClausesRow dr)
		{		
			if(dr["clauseName"] == System.DBNull.Value || dr["clauseId"] == System.DBNull.Value || dr["clauseLibrary"] == System.DBNull.Value)
			{
				return null;
			}

			// Row
			TableRow row = new TableRow();
			row.CssClass = "reportInfo";

			//Clause Name
			TableCell tcClauseName = new TableCell();
			tcClauseName.CssClass = "reportInfo";
			Label lName = new Label();	
			lName.Text = GetClauseLink(dr.clauseName, dr.clauseId);

			lName.CssClass = "parentPackageName";
			Image iClause = new Image();
			iClause.CssClass = "iconLeft";
			iClause.ImageUrl = m_imagesPath + "/reports/text.gif";
			tcClauseName.Controls.Add(iClause);
			tcClauseName.Controls.Add(lName);
			row.Cells.Add(tcClauseName);
			
			//Clause ID
			TableCell tcClauseId = new TableCell();
			tcClauseId.CssClass = "reportInfo";
			Label lClauseId = new Label();
			lClauseId.Text = dr.clauseId;
			tcClauseId.Controls.Add(lClauseId);
			row.Cells.Add(tcClauseId);

			// Clause Library
			TableCell tcClauseLibrary = new TableCell();
			tcClauseLibrary.CssClass = "reportInfo";
			Label lClauseLibrary = new Label();

			lClauseLibrary.Text = dr.clauseLibrary;
			tcClauseLibrary.Controls.Add(lClauseLibrary);
			row.Cells.Add(tcClauseLibrary);

			return row;
		}

		private string GetClauseLink(string clauseName, string clauseId)
		{
			//TODO: cant hardcode report.aspx like this.			
			return String.Format("<A class='packageLink' href='report.aspx?report=packagesByClause&clauseName={0}&clauseId={1}'>{0}</a>", clauseName, clauseId);
		}
		
		private TableRow GetHeaderRow()
		{			

			TableRow hdrRow = new TableRow();
			hdrRow.CssClass = "header";		

			table.Rows.Add(hdrRow);

			TableCell tchdrClause = new TableCell();
			tchdrClause.CssClass = "header";
			hdrRow.Cells.Add(tchdrClause);

			Label lhdrClause = new Label();
			lhdrClause.Text = "Clause";
			tchdrClause.Controls.Add(lhdrClause);

			TableCell tchdrId = new TableCell();
			tchdrId.CssClass = "header";
			hdrRow.Cells.Add(tchdrId);

			Label lhdrId = new Label();
			lhdrId.Text = "Clause Id";
			tchdrId.Controls.Add(lhdrId);
					
			TableCell tchdrLibrary = new TableCell();
			tchdrLibrary.CssClass = "header";
			hdrRow.Cells.Add(tchdrLibrary);

			Label lhdrLibrary = new Label();
			lhdrLibrary.Text = "Library";
			tchdrLibrary.Controls.Add(lhdrLibrary);

			return hdrRow;
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			//			DateTime endDate = cal2.SelectedDate;
			//			endDate = endDate.AddDays(1);
			//
			//			if(cal1.SelectedDate > cal2.SelectedDate)
			//			{
			//				System.Web.HttpContext.Current.Response.Write("the end date must come after the start date");
			//			}
			//			else
			//			{
			GetData(txtClauseName.Text, txtClauseId.Text, txtClauseLibrary.Text);
			BuildReportTable();
			//			}
		}
	}
}
