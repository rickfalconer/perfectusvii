using System;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Server.Common.Data;
using Image = System.Web.UI.WebControls.Image;
using eWorld.UI;

namespace Perfectus.Server.WebUI.Library.Reporting
{
	/// <summary>
	/// Summary description for PackagesUsingLibraryItemReport.
	/// </summary>
	public class PackagesUsingLibraryItemReport : ReportBase
	{
		private Guid itemId;
		private ReportSet reportSet;
		private string itemName;
		private string itemType;
		private CalendarPopup cal1;
		private CalendarPopup cal2;
		private TextBox txtPackage;
        
		public PackagesUsingLibraryItemReport(Guid libraryItemId, string imagesPath, string libraryItemName, string libraryItemType)
		{
			itemId = libraryItemId;
			itemName = libraryItemName;
			itemType = libraryItemType;
			m_imagesPath = imagesPath;
			table = new Table();
			table.CssClass = "outsideReportTable";			
			CreateTopBar("Shared Library Elements", false);
			GetData(null, DateTime.MinValue, DateTime.MinValue);
			table.Rows.Add(GetSearchRow());			
			table.Rows.Add(GetItemNameRow());			
			BuildReportTable();
			Controls.Add(table);
		}

		private void GetData(string packageName, DateTime startDate, DateTime endDate)
		{
			reportSet = Perfectus.Server.Reporting.Report.GetPackagesBySharedLibraryElement(itemId, packageName, startDate, endDate);
		}

		private TableRow GetSearchRow()
		{

			TableRow tr = new TableRow();

			//Search controls
			TableCell tcSearch = new TableCell();
			tcSearch.ColumnSpan = 1;
			tcSearch.CssClass = "searchCell";
			tcSearch.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;
		
			Label lPackage = new Label();
			lPackage.Text = "Package: ";
			tcSearch.Controls.Add(lPackage);
			
			txtPackage = new TextBox();
			tcSearch.Controls.Add(txtPackage);

			Label lID = new Label();
			lID.Text = " Between: ";
			tcSearch.Controls.Add(lID);

			cal1 = GetCalendarControl();
			cal1.ID = "cal1";
			tcSearch.Controls.Add(cal1);

			Label lLibrary = new Label();
			lLibrary.Text = "And: ";
			tcSearch.Controls.Add(lLibrary);

			cal2 = GetCalendarControl();
			cal2.ID = "cal2";
			tcSearch.Controls.Add(cal2);

			// Go button
			Button btnSearch = new Button();
			btnSearch.Text = "Go";
			btnSearch.ID = "btnSearch";
			btnSearch.CssClass = "btnGo";
			btnSearch.Click +=new EventHandler(btnSearch_Click);
			tcSearch.Controls.Add(btnSearch);

			// end search controls
			tr.Cells.Add(tcSearch);
		
			return tr;
		}

		private TableRow GetItemNameRow()
		{

			TableRow tr = new TableRow();

			TableCell itemNameCell = new TableCell();			
			itemNameCell.CssClass = "LibraryItemName";
			string imageLink = String.Format("<img src='{0}' /> ", this.m_imagesPath + "/" + GetLibItemTypeImage(itemType));
			itemNameCell.Text = imageLink + " " + itemName;

			tr.Cells.Add(itemNameCell);
			return tr;
		}
	

		private void BuildReportTable()
		{
			// remove any table already called reportTable as on postback BuildReportTable() will be called again.
			for(int i = 0; i < table.Controls.Count; i ++)
			{
				Control c = table.Controls[i];
				if(c.ID == "reportTable" || c.ID == "reportHolderRow" || c.ID == "reportHolderCell")
				{
					table.Controls.Remove(c);
					i --;
				}
			}

			Table reportTable = new Table();
			reportTable.ID = "reportTable"; 
			reportTable.CssClass = "reportInnerTable";
           	
			reportTable.Rows.Add(GetHeaderRow());
            
			if(reportSet.PackagesUsingLibraryItem.Count == 0)
			{
				reportTable.Rows.Add(GetNoRecordsFoundRow(5));
			}
			else
			{
				int count = 0;
				foreach(ReportSet.PackagesUsingLibraryItemRow rr in reportSet.PackagesUsingLibraryItem)	
				{
					count = count + 1;
					if(count > maxResults)
					{
						reportTable.Rows.Add(GetMaxResultsFoundRow(6));
						break;
					}
					reportTable.Rows.Add(GetRowFromDataRow(rr));	
				}
			}

			reportTable.Rows.Add(GetPrintRow(5));
			
			// add the table to the global table, as a nested table.
			TableRow tableHolderRow = new TableRow();
			tableHolderRow.ID = "reportHolderRow";
			TableCell tableHolderCell = new TableCell();
			tableHolderCell.ID = "reportHolderCell";
			tableHolderCell.ColumnSpan = 1;

			tableHolderRow.Controls.Add(tableHolderCell);
			tableHolderCell.Controls.Add(reportTable);

			table.Controls.Add(tableHolderRow);
		}

		private TableRow GetRowFromDataRow(DataRow dr)
		{			
			// Row
			TableRow row = new TableRow();
			row.CssClass = "reportInfo";

			//Package Name
			TableCell tcPackageName = new TableCell();
			tcPackageName.CssClass = "reportInfo";
			Label lName = new Label();
			lName.Text = GetInstanceLink(dr["Name"].ToString(), dr["PackageId"].ToString());
			lName.CssClass = "parentPackageName";
			Image iPackage = new Image();
			iPackage.CssClass = "iconLeft";
			iPackage.ImageUrl = m_imagesPath + "/reports/package.gif";
			tcPackageName.Controls.Add(iPackage);
			tcPackageName.Controls.Add(lName);
			row.Cells.Add(tcPackageName);
			
			//Package published date
			TableCell tcPublished = new TableCell();
			tcPublished.CssClass = "reportInfo";
			Label lPublished = new Label();
			DateTime whenCreated = Convert.ToDateTime(dr["WhenCreated"]).ToLocalTime();
			lPublished.Text = whenCreated.ToShortDateString() + "<br />" + whenCreated.ToShortTimeString();
			tcPublished.Controls.Add(lPublished);
			row.Cells.Add(tcPublished);

			//Author
			TableCell tcAuthor = new TableCell();
			tcAuthor.CssClass = "reportInfo";
			Label lAuthor = new Label();
			lAuthor.Text = dr["PublishedBy"].ToString();
			tcAuthor.Controls.Add(lAuthor);
			row.Cells.Add(tcAuthor);

			//Server
			TableCell tcServer = new TableCell();
			tcServer.CssClass = "reportInfo";
			Label lServer = new Label();
			lServer.Text = dr["ServerName"].ToString();
			tcServer.Controls.Add(lServer);
			row.Cells.Add(tcServer);

			return row;
		}

		private string GetInstanceLink(string packageName, string packageId)
		{
			//TODO: don't hardcode report.aspx like this.			
			return String.Format("<A class='packageLink' href='report.aspx?report=InstancesUsingLibraryItem&itemId={0}&itemName={1}&itemType={2}&packageId={3}&packageName={4}'>{4}</a>", itemId, itemName, itemType, packageId, packageName);
		}
		
		private TableRow GetHeaderRow()
		{			

			TableRow hdrRow = new TableRow();
			hdrRow.CssClass = "header";		

			table.Rows.Add(hdrRow);

			TableCell tchdrReference = new TableCell();
			tchdrReference.CssClass = "header";
			hdrRow.Cells.Add(tchdrReference);

			Label lhdrReference = new Label();
			lhdrReference.Text = "Package";
			tchdrReference.Controls.Add(lhdrReference);


			TableCell tchdrCreated = new TableCell();
			tchdrCreated.CssClass = "header";
			hdrRow.Cells.Add(tchdrCreated);

			Label lhdrCreated = new Label();
			lhdrCreated.Text = "Package Created";
			tchdrCreated.Controls.Add(lhdrCreated);
					
			TableCell tchdrCompleted = new TableCell();
			tchdrCompleted.CssClass = "header";
			hdrRow.Cells.Add(tchdrCompleted);

			Label lhdrCompleted = new Label();
			lhdrCompleted.Text = "Author";
			tchdrCompleted.Controls.Add(lhdrCompleted);

			TableCell tchdrDistibutor = new TableCell();
			tchdrDistibutor.CssClass = "header";
			hdrRow.Cells.Add(tchdrDistibutor);

			Label lhdrDistibutor = new Label();
			lhdrDistibutor.Text = "ProcessManager";
			tchdrDistibutor.Controls.Add(lhdrDistibutor);

			TableCell tchdrDelete = new TableCell();
			tchdrDelete.CssClass = "header";
			hdrRow.Cells.Add(tchdrDelete);

			return hdrRow;
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
//			DateTime endDate = cal2.SelectedDate;
//			endDate = endDate.AddDays(1);
//
//			if(cal1.SelectedDate > cal2.SelectedDate)
//			{
//				System.Web.HttpContext.Current.Response.Write("the end date must come after the start date");
//			}
//			else
//			{
				GetData(txtPackage.Text, cal1.SelectedDate, cal2.SelectedDate);
				BuildReportTable();
//			}
		}
	}
}
