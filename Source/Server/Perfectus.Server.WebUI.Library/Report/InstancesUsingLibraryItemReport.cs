using System;
using System.ComponentModel;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Perfectus.Server.Common.Data;
using Image = System.Web.UI.WebControls.Image;
using eWorld.UI;
using Perfectus.Common;

namespace Perfectus.Server.WebUI.Library.Reporting
{
	/// <summary>
	/// Summary description for InstancesUsingLibraryItemReport.
	/// </summary>
	public class InstancesUsingLibraryItemReport : ReportBase
	{
		private Guid itemId;
		private ReportSet reportSet;
		private string itemName;
		private string itemType;
		private Guid packageId;
		private string packageName;
		private CalendarPopup cal1;
		private CalendarPopup cal2;
		private TextBox txtReference;
        
		public InstancesUsingLibraryItemReport(Guid libraryItemId, string imagesPath, string libraryItemName, string libraryItemType, Guid pId, string pName)
		{
			itemId = libraryItemId;
			itemName = libraryItemName;
			itemType = libraryItemType;
			packageId = pId;
			packageName = pName;
			m_imagesPath = imagesPath;
			table = new Table();
			table.CssClass = "reportTable";
			CreateTopBar(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("InstancesUsingSLElement"), true);
			GetData(null, DateTime.MinValue, DateTime.MinValue);
			table.Rows.Add(GetSearchRow());		
			table.Rows.Add(GetItemNameRow());
			table.Rows.Add(GetPackageNameRow());		
			BuildReportTable();
			Controls.Add(table);			
		}

		private void GetData(string referenceName, DateTime startDate, DateTime endDate)
		{
			reportSet = Perfectus.Server.Reporting.Report.GetInstancesByPackage(packageId, referenceName, startDate, endDate);
		}

		private TableRow GetSearchRow()
		{			
			TableRow tr = new TableRow();

			TableCell tcSearch = new TableCell();
			tcSearch.ColumnSpan = 1;
			tcSearch.CssClass = "searchCell";
			tcSearch.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Right;
		
			Label lPackage = new Label();
			lPackage.Text = "Package: ";
			tcSearch.Controls.Add(lPackage);
			
			txtReference = new TextBox();
			tcSearch.Controls.Add(txtReference);

			Label lID = new Label();
			lID.Text = " Between: ";
			tcSearch.Controls.Add(lID);

			cal1 = GetCalendarControl();
			cal1.ID = "cal1";
			tcSearch.Controls.Add(cal1);

			Label lLibrary = new Label();
			lLibrary.Text = "And: ";
			tcSearch.Controls.Add(lLibrary);

			cal2 = GetCalendarControl();
			cal2.ID = "cal2";
			tcSearch.Controls.Add(cal2);

			Button btnSearch = new Button();
			btnSearch.Text = "Go";
			btnSearch.ID = "btnSearch";
			btnSearch.CssClass = "btnGo";
			btnSearch.Click +=new EventHandler(btnSearch_Click);
			tcSearch.Controls.Add(btnSearch);

			tr.Cells.Add(tcSearch);
		
			return tr;			
		}		

		private TableRow GetItemNameRow()
		{			
			TableRow tr = new TableRow();

			TableCell itemNameCell = new TableCell();			
			itemNameCell.CssClass = "LibraryItemName";
			string imageLink = String.Format("<img src='{0}' /> ", this.m_imagesPath + "/" + GetLibItemTypeImage(itemType));
			itemNameCell.Text = imageLink + " " + itemName;

			tr.Cells.Add(itemNameCell);		
			return tr;			
		}		

		private TableRow GetPackageNameRow()
		{
			// Package name row.
			TableRow packageNameRow = new TableRow();

			TableCell pNameCell = new TableCell();
			pNameCell.ColumnSpan = 2;
		
			pNameCell.CssClass = "LibraryItemName";
			string imageLink = String.Format("<img src='{0}' /> ", this.m_imagesPath + "/reports/package.gif");
			pNameCell.Text = imageLink + " " + packageName;						
		
			packageNameRow.Cells.Add(pNameCell);
			return packageNameRow;
		}

		private void BuildReportTable()
		{
			// remove any table already called reportTable as on postback BuildReportTable() will be called again.
			for(int i = 0; i < table.Controls.Count; i ++)
			{
				Control c = table.Controls[i];
				if(c.ID == "reportTable" || c.ID == "reportHolderRow" || c.ID == "reportHolderCell")
				{
					table.Controls.Remove(c);
					i --;
				}
			}

			Table reportTable = new Table();
			reportTable.ID = "reportTable"; 
			reportTable.CssClass = "reportInnerTable";
           	
			reportTable.Rows.Add(GetHeaderRow());
            		
			if(reportSet.InstancesUsingPackage.Count == 0)
			{
				reportTable.Rows.Add(GetNoRecordsFoundRow(6));
			}
			else
			{
				int count = 0;
				foreach(ReportSet.InstancesUsingPackageRow rr in reportSet.InstancesUsingPackage)	
				{
					count = count + 1;

					if(count > maxResults)
					{
						reportTable.Rows.Add(GetMaxResultsFoundRow(6));
						break;
					}

					reportTable.Rows.Add(GetRowFromDataRow(rr));	
				}
			}

			reportTable.Rows.Add(GetPrintRow(6));

			// add the table to the global table, as a nested table.
			TableRow tableHolderRow = new TableRow();
			tableHolderRow.ID = "reportHolderRow";
			TableCell tableHolderCell = new TableCell();
			tableHolderCell.ID = "reportHolderCell";
			tableHolderCell.ColumnSpan = 2;

			tableHolderRow.Controls.Add(tableHolderCell);
			tableHolderCell.Controls.Add(reportTable);

			table.Controls.Add(tableHolderRow);
		}

		private TableRow GetRowFromDataRow(ReportSet.InstancesUsingPackageRow dr)
		{			
			// Row
			TableRow row = new TableRow();
			row.CssClass = "reportInfo";

			// Instance Name/reference
			TableCell tcInstanceName = new TableCell();
			tcInstanceName.CssClass = "reportInfo";
			Label lName = new Label();
			lName.Text = dr.Reference;
			//lName.CssClass = "parentInstanceName";
			Image iInstance = new Image();
			iInstance.CssClass = "iconLeft";
			iInstance.ImageUrl = m_imagesPath + "/reports/item.gif";
			tcInstanceName.Controls.Add(iInstance);
			tcInstanceName.Controls.Add(lName);
			row.Cells.Add(tcInstanceName);
			
			// Instance When Created
			TableCell tcWhenCreated = new TableCell();
			tcWhenCreated.CssClass = "reportInfo";
			Label lWhenCreated = new Label();
			lWhenCreated.Text = Convert.ToDateTime(dr.WhenCreated).ToLocalTime().ToShortDateString();
			tcWhenCreated.Controls.Add(lWhenCreated);
			row.Cells.Add(tcWhenCreated);

			// Instance created by
			TableCell tcCreatedBy = new TableCell();
			tcCreatedBy.CssClass = "reportInfo";
			Label lCreatedBy = new Label();
			lCreatedBy.Text = dr.CreatedByPersonId;
			tcCreatedBy.Controls.Add(lCreatedBy);
			row.Cells.Add(tcCreatedBy);

			// Instance Is Document Distributed
			TableCell tcIsDoc = new TableCell();
			tcIsDoc.CssClass = "reportInfo";
			Label lIsDoc = new Label();
			lIsDoc.Text = GetDocMadeText(dr.IsComplete);
			tcIsDoc.Controls.Add(lIsDoc);
			row.Cells.Add(tcIsDoc);

			// Instances Package Version
			TableCell tcVer = new TableCell();
			tcVer.CssClass = "reportInfo";
			Label lVer = new Label();
			lVer.Text = dr.PackageVersionNumber.ToString();
			tcVer.Controls.Add(lVer);
			row.Cells.Add(tcVer);

			return row;
		}

		private string GetDocMadeText(int isMade)
		{
			if(isMade == 1)
			{
				return "Yes";
			}
			else
			{
				return "No";
			}
		}
	
		private TableRow GetHeaderRow()
		{			

			TableRow hdrRow = new TableRow();
			hdrRow.CssClass = "header";		

			table.Rows.Add(hdrRow);

			//
			TableCell tchdrReference = new TableCell();
			tchdrReference.CssClass = "header";
			hdrRow.Cells.Add(tchdrReference);

			Label lhdrReference = new Label();
			lhdrReference.Text = "Reference";
			tchdrReference.Controls.Add(lhdrReference);

            //
			TableCell tchdrCreated = new TableCell();
			tchdrCreated.CssClass = "header";
			hdrRow.Cells.Add(tchdrCreated);

			Label lhdrCreated = new Label();
			lhdrCreated.Text = "Created";
			tchdrCreated.Controls.Add(lhdrCreated);

			//
			TableCell tchdrCreatedBy = new TableCell();
			tchdrCreatedBy.CssClass = "header";
			hdrRow.Cells.Add(tchdrCreatedBy);

			Label lhdrCreatedBy = new Label();
			lhdrCreatedBy.Text = "Created By";
			tchdrCreatedBy.Controls.Add(lhdrCreatedBy);
			
			//		
			TableCell tchdrCompleted = new TableCell();
			tchdrCompleted.CssClass = "header";
			hdrRow.Cells.Add(tchdrCompleted);

			Label lhdrCompleted = new Label();
			lhdrCompleted.Text = "Doc Made";
			tchdrCompleted.Controls.Add(lhdrCompleted);

			//
			TableCell tchdrVersion = new TableCell();
			tchdrVersion.CssClass = "header";
			hdrRow.Cells.Add(tchdrVersion);

			Label lhdrVersion = new Label();
			lhdrVersion.Text = "Package Version";
			tchdrVersion.Controls.Add(lhdrVersion);		

			return hdrRow;
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			//DateTime endDate = cal2.SelectedDate;
			//endDate = endDate.AddDays(1);

			GetData(txtReference.Text, cal1.SelectedDate, cal2.SelectedDate);
			BuildReportTable();
		}
	}
}
