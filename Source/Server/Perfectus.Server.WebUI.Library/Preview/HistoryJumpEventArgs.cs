using System;

namespace Perfectus.Server.WebUI.Library.Preview
{
	/// <summary>
	/// Summary description for InterviewFinishedEventArgs.
	/// </summary>
	public class HistoryJumpEventArgs : EventArgs
	{
		private int positionInStack;

		public int PositionInStack
		{
			get { return positionInStack; }
		}

		public HistoryJumpEventArgs(int postionInStack)
		{
			positionInStack = postionInStack;
		}
	}
}
