using System;
using System.Xml.XPath;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.WebUI.Library.Preview
{
	/// <summary>
	/// Summary description for PreviewSupport.
	/// </summary>
	public class PreviewSupport
	{
		public string GenerateImageURL(string imageId, XPathNodeIterator bindata, string renderPagePath)
		{
			Guid imgGuid = Guid.NewGuid();
			byte[] data;
			try
			{
				data = Convert.FromBase64String(bindata.Current.Value);
			}
			catch
			{
				data = new byte[0];
			}
			
			if (data.Length == 0)
			{
				return null;
			}
			else
			{
				InstanceManagement.SavePreviewImage(imgGuid, data);
				string imageSrc = renderPagePath + imgGuid.ToString();
				return imageSrc;
			}
		}
	}
}