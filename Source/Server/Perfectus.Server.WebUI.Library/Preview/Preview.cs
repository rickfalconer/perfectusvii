using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Server.AssemblySystem;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.PackageManager;
using Perfectus.Common;
using System.Text.RegularExpressions;

using System.Diagnostics;
using System.Reflection;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;

namespace Perfectus.Server.WebUI.Library.Preview
{
    /// <summary>
    /// Summary description for Preview.
    /// </summary>
    [DefaultProperty("PackageInstanceId")]
    [ToolboxData("<{0}:Preview runat=server></{0}:Preview>")]
    public class Preview : WebControl, INamingContainer, IPostBackEventHandler
    {
        public event EventHandler PreviewRequested;
        public event EventHandler<HistoryJumpEventArgs> HistoryJump;
        private Package instance = null;
        protected DropDownList ddl;
        protected TextBox hd01;
        protected Literal lt;
        protected HtmlGenericControl iDisplay;
        protected HtmlTextArea iHidden;
        private string cssPath;

        // Constant value for PDF conversion
        const int SUCCESS_SUBMIT = 0;
        const int SUCCESS_CONVERT = 1;

        private Guid packageInstanceId;

        private bool showTemplateDropDown = false;


        public Guid PackageInstanceId
        {
            get { return packageInstanceId; }
            set { packageInstanceId = value; }
        }

        public bool ShowTemplateDropDown
        {
            get { return showTemplateDropDown; }
            set { showTemplateDropDown = value; }
        }

        [Browsable(true)]
        public string JavascriptPath
        {
            get { return javascriptPath; }
            set { javascriptPath = value; }
        }

        private string javascriptPath = null;

        [Browsable(true)]
        public string RenderPagePath
        {
            get { return renderPagePath; }
            set { renderPagePath = value; }
        }

        [Browsable(true)]
        public string CssPath
        {
            get { return cssPath; }
            set { cssPath = value; }
        }

        private string renderPagePath = null;

        private System.Web.UI.WebControls.Table table = null;

        // Value for determining create pdf or html preview
        private InternalPage.ListPreviewStyle style = InternalPage.ListPreviewStyle.HTML;

        /// <summary>
        /// Property for PreviewStyle
        /// </summary>
	    private InternalPage.ListPreviewStyle previewStyle;


        public InternalPage.ListPreviewStyle PreviewStyle
        {
            get { return previewStyle; }
            set { previewStyle = value; }
        }

        public Preview() : base("div")
        {
        }

        protected override void CreateChildControls()
        {
            Page.Trace.Write("Perfectus", "Preview begin CreateChildControls");
            CreateControls();
            Page.Trace.Write("Perfectus", "Preview end CreateChildControls");
        }

        private void CreateControls()
        {
            Page.Trace.Write("Perfectus", "Preview begin CreateControls");
            if (packageInstanceId != Guid.Empty)
            {
                TemplateData[] templates = InstanceManagement.GetTemplateList(packageInstanceId);

                table = new System.Web.UI.WebControls.Table();
                table.ID = "pt";

                Controls.Add(table);

                ddl = new DropDownList();
                ddl.DataSource = templates;
                ddl.DataValueField = "UniqueIdentifier";
                ddl.DataTextField = "Name";
                ddl.ID = "ddlPv";
                ddl.DataBind();
                ddl.Attributes.Add("onchange", "seldrdw(this);");
                //				ddl.EnableViewState = true;

                // Dynamically added TextBox fields 'survive' a postback, whereas the DropDownList 'ddl'
                // lost the selected index. We copy the selected preview page into the hidden textbox
                // and the postback of the preview button will read from there.
                hd01 = new TextBox();
                hd01.ID = "hd01";
                hd01.Style.Add("display", "none");
                hd01.EnableViewState = true;
                // want an initial value ...
                if (templates.Length > 0)
                    hd01.Text = templates[0].UniqueIdentifier.ToString();
                Controls.Add(hd01);

                Button btn = new Button();
                //				btn.EnableViewState = true;
                btn.Text = ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("Preview");
                btn.Click += new EventHandler(btn_Click);
                btn.ID = "btnPv";
                btn.CausesValidation = false;
                // Prevent button from being the default submit button. FB2045
                btn.UseSubmitBehavior = false;

                if (showTemplateDropDown)
                {
                    TableRow controlRow = new TableRow();
                    controlRow.CssClass = "header";
                    TableCell titleCell = new TableCell();
                    titleCell.CssClass = "title";
                    titleCell.Controls.Add(new LiteralControl("<span>Preview</span>"));
                    controlRow.Cells.Add(titleCell);
                    TableCell controlCell1 = new TableCell();
                    controlRow.Cells.Add(controlCell1);
                    controlCell1.Controls.Add(ddl);
                    controlCell1.Controls.Add(btn);
                    table.Rows.Add(controlRow);
                }

                iDisplay = new HtmlGenericControl("iframe");
                iDisplay.ID = "iDsp";
                iDisplay.Attributes.Add("class", "previewFrame");
                iDisplay.Attributes.Add("src", "blank.html");

                TableRow frameRow = new TableRow();
                frameRow.CssClass = "preview";
                TableCell frameCell = new TableCell();
                frameCell.ColumnSpan = 2;
                frameRow.Cells.Add(frameCell);
                frameCell.Controls.Add(iDisplay);
                frameCell.CssClass = "previewCell";
                table.Rows.Add(frameRow);

                table.CellSpacing = 0;
                table.CellPadding = 0;

                iHidden = new HtmlTextArea();
                iHidden.Style.Add("display", "none");
                iHidden.EnableViewState = false;
                iHidden.ID = "iHdn";
                Controls.Add(iHidden);
                ChildControlsCreated = true;
            }
            Page.Trace.Write("Perfectus", "Preview end CreateControls");
        }

        public void Assemble(string templateName)
        {
            if (instance == null)
            {
                instance = InstanceManagement.GetInstance(packageInstanceId);
            }

            templateName = templateName.Trim().ToLower();

            foreach (TemplateDocument t in instance.Templates)
            {
                if (t.Name.Trim().ToLower() == templateName)
                {
                    Assemble(t.UniqueIdentifier);
                    return;
                }
            }

            // If we've fallen through this far, the template wasn't found.
            throw new ArgumentOutOfRangeException("templateName", string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("SpecifiedNameError"), templateName));
        }

        public void Assemble(Guid templateId)
        {
            Page.Trace.Write("Perfectus", "Begin preview Assemble(Guid)");
            WordTemplateDocument2 t = null;

            if (instance == null)
            {
                instance = InstanceManagement.GetInstance(packageInstanceId);
            }
            EnsureChildControls();

            foreach (WordTemplateDocument2 t1 in instance.Templates)
            {
                if (t1.UniqueIdentifier == templateId)
                {
                    t = t1;
                    break;
                }
            }

            if (t == null)
            {
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Library.Localisation").GetString("NullTemplate"));
            }

            MemoryStream msAnswers;
            int ver;
            int rev;
            InstanceManagement.GetVersion(packageInstanceId, out ver, out rev);
            msAnswers = InstanceToAnswerSetXml.GetAnswerSetXml(instance, packageInstanceId, ver, rev, AnswerProvider.Caller.Assembly, false);
            Assembler assembler = new Assembler();

            byte[] templateBytes = Retrieval.GetTemplateBytes(instance.UniqueIdentifier, ver, rev, t.UniqueIdentifier);
            t.OfficeOpenXMLBytes = templateBytes;

            // Set up the XmlReaders for the assembler
            MemoryStream msTemplate = null;
            MemoryStream msOutput = null;

            try
            {
                msTemplate = new MemoryStream(t.OfficeOpenXMLBytes);

                msAnswers.Seek(0, SeekOrigin.Begin);

                //XmlTextReader templateReader = new XmlTextReader(msTemplate);
                //XmlTextReader answersReader = new XmlTextReader(msAnswers);
                //StreamWriter outputWriter = new StreamWriter(msOutput, Encoding.UTF8);

                //assembler.Template = templateReader;
                //assembler.AnswerSet = answersReader;
                //assembler.Output = outputWriter;

                //Logging.TraceWrite("Starting assembly", "ProcessCoordinator");
                //assembler["bPreservePerfectusTags"] = "true";

                //assembler.Execute();
                //assembler.Execute(msTemplate, msAnswers, msOutput);

                //Logging.TraceWrite("Assembly complete", "ProcessCoordinator");

                //XmlDocument wordMLDoc = new XmlDocument();
                //msOutput.Seek(0, SeekOrigin.Begin);
                //msOutput.Position = 0;

                //pf-2944 fix w:numbering namespace
                //wordMLDoc.Load(msOutput);

                //WordTemplateDocument2 wt = new WordTemplateDocument2();
                //msOOXMLOutput = new MemoryStream();
                //wt.PatchNumberingNamespace(msOutput, ref msOOXMLOutput);
                //msOOXMLOutput.Seek(0, SeekOrigin.Begin);
                //wordMLDoc.Load(msOOXMLOutput);

                //msOOXMLOutput.Seek(0, SeekOrigin.Begin);
                //XmlTextReader reader = new XmlTextReader(msOOXMLOutput);
                //XmlTextReader reader = new XmlTextReader(msOutput);
                //XDocument transformedDoc = XDocument.Load(reader);

                //XmlDocument metaDoc = new XmlDocument();
                //MemoryStream ms = InstanceToMetaSet.GetMetaSetXml(instance, packageInstanceId, AnswerProvider.Caller.Interview);
                //metaDoc.Load(ms);
                //metaDoc.Save(@"c:\inetpub\metacruft.xml");

                //jan 2017
                //MemoryStream docxOutput = new MemoryStream();
                //t.FlatToOpc(transformedDoc, ref docxOutput);

                // Create Preview 
                if (previewStyle == InternalPage.ListPreviewStyle.HTML)
                {
                    string s = string.Empty;
                    assembler.ExecuteToHTML(msTemplate, msAnswers, ref s);
                    //copy markup into hidden control
                    iHidden.Value = HttpContext.Current.Server.HtmlEncode(s);
                    iHidden.Disabled = true;
                    Controls.Add(new LiteralControl(string.Format(@"<script type=""text/javascript"">PopulateIframeDocument(document.getElementById(""{0}""), document.getElementById(""{1}""));</script>", iDisplay.ClientID, iHidden.ClientID)));
                }
                else if (previewStyle == InternalPage.ListPreviewStyle.PDF)
                {
                    string distinationFile = string.Empty;
                    assembler.ExecuteToPDF(msTemplate, msAnswers, ref distinationFile);

                    // Reset the src for the iframe and reload the ifram
                    distinationFile = distinationFile.Replace(@"\", @"\\");
                    string distinationSrc = string.Format(@"GetPreviewFile.aspx?file={0}", distinationFile);
                    Controls.Add(new LiteralControl(string.Format(@"<script type=""text/javascript"">ReloadIfram(document.getElementById(""{0}""), '{1}');</script>", iDisplay.ClientID, distinationSrc)));
                }

                //templateReader.Close();
                //answersReader.Close();
                //outputWriter.Close();
            }
            finally
            {
                if (msTemplate != null)
                {
                    msTemplate.Close();
                }
                if (msOutput != null)
                {
                    msOutput.Close();
                }
                if (msAnswers != null)
                {
                    msAnswers.Close();
                }
                //if (msOOXMLOutput != null)
                //{
                //    msOOXMLOutput.Close();
                //}
            }
            Page.Trace.Write("Perfectus", "End preview Assemble(Guid)");
        }

        ///// <summary>
        ///// New way
        ///// Convert ooxml flat fox to docx
        ///// Either save as HTML using automation
        ///// or
        ///// Use third party product (eg WordCleaner)
        ///// </summary>
        ///// <param name="wordMLDoc"></param>
        ///// <param name="metaDoc"></param>
        //private void CreateHTML2(XmlDocument wordOOXMLDoc, XmlDocument metaDoc)
        //{
        //    //metaDoc.Load(@"C:\answerSetMetaData.xml");
        //    XmlNode metaNode = metaDoc.SelectSingleNode("metaSet");
        //    XmlNode importedMetaNode = wordOOXMLDoc.ImportNode(metaNode, true);
        //    wordOOXMLDoc.DocumentElement.AppendChild(importedMetaNode);

        //    // create docx package from flat xml
        //    XDocument flatXmlDoc = XDocument.Parse(wordOOXMLDoc.OuterXml);
        //    MemoryStream docxOutput = new MemoryStream();
        //    using (WordTemplateDocument2 wt = new WordTemplateDocument2())
        //    {
        //        wt.FlatToOpc(flatXmlDoc, ref docxOutput);
        //        docxOutput.Position = 0;
        //        byte[] outbytes = new byte[docxOutput.Length];
        //        docxOutput.Read(outbytes, 0, outbytes.Length);

        //        //using (FileStream fs = File.OpenWrite("C:\\temp\\crap.docx"))
        //        //{
        //        //    fs.Write(outbytes, 0, outbytes.Length);
        //        //    fs.Close();
        //        //}
        //    }

        //    //open docx, save as html
        //    MemoryStream msHtml = new MemoryStream();
        //    Perfectus.Common.SharedLibrary.GemBoxAPI.ConvertWordToHtml(docxOutput, ref msHtml);
        //    msHtml.Seek(0, SeekOrigin.Begin);
        //    string sHtml = Encoding.UTF8.GetString(msHtml.ToArray());
        //    string s = sHtml.Replace("&#xa0;", string.Empty);

        //    //copy markup into hidden control
        //    iHidden.Value = HttpContext.Current.Server.HtmlEncode(s);
        //    iHidden.Disabled = true;
        //    Controls.Add(new LiteralControl(string.Format(@"<script type=""text/javascript"">PopulateIframeDocument(document.getElementById(""{0}""), document.getElementById(""{1}""));</script>", iDisplay.ClientID, iHidden.ClientID)));
        //}

        ///// <summary>
        ///// Old way - using wordML
        ///// </summary>
        ///// <param name="wordMLDoc"></param>
        ///// <param name="metaDoc"></param>
        //private void CreateHTML(XmlDocument wordMLDoc, XmlDocument metaDoc)
        //{
        //    //metaDoc.Load(@"C:\answerSetMetaData.xml");
        //    XmlNode metaNode = metaDoc.SelectSingleNode("metaSet");
        //    XmlNode importedMetaNode = wordMLDoc.ImportNode(metaNode, true);
        //    wordMLDoc.DocumentElement.AppendChild(importedMetaNode);
        //    XmlDocument HTMLDoc = new XmlDocument();
        //    XslTransform HTMLTransform = new XslTransform();
        //    HTMLTransform.Load(Config.Shared.assembly.htmlPreviewXsltPath);
        //    XsltArgumentList argList = new XsltArgumentList();
        //    argList.AddParam("renderPagePath", string.Empty, renderPagePath);
        //    argList.AddParam("cssPath", string.Empty, cssPath);
        //    PreviewSupport previewSupport = new PreviewSupport();
        //    argList.AddExtensionObject("urn:previewSupport", previewSupport);
        //    XmlReader wordDocReader = HTMLTransform.Transform(wordMLDoc.DocumentElement.CreateNavigator(), argList, new XmlUrlResolver());
        //    HTMLDoc.Load(wordDocReader);
        //    HTMLDoc.PreserveWhitespace = true;
        //    wordDocReader.Close();
        //    iHidden.Value = HttpContext.Current.Server.HtmlEncode(HTMLDoc.InnerXml);
        //    iHidden.Disabled = true;
        //    Controls.Add(new LiteralControl(string.Format(@"<script type=""text/javascript"">PopulateIframeDocument(document.getElementById(""{0}""), document.getElementById(""{1}""));</script>", iDisplay.ClientID, iHidden.ClientID)));
        //}

        ///// <summary>
        ///// Create PDF in preview iFrame
        ///// </summary>
        ///// <param name="wordMLDoc"></param>
        //private void CreatePDF(XmlDocument wordMLDoc)
        //{
        //    //pf-3266 remove word automation office12

        //    //// Convert WordML into PDF

        //    //// ToDo - Is this a good way to set up the file name?
        //    //string fileName = packageInstanceId.ToString();
        //    //string folderPath = Config.Shared.interview.activePDFSharedFolder;
        //    //folderPath = folderPath.TrimEnd('\\').Trim();
        //    //string wordMLPath = string.Format(@"{0}\{1}.xml", folderPath, fileName);
        //    //string docPath = string.Format(@"{0}\{1}.doc", folderPath, fileName);
        //    //string pdfPath = string.Format(@"{0}\{1}.pdf", folderPath, fileName);

        //    //object oWordMlPath = wordMLPath;
        //    //object oConvertedWordFile = docPath;
        //    //object oConvertedPDFFile = pdfPath;

        //    //// Save wordML to shared folder for PDF conversion later
        //    //wordMLDoc.Save(wordMLPath);

        //    //// Convert WordML to Word
        //    ////ApplicationClass app = new ApplicationClass();
        //    //Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();

        //    //try
        //    //{
        //    //    object oMissing = Missing.Value;
        //    //    Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(ref oWordMlPath, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        //    //    app.Selection.WholeStory();
        //    //    app.Selection.Fields.Update();

        //    //    foreach (TableOfContents toc in doc.TablesOfContents)
        //    //    {
        //    //        toc.Update();
        //    //    }

        //    //    //object oDocFormat = WdSaveFormat.wdFormatDocument;
        //    //    object oDocFormat = WdSaveFormat.wdFormatHTML;
        //    //    doc.SaveAs(ref oConvertedWordFile, ref oDocFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

        //    //    object oFalse = false;
        //    //    doc.Close(ref oFalse, ref oMissing, ref oMissing);
        //    //    app.Quit(ref oFalse, ref oMissing, ref oMissing);
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    Trace.WriteLine(ex.Message);
        //    //}

        //    ////Convert Word into PDF
        //    //string tempFile = Path.GetTempFileName();

        //    //int convertStatus = ConvertToPDF(oConvertedWordFile.ToString(),folderPath);


        //    //if (convertStatus == SUCCESS_CONVERT && File.Exists(oConvertedPDFFile.ToString()))
        //    //{
        //    //    // Move the Converted PDF to Temp file
        //    //    string distinationFile = tempFile + ".pdf";
        //    //    File.Move(oConvertedPDFFile.ToString(), distinationFile);

        //    //    // Reset the src for the iframe and reload the ifram
        //    //    distinationFile = distinationFile.Replace(@"\", @"\\");
        //    //    string distinationSrc = string.Format(@"GetPreviewFile.aspx?file={0}", distinationFile);
        //    //    Controls.Add(new LiteralControl(string.Format(@"<script type=""text/javascript"">ReloadIfram(document.getElementById(""{0}""), '{1}');</script>", iDisplay.ClientID, distinationSrc)));
        //    //}

        //    //// Clear temp files
        //    //if (File.Exists(oWordMlPath.ToString()))
        //    //{
        //    //    File.Delete(oWordMlPath.ToString());
        //    //}
        //    //if (File.Exists(oConvertedWordFile.ToString()))
        //    //{
        //    //    File.Delete(oConvertedWordFile.ToString());
        //    //}
        //    //if (File.Exists(oConvertedPDFFile.ToString()))
        //    //{
        //    //    File.Delete(oConvertedPDFFile.ToString());
        //    //}
        //    //if (File.Exists(tempFile))
        //    //{
        //    //    File.Delete(tempFile);
        //    //}
        //}

        ///// <summary>
        ///// Call ActivePDF server to convert word into pdf
        ///// </summary>
        ///// <param name="file"></param>
        ///// <param name="folder"></param>
        ///// <returns></returns>
        //private static int ConvertToPDF(string file, string folder)
        //{
        //    int intReturnSubmit = -1;
        //    int convertStatus = 0;
        //    APDocConvNET.APDocConverter docConverter = new APDocConvNET.APDocConverter();

        //    // Get ActivePDF settings from shared.config
        //    string serverIP = Config.Shared.interview.activePDFServerIP;
        //    int port = Convert.ToInt32(Config.Shared.interview.activePDFPort);
        //    short timeout = Convert.ToInt16(Config.Shared.interview.activePDFTimeOut);

        //    //----Security Settings----
        //    string userPassword = Config.Shared.interview.activePDFuserPassword;
        //    string ownerPassword = Config.Shared.interview.activePDFownerPassword;
        //    int allowPrinting = Convert.ToInt32(Config.Shared.interview.activePDFallowPrinting);
        //    int allowEdit = Convert.ToInt32(Config.Shared.interview.activePDFallowEdit);
        //    int allowCopy = Convert.ToInt32(Config.Shared.interview.activePDFallowCopy);
        //    int allowModify = Convert.ToInt32(Config.Shared.interview.activePDFallowModify);
        //    int allowFillIn = Convert.ToInt32(Config.Shared.interview.activePDFallowFillIn);
        //    int allowMakeAccessible = Convert.ToInt32(Config.Shared.interview.activePDFallowMakeAccessible);
        //    int allowAssembly = Convert.ToInt32(Config.Shared.interview.activePDFallowAssembly);
        //    int allowReproduce = Convert.ToInt32(Config.Shared.interview.activePDFallowReproduce);

        //    // Set properties for ActivePDF
        //    docConverter.StatusTimeout = timeout;
        //    docConverter.SetSecurityOptions128(userPassword,ownerPassword,allowPrinting,allowEdit,allowCopy,allowModify,allowFillIn,allowMakeAccessible,allowAssembly,allowReproduce);

        //    // Submit file to ActivePDF DocConverter
        //    if (File.Exists(file))
        //    {
        //        intReturnSubmit = docConverter.Submit(serverIP, port, file, folder, folder, folder, "", "", 0, "");

        //        if (intReturnSubmit == SUCCESS_SUBMIT)
        //        {
        //            convertStatus = docConverter.CheckStatus(serverIP, port, file, folder, folder);
        //        }
        //    }

        //    return convertStatus;
        //}

        protected override void OnPreRender(EventArgs e)
        {
            Page.RegisterClientScriptBlock("lib", string.Format(@"<script type=""text/javascript"" src=""{0}/Perfectus_Preview.js""></script>", javascriptPath));
            Page.RegisterClientScriptBlock("jump", string.Format(@"<script type=""text/javascript"">function historyJump(pageId){{__doPostBack('{0}', pageId)}}</script>", this.UniqueID.Replace(":", "$")));
            if (null != hd01)
                Page.RegisterClientScriptBlock("seldrdw", string.Format(@"<script type=""text/javascript"">function seldrdw(dropdown){{ var myindex = dropdown.selectedIndex; var SelValue = dropdown.options[myindex].value; var myHiddenField = document.getElementById('{0}'); myHiddenField.value = SelValue;}}</script>", hd01.UniqueID.Replace("$", "_")));
            base.OnPreRender(e);
        }

        private void btn_Click(object sender, EventArgs e)
        {
            string templateId = hd01.Text;

            /*
            if (templateId == null || templateId.Length == 0)
            {
                TemplateData[] templates = InstanceManagement.GetTemplateList(packageInstanceId);
                if (templates.Length >= 1 )
                    templateId = hd01.Text = templates[0].UniqueIdentifier.ToString();
            }
            */

            if (templateId != ddl.SelectedValue)
                ddl.SelectedValue = templateId;

            Assemble(new Guid(templateId));

            if (PreviewRequested != null)
            {
                PreviewRequested(sender, e);
            }
        }


        #region IPostBackEventHandler Members

        public void RaisePostBackEvent(string eventArgument)
        {
            int positionInStack = Convert.ToInt32(eventArgument);
            OnHistoryJump(positionInStack);
        }

        #endregion

        protected void OnHistoryJump(int positionInStack)
        {
            if (HistoryJump != null)
            {
                HistoryJump(this, new HistoryJumpEventArgs(positionInStack));
            }
        }
    }
}
