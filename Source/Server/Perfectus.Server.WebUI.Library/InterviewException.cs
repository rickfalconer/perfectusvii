using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Server.WebUI.Library
{
    public class InterviewException : ApplicationException
    {
        string uiMessage;
        string title;

        public string UIMessage
        {
            get
            {
                return uiMessage;
            }
            set
            {
                uiMessage = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public InterviewException(string message, string uiMessage) : base(message)
        {
            this.uiMessage = uiMessage;
        }
    }
}