﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Handlers;
using System.Xml;
using Perfectus.Common.PackageObjects;
using Newtonsoft.Json;
using Perfectus.Server.PackageManager;
using Perfectus.Server.Abstractions;

/// TODO
// get package - include list of questions?
// put id with no body - assume delete?
// error handling - respond with http error numbers
///
namespace Perfectus.Server
{
    /// <summary>
    /// Summary description for RestService
    /// </summary>
    public class RestService : IHttpHandler
    {
        private ErrorHandler errHandler = new ErrorHandler();
        
        // package state
        Package _package;

        // request vars
        private HttpContext _context;
        private Guid _tempGuid;
        private int _segment;
        private Guid _packageId
        {
            get
            {
                _segment = Array.IndexOf(_context.Request.Url.Segments, "package/");
                if (_segment == -1)
                {
                    throw new System.FormatException("URL segment 'package' not found in request: " + _context.Request.RawUrl);
                }
                if (_context.Request.Url.Segments.Length <= ++_segment)
                {
                    throw new System.FormatException("Package id not found in request. Expected '/package/{guid1}'. Request: " + _context.Request.RawUrl);
                }
                if (Guid.TryParse((string)_context.Request.Url.Segments[_segment].Substring(0,36), out _tempGuid))
                {
                    return _tempGuid;
                }
                else
                {
                    throw new System.FormatException("Package GUID should contain 32 digits with 4 dashes. Request: " + _context.Request.RawUrl);
                }
            }
        }
        private string _urlObjectType
        {
            get
            {
                string lastSegment = _context.Request.Url.Segments[_context.Request.Url.Segments.Length - 1];
                if (lastSegment.Length == 36)
                {
                    if (Guid.TryParse((string)_context.Request.Url.Segments[_context.Request.Url.Segments.Length - 1].Substring(0, 36), out _tempGuid))
                    {
                        // 2nd last segment is object type, strip trailing /
                        return _context.Request.Url.Segments[_context.Request.Url.Segments.Length - 2].TrimEnd('/');
                    }
                    else
                    {
                        throw new System.FormatException("Package GUID should contain 32 digits with 4 dashes. Request: " + _context.Request.RawUrl);
                    }
                }
                else
                {
                    // last segment is object type
                    return lastSegment;
                }
            }
        }
        private Guid _urlObjectId
        {
            get
            {
                string lastSegment = _context.Request.Url.Segments[_context.Request.Url.Segments.Length - 1];
                if (lastSegment.Length == 36)
                {
                    if (Guid.TryParse((string)_context.Request.Url.Segments[_context.Request.Url.Segments.Length - 1].Substring(0, 36), out _tempGuid))
                    {
                        return _tempGuid;
                    }
                    else
                    {
                        return Guid.Empty;
                    }
                }
                else
                {
                    return Guid.Empty;
                }
                
            }
        }

        public RestService()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        //pooling
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            _context = context;

            //Handling CRUD
            switch (context.Request.HttpMethod)
            {
                case "GET":
                    //Perform READ Operation
                    READ(context);
                    break;
                case "POST":
                    //Perform CREATE Operation
                    CREATE(context);
                    break;
                case "PUT":
                    //Perform UPDATE Operation
                    UPDATE(context);
                    break;
                case "DELETE":
                    //Perform DELETE Operation
                    DELETE(context);
                    break;
                default:
                    break;
            }
        }

        private void DELETE(HttpContext context)
        {
            bool deleteWholePackageImage = false;

            // load the package image into local state
            _package = PackageManager.PackageImage.ReadPackageImage(_packageId);
            
            switch (_urlObjectType)
            {
                case "package":
                    deleteWholePackageImage = true;
                    break;
                case "questions":
                    if (_urlObjectId == Guid.Empty)
                    {
                        // error - expected question id
                    }
                    else
                    {
                        // remove question from package
                        _package.Questions.RemoveAt(_package.Questions.IndexOf(_package.QuestionByGuid[_urlObjectId]));
                    }
                    break;
            }

            // save or delete the package image
            if (deleteWholePackageImage)            
                PackageManager.PackageImage.DeletePackageImage(_packageId);
            else
                PackageManager.PackageImage.UpdatePackageImage(_package);
        }

        private void CREATE(HttpContext context)
        {
            bool packageImageExists = true;

            // load the package image into local state
            try
            {
                _package = PackageManager.PackageImage.ReadPackageImage(_packageId);
            }
            catch (System.Runtime.Serialization.SerializationException se)
            {
                // package image does not exist
                _package = new Package();
                packageImageExists = false;
            }

            // request body contains data
            byte[] RequestByte = context.Request.BinaryRead(context.Request.ContentLength);
            string RequestStr = Encoding.UTF8.GetString(RequestByte);

            // identify and map each type, e.g. question
            // delegate to object type helper to map abstract data to perfectus data 
            switch (_urlObjectType)
            {
                case "package":
                    SetPackageFromAbstract(RequestStr);
                    break;
                case "questions":
                    if (_urlObjectId == Guid.Empty)
                    {
                        // error - expected new question id
                    }
                    else
                    {
                        // map new question
                        Question question = _package.CreateQuestion(_urlObjectId);
                        SetQuestionFromAbstract(RequestStr, question);
                    }
                    break;
            }

            // save package image 
            if (packageImageExists)
                PackageManager.PackageImage.UpdatePackageImage(_package);
            else
                PackageManager.PackageImage.CreatePackageImage(_package);
        }

        private void SetPackageFromAbstract(string RequestStr)
        {
            // deserialise into abstract package
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                Context = new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.Other)
            };
            A_Package newP = JsonConvert.DeserializeObject<A_Package>(RequestStr);

            // map abstract props to package
            _package.UniqueIdentifier = _packageId;
            _package.Name = newP.name;
            _package.Description = newP.description;
        }

        private void SetQuestionFromAbstract(string RequestStr, Question question)
        {
            // deserialise into abstract package
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                Context = new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.Other)
            };
            A_Question newQ = JsonConvert.DeserializeObject<A_Question>(RequestStr);

            // map abstract props
            if (newQ.buttonText != null) 
                question.ServiceCallButtonText = newQ.buttonText;
            if (newQ.createdBy != null)
                question.CreatedBy = newQ.createdBy;
            if (newQ.creationDateTime != null)
                question.CreatedDateTime = newQ.creationDateTime;
            if (newQ.dataBindings != null)
            {
                question.DataBindings = new DataBindingInfo();
                question.DataBindings.AnswerBinding = newQ.dataBindings;
            }
            if (newQ.dataSize > 0)
                question.DataSize = newQ.dataSize;

            switch (newQ.dataType.ToLower())
            {
                case "answerset":
                    question.DataType = Common.PerfectusDataType.AnswerSet;
                    break;
                case "attachment":
                    question.DataType = Common.PerfectusDataType.Attachment;
                    break;
                case "date":
                    question.DataType = Common.PerfectusDataType.Date;
                    break;
                case "file":
                    question.DataType = Common.PerfectusDataType.File;
                    break;
                case "html":
                    question.DataType = Common.PerfectusDataType.HTML;
                    break;
                case "number":
                    question.DataType = Common.PerfectusDataType.Number;
                    break;
                case "text":
                    question.DataType = Common.PerfectusDataType.Text;
                    break;
                case "yesno":
                    question.DataType = Common.PerfectusDataType.YesNo;
                    break;
                default:
                    question.DataType = Common.PerfectusDataType.Text;
                    break;
            }

            if (newQ.defaultAnswer != null)
                question.DefaultAnswer.TextValue = newQ.defaultAnswer;

            if (newQ.direction == "horizontal")
            {   
                question.Direction = Question.ListDirection.Horizontal;
            }
            else
            {
                // this is the default
                question.Direction = Question.ListDirection.Vertical;
            }

            if (newQ.displayHeight > 0)
                question.DisplayHeight = newQ.displayHeight;
            
            switch (newQ.displayType.ToLower())
            {
                case "calendar":
                    question.DisplayType = Common.QuestionDisplayType.Calendar;
                    break;
                case "checkbox":
                    question.DisplayType = Common.QuestionDisplayType.CheckBox;
                    break;
                case "checkboxlist":
                    question.DisplayType = Common.QuestionDisplayType.CheckBoxList;
                    break;
                case "dropdownlist":
                    question.DisplayType = Common.QuestionDisplayType.DropDownList;
                    break;
                case "listbox":
                    question.DisplayType = Common.QuestionDisplayType.ListBox;
                    break;
                case "multiselectlistbox":
                    question.DisplayType = Common.QuestionDisplayType.MultiSelectListBox;
                    break;
                case "password":
                    question.DisplayType = Common.QuestionDisplayType.Password;
                    break;
                case "radiolist":
                    question.DisplayType = Common.QuestionDisplayType.RadioList;
                    break;
                case "textarea":
                    question.DisplayType = Common.QuestionDisplayType.TextArea;
                    break;
                case "textbox":
                    question.DisplayType = Common.QuestionDisplayType.TextBox;
                    break;
                default:
                    question.DisplayType = Common.QuestionDisplayType.TextBox;
                    break;
            }

            if (newQ.displayWidth > 0)
                question.DisplayWidth = newQ.displayWidth;

            question.Enabled.YesNoValue = newQ.enabled;
            
            if (newQ.example != null)
                question.Example.TextValue = newQ.example;
            if (newQ.help != null)
                question.Help = newQ.help;
            if (newQ.hint != null)
                question.Hint.TextValue = newQ.hint;
            if (newQ.interviewFormatString != null)
                question.InterviewFormatString = newQ.interviewFormatString;

            question.Mandatory.YesNoValue = newQ.mandatory;
            
            if (newQ.modificationDateTime != null)
                question.ModifiedDateTime = newQ.modificationDateTime;
            if (newQ.modifiedBy != null)
                question.ModifiedBy = newQ.modifiedBy;
            if (newQ.name != null)
                question.Name = newQ.name;
            if (newQ.notes != null)
                question.Notes = newQ.notes;
            if (newQ.parameter != null)
                question.Parameter.TextValue = newQ.parameter;
            if (newQ.prefix != null)
                question.Prefix.TextValue = newQ.prefix;
            if (newQ.previewHoverText != null)
                question.PreviewHoverText = newQ.previewHoverText;
            if (newQ.prefix != null)
                question.Prompt.TextValue = newQ.prompt;
            if (newQ.suffix != null)
                question.Suffix.TextValue = newQ.suffix;
            if (newQ.validation != null)
                question.Validation.RegexValue = newQ.validation;
            if (newQ.validationMessage != null)
                question.ValidationErrorMessage.TextValue = newQ.validationMessage;

            question.Visible.YesNoValue = newQ.visible;
        }


        private void READ(HttpContext context)
        {
            string serialisedAbstract = string.Empty;

            // load the package image into local state
            _package = PackageManager.PackageImage.ReadPackageImage(_packageId);

            // identify the object type for http READ
            // delegate to object type helper to map perfectus data to abstract data, serialise it 
            switch (_urlObjectType)
            {
                case "package":
                    serialisedAbstract = GetAbstractPackageProperties();
                    break;
                case "questions":
                    if (_urlObjectId == Guid.Empty)
                    {
                        // list questions for package
                        serialisedAbstract = GetAbstractQuestionList();
                    }
                    else
                    {
                        // map specific question
                        serialisedAbstract = GetAbstractQuestionProperties();
                    }
                    break;
            }

            // return serialised abstract object to caller
            context.Response.Write(serialisedAbstract);
        }

        /// <summary>
        /// returns JSON package properties only, not the data collections.
        /// </summary>
        /// <returns>JSON</returns>
        private string GetAbstractPackageProperties()
        {
            string serialisedAbstractPackage = string.Empty;
            A_Package abstractPackage = new A_Package();

            //map package props to abstract package
            abstractPackage.uniqueIdentifier = _package.UniqueIdentifier;
            abstractPackage.createdBy = _package.CreatedBy;
            abstractPackage.creationDateTime = _package.CreationDateTime;
            abstractPackage.description = _package.Description;
            abstractPackage.modificationDateTime = _package.ModificationDateTime;
            abstractPackage.modifiedBy = _package.ModifiedBy;
            abstractPackage.name = _package.Name;

            //serialise abstract package
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                Context = new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.CrossAppDomain),
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Newtonsoft.Json.Formatting.Indented
            };
            serialisedAbstractPackage = JsonConvert.SerializeObject(abstractPackage, jsonSetting);

            return serialisedAbstractPackage;
        }

        /// <summary>
        /// returns JSON containing all questions in package
        /// </summary>
        /// <returns>JSON collection of questions</returns>
        private string GetAbstractQuestionList()
        {
            string serialisedAbstractQuestionList = string.Empty;
            List<A_Question> aQList = new List<A_Question>();

            foreach (Question q in _package.Questions)
            {
                A_Question aq = new A_Question();

                //map question props to abstract question
                GetAbstractQuestionFromQuestion(aq, q);

                aQList.Add(aq);
            }

            //serialise abstract question list
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                Context = new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.CrossAppDomain),
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Newtonsoft.Json.Formatting.Indented
            };
            serialisedAbstractQuestionList = JsonConvert.SerializeObject(aQList, jsonSetting);

            return serialisedAbstractQuestionList;
        }

        /// <summary>
        /// returns JSON for question specified by url /questions/{id}
        /// </summary>
        /// <returns>JSON for 1 question</returns>
        private string GetAbstractQuestionProperties()
        {
            string serialisedAbstractQuestion = string.Empty;
            A_Question aq = new A_Question();

            //map question props to abstract question
            GetAbstractQuestionFromQuestion(aq, _package.QuestionByGuid[_urlObjectId]);

            //serialise abstract question
            JsonSerializerSettings jsonSetting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                Context = new System.Runtime.Serialization.StreamingContext(System.Runtime.Serialization.StreamingContextStates.CrossAppDomain),
                TypeNameHandling = TypeNameHandling.All,
                Formatting = Newtonsoft.Json.Formatting.Indented
            };
            serialisedAbstractQuestion = JsonConvert.SerializeObject(aq, jsonSetting);

            return serialisedAbstractQuestion;
        }

        /// <summary>
        /// Question helper method - populates aq
        /// </summary>
        /// <param name="aq">abstract question object</param>
        /// <param name="question">source question used to populate aq</param>
        private static void GetAbstractQuestionFromQuestion(A_Question aq, Question question)
        {
            aq.uniqueIdentifier = question.UniqueIdentifier;
            aq.buttonText = question.ServiceCallButtonText;
            aq.createdBy = question.CreatedBy;
            aq.creationDateTime = question.CreatedDateTime;

            if (question.DataBindings != null)
                aq.dataBindings = question.DataBindings.AnswerBinding;

            aq.dataSize = question.DataSize;

            switch (question.DataType)
            {
                case Common.PerfectusDataType.AnswerSet:
                    aq.dataType = "answerset";
                    break;
                case Common.PerfectusDataType.Attachment:
                    aq.dataType = "attachment";
                    break;
                case Common.PerfectusDataType.Date:
                    aq.dataType = "date";
                    break;
                case Common.PerfectusDataType.File:
                    aq.dataType = "file";
                    break;
                case Common.PerfectusDataType.HTML:
                    aq.dataType = "html";
                    break;
                case Common.PerfectusDataType.Number:
                    aq.dataType = "number";
                    break;
                case Common.PerfectusDataType.Text:
                    aq.dataType = "text";
                    break;
                case Common.PerfectusDataType.YesNo:
                    aq.dataType = "yesno";
                    break;
                default:
                    aq.dataType = "text";
                    break;
            }

            aq.defaultAnswer = question.DefaultAnswer.TextValue;

            if (question.Direction == Question.ListDirection.Horizontal)
                aq.direction = "horizontal";
            else
                aq.direction = "vertical";

            aq.displayHeight = question.DisplayHeight;

            switch (question.DisplayType)
            {
                case Common.QuestionDisplayType.Calendar:
                    aq.displayType = "calendar";
                    break;
                case Common.QuestionDisplayType.CheckBox:
                    aq.displayType = "checkbox";
                    break;
                case Common.QuestionDisplayType.CheckBoxList:
                    aq.displayType = "checkboxlist";
                    break;
                case Common.QuestionDisplayType.DropDownList:
                    aq.displayType = "dropdownlist";
                    break;
                case Common.QuestionDisplayType.ListBox:
                    aq.displayType = "listbox";
                    break;
                case Common.QuestionDisplayType.MultiSelectListBox:
                    aq.displayType = "multiselectlistbox";
                    break;
                case Common.QuestionDisplayType.Password:
                    aq.displayType = "password";
                    break;
                case Common.QuestionDisplayType.RadioList:
                    aq.displayType = "radiolist";
                    break;
                case Common.QuestionDisplayType.TextArea:
                    aq.displayType = "textarea";
                    break;
                case Common.QuestionDisplayType.TextBox:
                    aq.displayType = "textbox";
                    break;
                default:
                    aq.displayType = "textbox";
                    break;
            }

            aq.displayWidth = question.DisplayWidth;
            aq.enabled = question.Enabled.YesNoValue;
            aq.example = question.Example.TextValue;
            aq.help = question.Help;
            aq.hint = question.Hint.TextValue;
            aq.interviewFormatString = question.InterviewFormatString;
            aq.mandatory = question.Mandatory.YesNoValue;
            aq.modificationDateTime = question.ModifiedDateTime;
            aq.modifiedBy = question.ModifiedBy;
            aq.name = question.Name;
            aq.notes = question.Notes;
            aq.parameter = question.Parameter.TextValue;
            aq.prefix = question.Prefix.TextValue;
            aq.previewHoverText = question.PreviewHoverText;
            aq.prefix = question.Prompt.TextValue;
            aq.suffix = question.Suffix.TextValue;
            if (question.Validation != null)
                aq.validation = question.Validation.RegexValue;
            aq.validationMessage = question.ValidationErrorMessage.TextValue;
            aq.visible = question.Visible.YesNoValue;
        }


        private void UPDATE(HttpContext context)
        {
            // load the package image into local state
            _package = PackageManager.PackageImage.ReadPackageImage(_packageId);

            // request body contains data
            byte[] RequestByte = context.Request.BinaryRead(context.Request.ContentLength);
            string RequestStr = Encoding.UTF8.GetString(RequestByte);

            // delegate to object type helper to map abstract data to perfectus data 
            //TODO identify and map each type, e.g. question
            switch (_urlObjectType)
            {
                case "package":
                    SetPackageFromAbstract(RequestStr);
                    break;
                case "questions":
                    if (_urlObjectId == Guid.Empty)
                    {
                        // error - expected new question id
                    }
                    else
                    {
                        // map properties to existing question
                        Question question = _package.QuestionByGuid[_urlObjectId];
                        SetQuestionFromAbstract(RequestStr, question);
                    }
                    break;
            }

            // update the package image
            PackageManager.PackageImage.UpdatePackageImage(_package);
        }

    }

    public class ErrorHandler
    {
        static StringBuilder errMessage = new StringBuilder();

        static ErrorHandler()
        {
        }
        public string ErrorMessage
        {
            get { return errMessage.ToString(); }
            set
            {
                errMessage.AppendLine(value);
            }
        }
    }

}