using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Logging;
using System.Reflection;

namespace Perfectus.Server.ProcessCoordinator.Receiver
{
    public delegate ProcessingTask JobRead(out Object id);
    public delegate void JobResult(Object id, JobStatus jobStatus);
    public delegate void JobAvailable(JobRead jr, JobResult je);

    public interface IMessageReader
    {
        bool HealthCheck(out string reason);
        void MaybeFreeOrphans(int timeoutSeconds);
        void Start(JobAvailable ja);
        void Stop();
    }


    class BaseReceiver
    {
        protected int _maxRetry;
        protected int _delayTime;
        protected TimeSpan _delayTimeSpan;

        protected BaseReceiver()
        {
            _maxRetry = Config.Shared.queue.maxRetry;
            _delayTime = Config.Shared.queue.delayTime;
            _delayTimeSpan = new TimeSpan(0, 0, _delayTime);
        }

        // The URI of the Process Coordinator
        private String _identity;
        public String Identity
        {
            get
            {
                if (_identity == null)
                    _identity = String.Format("{0}::{1}",
                                    System.Environment.MachineName,
                                    Assembly.GetExecutingAssembly().Location);
                return _identity;
            }
        }
	
        protected bool CanReQueue(Job job)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            bool canRequeue = job.IncrementRetry() < _maxRetry;
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return canRequeue;
        }

        virtual public bool HealthCheck(out string reason) {
            reason = null;
            return true;
        }
    }


    public class JobReceiver
    {
        public static IMessageReader [] PerfectusReceiver()
        {
            List<IMessageReader> readers = new List<IMessageReader>();

            IMessageReader reader = null;// PerfectusQueue.MessageReader();
            if (reader != null) readers.Add(reader);

            reader = DatabaseReceiver.MessageReader();
            if (reader != null) readers.Add(reader);

            return readers.ToArray();
        }
    }

}

