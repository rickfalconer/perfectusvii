using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Exception;
using System.Reflection;
/*
using System.Messaging;
*/
using Perfectus.Library.Localisation;
using Perfectus.Library.Logging;
using Perfectus.Server.Common.Data;

namespace Perfectus.Server.ProcessCoordinator.Receiver
{
    internal class PerfectusQueue : BaseReceiver //, IMessageReader
    {
        /*
        private event JobAvailable jobAvailable;

        private static PerfectusQueue _ptr = null;

        private MessageQueue reveiverQueue;
        private MessageQueue transactionQueue;
        private MessageQueue reportingQueue;

        private String receiverQueueName;
        private String transactionQueueName;
        private String receiverTransactionMachineName;
        private Boolean isReceiverTransactionPrivate;

        private String messageQueueName;
        private String messageMachineName;
        private Boolean isMessageTransactionPrivate;

        Cursor queueCursor;
        PeekAction queueCursorAction;

        private PerfectusQueue()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            receiverQueueName = Config.Shared.queue.name;
            transactionQueueName = Config.Shared.queue.transactionQueueName;
            receiverTransactionMachineName = Config.Shared.queue.server;
            isMessageTransactionPrivate = isReceiverTransactionPrivate = Config.Shared.queue.isPrivate;


            messageQueueName = receiverQueueName + "Reporting";
            messageMachineName = receiverTransactionMachineName;

            if (Log.ShallTrace(Log.Category.Receiver, 100))
            {
                List<String> msg = new List<string>();
                msg.Add("Queue data found in configuration file:");
                msg.Add(String.Format("Receiver    queue: {0}, on server {1}, as {2}", receiverQueueName, receiverTransactionMachineName, isReceiverTransactionPrivate ? "private" : "public"));
                msg.Add(String.Format("Transaction queue: {0}, on server {1}, as {2}", transactionQueueName, receiverTransactionMachineName, isReceiverTransactionPrivate ? "private" : "public"));
                msg.Add(String.Format("Message queue:     {0}, on server {1}, as {2}", messageQueueName, messageMachineName, isMessageTransactionPrivate ? "private" : "public"));
                Log.Trace(Log.Category.Receiver, 100, msg.ToArray());
            }
            InitializeAllQueues();

            queueCursor = null;
            queueCursorAction = PeekAction.Current;

            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        public static IMessageReader MessageReader()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            if (_ptr == null)
                _ptr = new PerfectusQueue();
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return _ptr;
        }

        private void InitializeAllQueues()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            reveiverQueue = Initialise(receiverTransactionMachineName, receiverQueueName, isMessageTransactionPrivate);
            transactionQueue = Initialise(receiverTransactionMachineName, transactionQueueName, isMessageTransactionPrivate);
            reportingQueue = Initialise(messageMachineName, messageQueueName, isMessageTransactionPrivate);
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);

            reveiverQueue.MessageReadPropertyFilter.ArrivedTime = true;
        }

        private MessageQueue Initialise(string machineName, string queueName, bool isPrivate)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            string queuePath;
            MessageQueue queue;
            if (isPrivate)
                queuePath = string.Format(@"{0}\Private$\{1}", machineName, queueName);
            else
                queuePath = string.Format(@"{0}\{1}", machineName, queueName);

            if (!MessageQueue.Exists(queuePath))
            {
                try
                {
                    Log.Trace(Log.Category.Receiver, 10, System.String.Format("Creating queue '{0}'", queuePath));
                    queue = MessageQueue.Create(queuePath, true);
                }
                catch (Exception ex)
                {
                    Log.Error(Log.Category.Receiver, ex, new ID("ProcessCoordinator.FailedToCreateMessageQueue"), queuePath);
                    throw;
                }
            }
            else
            {
                try
                {
                    Log.Trace(Log.Category.Receiver, 10, System.String.Format("Opening queue '{0}'", queuePath));
                    queue = new MessageQueue(queuePath, false);
                }
                catch (Exception ex)
                {
                    Log.Error(Log.Category.Receiver, ex, new ID("ProcessCoordinator.FailedToOpenMessageQueue"), queuePath);
                    throw;
                }
            }
            queue.Formatter = new BinaryMessageFormatter();
            CheckQueueCompatibility(queue);
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return queue;
        }

        private void CheckQueueCompatibility(MessageQueue queue)
        {
            try
            {
                // Queue should be transactional
                if (!queue.Transactional)
                    throw new PerfectusException(Log.Category.Receiver, new ID("ProcessCoordinator.MessageQueueNotTransactional"), queue.QueueName);

                // Queue should be SendAndReceive 
                if (queue.AccessMode != QueueAccessMode.SendAndReceive)
                    throw new PerfectusException(Log.Category.Receiver, new ID("ProcessCoordinator.MessageQueueNotSendAndReceive"), queue.QueueName);

                // Queue should be SendAndReceive 
                if (queue.DenySharedReceive)
                    Log.Warn(Log.Category.Receiver, new ID("ProcessCoordinator.MessageQueueIsDenySharedReceive"), queue.QueueName);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Receiver, new ID("ProcessCoordinator.MessageQueueFailedTest"), queue.QueueName);
            }
        }

        private bool IsPerfectusCompatibleMessage(Message message)
        {
            bool success = true;

            if (success && message.Body == null)
            {
                Log.Error(Log.Category.Receiver, new ID("ProcessCoordinator.MessageQueuePayloadEmpty"),
                        message.Label,
                        reveiverQueue.QueueName);
                success = false;
            }
            if (success && (!(message.Body is ProcessingTask)))
            {
                Log.Error(Log.Category.Receiver,
                   new ID("ProcessCoordinator.MessageQueuePayloadMalformed"),
                       message.Label,
                       reveiverQueue.QueueName,
                       message.Body.GetType().ToString());
                success = false;
            }
            return success;
        }

        #region IMessageReader Members


        #endregion


        private void PeekMessage()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            // timeout 5 sec.
            object dummyState = new object();

            AsyncCallback callBack = new AsyncCallback(reveiverQueue_PeekAsync);

            if (queueCursorAction == PeekAction.Current)
                queueCursor = reveiverQueue.CreateCursor();

            reveiverQueue.BeginPeek(
                new System.TimeSpan(0, 0, 30),
                queueCursor,
                queueCursorAction,
                dummyState,
                new AsyncCallback(reveiverQueue_PeekAsync));
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        void reveiverQueue_PeekAsync(IAsyncResult result)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            try
            {
                Message m = reveiverQueue.EndPeek(result);
                Log.Trace(Log.Category.Receiver, 10, "Calling subscribers");
                if (IsPerfectusCompatibleMessage(m) &&
                    CanUseMessage(m))
                    OnJobAvailable();
                queueCursorAction = PeekAction.Next;
            }
            catch (MessageQueueException msmqException)
            {
                if (msmqException.MessageQueueErrorCode != MessageQueueErrorCode.IOTimeout)
                    throw new PerfectusException(
                        Log.Category.Receiver,
                        new ID("ProcessCoordinator.MessageQueueErrorPeek"),
                        msmqException,
                        reveiverQueue.QueueName);

                queueCursorAction = PeekAction.Current;
                Log.Trace(Log.Category.Receiver, 10, "Timeout");
            }

            finally
            {
                PeekMessage();
            }
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        private bool CanUseMessage(Message m)
        {
            // Setting the label initiates the retry counter
            Job dummyJob = new Job();
            dummyJob.Label = m.Label;

            // Delay the job?
            if (dummyJob.Retry > 0)
            {
                int multiplyer = 1;

                // double the delay time for each retry?
                if (_delayEachRetry)
                    multiplyer = System.Convert.ToInt32(Math.Pow(2, dummyJob.Retry - 1));
                
                TimeSpan timeSpanForThisMessage = new TimeSpan(_delayTimeSpan.Ticks * multiplyer);
                if (DateTime.Now - m.ArrivedTime < timeSpanForThisMessage)
                {
                    Log.Trace(Log.Category.Receiver, Log.Priority.HIGH, String.Format("Job found, but won't execute before {0}", m.ArrivedTime + timeSpanForThisMessage));
                    return false;
                }
            }
            return true;
        }



        public ProcessingTask readJob(out Object id)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            id = String.Empty;

            MessageQueueTransaction transaction = new MessageQueueTransaction();
            transaction.Begin();
            try
            {
                ProcessingTask processingTask = null;
                Message message = reveiverQueue.Receive(transaction);

                Log.Trace(Log.Category.Receiver, 10, "Received message");

                if (IsPerfectusCompatibleMessage(message))
                {
                    processingTask = message.Body as ProcessingTask;
                    Job job = new Job(processingTask);
                    job.Label = message.Label;
                    id = job.Label;

                    transactionQueue.Send(message, job.Label, transaction);

                    transaction.Commit();


                    Log.Transaction(
                    new ID("Transaction.JobAssembling"),
                        processingTask.InstanceId.ToString(),
                        processingTask.PackageId.ToString(),
                        processingTask.VersionNumber.ToString(),
                        processingTask.RevisionNumber.ToString(),
                        processingTask.PageToProcessId.ToString(),
                        processingTask.TaskSubmittedBy,
                        job.Label,
                        Identity,
                        message.Id.ToString());

                    Log.Trace(Log.Category.Receiver, 10, String.Format("Message: {0}", processingTask.ToString()));

                }
                else
                {
                    reportingQueue.Send(message, message.Label, transaction);
                    transaction.Commit();
                    Log.Transaction(
                        new ID("ProcessCoordinator.IncompatibleMessage"),
                        message.Id,
                        message.Label);

                    Log.Trace(Log.Category.Receiver, 10, String.Format("Message {0} is incompatible", message.Label));
                }


                Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);

                return processingTask;
            }

            catch (Exception ex)
            {
                transaction.Abort();

                if (ex is PerfectusException)
                    throw ex;

                throw new PerfectusException(
                Perfectus.Library.Logging.Log.Category.Receiver,
                new ID("NOT_IMPLEMENTED"),
                ex);
            }
        }

        private Message GetMessage(String id, MessageQueueTransaction transaction)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            Cursor messageQueueSerachCursor = transactionQueue.CreateCursor();

            PeekAction action = PeekAction.Current;
            try
            {
                while (true)
                {
                    Message msg = transactionQueue.Peek(new TimeSpan(1), messageQueueSerachCursor, action);
                    if (msg.Label.Contains(id))
                    {
                        msg = transactionQueue.Receive(new TimeSpan(1000), messageQueueSerachCursor, transaction);
                        return msg;
                    }
                    action = PeekAction.Next;
                }
            }
            catch
            {
                return null;
            }
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);

        }

        public new void HandleResult(Object id, JobStatus  jobStatus)
        {
            bool success = !(jobStatus == JobStatus.ERROR);
            
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            MessageQueueTransaction transaction = new MessageQueueTransaction();

            try
            {
                transaction.Begin();
                Message message = GetMessage(id.ToString(), transaction);

                Job job = new Job((ProcessingTask)message.Body);
                job.Label = message.Label;

                if (!success)
                {
                    if (CanReQueue(job))
                        ReQueue(job, transaction);
                    else
                        Fail(job);
                }
                else
                    Complete(job);

                transaction.Commit();
            }
            catch
            {
                transaction.Abort();
            }
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        private void Complete(Job job)
        {
            Log.Transaction(new ID("Transaction.JobComplete"), job.Label);
        }

        private void Fail(Job job)
        {
            Log.Transaction(new ID("Transaction.JobFailed"), job.Label);
        }

        private void ReQueue(Job job, MessageQueueTransaction transaction)
        {
            Message message = new Message();
            message.Formatter = reveiverQueue.Formatter;
            message.Body = (ProcessingTask)job;

            reveiverQueue.Send(message, job.Label, transaction);
            Log.Transaction(new ID("Transaction.JobRequeud"), job.Label, job.Retry, message.Id);
        }

        private void OnJobAvailable()
        {
            if (jobAvailable != null)
                jobAvailable(this.readJob, this.HandleResult);
        }


        #region IMessageReader Members

        public void Start(JobAvailable ja)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            jobAvailable += ja;
            PeekMessage();
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        public void Stop()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            jobAvailable = null;
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        #endregion


        #region IMessageReader Members


        public void MaybeFreeOrphans(int timeoutSeconds)
        {
        }

        #endregion
    */
    }
}
