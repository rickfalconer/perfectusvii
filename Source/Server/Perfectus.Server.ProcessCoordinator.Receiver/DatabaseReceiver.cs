using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Library.Logging;
using System.Reflection;
using Perfectus.Server.ConfigurationSystem;
using System.Threading;
using System.Data;
using Perfectus.Server.Common.Data;
using Perfectus.Server.DataAccess.DatabaseJobQueue;
using Perfectus.Library.Localisation;
using Perfectus.Library.Exception;

namespace Perfectus.Server.ProcessCoordinator.Receiver
{
    class DatabaseReceiver : BaseReceiver, IMessageReader
    {
        private static DatabaseReceiver _ptr = null;
        
        private ManualResetEvent _mre = null;
        private Thread _timer = null;
        private event JobAvailable jobAvailable;

        private DatabaseReceiver() {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            Log.Info(Log.Category.Receiver, new ID("ProcessCoordinator.DatabaseReaderInit"), Identity);
            
            if (!Config.Shared.queue.databaseQueue)
                throw new PerfectusException(Log.Category.Receiver, new ID("ProcessCoordinator.DatabaseReaderNotEnabled"));
            
            DatabaseJobQueue.SetConfig(_delayTime);

            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);

            MaybeFreeOrphans(Config.Shared.queue.timeout);
        }


        internal static IMessageReader MessageReader()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            try
            {
                if (_ptr == null)
                    _ptr = new DatabaseReceiver();
            }
            catch { }
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return _ptr;
        }

        public override bool HealthCheck(out string reason)
        {
            return DatabaseJobQueue.PerformConnectivityCheck( out reason );
        }

        private  void OnJobAvailable()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);
            if (jobAvailable != null)
                jobAvailable(this.readJob, this.HandleResult);
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        public ProcessingTask readJob(out Object messageId)
        {
            messageId = null;

            ProcessingTask processingTask = DatabaseJobQueue.Read(Identity, out messageId);
        
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
            return processingTask;
        }


        public void HandleResult(Object messageId, JobStatus jobStatus)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            switch (jobStatus)
            {
                case JobStatus.ASSEMBLE:
                    DatabaseJobQueue.Assemble(messageId);
                    break;
                case JobStatus.DISTRIBUTE:
                    DatabaseJobQueue.Distribute(messageId);
                    break;
                case JobStatus.COMPLETE:
                    DatabaseJobQueue.Complete(messageId);
                    break;
                case JobStatus.CONVERT:
                    DatabaseJobQueue.Convert(messageId);
                    break;
                case JobStatus.ERROR:
                    Job job = DatabaseJobQueue.ReadNoWait(messageId);
                    if (CanReQueue(job))
                        DatabaseJobQueue.Queue(messageId);
                    else
                        DatabaseJobQueue.Fail(messageId);
                    break;
                default :
                    throw new Exception("case not caterred for");
            }
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }
        

        public  void PollDatabase(object data)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            ManualResetEvent mre = (ManualResetEvent)data;

            while (true)
            {
                int numOfJobsQueued = DatabaseJobQueue.Peek();
                for (int jobNumber = 0; jobNumber < numOfJobsQueued; jobNumber++)
                    OnJobAvailable();

                if (mre.WaitOne(new TimeSpan(0, 0, 10), false))
                    break;
            }

            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        #region IMessageReader Members

        public void Start(JobAvailable jobAvailableDelegate)
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            jobAvailable += jobAvailableDelegate;
            _mre = new ManualResetEvent(false);

            _timer = new Thread(new ParameterizedThreadStart(PollDatabase));
            _timer.Start(_mre);

            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        public void Stop()
        {
            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.In);

            _mre.Set();
            // Wait for 30 seconds
            _timer.Join(new TimeSpan(0, 0, 30));

            if (_timer.ThreadState != ThreadState.Stopped)
                _timer.Abort();

            _mre.Close();
            _timer = null;
            _mre = null;

            Log.Trace(Log.Category.Receiver, MethodBase.GetCurrentMethod(), Log.InOut.Out);
        }

        #endregion

        public void MaybeFreeOrphans(int timeoutSeconds) {
            DatabaseJobQueue.ResetOverdueJobs(timeoutSeconds);
        }
    }
}
