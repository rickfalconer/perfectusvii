using System;
using Perfectus.Server.Common.Data;

namespace Perfectus.Server.PackageManager
{
	/// <summary>
	/// Summary description for Retrieval.
	/// </summary>
	public sealed class Retrieval
	{
		private Retrieval()
		{
		}

		public static byte[] GetTemplateBytes(Guid packageId, int versionNumber, int revisionNumber, Guid templateId)
		{
			return DataAccess.PackageManager.Retrieval.GetTemplateBytes(packageId, versionNumber, revisionNumber, templateId);
		}

		public static byte[] GetOutcomeActionBytes(Guid packageId, int versionNumber, int revisionNumber, Guid outcomeId, Guid actionId)
		{
			return DataAccess.PackageManager.Retrieval.GetOutcomeActionBytes(packageId, versionNumber,  revisionNumber, outcomeId, actionId);
		}

		public static PackageListSet GetPackageList()
		{
			return DataAccess.PackageManager.Retrieval.GetPackageList(null);
		}

		public static PackageListSet GetPackageList(string createdBy)
		{
			return DataAccess.PackageManager.Retrieval.GetPackageList(createdBy);
		}

		public static StatusSet GetPackageStatus(string createdBy, int recentTransactionCount, int inProgressStart, int inProgressEnd, int myHistoryStart, int myHistoryEnd, out int inProgressRecordCount, out int historyRecordCount)
		{
			return DataAccess.PackageManager.Retrieval.GetPackageStatus(createdBy, recentTransactionCount, inProgressStart, inProgressEnd, myHistoryStart, myHistoryEnd, out inProgressRecordCount, out historyRecordCount);
		}
	}
}
