using System;
using System.IO;
using System.Runtime.Serialization;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Reporting;
using System.Collections.Generic;
namespace Perfectus.Server.PackageManager
{
	/// <summary>
	/// Summary description for Publishing.
	/// </summary>
	public sealed class Publishing
	{
		private Publishing()
		{
		}
		public static void Publish(Package package, bool isNewVersion)
		{
			int versionNumber;
			int revisionNumber;
			Publish(package, isNewVersion, out versionNumber, out revisionNumber);
		} 

		//		[Obsolete("Use the one in InterviewSystem.InstanceManagement")]
		//		public static void RenameInstance(int instanceId, string newName)
		//		{
		//			DataAccess.PackageManager.Publishing.RenameInstance(instanceId, newName);
		//		}
		//
		//		[Obsolete]
		//		public static void PackageInstances_Delete(int folderId, Guid packageId, int versionNumber, int revisionNumber)
		//		{
		//			DataAccess.PackageManager.Publishing.PackageInstances_Delete(Person.LoggedinPersonIdGet(), folderId, packageId, versionNumber, revisionNumber, DateTime.UtcNow);
		//		}

		public static void Publish(Package package, bool isNewVersion, out int versionNumber, out int revisionNumber)
		{
			Guid id = package.UniqueIdentifier;
			string name = package.Name;
			
			string startPageName;
			Guid startPageId;
			if (package.Interviews.Count > 0)
			{
				startPageName = package.Interviews[0].StartPage.Name;
				startPageId = package.Interviews[0].StartPage.UniqueIdentifier;
				package.Interviews[0].Diagram = null;
			}
			else
			{
				startPageName = null;
				startPageId= Guid.Empty;
			}
            /*
            TryTestUpdateV6(ref package);
            */
			TemplateDocumentCollection templates = package.Templates;
			OutcomeCollection outcomes = package.Outcomes;
            List<AttachmentBase> attachments = package.Attachments;

            string versionXML = package.VersionXml;
            if (versionXML == null || versionXML.Length == 0)
            {
                versionXML = "<?xml version=\"1.0\"?>" +
                              "  <PackageVersionInfo>" +
                              "  <version>5.0.0.0</version>" +
                              "  </custom>" +
                              "</PackageVersionInfo>";
            }

			string createdBy = package.CreatedBy;
			string modifiedBy = package.ModifiedBy;
			string publishedBy = package.PublishedBy;
			DateTime createdUTC = package.CreationDateTime;
			DateTime modifiedUTC = package.ModificationDateTime;
			DateTime publishedUTC = package.PublishedDateTime;
			string description = package.Description;
			string serverName = Perfectus.Server.ConfigurationSystem.Config.Shared.identity.serverName;
			bool isReportingEnabled = Perfectus.Server.ConfigurationSystem.Config.Shared.reporting.enableServerReporting;

			byte[] packageBytes;

			package.PublishedDateTime = DateTime.UtcNow;

			// Remove all folders
			foreach(Question q in package.Questions)
			{
				q.ParentFolder = null;
			}

			foreach(Outcome o in package.Outcomes)
			{
				o.ParentFolder = null;
			}

			foreach(SimpleOutcome so in package.SimpleOutcomes)
			{
				so.ParentFolder = null;
			}

			package.OutcomeFolders.Clear();
			package.QuestionsFolders.Clear();
			package.SimpleOutcomeFolders.Clear();

			// Serialise the package 

			MemoryStream ms = new MemoryStream();
			
			// Persistence = do not serialise wordml (as we've already captured the templates collection)
			// Also, serialise Outcomes without any of their Actions' WordML.
			Package.SaveToStream(package, ms, StreamingContextStates.Persistence);

			// Get the bytes of the package from the stream
			ms.Seek(0, SeekOrigin.Begin);
			packageBytes = ms.ToArray();

			ms.Close();
						
			// Get the person doing this
			// int personId = Perfectus.Server.Common.Person.LoggedinPersonIdGet();
			int personId = int.MinValue;

            DataAccess.PackageManager.Publishing.Publish(id, packageBytes, personId, isNewVersion, templates, outcomes, attachments, name, startPageName, startPageId, createdBy, modifiedBy, publishedBy, createdUTC, modifiedUTC, publishedUTC, description, out versionNumber, out revisionNumber, !isReportingEnabled, versionXML);

       //     if ( package.VersionXml.Length > 0)
       //         DataAccess.PackageManager.Publishing.InsertVersionXml(id, versionNumber, revisionNumber, package.VersionXml);
			
			// Publish reporting information to reporting database if available.
			if(isReportingEnabled)
			{
                DataAccess.Reporting.Report.PublishReportingPackageInfo(id, personId, name, createdBy, modifiedBy, publishedBy, createdUTC, modifiedUTC, publishedUTC, description, versionNumber, revisionNumber, serverName,  Guid.Empty);
				DataAccess.Reporting.Report.PublishReportingLibItemInfo(id, package, versionNumber, revisionNumber);
				DataAccess.Reporting.Report.PublishReportingClauseInfo(id, outcomes, versionNumber, revisionNumber);
			}
		}
        /*
        private static void TryTestUpdateV6(ref Package package)
        {
            foreach (Perfectus.Common.PackageObjects.Server s in package.Servers)
            {
                if (s.Rule2.Count > 0 && s.Rules.Count == 0)
                    continue;

                if (s.Rules.Count > 0)
                {
                    while (s.Rules.Count > 0)
                        s.Rules.RemoveAt(0);
                    continue;
                }

                foreach (Rule r in s.Rules)
                {
                    System.Collections.Generic.List<Document> documents = new System.Collections.Generic.List<Document>();
                    Rule2 rule2 = new Rule2(s);

                    rule2.Name = r.Name;
                    rule2.DoRuleAtFinish = r.DoRuleAtFinish;
                    rule2.DeliveryRule = r.DeliveryRule;

                    foreach (Converter c in r.Converters)
                    {
                        if (c.Descriptor.PreferredDoc == "AnswerSetXml")
                        {
                            Document d = new Document((Converter)c);
                            d.Name = String.Format("{0}", c.Name);
                            documents.Add(d);
                        }
                        else foreach (TemplateDocument t in r.Templates)
                        {
                            Document d = new Document(t, (Converter)c);
                            d.Name = String.Format("{0}|{1}", t.Name, c.Name);
                            documents.Add(d);
                        }
                    }

                    foreach (Distributor d in r.Distributors)
                    {
                        Delivery delivery = new Delivery();
                        delivery.SetDistributor(d);
                        foreach (Document e in documents)
                        {
                            delivery.AddDocument(e);

                        }
                        rule2.Add(delivery);
                    }
                    s.Rule2.Add(rule2);
                    foreach (Perfectus.Common.PackageObjects.Interview interview in package.Interviews)
                        foreach (InternalPage page in interview.Pages)
                            foreach (Rule r2 in page.PageRules)
                            {
                                if (r2 == r)
                                {
                                    if (page.PageRules2 == null)
                                        page.PageRules2 = new Rule2Collection();
                                    page.PageRules2.Add(rule2);
                                }
                            }
                }
            }
        }
        */
	
		//		public static int CreateInstance(Guid PackageId, int PackageVersionNumber, int PackageRevisionNumber, string Reference, int FolderId, int CreatedByPersonId)
		//		{
		//			return DataAccess.PackageManager.Publishing.CreateInstance(PackageId, PackageVersionNumber, PackageRevisionNumber, Reference, FolderId, CreatedByPersonId, DateTime.UtcNow);
		//			
		//		}
		//
		//		[Obsolete]
		//		public static int CreateInstanceCopy(int PackageInstanceId, int PositionInStack, int CreatedByPersonId)
		//		{
		//			return DataAccess.PackageManager.Publishing.CreateInstanceCopy(PackageInstanceId, PositionInStack, CreatedByPersonId, DateTime.UtcNow);
		//		}
		//
		//		[Obsolete]
		//		public static void PackageInstanceState_Push(int PackageInstanceId, Package package, Guid PageId, string PageName, bool Completed)
		//		{
		//			//int PackageInstanceId, Guid PageId, string PageName, byte[] State
		//
		//			// If the name has changed, rename the instance stored in the DB
		//			if (package.Interviews.Count > 0)
		//			{
		//				if (package.Interviews[0].Reference != null)
		//				{
		//					if (package.Interviews[0].Reference.ValueType == TextQuestionValue.TextQuestionValueType.Question)
		//					{
		//						if (package.Interviews[0].Reference.QuestionValue != null)
		//						{
		//							Question q = package.Interviews[0].Reference.QuestionValue;
		//							if (q.Answers != null && q.Answers.Count > 0)
		//							{
		//								object answer = q.Answers[0];
		//								if (answer != null && answer is string)
		//								{
		//									string answerStr = answer.ToString();
		//							
		//									if (answerStr != null && answerStr.Trim() != package.InstanceReference)
		//									{
		//										package.InstanceReference = answerStr;
		//										RenameInstance(PackageInstanceId, answerStr.Trim());
		//									}
		//								}
		//							}
		//						}
		//					} else
		//					if (package.Interviews[0].Reference.ValueType == TextQuestionValue.TextQuestionValueType.Text)
		//					{
		//						string preferredReference = package.Interviews[0].Reference.TextValue;
		//						if (preferredReference.Trim() != package.InstanceReference)
		//						{
		//							package.InstanceReference = preferredReference;
		//							RenameInstance(PackageInstanceId, preferredReference.Trim());
		//						}
		//					}
		//				}
		//			}
		//			
		//			byte[] packageBytes;
		//
		//			// Serialise the package 
		//
		//			MemoryStream ms = new MemoryStream();
		//			
		//			// Persistence = do not serialise wordml (as we've already captured the templates collection)
		//			Package.SaveToStream(package, ms, StreamingContextStates.Persistence);
		//
		//
		//			// Get the bytes of the package from the stream
		//			ms.Seek(0, SeekOrigin.Begin);
		//			packageBytes = ms.ToArray();
		//
		//			ms.Close();
		//			
		//			// Drop it for the GC
		//			ms = null;
		//			
		//			// Get the person doing this
		//			int personId = Perfectus.Server.Common.Person.LoggedinPersonIdGet();
		//
		//			DataAccess.PackageManager.Publishing.PackageInstanceState_Push(PackageInstanceId, PageId, PageName, Completed, packageBytes, DateTime.UtcNow);
		//
		//			packageBytes = null;
		//		}


		//		
		//		public static void Package_Delete(Guid PackageId, int PackageVersionNumber, int PackageRevisionNumber)
		//		{
		//			DataAccess.PackageManager.Publishing.Package_Delete(PackageId, PackageVersionNumber, PackageRevisionNumber, Perfectus.Server.Common.Person.LoggedinPersonIdGet(), DateTime.UtcNow);
		//		}

		
		//		[Obsolete]
		//		public static void PackageInstanceId_Delete(int PackageInstanceId)
		//		{
		//			DataAccess.PackageManager.Publishing.PackageInstance_Delete(PackageInstanceId, Perfectus.Server.Common.Person.LoggedinPersonIdGet(), DateTime.UtcNow);
		//		}

		//		[Obsolete]
		//		public static void TruncateStack(int PackageInstanceId, int positionInStack)
		//		{
		//			int personId = Perfectus.Server.Common.Person.LoggedinPersonIdGet();
		//
		//			DataAccess.PackageManager.Publishing.TruncateStack(PackageInstanceId, positionInStack, personId);
		//		}
		//		
		//		[Obsolete]
		//		public static void PackageInstance_File_AddUpdate(int PackageInstanceId, int SequenceNumber, Guid QuestionUID, int PositionInStack, string OriginalFilePath, string ContentType, byte[] FileData)
		//		{
		//			DataAccess.PackageManager.Publishing.PackageInstance_File_AddUpdate(PackageInstanceId, SequenceNumber, QuestionUID, PositionInStack, OriginalFilePath, ContentType, FileData);
		//		}
		//		
		//		[Obsolete]
		//		public static void PackageInstance_File_Delete(int PackageInstanceId, int SequenceNumber, Guid QuestionUID)
		//		{
		//			DataAccess.PackageManager.Publishing.PackageInstance_File_Delete(PackageInstanceId, SequenceNumber, QuestionUID);
		//		}

	}
}
