﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Perfectus.Common.PackageObjects;
using Perfectus.DataAccess;
using Perfectus.Common.Helper;

namespace Perfectus.Server.PackageManager
{
    /// <summary>
    /// Package operations exposed to non-perfectus clients, via services, e.g. RestService.
    /// The caller (web service) is responsible for translating client objects to perfectus objects.
    /// This class is stateless.
    /// </summary>
    public static class PackageImage
    {
        /// <summary>
        /// Creates a new PackageImage record in the database using the supplied package object.
        /// </summary>
        /// <param name="package">A Perfectus Package object</param>
        public static void CreatePackageImage(Package package)
        {
            PackageImageInsert(package);
        }

        /// <summary>
        /// Loads a PackageImage record, deserialises it into a Package object
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Perfectus Package object</returns>
        public static Package ReadPackageImage(System.Guid id)
        {
            return PackageImageLoad(id);
        }

        /// <summary>
        /// Updates a PackageImage record with the serialised contents of a Package.
        /// </summary>
        /// <param name="package">A Perfectus Package object</param>
        public static void UpdatePackageImage(Package package)
        {
            PackageImageUpdate(package);
        }

        /// <summary>
        /// Deletes a PackageImage record, including the Package contained in the PackageImage.
        /// </summary>
        /// <param name="id">PackageImage GUID</param>
        public static void DeletePackageImage(System.Guid id)
        {
            PackageImageDelete(id);
        }

        #region packageimage private ops

        private static void PackageImageInsert(Package package)
        {
            byte[] packageBytes;

            // Serialise the package 
            MemoryStream ms = new MemoryStream();
            Package.SaveToStream(package, ms, StreamingContextStates.CrossAppDomain);

            // Get the bytes of the package from the stream
            ms.Seek(0, SeekOrigin.Begin);
            packageBytes = ms.ToArray();
            ms.Close();

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGEIMAGE_INSERT";

                fleet.AddInParameterGuid("PackageImageId", package.UniqueIdentifier);
                fleet.AddInParameterString("Name", package.Name);
                fleet.AddInParameterString("CreatedBy", package.CreatedBy);
                fleet.AddInParameterString("ModifiedBy", package.CreatedBy);
                fleet.AddInParameterDateTime("WhenCreated", System.DateTime.UtcNow);
                fleet.AddInParameterDateTime("WhenModified", System.DateTime.UtcNow);
                fleet.AddInParameterBinary("PackageImageBytes", packageBytes);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        private static void PackageImageUpdate(Package package)
        {
            byte[] packageBytes;

            // Serialise the package 
            MemoryStream ms = new MemoryStream();
            Package.SaveToStream(package, ms, StreamingContextStates.CrossAppDomain);

            // Get the bytes of the package from the stream
            ms.Seek(0, SeekOrigin.Begin);
            packageBytes = ms.ToArray();
            ms.Close();

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGEIMAGE_UPDATE";

                fleet.AddInParameterGuid("PackageImageId", package.UniqueIdentifier);
                fleet.AddInParameterString("Name", package.Name);
                fleet.AddInParameterString("ModifiedBy", package.ModifiedBy);
                fleet.AddInParameterDateTime("WhenModified", System.DateTime.UtcNow);
                fleet.AddInParameterBinary("PackageImageBytes", packageBytes);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        private static void PackageImageDelete(Guid id)
        {
            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGEIMAGE_DELETE";

                fleet.AddInParameterGuid("PackageImageId", id);

                fleet.ExecuteNonQuerySP(commandName);
            }
        }

        private static Package PackageImageLoad(Guid id)
        {
            object o = null;

            using (IFleet fleet = FleetContainer.Create())
            {
                string commandName = "PACKAGEIMAGE_GET";

                fleet.AddInParameterGuid("PackageImageId", id);

                IDataReader rdr = null;

                try
                {
                    rdr = fleet.ExecuteReaderSP(commandName);
                    //outBytes = GetBytesFromDataReader(rdr);
                    using (rdr)
                    using (MemoryStream ms = new MemoryStream())
                    {
                        const int chunkSize = 1024;
                        if (rdr.Read())
                        {
                            byte[] buffer = new byte[chunkSize];
                            long idx = 0;
                            long size = 0;
                            //Write the BLOB chunk by chunk to the stream.
                            while ((size = rdr.GetBytes(0, idx, buffer, 0, chunkSize)) == chunkSize)
                            {
                                ms.Write(buffer, 0, chunkSize);
                                idx += chunkSize;
                            }
                            //Write the last bytes.
                            byte[] remaining = new byte[size];
                            Array.Copy(buffer, 0, remaining, 0, size);
                            ms.Write(remaining, 0, remaining.Length);
                        }

                        // Rewind the stream
                        ms.Seek(0, SeekOrigin.Begin);
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Binder = new PackageSerialisationBinder();
                        formatter.Context = new StreamingContext(StreamingContextStates.CrossAppDomain);

                        if (CompressionHelper.IsCompressed(ms))
                        {
                            // What kind of stream do we have ?!?
                            byte[] fBytes = new byte[ms.Length];
                            int cnt = 0;
                            int k = ms.ReadByte();
                            while (k != -1)
                            {
                                fBytes[cnt] = (byte)k;
                                k = ms.ReadByte();
                                cnt++;
                            }
                            MemoryStream m = new MemoryStream(CompressionHelper.DecompressIfNecessary(fBytes));

                            o = formatter.UnsafeDeserialize(m, null);
                        }
                        else
                        {
                            o = formatter.UnsafeDeserialize(ms, null);
                        }
                    }
                }
                finally
                {
                    if (rdr != null)
                    {
                        rdr.Close();
                    }
                }
            }
            return o as Package;
        }

        #endregion packageimage private ops
    }

}
