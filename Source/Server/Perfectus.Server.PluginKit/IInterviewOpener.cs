using System.Xml;

namespace Perfectus.Server.PluginKit
{
	public interface IInterviewOpener
	{	
		string ClientSideFunctionName
		{
			get;
		}

		string DocRefFieldId
		{
			get;
		}

		XmlDocument GetAnswerSet(string documentReference);

	}
}
