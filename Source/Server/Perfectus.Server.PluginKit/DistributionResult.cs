using System;

namespace Perfectus.Server.PluginKit
{
    [Serializable]
    public class DistributionResult2 : DistributionResult
    {
        private string distributorName;
        public string DistributorName
        {
            get { return distributorName; }
            set { distributorName = value; }
        }
        private string mIMEFormat;
        public string MIMEFormat
        {
            get { return mIMEFormat; }
            set { mIMEFormat = value; }
        }

        private int jobQueueID;
        public int JobQueueID
        {
            get { return jobQueueID; }
            set { jobQueueID = value; }
        }
        private string pageName;
        public string PageName
        {
            get { return pageName; }
            set { pageName = value; }
        }
        private Guid pageId;
        public Guid PageId
        {
            get { return pageId; }
            set { pageId = value; }
        }
        public DistributionResult2(Guid distributorId, string link, string linkText, Guid templateId, string templateName, string internalReference)
            : base(distributorId, link, linkText, templateId, templateName, internalReference)
        { }
        public DistributionResult2(DistributionResult result)
            : this (result.DistributorId, result.Link, result.LinkText, result.TemplateId, result.TemplateName, result.InternalReference )
        { }
    }
	

	/// <summary>
	/// Summary description for DistributionResult.
	/// </summary>
	[Serializable]
	public class DistributionResult
	{
		private string link;
		private string linkText;
		private Guid templateId;
		private string templateName;
		private string internalReference;
		private Guid distributorId;

		public string Link
		{
			get { return link; }
		}

		public string LinkText
		{
			get { return linkText; }
		}

		public Guid TemplateId
		{
			get { return templateId; }
		}

		public string TemplateName
		{
			get { return templateName; }
		}

		public string InternalReference
		{
			get { return internalReference; }
		}

		public Guid DistributorId
		{
			get { return distributorId; }
		}

		public DistributionResult(Guid distributorId, string link, string linkText, Guid templateId, string templateName, string internalReference)
		{
			this.distributorId = distributorId;
			this.link = link;
			this.linkText = linkText;
			this.templateId = templateId;
			this.templateName = templateName;
			this.internalReference = internalReference;
		}
	}
}
