using System;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginKit
{
	/// <summary>
	/// Summary description for IFetcher.
	/// </summary>
	public interface IFetcher
	{
		/// <summary>
		///		Retrieve a document from an external store (e.g. DMS)
		/// </summary>
		/// <param name="documentId">The arbitrary (string) ID of the document</param>
		/// <param name="contentType">Must return the mime type (e.g. text/xml) of the retrieved document.</param>
		/// <returns></returns>
		Stream Fetch(string documentId, Package instance, Guid instanceId, out string contentType);
	}
}
