using System;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginKit
{
    /// <summary>
    ///     Invoked when the user clicks the Suspend button in the Interview control.
    /// </summary>
    public interface IInterviewSuspender
    {
        /// <summary>
        ///     Invoked when the user clicks the Suspend button in the Interview control.
        /// </summary>
        /// <param name="instance">The Package Instance that was suspended.</param>
        /// <param name="instanceId">The unique identifier of the instance in the Perfectus database.</param>
        void Suspend(Package instance, Guid instanceId);
    }
}
