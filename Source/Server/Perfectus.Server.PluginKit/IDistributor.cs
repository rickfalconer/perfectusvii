using System;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginKit
{
	/// <summary>
	///		Distributor plug-ins must implement this interface.
	/// </summary>
	public interface IDistributor
	{
        /// <summary>
        /// Metadata that is used for string bound questions assigned to distributor properties
        /// </summary>
        StringBindingMetaData InstanceMetaData
        {
            get;
            set;
        }
        
        /// <summary>
		///		The unique identifier of the package instance (interview) from which the documents were generated.
		/// </summary>
		Guid PackageInstanceId
		{
			get;
			set;
		}
		
		/// <summary>
		///		A unique identifier for each individual plug-in class.
		/// </summary>
		Guid UniqueIdentifier
		{
			get;
		}

		/// <summary>
		///		The name of the authenticated user when the interview was submitted to the ProcessCoordinator.
		/// </summary>
		string UserId
		{
			get;
			set;

		}

		/// <summary>
		///		Append one or several documents for distribution, wrapped in StreamReader objects.
		/// </summary>
		/// <param name="sr">The document to be distributed should be passed to this plug-in as a StreamReader.</param>
		/// <param name="fileName">The filename that was given to the document as it was assembled and converted.</param>
		/// <param name="templateId">The GUID of the template from which the document was created. If no template was used (e.g. an AnswerSet) this should be Guid.Empty</param>
		/// <param name="templateName">The name of the template from which the document was created. Used in the DistributionResult log table.</param>
		void AppendStreamReader(StreamReader sr, string fileName, Guid templateId, string templateName);

		/// <summary>
		///		This method is called when the ProcessCoordinator has appeneded all expected documents. It should enumerate the StreamReaders added by AppendStreamReader 
		///		and distribute each document.
		/// </summary>
		/// <returns>
		///		Returns an array of DistributionResult records describing the status of each appeneded document. The details in this array are logged in the DistributionResult table,
		///		and appear in the Perfectus Status control.
		/// </returns>
		DistributionResult[] Distribute();
	}
}
