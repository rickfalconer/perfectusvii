using System;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginKit
{
	/// <summary>
	/// Summary description for IConverter.
	/// </summary>
	public interface IConverter
	{
        /// <summary>
        /// Metadata that is used for string bound questions assigned to distributor properties
        /// </summary>
        StringBindingMetaData InstanceMetaData
        {
            get;
            set;
        }
        
        SourceDocument DocumentTypeConverted
		{
			get;
		}

		Guid UniqueIdentifier
		{
			get;
		}

		Stream ConvertMemoryStream(StreamReader source, string fileNameWithoutExtension, out string convertedFileName);
	}
}
