using System.Xml;

namespace Perfectus.Server.PluginKit
{
	public interface IInterviewSaver
	{	
		string ClientSideFunctionName
		{
			get;
		}

		string DocRefFieldId
		{
			get;
		}

		void SaveAnswerSet(XmlDocument answerSet, string documentReference);

	}
}
