
using Perfectus.Common.PackageObjects;
namespace Perfectus.Server.PluginKit
{
	/// <summary>
	/// Summary description for IStringFormatter.
	/// </summary>
	public interface IStringFormatter
	{
		// Accepts a string and paramaters, and converts to a new string.
		string FormatString(string inputString, string inputParams);
	}

    public interface IStringFormatter2 : IStringFormatter
    {
        // Accepts a string and paramaters, and converts to a new string.
        string FormatQuestion(Question inputQuestion, string inputString, string parameter);
    }
}
