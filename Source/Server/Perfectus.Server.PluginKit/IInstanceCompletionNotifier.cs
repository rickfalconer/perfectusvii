using System;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.PluginKit
{
    /// <summary>
    ///     This plug-in is invoked at the completion of every ProcessCoordinator task (Assembly, Conversion and Distribution).
    /// </summary>
    public interface IInstanceCompletionNotifier
    {
        /// <summary>
        ///     This method is called when the processing of a given Instance is complete.
        ///     Typically it would call the Status API to determine further details about the instance.
        /// </summary>
        /// <param name="instanceId"></param>
        /// <param name="instance"></param>
        /// <param name="statusXml"></param>
        void Notify(Guid instanceId, Package instance, string statusXml);

    }
}
