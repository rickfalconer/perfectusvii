using System;

namespace Perfectus.Server.PluginKit
{
	/// <summary>
	///		Types of document that can be passsed to a Converter plug-in by the <b>Process Coordinator</b>.
	/// </summary>
	public enum SourceDocument
	{
		/// <summary>
		///		The Converter operates on the assembled WordML document.
		/// </summary>
		WordML, 
		/// <summary>
		///		The Converter operates on the raw AnswerSet XML document.
		/// </summary>
		AnswerSetXml
	};
}
