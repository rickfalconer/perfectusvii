using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using Perfectus.Server.InterviewSystem;
using System.Reflection;

namespace Perfectus.Server.ProcessCoordinator.Engine2
{
    class ControllerThread
    {
        AutoResetEvent _finishedEvent;
        WorkerData _workerData;
        int _timeoutSeconds;
        
        Int32 _jobId;
        String _packageName;
        ProcessingTask _task;
        JobStatus _lastJobStatus = JobStatus.QUEUE;

        public ControllerThread(AutoResetEvent finishedEvent, WorkerData workerData)
        {
            _finishedEvent = finishedEvent;
            _workerData = workerData;
            _timeoutSeconds = Config.Shared.queue.timeout;
        }

        internal void jobStatusValues(JobStatus jobStatus, String message)
        {
            if (jobStatus > JobStatus.QUEUE && 
                jobStatus < JobStatus.ERROR && 
                jobStatus >= _lastJobStatus)
            {
                _lastJobStatus = jobStatus;
                if ( message != null && message.Length > 0)
                    LOG.Info(jobStatus, _packageName, _jobId, _task.InstanceId, message);
                _workerData.jobResult(_jobId, jobStatus);
            }
            else throw new Exception("not accepted");
        }

        public void Start()
        {
            Thread tWorker = null;

            try
            {
                Object objJob;
                _task = _workerData.jobRead(out objJob);
                _jobId = System.Convert.ToInt32(objJob);

                if (_task == null)
                    _workerData.Result = WorkerData.resultEnum.Unstarted;
                else
                {
                    _packageName = InstanceManagement.GetInstanceReference(_task.InstanceId);

                    // Setup worker thread
                    ManualResetEvent workerFinishedEvent = new ManualResetEvent(false);
                    WorkerThread f = new WorkerThread(workerFinishedEvent, jobStatusValues, _task, _jobId);

                    // Start worker thread
                    tWorker = new Thread(new ThreadStart(f.ThreadStartProc));
                    tWorker.Name = "Worker-";
                    tWorker.Start();

                    LOG.Info(JobStatus.QUEUE, _packageName, _jobId, _task.InstanceId, "Assigned for processing");
                    Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Controller - Started worker");

                    _workerData.Result = WorkerData.resultEnum.Running;

                    // Wait for the worker to finish, or until timeout reached
                    if (!WaitHandle.WaitAll(new WaitHandle[] { workerFinishedEvent }, _timeoutSeconds * 1000, false))
                    {
                        // timeout -> Abort worker
                        _workerData.Result = WorkerData.resultEnum.Failed;
                        tWorker.Abort();

                        LOG.Info(JobStatus.ERROR, _packageName, _jobId, _task.InstanceId, "!!!TimeOut");
                        _workerData.jobResult(_jobId, JobStatus.ERROR);

                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "Controller - Worker timed out");

                        try
                        {
                            Perfectus.Server.DataAccess.Logging.LogException(
                                DateTime.UtcNow,
                                "ERRO",
                                int.MinValue,
                                _task.PackageId,
                                _task.VersionNumber,
                                _task.RevisionNumber,
                                _task.InstanceId,
                                "Job timed out",
                                String.Format("Job with Id '{0}' did not complete within {1} seconds and was aborted.", _jobId, _timeoutSeconds),
                                String.Empty,
                                Assembly.GetCallingAssembly().GetName().Name,
                                Assembly.GetCallingAssembly().CodeBase,
                                Assembly.GetCallingAssembly().GetName().Version.ToString(4),
                                int.MinValue, _jobId, null, null, "Assembly");
                        }
                        catch { }
                    }
                    else
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Controller - Worker success");

                        // Success
                        _workerData.Result = f.Result;

                        if (_workerData.Result == WorkerData.resultEnum.Finished)
                        {
                            LOG.Info(JobStatus.COMPLETE, _packageName, _jobId, _task.InstanceId, "Complete");
                            _workerData.jobResult(_jobId, JobStatus.COMPLETE);
                        }
                        else
                        {
                            LOG.Info(JobStatus.ERROR, _packageName, _jobId, _task.InstanceId, "!!!Error");
                            _workerData.jobResult(_jobId, JobStatus.ERROR);
                        }
                    }
                }
            }
            
            catch (Exception ex) {

                if ( ex is ThreadAbortException)
                    Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Controller - Aborted by main thread");
                else
                
                Log.Error(Log.Category.Engine2, ex, new ID("ProcessCoordinator.ControllerFailed"));
                // Controler is cancelled by main thread
                try
                {
                    _workerData.Result = WorkerData.resultEnum.Failed;
                    _workerData.jobResult(_jobId, JobStatus.ERROR);
                    tWorker.Abort();
                }
                catch { }
            }
            finally
            {
                _finishedEvent.Set();
            }
        }
    }
}