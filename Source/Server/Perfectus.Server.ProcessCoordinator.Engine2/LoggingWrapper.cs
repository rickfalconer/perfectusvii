using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.Common.Data;
using Perfectus.Library.Localisation;
using Perfectus.Library.Logging;

namespace Perfectus.Server.ProcessCoordinator.Engine2
{
    static class LOG
    {
        internal static void Info(JobStatus jobStatus, String packageName,
            int jobId, Guid packageInstance,
            String message)
        {
            Log.Info(Log.Category.Engine2, new ID("ProcessCoordinator.pattern"),
                jobStatus != JobStatus.INVALID ? jobStatus.ToString() : String.Empty,
                packageName != null ? packageName : String.Empty,
                jobId >= 0 ? jobId : 0,
                packageInstance != Guid.Empty ? packageInstance.ToString() : String.Empty,
                message);
        }

        internal static void Info ( JobStatus jobStatus, String packageName, 
            int jobId, Guid packageInstance,
            ID id, params IConvertible[] messages)
        {
            Info(jobStatus, packageName, jobId, packageInstance, id.ToString(messages));
        }
    }
}
