using System;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Server.AssemblySystem;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.PackageManager;
using Perfectus.Server.PluginKit;
using Perfectus.Server.Reporting;
using PluginBehaviour=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginBehaviour;
using PluginDescriptor=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginDescriptor;
using PluginDescriptorCollection=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginDescriptorCollection;
using Plugins=Perfectus.Server.ConfigurationSystem.Plugins;
using Perfectus.Library.Logging;
using Perfectus.Library.Exception;
using Perfectus.Library.Localisation;
using System.Collections.Generic;


namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {
        private readonly string cacheKeyFormat = "Template_{0}_{1}.{2}";
        private Package package;
        private int versionNumber = int.MinValue;
        private int revisionNumber = int.MinValue;
        private Guid instanceId = Guid.Empty;
        private Guid packageId = Guid.Empty;
        private string userId;
        private PluginDescriptorCollection pluginsOnServer = Plugins.PluginDescriptors;
        private InternalPage pageToProcess = null;
        private StringBindingMetaData stringBindingMetaData;

        private StatusUpdateDelegate _updateProc;
        private int _jobId;

        public Agent(ProcessingTask task, StatusUpdateDelegate updateProc, int jobId)
        {
            try
            {
                _jobId = jobId;

                package = InstanceManagement.GetInstance(task.InstanceId);
                packageId = package.UniqueIdentifier;
                versionNumber = task.VersionNumber;
                revisionNumber = task.RevisionNumber;
                instanceId = task.InstanceId;
                userId = task.TaskSubmittedBy;

                _updateProc = updateProc;

                Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Agent started for {0}", task.ToString()));
                if (task.PageToProcessId != Guid.Empty)
                {
                    pageToProcess = package.InterviewPageByGuid[task.PageToProcessId] as InternalPage;
                    Log.Trace(Log.Category.Engine2, Log.Priority.MID, String.Format("Processing internal page {0}", pageToProcess.Name));
                }
                string metaDataPackageReference = InstanceManagement.GetInstanceReference(instanceId);
                stringBindingMetaData = new StringBindingMetaData(package.Name, metaDataPackageReference, instanceId, versionNumber, revisionNumber);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.ProcessCoordinatorError"), ex, packageId.ToString(), versionNumber, revisionNumber, instanceId.ToString());
            }
        }

        /// <summary>
        /// Start
        /// </summary>
        public void Start()
        {
            try
            {
                CreateTempFolder();
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.FailedCreateTempDirectory"), ex, _jobId, instanceId.ToString());
            }

            try
            {
                if (pageToProcess != null)
                    StartPerPageProcess();
                else
                    StartFinalProcess();
            }
            catch (Exception ex)
            {
                // Keep teh already generated results for the retry or later investigation
                WriteAllCacheToDiskAndClear();

                // PerfectusException should use the log automatically
                if (!(ex is PerfectusException))
                    Log.Error(Log.Category.Engine2, ex, new ID("ProcessCoordinator.RuleError"),
                            string.Format("Package {0} v{1}.{2}, Instance {3}", packageId, versionNumber, revisionNumber, instanceId));


                Exception innerException = ex.InnerException;

                Perfectus.Server.DataAccess.Logging.LogException
                    (DateTime.UtcNow,
                    "ERRO",
                    int.MinValue,
                    packageId,
                    versionNumber,
                    revisionNumber,
                    instanceId,
                    ex.Message,
                    innerException != null ? ex.InnerException.Message : String.Empty,
                    innerException != null ? ex.InnerException.StackTrace : String.Empty,
                    Assembly.GetCallingAssembly().GetName().Name,
                    Assembly.GetCallingAssembly().CodeBase,
                    Assembly.GetCallingAssembly().GetName().Version.ToString(4),
                    int.MinValue, _jobId, null, null, "Assembly");


                // Now tell our host that we faled
                throw;
            }

// We ignore any problem of removing the temporary directory
            try
            {
                RemoveTempFolder();
            }
            catch { }
        }

        /// <summary>
        /// StartPerPageProcess
        /// </summary>
        private void StartPerPageProcess()
        {
            bool foundForThisServer = false;
            String myServer = Config.Shared.identity.serverName;
            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("StartPerPageProcess started, using server '{0}'", myServer));

            String allSrvInPackages = String.Empty;

            foreach (Perfectus.Common.PackageObjects.Server s in package.Servers)
            {
                if (allSrvInPackages.Length != 0)
                    allSrvInPackages += ", ";
                allSrvInPackages += s.Name;

                if (s.Name == myServer)
                {
                    foundForThisServer = true;
                    ProcessRule(pageToProcess.PageRules2);
                }
            }
            if (!foundForThisServer)
            {
                Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.norules2"), allSrvInPackages);
            }
            else
            {
                // Log the interview to the reporting database
                Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Maybe setting report instance complete for instance {0}", instanceId));
                Report.SetReportingInstanceComplete(instanceId);
            }
        }


        private void StartFinalProcess()
        {
            try
            {
                String myServer = Config.Shared.identity.serverName;
                Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("StartFinalProcess started, using server '{0}'", myServer));

                String allSrvInPackages = String.Empty;

                bool foundForThisServer = false;
                foreach (Perfectus.Common.PackageObjects.Server s in package.Servers)
                {
                    if (allSrvInPackages.Length != 0)
                        allSrvInPackages += ", ";
                    allSrvInPackages += s.Name;

                    if (s.Name == myServer)
                    {
                        foundForThisServer = true;
                        ProcessRule(s.Rule2);
                    }
                }
                if (!foundForThisServer)
                {
                    Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.norules2"), allSrvInPackages);
                }
                else
                {
                    // Log the interview to the reporting database
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Maybe setting report instance complete for instance {0}", instanceId));
                    Report.SetReportingInstanceComplete(instanceId);
                }
            }
            catch 
            {
                throw;
            }
            finally
            {
                Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "Scanning for plugin [InstanceComplete]");

                foreach (PluginDescriptor serverPd in pluginsOnServer)
                    if (serverPd.PluginKind == PluginBehaviour.InstanceComplete)
                    {
                        // Invoke the plugin's Notify() method
                        try
                        {
                            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("plugin {0} found", serverPd.FullName));
                            string statusXml = InstanceManagement.GetInstanceStatusXml(instanceId);
                            IInstanceCompletionNotifier completer =
                                (IInstanceCompletionNotifier)
                                (AppDomain.CurrentDomain.CreateInstanceFromAndUnwrap(serverPd.Path, serverPd.TypeName));
                            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Invoking plugin {0} with xml {1}", serverPd.FriendlyName, statusXml));
                            completer.Notify(instanceId, package, statusXml);
                        }
                        catch (Exception ex)
                        {
                            Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.FailedInstanceCompletePlugin"), serverPd.FriendlyName);
                        }
                    }
            }
        }


        private String CurrentPageName()
        {
            if (IsEndOfPageJob())
                return pageToProcess.Name;
            else
                return "End-of-interview";
        }

        public bool IsEndOfPageJob()
        {
            return pageToProcess != null;
        }


        //---------------------
        private void ProcessRule(Rule2Collection allRules)
        {
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Processing job {0}, process '{1}', with {2} delivery manager", _jobId, CurrentPageName(), allRules.Count));

            // List of deliveries that must be created
            List<Delivery> evaluatedDeliveries = GetEvaluatedDeliveries(allRules);

            if (evaluatedDeliveries.Count == 0)
            {
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("No deliveries evaluated true for job {0}", _jobId));
                return;
            }

            if ( ! CheckRequiredPlugins (evaluatedDeliveries))
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.RequiredPluginNotFound")); 
            }


            // Evaluate which building parts are required.
            // Normally we would look for a document, and if not existing,
            // would compose it. That might involve the creation of the answerset, the
            // merge and the conversion.
            // Just that this could flip the status from CONVERT of DOC1 back
            // to ASSEMBLE for DOC2. We don't want that transition, so we need to know
            // beforehand if the answerset must be created, if templates must be merged 
            // and if documents needs conversion.

            // How to handle errors...
            // I expect that each process step can handle errors (correcting, logging, whatsoever).
            // If that is not possible, then the process will be aborted with the catch handler of that step
            // Consider this: There are 3 deliveries each having 10 documents. Assume only one document fails.
            // Then we know that the owning delivery is incomplete and cannot be distributed.
            // However, the other two can. The error must be rememberred so that the retry mechanism
            // works. But we still want to continue and distribute the other two 'good' deliveries.
            // Solution: The distribution tests, if all required documents are available (as that's 
            // what its all about). If not it will raise the 'error' exception at the end.


            // Set status 'ASSEMBLE' in the database
            _updateProc(JobStatus.ASSEMBLE, null);

            // 1. Maybe create the answerset 
            //    It could already exist in the temp folder, if that is a retry, or no
            //    document needs merging. 
            try
            {
                maybeCreateAnswerset(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.AnswerSetCreationFailed"), ex);
            }

            // 2. Handle mirror packages / mirror converter
            //    This may modify an AnswerSet
            try
            {
                maybeHandleMirrorPackages(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.MirrorSetCreationFailed"), ex);
            }

            // 3. Ensure that all templates are available and merged
            try
            {
                maybeMergeTemplates(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.MergeFailed"), ex);
            }

            // 4. Load attachments
            try
            {

                maybeLoadAttachments(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.LoadAttachmentFailed"), ex);
            }

            // Set status 'CONVERT' in the database
            _updateProc(JobStatus.CONVERT, null);

            // 5. Convert documents
            try
            {
                maybeConvertDocuments(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.ConvertingFailed"), ex);
            }

            // Set status 'DISTRIBUTE_START' in the database
            _updateProc(JobStatus.DISTRIBUTE, null);

            // 6. Distribute everything (Documents, Attachments and XML)
            try
            {
                DistributeDocument(evaluatedDeliveries);
            }
            catch (Exception ex)
            {
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.DistributingFailed"), ex);
            }
        }

        private bool CheckRequiredPlugins(List<Delivery> evaluatedDeliveries)
        {
            foreach (Delivery delivery in evaluatedDeliveries)
            {
                // Find the distributor
                bool found = false;
                Guid requestedPluginId = delivery.Distributor.Descriptor.UniqueIdentifier;
                foreach (PluginDescriptor serverPd in pluginsOnServer)
                    if (serverPd.UniqueIdentifier == requestedPluginId)
                    {
                        found = true;
                        break;
                    }
                if (!found)
                {
                    Log.Error(Log.Category.Engine2, new ID("ProcessCoordinator.FailedToFindDistributer"), 
                        delivery.Distributor.Descriptor.FullName,
                        requestedPluginId.ToString());

                    return false;
                }

                foreach (IDocument doc in delivery.Documents)
                    // Attachments do not require convert
                    if (doc is Document)
                    {
                        // Find all converter
                        found = false;

                        requestedPluginId = ((Document)doc).Converter.Descriptor.UniqueIdentifier;
                        foreach (PluginDescriptor serverPd in pluginsOnServer)
                            if (serverPd.UniqueIdentifier == requestedPluginId)
                            {
                                found = true;
                                break;
                            }
                        if (!found)
                        {
                            Log.Error(Log.Category.Engine2, new ID("ProcessCoordinator.FailedToFindConverter"),
                                ((Document)doc).Converter.Descriptor.FullName,
                                requestedPluginId.ToString());
                            return false;
                        }
                    }

            } 
            return true;
        }
        
        private List<Delivery> GetEvaluatedDeliveries(Rule2Collection allRules)
        {
            List<Delivery> evaluatedDeliveries = null;

            // 1. Lookup if deliveries exist in the database
            //    If this is a retry, then elements that needs processing must be loaded from
            //    the server
            if (DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributionPeek(_jobId) > 0)
            {
                // 1.1. Load the list of (remaining) deliveries into memory
                evaluatedDeliveries = LoadDeliveriesFromQueueTable(allRules);
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Loaded {0} document instructions from database", evaluatedDeliveries.Count));
            }
            else
            {
                // 2. This is a brand new job that was never executed before
                evaluatedDeliveries = new List<Delivery>();

                // 2.1. A job can have many delivery manager. 
                //      Each delivery manager can have many deliveries
                //      We want to evaluate and enqueue each delivery before processing starts
                foreach (Rule2 currentRule in allRules)
                {
                    // Test is the delivery manager shall fire at trhe end of an interview
                    if (!IsEndOfPageJob())
                        if (!currentRule.DoRuleAtFinish.Evaluate(true, AnswerProvider.Caller.System))
                        {
                            Log.Trace(Log.Category.Engine2, Log.Priority.MID, String.Format("Rule '{0}' does not fire at FINISH", currentRule.Name));
                            continue;
                        }

                    // Test is the delivery manager shall fire at all
                    if (!currentRule.DeliveryRule.Evaluate(true, AnswerProvider.Caller.System))
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.MID, String.Format("Rule '{0}' does not fire at '{1}'", currentRule.Name, CurrentPageName()));
                        continue;
                    }
                    // 2.2. Evaluate deliveries
                    // Evaluate the conditions and create the list of all deliveries in memory
                    evaluatedDeliveries.AddRange(EvaluateDeliveries(currentRule.Deliveries));
                }

                // Enqueue deliveries into the database
                EnqueueDeliveries_To_DistributQueue(evaluatedDeliveries);
            }
            return evaluatedDeliveries;
        }

        private List<Delivery> EvaluateDeliveries(List<Delivery> deliveryList)
        {
            List<Delivery> localDeliveryList = new List<Delivery>();

            foreach (Delivery delivery in deliveryList)
            {
                if (!delivery.Valid())
                {
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Delivery {0} is malformed and will be ignored", delivery.Name));
                    continue;
                }

                if (!delivery.Condition.Evaluate(true, AnswerProvider.Caller.System))
                {
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Delivery {0} evaluated FALSE", delivery.Name));
                    continue;
                }
                Delivery myDelivery = new Delivery();
                myDelivery.Name = delivery.Name;
                myDelivery.SetDistributor(delivery.Distributor);

                foreach (IDocument document in delivery.Documents)
                {
                    if (!document.Condition.Evaluate(true, AnswerProvider.Caller.System))
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Document {0}, Delivery {1} evaluated FALSE", document.Name, delivery.Name));
                        continue;
                    }
                    myDelivery.AddDocument(document);
                }
                // maybe there are no documents, then the delivery is meaningless
                if (myDelivery.Documents.Count > 0)
                    localDeliveryList.Add(myDelivery);
            }

            return localDeliveryList;
        }


        private void EnqueueDeliveries_To_DistributQueue(List<Delivery> deliveryList)
        {
            foreach (Delivery delivery in deliveryList)
                foreach (IDocument document in delivery.Documents)
                {
                    DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributionCreate(
                        _jobId,             // jobId
                        document.UniqueIdentifier,  // docu
                        delivery.Distributor.UniqueIdentifier,
                        document.Name,
                        // FB2306. We pass on the Delivery Name (Instead of the Distribution Name) to the distribution queue
                        // because the distribution name represents the distribution type, where as the delivery name represents 
                        // input from the user to describe the delivery. Refer to case.
                        delivery.Name); // distr

                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Enqueue {0} {1} {2}", _jobId, document.Name, delivery.Name));
                }
        }



        /// <summary>
        /// Load existing distributions from the database
        /// Distributions are stored in the database as Document/Distributor tuple
        /// If a job partly failed, then this is the list of things that still must be done.
        /// Therefore there are equal or less Document/Distributor tuple of what the job 
        /// is expected to crate
        /// </summary>
        /// <param name="rule">current delivery manager</param>
        /// <returns>List of deliveries to process</returns>
        private List<Delivery> LoadDeliveriesFromQueueTable(Rule2Collection allRules)
        {
            Guid currentDistributor = Guid.Empty;
            List<Delivery> deliveries = new List<Delivery>();

            // Load all Distributor/Document pairs for the given job using the JobQueueID
            List<Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributorDocumentPair> queueElements = DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributionReadNoneBlocking(_jobId);

            // The order of the result set is arbitrary. But we have to group documents
            // The grouping criteria is the guid of the Distributor (that's NOT the guid of IDistributor implem.)
            queueElements.Sort(new DistributorDocumentCompare());
            Delivery newDelivery = null;

            // For each document in the database distribution queue...
            foreach (Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributorDocumentPair queuePair in queueElements)
                foreach (Rule2 rule in allRules)
                    // ...find the matching delivery manager in the rule
                    foreach (Delivery delivery in rule.Deliveries)
                    {
                        if (delivery.Distributor==null || delivery.Distributor.UniqueIdentifier != queuePair.distributorGuid)
                            continue;

                        // the delivery has exactly one distributor, that's our 'current' one where 
                        // we want to assign the docs to.
                        if (currentDistributor != queuePair.distributorGuid)
                        {
                            // First time? 
                            // Then create a new delivery in memory
                            currentDistributor = queuePair.distributorGuid;
                            newDelivery = new Delivery();
                            newDelivery.Name = delivery.Name;
                            newDelivery.SetDistributor(delivery.Distributor);
                            deliveries.Add(newDelivery);

                            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Created delivery. Delivery={0}, Distributor={1}", delivery.Name, delivery.Distributor.Name));
                        }

                        // Again scan the current rule and find the document that shall be
                        // created.
                        foreach (PackageItem packageItem in delivery.Documents)
                            if (packageItem.UniqueIdentifier == queuePair.documentGuid)
                            {
                                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Added document to delivery. Delivery={0}, Document={1}", delivery.Name, packageItem.Name));
                                newDelivery.AddDocument(packageItem as IDocument);
                            }
                    }
            return deliveries;
        }
    }

    public class DistributorDocumentCompare : IComparer<Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributorDocumentPair>
    {
        #region IComparer<DistributorDocumentPair> Members

        public int Compare(Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributorDocumentPair x, Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributorDocumentPair y)
        {
            return x.distributorGuid.CompareTo(y.distributorGuid);
        }

        #endregion
    }
}
