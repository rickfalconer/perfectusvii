﻿using System;
using System.Collections.Generic;
using Perfectus.Common.PackageObjects;
using System.IO;
using System.IO.Packaging;
using Perfectus.Server.Common.Data;
using Perfectus.Server.AssemblySystem;
using System.Xml;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using Perfectus.Server.ConfigurationSystem;
using System.Web;
using System.Text;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Server.PackageManager;
using System.Web.Caching;
using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {
        private static XNamespace ns = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        private static XNamespace ns_ve = "http://schemas.openxmlformats.org/markup-compatibility/2006";
        private static XNamespace ns_o = "urn:schemas-microsoft-com:office:office";
        private static XNamespace ns_r = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
        private static XNamespace ns_m = "http://schemas.openxmlformats.org/officeDocument/2006/math";
        private static XNamespace ns_v = "urn:schemas-microsoft-com:vml";
        private static XNamespace ns_wp = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
        private static XNamespace ns_w10 = "urn:schemas-microsoft-com:office:word";
        private static XNamespace ns_wne = "http://schemas.microsoft.com/office/word/2006/wordml";
        private static XNamespace ns_a = "http://schemas.openxmlformats.org/drawingml/2006/main";
        private static XNamespace ns_pic = "http://schemas.openxmlformats.org/drawingml/2006/picture";
        private static XNamespace ns_dgm = "http://schemas.openxmlformats.org/drawingml/2006/diagram";
        private static XNamespace ns_ds = "http://schemas.openxmlformats.org/officeDocument/2006/customXml";
        private static XNamespace ns_c = "http://schemas.openxmlformats.org/drawingml/2006/chart";

        private void maybeCreateAnswerset(List<Delivery> evaluatedDeliveries)
        {
            // No need to determine whether these documents are needed, if they already exist
            if (ExistsDocument(answersetGuid) &&
                 ExistsDocument(answersetGuidOriginal))
                return;

            // Maybe the Mirroring requites 
            if (MirroringRequiresAnswerset(evaluatedDeliveries))
            {
                CreateAnswerset();
                return;
            }

            // Maybe a document requires the Answerset
            foreach (Delivery delivery in evaluatedDeliveries)
                foreach (IDocument document in delivery.Documents)
                {
                    if (ExistsDocument(document.UniqueIdentifier))
                        continue;

                    if ((document is Document) && ((document as Document).HasTemplate))
                        if (ExistsDocument((document as Document).Template.UniqueIdentifier))
                            continue;

                    // Note that we need to create an AnswerSet if we have a dynamic attachment,
                    // as we only know from the AnswerSet how many attachments there might be
                    // -> if (document is AttachmentDynamic )

                    // The document does not exist and neither its merged template
                    // So we need to create the AnswertSet.
                    CreateAnswerset();
                    return;
                }
        }

        private void maybeMergeTemplates(List<Delivery> evaluatedDeliveries)
        {
            foreach (Delivery delivery in evaluatedDeliveries)
            {
                foreach (IDocument document in delivery.Documents)
                {
                    if ((document is Document) && ((document as Document).HasTemplate))
                    {
                        Document doc = document as Document;
                        TemplateDocument template = doc.Template;

                        // We got a reference to the template
                        // now need to load it from the package
                        if (template is TemplateDocumentLink)
                            foreach (TemplateDocument t in package.Templates)
                                if (t.UniqueIdentifier == doc.Template.UniqueIdentifier)
                                {
                                    template = t;
                                    break;
                                }

                        if (!(template is WordTemplateDocument2))
                            continue;

                        WordTemplateDocument2 wt = (WordTemplateDocument2)template;

                        if (!ExistsDocument(template.UniqueIdentifier))
                        {
                            //_updateProc(JobStatus.ASSEMBLE, String.Format("Template='{0}'", wt.Name));

                            //wt.OfficeOpenXMLBytes = GetWordTemplateBytes(wt);

                            //// Set up the XmlReaders for the assembler
                            //MemoryStream msTemplate = null;
                            //MemoryStream msAnswers = GetAnswersetDocument(answersetGuid);

                            //msTemplate = new MemoryStream();
                            //msTemplate.Write(wt.OfficeOpenXMLBytes, 0, wt.OfficeOpenXMLBytes.Length);
                            //msTemplate.Seek(0, SeekOrigin.Begin);

                            //msAnswers.Seek(0, SeekOrigin.Begin);
                            //XmlTextReader answersReader = new XmlTextReader(msAnswers);
                            //XmlDocument answers = new XmlDocument();
                            //answers.Load(answersReader);
                            //msAnswers.Seek(0, SeekOrigin.Begin);

                            //using (WordprocessingDocument docPackage = WordprocessingDocument.Open(msTemplate, true))
                            //{
                            //    ProcessOutcomes(answers, docPackage);
                            //    ProcessUploadedImages(answers, docPackage);

                            //    //main document body processing
                            //    docPackage.MainDocumentPart.Document.InnerXml = TransformPart(answers, docPackage.MainDocumentPart.Document.OuterXml);

                            //    //header processing
                            //    foreach (HeaderPart headerPart in docPackage.MainDocumentPart.HeaderParts)
                            //    {
                            //        headerPart.Header.InnerXml = TransformPart(answers, headerPart.Header.OuterXml);
                            //    }

                            //    //footer processing
                            //    foreach (FooterPart footerPart in docPackage.MainDocumentPart.FooterParts)
                            //    {
                            //        footerPart.Footer.InnerXml = TransformPart(answers, footerPart.Footer.OuterXml);
                            //    }
                            //}

                            //StoreDocument(template.UniqueIdentifier, template.Name, msTemplate);

                            _updateProc(JobStatus.ASSEMBLE, String.Format("Template='{0}'", wt.Name));

                            wt.OfficeOpenXMLBytes = GetWordTemplateBytes(wt);

                            Assembler assembler = new Assembler();

                            // Set up the XmlReaders for the assembler
                            MemoryStream msTemplate = null;
                            MemoryStream msOutput = null;
                            MemoryStream msAnswers = GetAnswersetDocument(answersetGuid);

                            msTemplate = new MemoryStream(wt.OfficeOpenXMLBytes);
                            msOutput = new MemoryStream();

                            msAnswers.Seek(0, SeekOrigin.Begin);
                            msAnswers.Position = 0;

                            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, LocalisationSupport.Text(new ID("ProcessCoordinator.StartingAssembly")));
                            assembler.Execute(msTemplate, msAnswers, ref msOutput);
                            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, LocalisationSupport.Text(new ID("ProcessCoordinator.AssemblyComplete")));

                            msOutput.Seek(0, SeekOrigin.Begin);
                            msOutput.Position = 0;

                            XmlTextReader reader = new XmlTextReader(msOutput);
                            XDocument transformedDoc = XDocument.Load(reader);
                            MemoryStream docxOutput = new MemoryStream();
                            wt.FlatToOpc(transformedDoc, ref docxOutput);

                            StoreDocument(template.UniqueIdentifier, template.Name, docxOutput);
                        }
                    }
                }
            }
        }


        //private static void ProcessUploadedImages(XmlDocument answers, WordprocessingDocument docPackage)
        //{
        //    //check for images in the answerset, if found we need to place them in the media folder and also place the applicable content control in the right location in the document
        //    XPathNavigator imageXpn = answers.CreateNavigator();
        //    XPathExpression imageXpe;
        //    imageXpe = imageXpn.Compile("*/answer[@image='true']");
        //    XPathNodeIterator iterator = imageXpn.Select(imageXpe);

        //    while (iterator.MoveNext())
        //    {
        //        //get the template using OpenXmlSDK and add the images
        //        XPathNavigator n = iterator.Current;

        //        //Add an imagepart with data to the package for every image found
        //        ImagePart imagePart = docPackage.MainDocumentPart.AddImagePart(ImagePartType.Jpeg);

        //        byte[] imageBytes = Convert.FromBase64String(n.InnerXml);
        //        imagePart.FeedData(new MemoryStream(imageBytes));
        //        string id = docPackage.MainDocumentPart.GetIdOfPart(imagePart);
        //        n.CreateAttribute(null, "relationshipId", null, id);
        //    }
        //}

        //private void ProcessOutcomes(XmlDocument answers, WordprocessingDocument docPackage)
        //{
        //    //outcome image handling, needs to be done before merge so that new and old are not mixed
        //    //look at outcomes and grab styles/themes/pics etc
        //    foreach (Outcome o in package.Outcomes)
        //    {
        //        try
        //        {
        //            string wml;
        //            ActionBase ab = o.Definition.Evaluate(AnswerProvider.Caller.Assembly);

        //            byte[] outcomeActionBytes;
        //            OutcomeAction oa = (OutcomeAction)ab;
        //            //either get from cache or get from db and put in cache
        //            outcomeActionBytes = InstanceToAnswerSetXml.GetOutcomeBytes(package, versionNumber, revisionNumber, o, (OutcomeAction)ab);

        //            // Turn the bytes back into an OutcomeAction
        //            using (MemoryStream msOa = new MemoryStream())
        //            {
        //                BinaryFormatter bf = new BinaryFormatter();
        //                msOa.Write(outcomeActionBytes, 0, outcomeActionBytes.Length);
        //                msOa.Seek(0, SeekOrigin.Begin);
        //                object deserialised = bf.Deserialize(msOa);
        //                oa = (OutcomeAction)deserialised;
        //            }

        //            byte[] outcomeBytes = Encoding.UTF8.GetBytes(oa.OfficeOpenXML);
        //            MemoryStream outcomeStream = new MemoryStream(outcomeBytes);

        //            //List<string[]> imageRefs = new List<string[]>(); //so that we update the image id's from all outcomes into the merged doc
        //            Dictionary<string, string> imageRefs = new Dictionary<string, string>();
        //            using (WordprocessingDocument outcomePackage = WordprocessingDocument.Open(outcomeStream, false))
        //            {
        //                IEnumerable<XElement> contents = AgentMergeDocumentReferences.GetXDocument(outcomePackage.MainDocumentPart).Descendants(ns + "body").Elements();
        //                List<ImageData> images = new List<ImageData>();
        //                AgentMergeDocumentReferences.CopyReferences(outcomePackage, docPackage, contents, images, ref imageRefs);
        //            }

        //            //update the outcomes in the answerset so that they have the new id's.
        //            XmlNamespaceManager nsm = new XmlNamespaceManager(new NameTable());
        //            nsm.AddNamespace("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
        //            nsm.AddNamespace("a", "http://schemas.openxmlformats.org/drawingml/2006/main");
        //            nsm.AddNamespace("pic", "http://schemas.openxmlformats.org/drawingml/2006/picture");

        //            string answerXPathExpression = string.Format("//answer[@answeredItemId='{0}']", o.UniqueIdentifier.ToString());
        //            XmlElement outcomeAnswer = (XmlElement)answers.SelectSingleNode(answerXPathExpression);

        //            XPathNavigator outcomeImageXpn = outcomeAnswer.CreateNavigator();
        //            XPathExpression outcomeImageXpe;
        //            outcomeImageXpe = outcomeImageXpn.Compile("*//pic:blipFill/a:blip[@r:embed]");
        //            outcomeImageXpe.SetContext(nsm);
        //            XPathNodeIterator outcomeImageIterator = outcomeImageXpn.Select(outcomeImageXpe);

        //            while (outcomeImageIterator.MoveNext())
        //            {
        //                XPathNavigator n = outcomeImageIterator.Current;
        //                XPathNavigator a = n.SelectSingleNode("@r:embed", nsm);
        //                a.SetValue(imageRefs[a.Value]);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            //TODO: copy this stuff from instanceToAnswerSetXml.cs // logging
        //        }
        //    }
        //}

        //        private static string TransformPart(XmlDocument answers, string partOuterXml)
        //        {
        //            Assembler assemblerDocument = new Assembler();
        //            MemoryStream msDocumentOutput = new MemoryStream();
        //            MemoryStream documentMlMs = null;
        //            XmlTextReader answersReader;

        //            MemoryStream alteredAnswers = new MemoryStream();
        //            answers.Save(alteredAnswers);
        //            alteredAnswers.Seek(0, SeekOrigin.Begin);

        //            byte[] xmlByteArray = Encoding.ASCII.GetBytes(partOuterXml);
        //            documentMlMs = new MemoryStream(xmlByteArray);

        //            alteredAnswers.Seek(0, SeekOrigin.Begin);
        //            answersReader = new XmlTextReader(alteredAnswers);

        //            documentMlMs.Seek(0, SeekOrigin.Begin);
        //            msDocumentOutput = new MemoryStream();

        //            XmlTextReader templateReader = new XmlTextReader(documentMlMs);
        //            StreamWriter outputWriter = new StreamWriter(msDocumentOutput, Encoding.UTF8);

        //            //RDF trace it
        //#if Debug                            
        //            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "part template " + System.Text.Encoding.Default.GetString(documentMlMs.ToArray()));
        //            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "part answers " + System.Text.Encoding.Default.GetString(alteredAnswers.ToArray()));
        //#endif
        //            assemblerDocument.Template = templateReader;
        //            assemblerDocument.AnswerSet = answersReader;
        //            assemblerDocument.Output = outputWriter;

        //            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, LocalisationSupport.Text(new ID("ProcessCoordinator.StartingAssembly")));
        //            assemblerDocument.Execute();
        //            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, LocalisationSupport.Text(new ID("ProcessCoordinator.AssemblyComplete")));

        //            //RDF trace it
        //#if Debug                            
        //            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "part output " + System.Text.Encoding.Default.GetString(msDocumentOutput.ToArray()));
        //#endif
        //            msDocumentOutput.Seek(0, SeekOrigin.Begin);
        //            using (StreamReader reader = new StreamReader(msDocumentOutput))
        //            {
        //                string trasformedXml = reader.ReadToEnd();

        //                if (!string.IsNullOrEmpty(trasformedXml))
        //                {
        //                    XmlDocument xmldoc = new XmlDocument();
        //                    xmldoc.LoadXml(trasformedXml);
        //                    xmldoc.RemoveChild(xmldoc.FirstChild);
        //                    return xmldoc.FirstChild.InnerXml;
        //                }
        //            }

        //            return null;
        //        }

        private MemoryStream GetAnswersetDocument(Guid guid)
        {
            if (guid != this.answersetGuid && guid != this.answersetGuidOriginal)
                throw new Exception("Expect AnswerSet guid");

            MemoryStream returnStream = GetCopyOfDocument(guid);
            if (null != returnStream)
                return returnStream;
            CreateAnswerset();
            return GetCopyOfDocument(guid);
        }

        private void CreateAnswerset()
        {
            // Maybe the answerset already exists
            if (!ExistsDocument(answersetGuid) ||
                !ExistsDocument(answersetGuidOriginal))
            {
                // Create teh answerset
                MemoryStream msAnswers;
                MemoryStream msAnswersOriginal;
                // used to hold an original instance of an answerSet, which some converters may need.
                msAnswers =
                    InstanceToAnswerSetXml.GetAnswerSetXml(package, instanceId, versionNumber, revisionNumber,
                                                           AnswerProvider.Caller.Assembly, false);
                msAnswersOriginal = new MemoryStream(msAnswers.ToArray());
                msAnswers.Seek(0, SeekOrigin.Begin);

                // cache it (filenames are predefined...)
                StoreDocument(answersetGuid, answersetName, msAnswers);
                StoreDocument(answersetGuidOriginal, answersetNameOriginal, msAnswersOriginal);
            }
        }

        //private void EnsureDocumentExist(List<Delivery> evaluatedDeliveries)
        //{
        //    foreach (Delivery delivery in evaluatedDeliveries)
        //        foreach (IDocument doc in delivery.Documents)
        //        {
        //            // Nothing to do if the document already exists.
        //            if (ExistsDocument(doc.UniqueIdentifier))
        //                continue;

        //            // Documents need merging (Attachments do not)
        //            // Don't consider AnswerSetXml converter
        //            if ((doc is Document) && ((doc as Document).HasTemplate))
        //            {
        //                MergeDocument(doc as Document);

        //                // Converting should be possible yet
        //                // TODO convert attachments
        //                maybeConvertDocument(doc as Document);
        //            }
        //            else if (doc is IAttachment)
        //            {
        //                if (doc is AttachmentStatic)
        //                {
        //                    AttachmentBase attachment = doc as AttachmentBase;
        //                    byte[] b = DataAccess.PackageManager.Retrieval.GetAttachmentBytes(
        //                        packageId,
        //                        versionNumber,
        //                        revisionNumber,
        //                        attachment.OriginalUniqueueIdentifier != Guid.Empty ?
        //                        attachment.OriginalUniqueueIdentifier :
        //                        attachment.UniqueIdentifier);
        //                    StoreDocument(attachment.UniqueIdentifier, attachment.Name, new MemoryStream(b));
        //                }
        //                else if (doc is AttachmentDynamic)
        //                {
        //                    // ToDo
        //                }

        //                else if (doc is AttachmentIntegration)
        //                {
        //                    // ToDo
        //                }
        //            }
        //        }
        //}

        private byte[] GetWordTemplateBytes(WordTemplateDocument2 t)
        {
            try
            {
                int cacheTtl = Config.Shared.assembly.templateCacheTtl;

                string cacheKey = string.Format(cacheKeyFormat, t.UniqueIdentifier, versionNumber, revisionNumber);
                if (cacheTtl == 0 || (cacheTtl > 0 && HttpRuntime.Cache[cacheKey] == null))
                {
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW,
                        string.Format(
                            LocalisationSupport.Text(new ID("ProcessCoordinator.RetrievingTemplate")), t.Name, t.UniqueIdentifier));
                    byte[] templateBytes =
                        Retrieval.GetTemplateBytes(package.UniqueIdentifier, versionNumber, revisionNumber,
                                                   t.UniqueIdentifier);

                    if (cacheTtl > 0)
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW,
                            string.Format(
                                    LocalisationSupport.Text(new ID("ProcessCoordinator.AddingTemplate")), cacheTtl));
                        HttpRuntime.Cache.Add(cacheKey, templateBytes, null, Cache.NoAbsoluteExpiration,
                                              new TimeSpan(0, 0, 0, cacheTtl, 0), CacheItemPriority.Normal, null);
                    }
                    return templateBytes;
                }
                else
                {
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW,
                        string.Format(
                            LocalisationSupport.Text(new ID("ProcessCoordinator.RetrievingFromCache")), cacheKey));
                    byte[] templateBytes = (byte[])HttpRuntime.Cache[cacheKey];
                    return templateBytes;
                }
            }
            catch (Exception ex)
            {
                Log.Error(Log.Category.Engine2,
                    ex, new ID("ProcessCoordinator.templateError"));
                return null;
            }
        }
    }
}
