using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using Perfectus.Library.Exception;

namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    public partial class Agent
    {
        // The filename is evaluated only once. If we cache an object and rerun later
        // (e.g. we failed and retry) we still need the already evaluated filename.
        // Hence stream and filename come together
        
        internal class Key : IComparable<Key>, IComparable {
            internal Key (Guid k1, Guid k2)
            {
                key1=k1; 
                key2=k2;
            }
            internal Guid key1;
            internal Guid key2;
            public override string ToString()
            {
                return String.Format("{0}_{1}", key1, key2);
            }

            #region IComparable<Key> Members

            public int CompareTo(Key other)
            {
                int firstKey = key1.CompareTo(other.key1);
                int secondKey = key2.CompareTo(other.key2);
                if (firstKey == 0)
                    return secondKey;
                return firstKey;
            }

            #endregion

            #region IComparable Members

            public int CompareTo(object obj)
            {
                return CompareTo(obj as Key);
            }

            #endregion
        }
        internal class Data {
            internal Data(Key k, String fname)
            {
                key = k;
                filename = fname;
            }
            internal Key key;
            internal String filename;
        }


        private string _tmpDir;
        private Dictionary<String, Stream> cache = new Dictionary<String, Stream>();
        private Dictionary<Guid, Data> index = new Dictionary<Guid, Data>();

        private Guid answersetGuid = new Guid("BE2DE0EE-6C86-40bf-ADEC-8D9A1D27CBCD");
        private Guid answersetGuidOriginal = new Guid("622C5EE6-BC6C-4fd5-AA65-D78692848298");
        private const string answersetName = "AnswerSet.xml";
        private const string answersetNameOriginal = "AnswerSetOriginal.xml";

        #region temporary folder

        private void CreateTempFolder()
        {
            if (_jobId >= 0)
                CreateTempFolder(_jobId);
        }

        private string CreateTempFolder(int jobId)
        {
            DirectoryInfo tmpDirectory = new DirectoryInfo(Config.Shared.coordinator.tempDirectory);
            if (!tmpDirectory.Exists)
                tmpDirectory.Create();

            _tmpDir = tmpDirectory.CreateSubdirectory(jobId.ToString()).FullName;
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Created temporary folder {0}", _tmpDir));

            return _tmpDir;
        }

        private void RemoveTempFolder()
        {
            if (_jobId >= 0)
                RemoveTempFolder(_jobId);
        }

        private void RemoveFilesAndFolder(String folder) { 

            DirectoryInfo tmpDirectory = new DirectoryInfo(folder);

            foreach (FileInfo file in tmpDirectory.GetFiles())
                file.Delete();

            foreach (DirectoryInfo directory in tmpDirectory.GetDirectories())
            {
                RemoveFilesAndFolder(directory.FullName);
            }

            tmpDirectory.Delete();
        }

        private void RemoveTempFolder(int jobId)
        {
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("About to remove temporary folder {0}", _tmpDir));

            if (_tmpDir == null || _tmpDir.Length == 0)
                return;

            RemoveFilesAndFolder(_tmpDir);

            _tmpDir = String.Empty;
        }


        #endregion

        #region cache - file management
        
        private void StoreDocument(Guid documentGuid, Guid pluginGuid, Guid templateGuid, string newFileName, MemoryStream ms)
        {
            Key k = new Key(pluginGuid, templateGuid);
            if (!cache.ContainsKey (k.ToString()))
            {
                if (ms == null)
                    throw new Exception("bug");
                StoreDocumentInternal(k.ToString(), ms);
            }
            index.Add ( documentGuid, new Data(k,newFileName));
        }

        // Used by mirror answerset
        private void ReplaceDocument(Guid documentGuid, MemoryStream ms)
        {
            Key k = new Key(documentGuid, Guid.Empty);
            StoreDocumentInternal(k.ToString(), ms);
            if (!index.ContainsKey(documentGuid))
                index.Add(documentGuid, new Data(k, String.Empty));
        }

        private void StoreDocumentInternal(String key, Stream stream)
        {
            if (cache.ContainsKey(key))
                cache.Remove(key);

            if (stream.Length / 1024 > Config.Shared.coordinator.maxcachedocumentsize)
            {
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Caching content with key {0} as file", key.ToString()));
            
                WriteStreamToDisk(key.ToString(), stream);
                cache.Add(key, null);
            }
            else
            {
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Caching content with key {0} in memory", key.ToString()));
                cache.Add(key, stream);
            }
        }

        private void StoreDocument(Guid guid, String proposedFilename, MemoryStream stream)
        {
            StoreDocument(guid, guid, Guid.Empty, proposedFilename, stream);
        }

        private MemoryStream GetCopyOfDocument(Guid guid)
        {
            String dummy;
            return GetCopyOfDocument(guid, out dummy);
        }

        private MemoryStream GetCopyOfDocument(Guid guid, out String proposedFilename)
        {
            return new MemoryStream(GetDocument(guid,out proposedFilename).ToArray() );
        }

        private MemoryStream GetDocument(Guid guid)
        {
            String dummy;
            return GetDocument(guid, out dummy);
        }

        private MemoryStream GetDocument(Guid guid, out String proposedFilename)
        {
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Requesting content from cache '{0}'", guid));

            proposedFilename = string.Empty;
            MemoryStream stream = null;

            if (index.ContainsKey(guid))
            {
                Data dta = index[guid];

                if (cache.ContainsKey(dta.key.ToString()))
                {
                    proposedFilename = dta.filename;
                    return GetContent(dta.key);
                }
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Requested content from cache '{0}' not found", guid));

                index.Remove(guid);
                return null;
            }
            else
            {
                // Must be on disk only
                Data dta = indexFile(guid);
                if (dta == null)
                    return null;

                if (cache.ContainsKey(dta.key.ToString()))
                {
                    stream = GetContent(dta.key);
                    proposedFilename = dta.filename;
                    if (stream == null)
                        cache.Remove(dta.key.ToString());
                        return stream;
                }

                stream = LoadFromDisk(dta.key);
                proposedFilename = dta.filename;

                if (stream.Length / 1024 <= Config.Shared.coordinator.maxcachedocumentsize)
                    cache.Add(dta.key.ToString(), stream);
                else
                    cache.Add(dta.key.ToString(), null);
                index.Add(guid, dta);
            }
            return stream;
        }

        private MemoryStream GetContent(Key key)
        {
            MemoryStream stream = cache[key.ToString()] as MemoryStream;
            if (stream == null)
                stream = LoadFromDisk(key);
            return stream;
        }

        /*
        private bool ExistsDocumentContent(Guid pluginGuid, Guid templateGuid)
        {
            Key k = new Key(pluginGuid, templateGuid);

            if (! cache.ContainsKey(k.ToString()))
            {
                if (File.Exists(GetFilename(k.ToString())))
                {
                    cache.Add(k.ToString(), null);
                    return ExistsDocumentContent(pluginGuid, templateGuid);
                }
            }
            else
            {
                if (cache[k.ToString()] == null)
                {
                    if (!File.Exists(GetFilename(k.ToString())))
                    {
                        cache.Remove(k.ToString());
                        return ExistsDocumentContent(pluginGuid, templateGuid);
                    }
                }
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Exists content in cache for template '{0}' converter '{1}' TRUE", pluginGuid, templateGuid));

                return true;
            }
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Exists content in cache for template '{0}' converter '{1}' FALSE", pluginGuid, templateGuid));
            return false;
        }
        */
        private bool ExistsDocument(Guid guid)
        {
            bool success = true;
            if (index.ContainsKey(guid))
            {
                Data dta = index[guid];

                if (cache.ContainsKey(dta.key.ToString()))
                {
                    if (cache[dta.key.ToString()] == null)
                        return File.Exists(GetFilename(dta.key.ToString()));
                    return true;
                }
                index.Remove(guid);
                success = false;
            }
            else
            {
                Data dta = indexFile(guid);
                if (dta == null)
                    return false;

                if (!cache.ContainsKey(dta.key.ToString()))
                    success = File.Exists(GetFilename(dta.key.ToString()));
            }
            Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, string.Format("Exists document in cache '{0}' {1}", success.ToString(), success));
            return success;
        }

        private Data indexFile(Guid guid)
        {
            try
            {
                String[] lines = File.ReadAllLines(GetFilename("index.dat"), Encoding.UTF8);

                foreach (String line in lines)
                {
                    String[] parts = line.Split('\t');

                    if (parts[0].StartsWith(guid.ToString(), true, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        String[] keyStr = parts[1].Split('_');
                        return new Data(new Key(new Guid(keyStr[0]), new Guid(keyStr[1])), parts[2]);
                    }
                }
            }
            catch { }
            return null;
        }
    

        private String GetFilename( String proposedFilename)
        {
            return String.Format("{0}\\{1}", _tmpDir, proposedFilename);
        }

        private void WriteStreamToDisk(String proposedFilename, Stream streamToWrite)
        {
            String filename = GetFilename(proposedFilename);
            streamToWrite.Position = 0;
            byte[] bytes = new byte[streamToWrite.Length];
            streamToWrite.Read(bytes, 0, bytes.Length);

            using (FileStream fs = File.OpenWrite(filename))
                fs.Write(bytes, 0, bytes.Length);
        }

        private void WriteAllCacheToDiskAndClear()
        {
            foreach (String key in cache.Keys)
                WriteCacheToDisk(key);
            
            foreach (Guid guid in index.Keys)
                WriteIndexToDisk(guid);

            cache.Clear();
            index.Clear();
        }

        private void WriteCacheToDisk(String cachedKey)
        {
            if (cache.ContainsKey(cachedKey) &&
                cache[cachedKey] != null)
                WriteStreamToDisk(cachedKey, cache[cachedKey]);
        }

        private void WriteIndexToDisk(Guid cachedGuid)
        {
            // does it already exist?
            if (indexFile(cachedGuid) != null)
                return;
        
            Data dta = index[cachedGuid];

            String content = String.Format("{0}\t{1}\t{2}{3}",
                cachedGuid.ToString(),
                dta.key.ToString(),
                dta.filename,
                System.Environment.NewLine);

            File.AppendAllText(GetFilename("index.dat"), content, Encoding.UTF8);
        }


        private MemoryStream LoadFromDisk(Key key)
        {
            String[] filenames = Directory.GetFiles(_tmpDir, String.Format("{0}", key.ToString()), SearchOption.TopDirectoryOnly);

            if (filenames.Length == 1)
                return new MemoryStream(File.ReadAllBytes(filenames[0]));
            return null;
        }
         
        #endregion

    }
}
