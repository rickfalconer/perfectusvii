using PluginBehaviour=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginBehaviour;
using PluginDescriptor=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginDescriptor;
using PluginDescriptorCollection=Perfectus.Server.ConfigurationSystem.PluginSystem.PluginDescriptorCollection;
using Plugins=Perfectus.Server.ConfigurationSystem.Plugins;
using Perfectus.Common.PackageObjects;
using System;
using System.Collections.Generic;
using Perfectus.Server.PluginKit;
using Perfectus.Server.Common.Data;
using System.IO;
using Perfectus.Library.Localisation;
using System.Reflection;
using System.ComponentModel;
using Perfectus.Library.Logging;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Server.InterviewSystem;
using System.Text;
using Perfectus.Library.Exception;


namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {

        private bool TestDelivery(Delivery delivery, out String message)
        {
            message = String.Empty;
            StringBuilder bld = new StringBuilder();

            foreach (IDocument doc in delivery.Documents)
            {
                // There might be zero, one or many attachments, depnding on what
                // the user provided during the interview.
                if (doc is AttachmentDynamic)
                    continue;

                // However, a document or static attchment MUST exist
                if (null == GetDocument(doc.UniqueIdentifier))
                {
                    if (bld.Length > 0)
                        bld.Append(", ");
                    bld.Append(doc.Name);
                }
            }

            if (bld.Length > 0)
            {
                message = bld.ToString();
                return false;
            }
            return true;
        }


        private void DistributeDocument(List<Delivery> deliveries)
        {
            // Store the very first error.
            // Subsequent errors are not thrown back
            Exception pe = null;

            foreach (Delivery delivery in deliveries)
            {
                String message;
                if (TestDelivery(delivery, out message))
                {
                    _updateProc(JobStatus.DISTRIBUTE, String.Format("Distributor='{0}', '{1}'", delivery.Distributor.Name, delivery.Distributor.Descriptor.UniqueIdentifier));

                    IDistributor distributorInstance = null;
                    /////////////////
                    // 1. Load the distributor plugin and initialize it
                    try
                    {
                        distributorInstance = LoadDistributorForDelivery(delivery);
                    }
                    catch (Exception ex)
                    {
                        if (pe == null)
                            pe = ex;

                        // Continue with the next delivery, as this one failed
                        continue;
                    }

                    /////////////////
                    // 2.  Append all documents to the one distributor - no try/catch
                    
                    foreach (IDocument document in delivery.Documents)
                    {
                        String filename;
                        // repeater used?
                        int questionSequenceInRepeater = 0;

                        do
                        {
                            // Dynamic questions can refer to zero, one or many attachments...
                            Guid idWeAreLookingFor = CreateFakeGuid(document.UniqueIdentifier, questionSequenceInRepeater);
                            if (document is AttachmentDynamic)
                            {
                                if (!ExistsDocument(idWeAreLookingFor))
                                    break;
                                questionSequenceInRepeater++;
                            }

                            StreamReader sr = new StreamReader(GetDocument(idWeAreLookingFor, out filename));

                            String templateName = String.Empty;
                            if (document is Document)
                            {
                                if (((Document)document).HasTemplate)
                                    templateName = ((Document)document).Template.Name;
                                else
                                    templateName = ((Document)document).Converter.Name;
                            }
                            else
                                templateName = "Attachment";

                            distributorInstance.AppendStreamReader(
                                sr,
                                filename,
                                idWeAreLookingFor,
                                templateName);
                            distributorInstance.PackageInstanceId = instanceId;
                            distributorInstance.UserId = userId;
                        
                        } while (questionSequenceInRepeater > 0);
                    }

                    /////////////////
                    // 3. Distribute
                    DistributionResult[] results = null;
                    List<DistributionResult2> results2 = null;
                    try
                    {
                        results = distributorInstance.Distribute();
                        results2 = new List<DistributionResult2>(results.Length);
                    }
                    catch (Exception ex)
                    {
                        // That exception is completly arbitrary - so don't modify anything
                        if (pe == null)
                            pe = ex;
                    }

                    /////////////////
                    // 4. Fill the distribution results
                    try
                    {
                        // Enhance the distribution result returned by the distributor
                        if (results != null)
                        {
                            foreach (DistributionResult result in results)
                            {
                                //pf-3113 similar to above arbitary exception, e.g. metadata container won't have results, ignore it
                                if (result != null)
                                {
                                    DistributionResult2 result2 = new DistributionResult2(result);

                                    // FB2306. We pass on the Delivery Name (Instead of the Distribution Name) to the distribution results
                                    // because the distribution name represents the distribution type, where as the delivery name represents 
                                    // input from the user to describe the delivery. Refer to case.
                                    result2.DistributorName = delivery.Name;

                                    result2.JobQueueID = _jobId;
                                    if (pageToProcess != null)
                                    {
                                        result2.PageId = pageToProcess.UniqueIdentifier;
                                        result2.PageName = pageToProcess.Name;
                                    }

                                    results2.Add(result2);
                                }
                            }

                            LogDistributionResults(instanceId, results2.ToArray());

                            DataAccess.DatabaseJobQueue.DatabaseJobQueue.DistributionRemove(
                                _jobId,
                                delivery.Distributor.UniqueIdentifier);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Want to keep original exception, probably from the database layer
                        if (pe == null)
                            pe = ex;
                    }
                    distributorInstance = null;
                }
                else if (pe == null)
                {
                    pe = new Exception(new ID("ProcessCoordinator.DistributeFailedAsDeliveryIncomplete").ToIdString(delivery.Name, message));
                }
            }

            if (pe != null)
                throw pe;
        }

        private IDistributor LoadDistributorForDelivery(Delivery delivery)
        {
            IDistributor distributorInstance = null;

            //////////////////////
            // Find the path of the plugin
            Guid requestedPluginId = delivery.Distributor.Descriptor.UniqueIdentifier;
            string pathToPlugin = null;
            string serverPluginTypeName = null;

            foreach (PluginDescriptor serverPd in pluginsOnServer)
            {
                if (serverPd.UniqueIdentifier == requestedPluginId)
                {
                    pathToPlugin = serverPd.Path;
                    serverPluginTypeName = serverPd.TypeName;
                    break;
                }
            }

            Log.Trace(Log.Category.Engine2, Log.Priority.LOW,
                string.Format(
                    LocalisationSupport.Text(new ID("ProcessCoordinator.PathToPlugin")),
                    pathToPlugin));

            if (pathToPlugin == null)
                throw new Exception(new ID("ProcessCoordinator.DistributorNotFoundError").ToIdString(delivery.Distributor.Descriptor.FriendlyName));

            //////////////////////
            // load into memory
            try
            {
                distributorInstance = (IDistributor)(AppDomain.CurrentDomain.CreateInstanceFromAndUnwrap(pathToPlugin, serverPluginTypeName));
                distributorInstance.InstanceMetaData = stringBindingMetaData;
            }
            catch (Exception ex)
            {
                throw new Exception(new ID("ProcessCoordinator.DistributorFailedToLoadError").ToIdString(pathToPlugin), ex);
            }

            //////////////////////
            // Put in the properties...

            try
            {
                foreach (PluginPropertyDescriptor pd in delivery.Distributor.Descriptor.PropertyDescriptors)
                {
                    object propertyValue = delivery.Distributor[pd.Name];
                    foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(distributorInstance))
                        if (prop.Name == pd.Name)
                        {
                            if (propertyValue is TextQuestionQueryValue)
                            {
                                TextQuestionQueryValue tv = (TextQuestionQueryValue)propertyValue;
                                if (tv.QuestionValue == null)
                                    Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Setting property [{0}] to '{1}'", pd.Name, tv == null ? "null" : tv.ToString()));
                                else
                                    Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Setting property [{0}] to '{1}'", pd.Name, tv == null ? "null" : tv.QuestionValue.ToString()));
                            }
                            else
                                Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Setting property [{0}] to '{1}'", pd.Name, propertyValue == null ? "null" : propertyValue.ToString()));

                            prop.SetValue(distributorInstance, propertyValue == null ? null : propertyValue);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                throw new Exception
                (new ID("ProcessCoordinator.DistributorInitializeError").ToIdString(delivery.Distributor.Descriptor.FriendlyName));
            }

            return distributorInstance;
        }



        private static void LogDistributionResults(Guid instanceId, DistributionResult2[] results)
        {
            foreach (DistributionResult distributionResult in results)
            {
                Status.SetDistributionResult(instanceId, distributionResult);

                if (Log.ShallTrace(Log.Category.Engine2, Log.Priority.MID))
                    Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("distributor {0} result for {1}, {2}, {3}, {4}",
                        distributionResult.DistributorId.ToString(),
                        distributionResult.InternalReference,
                        distributionResult.TemplateId.ToString(),
                        distributionResult.TemplateName,
                        distributionResult.LinkText));
            }
        }

    }
}
