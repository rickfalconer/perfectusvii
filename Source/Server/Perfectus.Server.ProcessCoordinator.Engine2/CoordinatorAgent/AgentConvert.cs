using System;
using System.Collections.Generic;
using Perfectus.Common.PackageObjects;
using Perfectus.Library.Logging;
using Perfectus.Server.DataAccess.InterviewSystem;
using Perfectus.Server.PluginKit;
using System.Reflection;
using System.IO;
using Perfectus.Library.Localisation;
using Perfectus.Server.Common.Data;
using PluginDescriptor = Perfectus.Server.ConfigurationSystem.PluginSystem.PluginDescriptor;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Library.Exception;


namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {
        private void maybeConvertDocuments(List<Delivery> evaluatedDeliveries)
        {
            foreach (Delivery delivery in evaluatedDeliveries)
            {
                // Scope for failure is the delivery, not the document.
                // If one fails, then process shall continue with the remaining deliveries.
                // However, the failed one must be ignored later, as it is assumed that ALL 
                // documents must be available at distribution time
                foreach (IDocument document in delivery.Documents)
                    if (document is Document)
                        if (!ExistsDocument(document.UniqueIdentifier))
                            try
                            {
                                maybeConvertDocument(document as Document);
                            }
                            catch (Exception ex)
                            {
                                // We do want to log the error, but we don't want to stop
                                if (!(ex is PerfectusException))
                                    new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.ConvertOneDocumentFailed"), ex, document.Name);

                                // continue with the next delivery
                                break;
                            }
            }
        }

        private void maybeConvertDocument(Document doc)
        {
            String templateName = String.Empty;

            try
            {
                templateName = doc.Name;

                if (templateName.Length == 0)
                    if (doc.HasTemplate)
                        templateName = doc.Template.Name;
                    else
                        templateName = doc.Converter.Descriptor.PreferredDoc;

                Converter converter = doc.Converter;

                if (converter.Descriptor.PreferredDoc == "WordML")
                {
                    // If Document.Filename is not set, 
                    // then it will evaluate Template.PublishedFileName
                    if (doc.FileName != null)
                    {

                        string metaDataPackageReference = InstanceManagement.GetInstanceReference(instanceId);
                        StringBindingMetaData sbmd = new StringBindingMetaData(package.Name, metaDataPackageReference, instanceId, versionNumber, revisionNumber);

                        try
                        {
                            string eval = doc.FileName.Evaluate(AnswerProvider.Caller.Assembly, sbmd);
                            if (eval != null && eval.Trim().Length > 0)
                                templateName = eval;
                        }
                        catch
                        {
                            Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Failed to evaluate filename: {0}", doc.Name));
                        }
                    }
                }

                Guid requestedPluginId = converter.Descriptor.UniqueIdentifier;

                if (doc.HasTemplate)
                {
                    _updateProc(JobStatus.CONVERT, String.Format("Document='{0}', Converter='{1}', Template='{2}'", doc.Name, converter.Name, doc.Template.Name));
                }
                else
                {
                    _updateProc(JobStatus.CONVERT, String.Format("Document='{0}', Converter='{1}'", doc.Name, converter.Name));
                }

                Log.Trace(Log.Category.Engine2, Log.Priority.LOW,
                     string.Format(
                         LocalisationSupport.Text(new ID("ProcessCoordinator.LoadingPlugin")) + " from [{1}]", converter.Descriptor.FullName, converter.Descriptor.Path));
                string pathToPlugin = null;
                foreach (PluginDescriptor serverPd in pluginsOnServer)
                {
                    if (serverPd.UniqueIdentifier == requestedPluginId)
                    {
                        pathToPlugin = serverPd.Path;
                        break;
                    }
                }

                if (pathToPlugin != null)
                {
                    IConverter cInstance =
                        (IConverter)
                        (AppDomain.CurrentDomain.CreateInstanceFromAndUnwrap(pathToPlugin, converter.Descriptor.TypeName));

                    cInstance.InstanceMetaData = stringBindingMetaData;

                    Type t = cInstance.GetType();

                    foreach (PluginPropertyDescriptor ppd in converter.Descriptor.PropertyDescriptors)
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.MID,
                            string.Format("Method {0}, value {1}", ppd.Name, converter[ppd.Name]));
                        t.InvokeMember(ppd.Name, BindingFlags.SetProperty, null, cInstance, new object[] { converter[ppd.Name] });
                    }

                    MemoryStream ms;
                    string newFileName;

                    if (converter.Descriptor.PreferredDoc == "AnswerSetXml")
                    {
                        String distributedFilename = default(String);
                        if (doc.FileName != null)
                            distributedFilename = doc.FileName.Evaluate(AnswerProvider.Caller.Assembly, sbmd);
                        
                        // If mirrorConverter then send the original instead of msAnswers (msAnswers has had primaryInclude = 'false' answers removed).
                        // TODO: Add more info to the converter instead of looking at its UId. For example add a property called ExpectsOriginalAnswerSet to the converter interface.
                        if (converter.Descriptor.UniqueIdentifier.ToString() == "3542e8d6-f18e-442e-8683-8c87cdf0a666")
                        {
                            ms =
    (MemoryStream)cInstance.ConvertMemoryStream(
    new StreamReader(GetAnswersetDocument(answersetGuidOriginal)), distributedFilename, out newFileName);

                        }
                        else
                        {
                            ms =
    (MemoryStream)cInstance.ConvertMemoryStream(
    new StreamReader(GetAnswersetDocument(answersetGuid)), distributedFilename, out newFileName);
                        }

                        // In case a converter doesn't set a 'default' filename 
                        if (newFileName == null || newFileName.Trim().Length == 0)
                            newFileName = "Answers";
                        
                    }
                    else
                    {
                        ms =
                            (MemoryStream)cInstance.ConvertMemoryStream(
                            new StreamReader(GetDocument(doc.Template.UniqueIdentifier)), templateName, out newFileName);
                    }
                    if (doc.HasTemplate)
                        StoreDocument(doc.UniqueIdentifier, doc.UniqueIdentifier, doc.Template.UniqueIdentifier, newFileName, ms);
                    else
                        StoreDocument(doc.UniqueIdentifier, doc.UniqueIdentifier, Guid.Empty, newFileName, ms);
                }
            }
            catch
            {
                throw;
            }
        }
    }
}