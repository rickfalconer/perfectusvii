﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using OpenXmlWordProcessing = DocumentFormat.OpenXml.Wordprocessing;
using System.IO;
using System.Xml;

namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    static public class AgentMergeDocumentReferences
    {
        private static XNamespace ns = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
        private static XNamespace ns_ve = "http://schemas.openxmlformats.org/markup-compatibility/2006";
        private static XNamespace ns_o = "urn:schemas-microsoft-com:office:office";
        private static XNamespace ns_r = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
        private static XNamespace ns_m = "http://schemas.openxmlformats.org/officeDocument/2006/math";
        private static XNamespace ns_v = "urn:schemas-microsoft-com:vml";
        private static XNamespace ns_wp = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
        private static XNamespace ns_w10 = "urn:schemas-microsoft-com:office:word";
        private static XNamespace ns_wne = "http://schemas.microsoft.com/office/word/2006/wordml";
        private static XNamespace ns_a = "http://schemas.openxmlformats.org/drawingml/2006/main";
        private static XNamespace ns_pic = "http://schemas.openxmlformats.org/drawingml/2006/picture";
        private static XNamespace ns_dgm = "http://schemas.openxmlformats.org/drawingml/2006/diagram";
        private static XNamespace ns_ds = "http://schemas.openxmlformats.org/officeDocument/2006/customXml";
        private static XNamespace ns_c = "http://schemas.openxmlformats.org/drawingml/2006/chart";
        private static XAttribute[] ns_attrs =
        {
            new XAttribute(XNamespace.Xmlns + "ve", ns_ve),
            new XAttribute(XNamespace.Xmlns + "o", ns_o),
            new XAttribute(XNamespace.Xmlns + "r", ns_r),
            new XAttribute(XNamespace.Xmlns + "m", ns_m),
            new XAttribute(XNamespace.Xmlns + "v", ns_v),
            new XAttribute(XNamespace.Xmlns + "wp", ns_wp),
            new XAttribute(XNamespace.Xmlns + "w10", ns_w10),
            new XAttribute(XNamespace.Xmlns + "w", ns),
            new XAttribute(XNamespace.Xmlns + "wne", ns_wne)
        };

        public static void CopyReferences(WordprocessingDocument oldDoc, WordprocessingDocument newDoc, IEnumerable<XElement> paragraphs, List<ImageData> images, ref Dictionary<string, string> imageRefs)
        {
            // Copy all styles to the new document
            if (oldDoc.MainDocumentPart.StyleDefinitionsPart != null)
            {
                XDocument oldStyles = GetXDocument(oldDoc.MainDocumentPart.StyleDefinitionsPart);
                if (newDoc.MainDocumentPart.StyleDefinitionsPart == null)
                {
                    newDoc.MainDocumentPart.AddNewPart<StyleDefinitionsPart>();
                    XDocument newStyles = GetXDocument(newDoc.MainDocumentPart.StyleDefinitionsPart);
                    newStyles.Add(oldStyles.Root);
                }
                else
                {
                    XDocument newStyles = GetXDocument(newDoc.MainDocumentPart.StyleDefinitionsPart);
                    MergeStyles(oldStyles, newStyles);
                }
            }

            // Copy fontTable to the new document
            if (oldDoc.MainDocumentPart.FontTablePart != null)
            {
                XDocument oldFontTable = GetXDocument(oldDoc.MainDocumentPart.FontTablePart);
                if (newDoc.MainDocumentPart.FontTablePart == null)
                {
                    newDoc.MainDocumentPart.AddNewPart<FontTablePart>();
                    XDocument newFontTable = GetXDocument(newDoc.MainDocumentPart.FontTablePart);
                    newFontTable.Add(oldFontTable.Root);
                }
                else
                {
                    XDocument newFontTable = GetXDocument(newDoc.MainDocumentPart.FontTablePart);
                    MergeFontTables(oldFontTable, newFontTable);
                }
            }

            //CopyNumbering(oldDoc, newDoc, paragraphs);
            //CopyFootnotes(oldDoc, newDoc, paragraphs);
            //CopyEndnotes(oldDoc, newDoc, paragraphs);
            //CopyHyperlinks(oldDoc, newDoc, paragraphs);
            //CopyComments(oldDoc, newDoc, paragraphs);
            //CopyHeaders(oldDoc, newDoc, paragraphs, images);
            //CopyFooters(oldDoc, newDoc, paragraphs, images);
            CopyImages(oldDoc, newDoc, paragraphs, images, ref imageRefs);
            //CopyDiagrams(oldDoc, newDoc, paragraphs);
            //CopyShapes(oldDoc, newDoc, paragraphs, images);
            //CopyCustomXml(oldDoc, newDoc, paragraphs);
            //CopyEmbeddedObjects(oldDoc, newDoc, paragraphs);
            //CopyCharts(oldDoc, newDoc, paragraphs);
        }

        private static void CopyImages(WordprocessingDocument oldDoc, WordprocessingDocument newDoc, IEnumerable<XElement> paragraphs, List<ImageData> images, ref Dictionary<string, string> imageRefs)
        {
            foreach (XElement imageReference in paragraphs.Descendants(ns_a + "blip"))
            {
                string relId = imageReference.Attribute(ns_r + "embed").Value;
                ImagePart oldPart = (ImagePart)oldDoc.MainDocumentPart.GetPartById(relId);
                ImageData temp = ManageImageCopy(oldPart, images);
                if (temp.ResourceID == null)
                {
                    ImagePart newPart = newDoc.MainDocumentPart.AddImagePart(oldPart.ContentType);
                    temp.ResourceID = newDoc.MainDocumentPart.GetIdOfPart(newPart);
                    temp.WriteImage(newPart);
                }
                imageReference.Attribute(ns_r + "embed").Value = temp.ResourceID;
                
                //add to image ref list so answerset can be updated
                imageRefs.Add(relId, temp.ResourceID);
            }
        }

        public static XDocument GetXDocument(OpenXmlPart part)
        {
            XDocument xdoc = part.Annotation<XDocument>();
            if (xdoc != null)
                return xdoc;

            using (StreamReader sr = new StreamReader(part.GetStream()))
            using (XmlReader xr = XmlReader.Create(sr))
            {
                xdoc = XDocument.Load(xr);
            }

            part.AddAnnotation(xdoc);
            return xdoc;
        }

        private static void MergeFontTables(XDocument fromFontTable, XDocument toFontTable)
        {
            foreach (XElement font in fromFontTable.Root.Elements(ns + "font"))
            {
                string name = font.Attribute(ns + "name").Value;
                if (toFontTable.Root.Elements(ns + "font").Where(o => o.Attribute(ns + "name").Value == name).Count() == 0)
                    toFontTable.Root.Add(new XElement(font));
            }
        }

        private static void MergeStyles(XDocument fromStyles, XDocument toStyles)
        {
            foreach (XElement style in fromStyles.Root.Elements(ns + "style"))
            {
                string name = style.Attribute(ns + "styleId").Value;
                if (toStyles.Root.Elements(ns + "style").Where(o => o.Attribute(ns + "styleId").Value == name).Count() == 0)
                    toStyles.Root.Add(new XElement(style));
            }
        }

        // General function for handling images that tries to use an existing image if they are the same
        private static ImageData ManageImageCopy(ImagePart oldImage, List<ImageData> images)
        {
            ImageData oldImageData = new ImageData(oldImage);
            foreach (ImageData item in images)
            {
                if (item.Compare(oldImageData))
                    return item;
            }
            images.Add(oldImageData);
            return oldImageData;
        }
    }

    // This class is used to prevent duplication of images
    public class ImageData
    {
        private byte[] m_Image;
        private string m_ContentType;
        private string m_ResourceID;

        public ImageData(ImagePart part)
        {
            m_ContentType = part.ContentType;
            using (Stream s = part.GetStream(FileMode.Open, FileAccess.Read))
            {
                m_Image = new byte[s.Length];
                s.Read(m_Image, 0, (int)s.Length);
            }
        }

        public void WriteImage(ImagePart part)
        {
            using (Stream s = part.GetStream(FileMode.Create, FileAccess.ReadWrite))
            {
                s.Write(m_Image, 0, m_Image.GetUpperBound(0));
            }
        }

        public string ResourceID
        {
            get { return m_ResourceID; }
            set { m_ResourceID = value; }
        }

        public bool Compare(ImageData arg)
        {
            if (m_ContentType != arg.m_ContentType)
                return false;
            if (m_Image.GetLongLength(0) != arg.m_Image.GetLongLength(0))
                return false;
            // Compare the arrays byte by byte
            long length = m_Image.GetLongLength(0);
            for (long n = 0; n < length; n++)
                if (m_Image[n] != arg.m_Image[n])
                    return false;
            return true;
        }
    }
}
