using System;
using System.IO;
using System.Text;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using System.Collections.Generic;
using Perfectus.Server.Common.Data;


namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {
        private bool MirroringRequiresAnswerset(List<Delivery> evaluatedDeliveries)
        {
            bool have_mirror_plugin = false;
            foreach (Delivery delivery in evaluatedDeliveries)
                foreach (IDocument doc in delivery.Documents)
                    if ((doc is Document) && ((doc as Document).Converter.Descriptor.UniqueIdentifier.ToString() == "3542e8d6-f18e-442e-8683-8c87cdf0a666"))
                    {
                        have_mirror_plugin = true;
                        break;
                    }

            if (package.MakeMirror.Evaluate(false, AnswerProvider.Caller.System) || have_mirror_plugin)
                return true;
            return false;
        }


        private void maybeHandleMirrorPackages(List<Delivery> deliveries)
        {
            try
            {
                // As far as the old code worked the rule might be...

                // Clean up the document only if mirroring is enabled.
                // or if the mirror plugin is used.

                // Check for the mirror plugin...
                bool have_mirror_plugin = false;
                foreach (Delivery delivery in deliveries)
                    foreach (IDocument doc in delivery.Documents)
                        if ((doc is Document) && ((doc as Document).Converter.Descriptor.UniqueIdentifier.ToString() == "3542e8d6-f18e-442e-8683-8c87cdf0a666"))
                        {
                            have_mirror_plugin = true;
                            break;
                        }


                if (package.MakeMirror.Evaluate(false, AnswerProvider.Caller.System) || have_mirror_plugin)
                {
                    Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "Making mirror");

                    string mirrorDocType = package.MirrorDocType;
                    MemoryStream msAnswers = GetAnswersetDocument(this.answersetGuid);

                    // save input stream to xmlTextReader then load xmlDocument from it
                    XmlDocument returnDoc = new XmlDocument();
                    String xml = Encoding.UTF8.GetString(msAnswers.ToArray());
                    int index = xml.IndexOf('<');
                    xml = xml.Substring(index);
                    returnDoc.LoadXml(xml);

                    //if we are the master document create mirror doc, do master clean ups, submit mirror to perfectus.
                    if (mirrorDocType == "Master")
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "mirrorDocType == Master");

                        // make mirror copy.				
                        XmlDocument mirrorDoc = new XmlDocument();
                        mirrorDoc.LoadXml(returnDoc.DocumentElement.OuterXml);
                        XmlToMirrorAnswerSet.CreateMirror(mirrorDoc);
                        XmlToMirrorAnswerSet.CleanUpMasterDocument(returnDoc);
                        SubmitToPerfectus.UploadMirrorAnswerSet(mirrorDoc, userId);
                    }
                    else if (mirrorDocType == "Mirror")
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, "mirrorDocType == Mirror");
                        // if we are the mirror document do mirror clean ups.
                        XmlToMirrorAnswerSet.CleanUpMirrorDocument(returnDoc);
                    }

                    msAnswers.Dispose();
                    msAnswers = new MemoryStream();
                    returnDoc.Save(msAnswers);
                    ReplaceDocument(answersetGuid, msAnswers);
                }
            }
            catch (Exception ex)
            {
                Log.Error(Log.Category.Engine2, new ID("ProcessCoordinator.MirrorError"));
                //                        Status.SetInstanceStatus(instanceId, Status.CoordinatorStatus.FAILED);
            }
        }


        public sealed class SubmitToPerfectus
        {
            public static void UploadMirrorAnswerSet(XmlDocument xInterview, string userIden)
            {
                int versionNumber = int.MinValue;
                int revisionNumber = int.MinValue;
                Guid packageId;
                Guid oldPackageInstanceId;

                // get package information from original answerset
                versionNumber = Convert.ToInt32(xInterview.SelectSingleNode("/answerSet/@packageVersion").Value);
                revisionNumber = Convert.ToInt32(xInterview.SelectSingleNode("/answerSet/@packageRevision").Value);
                packageId = new Guid(xInterview.SelectSingleNode("/answerSet/@packageId").Value);
                oldPackageInstanceId = new Guid(xInterview.SelectSingleNode("/answerSet/@packageInstanceId").Value);

                // Create a new instance of the given package
                Guid instanceId = Perfectus.Server.InterviewSystem.InstanceManagement.CreateInstance(packageId, versionNumber, revisionNumber, userIden);

                // Get the instance we just created
                Guid currentPage;
                Package instance = Perfectus.Server.InterviewSystem.InstanceManagement.GetInstance(instanceId, out currentPage);

                // Apply the XML answerSet to the instance
                Perfectus.Server.AnswerAcquirer.Xml.ApplyAnswerSetXml.Apply(instanceId, instance, xInterview, currentPage, Guid.Empty);

                // Copy and files that not to be copied, eg files from file upload.			
                Perfectus.Server.InterviewSystem.InstanceManagement.CopyMirrorFiles(instanceId, oldPackageInstanceId);

                // Flag the instance as been a Mirror document				
                instance.MirrorDocType = "Mirror";

                // Force a save. Otherwise reference wont get mirrored.
                Perfectus.Server.InterviewSystem.InstanceManagement.SaveInstance(instanceId, instance);

                // Submit the instance through perfectus
                Job job = new Job(instance.UniqueIdentifier, instanceId, versionNumber, revisionNumber, userIden);
                Perfectus.Server.InterviewSystem.InstanceManagement.EnQueuePackageInstance(job);
            }

        }

    }
}