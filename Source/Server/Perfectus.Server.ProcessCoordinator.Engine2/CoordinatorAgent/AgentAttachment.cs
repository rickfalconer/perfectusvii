using System;
using Perfectus.Library.Logging;
using Perfectus.Library.Exception;
using Perfectus.Library.Localisation;
using Perfectus.Server.DataAccess.InterviewSystem;
using System.Collections.Generic;
using Perfectus.Common.PackageObjects;
using System.IO;
using Perfectus.Server.Common.Data;
using System.Xml;
using Perfectus.Common;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.PluginKit;
using System.Globalization;
using System.Text;


namespace Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent
{
    /// <summary>
    /// Summary description for Agent.
    /// </summary>
    public partial class Agent
    {
        private StringBindingMetaData sbmd;

        private void maybeLoadAttachments(List<Delivery> evaluatedDeliveries)
        {
            foreach (Delivery delivery in evaluatedDeliveries)
                foreach (IDocument attachment in delivery.Documents)
                    if (attachment is IAttachment)
                    {
                        Guid guidToLookFor = attachment.UniqueIdentifier;
                        if (!ExistsDocument(guidToLookFor))
                        {
                            _updateProc(JobStatus.ASSEMBLE, String.Format("Attachment='{0}'", attachment.Name));

                            if (attachment is AttachmentStatic)
                            {
                                AttachmentBase att = attachment as AttachmentBase;

                                String attachmentName = att.Name;

                                // If Document.Filename is not set, 
                                // then it will evaluate Template.PublishedFileName
                                if (att.FileName != null)
                                {
                                    string metaDataPackageReference = InstanceManagement.GetInstanceReference(instanceId);
                                    sbmd = new StringBindingMetaData(package.Name, metaDataPackageReference, instanceId, versionNumber, revisionNumber);

                                    try
                                    {
                                        string eval = att.FileName.Evaluate(AnswerProvider.Caller.Assembly, sbmd);
                                        if (eval != null && eval.Trim().Length > 0)
                                            attachmentName = eval;
                                    }
                                    catch
                                    {
                                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Failed to evaluate filename: {0}", att.Name));
                                    }
                                }

                                byte[] b = DataAccess.PackageManager.Retrieval.GetAttachmentBytes(
                                    packageId,
                                    versionNumber,
                                    revisionNumber,
                                    att.OriginalUniqueueIdentifier != Guid.Empty ?
                                    att.OriginalUniqueueIdentifier :
                                    att.UniqueIdentifier);
                                StoreDocument(guidToLookFor, att.OriginalUniqueueIdentifier, Guid.Empty, attachmentName, new MemoryStream(b));
                            }
                            else if (attachment is AttachmentDynamic)
                            {
                                HandleDynamicAttachment(attachment as AttachmentDynamic);
                            }
                        }
                    }
        }


        private void HandleDynamicAttachment(AttachmentDynamic attachment)
        {
            // Maybe the picker is in a repeater, so we have to invesitigate the AnswerSet to
            // find the number of answers
            MemoryStream msAnswers = GetAnswersetDocument(answersetGuid);

            XmlDocument answerSetXml = new XmlDocument();
            String xml = System.Text.Encoding.UTF8.GetString(msAnswers.ToArray());
            int index = xml.IndexOf('<');
            xml = xml.Substring(index);
            answerSetXml.LoadXml(xml);

            String xPath = String.Format("*/answer[@answeredItemId='{0}']", attachment.GetPayload());
            XmlNodeList lstAnswers = answerSetXml.SelectNodes(xPath);

            int numberOfANswers = Math.Max(1, lstAnswers.Count);

            // The handlingbetween build-in display types and integration types is different
            Question q = package.GetLibraryItemByUniqueIdentifier(attachment.GetPayload()) as Question;

            if (q.DisplayType >= QuestionDisplayType.Extension1)
                HandleDynamicIntegrationAttachment(attachment, q, numberOfANswers);
            else
                HandleDynamicInterviewAttachment(attachment, numberOfANswers);
        }


        private void HandleDynamicIntegrationAttachment(AttachmentDynamic attachment, Question q, int maxNumberOfAnswers)
        {
            QuestionDisplayType qdt = q.DisplayType;
            DisplayTypeExtensionInfo extensionInfo;

            if (!package.ExtensionMapping.ContainsKey(qdt))
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.NoExtensionInformation"));

            extensionInfo = package.ExtensionMapping[qdt];
            string pluginId = extensionInfo.ServerSideFetcherPluginId;
            if (pluginId == null || pluginId.Length == 0)
                throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.NoComponentSpecified"));

            foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
            {
                if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "fetcher" &&
                    plugin.key.ToLower(CultureInfo.InvariantCulture) == pluginId.ToLower(CultureInfo.InvariantCulture))
                {
                    IFetcher fetcher = (IFetcher)(AppDomain.CurrentDomain.CreateInstanceFromAndUnwrap(plugin.path, plugin.typeName));
                    if (fetcher == null)
                        throw new PerfectusException(Log.Category.Engine2, new ID("ProcessCoordinator.FailedToLoadPlugin"), plugin.path, plugin.typeName);

                    string contentType;
                    byte[] fileBytes;

                    Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Integration attachment '{0}' of type '{1}' with '{2}' repeatation, plugin '{3}'", q.Name, q.DisplayType, maxNumberOfAnswers, plugin.typeName));

                    for (int sequenceNumber = 0; sequenceNumber < maxNumberOfAnswers; sequenceNumber++)
                    {
                        String fileData = String.Empty;

                        // Expect to find as many answers bound to the question, as there were answers in the AnswerSet
                        AnswerCollection answers = q.GetAnswer(sequenceNumber, AnswerProvider.Caller.Assembly);

                        // Let's assume that the data portion is always a string
                        // (IFetcher expects it...)
                        if (answers != null && answers.Count > 0)
                            fileData = AnswerProvider.ConvertToBestType(answers[0], PerfectusDataType.Attachment, AnswerProvider.Caller.Assembly, sbmd).ToString();

                        // Something might be wrong here
                        if (fileData == String.Empty)
                        {
                            // Maybe the question is not answered...

                            //Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.InvalidAttachmentIntegrationData"), q.Name, q.DisplayType, maxNumberOfAnswers, plugin.typeName);
                            continue;
                        }

                        // Get the content
                        using (Stream s = fetcher.Fetch(fileData, package, instanceId, out contentType))
                        {
                            if (s == null)
                            {
                                throw new PerfectusException(Log.Category.Engine2,
                                    new ID("ProcessCoordinator.LoadFetcherReturnedNull"), q.Name, sequenceNumber, plugin.typeName);
                            }
                            s.Seek(0, SeekOrigin.Begin);

                            // Fetch the file into a byteArray
                            fileBytes = new byte[s.Length];
                            s.Read(fileBytes, 0, fileBytes.Length);

                            String originalPath = String.Format("IntegrationFile_{0}_{1}", sequenceNumber, contentType);

                            // UGLY!!!!! Remove in future version with implementation for MIMETYPE
                            // For now it's the only way to retrieve a filename from SharePoint without modifying the existing implementation and/or
                            // the IFetcher interface

                            try
                            {
                                XmlDocument sharePointFileXml = new XmlDocument();
                                sharePointFileXml.LoadXml(fileData);
                                originalPath = sharePointFileXml.SelectSingleNode("//Data/ItemUrl").InnerText;

                                if (originalPath.Contains("/"))
                                    originalPath = originalPath.Substring(originalPath.LastIndexOf("/") + 1);

                            }
                            catch
                            {
                            }

                            // Create a compatible filename
                            originalPath = GetValidFilename(originalPath);
                            // <---
                            Guid idToUse = CreateFakeGuid(attachment.UniqueIdentifier, sequenceNumber);
                            StoreDocument(idToUse, idToUse, attachment.GetPayload(), originalPath, new MemoryStream(fileBytes));
                        }
                    }
                    break;
                }
            }
        }

        private String GetValidFilename(String inputFilename)
        {
            char[] invalidFileChars = Path.GetInvalidFileNameChars();
            StringBuilder fileName = new StringBuilder(inputFilename);
            foreach (char c in invalidFileChars)
                fileName.Replace(c, '_');
            return fileName.ToString();
        }


        private void HandleDynamicInterviewAttachment(AttachmentDynamic attachment, int maxNumberOfAnswers)
        {
            for (int sequenceNumber = 0; sequenceNumber < maxNumberOfAnswers; sequenceNumber++)
            {
                string originalPath;
                string contentType;

                byte[] fileBytes = InstanceManagement.File_GetBytes(instanceId, sequenceNumber, attachment.GetPayload(), out originalPath, out contentType);

                Log.Trace(Log.Category.Engine2, Log.Priority.MID, string.Format("Interview attachment for id '{0}' sequence {1} path '{2}' type '{3}'", attachment.GetPayload(), sequenceNumber, originalPath, contentType));

                if (fileBytes == null || fileBytes.Length == 0)
                {
                    return;
                }
                contentType = contentType.ToLower();

                int index = originalPath.LastIndexOf("\\");

                if (originalPath == null || originalPath.Length == 0)
                    originalPath = String.Format("DynamicUpload-{0}-{1}", sequenceNumber, attachment.GetPayload());
                else
                    originalPath = originalPath.Substring(index + 1);

                originalPath = GetValidFilename(originalPath);
                //pf3235 use published file name
                // evaluate .PublishedFileName
                if (attachment.FileName != null)
                {
                    string metaDataPackageReference = InstanceManagement.GetInstanceReference(instanceId);
                    sbmd = new StringBindingMetaData(package.Name, metaDataPackageReference, instanceId, versionNumber, revisionNumber);

                    try
                    {
                        string eval = attachment.FileName.Evaluate(AnswerProvider.Caller.Assembly, sbmd);
                        if (eval != null && eval.Trim().Length > 0)
                            originalPath = eval;
                    }
                    catch
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, string.Format("Failed to evaluate filename: {0}", attachment.Name));
                    }
                }


                Guid idToUse = CreateFakeGuid(attachment.UniqueIdentifier, sequenceNumber);
                StoreDocument(idToUse, idToUse, attachment.GetPayload(), originalPath, new MemoryStream(fileBytes));
            }
        }

        // Need a unique ID for the caching system
        private Guid CreateFakeGuid(Guid guid, int sequence)
        {
            int pos = 15;
            Byte[] byteArray = guid.ToByteArray();

            for (int i = 0; i < sequence; i++)
            {
                if (byteArray[pos] == Byte.MaxValue)
                {
                    byteArray[pos] = 0;
                    pos--;
                    continue;
                }
                byteArray[pos]++;
            }
            return new Guid(byteArray);
        }
    }
}
