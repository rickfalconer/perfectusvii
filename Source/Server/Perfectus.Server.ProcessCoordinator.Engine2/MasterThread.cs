using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.ProcessCoordinator.Receiver;
using System.Threading;
using Perfectus.Library.Logging;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Localisation;

namespace Perfectus.Server.ProcessCoordinator.Engine2
{
    class MasterThread
    {
        private Object _lock;                   // used to syncronize tasks
        private bool _shutdown;
        private int _timeOutInMilliSeconds;
        private int _threadsAtOnce;
        private AutoResetEvent _signalToStop;
        private IMessageReader [] _reader;

        private AutoResetEvent[] doneEvents;    
        private WorkerData[] dataArray;
        private Thread[] threadArray;

        internal MasterThread(AutoResetEvent signalToStop, IMessageReader [] reader)
        {
            _reader = reader;
            _signalToStop = signalToStop;

            // timeoput shopuld be between 1 minute and 1 hour
            _timeOutInMilliSeconds = 1000 * Math.Min(Math.Max(Config.Shared.queue.timeout, 60), 3600);

            // number of threads should be between 1 and 8
            _threadsAtOnce = Math.Min(Math.Max(Config.Shared.coordinator.threadsAtOnce, 1), 8);

            Log.Info(Log.Category.Engine2, new ID("ProcessCoordinator.ServiceInit"),
                System.Reflection.Assembly.GetExecutingAssembly().GetName(false).Version.ToString(),
                _threadsAtOnce,
                _timeOutInMilliSeconds / 1000,
                Config.Shared.queue.maxRetry,
                Config.Shared.queue.delayTime);
        }

        private int FindFreeController()
        {
            lock (_lock)
                for (int index = 0; index < threadArray.Length; index++)
                    if (threadArray[index] == null)
                        return index;
            return -1;
        }
        private bool ControllerAvailable() { return !_shutdown && FindFreeController() >= 0; }

        private void StartControllerThread(JobRead jobRead, JobResult jobResult)
        {
            lock (_lock)
            {
                int index = FindFreeController();
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Main - starting controller {0}",index));

                dataArray[index] = new WorkerData(jobRead, jobResult);
                
                // The first two events in the array are reseved for Start/Stop and 'message available' notifications
                ControllerThread controllerThread = new ControllerThread(doneEvents[index + 1], dataArray[index]);
                threadArray[index] = new Thread(new ThreadStart(controllerThread.Start));
                threadArray[index].Start();
            }
        }

        /// <summary>
        /// Invoked by any listener that 'sees' a job on a queue
        /// </summary>
        /// <param name="jobRead">Used to retrieve the job</param>
        /// <param name="jobResult">Used to update the result</param>
        public void jobAvailable(JobRead jobRead, JobResult jobResult)
        {
            // The receiver tells us that a job is available for processing
            // Only pick the job if it can be processed. 
            // Otherwise we rely on the fact that the receiver tells us again later
            if (ControllerAvailable())
                StartControllerThread(jobRead, jobResult);
            else
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Main - Job advertised but no thread available");
        }


        private void Init()
        {
            _shutdown = false;

            _lock = new Object();

            // Simple thread pool...
            threadArray = new Thread[_threadsAtOnce];
            dataArray = new WorkerData[_threadsAtOnce];

            // MasterThread will listen to controllers using the ResetEvent array
            // The first event is reserved for the hosting app. to signal a STOP command
            doneEvents = new AutoResetEvent[_threadsAtOnce + 1];
            doneEvents[0] = _signalToStop;
            for (int i = 1; i <= _threadsAtOnce; i++)
            {
                AutoResetEvent autoResetEvent = new AutoResetEvent(false);
                doneEvents[i] = autoResetEvent;
            }
        }

        /// <summary>
        /// Main loop of the MainThread
        /// </summary>
        internal void Start()
        {
            Log.Info(Log.Category.Engine2, new ID("ProcessCoordinator.ServiceStart"));

            DateTime lastOrphanCheck = DateTime.Now;
            
            Init();
            // Forever...
            while (true)
            {
                // Wait for any notification, or 30 seconds (whatever comes first)
                int result = WaitHandle.WaitAny(doneEvents, 30000, false);

                // 30 sec. timeout
                if (result == WaitHandle.WaitTimeout)
                {
                    if (DateTime.Now - lastOrphanCheck > new TimeSpan(0, 10, 0))
                    {
                        foreach (IMessageReader rdr in _reader)
                            rdr.MaybeFreeOrphans(_timeOutInMilliSeconds / 1000);
                        lastOrphanCheck = DateTime.Now;
                    }

                    // If necessary, we can place any admin / maintenance code here
                    Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Main - Performing health check");

                    foreach (IMessageReader rdr in _reader)
                    { 
                        String message;
                        if (!rdr.HealthCheck(out message))
                        {
                            Log.Error(Log.Category.Engine2, new ID("ProcessCoordinator.HealthTestFailed"), message);
                            
                            if (Config.Shared.coordinator.stopOnCriticalCondition > 0)
                            {
                                Log.Critical( Log.Category.Engine2, new ID("ProcessCoordinator.HealthTestFailed"), message);
                                StopInternal();
                            }
                            return;
                        }
                    }
                }
                else if (result == 0)
                {
                    Log.Info(Log.Category.Engine2, new ID("ProcessCoordinator.ServiceStop"));
                    StopInternal();
                    doneEvents[0].Set();
                    break;
                }
                else
                {
                    // Controller with index 'result-2' finished (note that result is the index in 'doneEvents'
                    WorkerData tran = dataArray[result - 1];

                    if (tran.Result == WorkerData.resultEnum.Finished)
                        Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, String.Format("Main - Job with index {0} succeeded",result - 1));
                    else
                        Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Main - Job with index {0} failed", result - 1));

                    dataArray[result - 1] = null;
                    threadArray[result - 1] = null;

                    /*
                    // Maybe we want to terminate the entire processCoordinator if a single worker fails
                    if (tran.Result == WorkerData.resultEnum.Failed && _ShallTerminateOnError)
                    {
                        doneEvents[0].Set();
                        //   Log.WriteWarning("ProcessCoordinator will terminate itself");
                    }
                    */
                }
            }
        }

        private void StopInternal()
        {
            _shutdown = true;
            lock (_lock)
            {
                // 1. Wait for running controllers to finish the work.
                foreach (Thread thread in threadArray)
                    if (thread != null && thread.IsAlive)
                    {
                        Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Main - waiting for controller to finish during shutdown");
                        thread.Join(_timeOutInMilliSeconds);
                    }

                // 2. Maybe a controller hangs (should 'almost' never happen)
                foreach (Thread thread in threadArray)
                    if (thread != null && thread.IsAlive)
                    {
                        Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.ControllerNotResponsiveDuringShutdown"));
                        thread.Abort();
                    }

                // 3. Wait again - thread might need time to requeue message in its exception handler
                foreach (Thread thread in threadArray)
                    if (thread != null && thread.IsAlive)
                        thread.Join(_timeOutInMilliSeconds);
            }
            // Do not close doneEvents[0] - its owned by the caller
            for (int i = 1; i <= _threadsAtOnce; i++)
                doneEvents[i].Close();
        }
    }

    public class WorkerData
    {
        public enum resultEnum { Unstarted, Running, Finished, Failed }
        private resultEnum result;
        public resultEnum Result
        {
            get { return result; }
            set { result = value; }
        }

        private JobRead _jobRead;
        internal JobRead jobRead { get { return _jobRead; } }

        private JobResult _jobResult;
        internal JobResult jobResult { get { return _jobResult; } }

        public WorkerData(JobRead paramJobRead, JobResult paramJobResult)
            : this()
        {
            _jobRead = paramJobRead;
            _jobResult = paramJobResult;
        }
        
        internal WorkerData()
        {
            Result = resultEnum.Unstarted;
        }
    }
}
