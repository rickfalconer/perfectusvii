using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Server.ProcessCoordinator.Receiver;
using Perfectus.Server.Common.Data;
using System.Threading;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using System.IO;

namespace Perfectus.Server.ProcessCoordinator.Engine2
{
    public class Ctrl
    {
        private IMessageReader[] irms;      // List of job readers (database, MSMQ, ...)
        private static Ctrl _ptr;           // singleton
        
        // Used to create, start and stop the master thread
        private MasterThread masterThread;  
        private Thread master;
        private AutoResetEvent signalMasterToStop;

        // use singleton
        public static Ctrl ptr()
        {
            if (null == _ptr)
                _ptr = new Ctrl();
            return _ptr;
        }

        private Ctrl()
        {
            // Load available job receiver (database, MSMSQ, ...)
            irms = JobReceiver.PerfectusReceiver();

            // ResetEvent to signal master to stop processing
            signalMasterToStop = new AutoResetEvent(false);
            masterThread = new MasterThread(signalMasterToStop, irms);
        }


        /// <summary>
        /// Clean up the temp folder
        /// </summary>
        /// <param name="folder"></param>
        private void RemoveFilesAndFolder(String folder)
        {
            DirectoryInfo tmpDirectory = new DirectoryInfo(folder);

            foreach (FileInfo file in tmpDirectory.GetFiles())
                file.Delete();

            foreach (DirectoryInfo directory in tmpDirectory.GetDirectories())
                RemoveFilesAndFolder(directory.FullName);

            tmpDirectory.Delete();
        }

        private void RemoveFiles(String folder)
        {
            try
            {
                DirectoryInfo tmpDirectory = new DirectoryInfo(folder);

                foreach (FileInfo file in tmpDirectory.GetFiles())
                    file.Delete();

                foreach (DirectoryInfo directory in tmpDirectory.GetDirectories())
                    RemoveFilesAndFolder(directory.FullName);
            }
            catch { }
        }

        /// <summary>
        /// External host starts Process Coordinator
        /// </summary>
        public void Start()
        {
            DirectoryInfo tmpDirectory = new DirectoryInfo(Config.Shared.coordinator.tempDirectory);
            if (!tmpDirectory.Exists)
                tmpDirectory.Create();

            DirectoryInfo subDirectory = new DirectoryInfo(String.Format ("{0}\\{1}",tmpDirectory.FullName,"temp"));
            if (!subDirectory.Exists)
                subDirectory.Create();

            RemoveFiles(subDirectory.FullName);

            System.Environment.SetEnvironmentVariable("Temp", subDirectory.FullName);
            System.Environment.SetEnvironmentVariable("TMP", subDirectory.FullName);

            // Start the master 
            master = new Thread(new ThreadStart(masterThread.Start));
            master.Start();

            // Now we can listen to receivers, 
            // the delegate 'masterThread.jobAvailable' will be called if jobs arrive
            foreach (IMessageReader irm in irms)
                irm.Start(masterThread.jobAvailable);

            // The master might terminate (maybe expected ?!?)
            // Want to know...
            Thread masterControler = new Thread(new ThreadStart(StartMasterControler));
            masterControler.Start();
        }

        /// <summary>
        /// Callback - Master thread terminated unexpected
        /// </summary>
        private void StartMasterControler()
        {
            master.Join();

            // Nothing to do if the master terminated because of a Stop-command
            // Otherwise its an error and we need to clean up
            if (!signalMasterToStop.WaitOne(0, true))
            {
                Log.Warn(Log.Category.Engine2, new ID("ProcessCoordinator.CoordinatorStoppedUnexpected"));
                Stop();

                // whatever is necessary in the host environment
                Environment.Exit(new ID("ProcessCoordinator.CoordinatorStoppedUnexpected").MessageNumber);
            }
        }

        /// <summary>
        /// External host stops Process Coordinator
        /// </summary>
        public void Stop()
        {
            // Stop each receiver
            foreach (IMessageReader irm in irms)
                irm.Stop();

            // Tell the master to stop
            signalMasterToStop.Set();
            
            // It is unlikely that the master immediatly stops, as documents might currently be processed
            // At least any assembling should not take longer than "timeout". 
            // So we can assume that waiting for twice the time is sufficient
            master.Join(new TimeSpan(0, 0, 2 * Config.Shared.queue.timeout));
            if (master.ThreadState != ThreadState.Stopped)
            {
                // Something REALLY hangs... kill it.
                Log.Trace(Log.Category.Engine2, Log.Priority.LOW, String.Format("Ctrl - master thread did not response within {0} time to the stop command and will be aborted", 
                    new TimeSpan(0, 0, 2 * Config.Shared.queue.timeout)));
                master.Abort();
            }
        }
    }
}
