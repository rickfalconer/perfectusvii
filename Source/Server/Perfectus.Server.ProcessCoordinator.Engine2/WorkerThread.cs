using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Perfectus.Library.Logging;
using Perfectus.Server.Common.Data;
using Perfectus.Server.ProcessCoordinator.Engine.CoordinatorAgent;

namespace Perfectus.Server.ProcessCoordinator.Engine2
{
    class WorkerThread
    {
        private ManualResetEvent _finshedEvent; // Used to signal parent that work completed successfully
        private StatusUpdateDelegate _updateProc;
        private ProcessingTask _task;
        private Int32 _jobId;

        private WorkerData.resultEnum result;
        public WorkerData.resultEnum Result { get { return result;}}

        internal WorkerThread(ManualResetEvent finshedEvent, StatusUpdateDelegate updateProc, ProcessingTask task, Int32 jobId)
        {
            _finshedEvent = finshedEvent;
            _updateProc = updateProc;
            _task = task;
            _jobId = jobId;
        }

        public void ThreadStartProc()
        {
            try
            {
                Log.Trace(Log.Category.Engine2, Log.Priority.HIGH, "Worker running");

                Agent agent = new Agent(_task, _updateProc, _jobId);
                agent.Start();

                SetSuccess(true);
            }
            catch (Exception ex)
            {
                SetSuccess(false);
            }
        }

        private void SetSuccess(bool success)
        {
            result = success ? WorkerData.resultEnum.Finished:WorkerData.resultEnum.Failed;
            _finshedEvent.Set();
        }
    }
}