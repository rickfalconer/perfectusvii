using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Caching;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;

namespace Perfectus.Server.InterviewSystem
{
	/// <summary>
	/// Summary description for InstanceManagement.
	/// </summary>
	public sealed class InstanceManagement
	{
		private InstanceManagement()
		{
		}

		public static Package GetInstance(Guid packageInstanceId)
		{
			Guid x;
			return GetInstance(packageInstanceId, out x);
		}

		public static Package GetInstance(Guid packageInstanceId, out Guid currentPageId)
		{
			// Does it exist in the cache?
			HttpContext context = HttpContext.Current;
			string cacheInstanceKey = null;
			string cachePageKey = null;
			object cacheInstanceValue = null;
			object cachePageValue = null;
			Package p;
			if (context != null)
			{
				cacheInstanceKey = string.Format("instance_{0:N}", packageInstanceId);
				cachePageKey = string.Format("curpage_{0:N}", packageInstanceId);
				cacheInstanceValue = HttpContext.Current.Cache[cacheInstanceKey];
				cachePageValue = HttpContext.Current.Cache[cachePageKey];
			}
			if (context != null && cacheInstanceValue != null && cacheInstanceValue is Package && cachePageValue != null && cachePageValue is Guid)
			{
				Debug.WriteLine("Retrieving instance from cache");
				p = (Package)cacheInstanceValue;
				currentPageId = (Guid)cachePageValue;
			}
			else
			{
				p = DataAccess.InterviewSystem.InstanceManagement.GetInstance(packageInstanceId, out currentPageId);
				// Add it to the cache
				if(context != null)
				{
					HttpContext.Current.Cache.Add(cacheInstanceKey, p, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 30, 0, 0), CacheItemPriority.Normal, null);
					HttpContext.Current.Cache.Add(cachePageKey, currentPageId, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 30, 0, 0), CacheItemPriority.Normal, null);
				}
			}

			using (MemoryStream ms = DataAccess.InterviewSystem.InstanceManagement.GetInstanceAnswerState(packageInstanceId))
			{
				if (ms.Length > 0)
				p.ApplyAnswerState(ms);
			}
			return p;
		}

        public static string GetInstanceReference(Guid packageInstanceId)
        {
            return DataAccess.InterviewSystem.InstanceManagement.GetInstanceReference(packageInstanceId);
        }

		public static void RenameInstance(Guid packageInstanceId, string newName)
		{
			DataAccess.InterviewSystem.InstanceManagement.RenameInstance(packageInstanceId, newName);
		}

		public static void DeleteInstance(Guid packageInstanceId)
		{
			DataAccess.InterviewSystem.InstanceManagement.DeleteInstance(packageInstanceId, Identity.GetRunningUserId());
		}

		public static TemplateData[] GetTemplateList(Guid instanceId)
		{
			return DataAccess.InterviewSystem.InstanceManagement.GetTemplateList(instanceId);
		}

		public static void SaveInstance(Guid packageInstanceId, Package instance)
		{
			if (instance.Interviews.Count > 0)
			{
				if (instance.Interviews[0].Reference != null)
				{
                    int version;
                    int revision;
                    InstanceManagement.GetVersion(packageInstanceId, out version, out revision);
                    StringBindingMetaData sbmd = new StringBindingMetaData(instance.Name, instance.InstanceReference, packageInstanceId, version, revision);
                    string strAnswer = instance.Interviews[0].Reference.Evaluate(AnswerProvider.Caller.Interview, sbmd);

                    if (strAnswer != null)
                    {
                        strAnswer = strAnswer.Trim();
                    }
                    else
                    {
                        strAnswer = instance.Name;
                    }

					if(instance.InstanceReference != strAnswer)
					{
						instance.InstanceReference = strAnswer;
						RenameInstance(packageInstanceId, strAnswer);
					}
				}
			}

			byte[] stateBytes;
			using (MemoryStream ms = instance.GetAnswerState())
			{
				ms.Seek(0, SeekOrigin.Begin);
				stateBytes = ms.ToArray();
			}
			DataAccess.InterviewSystem.InstanceManagement.SaveInstanceAnswerState(packageInstanceId, stateBytes);
			stateBytes = null;
			/*

			byte[] instanceBytes;

			// Serialise the package 

			using(MemoryStream ms = new MemoryStream())
			{

				// Persistence = do not serialise wordml (as we've already captured the templates collection)
				Package.SaveToStream(instance, ms, StreamingContextStates.Persistence);

				// Get the bytes of the package from the stream
				ms.Seek(0, SeekOrigin.Begin);
				instanceBytes = ms.ToArray();
			}

			DataAccess.InterviewSystem.InstanceManagement.SaveInstance(packageInstanceId, instanceBytes);
			instanceBytes = null;
			
			*/
		}

		public static Guid CreateInstance(Guid packageId, int version, int revision)
		{
			return DataAccess.InterviewSystem.InstanceManagement.CreateInstance(packageId, version, revision, Identity.GetRunningUserId());
		}

		public static Guid CreateInstance(Guid packageId, int version, int revision, string userIden)
		{
			return DataAccess.InterviewSystem.InstanceManagement.CreateInstance(packageId, version, revision, userIden);
		}

		public static void NavigationStackPeek(Guid packageInstanceId, out Guid pageId)
		{
			DataAccess.InterviewSystem.InstanceManagement.NavigationStackPeek(packageInstanceId, out pageId);
		}

		public static StackEntry[] NavigationStackGet(Guid packageInstanceId)
		{
			return DataAccess.InterviewSystem.InstanceManagement.NavigationStackGet(packageInstanceId);
		}

		public static StackEntry[] NavigationStackGetPendingDeletion(Guid packageInstanceId)
		{
			return DataAccess.InterviewSystem.InstanceManagement.NavigationStackGetPendingDeletion(packageInstanceId);
		}

		public static void NavigationStackPop(Guid packageInstanceId)
		{
			DataAccess.InterviewSystem.InstanceManagement.NavigationStackPop(packageInstanceId, false);
		}

        public static void NavigationStackPop(Guid packageInstanceId, bool forcePop)
        {
            DataAccess.InterviewSystem.InstanceManagement.NavigationStackPop(packageInstanceId, forcePop);
        }

		public static void NavigationStackFastForward(Guid packageInstanceId)
		{
			DataAccess.InterviewSystem.InstanceManagement.NavigationStackFastForward(packageInstanceId);
		}

		public static void NavigationStackPopBackToPoint(Guid packageInstanceId, int pointInStack)
		{
			DataAccess.InterviewSystem.InstanceManagement.NavigationStackPopBackToPoint(packageInstanceId, pointInStack);
		}

		public static void NavigationStackPush(Guid packageInstanceId, Guid pageId, string pageName)
		{
			DataAccess.InterviewSystem.InstanceManagement.NavigationStackPush(packageInstanceId, pageId, pageName);
		}

/*
		public static void StackPointer(Guid packageInstanceId, int position)
		{
			DataAccess.InterviewSystem.InstanceManagement.StackPointer(packageInstanceId, position);
		}

		public static void GetPointer(Guid packageInstanceId, out int position)
		{
			DataAccess.InterviewSystem.InstanceManagement.GetPointer(packageInstanceId, out position);
		}

*/
		public static void SavePreviewImage(Guid imageId, byte[] imageData)
		{
			DataAccess.InterviewSystem.InstanceManagement.SavePreviewImage(imageId, imageData);
		}

		public static MemoryStream GetPreviewImage(Guid imageId)
		{
			return DataAccess.InterviewSystem.InstanceManagement.GetPreviewImage(imageId);
		}

		public static void File_AddUpdate(Guid PackageInstanceId, int SequenceNumber, Guid QuestionUID, string OriginalFilePath, string ContentType, byte[] FileData)
		{
			DataAccess.InterviewSystem.InstanceManagement.File_AddUpdate(PackageInstanceId, SequenceNumber, QuestionUID, OriginalFilePath, ContentType, FileData);
		}


		public static void File_Delete(Guid PackageInstanceId, int SequenceNumber, Guid QuestionUID)
		{
			DataAccess.InterviewSystem.InstanceManagement.File_Delete(PackageInstanceId, SequenceNumber, QuestionUID);
		}

		public static byte[] File_GetBytes(Guid PackageInstanceId, int SequenceNumber, Guid QuestionUniqueIdentifier, out string OriginalFilePath, out string ContentType)
		{
			return DataAccess.InterviewSystem.InstanceManagement.File_GetBytes(PackageInstanceId, SequenceNumber, QuestionUniqueIdentifier, out OriginalFilePath, out ContentType);
		}

		public static void GetVersion(Guid PackageInstanceId, out int VersionNumber, out int RevisionNumber)
		{
			DataAccess.InterviewSystem.InstanceManagement.GetVersion(PackageInstanceId, out VersionNumber, out RevisionNumber);
		}

		public static void CopyMirrorFiles(Guid PackageInstanceId, Guid OldPackageInstanceId)
		{
			DataAccess.InterviewSystem.InstanceManagement.CopyMirrorFiles(PackageInstanceId, OldPackageInstanceId);
		}

        public static void EnQueuePackageInstance(Job job)
		{
			EnQueuePackageInstance(job, false);
		}

        public static void EnQueuePackageInstance(Job job, bool IsHighPriority)
		{
			try
			{
                DataAccess.InterviewSystem.InstanceManagement.EnQueuePackageInstance(job, IsHighPriority);
                if (job.PageToProcessId == Guid.Empty)
				    Status.SetInstanceStatus(job.InstanceId, Status.InterviewStatus.FINISHED);
                else
                    Status.SetInstanceStatus(job.InstanceId, Status.InterviewStatus.INTERVIEW);
			}
			catch (Exception ex)
			{
				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.InterviewSystem.Localisation").GetString("LogMessage"), ex, job.InstanceId, job.PackageId, job.VersionNumber, job.RevisionNumber, int.MinValue, Logging.ExceptionLogSeverityCode.ERROr, true);
				Status.SetInstanceStatus(job.InstanceId, Status.InterviewStatus.FAILED);
			}

		}
		
		public static void CopyPackageInstance(Guid newPackageInstanceId, Guid oldPackageInstanceId, string userIdent)
		{
			DataAccess.InterviewSystem.InstanceManagement.CopyPackageInstance(newPackageInstanceId, oldPackageInstanceId, userIdent);
		}

        public static void SetSynchronized(Guid packageInstanceId)
        {
            DataAccess.InterviewSystem.InstanceManagement.SetSynchronized(packageInstanceId);
        }

        public static void SetCompleted(Guid packageInstanceId)
        {
            DataAccess.InterviewSystem.InstanceManagement.SetCompleted(packageInstanceId);
        }

        public static void ResumeInstance(Guid packageInstanceId)
        {
            DataAccess.InterviewSystem.InstanceManagement.ResumeInstance(packageInstanceId);
        }

        /// <summary>
        ///     Gets the status information for a given package instance.
        /// </summary>
        /// <param name="instanceID">
        ///     The instance id of the given package whose status information is to be retrieved.
        /// </param>
        /// <returns>Xml string containing the status information for the given package instance.</returns>
        public static string GetInstanceStatusXml(Guid instanceID)
        {
            string instanceStatusXml = string.Empty;
            instanceStatusXml = DataAccess.InterviewSystem.InstanceManagement.GetInstanceStatusXml(instanceID);

            // Oracle XML is uppercase whereas MSSql is mixed case. We need one solution
            instanceStatusXml = instanceStatusXml.Replace("PACKAGEINSTANCEID", "PackageInstanceId")
                .Replace("PACKAGEID", "PackageId")
                .Replace("PACKAGEVERSIONNUMBER", "PackageVersionNumber")
                .Replace("PACKAGEREVISIONNUMBER", "PackageRevisionNumber")
                .Replace("REFERENCE", "Reference")
                .Replace("WHENCREATED", "WhenCreated")
                .Replace("WHENCOMPLETED", "WhenCompleted")
                .Replace("PARENTPACKAGENAME", "ParentPackageName")
                .Replace("CREATEDBYPERSONID", "CreatedByPersonId")
                .Replace("INTERVIEWSTATUSNAME", "InterviewStatusName")
                .Replace("INTERVIEWSTATUSRANK", "InterviewStatusRank")
                .Replace("INTERVIEWSTATE", "InterviewState")
                .Replace("TEMPLATENAME", "TemplateName")
                .Replace("LINKTEXT", "LinkText")
                .Replace("INTERNALREFERENCE", "InternalReference")
                .Replace("WHENLOGGEDUTC", "WhenLoggedUtc")
                .Replace("TEMPLATEID", "TemplateId");
            return instanceStatusXml;
        }
	}
}