using System.Reflection;
using System.Runtime.CompilerServices;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus Server Interview System")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs