using System;
using System.Globalization;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.PluginKit;
using Perfectus.Common;

namespace Perfectus.Server.InterviewSystem
{
	/// <summary>
	/// Summary description for Status.
	/// </summary>
	public sealed class Status
	{

		//public enum CoordinatorStatus {DONE, ERROr, PROGress, QUEUed, WARNing};
        public enum InterviewStatus {CREATED, INTERVIEW, SUSPENDED, FINISHED, FAILED, DONE};

		
		private Status()
		{
		}

		public static void SetInstanceStatus(Guid instanceId, InterviewStatus status)
		{
			string code = Enum.GetName(typeof(InterviewStatus), status).Substring(0, 4);
			DataAccess.InterviewSystem.InstanceManagement.SetInstanceStatus(instanceId, code);			
		}

        public static void SetDistributionResult(Guid instanceId, DistributionResult2 result)
        {
            DataAccess.InterviewSystem.InstanceManagement.SetDistributionResult(instanceId,
                result.DistributorId,
                result.TemplateId,
                result.TemplateName,
                result.Link,
                result.LinkText,
                result.InternalReference,
                result.DistributorName,
                result.MIMEFormat,
                result.JobQueueID,
                result.PageId,
                result.PageName);
        }
        public static void SetDistributionResult(Guid instanceId, DistributionResult result)
        {
            if (result is DistributionResult2)
                SetDistributionResult(instanceId, (DistributionResult2)result);
            else
                DataAccess.InterviewSystem.InstanceManagement.SetDistributionResult(instanceId, result.DistributorId, result.TemplateId, result.TemplateName, result.Link, result.LinkText, result.InternalReference);
        }

		public static void GetDistributorDetails(Guid distributorId, out string localisedName, out string iconFileName)
		{
			iconFileName = null;
			foreach(perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
			{
				if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "distributor" && plugin.uniqueIdentifier == distributorId)
				{
					localisedName = plugin.GetLocalisedName();
					if (plugin.iconFileName != null && plugin.iconFileName.Trim().Length > 0)
					{
						iconFileName = plugin.iconFileName;
					}
					return;
				}
			}
		
			localisedName = ResourceLoader.GetResourceManager("Perfectus.Server.InterviewSystem.Localisation").GetString("localisedName");
		}
		
	}
}
