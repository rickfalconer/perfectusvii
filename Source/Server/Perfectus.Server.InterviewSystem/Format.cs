using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.InterviewSystem
{
    public static class Format
    {
        public static string FormatForDisplay(object oAnswer, string formatString, string formatParameter, PerfectusDataType dataType, Package instance)
        {
            if (formatString.IndexOf("#formatplugin#") >= 0)
            {

                return FormatWithPlugin(
                        formatString.Replace("#formatplugin#", ""), oAnswer.ToString(), formatParameter, instance, null);
            }
            else
            {
                if (oAnswer is bool)
                {
                    return
                        ((bool) oAnswer)
                            ? ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                  GetString("Yes")
                            : ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                  GetString("No");
                }

                if (oAnswer is string && dataType == PerfectusDataType.Text)
                {
                    switch (formatString.ToLower())
                    {
                        case "lower":
                            return oAnswer.ToString().ToLower();
                        case "upper":
                            return oAnswer.ToString().ToUpper();
                        case "title":
                            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(oAnswer.ToString());
                        default:
                            return oAnswer.ToString();
                    }
                }

                double d;
                if (formatString == string.Empty || formatString.Length == 0)
                {
                    if (dataType == PerfectusDataType.Date)
                    {
                        try
                        {
                            return DateTime.Parse(oAnswer.ToString()).ToShortDateString();
                        }
                        catch
                        {
                            return oAnswer == null ? string.Empty : oAnswer.ToString();
                        }
                    }

                    if (oAnswer != null &&
                        double.TryParse(oAnswer.ToString(), NumberStyles.Any, Thread.CurrentThread.CurrentCulture.NumberFormat, out d))
                    {
                        // Use General format as default
                        return (double.Parse(oAnswer.ToString(), NumberStyles.Any, Thread.CurrentThread.CurrentCulture.NumberFormat)).ToString("G");
                    }
                }
                else
                {
                    if (dataType == PerfectusDataType.Date)
                    {
                        try
                        {
                            return DateTime.Parse(oAnswer.ToString()).ToString(formatString);
                        }
                        catch
                        {
                            return oAnswer == null ? string.Empty : oAnswer.ToString();
                        }
                    }

                    if (oAnswer != null &&
                        double.TryParse(oAnswer.ToString(), NumberStyles.Any, Thread.CurrentThread.CurrentCulture.NumberFormat, out d))
                    {
                        return
                            (double.Parse(oAnswer.ToString(), NumberStyles.Any, Thread.CurrentThread.CurrentCulture.NumberFormat)).
                                ToString
                                (formatString);
                    }
                }
            }

            //TODO: Do.
            return oAnswer == null ? string.Empty : oAnswer.ToString();
        }

        public static string FormatWithPlugin(string requestedPluginKey, string stringToFormat, string formatParams,
                                       Package package, ITemplateItem item)
        {
            string retval = stringToFormat;
            string key = null;

            try
            {
                if (package.FormatStringExtensionMapping != null)
                {
                    FormatStringExtensionInfo fsei;
                    fsei = package.FormatStringExtensionMapping[requestedPluginKey];
                    key = fsei.ServerSideFormatterPluginKey.ToLower(CultureInfo.InvariantCulture);
                }

                if (key != null)
                {
                    foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
                    {
                        if (IsPluginFormatStringExtension(key, plugin) || IsPluginFormatQuestionExtension(plugin))
                        {
                            return FormatValue(plugin, stringToFormat, formatParams, item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.InterviewSystem.Localisation").GetString("PluginKeyLoadError"), requestedPluginKey), ex);
            }

            return retval;
        }

        /// <summary>
        /// Determines whether the plugin is a format string extension.
        /// </summary>
        /// <param name="key">server side formatter plugin key</param>
        /// <param name="plugin">server side formatter</param>
        /// <returns>true if plugin is format string extension; false otherwise</returns>
        private static bool IsPluginFormatStringExtension(string key, perfectusServerConfigurationPluginsPlugin plugin)
        {
            return (plugin.type.ToLower(CultureInfo.InvariantCulture) == "formatstringextension" &&
                            plugin.path != null && plugin.path.Trim().Length > 0 &&
                            plugin.key.ToLower(CultureInfo.InvariantCulture) == key);
        }

        /// <summary>
        /// Determines whether the plugin is a format question extension.
        /// </summary>
        /// <param name="plugin">format string extension plugin</param>
        /// <returns>true if plugin is format question extension; false otherwise</returns>
        private static bool IsPluginFormatQuestionExtension(perfectusServerConfigurationPluginsPlugin plugin)
        {
            return plugin.path.ToLowerInvariant().Contains("FormatQuestionExtension.ItemListDisplay.dll".ToLowerInvariant());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plugin">format string extension plugin</param>
        /// <param name="valueString">value string</param>
        /// <param name="formatParameters">format parameters</param>
        /// <param name="item">item template</param>
        /// <returns>formatted value</returns>
        private static string FormatValue(perfectusServerConfigurationPluginsPlugin plugin, string valueString, string formatParameters, ITemplateItem item)
        {
            string retval = valueString;

            if (IsPluginFormatQuestionExtension(plugin))
            {
                Question question = (Question)item;

                IStringFormatter2 fp =
                    (IStringFormatter2)
                    Activator.CreateInstanceFrom(plugin.path, plugin.typeName, true,
                                                 BindingFlags.CreateInstance, null, new object[] { },
                                                 Thread.CurrentThread.CurrentUICulture, null, null).Unwrap();
                retval = fp.FormatQuestion(question, valueString, string.IsNullOrEmpty(formatParameters) ? string.Empty : formatParameters);
            }
            else
            {
                IStringFormatter fp = (IStringFormatter)Activator.CreateInstanceFrom(plugin.path,
                                                plugin.typeName,
                                                true,
                                                BindingFlags.CreateInstance, null, new object[] { },
                                                Thread.CurrentThread.CurrentUICulture, null, null).Unwrap();
                retval = fp.FormatString(valueString, string.IsNullOrEmpty(formatParameters) ? string.Empty : formatParameters);
            }

            return retval;
        }
        
        //public static string FormatWithPlugin(string requestedPluginKey, string stringToFormat, string formatParams,
        //                               Package package, ITemplateItem item)
        //{
        //    string retval = stringToFormat;
        //    string key = null;

        //    try
        //    {
        //        if (package.FormatStringExtensionMapping != null)
        //        {
        //            FormatStringExtensionInfo fsei;
        //            fsei = package.FormatStringExtensionMapping[requestedPluginKey];
        //            key = fsei.ServerSideFormatterPluginKey.ToLower(CultureInfo.InvariantCulture);
        //        }

        //        if (key != null)
        //        {
        //            foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
        //            {
        //                if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "formatstringextension" &&
        //                    plugin.path != null && plugin.path.Trim().Length > 0 &&
        //                    plugin.key.ToLower(CultureInfo.InvariantCulture) == key)
        //                {
        //                    IStringFormatter fp =
        //                        (IStringFormatter)
        //                        Activator.CreateInstanceFrom(plugin.path, plugin.typeName, true,
        //                                                     BindingFlags.CreateInstance, null, new object[] { },
        //                                                     Thread.CurrentThread.CurrentUICulture, null, null).Unwrap();

        //                    if (item is Question && fp is IStringFormatter2)
        //                        stringToFormat = ((IStringFormatter2)fp).FormatQuestion(item as Question, stringToFormat, formatParams == null ? string.Empty : formatParams);

        //                    retval = fp.FormatString(stringToFormat, formatParams == null ? string.Empty : formatParams);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.InterviewSystem.Localisation").GetString("PluginKeyLoadError"), requestedPluginKey), ex);
        //    }

        //    return retval;
        //}

    }
}
