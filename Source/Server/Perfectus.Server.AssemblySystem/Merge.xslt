<xsl:stylesheet xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
	              xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	              exclude-result-prefixes="xsl"
                xmlns:rel="http://schemas.openxmlformats.org/package/2006/relationships"
                xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:cus="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties"
                xmlns:ext="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
  xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
  xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"
  xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"
                version="1.0" >
  <!--original header element too many namespaces 
  <xsl:stylesheet 
  xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" 
  xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
  xmlns:v="urn:schemas-microsoft-com:vml" 
  xmlns:w10="urn:schemas-microsoft-com:office:word" 
  xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
  xmlns:aml="http://schemas.microsoft.com/aml/2001/core" 
  xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
  xmlns:o="urn:schemas-microsoft-com:office:office" 
  xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
  xmlns:st1="urn:schemas-microsoft-com:office:smarttags" 
  exclude-result-prefixes="xsl" 
  xmlns:rel="http://schemas.openxmlformats.org/package/2006/relationships"
  xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" 
  xmlns:dc="http://purl.org/dc/elements/1.1/" 
  xmlns:dcterms="http://purl.org/dc/terms/" 
  xmlns:dcmitype="http://purl.org/dc/dcmitype/" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:cus="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties" 
  xmlns:ext="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
  xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" 
  xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
  xmlns:perfectus="http://www.perfectus.net/adhoc" 
  version="1.0" >-->

  <!--good numbering props 
  xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" 
  xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
  xmlns:o="urn:schemas-microsoft-com:office:office" 
  xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" 
  xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" 
  xmlns:v="urn:schemas-microsoft-com:vml" 
  xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" 
  xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" 
  xmlns:w10="urn:schemas-microsoft-com:office:word" 
  xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" 
  xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" 
  xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" 
  xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" 
  xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" 
  xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" 
  mc:Ignorable="w14 wp14"-->

  <xsl:output method="xml" encoding="utf-8" />
  <xsl:preserve-space elements="allowspace"/>
  <!-- parameters -->
  <xsl:param name="sListDelimiter" select="', '" />
  <xsl:param name="iTabWidth" select="300" />
  <xsl:param name="bPreservePerfectusTags"/>
  <xsl:param name="idStartNumber" select="count(/root/pkg:package//@Id)" />
  <xsl:param name="countNumberingPart" select="count(/root/pkg:package/pkg:part/pkg:xmlData/w:numbering)" />
  <!-- global variables -->
  <xsl:variable name="sPara" select="'#PARA#'" />
  <xsl:variable name="sTab" select="'#TAB#'" />
  <xsl:variable name="sCR" select="'#CR#'" />
  <!-- pf-3037 word 2013 shows item guids in content controls, swap the tag and alias properties
      use w:document mc:Ignorable=\"w14 w15 wp14\"-->
  <!-- Gembox - tags no longer swapped -->
  <!--<xsl:variable name="Ignorable" select="string(//w:document/@mc:Ignorable)"/>
  <xsl:variable name="wordVer">
    <xsl:choose>
      <xsl:when test="contains($Ignorable, 'w15')">
        <xsl:value-of select ="15"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select ="12"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>-->

  <!-- keys -->
  <xsl:key name="xk_answer" match="root/answerSet/answer" use="@answeredItemId" />

  <!-- main entry point - add the pi that lets word know its a word document -->
  <xsl:template match="/">
    <xsl:processing-instruction name="mso-application">
      <xsl:text>progid="Word.Document"</xsl:text>
    </xsl:processing-instruction>
    <xsl:apply-templates select="root/pkg:package" />
  </xsl:template>

  <xsl:template match="root/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']">
    <!--pf-3083 lists in outcomes. suppress copying the main numbering part-->
  </xsl:template>

  <xsl:template match="root/pkg:package">
    <xsl:param name="iListLevel" select="/.." />
    <xsl:param name="iListDefinition" select="/.." />

    <xsl:if test="name() != 'w:sectPr' or (name() = 'w:sectPr' and count(ancestor::answerSet) = 0 )">
      <xsl:copy>

        <!--pf-3083 lists in outcomes: Ensure a numbering part is present
        This forcibly creates the numbering part: <pkg:part pkg:name="/word/numbering.xml"
        If the main numbering part is missing create it and attempt to merge numbering parts from answers.
        If a main numbering part is present fill it from ./w:numbering & ./w:num, and merge answers.
        -->
        <xsl:choose>
          <xsl:when test="$countNumberingPart=0">
            <!--pf-3120 remove this as it breaks outcomes-->
            <!--<pkg:part pkg:name="/word/numbering.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
              <pkg:xmlData>
                <w:numbering mc:Ignorable="w14 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
                  <xsl:apply-templates select="." mode="copyNumbering">
                  </xsl:apply-templates>
                </w:numbering>
              </pkg:xmlData>
            </pkg:part>-->
          </xsl:when>
          <xsl:otherwise>
            <pkg:part pkg:name="/word/numbering.xml" pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
              <pkg:xmlData>
                <w:numbering mc:Ignorable="w14 wp14" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape">
                  <xsl:apply-templates select="pkg:part/pkg:xmlData/w:numbering" mode="copyNumbering">
                  </xsl:apply-templates>
                </w:numbering>
              </pkg:xmlData>
            </pkg:part>
          </xsl:otherwise>
        </xsl:choose>

        <!--Uploaded images need to have their own part created-->
        <xsl:for-each select="/root/answerSet/answer[@image='true']" >
          <pkg:part pkg:contentType="image/jpeg" pkg:compression="store">
            <xsl:attribute name="pkg:name">
              <xsl:value-of select="concat('/word/media/',@answeredItemId)" />
            </xsl:attribute>
            <pkg:binaryData>
              <xsl:value-of select="." />
            </pkg:binaryData>
          </pkg:part>
        </xsl:for-each>

        <!--Outcome images need to have their own part created, and referenced.
        1 - Get part from answer, assign answereditemid as the name
        2 - Create new rel in /word/_rels/document.xml.rels referencing the image name (answereditemid)
        3 - Alter the picture xml to use the new rel# above
        Only part 1 is done here, parts 2 and 3 are elsewhere.  -->
        <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[contains(@pkg:contentType,'image/')]" >
          <xsl:variable name="answeredItemId" select="ancestor::answer/@answeredItemId" />
          <xsl:variable name="sPackageName" select="substring-after(@pkg:name,'/word/media/')"/>
          <xsl:element name="pkg:part">
            <xsl:attribute name="pkg:contentType">
              <xsl:value-of select="@pkg:contentType"/>
            </xsl:attribute>
            <xsl:attribute name="pkg:compression">store</xsl:attribute>
            <xsl:attribute name="pkg:name">
              <xsl:value-of select="concat('/word/media/', concat($answeredItemId, $sPackageName))"/>
            </xsl:attribute>
            <xsl:element name="pkg:binaryData">
              <xsl:value-of select="." />
            </xsl:element>
          </xsl:element>
        </xsl:for-each>

        <!--Uploaded documents need to have their own part created-->
        <!--rick 11 aug how to prevent outcomes dropping in here?? -->
        <!--<xsl:for-each select="/root/answerSet/answer/pkg:package">
          <pkg:part pkg:contentType="application/xml">
            <xsl:attribute name="pkg:name">/word/<xsl:value-of select="../@answeredItemId" /></xsl:attribute>
            <pkg:xmlData>
              <xsl:copy>
                <xsl:apply-templates select="* | @* | node()">
                  <xsl:with-param name="iListLevel" select="$iListLevel" />
                  <xsl:with-param name="iListDefinition" select="$iListDefinition" />
                </xsl:apply-templates>
              </xsl:copy>
            </pkg:xmlData>
          </pkg:part>
        </xsl:for-each>-->

        <xsl:apply-templates select="* | @* | node()">
          <xsl:with-param name="iListLevel" select="$iListLevel" />
          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
        </xsl:apply-templates>

      </xsl:copy>
    </xsl:if>

    <xsl:apply-templates  select="/.."  />
  </xsl:template>

  <!--pf-3083 lists in outcomes.
  - Outcomes have their own package parts. 
  - Do a full merge with unique numIds for each numbering set
  - Take the main numbering part, add all extras with new numIds based on answer//numbering position()*100+numId
  - When replacing the answers infer the abstractNumId and the numId using answer//numbering position()*100+numId
  -->
  <xsl:template match="*" mode="copyNumbering">

    <xsl:copy-of select="./w:abstractNum" />

    <xsl:choose >
      <xsl:when test="/root/answerSet/answer/pkg:package/pkg:part/pkg:xmlData/w:numbering">
        <!--assign each answer part an id (answer position * 100 + numId)-->
        <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']/pkg:xmlData/w:numbering">
          <xsl:variable name="answerBase" select="position()*100"></xsl:variable>
          <xsl:for-each select="w:abstractNum">
            <w:abstractNum w:abstractNumId="{$answerBase+@w:abstractNumId}">
              <xsl:copy-of select="./*" />
            </w:abstractNum>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:when>
    </xsl:choose>

    <xsl:copy-of select="./w:num"/>

    <xsl:choose >
      <xsl:when test="/root/answerSet/answer/pkg:package/pkg:part/pkg:xmlData/w:numbering">
        <!--assign each answer part an id (answer position * 100 + id value)-->
        <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']/pkg:xmlData/w:numbering">
          <xsl:variable name="answerBase" select="position()*100"></xsl:variable>
          <xsl:for-each select="w:num">
            <w:num w:numId="{$answerBase+@w:numId}">
              <w:abstractNumId w:val="{$answerBase+w:abstractNumId/@w:val}"/>
            </w:num>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:when>
    </xsl:choose>

    <!--any answers use word numbering? e.g. outcome with a list-->
    <!--<xsl:choose >

      <xsl:when test="/root/answerSet/answer/pkg:package/pkg:part/pkg:xmlData/w:numbering">

          -->
    <!--take the main w:numbering/w:abstractNum nodes, then take any extra fragments-->
    <!--
          <xsl:copy-of select="./w:abstractNum" />

          -->
    <!--assign each answer part an id (answer position * 100 + numId)-->
    <!--
          <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']/pkg:xmlData/w:numbering">
            <xsl:variable name="answerBase" select="position()*100"></xsl:variable>
            <xsl:for-each select="w:abstractNum">
              <w:abstractNum w:abstractNumId="{$answerBase+@w:abstractNumId}">
                <xsl:copy-of select="./*" />
              </w:abstractNum>
            </xsl:for-each>
          </xsl:for-each>

          -->
    <!--repeat the process for w:num, first the main section, then fragments-->
    <!--
          <xsl:copy-of select="./w:num"/>

          -->
    <!--assign each answer part an id (answer position * 100 + id value)-->
    <!--
          <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']/pkg:xmlData/w:numbering">
            <xsl:variable name="answerBase" select="position()*100"></xsl:variable>
            <xsl:for-each select="w:num">
              <w:num w:numId="{$answerBase+@w:numId}">
                <w:abstractNumId w:val="{$answerBase+w:abstractNumId/@w:val}"/>
              </w:num>
            </xsl:for-each>
          </xsl:for-each>

      </xsl:when>
      <xsl:otherwise>
        -->
    <!--there was no answer numbering, take the main numbering part as is-->
    <!--
        <xsl:copy-of select="pkg:part[@pkg:name='/word/numbering.xml']"/>
      </xsl:otherwise>

    </xsl:choose>-->
  </xsl:template>

  <xsl:template match="rel:Relationships[ancestor::pkg:part[@pkg:name='/word/_rels/document.xml.rels'] and count(ancestor::answerSet) = 0 ]" >
    <xsl:param name="iListLevel" select="/.." />
    <xsl:param name="iListDefinition" select="/.." />


    <xsl:if test="name() != 'w:sectPr' or (name() = 'w:sectPr' and count(ancestor::answerSet) = 0 )">
      <xsl:copy>

        <!-- Uploaded images need to be added to references-->
        <xsl:for-each select="/root/answerSet/answer[@image='true']" >
          <xsl:variable name="imageAnswerCount" select="count(preceding-sibling::answer) + 1" />
          <Relationship xmlns="http://schemas.openxmlformats.org/package/2006/relationships" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image">
            <xsl:attribute name="Id">
              <xsl:value-of select="concat('rId',$imageAnswerCount + $idStartNumber)" />
            </xsl:attribute>
            <xsl:attribute name="Target">
              <xsl:value-of select="concat('media/',@answeredItemId)" />
            </xsl:attribute>
          </Relationship>
        </xsl:for-each>

        <!--outcome images pt 2 - need to be added to references with unique rId and target [answeredItemId & pkg:name]-->
        <xsl:for-each select="/root/answerSet/answer/pkg:package/pkg:part[contains(@pkg:contentType,'image/')]" >
          <xsl:variable name="answeredItemId" select="ancestor::answer/@answeredItemId" />
          <xsl:variable name="outcomeImageName" select="substring-after(@pkg:name, '/word/media/')" />
          <xsl:variable name="uniqueImageName" select="concat($answeredItemId,$outcomeImageName)" />
          <Relationship xmlns="http://schemas.openxmlformats.org/package/2006/relationships" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/image">
            <xsl:attribute name="Id">
              <xsl:value-of select="concat('rId-',$uniqueImageName)" />
            </xsl:attribute>
            <xsl:attribute name="Target">
              <xsl:value-of select="concat('/word/media/',$uniqueImageName)" />
            </xsl:attribute>
          </Relationship>
        </xsl:for-each>

        <!-- Uploaded word files need to be added to references-->
        <xsl:for-each select="/root/answerSet/answer/pkg:package" >
          <xsl:variable name="imageAnswerCount" select="count(../preceding-sibling::answer) + 1" />
          <Relationship xmlns="http://schemas.openxmlformats.org/package/2006/relationships" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/aFChunk">
            <xsl:attribute name="Id">
              <xsl:value-of select="concat('rId',$imageAnswerCount + $idStartNumber)" />
            </xsl:attribute>
            <xsl:attribute name="Target">
              <xsl:value-of select="concat('/word/',../@answeredItemId)" />
            </xsl:attribute>
          </Relationship>
        </xsl:for-each>

        <xsl:apply-templates select="* | @* | node()">
          <xsl:with-param name="iListLevel" select="$iListLevel" />
          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
        </xsl:apply-templates>
      </xsl:copy>
    </xsl:if>
  </xsl:template>


  <!-- if no lists tag present, make one and fill it from the outcomes -->
  <!--  <xsl:template match = "root/w:wordDocument[count(w:lists) = 0]">-->
  <!--//TODO Aug 2014 change this to allow correct handling of new list formats-->
  <xsl:template match = "root/w:wordDocument[count(w:lists) = 0]">
    <xsl:copy>

      <xsl:attribute name="xml:space">preserve</xsl:attribute>

      <!-- Ensure the attributes are copied across if the setting is 'yes' -->
      <xsl:if test="count(self::node()[@w:macrosPresent='yes']) = 1">
        <xsl:attribute name="w:macrosPresent">yes</xsl:attribute>
      </xsl:if>

      <xsl:if test="count(self::node()[@w:embeddedObjPresent='yes']) = 1">
        <xsl:attribute name="w:embeddedObjPresent">yes</xsl:attribute>
      </xsl:if>

      <xsl:if test="count(self::node()[@w:ocxPresent='yes']) = 1">
        <xsl:attribute name="w:ocxPresent">yes</xsl:attribute>
      </xsl:if>

      <w:lists>
        <xsl:apply-templates select="w:listPicBullet | //answerSet/answer//w:lists/w:listPicBullet" />
        <xsl:apply-templates select="w:listDef | //answerSet/answer//w:lists/w:listDef" />
        <xsl:apply-templates select="w:list | //answerSet/answer//w:lists/w:list" />
      </w:lists>
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>


  <xsl:template match="w:lvlPicBulletId">
    <xsl:variable name="iListBulletId" select="100 * count(ancestor::answer | ancestor::answer/preceding-sibling::answer)" />
    <w:lvlPicBulletId w:val="{@w:val + $iListBulletId}"/>
  </xsl:template>


  <xsl:template match="w:lists/w:list">
    <xsl:variable name="iListDefId" select="100 * count(ancestor::answer | ancestor::answer/preceding-sibling::answer)" />
    <w:list w:ilfo="{@w:ilfo + $iListDefId}">
      <w:ilst w:val="{w:ilst/@w:val + $iListDefId}" />
    </w:list>
  </xsl:template>

  <!-- Document core properties starts -->
  <xsl:template match="cp:coreProperties">
    <cp:coreProperties  xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <xsl:for-each select="*">
        <xsl:element name="{name()}">
          <xsl:if test="@xsi:type">
            <xsl:attribute name="xsi:type">
              <xsl:value-of select="@xsi:type" />
            </xsl:attribute>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="contains(., 'answerTo')">
              <xsl:value-of select="/root/answerSet/answer[@answeredItemId = substring-after(current(), 'answerTo')]" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="." />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:element>
      </xsl:for-each>
    </cp:coreProperties>
  </xsl:template>

  <!-- Document core extended properties starts -->
  <xsl:template match="pkg:part/pkg:xmlData/ext:Properties">
    <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
      <xsl:for-each select="*">
        <xsl:element name="{name()}">
          <xsl:choose>
            <xsl:when test="contains(., 'answerTo')">
              <xsl:value-of select="/root/answerSet/answer[@answeredItemId = substring-after(current(), 'answerTo')]" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="." />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:element>
      </xsl:for-each>
    </Properties>
  </xsl:template>

  <!-- Document custom properties starts -->
  <xsl:template match="pkg:part/pkg:xmlData/cus:Properties">
    <Properties xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes" xmlns="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties">
      <xsl:for-each select="*">
        <xsl:element name="{name()}">
          <xsl:for-each select="@*">
            <xsl:attribute name="{name()}">
              <xsl:value-of select="." />
            </xsl:attribute>
          </xsl:for-each>
          <xsl:for-each select="*">
            <xsl:element name="{name()}">
              <xsl:choose>
                <xsl:when test="contains(., 'answerTo')">
                  <xsl:value-of select="/root/answerSet/answer[@answeredItemId = substring-after(current(), 'answerTo')]" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="." />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:element>
          </xsl:for-each>
        </xsl:element>
      </xsl:for-each>
    </Properties>
  </xsl:template>

  <!-- New table processing -->
  <!-- Matched a row containing one or more answer placeholders with a para separator (which means we should repeat rows for all subsequent answers -->
  <!--pf-3164 / pf-3172 repeater in table -->
  <!--<xsl:template match="w:tr[.//@w:xpath and key('xk_answer', .//@w:xpath)/@repeatSeparator='#PARA#']">-->
  <!--<xsl:template match="w:tr[//w:sdt[w:sdtPr] and key('xk_answer', //w:sdt/w:sdtPr/w:alias/@w:val)/@repeatSeparator='#PARA#']">-->
  <xsl:template match="w:tr">

    <xsl:variable name="xThisRow" select="." />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select="./*/*/*/w:sdtPr/w:tag/@w:val"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="./*/*/*/w:sdtPr/w:alias/@w:val"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select="./*/*/*/w:sdtPr/w:alias/@w:val" />

    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)" />

    <xsl:choose>
      <xsl:when test="$xInsert">

        <!--Find the most-answered question in the row - that's how many rows to make-->
        <xsl:variable name="sMaxAnswerSet">
          <xsl:call-template name="maxAnswerSet">
            <xsl:with-param name="xAnswers" select="$xInsert" />
          </xsl:call-template>
        </xsl:variable>
        <xsl:for-each select="key('xk_answer', $sMaxAnswerSet)">
          <xsl:apply-templates select="$xThisRow" mode="tableRow">
            <xsl:with-param name="iSequenceNumber" select="position()" />
          </xsl:apply-templates>
        </xsl:for-each>

      </xsl:when>
      <xsl:otherwise>

        <!--to take ordinary tr no answer-->
        <xsl:apply-templates select="$xThisRow" mode="tableRow">
          <xsl:with-param name="iSequenceNumber" select="position()" />
        </xsl:apply-templates>

      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>
  <!-- Tables for multiple answers end -->

  <!-- matched an answer placholder - need to select and insert the answer -->
  <xsl:template match="*/w:sdt[w:sdtPr/w:alias[@w:val]]">
    <!--<xsl:template match="*[@w:xpath]">-->
    <xsl:param name="iListLevel" select="/.." />
    <xsl:param name="iListDefinition" select="/.." />
    <xsl:param name="iSequenceNumber" select="-1" />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select="w:sdtPr/w:tag/@w:val"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="w:sdtPr/w:alias/@w:val"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select="w:sdtPr/w:alias/@w:val" />
    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)[@sequenceNumber = $iSequenceNumber or $iSequenceNumber = -1]" />

    <!-- Replace the tag so we can include info for the preview -->
    <xsl:if test="$bPreservePerfectusTags = 'true'">
      <xsl:text disable-output-escaping="yes"><![CDATA[<]]></xsl:text>
      <xsl:value-of select="name(.)"/> uid="<xsl:value-of select="@uid"/>"
      <xsl:text disable-output-escaping="yes"><![CDATA[>]]></xsl:text>
    </xsl:if>

    <xsl:choose>
      <xsl:when test="count($xInsert) = 0">
      </xsl:when>
      <!--pf 3084 watch this...orignal...keep or ditch?-->
      <xsl:when test="$xInsert[@isCondition='true']">
        <xsl:choose>
          <xsl:when test="$xInsert='true'">
            <xsl:apply-templates select="w:sdtContent/child::node()" />
          </xsl:when>
          <xsl:otherwise>
            <!--Special case for false conditions in a table cell: If the condition includes the p tag, and we're immediately inside a tablecell, we need to insert an empty para to keep Word happy-->
            <xsl:if test="name(..)='w:tc' and ./w:p">
              <w:p/>
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$xInsert[@image='true']">
        <xsl:apply-templates select="$xInsert" mode="image" />
      </xsl:when>

      <xsl:otherwise>
        <!-- pf-3120 docx outcomes cause double w:p sections. word does not like this. place a dummy run to overcome-->
        <w:r>
          <w:t/>
        </w:r>
        <xsl:choose>

          <!-- simple insert into a list with multiple answers seperated by CR's -->
          <!-- FB1102: A simple repeater in an outcome would only have 1 <w:p> element and hence we end up here
          We need to process the repeater together with the bullet list elements we've already processed. -->
          <!--pf-415 obselete <xsl:when test="../w:pPr/w:listPr and count($xInsert[@repeatSeparator=$sCR]) > 1">
            <xsl:apply-templates select="." mode="CRrepeat">
              <xsl:with-param name="iListLevel"/>
              <xsl:with-param name="iListDefinition"/>
              <xsl:with-param name="pPr" select="../w:pPr" />
              <xsl:with-param name="rPr" select="..//w:r/w:rPr" />
            </xsl:apply-templates>
          </xsl:when>-->

          <!-- 3158 copy runs from inline outcome, this ignores any content controls-->
          <xsl:when test="$xInsert//w:body//w:p">
            <xsl:apply-templates select="$xInsert" mode="copyBodyPara"></xsl:apply-templates>
          </xsl:when>

          <xsl:when test="$xInsert//w:body and count($xInsert//w:p) = 1">
            <xsl:choose>
              <xsl:when test="child::w:p[position()=1]">
                <w:p>
                  <xsl:copy-of select="child::w:p[position()=1]/w:pPr" />
                  <w:r>
                    <xsl:apply-templates select="$xInsert" mode="copyBodyPara"></xsl:apply-templates>
                  </w:r>
                </w:p>
              </xsl:when>
              <xsl:when test="parent::w:body">
                <xsl:variable name="outcomeAnswerCount" select="count($xInsert/preceding-sibling::answer) + 1" />
                <w:altChunk>
                  <xsl:attribute name="r:id">
                    <xsl:value-of select="concat('rId',$outcomeAnswerCount + $idStartNumber)" />
                  </xsl:attribute>
                </w:altChunk>
              </xsl:when>
              <xsl:otherwise>
                <w:r>
                  <xsl:apply-templates select="$xInsert" mode="copyBodyPara"></xsl:apply-templates>
                </w:r>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="$xInsert//w:body and (count($xInsert//w:p) > 1)">
            <xsl:apply-templates select="$xInsert" mode="copyBody"></xsl:apply-templates>
          </xsl:when>

          <!--pf-414 get format from content control-->
          <xsl:when test="count(w:sdtContent/w:r/w:rPr) = 1">
            <w:r>
              <xsl:apply-templates select ="w:sdtContent" mode="copyContentControlrPr"></xsl:apply-templates>
              <xsl:apply-templates select="$xInsert" mode="standardInsert">
                <xsl:sort data-type="number" select="@sequenceNumber" />
              </xsl:apply-templates>
            </w:r>
          </xsl:when>

          <!--pf-3084 rick 1 sept 2014 - probably don;t need this, test this breakpoint on simple outcomes-->
          <xsl:when test="count(w:sdtContent/w:p/w:r/w:rPr) = 1">
            <w:p>
              <w:r>
                <xsl:apply-templates select ="w:sdtContent" mode="copyContentControlrPr"></xsl:apply-templates>
                <xsl:apply-templates select="$xInsert" mode="standardInsert">
                  <xsl:sort data-type="number" select="@sequenceNumber" />
                </xsl:apply-templates>
              </w:r>
            </w:p>
          </xsl:when>

          <xsl:otherwise>
            <w:r>
              <xsl:copy-of select="w:r/w:rPr" />
              <xsl:apply-templates select="$xInsert" mode="standardInsert">
                <xsl:sort data-type="number" select="@sequenceNumber" />
              </xsl:apply-templates>
            </w:r>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>

    <!-- Close the new tag that we have added for the preview -->
    <xsl:if test="$bPreservePerfectusTags = 'true'">
      <xsl:text disable-output-escaping="yes"><![CDATA[</]]></xsl:text>
      <xsl:value-of select="name(.)"/>
      <xsl:text disable-output-escaping="yes"><![CDATA[ >]]></xsl:text>
    </xsl:if>
  </xsl:template>

  <!-- insert a base64 image -->
  <!--<xsl:template match="answer" mode="image">-->
  <!--<w:r>
			<w:pict>
				<v:shape id="{generate-id()}" style="width:{@width}px;height:{@height}px">
					<w:binData w:name="{@url}">
						<xsl:value-of select="." />
					</w:binData>
					<v:imagedata src="{@url}" o:title="{@title}" />
				</v:shape>
			</w:pict>
		</w:r>-->
  <!-- insert a base64 image -->
  <xsl:template match="answer" mode="image">
    <!--Count all ids so that our new images dont conflict-->
    <xsl:param name="imageAnswerCount" select="count(preceding-sibling::answer) + 1" />
    <!--Count previous images so that their ids dont conflict-->
    <xsl:variable name="width" select="ancestor::wp:extent/@cx" />
    <xsl:variable name="height" select="ancestor::wp:extent/@cy" />
    <w:r>
      <w:drawing xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
        <wp:inline xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing">
          <wp:extent cx="{@width}" cy="{@height}" />
          <wp:docPr name="image" id="1" />
          <wp:cNvGraphicFramePr>
            <a:graphicFrameLocks noChangeAspect="1" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" />
          </wp:cNvGraphicFramePr>
          <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
            <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
              <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                <pic:nvPicPr>
                  <pic:cNvPr id="1" name="image" />
                  <pic:cNvPicPr />
                </pic:nvPicPr>
                <pic:blipFill>
                  <a:blip xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
                    <xsl:attribute name="r:embed">
                      <xsl:value-of select="concat('rId',$idStartNumber + $imageAnswerCount)" />
                    </xsl:attribute>
                  </a:blip>
                  <a:stretch>
                    <a:fillRect />
                  </a:stretch>
                </pic:blipFill>
                <pic:spPr>
                  <a:xfrm>
                    <a:off x="0" y="0" />
                    <a:ext cx="{@width}" cy="{@height}" />
                  </a:xfrm>
                  <a:prstGeom prst="rect" />
                </pic:spPr>
              </pic:pic>
            </a:graphicData>
          </a:graphic>
        </wp:inline>
      </w:drawing>
    </w:r>
  </xsl:template>

  <!-- standard insert - needs to have the text elements added -->
  <xsl:template match="answer" mode="standardInsert">
    <xsl:param name="iAnswerCount" select="count(following-sibling::answer[@answeredItemId = current()/@answeredItemId]) + 1" />
    <w:t xml:space="preserve"><xsl:call-template name="substitute"><xsl:with-param name="string" select="node()" /></xsl:call-template><xsl:if test="$iAnswerCount > 1 and @repeatSeparator != $sTab and @repeatSeparator != $sCR and @repeatSeparator != $sPara"><xsl:value-of select="@repeatSeparator" /></xsl:if></w:t>
    <xsl:if test="$iAnswerCount > 1 and @repeatSeparator = $sTab">
      <w:tab wx:wTab="{$iTabWidth}" wx:tlc="none" />
    </xsl:if>
    <xsl:if test="$iAnswerCount > 1 and @repeatSeparator = $sCR">
      <w:br w:type="text-wrapping" />
    </xsl:if>
  </xsl:template>

  <!-- Process a paragraph containing an answer place holder  -->
  <!--<xsl:template match="w:p[*/@uid]">-->
  <xsl:template match="w:p[*/w:sdtPr/w:alias[@w:val]]">
    <xsl:param name="iListLevel" select="/.." />
    <xsl:param name="iListDefinition" select="/.." />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select="./*/w:sdtPr/w:tag/@w:val"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="./*/w:sdtPr/w:alias/@w:val"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select="./*/w:sdtPr/w:alias/@w:val" />
    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)" />

    <xsl:variable name="bRecursive" select="ancestor::answer" />

    <!--<xsl:variable name="_iListLevel" select="$iListLevel[$bRecursive] | w:pPr/w:listPr/w:ilvl/@w:val" />
    <xsl:variable name="_iListDefinition" select="$iListDefinition[$bRecursive] | w:pPr/w:listPr/w:ilfo/@w:val[not($bRecursive)]" />-->
    <!-- pf-415-->
    <xsl:variable name="_iListLevel" select="$iListLevel[$bRecursive] | w:pPr/w:numPr/w:ilvl/@w:val" />
    <xsl:variable name="_iListDefinition" select="$iListDefinition[$bRecursive] | w:pPr/w:numPr/w:numId/@w:val[not($bRecursive)]" />

    <xsl:choose>
      <xsl:when test="name(..) = 'w:tc' and (count($xInsert) = 0 or $xInsert='')">
        <w:p>
          <xsl:apply-templates select="./*"></xsl:apply-templates>
        </w:p>
      </xsl:when>
      <xsl:when test="(string($xInsert/node()) = '' or count($xInsert) = 0) and (count(./w:r) > 0 or count(./w:t) > 0)">
        <xsl:copy>
          <xsl:apply-templates select="* | @* | node()">
            <xsl:with-param name="iListLevel" select="$_iListLevel" />
            <xsl:with-param name="iListDefinition" select="$_iListDefinition" />
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:when>
      <xsl:when test="(string($xInsert/node()) = '' or count($xInsert) = 0) and (count(./w:r) = 0 and count(./w:t) = 0)">
      </xsl:when>
      <xsl:when test="string($xInsert/node()) != '' and count($xInsert) > 0">

        <xsl:choose>

          <!-- pf-3084 rick 1 sept 2014 be more selective with lists as this prevents listless outcomes...-->
          <!--<xsl:when test="$xInsert//w:body">-->
          <xsl:when test="$xInsert//w:body//w:pStyle[@w:val='ListParagraph']">
            <!-- pf-3083 Lists in Outcome: infer list def from position * 100 + numId-->
            <xsl:for-each select="$xInsert/pkg:package/pkg:part/pkg:xmlData/w:document/w:body/w:p">
              <xsl:variable name="answerPos" select="count(self::node()/../../../../../../preceding-sibling::answer[pkg:package/pkg:part/pkg:xmlData/w:numbering])+1"/>
              <xsl:apply-templates select="." mode="paragraph">
                <xsl:with-param name="iListLevel" select="w:pPr/w:numPr/w:ilvl/@w:val" />
                <xsl:with-param name="iListDefinition" select="$answerPos*100+w:pPr/w:numPr/w:numId/@w:val" />
              </xsl:apply-templates>
            </xsl:for-each>

            <!--pf-3083 lists in outcomes: drop in the outcome package, alt chunk does not work, breaks the word doc -->
            <!--<w:altChunk>
                  <xsl:attribute name="r:id">rId<xsl:value-of select="$outcomeAnswerCount + $idStartNumber" />
                  </xsl:attribute>
                  <pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage" pkg:name="/word/{$xInsertAnsweredId}" >
                    <xsl:for-each select="$xInsert/pkg:package/pkg:part">
                      <xsl:copy>
                        <xsl:apply-templates select="* | @* | node()">
                          <xsl:with-param name="iListLevel" select="$iListLevel" />
                          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
                        </xsl:apply-templates>
                      </xsl:copy>
                    </xsl:for-each>
                  </pkg:package>
                </w:altChunk>-->

          </xsl:when>
          <!-- test="$xInsert//w:body" -->

          <!-- Process the w:p as per normal -->
          <xsl:otherwise>
            <xsl:apply-templates select="." mode="paragraph">
              <xsl:with-param name="iListLevel" select="$_iListLevel" />
              <xsl:with-param name="iListDefinition" select="$_iListDefinition" />
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Process a paragraph ignoring embedded outcomes. -->
  <xsl:template match="w:p" mode="paragraph">
    <xsl:param name="iListLevel"/>
    <xsl:param name="iListDefinition"/>
    <xsl:param name="pPr" select="/.."/>
    <xsl:param name="rPr" select="/.." />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select="./*/w:sdtPr/w:tag/@w:val" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="./*/w:sdtPr/w:alias/@w:val" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select="./*/w:sdtPr/w:alias/@w:val" />
    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)" />

    <xsl:variable name="bRecursive" select="ancestor::answer" />

    <xsl:choose>

      <!-- pf-415 bulleted items in repeater seperated by CR's 
      - Each distinct list is defined in a w:pPr/w:pStyle[@w:val='ListParagraph'] node complete with rsid attrs
      - The answers need to be joined to the template definition w:p/w:sdt/w:sdtPr/w:alias[@w:val=xx] on the answeredItemId
      -->
      <xsl:when test="w:pPr/w:pStyle[@w:val='ListParagraph'] and count($xInsert[@repeatSeparator=$sCR]) > 0">
        <xsl:apply-templates select="." mode="PARArepeat">
          <xsl:with-param name="iListLevel" select="$iListLevel" />
          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
          <xsl:with-param name="pPr" select="w:pPr" />
          <xsl:with-param name="sdtContent" select="w:sdt/w:sdtContent" />
        </xsl:apply-templates>
      </xsl:when>

      <!-- simple insert into the doc body with multiple answers seperated by PARA's -->
      <xsl:when test="count($xInsert[@repeatSeparator=$sPara]) > 1 and count($xInsert[@image='true']) = 0">
        <xsl:for-each select="$xInsert">
          <w:p>
            <w:r>
              <w:t>
                <xsl:value-of select="." />
              </w:t>
            </w:r>
          </w:p>
        </xsl:for-each>
      </xsl:when>

      <!-- simple insert into a paragraph of some description -->
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="* | @* | node()">
            <xsl:with-param name="iListLevel" select="$iListLevel" />
            <xsl:with-param name="iListDefinition" select="$iListDefinition" />
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- pf-415 repeater - ooxml requires each list item in a w:p paragraph -->
  <xsl:template match="*" mode="PARArepeat">
    <xsl:param name="iListLevel" />
    <xsl:param name="iListDefinition"/>
    <xsl:param name="pPr" />
    <xsl:param name="sdtContent"/>

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select=".//w:sdtPr/w:tag/@w:val" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select=".//w:sdtPr/w:alias/@w:val" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select=".//w:sdtPr/w:alias/@w:val" />
    <xsl:variable name="xInsert" select="/root/answerSet/answer[@answeredItemId=$keyobj]" />
    <xsl:variable name="xParent" select="ancestor::answer" />
    <xsl:variable name="styleId" select="/root/pkg:package/pkg:part/pkg:xmlData/w:styles/w:style[@w:styleId='ListParagraph']/w:rsid/@w:val" />

    <xsl:for-each select="$xInsert">
      <!--<w:p w:rsidR="{$styleId}" w:rsidRDefault="{$styleId}" w:rsidP="{$styleId}">-->
      <w:p>
        <xsl:apply-templates select="$pPr" mode="copyListPpr">
          <xsl:with-param name="iListLevel" select="$iListLevel" />
          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
        </xsl:apply-templates>
        <w:r>
          <xsl:apply-templates select ="$sdtContent" mode="copyContentControlrPr"></xsl:apply-templates>
          <w:t>
            <xsl:value-of select="." />
          </w:t>
        </w:r>
      </w:p>
    </xsl:for-each>
  </xsl:template>

  <!-- copy the body of the answer set - pass the current list level and format down -->
  <xsl:template match="answer" mode="copyBody">
    <xsl:param name="iListLevel" />
    <xsl:param name="iListDefinition" />
    <xsl:param name="pNode" />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select=".//w:sdtPr/w:tag/@w:val" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select=".//w:sdtPr/w:alias/@w:val" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select=".//w:sdtPr/w:alias/@w:val" />
    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)" />

    <xsl:choose>
      <!-- Check whether this is a complex outcome within its own paragraph, or a simple inline outcome. -->
      <xsl:when test="(count(.//w:p) > 1) or (.//w:tbl) or ((count(.//w:p) = 1) and (.//w:p/w:pPr/w:listPr) and (count($xInsert[@repeatSeparator=$sCR]) > 1))">
        <xsl:choose>
          <!-- Word 2007 on longer includes the wx:sect element -->
          <xsl:when test=".//w:body//wx:sect">
            <xsl:apply-templates select=".//w:body//wx:sect/*[name() != 'w:sectPr']">
              <xsl:with-param name="iListLevel" select="$iListLevel" />
              <xsl:with-param name="iListDefinition" select="$iListDefinition" />
            </xsl:apply-templates>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select=".//w:body/*[name() != 'w:sectPr']">
              <xsl:with-param name="iListLevel" select="$iListLevel" />
              <xsl:with-param name="iListDefinition" select="$iListDefinition" />
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <xsl:otherwise>
        <!-- Ignore -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- copy the first para of the answer set - pass the current list level and format down -->
  <xsl:template match="answer" mode="copyBodyPara">
    <!--pf-3195 paras are missing - recreate each para and insert runs
    <xsl:apply-templates select=".//w:body//w:p/*"></xsl:apply-templates>-->
    <xsl:for-each select=".//w:body//w:p">
      <!--<w:p> pf-3278 remove para
        <xsl:apply-templates select="w:r"></xsl:apply-templates>
      </w:p>-->
      <xsl:apply-templates select="w:r"></xsl:apply-templates>
    </xsl:for-each>
  </xsl:template>

  <!-- find the higest sequence number in the answersets -->
  <xsl:template name="maxAnswerSet">
    <xsl:param name="xAnswers" />
    <xsl:variable name="xCurrent" select="$xAnswers[1]" />
    <xsl:variable name="xBigger" select="$xAnswers[@sequenceNumber > $xCurrent/@sequenceNumber]" />
    <xsl:choose>
      <xsl:when test="$xBigger">
        <xsl:call-template name="maxAnswerSet">
          <xsl:with-param name="xAnswers" select="$xBigger" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$xCurrent/@answeredItemId" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- copy all the nodes passing down the sequence number -->
  <xsl:template match="* | @* | node()" mode="tableRow">
    <xsl:param name="iSequenceNumber" />
    <xsl:copy>
      <xsl:apply-templates select="* | @* | node()" mode="tableRow">
        <xsl:with-param name="iSequenceNumber" select="$iSequenceNumber" />
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <!-- insert the answer with the given sequence number -->
  <!--pf-3164 repeater in table -->
  <!--<xsl:template match="*[@w:xpath]" mode="tableRow">-->
  <xsl:template match="//w:sdt[w:sdtPr/w:alias[@w:val]]" mode="tableRow">
    <xsl:param name="iSequenceNumber" />

    <!-- Gembox - tags no longer swapped -->
    <!--<xsl:variable name="keyobj">
      <xsl:choose>
        <xsl:when test="$wordVer > 14">
          <xsl:value-of select="w:sdtPr/w:tag/@w:val"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="w:sdtPr/w:alias/@w:val"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>-->
    <xsl:variable name="keyobj" select="w:sdtPr/w:alias/@w:val"/>
    <xsl:variable name="xInsert" select="key('xk_answer', $keyobj)" />

    <xsl:apply-templates select="key('xk_answer', $xInsert)[@sequenceNumber = $iSequenceNumber]" mode="standardInsert">
      <xsl:with-param name="iAnswerCount" select="1" />
    </xsl:apply-templates>
    <xsl:apply-templates select=".">
      <xsl:with-param name="iSequenceNumber" select="$iSequenceNumber" />
    </xsl:apply-templates>
  </xsl:template>

  <!-- matched a list level identifier - need to adjust it for the current list -->
  <!--pf-415 replace <xsl:template match="w:listPr/w:ilvl/@w:val">-->
  <xsl:template match="w:numPr/w:ilvl/@w:val">
    <xsl:param name="iListLevel"  select="/.." />
    <xsl:attribute name="w:val">
      <xsl:choose>
        <xsl:when test="$iListLevel">
          <xsl:value-of select="sum(. | $iListLevel)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="." />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <!--outcome image pt 3 - replace blip r:embed with rId of the image in the main doc
  i - get original outcome r:embed rId
  ii - lookup outcome relationship target for rId
  iii - construct unique rId for main doc using answeredItemID & image name   -->
  <xsl:template match ="//answer//w:drawing">
    <xsl:variable name="answeredItemId" select="ancestor::answer/@answeredItemId" />
    <xsl:variable name="outcomeRelId" select=".//a:blip/@r:embed"/>
    <xsl:variable name="outcomeRelTarget" select="ancestor::pkg:package/pkg:part[@pkg:name='/word/_rels/document.xml.rels']/pkg:xmlData/*/*[@Id=$outcomeRelId]/@Target" />
    <xsl:variable name="outcomeImageName" select="substring-after($outcomeRelTarget, '/')" />
    <xsl:variable name="newRelId" select="concat('rId-',concat($answeredItemId,$outcomeImageName))" />
    <xsl:variable name="width" select="wp:inline/wp:extent/@cx" />
    <xsl:variable name="height" select="wp:inline/wp:extent/@cy" />
    <!--Count previous images so that their ids dont conflict-->
    <w:drawing xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main">
      <wp:inline xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing">
        <wp:extent cx="{$width}" cy="{$height}" />
        <wp:docPr name="image" id="1" />
        <wp:cNvGraphicFramePr>
          <a:graphicFrameLocks noChangeAspect="1" xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" />
        </wp:cNvGraphicFramePr>
        <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
          <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
            <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
              <pic:nvPicPr>
                <pic:cNvPr id="1" name="image" />
                <pic:cNvPicPr />
              </pic:nvPicPr>
              <pic:blipFill>
                <a:blip xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">
                  <xsl:attribute name="r:embed">
                    <xsl:value-of select="$newRelId"/>
                  </xsl:attribute>
                </a:blip>
                <a:stretch>
                  <a:fillRect />
                </a:stretch>
              </pic:blipFill>
              <pic:spPr>
                <a:xfrm>
                  <a:off x="0" y="0" />
                  <a:ext cx="{$width}" cy="{$height}" />
                </a:xfrm>
                <a:prstGeom prst="rect" />
              </pic:spPr>
            </pic:pic>
          </a:graphicData>
        </a:graphic>
      </wp:inline>
    </w:drawing>
  </xsl:template>

  <!-- tags we don't ever want -->
  <xsl:template match="w:proofErr">
    <!-- suppress -->
  </xsl:template>

  <!-- matches every other tag and copies them to the output - passes through state information for lists -->
  <xsl:template match="* | @* | node()">
    <xsl:param name="iListLevel" select="/.." />
    <xsl:param name="iListDefinition" select="/.." />
    <xsl:if test="name() != 'w:sectPr' or (name() = 'w:sectPr' and count(ancestor::answerSet) = 0 )">
      <xsl:copy>
        <xsl:apply-templates select="* | @* | node()">
          <xsl:with-param name="iListLevel" select="$iListLevel" />
          <xsl:with-param name="iListDefinition" select="$iListDefinition" />
        </xsl:apply-templates>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <!-- Replaces CRLF with <w:br> etc. -->
  <xsl:template name="substitute">
    <xsl:param name="string" />
    <xsl:param name="from" select="'&#xA;'" />
    <xsl:param name="to">
      <w:br />
    </xsl:param>
    <xsl:choose>
      <xsl:when test="contains($string, $from)">
        <xsl:value-of select="substring-before($string, $from)" />
        <xsl:copy-of select="$to" />
        <xsl:call-template name="substitute">
          <xsl:with-param name="string" select="substring-after($string, $from)" />
          <xsl:with-param name="from" select="$from" />
          <xsl:with-param name="to" select="$to" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="w:pPr" mode="copyListPpr">
    <xsl:param name="iListLevel" />
    <xsl:param name="iListDefinition"/>
    <xsl:copy>
      <xsl:apply-templates select="* | @* | node()" mode="copyListPpr">
        <xsl:with-param name="iListLevel" select="$iListLevel" />
        <xsl:with-param name="iListDefinition" select="$iListDefinition" />
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="* | @* | node()" mode="copyListPpr">
    <xsl:param name="iListLevel" />
    <xsl:param name="iListDefinition"/>
    <xsl:choose>
      <xsl:when test="name()='w:ilvl'">
        <w:ilvl w:val="{$iListLevel}" />
      </xsl:when>
      <xsl:when test="name()='w:ilfo'">
        <w:ilfo w:val="{$iListDefinition}" />
      </xsl:when>
      <!--pf-415 ilfo is obselete, use numId-->
      <xsl:when test="name()='w:numId'">
        <w:numId w:val="{$iListDefinition}" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="* | @* | node()" mode="copyListPpr">
            <xsl:with-param name="iListLevel" select="$iListLevel" />
            <xsl:with-param name="iListDefinition" select="$iListDefinition" />
          </xsl:apply-templates>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="*" mode="copyContentControlrPr">
    <xsl:apply-templates select="w:r/w:rPr"></xsl:apply-templates>
  </xsl:template>

</xsl:stylesheet>