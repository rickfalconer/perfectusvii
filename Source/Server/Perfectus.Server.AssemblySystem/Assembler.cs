using System;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Common;
using System.Text;
using System.Xml.Linq;
using GemBox.Document;
using System.Text.RegularExpressions;
using Perfectus.Common.PackageObjects;
using System.IO.Packaging;
using System.Linq;
using Perfectus.Common;

namespace Perfectus.Server.AssemblySystem
{
    /// <summary>
    /// Summary description for Assembler.
    /// </summary>
    public class Assembler
    {

        private XmlReader oTemplate;
        private XmlReader oAnswerSet;
        private StreamWriter oWriter;
        private SortedList oParameters;
        private static XslTransform oTransform = new XslTransform();

        private class AnswerDocument
        {
            public string answerName;
            public string answeredItemId;
            public bool isCondition;
            public string answerValue;
            public MemoryStream docBefore;
            public int mergedCount;
            public MemoryStream docAfter;
        }

        static Assembler()
        {
            LoadTransform();
        }

        private static void LoadTransform()
        {
            XmlDocument tempDoc = new XmlDocument();

            tempDoc.Load(Config.Shared.assembly.mergeXsltPath);

            oTransform.Load(tempDoc.CreateNavigator(), new XmlUrlResolver(), typeof(Assembler).Assembly.Evidence);
            tempDoc = null;
        }

        // Added to support assembly unit testing
        public void LoadTransform(string transformPath)
        {
            XmlDocument tempDoc = new XmlDocument();
            tempDoc.Load(transformPath);
            oTransform.Load(tempDoc.CreateNavigator(), new XmlUrlResolver(), typeof(Assembler).Assembly.Evidence);
            tempDoc = null;
        }

        public Assembler()
        {
            // create private objects
            oParameters = new SortedList();
        }

        // properties
        public XmlReader Template
        {
            set { oTemplate = value; }
            get { return oTemplate; }
        }

        public XmlReader AnswerSet
        {
            set { oAnswerSet = value; }
            get { return oAnswerSet; }
        }

        public StreamWriter Output
        {
            set { oWriter = value; }
            get { return oWriter; }
        }

        public object this[string sParameterName]
        {
            set
            {
                if (oParameters.Contains(sParameterName))
                    oParameters[sParameterName] = value;
                else
                    oParameters.Add(sParameterName, value);
            }
            get
            {
                return oParameters[sParameterName];
            }
        }


        // clear the parameters
        public void ClearParameters()
        {
            oParameters.Clear();
        }


        // excute the transform
        private void Execute()
        {
            // make sure template has been set
            if (oTemplate == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("TemplateNotSet."));

            // make sure answer set has been set
            if (oAnswerSet == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("AnswerSetNotSet."));

            // make sure output has been set
            if (oWriter == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("OutputSetNotSet."));

            // create local objects
            ExtendedReader oReader = new ExtendedReader();
            XmlDocument oSource = new XmlDocument();
            XsltArgumentList oArgs = new XsltArgumentList();
            XmlResolver oResolver = new XmlUrlResolver();

            try
            {
                // add template and answer set to uber reader
                oReader.Add(oTemplate);
                oReader.Add(oAnswerSet);

                // load the xml reader into a document
                oSource.LoadXml(oReader.ReadOuterXml());

                // add arguments
                foreach (object oParamName in oParameters.Keys)
                {
                    string sParamName = (String)oParamName;
                    oArgs.AddParam(sParamName, "", oParameters[sParamName]);
                }

                // execute the transform
                oTransform.Transform(oSource, oArgs, oWriter, oResolver);

            }
            finally
            {
                // clean up
                //oReader.Close();
            }
        }

        // excute the transform
        public void Execute(MemoryStream msTemplate, MemoryStream msAnswers, ref MemoryStream msOutput)
        {
            // make sure template has been set
            if (msTemplate == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("TemplateNotSet."));

            // make sure answer set has been set
            if (msAnswers == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("AnswerSetNotSet."));

            // make sure output has been set
            if (msOutput == null)
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AssemblySystem.Localisation").GetString("OutputSetNotSet."));

            // Gembox - pre process answer documents (outcomes/uploads) into template
            PreProcessAnswerDocuments(ref msTemplate, ref msAnswers);

            msAnswers.Seek(0, SeekOrigin.Begin);
            msAnswers.Position = 0;
            msTemplate.Seek(0, SeekOrigin.Begin);
            msTemplate.Position = 0;

            Template = new XmlTextReader(msTemplate);
            AnswerSet = new XmlTextReader(msAnswers);
            Output = new StreamWriter(msOutput, Encoding.UTF8);

            this.Execute();

            //msOutput.Seek(0, SeekOrigin.Begin);
            //msOutput.Position = 0;

            //XmlTextReader reader = new XmlTextReader(msOutput);
            //XDocument transformedDoc = XDocument.Load(reader);
            //MemoryStream docxOutput = new MemoryStream();
            //wt.FlatToOpc(transformedDoc, ref docxOutput);
        }

        public void ExecuteToXDocument(MemoryStream msTemplate, MemoryStream msAnswers, ref XDocument xdOutput)
        {
            MemoryStream msOutput = new MemoryStream();
            Execute(msTemplate, msAnswers, ref msOutput);
            msOutput.Seek(0, SeekOrigin.Begin);
            msOutput.Position = 0;
            XmlTextReader reader = new XmlTextReader(msOutput);
            xdOutput = XDocument.Load(reader);
        }

        public void ExecuteToDocx(MemoryStream msTemplate, MemoryStream msAnswers, ref MemoryStream docxOutput)
        {
            XDocument xdOutput = new XDocument();
            ExecuteToXDocument(msTemplate, msAnswers, ref xdOutput);
            WordTemplateDocument2 wt = new WordTemplateDocument2();
            wt.FlatToOpc(xdOutput, ref docxOutput);
        }

        public void ExecuteToHTML(MemoryStream msTemplate, MemoryStream msAnswers, ref string htmlOutput)
        {
            MemoryStream docxOutput = new MemoryStream();
            ExecuteToDocx(msTemplate, msAnswers, ref docxOutput);
            MemoryStream msHtml = new MemoryStream();
            Perfectus.Common.SharedLibrary.GemBoxAPI.ConvertWordToHtml(docxOutput, ref msHtml);
            msHtml.Seek(0, SeekOrigin.Begin);
            string sHtml = Encoding.UTF8.GetString(msHtml.ToArray());
            sHtml = sHtml.Replace("&#xa0;", string.Empty);
            htmlOutput = sHtml.Replace("&nbsp;", " ");
        }

        public void ExecuteToPDF(MemoryStream msTemplate, MemoryStream msAnswers, ref string fileName)
        {
            MemoryStream docxOutput = new MemoryStream();
            ExecuteToDocx(msTemplate, msAnswers, ref docxOutput);
            string tempFile = Path.GetTempFileName();
            string destinationFile = tempFile + ".pdf";
            Perfectus.Common.SharedLibrary.GemBoxAPI.ConvertWord(docxOutput, destinationFile);
            fileName = destinationFile;
        }

        public void ExecuteToPDF(MemoryStream msTemplate, MemoryStream msAnswers, ref MemoryStream msOutput)
        {
            string tempFile = string.Empty;
            ExecuteToPDF(msTemplate, msAnswers, ref tempFile);
            msOutput = new MemoryStream(File.ReadAllBytes(tempFile));
        }

        private void PreProcessAnswerDocuments(ref MemoryStream msTemplate, ref MemoryStream msAnswers)
        {
            // pre transform source contains entire docx as an answer, modified by the transform, styles broken.
            // same for outcomes. 
            // outcomes need transform to apply answers to enbedded questions, uploaded doc does not.... treat them the same as there is no way to tell them apart in the answerset.
            // new design - prior to the transform:
            // clone the template and answerset
            // extract each answer doc into an array containing fields
            // answer name
            // answeredItemId
            // old doc memorystream
            // merged flag
            // newdoc memorystream
            // scan the template looking for each content control that references an answer doc
            // � recursively scan the answer doc looking for nested answer docs
            // ---- merge the answer doc into new copy of parent, replacing the content control
            // � merge the new answer doc into the template, replacing the content control
            // at this point all docs have been merged up into the template
            // remove merged answers from the answerset
            // continue to perform the xslt transform on the new template/answerset data

            //note defect pf-3267 with outcome on its own line - breaks doc. extra w:p with several namespaces cause it. 
            //does it come from the answerset? hence the original outcome doc source

            // load array with answer docs
            System.Collections.ArrayList answerDocuments = ExtractAnswerDocuments(msAnswers);
            // load array with answer conditions
            System.Collections.ArrayList answerConditions = ExtractAnswerConditions(msAnswers);

            //process conditionals that exist in answer docs
            foreach (AnswerDocument ad in answerDocuments)
            {
                ad.docAfter = ReplaceConditionalControls(ad.docBefore, answerConditions);
            }
            //process conditionals that exist in the template
            msTemplate = ReplaceConditionalControls(msTemplate, answerConditions);

            //TODO recurse all answerdocs changing sdts for new content...
            //extract following three sections into recursive process

            // use OOXML to scan template looking for ref to answerid, swap entire content control with answerid as a placeholder
            XmlDocument xmldocTemplate = ReplaceTemplateContentControlWithAnswerId(msTemplate, answerDocuments);

            //done with this, reset it for output
            msTemplate = new MemoryStream();

            // create docx package from flat xml to avoid gb corrupt doc
            using (MemoryStream msTemplateOPC = ConvertFlatToOPC(xmldocTemplate))
            {

                // use gembox to replace uid refs with complete answerdoc
                using (MemoryStream msMergedTemplate = ReplaceTemplateAnswerIdWithAnswerDoc(answerDocuments, msTemplateOPC))
                {

                    // template needs to be flattened again
                    XDocument flatMergedTemplate = new XDocument();
                    flatMergedTemplate = OpcToFlat(msMergedTemplate);

                    // convert flat xdoc to stream
                    XmlWriterSettings xws = new XmlWriterSettings();
                    xws.OmitXmlDeclaration = true;
                    using (XmlWriter writer = XmlWriter.Create(msTemplate, xws))
                    {
                        flatMergedTemplate.Save(writer);
                    }

                }
            }
        }

        private System.Collections.ArrayList ExtractAnswerDocuments(MemoryStream msAnswers)
        {
            System.Collections.ArrayList answerDocuments = new System.Collections.ArrayList();
            XmlTextReader xtrAnswers = new XmlTextReader(msAnswers);
            XmlDocument xmlDocAnswers = new XmlDocument();
            xmlDocAnswers.Load(xtrAnswers);
            XmlNamespaceManager nsm = new XmlNamespaceManager(xmlDocAnswers.NameTable);
            nsm.AddNamespace("pkg", "http://schemas.microsoft.com/office/2006/xmlPackage");
            XmlNodeList xnl = xmlDocAnswers.SelectNodes("/answerSet/answer/pkg:package", nsm);
            foreach (XmlNode xn in xnl)
            {
                AnswerDocument ad = new AnswerDocument();
                ad.answeredItemId = xn.ParentNode.Attributes["answeredItemId"].Value;
                ad.answerName = xn.ParentNode.Attributes["name"].Value;
                //ad.isCondition = xn.ParentNode.Attributes["isCondition"].Value.ToLower() == "true";
                //ad.answerValue = xn.ParentNode.InnerText;
                ad.docBefore = new MemoryStream(Encoding.UTF8.GetBytes(xn.OuterXml));
                answerDocuments.Add(ad);
            }

            return answerDocuments;
        }

        private System.Collections.ArrayList ExtractAnswerConditions(MemoryStream msAnswers)
        {
            //ensure streams are ready
            msAnswers.Seek(0, SeekOrigin.Begin);
            msAnswers.Position = 0;

            System.Collections.ArrayList answerDocuments = new System.Collections.ArrayList();
            XmlTextReader xtrAnswers = new XmlTextReader(msAnswers);
            XmlDocument xmlDocAnswers = new XmlDocument();
            xmlDocAnswers.Load(xtrAnswers);
            XmlNodeList xnl = xmlDocAnswers.SelectNodes("/answerSet/answer[@isCondition='true']");
            foreach (XmlNode xn in xnl)
            {
                AnswerDocument ad = new AnswerDocument();
                ad.answeredItemId = xn.Attributes["answeredItemId"].Value;
                ad.answerName = xn.Attributes["name"].Value;
                ad.isCondition = true;
                ad.answerValue = xn.InnerText;
                answerDocuments.Add(ad);
            }

            return answerDocuments;
        }


        private MemoryStream ReplaceConditionalControls(MemoryStream ms, System.Collections.ArrayList answerConditions)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(ms);

            XmlNamespaceManager nsm = new XmlNamespaceManager(xmlDoc.NameTable);
            nsm.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            // in addition to conditional controls, check all remaining w:sdt for word 15 title-tag swap
            string matchSdtPr = "//w:sdt/w:sdtPr";
            XmlNodeList mySdtPrs = xmlDoc.SelectNodes(matchSdtPr, nsm);
            foreach (XmlNode mySdt in mySdtPrs)
            {
                if (mySdt != null)
                {
                    string title = mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value;
                    string tag = mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value;
                    if (IsGuid(tag))
                    {
                        mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value = tag;
                        mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value = title;
                    }
                }
            }

            foreach (AnswerDocument ad in answerConditions)
            {
                string match = string.Format("//w:sdt[w:sdtPr/w:alias[@w:val='{0}']]", ad.answeredItemId);
                XmlNodeList mySdts = xmlDoc.SelectNodes(match, nsm);
                foreach (XmlNode mySdt in mySdts)
                {
                    if (mySdt != null)
                    {
                        if (ad.answerValue.ToLower() == "true")
                        {
                            // keep the contents of the conditional sdt - clone them
                            XmlNodeList keepNodes = mySdt.SelectNodes("./w:sdtContent/*", nsm);
                            foreach (XmlNode xn in keepNodes)
                                mySdt.ParentNode.InsertBefore(xn, mySdt);
                        }
                        //remove the original sdt and its contents
                        mySdt.ParentNode.RemoveChild(mySdt);
                    }
                }
            }

            MemoryStream msOutput = new MemoryStream();
            xmlDoc.Save(msOutput);
            msOutput.Seek(0, SeekOrigin.Begin);
            msOutput.Position = 0;
            return msOutput;
        }


        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        private static bool IsGuid(string candidate)
        {
            if (candidate != null)
            {
                if (isGuid.IsMatch(candidate))
                {
                    return true;
                }
            }

            return false;
        }

        private XmlDocument ReplaceTemplateContentControlWithAnswerId(MemoryStream msTemplate, System.Collections.ArrayList answerDocuments)
        {
            // replace entire w:sdt element with the UID (gembox can't manipulate sdt, but can find and replace text)
            XmlDocument xmlDocTemplate = new XmlDocument();
            xmlDocTemplate.Load(msTemplate);

            XmlNamespaceManager nsm = new XmlNamespaceManager(xmlDocTemplate.NameTable);
            nsm.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            foreach (AnswerDocument ad in answerDocuments)
            {
                string match = string.Format("//w:sdt[w:sdtPr/w:alias[@w:val='{0}']]", ad.answeredItemId);
                XmlNodeList mySdts = xmlDocTemplate.SelectNodes(match, nsm);
                foreach (XmlNode mySdt in mySdts)
                {
                    if (mySdt != null)
                    {
                        // replace with a run containing the uid
                        XmlNode mySdtParent = mySdt.ParentNode;
                        XmlNode newnodeR = xmlDocTemplate.CreateNode(XmlNodeType.Element, "w", "r", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                        XmlNode newnodeT = xmlDocTemplate.CreateNode(XmlNodeType.Element, "w", "t", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                        newnodeT.InnerText = ad.answeredItemId;
                        newnodeR.AppendChild(newnodeT);
                        mySdtParent.InsertBefore(newnodeR, mySdt);
                        mySdtParent.RemoveChild(mySdt);
                    }
                }
            }
            return xmlDocTemplate;
        }

        private MemoryStream ConvertFlatToOPC(XmlDocument xmlDocFlatXML)
        {
            XDocument xDocFlatXml = XDocument.Parse(xmlDocFlatXML.OuterXml);
            MemoryStream msOutput = new MemoryStream();
            using (WordTemplateDocument2 wt = new WordTemplateDocument2())
            {
                wt.FlatToOpc(xDocFlatXml, ref msOutput);
                msOutput.Position = 0;
                byte[] outbytes = new byte[msOutput.Length];
                msOutput.Read(outbytes, 0, outbytes.Length);
            }
            return msOutput;
        }

        private MemoryStream ReplaceTemplateAnswerIdWithAnswerDoc(System.Collections.ArrayList answerDocuments, MemoryStream msTemplateGembox)
        {
            Perfectus.Common.SharedLibrary.GemBoxAPI gb = new Perfectus.Common.SharedLibrary.GemBoxAPI();

            DocumentModel dm = DocumentModel.Load(msTemplateGembox, new DocxLoadOptions() { PreserveUnsupportedFeatures = true });
            //DocumentModel dm = DocumentModel.Load("c:\\testing\\testgemboxsource - Copy.docx");

            foreach (AnswerDocument ad in answerDocuments)
            {
                // is this uid in our template?
                int findCount = dm.Content.Find(ad.answeredItemId).Count();

                if (findCount > 0)
                {
                    // replace content control with answer doc

                    // this find method only finds text, we are looking for the uid
                    //foreach (ContentRange cr in dm.Content.Find(ad.answeredItemId))

                    //iterate blocks as we need to manipulate at the block level and below
                    for (int iSec = 0; iSec < dm.Sections.Count(); iSec++)
                    {
                        // main body - blocks in the section
                        ReplaceBlocksAnswerIdWithAnswerDoc(dm, ad, dm.Sections[iSec].Blocks);

                        // special case for blocks in the sections headerfooter collection
                        foreach (HeaderFooter hf in dm.Sections[iSec].HeadersFooters)
                            ReplaceBlocksAnswerIdWithAnswerDoc(dm, ad, hf.Blocks);
                    }
                }

            }
            // gb doc to stream
            MemoryStream msMergedTemplate = new MemoryStream();
            GemBox.Document.SaveOptions savOpt = GemBox.Document.SaveOptions.DocxDefault;
            dm.Save(msMergedTemplate, savOpt);

            return msMergedTemplate;
        }

        private void ReplaceBlocksAnswerIdWithAnswerDoc(DocumentModel dm, AnswerDocument ad, BlockCollection bc)
        {
            bool[] DeleteBlock = new bool[1234];

            //todo this will run short as more are inserted
            for (int iblk = 0; iblk < bc.Count(); iblk++)
            {
                //skip blocks marked for deletion
                if (DeleteBlock[iblk])
                    continue;


                // tables - recurse each cell
                if (bc[iblk].ElementType == ElementType.Table)
                {
                    GemBox.Document.Tables.Table t = (GemBox.Document.Tables.Table)bc[iblk];
                    foreach (GemBox.Document.Tables.TableRow tr in t.Rows)
                    {
                        foreach (GemBox.Document.Tables.TableCell tc in tr.Cells)
                            ReplaceBlocksAnswerIdWithAnswerDoc(dm, ad, tc.Blocks);
                    }
                    continue;
                }

                bool bDeleteBlock = false;
                int iAnswerRun = 999999;
                int iRun = 0;
                int iClones = 0;
                Run[] clonedRun = new Run[bc[iblk].GetChildElements(true, ElementType.Run).Count()];

                //used to find the end of the insert, after it has been done, before we append postText
                ContentPosition lastcp = null;
                int insertedBlocks = 0;
                int insertedSections = 0;

                //save a copy of all run before insert wrecks the collection
                foreach (Run run in bc[iblk].GetChildElements(false, ElementType.Run))
                    clonedRun[iClones++] = run.Clone();


                foreach (Run run in bc[iblk].GetChildElements(false, ElementType.Run))
                {
                    if (run.Text.Contains(ad.answeredItemId))
                    {
                        iAnswerRun = iRun;

                        int ccStart = 0, ccEnd = 0;
                        string preText = string.Empty;
                        string postText = string.Empty;

                        // this pre and post text will need to be grafted into the merged run(s)
                        ccStart = run.Text.IndexOf(ad.answeredItemId);
                        ccEnd = ccStart;
                        if (ccStart > 0)
                        {
                            preText = run.Text.Substring(0, ccStart);
                        }
                        ccEnd = run.Text.Length - (ccStart + ad.answeredItemId.Length);
                        if (ccEnd > 0)
                            postText = run.Text.Substring(ccStart + ad.answeredItemId.Length, ccEnd);

                        //used to find the end of the insert, after it has been done, before we append postText
                        lastcp = null;

                        //used to insert into current position
                        ContentPosition cp = run.Content.End;

                        // import answerdoc
                        // create docx package from flat xml to avoid gb corrupt doc

                        // ensure streams are ready
                        ad.docBefore.Seek(0, SeekOrigin.Begin);
                        ad.docBefore.Position = 0;
                        ad.docAfter.Seek(0, SeekOrigin.Begin);
                        ad.docAfter.Position = 0;

                        XmlDocument xmlDocAnswerDoc = new XmlDocument();

                        xmlDocAnswerDoc.Load(ad.docAfter != null ? ad.docAfter : ad.docBefore);
                        using (MemoryStream msAnswerDoc = ConvertFlatToOPC(xmlDocAnswerDoc))
                        {

                            DocumentModel sourcedm = DocumentModel.Load(msAnswerDoc, new DocxLoadOptions() { PreserveUnsupportedFeatures = true });

                            int sectionNum = 1;

                            // move to cp - tested and working
                            foreach (Section sourceSection in sourcedm.Sections.Reverse())
                            {
                                Section destSection = dm.Import(sourceSection, true, false);
                                destSection.PageSetup.SectionStart = sourceSection.PageSetup.SectionStart;

                                if (sectionNum == 1)
                                {
                                    if (destSection.Blocks != null)
                                    {
                                        bool bFirst = true;

                                        //avoid unwanted section break by inserting 1st section blocks
                                        foreach (Block bb in destSection.Blocks.Reverse())
                                        {
                                            insertedBlocks++;
                                            if (bFirst)
                                            {
                                                lastcp = cp.InsertRange(bb.Content);
                                                bFirst = false;
                                            }
                                            else
                                                cp.InsertRange(bb.Content);
                                        }
                                    }
                                }
                                else
                                {
                                    lastcp = cp.InsertRange(destSection.Content);
                                    insertedSections++;
                                }

                                sectionNum++;
                            }
                        }

                        //add pre and post if present
                        if (ccStart > 0)
                        {
                            // new block has been inserted, current +1
                            Run myrun = run.Clone();
                            myrun.Text = preText;
                            if (bc[iblk].ElementType == ElementType.Paragraph)
                            {
                                Paragraph thisPara = (Paragraph)bc[iblk];
                                if (bc[iblk + 1].ElementType == ElementType.Paragraph)
                                {
                                    Paragraph p = (Paragraph)bc[iblk + 1];
                                    if (p != null)
                                    {
                                        if (thisPara.ListFormat != null)
                                            p.ListFormat = thisPara.ListFormat.Clone();
                                        p.Inlines.Insert(0, myrun);
                                    }
                                }
                            }
                        }
                        if (ccEnd > 0)
                        {
                            // new block will be last section, last block
                            Run myrun = run.Clone();
                            myrun.Text = postText;
                            if (lastcp.Parent.ElementType == ElementType.Paragraph)
                            {
                                Paragraph p = (Paragraph)lastcp.Parent;
                                if (p != null)
                                    p.Inlines.Add(myrun);
                            }
                        }

                        bDeleteBlock = true;
                        break;
                    }
                    iRun++;
                }

                //pf3287 copy runs from origin block up to the point containing the answer, and after the answer to the end
                if (bDeleteBlock)
                {
                    int i = 0;
                    for (i = 0; i < iClones; i++)
                    {
                        if (i < iAnswerRun)
                        {
                            if (bc[iblk + 1].ElementType == ElementType.Paragraph)
                            {
                                Paragraph pbefore = (Paragraph)bc[iblk + 1];
                                if (pbefore != null)
                                    pbefore.Inlines.Insert(i, clonedRun[i]);
                            }
                        }
                        if (i > iAnswerRun)
                        {
                            if (lastcp.Parent.ElementType == ElementType.Paragraph)
                            {
                                Paragraph pafter = lastcp != null ? (Paragraph)lastcp.Parent : null;
                                if (pafter != null)
                                {
                                    pafter.Inlines.Add(clonedRun[i]);
                                    //delete the block that has the post runs in it, insert pushed these out
                                    //pafter.inlines.last()
                                    if (insertedSections == 0)
                                        DeleteBlock[iblk + 1 + insertedBlocks] = true;
                                    //TODO mark section/block for deletion if sec>0
                                }
                            }
                        }
                    }
                }

                DeleteBlock[iblk] = bDeleteBlock;
            }
            // delete the blocks
            for (int iblk = bc.Count(); iblk >= 0; iblk--)
                if (DeleteBlock[iblk])
                    bc[iblk].Content.Delete();
        }

        static XDocument OpcToFlat(MemoryStream docxStream)
        {
            using (System.IO.Packaging.Package package = System.IO.Packaging.Package.Open(docxStream))
            {
                XNamespace pkg = "http://schemas.microsoft.com/office/2006/xmlPackage";

                XDeclaration declaration = new XDeclaration("1.0", "UTF-8", "yes");
                XDocument doc = new XDocument(
                    declaration,
                    new XProcessingInstruction("mso-application", "progid=\"Word.Document\""),
                    new XElement(pkg + "package",
                        new XAttribute(XNamespace.Xmlns + "pkg", pkg.ToString()),
                        package.GetParts().Select(part => GetContentsAsXml(part))
                    )
                );
                return doc;
            }
        }

        static XElement GetContentsAsXml(System.IO.Packaging.PackagePart part)
        {
            XNamespace pkg = "http://schemas.microsoft.com/office/2006/xmlPackage";

            if (part.ContentType.EndsWith("xml"))
            {
                using (Stream str = part.GetStream())
                using (StreamReader streamReader = new StreamReader(str))
                using (XmlReader xr = XmlReader.Create(streamReader))
                    return new XElement(pkg + "part",
                        new XAttribute(pkg + "name", part.Uri),
                        new XAttribute(pkg + "contentType", part.ContentType),
                        new XElement(pkg + "xmlData",
                            XElement.Load(xr)
                        )
                    );
            }
            else
            {
                using (Stream str = part.GetStream())
                using (BinaryReader binaryReader = new BinaryReader(str))
                {
                    int len = (int)binaryReader.BaseStream.Length;
                    byte[] byteArray = binaryReader.ReadBytes(len);
                    // the following expression creates the base64String, then chunks
                    // it to lines of 76 characters long
                    string base64String = (System.Convert.ToBase64String(byteArray))
                        .Select
                        (
                            (c, i) => new
                            {
                                Character = c,
                                Chunk = i / 76
                            }
                        )
                        .GroupBy(c => c.Chunk)
                        .Aggregate(
                            new StringBuilder(),
                            (s, i) =>
                                s.Append(
                                    i.Aggregate(
                                        new StringBuilder(),
                                        (seed, it) => seed.Append(it.Character),
                                        sb => sb.ToString()
                                    )
                                )
                                .Append(Environment.NewLine),
                            s => s.ToString()
                        );
                    return new XElement(pkg + "part",
                        new XAttribute(pkg + "name", part.Uri),
                        new XAttribute(pkg + "contentType", part.ContentType),
                        new XAttribute(pkg + "compression", "store"),
                        new XElement(pkg + "binaryData", base64String)
                    );
                }
            }
        }

    }

}
