using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Xml;
using System.Security.Policy;
using System.Net;

namespace Perfectus.Server.AssemblySystem
{
	public class ExtendedReader: System.Xml.XmlReader
	{
		private SortedList aReaders;
		private int iCurrentReader;
        private XmlUrlResolver resolver;
        private Evidence evidence;
        private XmlSecureResolver secureResolver;
        private XmlReaderSettings settings;
    
		public ExtendedReader()
		{
            resolver = new System.Xml.XmlUrlResolver();
            resolver.Credentials = CredentialCache.DefaultCredentials;
            evidence = typeof(Assembler).Assembly.Evidence;
            secureResolver = new XmlSecureResolver(resolver, evidence);
            settings = new XmlReaderSettings();
            settings.XmlResolver = secureResolver;
			aReaders = new SortedList();
			iCurrentReader = 0;
		}
    
		// bonus methods for adding readers to the collection
		public void Add(XmlReader oReader)
		{
			aReaders.Add(aReaders.Count+1, oReader);
		}
    
		public void Add(Stream oStream)
		{
			XmlReader oReader = XmlTextReader.Create(oStream, settings);
			this.Add(oReader);
		}
    
		public void Add(DataSet oDataSet)
		{
			XmlDataDocument xDocument = new XmlDataDocument(oDataSet);
			this.Add(new XmlNodeReader( xDocument.DocumentElement ));
		}
    
        //public void Add(ref string sDocument)
        //{
        //    if(sDocument != "")
        //    {
        //        XmlReader oReader = new XmlTextReader(sDocument, XmlNodeType.Document, null);
        //        this.Add(oReader);
        //    }
        //}
    

		// xml reader properties
		public override int AttributeCount
		{
			get
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.AttributeCount;
			}
		}
    
		public override string BaseURI
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.BaseURI;
				}
				else
					return String.Empty;
			}
		}
    
		public override int Depth
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.Depth + 1;
				}
				else
					return 0;
			}
		}
    
		public override bool EOF
		{
			get
			{
				return iCurrentReader > aReaders.Count;
			}
		}   

		public override bool HasAttributes
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.HasAttributes;
				}
				else
					return false;
			}
		}
    
		public override bool HasValue
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.HasValue;
				}
				else
					return false;
			}
		}
    
		public override bool IsDefault
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.IsDefault;
				}
				else
					return false;
			}
		}
    
		public override bool IsEmptyElement
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.IsEmptyElement;
				}
				else
					return false;
			}
		}
    
		public override string LocalName
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.LocalName;
				}
				else
					return "root";
        
			}
		}
    
		public override string Name
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.Name;
				}
				else
					return "root";
			}
		}

		public override string NamespaceURI
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.NamespaceURI;
				}
				else
					return null;
			}
		}
    
		public override XmlNameTable NameTable
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.NameTable;
				}
				else
					return null;
			}
		}
    
		public override XmlNodeType NodeType
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.NodeType;
				}
				else
					return XmlNodeType.Element;
			}
		}

		public override string Prefix
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.Prefix;
				}
				else
					return null;
			}
		}
    
		public override char QuoteChar
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.QuoteChar;
				}
				else
					return '"';
			}
		}
    
		public override ReadState ReadState
		{
			get
			{
				if(iCurrentReader < aReaders.Count)
					return ReadState.Interactive;
				else
					return ReadState.EndOfFile;
			}
		}
    
		public override string this[int i]
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader[i];
				}
				else
					return null;
			}
		}
    
		public override string this[string name, string namespaceURI]
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader[name, namespaceURI];
				}
				else
					return null;
			}
		}
    
		public override string this[string name]
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader[name];
				}
				else
					return null;
			}
		}

		public override string Value
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.Value;
				}
				else
					return null;
			}
		}

		public override string XmlLang
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.XmlLang;
				}
				else
					return null;
			}
		}
    
		public override XmlSpace XmlSpace
		{
			get
			{
				if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
				{
					XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
					return oReader.XmlSpace;
				}
				else
					return XmlSpace.Default;
			}
		}
    
    
    
		// methods
		public override void Close()
		{
			for(int iReader=1; iReader<=aReaders.Count; iReader++)
			{
				XmlReader oReader = (XmlReader) aReaders[iReader];
				oReader.Close();
			}
		}
    
		public override string GetAttribute(int i)
		{
			return this[i];
		}
    
		public override string GetAttribute(string name)
		{
			return this[name];
		}

		public override string GetAttribute(string name, string namespaceURI)
		{
			return this[name, namespaceURI];
		}
    
		public override string LookupNamespace(string prefix)
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.LookupNamespace(prefix);
			}
			else
				return null;
		}
    
		public override void MoveToAttribute(int i)
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				oReader.MoveToAttribute(i);
			}
		}
    
		public override bool MoveToAttribute(string name)
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.MoveToAttribute(name);
			}
			else
				return false;
		}
    
		public override bool MoveToAttribute(string name, string ns)
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.MoveToAttribute(name, ns);
			}
			else
				return false;
		}
    
		public override bool MoveToElement()
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.MoveToElement();
			}
			else
				return false;
		}
    
		public override bool MoveToFirstAttribute()
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.MoveToFirstAttribute();
			}
			else
				return false;
		}
    
		public override bool MoveToNextAttribute()
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.MoveToNextAttribute();
			}
			else
				return false;
		}
    

		// the guts and bolts
		public override bool Read()
		{
			bool bReturn = false;
			if(iCurrentReader == 0) iCurrentReader++;

			while(!bReturn & iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				bReturn = oReader.Read();
				// next reader if current one has ended
				if(!bReturn)
					iCurrentReader ++;
				else
					bReturn = !(this.NodeType == XmlNodeType.None | this.NodeType == XmlNodeType.XmlDeclaration);
			}
      
			if(iCurrentReader == aReaders.Count+1)
			{
				iCurrentReader ++;
				bReturn = true;
			}

			if(iCurrentReader > aReaders.Count+1)
				bReturn = false;
      
			return bReturn;
		}
    

    
		public override bool ReadAttributeValue()
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				return oReader.ReadAttributeValue();
			}
			else
				return false;
		}
    
		public override void ResolveEntity()
		{
			if(iCurrentReader > 0 & iCurrentReader <= aReaders.Count)
			{
				XmlReader oReader = (XmlReader) aReaders[iCurrentReader];
				oReader.ResolveEntity();
			}
		}
    
	}
}
