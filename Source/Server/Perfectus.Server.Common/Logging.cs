using System;
using System.Diagnostics;
using System.Reflection;

namespace Perfectus.Server.Common
{
	/// <summary>
	/// Summary description for Logging.
	/// </summary>
	public class Logging
	{
		public enum ExceptionLogSeverityCode { INFO, WARNing, ERROr};

		private Logging()
		{
		}

		public static void TraceWrite(string message, string source)
		{
		    string msg = string.Format("[{0} UTC ({1}) {2}]"+System.Environment.NewLine+"   {3}", 
                DateTime.UtcNow.ToString("s"),                              // sortable date ISO 8601
                System.Threading.Thread.CurrentThread.ManagedThreadId, 
                source, 
                message);
			Trace.WriteLine(msg);
		}

		public static int LogMessage(string friendlyMessage, Exception ex, Guid packageInstanceId, Guid packageId, int packageVersionNumber, int packageRevisionNumber, int parentExceptionId, ExceptionLogSeverityCode severity, bool SetCoordinatorStatus)
		{
			if (ex != null)
				// First chance:  Trace exception
			{
				TraceException(ex, 0);
			}

			try
			{
				if (SetCoordinatorStatus && packageInstanceId != Guid.Empty)
				{
					switch(severity)
					{
						case ExceptionLogSeverityCode.ERROr:
							DataAccess.InterviewSystem.InstanceManagement.SetInstanceStatus(packageInstanceId, "FAIL");
							break;
					}
					
				}
			}
			catch
			{
#if DEBUG
				throw;
#endif
			}

			DateTime utcNow = DateTime.UtcNow;
			int personId;
			Assembly callingAssembly;
			string callingAssemblyName;
			string callingAssemblyPath;
			string callingAssemblyVersion;

			//			// See if we can get the personId
			//			try
			//			{
			//				personId = Common.Person.LoggedinPersonIdGet();
			//			}
			//			catch
			//			{
			personId = int.MinValue;
			//			}

			try
			{
				callingAssembly = Assembly.GetCallingAssembly();
				callingAssemblyName = callingAssembly.GetName().Name;
				callingAssemblyPath = callingAssembly.CodeBase;
				callingAssemblyVersion = callingAssembly.GetName().Version.ToString(4);

			}
			catch
			{
				callingAssemblyName = null;
				callingAssemblyPath = null;
				callingAssemblyVersion = null;
			}

			string exMessage = null;
			string exStackTrace = null;

			if (ex != null)
			{
				exMessage = ex.Message;
				exStackTrace = ex.StackTrace;

			}

			string severityCode = Enum.GetName(typeof(ExceptionLogSeverityCode), severity).Substring(0, 4);
			
			int parentId = DataAccess.Logging.LogException(utcNow, severityCode, personId, packageId, packageVersionNumber, packageRevisionNumber, packageInstanceId, friendlyMessage, exMessage, exStackTrace, callingAssemblyName, callingAssemblyPath, callingAssemblyVersion, parentExceptionId);
			if (ex != null && ex.InnerException != null)
			{
				Logging.LogMessage(friendlyMessage, ex.InnerException, packageInstanceId, packageId, packageVersionNumber, packageRevisionNumber, parentId, severity, true);
			}
			return parentId;
		}

		public static int LogMessage(string friendlyMessage, Exception ex)
		{

			return Logging.LogMessage(friendlyMessage, ex, Guid.Empty, Guid.Empty, int.MinValue, int.MinValue, int.MinValue, ExceptionLogSeverityCode.ERROr, true);
		}


		private static void TraceException(Exception ex, int indent)
		{
			string indentChars = new String(' ', indent);
			Trace.WriteLine(string.Format("{0}*******Exception********", indentChars));

			Trace.WriteLine(string.Format("{0}{1}", indentChars, ex.Message));
			Trace.WriteLine(string.Format("{0}{1}", indentChars, ex.StackTrace));
			if (ex.InnerException != null)
			{
				TraceException(ex.InnerException, indent++);
			}

			Trace.WriteLine(string.Format("{0}*****End Exception******", indentChars));
		}


/*		public static ExceptionLogSet ExceptionLog_GetByPackageInstanceId(int PackageInstanceId)
		{
			return DataAccess.Logging.ExceptionLog_GetByPackageInstanceId(PackageInstanceId);
		}*/
	}
}
