using System.Reflection;
using System.Runtime.CompilerServices;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus Server Common Library")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs