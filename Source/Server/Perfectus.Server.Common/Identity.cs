using System;
using System.Web;

namespace Perfectus.Server.Common
{
	/// <summary>
	/// Summary description for Identity.
	/// </summary>
	public class Identity
	{
		private Identity()
		{
		}

		public static string GetRunningUserId()
		{
			if (HttpContext.Current != null)
			{
				return HttpContext.Current.User.Identity.Name;
			}
			else
			{
				return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
			}
		}
	}
}
