using System.ComponentModel;
using System.Diagnostics;

namespace Perfectus.Server.Submitters.FileAcceptor.Service
{
	public class FileAcceptorService : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		
        private static Perfectus.Server.Submitters.FileAcceptor.Engine.FileAcceptor _FileAcceptor;

		public FileAcceptorService()
		{
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the process
		static void Main()
		{
			System.ServiceProcess.ServiceBase[] ServicesToRun;
	
			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = new System.ServiceProcess.ServiceBase[] {new FileAcceptorService(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new FileAcceptorService() };

			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new Container();
			this.ServiceName = "Perfectus File Acceptor Service";
		}
		#endregion

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			_FileAcceptor = FileAcceptor.Engine.FileAcceptor.GetInstance();
			_FileAcceptor.Start();
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			Trace.WriteLine("File Acceptor service stopping...");
			if (_FileAcceptor != null)
			{
				_FileAcceptor.Stop();
			}

			foreach(TraceListener tl in Trace.Listeners)
			{
				try
				{
					Trace.WriteLine(string.Format("Closing trace listener: {0}", tl.Name));
					tl.Flush();
					tl.Close();
				}
				catch
				{
					Trace.WriteLine("Couldn't close listener.");
				}
			}
		}
	}
}
