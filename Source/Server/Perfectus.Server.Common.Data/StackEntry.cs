using System;
using System.ComponentModel;

namespace Perfectus.Server.Common.Data
{	
	[TypeConverter(typeof (StackEntryStringConverter))]
	public struct StackEntry
	{
		public string PageName;
		public int PositionInStack;
		public Guid PageId;
	}

	/// <summary>
	///		This TypeConverter is for use by the ViewState when serialising the StackEntry struct.
	///		ViewState is optimised for strings and other simple types, so this turns the struct into a delimited string, and back again.
	///		Saves around 300b per entry.
	/// </summary>
	public class StackEntryStringConverter : StringConverter
	{
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string v = value.ToString();
				string[] parts = v.Split((char)1);
				StackEntry se = new StackEntry();
				se.PageName = parts[0];
				se.PositionInStack = Convert.ToInt32(parts[1]);
				se.PageId = new Guid(parts[2]);
				return se;
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is StackEntry)
			{
				StackEntry se = (StackEntry)value;
				return string.Format("{0}{1}{2}{1}{3}", se.PageName, (char)1, se.PositionInStack, se.PageId);
			}
			else
			{
				return base.ConvertTo (context, culture, value, destinationType);
			}
		}
	}
}
