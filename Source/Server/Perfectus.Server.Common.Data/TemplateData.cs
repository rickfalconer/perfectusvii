using System;

namespace Perfectus.Server.Common.Data
{
	/// <summary>
	/// Summary description for TemplateData.
	/// </summary>
	public struct TemplateData
	{
		private string name;
		private Guid uniqueIdentifier;

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
			set { uniqueIdentifier = value; }
		}
	}
}
