using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Perfectus.Common.PackageObjects;
using System.Text;

namespace Perfectus.Server.Common.Data
{
    public delegate void StatusUpdateDelegate(JobStatus jobStatus, String message);

    /// <summary>
    /// Validity status of a job 
    /// </summary>
    public enum JobStatus
    {
        INVALID = -1,   // not used yet 
        QUEUE = 0,     // Job can be pcked
        ASSEMBLE,       // Job is assembling
        CONVERT,
        DISTRIBUTE,
        COMPLETE,       // Job is complete
        ERROR
    }         // Job is in a permanent error state (no rerun without Admin intervention)

    [Serializable]
    public class Job : ProcessingTask
    {
        private int retry;
        public int Retry { get { return retry;} set { retry = value;}}

        private string sourcename;
        public string SourceName { get { return sourcename; } set { sourcename = value; } }

        public String Label
        {
            get
            {
                StringBuilder tempStr = new StringBuilder(String.Format("Instance {0}", this.InstanceId));
                if (this.PageToProcessId != Guid.Empty)
                    tempStr.Append(String.Format(" Page {0}", this.PageToProcessId));
                tempStr.Append(String.Format(" of Package {0} {1}.{2}", this.PackageId, this.VersionNumber, this.RevisionNumber));
                if (retry > 0)
                    tempStr.Append(String.Format(" Retry {0}", retry));
                return tempStr.ToString();
            }
            set {
                int index;
                if (-1 != (index = value.LastIndexOf(" Retry ")))
                    if (Int32.TryParse(value.Substring(index + 7), out index))
                        Retry = index;
            }
        }

        public override string ToString()
        {
            return String.Format("Task for package {0} v{1}.{2}, instance {3} from {4}",
                base.PackageId, base.VersionNumber, base.RevisionNumber, base.InstanceId, base.TaskSubmittedBy);
        }

        public int IncrementRetry()
        {
            return ++retry;
        }
        /// <summary>
        /// return a clone of teh base class
        /// (have had a problem with MSMQ deserialisation)
        /// </summary>
        /// <returns></returns>
        public ProcessingTask Task()
        {
            return new ProcessingTask(
                base.PackageId,
                base.InstanceId,
                base.VersionNumber,
                base.RevisionNumber,
                base.TaskSubmittedBy,
                base.PageToProcessId);
        }

        private void Initialize()
        {
            retry = 0;
            int index;
            if (-1 != (index = Label.LastIndexOf(" Retry ")))
            {
                Int32.TryParse(Label.Substring(index + 7),out retry);
            }
            SourceName = string.Empty;
        }

        public Job(Guid packageId, Guid instanceId, int versionNumber, int revisionNumber, string taskSubmittedBy)
            : this(packageId, instanceId, versionNumber, revisionNumber, taskSubmittedBy, Guid.Empty)
        {
        }

        /// <summary>
        /// A processing task for per Page processing only.
        /// </summary>
        public Job(Guid packageId, Guid instanceId, int versionNumber, int revisionNumber, string taskSubmittedBy, Guid pageId)
            : base(packageId, instanceId, versionNumber, revisionNumber, taskSubmittedBy, pageId)
        {
            Initialize();
        }

        public Job(ProcessingTask task)
            : this(task.PackageId, task.InstanceId, task.VersionNumber, task.RevisionNumber, task.TaskSubmittedBy, task.PageToProcessId)
        { }

        public Job()
            : base() { Initialize(); }
    }

	/// <summary>
	/// Summary description for Task.
	/// </summary>
	[Serializable]
	public class ProcessingTask
	{
		private Guid packageId;
		private int versionNumber;
		private int revisionNumber;
		private Guid instanceId;
		private string taskSubmittedBy;	
	    private Guid pageToProcessId = Guid.Empty;

		public Guid PackageId
		{
			get { return packageId; }
		}

		public int VersionNumber
		{
			get { return versionNumber;}
		}

		public int RevisionNumber
		{
			get { return revisionNumber;}
		}

		public Guid InstanceId
		{
			get { return instanceId;}
		}

		public string TaskSubmittedBy
		{
			get { return taskSubmittedBy; }
		}	
	
		public Guid PageToProcessId
		{
			get { return pageToProcessId; }
		}


        protected ProcessingTask()
        {
        }

        /// <summary>
		///  A standard processing task
		/// </summary>
		public ProcessingTask(Guid packageId, Guid instanceId, int versionNumber, int revisionNumber, string taskSubmittedBy)
		: this (packageId, instanceId, versionNumber, revisionNumber, taskSubmittedBy, Guid.Empty)
        {
		}

		/// <summary>
		/// A processing task for per Page processing only.
		/// </summary>
		public ProcessingTask(Guid packageId, Guid instanceId, int versionNumber, int revisionNumber, string taskSubmittedBy, Guid pageId)
            : this()
        {
			//this.packageInstance = packageInstance;
			this.packageId = packageId;
			this.versionNumber = versionNumber;
			this.revisionNumber = revisionNumber;
			this.instanceId = instanceId;
			this.taskSubmittedBy = taskSubmittedBy;	
			this.pageToProcessId = pageId;
		}

		public static ProcessingTask OpenFromStream(Stream s, StreamingContextStates state)
		{			
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Binder = new PackageSerialisationBinder();
			formatter.Context = new StreamingContext(state);
			object o = formatter.Deserialize(s); 
			return o as ProcessingTask;
		}
	}
}
