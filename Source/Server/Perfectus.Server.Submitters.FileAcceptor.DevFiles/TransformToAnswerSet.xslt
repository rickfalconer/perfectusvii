<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <answerSet packageName="File Acceptor Demo Package" packageId="13dacb77-6ad8-4fab-b400-44706f616ee6" packageVersion="1" packageRevision="0">
		<xsl:apply-templates/>
    </answerSet>

  </xsl:template>

  <xsl:template match="question">
    <answer>
      <xsl:attribute name="name">
        <xsl:value-of select="name" />
      </xsl:attribute>
      <xsl:value-of select="value" />
    </answer>
  </xsl:template>

</xsl:stylesheet>