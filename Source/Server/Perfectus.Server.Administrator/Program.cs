﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Administrator
{
    static class AdminProgram
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles( );
            Application.SetCompatibleTextRenderingDefault( false );
            Application.Run( new AdminConsole( ) );
        }
    }
}