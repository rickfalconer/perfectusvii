using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using System.IO.IsolatedStorage;
using System.Security.Permissions;
using System.Xml.Serialization;
using Perfectus.Library.Exception;
using Perfectus.Library.Localisation;
using Perfectus.Library.Logging;

namespace Administrator
{
    // Class to support the transaction maintenance window.
    [IsolatedStorageFilePermission(SecurityAction.Demand)]
    public class TransClass
    {
        // Represents the maximum rows the system must return.
        const int MAXROWS = 1000;
        // Filter/selections filename
        const string SELECTFILENAME = "selections.xml";

        // This represents the actual instance or transaction data.
        private DataView _theInstanceView = null;
        private DataView _theTransactionView = null;

        // Our cache of selection/filters 
        private FilterClass _filters = null;
        public FilterClass Filters
        {
            get
            {
                // If not yet created, attempt to load from xml file, or create new one.
                if( _filters == null )
                {
                    try
                    { this.LoadFilters( ); }
                    catch
                    { }
                    // If all else failed, just create a new class.
                    if( _filters == null )
                        _filters = new FilterClass( );
                }
                return _filters;
            }
        }

        // Represents a job transaction with multiple distributions.
        public class JobClass
        {
            private Guid _packageInstanceID;
            public Guid PackageInstanceID
            {
                get { return _packageInstanceID; }
                set { _packageInstanceID = value; }
            }

            private int? _jobQueueID = null;
            public int? JobQueueID
            {
                get { return _jobQueueID; }
                set { _jobQueueID = value; }
            }

            private Guid? _pageID = null;
            public Guid? PageID
            {
                get { return _pageID; }
                set { _pageID = value; }
            }

            private string _pageName = null;
            public string PageName
            {
                get { return _pageName; }
                set { _pageName = value; }
            }

            private string _statusValue = null;
            public string StatusValue
            {
                get { return _statusValue; }
                set { _statusValue = value; }
            }

            private string _statusName = null;
            public string StatusName
            {
                get { return _statusName; }
                set { _statusName = value; }
            }

            private DateTime? _lastStatusChange = null;
            public DateTime? LastStatusChange
            {
                get { return _lastStatusChange; }
                set { _lastStatusChange = value; }
            }

            private string _exceptionMessage = null;
            public string ExceptionMessage
            {
                get { return _exceptionMessage; }
                set { _exceptionMessage = value; }
            }

            private List<DistributionClass> _distributions = new List<DistributionClass>( );
            public List<DistributionClass> Distributions
            {
                get { return _distributions; }
            }
        }

        // Represents a distribution object with multiple documents
        public class DistributionClass
        {
            private Guid _distributionID;
            public Guid DistributionID
            {
                get { return _distributionID; }
                set { _distributionID = value; }
            }

            private string _distributionName;
            public string DistributionName
            {
                get { return _distributionName; }
                set { _distributionName = value; }
            }

            private string _statusValue = null;
            public string StatusValue
            {
                get { return _statusValue; }
                set { _statusValue = value; }
            }

            private string _statusName = null;
            public string StatusName
            {
                get { return _statusName; }
                set { _statusName = value; }
            }

            private DateTime? _lastStatusChange = null;
            public DateTime? LastStatusChange
            {
                get { return _lastStatusChange; }
                set { _lastStatusChange = value; }
            }

            private List<DocumentClass> _documents = new List<DocumentClass>( );
            public List<DocumentClass> Documents
            {
                get { return _documents; }
            }
        }

        // Represents a document object
        public class DocumentClass
        {
            private Guid _documentID;
            public Guid DocumentID
            {
                get { return  _documentID; }
                set { _documentID = value; }
            }

            private string _documentName;
            public string DocumentName
            {
                get { return _documentName; }
                set { _documentName = value; }
            }

            private string _reference = null;
            public string Reference
            {
                get { return _reference; }
                set { _reference = value; }
            }

            private string _linkText;
            public string LinkText
            {
                get { return _linkText; }
                set { _linkText = value; }
            }
        }

        // ###########  Package data routines ############# //
       /// <summary>
        /// Loads the appropriate package instance data  and returns the DataSet
        /// </summary>
        /// <param name="selections">Class that contains all the selection criteria</param>
        public static TransDataSet LoadPackageInstanceView( SelectionClass selections )
        {
            // Build string of valid status values.
            List<string> statusvalues = selections.GetStatusSelect( AdminClass.dataViewType.PACKAGE );
            StringBuilder excludestring = new StringBuilder( );
            foreach( string str in statusvalues )
            {
                if( excludestring.Length > 0 )
                    excludestring.Append( "," );
                excludestring.Append( "'" );
                excludestring.Append( str );
                excludestring.Append( "'" );
            }

            // The JobQueueID column is the only column that is not displayed in both views. It is hence invalid for this view.
            string filtercolumn = selections.FilterTextColumn;
            if( filtercolumn == "JobQueueID" )
                filtercolumn = null;

            // Create a min datetime from the selected timespan.  (We have to specify something as a default.)
            DateTime MinDate = new DateTime( 1900, 1, 1 );
            if( selections.TimePeriod != null )
                MinDate = DateTime.Now - selections.TimePeriod.Period;

            // This is what we return
            TransDataSet thedata = new TransDataSet( AdminClass.dataViewType.PACKAGE );

            // Create class object from DataAcces library to query the database for us.
            thedata.CurrentData = AdminClass.Instance.PackageDBObj.GetPackageInstanceData( 
                MAXROWS,
                MinDate,
                selections.GetRequestedSortByColumn( ),
                selections.GetRequestedSortOrder( ),
                filtercolumn,
                selections.FilterText,
                selections.FilterDateColumn,
                selections.FilterFromDate,
                selections.FilterToDate,
                excludestring.ToString( ) );

            return thedata;

        }

        /// <summary>
        /// Creates the local Package Instance dataview object based on the dataset parameter. Used to
        /// support multi threaded queries as we don't want the worker thread to update our class itself.
        /// </summary>
        /// <param name="theDataSet"></param>
        public void AssignPackageInstances( DataSet theDataSet )
        {
            _theInstanceView = null;

            if( theDataSet == null || theDataSet.Tables[ "PackageInstances" ] == null )
                return;

            // Create a new view of the loaded data.
            _theInstanceView = new DataView( theDataSet.Tables[ "PackageInstances" ] );
        }

        /// <summary>
        /// Returns the PackageInstance Table created by the call to "LoadPackageInstanceView"
        /// </summary>
        /// <returns>DataTable</returns>
        public DataView GetPackageInstanceView( )
        {
            if( _theInstanceView == null )
                throw new InvalidOperationException( "The 'GetPackageInstanceView' operation is not possible until the correct data is first loaded." );

            return _theInstanceView;
        }

        /// <summary>
        /// Builds a hierarchical list of jobs, distributions and documents for a package instance. 
        /// </summary>
        /// <param name="selectedrow">DataRow in the PackageInstances table.</param>
        /// <returns></returns>
        public List<JobClass> GetPackageInstanceJobs( DataRow selectedrow )
        {
            if( _theInstanceView == null )
                throw new InvalidOperationException( "The 'GetPackageInstanceJobs' operation is not possible until the correct data is first loaded." );

            if( selectedrow == null )
                throw new ArgumentNullException( "No selected table row." );

            if( !AdminClass.gHandleNull( selectedrow[ "PackageInstanceId" ] ).HasValue )
                throw new ArgumentNullException( "Missing package instance identifier (guid)." );

            // Determine the package instance guid of the instance we need to return.
            Guid packageInstanceGuid = (Guid)AdminClass.gHandleNull( selectedrow[ "PackageInstanceId" ] );

            // Get view of all jobs for this package.
            DataSet ds = AdminClass.Instance.PackageDBObj.GetPackageDistributions( packageInstanceGuid );
            DataTable dt = ds.Tables[ "Distributions" ];
            DataTable et = ds.Tables[ "Exceptions" ];

            // This is what we return
            List<JobClass> jobClassList = new List<JobClass>( );

            // Scan through all the jobs.
            foreach( DataRow jobrow in dt.Rows )
            {
                int? jobID = AdminClass.iHandleNull( jobrow[ "JobQueueId" ] );
                
                // Check we don't already have it
                if( jobClassList.Find( delegate( JobClass item ) { return item.JobQueueID == jobID; } ) != null )
                    continue;

                // Create the object to represent the Job.
                JobClass jc = CreateJobClass( jobrow );
                jobClassList.Add( jc );

                // Create View of distributions.
                StringBuilder distfilter = new StringBuilder( );
                distfilter.Append( "PackageInstanceId='" );
                distfilter.Append( jc.PackageInstanceID );
                distfilter.Append( "' AND DistributorId IS NOT NULL " );
                if( jc.JobQueueID.HasValue )
                {
                    distfilter.Append( "AND JobQueueID = " );
                    distfilter.Append( jc.JobQueueID.ToString( ) );
                    distfilter.Append( " " );
                }

                // Extract all the appropriate distributions.
                GetDistributions( dt, distfilter.ToString( ), "DistributorName", jc );

                // Extract exception data. (Only first row.)
                if( jc.StatusValue != "COMPLETE" )
                {
                    StringBuilder excepfilter = new StringBuilder( );
                    excepfilter.Append( "RelatedPackageInstanceId='" );
                    excepfilter.Append( jc.PackageInstanceID );
                    excepfilter.Append( "' " );
                    if( jc.JobQueueID.HasValue )
                    {
                        excepfilter.Append( "AND JobQueueID = " );
                        excepfilter.Append( jc.JobQueueID.ToString( ) );
                        excepfilter.Append( " " );
                    }
                    DataView exceptionView = new DataView( et, excepfilter.ToString( ), null, DataViewRowState.CurrentRows );
                    if( exceptionView.Count > 0 )
                        jc.ExceptionMessage = AdminClass.sHandleNull( exceptionView[ 0 ][ "ExceptionMessage" ] );
                }
            }

            return jobClassList;
        }


        // ###########  Transaction data routines ############# //
        /// <summary>
        /// Loads the appropriate package transactional data and returns the DataSet
        /// </summary>
        /// <param name="selections">Class that contains all the selection criteria</param>
        public static TransDataSet LoadTransactionView( SelectionClass selections )
        {
            // Build string of valid status values.
            List<string> statusvalues = selections.GetStatusSelect( AdminClass.dataViewType.TRANS );
            StringBuilder excludestring = new StringBuilder( );
            foreach( string str in statusvalues )
            {
                if( excludestring.Length > 0 )
                    excludestring.Append( "," );
                excludestring.Append( "'" );
                excludestring.Append( str );
                excludestring.Append( "'" );
            }

            // Create a min datetime from the selected timespan.  (We have to specify something as a default.)
            DateTime MinDate = new DateTime( 1900, 1, 1 );
            if( selections.TimePeriod != null )
                MinDate = DateTime.Now - selections.TimePeriod.Period;

            // This is what we return
            TransDataSet thedata = new TransDataSet( AdminClass.dataViewType.TRANS );

            // Create class object from DataAcces library to query the database for us.
            thedata.CurrentData = AdminClass.Instance.PackageDBObj.GetTransactionData(
                MAXROWS,
                MinDate,
                selections.GetRequestedSortByColumn( ),
                selections.GetRequestedSortOrder( ),
                selections.FilterTextColumn,
                selections.FilterText,
                selections.FilterDateColumn,
                selections.FilterFromDate,
                selections.FilterToDate,
                excludestring.ToString( ) );

            return thedata;
        }

        /// <summary>
        /// Creates the local Transaction dataview object based on the dataset parameter. Used to
        /// support multi threaded queries as we don't want the worker thread to update our class itself.
        /// </summary>
        /// <param name="theDataSet"></param>
        public void AssignTransactions( DataSet theDataSet )
        {
            _theTransactionView = null;

            if( theDataSet == null || theDataSet.Tables[ "Transactions" ] == null )
                return;

            // Create a new view of the loaded data.
            _theTransactionView = new DataView( theDataSet.Tables[ "Transactions" ] );
        }

        /// <summary>
        /// Returns the Transactions Table created by the call to "LoadTransactionView"
        /// </summary>
        /// <returns>DataTable</returns>
        public DataView GetTransactionView( )
        {
            if( _theTransactionView == null )
                throw new InvalidOperationException( "The 'GetTransactionView' operation is not possible until the correct data is first loaded." );

            return _theTransactionView;
        }

        /// <summary>
        /// Builds a hierarchical list of distributions and documents for a particular job. 
        /// </summary>
        /// <param name="selectedrow">DataRow in the PackageInstances table.</param>
        /// <returns></returns>
        public JobClass GetTransactionJob( DataRow selectedrow )
        {
            if( _theTransactionView == null )
                throw new InvalidOperationException( "The 'GetTransactionDistributions' operation is not possible until the correct data is first loaded." );

            if( selectedrow == null )
                throw new ArgumentNullException( "No selected table row." );

            if( !AdminClass.gHandleNull( selectedrow[ "PackageInstanceId" ] ).HasValue )
                throw new ArgumentNullException( "Missing package instance identifier (guid)." );

            if( !AdminClass.iHandleNull( selectedrow[ "JobQueueId" ] ).HasValue )
                throw new ArgumentNullException( "Missing Job Queue Id." );

            // This is what we return
            JobClass parentJobClass = CreateJobClass( selectedrow );

            // Job filter
            StringBuilder jobfilter = new StringBuilder( );
            jobfilter.Append( "PackageInstanceId='" );
            jobfilter.Append( parentJobClass.PackageInstanceID.ToString( ) );
            jobfilter.Append( "' AND JobQueueID = " );
            jobfilter.Append( parentJobClass.JobQueueID.ToString( ) );

            // Get view of all jobs for this package.
            DataSet ds = AdminClass.Instance.PackageDBObj.GetJobDistributions( (int)AdminClass.iHandleNull( selectedrow[ "JobQueueId" ] ) );
            DataTable dt = ds.Tables[ "Distributions" ];

            // Extract all the appropriate distributions.
            GetDistributions( ds.Tables[ "Distributions" ], jobfilter.ToString( ), "DistributorName", parentJobClass );

            // Extract exception data. (Only first row.)
            if( parentJobClass.StatusValue != "COMPLETE" )
            {
                DataTable et = ds.Tables[ "Exceptions" ];
                if( et.Rows.Count > 0 )
                    parentJobClass.ExceptionMessage = AdminClass.sHandleNull( et.Rows[ 0 ][ "ExceptionMessage" ] );
            }
            return parentJobClass;
        }


        // ###########  Generic data routines ############# //
 
        /// <summary>
        /// Resubmits failed jobs that are already on the JobQueue.
        /// </summary>
        /// <param name="JobQueueId"></param>
        /// <returns>Whether operation was successful</returns>
        public void ResubmitJob( Guid packageInstanceID )
        {
            int numjobs = Perfectus.Server.DataAccess.DatabaseJobQueue.DatabaseJobQueue.ReTry( packageInstanceID );

            // Record the event in trace log.
            if( numjobs > 0 )
                Log.Info( new ID( "Administrator.ResubmitPackageInstance" ), packageInstanceID.ToString( ), numjobs,
                            System.Security.Principal.WindowsIdentity.GetCurrent( ).Name );
        }

         /// <summary>
         /// Creates a copy of the instance (all jobs) and either return url or place job(s) on the queue immediately based on EasyInterview requirement.
         /// </summary>
         /// <param name="PackageInstanceId"></param>
         /// <param name="url"></param>
        /// <returns>Whether operation was successful</returns>
        public void ReprocessPackage( Guid PackageInstanceId, ref string url )
        {
            throw new NotSupportedException( );
        }

        /// <summary>
        /// Changes priority of job on the JobQueue.
        /// </summary>
        /// <param name="JobQueueId"></param>
        /// <returns>Whether operation was successful</returns>
        public void PushJob( int JobQueueId )
        {
            throw new NotSupportedException( );
        }

        /// <summary>
        /// Deletes the package instance in the same way the Status page does.
        /// </summary>
        /// <param name="PackageInstanceId"></param>
        /// <returns></returns>
        public void DeletePackage( Guid PackageInstanceId )
        {
            Perfectus.Server.InterviewSystem.InstanceManagement.DeleteInstance( PackageInstanceId );
        }

        /// <summary>
        /// Creates a JobClass object that represents a single job, which is a subset of a package instance
        /// </summary>
        /// <param name="row">The DataRow which represents a row in the JobQueue table </param>
        /// <returns></returns>
        private JobClass CreateJobClass( DataRow row )
        {
            JobClass jc = new JobClass( );

            jc.PackageInstanceID = (Guid)AdminClass.gHandleNull( row[ "PackageInstanceId" ] );
            jc.JobQueueID = AdminClass.iHandleNull( row[ "JobQueueId" ] );
            jc.PageID = AdminClass.gHandleNull( row[ "PageId" ] );
            jc.PageName = AdminClass.sHandleNull( row[ "PageName" ] );
            jc.StatusValue = AdminClass.sHandleNull( row[ "StatusValue" ] );
            jc.StatusName = AdminClass.sHandleNull( row[ "StatusName" ] );
            jc.LastStatusChange = AdminClass.dateHandleNull( row[ "LastStatusChange" ] );

            return jc;
        }

        /// <summary>
        /// Assembles all the distributions for a particular job.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <param name="parentobj"></param>
        private void GetDistributions( DataTable dt, string filter, string sort, JobClass parentobj )
        {
            DataView distributionView = new DataView( dt, filter, sort, DataViewRowState.CurrentRows );

            // Scan throught all distributions  (The DistributorId really has no meaning here as it is defined by the distributor dll used, not the IPManager 
            // distributor instance. Hence we have to use the distributor name to distinguish btw distributions. However whilst
            // from version 6.0 onwards we distinquish btw distribtions via its name, this field never existed prior so if the name is null, the best 
            // we can do is use the distribution guid.)
            foreach( DataRowView distrow in distributionView )
            {
                string distributionName = AdminClass.sHandleNull( distrow[ "DistributorName" ] );
                Guid? distributionId = AdminClass.gHandleNull( distrow[ "DistributorId" ] );

                // Should not happen, however double check this row doesn't just represent the job queue.
                if( !distributionId.HasValue )
                    continue;

                // Check we don't already have it
                if( !String.IsNullOrEmpty( distributionName ) )
                {
                    if( parentobj.Distributions.Find( delegate( DistributionClass item ) { return item.DistributionName == distributionName; } ) != null )
                        continue;
                }
                else if( parentobj.Distributions.Find( delegate( DistributionClass item ) { return item.DistributionID == distributionId; } ) != null )
                    continue;

                // Create the object to represent the Distributor.
                DistributionClass dc = new DistributionClass( );
                dc.DistributionID = (Guid)distributionId;
                dc.DistributionName = distributionName;
                dc.StatusValue = AdminClass.sHandleNull( distrow[ "StatusValue" ] );
                dc.StatusName = AdminClass.sHandleNull( distrow[ "StatusName" ] );
                dc.LastStatusChange = AdminClass.dateHandleNull( distrow[ "LastStatusChange" ] );

                // Add to job and assign LastChangeDate
                parentobj.Distributions.Add( dc );
                if( !parentobj.LastStatusChange.HasValue || parentobj.LastStatusChange < dc.LastStatusChange )
                    parentobj.LastStatusChange = dc.LastStatusChange;

                // Create View of documents. (From version 6.0 onwards we distinquish btw distribtions via its name, however this field never
                // existed prior so if the name is null, the best we can do is use the distribution guid.)
                StringBuilder docfilter = new StringBuilder( filter );
                if( dc.DistributionName != null )
                {
                    docfilter.Append( "AND DistributorName = '" );
                    docfilter.Append( dc.DistributionName );
                    docfilter.Append( "' " );
                }
                else
                {
                    docfilter.Append( "AND DistributorId = '" );
                    docfilter.Append( dc.DistributionID );
                    docfilter.Append( "' " );
                }

                // Extract all the appropriate documents.
                GetDocuments( dt, docfilter.ToString( ), String.Empty, dc );
            }
        }

        /// <summary>
        /// Assembles all the Documents for a particular distribution.
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sort"></param>
        /// <param name="parentobj"></param>
        private void GetDocuments( DataTable dt, string filter, string sort, DistributionClass parentobj )
        {
            DataView docView = new DataView( dt, filter, sort, DataViewRowState.CurrentRows );

            // Scan throught all documents
            foreach( DataRowView docrow in docView )
            {
                Guid? documentID = AdminClass.gHandleNull( docrow[ "DocumentId" ] );

                if( !documentID.HasValue )
                    continue;

                // Create the object to represent the Document.
                DocumentClass docc = new DocumentClass( );
                docc.DocumentID = (Guid)documentID;
                docc.DocumentName = AdminClass.sHandleNull( docrow[ "DocumentName" ] );
                docc.Reference = AdminClass.sHandleNull( docrow[ "InternalReference" ] );
                docc.LinkText = AdminClass.sHandleNull( docrow[ "LinkText" ] );
                parentobj.Documents.Add( docc );
            }
        }

        // ###########  Filter access and storage functions ############# //
        /// <summary>
        /// Loads the filters from our xml file into the filter collection.
        /// </summary>
        /// <returns>The new filter collection.</returns>
        public void LoadFilters( )
        {
            // First try to access file from isolated storage.
            try
            {
                IsolatedStorageFile istore = IsolatedStorageFile.GetMachineStoreForAssembly( );
                if( istore.GetFileNames( SELECTFILENAME ).Length != 0 )
                {
                    using( IsolatedStorageFileStream istream = new IsolatedStorageFileStream( SELECTFILENAME, FileMode.Open, istore ) )
                    {
                        XmlSerializer xs = new XmlSerializer( typeof( FilterClass ) );
                        this._filters = (FilterClass)xs.Deserialize( istream );
                        istream.Close( );
                    }
                }
            }
            catch( System.Security.SecurityException )
            {
                Log.Error( new ID( "Administrator.IsolatedStoragePermissionException" ) );
            }
            catch( Exception ex )
            {
                Log.Error( new ID( "Administrator.GeneralException" ), "LoadFilters", ex.Message );
            }

            // If unable to read from isolated storage (maybe file corrupted) try deploy file.
             if( _filters == null )
                LoadDeployFilters( );
        }

        // Internal call to load the filter file deployed with application.
        private void LoadDeployFilters( )
        {
            // First try to access file from isolated storage.
            try
            {
                using( FileStream fs = new FileStream( SELECTFILENAME, FileMode.Open, FileAccess.Read ) )
                {
                    XmlSerializer xs = new XmlSerializer( typeof( FilterClass ) );
                    this._filters = (FilterClass)xs.Deserialize( fs );
                    fs.Close( );
                }
            }
            catch( Exception ex )
            {
                Log.Error( new ID( "Administrator.GeneralException" ), "LoadDeployFilters", ex.Message );
            }
        }

        /// <summary>
        /// Saves the current filter collection.
        /// </summary>
        public void SaveFilters( )
        {
            try
            {
                IsolatedStorageFile istore = IsolatedStorageFile.GetMachineStoreForAssembly( );
                using( IsolatedStorageFileStream istream = new IsolatedStorageFileStream( SELECTFILENAME, FileMode.Create, istore ) )
                {
                    XmlSerializer xs = new XmlSerializer( typeof( FilterClass ) );
                    xs.Serialize( istream, this.Filters );
                    istream.Close( );
                }

                // Record who changed the filter file in trace log.
                Log.Info( new ID( "Administrator.FilterFileUpdated" ), System.Security.Principal.WindowsIdentity.GetCurrent( ).Name );
            }
            catch( System.Security.SecurityException )
            {
                Log.Error( new ID( "Administrator.IsolatedStoragePermissionException" ) );
            }
            catch( Exception ex )
            {
                Log.Error( new ID( "Administrator.GeneralException" ), "SaveFilters", ex.Message );
            }
        }

        /// <summary>
        /// Deletes the filter identified by name.
        /// </summary>
        public void DeleteFilter( string name )
        {
            // First find the item
            SelectionClass sc = this.Filters.GetFilter( name );
            if( sc == null )
                throw new Exception( "The filter '" + name + "' has already been removed." );

            // Remove the filter 
            this.Filters.Selections.Remove( sc );

            // Re-save all filters.
            this.SaveFilters( );
        }
    }

    // A simple class derived from DataTable that records the view (Instance or Transaction) of the dataset.
    public class TransDataSet 
    {
        public TransDataSet( AdminClass.dataViewType theType )
        {
            CurrentView = theType;
        }

        public AdminClass.dataViewType CurrentView = AdminClass.dataViewType.NONE;
        public DataSet CurrentData = null;
    }
}



