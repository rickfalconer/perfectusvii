using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;

namespace Administrator
{
    /// <summary>
    /// Base form class from which all forms within this application are derived.
    /// </summary>
    public class BaseFormView : Form
    {
        /// <summary>
        /// Indicates whether multiple instances of the form can exists with the same application.
        /// </summary>
        private bool _allowMultiInstances = false;
        public bool AllowMultiInstances
        {
            get { return _allowMultiInstances; }
            set { _allowMultiInstances = value; }
        }
        
        public BaseFormView( )
        {
            InitializeComponent( );
        }

        private void InitializeComponent( )
        {
            this.SuspendLayout( );
            // 
            // BaseFormView
            // 
            this.ClientSize = new System.Drawing.Size( 292, 273 );
            this.Name = "BaseFormView";
            this.Load += new System.EventHandler( this.BaseFormView_Load );
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.BaseFormView_FormClosed );
            this.ResumeLayout( false );

        }

        private void BaseFormView_Load( object sender, EventArgs e )
        {
            // Register this form with the application controller.
            Controller.Instance.RegisterView( this );
            // Call the standard method to request the form to construct its interface.
            LoadInterface( );
        }

        private void BaseFormView_FormClosed( object sender, FormClosedEventArgs e )
        {
            // Unregister this form with the application controller.
            Controller.Instance.UnregisterView( this );
        }

        /// <summary>
        /// Called during the form loading process. Used to configure all interface controls.
        /// </summary>
        public virtual void LoadInterface( )
        {
            // Do standard stuff btw all forms.
            this.Icon = Properties.Resources.App;
        }

        /// <summary>
        /// Every form derived from this class will be registered in the controller class. If a window registers a change to the class models,
        /// this event is called to notify other forms about the change. Hence if a form needs to respond to changes from other forms, 
        /// they should override this method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void ModelChange( object sender, ModelChangeEventArgs e ) { }

        /// <summary>
        /// If overridden, must return whether it is possible to close a particular form.
        /// </summary>
        /// <returns></returns>
        public virtual bool CanClose( ) { return true; }

        // Default error message display.
        public virtual void ShowErrorMessage( string message )
        {
            MessageBox.Show( message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }
    }

    /// <summary>
    /// Used by the View class to notify other forms about changes to the class models.
    /// </summary>
    public class ModelChangeEventArgs : EventArgs
    {
        private object _modelObj;
        public object ModelObj
        {
            get
            {
                return _modelObj;
            }
        }

        public ModelChangeEventArgs( object modelObj )
        {
            _modelObj = modelObj;
        }
    }

}
