using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Administrator
{
    /// <summary>
    /// Represents a sort and filter selection. This object contains all the filtering and sorting information
    /// associated with a transactions form.
    /// </summary>
    public class SelectionClass : ICloneable
    {
        // Necessary for serialization.
        public SelectionClass(  )
        {  }

        public SelectionClass( AdminClass.dataViewType theview )
        {
            CurrentView = theview;
            RequestedView = theview;
        }

        // Copy contructor
        public SelectionClass( AdminClass.dataViewType theview, SelectionClass selectObj )
        {
            CurrentView = theview;
            RequestedView = theview;

            if( selectObj != null )
            {
                this.Name = selectObj.Name;
                this._filterDateColumn = selectObj._filterDateColumn;
                this._filterFromDate = selectObj._filterFromDate;
                this._filterToDate = selectObj._filterToDate;
                this._filterTextColumn = selectObj._filterTextColumn;
                this._filterText = selectObj._filterText;
                this._sortByColumn_Instance = selectObj._sortByColumn_Instance;
                this._sortByColumn_Job = selectObj._sortByColumn_Job;
                this._sortOrder_Instance = selectObj._sortOrder_Instance; 
                this._sortOrder_Job = selectObj._sortOrder_Job;
                this.SetStatusSelect( AdminClass.dataViewType.PACKAGE, selectObj._statusSelect_Instance );
                this.SetStatusSelect( AdminClass.dataViewType.TRANS, selectObj._statusSelect_Job );
                if( selectObj.TimePeriod != null )
                    this.TimePeriod = (TimePeriodClass)selectObj.TimePeriod.Clone( );
            }
        }

        public object Clone( )
        {
            return new SelectionClass( this.CurrentView, this );
        }

        // This represents the view we current display
        private AdminClass.dataViewType _currentView = AdminClass.dataViewType.NONE;
        [XmlIgnore]
        public AdminClass.dataViewType CurrentView
        {
            get { return _currentView; }
            set { _currentView = value; }
        }

        // This represents the view we want to display, (waiting for db)
        private AdminClass.dataViewType _requestedView = AdminClass.dataViewType.NONE;
        [XmlIgnore]
        public AdminClass.dataViewType RequestedView
        {
            get { return _requestedView; }
            set { _requestedView = value; }
        }

        // The name of this selection.
        private string _name = String.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        // This is our default sort column for packages
        private string _sortByColumn_Instance = "WhenCreatedLcl";
        public string SortByColumn_Instance
        {
            get { return _sortByColumn_Instance; }
            set { _sortByColumn_Instance = value; }
        }

        // This is our default sort column for jobs
        private string _sortByColumn_Job = "JobQueueID";
        public string SortByColumn_Job
        {
            get { return _sortByColumn_Job; }
            set { _sortByColumn_Job = value; }
        }

        public string GetSortByColumn(  )
        {
            return ( this.CurrentView == AdminClass.dataViewType.PACKAGE ) ? _sortByColumn_Instance : _sortByColumn_Job;
        }
        public string GetRequestedSortByColumn( )
        {
            return ( this.RequestedView == AdminClass.dataViewType.PACKAGE ) ? _sortByColumn_Instance : _sortByColumn_Job;
        }
        public void SetSortByColumn( string value )
        {
            if( this.CurrentView == AdminClass.dataViewType.PACKAGE )
                _sortByColumn_Instance = value;
            else
                _sortByColumn_Job = value;
        }

        // This is our default sort direction 
        private ListSortDirection _sortOrder_Instance = ListSortDirection.Descending;
        public ListSortDirection SortOrder_Instance
        {
            get { return _sortOrder_Instance; }
            set { _sortOrder_Instance = value; }
        }

        private ListSortDirection _sortOrder_Job = ListSortDirection.Descending;
        public ListSortDirection SortOrder_Job
        {
            get { return _sortOrder_Job; }
            set { _sortOrder_Job = value; }
        }

        public ListSortDirection GetSortOrder(  )
        {
            return ( this.CurrentView == AdminClass.dataViewType.PACKAGE ) ? _sortOrder_Instance : _sortOrder_Job;
        }
        public ListSortDirection GetRequestedSortOrder( )
        {
            return ( this.RequestedView == AdminClass.dataViewType.PACKAGE ) ? _sortOrder_Instance : _sortOrder_Job;
        }
        public void SetSortOrder( ListSortDirection value )
        {
            if( this.CurrentView == AdminClass.dataViewType.PACKAGE )
                _sortOrder_Instance = value;
            else
                _sortOrder_Job = value;
        }

        // The minimum period for searches
        private TimePeriodClass _timePeriod = null;
        public TimePeriodClass TimePeriod
        {
            get { return _timePeriod; }
            set { _timePeriod = value; }
        }

        private string _filterTextColumn = null;
        public string FilterTextColumn
        {
            get { return _filterTextColumn; }
            set { _filterTextColumn = value; }
        }

        private string _filterText = null;
        public string FilterText
        {
            get { return _filterText; }
            set { _filterText = value; }
        }

        private string _filterDateColumn = null;
        public string FilterDateColumn
        {
            get { return _filterDateColumn; }
            set { _filterDateColumn = value; }
        }

        private DateTime? _filterFromDate = null;
        public DateTime? FilterFromDate
        {
            get { return _filterFromDate; }
            set { _filterFromDate = value; }
        }

        private DateTime? _filterToDate = null;
        public DateTime? FilterToDate
        {
            get { return _filterToDate; }
            set { _filterToDate = value; }
        }

        // List of valid package instance status's
        private List<string> _statusSelect_Instance = new List<string>( );
        public List<string> StatusSelect_Instance
        {
            get { return _statusSelect_Instance; }
            set { _statusSelect_Instance = value; }
        }

        // List of valid Job status's
        private List<string> _statusSelect_Job = new List<string>( );
        public List<string> StatusSelect_Job
        {
            get { return _statusSelect_Job; }
            set { _statusSelect_Job = value; }
        }

        public List<string> GetStatusSelect( AdminClass.dataViewType view )
        {
            return ( view == AdminClass.dataViewType.PACKAGE ) ? _statusSelect_Instance : _statusSelect_Job;
        }

        // Create list of status's based on string collection.
        public void SetStatusSelect( List<string> value )
        {
            SetStatusSelect( this.CurrentView, value );
        }

        public void SetStatusSelect( AdminClass.dataViewType view, List<string> value )
        {
            if( view == AdminClass.dataViewType.PACKAGE )
            {
                _statusSelect_Instance.Clear( );
                foreach( string str in value )
                    _statusSelect_Instance.Add( (string)str.Clone( ) );
            }
            else
            {
                _statusSelect_Job.Clear( );
                foreach( string str in value )
                    _statusSelect_Job.Add( (string)str.Clone( ) );
            }
        }

        // Create list of status's based on collection of keys.
        public void SetStatusSelect( Dictionary<string, string> value )
        {
            List<string> listofvalues = new List<string>( );
            foreach( string str in value.Keys )
                listofvalues.Add( str );
            this.SetStatusSelect( listofvalues );
        }

        /// <summary>
        /// Returns whether this selection object has any specific filtering selections.
        /// This does not refer to the time period settings as it is controller separately.
        /// </summary>
        /// <returns></returns>
        public bool HasFilterSelections( )
        {
            return (
                !String.IsNullOrEmpty( this.Name ) ||
                !String.IsNullOrEmpty( this._filterDateColumn ) ||
                !String.IsNullOrEmpty( this._filterTextColumn ) ||
                !String.IsNullOrEmpty( this._filterText ) ||
                _filterFromDate.HasValue ||
                _filterToDate.HasValue ||
                _statusSelect_Instance.Count > 0 ||
                _statusSelect_Job.Count > 0 );
        }
    }

    /// <summary>
    /// Class that represents time periods to filter by.
    /// </summary>
    public class TimePeriodClass : IComparable, ICloneable
    {
        // The name shown on the interface.
        private string _name = null;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private TimeSpan _period;
        [XmlIgnore]
        public TimeSpan Period
        {
            get { return _period; }
            set { _period = value; }
        }

        [XmlElement( "Period" )]
        public string XmlPeriod
        {
            get { return _period.ToString( ); }
            set { _period = TimeSpan.Parse( value ); }
        }

        public int CompareTo( object tpc )
        {
            if( tpc is TimePeriodClass )
                return Period.CompareTo( ((TimePeriodClass)tpc).Period );
            throw new ArgumentException( "Invalid type comparison" );
        }

        public object Clone( )
        {
            return this.MemberwiseClone( );
        }
    }

    // ###########  Filter access and storage ############# //
    // 
   
    /// <summary>
    /// Represents a collection of sort/filter options. This class is serialised to a xml file
    /// and used to record all filter, time period restictions and default settings which need to 
    /// be persistence for a specific installation.
    /// </summary>
    public class FilterClass
    {
        // This represents the default filter (should be one of the selections in the selection list below.
        private string _defaultFilter = null;
        public string DefaultFilter
        {
            get { return _defaultFilter; }
            set { _defaultFilter = value; }
        }

        // List of possible time periods.
        private List<TimePeriodClass> _timePeriods = new List<TimePeriodClass>( );
        public List<TimePeriodClass> TimePeriods
        {
            get { return _timePeriods; }
            set { _timePeriods = value; }
        }

        // List of selections recorded in the application's xml file.
        private List<SelectionClass> _selections = new List<SelectionClass>( );
        public List<SelectionClass> Selections
        {
            get { return _selections; }
            set { _selections = value; }
        }

        /// <summary>
        /// Appends a filter/selection object to our filters collection.
        /// </summary>
        /// <param name="selectionObj"></param>
        public void AddFilter( SelectionClass selectionObj )
        {
            // First check the filter doesn't exist.
            if( GetFilter( selectionObj.Name ) != null )
                throw new Exception( "Filter with the name '" + selectionObj.Name + "' already exists." );

            // Add to our list.
            Selections.Add( new SelectionClass( AdminClass.dataViewType.NONE, selectionObj ) );
        }

        /// <summary>
        /// Retrieves a selection object by name.
        /// </summary>
        /// <param name="selectionObj"></param>
        public SelectionClass GetFilter( string name )
        {
            return Selections.Find( delegate( SelectionClass item ) { return item.Name == name; } );
        }
    }
}
