using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.ConfigurationSystem.ServerSystem;
using Perfectus.Server.DataAccess.Administration;

using System.Security.Principal;

namespace Administrator
{
    public class AdminClass
    {
        // Instance of this object.
        static AdminClass _instance;
        public static AdminClass Instance
        {
            get
            {
                if ( _instance == null )
                    _instance = new AdminClass( );

                return _instance;
            }
        }

        // The types of views to the data the admin console can provide 
        public enum dataViewType { NONE, PACKAGE, TRANS };

        // Class to support the administration of the configuration files.
        private ConfigClass _configurationClass = null;
        public ConfigClass ConfigurationClass
        {
            get
            {
                if( _configurationClass == null )
                {
                    _configurationClass = new ConfigClass( );
                    _configurationClass.LoadDataConfigurationFile( );
                }
                return _configurationClass;
            }
        }

        // The server information for the current application.
        private ServerInfo _serverinfo = new ServerInfo( );
        public string ServerName
        {
            get { return _serverinfo.Name; }
        }

        // Our data access class
        private PackageTransaction _packageDBObj = null;
        public PackageTransaction PackageDBObj
        {
            get
            {
                if( _packageDBObj == null )
                    _packageDBObj = new PackageTransaction( );
                return _packageDBObj;
            }
        }

        // Default Constructor
        private AdminClass( )
        {     }

        // Cache of Package status's
        private Dictionary<string, string> _packageStatus = null;
        public Dictionary<string, string> PackageStatus
        {
            get
            {
                if( _packageStatus == null )
                    _packageStatus = PackageDBObj.GetPackageStatus( );
                return _packageStatus;
            }
        }

        // Cache of Job status's
        private Dictionary<string, string> _jobStatus = null;
        public Dictionary<string, string> JobStatus
        {
            get
            {
                if( _jobStatus == null )
                    _jobStatus = PackageDBObj.GetJobStatus( );
                return _jobStatus;
            }
        }

        /// <summary>
        /// Method for checking to see if the logged in user is in the Administrator's or System Operator's group
        /// </summary>
        /// <returns></returns>
        public bool IsUserAuthorised( )
        {
            // Get the currently logged in user
            WindowsIdentity user = WindowsIdentity.GetCurrent( );
            WindowsPrincipal principal = new WindowsPrincipal( user );

            return ( principal.IsInRole( WindowsBuiltInRole.Administrator ) ||
                        principal.IsInRole( WindowsBuiltInRole.SystemOperator ) );
        }

        // Type check functions.
        #region DBTypeChecking Functions
        public static string sHandleNull( object DBValue )
        {
            if( DBValue == DBNull.Value )
                return null;
            else
                return Convert.ToString( DBValue );
        }

        public static int? iHandleNull( object DBValue )
        {
            if( DBValue == DBNull.Value )
                return null;
            else
                return Convert.ToInt32( DBValue );
        }

        public static Guid? gHandleNull( object DBValue )
        {
            // To support Oracle.
            if( DBValue is string )
            {
                string str = sHandleNull( DBValue );
                if( str == null ) return null;
                return new Guid( str );
            }
            if( DBValue == DBNull.Value )
                return null;
            else
                return (Guid)DBValue;
        }

        public static DateTime? dateHandleNull( object DBValue )
        {
            if( DBValue == DBNull.Value )
                return null;
            else
                return Convert.ToDateTime( DBValue );
        }
        #endregion
    }

    
}
