using System;
using System.Collections.Generic;
using System.Text;

namespace Administrator
{
    // The event type generated to signal changes btw the application forms.
    public delegate void ModelChangedHandler(object sender, ModelChangeEventArgs e);

    /// <summary>
    /// This class is the application controller to primarily signal changes between the various forms within
    /// the application, in a MVC type implementation.
    /// </summary>
    public class Controller
    {
        #region Singleton
        private static Controller instance;
        public static Controller Instance
        {
            get
            {
                if (instance == null) instance = new Controller();

                return instance;
            }
        }
        #endregion

        public event ModelChangedHandler ModelChangeEvent;

        /// <summary>
        /// Register a particular form with this controller to be notified of changes.
        /// </summary>
        /// <param name="view"></param>
        public void RegisterView( System.Windows.Forms.Form view )
        {
            BaseFormView theform = view as BaseFormView;
            if( theform != null )
                this.ModelChangeEvent += new ModelChangedHandler( theform.ModelChange );
        }

        /// <summary>
        /// Unregister a form from this controller.
        /// </summary>
        /// <param name="view"></param>
        public void UnregisterView( System.Windows.Forms.Form view )
        {
            BaseFormView theform = view as BaseFormView;
            if( theform != null )
                this.ModelChangeEvent -= new ModelChangedHandler( theform.ModelChange );
        }

        /// <summary>
        /// Called by forms to notify other forms with the application of changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RaiseModelChange( object sender, ModelChangeEventArgs e )
        {
            if( ModelChangeEvent != null )
                ModelChangeEvent( sender, e );
        }
    }
}
