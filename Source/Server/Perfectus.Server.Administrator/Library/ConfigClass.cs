using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using Microsoft.Win32;

using Perfectus.Common;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.ConfigurationSystem.ServerSystem;
using Perfectus.Server.Common.Data;
using Perfectus.DataAccess;

using Perfectus.Library.Exception;
using Perfectus.Library.Logging;
using Perfectus.Library.Localisation;
using Perfectus.Library.Install;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;


namespace Administrator
{
    /// <summary>
    /// Class to support the administration of the configuration files.
    /// </summary>
    public class ConfigClass
    {
        // The external data configuration file.
        private Configuration _dataConfigFile = null;
        // Get Enterprise Library database settings.
        private DatabaseSettings _entlibDatabaseSettings = null;

        // The connection settings recorded in the data configuration file.
        private BindingList<PerfectusConnectionSetting> _connectionSettings = null;
        public BindingList<PerfectusConnectionSetting> ConnectionSettings
        {
            get 
            {
                if( _connectionSettings == null )
                    LoadConnectionSettings( );
                return _connectionSettings;
            }
        }

        // The DB providers.
        private string[ ] _databaseProviders = null;
        public string[] DatabaseProviders
        {
            get
            {
                if( _databaseProviders == null )
                {
                    if( _entlibDatabaseSettings == null )
                        _entlibDatabaseSettings = new DatabaseSettings( );

                    _databaseProviders = new string[ _entlibDatabaseSettings.ProviderMappings.Count + 1 ];
                    _databaseProviders[ 0 ] = String.Empty;
                    for( int i = 0; i < _entlibDatabaseSettings.ProviderMappings.Count; i++ )
                        _databaseProviders[ i+1 ] = _entlibDatabaseSettings.ProviderMappings.Get( i ).DbProviderName;
                }
                return _databaseProviders; 
            }
        }

        // The default database. Represents one of the connection strings.
        public string DefaultDatabase
        {
            get{ return ( _entlibDatabaseSettings == null ) ? null : _entlibDatabaseSettings.DefaultDatabase; }
            set
            {
                if( _entlibDatabaseSettings != null )
                    _entlibDatabaseSettings.DefaultDatabase = value;

                // Set the appropriate connection object.
                SetDefaultConnection( value );
            }
        }

        public ConfigClass( )
        {
            // This is our shared config file class serialised (read only)... Not currently used here. 
            // perfectusServerConfiguration perfectusSharedConfig = Config.Shared;
        }

        /// <summary>
        /// Looks for the external dataconfiguration file based on the Enterprise Library configuration settings.
        /// </summary>
        public void LoadDataConfigurationFile( )
        {
            // First clear all current data.
            ClearConfigurationData( );

            // Get the path to the external data configuration file. This is used by the Enterprise Libraries.
            // To get a custom section using the EntLib handler. It actually returns a ConfigurationSourceSection section object....
            ConfigurationSourceSection entlibSection = (ConfigurationSourceSection)ConfigurationManager.GetSection( "enterpriseLibrary.ConfigurationSource" );
            if( entlibSection == null )
                throw new Exception( "Application configuration file is missing custom configuration section <enterpriseLibrary.ConfigurationSource>." );

            // This will return the particular source in this ConfigurationSourceSection
            FileConfigurationSourceElement source = (FileConfigurationSourceElement)entlibSection.Sources.Get( "PerfectusDataSource" );
            if( source == null )
                throw new Exception( "Application configuration file is missing the 'PerfectusDataSource' configuration source." );

            // This is then our path to the external config file
            string dataconfigpath = source.FilePath;
            if( String.IsNullOrEmpty( dataconfigpath ) )
                throw new Exception( "Application configuration element <enterpriseLibrary.ConfigurationSource/sources/add> is missing the attribute 'filePath'." );

            // These commands give us direct access to the external configuration file
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap( );
            fileMap.ExeConfigFilename = dataconfigpath;
            _dataConfigFile = ConfigurationManager.OpenMappedExeConfiguration( fileMap, ConfigurationUserLevel.None );

            if( _dataConfigFile == null || _dataConfigFile.HasFile == false )
                throw new Exception( "Missing configuration file : " + dataconfigpath );

            // Get Enterprise Library database settings.
            _entlibDatabaseSettings = (DatabaseSettings)_dataConfigFile.GetSection( "dataConfiguration" );
            if( _entlibDatabaseSettings == null )
                throw new Exception( "Data configuration file is missing the <dataConfiguration> configuration element." );

            // Alternative way to get config section in external file directly (read only)
            //IConfigurationSource sourcefactory = ConfigurationSourceFactory.Create( );
            //DatabaseSettings entlibDatabaseSettings0 = (DatabaseSettings)sourcefactory.GetSection( "dataConfiguration" );
            //DatabaseSettings entlibDatabaseSettings1 = DatabaseSettings.GetDatabaseSettings( sourcefactory );
            //DatabaseSettings entlibDatabaseSettings2 = DatabaseSettings.GetDatabaseSettings( new SystemConfigurationSource( ) );
        }

        /// <summary>
        /// Sets the connection object, identified by the name, to the default connection.
        /// </summary>
        /// <param name="name">Name of the connection</param>
        private void SetDefaultConnection( string name )
        {
            foreach( PerfectusConnectionSetting connection in _connectionSettings )
                connection.Default = ( ( connection.Name == name ) ? true : false );
        }

        /// <summary>
        /// Sets the connection object passed in to the default connection.
        /// </summary>
        /// <param name="name">Name of the connection</param>
        public void SetDefaultConnection( PerfectusConnectionSetting setting )
        {
            this.DefaultDatabase = setting.Name;

            foreach( PerfectusConnectionSetting connection in _connectionSettings )
                connection.Default = ( ( connection == setting ) ? true : false );
        }

        /// <summary>
        /// Saves the external data configuration file.
        /// </summary>
        public void SaveDataConfigurationFile( )
        {
            try
            {
                if( _dataConfigFile == null || _dataConfigFile.HasFile == false )
                    return;

                if( String.IsNullOrEmpty( this.DefaultDatabase ) )
                    throw new Exception( "The default connection must have a connection name." );

                // Save the actual connection settings.
                SaveConnectionSettings( );

                // Save configuration file.
                _dataConfigFile.Save( ConfigurationSaveMode.Modified );

                // Record who changed the config file in trace log.
                Log.Info( new ID( "Administrator.DataConfigurationUpdated" ),
                            _dataConfigFile.FilePath, System.Security.Principal.WindowsIdentity.GetCurrent( ).Name );
            }
            catch( Exception ex )
            {
                throw new PerfectusException( new ID( "Administrator.DataConfigurationSaveError" ),
                    _dataConfigFile.FilePath, ex.Message );
            }
        }

        /// <summary>
        /// Clears all current configuration content within this class.
        /// </summary>
        public void ClearConfigurationData( )
        {
            // Connection settings.
            if( _connectionSettings != null )
            {
                _connectionSettings.Clear( );
                _connectionSettings = null;
            }
            // Database providers
            _databaseProviders = null;
        }

        /// <summary>
        /// Tests a database connection
        /// </summary>
        /// <param name="connect">the connection object</param>
        /// <param name="result">the result on the connection</param>
        /// <returns></returns>
        public bool TestConnection( PerfectusConnectionSetting connect, out string result )
        {
            result = String.Empty;

            // Find the provider
            DbProviderMapping providerMap = GetProvider( connect.ProviderName );

            // We must have one of these.
            if( providerMap == null )
            {
                result = "Unknown Database Provider";
                return false;
            }

           IFleet  fleet = null;

            try
            {
                // Create fleet using the provider and connection string, and simply create a transaction which will actually create a db connection
                using( fleet = FleetContainer.Create( connect.ConnectionString, providerMap.DatabaseType ) )
                {
                    fleet.BeginTransaction( );
                    fleet.Rollback( );

                    result = "Connection Successful";
                    return true;
                }
            }
            catch( Exception ex )
            {
                result = String.Format( "Failed to connect. The following exception was returned : {0}", ex.Message );
                return false;
            }
            finally
            {
                if( fleet != null )
                    fleet.Dispose( );
            }
        }

        /// <summary>
        /// Return the provider object based on the provider name.
        /// </summary>
        /// <param name="name">name on the provider as listed in data configuration file</param>
        /// <returns></returns>
        public DbProviderMapping GetProvider( string name )
        {
            // Find the provider
            DbProviderMapping providerMap = null;
            foreach( DbProviderMapping map in _entlibDatabaseSettings.ProviderMappings )
            {
                if( map.DbProviderName == name )
                    return map;
            }

            return null;
        }

        /// <summary>
        /// Will initiate the database upgrade or install wizard.
        /// </summary>
        /// <param name="connect">the connection string pbject matching the database to be upgraded or installed.</param>
        public bool UpgradeInstallDatabase( PerfectusConnectionSetting connect )
        {
            // Silently fail if no connect obj.
            if( connect == null )
                return false;

            // User must be administrator or system operator to do this.
            if( AdminClass.Instance.IsUserAuthorised( ) == false )
                throw new Exception( "You are not authorised to upgrade/install the database. Only Administrators and System Operators may perform this operation." );

            // Currently we only support sql server databases to upgrade.
            if( String.IsNullOrEmpty( connect.ProviderName ) )
                throw new Exception( "A database Provider has not been specified." );
            DbProviderMapping providerMap = GetProvider( connect.ProviderName );
            if( ( providerMap == null ) || ( providerMap.DatabaseType.Name != "SqlDatabase" ) )
                throw new Exception( "The installation and upgrading of databases is currently only available for Sql Server databases." );

            // Construct the path where the sql scripts are. 
            string sqlscriptspath = null;
            RegistryKey key = About.GetRootRegistry( );
            if( key != null )
            {
                key = key.OpenSubKey( "Perfectus Server" );
                if( key != null )
                    sqlscriptspath = Path.Combine(  key.GetValue( "InstallDir" ).ToString( ),  "SqlScripts" );
            }
            if( String.IsNullOrEmpty( sqlscriptspath ) || !Directory.Exists( sqlscriptspath ) )
                throw new Exception( String.Format( "Unable to locate database scripts directory at '{0}'", sqlscriptspath  ) );

            try
            {
                // Create database configuration object to start installation/upgrading database
                DatabaseConfiguration dataconfigobj = new DatabaseConfiguration( );

                // First pass in the configuration settings
                dataconfigobj.Start( "SqlServer",
                    (connect.Name.ToLower( ) == "sqlreporting") ? DatabaseConfiguration.ReportingDatabaseKey : DatabaseConfiguration.CoreDatabaseKey,
                    connect.Database,
                    connect.Server,
                    connect.User,
                    connect.Password,
                    sqlscriptspath, null );

                // Start wizard
                if( dataconfigobj.DoAction( ) == Utils.WizardAction.Success )
                {
                    string database, server, user, password;

                    // Collect info after a successful install/upgrade
                    dataconfigobj.End( out database, out server, out user, out password );

                    // Record who changed the config file in trace log.
                    Log.Info( new ID( "Administrator.DataBaseUpgraded" ),
                        database, ( database == connect.Database ) ? "upgraded" : "installed",
                        System.Security.Principal.WindowsIdentity.GetCurrent( ).Name );

                    // Reocrd the new settings in our connect object
                    connect.Database = database;
                    connect.Server = server;
                    connect.User = user;
                    connect.Password = password;

                    return true;
                }
                return false;
            }
            catch( Exception ex )
            {
                throw new PerfectusException( new ID( "Administrator.DataBaseUpgradeError" ), ex.Message );
            }
        }

        
        #region Private functions
        /// <summary>
        /// Having loaded the dataConfiguration file, this routine will load the database connection settings into the bindable class.
        /// It will ensure the bindable class has exactly 10 connection settings (requirement to create a fixed number of rows in the 
        /// binding datagridview table)
        /// </summary>
        private void LoadConnectionSettings( )
        {
            if( _dataConfigFile == null || _dataConfigFile.HasFile == false )
                return;

            // Create and configure the connection list to allow us to edit the list.
            _connectionSettings = new BindingList<PerfectusConnectionSetting>( );
            _connectionSettings.AllowNew = true;
            _connectionSettings.AllowEdit = true;
            _connectionSettings.AllowRemove = true;

            // Find the connectionStrings section first
            ConnectionStringsSection connectsection = _dataConfigFile.ConnectionStrings;
            if( connectsection == null )
                throw new Exception( "Data configuration file is missing the <connectionStrings> element." );

            // Scan through each connection string object and create a perfection connection setting object if it has a valid provider.
            foreach( ConnectionStringSettings setting in connectsection.ConnectionStrings )
            {
                // We only display providers listed in this configuration file. Otherwise the default connections (eg. Local Server) in machine.config will also be shown.
                if( HaveProvider( setting.ProviderName ) )
                {
                    // Create bindable connection object, set whether default, and add to list.
                    PerfectusConnectionSetting perfectusConnectObject = new PerfectusConnectionSetting( setting );
                    perfectusConnectObject.Default = ( perfectusConnectObject.Name == DefaultDatabase );
                    _connectionSettings.Add( perfectusConnectObject );
                }
            }

            // Create blank connection strings to 'buffer' this collection to exactly 10 rows. If there are more (near impossible)
            // the interface should display scroll bars.
            while( _connectionSettings.Count < 10 )
            {
                PerfectusConnectionSetting connect = new PerfectusConnectionSetting( );

                // Default the provider to the first
                if( _databaseProviders.Length > 0 )
                    connect.ProviderName = _databaseProviders[ 0 ];

                _connectionSettings.Add( connect );
            }
        }

        /// <summary>
        /// Updates the connection settings in configuration file connection section, does not save the actual config file.
        /// </summary>
        private void SaveConnectionSettings( )
        {
            // Update the official connection string settings.
            ConnectionStringsSection connectsection = _dataConfigFile.ConnectionStrings;
            if( connectsection == null )
                throw new Exception( "Data configuration file is missing the <connectionStrings> element." );

            // First remove all current connections
            foreach( ConnectionStringSettings sConnection in connectsection.ConnectionStrings )
            {
                // We only remove connection with providers listed in this configuration file. 
                if( HaveProvider( sConnection.ProviderName ) )
                    connectsection.ConnectionStrings.Remove( sConnection );
            }

            // Next insert all current connections
            foreach( PerfectusConnectionSetting pConnection in _connectionSettings )
            {
                // Important to ignore the blank rows
                if( String.IsNullOrEmpty( pConnection.Name ) )
                    continue;

                // Important to ignore blank or invalid providers.
                if( !HaveProvider( pConnection.ProviderName ) )
                    continue;

                // Add to connection string object.
                connectsection.ConnectionStrings.Add( new ConnectionStringSettings(
                    pConnection.Name, pConnection.ConnectionString, pConnection.ProviderName ) );
            }
        }

        // Quick check to see if provider exists.
        private bool HaveProvider( string name )
        {
            foreach( string providername in DatabaseProviders )
            {
                if( providername == name )
                    return true;
            }
            return false;
        }
        #endregion
    }

    /// <summary>
    /// Class to represent the database connection settings for editing.
    /// </summary>
    public class PerfectusConnectionSetting
    {
        // The official connection string object that created this.
        public ConnectionStringSettings _connectionSetting = null;

        private bool _default = false;
        public bool Default
        {
            get { return _default; }
            set { _default = value; }
        }

        private string _name = String.Empty;
        public string Name
        {
            get{ return _name; }
            set { _name = value; }
        }

        private string _server = String.Empty;
        public string Server
        {
            get{ return _server; }
            set { _server = value; }
        }

        private string _database = String.Empty;
        public string Database
        {
            get{ return _database; }
            set { _database = value; }
        }

        private string _user = String.Empty;
        public string User
        {
            get{ return _user; }
            set { _user = value; }
        }

        private string _password = String.Empty;
        public string Password
        {
            get{ return _password; }
            set { _password = value; }
        }

        private string _providerName = String.Empty;
        public string ProviderName
        {
            get { return _providerName; }
            set { _providerName = value; }
        }

        /// <summary>
        /// Returns the connection string based on whether its for a Sql Server to Oracle database.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                // NB: This introduces a configuration requirement... All db providers for Oracle MUST have the word Oracle in them.
                // Currently we only recognise 2 types of connections for the 2 dbs we support, Sql Server and Oracle. However the config file can have
                // multiple providers which must either belong to one of the dbs, or versions of them. Which provider belongs to which db is not easy to determine
                // programmatically from the config file alone, hence for now only if the provider has the text 'Oracle' in it does the code assume the connection must be for
                // the Oracle db. Otherwise we would have to restict the config file to only have 2 providers, but dev try different versions and hence different
                // providers. Alternatively we would have to allow users to edit the connection string directly as provided by the InstallationConfig.exe.
                if( ProviderName.Contains( "Oracle" ) )
                    return String.Format( @"data source={0};user id={1};password={2};validate connection=true;", Server, User, Password );
                else
                    return String.Format( @"Server={0};Database={1};Integrated Security=False;User={2};Password={3};", Server, Database, User, Password );
            }
        }

        /// <summary>
        /// Default zero parameter constructor required to allow adding new items
        /// </summary>
        public PerfectusConnectionSetting( )
        {   }

        /// <summary>
        /// Clears all member data to their default settings.
        /// </summary>
        public void Clear( )
        {
            Default = false;
            Name = String.Empty;
            Server = String.Empty;
            Database = String.Empty;
            User = String.Empty;
            Password = String.Empty;
            ProviderName = String.Empty;
        }

        /// <summary>
        /// Used to create based on existing connection string.
        /// </summary>
        /// <param name="connection">The connection string object as recording in config file</param>
        public PerfectusConnectionSetting( ConnectionStringSettings connection )
        {
            _connectionSetting = connection;

            Name = connection.Name;
            ProviderName = connection.ProviderName;

            // Create class to parse the connection string.
            System.Data.Common.DbConnectionStringBuilder builder = new System.Data.Common.DbConnectionStringBuilder( );
            builder.ConnectionString = connection.ConnectionString;

            // Determine connection values from connection string. See notes on assumptions in property ConnectionString
            if( ProviderName.Contains( "Oracle" ) )
            {
                Server = ( builder.ContainsKey( "data source" ) ) ? builder[ "data source" ].ToString( ) : String.Empty;
                User = ( builder.ContainsKey( "user id" ) ) ? builder[ "user id" ].ToString( ) : String.Empty;
                Password = ( builder.ContainsKey( "password" ) ) ? builder[ "password" ].ToString( ) : String.Empty;
            }
            else
            {
                Database = ( builder.ContainsKey( "Database" ) )  ? builder[ "Database" ].ToString( ) : String.Empty;
                Server = ( builder.ContainsKey( "Server" ) ) ? builder[ "Server" ].ToString( ) : String.Empty;
                User = ( builder.ContainsKey( "User" ) ) ? builder[ "User" ].ToString( ) : String.Empty;
                Password = ( builder.ContainsKey( "Password" ) ) ? builder[ "Password" ].ToString( ) : String.Empty;
            }
        }
    }

}


