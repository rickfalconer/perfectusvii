namespace Administrator
{
    partial class AdminConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container( );
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AdminConsole ) );
            this.topToolStrip = new System.Windows.Forms.ToolStrip( );
            this.productImage = new System.Windows.Forms.ToolStripLabel( );
            this.serverName = new System.Windows.Forms.ToolStripLabel( );
            this.leftpanel = new System.Windows.Forms.Panel( );
            this.serviceStatus = new System.Windows.Forms.Label( );
            this.serviceDisplayName = new System.Windows.Forms.Label( );
            this.pausebutton = new System.Windows.Forms.Button( );
            this.stopbutton = new System.Windows.Forms.Button( );
            this.startbutton = new System.Windows.Forms.Button( );
            this.label3 = new System.Windows.Forms.Label( );
            this.label2 = new System.Windows.Forms.Label( );
            this.label1 = new System.Windows.Forms.Label( );
            this.bevel2 = new Perfectus.Library.UserControls.Bevel( );
            this.rightpanel = new System.Windows.Forms.Panel( );
            this.transbut = new System.Windows.Forms.Button( );
            this.exitbtn = new System.Windows.Forms.Button( );
            this.configbtn = new System.Windows.Forms.Button( );
            this.servicetimer = new System.Windows.Forms.Timer( this.components );
            this.bevel1 = new Perfectus.Library.UserControls.Bevel( );
            this.topToolStrip.SuspendLayout( );
            this.leftpanel.SuspendLayout( );
            this.rightpanel.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // topToolStrip
            // 
            this.topToolStrip.AutoSize = false;
            this.topToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.topToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.productImage,
            this.serverName} );
            this.topToolStrip.Location = new System.Drawing.Point( 0, 0 );
            this.topToolStrip.Name = "topToolStrip";
            this.topToolStrip.Size = new System.Drawing.Size( 404, 60 );
            this.topToolStrip.TabIndex = 1;
            this.topToolStrip.Text = "toolStrip1";
            // 
            // productImage
            // 
            this.productImage.Image = global::Administrator.Properties.Resources.harddisk_network;
            this.productImage.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.productImage.Name = "productImage";
            this.productImage.Padding = new System.Windows.Forms.Padding( 25, 0, 0, 0 );
            this.productImage.Size = new System.Drawing.Size( 57, 57 );
            this.productImage.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // serverName
            // 
            this.serverName.Font = new System.Drawing.Font( "Tahoma", 14F );
            this.serverName.Name = "serverName";
            this.serverName.Size = new System.Drawing.Size( 119, 57 );
            this.serverName.Text = "Server Name";
            // 
            // leftpanel
            // 
            this.leftpanel.Controls.Add( this.serviceStatus );
            this.leftpanel.Controls.Add( this.serviceDisplayName );
            this.leftpanel.Controls.Add( this.pausebutton );
            this.leftpanel.Controls.Add( this.stopbutton );
            this.leftpanel.Controls.Add( this.startbutton );
            this.leftpanel.Controls.Add( this.label3 );
            this.leftpanel.Controls.Add( this.label2 );
            this.leftpanel.Controls.Add( this.label1 );
            this.leftpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.leftpanel.Location = new System.Drawing.Point( 0, 62 );
            this.leftpanel.Name = "leftpanel";
            this.leftpanel.Size = new System.Drawing.Size( 218, 251 );
            this.leftpanel.TabIndex = 7;
            // 
            // serviceStatus
            // 
            this.serviceStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.serviceStatus.Location = new System.Drawing.Point( 12, 109 );
            this.serviceStatus.Name = "serviceStatus";
            this.serviceStatus.Padding = new System.Windows.Forms.Padding( 0, 3, 0, 0 );
            this.serviceStatus.Size = new System.Drawing.Size( 185, 23 );
            this.serviceStatus.TabIndex = 2;
            // 
            // serviceDisplayName
            // 
            this.serviceDisplayName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.serviceDisplayName.Location = new System.Drawing.Point( 12, 56 );
            this.serviceDisplayName.Name = "serviceDisplayName";
            this.serviceDisplayName.Padding = new System.Windows.Forms.Padding( 0, 3, 0, 0 );
            this.serviceDisplayName.Size = new System.Drawing.Size( 185, 23 );
            this.serviceDisplayName.TabIndex = 1;
            // 
            // pausebutton
            // 
            this.pausebutton.Enabled = false;
            this.pausebutton.Image = global::Administrator.Properties.Resources.gear_pause;
            this.pausebutton.Location = new System.Drawing.Point( 159, 157 );
            this.pausebutton.Name = "pausebutton";
            this.pausebutton.Size = new System.Drawing.Size( 40, 40 );
            this.pausebutton.TabIndex = 5;
            this.pausebutton.UseVisualStyleBackColor = true;
            this.pausebutton.Click += new System.EventHandler( this.pausebutton_Click );
            // 
            // stopbutton
            // 
            this.stopbutton.Enabled = false;
            this.stopbutton.Image = global::Administrator.Properties.Resources.gear_stop;
            this.stopbutton.Location = new System.Drawing.Point( 85, 158 );
            this.stopbutton.Name = "stopbutton";
            this.stopbutton.Size = new System.Drawing.Size( 40, 40 );
            this.stopbutton.TabIndex = 4;
            this.stopbutton.UseVisualStyleBackColor = true;
            this.stopbutton.Click += new System.EventHandler( this.stopbutton_Click );
            // 
            // startbutton
            // 
            this.startbutton.Enabled = false;
            this.startbutton.Image = global::Administrator.Properties.Resources.gear_run;
            this.startbutton.Location = new System.Drawing.Point( 15, 157 );
            this.startbutton.Name = "startbutton";
            this.startbutton.Size = new System.Drawing.Size( 40, 41 );
            this.startbutton.TabIndex = 3;
            this.startbutton.UseVisualStyleBackColor = true;
            this.startbutton.Click += new System.EventHandler( this.startbutton_Click );
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point( 9, 96 );
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size( 43, 13 );
            this.label3.TabIndex = 3;
            this.label3.Text = "Status :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.label2.Location = new System.Drawing.Point( 9, 43 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size( 41, 13 );
            this.label2.TabIndex = 1;
            this.label2.Text = "Name :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.label1.Location = new System.Drawing.Point( 9, 13 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 130, 16 );
            this.label1.TabIndex = 0;
            this.label1.Text = "Perfectus Service";
            // 
            // bevel2
            // 
            this.bevel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.bevel2.Location = new System.Drawing.Point( 218, 62 );
            this.bevel2.Name = "bevel2";
            this.bevel2.Shape = System.Windows.Forms.Border3DSide.Right;
            this.bevel2.Size = new System.Drawing.Size( 2, 251 );
            this.bevel2.TabIndex = 8;
            // 
            // rightpanel
            // 
            this.rightpanel.Controls.Add( this.transbut );
            this.rightpanel.Controls.Add( this.exitbtn );
            this.rightpanel.Controls.Add( this.configbtn );
            this.rightpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightpanel.Location = new System.Drawing.Point( 220, 62 );
            this.rightpanel.Name = "rightpanel";
            this.rightpanel.Size = new System.Drawing.Size( 184, 251 );
            this.rightpanel.TabIndex = 9;
            // 
            // transbut
            // 
            this.transbut.Location = new System.Drawing.Point( 19, 56 );
            this.transbut.Name = "transbut";
            this.transbut.Size = new System.Drawing.Size( 150, 30 );
            this.transbut.TabIndex = 7;
            this.transbut.Tag = "AdminTrans";
            this.transbut.Text = "Transactions";
            this.transbut.UseVisualStyleBackColor = true;
            this.transbut.Click += new System.EventHandler( this.ApplicationInvoke_Click );
            // 
            // exitbtn
            // 
            this.exitbtn.Location = new System.Drawing.Point( 19, 206 );
            this.exitbtn.Name = "exitbtn";
            this.exitbtn.Size = new System.Drawing.Size( 150, 30 );
            this.exitbtn.TabIndex = 8;
            this.exitbtn.Text = "Exit";
            this.exitbtn.UseVisualStyleBackColor = true;
            this.exitbtn.Click += new System.EventHandler( this.exitbtn_Click );
            // 
            // configbtn
            // 
            this.configbtn.Location = new System.Drawing.Point( 19, 13 );
            this.configbtn.Name = "configbtn";
            this.configbtn.Size = new System.Drawing.Size( 150, 30 );
            this.configbtn.TabIndex = 6;
            this.configbtn.Tag = "AdminConfig";
            this.configbtn.Text = "System Configuration";
            this.configbtn.UseVisualStyleBackColor = true;
            this.configbtn.Click += new System.EventHandler( this.ApplicationInvoke_Click );
            // 
            // servicetimer
            // 
            this.servicetimer.Interval = 1000;
            this.servicetimer.Tick += new System.EventHandler( this.servicetimer_Tick );
            // 
            // bevel1
            // 
            this.bevel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bevel1.Location = new System.Drawing.Point( 0, 60 );
            this.bevel1.Name = "bevel1";
            this.bevel1.Size = new System.Drawing.Size( 404, 2 );
            this.bevel1.TabIndex = 10;
            // 
            // AdminConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 404, 313 );
            this.Controls.Add( this.rightpanel );
            this.Controls.Add( this.bevel2 );
            this.Controls.Add( this.leftpanel );
            this.Controls.Add( this.bevel1 );
            this.Controls.Add( this.topToolStrip );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ( (System.Drawing.Icon)( resources.GetObject( "$this.Icon" ) ) );
            this.MaximizeBox = false;
            this.Name = "AdminConsole";
            this.Text = "Perfectus Administration Console";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.AdminConsole_FormClosing );
            this.topToolStrip.ResumeLayout( false );
            this.topToolStrip.PerformLayout( );
            this.leftpanel.ResumeLayout( false );
            this.leftpanel.PerformLayout( );
            this.rightpanel.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ToolStrip topToolStrip;
        private System.Windows.Forms.ToolStripLabel productImage;
        private System.Windows.Forms.ToolStripLabel serverName;
        private System.Windows.Forms.Panel leftpanel;
        private Perfectus.Library.UserControls.Bevel bevel2;
        private System.Windows.Forms.Panel rightpanel;
        private System.Windows.Forms.Button configbtn;
        private System.Windows.Forms.Button exitbtn;
        private System.Windows.Forms.Button transbut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button startbutton;
        private System.Windows.Forms.Button stopbutton;
        private System.Windows.Forms.Button pausebutton;
        private System.Windows.Forms.Label serviceStatus;
        private System.Windows.Forms.Label serviceDisplayName;
        private System.Windows.Forms.Timer servicetimer;
        private Perfectus.Library.UserControls.Bevel bevel1;
    }
}