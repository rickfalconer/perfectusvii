namespace Administrator
{
    partial class AdminTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container( );
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle( );
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle( );
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle( );
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle( );
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AdminTrans ) );
            this.topToolStrip = new System.Windows.Forms.ToolStrip( );
            this.productImage = new System.Windows.Forms.ToolStripLabel( );
            this.serverName = new System.Windows.Forms.ToolStripLabel( );
            this.closeButton = new System.Windows.Forms.ToolStripButton( );
            this.settingsButton = new System.Windows.Forms.ToolStripButton( );
            this.periodButton = new System.Windows.Forms.ToolStripDropDownButton( );
            this.filterButton = new System.Windows.Forms.ToolStripDropDownButton( );
            this.separatorFilter = new System.Windows.Forms.ToolStripSeparator( );
            this.clearFilter = new System.Windows.Forms.ToolStripMenuItem( );
            this.defaultFilter = new System.Windows.Forms.ToolStripMenuItem( );
            this.deleteFilter = new System.Windows.Forms.ToolStripMenuItem( );
            this.modeButton = new System.Windows.Forms.ToolStripButton( );
            this.refreshButton = new System.Windows.Forms.ToolStripButton( );
            this.splitContainer = new System.Windows.Forms.SplitContainer( );
            this.toppanel = new System.Windows.Forms.Panel( );
            this.packageInstanceGridView = new System.Windows.Forms.DataGridView( );
            this.statusStripTop = new System.Windows.Forms.StatusStrip( );
            this.viewprompt = new System.Windows.Forms.ToolStripStatusLabel( );
            this.viewLabel = new System.Windows.Forms.ToolStripStatusLabel( );
            this.filterprompt = new System.Windows.Forms.ToolStripStatusLabel( );
            this.filterLabel = new System.Windows.Forms.ToolStripStatusLabel( );
            this.periodprompt = new System.Windows.Forms.ToolStripStatusLabel( );
            this.periodLabel = new System.Windows.Forms.ToolStripStatusLabel( );
            this.bottompanel = new System.Windows.Forms.Panel( );
            this.btmrightpanel = new System.Windows.Forms.Panel( );
            this.propertiesGridView = new System.Windows.Forms.DataGridView( );
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn( );
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn( );
            this.splitter1 = new System.Windows.Forms.Splitter( );
            this.btmleftpanel = new System.Windows.Forms.Panel( );
            this.distributortreeView = new System.Windows.Forms.TreeView( );
            this.statusStrip = new System.Windows.Forms.StatusStrip( );
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar( );
            this.windowtimer = new System.Windows.Forms.Timer( this.components );
            this.imageList = new System.Windows.Forms.ImageList( this.components );
            this.loadTimer = new System.Windows.Forms.Timer( this.components );
            this.topToolStrip.SuspendLayout( );
            this.splitContainer.Panel1.SuspendLayout( );
            this.splitContainer.Panel2.SuspendLayout( );
            this.splitContainer.SuspendLayout( );
            this.toppanel.SuspendLayout( );
            ( (System.ComponentModel.ISupportInitialize)( this.packageInstanceGridView ) ).BeginInit( );
            this.statusStripTop.SuspendLayout( );
            this.bottompanel.SuspendLayout( );
            this.btmrightpanel.SuspendLayout( );
            ( (System.ComponentModel.ISupportInitialize)( this.propertiesGridView ) ).BeginInit( );
            this.btmleftpanel.SuspendLayout( );
            this.statusStrip.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // topToolStrip
            // 
            this.topToolStrip.AutoSize = false;
            this.topToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.topToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.productImage,
            this.serverName,
            this.closeButton,
            this.settingsButton,
            this.periodButton,
            this.filterButton,
            this.modeButton,
            this.refreshButton} );
            this.topToolStrip.Location = new System.Drawing.Point( 0, 0 );
            this.topToolStrip.Name = "topToolStrip";
            this.topToolStrip.Size = new System.Drawing.Size( 792, 60 );
            this.topToolStrip.TabIndex = 1;
            this.topToolStrip.Text = "toolStrip1";
            // 
            // productImage
            // 
            this.productImage.Image = global::Administrator.Properties.Resources.harddisk_network;
            this.productImage.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.productImage.Name = "productImage";
            this.productImage.Padding = new System.Windows.Forms.Padding( 25, 0, 0, 0 );
            this.productImage.Size = new System.Drawing.Size( 57, 57 );
            this.productImage.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // serverName
            // 
            this.serverName.Font = new System.Drawing.Font( "Tahoma", 14F );
            this.serverName.Name = "serverName";
            this.serverName.Size = new System.Drawing.Size( 119, 57 );
            this.serverName.Text = "Server Name";
            // 
            // closeButton
            // 
            this.closeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.closeButton.AutoSize = false;
            this.closeButton.Image = global::Administrator.Properties.Resources.gear_stop;
            this.closeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.closeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size( 60, 57 );
            this.closeButton.Text = "Close";
            this.closeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.closeButton.Click += new System.EventHandler( this.closeButton_Click );
            // 
            // settingsButton
            // 
            this.settingsButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.settingsButton.AutoSize = false;
            this.settingsButton.Image = global::Administrator.Properties.Resources.gear_preferences;
            this.settingsButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.settingsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.settingsButton.Name = "settingsButton";
            this.settingsButton.Size = new System.Drawing.Size( 60, 57 );
            this.settingsButton.Text = "Settings";
            this.settingsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.settingsButton.Click += new System.EventHandler( this.settingsButton_Click );
            // 
            // periodButton
            // 
            this.periodButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.periodButton.AutoSize = false;
            this.periodButton.Image = global::Administrator.Properties.Resources.gear_time;
            this.periodButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.periodButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.periodButton.Name = "periodButton";
            this.periodButton.Size = new System.Drawing.Size( 60, 57 );
            this.periodButton.Text = "Period";
            this.periodButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.periodButton.ToolTipText = "Select transaction time period";
            this.periodButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler( this.periodButton_DropDownItemClicked );
            // 
            // filterButton
            // 
            this.filterButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.filterButton.AutoSize = false;
            this.filterButton.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.separatorFilter,
            this.clearFilter,
            this.defaultFilter,
            this.deleteFilter} );
            this.filterButton.Image = global::Administrator.Properties.Resources.gear_view;
            this.filterButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.filterButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filterButton.Name = "filterButton";
            this.filterButton.Size = new System.Drawing.Size( 60, 57 );
            this.filterButton.Text = "Filter";
            this.filterButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.filterButton.ToolTipText = "Select filter to limit transactions.";
            this.filterButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler( this.filterButton_DropDownItemClicked );
            // 
            // separatorFilter
            // 
            this.separatorFilter.Name = "separatorFilter";
            this.separatorFilter.Size = new System.Drawing.Size( 180, 6 );
            // 
            // clearFilter
            // 
            this.clearFilter.Name = "clearFilter";
            this.clearFilter.Size = new System.Drawing.Size( 183, 22 );
            this.clearFilter.Text = "Clear Current Filter";
            this.clearFilter.ToolTipText = "Removes the current filter.";
            // 
            // defaultFilter
            // 
            this.defaultFilter.Name = "defaultFilter";
            this.defaultFilter.Size = new System.Drawing.Size( 183, 22 );
            this.defaultFilter.Text = "Set As Default";
            this.defaultFilter.ToolTipText = "Set the current filter as the default filter.";
            // 
            // deleteFilter
            // 
            this.deleteFilter.Name = "deleteFilter";
            this.deleteFilter.Size = new System.Drawing.Size( 183, 22 );
            this.deleteFilter.Text = "Delete Current Filter";
            this.deleteFilter.ToolTipText = "Will delete the current filter from the list.";
            // 
            // modeButton
            // 
            this.modeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.modeButton.AutoSize = false;
            this.modeButton.Image = global::Administrator.Properties.Resources.gear_replace;
            this.modeButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.modeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.modeButton.Name = "modeButton";
            this.modeButton.Size = new System.Drawing.Size( 60, 57 );
            this.modeButton.Text = "Trans";
            this.modeButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.modeButton.ToolTipText = "Toggle Package & Transaction Views";
            this.modeButton.Click += new System.EventHandler( this.modeButton_Click );
            // 
            // refreshButton
            // 
            this.refreshButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.refreshButton.AutoSize = false;
            this.refreshButton.Image = global::Administrator.Properties.Resources.gear_refresh;
            this.refreshButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.refreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size( 60, 57 );
            this.refreshButton.Text = "Refresh";
            this.refreshButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.refreshButton.ToolTipText = "Reloads the selection.";
            this.refreshButton.Click += new System.EventHandler( this.refreshButton_Click );
            // 
            // splitContainer
            // 
            this.splitContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point( 0, 60 );
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add( this.toppanel );
            this.splitContainer.Panel1.Controls.Add( this.statusStripTop );
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add( this.bottompanel );
            this.splitContainer.Panel2.Controls.Add( this.statusStrip );
            this.splitContainer.Size = new System.Drawing.Size( 792, 506 );
            this.splitContainer.SplitterDistance = 330;
            this.splitContainer.TabIndex = 2;
            // 
            // toppanel
            // 
            this.toppanel.Controls.Add( this.packageInstanceGridView );
            this.toppanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toppanel.Location = new System.Drawing.Point( 0, 28 );
            this.toppanel.Name = "toppanel";
            this.toppanel.Size = new System.Drawing.Size( 790, 300 );
            this.toppanel.TabIndex = 0;
            // 
            // packageInstanceGridView
            // 
            this.packageInstanceGridView.AllowUserToAddRows = false;
            this.packageInstanceGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            this.packageInstanceGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.packageInstanceGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.packageInstanceGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.packageInstanceGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.packageInstanceGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.packageInstanceGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.packageInstanceGridView.ColumnHeadersHeight = 35;
            this.packageInstanceGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.packageInstanceGridView.DataMember = "PackageInstances";
            this.packageInstanceGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.packageInstanceGridView.Location = new System.Drawing.Point( 0, 0 );
            this.packageInstanceGridView.MultiSelect = false;
            this.packageInstanceGridView.Name = "packageInstanceGridView";
            this.packageInstanceGridView.ReadOnly = true;
            this.packageInstanceGridView.RowHeadersVisible = false;
            this.packageInstanceGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.packageInstanceGridView.Size = new System.Drawing.Size( 790, 300 );
            this.packageInstanceGridView.TabIndex = 0;
            this.packageInstanceGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler( this.packageInstanceGridView_CellMouseUp );
            this.packageInstanceGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler( this.packageInstanceGridView_ColumnHeaderMouseClick );
            this.packageInstanceGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler( this.packageInstanceGridView_CellFormatting );
            this.packageInstanceGridView.SelectionChanged += new System.EventHandler( this.packageInstanceGridView_SelectionChanged );
            // 
            // statusStripTop
            // 
            this.statusStripTop.AutoSize = false;
            this.statusStripTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStripTop.Items.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.viewprompt,
            this.viewLabel,
            this.filterprompt,
            this.filterLabel,
            this.periodprompt,
            this.periodLabel} );
            this.statusStripTop.Location = new System.Drawing.Point( 0, 0 );
            this.statusStripTop.Name = "statusStripTop";
            this.statusStripTop.Size = new System.Drawing.Size( 790, 28 );
            this.statusStripTop.TabIndex = 1;
            this.statusStripTop.Text = "statusStrip1";
            // 
            // viewprompt
            // 
            this.viewprompt.BackColor = System.Drawing.SystemColors.MenuBar;
            this.viewprompt.Margin = new System.Windows.Forms.Padding( 25, 3, 0, 5 );
            this.viewprompt.Name = "viewprompt";
            this.viewprompt.Size = new System.Drawing.Size( 36, 20 );
            this.viewprompt.Text = "View :";
            // 
            // viewLabel
            // 
            this.viewLabel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.viewLabel.BorderSides = ( (System.Windows.Forms.ToolStripStatusLabelBorderSides)( ( ( ( System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom ) ) );
            this.viewLabel.Margin = new System.Windows.Forms.Padding( 5, 3, 0, 5 );
            this.viewLabel.Name = "viewLabel";
            this.viewLabel.Padding = new System.Windows.Forms.Padding( 10, 0, 10, 0 );
            this.viewLabel.Size = new System.Drawing.Size( 24, 20 );
            // 
            // filterprompt
            // 
            this.filterprompt.BackColor = System.Drawing.SystemColors.MenuBar;
            this.filterprompt.Margin = new System.Windows.Forms.Padding( 25, 3, 0, 5 );
            this.filterprompt.Name = "filterprompt";
            this.filterprompt.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.filterprompt.Size = new System.Drawing.Size( 38, 20 );
            this.filterprompt.Text = "Filter :";
            // 
            // filterLabel
            // 
            this.filterLabel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.filterLabel.BorderSides = ( (System.Windows.Forms.ToolStripStatusLabelBorderSides)( ( ( ( System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom ) ) );
            this.filterLabel.Margin = new System.Windows.Forms.Padding( 5, 3, 0, 5 );
            this.filterLabel.Name = "filterLabel";
            this.filterLabel.Padding = new System.Windows.Forms.Padding( 10, 0, 10, 0 );
            this.filterLabel.Size = new System.Drawing.Size( 24, 20 );
            this.filterLabel.ToolTipText = "The current filter.";
            // 
            // periodprompt
            // 
            this.periodprompt.BackColor = System.Drawing.SystemColors.MenuBar;
            this.periodprompt.Margin = new System.Windows.Forms.Padding( 25, 3, 0, 5 );
            this.periodprompt.Name = "periodprompt";
            this.periodprompt.Size = new System.Drawing.Size( 72, 20 );
            this.periodprompt.Text = "Time Period : ";
            // 
            // periodLabel
            // 
            this.periodLabel.BackColor = System.Drawing.SystemColors.MenuBar;
            this.periodLabel.BorderSides = ( (System.Windows.Forms.ToolStripStatusLabelBorderSides)( ( ( ( System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right )
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom ) ) );
            this.periodLabel.Margin = new System.Windows.Forms.Padding( 5, 3, 0, 5 );
            this.periodLabel.Name = "periodLabel";
            this.periodLabel.Padding = new System.Windows.Forms.Padding( 10, 0, 10, 0 );
            this.periodLabel.Size = new System.Drawing.Size( 24, 20 );
            this.periodLabel.ToolTipText = "The current time period.";
            // 
            // bottompanel
            // 
            this.bottompanel.Controls.Add( this.btmrightpanel );
            this.bottompanel.Controls.Add( this.splitter1 );
            this.bottompanel.Controls.Add( this.btmleftpanel );
            this.bottompanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottompanel.Location = new System.Drawing.Point( 0, 0 );
            this.bottompanel.Name = "bottompanel";
            this.bottompanel.Padding = new System.Windows.Forms.Padding( 0, 0, 0, 10 );
            this.bottompanel.Size = new System.Drawing.Size( 790, 147 );
            this.bottompanel.TabIndex = 0;
            // 
            // btmrightpanel
            // 
            this.btmrightpanel.Controls.Add( this.propertiesGridView );
            this.btmrightpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btmrightpanel.Location = new System.Drawing.Point( 388, 0 );
            this.btmrightpanel.Name = "btmrightpanel";
            this.btmrightpanel.Padding = new System.Windows.Forms.Padding( 10, 10, 10, 0 );
            this.btmrightpanel.Size = new System.Drawing.Size( 402, 137 );
            this.btmrightpanel.TabIndex = 2;
            // 
            // propertiesGridView
            // 
            this.propertiesGridView.AllowUserToAddRows = false;
            this.propertiesGridView.AllowUserToDeleteRows = false;
            this.propertiesGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.propertiesGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.propertiesGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.propertiesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.propertiesGridView.ColumnHeadersVisible = false;
            this.propertiesGridView.Columns.AddRange( new System.Windows.Forms.DataGridViewColumn[ ] {
            this.Column1,
            this.Column2} );
            this.propertiesGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertiesGridView.GridColor = System.Drawing.Color.Black;
            this.propertiesGridView.Location = new System.Drawing.Point( 10, 10 );
            this.propertiesGridView.MultiSelect = false;
            this.propertiesGridView.Name = "propertiesGridView";
            this.propertiesGridView.ReadOnly = true;
            this.propertiesGridView.RowHeadersVisible = false;
            this.propertiesGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.propertiesGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.propertiesGridView.ShowCellErrors = false;
            this.propertiesGridView.ShowCellToolTips = false;
            this.propertiesGridView.ShowEditingIcon = false;
            this.propertiesGridView.ShowRowErrors = false;
            this.propertiesGridView.Size = new System.Drawing.Size( 382, 127 );
            this.propertiesGridView.TabIndex = 0;
            // 
            // Column1
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.Column1.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 150;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point( 385, 0 );
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size( 3, 137 );
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // btmleftpanel
            // 
            this.btmleftpanel.Controls.Add( this.distributortreeView );
            this.btmleftpanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.btmleftpanel.Location = new System.Drawing.Point( 0, 0 );
            this.btmleftpanel.Name = "btmleftpanel";
            this.btmleftpanel.Padding = new System.Windows.Forms.Padding( 0, 0, 25, 0 );
            this.btmleftpanel.Size = new System.Drawing.Size( 385, 137 );
            this.btmleftpanel.TabIndex = 0;
            // 
            // distributortreeView
            // 
            this.distributortreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.distributortreeView.CausesValidation = false;
            this.distributortreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.distributortreeView.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.distributortreeView.FullRowSelect = true;
            this.distributortreeView.ItemHeight = 20;
            this.distributortreeView.Location = new System.Drawing.Point( 0, 0 );
            this.distributortreeView.Margin = new System.Windows.Forms.Padding( 0 );
            this.distributortreeView.Name = "distributortreeView";
            this.distributortreeView.Size = new System.Drawing.Size( 360, 137 );
            this.distributortreeView.TabIndex = 1;
            this.distributortreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler( this.distributortreeView_AfterSelect );
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.progressBar} );
            this.statusStrip.Location = new System.Drawing.Point( 0, 147 );
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size( 790, 23 );
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size( 100, 17 );
            // 
            // windowtimer
            // 
            this.windowtimer.Tick += new System.EventHandler( this.windowtimer_Tick );
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size( 16, 16 );
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // loadTimer
            // 
            this.loadTimer.Tick += new System.EventHandler( this.loadTimer_Tick );
            // 
            // AdminTrans
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size( 792, 566 );
            this.Controls.Add( this.splitContainer );
            this.Controls.Add( this.topToolStrip );
            this.Icon = ( (System.Drawing.Icon)( resources.GetObject( "$this.Icon" ) ) );
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size( 800, 600 );
            this.Name = "AdminTrans";
            this.Text = "Perfectus Transaction Administration";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.topToolStrip.ResumeLayout( false );
            this.topToolStrip.PerformLayout( );
            this.splitContainer.Panel1.ResumeLayout( false );
            this.splitContainer.Panel2.ResumeLayout( false );
            this.splitContainer.Panel2.PerformLayout( );
            this.splitContainer.ResumeLayout( false );
            this.toppanel.ResumeLayout( false );
            ( (System.ComponentModel.ISupportInitialize)( this.packageInstanceGridView ) ).EndInit( );
            this.statusStripTop.ResumeLayout( false );
            this.statusStripTop.PerformLayout( );
            this.bottompanel.ResumeLayout( false );
            this.btmrightpanel.ResumeLayout( false );
            ( (System.ComponentModel.ISupportInitialize)( this.propertiesGridView ) ).EndInit( );
            this.btmleftpanel.ResumeLayout( false );
            this.statusStrip.ResumeLayout( false );
            this.statusStrip.PerformLayout( );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ToolStrip topToolStrip;
        private System.Windows.Forms.ToolStripLabel productImage;
        private System.Windows.Forms.ToolStripLabel serverName;
        private System.Windows.Forms.ToolStripButton closeButton;
        private System.Windows.Forms.ToolStripButton settingsButton;
        private System.Windows.Forms.ToolStripDropDownButton periodButton;
        private System.Windows.Forms.ToolStripDropDownButton filterButton;
        private System.Windows.Forms.ToolStripButton refreshButton;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel toppanel;
        private System.Windows.Forms.Panel bottompanel;
        private System.Windows.Forms.DataGridView packageInstanceGridView;
        private System.Windows.Forms.Timer windowtimer;
        private System.Windows.Forms.TreeView distributortreeView;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripButton modeButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripMenuItem clearFilter;
        private System.Windows.Forms.ToolStripSeparator separatorFilter;
        private System.Windows.Forms.ToolStripMenuItem defaultFilter;
        private System.Windows.Forms.ToolStripMenuItem deleteFilter;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.Timer loadTimer;
        private System.Windows.Forms.Panel btmrightpanel;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel btmleftpanel;
        private System.Windows.Forms.DataGridView propertiesGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.StatusStrip statusStripTop;
        private System.Windows.Forms.ToolStripStatusLabel filterprompt;
        private System.Windows.Forms.ToolStripStatusLabel filterLabel;
        private System.Windows.Forms.ToolStripStatusLabel periodprompt;
        private System.Windows.Forms.ToolStripStatusLabel periodLabel;
        private System.Windows.Forms.ToolStripStatusLabel viewprompt;
        private System.Windows.Forms.ToolStripStatusLabel viewLabel;
    }
}