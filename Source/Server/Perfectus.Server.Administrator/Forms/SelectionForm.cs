using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Administrator
{
    public partial class SelectionForm : BaseFormView
    {
        // Our slection class object
        private  SelectionClass _localSelectionObj = null;
        // The current view
        private AdminClass.dataViewType _currentView = AdminClass.dataViewType.NONE;


        public SelectionForm( )
        {
            InitializeForm( );

            // Create default selection object
            _localSelectionObj = new SelectionClass( AdminClass.dataViewType.PACKAGE );
        }

        public SelectionForm( SelectionClass selectionObj )
        {
            InitializeForm( );

            // Create copy of selection object
            if( selectionObj != null )
                _localSelectionObj = (SelectionClass)selectionObj.Clone( );
            else
                _localSelectionObj = new SelectionClass( AdminClass.dataViewType.PACKAGE );
        }

        // Common code for constructors
        private void InitializeForm( )
        {
            InitializeComponent( );
        }

        #region Base class overrides.
        public override void LoadInterface( )
        {
            // Configure interface based on our owners grid view columns
            ConfigureInterface( ((AdminTrans)Owner).GetPackageInstanceColumns( ) );
        }
        #endregion

        #region Event Handlers.
        // Event to signal the model has changed.
        public override void ModelChange( object sender, ModelChangeEventArgs e )
        {
            if( e.ModelObj == null )
                return;

            // Important to ignore enents from ourselves
            // Make sure this is from our parent
            if( sender == this && sender != this.Owner )
                return;

            // The selection class object.
            if( e.ModelObj.GetType( ) == typeof( SelectionClass ) )
            {
                // Create copy of selection object
                _localSelectionObj = (SelectionClass)( (SelectionClass)e.ModelObj ).Clone( );

                // Update interface
                this.LoadInterface( );
            }
            
            // The mode/view of our parent has changed.
            else if( e.ModelObj.GetType( ) == typeof( AdminClass.dataViewType ) )
            {
                _localSelectionObj.CurrentView = (AdminClass.dataViewType)e.ModelObj;

                // Save current settings in memory first... then reload interface only once owner has updated itself.
                loadSelections( );
            }

            // Package/Transaction grid has changed.
            else if( e.ModelObj.GetType( ) == typeof( DataGridViewColumnCollection ) )
            {
                // Configure interface based on our owners grid view columns
                ConfigureInterface( (DataGridViewColumnCollection)e.ModelObj );
            }

                // The sort order has changed.
            else if( e.ModelObj.GetType( ) == typeof( ListSortDirection ) )
            {
                _localSelectionObj.SetSortByColumn( ((AdminTrans)Owner).SelectionObj.GetSortByColumn(  ) );
                _localSelectionObj.SetSortOrder( (ListSortDirection)e.ModelObj );
            }

            // The time period object.
            if( e.ModelObj.GetType( ) == typeof( TimePeriodClass ) )
            {
                // Update our object.
                _localSelectionObj.TimePeriod = (TimePeriodClass)((TimePeriodClass)e.ModelObj).Clone( );
            }

        }

        // The form gains focus, ensure it has the correct opacity.
        private void SelectionForm_Activated( object sender, EventArgs e )
        {
            // This prevents the flash on loading.
            this.Opacity = 0.5;
        }

        // The user selects a specific filter column.
        private void filtercomboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            DataGridViewColumn column = ((DataGridViewColumn)(((ComboBox)sender).SelectedItem));
            if( column == null )
                return;

            // Swap the text and date/time entry fields around.
            if( column.ValueType == typeof( DateTime ) )
                ShowDateFilterControls( );
            else
                ShowTextFilterControls( );
        }

        // On clicking the refresh button, construct new select object and update the parent forms.
        private void refreshbutton_Click( object sender, EventArgs e )
        {
            // Clear the name of the filter.
            _localSelectionObj.Name = saveastextBox.Text;

            // Load the selections in the interface into our object 
            loadSelections( );

            // Notify other windows of this change.
            Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( _localSelectionObj ) );
        }

        // User has requested to save a selection.
        private void savebutton_Click( object sender, EventArgs e )
        {
            try
            {
                // Get the name of the filter.
                _localSelectionObj.Name = saveastextBox.Text;
                if( String.IsNullOrEmpty( _localSelectionObj.Name ) )
                    throw new Exception( "You must specify a 'Save As' name first." );

                // Confirm
                if( MessageBox.Show( "Are you sure you want to record current filter selections?", "Saving Filter",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Question ) == DialogResult.Cancel )
                    return;

                // Load the selections in the interface into our object 
                loadSelections( );

                // Add the filter.
                ( (AdminTrans)Owner ).TransactionClass.Filters.AddFilter( _localSelectionObj );

                // Display a wait cursor
                Cursor.Current = Cursors.WaitCursor;

                // Save the filter.
                ( (AdminTrans)Owner ).TransactionClass.SaveFilters( );

                // Display default cursor
                Cursor.Current = Cursors.Default;

                // Clear text box and notify.
                saveastextBox.Clear( );
                MessageBox.Show( "Filter has been successfully saved.", "Saving Filter", MessageBoxButtons.OK, MessageBoxIcon.Information );

                // Notify other windows of this change.
                Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( ( (AdminTrans)Owner ).TransactionClass.Filters ) );

                // Log trace
                // .....
            }
            catch( Exception ex )
            {
                // Log trace MUST BE IN TRANS CLASS.
                // .....

                // Notify user.
                this.ShowErrorMessage( "Failed to save filter: " + ex.Message );
            }
            finally
            {
                // Display a wait cursor
                Cursor.Current = Cursors.Default;
            }
        }

        // User clicked the close button.
        private void closebutton_Click( object sender, EventArgs e )
        {
            this.Close( );
        }
        #endregion

        // Configure interface controls
        private void ConfigureInterface( DataGridViewColumnCollection columns  )
        {
            try
            {
                this.SuspendLayout( );

                // This is the view we will load.
                _currentView = _localSelectionObj.CurrentView;

                // Compile list of columns
                List<DataGridViewColumn> columncollection = new List<DataGridViewColumn>( );
                foreach( DataGridViewColumn col in columns )
                {
                    if( !col.Visible )
                        continue;

                    columncollection.Add( col );
                }

                // Load the list of possible columns to filter by.
                filtercomboBox.DisplayMember = "HeaderText";
                filtercomboBox.ValueMember = "DataPropertyName";
                filtercomboBox.DataSource = columncollection;

                // Get dictionary of possible status's
                Dictionary<string, string> statusvalues = null;
                if( _currentView == AdminClass.dataViewType.PACKAGE )
                    statusvalues = AdminClass.Instance.PackageStatus;
                else
                    statusvalues = AdminClass.Instance.JobStatus;

                // Clear current list of checkboxes
                this.statusgroupBox.Controls.Clear( );

                // Load the list of possible job status's
                int xpos = this.statusgroupBox.Location.X;
                int ypos = this.statusgroupBox.Location.X;
                foreach( KeyValuePair<string, string> kvp in statusvalues )
                {
                    CheckBox checkBox = new CheckBox( );
                    checkBox.Name = kvp.Key;
                    checkBox.Text = kvp.Value;
                    checkBox.AutoSize = true;
                    checkBox.Location = new Point( xpos, ypos );
                    checkBox.Size = new System.Drawing.Size( 80, 17 );
                    checkBox.TabIndex = 0;
                    checkBox.UseVisualStyleBackColor = true;

                    // Conditionally check the box....
                    checkBox.Checked = !( _localSelectionObj.GetStatusSelect( _currentView ).Contains( kvp.Key ) );

                    this.statusgroupBox.Controls.Add( checkBox );
                    ypos += 20;
                }

                // Default form based on the selection Obj.
                if( !String.IsNullOrEmpty( _localSelectionObj.FilterTextColumn ) )
                {
                    filtercomboBox.SelectedValue = _localSelectionObj.FilterTextColumn;
                    ShowTextFilterControls( );
                }
                else if( !String.IsNullOrEmpty( _localSelectionObj.FilterDateColumn ) )
                {
                    filtercomboBox.SelectedValue = _localSelectionObj.FilterDateColumn;
                    ShowDateFilterControls( );
                }
                liketextBox.Text = _localSelectionObj.FilterText;
                fromdateTimePicker.Text = _localSelectionObj.FilterFromDate.HasValue ? _localSelectionObj.FilterFromDate.ToString( ) : String.Empty;
                todateTimePicker.Text = _localSelectionObj.FilterToDate.HasValue ? _localSelectionObj.FilterToDate.ToString( ) : String.Empty;

                this.ResumeLayout( );
            }
            catch( Exception ex )
            {
                // Notify user.
                this.ShowErrorMessage( ex.Message );
            }
        }

        // Load interface data into our select object.
        private void loadSelections( )
        {
            try
            {
                // Clear current values
                _localSelectionObj.FilterTextColumn = null;
                _localSelectionObj.FilterText = null;
                _localSelectionObj.FilterDateColumn = null;
                _localSelectionObj.FilterFromDate = null;
                _localSelectionObj.FilterToDate = null;

                // The corresponding column to filter by
                DataGridViewColumn column = ( (DataGridViewColumn)( filtercomboBox.SelectedItem ) );

                // Extract the relevent information.
                DateTime from = new DateTime( fromdateTimePicker.Value.Year, fromdateTimePicker.Value.Month, fromdateTimePicker.Value.Day, 0, 0, 0 );
                DateTime to = new DateTime( todateTimePicker.Value.Year, todateTimePicker.Value.Month, todateTimePicker.Value.Day, 11, 59, 59 );
                _localSelectionObj.FilterFromDate = from;
                _localSelectionObj.FilterToDate = to;
                _localSelectionObj.FilterText = liketextBox.Text.TrimEnd( '*' ).TrimStart( '*' );

                if( column != null && column.ValueType == typeof( DateTime ) )
                    _localSelectionObj.FilterDateColumn = column.DataPropertyName;
                else if( column != null && !String.IsNullOrEmpty( liketextBox.Text ) )
                    _localSelectionObj.FilterTextColumn = column.DataPropertyName;

                // Update list of status's
                List<string> templist = new List<string>( );
                foreach( Control ctrl in this.statusgroupBox.Controls )
                {
                    if( ctrl.GetType( ) != typeof( CheckBox ) )
                        continue;

                    // If NOT checked, add to list.
                    if( !( (CheckBox)ctrl ).Checked )
                        templist.Add( ( (CheckBox)ctrl ).Name );
                }
                _localSelectionObj.SetStatusSelect( templist );
            }
            catch
            {
                // Ignore if unable to .....
            }
        }

        // Makes the text filter control visible.
        private void ShowTextFilterControls( )
        {
            fromlabel.Text = "Search";
            liketextBox.Visible = true;
            tolabel.Visible = false;
            fromdateTimePicker.Visible = false;
            todateTimePicker.Visible = false;
        }

        // Makes the date filter controls visible.
        private void ShowDateFilterControls( )
        {
            fromlabel.Text = "From";
            liketextBox.Visible = false;
            tolabel.Visible = true;
            fromdateTimePicker.Visible = true;
            todateTimePicker.Visible = true;
        }
    }
}