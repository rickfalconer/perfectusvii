using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.ServiceProcess;



namespace Administrator
{
    public partial class AdminConsole : BaseFormView
    {
        // The Perfectus Coordinator Service
        private const string SERVICENAME = "Perfectus.Server.ProcessCoordinator.Service";

        public AdminConsole( )
        {
            InitializeComponent();
        }

        #region Event Handlers.
        // Click event on buttons that invoke forms to host sub-instances of the administration application.
        private void ApplicationInvoke_Click( object sender, EventArgs e )
        {
            // This is the base class of any form we are going to create.
            BaseFormView formappl = null;

            // Create the appropriate form based on the button tag value.
            switch( (string)( (Button)sender ).Tag )
            {
                case "AdminConfig":
                    formappl = new AdminConfig( );
                    break;
                case "AdminTrans":
                    formappl = new AdminTrans( );
                    break;
            }

            if( formappl == null )
                return;

            // If we are not allowed to have multiple instances of the same window, check the form does not already
            // exist, and if so activate it rather than creating a new window.
            if( !formappl.AllowMultiInstances )
            {
                foreach( Form frm in Application.OpenForms )
                {
                    if( frm.Name == formappl.Name )
                    {
                        frm.Activate( );
                        // Dispose of this form as we are not going to use the form.
                        formappl.Dispose( );
                        return;
                    }
                }
            }

            // Finally open up the form and return.
            formappl.Show( );
        }

        // Click event on Exit button.
        private void exitbtn_Click( object sender, EventArgs e )
        {
            //Close this and child windows.
            this.Close( );
        }

        // User clicks the start button on the service.
        private void startbutton_Click( object sender, EventArgs e )
        {
            try
            {
                ServiceController controller = GetServiceController( );
                controller.Start( );
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // User clicks the stop button.
        private void stopbutton_Click( object sender, EventArgs e )
        {
            try
            {
                ServiceController controller = GetServiceController( );
                controller.Stop( );
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // User clicks the pause button.
        private void pausebutton_Click( object sender, EventArgs e )
        {
            try
            {
                ServiceController controller = GetServiceController( );
                controller.Pause( );
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // Periodic check of service.
        private void servicetimer_Tick( object sender, EventArgs e )
        {
            try
            {
                ServiceController controller = GetServiceController( );

                // Load/Display service details.
                serviceDisplayName.Text = controller.DisplayName;
                serviceStatus.Text = controller.Status.ToString( );

                // Configure the service buttons.
                ConfigureServiceButtons( controller );
            }
            catch( Exception )
            {
                // Immediately stop the timer.
                servicetimer.Stop( );
                // Disable any service functionality
                ConfigureServiceButtons( null );

                // Just quitely return, maybe the service is not installed.
                return;
            }
        }

        // Form is about to close.
        private void AdminConsole_FormClosing( object sender, FormClosingEventArgs e )
        {
            // Scan through all child applications and notify that we are closing.
            foreach( Form frm in Application.OpenForms )
            {
                // If form is derived from BaseFormView, check if possible to close.
                if( frm is BaseFormView )
                {
                    if( ( (BaseFormView)frm ).CanClose( ) == false )
                    {
                        // Switch to the form.
                        frm.Activate( );
                        frm.Close( );
                        
                        // Cancel this event and return.
                        e.Cancel = true;
                        return;
                    }
                }
            }

        }
        #endregion

        #region Base class overrides.
        public override void LoadInterface( )
        {
            try
            {
                // Call base class to perform common actions.
                base.LoadInterface( );

                // Name of server in the form header section.
                serverName.Text = AdminClass.Instance.ServerName;

                // Start the service timer.
                servicetimer.Start( );
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }
        #endregion

        // Creates a service controller object.
        private ServiceController GetServiceController( )
        {
            ServiceController controller = new ServiceController( );
            controller.MachineName = ".";
            controller.ServiceName = SERVICENAME;
            return controller;
        }

        // Configures the service buttons based on the service status.
        private void ConfigureServiceButtons( ServiceController controller )
        {
            this.SuspendLayout( );

            // Initially set all buttons disabled.
            stopbutton.Enabled = false;
            pausebutton.Enabled = false;
            startbutton.Enabled = false;

            if( controller != null )
            {
                switch( controller.Status )
                {
                    case ServiceControllerStatus.ContinuePending:
                        break;
                    case ServiceControllerStatus.Paused:
                        startbutton.Enabled = true;
                        stopbutton.Enabled = true;
                        break;
                    case ServiceControllerStatus.PausePending:
                        break;
                    case ServiceControllerStatus.Running:
                        stopbutton.Enabled = true;
                        pausebutton.Enabled = true;
                        break;
                    case ServiceControllerStatus.StartPending:
                        break;
                    case ServiceControllerStatus.Stopped:
                        startbutton.Enabled = true;
                        break;
                    case ServiceControllerStatus.StopPending:
                        break;
                }

                // Final checks on button availability
                if( !controller.CanStop )
                    stopbutton.Enabled = false;
                if( !controller.CanPauseAndContinue )
                    pausebutton.Enabled = false;
            }
            this.ResumeLayout( );
        }
    }
}