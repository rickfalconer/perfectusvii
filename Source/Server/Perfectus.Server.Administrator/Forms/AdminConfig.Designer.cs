namespace Administrator
{
    partial class AdminConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle( );
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( AdminConfig ) );
            this.topToolStrip = new System.Windows.Forms.ToolStrip( );
            this.productImage = new System.Windows.Forms.ToolStripLabel( );
            this.serverName = new System.Windows.Forms.ToolStripLabel( );
            this.configTabControl = new System.Windows.Forms.TabControl( );
            this.databasetab = new System.Windows.Forms.TabPage( );
            this.databasedesc4 = new System.Windows.Forms.Label( );
            this.databasedesc3 = new System.Windows.Forms.Label( );
            this.databasedesc2 = new System.Windows.Forms.Label( );
            this.databasedesc1 = new System.Windows.Forms.Label( );
            this.dbconnectDataGridView = new System.Windows.Forms.DataGridView( );
            this.applybut = new System.Windows.Forms.Button( );
            this.cancelbut = new System.Windows.Forms.Button( );
            this.okbut = new System.Windows.Forms.Button( );
            this.bevel1 = new Perfectus.Library.UserControls.Bevel( );
            this.topToolStrip.SuspendLayout( );
            this.configTabControl.SuspendLayout( );
            this.databasetab.SuspendLayout( );
            ( (System.ComponentModel.ISupportInitialize)( this.dbconnectDataGridView ) ).BeginInit( );
            this.SuspendLayout( );
            // 
            // topToolStrip
            // 
            this.topToolStrip.AutoSize = false;
            this.topToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.topToolStrip.Items.AddRange( new System.Windows.Forms.ToolStripItem[ ] {
            this.productImage,
            this.serverName} );
            this.topToolStrip.Location = new System.Drawing.Point( 0, 0 );
            this.topToolStrip.Name = "topToolStrip";
            this.topToolStrip.Size = new System.Drawing.Size( 646, 60 );
            this.topToolStrip.TabIndex = 1;
            this.topToolStrip.Text = "toolStrip1";
            // 
            // productImage
            // 
            this.productImage.Image = global::Administrator.Properties.Resources.harddisk_network;
            this.productImage.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.productImage.Name = "productImage";
            this.productImage.Padding = new System.Windows.Forms.Padding( 25, 0, 0, 0 );
            this.productImage.Size = new System.Drawing.Size( 57, 57 );
            this.productImage.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // serverName
            // 
            this.serverName.Font = new System.Drawing.Font( "Tahoma", 14F );
            this.serverName.Name = "serverName";
            this.serverName.Size = new System.Drawing.Size( 119, 57 );
            this.serverName.Text = "Server Name";
            // 
            // configTabControl
            // 
            this.configTabControl.Controls.Add( this.databasetab );
            this.configTabControl.HotTrack = true;
            this.configTabControl.Location = new System.Drawing.Point( 7, 68 );
            this.configTabControl.Name = "configTabControl";
            this.configTabControl.SelectedIndex = 0;
            this.configTabControl.Size = new System.Drawing.Size( 627, 379 );
            this.configTabControl.TabIndex = 0;
            // 
            // databasetab
            // 
            this.databasetab.Controls.Add( this.databasedesc4 );
            this.databasetab.Controls.Add( this.databasedesc3 );
            this.databasetab.Controls.Add( this.databasedesc2 );
            this.databasetab.Controls.Add( this.databasedesc1 );
            this.databasetab.Controls.Add( this.dbconnectDataGridView );
            this.databasetab.Location = new System.Drawing.Point( 4, 22 );
            this.databasetab.Name = "databasetab";
            this.databasetab.Padding = new System.Windows.Forms.Padding( 3 );
            this.databasetab.Size = new System.Drawing.Size( 619, 353 );
            this.databasetab.TabIndex = 1;
            this.databasetab.Text = "Database";
            this.databasetab.UseVisualStyleBackColor = true;
            // 
            // databasedesc4
            // 
            this.databasedesc4.AutoSize = true;
            this.databasedesc4.Location = new System.Drawing.Point( 23, 62 );
            this.databasedesc4.Name = "databasedesc4";
            this.databasedesc4.Size = new System.Drawing.Size( 35, 13 );
            this.databasedesc4.TabIndex = 4;
            this.databasedesc4.Text = "label2";
            // 
            // databasedesc3
            // 
            this.databasedesc3.AutoSize = true;
            this.databasedesc3.Location = new System.Drawing.Point( 23, 45 );
            this.databasedesc3.Name = "databasedesc3";
            this.databasedesc3.Size = new System.Drawing.Size( 35, 13 );
            this.databasedesc3.TabIndex = 3;
            this.databasedesc3.Text = "label1";
            // 
            // databasedesc2
            // 
            this.databasedesc2.AutoSize = true;
            this.databasedesc2.Location = new System.Drawing.Point( 23, 29 );
            this.databasedesc2.Name = "databasedesc2";
            this.databasedesc2.Size = new System.Drawing.Size( 35, 13 );
            this.databasedesc2.TabIndex = 2;
            this.databasedesc2.Text = "label1";
            // 
            // databasedesc1
            // 
            this.databasedesc1.AutoSize = true;
            this.databasedesc1.Location = new System.Drawing.Point( 7, 7 );
            this.databasedesc1.Name = "databasedesc1";
            this.databasedesc1.Size = new System.Drawing.Size( 35, 13 );
            this.databasedesc1.TabIndex = 1;
            this.databasedesc1.Text = "label1";
            // 
            // dbconnectDataGridView
            // 
            this.dbconnectDataGridView.AllowUserToAddRows = false;
            this.dbconnectDataGridView.AllowUserToResizeRows = false;
            this.dbconnectDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dbconnectDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dbconnectDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dbconnectDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dbconnectDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dbconnectDataGridView.ColumnHeadersHeight = 32;
            this.dbconnectDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dbconnectDataGridView.Location = new System.Drawing.Point( 6, 85 );
            this.dbconnectDataGridView.MultiSelect = false;
            this.dbconnectDataGridView.Name = "dbconnectDataGridView";
            this.dbconnectDataGridView.RowHeadersVisible = false;
            this.dbconnectDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.dbconnectDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dbconnectDataGridView.Size = new System.Drawing.Size( 607, 267 );
            this.dbconnectDataGridView.TabIndex = 0;
            this.dbconnectDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler( this.dbconnectDataGridView_CellValueChanged );
            this.dbconnectDataGridView.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler( this.dbconnectDataGridView_CellMouseUp );
            this.dbconnectDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler( this.dbconnectDataGridView_CellFormatting );
            this.dbconnectDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler( this.dbconnectDataGridView_CellValidating );
            this.dbconnectDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler( this.dbconnectDataGridView_EditingControlShowing );
            this.dbconnectDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler( this.dbconnectDataGridView_CellContentClick );
            // 
            // applybut
            // 
            this.applybut.Location = new System.Drawing.Point( 559, 453 );
            this.applybut.Name = "applybut";
            this.applybut.Size = new System.Drawing.Size( 75, 23 );
            this.applybut.TabIndex = 1;
            this.applybut.Text = "Apply";
            this.applybut.UseVisualStyleBackColor = true;
            this.applybut.Click += new System.EventHandler( this.applybut_Click );
            // 
            // cancelbut
            // 
            this.cancelbut.CausesValidation = false;
            this.cancelbut.Location = new System.Drawing.Point( 478, 453 );
            this.cancelbut.Name = "cancelbut";
            this.cancelbut.Size = new System.Drawing.Size( 75, 23 );
            this.cancelbut.TabIndex = 2;
            this.cancelbut.Text = "Cancel";
            this.cancelbut.UseVisualStyleBackColor = true;
            this.cancelbut.Click += new System.EventHandler( this.cancelbut_Click );
            // 
            // okbut
            // 
            this.okbut.Location = new System.Drawing.Point( 397, 453 );
            this.okbut.Name = "okbut";
            this.okbut.Size = new System.Drawing.Size( 75, 23 );
            this.okbut.TabIndex = 3;
            this.okbut.Text = "OK";
            this.okbut.UseVisualStyleBackColor = true;
            this.okbut.Click += new System.EventHandler( this.okbut_Click );
            // 
            // bevel1
            // 
            this.bevel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bevel1.Location = new System.Drawing.Point( 0, 60 );
            this.bevel1.Name = "bevel1";
            this.bevel1.Size = new System.Drawing.Size( 646, 2 );
            this.bevel1.TabIndex = 4;
            // 
            // AdminConfig
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size( 646, 488 );
            this.Controls.Add( this.bevel1 );
            this.Controls.Add( this.topToolStrip );
            this.Controls.Add( this.okbut );
            this.Controls.Add( this.cancelbut );
            this.Controls.Add( this.applybut );
            this.Controls.Add( this.configTabControl );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ( (System.Drawing.Icon)( resources.GetObject( "$this.Icon" ) ) );
            this.MaximizeBox = false;
            this.Name = "AdminConfig";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Perfectus System Configuration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.AdminConfig_FormClosing );
            this.topToolStrip.ResumeLayout( false );
            this.topToolStrip.PerformLayout( );
            this.configTabControl.ResumeLayout( false );
            this.databasetab.ResumeLayout( false );
            this.databasetab.PerformLayout( );
            ( (System.ComponentModel.ISupportInitialize)( this.dbconnectDataGridView ) ).EndInit( );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.ToolStrip topToolStrip;
        private System.Windows.Forms.ToolStripLabel productImage;
        private System.Windows.Forms.ToolStripLabel serverName;
        private System.Windows.Forms.TabControl configTabControl;
        private System.Windows.Forms.TabPage databasetab;
        private System.Windows.Forms.DataGridView dbconnectDataGridView;
        private System.Windows.Forms.Button applybut;
        private System.Windows.Forms.Button cancelbut;
        private System.Windows.Forms.Button okbut;
        private Perfectus.Library.UserControls.Bevel bevel1;
        private System.Windows.Forms.Label databasedesc1;
        private System.Windows.Forms.Label databasedesc4;
        private System.Windows.Forms.Label databasedesc3;
        private System.Windows.Forms.Label databasedesc2;

    }
}