using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Perfectus.Server.Common.Data;

namespace Administrator
{
    
    public partial class AdminTrans : BaseFormView
    {
        // Our data class object
        private TransClass _transactionClass = null;
        public TransClass TransactionClass
        {
            get
            {
                if( _transactionClass == null )
                    _transactionClass = new TransClass( );
                return _transactionClass;
            }
        }
        // Our selection class objects
        private SelectionClass _selectionObj = new SelectionClass( AdminClass.dataViewType.PACKAGE  );
        public SelectionClass SelectionObj
        {
            get { return _selectionObj; }
        }
        // Temporary variable which indicates the grid columns need to be re-configured.
        private bool _mustConfigureGridTable = true;
        // Container of background workers..
        private List<BackgroundWorker> backgroundworkers = new List<BackgroundWorker>( );
        // Set of ToolStripMenu items.
        ToolStripMenuItem _submitJobToolStripMenu = new ToolStripMenuItem( );
        ToolStripMenuItem _deleteInstanceToolStripMenu = new ToolStripMenuItem( );
        ToolStripMenuItem _packageLinkToolStripMenu = new ToolStripMenuItem( );
        ToolStripMenuItem _transactionLinkToolStripMenu = new ToolStripMenuItem( );

        public AdminTrans( )
        {
            InitializeComponent( );

            // Flag that more than 1 instance can be opened.
            base.AllowMultiInstances = true;
            // Start our window timer.
            windowtimer.Start( );
        }

        #region Event Handlers.
        // Event to signal the model has changed.
        public override void ModelChange( object sender, ModelChangeEventArgs e )
        {
            if( e.ModelObj == null )
                return;

            // Important to ignore enents from ourselves
            if( sender == this )
                return;

            // The selection class object. Need to refresh interface with new selections.
            if( e.ModelObj.GetType( ) == typeof( SelectionClass ) )
            {
                // Only respond if the event is from one of your child forms.
                if( ( (BaseFormView)sender ).Owner != this )
                    return;

                // Create a new selection object as a copy 
                _selectionObj = new SelectionClass( _selectionObj.CurrentView, (SelectionClass)e.ModelObj );

                // Reload data and update interface
                LoadData( );
            }
            
            // The list of Filters has changed. Reload and update interface.
            else if( e.ModelObj.GetType( ) == typeof( FilterClass ) )
            {
                // Reload ...
                TransactionClass.LoadFilters( );

                // Update interface.....
                this.LoadSelectFilters( );
            }

            // The dataconfiguration file has changed
            else if( e.ModelObj.GetType( ) == typeof( ConfigClass ) )
            {
                // Reload data and update interface. Note: changes to config file are not necessary reflected immediately.
                // Not sure how to ensure the enterprise library immediately refresh its data configuration settings. TBDL.
                // LoadData( );
            }
        }

        // User clicks the refresh button
        private void refreshButton_Click( object sender, EventArgs e )
        {
            // Reload data and update interface
            LoadData( );
        }

        // User selects a filter option.
        private void filterButton_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if( e.ClickedItem != null && !String.IsNullOrEmpty( e.ClickedItem.Text ) )
            {
                try
                {
                    switch( e.ClickedItem.Name )
                    {
                        case "clearFilter":
                            // Create a blank selection object. (however keep current period.)
                            TimePeriodClass tpc = ( _selectionObj.TimePeriod != null ) ? (TimePeriodClass)_selectionObj.TimePeriod.Clone( ) : null;
                            _selectionObj = new SelectionClass( _selectionObj.CurrentView );
                            _selectionObj.TimePeriod = tpc;
                            break;
                        case "deleteFilter":
                            // Delete the current filter
                            DeleteCurrentFilter( );
                            break;
                        case "defaultFilter":
                            // Sets the current filter as the default
                            DefaultCurrentFilter( );
                            break;
                        default:
                            {
                                // First find the selection object
                                SelectionClass filterselect = TransactionClass.Filters.GetFilter( e.ClickedItem.Text );
                                if( filterselect == null )
                                    throw new Exception( "Missing filter object. Please close and re-open window to refresh its data" );

                                // Update our current select object
                                _selectionObj = new SelectionClass( _selectionObj.CurrentView, filterselect );
                            }
                            break;
                    }

                    // Reload data and update interface
                    LoadData(  );

                    // Notify other windows of this change.
                    Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( this.SelectionObj ) );
                }
                catch( Exception ex )
                {
                    this.ShowErrorMessage( ex.Message );
                }
                finally
                {
                    // Display default cursor
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        // User selects a particular period.
        private void periodButton_DropDownItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            if( e.ClickedItem != null )
            {
                try
                {
                    // Get the time period object 
                    TimePeriodClass theperiod = null;
                    if( e.ClickedItem.Tag != null )
                        // Assign to current selection object
                        theperiod = (TimePeriodClass)e.ClickedItem.Tag;

                    // Assign the period to our selection object.
                     _selectionObj.TimePeriod = (theperiod == null) ? null : (TimePeriodClass)theperiod.Clone( );

                    // Reload data and update interface
                    LoadData( );

                    // Notify other windows of this change.
                    Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( theperiod ) );
                }
                catch( Exception ex )
                {
                    this.ShowErrorMessage( ex.Message );
                }
                finally
                {
                    // Display default cursor
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        // User switches btw package and transaction views
        private void modeButton_Click( object sender, EventArgs e )
        {
            // Toggle the current view btw packages and transactions.
            ChangeView( );
        }

        // Invokes the settings form which allows the user to select filters etc.
        private void settingsButton_Click( object sender, EventArgs e )
        {
            // If form is already open, simply activate the form.
            foreach( Form frm in this.OwnedForms )
            {
                if( frm.Name == "SelectionForm" )
                {
                    frm.Activate( );
                    return;
                }
            }

            // Create and show the filter configuration form.
            SelectionForm selectfrm = new SelectionForm( _selectionObj );
            selectfrm.Location = new Point( this.Width - selectfrm.Width - 20, 100 );
            selectfrm.Owner = this;
            selectfrm.Show( );
        }

        // Tells the form to close, after checking it can close.
        private void closeButton_Click( object sender, EventArgs e )
        {
            if( this.CanClose( ) )
                this.Close( );
        }

        // Regularly check the position of the mouse quickly....
        private void windowtimer_Tick( object sender, EventArgs e )
        {
            if( MousePosition.IsEmpty )
                return;

            // If we have a dialog window open, fade it if the mouse is not over the window...
            foreach( Form frm in this.OwnedForms )
            {
                if( frm.Name == "SelectionForm" )
                {
                    if( ( MousePosition.X > frm.Location.X ) && ( MousePosition.X < ( frm.Location.X + frm.Width ) ) &&
                        ( MousePosition.Y > frm.Location.Y ) && ( MousePosition.Y < ( frm.Location.Y + frm.Height ) ) )
                    {
                        FadeinDialog( frm );
                        return;
                    }
                    else
                        FadeoutDialog( frm );
                }
            }
        }

        // Fires when a particular job on the package instance table is fired.
        private void packageInstanceGridView_SelectionChanged( object sender, EventArgs e )
        {
            DataRow selectedRow = null;

            if( packageInstanceGridView.SelectedRows.Count > 0 )
            {
                // Using the first selected rows (should only be 1 as we don't allow multi select), get the datarow object bound to the row.
                selectedRow = ( (DataRowView)packageInstanceGridView.SelectedRows[ 0 ].DataBoundItem ).Row;
            }

            // Update the distribution table...
            LoadDistributions( selectedRow );

            // Notify other windows of this change.
            Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( selectedRow ) );
        }

        // Clicking the column header, we need to sort.
        private void packageInstanceGridView_ColumnHeaderMouseClick( object sender, DataGridViewCellMouseEventArgs e )
        {
            if( e.ColumnIndex >= 0 )
            {
                DataGridViewColumn column = packageInstanceGridView.Columns[ e.ColumnIndex ];

                if( column.SortMode == DataGridViewColumnSortMode.NotSortable )
                    return;

                //Assign sorting parameters to this form's selection class
                _selectionObj.SetSortByColumn( column.DataPropertyName );

                if( column.HeaderCell.SortGlyphDirection == SortOrder.None ||
                        column.HeaderCell.SortGlyphDirection == SortOrder.Descending )
                    _selectionObj.SetSortOrder( ListSortDirection.Ascending );
                else if( column.HeaderCell.SortGlyphDirection == SortOrder.Ascending )
                    _selectionObj.SetSortOrder( ListSortDirection.Descending );

                // Reload data and update interface
                LoadData( );

                // Notify others of change.
                Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( _selectionObj.GetSortOrder(  ) ) );
            }
        }

        // Cell specific formatting requirements.
        private void packageInstanceGridView_CellFormatting( object sender, DataGridViewCellFormattingEventArgs e )
        {
            // Immediately ignore all row and column headings.
            if( e.RowIndex < 0 || e.ColumnIndex < 0 )
                return;

            try
            {
                switch( packageInstanceGridView.Columns[ e.ColumnIndex ].Name )
                {
                    case "StatusName":
                        {
                            // This is the object we are binding.
                            DataRowView therow = (DataRowView)packageInstanceGridView.Rows[ e.RowIndex ].DataBoundItem;

                            // Maybe change the colour of the status column.
                            string statusvalue = therow[ "StatusValue" ].ToString( );
                            if( statusvalue == "FAIL" || statusvalue == "ERROR" || statusvalue == "INVALID" )
                            {
                                // Failure Text Style
                                e.CellStyle.ForeColor = Color.Red;
                                e.CellStyle.Font = new Font( e.CellStyle.Font, FontStyle.Bold );
                            }

                            // Check also whether the item has been deleted!
                            if( _selectionObj.CurrentView == AdminClass.dataViewType.PACKAGE )
                            {
                                DateTime? thedatetime = AdminClass.dateHandleNull( therow[ "WhenDeleted" ] );
                                if( thedatetime.HasValue )
                                    packageInstanceGridView.Rows[ e.RowIndex ].DefaultCellStyle.ForeColor = Color.Gray;
                            }

                        }
                        break;
                }
            }
            catch
            {
                // Ignore any exceptions here.
            }
        }

        // Worker process starting (this code is the worker process hence must not refer to any form data.)
        private void backgroundWorker_DoWork( object sender, DoWorkEventArgs e )
        {
            // This will be our worker parameter.
            SelectionClass theSelectObject = (SelectionClass)e.Argument;

            // Load data based on view in select object.
            if( theSelectObject.RequestedView == AdminClass.dataViewType.PACKAGE )
                e.Result = TransClass.LoadPackageInstanceView( (SelectionClass)e.Argument );
            else if( theSelectObject.RequestedView == AdminClass.dataViewType.TRANS )
                e.Result = TransClass.LoadTransactionView( (SelectionClass)e.Argument );

            if( ( (BackgroundWorker)sender ).CancellationPending )
                e.Cancel = true;
        }

        // Work process has finished. Load interface.
        private void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            try
            {
                // Remove worker from our list
                backgroundworkers.Remove( (BackgroundWorker)sender );

                // Stop timer if no worker processes left.
                if( backgroundworkers.Count == 0 )
                {
                    loadTimer.Stop( );
                    progressBar.Value = 0;
                }

                if( e.Cancelled )
                    return;

                else if( e.Error != null )
                    this.ShowErrorMessage( e.Error.Message );

                else
                {
                    // Assign the data to the appropriate views.
                    if( ( (TransDataSet)e.Result ).CurrentView == AdminClass.dataViewType.PACKAGE )
                        TransactionClass.AssignPackageInstances( ( (TransDataSet)e.Result ).CurrentData );
                    else if( ( (TransDataSet)e.Result ).CurrentView == AdminClass.dataViewType.TRANS )
                        TransactionClass.AssignTransactions( ( (TransDataSet)e.Result ).CurrentData );

                    // Finally flag which view we have
                    _selectionObj.CurrentView = ( (TransDataSet)e.Result ).CurrentView;

                    // Place data onto form.
                    RefreshInterface( );
                }
            }
            catch
            {
                // Simply ignore any errors at this stage. Things that cause exceptions here is if the user closes the 
                // form down prior to the worker process finishing.
            }
        }

        // Interval during data loading. Simple hack to show progress during data access.
        private void loadTimer_Tick( object sender, EventArgs e )
        {
            // Simple hack to simulate progress
            int value = progressBar.Value + 10;
            if( value > 100 )
                value = 0;
            progressBar.Value = value;
        }

        // Distribution tree view item selection event.
        private void distributortreeView_AfterSelect( object sender, TreeViewEventArgs e )
        {
            // Load the properties for the selected node.
            LoadPropertiesTable( e.Node );
        }

        // Events on clicking the grid view table. Maybe assign context menu.
        private void packageInstanceGridView_CellMouseUp( object sender, DataGridViewCellMouseEventArgs e )
        {
            // If right mouse and within a data row, display context menu.
            if( ( e.Button == MouseButtons.Right ) && ( e.RowIndex >= 0 ) )
            {
                ContextMenuStrip menu = new ContextMenuStrip( );

                // Get the row
                DataGridViewRow dataRow = packageInstanceGridView.Rows[ e.RowIndex ];
                if( dataRow == null )
                    return;

                // Maybe add Resubmit menu item
                if( _selectionObj.CurrentView == AdminClass.dataViewType.TRANS )
                {
                    // Add link to package instance table
                    _packageLinkToolStripMenu.Tag = dataRow;
                    menu.Items.Add( _packageLinkToolStripMenu );

                    // Set state of menu item and add to context menu
                    _submitJobToolStripMenu.Enabled = ( (string)dataRow.Cells[ "StatusValue" ].Value == "ERROR" );
                    _submitJobToolStripMenu.Tag = dataRow;
                    menu.Items.Add( _submitJobToolStripMenu );
                }

                // Maybe add Delete menu item
                if( _selectionObj.CurrentView == AdminClass.dataViewType.PACKAGE )
                {
                    // Add link to transaction table
                    _transactionLinkToolStripMenu.Tag = dataRow;
                    menu.Items.Add( _transactionLinkToolStripMenu );

                    // Set state of menu item and add to context menu
                    _deleteInstanceToolStripMenu.Enabled = ( AdminClass.dateHandleNull( dataRow.Cells[ "WhenDeleted" ].Value ) == null );
                    _deleteInstanceToolStripMenu.Tag = dataRow;
                    menu.Items.Add( _deleteInstanceToolStripMenu );
                }

                if( menu.Items.Count > 0 )
                {
                    // Show context menu.
                    Point pt = packageInstanceGridView.PointToClient( Control.MousePosition );
                    menu.Show( packageInstanceGridView, pt.X, pt.Y );
                }
            }
        }

        // User requested to re-submit job.
        private void submitJobToolStripMenu_Click( object sender, EventArgs args )
        {
            try
            {
                // We need the job ID to do this.
                Guid? packageInstanceID = null;
                int? jobqueueID = null;

                // Determine job ID from the tag.
                if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is DataGridViewRow )
                {
                    DataGridViewRow row = (DataGridViewRow)( (ToolStripMenuItem)sender ).Tag;
                    packageInstanceID = AdminClass.gHandleNull( row.Cells[ "PackageInstanceId" ].Value );
                    jobqueueID = AdminClass.iHandleNull( row.Cells[ "JobQueueID" ].Value );
                }
                else if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is TransClass.JobClass )
                {
                    TransClass.JobClass job = (TransClass.JobClass)( (ToolStripMenuItem)sender ).Tag;
                    packageInstanceID = job.PackageInstanceID;
                    jobqueueID = job.JobQueueID;
                }

                if( !packageInstanceID.HasValue || !jobqueueID.HasValue )
                    return;

                if( MessageBox.Show( String.Format( "Are you sure you want to retry the Job '{0}' and associated Jobs?", (int)jobqueueID ), "Retry Job",
                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Question ) == DialogResult.Retry )
                {
                    // Resubmit job now.
                    TransactionClass.ResubmitJob( (Guid)packageInstanceID );
                    // Reload data
                    LoadData( );
                }
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // User requested to delete interview
        private void deleteInstanceToolStripMenu_Click( object sender, EventArgs args )
        {
            try
            {
                // We need the package Instance Id to do this.
                Guid? PackageInstanceId = null;
                DataGridViewRow row = null;

                // Determine Instance ID from the tag.
                if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is DataGridViewRow )
                {
                    row = (DataGridViewRow)( (ToolStripMenuItem)sender ).Tag;
                    PackageInstanceId = AdminClass.gHandleNull( row.Cells[ "PackageInstanceId" ].Value );
                }

                if( !PackageInstanceId.HasValue )
                    return;

                if( MessageBox.Show( String.Format( "Are you sure you want to delete the Interview '{0}'?",
                            AdminClass.sHandleNull( row.Cells[ "Reference" ].Value ) ), "Delete Interview",
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Question ) == DialogResult.OK )
                {
                    // Delete instance now.
                    TransactionClass.DeletePackage( (Guid)PackageInstanceId );

                    // Reload data and update interface
                    LoadData( );
                }
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // User requested to view  package instance
        private void viewInstanceToolStripMenu_Click( object sender, EventArgs args )
        {
            try
            {
                // We need the package Instance Id to do this.
                Guid? PackageInstanceId = null;
                DataGridViewRow row = null;

                // Determine Instance ID from the tag.
                if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is DataGridViewRow )
                {
                    row = (DataGridViewRow)( (ToolStripMenuItem)sender ).Tag;
                    PackageInstanceId = AdminClass.gHandleNull( row.Cells[ "PackageInstanceId" ].Value );
                }

                if( !PackageInstanceId.HasValue )
                    return;

                // Create copy of the current selections and add package instance filter.
                SelectionClass tempobj = (SelectionClass)_selectionObj.Clone( );
                _selectionObj.FilterTextColumn = "PackageInstanceId";
                _selectionObj.FilterText = PackageInstanceId.ToString( );

                // Toggle the current view btw packages and transactions.
                ChangeView( );

                // Install the original filters.
                _selectionObj.FilterTextColumn = tempobj.FilterTextColumn;
                _selectionObj.FilterText = tempobj.FilterText;
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }
        #endregion

        #region Base class overrides.
        public override void LoadInterface( )
        {
            try
            {
                // Call base class to perform common actions.
                base.LoadInterface( );

                // Name of server in the form header section.
                serverName.Text = AdminClass.Instance.ServerName;

                // Create image list for display
                imageList.Images.Add( Properties.Resources.check2 );
                imageList.Images.Add( Properties.Resources.delete2 );
                imageList.Images.Add( Properties.Resources.gears );
                distributortreeView.ImageList = imageList;

                // Update some window controls
                LoadTimeFilters( );
                LoadSelectFilters( );

                // Default a selection object.
                SelectionClass defSelection = null;
                if( !String.IsNullOrEmpty( TransactionClass.Filters.DefaultFilter ) &&
                            (defSelection =  TransactionClass.Filters.GetFilter( TransactionClass.Filters.DefaultFilter)) != null )
                    _selectionObj = new SelectionClass( AdminClass.dataViewType.PACKAGE, defSelection );

                    // Assign the last period as a default.
                else if( TransactionClass.Filters.TimePeriods.Count > 0 )
                    _selectionObj.TimePeriod = (TimePeriodClass)TransactionClass.Filters.TimePeriods[ TransactionClass.Filters.TimePeriods.Count - 1 ].Clone( );

                // Configure the context toolstripmenu items
                _submitJobToolStripMenu.Text = "Retry Job (Queue)";
                _submitJobToolStripMenu.Click += new EventHandler( submitJobToolStripMenu_Click );
                _deleteInstanceToolStripMenu.Text = "Delete Interview";
                _deleteInstanceToolStripMenu.Click += new EventHandler( deleteInstanceToolStripMenu_Click );
                _packageLinkToolStripMenu.Text = "View Package Instance";
                _packageLinkToolStripMenu.Click += new EventHandler( viewInstanceToolStripMenu_Click );
                _transactionLinkToolStripMenu.Text = "View Transactions";
                _transactionLinkToolStripMenu.Click += new EventHandler( viewInstanceToolStripMenu_Click );

                // Load the default data into the window.
                LoadData(  );
            }
            catch ( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }
        #endregion

        // Load the list of selectable filters
        private void LoadSelectFilters( )
        {
            // First clear current list. (The last 4 items are system items)
            while( filterButton.DropDownItems.Count > 4 )
                filterButton.DropDownItems.RemoveAt( 0 );

            // Display the name of each selection.
            foreach( SelectionClass sc in TransactionClass.Filters.Selections )
                filterButton.DropDownItems.Insert( 0, new ToolStripMenuItem( sc.Name ) );
        }

        // Load the list of time filters
        private void LoadTimeFilters( )
        {
            // First clear current list. 
            periodButton.DropDownItems.Clear( );

            // Ensure list is sorted before we display.
            TransactionClass.Filters.TimePeriods.Sort( );

            // Display the name of each selection.
            foreach( TimePeriodClass pc in TransactionClass.Filters.TimePeriods )
            {
                ToolStripMenuItem item = new ToolStripMenuItem( pc.Name );
                item.Tag = pc;
                periodButton.DropDownItems.Add( item );
            }

            // Add option for null period
            periodButton.DropDownItems.Add( new ToolStripMenuItem( "Show All" ) );
        }

        // Programmatic configuration of Package instance table
        private void ConfigurePackageInstanceTable( )
        {
            // Ensure all sorting is done programatically.
            foreach( DataGridViewColumn col in packageInstanceGridView.Columns )
                col.SortMode = DataGridViewColumnSortMode.Programmatic;

            // Configure databound columns the way we want them
            packageInstanceGridView.Columns[ "PackageInstanceId" ].Visible = false;

            if( packageInstanceGridView.Columns[ "JobQueueId" ] != null )
            {
                packageInstanceGridView.Columns[ "JobQueueId" ].DisplayIndex = 0;
                packageInstanceGridView.Columns[ "JobQueueId" ].HeaderText = "Job ID";
                packageInstanceGridView.Columns[ "JobQueueId" ].Width = 75;
                packageInstanceGridView.Columns[ "JobQueueId" ].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            if( packageInstanceGridView.Columns[ "PageId" ] != null )
                packageInstanceGridView.Columns[ "PageId" ].Visible = false;
            // The page name is only displayed at the bottom grid as few (none) clients use this functionality.
            if( packageInstanceGridView.Columns[ "PageName" ] != null )
                packageInstanceGridView.Columns[ "PageName" ].Visible = false;
            packageInstanceGridView.Columns[ "PackageVersion" ].HeaderText = "Version";
            packageInstanceGridView.Columns[ "PackageVersion" ].Width = 75;
            packageInstanceGridView.Columns[ "PackageVersion" ].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            packageInstanceGridView.Columns[ "PackageVersion" ].DefaultCellStyle.Format = "#.###";
            packageInstanceGridView.Columns[ "CreatedByPersonId" ].HeaderText = "Created By";
            packageInstanceGridView.Columns[ "CreatedByPersonId" ].Width = 150;
            packageInstanceGridView.Columns[ "WhenCreatedLcl" ].HeaderText = "When Created";
            packageInstanceGridView.Columns[ "WhenCreatedLcl" ].Width = 125;
            packageInstanceGridView.Columns[ "WhenCreatedLcl" ].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";
            packageInstanceGridView.Columns[ "StatusValue" ].Visible = false;
            packageInstanceGridView.Columns[ "StatusName" ].HeaderText = "Status";
            packageInstanceGridView.Columns[ "StatusName" ].Width = 100;
            packageInstanceGridView.Columns[ "LastStatusChange" ].HeaderText = "Last Status Change";
            packageInstanceGridView.Columns[ "LastStatusChange" ].Width = 150;
            packageInstanceGridView.Columns[ "LastStatusChange" ].DefaultCellStyle.Format = "dd/MM/yyyy HH:mm:ss";
            if( packageInstanceGridView.Columns[ "WhenDeleted" ] != null )
                packageInstanceGridView.Columns[ "WhenDeleted" ].Visible = false;
            packageInstanceGridView.Columns[ "Reference" ].Width = 150;
            packageInstanceGridView.Columns["Reference"].HeaderText = "Reference";
            packageInstanceGridView.Columns["FriendlyMessage"].DisplayIndex = packageInstanceGridView.Columns.Count - 1;
            packageInstanceGridView.Columns[ "FriendlyMessage" ].HeaderText = "Message";
            packageInstanceGridView.Columns[ "FriendlyMessage" ].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            packageInstanceGridView.Columns[ "FriendlyMessage" ].SortMode = DataGridViewColumnSortMode.NotSortable;

            // Reset interface flag
            _mustConfigureGridTable = false;

            // Notify other windows of this change.
            Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( packageInstanceGridView.Columns ) );
        }

        // Calls database code to extract all relevent data
        private void LoadData( )
        {
            // Dim the display
            packageInstanceGridView.ForeColor = Color.LightGray;

            // Start timer
            loadTimer.Start( );

            // First cancel all current background processes.
            foreach( BackgroundWorker bw in backgroundworkers )
                if( bw.IsBusy && !bw.CancellationPending )
                    bw.CancelAsync( );

            // Create new background worker process.
            BackgroundWorker bckwrk = new BackgroundWorker( );
            bckwrk.DoWork += new DoWorkEventHandler( this.backgroundWorker_DoWork );
            bckwrk.RunWorkerCompleted += new RunWorkerCompletedEventHandler( this.backgroundWorker_RunWorkerCompleted );
            bckwrk.WorkerReportsProgress = false;
            bckwrk.WorkerSupportsCancellation = true;
            backgroundworkers.Add( bckwrk );

            // Start worker process to update data
            bckwrk.RunWorkerAsync( _selectionObj );
        }

        // Reloads the data onto the interface.
        private void RefreshInterface(  )
        {
            try
            {
                packageInstanceGridView.SuspendLayout( );

                // Reset fore colour.
                packageInstanceGridView.ForeColor = Color.Black;

                // Databind the parent table.
                switch( _selectionObj.CurrentView )
                {
                    case AdminClass.dataViewType.PACKAGE:
                        packageInstanceGridView.DataSource = TransactionClass.GetPackageInstanceView( );
                        break;
                    case AdminClass.dataViewType.TRANS:
                        packageInstanceGridView.DataSource = TransactionClass.GetTransactionView( );
                        break;
                }

                // Final settings of columns etc on instance table
                if( this._mustConfigureGridTable )
                    ConfigurePackageInstanceTable( );

                // Update the column header for sorting (Note the column might not exist).
                string sortcol = _selectionObj.GetSortByColumn( );
                if( !String.IsNullOrEmpty( sortcol ) && packageInstanceGridView.Columns[ sortcol ] != null )
                {
                    packageInstanceGridView.Columns[ sortcol ].HeaderCell.SortGlyphDirection =
                        _selectionObj.GetSortOrder( ) == ListSortDirection.Ascending ? SortOrder.Ascending :
                        _selectionObj.GetSortOrder( ) == ListSortDirection.Descending ? SortOrder.Descending :
                        SortOrder.None;
                }

                // Finally update the grid view.
                packageInstanceGridView.Refresh( );
                packageInstanceGridView.ResumeLayout( );

                // Enable/Disable some controls
                // If we have a named seelction object, (ie has been saved), enable some buttons. These are fixed buttons.
                bool haveCurrentFilter = _selectionObj.HasFilterSelections( );
                defaultFilter.Enabled = !String.IsNullOrEmpty( _selectionObj.Name );
                deleteFilter.Enabled = !String.IsNullOrEmpty( _selectionObj.Name );
                clearFilter.Enabled = haveCurrentFilter;

                // Status bar
                // Indicate the current view
                if( _selectionObj.CurrentView == AdminClass.dataViewType.PACKAGE )
                    viewLabel.Text = "Package Instances";
                else if( _selectionObj.CurrentView == AdminClass.dataViewType.TRANS )
                    viewLabel.Text = "Job Transactions";

                // Indicate the current filter.
                if( haveCurrentFilter )
                {
                    if( !String.IsNullOrEmpty( _selectionObj.Name ) )
                        filterLabel.Text = _selectionObj.Name;
                    else
                        filterLabel.Text = "Custom";
                }
                else
                    filterLabel.Text = "Unrestricted";

                // Indicate the current time period.
                if( _selectionObj.TimePeriod != null && !String.IsNullOrEmpty( _selectionObj.TimePeriod.Name ) )
                    periodLabel.Text = _selectionObj.TimePeriod.Name;
                else
                    periodLabel.Text = "Show All";
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // Changes the view btw packages and transactions.
        private void ChangeView( )
        {
            // Toggle the current view btw packages and transactions.
            switch( _selectionObj.CurrentView )
            {
                case AdminClass.dataViewType.PACKAGE:
                    modeButton.Text = "Package";
                    _selectionObj.RequestedView = AdminClass.dataViewType.TRANS;
                    break;
                case AdminClass.dataViewType.TRANS:
                    modeButton.Text = "Trans";
                    _selectionObj.RequestedView = AdminClass.dataViewType.PACKAGE;
                    break;
            }

            // Place data onto form.
            LoadData( );

            // We will need to realign columns etc...
            _mustConfigureGridTable = true;

            // Notify other windows of this change.
            Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( _selectionObj.CurrentView ) );
        }
        
        // Loads the distribution tree view.
        private void LoadDistributions( DataRow selectedrow )
        {
            // Clear current tree...
            distributortreeView.Nodes.Clear( );
            // Clear current data.
            propertiesGridView.Rows.Clear( );

            // Return if there is no row currently selected.
            if( selectedrow == null )
                return;

            distributortreeView.BeginUpdate( );

            // Get all child rows for the selected row based on the relation between the 2
            switch( _selectionObj.CurrentView )
            {
                case AdminClass.dataViewType.PACKAGE:
                    LoadPackageDistributions( selectedrow );
                    break;
                case AdminClass.dataViewType.TRANS:
                    LoadTransDistributions( selectedrow );
                    break;
            }

            distributortreeView.ExpandAll( );
            distributortreeView.EndUpdate( );

            if( distributortreeView.Nodes.Count > 0 )
            {
                // Ensure the top node is visible.
                distributortreeView.Nodes[ 0 ].EnsureVisible( );
                // Make the first job node selected.
                distributortreeView.SelectedNode = distributortreeView.Nodes[ 0 ];
            }
        }

        // Loads the distribution tree view for a given package instance datarow.
        private void LoadPackageDistributions( DataRow selectedrow )
        {
            // Ask for the list of jobs.
            List<TransClass.JobClass> joblist = TransactionClass.GetPackageInstanceJobs( selectedrow );
            if( joblist.Count == 0 )
                return;

            // Iterate through our list of jobs and create the tree view display.
            foreach( TransClass.JobClass jc in joblist )
            {
                // This represents a Job 
                TreeNode jobnode = null;

                // Prior to the JobQueue, package instances where not broken down into jobs.
                if( jc.JobQueueID.HasValue )
                {
                    // Most jobs don't have page numbers, hence we show the job id instead.
                    jobnode = new TreeNode(  !String.IsNullOrEmpty( jc.PageName ) ? jc.PageName : "Job ID " +  jc.JobQueueID.ToString( ) );
                    // Prevent image from being shown
                    jobnode.ImageIndex = distributortreeView.ImageList.Images.Count;
                    jobnode.SelectedImageIndex = jobnode.ImageIndex;
                    jobnode.Tag = jc;

                    // Maybe add a context menu to the node.
                    if( jc.StatusValue == "ERROR" )
                    {
                        ContextMenuStrip menu = new ContextMenuStrip( );
                        ToolStripMenuItem menuitem = new ToolStripMenuItem( _submitJobToolStripMenu.Text, null, new EventHandler( submitJobToolStripMenu_Click ) );
                        menuitem.Tag = jc;
                        menu.Items.Add( menuitem );
                        jobnode.ContextMenuStrip = menu;
                    }
                    
                    distributortreeView.Nodes.Add( jobnode );
                }

                // Add additional distribution nodes.
                LoadDistributorTree( jobnode, jc.Distributions );
            }
        }

        // Loads the distribution tree view for a given transaction
        private void LoadTransDistributions( DataRow selectedrow )
        {
            // Ask for the list of jobs.
            TransClass.JobClass transJob = TransactionClass.GetTransactionJob( selectedrow );
            if( transJob == null )
                return;

            // Add initial node for the job itself. 
            TreeNode jobnode = new TreeNode( !String.IsNullOrEmpty( transJob.PageName ) ? transJob.PageName : "Job ID " + transJob.JobQueueID.ToString( ) );
            // Prevent image from being shown
            jobnode.ImageIndex = distributortreeView.ImageList.Images.Count;
            jobnode.SelectedImageIndex = jobnode.ImageIndex;
            jobnode.Tag = transJob;

            // Maybe add a context menu to the node.
            if( transJob.StatusValue == "ERROR" )
            {
                ContextMenuStrip menu = new ContextMenuStrip( );
                ToolStripMenuItem menuitem = new ToolStripMenuItem( _submitJobToolStripMenu.Text, null, new EventHandler( submitJobToolStripMenu_Click ) );
                menuitem.Tag = transJob;
                menu.Items.Add( menuitem );
                jobnode.ContextMenuStrip = menu;
            }

            distributortreeView.Nodes.Add( jobnode );

            // Add additional distribution nodes.
            LoadDistributorTree( jobnode, transJob.Distributions );
        }

        // Creates the distributor tree from the job node down.
        private void LoadDistributorTree( TreeNode rootnode,  List<TransClass.DistributionClass> Distributions )
        {            
            // Interate through our list of distributions and create the tree view display.
            foreach( TransClass.DistributionClass dc in Distributions )
            {
                // This represents a distributor
                TreeNode distnode = new TreeNode( !String.IsNullOrEmpty( dc.DistributionName ) ? dc.DistributionName : "Delivery" );
                // We only monitor whether the whole distribution succeeded, not each document.
                int imageindex = ( dc.StatusValue == "COMPLETE") ? 0 : ( (dc.StatusValue == "ERROR" || dc.StatusValue == "INVALID" ) ? 1 : 2 );
                distnode.ImageIndex = imageindex;
                distnode.SelectedImageIndex = imageindex;
                distnode.Tag = dc;

                if( rootnode != null )
                    rootnode.Nodes.Add( distnode );
                else
                    distributortreeView.Nodes.Add( distnode );

                foreach( TransClass.DocumentClass docc in dc.Documents )
                {
                    TreeNode docnode = new TreeNode( docc.DocumentName );
                    docnode.ImageIndex = imageindex;
                    docnode.SelectedImageIndex = imageindex;
                    docnode.Tag = docc;

                    distnode.Nodes.Add( docnode );
                }
            }
        }

        // Load the properties grid table
        private void LoadPropertiesTable( TreeNode node )
        {
            propertiesGridView.SuspendLayout( );
            
            // Clear current data.
            propertiesGridView.Rows.Clear( );

            if( node.Tag == null )
                return;

            // Load fields based on the class type.
            if( node.Tag is TransClass.JobClass )
            {
                TransClass.JobClass jc = (TransClass.JobClass)node.Tag;

                if( !String.IsNullOrEmpty( jc.PageName ) )
                    propertiesGridView.Rows.Add( "Execution Page", jc.PageName );
                propertiesGridView.Rows.Add( "Job ID", jc.JobQueueID );
                propertiesGridView.Rows.Add( "Status", jc.StatusName );
                if( jc.LastStatusChange.HasValue )
                    propertiesGridView.Rows.Add( "Time", ((DateTime)jc.LastStatusChange).ToString( "hh:mm:ss dd/MM/yyyy" ) );
                propertiesGridView.Rows.Add( "Message", jc.ExceptionMessage );
            }
            else if( node.Tag is TransClass.DistributionClass)
            {
                TransClass.DistributionClass dc = (TransClass.DistributionClass)node.Tag;

                propertiesGridView.Rows.Add( "Delivery Name", dc.DistributionName );
                propertiesGridView.Rows.Add( "Status", dc.StatusName );
                if( dc.LastStatusChange.HasValue )
                    propertiesGridView.Rows.Add( "Time", ( (DateTime)dc.LastStatusChange ).ToString( "hh:mm:ss dd/MM/yyyy" ) );
            }
            else if( node.Tag is TransClass.DocumentClass )
            {
                TransClass.DocumentClass docc = (TransClass.DocumentClass)node.Tag;

                propertiesGridView.Rows.Add( "Template/Document", docc.DocumentName );
                propertiesGridView.Rows.Add( "Link/Document", docc.LinkText );
                propertiesGridView.Rows.Add( "Reference", docc.Reference );
            }
            
            // We don't want the grid to show a default selected cell.
            propertiesGridView.ClearSelection( );
            propertiesGridView.ResumeLayout( );
        }

        /// <summary>
        /// Returns a list of valid columns shown in the package instance table. 
        /// </summary>
        /// <returns></returns>
        public DataGridViewColumnCollection GetPackageInstanceColumns( )
        {
            return packageInstanceGridView.Columns;
        }

        // Removes the current filter
        private void DeleteCurrentFilter( )
        {
            // No 'saved' filter has been applied to the form.
            if( String.IsNullOrEmpty( _selectionObj.Name ) )
                return;

            // Warn user
            if( MessageBox.Show( "The filter '" + _selectionObj.Name + "' will be removed permanently. Are you sure you want to continue?.", "Delete Filter",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question ) == DialogResult.Cancel )
                return;

            // Display a wait cursor
            Cursor.Current = Cursors.WaitCursor;

            // Delete the filter.
            TransactionClass.DeleteFilter( _selectionObj.Name );

            // Create a blank selection object, however keep current period.
            TimePeriodClass tpc = ( _selectionObj.TimePeriod != null ) ? (TimePeriodClass)_selectionObj.TimePeriod.Clone( ) : new TimePeriodClass( );
            _selectionObj = new SelectionClass( _selectionObj.CurrentView );
            _selectionObj.TimePeriod = tpc;

            // Update interface.....
            this.LoadSelectFilters( );
        }

        // Sets the current filter as the default
        private void DefaultCurrentFilter( )
        {
            // No 'saved' filter has been applied to the form.
            if( String.IsNullOrEmpty( _selectionObj.Name ) )
                return;

            // Warn user
            if( MessageBox.Show( "The filter '" + _selectionObj.Name + "' will be set as the default filter whenever the window is opened. Are you sure you want to continue?.", "Default Filter",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question ) == DialogResult.Cancel )
                return;

            // Display a wait cursor
            Cursor.Current = Cursors.WaitCursor;
            
            // Set the default
            TransactionClass.Filters.DefaultFilter = _selectionObj.Name;

            // Save the filters
            TransactionClass.SaveFilters( );
        }

        // Shade the visibility of the dialog window. 
        private void FadeoutDialog( Form frm )
        {
            frm.Opacity = 0.5;
        }

        // Enhance the visibility of the dialog window. 
        private void FadeinDialog( Form frm )
        {
            frm.Opacity = 1.0;
        }
    }
}

