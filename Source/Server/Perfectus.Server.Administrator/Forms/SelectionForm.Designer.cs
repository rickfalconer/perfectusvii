namespace Administrator
{
    partial class SelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( SelectionForm ) );
            this.containerPanel = new System.Windows.Forms.Panel( );
            this.statuslabel = new System.Windows.Forms.Label( );
            this.statusgroupBox = new System.Windows.Forms.GroupBox( );
            this.closebutton = new System.Windows.Forms.Button( );
            this.todateTimePicker = new System.Windows.Forms.DateTimePicker( );
            this.fromdateTimePicker = new System.Windows.Forms.DateTimePicker( );
            this.refreshbutton = new System.Windows.Forms.Button( );
            this.savebutton = new System.Windows.Forms.Button( );
            this.saveastextBox = new System.Windows.Forms.TextBox( );
            this.saveaslabel = new System.Windows.Forms.Label( );
            this.tolabel = new System.Windows.Forms.Label( );
            this.liketextBox = new System.Windows.Forms.TextBox( );
            this.fromlabel = new System.Windows.Forms.Label( );
            this.filtercomboBox = new System.Windows.Forms.ComboBox( );
            this.filterlabel = new System.Windows.Forms.Label( );
            this.containerPanel.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // containerPanel
            // 
            this.containerPanel.Controls.Add( this.statuslabel );
            this.containerPanel.Controls.Add( this.statusgroupBox );
            this.containerPanel.Controls.Add( this.closebutton );
            this.containerPanel.Controls.Add( this.todateTimePicker );
            this.containerPanel.Controls.Add( this.fromdateTimePicker );
            this.containerPanel.Controls.Add( this.refreshbutton );
            this.containerPanel.Controls.Add( this.savebutton );
            this.containerPanel.Controls.Add( this.saveastextBox );
            this.containerPanel.Controls.Add( this.saveaslabel );
            this.containerPanel.Controls.Add( this.tolabel );
            this.containerPanel.Controls.Add( this.liketextBox );
            this.containerPanel.Controls.Add( this.fromlabel );
            this.containerPanel.Controls.Add( this.filtercomboBox );
            this.containerPanel.Controls.Add( this.filterlabel );
            this.containerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerPanel.Location = new System.Drawing.Point( 0, 0 );
            this.containerPanel.Name = "containerPanel";
            this.containerPanel.Size = new System.Drawing.Size( 232, 472 );
            this.containerPanel.TabIndex = 0;
            // 
            // statuslabel
            // 
            this.statuslabel.AutoSize = true;
            this.statuslabel.Location = new System.Drawing.Point( 15, 153 );
            this.statuslabel.Name = "statuslabel";
            this.statuslabel.Size = new System.Drawing.Size( 96, 13 );
            this.statuslabel.TabIndex = 16;
            this.statuslabel.Text = "Transaction States";
            // 
            // statusgroupBox
            // 
            this.statusgroupBox.Location = new System.Drawing.Point( 15, 167 );
            this.statusgroupBox.Name = "statusgroupBox";
            this.statusgroupBox.Size = new System.Drawing.Size( 200, 186 );
            this.statusgroupBox.TabIndex = 4;
            this.statusgroupBox.TabStop = false;
            // 
            // closebutton
            // 
            this.closebutton.Location = new System.Drawing.Point( 119, 427 );
            this.closebutton.Name = "closebutton";
            this.closebutton.Size = new System.Drawing.Size( 70, 23 );
            this.closebutton.TabIndex = 8;
            this.closebutton.Text = "Close";
            this.closebutton.UseVisualStyleBackColor = true;
            this.closebutton.Click += new System.EventHandler( this.closebutton_Click );
            // 
            // todateTimePicker
            // 
            this.todateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.todateTimePicker.Location = new System.Drawing.Point( 15, 114 );
            this.todateTimePicker.Name = "todateTimePicker";
            this.todateTimePicker.Size = new System.Drawing.Size( 198, 20 );
            this.todateTimePicker.TabIndex = 3;
            this.todateTimePicker.Visible = false;
            // 
            // fromdateTimePicker
            // 
            this.fromdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fromdateTimePicker.Location = new System.Drawing.Point( 15, 69 );
            this.fromdateTimePicker.Name = "fromdateTimePicker";
            this.fromdateTimePicker.Size = new System.Drawing.Size( 198, 20 );
            this.fromdateTimePicker.TabIndex = 2;
            this.fromdateTimePicker.Visible = false;
            // 
            // refreshbutton
            // 
            this.refreshbutton.Location = new System.Drawing.Point( 43, 427 );
            this.refreshbutton.Name = "refreshbutton";
            this.refreshbutton.Size = new System.Drawing.Size( 70, 23 );
            this.refreshbutton.TabIndex = 7;
            this.refreshbutton.Text = "Apply";
            this.refreshbutton.UseVisualStyleBackColor = true;
            this.refreshbutton.Click += new System.EventHandler( this.refreshbutton_Click );
            // 
            // savebutton
            // 
            this.savebutton.Location = new System.Drawing.Point( 163, 388 );
            this.savebutton.Name = "savebutton";
            this.savebutton.Size = new System.Drawing.Size( 50, 23 );
            this.savebutton.TabIndex = 6;
            this.savebutton.Text = "Save";
            this.savebutton.UseVisualStyleBackColor = true;
            this.savebutton.Click += new System.EventHandler( this.savebutton_Click );
            // 
            // saveastextBox
            // 
            this.saveastextBox.Location = new System.Drawing.Point( 15, 389 );
            this.saveastextBox.Name = "saveastextBox";
            this.saveastextBox.Size = new System.Drawing.Size( 141, 20 );
            this.saveastextBox.TabIndex = 5;
            // 
            // saveaslabel
            // 
            this.saveaslabel.AutoSize = true;
            this.saveaslabel.Location = new System.Drawing.Point( 12, 373 );
            this.saveaslabel.Name = "saveaslabel";
            this.saveaslabel.Size = new System.Drawing.Size( 47, 13 );
            this.saveaslabel.TabIndex = 7;
            this.saveaslabel.Text = "Save As";
            // 
            // tolabel
            // 
            this.tolabel.AutoSize = true;
            this.tolabel.Location = new System.Drawing.Point( 12, 98 );
            this.tolabel.Name = "tolabel";
            this.tolabel.Size = new System.Drawing.Size( 20, 13 );
            this.tolabel.TabIndex = 4;
            this.tolabel.Text = "To";
            // 
            // liketextBox
            // 
            this.liketextBox.Location = new System.Drawing.Point( 15, 69 );
            this.liketextBox.Name = "liketextBox";
            this.liketextBox.Size = new System.Drawing.Size( 198, 20 );
            this.liketextBox.TabIndex = 2;
            // 
            // fromlabel
            // 
            this.fromlabel.AutoSize = true;
            this.fromlabel.Location = new System.Drawing.Point( 12, 53 );
            this.fromlabel.Name = "fromlabel";
            this.fromlabel.Size = new System.Drawing.Size( 30, 13 );
            this.fromlabel.TabIndex = 2;
            this.fromlabel.Text = "From";
            // 
            // filtercomboBox
            // 
            this.filtercomboBox.FormattingEnabled = true;
            this.filtercomboBox.Location = new System.Drawing.Point( 15, 25 );
            this.filtercomboBox.Name = "filtercomboBox";
            this.filtercomboBox.Size = new System.Drawing.Size( 198, 21 );
            this.filtercomboBox.TabIndex = 1;
            this.filtercomboBox.SelectedIndexChanged += new System.EventHandler( this.filtercomboBox_SelectedIndexChanged );
            // 
            // filterlabel
            // 
            this.filterlabel.AutoSize = true;
            this.filterlabel.Location = new System.Drawing.Point( 12, 9 );
            this.filterlabel.Name = "filterlabel";
            this.filterlabel.Size = new System.Drawing.Size( 44, 13 );
            this.filterlabel.TabIndex = 0;
            this.filterlabel.Text = "Filter By";
            // 
            // SelectionForm
            // 
            this.AcceptButton = this.refreshbutton;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 232, 472 );
            this.Controls.Add( this.containerPanel );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ( (System.Drawing.Icon)( resources.GetObject( "$this.Icon" ) ) );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectionForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Filter Settings";
            this.TopMost = true;
            this.Activated += new System.EventHandler( this.SelectionForm_Activated );
            this.containerPanel.ResumeLayout( false );
            this.containerPanel.PerformLayout( );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Panel containerPanel;
        private System.Windows.Forms.Label fromlabel;
        private System.Windows.Forms.ComboBox filtercomboBox;
        private System.Windows.Forms.Label filterlabel;
        private System.Windows.Forms.Label tolabel;
        private System.Windows.Forms.TextBox liketextBox;
        private System.Windows.Forms.Button refreshbutton;
        private System.Windows.Forms.Button savebutton;
        private System.Windows.Forms.TextBox saveastextBox;
        private System.Windows.Forms.Label saveaslabel;
        private System.Windows.Forms.DateTimePicker fromdateTimePicker;
        private System.Windows.Forms.DateTimePicker todateTimePicker;
        private System.Windows.Forms.Button closebutton;
        private System.Windows.Forms.GroupBox statusgroupBox;
        private System.Windows.Forms.Label statuslabel;

    }
}