using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Administrator
{
    public partial class AdminConfig : BaseFormView
    {
        // Interface components.
        private DataGridViewComboBoxColumn _providerList;
        // Class object we are displaying/changing.
        private ConfigClass _configObject = new ConfigClass( );
        // Set of ToolStripMenu items.
        ToolStripMenuItem _deleteToolStripMenu = new ToolStripMenuItem( );
        ToolStripMenuItem _testToolStripMenu = new ToolStripMenuItem( );
        ToolStripMenuItem _upgradeToolStripMenu = new ToolStripMenuItem( );
        // Record if anything changed
        private bool _haveChanges = false;

        public AdminConfig( )
        {
            InitializeComponent( );
        }

        #region Base class overrides.
        public override void LoadInterface( )
        {
            try
            {
                // Call base class to perform common actions.
                base.LoadInterface( );

                // Name of server in the form header section.
                serverName.Text = AdminClass.Instance.ServerName;

                // Database tab description....
                databasedesc1.Text = @"This tab is used to change and test database connection settings.";
                databasedesc2.Text = @"To change a database connection value, click the relevant cell to edit the contents.";
                databasedesc3.Text = @"To add a new database connection, click into an empty row and start by entering a connection name.";
                databasedesc4.Text = @"To test, upgrade, install or remove a database connection, right click anywhere in the row";

                // Load our configuration object 
                _configObject.LoadDataConfigurationFile( );

                // Load and configure database connection string grid.
                dbconnectDataGridView.DataSource = _configObject.ConnectionSettings;
                dbconnectDataGridView.Columns[ "ConnectionString" ].Visible = false;
                dbconnectDataGridView.Columns[ "ProviderName" ].Visible = false;
                dbconnectDataGridView.Columns[ "Default" ].HeaderText = "Def";
                dbconnectDataGridView.Columns[ "Default" ].Width = 40;
                dbconnectDataGridView.Columns[ "Name" ].Frozen = true;
                dbconnectDataGridView.Columns[ "Name" ].MinimumWidth = 80;
                dbconnectDataGridView.Columns[ "Server" ].MinimumWidth = 80;
                dbconnectDataGridView.Columns[ "Database" ].MinimumWidth = 80;
                dbconnectDataGridView.Columns[ "User" ].MinimumWidth = 80;
                dbconnectDataGridView.Columns[ "Password" ].MinimumWidth = 80;

                // Database providers.
                _providerList = new DataGridViewComboBoxColumn( );
                _providerList.DataSource = _configObject.DatabaseProviders;
                _providerList.DataPropertyName = "ProviderName";
                _providerList.HeaderText = "Provider";
                _providerList.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                _providerList.MinimumWidth = 80;
                dbconnectDataGridView.Columns.Add( _providerList );

                // Configure the context toolstripmenu items
                _deleteToolStripMenu.Text = "Delete";
                _deleteToolStripMenu.Click += new EventHandler( deleteMenuItem_Click );
                _testToolStripMenu.Text = "Test";
                _testToolStripMenu.Click += new EventHandler( testMenuItem_Click );
                _upgradeToolStripMenu.Text = "Upgrade/Install";
                _upgradeToolStripMenu.Click += new EventHandler( upgradeMenuItem_Click );
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        public override bool CanClose( ) 
        {
            return !_haveChanges; 
        }
        #endregion

        // Forces interface to reload data, typically after saving.
        private void ReLoadInterfaceData( )
        {
            try
            {
                // Load configuration class for display
                _configObject.LoadDataConfigurationFile( );
                // Load and configure database connection string grid.
                dbconnectDataGridView.DataSource = _configObject.ConnectionSettings;
                // Database providers.
                _providerList.DataSource = _configObject.DatabaseProviders;
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // Internal save routine
        private bool SaveConfigFile(  )
        {
            try
            {
                // User must be administrator or system operator to do this.
                if( AdminClass.Instance.IsUserAuthorised( ) == false )
                    throw new Exception( "You are not authorised to change the configuration settings. Only Administrators and System Operators may change the settings." );

                // Save configuration file
                _configObject.SaveDataConfigurationFile( );
                // Record we have no further changes
                _haveChanges = false;
                // Notify other windows
                Controller.Instance.RaiseModelChange( this, new ModelChangeEventArgs( _configObject ) );

                return true;
            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
            return false;
        }

        #region Event Handlers.
        // Called during cell formatting of connection grid
        private void dbconnectDataGridView_CellFormatting( object sender, DataGridViewCellFormattingEventArgs e )
        {
            // Immediately ignore all row and column headings.
            if( e.RowIndex < 0 || e.ColumnIndex < 0 )
                return;

            // To support password masking in the DataGridView, we record the true value in the DataRow tag property and display * characters instead.
            if( dbconnectDataGridView.Columns[ e.ColumnIndex ].Name == "Password" && e.Value != null )
            {
                dbconnectDataGridView.Rows[ e.RowIndex ].Tag = e.Value;
                e.Value = new string( '*', e.Value.ToString( ).Length );
            }
        }

        // Called whenever cell becomes editable on connection grid
        private void dbconnectDataGridView_EditingControlShowing( object sender, DataGridViewEditingControlShowingEventArgs e )
        {
            // To support password masking in the DataGridView, we record the true value in the DataRow tag property, and set the appropriate 
            // property when the user edits the cell.
            if( ( (DataGridView)sender ).CurrentCell.OwningColumn.Name == "Password" )
            {
                if( dbconnectDataGridView.CurrentRow.Tag != null )
                    e.Control.Text = dbconnectDataGridView.CurrentRow.Tag.ToString( );
                ( (DataGridViewTextBoxEditingControl)e.Control ).UseSystemPasswordChar = true;
            }
            else if( e.Control.GetType( ) == typeof(DataGridViewTextBoxEditingControl) )
                ( (DataGridViewTextBoxEditingControl)e.Control ).UseSystemPasswordChar = false;
        }

        // Called whenever something changes on the connection grid
        private void dbconnectDataGridView_CellValidating( object sender, DataGridViewCellValidatingEventArgs e )
        {
            // Immediately ignore all row and column headings.
            if( e.RowIndex < 0 || e.ColumnIndex < 0 )
                return;

            // The name column controls whether the row is a valid row, and must be saved. It must also be unique.
            if( dbconnectDataGridView.Columns[ e.ColumnIndex ].Name == "Name" )
            {
                dbconnectDataGridView.CurrentRow.ErrorText = String.Empty;

                // Ensure connection name is always unique.
                if( !String.IsNullOrEmpty( (string)e.FormattedValue ) )
                {
                    foreach( PerfectusConnectionSetting pConnect in _configObject.ConnectionSettings )
                    {
                        // Ignore ourselves.
                        if( dbconnectDataGridView.CurrentRow.DataBoundItem == pConnect )
                            continue;

                        if( pConnect.Name == (string)e.FormattedValue )
                        {
                            this.ShowErrorMessage( "Connection Name is already in use." );
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        // Called whenever something changes on the connection grid
        private void dbconnectDataGridView_CellValueChanged( object sender, DataGridViewCellEventArgs e )
        {
            // Immediately ignore all row and column headings.
            if( e.RowIndex < 0 || e.ColumnIndex < 0 )
                return;

            // Flag that something has changed.
            _haveChanges = true;

            // Reset the default connection based on the new name if this is the default.
            if( dbconnectDataGridView.Columns[ e.ColumnIndex ].Name == "Name" && dbconnectDataGridView.CurrentRow.DataBoundItem != null )
            {
                PerfectusConnectionSetting connectObj = (PerfectusConnectionSetting)dbconnectDataGridView.CurrentRow.DataBoundItem;
                if( connectObj.Default )
                    _configObject.SetDefaultConnection( connectObj );
            }
        }

        // Apply button clicked, save changes and notify other windows.
        private void applybut_Click( object sender, EventArgs e )
        {
            // Call internal routine to save.
            SaveConfigFile( );
        }

        // Cancel button click, simply closes window
        private void cancelbut_Click( object sender, EventArgs e )
        {
            // Force the cancellation of any edits to the data connection grid.
            dbconnectDataGridView.CancelEdit( );
            // Shut down
            this.Close( );
        }

        // OK button click, saves and closes window
        private void okbut_Click( object sender, EventArgs e )
        {
            // Call internal routine to save.
            if( SaveConfigFile( ) )
                // Shut down
                this.Close( );
        }

        // The user clicks a particular cell.
        private void dbconnectDataGridView_CellContentClick( object sender, DataGridViewCellEventArgs e )
        {
            // Immediately ignore all row and column headings.
            if( e.RowIndex < 0 || e.ColumnIndex < 0 )
                return;

            // Check if they clicked a connection default button.
            if( dbconnectDataGridView.Columns[ e.ColumnIndex ].Name == "Default" )
            {
                // Set default connection on config list and reload bindings.
                _configObject.SetDefaultConnection( (PerfectusConnectionSetting)dbconnectDataGridView.CurrentRow.DataBoundItem );
                _configObject.ConnectionSettings.ResetBindings( );

                // Flag that something has changed.
                _haveChanges = true;
            }
        }

        // Events on clicking the grid view table. Maybe assign context menu.
        private void dbconnectDataGridView_CellMouseUp( object sender, DataGridViewCellMouseEventArgs e )
        {
            // If right mouse and within a data row, display context menu.
            if( ( e.Button == MouseButtons.Right ) && ( e.RowIndex >= 0 ) && (e.ColumnIndex >= 0) )
            {
                // End the editing of the current cell so that the data will be incorporated into any further action.
                dbconnectDataGridView.EndEdit( );

                // Get the row
                DataGridViewRow dataRow = dbconnectDataGridView.Rows[ e.RowIndex ];
                if( dataRow == null )
                    return;

                // Check its a valid row.
                bool hasContent = false;
                foreach( DataGridViewCell cell in dataRow.Cells )
                {
                    if( cell.Visible == false || cell.OwningColumn.CellType != typeof( DataGridViewTextBoxCell ) )
                        continue;

                    if( cell.Value is string && !String.IsNullOrEmpty( (string)cell.Value ) )
                    {  hasContent = true; break; }
                }

                if( !hasContent )
                    return;

                // Create context menu
                ContextMenuStrip menu = new ContextMenuStrip( );
                menu.Items.Add( _upgradeToolStripMenu );
                menu.Items.Add( _testToolStripMenu );
                menu.Items.Add( _deleteToolStripMenu );
                // Assign bound item 
                _upgradeToolStripMenu.Tag = dataRow.DataBoundItem;
                _testToolStripMenu.Tag = dataRow.DataBoundItem;
                _deleteToolStripMenu.Tag = dataRow.DataBoundItem;

                // Show context menu.
                Point pt = dbconnectDataGridView.PointToClient( Control.MousePosition );
                    menu.Show( dbconnectDataGridView, pt.X, pt.Y );
            }
        }

        // User clicks option to test a row.
        private void testMenuItem_Click( object sender, EventArgs e )
        {
            // Extract bound object
            if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is PerfectusConnectionSetting )
            {
                PerfectusConnectionSetting connect = (PerfectusConnectionSetting)( (ToolStripMenuItem)sender ).Tag;

                // Perform the test now.
                string result;
                if( _configObject.TestConnection( connect, out result ) )
                    MessageBox.Show( result, "Connection Test", MessageBoxButtons.OK, MessageBoxIcon.Information );
                else
                    MessageBox.Show( result, "Connection Test", MessageBoxButtons.OK, MessageBoxIcon.Error );
            }
        }

        // User clicks option to upgraw/install a row.
        private void upgradeMenuItem_Click( object sender, EventArgs e )
        {
            try
            {
                // Extract bound object
                if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is PerfectusConnectionSetting )
                {
                    // Perform the actual installing and or upgrading.
                    if( _configObject.UpgradeInstallDatabase( (PerfectusConnectionSetting)( (ToolStripMenuItem)sender ).Tag ) )
                    {
                        // Reset bindings to have the grid updated.
                        _configObject.ConnectionSettings.ResetBindings( );

                        // Flag that something has changed.
                        _haveChanges = true;
                    }
                }

            }
            catch( Exception ex )
            {
                this.ShowErrorMessage( ex.Message );
            }
        }

        // User clicks option to delete a row.
        private void deleteMenuItem_Click( object sender, EventArgs e )
        {
            // Extract bound object
            if( ( ( (ToolStripMenuItem)sender ).Tag != null ) && ( (ToolStripMenuItem)sender ).Tag is PerfectusConnectionSetting )
            {
                // Clear all data from the selected object
                PerfectusConnectionSetting connect = (PerfectusConnectionSetting)( (ToolStripMenuItem)sender ).Tag;
                connect.Clear( );

                // Remove and re-add the object to move it to the end and keep the same grid size.
                _configObject.ConnectionSettings.Remove( connect );
                _configObject.ConnectionSettings.Add( connect );

                // Reset bindings to have the grid updated.
                _configObject.ConnectionSettings.ResetBindings( );

                // Flag that something has changed.
                _haveChanges = true;
            }
        }

        // Form is just about to close.
        private void AdminConfig_FormClosing( object sender, FormClosingEventArgs e )
        {
            if( !this.CanClose( ) )
            {
                if( MessageBox.Show( "Are you sure you want to discard your changes?", "Confirmation",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning ) != DialogResult.Yes )
                    e.Cancel = true;
            }
        }
        #endregion

    }
}