using System;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.API
{
	/// <summary>
	/// New APIs (introduced in 5.1.5) for submitting AnswerSet's and other XML documents to Perfectus.
	/// </summary>
	public sealed class Submit
	{

		public static Guid SubmitAnswersetDocument(XmlDocument answerSet, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{		
			return Submitter.SubmitAnswersetDocument(answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static Guid SubmitAnswersetFile(string inputFilePath, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return Submitter.SubmitAnswersetFile(inputFilePath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static Guid SubmitAnswersetString(string inputXml, string inputTransform, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return Submitter.SubmitAnswersetString(inputXml, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}	
				
		public static Guid[] SubmitXmlFileBatch(string inputXmlFilePath, string transformPath, string batchRootXPath, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return Submitter.SubmitXmlFileBatch(inputXmlFilePath, transformPath, batchRootXPath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}

		public static Guid[] SubmitXPathDocumentBatch(XPathDocument xPathDocument, XslTransform transform, string batchRootXPath, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return Submitter.SubmitXPathDocumentBatch(xPathDocument, transform, batchRootXPath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}

		public static void SubmitAnswersetDocumentToInstance(Guid instanceId, XmlDocument answerSet, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{		
			Submitter.SubmitAnswersetDocumentToInstance(instanceId, answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static void SubmitAnswersetFileToInstance(Guid instanceId, string inputFilePath, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			Submitter.SubmiAnswersetFileToInstance(instanceId, inputFilePath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static void SubmitAnswersetStringToInstance(Guid instanceId, string inputXml, Submitter.QuestionMatchModeOption questionMatchMode, Submitter.ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			Submitter.SubmiAnswersetlStringToInstance(instanceId, inputXml, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}	
	}
}
