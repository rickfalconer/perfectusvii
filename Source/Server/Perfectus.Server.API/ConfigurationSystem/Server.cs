using Perfectus.Server.ConfigurationSystem.ServerSystem;

namespace Perfectus.Server.API.ConfigurationSystem
{
	/// <summary>
	///		This class provides a means to query the server for its standard information.
	/// </summary>
	public sealed class Server
	{
		/// <summary>
		///		Private constructor.
		/// </summary>
		private Server()
		{}

		/// <summary>
		///		Asks the server to provide its name, message and unique identifier.
		/// </summary>
		/// <returns>Returns a <b>ServerInfo</b> object, which contains the server's unique identifier, its name, and its description.  This information is used by client applications (for example, the IPManager) to uniquely identify a particular server and its capabilities.</returns>
		/// <example>
		/// <code>
		///		ServerInfo info = Perfectus.Server.API.ConfigurationSystem.Server.GetServerInfo();
		/// </code>
		/// </example>
		public static ServerInfo GetServerInfo()
		{
			return new ServerInfo();
		}

		public static ServerPublishInfo GetServerPublishInfo()
		{
			return new ServerPublishInfo();
		}

	}
}