using Perfectus.Server.ConfigurationSystem.PluginSystem;

namespace Perfectus.Server.API.ConfigurationSystem
{
    /// <summary>
    ///		This class provides a means for querying the server's plug-in configuration.
    /// </summary>
    public sealed class Plugins
    {
        /// <summary>
        ///		Private constructor.
        /// </summary>
        private Plugins()
        { }

        /// <summary>
        ///		Asks the server to scan its configured Plugins directory.
        /// </summary>
        /// <returns>Returns a collection of <b>PluginDescriptor</b> objects.  These contain the information necessary to reference server-side plug-ins from a client application.  For example, the IPManager application, or the <b>SDK Samples/CreateAndPublishPackage</b> sample application.</returns>
        /// <example>
        /// <code>
        ///		PluginDescriptorCollection plugins = Perfectus.Server.API.ConfigurationSystem.Plugins.GetInstalledPlugins();
        /// </code>
        /// </example>
        public static PluginDescriptorCollection GetInstalledPlugins()
        {
            return Perfectus.Server.ConfigurationSystem.Plugins.PluginDescriptors;
        }
    }
}