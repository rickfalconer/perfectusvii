using System;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.API.PackageManagement
{
	/// <summary>
	///		This class provides the means to submit a package to the server.
	/// </summary>
	public sealed class Publishing
	{
		/// <summary>
		///  Private constructor.
		/// </summary>
		private Publishing()
		{}

		/// <summary>
		///		Accepts a deserialised Package object (typically with Template WordML stored in-line) and stores it in the Perfectus database.  Shortcuts to the Package will be placed in each folder specified in the <b>folderIdsToPublishTo</b> array.
		/// </summary>
		/// <param name="packageToPublish">The Package to publish.</param>
		/// <param name="isNewVersion">If set to true, the Package's version number will be incremented if it has been previously published to the same server.</param>
		public static void Publish(Package packageToPublish, bool isNewVersion)
		{
			int versionNumber;
			int revisionNumber;
			Publish(packageToPublish, isNewVersion, out versionNumber, out revisionNumber);
		}

		/// <summary>
		///		Accepts a deserialised Package object (typically with Template WordML stored in-line) and stores it in the Perfectus database.  Shortcuts to the Package will be placed in each folder specified in the <b>folderIdsToPublishTo</b> array.
		/// </summary>
		/// <param name="packageToPublish">The Package to publish.</param>
		/// <param name="isNewVersion">If set to true, the Package's version number will be incremented if it has been previously published to the same server.</param>
		/// <param name="versionNumber">[out] Returns the version number assigned to the published Package.</param>
		/// <param name="revisionNumber">[out] Returns the revision number assigned to the published Package.</param>
		public static void Publish(Package packageToPublish, bool isNewVersion, out int versionNumber, out int revisionNumber)
		{
			packageToPublish.PublishedDateTime = DateTime.UtcNow;
			PackageManager.Publishing.Publish(packageToPublish, isNewVersion, out versionNumber, out revisionNumber);
		}
	}
}