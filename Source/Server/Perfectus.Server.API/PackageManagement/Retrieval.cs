/*using System;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.API.PackageManagement
{
	/// <summary>
	///		This class provides the means to retrieve Packages and Package instances from the server.
	/// </summary>
	public sealed class Retrieval
	{
		/// <summary>
		///		Private constructor.
		/// </summary>
		private Retrieval()
		{}

		/// <summary>
		///		Retrieves a Package (without the Template WordML) from the server.
		/// </summary>
		/// <param name="packageId">The <see cref="Guid"></see> identifying the package to retrieve.</param>
		/// <param name="versionNumber">The version number of the package to retrieve - as assigned by the server at publication-time.</param>
		/// <param name="revisionNumber">The revision number of the package to retrieve - as assigned by the server at publication-time.</param>
		/// <returns></returns>
		public static Package GetPackageWithoutTemplateData(Guid packageId, int versionNumber, int revisionNumber)
		{
			return PackageManager.Retrieval.GetPackageWithoutTemplateData(packageId, versionNumber, revisionNumber);
		}
	}
}*/