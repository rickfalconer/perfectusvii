using System;
using System.Xml;
using Perfectus.Server.AnswerAcquirer.Xml;
using Perfectus.Server.Common;
using Perfectus.Server.PackageManager;

namespace Perfectus.Server.API.AnswerAcquisition
{
	/// <summary>
	///		This class provides a means to create a new Perfectus EasyInterview instance, and answer it with a
	///		full or partial set of answers.
	/// </summary>
	/// <remarks>
	///		<para>
	///		When called, the Perfectus server will create a new, empty instance of the specified Package.
	///		Then, the Interview will be automatically completed on a page-by-page basis.
	///		</para>
	/// </remarks>
	/// <example>
	///		To create a new instance of a specific Package and place it in the folder with an ID of '1':
	///		<code>
	///			Perfectus.Server.API.AnswerAcquisition.Acquirer.Acquire(answerSetXml, "Sample Instance created by the API", 1);
	///		</code>
	///		See the <b>SDK Samples/CreatePublishAndProvideAnswers</b> sample project for an example XML AnswerSet document.
	/// </example>
	public sealed class Acquirer
	{
	
		/// <summary>
		///		Private constructor.
		/// </summary>
		private Acquirer()
		{}

		/// <summary>
		///		Creates a new EasyInterview instance and provides it with answers.  Optionally, the interview completion can be configured to stop at a particular page.
		/// </summary>
		/// <remarks>
		///		This method causes the server to take the following steps:
		///		1. Create a new instance of the specified Package in the specified folder.
		///		2. Walk through the EasyInterview, from the Start Page, calling Web Services and applying answers, as required.
		///		
		///		To submit a completed instance to the ProcessCoordinator see <see cref="Perfectus.Server.API.ProcessCoordinator.TaskSubmission">ProcessCoordinator.TaskSubmission</see>.
		/// </remarks>
		/// <example>
		///		<code>
		///			Perfectus.Server.API.AnswerAcquisition.Acquire(answerSetXml, "Sample Instance created by the API", 1);
		///		</code>
		/// </example>
		/// <param name="answerSetXml">An <see cref="XmlDocument"></see> containing the answers to some or all of the Questions in the Package.</param>
		/// <param name="reference">A user-friendly reference to identify this instance in the My Documents view of a folder.  E.g. "Sample Instance: 12 Oct 2004".</param>
		/// <param name="folderId">The id of the folder into which the Instance should be placed.  A list of available folders can be retrieved via the <see cref="Perfectus.Server.API.ConfigurationSystem.Folders">ConfigurationSystem.Folders API</see>.</param>
		/// <param name="stopAtPageId">The unique identifier of the page at which the interview 'walk' should stop.  No answers will be provided to Pages that appear after this page in the chosen Interview flow.</param>
		/// <returns>Returns the server's unique identifier for the newly created EasyInterview Instance.</returns>	
		public static Guid Acquire(XmlDocument answerSetXml, string reference, int folderId, Guid stopAtPageId)
		{
			// Get the package information (Id, version and revision) from the XML

			string packageIdString = answerSetXml.SelectSingleNode("/answerSet/@packageId").Value;
			string packageVersionString = answerSetXml.SelectSingleNode("/answerSet/@packageVersion").Value;
			string packageRevisionString = answerSetXml.SelectSingleNode("/answerSet/@packageRevision").Value;

			Guid id = new Guid(packageIdString);
			int version = Convert.ToInt32(packageVersionString);
			int revision = Convert.ToInt32(packageRevisionString);

			// Create a new instance of the given package
			//int personId = Person.LoggedinPersonIdGet();
			//int instanceId = Publishing.CreateInstance(id, version, revision, reference, folderId, personId);
			Guid instanceId = InterviewSystem.InstanceManagement.CreateInstance(id, version, revision);

			// Apply the XML answerSet to the instance
			ApplyAnswerSetXml.Apply(instanceId, answerSetXml, stopAtPageId);

			return instanceId;
		}

		/// <summary>
		///		Creates a new EasyInterview instance and provides it with answers.  Optionally, the interview completion can be configured to stop at a particular page.
		/// </summary>
		/// <remarks>
		///		This method causes the server to take the following steps:
		///		1. Create a new instance of the specified Package in the specified folder.
		///		2. Walk through the EasyInterview, from the Start Page, calling Web Services and applying answers, as required.
		///		
		///		To submit a completed instance to the ProcessCoordinator see <see cref="Perfectus.Server.API.ProcessCoordinator.TaskSubmission">ProcessCoordinator.TaskSubmission</see>.
		/// </remarks>
		/// <example>
		///		<code>
		///			Perfectus.Server.API.AnswerAcquisition.Acquire(answerSetXml, "Sample Instance created by the API", 1);
		///		</code>
		/// </example>
		/// <param name="answerSetXml">An <see cref="XmlDocument"></see> containing the answers to some or all of the Questions in the Package.</param>
		/// <param name="reference">A user-friendly reference to identify this instance in the My Documents view of a folder.  E.g. "Sample Instance: 12 Oct 2004".</param>
		/// <param name="folderId">The id of the folder into which the Instance should be placed.  A list of available folders can be retrieved via the <see cref="Perfectus.Server.API.ConfigurationSystem.Folders">ConfigurationSystem.Folders API</see>.</param>
		/// <returns>Returns the server's unique identifier for the newly created EasyInterview Instance.</returns>		
		public static Guid Acquire(XmlDocument answerSetXml, string reference, int folderId)
		{
			return Acquire(answerSetXml, reference, folderId, Guid.Empty);
		}


	}
}