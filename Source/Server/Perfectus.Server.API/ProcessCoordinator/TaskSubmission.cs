using System;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.API.ProcessCoordinator
{
	/// <summary>
	///		Allows processing tasks (answered package instances) to be submitted to the ProcessCoordinator service via the Coordinator's message queue.
	/// </summary>
	public sealed class TaskSubmission
	{
		/// <summary>
		///		Private constructor.
		/// </summary>
		private TaskSubmission()
		{
		}

		/// <summary>
		///		Submits an answered Package object (instance) to the Process Coordinator service's message queue.
		/// </summary>
		/// <param name="instance">The answered instance of a Package.</param>
		/// <param name="instanceId">The unique identifier the server uses for this instance.</param>
		/// <param name="versionNumber">The version number of the Package answered by this instance.</param>
		/// <param name="revisionNumber">The revision number of the Package answered by this instance.</param>
		public static void Submit(Package instance, Guid instanceId, int versionNumber, int revisionNumber)
		{
            Job job = new Job(instance.UniqueIdentifier, instanceId, versionNumber, revisionNumber, Identity.GetRunningUserId());
            InstanceManagement.EnQueuePackageInstance(job);
		}
	}
}
