﻿            $serviceName = "Perfectus.Server.ProcessCoordinator.Service"
            $displayName = "Perfectus ProcessCoordinator V7"
            # location where our AzureVMs File Copy RM Task copies our build outputs
            $sourceLocation = "C:\temp\*"
            $destinationLocation = "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator\"
            $binaryName = $destinationLocation + "Perfectus.Server.ProcessCoordinator.Service.exe"
            $serviceDef = Get-Service -Name $serviceName -ErrorAction SilentlyContinue

            If ($serviceDef -eq $null)
            {
                # first install - create directory
                #New-Item $destinationLocation -ItemType directory 
                #Copy-Item $sourceLocation $destinationLocation -Force
                New-Service -Name $serviceName -StartupType Automatic -DisplayName $displayName -BinaryPathName $binaryName
            }
            else
            {
                # has already been installed
                if($serviceDef.Status -eq "Running")
                {
                    #Stop-Service -Name $serviceName
                }
                #Copy-Item $sourceLocation $destinationLocation -Force
            }
            
            Start-Service -Name $serviceName

