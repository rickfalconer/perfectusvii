using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus Server Process Coordinator Service Host")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs