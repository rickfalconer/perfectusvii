using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Messaging;
using System.Text;
using System.Xml;

namespace Devharness
{
    class Program
    {
        static void Main(string[] args)
        {
            string importFolderPath = ConfigurationSettings.AppSettings["ImportFolder"];
            string MSMQ = ConfigurationSettings.AppSettings["MSMQ"];

            SubmitTestFilesToMSMQ(importFolderPath, MSMQ);
        }

        static void SubmitTestFilesToMSMQ(string importFolderPath, string MSMQ)
        {            
            // Get the files in the import folder
            string[] files = Directory.GetFiles(importFolderPath);

            // Submit them one at a time to the queue
            foreach (string file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(file);

                // Submit the xmlDocument to the MSMQ
                SubmitFileToMSMQ(MSMQ, xmlDocument, fileInfo.Name);
            }
        }

        static void SubmitFileToMSMQ(string MSMQ, XmlDocument xmlDocument, string filename)
        {
            try
            {
                if( !MessageQueue.Exists(MSMQ))
                {
                    MessageQueue.Create(MSMQ);    
                }                        
                
                MessageQueue queue= new MessageQueue(MSMQ);
                queue.Send(xmlDocument, filename);
            }           
            catch(MessageQueueException ex)
            {
                 Console.WriteLine(ex.Message);
            }
        }
    }
}