using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Server.ReportingSynchronizerEngine
{
    public delegate void ErrorHandler(ErrorHandlerArgs e);

    public class ErrorHandlerArgs
    {
        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private Exception exception;

        public Exception Exception
        {
            get { return exception; }
            set { exception = value; }
        }
    }
}
