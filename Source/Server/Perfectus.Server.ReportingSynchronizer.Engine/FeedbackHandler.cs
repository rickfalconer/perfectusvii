using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Server.ReportingSynchronizerEngine
{
    public delegate void FeedbackHandler(FeedbackHandlerArgs e);

    public class FeedbackHandlerArgs
    {
        private string message;

        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }
}
