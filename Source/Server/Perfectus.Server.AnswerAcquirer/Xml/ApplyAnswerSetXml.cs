using System;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.AnswerAcquirer.Xml
{
	/// <summary>
	/// Summary description for ApplyAnswerSetXml.
	/// </summary>
    [Obsolete("Please begin using the new Perfectus.Server.AnswerAcquirer.Xml.Submitter class. (as of 5.1.5)")]
	public sealed class ApplyAnswerSetXml
	{
		private ApplyAnswerSetXml()
		{
		}

		public static void Apply(Guid instanceId, XmlDocument answerSetXml, Guid stopAtPageId)
		{
			// Get the instance and its current page.
			Guid currentPageId;
			Package instance = Perfectus.Server.InterviewSystem.InstanceManagement.GetInstance(instanceId, out currentPageId);   
			ApplyAnswersToPage(instanceId, instance, currentPageId, answerSetXml, stopAtPageId);
		}	
	
		public static void Apply(Guid instanceId, Package instance, XmlDocument answerSetXml, Guid currentPageId, Guid stopAtPageId)
		{
			ApplyAnswersToPage(instanceId, instance, currentPageId, answerSetXml, stopAtPageId);
		}	

		public static void ApplyAnswerSet(Package instance, XmlDocument answerSetXml)
		{
//			Package instance = Perfectus.Server.InterviewSystem.InstanceManagement.GetInstance(instanceId, out currentPageId); 			
			ApplyAnswersFromAnswerSet(instance, answerSetXml);
		}

		public static void ApplyAnswerSetRaw(Package instance, XmlDocument answerSetXml)
		{
			ApplyAnswersFromAnswerSetRaw(instance, answerSetXml);
		}

        [Obsolete("Does nothing, will be removed.")]
		public static void Apply(int instanceId, XmlDocument answerSetXml, Guid stopAtPageId)
		{
			// Get the instance and its current page.
		}

		private static void ApplyAnswersToPage(Guid instanceId, Package instance, Guid pageId, XmlDocument answerSetXml, Guid stopAtPageId)
		{			
			// Get the first page we're answering
			InterviewPage page = instance.InterviewPageByGuid[pageId];
			do
			{
				bool shouldShowPage = page.Visible.Evaluate(true, AnswerProvider.Caller.System);
				if (page is InternalPage && shouldShowPage)
				{
					// Call any web services, &c. for this page
					new Perfectus.Server.AnswerAcquirer.AnswerInstance.BindingSystem(instance).Bind(page);

					// Get a collection of all questions on the page (by recursing through container objects)
					QuestionCollection questionsToAnswer = ((InternalPage)page).GetQuestionsOnPage();

					foreach(Question q in questionsToAnswer)
					{
						// Set all answers to new empty collections						
						if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
						{
							q.AnswersByPerson.Clear();
						}
					}

					// Answer each question					
					foreach(Question q in questionsToAnswer)
					{
						// Set all answers to new empty collections						
						if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
						{
							q.AnswersByPerson.Clear();
						}
						q.Seen = true;
						// Get the answer(s) from the XML
						XmlNodeList answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@answeredItemId='{0}']", q.UniqueIdentifier));
						foreach(XmlNode answerNode in answerNodes)
						{
							string answer = answerNode.InnerText.Trim();
						
							AnswerCollectionCollection collColl = q.AnswersByPerson;//GetAnswerCollectionCollection(AnswerProvider.Caller.Interview);

							if (collColl == null)
							{
								collColl = new AnswerCollectionCollection();
								q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
							}
						
							object bestTypeAnswer = AnswerProvider.ConvertToBestType(answer, q.DataType, AnswerProvider.Caller.Interview, q.StringBindingMetaData);
							collColl.Add(new AnswerCollection(new object[]{bestTypeAnswer}));
						
						}
					}
				}
				InterviewPage answeredPage = page;
				answeredPage.Seen = true;
				
				// Work out the next page
				if (page.NextPage != null && page.NextPage.ValueType != PageQueryValue.PageQueryValueType.NoValue)
				{
					if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query)
					{
						page = ((NavigationAction)(page.NextPage.QueryValue.Evaluate(AnswerProvider.Caller.System))).Page;
					} else if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Page)
					{
						page = page.NextPage.PageValue;
					} else
					{
						page = null;
					}
				}
				else
				{
					// Exit condition
					page = null;
				}

				//bool endOfInterview = (page == null);

				if (shouldShowPage)
				{
					Perfectus.Server.InterviewSystem.InstanceManagement.NavigationStackPush(instanceId, answeredPage.UniqueIdentifier, answeredPage.Name);
				}

			} while (page != null && page.UniqueIdentifier != stopAtPageId);
		}

		private static void ApplyAnswersFromAnswerSet(Package instance, XmlDocument answerSetXml)
		{
			// Get the first page we're answering
			InterviewPage page = instance.Interviews[0].StartPage;
			do
			{
			{
				// Call any web services, &c. for this page
				//new Perfectus.Server.AnswerAcquirer.AnswerInstance.BindingSystem(instance).Bind(page);

				// Get a collection of all questions on the page (by recursing through container objects)
				if(page is InternalPage)
				{
					QuestionCollection questionsToAnswer = ((InternalPage)page).GetQuestionsOnPage();

					// Set all answers to new empty collections, but only if they are specified in the AnswerSet.
					foreach (Question q in questionsToAnswer)
					{
						if (answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name)).Count > 0)
						{
							if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
							{
								q.AnswersByPerson.Clear();
							}
						}
					}

					// Answer each question
					foreach(Question q in questionsToAnswer)
					{
						q.Seen = true;
						// Get the answer(s) from the XML						
						XmlNodeList answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name));
						foreach(XmlNode answerNode in answerNodes)
						{
							string answer = answerNode.InnerText.Trim();
							bool success;
							AnswerProvider.ConvertToBestType(answer, q.DataType, out success, AnswerProvider.Caller.Interview, q.StringBindingMetaData);

							if(success == true && q.DataType != PerfectusDataType.File)
							{
								AnswerCollectionCollection collColl =  q.AnswersByPerson;       
								if (collColl == null)
								{
									collColl = new AnswerCollectionCollection();
									q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
								}

								object bestType = AnswerProvider.ConvertToBestType(answer, q.DataType,AnswerProvider.Caller.Interview, q.StringBindingMetaData);	
								collColl.Add(new AnswerCollection(new object[]{bestType}));
							}
						}
					}
				}
			}
			
				page.Seen = true;
				// Work out the next page
				if (page.NextPage != null && page.NextPage.ValueType != PageQueryValue.PageQueryValueType.NoValue)
				{
					if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query)
					{
						page = ((NavigationAction)(page.NextPage.QueryValue.Evaluate(AnswerProvider.Caller.System))).Page;
					} 
					else if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Page)
					{
						page = page.NextPage.PageValue;
					} 
					else
					{
						page = null;
					}
				}
				else
				{
					// Exit condition
					page = null;
				}


			} while (page != null);
		}

		private static void ApplyAnswersFromAnswerSetRaw(Package instance, XmlDocument answerSetXml)
		{
			foreach (Question q in instance.Questions)
			{
				if (answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name)).Count > 0)
				{
					if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
					{
						q.AnswersByPerson.Clear();
					}
				}
			}

			// Answer each question
			foreach(Question q in instance.Questions)
			{
				q.Seen = true;
				// Get the answer(s) from the XML						
				XmlNodeList answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name));
				foreach(XmlNode answerNode in answerNodes)
				{
					string answer = answerNode.InnerText.Trim();
					bool success;
					AnswerProvider.ConvertToBestType(answer, q.DataType, out success, AnswerProvider.Caller.Interview, q.StringBindingMetaData);

					if(success == true && q.DataType != PerfectusDataType.File)
					{
						AnswerCollectionCollection collColl =  q.AnswersByPerson;       
						if (collColl == null)
						{
							collColl = new AnswerCollectionCollection();
							q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
						}

						object bestType = AnswerProvider.ConvertToBestType(answer, q.DataType,AnswerProvider.Caller.Interview, q.StringBindingMetaData);	
						collColl.Add(new AnswerCollection(new object[]{bestType}));
					}
				}
			}
		}
	}
}
