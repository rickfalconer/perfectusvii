using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.Common.Data;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.AnswerAcquirer.Xml
{
	/// <summary>
	/// Summary description for Submitter.
	/// </summary>
	public class Submitter
	{ 
		/// <summary>
		///		Identifies the method that will be used to match answer elements in the answerset to Question objects in the package definition.
		/// </summary>
		public enum QuestionMatchModeOption
		{
			/// <summary>
			///		Perfectus will match the answer element's @name attribute to the Question's Name property, if possible.
			/// </summary>
			Name, 
			/// <summary>
			///		Perfectus will require the answer element's @answeredItemId attribute to match the Question's UniqueIdentifier (Guid) property.
			/// </summary>
			Guid
		};
		
		
		/// <summary>
		///		Determines the order in which answers will be applied to questions in the Interview.
		/// </summary>
		public enum ApplyModeOption
		{
			/// <summary>
			///		Answers are applied page by page, simulating a manual Interview and following the navigation rules. Questions on Pages not seen during an Interview are not answered.
			/// </summary>
			SimulateInterview, 
			/// <summary>
			///		Answers are applied in the order they appear in the AnswerSet, regardless of Interview structure. All Questions specified in the AnswerSet will be answered.
			/// </summary>
			Raw
		};
		
		public static Guid SubmitAnswersetDocumentToInstance(Guid instanceId, XmlDocument answerSet, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			if (applyMode == ApplyModeOption.Raw && pageToStopAt != Guid.Empty)
			{
				throw new ArgumentException(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("RawApplyMode"), "pageToStopAt");				
			}
			
			if (instanceId == Guid.Empty)
			{
				// Get the package details from the answerSet
				string packageIdString = answerSet.SelectSingleNode("/answerSet/@packageId").Value;
				string packageVersionString = answerSet.SelectSingleNode("/answerSet/@packageVersion").Value;
				string packageRevisionString = answerSet.SelectSingleNode("/answerSet/@packageRevision").Value;

				Guid id = new Guid(packageIdString);
				int version = Convert.ToInt32(packageVersionString);
				int revision = Convert.ToInt32(packageRevisionString);

				instanceId = InstanceManagement.CreateInstance(id, version, revision);
			}
			
			Guid currentPageId;
			Package instance = InstanceManagement.GetInstance(instanceId, out currentPageId);
			
			switch(applyMode)
			{
				case ApplyModeOption.SimulateInterview:
					ApplyAnswersToPage(instanceId, instance, currentPageId, answerSet, pageToStopAt, questionMatchMode, true);
					break;
				case ApplyModeOption.Raw:
					ApplyAnswersFromAnswerSetRaw(instance, answerSet, questionMatchMode);
					break;
			}
			
			// Save the updated instance
			InstanceManagement.SaveInstance(instanceId, instance);
			
			if (processAfterSubmit)
			{
				int version;
				int revision;
				InstanceManagement.GetVersion(instanceId, out version, out revision);
				Job job = new Job(instance.UniqueIdentifier, instanceId, version, revision, Identity.GetRunningUserId());
				InstanceManagement.EnQueuePackageInstance(job, processHighPriority);			
			}
		
			return instanceId;
		}
				
		public static Guid SubmitAnswersetDocument(XmlDocument answerSet, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return SubmitAnswersetDocumentToInstance(Guid.Empty, answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static Guid SubmitAnswersetFile(string inputFilePath, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return SubmiAnswersetFileToInstance(Guid.Empty, inputFilePath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}
		
		public static Guid SubmiAnswersetFileToInstance(Guid instanceId, string inputFilePath, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			XmlDocument answerSet = new XmlDocument();
			// Transform the document with the XSLT provided
			answerSet.Load(inputFilePath);				
			// Submit it via our own API
			return SubmitAnswersetDocumentToInstance(instanceId, answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);				
		}
		
		public static Guid SubmitAnswersetString(string inputXml, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			return SubmiAnswersetlStringToInstance(Guid.Empty, inputXml, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);	
		}		
		
		public static Guid SubmiAnswersetlStringToInstance(Guid instanceId, string inputXml, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			XmlDocument answerSet = new XmlDocument();
			answerSet.LoadXml(inputXml);
			
			// Submit it via our own API
			return SubmitAnswersetDocumentToInstance(instanceId, answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}	

		public static Guid[] SubmitXmlFileBatch(string inputXmlFilePath, string transformPath, string batchRootXPath, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			if (inputXmlFilePath == null || inputXmlFilePath.Trim().Length == 0)
			{
				throw new ArgumentNullException("inputXmlPath", ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("inputXmlPath"));
			}
			if (transformPath == null || transformPath.Trim().Length == 0)
			{
				throw new ArgumentNullException("transformPath", ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("transformPath"));
			}
			XslTransform transform = new XslTransform();
			transform.Load(transformPath);

			XPathDocument doc = new XPathDocument(inputXmlFilePath);
			return SubmitXPathDocumentBatch(doc, transform, batchRootXPath, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority);
		}				

		public static Guid[] SubmitXPathDocumentBatch(XPathDocument xPathDocument, XslTransform transform, string batchRootXPath, QuestionMatchModeOption questionMatchMode, ApplyModeOption applyMode, Guid pageToStopAt, bool processAfterSubmit, bool processHighPriority)
		{
			if (batchRootXPath == null || batchRootXPath.Trim().Length == 0)
			{
				throw new ArgumentNullException("batchRootXPath", ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("batchRootXPath"));
			}
			
			ArrayList instanceIds = new ArrayList();
			
			
			XmlDocument answerSet = new XmlDocument();
			//XPathDocument xpathInputDocument = new XPathDocument(inputXmlFilePath);
			// Set up an iterator
			XPathNavigator navigator = xPathDocument.CreateNavigator();
			XPathNodeIterator iterator = navigator.Select(batchRootXPath);
			XsltArgumentList args = new XsltArgumentList();
			while (iterator.MoveNext())
			{
				try
				{
					args.Clear();
					args.AddParam("iteration", string.Empty, iterator.CurrentPosition);
					XmlReader reader = transform.Transform(xPathDocument.CreateNavigator(), args, new XmlUrlResolver());
		
					// Load the output into an XmlDocument
					answerSet.Load(reader);

					// Submit it via our own API
					instanceIds.Add(SubmitAnswersetDocument(answerSet, questionMatchMode, applyMode, pageToStopAt, processAfterSubmit, processHighPriority));
				}
				catch (Exception ex)
				{
					// Add the iterator's position to whatever exception is caught.
					throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("IterationException"), iterator.CurrentPosition), ex);
				}
			}		
			return (Guid[]) instanceIds.ToArray(typeof (Guid));		
		}

        /// <summary>
        ///     Applies the AnswerSet to the given package instance.
        /// </summary>
        /// <remarks>
        ///     This function does not submit anything to Perfectus. The answers a simply applied to the instance and is then returned
        ///     to the caller.
        /// </remarks>
        /// <param name="instance">The package instance to have it's questions answers populated.</param>
        /// <param name="answerSetXml">The AnswerSet whose data is to be applied to the Package Instance.</param>
        /// <param name="questionMatchMode">The mode to match the AnswerSet answers to the instances questions.</param>
        /// <param name="updateNavigationStack">Change the resume location when a page is processed.</param>
        public static void ApplyAnswerSet(Package instance, XmlDocument answerSetXml, QuestionMatchModeOption questionMatchNode,
                                          bool updateNavigationStack)
        {
            Guid startPageID = instance.Interviews[0].StartPage.UniqueIdentifier;
            ApplyAnswersToPage(instance.UniqueIdentifier, instance, startPageID, answerSetXml, 
                               Guid.Empty, questionMatchNode, updateNavigationStack);
        }
	
	    /// <summary>
	    ///     Applies the <see cref="answerSetXml">AnswerSet's</see> answers to matched questions in the package <see cref="instance">Instance</see>.
	    /// </summary>
	    /// <param name="instanceId">The id of the given <see cref="instance">instance</see></param>
	    /// <param name="instance">A package instance.</param>
	    /// <param name="pageId">The id of the page where processing should start.</param>
	    /// <param name="answerSetXml">The AnswerSet</param>
	    /// <param name="stopAtPageId">The id of the page to stop processing at.</param>
	    /// <param name="questionMatchMode">The mode to match the AnswerSet answers to the instances questions.</param>
        /// <param name="updateNavigationStack">Pop processed pages onto the stack?</param>
		internal static void ApplyAnswersToPage(Guid instanceId, Package instance, Guid pageId, XmlDocument answerSetXml, 
                                                Guid stopAtPageId, QuestionMatchModeOption questionMatchMode, bool updateNavigationStack)
		{			
			// Get the first page we're answering
			InterviewPage page = instance.InterviewPageByGuid[pageId];
			do
			{
				bool shouldShowPage = page.Visible.Evaluate(true, AnswerProvider.Caller.System);
				if (page is InternalPage && shouldShowPage)
				{
					// Call any web services, &c. for this page
					new AnswerInstance.BindingSystem(instance).Bind(page);

					// Get a collection of all questions on the page (by recursing through container objects)
					QuestionCollection questionsToAnswer = ((InternalPage)page).GetQuestionsOnPage();

					foreach(Question q in questionsToAnswer)
					{
                        //pf-394 disabled qs are duplicating
                        //if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
                        if (q != null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
                        {
                            q.AnswersByPerson.Clear();
                        }

					}

					// Answer each question					
					foreach(Question q in questionsToAnswer)
					{
						// Set all answers to new empty collections						
						if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
						{
							q.AnswersByPerson.Clear();
						}
						q.Seen = true;

						// Get the answer(s) from the XML
						XmlNodeList answerNodes;
						switch (questionMatchMode)
						{
							case QuestionMatchModeOption.Guid:
								answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@answeredItemId='{0}']", q.UniqueIdentifier));
								break;
							case QuestionMatchModeOption.Name:
								answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name));
								break;
							default:
								throw new ArgumentOutOfRangeException("questionMatchMode");
						}

                        // Apply the answers to the given question
						if (q.GetRepeaterAnswerComplexity() == Question.RepeaterAnswerType.Complex)
                        {
                            // Handle complex questions (listboxes etc)

                            SortedList<Int32, AnswerCollection> ComplexRepeatedQuestions = HandleComplexQuestion(answerNodes, q);

                            // Add the answer collection to the Answer Coll Collection
                            foreach (KeyValuePair<Int32, AnswerCollection> repeaterRow in ComplexRepeatedQuestions)
                            {
                                AnswerCollectionCollection collColl = q.AnswersByPerson;

                                if (collColl == null)
                                {
                                    collColl = new AnswerCollectionCollection();
                                    q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
                                }

                                collColl.Add(repeaterRow.Value);
                            }
                        }
                        else
                        {
                            // Handle non-complex questions (textboxes etc)

                            foreach (XmlNode answerNode in answerNodes)
                            {
                                string answer = answerNode.InnerText.Trim();
                                AnswerCollectionCollection collColl = q.AnswersByPerson;

                                if (collColl == null)
                                {
                                    collColl = new AnswerCollectionCollection();
                                    q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
                                }

                                object bestTypeAnswer = AnswerProvider.ConvertToBestType(answer, q.DataType, AnswerProvider.Caller.Interview, q.StringBindingMetaData);
                                collColl.Add(new AnswerCollection(new object[] { bestTypeAnswer }));
                            }
                        }
					}
				}

				InterviewPage answeredPage = page;
				answeredPage.Seen = true;
				
				// Work out the next page
				if (page.NextPage != null && page.NextPage.ValueType != PageQueryValue.PageQueryValueType.NoValue)
				{
					if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query)
					{
						page = ((NavigationAction)(page.NextPage.QueryValue.Evaluate(AnswerProvider.Caller.System))).Page;
					} 
					else if (page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Page)
					{
						page = page.NextPage.PageValue;
					} 
					else
					{
						page = null;
					}
				}
				else
				{
					// Exit condition
					page = null;
				}

                if (shouldShowPage && updateNavigationStack)
				{
					InstanceManagement.NavigationStackPush(instanceId, answeredPage.UniqueIdentifier, answeredPage.Name);
				}

			} while (page != null && page.UniqueIdentifier != stopAtPageId);
		}

        private static SortedList<Int32, AnswerCollection> HandleComplexQuestion(XmlNodeList answerNodes, Question question)
        {
            object bestTypeAnswer;
            Int32 currentRepeaterRow = 0;  // Init to not set value
            Int32 previousRepeaterRow = 1; // Init to the first row  
            AnswerCollection repeaterRowAnswerCollection = new AnswerCollection();
            SortedList<Int32, AnswerCollection> complexRepeatedQuestions = new SortedList<int,AnswerCollection>();

            SortedList<Int32, XmlNode> sortedAnswerNodes = GetSortedAnswerNodes(answerNodes);
            
            foreach (XmlNode answerNode in sortedAnswerNodes.Values)
            {
                try
                {
                    currentRepeaterRow = Convert.ToInt32(answerNode.Attributes["repeaterRow"].InnerText);
                }
                catch (InvalidCastException ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("RepeaterRowCastException");
                    message = string.Format(message, answerNode.Attributes["name"].InnerText, answerNode.Attributes["repeaterRow"].InnerText);
                    throw new Exception(message, ex);
                }

                // Are we dealing the same repeater row as the previous answerNode?
                if (currentRepeaterRow != previousRepeaterRow)
                {
                    // New repeated row

                    // Add the previous row to the collection
                    if (currentRepeaterRow > 1)
                    {
                        complexRepeatedQuestions.Add(previousRepeaterRow, repeaterRowAnswerCollection);
                    }

                    // Create a collection for the new repeated row
                    repeaterRowAnswerCollection = new AnswerCollection();
                    previousRepeaterRow = currentRepeaterRow;
                }

                // Get the answer type and add to the collection
                string answer = answerNode.InnerText.Trim();
                bestTypeAnswer = AnswerProvider.ConvertToBestType(answer, question.DataType, AnswerProvider.Caller.Interview, 
                                                                  question.StringBindingMetaData);
                repeaterRowAnswerCollection.Add(bestTypeAnswer);
            }

            // Add the last repeated answerCollection to the list
            complexRepeatedQuestions.Add(currentRepeaterRow, repeaterRowAnswerCollection);

            return complexRepeatedQuestions;
        }

        private static SortedList<Int32, XmlNode> GetSortedAnswerNodes(XmlNodeList answerNodes)
        {
            // Ensure the nodes are sorted by repeater row
            SortedList<Int32, XmlNode> sortedAnswerNodes = new SortedList<Int32, XmlNode>();
            foreach (XmlNode answerNode in answerNodes)
            {
                Int32 sequenceNumber;

                try
                {
                    sequenceNumber = Convert.ToInt32(answerNode.Attributes["sequenceNumber"].InnerText);
                }
                catch (InvalidCastException ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("SequenceNumberCastException");
                    message = string.Format(message, answerNode.Attributes["name"].InnerText, answerNode.Attributes["sequenceNumber"].InnerText);
                    throw new Exception(message, ex);
                }

                try
                {
                    sortedAnswerNodes.Add(sequenceNumber, answerNode);
                }
                catch (Exception ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("SequenceNumberDuplicate");
                    message = string.Format(message, answerNode.Attributes["name"].InnerText, answerNode.Attributes["sequenceNumber"].InnerText);
                    throw new Exception(message, ex);
                }
            }

            return sortedAnswerNodes;
        }
		
		internal static void ApplyAnswersFromAnswerSetRaw(Package instance, XmlDocument answerSetXml, QuestionMatchModeOption questionMatchMode)
		{
			foreach (Question q in instance.Questions)
			{
				if (answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name)).Count > 0)
				{
					if (q != null && q.Enabled == null || q.Enabled.Evaluate(true, AnswerProvider.Caller.System))
					{
						q.AnswersByPerson.Clear();
					}
				}
			}

			// Answer each question
			foreach(Question q in instance.Questions)
			{
				q.Seen = true;
				// Get the answer(s) from the XML
				XmlNodeList answerNodes;
				switch (questionMatchMode)
				{
					case QuestionMatchModeOption.Guid:
						answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@answeredItemId='{0}']", q.UniqueIdentifier));
						break;
					case QuestionMatchModeOption.Name:
						answerNodes = answerSetXml.SelectNodes(string.Format("//answer[@name='{0}']", q.Name));
						break;
					default:
						throw new ArgumentOutOfRangeException("questionMatchMode");
				}

				foreach(XmlNode answerNode in answerNodes)
				{
					string answer = answerNode.InnerText.Trim();
					bool success;
					AnswerProvider.ConvertToBestType(answer, q.DataType, out success, AnswerProvider.Caller.Interview, q.StringBindingMetaData);

					if(success && q.DataType != PerfectusDataType.File)
					{
						AnswerCollectionCollection collColl =  q.AnswersByPerson;       
						if (collColl == null)
						{
							collColl = new AnswerCollectionCollection();
							q.SetAnswers(collColl, AnswerProvider.Caller.Interview);
						}

						object bestType = AnswerProvider.ConvertToBestType(answer, q.DataType,AnswerProvider.Caller.Interview, q.StringBindingMetaData);	
						collColl.Add(new AnswerCollection(new object[]{bestType}));
					}
				}
			}
		}

	}

}