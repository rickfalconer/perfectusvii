using System;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;

namespace Perfectus.Server.AnswerAcquirer.Xml
{
	/// <summary>
	/// Summary description for XmlToMirrorAnswerSet.
	/// </summary>
	public class XmlToMirrorAnswerSet
	{
		public XmlToMirrorAnswerSet()
		{
		}	
		
		public static void CreateMirror(XmlDocument doc)
		{
			doc.SelectSingleNode("/answerSet/@mirrorDocType").Value = "Mirror";
			XmlNodeList lstAnswers = doc.GetElementsByTagName("answer");
			
			// used on mirrorMapping attribute to check if it is a guid
			Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
			
			// loop through answers and perform neccessary mirrors.			
			// after a swap is performed flag it by adding attribute swapFinished=yes, this way we can do both swaps at once and save work server has to do.

			for(int i=0;i<lstAnswers.Count;i++)
			{
				// get mirrorMapping for current Node
				string mirrorMapping = "";	
				
				try 
				{				
					mirrorMapping = lstAnswers[i].Attributes["mirrorMapping"].InnerText;					
					
					if(mirrorMapping != "") // if true we have a mapping to do
					{
						string answeredItemId = lstAnswers[i].Attributes["answeredItemId"].InnerText; // need this to do reverse mapping
						string swapFinished = "no"; // changed to yes when mapping complete.
						
						try
						{
							swapFinished = lstAnswers[i].Attributes["swapFinished"].InnerText;
						}
						catch 
						{						
						}

						if(isGuid.IsMatch(mirrorMapping) && swapFinished != "yes")
						{
							XmlNode matchedNode = null;
							
							// get the node that has the mirror mapping
							for(int x=0;x<lstAnswers.Count;x++)
							{
								if(mirrorMapping == lstAnswers[x].Attributes["answeredItemId"].InnerText)
								{
									matchedNode = lstAnswers[x];
									break;
								}
							}
							
							if(matchedNode!=null)
							{
								string originalInnerXml = lstAnswers[i].InnerXml;
								string mirrorInnerXml = matchedNode.InnerXml;
								
								lstAnswers[i].InnerXml = mirrorInnerXml;
								
								// do swap
								if(matchedNode.Attributes["mirrorMapping"].InnerText == answeredItemId)
								{
									matchedNode.InnerXml = originalInnerXml;								
									
									// add attribute saying swap finished. dont need to do this
									// for the original as for loop will never call it again.

									XmlAttribute swapattr = doc.CreateAttribute("swapFinished");								
									swapattr.Value = "yes";
									matchedNode.Attributes.Append(swapattr);
								}
							}
						}
						else if(swapFinished != "yes")
						{					
							// we have a mirror that is a direct text swap. not a mapping.
							lstAnswers[i].InnerXml = mirrorMapping;						
						} 
					}
				}
				catch 
				{
					// handle.
				}
			}
		}

	
		public static void CleanUpMasterDocument(XmlDocument returnDoc)	
		{		
				
			XmlNodeList lstAnswers = returnDoc.GetElementsByTagName("answer");
			
			for(int i=0;i<lstAnswers.Count;i++)
			{
				try
				{
					string mirrorPrimaryInclude = lstAnswers[i].Attributes["mirrorPrimaryInclude"].InnerText;

					if(mirrorPrimaryInclude == "False")
					{
						lstAnswers[i].InnerXml = "";
					}	
				}
				catch
				{
					// handle.
				}
			}
			
		}

		public static void CleanUpMirrorDocument(XmlDocument returnDoc)
		{					
			XmlNodeList lstAnswers = returnDoc.GetElementsByTagName("answer");

			for(int i=0;i<lstAnswers.Count;i++)
			{
				try 
				{
					string mirrorSecondaryInclude = lstAnswers[i].Attributes["mirrorSecondaryInclude"].InnerText;

					if(mirrorSecondaryInclude == "False")
					{
						lstAnswers[i].InnerXml = "";
					}
				}
				catch
				{
					// handle
				}

			}			
		}

	}
}
