using System;
using System.Data;
using System.IO;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common.Data;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.AnswerAcquirer.Xml
{
	/// <summary>
	/// Summary description for InstanceToMetaSet.
	/// </summary>
	public class InstanceToMetaSet
	{
		private InstanceToMetaSet()
		{
		}

		public static MemoryStream GetMetaSetXml(Package instance, Guid instanceId, AnswerProvider.Caller caller)
		{
			MemoryStream ms = new MemoryStream();
			XmlTextWriter xw = new XmlTextWriter(ms, System.Text.Encoding.UTF8);

			xw.WriteStartDocument();
			xw.WriteStartElement("metaSet");

			xw.WriteAttributeString("packageInstanceId", instanceId.ToString());

			foreach(Question q in instance.Questions)
			{
				WriteItemData(xw, q, "question", instanceId, caller);
			}

			foreach(PFunction pf in instance.Functions)
			{
				WriteItemData(xw, pf, "function", instanceId, caller);
			}

			foreach(Outcome o in instance.Outcomes)
			{
				WriteItemData(xw, o, "outcome", instanceId, caller);
			}

			foreach(SimpleOutcome so in instance.SimpleOutcomes)
			{
				WriteItemData(xw, so, "simpleOutcome", instanceId, caller);
			}

			xw.WriteEndElement();
			xw.WriteEndDocument();

			xw.Flush();
			
			ms.Seek(0, SeekOrigin.Begin);
			
			return ms;
		}

		private static void WriteItemData(XmlTextWriter xw, PackageItem item, string itemType, Guid instanceId, AnswerProvider.Caller caller)
		{
			xw.WriteStartElement("answerInformation");
			xw.WriteAttributeString("answeredItemId", item.UniqueIdentifier.ToString());
			xw.WriteAttributeString("answeredItemType", itemType);
			xw.WriteElementString("title", item.Name);
			xw.WriteElementString("preamble", GetPreamble(itemType));

			if (item is ITemplateItem)
			{
				ITemplateItem iti = (ITemplateItem)item;
				if (iti.PreviewHoverText != null && iti.PreviewHoverText.Trim().Length > 0)
				{
					xw.WriteElementString("comment", iti.PreviewHoverText);
				}
			}

            

			xw.WriteStartElement("rows");
	
			WriteRows(xw, item, itemType, instanceId, caller);

			xw.WriteEndElement();
			xw.WriteEndElement();
		}

		private static void WriteRows(XmlTextWriter xw, PackageItem item, string itemType, Guid instanceId, AnswerProvider.Caller caller)
		{
			switch(itemType)
			{
				case "question":
				case "function":
					xw.WriteStartElement("row");
					xw.WriteStartElement("left");
					WriteRowItem(xw, item, itemType, instanceId, caller);
					xw.WriteEndElement(); // left
					xw.WriteEndElement(); // row
					break;
				case "outcome":
					Outcome o = (Outcome)item;
					if (o != null && o.Definition != null)
					{
						WriteOutcome(xw, o.Definition, instanceId, caller);
					}
					break;
				case "simpleOutcome":	
					SimpleOutcome so = (SimpleOutcome)item;
					if (so != null && so.Definition != null)
					{
						WriteQuery(xw, so.Definition, so.Definition.EvaluateBool(caller), instanceId, caller);
					}
					break;
				default:
					throw new ArgumentException(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("UnexpectedItemType"), itemType), "itemType");
			}			
		}

		private static void WriteOutcome(XmlTextWriter xw, Query query, Guid instanceId, AnswerProvider.Caller caller)
		{
			bool result = query.EvaluateBool(caller);
			WriteQuery(xw, query, result, instanceId, caller);
			
			if (result && query.ActionIfTrue != null && query.ActionIfTrue.SubQuery != null)
			{
				WriteOutcome(xw, query.ActionIfTrue.SubQuery, instanceId, caller);
			}
			else if (!result && query.ActionIfFalse != null && query.ActionIfFalse.SubQuery != null)
			{
				WriteOutcome(xw, query.ActionIfFalse.SubQuery, instanceId, caller);
			}

		}

		private static void WriteQuery(XmlTextWriter xw, Query q, bool queryResult, Guid instanceId, AnswerProvider.Caller caller)
		{
			xw.WriteStartElement("query");
			xw.WriteElementString("header", string.Format("{0}{1}",ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("HeaderString"), queryResult));
			
			foreach(DataRow dr in q.QueryExpression.Table.Rows)
			{
				xw.WriteStartElement("queryRow");
				xw.WriteElementString("operator1", dr[0].ToString());
				xw.WriteElementString("openParen", dr[1].ToString());
				xw.WriteStartElement("left");
				WriteRowItem(xw, (PackageItem)(dr[2]), TypeToString(dr[2]), instanceId, caller);
				xw.WriteEndElement(); // left
				xw.WriteElementString("operator2", dr[3].ToString());
				xw.WriteStartElement("right");
				WriteRowItem(xw, (PackageItem)(dr[4]), TypeToString(dr[4]), instanceId, caller);
				xw.WriteEndElement(); // right
				xw.WriteElementString("closeParen", dr[5].ToString());
				xw.WriteEndElement(); // queryRow
			}
			xw.WriteEndElement();

		}

		private static string TypeToString(object o)
		{
			if (o is Question)
			{
				return "question";
			} 
			else if (o is PFunction)
			{
				return "function";
			}
			else if (o is Outcome)
			{
				return "outcome";
			}
			else if (o is SimpleOutcome)
			{
				return "simpleOutcome";
			}
			else if (o is PackageConstant)
			{
				return "constant";
			}
			else
			{
                throw new ArgumentException(string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("UnexpectedItemType"), o.GetType().Name));
			}
		}

		private static void WriteRowItem(XmlTextWriter xw, PackageItem item, string itemType, Guid instanceId, AnswerProvider.Caller caller)
		{
			
			xw.WriteStartElement("item");

			int firstAppearance = GetPagePositionWhereFirstUsed(item, instanceId);
			if (firstAppearance != int.MinValue)
			{
				xw.WriteAttributeString("firstAppearsOnPageId", firstAppearance.ToString());
			}

			if (itemType == "question")
			{
                xw.WriteAttributeString("fullName", ((Question)item).Prompt.Evaluate(caller, ((Question)item).StringBindingMetaData));
			}
			else if (itemType == "constant")
			{
				xw.WriteAttributeString("fullName", ((PackageConstant)item).ToPreviewString());
			}
			else
			{
				xw.WriteAttributeString("fullName", item.Name);
			}

			xw.WriteAttributeString("itemType", itemType);


			xw.WriteEndElement(); // item
			
		}

		private static string GetPreamble(string itemType)
		{
			switch(itemType)
			{
				case "question":
				case "function":
					return ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("QuestionPreamble");
				case "simpleOutcome":
				case "outcome":
				default:
					return ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("MetaPreamble");
			}
			
		}

		private static int GetPagePositionWhereFirstUsed(PackageItem item, Guid instanceId)
		{
			
			if (item is Question && item.ParentPackage != null && item.ParentPackage.Interviews.Count > 0)
			{
				Package instance = item.ParentPackage;
				StackEntry[] stack = InstanceManagement.NavigationStackGet(instanceId);
				foreach(StackEntry se in stack)
				{
					InterviewPage page = instance.InterviewPageByGuid[se.PageId];
					if (page is InternalPage && ((InternalPage)page).ContainsReferenceTo(item))
					{
						return se.PositionInStack;
					}
				}
				return int.MinValue;
			}
			else
			{
				return int.MinValue;
			}
		}
	}
}
