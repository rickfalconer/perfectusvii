using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Xml.Schema;
//pf-3266 remove word automation office12
//using Microsoft.Office.Interop.Word;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.PackageManager;
using Perfectus.Server.PluginKit;

namespace Perfectus.Server.AnswerAcquirer.Xml
{
	/// <summary>
	/// Summary description for InstanceToAnswerSetXml.
	/// </summary>
	public sealed class InstanceToAnswerSetXml
	{
		private InstanceToAnswerSetXml()
		{
		}

		public static MemoryStream GetAnswerSetXml(Guid instanceId)
		{
			return GetAnswerSetXml(instanceId, false);
		}

		public static MemoryStream GetAnswerSetXml(Guid instanceId, bool simpleAnswerSet)
		{
			int ver;
			int rev;
			Package instance = InstanceManagement.GetInstance(instanceId);
            InstanceManagement.GetVersion(instanceId, out ver, out rev);
            return
                GetAnswerSetXml(instance, instanceId, ver, rev, AnswerProvider.Caller.Assembly, false, Guid.Empty,
                                simpleAnswerSet);
		}

		public static MemoryStream GetAnswerSetXml(Package instance, Guid instanceId, bool simpleAnswerSet)
		{
			int ver;
			int rev;
            InstanceManagement.GetVersion(instanceId, out ver, out rev);
            return
                GetAnswerSetXml(instance, instanceId, ver, rev, AnswerProvider.Caller.Assembly, false, Guid.Empty,
                                simpleAnswerSet);
		}

        public static MemoryStream GetAnswerSetXml(Package instance, Guid instanceId, int versionNumber,
                                                   int revisionNumber, AnswerProvider.Caller caller,
                                                   bool simpleAnswerSet)
		{
            return
                GetAnswerSetXml(instance, instanceId, versionNumber, revisionNumber, caller, false, Guid.Empty,
                                simpleAnswerSet);
		}

        public static MemoryStream GetAnswerSetXml(Package instance, Guid instanceId, int versionNumber,
                                                   int revisionNumber, AnswerProvider.Caller caller, bool isOffline,
                                                   Guid lastPageVisitedId, bool simpleAnswerSet)
		{
			MemoryStream ms = new MemoryStream();
            XmlTextWriter xw = new XmlTextWriter(ms, Encoding.UTF8);

			xw.WriteStartDocument();
			xw.WriteStartElement("answerSet");

			xw.WriteAttributeString("packageName", instance.Name);	
			xw.WriteAttributeString("packageId", instance.UniqueIdentifier.ToString());
			xw.WriteAttributeString("packageVersion", versionNumber.ToString());
			xw.WriteAttributeString("packageRevision", revisionNumber.ToString());
			xw.WriteAttributeString("packageInstanceId", instanceId.ToString());
			xw.WriteAttributeString("resumeURL", Config.Shared.interview.resumeURL.ToString());
            xw.WriteAttributeString("mirrorDocType", instance.MirrorDocType);

			if (! isOffline)
			{
				InstanceManagement.NavigationStackPeek(instanceId, out lastPageVisitedId);
			}

			if (lastPageVisitedId != Guid.Empty)
			{
				xw.WriteAttributeString("lastPageAnswered", lastPageVisitedId.ToString("N"));
			}

			#region Write questions

            foreach (Question q in instance.Questions)
			{
				AnswerCollectionCollection collColl = q.GetAnswerCollectionCollection(caller);

                StringBindingMetaData sbmd = new StringBindingMetaData(instance.Name, InstanceManagement.GetInstanceReference(instanceId), instanceId, versionNumber, revisionNumber);
                q.StringBindingMetaData = sbmd;

				if (collColl != null)
				{
					int sequence = 0;
                    for (int i = 0; i < collColl.Count; i ++)
					{
						AnswerCollection coll;
						coll = collColl[i];

                        for (int j = 0; j < coll.Count; j++)
						{
							sequence++;
							xw.WriteStartElement("answer");
                            try
                            {
                                xw.WriteAttributeString("name", q.Name);

                                if (!simpleAnswerSet)
                                {
                                    xw.WriteAttributeString("answeredItemId", q.UniqueIdentifier.ToString());
                                }

                                xw.WriteAttributeString("sequenceNumber", sequence.ToString());
                                xw.WriteAttributeString("repeaterRow", ((int)(i + 1)).ToString()); // translate the index to the row number

                                if (!simpleAnswerSet)
                                {
                                    string startSepararor = q.RepeatAnswerSeparator;
                                    string endSeparator = q.RepeatAnswerSeparatorFinal;

                                    if (endSeparator == null)
                                    {
                                        endSeparator = startSepararor;
                                    }

                                    if (coll.Count > 1)
                                    {
                                        if (coll.Count > 1)
                                        {
                                            if (sequence < coll.Count - 1)
                                            {
                                                xw.WriteAttributeString("repeatSeparator", startSepararor);
                                            }
                                            else
                                            {
                                                xw.WriteAttributeString("repeatSeparator", endSeparator);
                                            }
                                        }
                                        else
                                        {
                                            if (sequence == coll.Count - 1)
                                            {
                                                xw.WriteAttributeString("repeatSeparator", endSeparator);
                                            }
                                            else
                                            {
                                                xw.WriteAttributeString("repeatSeparator", startSepararor);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (collColl.Count > 1)
                                        {
                                            if (sequence < collColl.Count - 1)
                                            {
                                                xw.WriteAttributeString("repeatSeparator", startSepararor);
                                            }
                                            else
                                            {
                                                xw.WriteAttributeString("repeatSeparator", endSeparator);
                                            }
                                        }
                                        else
                                        {
                                            if (sequence == collColl.Count - 1)
                                            {
                                                xw.WriteAttributeString("repeatSeparator", endSeparator);
                                            }
                                            else
                                            {
                                                xw.WriteAttributeString("repeatSeparator", startSepararor);
                                            }
                                        }
                                    }
                                }

                                // Must call everytime, as even if package is Not make mirror, mirror converter may still be been used on package.							
                                if (!simpleAnswerSet)
                                {
                                    WriteMirrorAttributes(q, xw, caller);
                                }

                                if (!simpleAnswerSet && q.DataType == PerfectusDataType.Attachment)
                                {
                                    if (q.DisplayType < QuestionDisplayType.Extension1)
                                        xw.WriteString(String.Format("Question '{0}' is of type ATTACHMENT. The content cannot be included in document. Use FILE instead.", q.Name));
                                    else if (!simpleAnswerSet && (int)(q.DisplayType) > 1000 &&
                                         instance.ExtensionMapping != null &&
                                         instance.ExtensionMapping.ContainsKey(q.DisplayType) &&
                                         q.GetSingleAnswer(caller).ToString() != null &&
                                         q.GetSingleAnswer(caller).ToString().Length != 0)
                                    {
                                        xw.WriteString(String.Format("Integration Question '{0}' is ATTACHMENT. Content cannot be included in document. Use FILE instead.", q.Name));
                                    }
                                    else
                                    {
                                        xw.WriteString(String.Format("Unable to evalute question {0} of type ATTACHMENT", q.Name));
                                    }
                                }
                                else
                                    if (!simpleAnswerSet && q.DataType == PerfectusDataType.File)
                                    {
                                        if (isOffline)
                                        {
                                            if (coll[j] != null)
                                            {
                                                WriteOfflineUploadedFile(coll[j].ToString(), xw, q, sequence);
                                            }
                                        }
                                        else
                                        {
                                            WriteUploadedFile(xw, q, instanceId, sequence);
                                        }
                                    }
                                    else
                                        if (!simpleAnswerSet && (int)(q.DisplayType) > 1000 &&
                                             instance.ExtensionMapping != null &&
                                             instance.ExtensionMapping.ContainsKey(q.DisplayType) &&
                                             q.GetSingleAnswer(caller).ToString() != null &&
                                             q.GetSingleAnswer(caller).ToString().Length != 0 &&

                                            // Change to support SharePoint Choice fields ->
                                            // plugins are designed to fetch data from external sorces using 'IFetcher' of the plugin.
                                            // Data will be retrieved at assembly time (right here). That is different from the standard file
                                            // picker which load at interview time.
                                            // However, it is possible that a plugin 'creates' the data at interview time. In that case
                                            // it is assumed that the 'ServerSideFetcherPluginId' is not set (see config files).
                                            // In that case handle the question not differently from a normal Perfectus question.
                                             instance.ExtensionMapping != null &&
                                            instance.ExtensionMapping.ContainsKey(q.DisplayType) &&
                                            instance.ExtensionMapping[q.DisplayType].ServerSideFetcherPluginId != null &&
                                            instance.ExtensionMapping[q.DisplayType].ServerSideFetcherPluginId.Length > 0)
                                        {
                                            WriteFetchedFile(xw, instance, q, sequence, caller, instanceId);
                                        }
                                        else
                                        {
                                            xw.WriteString(
                                                FormatTemplateItem(
                                                    AnswerProvider.ConvertToBestType(coll[j], q.DataType, caller,
                                                                                     q.StringBindingMetaData), instanceId,
                                                    q.ParentPackage.UniqueIdentifier, q));

                                        }
                            }
                            catch (Exception ex)
                            {
                                string msg =
                                    string.Format("{0} [{1}]",
                                                  ResourceLoader.GetResourceManager(
                                                      "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                                      "RetrieveAnswerError"), q.Name);
                                if (!isOffline)
                                {
                                    Logging.LogMessage(msg, ex, instanceId, instance.UniqueIdentifier, int.MinValue,
                                                       int.MinValue, int.MinValue,
                                                       Logging.ExceptionLogSeverityCode.WARNing, true);
                                }
                                else
                                {
                                    throw new Exception(msg);
                                }
                            }
							finally
							{
								xw.WriteEndElement();
							}
						}
					}
				}
			}

            

			#endregion

			#region Write functions

			if (!simpleAnswerSet)
			{
                foreach (PFunction pf in instance.Functions)
				{
					try
					{
						xw.WriteStartElement("answer");
						xw.WriteAttributeString("name", pf.Name);
						xw.WriteAttributeString("answeredItemId", pf.UniqueIdentifier.ToString());
						xw.WriteAttributeString("sequenceNumber", "1");
						xw.WriteAttributeString("repeatSeparator", string.Empty);
					
						object oa = pf.GetSingleAnswer(caller);

						xw.WriteString(FormatTemplateItem(oa, instanceId, pf.ParentPackage.UniqueIdentifier, pf));

						xw.WriteEndElement();
					}
					catch (Exception ex)
					{
                        string msg =
                            string.Format("{0} [{1}]",
                                          ResourceLoader.GetResourceManager(
                                              "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                              "EvaluateFunctionError"), pf.Name);
                        Logging.LogMessage(msg, ex, instanceId, instance.UniqueIdentifier, int.MinValue, int.MinValue,
                                           int.MinValue, Logging.ExceptionLogSeverityCode.WARNing, true);
					}
				}
			}

			#endregion
			
			#region Write outcomes
			
			if (!simpleAnswerSet)
			{
                int chopLength =
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n<?mso-application progid=\"Word.Document\"?>\r\n"
                        .Length;

                foreach (Outcome o in instance.Outcomes)
				{
					try
					{
                        OutcomeAction oa;
						string wml;
						ActionBase ab = o.Definition.Evaluate(caller);
						if (ab == null)
						{
                            throw new NullReferenceException(
                                ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                    GetString("NullOutcomeQuery"));
						} 
                        else if (! (ab is OutcomeAction))
						{
                            throw new Exception(
                                ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                    GetString("UnexpextedActionType"));
						}
						else
						{
                            oa = (OutcomeAction) ab;
							try
							{							
                                if (! isOffline)
								{
									// The outcomeAction should be replaced with the one from SQL with all the WordML in it.  Only get this for the actual action - saves heaps of time and space
									// Get it from the cache if possible
									byte[] outcomeActionBytes;
									int cacheTtl = Config.Shared.assembly.templateCacheTtl;
									string cacheKeyFormat = "Template_{0}_{1}.{2}/{3}.{4}";
                                    string cacheKey =
                                        string.Format(cacheKeyFormat, instance.UniqueIdentifier, versionNumber,
                                                      revisionNumber, o.UniqueIdentifier, oa.UniqueIdentifier);
									if (cacheTtl == 0 || (cacheTtl > 0 && HttpRuntime.Cache[cacheKey] == null))
									{
                                        Logging.TraceWrite(
                                            string.Format(
                                                ResourceLoader.GetResourceManager(
                                                    "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                                    "RetrievingOutcomeAction"), o.UniqueIdentifier, oa.UniqueIdentifier),
                                            "ProcessCoordinator");
                                        outcomeActionBytes =
                                            Retrieval.GetOutcomeActionBytes(instance.UniqueIdentifier, versionNumber,
                                                                            revisionNumber, o.UniqueIdentifier,
                                                                            oa.UniqueIdentifier);

										if (cacheTtl > 0)
										{
                                            Logging.TraceWrite(
                                                string.Format(
                                                    ResourceLoader.GetResourceManager(
                                                        "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                                        "AddingOutcomeToCache"), cacheTtl), "ProcessCoordinator");
                                            HttpRuntime.Cache.Add(cacheKey, outcomeActionBytes, null,
                                                                  Cache.NoAbsoluteExpiration,
                                                                  new TimeSpan(0, 0, 0, cacheTtl, 0),
                                                                  CacheItemPriority.Normal, null);
										}
									}
									else
									{
                                        Logging.TraceWrite(
                                            string.Format(
                                                ResourceLoader.GetResourceManager(
                                                    "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                                    "RetrivingFromCache"), cacheKey), "ProcessCoordinator");
                                        outcomeActionBytes = (byte[]) HttpRuntime.Cache[cacheKey];
									}

									// Turn the bytes back into an OutcomeAction
									using (MemoryStream msOa = new MemoryStream())
									{
										BinaryFormatter bf = new BinaryFormatter();
										msOa.Write(outcomeActionBytes, 0, outcomeActionBytes.Length);
										msOa.Seek(0, SeekOrigin.Begin);
										object deserialised = bf.Deserialize(msOa);
                                        oa = (OutcomeAction) deserialised;
									}
								}
								wml = oa.OfficeOpenXML;					
							}
                            catch (Exception ex)
							{
                                string errormsg3 =
                                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                        GetString("AssemblyError");
                                Logging.LogMessage(errormsg3, ex);
								return null;
							}
						}

						// Invoke a fetcher
						if (oa != null && oa.FetcherId != null)
						{
							xw.WriteStartElement("answer");
							xw.WriteAttributeString("name", o.Name);
							xw.WriteAttributeString("answeredItemId", o.UniqueIdentifier.ToString());
							xw.WriteAttributeString("sequenceNumber", "1");
							xw.WriteAttributeString("repeatSeparator", string.Empty);
							try
							{
                                WriteFetchedFile(xw, oa, o, instanceId);
							}
							finally
							{
								xw.WriteEndElement();
							}
						}

                        else if (wml != null && wml.Length >= chopLength)
						{			
							int procStart = wml.IndexOf("<?");
							int procEnd = wml.LastIndexOf("?>");
							int procEndCount = (procEnd - procStart) + 3; // 2 = '?>'
							wml = wml.Remove(procStart, procEndCount);
							xw.WriteStartElement("answer");
							xw.WriteAttributeString("name", o.Name);
							xw.WriteAttributeString("answeredItemId", o.UniqueIdentifier.ToString());
							xw.WriteAttributeString("sequenceNumber", "1");
							xw.WriteAttributeString("repeatSeparator", string.Empty);
							xw.WriteRaw(wml);
							xw.WriteEndElement();
						}
					}
					catch (Exception ex)
					{
                        string msg =
                            string.Format("{0} [{1}]",
                                          ResourceLoader.GetResourceManager(
                                              "Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                              "EvaluateOutcomeError"), o.Name);
                        Logging.LogMessage(msg, ex, instanceId, instance.UniqueIdentifier, int.MinValue, int.MinValue,
                                           int.MinValue, Logging.ExceptionLogSeverityCode.WARNing, true);
					}
				}
			}

			#endregion

			#region Write simple outcomes

			if (!simpleAnswerSet)
			{
                foreach (SimpleOutcome so in instance.SimpleOutcomes)
				{
					if (so.Definition != null && so.Definition.QueryExpression != null)
					{
						xw.WriteStartElement("answer");
						xw.WriteAttributeString("name", so.Name);
						xw.WriteAttributeString("answeredItemId", so.UniqueIdentifier.ToString().ToLower());
						xw.WriteAttributeString("isCondition", "true");
						xw.WriteString(so.Definition.EvaluateBool(caller).ToString().ToLower());
						xw.WriteEndElement();
					}
				}
			}

			#endregion

			xw.WriteEndElement();
			xw.WriteEndDocument();

			xw.Flush();
			
			ms.Seek(0, SeekOrigin.Begin);
			
			
			return ms;
		}
		
        private static void WriteFetchedFile(XmlTextWriter xw, OutcomeAction oa, Outcome o, Guid instanceId)
		{
            WriteFetchedFile(xw, oa.FetcherId, oa.LibraryKey, o, 0, instanceId);
		}

        private static void WriteFetchedFile(XmlTextWriter xw, Package instance, Question q, int sequenceNumber,
                                             AnswerProvider.Caller caller, Guid instanceId)
		{
			QuestionDisplayType qdt = q.DisplayType;
			DisplayTypeExtensionInfo extensionInfo;
			if (! instance.ExtensionMapping.ContainsKey(qdt))
			{
                throw new Exception(
                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                        "NoExtensionInformation"));
			}
			extensionInfo = instance.ExtensionMapping[qdt];
			string pluginId = extensionInfo.ServerSideFetcherPluginId;
            if (pluginId == null || pluginId.Length == 0)
            {
                xw.WriteString(String.Format("Integration Question '{0}' cannot be used in the document, as it has no fetcher defined", q.Name));
                return;
            }

            // sequence start with 1...
            // question might be in repeater, hence need to get correct index
            string urlStr = AnswerProvider.ConvertToBestType(q.GetAnswer(sequenceNumber-1, caller)[0], q.DataType, caller, q.StringBindingMetaData).ToString();
            WriteFetchedFile(xw, pluginId, urlStr, q, sequenceNumber, instanceId);
		}

        private static void WriteFetchedFile(XmlTextWriter xw, string fetcherId, string libraryKey, PackageItem pi,
                                             int sequenceNumber, Guid instanceId)
		{
			bool found = false;
            foreach (perfectusServerConfigurationPluginsPlugin plugin in Config.Shared.plugins.plugin)
			{
				if (plugin.type.ToLower(CultureInfo.InvariantCulture) == "fetcher" 
                    &&
                    plugin.key.ToLower(CultureInfo.InvariantCulture) == fetcherId.ToLower(CultureInfo.InvariantCulture))
				{
					found = true;
			
                    IFetcher fetcher;
					try
					{
						fetcher = LoadFetcher(plugin);
						if (fetcher == null)
						{
                            throw new NullReferenceException(
                                ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                    GetString("LoadFetcherReturnedNull"));
						}
					}
					catch (Exception ex)
					{
                        throw new Exception(
                            ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                "FetcherPluginError"), ex);
					}
			
					string contentType;
					byte[] fileBytes;

                    using (Stream s = fetcher.Fetch(libraryKey, pi.ParentPackage, instanceId, out contentType))
					{
						if (s == null)
						{
                            throw new Exception(
                                ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                    GetString("LoadFetcherReturnedNull"));
						}
						s.Seek(0, SeekOrigin.Begin);	

                        Trace.WriteLine(
                            ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                "LoadFetcherReturnedStream") + s.Length);
						// Fetch the file into a byteArray
						fileBytes = new byte[s.Length];
						s.Read(fileBytes, 0, fileBytes.Length);
					}

					WriteFile(contentType, fileBytes, xw, string.Empty, pi, sequenceNumber);
				}
			}

			if (!found)
			{
                string mylogstr =
                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                        "PluginRegisterError");
                mylogstr = mylogstr.Replace("{0}", fetcherId);
				throw new Exception(mylogstr);
			}
		}


        private static string FormatTemplateItem(object answer, Guid instanceId, Guid packageIdentifier,
                                                 ITemplateItem item)
		{
			string formatString = item.FormatString;
			try
			{
				// If the formatString has marker indicating its a plugin then use a formatter plugin.
                if (formatString.IndexOf("#formatplugin#") >= 0)
				{
                    return
                        Format.FormatWithPlugin(formatString.Replace("#formatplugin#", ""), answer.ToString(),
                                         item.FormatStringParams, ((PackageItem)item).ParentPackage, item);
				}

				if (answer is bool)
				{
                    return
                        ((bool) answer)
                            ? ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                  GetString("Yes")
                            : ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                  GetString("No");
				}

                if (answer is string && item is AnswerProvider &&
                    ((AnswerProvider) item).DataType == PerfectusDataType.Text)
				{
                    switch (formatString.ToLower())
					{
						case "lower":
							return answer.ToString().ToLower();
						case "upper":
							return answer.ToString().ToUpper();
						case "title":
							return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(answer.ToString());
						default:
							return answer.ToString();
					}
				}

                double d;
				if (formatString.Trim().Length == 0)
				{
					if (answer is DateTime)
					{
                        return ((DateTime) answer).ToShortDateString();
					}
					if (answer != null && double.TryParse(answer.ToString(), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out d))
					{
                        // User General format as default
                        return (double.Parse(answer.ToString(), NumberStyles.Any, NumberFormatInfo.InvariantInfo)).ToString("G");
					}
				}
				else
				{
					if (answer is DateTime)
					{
                        return ((DateTime) answer).ToString(formatString);
					}
                    if (answer != null && double.TryParse(answer.ToString(), NumberStyles.Any, NumberFormatInfo.InvariantInfo, out d))
					{
                        return
                            (double.Parse(answer.ToString(), NumberStyles.Any, NumberFormatInfo.InvariantInfo)).ToString
                                (formatString);
					}						
				}
			}
			catch (Exception ex)
			{
                string mystr1 =
                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                        "FormatError");
                string mystr2 =
                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                        "FormatStringError");
                mystr2 = mystr2.Replace("{1}", formatString);
                string msg = string.Format("{0} {1}.  {2}", mystr1, ((PackageItem) item).Name, mystr2);
                Logging.LogMessage(msg, ex, instanceId, packageIdentifier, int.MinValue, int.MinValue, int.MinValue,
                                   Logging.ExceptionLogSeverityCode.WARNing, true);
			}

			// All else fails...
            if (answer != null)
            {
                return answer.ToString();
            }
            else
            {
                return string.Empty;
            }
		}
       

        // Write Offline (Desktop) uploaded file
        private static void WriteOfflineUploadedFile(string filePath, XmlTextWriter xw, Question q, int sequenceNumber)
        {
            if (filePath != null && filePath.Length > 0)
            {
                string originalPath = filePath.ToLower();
                string fileExtension =
                    originalPath.Substring(originalPath.LastIndexOf("."),
                                           originalPath.Length - originalPath.LastIndexOf("."));
                string contentType = CheckOfflineContentType(fileExtension);

                FileStream fs = File.OpenRead(originalPath);
                byte[] fileBytes = new byte[fs.Length];
                fs.Read(fileBytes, 0, fileBytes.Length);
                fs.Close();

                // If the file is WordML, (well, just XML) write it raw.
                // If it's an image, encode it and place it within the Word VML structure.
                WriteFile(contentType, fileBytes, xw, originalPath, q, sequenceNumber);
            }
        }

        // Check the file extension for the file type for Offline uploaded file
        private static string CheckOfflineContentType(string extensionString)
        {
            string contentType = null;
            switch (extensionString)
            {
                case ".xml":
                    contentType = "text/xml";
                    break;
                case ".pdf":
                    contentType = "application/pdf";
                    break;
                case ".doc":
                    contentType = "application/msword";
                    break;
                case ".docx":
                    contentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    break;
                case ".rtf":
                    contentType = "text/richtext";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".jpg":
                case ".jpeg":
                    contentType = "image/jpeg";
                    break;
                case ".bmp":
                    contentType = "image/bmp";
                    break;
                case ".png":
                    contentType = "image/x-png";
                    break;
                case ".tiff":
                case ".tif":
                    contentType = "image/tiff";
                    break;
            }
            return contentType;
        }

		private static void WriteUploadedFile(XmlTextWriter xw, Question q, Guid instanceId, int sequenceNumber)
		{
            string originalPath ;
            string contentType;
            byte[] fileBytes =
                InstanceManagement.File_GetBytes(instanceId, sequenceNumber - 1, q.UniqueIdentifier, out originalPath,
                                                 out contentType);
                //Perfectus.Server.PackageManager.Retrieval.PackageInstance_File_GetBytes(instanceId, sequenceNumber, q.UniqueIdentifier, out originalPath, out contentType);
            if (fileBytes == null || fileBytes.Length == 0)
			{
				return;
			}
			originalPath = originalPath.ToLower();
			contentType = contentType.ToLower();

			// If the file is WordML, (well, just XML) write it raw.
			// If it's an image, encode it and place it within the Word VML structure.

			WriteFile(contentType, fileBytes, xw, originalPath, q, sequenceNumber);
		}

        private static void WriteFile(string contentType, byte[] fileBytes, XmlTextWriter xw, string originalPath,
                                      PackageItem pi, int sequenceNumber)
		{
            if (contentType.StartsWith("application/vnd.openxmlformats-officedocument.word"))
			{
                //convert to flat format
                MemoryStream docxStream = new MemoryStream(fileBytes);
                XDocument docxXDocument = OpcToFlat(docxStream);
                string xmlContent = docxXDocument.ToString();

                int docStart = xmlContent.IndexOf("<pkg:package");
				if (docStart >= 0)
				{
                    xw.WriteRaw(xmlContent.Substring(docStart));
				}
				else
				{
                    throw new Exception(
                        ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                            "DocxUpload"));
				}
			}
			else if (contentType == "text/xml")
			{
				string wordMl = Encoding.UTF8.GetString(fileBytes);
				int docStart = wordMl.IndexOf("<pkg:package");
				if (docStart >= 0)
				{
					xw.WriteRaw(wordMl.Substring(docStart));
				}
				else
				{
                    throw new Exception(
                        ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                            "XMLUpload"));
				}
			}
			else if (contentType == "application/msword" || contentType == "text/richtext")
			{
                //string wordMl = ConvertToWordMl(fileBytes);
                //int docStart = wordMl.IndexOf("<w:wordDocument");
                //if (docStart >= 0)
                //{
                //	xw.WriteRaw(wordMl.Substring(docStart));
                //}
                //else
                //{
                //                throw new Exception(
                //                    ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                //                        "XMLUploadConverted"));
                //}
                throw new Exception(string.Format("Content Type {0} is not supported", contentType));

            }
            else if (contentType.Length >= 5 && contentType.Substring(0, 5) == "image")
			{
				xw.WriteAttributeString("image", "true");
                string extension = "img";
                string title = "image";
                
                if (originalPath != null && originalPath.Length > 0)
                {
                    FileInfo fi = new FileInfo(originalPath);
                    extension = fi.Extension;
                    title = fi.Name;
                }
                string url = string.Format("wordml://{0:N}_{1}{2}", pi.UniqueIdentifier, sequenceNumber, extension);
				int width;
				int height;

				// Get the image dimensions by loading it into a bitmap
				using (MemoryStream ms = new MemoryStream(fileBytes))
				{
					Bitmap b = null;
					try
					{
						b = new Bitmap(ms);
                        CalculateImageEmus(b, out width, out height);
					}
					catch
					{
                        throw new Exception(
                            ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
                                "ImageNotValid"));
					}
					finally
					{
						if (b != null)
						{
							b.Dispose();
						}
					}
				}

				xw.WriteAttributeString("url", url);
				xw.WriteAttributeString("title", title);
				xw.WriteAttributeString("width", width.ToString());
				xw.WriteAttributeString("height", height.ToString());

                string imageString = Convert.ToBase64String(fileBytes);
                WriteInChunks(ref imageString, 76);

                xw.WriteString(imageString);
				xw.Flush();
			}
			else
			{
				string file = Encoding.UTF8.GetString(fileBytes);
				xw.WriteRaw(file);
			}
		}

        static XDocument OpcToFlat(MemoryStream docxStream)
        {
            using (System.IO.Packaging.Package package = System.IO.Packaging.Package.Open(docxStream))
            {
                XNamespace pkg = "http://schemas.microsoft.com/office/2006/xmlPackage";

                XDeclaration declaration = new XDeclaration("1.0", "UTF-8", "yes");
                XDocument doc = new XDocument(
                    declaration,
                    new XProcessingInstruction("mso-application", "progid=\"Word.Document\""),
                    new XElement(pkg + "package",
                        new XAttribute(XNamespace.Xmlns + "pkg", pkg.ToString()),
                        package.GetParts().Select(part => GetContentsAsXml(part))
                    )
                );
                return doc;
            }
        }

        static XElement GetContentsAsXml(System.IO.Packaging.PackagePart part)
        {
            XNamespace pkg = "http://schemas.microsoft.com/office/2006/xmlPackage";

            if (part.ContentType.EndsWith("xml"))
            {
                using (Stream str = part.GetStream())
                using (StreamReader streamReader = new StreamReader(str))
                using (XmlReader xr = XmlReader.Create(streamReader))
                    return new XElement(pkg + "part",
                        new XAttribute(pkg + "name", part.Uri),
                        new XAttribute(pkg + "contentType", part.ContentType),
                        new XElement(pkg + "xmlData",
                            XElement.Load(xr)
                        )
                    );
            }
            else
            {
                using (Stream str = part.GetStream())
                using (BinaryReader binaryReader = new BinaryReader(str))
                {
                    int len = (int)binaryReader.BaseStream.Length;
                    byte[] byteArray = binaryReader.ReadBytes(len);
                    // the following expression creates the base64String, then chunks
                    // it to lines of 76 characters long
                    string base64String = (System.Convert.ToBase64String(byteArray))
                        .Select
                        (
                            (c, i) => new
                            {
                                Character = c,
                                Chunk = i / 76
                            }
                        )
                        .GroupBy(c => c.Chunk)
                        .Aggregate(
                            new StringBuilder(),
                            (s, i) =>
                                s.Append(
                                    i.Aggregate(
                                        new StringBuilder(),
                                        (seed, it) => seed.Append(it.Character),
                                        sb => sb.ToString()
                                    )
                                )
                                .Append(Environment.NewLine),
                            s => s.ToString()
                        );
                    return new XElement(pkg + "part",
                        new XAttribute(pkg + "name", part.Uri),
                        new XAttribute(pkg + "contentType", part.ContentType),
                        new XAttribute(pkg + "compression", "store"),
                        new XElement(pkg + "binaryData", base64String)
                    );
                }
            }
        }

        static void WriteInChunks(ref string input, int maxChunkSize)
        {
            string startString = input;
            input = string.Empty;
            int index = 0;
            while (index < startString.Length)
            {
                int size = Math.Min(startString.Length - index, maxChunkSize);
                input += startString.Substring(index, size) + "\r\n";
                index += size;
            }
        }

        //We need to calculate dimensions of image in Emus to insert into xml
        static void CalculateImageEmus(Bitmap bitmap, out int widthInEmu, out int heightInEmu)
        {
            int width = bitmap.Size.Width;
            int height = bitmap.Size.Height;

            const double emus = 9525;
            widthInEmu = (int)(width * emus);
            heightInEmu = (int)(height * emus);
        }

		private static IFetcher LoadFetcher(perfectusServerConfigurationPluginsPlugin fetcher)
		{
			string type = fetcher.typeName;
			//string key = fetcher.key;
			string path = fetcher.path;		
            return (IFetcher) (AppDomain.CurrentDomain.CreateInstanceFromAndUnwrap(path, type));
		}

        //pf-3266 remove word automation
        //private static string ConvertToWordMl(byte[] fileBytes)
        //{
        //	string tempDestFile = Path.GetTempFileName();
        //	try
        //	{
        //		Trace.WriteLine("Automating Word to convert a file to WordprocessorML");

        //		string tempSourceFile = Path.GetTempFileName();

        //              Trace.WriteLine(
        //                  string.Format("Source: {0} - Dest: {1}  Bytecount: {2}", tempSourceFile, tempDestFile,
        //                                fileBytes.Length));

        //		using (FileStream fs = File.OpenWrite(tempSourceFile))
        //		{
        //			fs.Write(fileBytes, 0, fileBytes.Length);
        //		}

        //		object oTrue = true;
        //		object oFalse = false;
        //		object oMissing = Missing.Value;
        //		Microsoft.Office.Interop.Word.Document doc = null;
        //              //dn4
        //              //Application app = null;
        //              Microsoft.Office.Interop.Word.Application app = null;

        //              try
        //		{
        //                  // We have to automate Word here, sadly.  We need to make sure the document gets saved as WordML, and there's no other way...

        //                  //dn4
        //                  //app = new ApplicationClass();
        //                  app = new Application();

        //			object oFileName = tempSourceFile;
        //			Trace.WriteLine(string.Format("Opening {0}...", oFileName));
        //                  doc =
        //                      app.Documents.Open(ref oFileName, ref oMissing, ref oTrue, ref oMissing, ref oMissing,
        //                                         ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                                         ref oMissing, ref oFalse, ref oMissing, ref oMissing, ref oMissing,
        //                                         ref oMissing);


        //			oFileName = tempDestFile;
        //			object oFileFormat = WdSaveFormat.wdFormatXML;

        //			Trace.WriteLine("...Saving " + oFileName);
        //                  doc.SaveAs(ref oFileName, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                             ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
        //                             ref oMissing, ref oMissing, ref oMissing, ref oMissing);
        //		}
        //		catch (Exception ex)
        //		{
        //                  throw new Exception(
        //                      ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
        //                          "WordConvertError"), ex);
        //		}
        //		finally
        //		{
        //			try
        //			{
        //				if (doc != null)
        //				{
        //					doc.Close(ref oFalse, ref oMissing, ref oMissing);
        //				}
        //				if (app != null)
        //				{
        //					app.Quit(ref oFalse, ref oMissing, ref oMissing);
        //				}
        //				try
        //				{
        //					if (File.Exists(tempSourceFile))
        //					{
        //						File.Delete(tempSourceFile);
        //					}
        //				}
        //                      catch
        //                      {
        //                      }
        //			}
        //			catch
        //			{
        //			}
        //		}

        //		string wordMl;
        //		Trace.WriteLine("Reading saved file: " + tempDestFile);
        //		using (StreamReader sr = File.OpenText(tempDestFile))
        //		{
        //			wordMl = sr.ReadToEnd();
        //		}
        //		Trace.WriteLine("Read string length: " + wordMl.Length);
        //		return wordMl;
        //	}
        //          catch (Exception ex)
        //	{
        //              throw new Exception(
        //                  ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString(
        //                      "WordConvertErrorWordprocessorML"), ex);
        //	}
        //	finally
        //	{
        //		try
        //		{
        //			if (File.Exists(tempDestFile))
        //			{
        //				File.Delete(tempDestFile);
        //			}
        //		}
        //              catch
        //              {
        //              }
        //	}
        //}

        private static void WriteMirrorAttributes(Question q, XmlWriter xw, AnswerProvider.Caller caller)
		{
			if (q.MirrorMapping.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
			{
                if (q.MirrorMapping.QuestionValue != null)
				{
					xw.WriteAttributeString("mirrorMapping", q.MirrorMapping.QuestionValue.UniqueIdentifier.ToString());
				}
				else
				{
                    xw.WriteAttributeString("mirrorMapping", "");
				}
			}
            else if (q.MirrorMapping.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
			{
                if (q.MirrorMapping.TextValue != null)
				{
					xw.WriteAttributeString("mirrorMapping", q.MirrorMapping.TextValue.ToString());
				}
				else
				{
                    xw.WriteAttributeString("mirrorMapping", "");
				}
			}
			else
			{
                xw.WriteAttributeString("mirrorMapping", "");
			}						
							

            if (q.MirrorPrimaryInclude.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo)
			{
				xw.WriteAttributeString("mirrorPrimaryInclude", q.MirrorPrimaryInclude.YesNoValue.ToString());
			}
            else if (q.MirrorPrimaryInclude.ValueType == YesNoQueryValue.YesNoQueryValueType.Query)
			{
                xw.WriteAttributeString("mirrorPrimaryInclude", q.MirrorPrimaryInclude.Evaluate(true, caller).ToString());
			}
			else
			{
				xw.WriteAttributeString("mirrorPrimaryInclude", "");
			}


            if (q.MirrorSecondaryInclude.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo)
			{
				xw.WriteAttributeString("mirrorSecondaryInclude", q.MirrorSecondaryInclude.YesNoValue.ToString());
			}
            else if (q.MirrorSecondaryInclude.ValueType == YesNoQueryValue.YesNoQueryValueType.Query)
			{							
                xw.WriteAttributeString("mirrorSecondaryInclude",
                                        q.MirrorSecondaryInclude.Evaluate(true, caller).ToString());
			}
			else
			{
				xw.WriteAttributeString("mirrorSecondaryInclude", "");
			}
		}
	}
}
