using System;

namespace Perfectus.Server.AnswerAcquirer.AnswerInstance
{
	/// <summary>
	/// A dictionary with keys of type string and values of type ServiceCallInfo
	/// </summary>
	public class ServiceCallInfoDictionary: System.Collections.DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the ServiceCallInfoDictionary class
		/// </summary>
		public ServiceCallInfoDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the ServiceCallInfo associated with the given string
		/// </summary>
		/// <param name="key">
		/// The string whose value to get or set.
		/// </param>
		public virtual ServiceCallInfo this[string key]
		{
			get
			{
				return (ServiceCallInfo) this.Dictionary[key];
			}
			set
			{
				this.Dictionary[key] = value;
			}
		}

		/// <summary>
		/// Adds an element with the specified key and value to this ServiceCallInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to add.
		/// </param>
		/// <param name="value">
		/// The ServiceCallInfo value of the element to add.
		/// </param>
		public virtual void Add(string key, ServiceCallInfo value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this ServiceCallInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this ServiceCallInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this ServiceCallInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this ServiceCallInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this ServiceCallInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this ServiceCallInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this ServiceCallInfoDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The ServiceCallInfo value to locate in this ServiceCallInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this ServiceCallInfoDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(ServiceCallInfo value)
		{
			foreach (ServiceCallInfo item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this ServiceCallInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to remove.
		/// </param>
		public virtual void Remove(string key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this ServiceCallInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Keys
		{
			get
			{
				return this.Dictionary.Keys;
			}
		}

		/// <summary>
		/// Gets a collection containing the values in this ServiceCallInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Values
		{
			get
			{
				return this.Dictionary.Values;
			}
		}
	}
}
