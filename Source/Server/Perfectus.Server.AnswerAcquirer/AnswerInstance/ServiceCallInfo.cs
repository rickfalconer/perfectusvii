using System;
using System.Collections;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.AnswerAcquirer.AnswerInstance
{
	/// <summary>
	/// Summary description for ServiceCallInfo.
	/// </summary>
	public class ServiceCallInfo
	{
		WebReference wr;
		string typeName;
		string methodName;
		Hashtable paramTable = new Hashtable();
		object result;


		public object Result
		{
			get { return result; }
			set { result = value; }
		}

		public string TypeName
		{
			get { return typeName; }
		}

		public Hashtable ParamTable
		{
			get { return paramTable; }
		}

		public WebReference Wr
		{
			get { return wr;}
		}

		public string MethodName
		{
			 get { return methodName; }
		}

		public ServiceCallInfo(WebReference wr, string typeName, string methodName)
		{
			this.wr = wr;
			this.typeName = typeName;
			this.methodName = methodName;
		}
	}
}
