using System;
using System.Collections;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services.Protocols;
using System.Xml;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.ConfigurationSystem;

namespace Perfectus.Server.AnswerAcquirer.AnswerInstance
{
    /// <summary>
    /// Summary description for BindingSystem.
    /// </summary>
    public class BindingSystem
    {
        private Package package;

        private BindingSystem()
        {
        }

        public BindingSystem(Package instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                                    GetString("NullInstance"), 
                                                ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                                    GetString("Instance"));
            }

            package = instance;
        }

        public void Bind(InterviewPage page)
        {
            CallServices(page);
            BindRepeatersToItems(page);
        }

        public bool WillCallServices(InterviewPage page)
        {
            ArrayList alQuestionsOnPageAnsweredByServices = new ArrayList();

            if (page.Items != null)
            {
                GetServiceBoundQuestionsOnPage(page.Items, ref alQuestionsOnPageAnsweredByServices);
            }

            if (alQuestionsOnPageAnsweredByServices.Count == 0)
            {
                return false; // No services to call for this page.
            }
            else
            {
                return true;
            }
        }

        private void BindRepeatersToItems(InterviewPage page)
        {
            ArrayList alQuestionsOnPageAnsweredByRepeaters = new ArrayList();
            GetRepeaterBoundQuestionsOnPage(page.Items, ref alQuestionsOnPageAnsweredByRepeaters);
            if (alQuestionsOnPageAnsweredByRepeaters.Count == 0)
            {
                return; // Nothing to do on this page
            }
            Question[] questionsOnPageAnsweredByRepeaters = (Question[])alQuestionsOnPageAnsweredByRepeaters.ToArray(typeof(Question));

            try
            {
                // We actually bind to the answer(s) of the specified question - be it in a repeater or not.  Current StudioUI permits repeaters only as a valid source.

                foreach (Question q in questionsOnPageAnsweredByRepeaters)
                {
                    ItemsItemCollection newCol = new ItemsItemCollection();
                    string itemBinding = q.DataBindings.ItemsValueBinding;
                    if (itemBinding == null || itemBinding.Substring(0, 1) != "Q")
                    {
                        continue;
                    }

                    string sourceQuestionId = itemBinding.Substring(1);
                    Guid sourceQuestionGuid = new Guid(sourceQuestionId);

                    Question sourceQuestion = package.QuestionByGuid[sourceQuestionGuid];
                    if (sourceQuestion != null)
                    {
                        AnswerCollectionCollection collColl = sourceQuestion.GetAnswerCollectionCollection(AnswerProvider.Caller.System);
                        foreach (AnswerCollection coll in collColl)
                        {
                            foreach (object o in coll)
                            {
                                ItemsItem ni = new ItemsItem();
                                ni.Display = o.ToString();
                                ni.Value = o.ToString();
                                newCol.Add(ni);
                            }
                        }

                        q.Items = newCol;
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("RepeaterBinding");
                package.TransientErrors.Add(new TransientError(false, page, msg, ex));
            }
        }

        private static void GetRepeaterBoundQuestionsOnPage(PageItemCollection pcol, ref ArrayList qsOnPage)
        {
            if (pcol != null)
            {
                foreach (IPageItem i in pcol)
                {
                    if (i is Question)
                    {
                        Question q = (Question)i;
                        if (q.DataBindings != null && (q.DataBindings.ItemsValueBinding != null && q.DataBindings.ItemsValueBinding.Substring(0, 1) == "Q"))
                        {
                            qsOnPage.Add(q);
                        }
                    }
                    else
                    {
                        if (i is HorizontalLayoutZone)
                        {
                            GetServiceBoundQuestionsOnPage((((HorizontalLayoutZone)i).Items), ref qsOnPage);
                        }
                        else
                        {
                            if (i is RepeaterZone)
                            {
                                GetServiceBoundQuestionsOnPage(((RepeaterZone)i).Items, ref qsOnPage);
                            }
                        }
                    }
                }
            }
        }

        private static void GetServiceBoundQuestionsOnPage(PageItemCollection pcol, ref ArrayList qsOnPage)
        {
            if (pcol != null)
            {
                foreach (IPageItem i in pcol)
                {
                    if (i is Question)
                    {
                        Question q = (Question)i;

                        if (q.DataBindings != null && (q.DataBindings.AnswerBinding != null || (q.DataBindings.ItemsValueBinding != null /*&& q.DataBindings.ItemsValueBinding.Substring(0, 1) != "Q"*/)))
                        {
                            qsOnPage.Add(q);
                        }
                    }
                    else
                    {
                        if (i is HorizontalLayoutZone)
                        {
                            GetServiceBoundQuestionsOnPage((((HorizontalLayoutZone)i).Items), ref qsOnPage);
                        }
                        else
                        {
                            if (i is RepeaterZone)
                            {
                                GetServiceBoundQuestionsOnPage(((RepeaterZone)i).Items, ref qsOnPage);
                            }
                        }
                    }
                }
            }
        }

        //TODO: Refactor
        //TODO: Handle deeper use of structs/arrays
        private void CallServices(InterviewPage page)
        {
            // Get all the questions that are bound in each of the three possible ways.
            ArrayList alQuestionsThatProvideParams = new ArrayList();
            ArrayList alQuestionsWithBoundAnswers = new ArrayList();
            ArrayList alQquestionsWithBoundItems = new ArrayList();
            ArrayList alQuestionsOnPageAnsweredByServices = new ArrayList();

            if (page.Items != null)
            {
                GetServiceBoundQuestionsOnPage(page.Items, ref alQuestionsOnPageAnsweredByServices);
            }

            if (alQuestionsOnPageAnsweredByServices.Count == 0)
            {
                return; // No services to call for this page.
            }

            foreach (Question question in package.Questions)
            {
                DataBindingInfo dataBindingInfo = question.DataBindings;
                if (dataBindingInfo == null)
                    continue;
                if (dataBindingInfo.AnswerBinding != null)
                {
                    alQuestionsWithBoundAnswers.Add(question);
                }

                if (dataBindingInfo.ParameterBindings != null)
                {
                    alQuestionsThatProvideParams.Add(question);
                }

                if (dataBindingInfo.ItemsValueBinding != null)
                {
                    alQquestionsWithBoundItems.Add(question);
                }
            }

            Question[] questionsThatProvideParams = (Question[])alQuestionsThatProvideParams.ToArray(typeof(Question));
            Question[] questionsWithBoundAnswers = (Question[])alQuestionsWithBoundAnswers.ToArray(typeof(Question));
            Question[] questionsWithBoundItems = (Question[])alQquestionsWithBoundItems.ToArray(typeof(Question));
            Question[] questionsOnPageAnsweredByServices = (Question[])alQuestionsOnPageAnsweredByServices.ToArray(typeof(Question));

            ServiceCallInfoDictionary serviceCalls = new ServiceCallInfoDictionary();
            ArrayList questionBindCalls = new ArrayList();

            // Step 1.  Make the serviceCallInfo dictionary for the services that this page dictates need calling. Also populate the questionBindCalls array, which is the array of questions that have their answers bound to another questions answer.
            foreach (Question question in questionsOnPageAnsweredByServices)
            {
                if (question.DataBindings.AnswerBinding != null)
                {
                    // check if we bind to a web service OR a question.
                    if (question.DataBindings.AnswerBinding.Substring(0, 1) != "Q")
                    {
                        AddServiceBindingCall(question.DataBindings, BindingProperty.Answer, serviceCalls);
                    }
                    else // bind to a question
                    {
                        questionBindCalls.Add(question);
                    }
                }

                // Set up a serviceCallInfo for all the questions on this page that have items bound to a ws
                if (question.DataBindings.ItemsValueBinding != null && question.DataBindings.ItemsValueBinding.Substring(0, 1) != "Q")
                {
                    AddServiceBindingCall(question.DataBindings, BindingProperty.ItemsValue, serviceCalls);
                }
            }

            // Find any other parameters needed by these services
            foreach (Question question in questionsThatProvideParams)
            {
                string[] paramBindings = question.DataBindings.ParameterBindings;
                int index = 0;
                foreach (string parameterBinding in paramBindings)
                {
                    if (parameterBinding != string.Empty)
                    {
                        HandleParameterBinding(question, parameterBinding, serviceCalls, index);
                        index++;
                    }
                }
            }

            // Call the services:
            CallServices(package, serviceCalls, page);

            // Step 2:  Apply the answers we just got to any bound ITEMS for questions
            foreach (Question question in questionsWithBoundItems)
            {
                if (question.DataBindings.ItemsValueBinding == null || question.DataBindings.ItemsValueBinding.Substring(0, 1) == "Q")
                {
                    continue;
                }

                ApplyAnswersToBoundItem(question, serviceCalls);
            }

            // Step 3: Apply the answers we just got to the Answers property of questions with Answer binding.
            foreach (Question question in questionsWithBoundAnswers)
            {
                if (question.DataBindings == null || question.DataBindings.AnswerBinding == null)
                {
                    continue;
                }

                ApplyAnswersToAnswerPropertyOfBoundItem(question, serviceCalls, questionBindCalls, page);
            }
        }

        private void AddServiceBindingCall(DataBindingInfo dataBindingInfo, BindingProperty bindingProperty,
                                           ServiceCallInfoDictionary serviceCalls)
        {
            string guidString = string.Empty;
            string bindingInfo = string.Empty;

            switch (bindingProperty)
            {
                case BindingProperty.Answer:
                    guidString = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID);
                    bindingInfo = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.BindingInformation);
                    break;
                case BindingProperty.ItemsDisplay:
                    guidString = dataBindingInfo.GetWebServiceComponentForItemsDisplayBinding(WebReferenceComponent.GUID);
                    bindingInfo = dataBindingInfo.GetWebServiceComponentForItemsDisplayBinding(WebReferenceComponent.BindingInformation);
                    break;
                case BindingProperty.ItemsValue:
                    guidString = dataBindingInfo.GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.GUID);
                    bindingInfo = dataBindingInfo.GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.BindingInformation);
                    break;
            }

            EnsureValidBinding(guidString, bindingInfo);
            Guid guid;

            try
            {
                guid = new Guid(guidString);
            }
            catch (Exception ex)
            { 
                //TODO - need to handle this
                return;
            }

               string typeName = Regex.Match(bindingInfo, @"/serviceLayout/serviceType\[@name='(?<className>.*?)'",
                                          RegexOptions.ExplicitCapture).Groups["className"].Value;
            string methodName = Regex.Match(bindingInfo, @"/method\[@name='(?<methodName>.*?)'",
                                            RegexOptions.ExplicitCapture).Groups["methodName"].Value;

            // Set up a serviceCallInfo for all the questions on this page that have answer bound to a ws
            ServiceCallInfo call;
            string serviceCallKey = string.Format("{0}_{1}_{2}", guid, typeName, methodName);

            if (serviceCalls.Contains(serviceCallKey))
            {
                call = serviceCalls[serviceCallKey];
            }
            else
            {
                call = new ServiceCallInfo(package.WebReferenceByGuid[guid], typeName, methodName);
                serviceCalls.Add(serviceCallKey, call);
            }
        }

        private void EnsureValidBinding(string guid, string bindingInfo)
        {
            if (guid == string.Empty || bindingInfo == string.Empty)
            {
                throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                        GetString("UnexpectedBinding"));
            }
        }

        private void HandleParameterBinding(Question question, string parameterBinding,
                                            ServiceCallInfoDictionary serviceCalls, int index)
        {
            DataBindingInfo dataBindingInfo = question.DataBindings;
            string guidString = dataBindingInfo.GetWebServiceComponentForParameterBindings(WebReferenceComponent.GUID, index);
            string bindingInfo = dataBindingInfo.GetWebServiceComponentForParameterBindings(WebReferenceComponent.BindingInformation, index);
            Guid guid;

            try
            {
                guid = new Guid(guidString);
            }
            catch (Exception ex)
            {
                //TODO - need to handle this
                return;
            }

            string typeName = Regex.Match(parameterBinding, @"/serviceLayout/serviceType\[@name='(?<className>.*?)'",
                                          RegexOptions.ExplicitCapture).Groups["className"].Value;
            string methodName = Regex.Match(parameterBinding, @"/method\[@name='(?<methodName>.*?)'",
                                            RegexOptions.ExplicitCapture).Groups["methodName"].Value;
            string paramName = Regex.Match(parameterBinding, @"/parameter\[@name='(?<parameterName>.*?)'",
                                           RegexOptions.ExplicitCapture).Groups["parameterName"].Value;

            string serviceCallKey = string.Format("{0}_{1}_{2}", guid, typeName, methodName);
            ServiceCallInfo call;

            if (serviceCalls.Contains(serviceCallKey))
            {
                call = serviceCalls[serviceCallKey];
            }
            else
            {
                return;  // It's not a service we need to call, so skip answering its params.
            }

            object answerToPassAsParam;
            AnswerCollectionCollection collColl = question.GetAnswerCollectionCollection(AnswerProvider.Caller.System);
            if (collColl == null || collColl.Count == 0 || collColl[0] == null || collColl[0].Count == 0)
            {
                answerToPassAsParam = null;
            }
            else
            {
                answerToPassAsParam = collColl[0][0];
                answerToPassAsParam = AnswerProvider.ConvertToBestType(answerToPassAsParam, question.DataType, AnswerProvider.Caller.System, question.StringBindingMetaData);
            }

            //pf-3200 when this is performed in an answerset situation we need to go deeper...use the interview mode
            if (answerToPassAsParam == "")
            {
                collColl = question.GetAnswerCollectionCollection(AnswerProvider.Caller.Interview);
                if (collColl == null || collColl.Count == 0 || collColl[0] == null || collColl[0].Count == 0)
                {
                    answerToPassAsParam = null;
                }
                else
                {
                    answerToPassAsParam = collColl[0][0];
                    answerToPassAsParam = AnswerProvider.ConvertToBestType(answerToPassAsParam, question.DataType, AnswerProvider.Caller.System, question.StringBindingMetaData);
                }
            }
            //end pf-3200

            if (call.ParamTable.ContainsKey(paramName))
            {
                call.ParamTable[paramName] = answerToPassAsParam;
            }
            else
            {
                call.ParamTable.Add(paramName, answerToPassAsParam);
            }
        }

        private void CallServices(Package package, ServiceCallInfoDictionary serviceCalls, InterviewPage page)
        {
            foreach (ServiceCallInfo serviceCallInfo in serviceCalls.Values)
            {
                try
                {
                    serviceCallInfo.Result = InvokeService(serviceCallInfo);
                }
                catch (Exception ex)
                {
                    string message;
                    if (serviceCallInfo != null && serviceCallInfo.Wr != null)
                    {
                        string myerrorstr = ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                                GetString("WebserviceCallError");
                        myerrorstr = myerrorstr.Replace("{0}", serviceCallInfo.Wr.Name);
                        message = string.Format("{0} {1}.", myerrorstr, ex.Message);
                    }
                    else
                    {
                        message = string.Format(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").
                                                    GetString("FailedWebService"), ex.Message);
                    }

                    package.TransientErrors.Add(new TransientError(false, page, message, ex));
                }
            }
                }

        private void ApplyAnswersToBoundItem(Question question, ServiceCallInfoDictionary serviceCalls)
        {
                string serviceCallKey;
                ServiceCallInfo call;

            DataBindingInfo dataBindingInfo = question.DataBindings;
            string address = dataBindingInfo.GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.Address);
            string guidString = dataBindingInfo.GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.GUID);
            string bindingInfo = dataBindingInfo.GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.BindingInformation);
            string displayBinding = question.DataBindings.ItemsDisplayBinding;

            EnsureValidBinding(guidString, bindingInfo);
            Guid guid;

            try
            {
                guid = new Guid(guidString);
            }
            catch (Exception ex)
            {
                //TODO - need to handle this
                return;
            }

            string typeName = Regex.Match(bindingInfo, @"/serviceLayout/serviceType\[@name='(?<className>.*?)'", 
                                          RegexOptions.ExplicitCapture).Groups["className"].Value;
            string methodName = Regex.Match(bindingInfo, @"/method\[@name='(?<methodName>.*?)'", 
                                            RegexOptions.ExplicitCapture).Groups["methodName"].Value;
            string valueFieldName = Regex.Match(bindingInfo, @"/returns.*?/field\[@name='(?<fieldName>.*?)'", 
                                                RegexOptions.ExplicitCapture).Groups["fieldName"].Value;
            serviceCallKey = string.Format("{0}_{1}_{2}", guid, typeName, methodName);

                if (!serviceCalls.ContainsKey(serviceCallKey))
                {
                return;  // Don't answer this one, its service wasn't called.
                }
                call = serviceCalls[serviceCallKey];

                object result = call.Result;

            if (question.Items != null)
                {
                question.Items.Clear();
                }

                // If null was returned, set the Items to null and move on to the next question with ItemsBinding.
                if (result == null)
                {
                return;
                }

                Type resultType = result.GetType();
                ItemsItemCollection col = new ItemsItemCollection();

                if (resultType.IsArray)
                {
                    foreach (object resultElement in (object[])result)
                    {
                        col.Add(GetItemsFromObject(resultElement, valueFieldName, displayBinding));
                    }
                }
                else
                {
                    col.Add(GetItemsFromObject(result, valueFieldName, displayBinding));
                }

            question.Items = col;
            }

        private void ApplyAnswersToAnswerPropertyOfBoundItem(Question question, ServiceCallInfoDictionary serviceCalls,
                                                             ArrayList questionBindCalls, InterviewPage page)
            {
                object result = null;

            DataBindingInfo dataBindingInfo = question.DataBindings;

            if (dataBindingInfo.AnswerBinding.Substring(0, 1) != "Q")
                {
                    string serviceCallKey;
                    ServiceCallInfo call;

                string address = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.Address);
                string guidString = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID);
                string bindingInfo = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.BindingInformation);

                EnsureValidBinding(guidString, bindingInfo);
                Guid guid;

                try
                {
                    guid = new Guid(guidString);
                }
                catch (Exception ex)
                {
                    //TODO - need to handle this
                    return;
                }

                string typeName = Regex.Match(bindingInfo, @"/serviceLayout/serviceType\[@name='(?<className>.*?)'", 
                                              RegexOptions.ExplicitCapture).Groups["className"].Value;
                string methodName = Regex.Match(bindingInfo, @"/method\[@name='(?<methodName>.*?)'", 
                                                RegexOptions.ExplicitCapture).Groups["methodName"].Value;
                string fieldName = Regex.Match(bindingInfo, @"/returns.*?/field\[@name='(?<fieldName>.*?)'", 
                                               RegexOptions.ExplicitCapture).Groups["fieldName"].Value;
                serviceCallKey = string.Format("{0}_{1}_{2}", guid, typeName, methodName);

                    if (!serviceCalls.ContainsKey(serviceCallKey))
                    {
                    return;
                    }

                call = serviceCalls[serviceCallKey];
                    result = call.Result;

                    if (fieldName != null && fieldName.Length > 0 && result != null)
                    {
                        try
                        {
                            result = result.GetType().InvokeMember(fieldName, BindingFlags.GetField, null, result, null);
                        }
                        catch (Exception ex)
                        {
                            string msg = string.Format("{0} {1}.", ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("BindWebserviceError"), ex.Message);
                            package.TransientErrors.Add(new TransientError(false, page, msg, ex));
                        }
                    }
                }
                else // questions bound to questions
                {
                    // if question is in our questionBind array then add to result.
                if (questionBindCalls.Contains(question))
                    {
                    string guidString = dataBindingInfo.GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID).Substring (1);
                    Guid guid;

                    try
                    {
                        guid = new Guid(guidString);
                    }
                    catch (Exception ex)
                    {
                        //TODO - need to handle this
                        return;
                    }

                    if (package.QuestionByGuid.Contains(guid))
                        {
                        result = package.QuestionByGuid[guid].GetAnswer(0, AnswerProvider.Caller.System);
                            if (result is AnswerCollection && ((AnswerCollection)result).Count > 0)
                            {
                            result = AnswerProvider.ConvertToBestType(((AnswerCollection)result)[0], question.DataType, 
                                                                      AnswerProvider.Caller.Interview, question.StringBindingMetaData);
                            }
                        }
                        else
                        {
                            result = null;
                        }
                    }
                    else
                    {
                    return;
                    }
                }

            AnswerCollectionCollection ivwColl = question.AnswersByPerson;
            AnswerCollectionCollection svcColl = question.AnswersBySystem;

                if (ivwColl != null)
                {
                    ivwColl.Clear();
                }
                if (svcColl != null)
                {
                    svcColl.Clear();
                }
                if (result != null)
                {
                    //service returned linebreaks are only \n. Make them \r\n
                    // TODO: Where does this change to linebreaks happen?
                    if (result is string && result.ToString().IndexOf('\r') == -1)
                    {
                        result = Regex.Replace(result.ToString(), "\n", "\r\n");
                    }

                    //q.Answers.Add(new AnswerCollection(new object[]{result}));
                    AnswerCollection answer = new AnswerCollection(new object[] { result });
                    ivwColl.Add(answer);
                    svcColl.Add(answer);
                }
            }

        private static ItemsItem GetItemsFromObject(object result, string valueField, string displayField)
        {
            ItemsItem retVal = new ItemsItem();

            if (valueField == null)
            {
                retVal.Display = result.ToString();
                retVal.Value = result.ToString();
            }
            else
            {
                Type t = result.GetType();
                object val = t.InvokeMember(valueField, BindingFlags.GetField, null, result, null);
                object disp = t.InvokeMember(displayField, BindingFlags.GetField, null, result, null);

                retVal.Display = disp.ToString( );
                retVal.Value = val.ToString( );
            }

            return retVal;
        }

        private static object InvokeService(ServiceCallInfo info)
        {
            WebReference wr = info.Wr;
            string methodName = info.MethodName;
            Hashtable paramsTable = info.ParamTable;

            // If we are allowed to cache the results of this service, see if they are there, and return them if so.
            string cacheKey = null;
            if (wr.CacheResults)
            {
                string paramCacheKey;
                StringBuilder sb = new StringBuilder();
                foreach (string k in paramsTable.Keys)
                {
                    sb.Append(string.Format("{0}={1}", k, paramsTable[k]));
                }

                paramCacheKey = sb.ToString();
                cacheKey = string.Format("webserviceResult_{0}_{1}_{2}", methodName, wr.WebServiceAddress, paramCacheKey);

                object cachedResults = HttpRuntime.Cache[cacheKey];
                if (cachedResults != null)
                {
                    return cachedResults;
                }
            }

            object result = null;

            // If we can't cache, or we aren't already in the cache, get a proxy instance and invoke.

            // Make our instance of the first type in the proxy assembly.
            // TODO: Is this always okay with our proxies?
            SoapHttpClientProtocol instance;

            Assembly proxyAssembly = new Perfectus.Common.WebServiceSystem.DynamicProxy(new Uri(wr.WebServiceAddress)).Assembly;

            Type instanceType = null;
            foreach (Type t in proxyAssembly.GetTypes())
            {
                if (t.Name == info.TypeName)
                {
                    instanceType = t;
                    break;
                }
            }

            if (instanceType == null)
            {
                throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("TypeNotFound"));
            }

            instance = (SoapHttpClientProtocol)Activator.CreateInstance(instanceType);

            // Find our method.

            bool foundMethod = false;
            foreach (MethodInfo mi in instanceType.GetMethods())
            {
                if (mi.Name == methodName)
                {
                    foundMethod = true;
                    // Populate its params
                    ArrayList paramsInOrder = new ArrayList(paramsTable.Count);
                    foreach (ParameterInfo pi in mi.GetParameters())
                    {
                        if (paramsTable.ContainsKey(pi.Name))
                        {
                            object paramCast;
                            if (paramsTable[pi.Name] == null)
                            {
                                paramCast = null;
                            }
                            else
                            {
                                paramCast = Convert.ChangeType(paramsTable[pi.Name], pi.ParameterType);
                            }

                            paramsInOrder.Add(paramCast);
                        }
                    }

                    instance.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    result = mi.Invoke(instance, paramsInOrder.ToArray());
                    break;
                }
            }

            if (foundMethod)
            {
                // If we are cache-able, cache the result of the service.
                if (wr.CacheResults && cacheKey != null)
                {
                    HttpRuntime.Cache.Add(cacheKey, result, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(0, 0, 0, Config.Shared.interview.serviceResultCacheTtl, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                }
            }
            else
            {
                throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("MethodNotFound"));
            }

            return result;
        }
    }
}
