using System;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Server.AnswerAcquirer.AnswerInstance
{
	/// <summary>
	/// Summary description for TypeChecker.
	/// </summary>
	public sealed class TypeChecker
	{
		private TypeChecker()
		{
		}

		public static bool IsValidAnswerType(Question q, object answer, out string Message)
		{
			Message = null;

			switch (q.DataType)
			{
				case PerfectusDataType.Date:
					if (answer is DateTime)
					{
						return true;
					} else
					{
						try
						{
							DateTime.Parse(answer.ToString());
							return true;
						}
						catch
						{
							//TODO: Localise
							Message = ErrorMessage(q, answer);
							return false;
						}
					}

				case PerfectusDataType.Number:
					if (typeof(decimal).IsAssignableFrom(answer.GetType()))
					{
						return true;
					} else
					{
						decimal d;
                        bool isDecimal = decimal.TryParse(answer.ToString(), NumberStyles.Number, CultureInfo.CurrentCulture, out d);
						if (isDecimal)
						{
							return true;
						}
						else
						{
							
							Message = ErrorMessage(q, answer);
							return false;
						}
					}
				case PerfectusDataType.YesNo:
					if (answer is bool)
					{
						return true;
					} else
					{
						if (answer.ToString().ToUpper(CultureInfo.InvariantCulture) == "YES" || answer.ToString().ToUpper(CultureInfo.InvariantCulture) == "NO" )
						{
							return true;
						}
						
						try
						{
							bool.Parse(answer.ToString());	
							return true;
						}
						catch
						{
							Message = ErrorMessage (q, answer);
							return false;
						}
					}
				case PerfectusDataType.Text:
				default:
					return true;
			}
		}


		private static string ErrorMessage(Question q, object answer)
		{
			string myerrormsg=ResourceLoader.GetResourceManager("Perfectus.Server.AnswerAcquirer.Localisation").GetString("AnswerError");
			myerrormsg=myerrormsg.Replace("{0}",q.DataType.ToString());
			myerrormsg=myerrormsg.Replace("{1}",q.Name);
			return string.Format("{0} '{2}'", myerrormsg, answer);
		}
	}
}
