using System;
using Perfectus.Server.AnswerAcquirer.Xml;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// Processor class for received files.
	/// </summary>
	public class FileAcceptorProcessor
	{
		/// <summary>
		/// Process the file.
		/// </summary>
		/// <param name="fullPathFileName"></param>
		/// <param name="transformSet"></param>
		public void Process(string fullPathFileName, fileAcceptorConfigurationTransformSetsTransformSet transformSet)
		{
			
			LogUtilities.WriteLine(transformSet.name, string.Format("Processing file '{0}' using transform set '{1}'", fullPathFileName, transformSet.name));
			
			// Validate the XML against the schema.
			if ((transformSet.schema.Length > 0) && !XMLValidator.Validate(fullPathFileName, transformSet.schema))
			{
				throw new ApplicationException("XML validation error");
			}
			
			// Transform into an answer set and send to Perfectus.
			LogUtilities.WriteLine(transformSet.name, string.Format("Transforming into an answer set using schema '{0}' and a priority of '{1}'...", transformSet.schema, transformSet.priority));

			// Submit to the Perfectus server for processing.
			Submitter.SubmitXmlFileBatch(fullPathFileName, transformSet.transformation, "/", 
			                             Submitter.QuestionMatchModeOption.Name, Submitter.ApplyModeOption.SimulateInterview, 
			                             Guid.Empty, true, transformSet.IsHighPriority());
		}
	}
}
