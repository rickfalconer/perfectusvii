using System.Xml;
using System.Xml.Schema;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// Validate an XML file against a schema.
	/// </summary>
	public class XMLValidator
	{
		private static bool _Valid;
		
		/// <summary>
		/// Validate the file using the schema supplied.
		/// </summary>
		/// <param name="fullPathFileName"></param>
		/// <param name="fullPathSchemaFile"></param>
		/// <returns></returns>
		public static bool Validate(string fullPathFileName, string fullPathSchemaFile)
		{
			// Validate the XML against the schema.
			XmlTextReader tr = new XmlTextReader(fullPathFileName);
			XmlValidatingReader vr = new XmlValidatingReader(tr);

			try
			{
				_Valid = true;
				vr.ValidationType = ValidationType.Schema;
				vr.Schemas.Add("", fullPathSchemaFile);
				vr.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);
				while (vr.Read()){}
			}
			finally
			{
				vr.Close();
			}
			return _Valid;
		}
		private static void ValidationHandler(object sender, ValidationEventArgs args)
		{
			LogUtilities.WriteLine("XMLValidator", args.Message);
			_Valid = false;
		}
	}
}