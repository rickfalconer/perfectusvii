using System.Xml;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// Configuration section handler for the File Acceptor configuration.
	/// </summary>
	public class FileAcceptorConfigurationSectionHandler : System.Configuration.IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			return Config.LoadFrom(section);
		}
	}
}
