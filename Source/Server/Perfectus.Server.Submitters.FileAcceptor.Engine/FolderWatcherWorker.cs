using System;
using System.IO;
using System.Threading;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// The FileWatcherWorker class is responsible for starting a watch on a folder and 
	/// processing any files found. It also handles processing any stray files left in 
	/// the folder (clean up) by checking at regular intervals as defined in the configuration 
	/// file. This clean up will take care of files that exist in the folder when the service is first started.
	/// </summary>
	public class FolderWatcherWorker
	{
		private fileAcceptorConfigurationTransformSetsTransformSet _TransformSet;
		private int	_FileCount;
		
		private ModeEnum _Mode;
		private enum ModeEnum
		{
			Unknown,
			Watch,
			CleanUp
		};

		private const string ERROR_EXTENSION = ".error";
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="transformSet">Transform set as defined in the application configuration file</param>
		public FolderWatcherWorker(fileAcceptorConfigurationTransformSetsTransformSet transformSet)
		{
			_FileCount = 0;
			_TransformSet = transformSet;
			_Mode = ModeEnum.Unknown;
		}

		/// <summary>
		/// Start watching the specified folder.
		/// </summary>
		public void WatchFolder()
		{
			_Mode = ModeEnum.Watch;
			WriteToLog(string.Format("Watch started for transform set '{0}'", _TransformSet.name));

			FolderWatcher folderWatcher = new FolderWatcher(_TransformSet.path, 
			                                                _TransformSet.filter, 
			                                                Config.FileAcceptorConfig.transformSets.fileTimeout, 
			                                                Config.FileAcceptorConfig.transformSets.fileChangeTimeout, 
			                                                _TransformSet, 
			                                                new FileHandlerDelegate(this.SendFileForProcessing));
			folderWatcher.Start();
		}
		
		/// <summary>
		/// Clean up the folder.
		/// Process any files that might be in the directory. There is a chance that events raised by the 
		/// FileSystemWatcher have been lost if too many happen at once.
		/// This process will also process files that exist when restarting the service.
		/// </summary>
		public void CleanUp()
		{
			_Mode = ModeEnum.CleanUp;
			WriteToLog(string.Format("Clean up thread started for transform set '{0}'", _TransformSet.name));
			while (true)
			{
				try
				{
					ProcessExistingFiles();
				}
				catch (Exception e)
				{
					WriteToLog(string.Format("Error during CleanUp for transform set '{0}': {1}", _TransformSet.name, e));
				}
				Thread.Sleep(Config.FileAcceptorConfig.transformSets.cleanUpInterval);
			}
		}
		
		/// <summary>
		/// Process any files in the folder.
		/// </summary>
		private void ProcessExistingFiles()
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(_TransformSet.path);
			string filter = _TransformSet.filter.Length == 0 ? "*" : _TransformSet.filter;
			foreach(FileInfo fi in directoryInfo.GetFiles(filter))
			{
				SendFileForProcessing(Path.Combine(_TransformSet.path, fi.Name), _TransformSet);
			}
		}
		
		/// <summary>
		/// Send the file for processing.
		/// </summary>
		/// <param name="fullPathFileName"></param>
		/// <param name="info"></param>
		private void SendFileForProcessing(string fullPathFileName, object info)
		{
			string fullPathErrorFileName;
			bool processed = false;
			
			// Retrieve the transform set information object.
			if (info == null)
			{
				throw new ApplicationException(string.Format("Could not process '{0}': transformSet object not provided.", fullPathFileName));
			}

			// Get transform set information.
			fileAcceptorConfigurationTransformSetsTransformSet transformSet = (fileAcceptorConfigurationTransformSetsTransformSet) info;

			try
			{
				if (!FileUtilities.IsFileLocked(fullPathFileName))
				{
					// Mark the file as read-only to prevent other threads from processing it.
					File.SetAttributes(fullPathFileName, File.GetAttributes(fullPathFileName) | FileAttributes.ReadOnly);
				
					// Process the file.
					FileAcceptorProcessor processor = new FileAcceptorProcessor();
					processor.Process(fullPathFileName, transformSet);
					processed = true;
					_FileCount += 1;

					// Move the file from the watch folder into the archive folder when successful.
					fullPathFileName = MoveFileToArchiveFolder(fullPathFileName);
			
					File.SetAttributes(fullPathFileName, FileAttributes.Normal);  //(archived file)

					WriteToLog(string.Format("Files processed: {0}", _FileCount));
				}
			}
			catch (Exception e)
			{
				if (!processed) // leave processed files as read-only so they don't get picked up again.
				{
					fullPathErrorFileName = string.Format("{0}{1}", fullPathFileName, ERROR_EXTENSION);
					File.Move(fullPathFileName, fullPathErrorFileName);
					File.SetAttributes(fullPathErrorFileName, FileAttributes.Normal);
				}
				WriteToLog(string.Format("Error processing file '{0}': {1}", fullPathFileName, e));

                // Write out any inner exception
                WriteToLog(string.Format("Error processing file '{0}': {1}", fullPathFileName, e.InnerException));
			}
		}

		/// <summary>
		/// Move a file to the archive folder.
		/// </summary>
		/// <param name="fullPathFileName">Fully pathed file name</param>
		/// <returns>Fully pathed archive file name</returns>
		private string MoveFileToArchiveFolder(string fullPathFileName)
		{
			// Construct the archive file name. Delete it if it already exists.
			string fullPathArchiveFileName = Path.Combine(Config.FileAcceptorConfig.transformSets.archiveFolder, Path.GetFileName(fullPathFileName));
			FileInfo fi = new FileInfo(fullPathArchiveFileName);
			if (fi.Exists)
			{
				WriteToLog(string.Format("Archive file '{0}' already exists and will be deleted.", fullPathArchiveFileName));
				fi.Delete();
			}
			
			// Move the file.
			fi = new FileInfo(fullPathFileName);
			fi.MoveTo(fullPathArchiveFileName);
			
			return fullPathArchiveFileName;
		}

		private void WriteToLog(string message)
		{
			LogUtilities.WriteLine(string.Format("{0}{1}", _TransformSet.name, _Mode), message);
		}
	}
}
