using System;
using System.Collections;
using System.IO;
using System.Threading;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// Main controlling File Acceptor class.
	/// This class is responsible for starting up and shutting down a folder watcher 
	/// thread and a folder cleanup thread for each transform set defined in the application 
	/// configuration file.
	/// </summary>
	public class FileAcceptor
	{
		private static FileAcceptor instance;
		private ArrayList _Threads;

		/// <summary>
		/// Start accepting files.
		/// </summary>
		public void Start()
		{
			try
			{
				LogUtilities.WriteLine("", "**** FileAcceptor starting ***");

				// Create archive folder.
				CreateArchiveFolder();

				// Set up a folder watcher for each transform set.
				_Threads = new ArrayList();
				for (int i = 0; i < Config.FileAcceptorConfig.transformSets.transformSet.Length; i++)
				{
					fileAcceptorConfigurationTransformSetsTransformSet transformSet = Config.FileAcceptorConfig.transformSets.transformSet[i];
					StartWatchThread(transformSet);
				} 

			}
			catch (Exception e)
			{
				LogUtilities.WriteLine("", string.Format("FileAcceptor Start error: {0}", e));
				throw;
			}
		}

		/// <summary>
		/// Create an archive folder for processed files.
		/// </summary>
		private static void CreateArchiveFolder()
		{
            string archiveFolderPath = Config.FileAcceptorConfig.transformSets.archiveFolder;

            if (archiveFolderPath.Length == 0)
            {
                throw new ArgumentNullException("Error, the archive folder is not specified.");
            }

			try
			{
				DirectoryInfo diArchiveFolder = new DirectoryInfo(Config.FileAcceptorConfig.transformSets.archiveFolder);
				if(!diArchiveFolder.Exists)
				{
					diArchiveFolder.Create();
				}
			}
			catch (Exception e)
			{
				LogUtilities.WriteLine("", string.Format("Error creating the archive folder: {0}", e));
				throw;
			}
		}

		/// <summary>
		/// Start a watch on the folder and also a thread to tidy up missed files.
		/// </summary>
		/// <param name="transformSet"></param>
		private void StartWatchThread(fileAcceptorConfigurationTransformSetsTransformSet transformSet)
		{
			// Start watch thread.
			FolderWatcherWorker folderWatcherWorker = new FolderWatcherWorker(transformSet);
			ThreadStart startIn = new ThreadStart(folderWatcherWorker.WatchFolder);
			Thread tIn = new Thread(startIn);
			tIn.Name = "Folder watcher";
			tIn.Start();
			_Threads.Add(tIn);

			// Start clean up thread at low priority.
			FolderWatcherWorker folderWatcherCleanup = new FolderWatcherWorker(transformSet);
			ThreadStart startCleanup = new ThreadStart(folderWatcherCleanup.CleanUp);
			Thread tCleanup = new Thread(startCleanup);
			tCleanup.Name = "Folder clean up";
			tCleanup.Priority = ThreadPriority.Lowest;
			tCleanup.Start();
			_Threads.Add(tCleanup);
		}
		
		/// <summary>
		/// Stop all threads.
		/// </summary>
		public void Stop()
		{
			foreach (Thread thread in _Threads)
			{
				thread.Abort();
			}
			LogUtilities.WriteLine("", "**** FileAcceptor ending ***");
		}

		/// <summary>
		/// Return an instance of this class.
		/// </summary>
		/// <returns></returns>
		public static FileAcceptor GetInstance()
		{
			if (null == instance)
			{
				instance = new FileAcceptor();
			}
			return instance;
		}
	}
}
