using System;
using System.IO;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	public delegate void FileHandlerDelegate(string fullPath, object info);

	/// <summary>
	/// FolderWatcher class.
	/// </summary>
	public class FolderWatcher
	{
		FileSystemWatcher _Watcher;

		private string	_DirectoryToWatch;
		private string	_Filter;
		private object	_Info;
		private int		_FileTimeOut;
		private int		_FileChangeTimeout;
		
		private bool	_IsProcessing = false;

		public event FileHandlerDelegate handler;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="directoryToWatch">Directory to watch</param>
		/// <param name="filter">Wild card filter</param>
		/// <param name="info">Information object that will be passed back with the FileHandlerDelegate</param>
		/// <param name="handler">FileHandlerDelegate</param>
		public FolderWatcher(string directoryToWatch, string filter, int fileTimeOut, int fileChangeTimeout, object info, FileHandlerDelegate handler)
		{
			this.handler = handler;
			_DirectoryToWatch = directoryToWatch;
			_Filter = filter;
			_Info = info;
			_FileTimeOut = fileTimeOut;				// maximum time to wait before giving up and skipping the file.
			_FileChangeTimeout = fileChangeTimeout;	// minimum time to wait for file changes to settle.
			
			_Watcher = new FileSystemWatcher(_DirectoryToWatch);
			_Watcher.Filter = _Filter;
			_Watcher.Created +=new FileSystemEventHandler(watcher_Created);
			_Watcher.Renamed +=new RenamedEventHandler(watcher_Renamed);
		}
		
		/// <summary>
		/// Start watching.
		/// </summary>
		public void Start()
		{
			// Begin watching.
			_Watcher.EnableRaisingEvents = true;

			LogUtilities.WriteLine(_DirectoryToWatch, string.Format("**Watching folder '{0}' with a filter of '{1}'...", _DirectoryToWatch, _Filter));
		}

		private void watcher_Created(object sender, FileSystemEventArgs e)
		{
			LogUtilities.WriteLine(_DirectoryToWatch, string.Format("(change event: {0})", e.ChangeType));
			ProcessFile(e.FullPath);
		}

		private void watcher_Renamed(object sender, RenamedEventArgs e)
		{
			LogUtilities.WriteLine(_DirectoryToWatch, "(rename event)");
			
			// Only process if the new name matches the filter.
			if (FileUtilities.IsFileNameLike(e.FullPath, _Filter))
				ProcessFile(e.FullPath);
		}

		private void ProcessFile(string fullPathFileName)
		{
			if(_IsProcessing)
			{
				LogUtilities.WriteLine(_DirectoryToWatch, string.Format("######### Skipped file '{0}' because still busy! #########", fullPathFileName));
			}
			else
			{
				_IsProcessing = true;
				try
				{
					LogUtilities.WriteLine(_DirectoryToWatch, string.Format("--Found file '{0}'--", fullPathFileName));

					// Ensure the file is fully written and has no locks before notifying listeners.
					if (FileUtilities.IsFileFree(fullPathFileName, _FileTimeOut, _FileChangeTimeout))
					{
						handler(fullPathFileName, _Info);
					}
					else
					{
						LogUtilities.WriteLine(_DirectoryToWatch, string.Format("***Timed out waiting for file '{0}' to free up***", fullPathFileName));
					}
				}
				catch(Exception ex)
				{
					LogUtilities.WriteLine(_DirectoryToWatch, string.Format("ProcessFile error while watching '{0}': {1}", _DirectoryToWatch, ex));
				}
				finally
				{
					_IsProcessing = false;
				}
			}
		}
		
	}
}
