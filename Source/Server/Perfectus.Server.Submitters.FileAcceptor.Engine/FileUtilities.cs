using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// File utilities.
	/// </summary>
	public class FileUtilities
	{

		/// <summary>
		/// Determines if a file is free of locks or recent changes.
		/// </summary>
		/// <param name="fullPathFileName">Fully pathed file name</param>
		/// <param name="fileTimeOut">Maximum time to wait before giving up and skipping the file</param>
		/// <param name="fileChangeTimeout">Minimum time to wait for file changes to settle</param>
		/// <returns></returns>
		public static bool IsFileFree(string fullPathFileName, int fileTimeOut, int fileChangeTimeout)
		{
			bool isFileFree = false;

			try
			{
				LogUtilities.WriteLine("", string.Format("Checking if file '{0}' is free", fullPathFileName));

				// Set up a FolderWatcher just for this file so changes can be monitored.
				FileSystemWatcher watcher = new FileSystemWatcher(Path.GetDirectoryName(fullPathFileName));
				watcher.Filter = fullPathFileName;

				// Set the how long to keep trying before giving up.
				DateTime dtTimeout = DateTime.Now.AddMilliseconds(fileTimeOut);
					
				while(DateTime.Now < dtTimeout)
				{
					WaitForChangedResult changeResult = watcher.WaitForChanged(WatcherChangeTypes.Changed, fileChangeTimeout);
					if (changeResult.TimedOut) // no changes detected during timeout period
					{
						if (!IsFileLocked(fullPathFileName))
						{
							isFileFree = true;
							break;
						}
					}

					// File may have been deleted while waiting for it to free up.
					if (!File.Exists(fullPathFileName))
					{
						LogUtilities.WriteLine("", string.Format("File '{0}' no longer exists", fullPathFileName));
						break;
					}
					
					LogUtilities.WriteLine("", string.Format("Still waiting for file '{0}' to free up", fullPathFileName));
				}
			}
			catch
			{
				isFileFree = false;
			}
			return isFileFree;
		}

		/// <summary>
		/// Determine if a file is locked.
		/// </summary>
		/// <param name="fullPathFileName"></param>
		/// <returns></returns>
		public static bool IsFileLocked(string fullPathFileName)
		{
			try
			{
				FileInfo fi = new FileInfo(fullPathFileName);
				FileStream fs = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
				fs.Close();
				return false;
			}
			catch
			{
				return true;
			}
		}

		/// <summary>
		/// Determine if a file name matches a wildcard pattern.
		/// The wildcard filter is converted into a regular expression.
		/// </summary>
		/// <param name="fullPathFileName"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		public static bool IsFileNameLike(string fullPathFileName, string filter)
		{
			try
			{
				string fileNameOnly = Path.GetFileName(fullPathFileName);
				return new Regex(filter.Replace("*", ".*").Replace(".", @"\.") + "\\Z").IsMatch(fileNameOnly);
			}
			catch
			{
				return false;
			}
		}
	}
}
