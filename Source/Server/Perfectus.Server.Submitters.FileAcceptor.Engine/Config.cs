using System;
using System.Configuration;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{

	/// <summary>
	/// Configuration class for the File Acceptor.
	/// </summary>
	public sealed class Config
	{
		private Config()
		{}
		static Config()
		{
			// Get configuration.
			FileAcceptorConfig = (fileAcceptorConfiguration) ConfigurationSettings.GetConfig("fileAcceptorSection");
		}

		/// <summary>
		/// Public access to the configuration.
		/// </summary>
		public static fileAcceptorConfiguration FileAcceptorConfig;

		/// <summary>
		/// Load the configuration from the configuration XML node passed in.
		/// </summary>
		/// <param name="section"></param>
		/// <returns></returns>
		internal static fileAcceptorConfiguration LoadFrom(XmlNode section)
		{
			fileAcceptorConfiguration retval;
			XmlTextReader xr;
			XmlSerializer ser;

			try
			{
				TextReader tr = new StringReader(section.InnerXml);
				xr = new XmlTextReader(tr);
				ser = new XmlSerializer(typeof(fileAcceptorConfiguration));
				retval = (fileAcceptorConfiguration)(ser.Deserialize(xr));
			
			}
			catch (Exception e)
			{
				Console.Write(e);
				throw new ApplicationException(e.ToString());
			}
			return retval;
		}
	}
}