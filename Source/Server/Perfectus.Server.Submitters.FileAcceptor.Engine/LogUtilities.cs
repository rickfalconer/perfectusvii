using System;
using System.Diagnostics;
using System.Threading;

namespace Perfectus.Server.Submitters.FileAcceptor.Engine
{
	/// <summary>
	/// Log utility methods.
	/// </summary>
	public class LogUtilities
	{
		/// <summary>
		/// Write a line to the trace log.
		/// The line is always preceded by the universal date and time and the thread ID.
		/// </summary>
		/// <param name="key">Key value (optional)</param>
		/// <param name="message">Log message</param>
		public static void WriteLine(string key, string message)
		{
			Trace.WriteLine(string.Format("{0}_{1:00000}_{2}: {3}", DateTime.UtcNow, Thread.CurrentThread.GetHashCode(), key, message));
		}
	}
}
