rem Rick Nov 2014
rem Run after changing server files in VS, this will refresh changes into the perfectus server website and program folders.
rem
xcopy "Perfectus.Server.Administrator\bin\x86\Debug\Administrator.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Administrator" /D
xcopy "Perfectus.Server.Administrator\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Administrator" /D

xcopy "Perfectus.Server.Submitters.FileAcceptor.Console\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\FileAcceptor" /D
xcopy "Perfectus.Server.Submitters.FileAcceptor.Engine\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\FileAcceptor" /D
xcopy "Perfectus.Server.Submitters.FileAcceptor.Service\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\FileAcceptor" /D

xcopy "Perfectus.Server.Submitters.MSMQ.Console\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\MSMQAcceptor" /D
xcopy "Perfectus.Server.Submitters.MSMQ.Engine\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\MSMQAcceptor" /D
xcopy "Perfectus.Server.Submitters.MSMQ.Service\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\MSMQAcceptor" /D

xcopy "Plugins\Perfectus.Server.Plugins.FormatStringExtensions.QuestionExtensionGeneric\bin\x86\Debug\FormatQuestionExtension.ItemListDisplay.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\FormatQuestionExtension" /D
xcopy "Plugins\Perfectus.Server.Plugins.FormatStringExtensions.NameFormatter\bin\x86\Debug\Perfectus.Server.Plugins.FormatStringExtensions.NameFormatter.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\NameFormatter\NameFormatter.*" /D
xcopy "Plugins\Perfectus.Server.Plugins.FormatStringExtensions.NumberToWords\bin\x86\Debug\Perfectus.Server.Plugins.FormatStringExtensions.NumberToWords.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\NumberToWords\NumberToWords.*" /D
xcopy "Plugins\Perfectus.Server.Plugins.Converters.AnswerSetXml\bin\x86\Debug\Perfectus.Server.Plugins.Converters.AnswerSetXml.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.AnswerSetXml" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.AnswerSummaryWordML\bin\x86\Debug\Perfectus.Server.Plugins.Converters.AnswerSummaryWordML.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.AnswerSummaryWordML" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.Doc.Word\bin\x86\Debug\Perfectus.Server.Plugins.Converters.Doc.Word.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.Doc.Word" /D
xcopy "Plugins\Perfectus.Server.Plugins.Converters.EasyResume\bin\x86\Debug\Perfectus.Server.Plugins.Converters.EasyResume.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.EasyResume" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.Html\bin\x86\Debug\Perfectus.Server.Plugins.Converters.Html.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.Html" /D
xcopy "Plugins\Perfectus.Server.Plugins.Converters.MirrorXml\bin\x86\Debug\Perfectus.Server.Plugins.Converters.MirrorXml.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.MirrorXml" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.Pdf.Word\bin\x86\Debug\Perfectus.Server.Plugins.Converters.Pdf.Word.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.Pdf.Word" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.Rtf.Word\bin\x86\Debug\Perfectus.Server.Plugins.Converters.Rtf.Word.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.Rtf.Word" /D
xcopy "Plugins\Perfectus.Server.Plugins.Converters.Word2007\bin\x86\Debug\Perfectus.Server.Plugins.Converters.Word2007.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.Word2007" /D
rem xcopy "Plugins\Perfectus.Server.Plugins.Converters.WordML\bin\x86\Debug\Perfectus.Server.Plugins.Converters.WordML.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Converters.WordML" /D
xcopy "Plugins\Perfectus.Server.Plugins.Distributors.Email\bin\x86\Debug\Perfectus.Server.Plugins.Distributors.Email.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Distributors.Email" /D
xcopy "Plugins\Perfectus.Server.Plugins.Distributors.Execute\bin\x86\Debug\Perfectus.Server.Plugins.Distributors.Execute.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Distributors.Execute" /D
xcopy "Plugins\Perfectus.Server.Plugins.Distributors.FileSystem\bin\x86\Debug\Perfectus.Server.Plugins.Distributors.FileSystem.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Distributors.FileSystem" /D
xcopy "Plugins\Perfectus.Server.Plugins.Distributors.MSMQ\bin\x86\Debug\Perfectus.Server.Plugins.Distributors.MSMQ.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Distributors.MSMQ" /D
xcopy "Plugins\Perfectus.Server.Plugins.Distributors.Printer\bin\x86\Debug\Perfectus.Server.Plugins.Distributors.Printer.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Plugins\Perfectus.Server.Plugins.Distributors.Printer" /D

xcopy "Plugins\Perfectus.Server.PluginBase\bin\x86\Debug\Perfectus.Server.PluginBase.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator" /D

xcopy "Perfectus.Server.ProcessCoordinator.Console\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator" /D
xcopy "Perfectus.Server.ProcessCoordinator.Engine2\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator" /D
xcopy "Perfectus.Server.ProcessCoordinator.Receiver\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator" /D
xcopy "Perfectus.Server.ProcessCoordinator.Service\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ProcessCoordinator" /D

xcopy "Perfectus.Server.Reporting\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ReportingSynchronizer" /D
xcopy "Perfectus.Server.ReportingSynchronizer.Console\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ReportingSynchronizer" /D
xcopy "Perfectus.Server.ReportingSynchronizer.Engine\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ReportingSynchronizer" /D
xcopy "Perfectus.Server.ReportingSynchronizer.Installer\bin\x86\Debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\ReportingSynchronizer" /D

xcopy "Perfectus.Server.WebUI.Library\bin\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\bin" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\bin\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\bin" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\AnswerSet.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\GetPreviewFile.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Identify.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Interview.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\InterviewPreview.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Preview.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Preview.xslt" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\RenderImage.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Status.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Web.Config" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\AnswerAcquisition\acquirer.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\AnswerAcquisition" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\ConfigurationSystem\Plugins.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\ConfigurationSystem" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\ConfigurationSystem\Server.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\ConfigurationSystem" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\InterviewSystem\InstanceStatus.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\InterviewSystem" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\PackageManager\publishing.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\PackageManager\Publishing.asmx" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\PackageManager\PublishStreaming.svc" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\PackageManager" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Reports\DesktopInstanceManagement.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\Reports" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Reports\DesktopPublish.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\Reports" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Reports\Report.aspx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\Reports" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Script\*.js" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\Script" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Styles\*.css" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\Styles" /D 

xcopy "Perfectus.WebServices\bin\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\WebServices\bin" /D 
xcopy "Perfectus.WebServices\SharedLibrary\SharedLibrary.asmx" "C:\Program Files (x86)\Perfectusvii\ProcessManager\Web\Interfaces\WebServices\SharedLibrary" /D 

xcopy "Perfectus.Server.RestService\bin\x86\debug\Perfectus.*" "C:\Program Files (x86)\Perfectusvii\Perfectus.RESTService" /D 

