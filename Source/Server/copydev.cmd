
xcopy "Perfectus.Server.WebUI.Library\bin\Perfectus.*" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\bin" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\bin\Perfectus.*" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\bin" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\AnswerSet.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\GetPreviewFile.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Identify.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Interview.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\InterviewPreview.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Preview.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Preview.xslt" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\RenderImage.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Status.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Web.Config" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\AnswerAcquisition\acquirer.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\AnswerAcquisition" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\ConfigurationSystem\Plugins.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\ConfigurationSystem" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\ConfigurationSystem\Server.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\ConfigurationSystem" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\InterviewSystem\InstanceStatus.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\InterviewSystem" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\PackageManager\publishing.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\PackageManager\Publishing.asmx" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\PackageManager\PublishStreaming.svc" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\PackageManager" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Reports\DesktopInstanceManagement.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\Reports" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Reports\DesktopPublish.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\Reports" /D 
xcopy "Perfectus.Server.WebUI.Interfaces\Reports\Report.aspx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\Reports" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Script\*.js" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\Script" /D 

xcopy "Perfectus.Server.WebUI.Interfaces\Styles\*.css" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\Styles" /D 

xcopy "Perfectus.WebServices\bin\Perfectus.*" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\WebServices\bin" /D 
xcopy "Perfectus.WebServices\SharedLibrary\SharedLibrary.asmx" "C:\Program Files (x86)\Perfectus\ProcessManager\Web\Interfaces\WebServices\SharedLibrary" /D 

xcopy "Perfectus.Server.RestService\bin\x86\debug\Perfectus.*" "C:\Program Files (x86)\Perfectus\Perfectus.RESTService" /D 

