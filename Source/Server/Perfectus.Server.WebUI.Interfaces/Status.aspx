<%@ Page language="c#" Inherits="Perfectus.Server.WebUI.Interfaces.Status" trace="false" %>
<%@ Register TagPrefix="cc1" Namespace="Perfectus.Server.WebUI.Library.Status" Assembly="Perfectus.Server.WebUI.Library, culture=neutral" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Status</title>
		<LINK href="Styles/default.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<cc1:Status id="Status1" runat="server" CssClass="status" ImagesPath="Images" JavascriptPath="Script"
			            RecentTransactionItemCount="5" InProgressPagingSize="8" HistoryPagingSize="8" />
		</form>
	</body>
</HTML>
