using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Server.InterviewSystem;
using Perfectus.Server.WebUI.Library.Interview;

namespace Perfectus.Server.WebUI.Interfaces
{
    /// <summary>
    /// Summary description for Interview.
    /// </summary>
    public partial class Interview : System.Web.UI.Page
    {
        protected void Page_Load( object sender, EventArgs e )
        {
            if( !Page.IsPostBack )
            {
                // Parse the query string
                // This page expects querystrings to look like:
                //
                // interview.aspx?EasyStart/packageId/version/revision
                // or   interview.aspx?EasyResume/instanceId
                // start, s, resume and r are also valid modes.
                //
                // or   interview.aspx?EasyPageResume/instanceId/pageId
                // or   interview.aspx?EasyResumeNext/instanceId/pageId
                // or   interview.aspx?EasyResumeNext/instanceId
                // or   interview.aspx?EasyResumePrevious/instanceId

                string q = Server.UrlDecode( Request.QueryString.ToString( ) );
                string[ ] parts = q.Split( '/' );

                if( parts.Length < 2 )
                {
                    throw new Exception( "Invalid EasyStart or EasyResume URL." );
                }

                string mode = parts[ 0 ].ToLower( );
                string id = parts[ 1 ];
                string instanceIdStr;
                Guid instanceId;

                if( mode == "easystart" || mode == "start" || mode == "s" )
                {
                    if( parts.Length < 4 )
                    {
                        throw new Exception( "Invalid EasyStart or EasyResume URL." );
                    }
                    string packageIdStr;
                    packageIdStr = id;
                    string verStr = parts[ 2 ];
                    string revStr = parts[ 3 ];

                    int ver = Convert.ToInt32( verStr );
                    int rev = Convert.ToInt32( revStr );
                    Guid packageId = new Guid( packageIdStr );
                    instanceId = Perfectus.Server.InterviewSystem.InstanceManagement.CreateInstance( packageId, ver, rev );
                    string newQ = string.Format( "{0}?r/{1:N}", Request.Path, instanceId );
                    Response.Redirect( newQ );
                }
                else if( mode == "easyresume" || mode == "resume" || mode == "r" )
                {
                    instanceIdStr = id;
                    instanceId = new Guid( instanceIdStr );
                    iui.PackageInstanceId = instanceId;
                    iui.ResumeInstance( );
                    InstanceManagement.ResumeInstance( instanceId );
                    iui.ForceRefresh = true;
                    iui.DataBind( );
                }
                else if( mode == "easypageresume" )
                {
                    // This makes a new instance of the interview and pushes a page to the top of the stack so that
                    // when the new instance is resumed it will go the the specified page.

                    instanceIdStr = id;
                    instanceId = new Guid( instanceIdStr );
                    string resumePageIdStr = parts[ 2 ];
                    Guid resumePageId = new Guid( resumePageIdStr );
                    Guid newInstanceId = Guid.NewGuid( );
                    InstanceManagement.CopyPackageInstance( newInstanceId, instanceId, Identity.GetRunningUserId( ) );
                    Package instance = InstanceManagement.GetInstance( newInstanceId );
                    InterviewPage page = instance.InterviewPageByGuid[ resumePageId ];
                    InstanceManagement.NavigationStackPush( newInstanceId, resumePageId, page.Name );
                    Response.Redirect( String.Format( "interview.aspx?easyresume/{0}", newInstanceId ) );
                }
                else if( mode == "easyresumenext" )
                {
                    // Resumes an easy interview instance, but on the next page to a specified page. Useful for authors of external pages.
                    instanceIdStr = id;
                    instanceId = new Guid( instanceIdStr );

                    // If a refferer page is specified force it to be at top of stack. (If it's not already)
                    if( parts.Length > 2 )
                    {
                        string reffererPageIdStr = parts[ 2 ];
                        Guid reffererPageId = new Guid( reffererPageIdStr );

                        Guid peekId;
                        InstanceManagement.NavigationStackPeek( instanceId, out peekId );

                        if( reffererPageId != peekId )
                        {
                            Package instance = InstanceManagement.GetInstance( instanceId );
                            InterviewPage page = instance.InterviewPageByGuid[ reffererPageId ];
                            InstanceManagement.NavigationStackPush( instanceId, reffererPageId, page.Name );
                        }
                    }

                    iui.PackageInstanceId = instanceId;
                    iui.SuspendExternalRedirect = true; // stops externalPage redirecting before the DoNext Call.
                    iui.DataBind( );
                    iui.SuspendExternalRedirect = false;
                    iui.DoNext( );

                    //we change to a resume URL so that when refresh is clicked we dont keep adding pages to the stack
                    RedirectToResumeUrl( instanceId );
                }
                else if( mode == "easyresumeprevious" )
                {
                    // Resumes an easy interview instance on the previous page. Useful for authors of external pages.
                    instanceIdStr = id;
                    instanceId = new Guid( instanceIdStr );
                    iui.PackageInstanceId = instanceId;
                    InstanceManagement.NavigationStackPop( instanceId );
                    //Package instance = InstanceManagement.GetInstance(instanceId);
                    iui.DataBind( );

                    //we change to a resume URL so that when refresh is clicked we dont keep trying to remove pages to the stack
                    RedirectToResumeUrl( instanceId );
                }
                else
                {
                    throw new Exception( "Invalid EasyStart or EasyResume URL." );
                }
            }
        }

        /// <summary>
        /// We overide the validation routine to sync validation controls server side with the client side effects. FB355
        /// </summary>
        public override void Validate( )
        {
            // Extract string of validations that have been disable client side through visibility changes.
            String disabledValidations = iui.ValidationDHTMLState.Value;

            // Scan through each validation and check whether we must disable them server side so that 
            // we're in sync with what happened client side.
            foreach( IValidator val in Validators )
            {
                BaseValidator baseVal = val as BaseValidator;

                if( ( baseVal == null ) || ( baseVal.Enabled == false ) )
                    continue;

                // If this validator is listed, we disable it before validation is performed.
                if( disabledValidations.Contains( baseVal.ClientID ) )
                    baseVal.Enabled = false;
            }

            base.Validate( );
        }


        private void RedirectToResumeUrl( Guid instanceId )
        {
            string newQ = string.Format( "{0}?r/{1:N}", Request.Path, instanceId );
            Response.Redirect( newQ );
        }

        #region Web Form Designer generated code
        override protected void OnInit( EventArgs e )
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent( );

            //GetUserConfigAndPrefs();

            base.OnInit( e );
        }


        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.iui.Finished += new EventHandler<InterviewFinishedEventArgs>( this.iui_Finished );
        }
        #endregion

        private void iui_Finished( object sender, InterviewFinishedEventArgs e )
        {
            if( e.DestinationUrl != null && e.DestinationUrl.Trim( ).Length > 0 )
            {
                Response.Redirect( e.DestinationUrl );
            }
            else
            {
                Response.Redirect( "status.aspx" );
            }
        }
    }
}
