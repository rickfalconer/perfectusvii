<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailLogger.aspx.cs" Inherits="Perfectus.Server.WebUI.Interfaces.EmailLogger" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html xmlns="http://www.w3.org/1999/xhtml">
	<head id="Head1" runat="server">
		<title>Interview Email Logger</title>
		<link href="Styles/default2.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<form id="Form1" method="post" runat="server" enableviewstate="false">
            <table cellpadding="5" cellspacing="5" border="0" id="TABLE1" style="width: 700px">
                <tr>
                    <td colspan="3"><h1>Perfectus Interview Email Exception Logger</h1></td>
                </tr>
                <tr>
                    <td colspan="3">
                        This page can be used to test the Email Exception Logging settings.<br />
                        The default values have been taken from the Perfectus.Server.Shared.config file.
                        These settings can then be changed to test different settings.<br />
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><h2>Email Settings</h2></td>
                </tr>
                <tr>
                    <td width="200">To</td>
                    <td><asp:TextBox ID="ToTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td width="150">&nbsp;</td>
                </tr>
                <tr>
                    <td>From</td>
                    <td><asp:TextBox ID="FromTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Subject</td>
                    <td><asp:TextBox ID="SubjectTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>SMTP Server</td>
                    <td><asp:TextBox ID="SMTPServerTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><h2>Exception Details</h2></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td><asp:TextBox ID="TitleTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>UI Message</td>
                    <td><asp:TextBox ID="UIMessageTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Message</td>
                    <td><asp:TextBox ID="MessageTextBox" runat="server" Width="400px"></asp:TextBox></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="right"></td>
                    <td>&nbsp;<asp:Button ID="SubmitButton" runat="server" Width="70" Text="Submit" OnClick="SubmitButton_Click" /></td>
                </tr>
                <tr runat="server" id="ExceptionTitleRow">
                    <td colspan="3"><h2><font color="red">Exception</font></h2></td>
                </tr>
                <tr runat="server" id="ExceptionDetailsRow">
                    <td colspan="3"><asp:Label ID="ExceptionDetailsLabel" runat="server" Width="650px" /></td>
                </tr>
            </table>
            <br />
		</form>
	</body>
</html>