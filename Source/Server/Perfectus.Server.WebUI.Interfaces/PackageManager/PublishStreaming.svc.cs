﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Activation;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.Common;
using Perfectus.Common;

namespace Perfectus.Server.WebUI.Interfaces.PackageManager
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PublishStreaming : IPublishStreaming
    {
        #region IPublishStreaming Members

            /// <summary>
        ///		Default constructor.
        /// </summary>
        public PublishStreaming ()
        {
        }

        /// <summary>
        ///		Accepts a serialised Package (typically with Templates' WordML serialised in-line) as a gzip-compressed MTOM attachment.
        /// </summary>
        /// <param name="newVersion">If true, the server will increment the package's version number.</param>
        /// <param name="versionNumber">[out] The version number assigned to the newly published package.</param>
        /// <param name="revisionNumber">[out] The revision number assigned to the newly published package.</param>
        /// <param name="zippedPackageBytes">A byte array containing a zipped package.</param>
        /// <example>
        /// <code>
        /// string serverWebApiBaseUrl = "http://localhost/perfectus.server.webApi";
        /// 
        /// // Publish the package
        /// pkg.PublishedBy = WindowsIdentity.GetCurrent().Name;
        ///
        /// // Compress the package attachemt using SharpZipLib's gzip (v0.80)
        /// MemoryStream memoryStream = new MemoryStream();
        /// GZipOutputStream gzip = new GZipOutputStream(memoryStream);
        /// 
        /// // By specifying 'CrossMachine' any Templates will also be serialsed.
        /// Package.SaveToStream(pkg, gzip, StreamingContextStates.CrossMachine);
        /// gzip.Finish();
        /// PublishWebApi.PublishingWse publisher = new PublishWebApi.PublishingWse();
        /// 
        /// publisher.Url = string.Format("{0}/PackageManager/publishing.asmx", serverWebApiBaseUrl);
        /// 
        /// publisher.Credentials = CredentialCache.DefaultCredentials;
        /// publisher.RequestSoapContext.Attachments.Add(da);
        /// 
        /// // Publish it to folder '1', and increment the version number if it's already present
        /// publisher.Publish_MTOM_Compressed(new int[] {1}, true, zippedPackageBytes);
        /// </code>
        /// </example>
        /// <remarks>
        /// When called, the client must also have the WSE 3.0 runtime installed.  
        /// To generate the appropriate WSE proxy objects from this service's WSDL, the WSE 3.0 developer tools must be installed.
        /// </remarks>
        /// 

        // Streaming using WSE allows only one parameter (the stream itself)
        public PublishResult Publish_Wse_MTOM(Stream packageStreamData)
        {
            return PublishInternal(packageStreamData, false);
        }

        public PublishResult Publish_Wse_MTOM_NewVersion(Stream packageStreamData)
        {
            return PublishInternal(packageStreamData, true);
        }

        private PublishResult PublishInternal(Stream packageStreamData, bool newVersion)
        {
            int versionNumber;
            int revisionNumber;
                    
            MemoryStream zippedPackageBytesStream = new MemoryStream();
            
            int size = 0;
            int bytesRead = 0;
            byte[] buffer = new byte[1024];

            // Read all the data from the stream
            do
            {
                bytesRead = packageStreamData.Read(buffer, 0, buffer.Length);
                zippedPackageBytesStream.Write(buffer, 0, bytesRead);
                size += bytesRead;
            } while (bytesRead > 0);
            packageStreamData.Close();
            zippedPackageBytesStream.Position = 0;


            if (zippedPackageBytesStream.Length == 0)
            {
                throw new FaultException(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("NullZippedPackage"));
            }
            else
            {
                Package p = null;
                try
                {
                    // CrossMachine = deserialise all the templates as in-line byte[], rather than out-of-band in the temp folder
                    p = Package.OpenFromStream(zippedPackageBytesStream, StreamingContextStates.CrossMachine);
                    API.PackageManagement.Publishing.Publish(p, newVersion, out versionNumber, out revisionNumber);

                    PublishResult result = new PublishResult();
                    result.VersionNumber = versionNumber;
                    result.RevisionNumber = revisionNumber;
                    return result;
                }
                catch (InvalidOperationException ioex)
                {
                    Guid packageId = Guid.Empty;
                    if (p != null)
                    {
                        packageId = p.UniqueIdentifier;
                    }

                    Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageError"), ioex, Guid.Empty, packageId, int.MinValue, int.MinValue, int.MinValue, Logging.ExceptionLogSeverityCode.ERROr, true);
                    throw new FaultException(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageCannotBePublished"));

                }
                catch (Exception ex)
                {
                    Guid packageId = Guid.Empty;
                    if (p != null)
                    {
                        packageId = p.UniqueIdentifier;
                    }

                    Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("PackageError"), ex, Guid.Empty, packageId, int.MinValue, int.MinValue, int.MinValue, Logging.ExceptionLogSeverityCode.ERROr, true);
                    throw new FaultException(ex.Message,new FaultCode("Perfectus Publishing Engine"));
                }
            }
        }


        #endregion
    }

}
