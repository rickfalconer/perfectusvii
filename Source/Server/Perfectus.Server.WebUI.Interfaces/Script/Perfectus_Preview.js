function PopulateIframeDocument(myFrame, theContentField)
{
	var htmlString = new String(theContentField.value);
	var rExp1 = /&lt;/gi;
	var rExp2 = /&gt;/gi;
	var rExp3 = /&quot;/gi;
	var rExp4 = /&amp;gt;/gi;
	var rExp5 = /&amp;lt;/gi;
	var rExp6 = /&amp;&amp;/gi;
	
	var htmlString1 = htmlString.replace(rExp1,'<');
	var htmlString2 = htmlString1.replace(rExp2,'>');
	var htmlString3 = htmlString2.replace(rExp3,'"');
	var htmlString4 = htmlString3.replace(rExp4,'>');
	var htmlString5 = htmlString4.replace(rExp5,'<');
	var htmlString6 = htmlString5.replace(rExp6,'&&');
	
	if (myFrame.contentDocument)
	{
		myFrame.contentDocument.write(htmlString6);
		myFrame.contentDocument.close();
	}
	else if (myFrame.contentWindow && myFrame.contentWindow.document)
	{
		myFrame.contentWindow.document.write(htmlString6);
		myFrame.contentWindow.document.close();
	}
	theContentField.value = '';
	htmlString = '';
	htmlString1 = '';
	htmlString2 = '';
	htmlString3 = '';
	htmlString4 = '';
	htmlString5 = '';
	htmlString6 = '';
}

function ReloadIfram (myFrame, srcURL) {
myFrame.src = srcURL;
}

