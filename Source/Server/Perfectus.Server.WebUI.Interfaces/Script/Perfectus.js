function validateLength(source, args)
{
	var tb = document.getElementById(source.controltovalidate);
	var maxLength = tb.getAttribute('maxlength');
	if (tb.value.length <= maxLength)
	{
		args.IsValid = true;
	}
	else
	{
		args.IsValid = false; 
	}
}

function toggleDisplay(control, previewText, interviewText)
{
	if(control)
	{
	    // Toggle the preview / interview display
		if (control.style.display == 'none')
		{
			control.style.display = '';
		}
		else
		{
			control.style.display = 'none';
		}
	}
}

function togglePreviewAnchorText(control, previewText, interviewText)
{
    if(control)
    {
        // Toggle the preview buttons links text
		if( gettext( control ) == previewText)
		    control.innerHTML = interviewText;
		else
		    control.innerHTML = previewText;    
	}
}

function validateUploadExtension(source, args)
{
	var fu = document.getElementById(source.controltovalidate);
	var path = fu.value;
	var extPos = path.lastIndexOf('.');
	var ext;
	if (extPos > 0 && extPos < path.length)
	{
		ext = path.substr(extPos + 1, path.length-extPos).toLowerCase();
	}
	else
	{
		ext = '';
	}

// pf-3031 3086 remove pdf doc xml rtf jpe from upload, however need to keep them here for use with attachments
//    switch (ext)
//	{
//	    case "xml":
//	    case "xslx":
//	    case "doc": 
//		case "pdf":
//        case "docx":
//		case "rtf":
//		case "png":
//		case "jpg":
//		case "jpeg":
//		case "jpe":
//		case "gif":
//		case "tif":
//		case "tiff":
//		case "bmp":
//			args.IsValid = true;
//			break;
//		default:
//			args.IsValid = false;
//			break;
//	}	
    args.IsValid = true;
} 

function validateUploadXmlExtension(source, args)
{
	var fu = document.getElementById(source.controltovalidate);
	var path = fu.value;
	var extPos = path.lastIndexOf('.');
	var ext;
	if (extPos > 0 && extPos < path.length)
	{
		ext = path.substr(extPos + 1, path.length-extPos).toLowerCase();
	}
	else
	{
		ext = '';
	}
	
	switch(ext)
	{
		case "xml":
			args.IsValid = true;
			break;
		default:
			args.IsValid = false;
			break;
	}	
}
 
 
 function BindChangeEvents(controls)
 {	
	var i;
	for(i = 0; i<controls.length; i++)
	{
		var qf = controls[i];
		if (qf)
		{
			var fieldId = qf.getAttribute('fieldid');
			var field = document.getElementById(fieldId);
			InterviewHookupControl(field, qf);
		}
	}
	
	if (pageIsPostBack)
	{
	KeepStringBindingOnPostBack(controls);
	}
	
	var ev = window.onload;
    if (typeof(ev) == "function" ) 
    {            
        ev = ev.toString();
        ev = ev.substring(ev.indexOf("{") + 1, ev.lastIndexOf("}"));
    }
    else 
    {
        ev = "";
    }
    var func = new Function("if (typeof(UpdateDhtml) != 'undefined'){UpdateDhtml();}" + ev);
    
	window.onload = func;

 }
 
 function InterviewHookupControl(control, qf) 
 {
    
    if (control)
    {
		if (typeof(control.tagName) == "undefined" && typeof(control.length) == "number") 
		{
			for (var i = 0; i < control.length; i++) 
			{
				var inner = control[i];
				if (typeof(inner) != 'undefined')
				{
					if (typeof(inner.value) == "string") 
						InterviewHookupControl(inner, qf);
				} 
			}
			return;
		}
		else if (control.tagName != "INPUT" && control.tagName != "TEXTAREA" && control.tagName != "SELECT") 
		{
			for (var i = 0; i < control.childNodes.length; i++) 
				InterviewHookupControl(control.childNodes[i], qf);
			return;
		}
		else 
		{
            // Determine the event we want to append to.
		    var ev;
		    if ((control.type == "radio") || (control.type == "checkbox")) 
			    ev = control.onclick;
            else
                ev = control.onchange;        

            // Check its an existing event function
		    if (typeof(ev) == "function") 
		    {            
			    ev = ev.toString();
			    ev = ev.substring(ev.indexOf("{") + 1, ev.lastIndexOf("}"));
		    }
		    else 
			    ev = "";

            // Determine the function we want to call  in the event      
            var funcStr = ev + "; InterviewOnChange(this, document.getElementById('" + qf.id + "')); ";
            
            // Construct the function
		    var func;
		    if (navigator.appName.toLowerCase().indexOf('explorer') > -1) 
                func = new Function(funcStr);
            else 
                func = new Function("event", funcStr);

            // Attach function to event
		    if((control.type == "radio") || (control.type == "checkbox"))
			    control.onclick = func;
		    else 
			    control.onchange = func;

			// Init the answer vars at the same time
			var questionId = qf.getAttribute('answers');
			var answer = InterviewGetValue(control);
			if (eval('typeof(answerTo' + questionId + ')') == 'undefined')
			{
				var val = 'answerTo' + questionId + ' = new Array()';
				eval(val); 
			}
			if (! qf.inited)		
			{
				SetAnswerVar(questionId, qf, answer, -1);
				qf.inited = true;
			}
		}
    }
}

// Generic function for retrieving text (FB2440)
function gettext( e )
{
    if( typeof(e.textContent) != 'undefined' )
        return e.textContent;
    return e.innerText;
}

// Manual trigger of javascript  event (FB2081)
function triggerEvent(element, eventName) 
{ 
    if (document.createEventObject)
    {
        // Trigger for IE
        var evt = document.createEventObject( );
        return element.fireEvent( 'on' + eventName, evt );
    }
    else
    {
        // Trigger for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(eventName, true, true ); // type,bubbling,cancellable
        return !element.dispatchEvent( evt );
    }
} 

function InterviewOnChange(src, qf)
{
	var fs = qf.getAttribute('fs');
	if (fs != null)
	{
	    var fsp = qf.getAttribute('fp');
	    var pdt = qf.getAttribute('pdt');
	    var aid = qf.getAttribute('answers');
	    var context = new Array(2);
	    context[0] = src;
	    context[1] = qf;
	    Format(src.value, fs, fsp, pdt, context); 
	}
	else
	{
	    UpdateDefaultStringBinding(src, qf);    
	    if(typeof(UpdateQueryDhtml)=='function')
        {		
	        UpdateQueryDhtml(src, qf);
        }	
	}
	
	if(typeof(UpdateDhtml)=='function')
	{		
		UpdateDhtml();
	}		
}


function ChangePromptRef( src, qf, id, newxmlInfo )
{
    var items = document.getElementsByTagName("span");	
    var i;
	for(i = 0; i < items.length; i++)
	{			
		var itemId = items[i].getAttribute('promptofquestion');	
		if ( itemId != id )
		    continue;
		    
        var strAnswer = EvalDefaultBindingString(newxmlInfo, 0, 0, "text");		
        
        if(document.all){
		    items[i].innerText = strAnswer;
		    }
		    else
		    {
		    items[i].textContent = strAnswer;
		}
		
		break;
    }
}

function ChangeDefaultAnswerRef ( src, qf, id, newxmlInfo )
{
    // We want to update exactly one question with id == GUID(Question)
    
    var items = document.getElementsByTagName("span");	
	var qfRepeat = qf.repeat;
	
	var updatedId = qf.getAttribute('answers');	
	var updatedRepeat = qf.getAttribute('repeat');
	var updatedInRepeater = qf.getAttribute('inRepeater') == "true";

    var i;
	for(i = 0; i < items.length; i++)
	{			
		var itemId = items[i].getAttribute('answers');	
		if ( itemId != id )
		    continue;
		
		var xmlInfo = items[i].getAttribute('defaultAnswersXml');
		items[i].setAttribute('defaultAnswersXml', newxmlInfo);
		
		// We do want to update the question if
		// 1. not the question itself caused the fireing of the event
		// 2. the old default string != new default string.
		//    Note that any arbitrary question can change the query result, hence we cannot test for the 'updatedId'
		// 2. the question that fired the event is actually listed as a source in the default-answer-xml
		//
		// otherwise we bugger off
		
		//1.
		if ( itemId == updatedId)
		    break;
		//2.
		var want_to_update = want_to_update = xmlInfo != newxmlInfo;
		//3.
		if ( ! want_to_update )
		    want_to_update = newxmlInfo.indexOf(updatedId) > -1;
		
		// Nothing more to do... bugger off
		if ( ! want_to_update )
		    break;
		
		var itemId = items[i].getAttribute('answers');	
		var repeat = items[i].getAttribute('repeat');
		var boundInRepeater = items[i].getAttribute('inRepeater') == "true";
        var matchOnRepeat = (updatedInRepeater && boundInRepeater);
		var pdt = qf.getAttribute('pdt');
		
		if((!matchOnRepeat) || (repeat == updatedRepeat)) // only want to update if question that has been updated is in the items binding xml.
		{
	        // evaluate
	        var strAnswer = EvalDefaultBindingString(newxmlInfo, qfRepeat, boundInRepeater, pdt);					
    								
	        inputTags = items[i].getElementsByTagName("input");			
    		
	        if(inputTags != null && inputTags.length > 0)
	        {						
		        inputTags[0].value = strAnswer;
		        UpdateDefaultStringBinding(inputTags[0],items[i]);
	        }
		}				
		break;
	}
}

function Format(answer, fs, fsp, pdt, context)
{
    Perfectus.Server.WebUI.Interfaces.ScriptServices.FormatAnswer(answer, fs, fsp, perfectus_instanceId, pdt, Format_Success, Format_Error, context);
}

function Format_Success(result, context)
{
    //pf-3034
    // some formatted values returned via JSON are not simple strings, but objects
    // this changed in asp.net 3.5 
    // http://encosia.com/a-breaking-change-between-versions-of-aspnet-ajax/#comment-34045
    if (typeof (result) == "string") {
        context[0].value = result;
    }
    else {
        if (result.d) {
            context[0].value = result.d;
        }
    }

    UpdateDefaultStringBinding(context[0], context[1]); 
    
    for (i = 0; i < context[0].Validators.length; i++) 
    {
        ValidatorValidate(context[0].Validators[i], '', null)
        ValidatorUpdateDisplay(context[0].Validators[i])
    }
    
    if(typeof(UpdateDhtml)=='function')
	{		
		UpdateDhtml();
	}   
}

function Format_Error()
{
    alert('An error occurred while reformatting the answer.');
}

function UpdateDefaultStringBinding(src, qf)
{
	var questionId = qf.getAttribute('answers');		
	var qfRepeat = qf.repeat;
	var answer = InterviewGetValue(src);
	
	SetAnswerVar(questionId, qf, answer, qfRepeat);
	
	// qf is question field that has been updated. Loop through each span and see
	// if we need to do any updates (if the spans defaultAnswerXml knows about qf).
	
	var items = document.getElementsByTagName("span");	
	var updatedId = qf.getAttribute('answers');	
	var updatedRepeat = qf.getAttribute('repeat');
	var updatedInRepeater = qf.getAttribute('inRepeater') == "true";

	var i;
	for(i = 0; i < items.length; i++)
	{			
		var xmlInfo = items[i].getAttribute('defaultAnswersXml');
		var itemId = items[i].getAttribute('answers');	
		var repeat = items[i].getAttribute('repeat');
		var boundInRepeater = items[i].getAttribute('inRepeater') == "true";
				
        var matchOnRepeat = (updatedInRepeater && boundInRepeater);
								
		var pdt = qf.getAttribute('pdt');
		if(xmlInfo != null && xmlInfo.indexOf(updatedId) > -1 && itemId != updatedId && ((!matchOnRepeat) || (repeat == updatedRepeat))) // only want to update if question that has been updated is in the items binding xml.
		{ 		
			// evaluate
			var strAnswer = EvalDefaultBindingString(xmlInfo, qfRepeat, boundInRepeater, pdt);					
									
			inputTags = items[i].getElementsByTagName("input");			
			
			if(inputTags != null && inputTags.length > 0)
			{						
				inputTags[0].value = strAnswer;
				UpdateDefaultStringBinding(inputTags[0],items[i]);
			}				
		}		
	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
	
	RefreshBoundSpans(updatedId, updatedInRepeater, updatedRepeat, pdt);	
}

function RefreshBoundSpans(updatedId, updatedInRepeater, updatedRepeat, pdt)
{
	var items = document.getElementsByTagName("span");	

	// Update any text areas with bound spans
	for (i = 0; i < items.length; i++)
	{
	    var uid = items[i].getAttribute('uId');
	    if (uid == null)
	    {
	        continue;
	    }
	    var boundInRepeater = items[i].getAttribute('inRepeater') == "true";
	    var matchOnRepeat = updatedInRepeater && boundInRepeater;
	    var repeat = items[i].getAttribute('repeat');
	    var danswer = items[i].getAttribute("dAnswer");
	    if (uid == "uId" + updatedId && ((!matchOnRepeat) || (repeat == updatedRepeat)))
	    {
	        var answer = eval('answerTo' + updatedId);
	        var strAnswer = null;
            if (updatedInRepeater && boundInRepeater)
            {
                strAnswer = AnswerToDisplayString(answer[updatedRepeat][0], pdt);	    
            }
            else if (updatedInRepeater && !boundInRepeater)
            {
                strAnswer = ConcatAnswersForDisplay(answer, pdt);
            }		
		    else if (!updatedInRepeater && boundInRepeater)
		    {
		        strAnswer = AnswerToDisplayString(answer[0][0], pdt);
		    }
		    else
		    {
		        try
		        {
		        strAnswer = AnswerToDisplayString(answer[0][0], pdt);
		    }
		        catch (e)
		        {
		            strAnswer = "";
		        }
		    }
		    
		    if(strAnswer == null || strAnswer == 'undefined')
		    {
		        strAnswer = danswer;			
		    }			
	        
	        items[i].innerHTML = strAnswer;
	    }
	}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

}

function EvalDefaultBindingString(xmlBody, repeat, boundInRepeater, pdt)
{
	if(xmlBody == null || xmlBody.indexOf("<text>") == -1)
	{	
		return xmlBody;
	}	
	
	var maxCount = 50;	
	var count = 0;
	
	while(xmlBody.indexOf("</item>") > -1 && count < maxCount)
	{
		count ++;
		
		// All this indexOf business is because of the difficulty using JavaScript to
		// make an an XmlDocument and loading a string into it. Would of been much easier if we had
		// a DOM to parse, but making a cross-browser xmlDocument object is a pain. We need to be
		// careful that if the item tag this method expects ever gets a new attribute (it probably never will) 
		// that nothing breaks.
		
		po1 = xmlBody.indexOf("<item");
		po2 = xmlBody.indexOf("</item>");		

		strItem = xmlBody.substring(po1, po2 +7);
		var uIdPos = strItem.indexOf("uId=\"") + 5;
		var uId = strItem.substring(uIdPos, strItem.indexOf("\"", uIdPos));
		var hasFieldId = strItem.indexOf("fieldId=\"") >= 0;
		
		var fieldIdStartPos = strItem.indexOf("fieldId=\"") + 9;
		var fieldId = strItem.substring(fieldIdStartPos, strItem.indexOf("\"", fieldIdStartPos));
		var strDisplay = strItem.substring(strItem.indexOf("\">")+2, strItem.indexOf("</item>"));
		var answer = eval("answerTo" + uId);
		var updatedInRepeater = answer.length > 1;
		
        var strAnswer;
        
        if (updatedInRepeater && boundInRepeater)
        {
            strAnswer = AnswerToDisplayString(answer[repeat][0]);
        }
        else if (updatedInRepeater && !boundInRepeater)
        {
            strAnswer = ConcatAnswersForDisplay(answer, pdt);
        }		
		else if (!updatedInRepeater && boundInRepeater)
		{
		    strAnswer = AnswerToDisplayString(answer[repeat][0], pdt);
		}
		else
		{
		    strAnswer = AnswerToDisplayString(answer[repeat][0], pdt);
		}

		if(strAnswer == null)
		{
		    strAnswer = strDisplay;			
		}			
        else if (hasFieldId && strAnswer.indexOf("<field id='" + fieldId + "'") >= 0)
        {
            var fieldStartPos = strAnswer.indexOf("<field id='" + fieldId + "'>") + 11 + fieldId.length + 2;
            strAnswer = strAnswer.substring(fieldStartPos, strAnswer.indexOf("</field>", fieldStartPos));
            strAnswer = strAnswer.replace(/&lt;/g, '<');
            strAnswer = strAnswer.replace(/&gt;/g, '>');
        }
        
		xmlBody = xmlBody.replace(strItem, strAnswer);	  
	}

	xmlBody = xmlBody.replace("<text>","");
	xmlBody = xmlBody.replace("</text>","");

	return xmlBody;
}

function ConcatAnswersForDisplay(answer, pdt)
{
    var strAnswer = "";
    var comma = "";
    var i;
    for (i = 0; i < answer.length; i++)
    {
        var thisAnswer = answer[i][0];
        if (thisAnswer != null)
        {
            strAnswer += comma + AnswerToDisplayString(thisAnswer, pdt);
        }
        comma = ", ";
    }
    return strAnswer;
}

function AnswerToDisplayString(answer, pdt)
{
    if (pdt == 'date')
    {
        var i;
        try
        {
            if (answer != null)
            {
            i = parseInt(answer.toString());
            var d = new Date();
            d.setTime(i);
            var day = d.getDate();
            var month = d.getMonth() + 1; // month is zero-based
            var year = d.getFullYear();
            switch(dateOrder)
            {
                case 'dmy':
                    return day + '/' + month + '/' + year;
                case 'mdy':
                    return month + '/' + day + '/' + year;
                case 'ymd':
                    return year + '/' + month + '/' + day;
                default:
                    return d.toString();
            }
        }
            else
            {
                return null;
            }
        }
        catch (e)
        {
            if (answer == null)
            {
                return null;
            }
            else
            {
            return answer.toString();
        }
    }
    }
    else
    {
        try
        {
            return answer.toString();
        }
        catch (e)
        {
            return '';
        }
    }
}

function SetAnswerVar(questionId, qf, answer, repeat)
{
		if (repeat == -1) // init
		{
			//if(eval('answerTo' + questionId).length == 0) // temp fix, to fix problem with answers been pushed onto array twice when previous button hit.
			//{	
				var val = 'answerTo' + questionId + '.push(' + AnswerToString(answer, qf) + ');';
				eval(val);
			//}		
				
			qf.repeat = eval('answerTo' + questionId).length - 1;
		}
		else
		{			
		    var val = 'answerTo' + questionId;
		    var answerArray = eval(val);
		    if (repeat > answerArray.length - 1)
		    {
		        val = 'answerTo' + questionId + '.push(' + AnswerToString(answer, qf) + ');';
		    }
		    else
		    {
			    val = 'answerTo' + questionId + '[' + repeat + '] = ' + AnswerToString(answer, qf) + ';';
			}
            
            eval(val);
		}
}

function AnswerToString(answer, qf)
{
	var retVal;
	retVal = '[';
	
	var comma = '';
	var i;
	
	if (answer == null)
	{
	    return null;
	}
	
	for (i = 0; i < answer.length; i++)
	{
		retVal += comma;
		var v;
		
		if (answer[i].length == 0)
		{
			retVal += "null";
		}
		else
		{			
			switch(qf.getAttribute('pdt'))
			{
				case "date":
				    try
				    {
					v = StringToDate(answer[i]).getTime();	
					}
					catch (e)
					{
					    v = "";
					}				    
					break;
				case "number":
				    
				    v = answer[i];
				    
				    //make sure our number is valid for the current culture (using .Net validators)
				    try
				    {
				        for (j = 0; j < Page_Validators.length; j++) 
                        {
                            if (Page_Validators[j].controltovalidate == qf.getAttribute('fieldid'))
                            {
                                ValidatorValidate(Page_Validators[j], '', null);
                                ValidatorUpdateDisplay(Page_Validators[j]);
                                
                                if (Page_Validators[j].isvalid == false)
                                {
                                    v = null;
                                    break;
                                }
                            }
                        }
                    }
                    catch(err)
                    {
                        //ignore
                    }
				    
				    //if our validation did not set v to null and we have a valid answer, make sure there are no thousands seperators in the number string so that JS evals work
				    if (v != null)
				    {
	                    v = getEvalFriendlyFloat(v);
					}
					    
					break;
				case "file":
				case "text":
					if (answer[i] != null)
					{
					v = "'" + answer[i].replace(/'/g, "\\'") + "'";
					}
					else
					{
					    v = "null";
					}
					break;
				case "yesno":
					v = answer[i];
					break;
			}
			
			// Because we may have fired before validation (firefox, especially), make sure the answer isn't rubbish.
			try
			{
				eval(v);
			}
			catch(e)
			{
				v = "null";
			}
			
			retVal += v;
		}
		comma = ',';
	}
	retVal += ']';
	return retVal;
}

function InterviewGetValue(control) 
{
	if (control.type == 'select-multiple')
	{		
		var retVal = new Array();
		var i;
		for(i = 0; i < control.options.length; i++)
		{
			if (control.options[i].selected)
			{
				retVal.push(control.options[i].value);
			}
		}		
		return retVal;
	}
	
	if ((control.type == 'checkbox' || control.type == 'radio') && typeof(control.value) == 'string')
	{
		var retVal = new Array();
		var parts = control.id.split('_');
		parts.pop();
		var i = 0;
		parts.push(i);
		var id = parts.join('_');

		if (! document.getElementById(id))
		{
			retVal.push(control.checked);
		}
		else
		{
			while(document.getElementById(id))
			{
				var c = document.getElementById(id);
				if (c.checked)
				{
					retVal.push(c.value);
				}

				parts.pop();
				i++;
				parts.push(i);
				id = parts.join('_');
			}
		}
		return retVal;		
	}
	
    if (typeof(control.value) == "string") 
    {        		
        return new Array(control.value);
    }
    if (typeof(control.tagName) == "undefined" && typeof(control.length) == "number") 
    {
        var j;
        for (j = 0; j < control.length; j++) 
        {
            var inner = control[j];
            if (typeof(inner.value) == "string" && (inner.type != "radio" || inner.status == true)) 
            {
                return new Array(inner.value);
            }
        }
    }
    else 
    {
        return InterviewGetValueRecursive(control);
    }
    return new Array("");
}

function InterviewGetValueRecursive(control)
{
    if (typeof(control.value) == "string" && (control.type != "radio" || control.status == true)) 
    {
        return new Array(control.value);
    }
    var i;
    var val;
    for (i = 0; i<control.childNodes.length; i++) 
    {
        val = InterviewGetValueRecursive(control.childNodes[i]);
        if (val != "") 
        {
			return new Array(val);
		}
    }
    return "";
}

function GetElements(id)
{
	var rows = document.getElementsByTagName('tr');
	var cells = document.getElementsByTagName('td');
	var els = new Array();
	var i;
	for(i = 0; i < rows.length; i++)
	{
		var attr = rows[i].getAttribute('item');
		if (typeof(attr) == 'string' && attr == id)
		{
			els.push(rows[i]);
		}
	}
	
	for (i = 0; i < cells.length; i++)
	{
		var attr = cells[i].getAttribute('item');
		if (typeof(attr) == 'string' && attr == id)
		{
			els.push(cells[i]);
		}
	}
	return els;
}

function Show( id )
{
    // Get list of table rows & columns that have an 'item' attribute == id.
	var els = GetElements( id );
	for( var i = 0; i < els.length; i++ )
        showRowCell( els[ i ] );
}

function Hide( id )
{
    // Get list of table rows & columns that have an 'item' attribute == id.
	var els = GetElements( id );
	for( var i = 0; i < els.length; i++ )
        hideRowCell( els[ i ] );
}

function showRowCell( ctrl )
{
	if( typeof(ctrl) === 'undefined' || ctrl === null ) return;

    // Show the control
	ctrl.style.display = '';

	// Reset all validations under this row/cell
	var spans = ctrl.getElementsByTagName( "span" );
	for( var i = 0; i < spans.length; i++ )
	{
	    // Validations are identified by their class.
	    if( spans[ i ].className == "validator" )
	    {
	        spans[ i ].enabled = typeof(spans[ i ].$enabled) !== 'undefined' ? spans[ i ].$enabled : true;
            if( typeof(ValidatorUpdateDisplay) !== 'undefined' ) ValidatorUpdateDisplay( spans[ i ] );
   	        trackVal( spans[ i ] );
   		}
	}
}

function hideRowCell( ctrl )
{
	if( typeof(ctrl) === 'undefined' || ctrl === null ) return;

    // Hide the control
	ctrl.style.display = 'none';

	// Disable all validations under this row/cell
	var spans = ctrl.getElementsByTagName( "span" );
	for( var i = 0; i < spans.length; i++ )
	{
	    // Validations are identified by their class
	    if( spans[ i ].className == "validator" ) 
	    {
	        if( typeof(spans[ i ].$enabled) === 'undefined' )
	            spans[ i ].$enabled = typeof(spans[ i ].enabled) !== 'undefined' ? spans[ i ].enabled : true;

            spans[ i ].enabled = false;
	        trackVal( spans[ i ] );
	    }
	}
}

function Required( id )
{
    if( typeof(Page_Validators) !== 'undefined' ) 
    {
        // Enable required field validators for all fields based on the question id.
        var pv = Page_Validators;
        for( var i = 0; i < pv.length; i++ )
        {
            // Is this validator associated with the id and a required field validator?
            if( (pv[ i ].getAttribute('quid') === id) &&  (pv[ i ].getAttribute('required') === 'yes') )
            { pv[ i ].$enabled = true;  pv[ i ].enabled = true; }
        }
    }	
}

function NotRequired( id )
{
    if( typeof( Page_Validators ) !== 'undefined' ) 
    {
        // Disable all validators for all fields based on the question id.
        var pv = Page_Validators;
        for( var i = 0; i < pv.length; i++ )
        {
            // Is this validator associated with the id and a required field validator?
            if( (pv[ i ].getAttribute('quid') === id) &&  (pv[ i ].getAttribute('required') === 'yes') )
            { pv[ i ].$enabled = false; ValidatorEnable( pv[ i ], false ); }
        }
    }	
}

// Keeps track of disabled validations due to visibility changes.
function trackVal( vl )
{
	if( typeof(validationCtrl) === 'undefined' || validationCtrl === null ) return;

    var hvalue = validationCtrl.value;
    var ctrlvalue = vl.id + ';';
    
    if( vl.enabled === true )
        // Remove from list..
        validationCtrl.value = hvalue.replace( ctrlvalue, "" );
    else if( hvalue.indexOf( ctrlvalue ) < 0 )
        // Add to list of disabled validations.
        validationCtrl.value = hvalue + ctrlvalue;
}

function StringToDate(s)
{
	var yearFirstExp = new RegExp("^\\s*((\\d{4})|(\\d{2}))([-/]|\\. ?)(\\d{1,2})\\4(\\d{1,2})\\s*$");
	m = s.match(yearFirstExp);
	var day, month, year;
	if (m != null && (m[2].length == 4 || dateOrder == "ymd")) 
	{
		day = m[6];
		month = m[5];
		year = (m[2].length == 4) ? m[2] : GetFullYear(parseInt(m[3], 10));
	}
	else 
	{
		if (dateOrder == "ymd")
		{
			return null;		
		}						
		
		var yearLastExp = new RegExp("^\\s*(\\d{1,2})([-/]|\\. ?)(\\d{1,2})\\2((\\d{4})|(\\d{2}))\\s*$");
		m = s.match(yearLastExp);
		if (m == null) 
		{
			return null;
		}
		if (dateOrder == "mdy") 
		{
			day = m[3];
			month = m[1];
		}
		else 
		{
			day = m[1];
			month = m[3];
		}
		
		// Get the year
		if (m[5].length == 4)
		{
		    // Four digit year - YYYY
		    year = m[5];
	    } 
	    else
	    {
	        // Work out the 2 digit year (YY) using the default .net rules - YY <= 29 => 20YY, YY > 29 => 19YY
	        var yearValue = parseInt(m[6], 10);
	        var cutOffYear = 2029;
	        var century = 2000;
	        
	        year = (yearValue + parseInt(century)) - ((yearValue + parseInt(century) <= cutOffYear) ? 0 : 100);
	    }
	}
	month -= 1;
	var date = new Date(year, month, day);
	return date;
}
	
function IsArray(a)
{
	return a!= null && a.constructor == Array;
}


function SingleAnswer(o)
{
	var o1 = o[0];
	if (typeof(o1) != 'undefined')
	{
		var o2 = o1[0];
		if (typeof(o2) != 'undefined')
		{
			return o2;
		}
	}
	return null;
}

function Eq(left, right)
{	 	
	if (!left || !right)
	{
		return false;
	}	
	
	// Following if statement means that null and 'not answered' will evaluate correctly for a radiobutton or checkbox list.
	if(left[0][0] == null && right[0][0] == null)
	{
		return true;
	}	
	
	var i;
	for (i = 0; i < left.length; i++)
	{
		for (var j = 0; j < left[i].length; j++)
		{
			for (var k = 0; k < right.length; k++)
			{
				for (var l = 0; l < right[k].length; l++)
				{
					if (simplify(left[i][j]) == simplify(right[k][l]))
					{						
						return true;
					}
				}
			}
		}		
	}
	return false;
}

function simplify(o)
{
	if (typeof(o) == 'object' && o != null)
	{	
		return parseFloat(o);
	}
	else
	{
		if(o == null)
		{
		 return ""; // bug fix, so that a match between null and "", will always evaulate to true.
		}
		
		return o;
	}
}

function Ne(left, right)
{
	// Following if statement means that null and 'not answered' will evaluate correctly for a radiobutton or checkbox list.
	if(left[0][0] == null && right[0][0] == null)
	{			
		return false;
	}	
	
	var i;
	for (i = 0; i < left.length; i++)
	{	var j;	
		for (j = 0; j < left[i].length; j++)
	{		
		    var k;
			for (k = 0; k < right.length; k++)
			{	var l;			
				for (l = 0; l < right[k].length; l++)
				{			
					if (simplify(left[i][j]) == simplify(right[k][l]))
					{
						return false;
					}
				}
			}
		}		
	}
	return true;
}

function Div(left, right)
{
	return SingleAnswer(left) / SingleAnswer(right);
}

function Ge(left, right)
{
	return parseFloat(left) >= parseFloat(right);
}

function Gt(left, right)
{
	return parseFloat(left) > parseFloat(right);
}

function Le(left, right)
{
	return parseFloat(left) <= parseFloat(right);
}

function Lt(left, right)
{
	return parseFloat(left) < parseFloat(right);
}

function Minus(left, right)
{
	return SingleAnswer(left) - SingleAnswer(right);
}

function Mult(left, right)
{
	return SingleAnswer(left) * SingleAnswer(right);
}

function Plus(left, right)
{
	return SingleAnswer(left) + SingleAnswer(right);
}


function KeepStringBindingOnPostBack(controls)
{	
	var i;
	for(i = 0; i<controls.length; i++)
	{
		var qf = controls[i];
		if (qf)
		{
			var fieldId = qf.getAttribute('fieldid');
			var uId = qf.getAttribute('answers');	
			var updatedInRepeater = qf.getAttribute('inRepeater') == "true";
			var repeat = qf.getAttribute('repeat');	
			var pdt = qf.getAttribute('pdt');	
							
			if(uId != null)
			{					
				var tag = "uId" + uId;					
				var strVal = null;				
                RefreshBoundSpans(uId, updatedInRepeater, repeat, pdt);	
			}
		}
	}
}

function DisableButton() 
{
	if (window.event)
	{
	    window.event.srcElement.onclick=DoDisableButton;
    }
} 

function DoDisableButton()
{	
	return false;
}

function disableControl(control)
{
	if(control)
	{
		if (control.enabled == 'true')
		{
			control.enabled = 'false';
		}
		else
		{
			control.enabled = 'true';
		}
	}
}
