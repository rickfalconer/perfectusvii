function PackageShowHideChildren(hdnExpanded, childId, name, imagePlusPath, imageMinusPath, parentImage)
{
	if (hdnExpanded.value == 'true')
		hdnExpanded.value = 'false'
	else
		hdnExpanded.value = 'true'
	SetParentImageSrc(hdnExpanded, imagePlusPath, imageMinusPath, parentImage)
	ExpandContractChildren(hdnExpanded, childId, name)
}

function SetParentImageSrc(hdnExpanded, imagePlusPath, imageMinusPath, parentImage)
{
	if (hdnExpanded.value == 'false')
		{
		parentImage.src = imagePlusPath;
		}
	else
		{
		parentImage.src = imageMinusPath;
		}
}

function ExpandContractChildren(hdnExpanded, childId, name)
{
	var els = GetElements(childId, name);
	for(i=0; i < els.length; i++)
	{
		if (hdnExpanded.value == 'false')
			els[i].style.display='none';
		else
			els[i].style.display='';
	}
}

function GetElements(childId, name)
{
	var rows = document.getElementsByTagName('tr');
	var els = new Array();
	for(var i = 0; i < rows.length; i++)
	{
		var attr = rows[i].getAttribute(name);
		if (typeof(attr) == 'string' && attr == childId)
		{
			els.push(rows[i]);
		}
	}
	
	return els;
}


function SectionShowHideChildren(hdnSection, childId, name, imagePlusPath, imageMinusPath, parentImage)
{
	if (hdnSection.value == 'true')
		hdnSection.value = 'false'
	else
		hdnSection.value = 'true'
	SetSectionImageSrc(hdnSection, imagePlusPath, imageMinusPath, parentImage)
	SectionExpandContractChildren(hdnSection, childId, name)
}

function SetSectionImageSrc(hdnSection, imagePlusPath, imageMinusPath, parentImage)
{
	if (hdnSection.value == 'true')
		{
		parentImage.src = imagePlusPath;
		}
	else
		{
		parentImage.src = imageMinusPath;
		}
}

function SectionExpandContractChildren(hdnSection, childId, name)
{
	var els = GetElements(childId, name);
	for(i=0; i < els.length; i++)
	{
		if (hdnSection.value == 'true')
			els[i].style.display='none';
		else
			{
			//if(childId == "History")
				if(els[i].getAttribute("item") == null)
					els[i].style.display='';
				else
					ExpandChildren(els[i]);
			//else
				//els[i].style.display='';
			}
	}
}


function ExpandChildren(row)
{
	if(row.getAttribute("item") != null)	
	{
		var hiddenFieldId = statusControlId + "_" + row.getAttribute("item");
		if(hiddenFieldId != null)
		{			
			var hiddenField = document.getElementById(hiddenFieldId);
			if(hiddenField != null){
				if(hiddenField.value == "true")
				{
					row.style.display='';
				}
			}			
		}
	}
}

function PackageShowHideChildrenNoImage(hdnExpanded, childId, name, parentLabel)
{
	if (hdnExpanded.value == 'true')
		hdnExpanded.value = 'false'
	else
		hdnExpanded.value = 'true'
	SetParentLabelText(hdnExpanded, parentLabel)
	ExpandContractChildren(hdnExpanded, childId, name)
}

function SetParentLabelText(hdnExpanded, parentLabel)
{
	if (hdnExpanded.value == 'false')
		{
		parentLabel.innerHTML == '+';
		}
	else
		{
		parentLabel.innerHTML == '-';
		}
}

function SectionShowHideChildrenNoImage(hdnSection, childId, name, parentLabel)
{
	if (hdnSection.value == 'true')
		hdnSection.value = 'false'
	else
		hdnSection.value = 'true'
	SetSectionLabelText(hdnSection, parentLabel)
	SectionExpandContractChildren(hdnSection, childId, name)
}

function SetSectionLabelText(hdnSection, parentLabel)
{
	if (hdnSection.value == 'true')
		{
		parentLabel.innerHTML == '+';
		}
	else
		{
		parentLabel.innerHTML == '-';
		}
}

// XmlHttpRequest fun starts here.

var req;
var statusUpdateDelay = 5000;
var statusUpdateIncrement = 1.3; // so we can do updates quicker at start, then slower as time goes on, eg with val set to 1.5, 5 seconds delay, 7.5 delay, 11.5 delay... etc etc
var maxResults = 999;
function DoStatusUpdate(){	

	////////////////////// DISABLE ///////////////////////////////
	return false;
	//////////////////////////////////////////////////////////////

	statusUpdateDelay = statusUpdateDelay * statusUpdateIncrement;	
	var url = "statusupdate.aspx?";	
	for(i = 0; i < statusImageIds.length && i < maxResults; i++){
		url += "id" + i + "=" + statusImageIds[i] + "&";
	}	
	//remove the last &
	//url = url.substr(0,url.length -1); commented out because of the random key added to querystring in the get request.
	
	loadXMLDoc(url);	
	setTimeout('DoStatusUpdate()',statusUpdateDelay);
}

function loadXMLDoc(url) {

	req = false;
    // branch for native XMLHttpRequest object
	try {
         req = new XMLHttpRequest();
        } 
        catch (e) 
        {
            var MSXML_XMLHTTP_PROGIDS = new Array(
                'MSXML2.XMLHTTP.5.0',
                'MSXML2.XMLHTTP.4.0',
                'MSXML2.XMLHTTP.3.0',
                'MSXML2.XMLHTTP',
                'Microsoft.XMLHTTP');

			req = null;
			var success = false;
			for (var i=0;i < MSXML_XMLHTTP_PROGIDS.length && !success; i++) 
			{
				try 
				{
					req = new ActiveXObject(MSXML_XMLHTTP_PROGIDS[i]);
					success = true;
				} catch (e) {}
			}
        }

	if(req) {
		
		req.onreadystatechange = processReqChange;	
		
		// should really be a POST, statusUpdate.aspx can currently only handle a GET. 
		//Making it a POST would eliminate the need for the non caching hack here.	
		url += "randomKey=" + Math.random() * Date.parse(new Date());
		
		req.open("GET", url, true);
		req.send("");
	}
}

function processReqChange() {
    // only if req shows "loaded"
    if (req.readyState == 4) {
        // only if "OK"
        if (req.status == 200) {
           processResults();            
           return true;         
        } else {
          //alert("There was a problem retrieving the XML data:\n" + req.statusText);
          return false
        }
    }
}

function processResults(response){
	
	//TODO - FireFox doesnt like childNodes, change code to use GetElementsBy instead.
	
	xmlObj = req.responseXML.documentElement;	

	for(i = 0; i< xmlObj.childNodes.length;i++){
	
		var packageId = xmlObj.childNodes(i).childNodes(0).firstChild.text;			
		
		// do matches
		for(j = 0; j < statusImageIds.length && j < maxResults; j++){
			var imageId = statusImageIds[j];	
			if(imageId.indexOf(packageId) > 0){
			
				var statusCode = "";
				if(xmlObj.childNodes(i).childNodes(1) != null){
					statusCode = xmlObj.childNodes(i).childNodes(1).firstChild.text;
				}
				
				var statusText = "&nbsp;";				
				if(xmlObj.childNodes(i).childNodes(2) != null){
				statusText = "<br />" + xmlObj.childNodes(i).childNodes(2).firstChild.text;	
				}
				else
				{
				statusText = "<br />In Progress";
				}	
						
				var imagePath = getImagePath(statusCode);
				var labelId = imageId.replace("image","label");					
				;
				var img = document.getElementById(imageId);
				img.src = imagePath;				
				document.getElementById(labelId).innerHTML = statusText;
			}			
		}
		
	}
}

function getImagePath(statusCode){
	
	switch (statusCode)
	{
		case "DONE":
			imageName = "images/complete.gif";
			break;
		case "FAIL":			
			imageName = "images/error.gif";
			break;
		case "FINI":
			imageName = "images/queued.gif";
			break;
		default:
			imageName = "images/progress.gif";
			break;
	}
	
	return imageName;
}

function preloadImages(){

	pic1= new Image(17,17); 
	pic1.src= "images/complete.gif"; 
	
	pic2= new Image(17,17); 
	pic2.src= "images/error.gif"; 
	
	pic3= new Image(17,17); 
	pic3.src= "images/progress.gif"; 
	
	pic4= new Image(17,17); 
	pic4.src= "images/queued.gif"; 
}

// error pop up
function toggleErrorBox(divId, show, e)
{
	var offset = 2;
	var obj = document.getElementById(divId);	
	
	if(show)
	{		
		if (document.all) {
			x = event.clientX + document.documentElement.scrollLeft	+ document.body.scrollLeft;
			y = event.clientY + document.documentElement.scrollTop+ document.body.scrollTop;
		}
		else {
			x = e.pageX;
			y = e.pageY;
		}
	
		if (x < 0){x = 0}
		if (y < 0){y = 0}
		
		obj.style.top  = (y - offset);
		obj.style.left = (x - offset);		
		obj.style.display = 'block';
		obj.style.visibility = 'visible';
	}
	else
	{
		obj.style.display = show ? 'block' : 'none';
		obj.style.visibility = show ? 'visible' : 'hidden';		
	}	
}

function ToggleUpload(i1, i2)
{
	var o1;
	var o2;
	
	o1 = document.getElementById(i1);
	o2 = document.getElementById(i2);

	if (o1 && o2)
	{
		if (o1.style.display == 'none')
		{
			o1.style.display = '';
			o2.style.display = '';
		}
		else
		{
			o1.style.display = 'none';
			o2.style.display = 'none';
		}
	}
}




  