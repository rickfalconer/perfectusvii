using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;

namespace Perfectus.Server.WebUI.Interfaces.Reports
{
	/// <summary>
	/// Summary description for OfflineInstanceManagement.
	/// </summary>
	public class OfflineInstanceManagement : System.Web.Services.WebService
	{
		public OfflineInstanceManagement()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion

		[WebMethod]
		public int CreateOfflineReportingInstance(Guid newInstanceId, Guid packageId, Guid tempOfflineId)
		{
		   int version = Perfectus.Server.Reporting.Report.OfflineGetVersionByTempOfflineId(tempOfflineId);
           Perfectus.Server.Reporting.Report.CreateOfflineInstance(packageId,newInstanceId,version,0);	
	
			return version;
		}

		[WebMethod]
		public void SetOfflineClauseUsed(Guid instanceId, string uniqueLibraryKey, string clauseName)
		{
			Perfectus.Server.Reporting.Report.SetReportingClauseUsed(instanceId, uniqueLibraryKey, clauseName);			
		}

		[WebMethod]
		public void SetOfflineInterviewComplete(Guid instanceId)
		{
			Perfectus.Server.Reporting.Report.SetReportingInstanceComplete(instanceId, true);		
		}
	}
}
