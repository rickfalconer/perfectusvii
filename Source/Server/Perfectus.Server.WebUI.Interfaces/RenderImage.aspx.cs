using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Perfectus.Server.WebUI.Interfaces
{
	/// <summary>
	/// Summary description for RenderImage.
	/// </summary>
	public partial class RenderImage : System.Web.UI.Page
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Guid imageId = new Guid(Request.QueryString.ToString());
			
			using(MemoryStream ms = Perfectus.Server.InterviewSystem.InstanceManagement.GetPreviewImage(imageId))
			{
				Bitmap bmp = new Bitmap(ms);
				bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
