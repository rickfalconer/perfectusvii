using System;
using System.ComponentModel;
using System.Web.Services;
using Perfectus.Server.Common;
using Perfectus.Server.ConfigurationSystem.ServerSystem;
using Perfectus.Common;

namespace Perfectus.Server.WebAPI.ConfigurationSystem
{
	/// <summary>
	///		Exposes the ConfigurationSystem.Server API as a web service.
	/// </summary>
	public class Server : WebService
	{
		/// <summary>
		///		Default constructor.
		/// </summary>
		public Server()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

		/// <summary>
		///		Exposes the <see cref="Perfectus.Server.API.ConfigurationSystem.Server.GetServerInfo">API's GetServerInfo</see> method as a web service.
		/// </summary>
		/// <returns>Returns a ServerInfo object containing the server's name, message and unique identifier.</returns>
		[WebMethod]
		public ServerInfo GetServerInfo()
		{
			try
			{
				return API.ConfigurationSystem.Server.GetServerInfo();
			}
			catch (Exception ex)
			{
				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("retrievserveridentityinformationError"), ex);
				throw;
			}
		}

		[WebMethod]
		public ServerPublishInfo GetServerPublishInfo()
		{
			try
			{
				return API.ConfigurationSystem.Server.GetServerPublishInfo();
			}
			catch (Exception ex)
			{
				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("retrievserverpublishdetailError"), ex);
				throw;
			}
			
		}
	}
}