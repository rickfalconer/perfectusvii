using System;
using System.ComponentModel;
using System.Web.Services;
using Perfectus.Server.Common;
using Perfectus.Common;
using Perfectus.Server.ConfigurationSystem.PluginSystem;

namespace Perfectus.Server.WebAPI.ConfigurationSystem
{
	/// <summary>
	///		Exposes the ConfigurationSystem.Plugins API as a web service
	/// </summary>
	// TODO: Consistent namespace across schemas and services
	[WebService(Namespace="http://www.perfectus.net/serviceDefinitions/Perfectus.Server.WebAPI.ConfigurationSystem")]
	public class Plugins : WebService
	{
		/// <summary>
		///		Default constructor
		/// </summary>
		public Plugins()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion


		/// <summary>
		///		Exposes the <see cref="Perfectus.Server.API.ConfigurationSystem.Plugins.GetInstalledPlugins">API's GetInstalledPlugins</see> method as a web service.
		/// </summary>
		/// <returns>Returns a collection of PluginDescriptor objects.</returns>
		[WebMethod]
		public PluginDescriptorCollection GetInstalledPlugins()
		{
			try
			{
				return API.ConfigurationSystem.Plugins.GetInstalledPlugins();
			}
			catch (Exception ex)
			{
				Logging.LogMessage(ResourceLoader.GetResourceManager("Perfectus.Server.WebUI.Interfaces.Localisation").GetString("retrievepluginError"), ex);
				throw;
			}
		}

	}
}