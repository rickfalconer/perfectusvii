using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Perfectus.Server.ConfigurationSystem;
using Perfectus.Server.WebUI.Library;
using Perfectus.Server.Common;

namespace Perfectus.Server.WebUI.Interfaces
{
    public partial class EmailLogger : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ExceptionDetailsRow.Visible = false;
            ExceptionDetailsLabel.Visible = false;
            ExceptionTitleRow.Visible = false;

            if (!Page.IsPostBack)
            {
                // Set the example 
                ToTextBox.Text = Config.Shared.interview.To;
                FromTextBox.Text = Config.Shared.interview.From;
                SubjectTextBox.Text = Config.Shared.interview.Subject;
                SMTPServerTextBox.Text = Config.Shared.interview.SMTPServer;

                // Set the example exception
                TitleTextBox.Text = "This is the example Title Exception Text.";
                UIMessageTextBox.Text = "This is the example UI Message Exception Text.";
                MessageTextBox.Text = "This is the example Message Details Exception Text.";
            }
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                InterviewException interviewException = new InterviewException(MessageTextBox.Text, UIMessageTextBox.Text);
                interviewException.Title = TitleTextBox.Text;
                InterviewEmailLogger interviewLogger = new InterviewEmailLogger(interviewException, ToTextBox.Text, FromTextBox.Text,
                                                                                SMTPServerTextBox.Text);

                interviewLogger.SendEmail(SubjectTextBox.Text, TitleTextBox.Text, "100", Identity.GetRunningUserId(), 
                                          this.Page.Request.RawUrl);
            }
            catch (Exception ex)
            {
                ExceptionDetailsLabel.Text = ex.Message;
                ExceptionDetailsRow.Visible = true;
                ExceptionDetailsLabel.Visible = true;
                ExceptionTitleRow.Visible = true;
            }
        }
    }
}