<%@ Register TagPrefix="cc1" Namespace="Perfectus.Server.WebUI.Library.Interview" Assembly="Perfectus.Server.WebUI.Library, culture=neutral" %>
<%@ Page language="c#" Inherits="Perfectus.Server.WebUI.Interfaces.InterviewPreview" AutoEventWireup="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD runat="server">
		<title>Interview Preview</title>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles/default2.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="PerfectusPreviewForm" method="post" runat="server">
            <ajaxToolkit:ToolkitScriptManager runat="Server" EnableScriptGlobalization="true"
                EnableScriptLocalization="true" ID="ScriptManager1" CombineScripts="false">
		        <Services>
		            <asp:ServiceReference Path="~/scriptServices.asmx" />
		        </Services>
            </ajaxToolkit:ToolkitScriptManager>
            <%--	pf-3096
		    <asp:ScriptManager runat="server" ID="ScriptManager1" EnableScriptLocalization="true" EnableScriptGlobalization="true">
		        <Services>
		            <asp:ServiceReference Path="~/scriptServices.asmx" />
		        </Services>
		    </asp:ScriptManager>
            --%>
			<input type="hidden" name="PAGEDATA">
			<asp:panel id="loading" runat="server">
				<CENTER>Generating preview...</CENTER>
			</asp:panel>
			<asp:panel id="loaded" runat="server" visible="false">
				<cc1:interview id="iui" runat="server" PreviewRenderPagePath="RenderImage.aspx?" ImagesPath="Images"
					JavascriptPath="Script" CssClass="interview" EnableViewState="false"></cc1:interview>
			</asp:panel>
		</form>
	</body>
</HTML>
