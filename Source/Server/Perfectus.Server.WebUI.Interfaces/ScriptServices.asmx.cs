using System;
using System.Web.Script.Services;
using System.Web.Services;
using System.ComponentModel;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Server.InterviewSystem;

namespace Perfectus.Server.WebUI.Interfaces
{
    /// <summary>
    /// Summary description for ScriptServices
    /// </summary>
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class ScriptServices : WebService
    {

        [WebMethod]
        public string FormatAnswer(string strAnswer, string formatString, string formatParameter, string strInstanceId, string strDataType)
        {
            Guid instanceId = new Guid(strInstanceId);
            string instanceName = InstanceManagement.GetInstanceReference(instanceId);
            int version;
            int revision;
            InstanceManagement.GetVersion(instanceId, out version, out revision);
            string packageName = string.Empty; // TODO: Maybe we should get this? The sbmd isn't really used from the client currently, so let's avoid a hit if we can
            PerfectusDataType dataType;
            switch (strDataType)
            {
                case "date":
                    dataType = PerfectusDataType.Date;
                    break;
                case "yesno":
                    dataType = PerfectusDataType.YesNo;
                    break;
                case "number":
                    dataType = PerfectusDataType.Number;
                    break;
                case "text":
                default:
                    dataType = PerfectusDataType.Text;
                    break;
            }



            StringBindingMetaData sbmd = new StringBindingMetaData(packageName, instanceName, instanceId, version, revision);
            // We get a string from the browser, so to format it nicely we will have to cast it to its appropriate type (according to the 'pdt' attribute)
            object oAnswer = strAnswer;
            oAnswer = AnswerProvider.ConvertToBestType(oAnswer, dataType, AnswerProvider.Caller.Interview, sbmd);
            Package instance = InstanceManagement.GetInstance(instanceId);
            try
            {
                return
                    Perfectus.Server.InterviewSystem.Format.FormatForDisplay(oAnswer, formatString, formatParameter, dataType, instance);
            }
            catch (Exception ex)
            {
                return strAnswer;
            }
        }
    }
}

