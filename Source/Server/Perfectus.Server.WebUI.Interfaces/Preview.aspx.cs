using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Perfectus.Server.WebUI.Interfaces
{
    /// <summary>
    /// Summary description for Preview.
    /// </summary>
    public partial class Preview : System.Web.UI.Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            string instanceId = Request.QueryString["instanceId"];
            string templateName = Request.QueryString["templateName"];
            try
            {
                Preview1.PackageInstanceId = new Guid(instanceId);
            }
            catch (Exception ex)
            {
                throw new Exception("The URL did not contain a valid instanceId", ex);
            }

            //Preview1.DataBind();

            if (templateName != null && templateName.Trim().Length > 0)
            {
                Preview1.Assemble(templateName);
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion
    }
}
