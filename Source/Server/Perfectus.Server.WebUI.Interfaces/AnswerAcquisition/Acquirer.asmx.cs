using System;
using System.ComponentModel;
using System.Web.Services;
using System.Xml;

namespace Perfectus.Server.WebAPI.AnswerAcquisition
{
	/// <summary>
	///		Exposes the AnswerAcquisition API as a Web Service.
	/// </summary>
	public class Acquirer : WebService
	{
		public Acquirer()
		{
			//CODEGEN: This call is required by the ASP.NET Web Services Designer
			InitializeComponent();
		}

		#region Component Designer generated code

		//Required by the Web Services Designer 
		private IContainer components = null;

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion

		/// <summary>
		///		Exposes the <see cref="Perfectus.Server.API.AnswerAcquisition.Acquirer.Acquire">API's Acquire method</see> as a web service.
		/// </summary>
		/// <param name="answerSetXml">An <see cref="XmlDocument"></see> containing the answers to apply.</param>
		/// <param name="reference">A user-friendly reference for the Instance when it appears in the My Documents list.</param>
		/// <param name="folderId">The server's unique ID for the folder into which the instance should be placed.</param>
		/// <returns>Returns an <see cref="int"></see> that uniquely identifies the newly created instance.</returns>
		public Guid Acquire(XmlDocument answerSetXml, string reference, int folderId)
		{
			return API.AnswerAcquisition.Acquirer.Acquire(answerSetXml, reference, folderId);
		}

	}
}