using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Net;
using Perfectus.Library.Localisation;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Library.Logging;

// ToDo Replace with proper policy handling

namespace Perfectus.Library.Exception
{
    [Serializable]
    public class PerfectusException : System.Exception, IXmlSerializable
    {
        String _message;
        String _capture;
        String _stacktrace;

        public
        override string StackTrace
        {
            get
            {
                if (_stacktrace != null && _stacktrace.Length > 0)
                    return _stacktrace;
                return base.StackTrace;
            }
        }

        public
        override string Message
        {
            get
            {
                if (_message.Length > 0)
                    return _message;
                return base.Message;
            }
        }
        public
        string Capture
        {
            get
            {
                return _capture;
            }
        }

        // For serialisation
        public
        PerfectusException()
            : base() { }

        ///   
        public PerfectusException(ID messageID)
            : this(Log.Category.None, messageID)
        {
        }

        public PerfectusException(ID messageID, params IConvertible[] additionalMessages)
            : this(Log.Category.None, messageID, additionalMessages)
        {
        }

        public PerfectusException(Log.Category category, ID messageID)
            : this(category, messageID, new IConvertible[0])
        {
        }

        public PerfectusException(Log.Category category, ID messageID, params IConvertible[] additionalMessages)
            : base(messageID.ToString())
        {
            _message = messageID.ToIdString(additionalMessages);

            if (_capture == null || _capture == String.Empty)
                _capture = LocalisationSupport.Text(new ID("StandardErrorCaption"));
            base.HelpLink = messageID.HelpLink();
            base.Source = messageID.FullName();
            try
            {
                Log.Error(category, messageID, additionalMessages);
            }
            catch { }
        }

        // with inner exception
        public PerfectusException(ID messageID, System.Exception innerException)
            : this(messageID, innerException, new IConvertible[0])
        {
        }

        public PerfectusException(ID messageID, System.Exception innerException, params IConvertible[] additionalMessages)
            : this(Log.Category.None, messageID, innerException, additionalMessages)
        {
        }

        public PerfectusException(Log.Category category, ID messageID, System.Exception innerException)
            : this(category, messageID, innerException, new IConvertible[0])
        {
        }

        public PerfectusException(Log.Category category, ID messageID, System.Exception innerException, params IConvertible[] additionalMessages)
            : base(messageID.ToString(), innerException)
        {
            _message = messageID.ToIdString(additionalMessages);

            if (_capture == null || _capture == String.Empty)
                _capture = LocalisationSupport.Text(new ID("StandardErrorCaption"));
            base.HelpLink = messageID.HelpLink();
            base.Source = messageID.FullName();

            // That doesn't really work....
            if (innerException.GetType().ToString().EndsWith("SPException"))
            {
                _message = innerException.Message;
                _capture = innerException.Source;
            }
            else if (innerException is WebException)
            {
                WebException webException = (WebException)innerException;
                if (webException.Status == WebExceptionStatus.ProtocolError)
                {
                    switch (((HttpWebResponse)webException.Response).StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:
                            _message = LocalisationSupport.Text(new ID("HTTP_401"));
                            break;
                        case HttpStatusCode.Forbidden:
                            _message = LocalisationSupport.Text(new ID("HTTP_Forbidden"));
                            break;
                        default:
                            _message = String.Format(LocalisationSupport.Text(new ID("HTTP_Any")),
                                ((HttpWebResponse)webException.Response).StatusCode.ToString(),
                                ((HttpWebResponse)webException.Response).StatusDescription);
                            break;
                    }
                }
                else if (webException.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    _message = innerException.Message;
                }
            }

            if (_message == String.Empty)
                _message = innerException.Message;
            if (_capture == String.Empty)
                _capture = LocalisationSupport.Text(new ID("StandardErrorCaption"));

            try
            {
                Log.Error(category, innerException, messageID, additionalMessages);
            }
            catch { }
        }

        private XmlNode ToXmlNode(System.Exception ex, XmlDocument xmlDocument)
        {
            XmlElement xmlException = xmlDocument.CreateElement("Exception");
            XmlElement xmlElement = xmlDocument.CreateElement("Type");
            xmlElement.InnerText = ex.GetType().ToString();
            xmlException.AppendChild(xmlElement);

            xmlElement = xmlDocument.CreateElement("Message");
            xmlElement.InnerText = ex.Message;
            xmlException.AppendChild(xmlElement);

            xmlElement = xmlDocument.CreateElement("StackTrace");
            xmlElement.InnerText = ex.StackTrace;
            xmlException.AppendChild(xmlElement);

            xmlElement = xmlDocument.CreateElement("Capture");
            if (ex is PerfectusException)
                xmlElement.InnerText = ((PerfectusException)ex).Capture;
            else
                xmlElement.InnerText = String.Empty;
            xmlException.AppendChild(xmlElement);

            xmlElement = xmlDocument.CreateElement("Source");
            xmlElement.InnerText = ex.Source;
            xmlException.AppendChild(xmlElement);

            return xmlException;
        }
        public XmlNode ToXmlNode()
        {
            XmlDocument xmlDocument = new XmlDocument();
            return ToXmlNode(this, xmlDocument);
        }
        public override string ToString()
        {
            return String.Format("{0}",
                //_capture,
                //System.Environment.NewLine,
                _message);
        }
        public string ToUserFriendlyString()
        {
            return ToString();
        }
        public string ToUserFriendlyCaption()
        {
            return _capture;
        }
        public string ToLongString()
        {
            StringBuilder retString = new StringBuilder(ToString());
            if (InnerException != null)
            {
                retString.Append(System.Environment.NewLine);
                retString.Append(InnerException.Message);
                retString.Append(System.Environment.NewLine);
                retString.Append(InnerException.StackTrace);
            }
            return retString.ToString();
        }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader r)
        {
            // Open the definition element and get the first parameters info
            r.ReadStartElement("PerfectusException");

            r.ReadStartElement("Type");
            r.Read();
            r.ReadEndElement();

            r.ReadStartElement("Message");
            _message = r.ReadString().Trim();
            r.ReadEndElement();

            r.ReadStartElement("StackTrace");
            _stacktrace = r.ReadString().Trim();
            r.ReadEndElement();

            r.ReadStartElement("Capture");
            _capture = r.ReadString().Trim();
            r.ReadEndElement();

            r.ReadStartElement("Source");
            Source = r.ReadString().Trim();
            r.ReadEndElement();
            r.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("PerfectusException");
            XmlNode node = ToXmlNode();
            writer.WriteString(node.InnerXml);
            writer.WriteEndElement();
        }

        #endregion

    }
}
