using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Perfectus.Library.Localisation;
using Perfectus.Library.Exception.View;

namespace Perfectus.Library.Exception
{
    public static class PerfectusMessage
    {
        public static void Show(System.Exception exception)
        {
            if (exception is PerfectusException)
            {
                PerfectusMessageBox b = new PerfectusMessageBox();
                b.Message = exception.Message;
                b.Hint = exception.HelpLink;
                b.Source = exception.Source;
                b.TheException = exception;
                b.ShowDialog();
            }
            else
                MessageBox.Show(exception.Message,
                    LocalisationSupport.Text(new ID("StandardErrorCaption")),
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
        }
    }
}
