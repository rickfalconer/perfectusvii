using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Perfectus.Library.Localisation;
using System.Diagnostics;

namespace Perfectus.Library.Exception.View
{
    internal partial class PerfectusMessageBox : Form
    {
        private TextBox txtStackTraceBox = null;
        private string hint;
        private string source;
        private System.Exception theException;

        internal PerfectusMessageBox()
        {
            InitializeComponent();
            Text = LocalisationSupport.Text(new ID("StandardErrorCaption"));

            btnMore.VisibleChanged += new EventHandler(btnMore_VisibleChanged);
            btnMore.Click += new EventHandler(btnMore_Click);
        }

        void btnMore_Click(object sender, EventArgs e)
        {
            if (txtStackTraceBox != null)
            {
                Controls.Remove(txtStackTraceBox);
                txtStackTraceBox = null;
                btnMore.Top -= 208;
                btnOK.Top -= 208;
                Height -= 208;
                btnMore.ImageIndex = 0;
                return;
            }

            txtStackTraceBox = new TextBox();
            txtStackTraceBox.AutoSize = true;
            txtStackTraceBox.Multiline = true;
            txtStackTraceBox.ReadOnly = true;
            txtStackTraceBox.WordWrap = true;

            StringBuilder bld = new StringBuilder(theException.StackTrace);
            if (theException.InnerException != null)
                bld.Append(String.Format("{0}Inner Exception:{1}{2}{3}{4}",
                    System.Environment.NewLine,
                    System.Environment.NewLine,
                    theException.InnerException.Message,
                    theException.InnerException,
                    theException.InnerException.StackTrace));

            txtStackTraceBox.Text = bld.ToString();
            txtStackTraceBox.Height = 200;
            txtStackTraceBox.ScrollBars = ScrollBars.Both;
            txtStackTraceBox.Width = Width - (2 * lblMessage.Left);
            txtStackTraceBox.Left = lblMessage.Left;
            txtStackTraceBox.Top = btnMore.Top;

            btnMore.Top += 208;
            btnMore.ImageIndex = 1;
            btnOK.Top += 208;
            Height += 208;

            Controls.Add(txtStackTraceBox);
        }

        void btnMore_VisibleChanged(object sender, EventArgs e)
        {
            SetButtonPosition();
        }

        internal System.Exception TheException
        {
            set
            {
                if (value == null)
                    return;
                theException = value;
                if (theException.StackTrace != null ||
                    theException.InnerException != null )
                    this.btnMore.Visible = true;
            }
        }
        internal String Message
        {
            set
            {
                if (value == null || value.Length == 0)
                    return;
                lblMessage.Text = value;

                Width = lblMessage.PreferredWidth + lblMessage.Left * 2;//(int)f.Width + lblMessage.Left * 2;
                Height += lblMessage.PreferredHeight;
                SetButtonPosition();
            }
        }
        internal String Hint
        {
            set
            {
                if (value == null || value.Length == 0)
                    return;

                hint = value;
                if (hint.ToLower().StartsWith("<html>"))
                    SetupBrowserWindow();
                else if (hint.ToLower().StartsWith("www."))
                    SetupLinkWindow();
                else
                    SetupTextWindow();
            }
        }

        internal String Source { set { source = value; } }

        private void SetupBrowserWindow()
        {
            Label lblInfo1 = new Label();
            WebBrowser webInfo2 = new WebBrowser();

            lblInfo1.AutoSize = true;
            webInfo2.AutoSize = true;

            lblInfo1.Text = LocalisationSupport.Text(new ID("LabelHintText"));
            webInfo2.DocumentText = hint;
            webInfo2.Height = 100;

            int sites = lblInfo1.Left = webInfo2.Left = lblMessage.Left;
            sites *= 2;

            lblInfo1.Top = lblMessage.Top + lblMessage.Height + 8;
            webInfo2.Top = lblInfo1.Top + lblInfo1.Height;


            int maximumWidth =
            Math.Max(
                lblMessage.PreferredWidth,
                Math.Max(
                webInfo2.PreferredSize.Width,
                lblInfo1.PreferredWidth));

            webInfo2.Width = lblMessage.Width = lblInfo1.Width = maximumWidth;

            Width = maximumWidth + sites;

            Height += lblInfo1.Height + webInfo2.Height + 12;

            Controls.AddRange(new Control[] { lblInfo1, webInfo2 });

            SetButtonPosition();
        }
        private void SetupLinkWindow()
        {
            Label lblInfo1 = new Label();
            LinkLabel lblInfo2 = new LinkLabel();
            lblInfo1.AutoSize = true;
            lblInfo2.AutoSize = true;

            lblInfo1.Text = LocalisationSupport.Text(new ID("LabelHintWebBrowser"));
            lblInfo2.Text = hint;
            lblInfo2.Links.Add(0, hint.Length, hint);
            lblInfo2.LinkClicked += new LinkLabelLinkClickedEventHandler(lblInfo2_LinkClicked);

            int sites = lblInfo1.Left = lblInfo2.Left = lblMessage.Left;
            sites *= 2;

            lblInfo1.Top = lblMessage.Top + lblMessage.Height + 8;
            lblInfo2.Top = lblInfo1.Top + lblInfo1.Height;


            int maximumWidth =
            Math.Max(
                lblMessage.PreferredWidth,
                Math.Max(
                lblInfo2.PreferredSize.Width,
                lblInfo1.PreferredWidth));

            lblInfo2.Width = lblMessage.Width = lblInfo1.Width = maximumWidth;

            Width = maximumWidth + sites;

            Height += lblInfo1.Height + lblInfo2.Height + 12;

            Controls.AddRange(new Control[] { lblInfo1, lblInfo2 });

            SetButtonPosition();
        }
        private void SetupTextWindow()
        {
            Label lblInfo1 = new Label();
            lblInfo1.AutoSize = true;
            lblInfo1.Text = LocalisationSupport.Text(new ID("LabelHintText"));

            TextBox txtBox = new TextBox();
            txtBox.AutoSize = true;
            txtBox.Multiline = true;
            txtBox.ReadOnly = true;
            txtBox.Text = hint;
            txtBox.Height = 100;

            int sites = lblInfo1.Left = txtBox.Left = lblMessage.Left;
            sites *= 2;

            lblInfo1.Top = lblMessage.Top + lblMessage.Height + 8;
            txtBox.Top = lblInfo1.Top + lblInfo1.Height;

            int maximumWidth = 
            Math.Max(
                lblMessage.PreferredWidth,
                Math.Max(
                txtBox.PreferredSize.Width,
                lblInfo1.PreferredWidth));

            txtBox.Width = lblMessage.Width = lblInfo1.Width = maximumWidth;

            Width = maximumWidth + sites;

            Height += lblInfo1.Height + txtBox.Height + 12;

            Controls.AddRange(new Control[] { lblInfo1, txtBox });

            SetButtonPosition();
        }

        void lblInfo2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string target = e.Link.LinkData as string;
            if (null != target && target.StartsWith("www"))
                System.Diagnostics.Process.Start(target);
        }

        private void SetButtonPosition()
        {
            this.btnMore.Left = lblMessage.Left;
            
            this.btnOK.Left = Width / 2 - this.btnOK.PreferredSize.Width / 2;
            this.btnMore.Top=this.btnOK.Top = Height - this.btnOK.PreferredSize.Height * 3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}