namespace Perfectus.Library.Exception.View
{
    partial class PerfectusMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PerfectusMessageBox));
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnMore = new System.Windows.Forms.Button();
            this.ImageMoreLess = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 9);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(60, 13);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "lblMessage";
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(47, 25);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(72, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "Ok";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnMore
            // 
            this.btnMore.FlatAppearance.BorderSize = 0;
            this.btnMore.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Silver;
            this.btnMore.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnMore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMore.ImageIndex = 0;
            this.btnMore.ImageList = this.ImageMoreLess;
            this.btnMore.Location = new System.Drawing.Point(179, 9);
            this.btnMore.Name = "btnMore";
            this.btnMore.Size = new System.Drawing.Size(24, 23);
            this.btnMore.TabIndex = 2;
            this.btnMore.UseVisualStyleBackColor = true;
            this.btnMore.Visible = false;
            // 
            // ImageMoreLess
            // 
            this.ImageMoreLess.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageMoreLess.ImageStream")));
            this.ImageMoreLess.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageMoreLess.Images.SetKeyName(0, "ExpandAll.bmp");
            this.ImageMoreLess.Images.SetKeyName(1, "CollapseAll.bmp");
            // 
            // PerfectusMessageBox
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(215, 54);
            this.Controls.Add(this.btnMore);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PerfectusMessageBox";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "PerfectusMessageBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnMore;
        private System.Windows.Forms.ImageList ImageMoreLess;
    }
}