namespace Perfectus.Library.Install
{
	/// <summary>
	/// Summary description for Utils.
	/// </summary>
	public class Utils
	{
        public enum WizardAction { Success, Failure, Cancel };

        public enum SqlAction
        {
            IsCurrent = 0,          // Existing Perfectus database and is on the current version
            ExistingIsNewer,        // The existing Perfectus database is newer than the one being installed
            CreateAndInstall,       // Create a new Perfectus database and apply the install scripts
            Install,                // Apply the Perfectus scripts
            UpgradeSupported,       // Perfectus database, upgrade to the new version
            UpgradeNotSupported,    // Perfectus database, but not supported
            Error                   // There was an error in attempting to evalute the sql action to take.
        };

		private Utils() {}

		public static string GetConnectionString(string serverName, string databaseName)
		{
            return string.Format("server={0};Database={1};Trusted_Connection=True;Connect Timeout=30;Network Library=DBMSSOCN", serverName, databaseName);
		}

		public static string GetConnectionString(string serverName, string loginName, string password, string databaseName)
		{
            return string.Format("Server={0};Database={1};User Id={2};Password={3};Trusted_Connection=False;Connect Timeout=30;Network Library=DBMSSOCN", serverName, databaseName, loginName, password);
		}
	}
}