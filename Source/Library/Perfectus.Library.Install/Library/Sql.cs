using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace Perfectus.Library.Install
{
	/// <summary>
	/// Summary description for Sql.
	/// </summary>
	public class Sql
	{
        // Hardcoded main xml file that contains the database install/upgrade commands
        private string dataBaseInstallFileName = "PerfectusDatabase.xml";
        // Path to the location of the database xml files.
        private string dataBaseInstallFilePath = null;
        // Path to the location of the database xml files. (DevHarness mode only! - No longer used... )
        private string dataBaseInstallFileRelease = null;
        // This is the name of the database node we are installing/upgrading.
        private string dataBaseType = null;
        // This is main database configuration xml file.
		private XmlDocument config = new XmlDocument();
		
		/// <summary>
		///	Loads the default config file that contains the perfectus database install script
		/// </summary>
        /// <param name="dataBaseKey">xml node name of the database type to be installed.</param>
        public Sql( string dataBaseKey, string path, string release )
		{
            // Set the database type base on the key given to us.
            dataBaseType = dataBaseKey;
            // Set the path and release version folder name.
            dataBaseInstallFilePath = path;
            dataBaseInstallFileRelease = release;

            // By default immediately load the main install/upgrade file.
            config.Load( GetFilePathAndName( dataBaseInstallFilePath, dataBaseInstallFileRelease, dataBaseInstallFileName ) );	
		}

        private string GetFilePathAndName( string path, string release, string filename )
        {
            string executingPath = String.Empty;

            // Construct the path where the sql scripts are. 
            if( String.IsNullOrEmpty( path ) )
                executingPath = Path.GetDirectoryName( Assembly.GetExecutingAssembly( ).CodeBase );
            else
            {
                executingPath = path;

                // As part of FB2423 we no longer need this as the product MUST be already installed
                //if( !String.IsNullOrEmpty( release ) )
                //    executingPath = Path.Combine( executingPath, release ); 
            }
                
            return Path.Combine( executingPath, filename );
        }

		public string GetDatabaseVersionCheckSql()
		{
			return config.SelectSingleNode(@"/SqlScripts/SqlServer/DatabaseVersion/Check").InnerText.Trim();
		}

		public string GetCreateSql(string databaseName, out bool isTransaction)
		{
            string nodeXPath = @"/SqlScripts/SqlServer/Create";
			string sql = config.SelectSingleNode( nodeXPath ).InnerText.Trim( );

            string transaction = config.SelectSingleNode(string.Format("{0}/@Transaction", nodeXPath)).Value;
            isTransaction = ConvertTransactionValueToBoolean(transaction, nodeXPath);
			
			return string.Format(sql, databaseName);
		}

		public string GetInstallSql(string databaseName, out bool isTransaction)
		{
            // First extract the file name.
            string nodeXPath = String.Format( @"/SqlScripts/SqlServer/{0}/Install/@FileName", this.dataBaseType );
            string xmlFileName = config.SelectSingleNode( nodeXPath ).Value;
            if( String.IsNullOrEmpty( xmlFileName ) )
                throw new Exception( "Missing install file name due to corrupt xml script file." );
 
            // Load the xml file
            XmlDocument xmlDoc = new XmlDocument( );
            xmlDoc.Load( GetFilePathAndName( dataBaseInstallFilePath, dataBaseInstallFileRelease, xmlFileName ) );	

            // Load the sql script
            nodeXPath = @"/SqlScripts/SqlServer/Install";
            string sql = xmlDoc.SelectSingleNode( nodeXPath ).InnerText.Trim( );

            // Check whether transaction based.
            string transaction = xmlDoc.SelectSingleNode( string.Format( "{0}/@Transaction", nodeXPath ) ).Value;
            isTransaction = ConvertTransactionValueToBoolean( transaction, nodeXPath );

            return string.Format( sql, databaseName );
		}

		public string GetPreInstallSql(string databaseName, string userName, string userPassword, out bool isTransaction)
		{
            string nodeXPath = @"/SqlScripts/SqlServer/PreInstall";
            string sql = config.SelectSingleNode( nodeXPath ).InnerText.Trim( );

            string transaction = config.SelectSingleNode( string.Format( "{0}/@Transaction", nodeXPath ) ).Value;
            isTransaction = ConvertTransactionValueToBoolean( transaction, nodeXPath );

            return string.Format( sql, databaseName, userName, userPassword );
        }

        public Utils.SqlAction GetUpgradeType(Int32 currentVersion)
        {
            string installXpath = String.Format( @"/SqlScripts/SqlServer/{0}/Install/@Sequence", this.dataBaseType  );
            string installVersionXpath = string.Format( @"/SqlScripts/SqlServer/{0}/Install[@Sequence='{1}']", this.dataBaseType, currentVersion.ToString( ) );
            string upgradeVersionXpath = string.Format( @"/SqlScripts/SqlServer/{0}/Upgrades/Upgrade[@From='{1}']", this.dataBaseType, currentVersion.ToString( ) );

            XmlNode versionNode = config.SelectSingleNode(installVersionXpath);

            // Set the appropriate sql action to be performed
            if (versionNode != null)
            {
                // The installed database version matched the version being installed
                return Utils.SqlAction.IsCurrent;
            }
            else if (config.SelectSingleNode(upgradeVersionXpath) != null)
            {
                // The installed database is an older perfectus version and can be upgraded
                return Utils.SqlAction.UpgradeSupported;
            }
            else
            {
                XmlNode installNode = config.SelectSingleNode(installXpath);

                if (installNode.Value != null)
                {
                    try
                    {
                        Int32 installVersion = Convert.ToInt32(installNode.Value);

                        if (currentVersion > installVersion)
                        {
                            // The installed perfectus database is newer than the version being installed
                            return Utils.SqlAction.ExistingIsNewer;
                        }
                    }
                    catch
                    { 
                        // Install version not set or not an int32
                        return Utils.SqlAction.Error;
                    }
                }

                // The database cannot be upgraded - install the entire script (don't create the db first through)
                return Utils.SqlAction.UpgradeNotSupported;
            }
        }

        /// <summary>
        ///     Gets a SortedList of sql upgrade scripts from the current sequence version in the given database.
        ///     Only those scripts which equal or are greater than the current sequence number are included. This 
        ///     ensures the database is only upgraded with scripts that move the database version forward, and do
        ///     not include retro scripts from previous versions.
        /// </summary>
        /// <param name="databaseName">The name of the database being upgraded.</param>
        /// <param name="oldVersion">The current sequence version number of the given database.</param>
        /// <returns>
        ///     A sorted list of sql upgrade scripts in name value pairs. The scripts are sorted by sequence number
        ///     to ensure they are applied in the correct order. The value contains the actualsql script.
        /// </returns>
        public SortedList<int, UpgradeScript> GetUpgradeSqlScripts(string databaseName, Int32 oldVersion)
		{
            SortedList<int, UpgradeScript> upgradeScripts = new SortedList<int, UpgradeScript>();

            // Get a list of upgrades from the script files starting from the last sequence number match
            XmlNodeList upgradeNodes = config.SelectNodes( String.Format( @"/SqlScripts/SqlServer/{0}/Upgrades/Upgrade", this.dataBaseType ) );
                        
            foreach (XmlNode upgradeNode in upgradeNodes)
            {
                AddUpgradeScript(databaseName, upgradeNode, oldVersion, upgradeScripts);
            }

            return upgradeScripts;
		}

        private void AddUpgradeScript(string databaseName, XmlNode upgradeNode, Int32 oldVersion, 
                                      SortedList<int, UpgradeScript> upgradeScripts)
        { 
            // Get the upgrade nodes sequence
            string sequenceString = upgradeNode.SelectSingleNode("@From").InnerText;
            Int32 sequence;

            try
            {
                sequence = Convert.ToInt32(sequenceString);
            }
            catch (InvalidCastException ex)
            {
                throw new Exception(string.Format("The Upgrade Node with the From attribute {0} is invalid. The From attributes value must be a valid int32 value.", sequenceString), ex);
            }


            // Ensure the upgrade node will be forwarding the sequence (is not a retro script)
            if( oldVersion <= sequence )
            {
                // First extract the file name.
                string xmlFileName = upgradeNode.SelectSingleNode( "@FileName" ).Value;
                if( String.IsNullOrEmpty( xmlFileName ) )
                    throw new Exception( string.Format( "The Upgrade Node with the From attribute {0} is invalid. The FileName attribute value must be a valid value.", sequenceString ) );

                // Load the xml file (Upgrades are not stored per release, hence don't pass in the release folder name.
                XmlDocument xmlDoc = new XmlDocument( );
                xmlDoc.Load( GetFilePathAndName( dataBaseInstallFilePath, "Upgrade", xmlFileName ) );
                XmlNode node = xmlDoc.SelectSingleNode( @"/SqlScripts/SqlServer/Upgrade" );
                if( node == null )
                    throw new Exception( string.Format( "Invalid upgrade format in file {0} .", xmlFileName ) );

                // Load the sql script
                string script = string.Format( node.InnerText, databaseName ); // Replace db name markers with the actual db name
                string transactionString = node.SelectSingleNode( "@Transaction" ).InnerText;
                string notes = node.SelectSingleNode( "@Note" ).InnerText;

                Boolean transaction;
                try
                {
                    transaction = Convert.ToBoolean( transactionString );
                }
                catch( InvalidCastException ex )
                {
                    throw new Exception( string.Format( "The Upgrade Node with the Transaction attribute {0} is invalid. The Transaction attributes value must be a valid yes / no boolean value.", transactionString ), ex );
                }

                UpgradeScript upgradeScript = new UpgradeScript( );
                upgradeScript = PopulateUpgradeScript( sequence, transaction, notes, script );
                upgradeScripts.Add(upgradeScript.From, upgradeScript);
            }
        }

        private UpgradeScript PopulateUpgradeScript(Int32 sequence, Boolean transaction, string notes, string script)
        { 
            // Add the Upgrade script
            UpgradeScript upgradeScript = new UpgradeScript();
            upgradeScript.Notes = notes;
            upgradeScript.From = sequence;
            upgradeScript.Script = script;
            upgradeScript.Transaction = transaction;

            return upgradeScript;
        }

        private bool ConvertTransactionValueToBoolean(string transaction, string nodeXPath)
        {
            try
            {
                return Convert.ToBoolean(transaction);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("There was an error in converting the transaction value to a boolean. Script location: {0}", transaction), ex);           
            }
        }
	}
}
