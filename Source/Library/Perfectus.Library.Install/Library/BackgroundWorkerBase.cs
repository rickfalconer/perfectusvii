using System;
using System.ComponentModel;
using System.Threading;

namespace Perfectus.Library.Install
{

	public abstract class BackgroundWorkerBase
	{
        BackgroundWorker worker;
		private int index;

		public abstract string Name { get; }
		public abstract string Message { get; }
        public abstract bool ProvidesProgress { get; }
        
        public BackgroundWorker Worker 
        { 
            get { return worker;  }
            set { worker = value; } 
        }

		public int Index
		{
			get { return index; }
		}

        public BackgroundWorkerBase(int index)
		{
			this.index = index;
		}

		public void Abort(bool raiseEvent)
		{
			if (worker != null)
			{
				worker.CancelAsync();
			}
		}

        public abstract void Start(BackgroundWorker worker, DoWorkEventArgs e);
	}
}