﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Perfectus.Library.Install
{
    /// <summary>
    /// Provides an API to initiate the database configuration wizard.
    /// </summary>
    public class DatabaseConfiguration
    {
        static private string _coreDatabaseKey = "PerfectusDatabase";
        static public string CoreDatabaseKey
        {
            get { return _coreDatabaseKey; }
        }

        static private string _reportingDatabaseKey = "ReportingDatabase";
        static public string ReportingDatabaseKey
        {
            get { return _reportingDatabaseKey; }
        }

        // These are the original database user name and password that where hardcoded into the system
        // FB1959 Resulted in the user having the ability to configure the user name and password on install.
        // However if they chose not to enter a name and password we use the original default values as specified below.
        // This should be the only place in the system where these values are specified.
        static private string _defaultUserName = "APP_Perfectus";
        static private string _defaultUserPassword = "Perfectus#01";
        static private string _proposedCoreDatabaseName = "Perfectus";
        static private string _proposedReportingDatabaseName = "Perfectus_Reporting";

        private string _provider = null;
        private string _dbkey = null;
        private string _databaseName = null;
        private string _serverName = null;
        private string _username = null;
        private string _userpassword = null;
        private string _path = null;
        private string _release = null;


        /// <summary>
        /// Called before the database upgrade routine is called. Sets ups optional parameters of the installed/upgraded database.
        /// </summary>
        /// <param name="dbkey">Either core or Reporting database. Used for UI purposes</param>
        /// <param name="path">Path to sql scripts</param>
        /// <param name="release">Release or Debug (Obsolete)</param>
        public void Start( string provider, string dbkey, string databaseName, string serverName, string username, string userpassword, string path, string release )
        {
            _provider = provider;
            _dbkey = dbkey;
            _databaseName = databaseName;
            _serverName = serverName;
            _username = username;
            _userpassword = userpassword;
            _path = path;
            _release = release;
        }

        /// <summary>
        /// Runs a set of wizard actions to connect to and install/upgrade database
        /// </summary>
        /// <returns></returns>
        public Utils.WizardAction DoAction(  )
        {
            // The following code only supports Sql Server for now.
            if( _provider != "SqlServer" )
                return Utils.WizardAction.Success;

            using( Connect connect = new Connect( _dbkey, _path, _release ) )
            {
                // Configure the default database name.
                if( !String.IsNullOrEmpty( _databaseName ) )
                    connect.ProposedDatabaseName = _databaseName;
                else if( _dbkey == CoreDatabaseKey )
                    connect.ProposedDatabaseName = _proposedCoreDatabaseName;
                else if( _dbkey == ReportingDatabaseKey )
                    connect.ProposedDatabaseName = _proposedReportingDatabaseName;

                if( !String.IsNullOrEmpty( _serverName ) )
                    connect.ServerName = _serverName;
                connect.UserName = _username;
                connect.UserPassword = _userpassword;

                connect.BringToFront( );
                // 25/6/09 : Needed for Vista.....
                InteropWrapper.MakeTopMost( connect );

                // Display dialog and get return result.
                DialogResult sqlConnectResult = connect.ShowDialog( );

                if( sqlConnectResult == DialogResult.Cancel )
                    return Utils.WizardAction.Cancel;

                else if( sqlConnectResult != DialogResult.Ignore )
                {
                    SqlInstallWrapper wrapper = new SqlInstallWrapper( 0, _dbkey, _path, _release );

                    wrapper.DatabaseName = connect.DatabaseName;
                    wrapper.WillUpgrade = connect.WillUpgrade;
                    wrapper.WillCreate = connect.WillCreate;
                    wrapper.WillUpgradeFrom = connect.WillUpgradeFrom;
                    wrapper.IsWindowsAuth = connect.IsWindowsAuth;
                    wrapper.ServerName = connect.ServerName;
                    wrapper.LoginName = connect.LoginName;
                    wrapper.Password = connect.Password;
                    wrapper.SqlAction = connect.SqlAction;

                    // If the installer specified a name and/or password, we use that.
                    string username = String.IsNullOrEmpty( connect.UserName ) ? _defaultUserName : connect.UserName;
                    string userpassword = String.IsNullOrEmpty( connect.UserPassword ) ? _defaultUserPassword : connect.UserPassword;
                    wrapper.UserName = username;
                    wrapper.UserPassword = userpassword;

                    BackgroundWorkerBase[ ] wrappers = new BackgroundWorkerBase[ ] { wrapper };

                    using( ServiceProgressDialog progressDialog = new ServiceProgressDialog( wrappers ) )
                    {
                        progressDialog.Start( );
                        progressDialog.BringToFront( );
                        // 25/6/09 : Needed for Vista.....
                        InteropWrapper.MakeTopMost( progressDialog );

                        DialogResult drWrapper;

                        try
                        {
                            drWrapper = progressDialog.ShowDialog( );
                        }
                        catch( Exception ex )
                        {
                            if( ex.InnerException != null )
                                MessageBox.Show( ex.InnerException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                            else
                                MessageBox.Show( ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );

                            drWrapper = DialogResult.Abort;
                        }

                        // 25/6/09 : Needed for Vista.....
                        InteropWrapper.MakeNormal( progressDialog );

                        if( drWrapper != DialogResult.OK )
                        {
                            // Recursively call this function again to give the installer another go.
                            this.DoAction( );
                        }
                        else
                        {
                            _databaseName = connect.DatabaseName;
                            _serverName = connect.ServerName;
                            _username = username;
                            _userpassword = userpassword;
                        }
                    } // using ServiceProgressDialog
                }
            } // using Forms.Connect

            return Utils.WizardAction.Success;
        }

        /// <summary>
        /// Called after the database has completed a successful install. Returns the parameters of the installed/upgraded database
        /// </summary>
        public void End( out string databaseName, out string serverName, out string username, out string userpassword )
        {
            databaseName = _databaseName;
            serverName = _serverName;
            username = _username;
            userpassword = _userpassword;
        }
    }
}
