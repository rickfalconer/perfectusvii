using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Install
{
    public struct UpgradeScript
    {
        private int from;
        private string script;
        private bool transaction;
        private string notes;

        public int From
        {
            get { return from; }
            set { from = value; }
        }

        public string Script
        {
            get { return script; }
            set { script = value; }
        }

        public bool Transaction
        {
            get { return transaction; }
            set { transaction = value; }
        }

        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
    }
}