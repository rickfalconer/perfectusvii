using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Perfectus.Library.Install
{
    /// <summary>
    /// Custom Exception to handle user cancellation of the backround worker.
    /// </summary>
    public class CancelException : System.ApplicationException
    {
        public CancelException( ) : base( )
        {     }
    }
    
    /// <summary>
    /// Summary description for SqlInstallWrapper.
	/// </summary>
    public class SqlInstallWrapper : BackgroundWorkerBase
	{
		private string message = string.Empty;
		private string databaseName;
		private bool willUpgrade;
		private Sql sql;
        private bool willCreate;
        private Int32 willUpgradeFrom;
        private bool isWindowsAuth;
        private string serverName;
        private string loginName;
        private string password;
        private Utils.SqlAction sqlAction;
        // This is the Perfectus application user.
        private string userName = string.Empty;
        private string userPassword = string.Empty;

		public string DatabaseName
		{
			get { return databaseName; }
			set { databaseName = value; }
		}

		public bool WillUpgrade
		{
			get { return willUpgrade; }
			set { willUpgrade = value; }
		}

		public bool WillCreate
		{
			get { return willCreate; }
			set { willCreate = value; }
		}

		public Int32 WillUpgradeFrom
		{
			get { return willUpgradeFrom; }
			set { willUpgradeFrom = value; }
		}

        public Utils.SqlAction SqlAction
        {
            get { return sqlAction; }
            set { sqlAction = value; }
        }

		public bool IsWindowsAuth
		{
			get { return isWindowsAuth; }
			set { isWindowsAuth = value; }
		}

		public string ServerName
		{
			get { return serverName; }
			set { serverName = value; }
		}

		public string LoginName
		{
			get { return loginName; }
			set { loginName = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		public override string Name
		{
			get { return serverName; }
		}

		public override string Message
		{
			get { return message; }
		}

		public override bool ProvidesProgress
		{
			get { return true; }
		}

        // This is the Perfectus application user's name.
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        // This is the Perfectus application user's password.
        public string UserPassword
        {
            get { return userPassword; }
            set { userPassword = value; }
        }
        
        
        
        public SqlInstallWrapper( int index, string dbkey, string path, string version ) : base( index )
		{
			sql = new Sql( dbkey,  path, version );
		}

		public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
		{
            this.Worker = worker;

            // Note. We don't catch any exceptions here because the we want the UI to catch them and display
            // the appropriate information to the user.
			string systemConnectionString;
			if (isWindowsAuth)
			{
				systemConnectionString = Utils.GetConnectionString(serverName, "master");
			}
			else
			{
				systemConnectionString = Utils.GetConnectionString(serverName, loginName, password, "master");
			}

			using (SqlConnection conn = new SqlConnection(systemConnectionString))
			{
				conn.Open();
				SqlTransaction transaction = conn.BeginTransaction("installer");

                try
                {
                    // The following actions always do the PreInstall as this currently checks the user exists and
                    // the user has the correct permissions.
                    switch( sqlAction )
                    {
                        case Utils.SqlAction.IsCurrent:
                            DoPreInstall( conn, transaction, systemConnectionString );
                            // DB is up to date - do nothing
                            break;
                        case Utils.SqlAction.CreateAndInstall:
                            DoCreate( conn, transaction, systemConnectionString );
                            DoPreInstall( conn, transaction, systemConnectionString );
                            DoInstall( conn, transaction, systemConnectionString );
                            break;
                        case Utils.SqlAction.Install:
                            DoPreInstall( conn, transaction, systemConnectionString );
                            DoInstall( conn, transaction, systemConnectionString );
                            break;
                        case Utils.SqlAction.UpgradeSupported:
                            DoPreInstall( conn, transaction, systemConnectionString );
                            DoUpgrade( conn, transaction, systemConnectionString, willUpgradeFrom );
                            break;
                        case Utils.SqlAction.UpgradeNotSupported:
                            DoPreInstall( conn, transaction, systemConnectionString );
                            DoInstall( conn, transaction, systemConnectionString );
                            break;
                    }

                    // First check the process has not had a cancel request.
                    if( this.Worker.CancellationPending == true )
                        throw new CancelException( );

                    transaction.Commit( );
                }

                // Catch requests to stop processing...
                catch( CancelException )
                {
                    // Rollback transaction and throw exception to be caught by UI.
                    if( transaction.Connection != null )
                        transaction.Rollback( "installer" );

                    // Set the flag so that the callback routine is notified.
                    e.Cancel = true;
                }
                catch( Exception ex )
                {
                    // Rollback transaction and throw exception to be caught by UI.
                    if( transaction.Connection != null )
                        transaction.Rollback( "installer" );

                    // We need to let the error happen unhandled, and then the backgroundworker will take 
                    // care of getting it to the RunWorkerCompleted() handler.
                    throw new Exception( ex.Message + ".  " + ( ( ex.InnerException != null ) ? ex.InnerException.Message : string.Empty) );
                }
			}
		}
		
		public void DoCreate(SqlConnection conn, SqlTransaction transaction, string systemConnectionString )
		{
            bool isTransaction;
            string sqlScript = sql.GetCreateSql( databaseName, out isTransaction );
            this.message = string.Format( "Creating {0} on {1}", databaseName, serverName );

            if( isTransaction )
            {
                ProcessSql( conn, transaction, sqlScript );
            }
            else
            {
                using( SqlConnection conn2 = new SqlConnection( systemConnectionString ) )
                {
                    conn2.Open( );
                    ProcessSql( conn2, null, sqlScript );
                }
            }
		}

		public void DoUpgrade(SqlConnection conn, SqlTransaction transaction, string systemConnectionString,  Int32 oldVersion)
		{
            bool currentDBVersionMatched = false;

            SortedList<int, UpgradeScript> upgradeScripts = sql.GetUpgradeSqlScripts( databaseName, oldVersion );
            this.message = string.Format( "Upgrading {0} on {1}", databaseName, serverName );

            // Apply the upgrade scripts in sequence
            foreach( KeyValuePair<int, UpgradeScript> sqlScript in upgradeScripts )
            {
                UpgradeScript upgradeScript = (UpgradeScript)sqlScript.Value;

                try
                {
                    this.message = string.Format( "Applying upgrade script: {0}", upgradeScript.Notes );

                    if( oldVersion == upgradeScript.From )
                    {
                        currentDBVersionMatched = true;
                    }

                    // Only start applying scripts from the current version (don't start if there is no match with the DB version sequence)
                    if( currentDBVersionMatched )
                    {
                        ProcessSql( conn, transaction, upgradeScript.Script );
                    }
                }

                 // Catch requests to stop processing...
                catch( CancelException )
                {
                    throw new CancelException( );
                }
                catch( Exception ex )
                {
                    throw new Exception( string.Format( "Error Upgrading {0} on {1}, script: {2}", databaseName, serverName, upgradeScript.Notes ), ex );
                }
            }
		}

		public void DoInstall(SqlConnection conn, SqlTransaction transaction, string systemConnectionString )
		{
            bool isTransaction;

            this.message = string.Format( "Installing {0} objects in {1}", databaseName, serverName );

            string sqlScript = sql.GetInstallSql( databaseName, out isTransaction );
            if( isTransaction )
            {
                ProcessSql( conn, transaction, sqlScript );
            }
            else
            {
                using( SqlConnection conn2 = new SqlConnection( systemConnectionString ) )
                {
                    conn2.Open( );
                    ProcessSql( conn2, null, sqlScript );
                }
            }
		}

		private void DoPreInstall(SqlConnection conn, SqlTransaction transaction, string systemConnectionString )
		{
            // We allow the user to enter a user name and password that Perfectus must login as. If they select a new user we create the 
            // user with the specified password and attach them to the Perfectus DB. If they select an existing user, we check that the 
            // password is valid. The only way I know to test the user and password is correct is to attempt to create a DB connection.
            // Hence we first need to test whether the user exists in this code, and not from within the sql scripts stored in the config
            // files.
            using( SqlConnection conntest1 = new SqlConnection( systemConnectionString ) )
            {
                conntest1.Open( );
                // Add Sql script to test whether the user exists.			    
                SqlCommand cmd = new SqlCommand( "SELECT COUNT(*) FROM master.dbo.syslogins WHERE name = @name", conntest1 );
                SqlParameter param = new SqlParameter( "@name", SqlDbType.NVarChar );
                param.Value = UserName;
                cmd.Parameters.Add( param );

                if( Convert.ToInt16( cmd.ExecuteScalar( ) ) == 1 )
                {
                    // The user already exists, hence create new connection using sql server authentication for the user name and password to test they are correct.
                    using( SqlConnection conntest2 = new SqlConnection( Utils.GetConnectionString( serverName, UserName, UserPassword, "master" ) ) )
                    {
                        // This will throw an exception if the password is incorrect.
                        conntest2.Open( );
                    }
                }
            }

            // Continue as normal to maybe install user and assign rights etc.....            
            bool isTransaction;
            string sqlScript = sql.GetPreInstallSql( databaseName, userName, userPassword, out isTransaction );
            if( isTransaction )
            {
                ProcessSql( conn, transaction, sqlScript );
            }
            else
            {
                using( SqlConnection conn2 = new SqlConnection( systemConnectionString ) )
                {
                    conn2.Open( );
                    ProcessSql( conn2, null, sqlScript );
                }
            }
		}

		private void ProcessSql(SqlConnection conn, SqlTransaction transaction, string query)
		{
            // split sql into batches
			string[] batches = Regex.Split(query, @"\w*GO\w*($|\r)");
			SqlCommand cmd = new SqlCommand();
			cmd.Connection = conn;
			cmd.Transaction = transaction;
			cmd.CommandType = CommandType.Text;
			for(int i = 0; i < batches.Length; i++)
			{
				if (batches[i].Trim().Length > 0)
				{
                    // First check the process has not had a cancel request.
                    if( this.Worker.CancellationPending )
                        throw new CancelException( );

                    // Publish our progress
                    double progress = ((double)i / (double)(batches.Length)) * 100;
					int prog = (int)(Math.Round(progress));
					this.Worker.ReportProgress(prog, message);

                    // Execute the sql command
					cmd.CommandText = batches[i];
					cmd.ExecuteNonQuery();
				}
			}
		}
	}
}