using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Perfectus.Library.Install
{
	public class ServiceProgressDialog : Form
	{
		private IContainer components = null;
        private Button btnClose;
        private ServiceProgressControl[] meters;
        private BackgroundWorkerBase[] workers;
        private int runningMeterCount;

        // These delegates enable asynchronous calls for setting the value properties on given controls.
        delegate void SetTextCallback(string text);
        delegate void ThreadedClose();

        public ServiceProgressControl[] Meters
        {
            get { return meters; }
        }

		public ServiceProgressDialog()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		public ServiceProgressDialog(BackgroundWorkerBase[] workers) : this()
		{
            this.workers = workers;
			meters = new ServiceProgressControl[workers.Length];
			runningMeterCount = meters.Length;

			int y = 0;
			for (int i = 0; i < workers.Length; i++)
			{
				ServiceProgressControl meter = new ServiceProgressControl(workers[i]);
                meter.Complete += new EventHandler<RunWorkerCompletedEventArgs>( workerComplete );
				meters[i] = meter;
				Controls.Add(meter);
				meter.Top = y;
				meter.Left = 0;
				meter.Width = Width;
				y += meter.Height;
			}
			this.Height = y + 67;
		}

        public void Start()
        {
            foreach (Control meter in Controls)
            {
                if (meter is ServiceProgressControl)
                {
                    ((ServiceProgressControl)meter).RunWorker();
                }
            }
        }

        private void workerComplete( object sender, RunWorkerCompletedEventArgs e )
        {
            runningMeterCount--;
            if (runningMeterCount == 0)
            {
                if( e.Error != null )
                {
                    // If we have an error, leave this form open. The progress 
                    // control would have displayed the error.
                    SetCloseText( "Cl&ose" );
                    this.btnClose.Enabled = true;
                }
                else if( e.Cancelled )
                {
                    // The user has cancelled, abort and close.
                    this.DialogResult = DialogResult.Cancel;
                    HandleFormClose( );
                }
                else
                {
                    // This will close this window and send back an OK response.
                    this.DialogResult = DialogResult.OK;
                    HandleFormClose( );
                }
            }
        }

        /// <summary>
        ///     Sets the value of the close button asynchronously using the invoke method otherwise
        ///     synchronously using the normal .text = "";
        /// </summary>
        /// <param name="text">The button text to be set.</param>
        private void SetCloseText(string text)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID 
            // of the creating thread.

            // If these threads are different, it returns true.
            if (this.btnClose.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetCloseText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.btnClose.Text = text;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            bool CanClose = true;

            foreach (Control ctrl in Controls)
            {
                if( (ctrl is ServiceProgressControl) && ( ( ServiceProgressControl )ctrl ).ProgressWorker.Worker.IsBusy )
                {
                    // Cancel this process.
                    ((ServiceProgressControl)ctrl).CancelProcessing();

                    // We don't want to close this until the process is finished...
                    CanClose = false;
                }
            }

            // Disable the button as we need to wait until the processes stop.
            this.btnClose.Enabled = false;

            // Only close this once all processes are complete.
            if( CanClose )
                HandleFormClose( );
        }

        /// <summary>
        ///     Handles the closing of the form asynchronously using the invoke method otherwise
        ///     synchronously using the normal close
        /// </summary>
        private void HandleFormClose()
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID 
            // of the creating thread.

            // If these threads are different, it returns true.
            if (this.InvokeRequired)
            {
                try
                {
                    ThreadedClose d = new ThreadedClose(HandleFormClose);
                    this.Invoke(d);
                }
                catch
                {
                    // Just close the form
                    this.Close();
                }
            }
            else 
            {
                this.Close();
            }
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ServiceProgressDialog ) );
            this.btnClose = new System.Windows.Forms.Button( );
            this.SuspendLayout( );
            // 
            // btnClose
            // 
            resources.ApplyResources( this.btnClose, "btnClose" );
            this.btnClose.Name = "btnClose";
            this.btnClose.Click += new System.EventHandler( this.btnClose_Click );
            // 
            // ServiceProgressDialog
            // 
            resources.ApplyResources( this, "$this" );
            this.Controls.Add( this.btnClose );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ServiceProgressDialog";
            this.ResumeLayout( false );

		}

		#endregion
	}
}