using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Perfectus.Library.Install
{
	/// <summary>
	/// Summary description for Connect.
	/// </summary>
	public class Connect : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtServerName;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.RadioButton rdoWindowsAuth;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtLogin;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.RadioButton rdoSqlAuth;
		private CheckBox InstallDatabaseCheckBox;
        private bool willUpgrade = false;
		private Int32 willUpgradeFrom;
		private bool willCreate = true;
		private string databaseName = null;
		private bool isWindowsAuth = true;
        private string installText = "&Connect";
        private string skipInstallText = "&Ok";
        private string proposedDatabaseName;
        private Utils.SqlAction sqlAction;
        // The following strings are set by the calling custom action. The path & release are for Devharness only.
        private string dbkey = null;
        private string path = null;
        private string release = null;
        
        // This is the Perfectus application user.
        private string userName = string.Empty;
        private string userPassword = string.Empty;
        
		public string ServerName
		{
            get { return txtServerName.Text; }
    		set { txtServerName.Text = value; }
    }

		public string LoginName
		{
			get { return txtLogin.Text; }
		}

		public string Password
		{
			get { return txtPassword.Text; }
		}

        // This is the Perfectus application user's name.
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        // This is the Perfectus application user's password.
        public string UserPassword
        {
            get { return userPassword; }
            set { userPassword = value; }
        }

        public bool IsWindowsAuth
		{
			get { return isWindowsAuth; }
		}

		public bool WillUpgrade
		{
			get { return willUpgrade; }
		}

		public Int32 WillUpgradeFrom
		{
			get { return willUpgradeFrom; }
		}

        public Utils.SqlAction SqlAction
        {
            get { return sqlAction; }
            set { sqlAction = value; }
        }

		public bool WillCreate
		{
			get { return willCreate; }
		}

		public string DatabaseName
		{
			get { return databaseName; }
		}

        public string ProposedDatabaseName
        {
            set { this.proposedDatabaseName = value; }
        }

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private string connectionString = null;

		public string ConnectionString
		{
			get { return connectionString; }
		}

        public Connect( string thedbkey, string thepath, string therelease )
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            this.dbkey = thedbkey;
            this.path = thepath;
            this.release = therelease;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Connect));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rdoWindowsAuth = new System.Windows.Forms.RadioButton();
            this.rdoSqlAuth = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.InstallDatabaseCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(24, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(450, 56);
            this.label1.TabIndex = 0;
            this.label1.Text = "Perfectus stores some of its data in a Microsoft SQL Server database. Please ente" +
                "r the name of the server or instance where you would like to store your data.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(56, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "SQL Server or Instance";
            // 
            // txtServerName
            // 
            this.txtServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerName.Location = new System.Drawing.Point(232, 114);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(186, 20);
            this.txtServerName.TabIndex = 2;
            this.txtServerName.Text = "localhost";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(24, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(442, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "The authentication information you enter here is only used by this setup applicat" +
                "ion.";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(56, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Connect using:";
            // 
            // rdoWindowsAuth
            // 
            this.rdoWindowsAuth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoWindowsAuth.Checked = true;
            this.rdoWindowsAuth.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdoWindowsAuth.Location = new System.Drawing.Point(96, 200);
            this.rdoWindowsAuth.Name = "rdoWindowsAuth";
            this.rdoWindowsAuth.Size = new System.Drawing.Size(322, 24);
            this.rdoWindowsAuth.TabIndex = 5;
            this.rdoWindowsAuth.TabStop = true;
            this.rdoWindowsAuth.Text = "&Windows authentication";
            this.rdoWindowsAuth.CheckedChanged += new System.EventHandler(this.rdoWindowsAuth_CheckedChanged);
            // 
            // rdoSqlAuth
            // 
            this.rdoSqlAuth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rdoSqlAuth.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rdoSqlAuth.Location = new System.Drawing.Point(96, 232);
            this.rdoSqlAuth.Name = "rdoSqlAuth";
            this.rdoSqlAuth.Size = new System.Drawing.Size(330, 24);
            this.rdoSqlAuth.TabIndex = 6;
            this.rdoSqlAuth.Text = "S&QL authentication";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(128, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Login name:";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(128, 288);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Password:";
            // 
            // txtLogin
            // 
            this.txtLogin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogin.Enabled = false;
            this.txtLogin.Location = new System.Drawing.Point(232, 264);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(186, 20);
            this.txtLogin.TabIndex = 9;
            // 
            // txtPassword
            // 
            this.txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(232, 288);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(186, 20);
            this.txtPassword.TabIndex = 10;
            // 
            // btnConnect
            // 
            this.btnConnect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnConnect.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnConnect.Location = new System.Drawing.Point(324, 333);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "&Connect";
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(412, 333);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Ca&ncel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // InstallDatabaseCheckBox
            // 
            this.InstallDatabaseCheckBox.AutoSize = true;
            this.InstallDatabaseCheckBox.Checked = true;
            this.InstallDatabaseCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.InstallDatabaseCheckBox.Location = new System.Drawing.Point(59, 75);
            this.InstallDatabaseCheckBox.Name = "InstallDatabaseCheckBox";
            this.InstallDatabaseCheckBox.Size = new System.Drawing.Size(102, 17);
            this.InstallDatabaseCheckBox.TabIndex = 13;
            this.InstallDatabaseCheckBox.Text = "Install Database";
            this.InstallDatabaseCheckBox.UseVisualStyleBackColor = true;
            this.InstallDatabaseCheckBox.CheckedChanged += new System.EventHandler(this.InstallDatabaseCheckBox_CheckedChanged);
            // 
            // Connect
            // 
            this.AcceptButton = this.btnConnect;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(498, 368);
            this.Controls.Add(this.InstallDatabaseCheckBox);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.rdoWindowsAuth);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rdoSqlAuth);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Connect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Connect";
            this.Load += new System.EventHandler(this.Connect_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private void Connect_Load(object sender, System.EventArgs e)
		{
            // Set some text labels based on database type.
            if( dbkey == DatabaseConfiguration.CoreDatabaseKey )
            {
                this.Text = "Perfectus Core Database";
                this.InstallDatabaseCheckBox.Text = "Install Perfectus Core Database";
            }
            else if( dbkey == DatabaseConfiguration.ReportingDatabaseKey )
            {
                this.Text = "Perfectus Reporting Database";
                this.InstallDatabaseCheckBox.Text = "Install Perfectus Reporting Database";
            }
		}

		private void rdoWindowsAuth_CheckedChanged(object sender, System.EventArgs e)
		{		
			txtLogin.Enabled = ! rdoWindowsAuth.Checked;
			txtPassword.Enabled = txtLogin.Enabled;
			isWindowsAuth = rdoWindowsAuth.Checked;
		}

		private void btnConnect_Click(object sender, System.EventArgs e)
		{
			try
			{
                if (InstallDatabaseCheckBox.Checked)
                {
                    Cursor = Cursors.WaitCursor;
                    string connectionString;
                    if (rdoWindowsAuth.Checked)
                    {
                        connectionString = Utils.GetConnectionString(txtServerName.Text, "master");
                    }
                    else
                    {
                        connectionString = Utils.GetConnectionString(txtServerName.Text, txtLogin.Text, txtPassword.Text, "master");
                    }

                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("sp_databases");
                        cmd.Connection = conn;
                        ChooseDatabase chooseDatabase = new ChooseDatabase( dbkey, path, release, proposedDatabaseName );
                        chooseDatabase.ServerName = txtServerName.Text;
                        chooseDatabase.IsWindowsAuth = rdoWindowsAuth.Checked;
                        chooseDatabase.LoginName = txtLogin.Text;
                        chooseDatabase.Password = txtPassword.Text;
                        chooseDatabase.UserName = this.UserName;
                        chooseDatabase.UserPassword = this.UserPassword;
                        
                        using (SqlDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string dbname = rdr.GetString(0);

                                // Ignore system databases
                                if( String.IsNullOrEmpty( dbname ) || dbname == "master" || dbname == "model" || dbname == "tempdb" || dbname == "msdb" )
                                    continue;
                                
                                // Add db to dropdown list.
                                chooseDatabase.cbxExistingDatabaseName.Items.Add( dbname);
                            }

                            // Reload UI now that we have the list of databases
                            chooseDatabase.ResetUI( true );

                            DialogResult dr = chooseDatabase.ShowDialog();
                            // 25/6/09 : Needed for Vista.....
                            InteropWrapper.MakeTopMost( chooseDatabase );
                            if (dr == DialogResult.OK)
                            {
                                willUpgrade = chooseDatabase.WillUpgrade;
                                willUpgradeFrom = chooseDatabase.WillUpgradeFrom;
                                willCreate = chooseDatabase.WillCreate;
                                databaseName = chooseDatabase.DatabaseName;
                                sqlAction = chooseDatabase.SqlAction;
                                this.UserName = chooseDatabase.UserName;
                                this.UserPassword = chooseDatabase.UserPassword;
                                this.DialogResult = DialogResult.OK;
                            }
                            else
                            {
                                this.DialogResult = DialogResult.None;
                            }
                        }

                    }
                    this.connectionString = connectionString;
                }
                else
                {
                    this.DialogResult = DialogResult.Ignore;
                }
			}
			catch(Exception ex)
			{
				Cursor = Cursors.Default;
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				this.DialogResult = DialogResult.None;
			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

        void InstallDatabaseCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (InstallDatabaseCheckBox.Checked)
            {
                btnConnect.Text = installText;
            }
            else
            {
                btnConnect.Text = skipInstallText;
            }
        }

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}
