using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Perfectus.Library.Install
{
	/// <summary>
	/// Summary description for ChooseProvider.
	/// </summary>
	public class ChooseProvider : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.RadioButton rdoSql;
		private System.Windows.Forms.RadioButton rdoOracle;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		public string Provider
		{
			get
			{
				return rdoOracle.Checked ? "ORACLE" : "SQL";
			}
		}

		public ChooseProvider()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ChooseProvider));
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOk = new System.Windows.Forms.Button();
			this.rdoSql = new System.Windows.Forms.RadioButton();
			this.rdoOracle = new System.Windows.Forms.RadioButton();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnCancel.Location = new System.Drawing.Point(290, 138);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Ca&ncel";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnOk.Location = new System.Drawing.Point(202, 138);
			this.btnOk.Name = "btnOk";
			this.btnOk.TabIndex = 2;
			this.btnOk.Text = "&OK";
			// 
			// rdoSql
			// 
			this.rdoSql.Checked = true;
			this.rdoSql.Location = new System.Drawing.Point(121, 64);
			this.rdoSql.Name = "rdoSql";
			this.rdoSql.Size = new System.Drawing.Size(136, 24);
			this.rdoSql.TabIndex = 0;
			this.rdoSql.TabStop = true;
			this.rdoSql.Text = "SQL Server";
			// 
			// rdoOracle
			// 
			this.rdoOracle.Location = new System.Drawing.Point(121, 96);
			this.rdoOracle.Name = "rdoOracle";
			this.rdoOracle.Size = new System.Drawing.Size(136, 24);
			this.rdoOracle.TabIndex = 1;
			this.rdoOracle.Text = "Oracle Database";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(17, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(344, 32);
			this.label1.TabIndex = 19;
			this.label1.Text = "Perfectus stores its data in a relational database. Please choose the database se" +
				"rver Perfectus will use.";
			// 
			// ChooseProvider
			// 
			this.AcceptButton = this.btnOk;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.btnCancel;
			this.ClientSize = new System.Drawing.Size(378, 176);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.rdoOracle);
			this.Controls.Add(this.rdoSql);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ChooseProvider";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Database Provider";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
