using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using Perfectus.Library.Install;

namespace Perfectus.Library.Install
{
	/// <summary>
	/// Summary description for ChooseDatabase.
	/// </summary>
	public class ChooseDatabase : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RadioButton rdoExistingDatabase;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.RadioButton rdoNewDatabase;
		internal System.Windows.Forms.ComboBox cbxExistingDatabaseName;
		private System.Windows.Forms.TextBox txtNewDatabaseName;
		private System.Windows.Forms.TextBox textBox1;
		private bool willUpgrade = false;
		private int willUpgradeFrom;
		private string databaseName = null;
		private bool willCreate = true;
		private string serverName = null;
		private string loginName = null;
		private string password = null;
		private bool isWindowsAuth;
		private bool unsupportedUpgrade = false;
        private  Utils.SqlAction sqlAction = Utils.SqlAction.CreateAndInstall;
        private Label databasetypelabel;
        private Panel panel1;
        private bool newDataBaseNameChanged = false;
        // This is the Perfectus application user.
        private string userName = string.Empty;
        private string userPassword = string.Empty;
        private Panel panel3;
        // The following strings are set by the calling custom action. The path & release are for Devharness only.
        private string dbkey = null;
        private string path = null;
        private string release = null;
        private Label label6;
        private Label label5;
        private TextBox userNameTextBox;
        private TextBox userPasswordTextBox1;
        private Label label1;
        private TextBox userPasswordTextBox2;
        private Label DatabaseAuthDescLabel;
        private Label label3;
        private Label label4;
        private Label label7;
                
		public string DatabaseName
		{
			get { return databaseName; }
		}

        // This is the Perfectus application user's name.
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        // This is the Perfectus application user's password.
        public string UserPassword
        {
            get { return userPassword; }
            set { userPassword = value; }
        }

        public bool WillCreate
		{
			get { return willCreate; }
		}

		public bool WillUpgrade
		{
			get { return willUpgrade; }
		}

		public Int32 WillUpgradeFrom
		{
			get { return willUpgradeFrom; }
		}
        
        public Utils.SqlAction SqlAction
        {
            get { return sqlAction; }
            set { sqlAction = value; }
        }

		public string LoginName
		{
			get { return loginName; }
			set { loginName = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		public bool IsWindowsAuth
		{
			get { return isWindowsAuth; }
			set { isWindowsAuth = value; }
		}

		public string ServerName
		{
			get { return serverName; }
			set { serverName = value; }
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        public ChooseDatabase( string thedbkey, string thepath, string therelease, string proposedDatabaseName )
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			txtNewDatabaseName.Text = proposedDatabaseName;
            this.dbkey = thedbkey;
            this.path = thepath;
            this.release = therelease;

            // 25/6/09 : Needed for Vista.....
            InteropWrapper.MakeTopMost( this );
            this.txtNewDatabaseName.Focus( );
        }

		public void ResetUI( bool initialise )
		{
            // Maybe default the new db name.
            if( initialise )
            {
                // If db by the same name already exists, add the '_' character on the end.
                string dbname = txtNewDatabaseName.Text;

                bool alreadyExists;
			    do
			    {
				    alreadyExists = false;
				    foreach(object o in cbxExistingDatabaseName.Items)
				    {
                        if( o.ToString( ).ToUpper( ) == dbname.ToUpper( ) )
					    {
						    alreadyExists = true;
						    break;
					    }
				    }
				    if (alreadyExists)
				    {
					    dbname += "_";
				    }
			    } while(alreadyExists);

                txtNewDatabaseName.Text = dbname;
                this.newDataBaseNameChanged = false; 
            }
                
            switch (sqlAction)
            {
                case Utils.SqlAction.IsCurrent:
                    databaseName = cbxExistingDatabaseName.Text;
                    textBox1.Text = string.Format("The database '{0}' on {1} contains the current Perfectus database version. No action required.", cbxExistingDatabaseName.Text, serverName);
                    break;
                case Utils.SqlAction.ExistingIsNewer:
                    databaseName = cbxExistingDatabaseName.Text;
                    textBox1.Text = string.Format( "The database '{0}' on {1} is a newer version than you are trying to install. Please install a newer version of perfectus or select another database.", cbxExistingDatabaseName.Text, serverName );
                    break;
                case Utils.SqlAction.CreateAndInstall:
                    databaseName = txtNewDatabaseName.Text;
                    textBox1.Text = string.Format("A new database called '{0}' will be created on {1}.", txtNewDatabaseName.Text, serverName);
                    break;
                case Utils.SqlAction.UpgradeSupported:
                    databaseName = cbxExistingDatabaseName.Text;
                    textBox1.Text = string.Format("The database '{0}' on {1} will be upgraded. Once upgraded, the database cannot be used by earlier versions of Perfectus.", cbxExistingDatabaseName.Text, serverName);
                    break;
                case Utils.SqlAction.Install:
                    databaseName = cbxExistingDatabaseName.Text;
                    textBox1.Text = string.Format("The database '{0}' on {1} will have the Perfectus tables and stored procedures added to it.", cbxExistingDatabaseName.Text, serverName);
                    break;
                case Utils.SqlAction.UpgradeNotSupported:
                    databaseName = cbxExistingDatabaseName.Text;
                    textBox1.Text = string.Format("The database '{0}' on {1} contains Perfectus data but cannot be upgraded to this version. Existing data will be lost.", cbxExistingDatabaseName.Text, serverName);
                    break;
                default:
                    textBox1.Text = string.Format( "There was an error in evaluating the database '{0}' on {1}. If this is an existing Perfectus database, please ensure the VersionInformation table is correct.", cbxExistingDatabaseName.Text, serverName );
                    break;
            }

			txtNewDatabaseName.Enabled = rdoNewDatabase.Checked;
			cbxExistingDatabaseName.Enabled = rdoExistingDatabase.Checked;
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChooseDatabase));
            this.rdoExistingDatabase = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.rdoNewDatabase = new System.Windows.Forms.RadioButton();
            this.cbxExistingDatabaseName = new System.Windows.Forms.ComboBox();
            this.txtNewDatabaseName = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.databasetypelabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.userPasswordTextBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.userPasswordTextBox2 = new System.Windows.Forms.TextBox();
            this.DatabaseAuthDescLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // rdoExistingDatabase
            // 
            this.rdoExistingDatabase.CausesValidation = false;
            this.rdoExistingDatabase.Location = new System.Drawing.Point(0, 20);
            this.rdoExistingDatabase.Name = "rdoExistingDatabase";
            this.rdoExistingDatabase.Size = new System.Drawing.Size(136, 33);
            this.rdoExistingDatabase.TabIndex = 2;
            this.rdoExistingDatabase.TabStop = true;
            this.rdoExistingDatabase.Text = "&Existing Database";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.Location = new System.Drawing.Point(393, 319);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 23);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Ca&ncel";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.Location = new System.Drawing.Point(305, 319);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(73, 23);
            this.btnOk.TabIndex = 13;
            this.btnOk.Text = "&OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdoNewDatabase
            // 
            this.rdoNewDatabase.CausesValidation = false;
            this.rdoNewDatabase.Checked = true;
            this.rdoNewDatabase.Location = new System.Drawing.Point(0, -6);
            this.rdoNewDatabase.Name = "rdoNewDatabase";
            this.rdoNewDatabase.Size = new System.Drawing.Size(136, 33);
            this.rdoNewDatabase.TabIndex = 0;
            this.rdoNewDatabase.TabStop = true;
            this.rdoNewDatabase.Text = "&New Database";
            this.rdoNewDatabase.CheckedChanged += new System.EventHandler(this.rdoNewDatabase_CheckedChanged);
            // 
            // cbxExistingDatabaseName
            // 
            this.cbxExistingDatabaseName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxExistingDatabaseName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxExistingDatabaseName.Enabled = false;
            this.cbxExistingDatabaseName.Location = new System.Drawing.Point(152, 29);
            this.cbxExistingDatabaseName.Name = "cbxExistingDatabaseName";
            this.cbxExistingDatabaseName.Size = new System.Drawing.Size(233, 21);
            this.cbxExistingDatabaseName.TabIndex = 3;
            this.cbxExistingDatabaseName.SelectedIndexChanged += new System.EventHandler(this.cbxExistingDatabaseName_SelectedIndexChanged);
            // 
            // txtNewDatabaseName
            // 
            this.txtNewDatabaseName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewDatabaseName.Location = new System.Drawing.Point(152, 3);
            this.txtNewDatabaseName.Name = "txtNewDatabaseName";
            this.txtNewDatabaseName.Size = new System.Drawing.Size(232, 20);
            this.txtNewDatabaseName.TabIndex = 1;
            this.txtNewDatabaseName.Text = "Perfectus";
            this.txtNewDatabaseName.TextChanged += new System.EventHandler(this.txtNewDatabaseName_TextChanged);
            this.txtNewDatabaseName.Validating += new System.ComponentModel.CancelEventHandler(this.txtNewDatabaseName_Validating);
            this.txtNewDatabaseName.Validated += new System.EventHandler(this.txtNewDatabaseName_Validated);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(377, 36);
            this.textBox1.TabIndex = 0;
            // 
            // databasetypelabel
            // 
            this.databasetypelabel.AutoSize = true;
            this.databasetypelabel.Location = new System.Drawing.Point(24, 125);
            this.databasetypelabel.Name = "databasetypelabel";
            this.databasetypelabel.Size = new System.Drawing.Size(127, 13);
            this.databasetypelabel.TabIndex = 2;
            this.databasetypelabel.Text = "Database Authentication.";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNewDatabaseName);
            this.panel1.Controls.Add(this.rdoNewDatabase);
            this.panel1.Controls.Add(this.cbxExistingDatabaseName);
            this.panel1.Controls.Add(this.rdoExistingDatabase);
            this.panel1.Location = new System.Drawing.Point(27, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(385, 56);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Location = new System.Drawing.Point(27, 68);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(3);
            this.panel3.Size = new System.Drawing.Size(385, 44);
            this.panel3.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(24, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Login Password:";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(24, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Login Name:";
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.userNameTextBox.Location = new System.Drawing.Point(128, 224);
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(223, 20);
            this.userNameTextBox.TabIndex = 8;
            // 
            // userPasswordTextBox1
            // 
            this.userPasswordTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.userPasswordTextBox1.Location = new System.Drawing.Point(128, 249);
            this.userPasswordTextBox1.Name = "userPasswordTextBox1";
            this.userPasswordTextBox1.PasswordChar = '*';
            this.userPasswordTextBox1.Size = new System.Drawing.Size(223, 20);
            this.userPasswordTextBox1.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 277);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Confirm Password:";
            // 
            // userPasswordTextBox2
            // 
            this.userPasswordTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.userPasswordTextBox2.Location = new System.Drawing.Point(128, 274);
            this.userPasswordTextBox2.Name = "userPasswordTextBox2";
            this.userPasswordTextBox2.PasswordChar = '*';
            this.userPasswordTextBox2.Size = new System.Drawing.Size(223, 20);
            this.userPasswordTextBox2.TabIndex = 12;
            // 
            // DatabaseAuthDescLabel
            // 
            this.DatabaseAuthDescLabel.AutoSize = true;
            this.DatabaseAuthDescLabel.Location = new System.Drawing.Point(30, 150);
            this.DatabaseAuthDescLabel.Name = "DatabaseAuthDescLabel";
            this.DatabaseAuthDescLabel.Size = new System.Drawing.Size(354, 13);
            this.DatabaseAuthDescLabel.TabIndex = 3;
            this.DatabaseAuthDescLabel.Text = "Enter the SQL login name that should be used by the Perfectus database:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(427, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "- If the login already exists in SQL Server, the installer will add it as a user " +
                "in the database.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(377, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "- If the login does not exist, the installer will create one with the specified n" +
                "ame.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(271, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "- The password entered must match your security policy.";
            // 
            // ChooseDatabase
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(494, 358);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.databasetypelabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.DatabaseAuthDescLabel);
            this.Controls.Add(this.userPasswordTextBox2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.userPasswordTextBox1);
            this.Controls.Add(this.userNameTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChooseDatabase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Choose Database";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChooseDatabase_FormClosing);
            this.Load += new System.EventHandler(this.ChooseDatabase_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        private void ChooseDatabase_Load( object sender, EventArgs e )
        {
            // Set some text labels based on database type.
            if( dbkey == DatabaseConfiguration.CoreDatabaseKey )
            {
                this.Text = "Choose Perfectus Core Database";
                this.DatabaseAuthDescLabel.Text = "Enter the SQL login name that should be used by the Perfectus database:";
            }
            else if( dbkey == DatabaseConfiguration.ReportingDatabaseKey )
            {
                this.Text = "Choose Perfectus Reporting Database";
                this.DatabaseAuthDescLabel.Text = "Enter the SQL login name that should be used by the Perfectus Reporting database:";
            }

            // Display the user name & password if we have one.
            if( !String.IsNullOrEmpty( UserName ) && !String.IsNullOrEmpty( UserPassword ) )
            {
                userNameTextBox.Text = UserName;
                userPasswordTextBox1.Text = UserPassword;
                userPasswordTextBox2.Text = UserPassword;
            }

            this.txtNewDatabaseName.Focus( );
            userNameTextBox.Focus( );
        }
        
        private void rdoNewDatabase_CheckedChanged( object sender, System.EventArgs e )
		{
            if (rdoNewDatabase.Checked)
            {
                willCreate = true;
                sqlAction = Utils.SqlAction.CreateAndInstall;
            }
            else 
            {
                willCreate = false;
                sqlAction = Utils.SqlAction.Install;

                checkDatabaseVersion( );
            }

			ResetUI( false );
		}

		private void txtNewDatabaseName_TextChanged(object sender, System.EventArgs e)
		{
            this.newDataBaseNameChanged = true; 
		}

        private void txtNewDatabaseName_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            // Check if db already exists.
            e.Cancel = !checkNewDBName( );
        }

        private void txtNewDatabaseName_Validated( object sender, EventArgs e )
        {
            if( this.newDataBaseNameChanged == true && rdoNewDatabase.Checked )
                ResetUI( false );
        }

        private void cbxExistingDatabaseName_SelectedIndexChanged( object sender, System.EventArgs e )
		{
            checkDatabaseVersion( );

            ResetUI( false );
		}

        private bool checkNewDBName( )
        {
            // Ignore unless checked.
            if( !rdoNewDatabase.Checked )
                return true;

            string dbname = txtNewDatabaseName.Text.ToUpper( );
            if( String.IsNullOrEmpty( dbname ) )
                return true;

            // Check if db already exists.
            foreach( object o in cbxExistingDatabaseName.Items )
            {
                if( o.ToString( ).ToUpper( ) == dbname )
                {
                    MessageBox.Show( "The database already exists. You must enter a unique database name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                    return false;
                }
            }
            return true;
        }

        private void checkDatabaseVersion(  )
        {
            // Ignore all this if we are installing a new db anyway
            if( rdoNewDatabase.Checked )
                return;

            // Ensure we have an existing db.
            if( String.IsNullOrEmpty( cbxExistingDatabaseName.Text ) )
                return;

            string connectionString;
            willUpgrade = false;
            unsupportedUpgrade = false;

            if( isWindowsAuth )
            {
                connectionString = Utils.GetConnectionString( serverName, cbxExistingDatabaseName.Text );
            }
            else
            {
                connectionString = Utils.GetConnectionString( serverName, loginName, password, cbxExistingDatabaseName.Text );
            }

            using( SqlConnection conn = new SqlConnection( connectionString ) )
            {
                conn.Open( );

                Sql sql = new Sql( dbkey, path, release );
                using( SqlCommand cmd = new SqlCommand( sql.GetDatabaseVersionCheckSql( ) ) )
                {
                    cmd.Connection = conn;
                    Int32 versionNo;
                    try
                    {
                        object o = cmd.ExecuteScalar( );

                        try
                        {
                            versionNo = Convert.ToInt32( o );
                        }
                        catch( InvalidCastException ex )
                        {
                            throw new Exception( string.Format( "The Sequence Version {0} in the database must be an int32.", o.ToString( ) ), ex );
                        }

                        sqlAction = sql.GetUpgradeType( versionNo );

                        switch( sqlAction )
                        {
                            case Utils.SqlAction.UpgradeSupported:
                                willUpgrade = true;
                                willUpgradeFrom = versionNo;
                                unsupportedUpgrade = false;
                                break;
                            case Utils.SqlAction.UpgradeNotSupported:
                                unsupportedUpgrade = true;
                                break;
                        }
                    }
                    catch( SqlException ex )
                    {
                        // The db has not version table, non Perfectus db.
                        if( ex.Number == 208 )
                        {
                            willUpgrade = false;

                            // We'll install the tables in this db.
                            sqlAction = Utils.SqlAction.Install;
                        }
                        else
                        {
                            MessageBox.Show( ex.Message );
                        }
                    }
                }

                conn.Close( );
                sql = null;
            }
        }

        /// <summary>
        /// User clicks the OK button.
        /// </summary>
        private void btnOk_Click( object sender, EventArgs e )
        {
            // Check if db already exists.
            if( !checkNewDBName( ) )
                return;

            this.DialogResult = DialogResult.OK;

            UserName = userNameTextBox.Text;
            UserPassword = userPasswordTextBox1.Text;
        }

        /// <summary>
        /// Form is closing, perform some validation.
        /// </summary>
        private void ChooseDatabase_FormClosing( object sender, FormClosingEventArgs e )
        {
            // Ensure we have the correct information.
            if ((DialogResult == DialogResult.OK))
            {
                // Ensure we don't have an error condition
                if (SqlAction == Utils.SqlAction.Error)
                {
                    MessageBox.Show("Please select a valid database action.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }

                // Check credentials.
                if ((String.IsNullOrEmpty(UserName) || String.IsNullOrEmpty(UserPassword)))
                {
                    MessageBox.Show("Both the user name and password must be entered.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                else if (userPasswordTextBox1.Text != userPasswordTextBox2.Text)
                {
                    MessageBox.Show("The user and confirmation passwords are not identical.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
            }
        }

	}
}