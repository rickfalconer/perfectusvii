using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Perfectus.Library.Install
{
    /// <summary>
	/// Summary description for ServiceProgressControl.
	/// </summary>
	public class ServiceProgressControl : UserControl
	{
        private BackgroundWorkerBase worker;
        private BackgroundWorker backgroundWorker;
		private ProgressBar progressBar1;
		private Label lblMessage;
		private ImageList imageList1;
		private PictureBox pictureBox1;
		private Label lblServerName;
		private Timer timer1;
		private IContainer components;

        public event EventHandler<RunWorkerCompletedEventArgs> Complete;

        // These delegates enable asynchronous calls for setting the value properties on given controls.
        delegate void SetTextCallback(string text);
        delegate void SetProgressCallback(int progressValue);
        delegate void SetProgressBarVisibilityCallback(bool visible);
        delegate void SetImageCallback(Image enabled);

        private enum ImageIcons
        { 
            Tick = 0,
            Error = 1,
            RightArrow = 2
        }

        private void OnComplete( RunWorkerCompletedEventArgs e )
        {
            if( Complete != null )
                Complete( this, e );
        }

        public BackgroundWorkerBase ProgressWorker
		{
			get { return worker; }
		}

		public ServiceProgressControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
            InitializeBackgroundWorker();

			// Set the 'arrow' picture.
            if (imageList1.Images != null && imageList1.Images.Count >= 3)
            {
                SetPictureBoxImage(imageList1.Images[(int)ImageIcons.RightArrow]);
            }

			// If we aren't getting real progress information, add a bit of randomness to the 
			// 'I can't believe it's not a progress bar' so they don't all march along exactly in step
			// and give the game away

			Random r = new Random(DateTime.Now.Second);
			timer1.Interval = timer1.Interval + (r.Next(0, 70)) * 20;
		}

        public ServiceProgressControl(BackgroundWorkerBase worker) : this()
		{
			this.worker = worker;
            SetServerNameText(worker.Name);
			SetMessageText(worker.Message);
		}

        private void InitializeBackgroundWorker()
        {
            // Hook up the workers events
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
        }

        public void RunWorker()
        {
            backgroundWorker.RunWorkerAsync();
        }

        public void CancelProcessing()
        {
            backgroundWorker.CancelAsync();
        }

        private void UpdateProgressBar(int progress)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressCallback d = new SetProgressCallback(UpdateProgressBar);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                timer1.Enabled = false;

                // ensure the progress value is valid
                if (progress < 0 || progress > this.progressBar1.Maximum)
                {
                    progress = 0;
                }

                this.progressBar1.Value = progress;
            }
        }

        private void UpdateProgressBarVisibility(bool visible)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarVisibilityCallback d = new SetProgressBarVisibilityCallback(UpdateProgressBarVisibility);
                this.Invoke(d, new object[] { visible });
            }
            else
            {
                progressBar1.Visible = visible;
            }
        }

        /// <summary>
        ///     Sets the value of the message label asynchronously using the invoke method otherwise
        ///     synchronously using the normal .text = "";
        /// </summary>
        /// <param name="message">The message to be set.</param>
        private void SetMessageText(string message)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.

            // If these threads are different, it returns true.
            if (this.lblMessage.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetMessageText);
                this.Invoke(d, new object[] { message });
            }
            else
            {
                this.lblMessage.Text = message;
            }
        }

        /// <summary>
        ///     Sets the value of the server name label asynchronously using the invoke method otherwise
        ///     synchronously using the normal .text = "";
        /// </summary>
        /// <param name="name">The server name to be set.</param>
        private void SetServerNameText(string name)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.

            // If these threads are different, it returns true.
            if (this.lblServerName.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetServerNameText);
                this.Invoke(d, new object[] { name });
            }
            else
            {
                this.lblServerName.Text = name;
            }
        }

        private void SetPictureBoxImage(Image image)
        {
            if (this.pictureBox1.InvokeRequired)
            {
                SetImageCallback d = new SetImageCallback(SetPictureBoxImage);
                this.Invoke(d, new object[] { image });
            }
            else
            {
                pictureBox1.Image = image;
            }
        }

        private void SetComplete( string message )
        {
            SetMessageText( message );

            if( imageList1.Images != null && imageList1.Images.Count > 0 )
                pictureBox1.Image = imageList1.Images[ ( int )ImageIcons.Tick ];

            UpdateProgressBar( 100 );
        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgressBar(e.ProgressPercentage);
            this.SetMessageText(e.UserState.ToString());
        }

        void backgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            if( e.Error != null )
            {
                // Change the interface to display the error message.
                this.SetMessageText( e.Error.Message );

                if( imageList1.Images != null && imageList1.Images.Count > 1 )
                    pictureBox1.Image = imageList1.Images[ ( int )ImageIcons.Error ];

                UpdateProgressBarVisibility( false );
                timer1.Enabled = false;
            }
            else if( e.Cancelled )
            {
                SetComplete( "User Cancelled" );
            }
            else
            {
                SetComplete( "Complete" );
            }

            // Pass this information onto our parent dialog
            OnComplete( e );
        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!worker.ProvidesProgress)
            {
                timer1.Start();
            }

            // Start the process, exceptions are caught by the background worker.
            worker.Start( backgroundWorker, e );
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.PerformStep();
            if (progressBar1.Value >= 100)
            {
                UpdateProgressBar(0);
            }
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof (ServiceProgressControl));
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.lblMessage = new System.Windows.Forms.Label();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.lblServerName = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
			this.SuspendLayout();
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
			// 
			// progressBar1
			// 
			this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar1.Location = new System.Drawing.Point(32, 45);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(386, 16);
			this.progressBar1.TabIndex = 0;
			// 
			// label1
			// 
			this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lblMessage.Location = new System.Drawing.Point(32, 28);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(386, 36);
			this.lblMessage.TabIndex = 1;
			this.lblMessage.Text = "label1";
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer) (resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new System.Drawing.Point(10, 11);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(16, 16);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// label2
			// 
			this.lblServerName.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.lblServerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte) (0)));
			this.lblServerName.Location = new System.Drawing.Point(32, 12);
			this.lblServerName.Name = "lblServerName";
			this.lblServerName.Size = new System.Drawing.Size(386, 15);
			this.lblServerName.TabIndex = 3;
			this.lblServerName.Text = "label2";
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1500;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// ServiceProgressControl
			// 
			this.Controls.Add(this.lblServerName);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.lblMessage);
			this.Name = "ServiceProgressControl";
			this.Size = new System.Drawing.Size(448, 64);
			this.ResumeLayout(false);
		}

		#endregion
    }
}