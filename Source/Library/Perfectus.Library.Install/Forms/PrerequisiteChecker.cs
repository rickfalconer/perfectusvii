using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Xml.XPath;
using Microsoft.Win32;

namespace Perfectus.Library.Install
{
    public partial class PrerequisiteChecker : Form
    {
        public enum CheckType { None, Check, Or, OSVersion, Registry, Assembly, File };
        public enum CheckResult { Success, Warning, Failure }; // NB This must match the list view's image list.

        // Simple helper class to support the listing of checks to be performed.
        public class CheckClass
        {
            public CheckType type = CheckType.None;
            public object[ ] paramList;
            public string displaytext;
            public string tooltiptext;
            public CheckResult result = CheckResult.Success;
            public List<CheckClass> checkList = new List<CheckClass>( );
            public bool optional = false;
            public bool complete = false;

            public CheckClass(  ) 
            {}

            public CheckClass( CheckType thetype )
            { type = thetype; }

            public void SetResult( bool success )
            {
                this.result= success ? CheckResult.Success : this.optional ? CheckResult.Warning : CheckResult.Failure;
            }

            public void SetResult( CheckResult value )
            {
                if( this.result.CompareTo( value ) == -1 )
                    this.result = value;
            }
        }


        // Parameter list
        // OSVersion Element.
        private const int PLATFORMID = 0;
        private const int VERSIONSTR = 1;
        private const int SERVICEPACK = 2;
        // Registry Keys
        private const int REGKEYID = 0;
        private const int REGKEYNAME = 1;
        private const int REGKEYVALUE = 2;
        // Assembly
        private const int ASMKEYID = 0;
        // File
        private const int FILEPATH = 0;
        private const int FILENAME = 1;
        private const int FILEREGKEY1 = 2;
        private const int FILEREGKEY2 = 3;
        private const int FILEVERSION = 4;

        // Contains list of checks to be performed.
        private CheckClass _rootCheckObj = new CheckClass( );
        private CheckClass _currentCheckObj = null;
        // Wait period between tests, just for visual effects.
        private const int WAITPERIOD = 200;
        // Name of product installing, must be provided via constructor.
        private string _productName = "None";
        
        public PrerequisiteChecker( )
        {
            InitializeComponent( );
        }

        public PrerequisiteChecker( string ProductName ) : this( )
        {
            _productName = ProductName;
        }

        /// <summary>
        /// Performs the necessary setup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrerequisiteChecker_Load( object sender, EventArgs e )
        {
            this.Text = _productName + " Setup";
            this.Icon = Properties.Resources.FormIcon;

            // Load the xml file which defines the requirements.
            if( !this.loadRequirementsFile( _productName ) )
                return;

            // The order of these images is important and must match the enumeration CheckResult.
            listviewimages.Images.Add( Properties.Resources.check2 );
            listviewimages.Images.Add( Properties.Resources.warning );
            listviewimages.Images.Add( Properties.Resources.delete2 );
            preListView.StateImageList = listviewimages;

            // Default button.
            this.AcceptButton = cancelbtn;

            // To enable the user to escape (by clicking the cancel button) the interface performs the checks using a timer.
            // Also to give the impression it is doing something of interest, we display text, wait for a period of  WAITPERIOD, 
            // and then actually do the test. As we have no idea how long the actual tests will take, using this timer should allow the
            // system to respond to user clicks between tests.
            start_Check( );

            // Set the period and wait.
            formtimer.Interval = WAITPERIOD;
            formtimer.Start( );
        }

        /// <summary>
        /// User clicks next. Check the prerequistes have passed and if not give a warning.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nextbtn_Click( object sender, EventArgs e )
        {
            if( GetResult( preListView.Items ) == CheckResult.Failure )
            {
                DialogResult result = MessageBox.Show(
                        "Your system does not meet the minimum requirements for installation, this could result in unpredictable \n" +
                        "behaviour when using Perfectus. It is recommended that you cancel the installation, install the prerequisites \n" + 
                        "listed in the Perfectus System Requirements document and then retry the installation. \n\n" + 
                        "Are you sure you want to continue the installation?",
                        "Prerequisite Failure Warning",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2 );

                switch( result )
                {
                    case DialogResult.Yes: this.DialogResult = DialogResult.Yes; break;
                    // QA Requested that 'No' stop the insallation, so we need to pass back 'Cancel' to simulate
                    // the user clicking the cancel button in the install wizard.
                    case DialogResult.No: this.DialogResult = DialogResult.Cancel; break;
                    case DialogResult.Cancel: return;
                }
            }
            else
                this.DialogResult = DialogResult.Yes;
            
            this.Close( );
        }

        /// <summary>
        /// User clicks back. Simply return.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backbtn_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.No;
            this.Close( );
        }

        /// <summary>
        /// Starts a particular check. All this means is that we update the display text and then wait a very small amount of time.
        /// </summary>
        private void start_Check( )
        {
            try
            {
                _currentCheckObj = null;

                // Find the next item to check. Because there are only going to be a few items, we simply
                // scan the list everytime checking for non-completed items.
                foreach( CheckClass checkobj in _rootCheckObj.checkList )
                {
                    if( checkobj.complete || (checkobj.type != CheckType.Check) )
                        continue;

                    // Ensure here and now we don't do this check again.
                    checkobj.complete = true;

                    _currentCheckObj = checkobj;
                    break;
                }
                
                // On start we load a temp item onto the interface list. The item represents the actual 
                // check we are performing.
                if( _currentCheckObj != null )
                {
                    ListViewItem item = new ListViewItem( );
                    item.StateImageIndex = -1;
                    item.Text = _currentCheckObj.displaytext;
                    preListView.Items.Add( item );
                    preListView.RedrawItems( 0, preListView.Items.Count - 1, false );
                }
                else
                {
                    // Stop execution and give feedback.
                    formtimer.Stop( );
                    nextbtn.Enabled = true;

                    // Obtain summary of all results.
                    CheckResult thefinalresult = GetResult( preListView.Items );

                    // Move focus to the failed item
                    if( thefinalresult != CheckResult.Success )
                    {
                        // Find and select first error.
                        foreach( ListViewItem item in preListView.Items )
                        {
                            if( item.StateImageIndex != (int)thefinalresult )
                                continue;

                            preListView.Focus( );
                            item.EnsureVisible( );
                            item.Selected = true;
                            item.Focused = true;
                            break;
                        }
                    }
                    else
                        // Change Default button.
                        this.AcceptButton = nextbtn;
                }
            }
            catch( Exception ex )
            {
                // Something went wrong. Stop timer and report.
                formtimer.Stop( );
                results.Text = ex.Message;
            }
        }

        /// <summary>
        /// Called very WAITPERIOD. Will process the current check and start the next.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void formtimer_Tick( object sender, EventArgs e )
        {
            if( _currentCheckObj == null )
                return;

            try
            {
                // Collection of checked item to be displayed.
                List<ListViewItem> checkItems = new List<ListViewItem>( );

                // Call the function to actually perform the check now!!
                PerformCheck( _currentCheckObj, checkItems );
                // Update the interface having completed the check.
                end_Check( checkItems );
                // Start the next check.
                start_Check( );
            }
            catch( Exception ex )
            {
                // Something went wrong. Stop timer and report.
                formtimer.Stop( );
                results.Text = ex.Message;
            }
        }

        /// <summary>
        /// End a particular check. All this means is that we update the display text.
        /// </summary>
        private void end_Check( List<ListViewItem> checkItems )
        {
            // Remove the last item on the list..., and add the actual checked items.
            if( preListView.Items.Count > 0 )
                preListView.Items.RemoveAt( preListView.Items.Count - 1 );
    
            // Add the new items.
            foreach( ListViewItem item in checkItems )
                preListView.Items.Add( item );

            // Force the interface to redraw itself.
            if( preListView.Items.Count > 0 )
                preListView.RedrawItems( 0, preListView.Items.Count - 1, false );
        }

        /// <summary>
        /// Performs  a recusive check on the parent's children.
        /// </summary>
        /// <param name="parent">The actual check class object that contains 1 or more checks to be performed.</param>
        /// <param name="checkitems">Will add a ListViewItem to this list.</param>
        /// <returns>Whether the check was successful or not.</returns>
        private bool PerformCheck( CheckClass parent, List<ListViewItem> checkitems )
        {
            if( parent == null )
                return true;

            // First test for conditional items, represented by the <Or> element.
            List<ListViewItem> conditionalitems = new List<ListViewItem>( );

            foreach( CheckClass child in parent.checkList )
            {
                switch( child.type )
                {
                    case CheckType.Or:
                        {
                            // Check we don't already have an Or condition that is valid.
                            if( ( conditionalitems.Count > 0 ) && ( GetResult( conditionalitems ) == CheckResult.Success ) )
                                continue;

                            // Recusively check the items below this child.
                            List<ListViewItem> tempitems = new List<ListViewItem>( );
                            PerformCheck( child, tempitems );

                            // If the item we checked is valid (or less invalid than what we currently have), load it into
                            // the conditional list to later maybe display.
                            if( (child.result == CheckResult.Success) || ( conditionalitems.Count == 0 ) ||
                                ( NumberSuccessItemsByOrder( tempitems ) > NumberSuccessItemsByOrder( conditionalitems ) ) )
                            {
                                // Throw away what we currently have and add the new items.
                                conditionalitems.Clear( );
                                conditionalitems.AddRange( tempitems );
                            }
                        }
                        break;

                    case CheckType.OSVersion :
                        child.SetResult( checkOperatingSystem( child ) );
                        parent.SetResult( child.result );
                        checkitems.Add( CreateListViewItem( child ) );
                        break;

                    case CheckType.Registry :
                        child.SetResult( checkRegistry( child ) );
                        parent.SetResult( child.result );
                        checkitems.Add( CreateListViewItem( child ) );
                        break;

                    case CheckType.Assembly :
                        child.SetResult( checkAssembly( child ) );
                        parent.SetResult( child.result );
                        checkitems.Add( CreateListViewItem( child ) );
                        break;

                    case CheckType.File :
                        child.SetResult( checkFile( child ) );
                        parent.SetResult( child.result );
                        checkitems.Add( CreateListViewItem( child ) );
                        break;

                    default: continue;
                }
            }

            // Add the conditional items to the list only at the end..
            if( conditionalitems.Count > 0 )
            {
                checkitems.AddRange( conditionalitems );
                parent.SetResult( this.GetResult( conditionalitems ) );
            }

            return (parent.result != CheckResult.Failure);
        }

        /// <summary>
        /// Performs a 'Registry' element check represented by the checkobj.
        /// </summary>
        /// <param name="checkobj">The checkclass object we are testing for</param>
        /// <returns></returns>
        private bool checkRegistry( CheckClass checkobj )
        {
            try
            {
                string key = (string)checkobj.paramList[ REGKEYID ];
                string name = (string)checkobj.paramList[ REGKEYNAME ];
                string value = (string)checkobj.paramList[ REGKEYVALUE ];

                // Look at registry and return the actual value.
                object regValue = Registry.GetValue( key, name, String.Empty );
                if( regValue == null )
                    return false;

                if( String.IsNullOrEmpty( name ) )
                    return true;

                 // Check the values if we have one.
                if( String.IsNullOrEmpty( value ) || ( regValue.ToString( ) == value ) )
                    return true;
            }
            catch
            {
                // Not much we can do here, but return false.
            }
            return false;
        }

        /// <summary>
        /// Performs a 'Assembly' element check represented by the checkobj.
        /// </summary>
        /// <param name="checkobj">The checkclass object we are testing for</param>
        /// <returns></returns>
        private bool checkAssembly( CheckClass checkobj )
        {
            try
            {
                // Performs assembly check. Will throw an exception if not found.
                Assembly asm = System.Reflection.Assembly.Load( (string)checkobj.paramList[ ASMKEYID ] );
                return ( asm != null );
            }
            catch
            {
                // Not much we can do here, but return false.
            }
            return false;
        }

        /// <summary>
        /// Performs a 'File' element check represented by the checkobj.
        /// </summary>
        /// <param name="checkobj">The checkclass object we are testing for</param>
        /// <returns></returns>
        private bool checkFile( CheckClass checkobj )
        {
            try
            {
                // Path
                string path = (string)checkobj.paramList[ FILEPATH ];
                if( String.IsNullOrEmpty( path ) && !String.IsNullOrEmpty( (string)checkobj.paramList[ FILEREGKEY1 ] ) )
                {
                    // Get path from registry.
                    object regValue = Registry.GetValue( (string)checkobj.paramList[ FILEREGKEY1 ], (string)checkobj.paramList[ FILEREGKEY2 ], null );
                    if( regValue == null )
                        return false;

                    path = regValue.ToString( );
                }

                // File name
                string name = (string)checkobj.paramList[ FILENAME ];
                if( String.IsNullOrEmpty( path ) || String.IsNullOrEmpty( name ) )
                    return false;

                // As long as the file version contains part of the specified version string, we're happy.
                System.Diagnostics.FileVersionInfo versioninfo = System.Diagnostics.FileVersionInfo.GetVersionInfo( Path.Combine( path, name ) );
                if( versioninfo.FileVersion.Contains( (string)checkobj.paramList[ FILEVERSION ] ) )
                    return true;
            }
            catch
            {
                // Not much we can do here, but return false.
            }
            return false;
        }

        /// <summary>
        /// Performs the operating system checks
        /// </summary>
        /// <param name="checkobj">The checkclass object we are testing for</param>
        /// <returns></returns>
        private bool checkOperatingSystem( CheckClass checkobj )
        {
            OperatingSystem obj = System.Environment.OSVersion;

            // Display the actual operating string version.
            checkobj.displaytext = obj.ToString( );

            // Create temp operating system object to test with.
            OperatingSystem checkos = new OperatingSystem( (PlatformID)Enum.Parse( typeof( PlatformID ),
                        (string)checkobj.paramList[ PLATFORMID ], true ), new Version( (string)checkobj.paramList[ VERSIONSTR ] ) );
            string servicePack = (string)checkobj.paramList[ SERVICEPACK ];
            
            bool success = ( obj.Platform == checkos.Platform );
            success = success && ( ( checkos.Version.Major < 0 ) || ( obj.Version.Major == checkos.Version.Major ) );
            success = success && ( ( checkos.Version.Minor < 0 ) || ( obj.Version.Minor == checkos.Version.Minor ) );
            success = success && ( ( checkos.Version.Build < 0 ) || ( obj.Version.Build == checkos.Version.Build ) );
            success = success && ( ( checkos.Version.Revision < 0 ) || ( obj.Version.Revision == checkos.Version.Revision ) );
            success = success && ( ( String.IsNullOrEmpty( servicePack ) ) || ( obj.ServicePack == servicePack ) );

            if( success )
                checkobj.tooltiptext = "The current operating system is supported.";

            return success;
        }

        /// <summary>
        /// Loads the system requirements xml file and creates necessary objects that must be checked.
        /// </summary>
        /// <param name="application">This represents the application that is being installed.</param>
        /// <returns>true if successfully loads file.</returns>
        private bool loadRequirementsFile( string application )
        {
            try
            {
                string requirementsfile = Path.Combine( Path.GetDirectoryName( Assembly.GetExecutingAssembly( ).CodeBase ), "PerfectusSystemRequirements.xml" );
                XPathDocument document = new XPathDocument( requirementsfile );
                XPathNavigator navigator = document.CreateNavigator( );

                // Navigate to the appropriate application node.
                XPathNavigator applicationnode = navigator.SelectSingleNode( "/SystemRequirements/Product[@Name='" + application + "']" );
                XPathNodeIterator nodes = applicationnode.SelectChildren( "Check", "" );

                // Don't check the attributes all exist as this file is edited by us and is expected to be correct.
                while( nodes.MoveNext( ) )
                {
                    CheckClass child = new CheckClass( CheckType.Check );
                    _rootCheckObj.checkList.Add( child );

                    child.displaytext = safeGetAttribute( nodes.Current, "display" );
                    child.tooltiptext = safeGetAttribute( nodes.Current, "tooltip" ); 

                    loadElement( nodes.Current, child );
                }
            }
            catch
            {
                // Missing or inaccessable requirements file.
                results.Text = "The system requirements xml file does not support the product " + application;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Constructs the CheckClass object for the xml node.
        /// </summary>
        /// <param name="node">The xml node</param>
        /// <param name="parent">The parent the newly created check class object must be attached to</param>
        private void loadElement( XPathNavigator node, CheckClass parent )
        {
            XPathNodeIterator nodes = node.SelectChildren( XPathNodeType.Element );

            while( nodes.MoveNext( ) )
            {
                CheckClass child = new CheckClass( );
                parent.checkList.Add( child );

                child.displaytext = safeGetAttribute( nodes.Current, "display" );
                child.tooltiptext = safeGetAttribute( nodes.Current, "tooltip" );
                child.optional = (safeGetAttribute( nodes.Current, "optional" ) == "true") ? true : false;
 
                switch( nodes.Current.Name )
                {
                    case "Check":
                        child.type = CheckType.Check;
                        break;
                    case "Or": 
                        child.type = CheckType.Or;
                        break;
                    case "OSVersion" :
                        child.type = CheckType.OSVersion;
                        child.paramList = new object[ 3 ];
                        child.paramList[ PLATFORMID ] = safeGetAttribute( nodes.Current, "platform" );
                        child.paramList[ VERSIONSTR ] = safeGetAttribute( nodes.Current, "versionstring" );
                        child.paramList[ SERVICEPACK ] = safeGetAttribute( nodes.Current, "servicepack" );
                        break;
                    case "Registry" :
                        child.type = CheckType.Registry;
                        child.paramList = new object[ 3 ];
                        child.paramList[ REGKEYID ] = safeGetAttribute( nodes.Current, "key" );
                        child.paramList[ REGKEYNAME ] = safeGetAttribute( nodes.Current, "name" );
                        child.paramList[ REGKEYVALUE ] = safeGetAttribute( nodes.Current, "value" );
                        break;
                    case "Assembly":
                        child.type = CheckType.Assembly;
                        child.paramList = new object[ 1 ];
                        child.paramList[ ASMKEYID ] = safeGetAttribute( nodes.Current, "key" );
                        break;
                    case "File":
                        child.type = CheckType.File;
                        child.paramList = new object[ 5 ];
                        child.paramList[ FILEPATH ] = safeGetAttribute( nodes.Current, "path" );
                        child.paramList[ FILENAME ] = safeGetAttribute( nodes.Current, "name" );
                        child.paramList[ FILEREGKEY1 ] = safeGetAttribute( nodes.Current, "regpathkey" );
                        child.paramList[ FILEREGKEY2 ] = safeGetAttribute( nodes.Current, "regpathname" );
                        child.paramList[ FILEVERSION ] = safeGetAttribute( nodes.Current, "versionstring" );
                        break;
                }

                loadElement( nodes.Current, child );
            }
        }

        /// <summary>
        /// Get attribute from Xml document, return empty string if missing.
        /// </summary>
        /// <param name="node">The xml node that the attribute is attached to.</param>
        /// <param name="key">The attribute name</param>
        /// <returns></returns>
        private string safeGetAttribute( XPathNavigator node, string key )
        {
            string value = node.GetAttribute( key, "" );
            return value == null ? String.Empty : value;
        }

        /// <summary>
        /// Create a ListViewItem object based on the check object.
        /// </summary>
        /// <param name="checkobj"></param>
        /// <param name="checkitems">Will add the item to this list.</param>
        private ListViewItem CreateListViewItem( CheckClass checkobj )
        {
            ListViewItem item = new ListViewItem( );
            item.Text = checkobj.displaytext;
            // The tooltips are not shown when the parent form is topmost, which needs to happen for Vista.
            // Hence rather disable the tooltips.
            //item.ToolTipText = checkobj.tooltiptext;
            item.StateImageIndex = (int)checkobj.result;
            return item;
        }

        /// <summary>
        /// Returns the number of successful items indicated by the StateImageIndex property. Used to
        /// determine which list of optional checks to display.
        /// </summary>
        /// <param name="checkitems">The list of items to scan through.</param>
        /// <returns></returns>
        private int NumberSuccessItems( List<ListViewItem> checkitems )
        {
            int count = 0;
            foreach( ListViewItem item in checkitems )
                count += ( item.StateImageIndex == (int)CheckResult.Success ) ? 1 : 0;

            return count;
        }

        /// <summary>
        /// Returns the number of successful items indicated by the StateImageIndex property. Used to
        /// determine which list of optional checks to display. This routine stops counting the moment it
        /// encounters an invalid item. Hence the order the items are placed in will matter.
        /// </summary>
        /// <param name="checkitems">The list of items to scan through.</param>
        /// <returns></returns>
        private int NumberSuccessItemsByOrder( List<ListViewItem> checkitems )
        {
            int count = 0;
            foreach( ListViewItem item in checkitems )
            {
                if( item.StateImageIndex != (int)CheckResult.Success )
                    break;
                count++;
            }

            return count;
        }

        /// <summary>
        /// Simply return the worse case found
        /// </summary>
        private CheckResult GetResult( ListView.ListViewItemCollection checkitems )
        {
            return GetResult( (ICollection)checkitems );
        }

        private CheckResult GetResult( List<ListViewItem> checkitems )
        {
            return GetResult( (ICollection)checkitems );
        }

        private CheckResult GetResult( ICollection checkitems )
        {
            int result = (int)CheckResult.Success;
            foreach( ListViewItem item in checkitems )
                result = Math.Max( result, item.StateImageIndex );

            return (CheckResult)result;
        }

        /// <summary>
        /// Changes the results text below the ListView as the user clicks on each item.
        /// </summary>
        private void preListView_ItemSelectionChanged( object sender, ListViewItemSelectionChangedEventArgs e )
        {
            if( e.Item == null )
                return;

            if( e.Item.StateImageIndex == (int)CheckResult.Failure )
            {
                resultBox.Text = "Prerequisite Error";
                results.Text = e.Item.ToolTipText;
            }
            else if( e.Item.StateImageIndex == (int)CheckResult.Warning )
            {
                resultBox.Text = "Optional Component";
                results.Text = e.Item.ToolTipText;
            }
            else
            {
                resultBox.Text = "Prerequisite Details";
                results.Text = e.Item.ToolTipText;
            }
        }
    }
}