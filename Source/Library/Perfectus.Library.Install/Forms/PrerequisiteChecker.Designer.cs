namespace Perfectus.Library.Install
{
    partial class PrerequisiteChecker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container( );
            this.cancelbtn = new System.Windows.Forms.Button( );
            this.panel1 = new System.Windows.Forms.Panel( );
            this.label2 = new System.Windows.Forms.Label( );
            this.label1 = new System.Windows.Forms.Label( );
            this.bevel1 = new Perfectus.Library.UserControls.Bevel( );
            this.preListView = new System.Windows.Forms.ListView( );
            this.column1 = new System.Windows.Forms.ColumnHeader( );
            this.panel2 = new System.Windows.Forms.Panel( );
            this.backbtn = new System.Windows.Forms.Button( );
            this.nextbtn = new System.Windows.Forms.Button( );
            this.bevel2 = new Perfectus.Library.UserControls.Bevel( );
            this.listviewimages = new System.Windows.Forms.ImageList( this.components );
            this.formtimer = new System.Windows.Forms.Timer( this.components );
            this.results = new System.Windows.Forms.Label( );
            this.resultBox = new System.Windows.Forms.GroupBox( );
            this.panel1.SuspendLayout( );
            this.panel2.SuspendLayout( );
            this.resultBox.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // cancelbtn
            // 
            this.cancelbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelbtn.Location = new System.Drawing.Point( 405, 10 );
            this.cancelbtn.Name = "cancelbtn";
            this.cancelbtn.Size = new System.Drawing.Size( 75, 23 );
            this.cancelbtn.TabIndex = 0;
            this.cancelbtn.Text = "Cancel";
            this.cancelbtn.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::Perfectus.Library.Install.Properties.Resources.dlgbmp_b4_green;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel1.Controls.Add( this.label2 );
            this.panel1.Controls.Add( this.label1 );
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point( 0, 0 );
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size( 492, 63 );
            this.panel1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point( 39, 35 );
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size( 236, 13 );
            this.label2.TabIndex = 1;
            this.label2.Text = "Checking you have all the prerequisites installed.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.label1.Location = new System.Drawing.Point( 23, 13 );
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size( 125, 13 );
            this.label1.TabIndex = 0;
            this.label1.Text = "Prerequisite Checker";
            // 
            // bevel1
            // 
            this.bevel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bevel1.Location = new System.Drawing.Point( 0, 63 );
            this.bevel1.Name = "bevel1";
            this.bevel1.Size = new System.Drawing.Size( 492, 2 );
            this.bevel1.TabIndex = 2;
            // 
            // preListView
            // 
            this.preListView.CausesValidation = false;
            this.preListView.Columns.AddRange( new System.Windows.Forms.ColumnHeader[ ] {
            this.column1} );
            this.preListView.FullRowSelect = true;
            this.preListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.preListView.LabelWrap = false;
            this.preListView.Location = new System.Drawing.Point( 45, 71 );
            this.preListView.Name = "preListView";
            this.preListView.ShowItemToolTips = true;
            this.preListView.Size = new System.Drawing.Size( 400, 170 );
            this.preListView.TabIndex = 3;
            this.preListView.UseCompatibleStateImageBehavior = false;
            this.preListView.View = System.Windows.Forms.View.Details;
            this.preListView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler( this.preListView_ItemSelectionChanged );
            // 
            // column1
            // 
            this.column1.Width = 390;
            // 
            // panel2
            // 
            this.panel2.Controls.Add( this.backbtn );
            this.panel2.Controls.Add( this.nextbtn );
            this.panel2.Controls.Add( this.cancelbtn );
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point( 0, 311 );
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size( 492, 45 );
            this.panel2.TabIndex = 5;
            // 
            // backbtn
            // 
            this.backbtn.Location = new System.Drawing.Point( 238, 10 );
            this.backbtn.Name = "backbtn";
            this.backbtn.Size = new System.Drawing.Size( 75, 23 );
            this.backbtn.TabIndex = 2;
            this.backbtn.Text = "Back";
            this.backbtn.UseVisualStyleBackColor = true;
            this.backbtn.Click += new System.EventHandler( this.backbtn_Click );
            // 
            // nextbtn
            // 
            this.nextbtn.Enabled = false;
            this.nextbtn.Location = new System.Drawing.Point( 314, 10 );
            this.nextbtn.Name = "nextbtn";
            this.nextbtn.Size = new System.Drawing.Size( 75, 23 );
            this.nextbtn.TabIndex = 1;
            this.nextbtn.Text = "Next";
            this.nextbtn.UseVisualStyleBackColor = true;
            this.nextbtn.Click += new System.EventHandler( this.nextbtn_Click );
            // 
            // bevel2
            // 
            this.bevel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bevel2.Location = new System.Drawing.Point( 0, 309 );
            this.bevel2.Name = "bevel2";
            this.bevel2.Size = new System.Drawing.Size( 492, 2 );
            this.bevel2.TabIndex = 6;
            // 
            // listviewimages
            // 
            this.listviewimages.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.listviewimages.ImageSize = new System.Drawing.Size( 16, 16 );
            this.listviewimages.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // formtimer
            // 
            this.formtimer.Tick += new System.EventHandler( this.formtimer_Tick );
            // 
            // results
            // 
            this.results.Dock = System.Windows.Forms.DockStyle.Fill;
            this.results.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte)( 0 ) ) );
            this.results.ForeColor = System.Drawing.Color.Black;
            this.results.Location = new System.Drawing.Point( 3, 16 );
            this.results.Name = "results";
            this.results.Padding = new System.Windows.Forms.Padding( 3 );
            this.results.Size = new System.Drawing.Size( 394, 37 );
            this.results.TabIndex = 7;
            // 
            // resultBox
            // 
            this.resultBox.Controls.Add( this.results );
            this.resultBox.ForeColor = System.Drawing.Color.Black;
            this.resultBox.Location = new System.Drawing.Point( 45, 247 );
            this.resultBox.Name = "resultBox";
            this.resultBox.Size = new System.Drawing.Size( 400, 56 );
            this.resultBox.TabIndex = 8;
            this.resultBox.TabStop = false;
            this.resultBox.Text = "Prerequisite Details ";
            // 
            // PrerequisiteChecker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelbtn;
            this.ClientSize = new System.Drawing.Size( 492, 356 );
            this.Controls.Add( this.resultBox );
            this.Controls.Add( this.bevel2 );
            this.Controls.Add( this.panel2 );
            this.Controls.Add( this.preListView );
            this.Controls.Add( this.bevel1 );
            this.Controls.Add( this.panel1 );
            this.Name = "PrerequisiteChecker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler( this.PrerequisiteChecker_Load );
            this.panel1.ResumeLayout( false );
            this.panel1.PerformLayout( );
            this.panel2.ResumeLayout( false );
            this.resultBox.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button cancelbtn;
        private System.Windows.Forms.Panel panel1;
        private Perfectus.Library.UserControls.Bevel bevel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView preListView;
        private System.Windows.Forms.ColumnHeader column1;
        private System.Windows.Forms.Panel panel2;
        private Perfectus.Library.UserControls.Bevel bevel2;
        private System.Windows.Forms.Button nextbtn;
        private System.Windows.Forms.Button backbtn;
        private System.Windows.Forms.ImageList listviewimages;
        private System.Windows.Forms.Timer formtimer;
        private System.Windows.Forms.Label results;
        private System.Windows.Forms.GroupBox resultBox;
    }
}