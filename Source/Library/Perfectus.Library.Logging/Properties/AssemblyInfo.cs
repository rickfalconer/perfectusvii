﻿using System;
using System.Reflection;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus Library Logging")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs