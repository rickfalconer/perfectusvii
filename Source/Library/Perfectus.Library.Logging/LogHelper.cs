using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Filters;
using Perfectus.Library.Localisation;

namespace Perfectus.Library.Logging
{
    static public class Log
    {

        const int disablePriority = (int)Priority.IGNORE;
        private static LogWriterFactory logFactory = null;

        // The current application's name!
        private static String applicationName = null;

        static Log( )
        {
            // Determine the application name (from the application config file)
            applicationName = System.Configuration.ConfigurationManager.AppSettings[ "ApplicationName" ];
        }

        public enum InOut
        {
            None = 0,
            In,
            Out
        }

        // Priority here represents the volume or number of log events that will occur. Hence HIGH
        // implies that a high number of events must occur and LOW implies that only a few events
        // are recorded....
        public enum Priority
        {
            IGNORE = -1,     // Ignore priority
            HIGH = 10,          // Log data to follow logical process & data flow (e.g. trace events)
            MID = 20,           // Log data to follow the data flow (e.g. what happens to PackageInstance)
            LOW = 30
        }

        // These are categories that represent assemblies.
        public enum Category
        {
            None,
            UnitTest,
            Transaction,
            Common,
            DataAccess,
            Engine2,
            Receiver,
            AnswerAcquirer,
            AssemblySystem,
            ServerCommon,
            ConfigurationSystem,
            InterviewSystem,
            PackageManager,
            ReportingSystem,
            WebServices,
            WebUI,
            WebLibrary,
            ServerPlugins,
            Converters,
            Distributors,
            Formatters,
            ClientPlugins,
            ClientSDK,
            ClientConfiguration,
            ClientPackageVersioning,
            ClientQuery,
            PerfectusLibrary,
            SharePoint,
            ThirdParty
        }

        public class MyLogEntry
        {
            internal string _callingMethodName = string.Empty;
            internal string _callingAssemblyName = string.Empty;
            internal IConvertible[] _messages = null;
            private LogWriter log = null;
            private LogEntry logEntry;


            // Overloaded Constructors....   
            internal MyLogEntry( )
                : this( TraceEventType.Verbose ) { }

            internal MyLogEntry( TraceEventType severity, params IConvertible[ ] messages )  
                : this( severity, null, messages ) { }

            internal MyLogEntry( TraceEventType severity, String title, params IConvertible[ ] messages )
                : this( severity, Category.None, title, messages ) { }

            internal MyLogEntry( TraceEventType severity, Category assCategory, params IConvertible[ ] messages )
                : this( severity, assCategory, null, messages ) { }

            internal MyLogEntry( TraceEventType severity, Category assCategory, String title, params IConvertible[ ] messages )
                : this( severity, assCategory, null, title, messages ) { }

            internal MyLogEntry( TraceEventType severity, Category assCategory, MethodBase callingMethod, String title, params IConvertible[ ] messages )
                : this( severity, assCategory, disablePriority, callingMethod, title, messages ) { }

            internal MyLogEntry( TraceEventType severity, Category assCategory, Int32 priority, MethodBase callingMethod, String title, params IConvertible[ ] messages )
            {
                logEntry = new LogEntry();

                // #1: Add log category based on executing application
                if( applicationName != null )
                    logEntry.Categories.Add( applicationName );
                
                // #2: Assign log category based on severity
                logEntry.Categories.Add( severity.ToString( ) );

                // #3: Assign log category based on assembly.
                if( assCategory != Category.None )
                    logEntry.Categories.Add( assCategory.ToString( ) );

                logEntry.Priority = priority;
                logEntry.Severity = severity;
                logEntry.Title = title;

                _messages = messages;
                if (callingMethod != null)
                {
                    _callingMethodName = callingMethod.ToString();
                    _callingAssemblyName = callingMethod.DeclaringType.ToString();
                }
                log = factory();
            }

            internal virtual bool ShouldLog()
            {
                bool shouldLog = ShouldLogInternal();
                log.Dispose();
                log = null;
                return shouldLog;
            }

            private bool ShouldLogInternal()
            {
                // Error reporting should never itself cause an error!!
                if (log == null)
                    return false;

                return log.ShouldLog(logEntry);
            }
            

            private LogWriter factory()
            {
                try
                {
                    if( Log.logFactory == null )
                    {
                        try
                        {
                            IConfigurationSource logSource = null;
                            logSource = ConfigurationSourceFactory.Create( "PerfectusLogging" );
                            Log.logFactory = new LogWriterFactory( logSource );
                        }
                        catch
                        {
                            Log.logFactory = new LogWriterFactory( );
                        }
                    }
                    if( Log.logFactory != null )
                        return Log.logFactory.Create( );
                }
                // Failure to log exceptions/errors, should never cause the system to fail to process the request.
                catch
                { }

                return null;
            }

            internal void Write()
            {
                // Error reporting should never itself cause an error!!
                if( log == null )
                    return;

                if( ShouldLogInternal( ) )
                {

                    StringBuilder totalMessage = new StringBuilder();

                    // Build up the calling context info
                    if (_callingAssemblyName != String.Empty)
                    {
                        totalMessage.Append(string.Format("Assembly: {0}{1}", _callingAssemblyName, System.Environment.NewLine));
                        totalMessage.Append(string.Format("Method:   {0}{1}", _callingMethodName, System.Environment.NewLine));
                    }

                    // Build up the message
                    foreach (IConvertible message in _messages)
                    {
                        if (totalMessage.Length > 0)
                            totalMessage.Append(System.Environment.NewLine);

                        totalMessage.Append(String.Format("  {0}", message));
                    }

                    logEntry.Message = totalMessage.ToString();
                    logEntry.Title = String.Format("Perfectus [{0}] {1}", (logEntry.Categories.Contains("Transaction") ? "Transaction" : "Other"), logEntry.MachineName);

                    log.Write(logEntry);
                    log.FlushContextItems();
                }
                log.Dispose();
                log = null;
            }
        }

        /// Trace
        /////////////////////////////////////////
        static public void Trace( params IConvertible[] messages )
        {
            new MyLogEntry( TraceEventType.Verbose, messages ).Write( );
        }
        static public void Trace( Category assCategory, params IConvertible[] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Verbose, assCategory, messages );
            log.Write( );
        }
        static public void Trace( Category assCategory, Priority priority, params IConvertible[] messages)
        {
            Trace( assCategory, (Int32)priority, messages );
        }
        static public void Trace( Category assCategory, Int32 priority, params IConvertible[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Verbose, assCategory, priority, null, null, messages );
            log.Write( );
        }
        // special trace
        static public void Trace( Category assCategory, MethodBase mBase, InOut inout )
        {
            Trace( assCategory, Priority.HIGH, String.Format( "{0} {1}", mBase.Name, inout == InOut.None ? "<->" : inout == InOut.In ? "->" : "<-" ) );
        }
        

        /// Info
        /////////////////////////////////////////
        static private void Info( String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Information, messages );
            log.Write( );
        }
        static private void Info( Category assCategory, String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Information, assCategory, messages );
            log.Write();
        }
        // <-private

        // public->
        static public void Info( ID localizedIdent )
        {
            Info( AddMessages(LocalisationSupport.Text(localizedIdent)) );
        }
        static public void Info( ID localizedIdent, params IConvertible[] messages )
        {
            Info( AddMessages(LocalisationSupport.Text(localizedIdent), messages));
        }
        static public void Info( Category assCategory, ID localizedIdent, params IConvertible[ ] messages )
        {
            Info( assCategory, AddMessages( LocalisationSupport.Text( localizedIdent ), messages ) );
        }
        static public void Info( Category assCategory, ID localizedIdent )
        {
            Info( assCategory, new String[ ] { LocalisationSupport.Text( localizedIdent ) } );
        }

        /// Warn
        /////////////////////////////////////////
        static private void Warn( String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Warning, messages );
            log.Write( );
        }

        static private void Warn( Category assCategory, String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Warning, assCategory, messages );
            log.Write();
        }
        // <-private

        // public->
        static public void Warn( ID localizedIdent )
        {
            Warn( AddMessages( LocalisationSupport.TextWithID(localizedIdent)) );
        }
        static public void Warn( ID localizedIdent, params IConvertible[] messages )
        {
            Warn( AddMessages( LocalisationSupport.TextWithID(localizedIdent), messages) );
        }
        static public void Warn( Category assCategory, ID localizedIdent, params IConvertible[] messages)
        {
            Warn( assCategory, AddMessages( LocalisationSupport.TextWithID( localizedIdent ), messages ) );
        }
        static public void Warn( Category assCategory, ID localizedIdent )
        {
            Warn( assCategory, new String[ ] { LocalisationSupport.TextWithID( localizedIdent ) } );
        }

        /// Error
        /////////////////////////////////////////
        static private void Error( String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Error, messages );
            log.Write( );
        }
        static private void Error( Category assCategory, String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Error, assCategory, messages );
            log.Write();
        }
        // <-private

        // public->
        static public void Error( ID localizedIdent )
        {
            Error( AddMessages(LocalisationSupport.TextWithID(localizedIdent)) );
        }
        static public void Error( ID localizedIdent, params IConvertible[] messages )
        {
            Error( AddMessages( LocalisationSupport.TextWithID(localizedIdent), messages ) );
        }
        static public void Error( Category assCategory, ID localizedIdent, params IConvertible[ ] messages )
        {
            Error( assCategory, AddMessages( LocalisationSupport.TextWithID( localizedIdent ), messages ) );
        }
        static public void Error( Category assCategory, ID localizedIdent )
        {
            Error( assCategory, new String[ ] { LocalisationSupport.TextWithID( localizedIdent ) } );
        }
        static public void Error( Category assCategory, System.Exception ex, ID localizedIdent, params IConvertible[ ] messages )
        {
            Error( assCategory, localizedIdent, AddExceptionMessage( ex, messages ) );
        }

        private static String[] AddMessages(String message, params IConvertible[] messages)
        {
            int parameterExpected = 0;
            while (message.Contains("{" + parameterExpected.ToString() + "}"))
                parameterExpected++;

            List<String> msg2 = new List<string>();

            if (messages != null)
            {
                int start = 0;

                // Otherwise we would get an error
                if (messages.Length >= parameterExpected)
                {
                    msg2.Add(String.Format(message, messages));
                    start = parameterExpected;
                }
                else
                    msg2.Add(message);
            
                if (messages.Length > parameterExpected)
                {
                    for (int j = start; j < messages.Length; j++)
                    {
                        if (messages[j] is string)
                            msg2.Add((string)messages[j]);
                        else if (messages[j] is IConvertible)
                            msg2.Add(messages[j].ToString());
                    }
                }
            }
            else
                msg2.Add(message);
            return msg2.ToArray();
        }

        private static String[] AddExceptionMessage(System.Exception ex, params IConvertible[] messages)
        {
            List<String> msg2 = new List<string>();

            if( messages != null )
            {
                foreach( IConvertible message in messages )
                    if( message is string )
                        msg2.Add( (string)message );
                    else
                        msg2.Add( message.ToString( ) );
            }

            msg2.Add(ex.Message);
            if (ex.StackTrace != null && ex.StackTrace != String.Empty)
                msg2.Add(ex.StackTrace);

            ///
            Exception inner = ex.InnerException;
            
            while ( inner != null )
            {
                msg2.Add(inner.Message);
                if (inner.StackTrace != null && inner.StackTrace != String.Empty)
                    msg2.Add(inner.StackTrace);
                inner = inner.InnerException;
            }

            return msg2.ToArray();
        }

        /// Critical
        /////////////////////////////////////////
        static private void Critical( String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Critical, messages );
            log.Write( );
        }
        static private void Critical( Category assCategory, String[ ] messages )
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Critical, assCategory, messages );
            log.Write();
        }
        // <-private

        // public->
        static public void Critical( ID localizedIdent )
        {
            Critical( AddMessages( LocalisationSupport.TextWithID(localizedIdent) ) );
        }
        static public void Critical( ID localizedIdent, params IConvertible[] messages )
        {
            Critical( AddMessages( LocalisationSupport.Text(localizedIdent), messages ) );
        }
        static public void Critical( Category assCategory, ID localizedIdent, params IConvertible[] messages )
        {
            Critical( assCategory, AddMessages( LocalisationSupport.TextWithID( localizedIdent ), messages ) );
        }
        static public void Critical( Category assCategory, ID localizedIdent )
        {
            Critical( assCategory, new String[ ] { LocalisationSupport.Text( localizedIdent ) } );
        }
        static public void Critical( Category assCategory, System.Exception ex, ID localizedIdent, params IConvertible[ ] messages )
        {
            Critical( assCategory, localizedIdent, AddExceptionMessage( ex, messages ) );
        }

        /// Transaction
        /////////////////////////////////////////
        static private void Transaction(String[] messages)
        {
            MyLogEntry log = new MyLogEntry( TraceEventType.Information, Category.Transaction, messages );
            log.Write();
        }
        // <-private

        // public->
        static public void Transaction( ID localizedIdent )
        {
            Transaction( AddMessages( LocalisationSupport.Text( localizedIdent ), localizedIdent.MessageID ) );
        }
        static public void Transaction( ID localizedIdent, params IConvertible[ ] messages )
        {
            Transaction( AddMessages( LocalisationSupport.Text( localizedIdent ), AddMessages( localizedIdent.MessageID, messages ) ) );
        }

        //---------------------------------
        static public bool ShallTrace( )
        {
            return new MyLogEntry( TraceEventType.Verbose ).ShouldLog( );
        }
        static public bool ShallTrace( Category assCategory )
        {
            return ShallTrace( assCategory, disablePriority );
        }
        static public bool ShallTrace( Category assCategory, Priority priority )
        {
            return ShallTrace( assCategory, (int)priority );
        }
        static public bool ShallTrace( Category assCategory, Int32 priority )
        {
            return new MyLogEntry( TraceEventType.Verbose, assCategory, priority, null, null ).ShouldLog( );
        }
    }
}