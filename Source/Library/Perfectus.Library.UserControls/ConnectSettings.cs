using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace Perfectus.Library.UserControls
{
    public partial class ConnectSettings : UserControl
    {
        private ExeConfigurationFileMap m_fileMap = new ExeConfigurationFileMap( );
        private Configuration m_config = null;
        // Records whether anything has changed.
        private bool m_changed = false;

        /// <summary>
        /// Configuration File path and name. Must be set prior to loading the interface data.
        /// </summary>
        public string ConfigFilePath
        {
            get { return m_fileMap.ExeConfigFilename; }
            set { m_fileMap.ExeConfigFilename = value; }
        }

        /// <summary>
        /// Indicates that the user changed one of the connection strings.
        /// </summary>
        public bool HasChanged
        {
            get { return m_changed; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public ConnectSettings( )
        {
            InitializeComponent( );
        }

        /// <summary>
        /// Load the configuration file and display the settings on the interface. Any errors with cause an exception to be thrown which
        /// must be handled by the parent container.
        /// </summary>
        public void LoadInterface( )
        {
            // If the information has not yet been provided, simply return.
            if( String.IsNullOrEmpty( ConfigFilePath ) )
                return;

            // Load up the configuration file as specified.  If unable to, report the error.
            m_config = ConfigurationManager.OpenMappedExeConfiguration( m_fileMap, ConfigurationUserLevel.None );
            if( m_config == null || m_config.HasFile == false )
                throw new Exception( "Missing configuration file : " + ConfigFilePath );

            // Load the data now...
            this.DisplayDBConnections( );
        }

        /// <summary>
        /// Load the User Control with all the necessary info. Any errors with cause an exception to be thrown which
        /// must be handled by the parent container.
        /// </summary>
        private void DisplayDBConnections( )
        {
            ConnectionStringsSection connectsection = m_config.ConnectionStrings;
            if( connectsection == null )
                throw new Exception( "No configuration <connectionStrings> element was found" );

            int count = 0;
            this.SuspendLayout( );

            // Removes all current controls from the table.
            this.connectionTable.Controls.Clear( );
            this.connectionTable.RowStyles.Clear( );

            for( int i = 0; i < connectsection.ConnectionStrings.Count; i++ )
            {
                ConnectionStringSettings setting = connectsection.ConnectionStrings[ i ];

                // We only display these providers. Otherwise the default connections (eg. Local Server) in machine.config will also be shown.
                if( setting.ProviderName != "Sql Server" && setting.ProviderName != "Oracle" )
                    continue;

                // Increase the number of rows.
                this.connectionTable.RowCount = (count * 2) + 1;
                this.connectionTable.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 20F ) );
                this.connectionTable.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Absolute, 40F ) );

                // Connection Name
                Label connectname = new System.Windows.Forms.Label( );
                connectname.AutoSize = true;
                connectname.Font = new System.Drawing.Font( "Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ( ( byte )( 0 ) ) );
                connectname.Location = new System.Drawing.Point( 0, 0 );
                connectname.Name = "name" + count.ToString( );
                connectname.Size = new System.Drawing.Size( 0, 20 );
                connectname.TabIndex = count * 2 + 1;
                connectname.Text = setting.Name;
                this.connectionTable.Controls.Add( connectname, 0, count * 2 );

                // Connection String
                TextBox connectvalue = new System.Windows.Forms.TextBox( );
                connectvalue.AutoSize = false;
                connectvalue.Name = count.ToString( );
                connectvalue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                connectvalue.Dock = System.Windows.Forms.DockStyle.None;
                connectvalue.Size = new System.Drawing.Size( 500, 20 );
                connectvalue.TabIndex = count * 2 + 2;
                connectvalue.Text = setting.ConnectionString;
                connectvalue.Tag = setting;
                connectvalue.TextChanged += new System.EventHandler( this.connectionstring_TextChanged );

                this.connectionTable.Controls.Add( connectvalue, 0, (count * 2) + 1 );

                count++;
            }
            this.ResumeLayout( );

            if( count == 0 )
                throw new Exception( "No connection string configuration settings where found" );
        }

        /// <summary>
        /// Saves the data initially displayed back into the configuration file and saves the file itself. Reloads the interface.
        /// </summary>
        /// <returns>true if something was saved.</returns>
        public bool SaveConnectionSettings( )
        {
            if( (m_config == null) || (m_changed == false) )
                return false;

            // Scan through the collection table and copy the connection strings into the settings objects in m_config.
            foreach( Control ctrl in this.connectionTable.Controls )
            {
                if( (ctrl.GetType( ) != typeof(TextBox)) || (((TextBox)ctrl).Tag == null ) )
                    continue;

                ((ConnectionStringSettings)ctrl.Tag).ConnectionString = ctrl.Text;
            }

            // Save the file.....
            m_config.Save( ConfigurationSaveMode.Modified );

            // Reload the interface to reflect any changes etc.
            DisplayDBConnections( );

            return true;
        }

        /// <summary>
        /// Invoked whenever a connection string text box is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connectionstring_TextChanged( object sender, EventArgs e )
        {
            m_changed = true;
        }
    }
}