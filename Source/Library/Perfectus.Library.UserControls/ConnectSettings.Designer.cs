namespace Perfectus.Library.UserControls
{
    partial class ConnectSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.connectionTable = new System.Windows.Forms.TableLayoutPanel( );
            this.SuspendLayout( );
            // 
            // connectionTable
            // 
            this.connectionTable.AutoSize = true;
            this.connectionTable.ColumnCount = 1;
            this.connectionTable.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
            this.connectionTable.Location = new System.Drawing.Point( 0, 0 );
            this.connectionTable.Name = "connectionTable";
            this.connectionTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.connectionTable.TabIndex = 1;
            // 
            // ConnectSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add( this.connectionTable );
            this.Name = "ConnectSettings";
            this.AutoSize = true;
            this.Size = new System.Drawing.Size( 500, 500 );

            this.ResumeLayout( false );
            this.PerformLayout( );

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel connectionTable;
    }
}
