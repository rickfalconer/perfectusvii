using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Perfectus.Library.UserControls
{
    public partial class Bevel : UserControl
    {

        /// <summary>Bevel border style.</summary>
        public enum BevelStyle
        {
            /// <summary>Lowered border.</summary>
            Lowered,
            /// <summary>Raised border.</summary>
            Raised,
            /// <summary>No border.</summary>
            Flat
        }

        private const Border3DSide DefaultShape = Border3DSide.Bottom;
        private const BevelStyle DefaultStyle = BevelStyle.Lowered;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bevel"/> class.
        /// </summary>
        public Bevel( )
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent( );

            this.SetStyle( ControlStyles.ResizeRedraw, true );
            this.SetStyle( ControlStyles.DoubleBuffer, true );

            style = DefaultStyle;
            shape = DefaultShape;
        }

        /// <summary>Paints the rule.</summary>
        protected override void OnPaint( PaintEventArgs e )
        {
            // Create a local version of the graphics object for the Bevel.
            Graphics g = e.Graphics;
            Rectangle r = ClientRectangle;

            if( Style != BevelStyle.Flat )
            {
                Border3DStyle style = Border3DStyle.SunkenOuter;
                if( Style == BevelStyle.Raised )
                    style = Border3DStyle.RaisedInner;

                // Draw the Bevel.
                switch( Shape )
                {
                    case Border3DSide.All:
                        ControlPaint.DrawBorder3D( g, r.Left, r.Top, r.Width, r.Height, style );
                        break;
                    case Border3DSide.Left:
                        ControlPaint.DrawBorder3D( g, r.Left, r.Top, 2, r.Height, style );
                        break;
                    case Border3DSide.Top:
                        ControlPaint.DrawBorder3D( g, r.Left, r.Top, r.Width, 2, style );
                        break;
                    case Border3DSide.Bottom:
                        ControlPaint.DrawBorder3D( g, r.Left, r.Bottom - 2, r.Width, 2, style );
                        break;
                    case Border3DSide.Middle:
                        break;
                    case Border3DSide.Right:
                        ControlPaint.DrawBorder3D( g, r.Right - 2, r.Top, 2, r.Height, style );
                        break;
                }
            }

            // Calling the base class OnPaint
            base.OnPaint( e );
        }

        private Border3DSide shape;
        /// <summary>
        /// Gets or sets the shape of the bevel.
        /// </summary>
        [DefaultValue( DefaultShape )]
        public Border3DSide Shape
        {
            get
            {
                return shape;
            }
            set
            {
                shape = value;
                Invalidate( );
            }
        }

        private BevelStyle style;
        /// <summary>
        /// Gets or sets the style of the bevel.
        /// </summary>
        [DefaultValue( DefaultStyle )]
        public BevelStyle Style
        {
            get
            {
                return style;
            }
            set
            {
                style = value;
                Invalidate( );
            }
        }
    }
}
