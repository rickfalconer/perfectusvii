using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Localisation
{
    public partial class ProcessCoordinator : MessageBase, IMessageEnumaration
    {
        public enum MEnum
        {
            // UnitTestText01 <- not included for unit testing
            UnitTestText02 = 2,
            UnitTestText03,
            UnitTestText04,

            pattern = 50,


            ProcessCoordinatorError = 100,
            FailedCreateTempDirectory,
            AnswerSetCreationFailed,
            MirrorSetCreationFailed,
            MergeFailed,
            LoadAttachmentFailed,
            ConvertingFailed,
            DistributingFailed,

            ConvertOneDocumentFailed,
            DistributeFailedAsDeliveryIncomplete,
            DistributorError,
            DistributorInitializeError,
            DistributorNotFoundError,
            DistributorFailedToLoadError,
            InvalidAttachmentIntegrationData,
            LoadFetcherReturnedNull,
            NoExtensionInformation,
            NoComponentSpecified,
            FailedToLoadPlugin,
            FailedToFindConverter,
            FailedToFindDistributer,
            RequiredPluginNotFound,

            norules,
            norules2,
            ProcessCoordinatoragentcallError,
            FailedInstanceCompletePlugin,
            Noconverters,
            Nodistributors,
            MirrorError,
            RuleError,
            templateError,
            templateError2,
            taskhandlerError,
            monitorthreadError,
            taskhandlerError2,
            AbortingRule,
            AddingTemplate,
            AssemblyComplete,
            CouldnotEvaluate,
            DebugFileWritten,
            EndOfStream,
            LoadingPlugin,
            PackageHasNoRulesForServer,
            PathToPlugin,
            QueryEvaluatedToFalse,
            RetrievingFromCache,
            RetrievingTemplate,
            RuleProcessing,
            StartingAssembly,
            CannotAdjustStatus,
            CannotAdjustThread,
            CannotAdjustThreadAfterStarted,
            CannotStopThreadPool,
            DynamicThreadDecay,
            MaxThreadCount,
            PoolHasBeenStarted,
            ThreadPoolName,
            ThreadTriggerTime,
            AlreadyExists,
            FileNotFound,
            FilePathIsNull,
            InvalidFolderType,
            ProcessCoordinatorQueueListener,
            ProcessCoordinatorthreadError,
            ProcessCoordinatorthreadPool,

            FailedToCreateMessageQueue = 1000,
            FailedToOpenMessageQueue,
            MessageQueueNotTransactional,
            MessageQueueFailedTest,
            MessageQueueIsDenySharedReceive,
            MessageQueuePayloadEmpty,
            MessageQueuePayloadMalformed,
            MessageQueueErrorPeek,

            // Others
            IncompatibleMessage = 1100,
            PackageInstanceReceived,

            // Info
            DatabaseReaderStart = 1200,
            DatabaseReaderStop,

            // Warning
            CoordinatorStoppedUnexpected = 1400,
            ControllerNotResponsiveDuringShutdown,
            OrphantJobFound,

            // Errors
            ReadingFromDatabaseFailed = 1600,
            ReadingDirtyFromDatabaseFailed,
            ReadingFromDatabaseTimeout,
            PeekFromDatabaseFailed,
            PeekFromDatabaseTimeout,
            UpdateStatusDatabaseFailed,
            CreatingJobFailed,
            CannotFindDatabaseRecord,
            DatabaseReaderNotEnabled,
            HealthTestFailed,
            ControllerFailed,
            RetryFromDatabaseFailed,
        }

        internal Dictionary<int, string> links = new Dictionary<int, string>();
        public ProcessCoordinator()
        {
            links.Add((int)MEnum.UnitTestText02, "<html><body><div><p>This is an HTML text for NUnit testing,<br>Resource: <b>Generic</b><br>Message: <b>UnitTestText02</b></p></div></body></html>");
            links.Add((int)MEnum.UnitTestText03, "www.doesnotexist.com?index=UnitTestText03");
            links.Add((int)MEnum.UnitTestText04, String.Format("{0}{1}{2}{3}{4}",
                                                "This is text for NUnit testing",
                                                System.Environment.NewLine,
                                                "Resource: Generic",
                                                System.Environment.NewLine,
                                                "Message: UnitTestText04"));
            links.Add((int)MEnum.MessageQueueNotTransactional, "www.perfectus.com?help=125");
        }
        override protected Dictionary<int, string> link { get { return links; } }

        protected override Type EnumType()
        {
            return typeof(MEnum);
        }
    }
}
