using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Localisation
{
    public interface IMessageEnumaration
    {
        String MessageByNumber(int number);
        int NumberByMessage(String messageId);
        String HelpLinkByMessage(String messageId);
    }
}
