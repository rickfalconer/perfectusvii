using System;
using System.Collections.Generic;
using System.Text;
using System.Resources;
using System.Reflection;
using System.Collections;
using System.IO;

namespace Perfectus.Library.Localisation
{
    public static class LocalisationSupport
    {
        private static Hashtable resourceManagersCache = new Hashtable();

        /// <summary>
        /// Return the Message to the given ID.
        /// The ID is of the form CLASS.IDENTIFIER
        /// If CLASS is omitted, then GENERIC is assumed
        /// </summary>
        /// <param name="identifier">Message ID</param>
        /// <returns>Message</returns>
        public static String Text(ID identifier)
        {
            String retString = GetResourceManager(identifier.AssemblyClassName).GetString(identifier.MessageID);

            // In case that we got the number (not the resource ID string)
            if (retString == null && identifier.IsNumber)
                retString = MessageByNumber(identifier.AssemblyClassName, identifier.MessageNumber);

            // Still no success - then return the messages identifier itself
            if (retString == null)
                retString = String.Format("Code({0}::{1})", identifier.AssemblyClassName, identifier.MessageID);

            return retString;
        }

        /// <summary>
        /// Return the Message to the given resource ID,
        /// add the code number to the message
        /// </summary>
        /// <param name="identifier">Message ID</param>
        /// <returns>Message</returns>
        public static String TextWithID(ID identifier)
        {
            // Assume ( norules = 100, norules2, ...)
            // Example: "ProcessCoordinator.norules2"
            // Result:  "The message itself"
            //          "Code(101)"    

            // Example: "ProcessCoordinator.101"
            // Result:  "The message itself"
            //          "Code(101)"    

            int msgNr = identifier.MessageNumber;
            // If the identifier is not a number
            if (!identifier.IsNumber)
                // then try to get the number from mapping table (e.g. "ProcessCoordinator.norules2")
                msgNr = NumberByMessage(identifier.AssemblyClassName, identifier.MessageID);

            // Get the message by the number 
            String returnMessage = MessageByNumber(identifier.AssemblyClassName, msgNr);

            if (returnMessage != null &&
                returnMessage.Length > 0)
                return String.Format("{0}{1}Code({2})", returnMessage, System.Environment.NewLine, msgNr == -1 ? identifier.MessageID : msgNr.ToString());
            else
                // case where the message could not be found, then print at least the code
                return String.Format("Code({0})", identifier.MessageID);
        }

        internal static string HelpLink(ID iD)
        {
            return HelpLinkByMessage(iD.AssemblyClassName, iD.MessageID);
        }

        private static Int32 NumberByMessage(string assemblyClass, String messageId)
        {
            IMessageEnumaration c = (IMessageEnumaration)Activator.CreateInstance(Type.GetType(assemblyClass));
            return c.NumberByMessage(messageId);
        }
        private static String MessageByNumber(string assemblyClass, int messageNumber)
        {
            IMessageEnumaration c = (IMessageEnumaration)Activator.CreateInstance(Type.GetType(assemblyClass));
            return c.MessageByNumber(messageNumber);
        }
        private static String HelpLinkByMessage(string assemblyClass, String messageId)
        {
            IMessageEnumaration c = (IMessageEnumaration)Activator.CreateInstance(Type.GetType(assemblyClass));
            return c.HelpLinkByMessage(messageId);
        }

        private static bool GetClassName(String identifier, out String assemblyClassName, out String ident)
        {
            assemblyClassName = "Generic";
            ident = identifier;

            if (identifier == null || identifier.Length == 0)
                return false;

            int i = identifier.IndexOf('.');
            if (i > 0)
            {
                assemblyClassName = identifier.Substring(0, i);
                ident = identifier.Substring(i + 1);
            }
            assemblyClassName = String.Format("Perfectus.Library.Localisation.{0}", assemblyClassName);
            return true;
        }


        internal static ResourceManager GetResourceManager(string resourceIdentifier)
        {
            // We could be deserialised on multiple threads by the coordinator service
            lock (resourceManagersCache)
            {
                // The identifier can be of the form '<class>' or '<assembly ref>, <class>'
                // The resource manager needs the class only.
                int u = resourceIdentifier.LastIndexOf(',');
                if (u > 0)
                    resourceIdentifier = resourceIdentifier.Substring(u + 1).Trim();

                if (resourceManagersCache.ContainsKey(resourceIdentifier))
                {
                    return (ResourceManager)resourceManagersCache[resourceIdentifier];
                }
                else
                {
                    // Test for other assemblies... 
                    Assembly ass = Assembly.GetCallingAssembly();
                    String path = new Uri(Path.GetDirectoryName(ass.CodeBase)).LocalPath;
                    path = Path.Combine(path, String.Format("{0}.DLL", resourceIdentifier));
                    if (File.Exists(path))
                        ass = Assembly.LoadFile(path);

                    // Register resource with correct assembly
                    ResourceManager rm = new ResourceManager(resourceIdentifier, ass);
                    resourceManagersCache.Add(resourceIdentifier, rm);
                    return rm;
                }
            }
        }
    }
}
