using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Perfectus.Library.Localisation
{
    delegate String MessageDelegate ( String identifier);

    abstract public class  MessageBase
    {
        virtual protected Dictionary<int, string> link { get { return null; } }

        protected abstract Type EnumType ();
        public MessageBase() { }

        #region IMessageEnumaration Members

        public string MessageByNumber(int number)
        {
            return LocalisationSupport.Text(new ID(String.Format("{0}.{1}", this.GetType().Name, Enum.GetName(EnumType(), number))));
        }

        public int NumberByMessage(string messageId)
        {
            try { return (int)Enum.Parse(EnumType(), messageId); }
            catch { return -1; }
        }

        public string HelpLinkByMessage(string messageId)
        {
            String retStr = null;
            if ( link != null)
            link.TryGetValue (NumberByMessage(messageId), out retStr);
            return retStr;
        }

        #endregion
    }


    public class ID
    {
        static Dictionary<String,String> classRegister = new Dictionary<string,string>();

        string assemblyClassName;
        public string AssemblyClassName { get { return assemblyClassName; } }

        string ident;
        public string MessageID { get { return ident; } }

        Int32 num = -1;
        public Int32 MessageNumber { get { return num; } }
        public bool IsNumber { get { return num != -1; } }

        public ID(int id)
            : this(id.ToString())
        {
        }
        public ID(string id)
        {
            // Example: "ProcessCoordinator.Error" is messageClass := "ProcessCoordinator", identifier := "Error"
            // Example: "Error" is messageClass := "Generic", identifier := "Error"
            if (id == null || id.Length == 0)
                throw new NullReferenceException("Message ID must not be null or empty");

            ident = SetClassName(id, out assemblyClassName);
            if (!Int32.TryParse(ident, out num))
                num = -1;
        }
        private string SetClassName(string id, out string className)
        {
            // The class containg relevant information can live in the library 
            // "Perfectus.Library.Localisation.dll" (e.g. Generic, ProcessCoordinator), or external 
            // "Perfectus.Library.Localisation.SharePoint.dll".
            // The class name contains the string used to load the assmbly if necessary.
            // For an internal class it's simply the class name, for an external class it's of the
            // form "Perfectus.Library.Localisation.SharePoint, Perfectus.Library.Localisation.SharePoint" with
            // Version, Culture and PublicKeyToken stripped out
            className = "Perfectus.Library.Localisation.Generic";
            int i = id.IndexOf('.');
            if (i > 0)
            {
                className = String.Format("Perfectus.Library.Localisation.{0}", id.Substring(0, i));
                id = id.Substring(i + 1);

                // First time the class is used?
                if (classRegister.ContainsKey(className))
                    className = classRegister[className];
                else
                {
                    // Test if there is another assembly.
                    // If the class is 'SharePoint' then test for 'Perfectus.Library.Localisation.SharePoint.dll'
                    // in the run folder of the app. If there is no such file, then assume that 'Perfectus.Library.Localisation.dll'
                    // shall be used.
                    String path = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;
                    path = Path.Combine(path, String.Format("{0}.DLL", className));
                    
                    if (File.Exists(path))
                    {
                        System.Reflection.Assembly ass = System.Reflection.Assembly.LoadFile(path);
                        String partName = ass.FullName;
                        String classKey = className;

                        // Strip out Version, Culture and PublicKeyToken
                        int j = partName.IndexOf(',');
                        if (j > 0)
                            partName = partName.Substring(0, j);
                        className = String.Format("{0}, {1}", partName, className);
                        classRegister.Add(classKey, className);
                    }
                    else
                        classRegister.Add(className, className);
                }
            }
            return id;
        }

        private String AddMessages(String message, params Object[] messages)
        {
            StringBuilder bld = new StringBuilder();
            int i = 0;
            while (message.Contains("{" + i.ToString() + "}"))
                i++;
            if (messages != null)
            {
                if (messages.Length >= i)
                    bld.Append(String.Format(message, messages));
                
                for (int j = i; j < messages.Length; j++)
                    bld.Append(String.Format("{0}{1}", System.Environment.NewLine, messages[j]));
            }
            else
                bld.Append(message);
            return bld.ToString();
        }

        public string ToString(params Object[] str)
        {
            String message = LocalisationSupport.Text(this);
            if (str != null && str.Length > 0)
                message = AddMessages(message, str);
            return message;
        }
        public string ToIdString(params Object[] str)
        {
            String message = LocalisationSupport.TextWithID(this);
            if (str != null && str.Length > 0)
                message = AddMessages(message, str);
            return message;
        }

        public string HelpLink()
        {
            return LocalisationSupport.HelpLink(this);
        }

        public string FullName()
        {
            return String.Format("{0}.{1}", assemblyClassName, this.MessageID);
        }
    }

    
}
