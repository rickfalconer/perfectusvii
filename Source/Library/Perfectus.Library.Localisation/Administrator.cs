using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Localisation
{
    public class Administrator : MessageBase, IMessageEnumaration
    {
        public enum MEnum
        {
            GeneralException = 100,
            SQLException,
            IsolatedStoragePermissionException,
            FilterFileUpdated,
            DataConfigurationSaveError,
            DataConfigurationUpdated,
            ResubmitPackageInstance,
            DataBaseUpgraded,
            DataBaseUpgradeError,
        }

        protected override Type EnumType( )
        {
            return typeof( MEnum );
        }
    }
}
