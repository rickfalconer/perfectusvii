using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Library.Localisation
{
    public class Generic : MessageBase, IMessageEnumaration
    {
        public enum MEnum
        {
            // UnitTestText01 <- not included for unit testing
            UnitTestText02 = 2,
            UnitTestText03,
            UnitTestText04,
            UnitTestText05,

            CompanyName = 100,
            CompanyNameShort,
            Copyright,
            Trademark,

            NOT_IMPLEMENTED = 200,
            StandardErrorCaption = 201,
            PackageFileNotFound,
        }

        internal Dictionary<int, string> links = new Dictionary<int, string>();
        public Generic()
        {
            links.Add((int)MEnum.UnitTestText02, LocalisationSupport.Text(new ID("UnitTestText02_help")));
            links.Add((int)MEnum.UnitTestText03, LocalisationSupport.Text(new ID("UnitTestText03_help"))); 
            links.Add((int)MEnum.UnitTestText04, LocalisationSupport.Text(new ID("UnitTestText04_help")));

            links.Add((int)MEnum.PackageFileNotFound, LocalisationSupport.Text(new ID("PackageFileNotFound_hint"))); 

        }
        override protected Dictionary<int, string> link { get { return links; } }

        protected override Type EnumType()
        {
            return typeof(MEnum);
        }
    }
}
