using System;
using System.Runtime.Serialization;

namespace Perfectus.Common
{
	/// <summary>
	/// Summary description for DisplayTypeExtension.
	/// </summary>
	[Serializable]
	public class DisplayTypeExtensionInfo : ISerializable
	{
		private string serverSideDisplayPluginId = null;
		private string localisedDisplayName = null;
		private string forDataType = null;
		private string serverSideFetcherPluginId = null;
	    private string[] fieldNames = new string[0];


	    public string[] FieldNames
	    {
	        get { return fieldNames; }
	        set { fieldNames = value; }
	    }

	    public string ServerSideFetcherPluginId
		{
			get { return serverSideFetcherPluginId; }
		}

		public string ServerSideDisplayPluginId
		{
			get { return serverSideDisplayPluginId; }
		}

		public string LocalisedDisplayName
		{
			get { return localisedDisplayName; }
			set { localisedDisplayName = value; }
		}

		
		private DisplayTypeExtensionInfo()
		{
		}


		public string ForDataType
		{
			get { return forDataType; }
		}

		public DisplayTypeExtensionInfo(string serverSideDisplayPluginId, string serverSideFetcherPluginId, string forDataType, string localisedDisplayName)
		{
			this.serverSideDisplayPluginId	= serverSideDisplayPluginId;
			this.serverSideFetcherPluginId = serverSideFetcherPluginId;
			this.localisedDisplayName = localisedDisplayName;
			this.forDataType = forDataType;
		}

		public DisplayTypeExtensionInfo(SerializationInfo info, StreamingContext context) : base()
		{
			serverSideDisplayPluginId = info.GetString("serverSideDisplayPluginId");
			localisedDisplayName = info.GetString("localisedDisplayName");
			forDataType = info.GetString("forDataType");
			serverSideFetcherPluginId = info.GetString("serverSideFetcherPluginId");
		    
            // Catch the situation when "fieldNames" not exists
            try
            {
                object oFieldNames = info.GetValue("fieldNames", typeof(string[]));
            if (oFieldNames != null)
            {
                    fieldNames = (string[])oFieldNames;
            }
		}
            catch
            {}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("serverSideDisplayPluginId", serverSideDisplayPluginId);
			info.AddValue("localisedDisplayName", localisedDisplayName);
			info.AddValue("forDataType", forDataType);
			info.AddValue("serverSideFetcherPluginId", serverSideFetcherPluginId);
            info.AddValue("fieldNames", fieldNames);
		}
	}
}
