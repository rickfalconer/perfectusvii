using System;
using System.Runtime.Serialization;

namespace Perfectus.Common
{
	/// <summary>
	/// Summary description for FormatStringExtensionInfo.
	/// </summary>
	[Serializable]
	public class FormatStringExtensionInfo : ISerializable
	{
		private string serverSideFormatterPluginKey;
		private string localisedDisplayName = null;
		private string forDataType = null;
		
		public string ServerSideFormatterPluginKey
		{
			get { return serverSideFormatterPluginKey; }
		}

		public string LocalisedDisplayName
		{
			get { return localisedDisplayName; }
			set { localisedDisplayName = value; }
		}
		
		private FormatStringExtensionInfo()
		{
		}

		public string ForDataType
		{
			get { return forDataType; }
		}

		public FormatStringExtensionInfo(string serverSideFormatterPluginId, string forDataType, string localisedDisplayName)
		{
			this.serverSideFormatterPluginKey = serverSideFormatterPluginId;
			this.localisedDisplayName = localisedDisplayName;
			this.forDataType = forDataType;
		}

		public FormatStringExtensionInfo(SerializationInfo info, StreamingContext context) : base()
		{
			serverSideFormatterPluginKey = info.GetString("serverSideFormatterPluginId");
			localisedDisplayName = info.GetString("localisedDisplayName");
			forDataType = info.GetString("forDataType");
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("serverSideFormatterPluginId", serverSideFormatterPluginKey);
			info.AddValue("localisedDisplayName", localisedDisplayName);
			info.AddValue("forDataType", forDataType);
		}
	}
}