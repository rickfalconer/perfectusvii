using System;

namespace Perfectus.Common
{
	/// <summary>
	/// A dictionary with keys of type QuestionDisplayType and values of type DisplayTypeExtensionInfo
	/// </summary>
	[Serializable]
	public class DisplayTypeExtensionInfoDictionary: System.Collections.DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the DisplayTypeExtensionInfoDictionary class
		/// </summary>
		public DisplayTypeExtensionInfoDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the DisplayTypeExtensionInfo associated with the given QuestionDisplayType
		/// </summary>
		/// <param name="key">
		/// The QuestionDisplayType whose value to get or set.
		/// </param>
		public virtual DisplayTypeExtensionInfo this[QuestionDisplayType key]
		{
			get
			{
				return (DisplayTypeExtensionInfo) this.Dictionary[key];
			}
			set
			{
				this.Dictionary[key] = value;
			}
		}

		/// <summary>
		/// Adds an element with the specified key and value to this DisplayTypeExtensionInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The QuestionDisplayType key of the element to add.
		/// </param>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo value of the element to add.
		/// </param>
		public virtual void Add(QuestionDisplayType key, DisplayTypeExtensionInfo value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this DisplayTypeExtensionInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The QuestionDisplayType key to locate in this DisplayTypeExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this DisplayTypeExtensionInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(QuestionDisplayType key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this DisplayTypeExtensionInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The QuestionDisplayType key to locate in this DisplayTypeExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this DisplayTypeExtensionInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(QuestionDisplayType key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this DisplayTypeExtensionInfoDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo value to locate in this DisplayTypeExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this DisplayTypeExtensionInfoDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(DisplayTypeExtensionInfo value)
		{
			foreach (DisplayTypeExtensionInfo item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this DisplayTypeExtensionInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The QuestionDisplayType key of the element to remove.
		/// </param>
		public virtual void Remove(QuestionDisplayType key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this DisplayTypeExtensionInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Keys
		{
			get
			{
				return this.Dictionary.Keys;
			}
		}

		/// <summary>
		/// Gets a collection containing the values in this DisplayTypeExtensionInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Values
		{
			get
			{
				return this.Dictionary.Values;
			}
		}
	}
}
