using System;

namespace Perfectus.Common
{
	/// <summary>
	/// Summary description for ProgressEventArgs.
	/// </summary>
	public class ProgressEventArgs : EventArgs
	{
		private string taskName;
		private int percentComplete;

		public string TaskName
		{
			get { return taskName; }
		}

		public int PercentComplete
		{
			get { return percentComplete; }
		}

		private ProgressEventArgs()
		{
		}

		public ProgressEventArgs(string taskName, int percentComplete)
		{
			this.taskName = taskName;
			this.percentComplete = percentComplete;
		}
	}
}