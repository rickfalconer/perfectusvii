using System.IO;
using System.Xml;

namespace Perfectus.Common
{
	public class XmlProgressReader : XmlTextReader, IProgressReporter
	{
		private Stream myStream;

		public XmlProgressReader(Stream s) : base(s)
		{
			myStream = s;
		}

		public long Progress
		{
			get { return myStream.Position; }
		}
	}
}