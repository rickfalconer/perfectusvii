using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;

namespace Perfectus.Common.Property.Class
{
    public class Property
    {
        private Property() { }
        public Property(String friendlyName, String internalName, String typeName)
        {
            FriendlyName = friendlyName;
            InternalName = internalName;
            TypeName = typeName;
            Value = null;
            DefaultValue = null;
            PerfectusType = Perfectus.Common.PerfectusDataType.Text;
        }
        private Object _value;
        public Object Value { get { return _value; } set { _value = value; } }

        private Object _defaultValue;
        public Object DefaultValue { get { return _defaultValue; } set { _defaultValue = value; } }
        public bool HasDefaultValue { get { return null != _defaultValue; } }

        private String _friendlyName;
        public String FriendlyName { get { return _friendlyName; } set { _friendlyName = value; } }

        private String _internalName;
        public String InternalName { get { return _internalName; } set { _internalName = value; } }

        private String _typeName;
        public String TypeName { get { return _typeName; } set { _typeName = value; } }

        private Perfectus.Common.PerfectusDataType _perfectusType;
        public Perfectus.Common.PerfectusDataType PerfectusType { get { return _perfectusType; } set { _perfectusType = value; } }

        private String _editorTypeName;
        public String EditorTypeName { get { return _editorTypeName; } set { _editorTypeName = value; } }

        private String _converterTypeName;
        public String ConverterTypeName { get { return _converterTypeName; } set { _converterTypeName = value; } }
    }

    public class PropertyListEntry : ICustomTypeDescriptor
    {
        private PropertyListEntry()
        {
            propertyDescriptors = new PropertyDescriptorCollection(new PropertyDescriptor[] { });
        }
        private PropertyDescriptorCollection propertyDescriptors;

        public List<Property> properties;

        public Object this[String fieldName]
        {
            get
            {
                foreach (Property p in properties)
                    if (p.InternalName == fieldName)
                        return p.Value;
                return null;
            }
        }

        public PropertyListEntry(String friendlyName, String internalName)
            : this()
        {
            FriendlyName = friendlyName;
            InternalName = internalName;
            properties = new List<Property>();
        }

        public PropertyDescriptorCollection GetAllProperties()
        {
            if (propertyDescriptors.Count == 0)
                foreach (Property p in
                    properties)
                {
                    PerfectusPropertyDescriptor prop = new PerfectusPropertyDescriptor(p);
                    propertyDescriptors.Add(prop);
                }
            return propertyDescriptors;
        }

        private String _friendlyName;
        public String FriendlyName { get { return _friendlyName; } set { _friendlyName = value; } }

        private String _internalName;
        public String InternalName { get { return _internalName; } set { _internalName = value; } }

        public override string ToString()
        {
            return _friendlyName;
        }

        #region ICustomTypeDescriptor Members

        public new TypeConverter GetConverter()
        {
            return (TypeDescriptor.GetConverter(this, true));
        }

        public new EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return (TypeDescriptor.GetEvents(this, attributes, true));
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return (TypeDescriptor.GetEvents(this, true));
        }

        public new string GetComponentName()
        {
            return (TypeDescriptor.GetComponentName(this, true));
        }

        public new object GetPropertyOwner(PropertyDescriptor propertyDescriptor)
        {
            return this;
        }

        public new AttributeCollection GetAttributes()
        {
            return (TypeDescriptor.GetAttributes(this, true));
        }

        public new PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return (GetAllProperties());
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return (GetAllProperties());
        }

        public new object GetEditor(Type editorBaseType)
        {
            return (TypeDescriptor.GetEditor(this, editorBaseType, true));
        }

        public new PropertyDescriptor GetDefaultProperty()
        {
            return (TypeDescriptor.GetDefaultProperty(this, true));
        }

        public new EventDescriptor GetDefaultEvent()
        {
            return (TypeDescriptor.GetDefaultEvent(this, true));
        }

        public new string GetClassName()
        {
            return (TypeDescriptor.GetClassName(this, true));
        }

        #endregion
    }

    public class PropertyList
    {
        private PropertyList() { ListElements = new List<PropertyListEntry>(); }
        public PropertyList(String typeName)
            : this()
        {
            ListElements = new List<PropertyListEntry>();
            TypeName = typeName;
            PerfectusType = Perfectus.Common.PerfectusDataType.Text;
        }
        public PropertyListEntry CurrentElement;
        public List<PropertyListEntry> ListElements;

        public PropertyListEntry GetSelectedEntry()
        {
            foreach (PropertyListEntry entry in ListElements)
                if (entry.InternalName == CurrentElement.InternalName)
                    return entry;
            return null;
        }

        private String _typeName;
        public String TypeName { get { return _typeName; } set { _typeName = value; } }

        private Perfectus.Common.PerfectusDataType _perfectusType;
        public Perfectus.Common.PerfectusDataType PerfectusType { get { return _perfectusType; } set { _perfectusType = value; } }

        private String _editorTypeName;
        public String EditorTypeName { get { return _editorTypeName; } set { _editorTypeName = value; } }

        private String _converterTypeName;
        public String ConverterTypeName { get { return _converterTypeName; } set { _converterTypeName = value; } }

        public static PropertyList CreateFromXML(String xml)
        {
            if (xml == null || xml.Length == 0)
                return null;
            XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
            return (PropertyList)ser.Deserialize(new MemoryStream(Encoding.ASCII.GetBytes(xml.ToCharArray())));
        }
    }

    internal class PerfectusPropertyDescriptor : PropertyDescriptor
    {
        Type componentType;

        Perfectus.Common.Property.Class.Property _p;

        internal PerfectusPropertyDescriptor(Perfectus.Common.Property.Class.Property p)
            : base(p.FriendlyName, null)
        {
            _p = p;
            componentType = Type.GetType(p.TypeName);
            /*
            Attribute[] attributes = new Attribute[] { new BrowsableAttribute(true),
                                                       new TypeConverterAttribute(p.ConverterTypeName),
                                                       new EditorAttribute(p.EditorTypeName, typeof (System.Drawing.Design.UITypeEditor)) };

            base.AttributeArray = attributes;
             * */
        }

        public override bool CanResetValue(object component)
        {
            return _p.HasDefaultValue;
        }

        public override Type ComponentType
        {
            get { return componentType; }
        }

        public override object GetValue(object component)
        {
            return _p.Value;
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type PropertyType
        {
            get { return componentType; }
        }

        public override void ResetValue(object component)
        {
            _p.Value = _p.DefaultValue;
        }

        public override void SetValue(object component, object value)
        {
            _p.Value = value;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }
    }
}