using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Property.Class;

namespace Perfectus.Common.Property.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
    public class XmlEncodedConverter : StringConverter
	{
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            if (value is String)
                value = PropertyList.CreateFromXML((String)value);

            if (value == null)
                return String.Empty;
            if (value is PropertyList)
                return ((PropertyList)value).CurrentElement.FriendlyName;

            return null;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return false;
        }
	}
}