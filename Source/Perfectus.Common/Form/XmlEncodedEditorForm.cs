using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using Perfectus.Common.Property.Class;

namespace Perfectus.Common.Property.Dialogs
{
    public partial class XmlEncodedEditorForm : Form
    {
        private PropertyList val;
        private String saveSettup;

        public String Val
        {
            get
            {
                if (val == null)
                    return String.Empty;

                String text = String.Empty;
                XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
                using (MemoryStream m = new MemoryStream())
                {
                    ser.Serialize(m, val);
                    text = Encoding.ASCII.GetString(m.ToArray(), 0, (int)m.Length);
                    return text;
                }
            }
            set
            {
                val = PropertyList.CreateFromXML(saveSettup = value);
                UpdateForm();
            }
        }

        private void UpdateForm()
        {
            listChoice.BeginUpdate();
            String rememberMe = val.CurrentElement.InternalName;
            listChoice.DataSource = val.ListElements;
            listChoice.DisplayMember = "FriendlyName";
            val.CurrentElement.InternalName = rememberMe;
            listChoice.EndUpdate();
            listChoice.SelectedItem = val.GetSelectedEntry();
        }


        public XmlEncodedEditorForm()
        {
            InitializeComponent();

            DialogResult = DialogResult.Cancel;
        }


        private void listChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            PropertyListEntry selectedEntry = (PropertyListEntry)listChoice.SelectedValue;
            propertyGrid.SelectedObject = selectedEntry;

            val.CurrentElement.FriendlyName = selectedEntry.FriendlyName;
            val.CurrentElement.InternalName = selectedEntry.InternalName;
        }

        private void XmlEncodedEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Val = saveSettup;
            DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}


