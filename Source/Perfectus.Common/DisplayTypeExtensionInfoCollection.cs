namespace Perfectus.Common
{
	/// <summary>
	/// A collection of elements of type DisplayTypeExtensionInfo
	/// </summary>
	public class DisplayTypeExtensionInfoCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the DisplayTypeExtensionInfoCollection class.
		/// </summary>
		public DisplayTypeExtensionInfoCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the DisplayTypeExtensionInfoCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new DisplayTypeExtensionInfoCollection.
		/// </param>
		public DisplayTypeExtensionInfoCollection(DisplayTypeExtensionInfo[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the DisplayTypeExtensionInfoCollection class, containing elements
		/// copied from another instance of DisplayTypeExtensionInfoCollection
		/// </summary>
		/// <param name="items">
		/// The DisplayTypeExtensionInfoCollection whose elements are to be added to the new DisplayTypeExtensionInfoCollection.
		/// </param>
		public DisplayTypeExtensionInfoCollection(DisplayTypeExtensionInfoCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this DisplayTypeExtensionInfoCollection.
		/// </param>
		public virtual void AddRange(DisplayTypeExtensionInfo[] items)
		{
			foreach (DisplayTypeExtensionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another DisplayTypeExtensionInfoCollection to the end of this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The DisplayTypeExtensionInfoCollection whose elements are to be added to the end of this DisplayTypeExtensionInfoCollection.
		/// </param>
		public virtual void AddRange(DisplayTypeExtensionInfoCollection items)
		{
			foreach (DisplayTypeExtensionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type DisplayTypeExtensionInfo to the end of this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo to be added to the end of this DisplayTypeExtensionInfoCollection.
		/// </param>
		public virtual void Add(DisplayTypeExtensionInfo value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic DisplayTypeExtensionInfo value is in this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo value to locate in this DisplayTypeExtensionInfoCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this DisplayTypeExtensionInfoCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(DisplayTypeExtensionInfo value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this DisplayTypeExtensionInfoCollection
		/// </summary>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo value to locate in the DisplayTypeExtensionInfoCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(DisplayTypeExtensionInfo value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the DisplayTypeExtensionInfoCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the DisplayTypeExtensionInfo is to be inserted.
		/// </param>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo to insert.
		/// </param>
		public virtual void Insert(int index, DisplayTypeExtensionInfo value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the DisplayTypeExtensionInfo at the given index in this DisplayTypeExtensionInfoCollection.
		/// </summary>
		public virtual DisplayTypeExtensionInfo this[int index]
		{
			get
			{
				return (DisplayTypeExtensionInfo) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific DisplayTypeExtensionInfo from this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The DisplayTypeExtensionInfo value to remove from this DisplayTypeExtensionInfoCollection.
		/// </param>
		public virtual void Remove(DisplayTypeExtensionInfo value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by DisplayTypeExtensionInfoCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(DisplayTypeExtensionInfoCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public DisplayTypeExtensionInfo Current
			{
				get
				{
					return (DisplayTypeExtensionInfo) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (DisplayTypeExtensionInfo) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this DisplayTypeExtensionInfoCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual DisplayTypeExtensionInfoCollection.Enumerator GetEnumerator()
		{
			return new DisplayTypeExtensionInfoCollection.Enumerator(this);
		}
	}
}
