using System;
using System.Text.RegularExpressions;

namespace Perfectus.Common
{
	public enum CheckOutStatus
	{
        None,
		CheckedIn,
		CheckedOut,
        CheckedOutOtherUser,
        Disassociated,
        Linked,
        Relinked,
        Unlinked
	}

    public enum MatchedAction
    {
        Replace = 0,
        CreateNew,
        Cancel
    }

	/// <summary>
	///		Global members that are utilised globally within the Perfectus.SharedLibrary namespace
	/// </summary>
	public static class Globals
	{
        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

		// Constants that relate to the named exception policies as defined in exceptionhandlingConfiguration.config
        public static string gcEXCEPTION_LEVEL1 = "Critical Priority Exception";
        public static string gcEXCEPTION_LEVEL2 = "Noncritical Priority Exception";
        public static string gcEXCEPTION_LEVEL3 = "Low Priority Exception";
		public static string gcEXCEPTION_WARN = "Warning Policy";
		public static string gcEXCEPTION_INFO = "Information Policy";

		// Constants that mark an action to perform on an object (often used in event bubbling of property checkouts)
		public static string gcLINK_MARKER = "_LINK_";
		public static string gcUNLINK_MARKER = "_UNLINK_";
		public static string gcCHECKOUT_MARKER = "_CHECKOUT_";
		public static string gcCHECKIN_MARKER = "_CHECKIN_";

		// Constant literal strings
		public static string gcSHARED_LIBRARY_TITLE = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Globals.SL");
		public static string gcCHECK_OUT_MESSAGE = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Globals.ModingSLItem");
		
        /// <summary>
        ///     Validates a date provided as a string.
        /// </summary>
        /// <param name="candidate">A string containing a date value.</param>
        /// <returns>Boolean indicating whether the candidate is a valid date or not.</returns>
		public static bool IsDate(string candidate)
		{
			bool bValid;
		
			try 
			{
                DateTime datetime = DateTime.Parse(candidate);
				bValid = true;
			}
			catch
			{
				bValid = false;
			}
			
			return bValid;
		}

        /// <summary>
        ///     Validates a guid provided as a string.
        /// </summary>
        /// <param name="candidate">A string containing a guid value.</param>
        /// <param name="output">The strongly typed guid from the given string.</param>
        /// <returns>Boolean indicating whether the candidate is a valid guid or not.</returns>
        public static bool IsGuid(string candidate, out Guid output)
        {
            bool isValid = false;
            output = Guid.Empty;

            if (candidate != null)
            {
                if (isGuid.IsMatch(candidate))
                {
                    output = new Guid(candidate);
                    isValid = true;
                }
            }

            return isValid;
        }
	}
}