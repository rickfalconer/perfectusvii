using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary.Configuration;
//dn4
//using SharedLibraryWS = Perfectus.Common.WebServices.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Web.Services3;

namespace Perfectus.Common.SharedLibrary
{
    public enum SharedLibraryAction
    {
        Add,
        CheckIn,
        CheckOut,
        GetLatestVersion,
        UndoCheckOut,
        Unlink,
        Delete
    }

	/// <summary>
	/// Summary description for SharedLibraryProxy.
	/// </summary>
	public class SharedLibraryProxy
	{
		private const string csWEBSERVICE_LOCATION = "WebServiceLocation";
        private bool _disableSharedLibraryDueToError = false;
		//dn4
        //SharedLibraryWS.SharedLibrary mSharedLibrary;

        public event EventHandler<SharedLibraryEventArgs> ProgressChanged;

		#region IRepository Members

        /// <summary>
        ///     Checks that the Shared Library is accessible and activated on the server.
        /// </summary>
        /// <returns>A flag indicating whether the Shared Library is accessible.</returns>
        public bool IsSharedLibraryActivated()
        {
            string message = string.Empty;
            return IsSharedLibraryActivated(out message);
        }

        /// <summary>
        ///     Checks that the Shared Library is accessible and activated on the server.
        /// </summary>
        /// <param name="sharedLibraryLoadMessage">
        /// Provides a message feedback mechanism that can expand on why the return value is either true or false.
        /// </param>
        /// <returns>A flag indicating whether the Shared Library is accessible.</returns>
        public bool IsSharedLibraryActivated(out string sharedLibraryLoadMessage)
		{
            //try
            //{
            //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();

            //             if (IsSharedLibraryClientActivated() && !_disableSharedLibraryDueToError)
            //             {
            //                 SetSharedLibraryWSE();
            //                 try
            //                 {
            //                     bool activated = mSharedLibrary.IsSharedLibraryActivated(out sharedLibraryLoadMessage);
            //                     return activated;
            //                 }
            //                 catch (Exception ex)
            //                 {
            //                     _disableSharedLibraryDueToError = true;

            //                     sharedLibraryLoadMessage = 
            //                         String.Format ("{0}{1}{2}",
            //                         GetResource("SharedLibraryPermanentError"),
            //                         System.Environment.NewLine,
            //                         ex.Message);

            //                     //System.Windows.Forms.MessageBox.Show(
            //                     //    sharedLibraryLoadMessage,
            //                     //    GetResource("SharedLibraryPermanentErrorCaption"),
            //                     //    System.Windows.Forms.MessageBoxButtons.OK,
            //                     //    System.Windows.Forms.MessageBoxIcon.Error);

            //                     return false;
            //                 }
            //             }
            //             else
            //             {
            //                 sharedLibraryLoadMessage = GetResource("SharedLibraryNotClientActivated");
            //                 return false;
            //             }
            //}
            //         catch(Exception ex)
            //{
            //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //             sharedLibraryLoadMessage = GetUIErrorMessage(ex);
            //             return false;
            //}
            //finally
            //{
            //	mSharedLibrary.Dispose();
            //             mSharedLibrary = null;
            //}
            sharedLibraryLoadMessage = "sharedlibrary is no longer supported";
            return false;
		}

		/// <summary>
        ///     Checks if shared library is set as activated in the client configuration file only. 
        ///     For example, in IPM a call to this method will check the IPM repository config activated flag, 
        ///     rather than going across a web service.
		/// </summary>
		/// <returns>A boolean indicating whether the shared library is active or not.</returns>
        public bool IsSharedLibraryClientActivated()
		{
			try 
			{
                RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();
                return repositoryConfigurationView.RepositoryActivated;
			}
			catch(Exception ex)
			{
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw SetSharedLibraryException(ex, null);
			}
		}

		public LibraryItem AddRepositoryItems(LibraryItem libraryItem)
		{
   //         byte[] response = null;
            
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();

   //             // Add the items to the repository
   //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryPrepareAdd")));
   //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem);

   //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryCompress")));
   //             byte[] request = Translate.ToCompressedByteArray(repositoryItem);

   //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryAdding")));
   //             response = mSharedLibrary.AddRepositoryItems(request);

   //             // Return the added items
   //             if (response != null || response.Length > 0)
   //             {
   //                 RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryDecompress")));
   //                 repositoryItem = Translate.ToUncompressedRepositoryItem(response);
   //                 libraryItem = Translate.ToLibraryItem(repositoryItem);
   //             }

                return libraryItem;
			//}
   //         catch(Exception ex)
			//{
			//	ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, libraryItem);
			//}
			//finally
			//{
			//	mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
			//}
		}

		public RepositoryFolder GetAllItemsByType(LibraryItemType type)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();
   //             //dn4
   //             //return mSharedLibrary.GetAllItemsByType(type);
                return new RepositoryFolder();
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, null);
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

        /// <summary>
        ///     Gets the latest version of the given library item from the Shared Library Web service
        /// </summary>
        /// <param name="libraryItem">The given item whose lates version is to be obtained.</param>
        /// <returns>The latest version of the given library item.</returns>
        /// <remarks>Uses MTOM WSE 3.0</remarks>
		public LibraryItem GetLatestVersion(LibraryItem libraryItem)
		{
			RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
            return GetLatestVersion(repositoryItem);
		}

        public LibraryItem GetLatestVersion(RepositoryItem repositoryItem)
        {
            //try
            //{
            //    mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //    SetSharedLibraryWSE();
                
            //    RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryCompress")));
            //    byte[] request = Translate.ToCompressedByteArray(repositoryItem);

            //    byte[] response = mSharedLibrary.GetLatestVersion(request);

            //    RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryDecompress")));
            //    repositoryItem = Translate.ToUncompressedRepositoryItem(response);

                return Translate.ToLibraryItem(repositoryItem);
            //}
            //catch (Exception ex)
            //{
            //    ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //    throw SetSharedLibraryException(ex, null);
            //}
            //finally
            //{
            //    mSharedLibrary.Dispose();
            //    mSharedLibrary = null;
            //}
        }

        public void GetCheckOutStatus(LibraryItem libraryItem)
        {
            RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
            repositoryItem = GetCheckOutStatus(repositoryItem);
            libraryItem.CheckedOutStatus = repositoryItem.CheckOutStatus;
        }

		public RepositoryItem GetCheckOutStatus(RepositoryItem repositoryItem)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();
                                
			//	byte[] request = Translate.ToByteArray(repositoryItem);
   //             byte[] response = mSharedLibrary.GetCheckOutStatus(request);
   //             repositoryItem = Translate.ToRepositoryItem(response);
                return repositoryItem;
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             repositoryItem.Linked = false;
   //             return repositoryItem;
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

		public void CheckIn(LibraryItem libraryItem)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();

			//	RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem);

   //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryCompress")));
   //             byte[] request = Translate.ToCompressedByteArray(repositoryItem);

			//	// Perform the check in
   //             byte[] response = mSharedLibrary.CheckIn(request);

   //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryDecompress")));
   //             repositoryItem = Translate.ToUncompressedRepositoryItem(response);
   //             libraryItem = Translate.ToLibraryItem(repositoryItem);
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, libraryItem);
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

		public LibraryItem CheckOut(LibraryItem libraryItem)
		{
            //try
            //{
            //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //	SetSharedLibraryWSE();

            //             // Check the item out and get the latest version
            //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
            //             byte[] request = Translate.ToByteArray(repositoryItem);
            //	byte[] response = mSharedLibrary.CheckOut(request);

            //             RaiseProgressChanged(new SharedLibraryEventArgs(GetResource("SharedLibraryDecompress")));
            //             repositoryItem = Translate.ToUncompressedRepositoryItem(response);

            //return Translate.ToLibraryItem(repositoryItem);
            //}
            //         catch (Exception ex)
            //         {
            //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //             throw SetSharedLibraryException(ex, libraryItem);
            //         }
            //         finally
            //         {
            //             mSharedLibrary.Dispose();
            //             mSharedLibrary = null;
            //         }
            return null;
		}

		public void UndoCheckout(LibraryItem libraryItem)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();

   //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
   //             byte[] request = Translate.ToByteArray(repositoryItem);
   //             mSharedLibrary.UndoCheckout(request);
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, libraryItem);
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

		public RepositoryItem DoesRepositoryItemExist(LibraryItem libraryItem)
		{
            //try
            //{
            //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //	SetSharedLibraryWSE();

            //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
            //             byte[] request = Translate.ToByteArray(repositoryItem);
            //             byte[] response = mSharedLibrary.DoesRepositoryItemExist(request);
            //             int itemNotFoundMinimumLength = 4;

            //             if (response != null && response.Length > itemNotFoundMinimumLength)
            //             {
            //return Translate.ToRepositoryItem(response);
            //             }
            //             else
            //             {
            //                 return null;
            //             }
            //}
            //         catch (Exception ex)
            //         {
            //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //             throw SetSharedLibraryException(ex, libraryItem);
            //         }
            //         finally
            //         {
            //             mSharedLibrary.Dispose();
            //             mSharedLibrary = null;
            //         }
            return null;
		}

		public bool CreateNewFolder(RepositoryFolder repositoryFolder, LibraryItemType libraryItemType)
		{
            //try
            //{
            //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //	SetSharedLibraryWSE();
            //	return mSharedLibrary.CreateNewFolder(repositoryFolder, libraryItemType);
            //}
            //         catch (Exception ex)
            //         {
            //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //             throw SetSharedLibraryException(ex, null);
            //         }
            //         finally
            //         {
            //             mSharedLibrary.Dispose();
            //             mSharedLibrary = null;
            //         }
            return false;
		}

		public void Rename(LibraryItem libraryItem, string newFileName)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
			//	SetSharedLibraryWSE();

   //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
   //             byte[] request = Translate.ToByteArray(repositoryItem);
   //             mSharedLibrary.Rename(request, newFileName);
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, libraryItem);
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

		public void Delete(LibraryItem libraryItem)
		{
			//try
			//{
   //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
   //             SetSharedLibraryWSE();

   //             RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
   //             byte[] request = Translate.ToByteArray(repositoryItem);
   //             mSharedLibrary.Delete(request);
			//}
   //         catch (Exception ex)
   //         {
   //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
   //             throw SetSharedLibraryException(ex, libraryItem);
   //         }
   //         finally
   //         {
   //             mSharedLibrary.Dispose();
   //             mSharedLibrary = null;
   //         }
		}

        public void RollBackCheckIn(LibraryItem libraryItem)
        { 
            //string user = string.Empty;

            //GetCheckOutStatus(libraryItem);

            //if (libraryItem.CheckedOutBy == String.Empty)
            //{
            //    try
            //    {
            //        mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //        SetSharedLibraryWSE();

            //        RepositoryItem repositoryItem = Translate.ToRepositoryItem(libraryItem, false);
            //        byte[] request = Translate.ToByteArray(repositoryItem);
            //        mSharedLibrary.RollBackLatestVersion(request);
            //    }
            //    catch (Exception ex)
            //    {
            //        ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //        throw ex;
            //    }
            //    finally 
            //    {
            //        mSharedLibrary.Dispose();
            //        mSharedLibrary = null;
            //    }
            //}
        }

		public string Search(string searchText)
		{
            //try
            //{
            //             mSharedLibrary = new SharedLibraryWS.SharedLibrary();
            //             SetSharedLibraryWSE();
            //	IAsyncResult result = mSharedLibrary.BeginSearch(searchText, null, null);

            //             while (!result.IsCompleted)
            //             {
            //                 Console.WriteLine("This is a test.");
            //             }

            //             return mSharedLibrary.EndSearch(result);
            //}
            //         catch (Exception ex)
            //         {
            //             ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
            //             throw SetSharedLibraryException(ex, null);
            //         }
            //         finally
            //         {
            //             mSharedLibrary.Dispose();
            //             mSharedLibrary = null;
            //         }
            return string.Empty;
		}

		#endregion

		#region Private Members

        private void SetSharedLibraryWSE()
        {
            //RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();
            //RepositoryInstance instance = repositoryConfigurationView.RepositoryConfiguration.Instances[repositoryConfigurationView.ActiveInstanceName];
            //ConnectionSetting connectionSetting = repositoryConfigurationView.RepositoryConfiguration.Settings[instance.ConnectionSetting];

            //// Populate the Sharepoint configuration settings
            //mSharedLibrary.Url = connectionSetting.ConnectionParameters[csWEBSERVICE_LOCATION].Value;
            //mSharedLibrary.PreAuthenticate = false;
            //mSharedLibrary.Credentials = CredentialCache.DefaultCredentials;
            //mSharedLibrary.UnsafeAuthenticatedConnectionSharing = true;
            //mSharedLibrary.Timeout = -1;
        }

        /// <summary>
        /// Strips a SoapException into a more user friendly error message.
        /// </summary>
        /// <param name="ex">An instance of the SoapExtension.</param>
        /// <returns>The friendly error message.</returns>
        private string GetUIErrorMessage(Exception ex)
        {
            string UIMessage;

            if (ex is SoapException)
            {
                UIMessage = ex.Message.Substring("System.Web.Services.Protocols.SoapException: Server was unable to process request. ---> ".Length);
                UIMessage = UIMessage.Substring(0, UIMessage.IndexOf("\n"));

                // Strip off the exception type from the message string
                int spacer = 2; // 2 chars - : and a space (": ")
                int colonIndex = UIMessage.IndexOf(":") > 0 ? (UIMessage.IndexOf(":") + spacer) : 0;
                UIMessage = UIMessage.Substring(colonIndex);
            }
            else
            {
                UIMessage = ex.Message;
            }

            return UIMessage;
        }

        /// <summary>
        /// Wraps a given exception with the library item being processed and sets the original exception's message
        /// to be it's message.
        /// </summary>
        /// <param name="ex">The exception being wrapped.</param>
        /// <param name="libraryItem">
        ///     The library item object that was being processed when the original exception was thrown.
        /// </param>
        /// <returns>A populated ShardLibraryException</returns>
        private SharedLibraryException SetSharedLibraryException(Exception ex, LibraryItem libraryItem)
        {
            SharedLibraryException sharedLibraryException = new SharedLibraryException(GetUIErrorMessage(ex), ex);
            sharedLibraryException.LibraryItem = libraryItem;
            return sharedLibraryException;
        }

        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        /// <summary>
        ///     Raises the ProgressChanged event notifying listeners of the change to the referenced repository item, or reports
        ///     the progress message.
        /// </summary>
        /// <param name="e"></param>
        private void RaiseProgressChanged(SharedLibraryEventArgs e)
        {
            if (ProgressChanged != null)
            {
                ProgressChanged(this, e);
            }
        }

		#endregion
	}
}
