using System;

namespace Perfectus.Common.SharedLibrary
{
    public sealed class SharedLibraryEventArgs : EventArgs
    {
        private RepositoryItem repositoryItem;
        private string message;

        public RepositoryItem CurrentRepositoryItem
        { 
            get { return repositoryItem; }
        }

        public string Message
        {
            get { return message; }
        }

        public SharedLibraryEventArgs(RepositoryItem repositoryItem, string message)
        {
            this.repositoryItem = repositoryItem;
            this.message = message;
        }

        public SharedLibraryEventArgs(string message)
        {
            this.message = message;
        }
    }
}