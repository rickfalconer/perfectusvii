#if UNIT_TESTS
using System;
using NUnit.Framework;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.SharedLibrary.Tests
{
	[TestFixture]
	public class RepositoryItemFixture
	{
		public RepositoryItemFixture(){}

		[SetUp]
		public void SetUp()
		{
		}

		[Test]
		public void ExampleTest()
		{
			Assert.AreEqual(1,2);
		}

		[TearDown]
		public void TearDown()
		{}

	}
}
#endif