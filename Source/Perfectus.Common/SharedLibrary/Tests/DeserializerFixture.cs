#if UNIT_TESTS
using System;
using System.IO;
using NUnit.Framework;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary.Tests
{
	[TestFixture]
	public class DeserializerFixture
	{
		public DeserializerFixture(){}

		[SetUp]
		public void SetUp()
		{
		}

		[Test]
		public void DeserializeArithmeticPFunction()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			// ArithmeticPFunction
			ArithmeticPFunction arithmeticPFunction = package.CreateArithmeticFunction();
			arithmeticPFunction.Name = "My Name";
			arithmeticPFunction.AlternateName1 = "Other Name";
			arithmeticPFunction.Definition = new ArithmeticExpression();

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(arithmeticPFunction);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			ArithmeticPFunction arithmeticPFunction2 = (ArithmeticPFunction)deserializer.DeserializeLibraryItem(stream, LibraryItemType.Function);

			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(arithmeticPFunction2);
			Assert.AreEqual(arithmeticPFunction.Name, arithmeticPFunction2.Name);
			Assert.AreEqual(arithmeticPFunction.AlternateName1, arithmeticPFunction2.AlternateName1);
			Assert.AreEqual(arithmeticPFunction.Definition.RootExpressionNode, arithmeticPFunction2.Definition.RootExpressionNode);

			package = null;
		}

		[Test]
		public void DeserializeDatePFunction()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			// DatePFunction
			DatePFunction datePFunction = package.CreateDateFunction();
			datePFunction.Name = "My Name";			
			datePFunction.AlternateName1 = "Other Name";
			datePFunction.Definition = new DateExpression();

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(datePFunction);
			stream.Position = 0;
			
			// Deserialize back again
			deserializer = new Deserializer();
			DatePFunction datePFunction2 = (DatePFunction)deserializer.DeserializeLibraryItem(stream, LibraryItemType.Function);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(datePFunction2);
			Assert.AreEqual(datePFunction.Name, datePFunction2.Name);
			Assert.AreEqual(datePFunction.AlternateName1, datePFunction2.AlternateName1);
			Assert.AreEqual(datePFunction.Definition.Parameter1, datePFunction2.Definition.Parameter1);

			package = null;
		}
		[Test]
		public void DeserializeInterviewPage()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			// Interview Page
			Interview interview = package.CreateInterview();
			InterviewPage interviewPage = package.CreatePage(interview);
			interviewPage.Name = "My Name";			
			interviewPage.AlternateName1 = "Other Name";
			interviewPage.Linked = true;

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(interviewPage);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			InterviewPage interviewPage2 = (InterviewPage)deserializer.DeserializeLibraryItem(stream, LibraryItemType.InterviewPage);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(interviewPage2);
			Assert.AreEqual(interviewPage.Name, interviewPage2.Name);
			Assert.AreEqual(interviewPage.AlternateName1, interviewPage2.AlternateName1);
			Assert.AreEqual(interviewPage.Seen, interviewPage2.Seen);

			package = null;
		}

		[Test]
		public void DeserializeOutcome()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			// Outcome
			Outcome outcome = package.CreateOutcome();
			outcome.Name = "My Name";
			outcome.AlternateName1 = "Other Name";
			outcome.Notes = "New Notes";

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(outcome);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			Outcome outcome2 = (Outcome)deserializer.DeserializeLibraryItem(stream, LibraryItemType.Outcome);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(outcome2);
			Assert.AreEqual(outcome.Name, outcome2.Name);
			Assert.AreEqual(outcome.AlternateName1, outcome2.AlternateName1);
			Assert.AreEqual(outcome.Definition.ActionIfTrue.UniqueIdentifier, outcome2.Definition.ActionIfTrue.UniqueIdentifier);

			package = null;
		}

		[Test]
		public void DeserializeQuestion()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			Question question = package.CreateQuestion();
			question.Name = "My Name";
			question.AlternateName1 = "Other Name";
			question.Notes = "New Notes";

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(question);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			Question question2 = (Question)deserializer.DeserializeLibraryItem(stream, LibraryItemType.Question);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(question2);
			Assert.AreEqual(question.Name, question2.Name);
			Assert.AreEqual(question.AlternateName1, question2.AlternateName1);
			Assert.AreEqual(question.UniqueIdentifier, question2.UniqueIdentifier);

			package = null;
		}

		[Test]
		public void DeserializeSimpleOutcome()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			SimpleOutcome simpleOutcome = package.CreateSimpleOutcome();
			simpleOutcome.Name = "My Name";
			simpleOutcome.AlternateName1 = "Other Name";
			simpleOutcome.Notes = "New Notes";

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(simpleOutcome);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			SimpleOutcome simpleOutcome2 = (SimpleOutcome)deserializer.DeserializeLibraryItem(stream, LibraryItemType.SimpleOutcome);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(simpleOutcome2);
			Assert.AreEqual(simpleOutcome.Name, simpleOutcome2.Name);
			Assert.AreEqual(simpleOutcome.AlternateName1, simpleOutcome2.AlternateName1);
			Assert.AreEqual(simpleOutcome.UniqueIdentifier, simpleOutcome2.UniqueIdentifier);

			package = null;
		}

		[Test]
		public void DeserializeTemplate()
		{
			MemoryStream stream;
			Deserializer deserializer;
			Package package = new Package();

			TemplateDocument templateDocument = package.CreateWordTemplate(true);
			templateDocument.Name = "My Name";
			templateDocument.AlternateName1 = "Other Name";
			templateDocument.Notes = "New Notes";

			// First serialize & reset the position of the memory stream to the start
			stream = Serializer.SerializeLibraryItem(templateDocument);
			stream.Position = 0;

			// Deserialize back again
			deserializer = new Deserializer();
			TemplateDocument templateDocument2 = (TemplateDocument)deserializer.DeserializeLibraryItem(stream, LibraryItemType.Template);
			
			// Ensure the objects values are the same
			Assert.IsNotNull(stream);
			Assert.IsNotNull(templateDocument2);
			Assert.AreEqual(templateDocument.Name, templateDocument2.Name);
			Assert.AreEqual(templateDocument.AlternateName1, templateDocument2.AlternateName1);
			Assert.AreEqual(templateDocument.UniqueIdentifier, templateDocument2.UniqueIdentifier);

			package = null;
		}

		[TearDown]
		public void TearDown()
		{}

	}
}
#endif