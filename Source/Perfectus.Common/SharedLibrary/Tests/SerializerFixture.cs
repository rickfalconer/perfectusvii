#if UNIT_TESTS
using System;
using System.IO;
using NUnit.Framework;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary.Tests
{
	[TestFixture]
	public class SerializerFixture
	{
		[SetUp]
		public void SetUp(){}

		[Test]
		public void SerializeArithmeticPFunction()
		{
			MemoryStream stream;
			Package package = new Package();

			// ArithmeticPFunction
			ArithmeticPFunction arithmeticPFunction = package.CreateArithmeticFunction();
			arithmeticPFunction.Name = "My Name";
			arithmeticPFunction.AlternateName1 = "Other Name";
			arithmeticPFunction.Definition = new ArithmeticExpression();

			stream = Serializer.SerializeLibraryItem(arithmeticPFunction);
			Assert.IsNotNull(stream);

			package = null;
		}

		[Test]
		public void SerializeDatePFunction()
		{
			MemoryStream stream;
			Package package = new Package();

			// DatePFunction
			DatePFunction datePFunction = package.CreateDateFunction();
			datePFunction.Name = "My Name";			
			datePFunction.AlternateName1 = "Other Name";
			datePFunction.Definition = new DateExpression();

			stream = Serializer.SerializeLibraryItem(datePFunction);
			Assert.IsNotNull(stream);

			package = null;
		}
		[Test]
		public void SerializeInterviewPage()
		{
			MemoryStream stream;
			Package package = new Package();

			// DatePFunction
			Interview interview = package.CreateInterview();
			InterviewPage page = package.CreatePage(interview);
			page.Name = "My Name";			
			page.AlternateName1 = "Other Name";
			page.Linked = true;

			stream = Serializer.SerializeLibraryItem(page);
			Assert.IsNotNull(stream);

			package = null;
		}

		[Test]
		public void SerializeOutcome()
		{
			MemoryStream stream;
			Package package = new Package();

			Outcome outcome = package.CreateOutcome();
			outcome.Name = "My Name";
			outcome.AlternateName1 = "Other Name";
			outcome.Notes = "New Notes";

			stream = Serializer.SerializeLibraryItem(outcome);
			Assert.IsNotNull(stream);

			package = null;
		}

		[Test]
		public void SerializeQuestion()
		{
			MemoryStream stream;
			Package package = new Package();

			Question question = package.CreateQuestion();
			question.Name = "My Name";
			question.AlternateName1 = "Other Name";
			question.Notes = "New Notes";

			stream = Serializer.SerializeLibraryItem(question);
			Assert.IsNotNull(stream);

			package = null;
		}

		[Test]
		public void SerializeSimpleOutcome()
		{
			MemoryStream stream;
			Package package = new Package();

			SimpleOutcome simpleOutcome = package.CreateSimpleOutcome();
			simpleOutcome.Name = "My Name";
			simpleOutcome.AlternateName1 = "Other Name";
			simpleOutcome.Notes = "New Notes";

			stream = Serializer.SerializeLibraryItem(simpleOutcome);
			Assert.IsNotNull(stream);

			package = null;
		}

		[Test]
		public void SerializeTemplate()
		{
			MemoryStream stream;
			Package package = new Package();

			TemplateDocument templateDocument = package.CreateWordTemplate(true);
			templateDocument.Name = "My Name";
			templateDocument.AlternateName1 = "Other Name";
			templateDocument.Notes = "New Notes";

			stream = Serializer.SerializeLibraryItem(templateDocument);
			Assert.IsNotNull(stream);

			package = null;
		}

		[TearDown]
		public void TearDown(){}
	}
}
#endif