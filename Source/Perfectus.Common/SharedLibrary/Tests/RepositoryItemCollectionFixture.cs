#if UNIT_TESTS
using System;
using System.Collections.Generic;
using NUnit.Framework;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.SharedLibrary.Tests
{
	[TestFixture]
	public class RepositoryItemCollectionFixture
	{
        private List<RepositoryItem> RepositoryItems;

		[SetUp]
		public void SetUp()
		{
            RepositoryItems = new List<RepositoryItem>();
		}

		[Test]
		public void EnumeratorCurrentTest()
		{
			RepositoryItem repositoryItem = new RepositoryItem();
			RepositoryItems.Add(repositoryItem);
			RepositoryItem repositoryItem2 = new RepositoryItem();
			RepositoryItems.Add(repositoryItem2);
			int count = 0;
			foreach (RepositoryItem item in RepositoryItems)
			{
				Assert.IsNotNull(item);
				count++;
				foreach (RepositoryItem item2 in RepositoryItems)
				{
					Assert.IsNotNull(item2);
					count++;
				}
			}
			Assert.AreEqual(6, count);
		}

		[Test]
		public void AddRemoveTest()
		{
			RepositoryItem repositoryItem = new RepositoryItem();
			RepositoryItems.Add(repositoryItem);
			Assert.AreEqual(1, RepositoryItems.Count);
			RepositoryItems.Remove(repositoryItem);
		}

		[Test]
		public void AddItemTest()
		{
			RepositoryItem repositoryItem = new RepositoryItem();
			RepositoryItems.Add(repositoryItem);
			Assert.AreEqual(1, RepositoryItems.Count);
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void RemoveNullObjectTest()
		{
			RepositoryItems.Remove(null);
		}

		[Test]
		public void ItemTest()
		{
			RepositoryItem repositoryItem = new RepositoryItem();
			RepositoryItems.Add(repositoryItem);
			Assert.AreEqual(1, RepositoryItems.Count);
		}

		[Test]
		public void ClearTest()
		{
			RepositoryItem repositoryItem = new RepositoryItem();
			RepositoryItems.Add(repositoryItem);
			Assert.AreEqual(1, RepositoryItems.Count);
			RepositoryItems.Clear();
			Assert.AreEqual(0, RepositoryItems.Count);
		}
	}
}
#endif