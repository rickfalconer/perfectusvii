/*
#if  UNIT_TESTS

using System;
using Microsoft.Practices.EnterpriseLibrary.Configuration;
using Perfectus.Common.SharedLibrary.Configuration;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.SharedLibrary.Tests
{
	public class TestConfigurationContext : ConfigurationContext
	{
		private static ConfigurationDictionary dictionary;

		public const string NorthwindDummyUser = "entlib";
		public const string NorthwindDummyPassword = "hdf7&834k(*KA";

		public TestConfigurationContext()
			: base(GenerateConfigurationDictionary())
		{
		}

		private static ConfigurationDictionary GenerateConfigurationDictionary()
		{
			if (dictionary == null)
			{
				dictionary = new ConfigurationDictionary();
				dictionary.Add(ConfigurationSettings.SectionName, GenerateConfigurationSettings());
				dictionary.Add(RepositorySettings.SectionName, GenerateDataSettings());
			}
			return dictionary;
		}

		private static ConfigurationSettings GenerateConfigurationSettings()
		{
			ConfigurationSettings settings = new ConfigurationSettings();
			settings.ConfigurationSections.Add(new ConfigurationSectionData(RepositorySettings.SectionName, false, new XmlFileStorageProviderData("XmlStorage", "repositoryConfiguration.config"), new XmlSerializerTransformerData("DataBuilder")));
			return settings;
		}

		private static RepositorySettings GenerateDataSettings()
		{
			RepositorySettings settings = new RepositorySettings();

			settings.DefaultInstance = "SharedLibraryRepository";

			settings.RepositoryTypes.Add(new RepositoryTypeData("Sharepoint", Type.GetType("Perfectus.SharedLibrary.Sharepoint.SharepointRepository, Perfectus.SharedLibrary.Sharepoint").AssemblyQualifiedName));

			ConnectionStringData data = new ConnectionStringData("SharepointRepository");
			data.Parameters.Add(new ParameterData("RepositoryType", "sharepoint"));
			data.Parameters.Add(new ParameterData("RepositoryName", "Perfectus%20Shared%20Library"));
			data.Parameters.Add(new ParameterData("RepositoryLocation", "http://localhost/"));
			data.Parameters.Add(new ParameterData("RepositorySubLocation", ""));
			settings.ConnectionStrings.Add(data);

			settings.Instances.Add(new InstanceData("SharedLibraryRepository", "Sharepoint", "SharepointRepository"));

			return settings;
		}
	}
}
#endif
*/