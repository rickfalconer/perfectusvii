using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary
{
	[Serializable]
	public class RepositoryItem
	{
		private Guid _LibraryUniqueIdentifier;
        private string _Name;
        private string _Title;
        private string _Filename;
		private int _Version;
		private RepositoryFolder _RepositoryFolder;
		private LibraryItemType _LibraryItemType;
        private string _PerfectusType;
		private bool _Linked;
		private string _CreatedBy;
		private DateTime _CreatedDateTime;
		private string _CheckedOutBy;
		private string _CheckedOutByLoginName;
		private DateTime _CheckedOutDateTime;
		private CheckOutStatus _CheckOutStatus;
		private string _ModifiedBy;
		private DateTime _ModifiedDateTime;
		private string _CheckInComment;
		private string _RelativePath;
		private bool _DependentObject;
		private List<RepositoryItem> _DependentRepositoryItems;
        private bool _IsNewLibraryItem;
		private string _XmlSerialisedContents;

		public Guid LibraryUniqueIdentifier
		{
			get { return _LibraryUniqueIdentifier;  }
			set { _LibraryUniqueIdentifier = value; }
		}

		public string Name
		{
			get { return _Name;  }
			set { _Name = value; }
		}

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Filename
        {
            get { return _Filename;  }
            set { _Filename = value; }
        }

		public int Version
		{
			get { return _Version;  }
			set { _Version = value; }
		}

		public RepositoryFolder Folder
		{
			get { return _RepositoryFolder;  }
			set { _RepositoryFolder = value; }
		}

		public LibraryItemType LibraryItemType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

        public string PerfectusType
        {
            get { return _PerfectusType;  }
            set { _PerfectusType = value; }
        }

		public bool Linked
		{
			get { return _Linked;  }
			set { _Linked = value; }
		}

		public string CreatedBy
		{
			get { return _CreatedBy;  }
			set { _CreatedBy = value; }
		}

		public DateTime CreatedDateTime
		{
			get { return _CreatedDateTime;  }
			set { _CreatedDateTime = value; }
		}

		public string CheckedOutBy
		{
			get { return _CheckedOutBy;  }
			set { _CheckedOutBy = value; }
		}

		public string CheckedOutByLoginName
		{
			get { return _CheckedOutByLoginName;  }
			set { _CheckedOutByLoginName = value; }
		}

		public DateTime CheckedOutDateTime
		{
			get { return _CheckedOutDateTime;  }
			set { _CheckedOutDateTime = value; }
		}

		public CheckOutStatus CheckOutStatus
		{
			get { return _CheckOutStatus;  }
			set { _CheckOutStatus = value; }
		}

		public string ModifiedBy
		{
			get { return _ModifiedBy;  }
			set { _ModifiedBy = value; }
		}

		public DateTime ModifiedDateTime
		{
			get { return _ModifiedDateTime;  }
			set { _ModifiedDateTime = value; }
		}

		public string CheckInComment
		{
			get { return _CheckInComment;  }
			set { _CheckInComment = value; }
		}

		public string RelativePath
		{
			get { return _RelativePath;  }
			set { _RelativePath = value; }
		}

		public bool DependentObject
		{
			get { return _DependentObject;  }
			set { _DependentObject = value; }
		}

		public List<RepositoryItem> DependentRepositoryItems
		{
			get { return _DependentRepositoryItems;  }
			set { _DependentRepositoryItems = value; }
		}

        public bool IsNewLibraryItem
        {
            get { return _IsNewLibraryItem;  }
            set { _IsNewLibraryItem = value; }
        }

		public string SerialisedContents
		{
			get { return _XmlSerialisedContents;  }
			set { _XmlSerialisedContents = value; }
		}

        public byte[] SerialisedContentsByteArray
        {
            get 
            {
                if (_XmlSerialisedContents != null && _XmlSerialisedContents != string.Empty)
                {
                    return UTF8Encoding.UTF8.GetBytes(_XmlSerialisedContents);
                }
                else
                {
                    return null;
                }
            }
            set { _XmlSerialisedContents = UTF8Encoding.UTF8.GetString(value); }
        }

		public RepositoryItem(){}

		public RepositoryItem(string name, LibraryItemType type, string relativePath,  string createdBy, 
                              CheckOutStatus checkOutStatus, int version, bool dependentObject)
		{
            this.Name = name;
			this.LibraryItemType = type;
			this.Linked = true;						// Default setting - initially the item is linked to the shared library
			this.RelativePath = relativePath;
			this.CheckOutStatus = checkOutStatus;
			this.Version = version;
            this.DependentObject = dependentObject;
		}
	}
}