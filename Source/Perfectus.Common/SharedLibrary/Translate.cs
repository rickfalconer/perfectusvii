using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.XPath;
using Perfectus.Common.PackageObjects;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common.Helper;

namespace Perfectus.Common.SharedLibrary
{
    public class Translate
    {
        public static byte[] ToByteArray(List<RepositoryItem> repositoryItems)
        {
            MemoryStream stream = Serializer.ToStream(repositoryItems);
            return stream.ToArray();
        }

        public static byte[] ToByteArray(RepositoryItem repositoryItem)
        {
            MemoryStream stream = Serializer.ToStream(repositoryItem);
            return stream.ToArray();
        }

        public static byte[] ToCompressedByteArray(RepositoryItem repositoryItem)
        {
            MemoryStream msCompressed = new MemoryStream();
            byte[] bytesBuffer = ToByteArray(repositoryItem);
            bytesBuffer = CompressionHelper.CompressIfSmaller(bytesBuffer);
            return bytesBuffer;
        }

        public static RepositoryItem ToRepositoryItem(LibraryItem libraryItem)
        {
            return ToRepositoryItem(libraryItem, true);
        }

        public static RepositoryItem ToRepositoryItem(LibraryItem libraryItem, bool serialiseLibraryItem)
        {
            RepositoryItem repositoryItem;

            // Populate the base repository from the library item
            repositoryItem = new RepositoryItem(libraryItem.Name, libraryItem.LibraryType, libraryItem.RelativePath, libraryItem.CreatedBy,
                                                libraryItem.CheckedOutStatus, libraryItem.Version, libraryItem.DependentObject);

            repositoryItem.Title = libraryItem.Name;
            repositoryItem.Folder = libraryItem.RepositoryFolder;
            repositoryItem.PerfectusType = libraryItem.GetType().ToString();
            repositoryItem.LibraryUniqueIdentifier = libraryItem.LibraryUniqueIdentifier;

            // Default the relative path if empty
            if (repositoryItem.RelativePath == null || repositoryItem.RelativePath == string.Empty)
            {
                if (repositoryItem.Folder != null)
                {
                    libraryItem.RelativePath =
                    repositoryItem.RelativePath = Serializer.GetRelativePath(repositoryItem.Folder, repositoryItem.Name, 
                                                                             repositoryItem.LibraryItemType);
                }
                else
                {
                    libraryItem.RelativePath = 
                    repositoryItem.RelativePath = Serializer.GetDefaultRelativePath(repositoryItem.Name, repositoryItem.LibraryItemType);
                }
            }

            // Only serialise when required - (not for simple checkout status checks etc).
            if (serialiseLibraryItem)
            {
                repositoryItem.DependentRepositoryItems = Translate.ToRepositoryItems(libraryItem.DependentLibraryItems);
                repositoryItem.SerialisedContentsByteArray = Serializer.SerializeLibraryItem(libraryItem).ToArray();
            }
            
            return repositoryItem;
        }

        public static List<RepositoryItem> ToRepositoryItems(List<LibraryItem> libraryItems)
        {
            List<RepositoryItem> repositoryItems = new List<RepositoryItem>();

            if (libraryItems == null)
            {
                return null;
            }

            foreach (LibraryItem libraryItem in libraryItems)
            {
                repositoryItems.Add(ToRepositoryItem(libraryItem));
            }

            return repositoryItems;
        }

        public static RepositoryItem ToUncompressedRepositoryItem(byte[] compbytes)
        {
            return ToRepositoryItem(CompressionHelper.DecompressIfNecessary(compbytes));
        }

        /// <summary>
        ///     Get the RepositoryItem from the request byte array
        /// </summary>
        /// <param name="bytes">Byte array containing the RepositoryItem.</param>
        /// <returns>An instance a populated RepositoryItem.</returns>
        public static RepositoryItem ToRepositoryItem(byte[] bytes)
        {
            MemoryStream stream = new MemoryStream();

            try
            {
                stream.Write(bytes, 0, bytes.Length);
                return Deserializer.ToRepositoryItem(stream);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw ex;
            }
        }

        /// <summary>
        ///     Get a list of RepositoryItem's from the bytes
        /// </summary>
        /// <param name="bytes">Byte array containing the RepositoryItems.</param>
        /// <returns>An instance a populated list of RepositoryItem instances.</returns>
        public static List<RepositoryItem> ToRepositoryItems(byte[] bytes)
        {
            MemoryStream stream = new MemoryStream();

            try
            {
                stream.Write(bytes, 0, bytes.Length);
                return Deserializer.ToRepositoryItems(stream);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw ex;
            }
        }

        public static LibraryItem ToLibraryItem(RepositoryItem repositoryItem)
        {
            Deserializer deserializer = new Deserializer();
            MemoryStream stream = new MemoryStream(repositoryItem.SerialisedContentsByteArray);
            LibraryItem libraryItem = deserializer.DeserializeLibraryItem(stream, repositoryItem.LibraryItemType);

            libraryItem.Linked = repositoryItem.Linked;
            libraryItem.LibraryType = repositoryItem.LibraryItemType;
            libraryItem.CreatedBy = repositoryItem.CreatedBy;
            libraryItem.CreatedDateTime = repositoryItem.CreatedDateTime;
            libraryItem.Filename = repositoryItem.Filename;
            libraryItem.PerfectusType = repositoryItem.PerfectusType;
            libraryItem.RelativePath = repositoryItem.RelativePath;
            libraryItem.Version = repositoryItem.Version;
            libraryItem.CheckedOutBy = repositoryItem.CheckedOutBy;
            libraryItem.CheckedOutByLogInName = repositoryItem.CheckedOutByLoginName;
            libraryItem.CheckedOutDateTime = repositoryItem.CheckedOutDateTime;
            libraryItem.CheckedOutStatus = repositoryItem.CheckOutStatus;
            libraryItem.DependentLibraryItems = new List<LibraryItem>();

            if (repositoryItem.DependentRepositoryItems != null)
            {
                foreach (RepositoryItem dependentRepositoryItem in repositoryItem.DependentRepositoryItems)
                {
                    libraryItem.DependentLibraryItems.Add(Translate.ToLibraryItem(dependentRepositoryItem));
                }
            }

            return libraryItem;
        }

        /// <summary>
        ///     Tranlsates a list of repostitory items into a list of library items.
        /// </summary>
        /// <param name="repositoryItems">The lsit of repository items to be translated.</param>
        /// <returns>The translated list of library items.</returns>
        public static List<LibraryItem> ToLibraryItems(List<RepositoryItem> repositoryItems)
        {
            LibraryItem libraryItem;
            List<LibraryItem> libraryItems = new List<LibraryItem>();

            foreach (RepositoryItem repositoryItem in repositoryItems)
            {
                libraryItem = ToLibraryItem(repositoryItem);
                libraryItems.Add(libraryItem);
            }

            return libraryItems;
        }

        public static RepositoryItem ToRepositoryItem(XPathNavigator navigator)
        {
            RepositoryItem repositoryItem = new RepositoryItem();

            // Type
            navigator.MoveToFirstChild();
            repositoryItem.PerfectusType = navigator.Value;

            // Name
            navigator.MoveToNext();
            repositoryItem.Name = navigator.Value;

            // Unique Identifier
            navigator.MoveToNext();
            repositoryItem.RelativePath = navigator.Value;

            // Relative Path
            navigator.MoveToNext();
            repositoryItem.RelativePath = navigator.Value;

            return repositoryItem;
        }

        private static LibraryItem GetNewLibraryItemByType(string type)
        {
            if (type == typeof(Question).ToString())
            {
                return new Question();
            }
            else if (type == typeof(Outcome).ToString())
            {
                return new Outcome();
            }
            else if (type == typeof(WordTemplateDocument2).ToString())
            {
                return new WordTemplateDocument2();
            }
            else if (type == typeof(SimpleOutcome).ToString())
            {
                return new SimpleOutcome();
            }
            else if (type == typeof(InternalPage).ToString())
            {
                return new InternalPage();
            }
            else if (type == typeof(ArithmeticPFunction).ToString())
            {
                return new ArithmeticPFunction();
            }
            else if (type == typeof(DatePFunction).ToString())
            {
                return new DatePFunction();
            }
            else
            { 
                //TODO - RESOURCE - Need to localise and provide the error message
                throw new InvalidOperationException("Could not determine the library item type to create.");
            }
        }
    }
}
