using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Perfectus.Common.SharedLibrary.Configuration;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary
{
	/// <summary>
	///		Interface defining the Repository and its operations
	/// </summary>
	public interface IRepository
	{
		// Property Methods
        bool IsSharedLibraryActivated();
		bool IsSharedLibraryActivated(out string sharedLibraryLoadMessage);

		// Upload Methods
        void AddRepositoryItems(RepositoryItem repositoryItem);
		
		// Download Methods
		RepositoryFolder GetAllItemsByType(LibraryItemType type);
        void GetLatestVersion(RepositoryItem repositoryItem);

		// Check In / Out Methods
        void GetCheckOutStatus(RepositoryItem repositoryItem);
        void CheckIn(RepositoryItem repositoryItem);
        void CheckOut(RepositoryItem repositoryItem);
        void UndoCheckout(RepositoryItem repositoryItem);
        void RollBackLatestVersion(RepositoryItem repositoryItem);

		// Misc Methods
        RepositoryItem DoesRepositoryItemExist(RepositoryItem repositoryItem);
		bool CreateNewFolder(RepositoryFolder repositoryFolder, LibraryItemType libraryItemType);

		// Additional Admin Functional Methods
        void Rename(RepositoryItem libraryItem, string newFileName);
        void Delete(RepositoryItem libraryItem);
        void DeleteAll();
        void DeleteByType(LibraryItemType type);
		string Search(string searchText);
	}
}