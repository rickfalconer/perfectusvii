using System;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    public class Parameter : ConfigurationElement
    {
        #region Properties

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return Convert.ToString(this["name"]); }
        }

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value
        {
            get { return Convert.ToString(this["value"]); }
        }

        #endregion
        #region Constructors

        public Parameter()
        { }

        public Parameter(string name)
        {
            this["name"] = name;
        }

        #endregion
    }
}