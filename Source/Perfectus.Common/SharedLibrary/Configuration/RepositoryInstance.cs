using System;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    public class RepositoryInstance : ConfigurationElement
    {
        #region Properties

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return Convert.ToString(this["name"]); }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
        }

        [ConfigurationProperty("connectionSetting", IsRequired = true)]
        public string ConnectionSetting
        {
            get { return Convert.ToString(this["connectionSetting"]); }
        }

        #endregion
        #region Constructors

        public RepositoryInstance()
        { }

        public RepositoryInstance(string name)
        {
            this["name"] = name;
        }

        #endregion
    }
}