using System;
using System.Collections;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    [ConfigurationCollection(typeof(RepositoryInstance), AddItemName = "instance")]
    public class RepositoryInstances : ConfigurationElementCollection
    {
        public RepositoryInstances()
        {
            AddElementName = "instance";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override string ElementName
        {
            get
            {
                return "instance";
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RepositoryInstance();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new RepositoryInstance(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((RepositoryInstance)element).Name;
        }

        public new string AddElementName
        {
            get { return base.AddElementName; }
            set { base.AddElementName = value; }
        }

        public new string ClearElementName
        {
            get { return base.ClearElementName; }
            set { base.AddElementName = value; }
        }

        public new string RemoveElementName
        {
            get { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }
        
        public RepositoryInstance this[int index]
        {
            get
            {
                return (RepositoryInstance)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        new public RepositoryInstance this[string Name]
        {
            get
            {
                return (RepositoryInstance)BaseGet(Name);
            }
        }

        public int IndexOf(RepositoryInstance instance)
        {
            return BaseIndexOf(instance);
        }

        public void Add(RepositoryInstance instance)
        {
            BaseAdd(instance);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(RepositoryInstance instance)
        {
            if (BaseIndexOf(instance) >= 0)
            {
                BaseRemove(instance.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}