using System;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    public class ConnectionSetting : ConfigurationElement
    {
        #region Properties

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return Convert.ToString(this["name"]); }
        }

        [ConfigurationProperty("parameters", IsRequired = true)]
        public Parameters ConnectionParameters
        {
            get { return (Parameters)this["parameters"]; }
        }

        #endregion
        #region Constructors

        public ConnectionSetting()
        { }

        public ConnectionSetting(string name)
        {
            this["name"] = name;
        }

        #endregion
    }
}