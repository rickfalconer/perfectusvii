using System;
using System.Collections;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    [ConfigurationCollection(typeof(Parameter), AddItemName = "parameter")]
    public class Parameters : ConfigurationElementCollection
    {
        public Parameters()
        {
            AddElementName = "parameter";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override string ElementName
        {
            get
            {
                return "parameter";
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Parameter();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new Parameter(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((Parameter)element).Name;
        }

        public new string AddElementName
        {
            get { return base.AddElementName; }
            set { base.AddElementName = value; }
        }

        public new string ClearElementName
        {
            get { return base.ClearElementName; }
            set { base.AddElementName = value; }
        }

        public new string RemoveElementName
        {
            get { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }


        public Parameter this[int index]
        {
            get
            {
                return (Parameter)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        new public Parameter this[string Name]
        {
            get
            {
                return (Parameter)BaseGet(Name);
            }
        }

        public int IndexOf(Parameter instance)
        {
            return BaseIndexOf(instance);
        }

        public void Add(Parameter instance)
        {
            BaseAdd(instance);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(Parameter instance)
        {
            if (BaseIndexOf(instance) >= 0)
            {
                BaseRemove(instance.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}