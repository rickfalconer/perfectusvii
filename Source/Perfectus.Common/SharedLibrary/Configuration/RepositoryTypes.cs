using System;
using System.Collections;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    [ConfigurationCollection(typeof(RepositoryType), AddItemName = "repositoryType")]
    public class RepositoryTypes : ConfigurationElementCollection
    {
        public RepositoryTypes()
        {
            AddElementName = "repositoryType";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override string ElementName
        {
            get
            {
                return "repositoryType";
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RepositoryType();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new RepositoryType(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((RepositoryType)element).Name;
        }

        public new string AddElementName
        {
            get { return base.AddElementName; }
            set { base.AddElementName = value; }
        }

        public new string ClearElementName
        {
            get { return base.ClearElementName; }
            set { base.AddElementName = value; }
        }

        public new string RemoveElementName
        {
            get { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }


        public RepositoryType this[int index]
        {
            get
            {
                return (RepositoryType)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        new public RepositoryType this[string Name]
        {
            get
            {
                return (RepositoryType)BaseGet(Name);
            }
        }

        public int IndexOf(RepositoryType instance)
        {
            return BaseIndexOf(instance);
        }

        public void Add(RepositoryType instance)
        {
            BaseAdd(instance);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(RepositoryType instance)
        {
            if (BaseIndexOf(instance) >= 0)
            {
                BaseRemove(instance.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}