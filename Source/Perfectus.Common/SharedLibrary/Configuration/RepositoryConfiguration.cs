using System;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    public class RepositoryConfiguration : System.Configuration.ConfigurationSection
    {
        private static RepositoryConfiguration repositoryConfiguration;
        private System.Configuration.Configuration configuration;

        public System.Configuration.Configuration Configuration
        {
            get { return configuration; }
            set { configuration = value; }
        }
        
        [ConfigurationProperty("activeInstance", IsRequired = true, IsKey = true)]
        public string ActiveInstance
        {
            get { return Convert.ToString(this["activeInstance"]); }
        }

        [ConfigurationProperty("activated", IsRequired = true, IsKey = true)]
        public bool Active
        {
            get { return Convert.ToBoolean(this["activated"]); }
        }

        [ConfigurationProperty("repositoryTypes", IsRequired = true, IsKey = true)]
        public RepositoryTypes Types
        {
            get { return (RepositoryTypes)(this["repositoryTypes"]); }
        }

        [ConfigurationProperty("instances", IsRequired = true, IsKey = true)]
        public RepositoryInstances Instances
        {
            get { return (RepositoryInstances)(this["instances"]); }
        }

        [ConfigurationProperty("connectionSettings", IsRequired = true, IsKey = true)]
        public ConnectionSettings Settings
        {
            get { return (ConnectionSettings)(this["connectionSettings"]); }
        }

        /// <summary>
        ///     Returns an instance of a RepositoryConfiguration object, which is a representation
        ///     of the configuration file settings for the shared library repository
        /// </summary>
        public static RepositoryConfiguration Current
        {
            get
            {
                if (repositoryConfiguration == null)
                {
                    System.Configuration.Configuration config;
                    HttpContext httpContext = HttpContext.Current;

                    // Handle the different app contexts (WinForms | ASPX)
                    if (httpContext == null)
                    {
                        config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    }
                    else
                    {
                        config = WebConfigurationManager.OpenWebConfiguration(httpContext.Request.ApplicationPath);
                    }

                    repositoryConfiguration = (RepositoryConfiguration)config.Sections["repositoryConfiguration"];
                    repositoryConfiguration.Configuration = config;
                }

                return RepositoryConfiguration.repositoryConfiguration;
            }
        }
    }
}