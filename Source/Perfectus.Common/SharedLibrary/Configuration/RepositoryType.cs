using System;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    public class RepositoryType : ConfigurationElement
    {
        #region Properties

        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return Convert.ToString(this["name"]); }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return Convert.ToString(this["type"]); }
        }

        #endregion
        #region Constructors

        public RepositoryType()
        { }

        public RepositoryType(string name)
        {
            this["name"] = name;
        }

        #endregion
    }
}