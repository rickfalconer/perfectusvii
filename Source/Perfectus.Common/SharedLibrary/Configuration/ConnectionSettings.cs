using System;
using System.Collections;
using System.Configuration;
using System.Text;

namespace Perfectus.Common.SharedLibrary.Configuration
{
    [ConfigurationCollection(typeof(ConnectionSetting), AddItemName = "connectionSetting")]
    public class ConnectionSettings : ConfigurationElementCollection
    {
        public ConnectionSettings()
        {
            AddElementName = "connectionSetting";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override string ElementName
        {
            get
            {
                return "connectionSetting";
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConnectionSetting();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            return new ConnectionSetting(elementName);
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((ConnectionSetting)element).Name;
        }

        public new string AddElementName
        {
            get { return base.AddElementName; }
            set { base.AddElementName = value; }
        }

        public new string ClearElementName
        {
            get { return base.ClearElementName; }
            set { base.AddElementName = value; }
        }

        public new string RemoveElementName
        {
            get { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }

        public ConnectionSetting this[int index]
        {
            get
            {
                return (ConnectionSetting)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }

                BaseAdd(index, value);
            }
        }

        new public ConnectionSetting this[string Name]
        {
            get
            {
                return (ConnectionSetting)BaseGet(Name);
            }
        }

        public int IndexOf(ConnectionSetting instance)
        {
            return BaseIndexOf(instance);
        }

        public void Add(ConnectionSetting instance)
        {
            BaseAdd(instance);
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(ConnectionSetting instance)
        {
            if (BaseIndexOf(instance) >= 0)
            {
                BaseRemove(instance.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}