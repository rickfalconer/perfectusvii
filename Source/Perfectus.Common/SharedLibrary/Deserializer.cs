using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary
{
	/// <summary>
	///		Provides the functionality to deserialize memory streams containing Xml data of Library Item objects into their
	///		specific IP Object Type.
	/// </summary>
	public class Deserializer
	{
		XmlSerializer xmlSerializer;

        public static RepositoryItem ToRepositoryItem(MemoryStream stream)
        {
            RepositoryItem repositoryItem;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Context = new StreamingContext(StreamingContextStates.Remoting);
            stream.Position = 0; // Ensure the stream pointer is at the start
            repositoryItem = (RepositoryItem)bformatter.Deserialize(stream);

            return repositoryItem;
        }

        public static List<RepositoryItem> ToRepositoryItems(MemoryStream stream)
        {
            List<RepositoryItem> repositoryItems;
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Context = new StreamingContext(StreamingContextStates.Remoting);
            stream.Position = 0; // Ensure the stream pointer is at the start
            repositoryItems = (List<RepositoryItem>)bformatter.Deserialize(stream);

            return repositoryItems;
        }

		/// <summary>
		///		Forwards the request to deserialize a memory stream into a library item object to the
		///		appropriate function, using the given LibraryItemType.
		/// </summary>
		/// <param name="stream">A memory stream containing the Xml of the object to be deserialized.</param>
		/// <param name="libraryItemType">The type of library item that is contained within the memory stream.</param>
		/// <returns>A populated library item of the specific type as given by the LibraryItemType.</returns>
		public LibraryItem DeserializeLibraryItem(MemoryStream stream, LibraryItemType libraryItemType)
		{
			LibraryItem libraryItem = null;

			try
			{
                // Deserialize the appropriate item type
                switch (libraryItemType)
                {				
				    case LibraryItemType.Question:
                        libraryItem = DeserializeQuestion(stream);
                        break;
                    case LibraryItemType.Outcome:
                        libraryItem = DeserializeOutcome(stream);
                        break;
                    case LibraryItemType.SimpleOutcome:
                        libraryItem = DeserializeSimpleOutcome(stream);
                        break;
                    case LibraryItemType.Template:
                        libraryItem = DeserializeTemplate(stream);
                        break;
                    case LibraryItemType.InterviewPage:
                        libraryItem = DeserializeInterviewPage(stream);
                        break;
                    case LibraryItemType.Function:
                        libraryItem = DeserializeFunction(stream);
                        break;
                    case LibraryItemType.None:
                	default:
						throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.Deserializer.SerializationTypeInvalid"));
				}
			}
			catch(Exception ex)
			{
				ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				throw ex;
			}

			return libraryItem;
		}

		/// <summary>
		///		Deserializes a PFunction object from the given memory stream, 			
		///		which contains the serialized PFunction object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeFunction(MemoryStream stream)
		{
			// Deserialize the specific function type
			xmlSerializer = new XmlSerializer(typeof(PFunction), new Type[]{typeof(AnswerProvider), 
																			typeof(DatePFunction),
																			typeof(ArithmeticPFunction),
																		    typeof(Question),
																			typeof(PackageConstant)});
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}

		/// <summary>
		///		Deserializes a InterviewPage object from the given memory stream, 			
		///		which contains the serialized InterviewPage object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeInterviewPage(MemoryStream stream)
		{
			// Deserialize the InterviewPage
			xmlSerializer = new XmlSerializer(typeof(InternalPage), new Type[]{typeof(ActionBase), 
																			   typeof(OutcomeAction), 
																			   typeof(WordTemplateDocument2)});
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}

		/// <summary>
		///		Deserializes a Outcome object from the given memory stream, 			
		///		which contains the serialized Outcome object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeOutcome(MemoryStream stream)
		{
			// Deserialize the Outcome
			xmlSerializer = new XmlSerializer(typeof(Outcome), new Type[]{typeof(Outcome), 
																		  typeof(ActionBase), 
																		  typeof(OutcomeAction), 
																		  typeof(WordTemplateDocument2)});			
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}

		/// <summary>
		///		Deserializes a Question object from the given memory stream, 
		///		which contains the serialized Question object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeQuestion(MemoryStream stream)
		{
			// Deserialize the question
			xmlSerializer = new XmlSerializer(typeof(Question), new Type[] {typeof(ItemsItemCollection), 
                                                                            typeof(ItemsItem)});
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}

		/// <summary>
		///		Deserializes a SimpleOutcome object from the given memory stream, 			
		///		which contains the serialized SimpleOutcome object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeSimpleOutcome(MemoryStream stream)
		{
			// Deserialize the SimpleOutcome
			xmlSerializer = new XmlSerializer(typeof(SimpleOutcome));
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}

		/// <summary>
		///		Deserializes a TemplateDocument object from the given memory stream, 			
		///		which contains the serialized TemplateDocument object as xml.
		/// </summary>
		/// <param name="stream">A stream that contains the objects Xml.</param>
		/// <returns>A populated LibraryItem object.</returns>
		private LibraryItem DeserializeTemplate(MemoryStream stream)
		{
			// Deserialize the TemplateDocument
			xmlSerializer = new XmlSerializer(typeof(WordTemplateDocument2), new Type[]{typeof(ActionBase), 
																						typeof(OutcomeAction), 
																						typeof(TemplateDocument)});
			return (LibraryItem) xmlSerializer.Deserialize(stream);
		}
	}
}