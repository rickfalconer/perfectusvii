using System;
using System.Reflection;
using System.Web;
using Perfectus.Common.SharedLibrary.Configuration;

namespace Perfectus.Common.SharedLibrary
{
	/// <summary>
	/// <para>Represents a factory for creating named instances Repository objects.</para>
	/// </summary>
	public class RepositoryProviderFactory
	{
		/// <summary>
		/// <para>Initialize a new instance of the Repository class.</para>
		/// </summary>
		public RepositoryProviderFactory()
		{}

		/// <summary>
		/// <para>Creates the <see cref="Repository"/> object from the configuration data associated with the active Repository instance.</para>
		/// </summary>
		/// <returns><para>A <see cref="Repository"/> object.</para></returns>
		public IRepository CreateActiveRepository()
		{
			return (IRepository)CreateActiveInstance();
		}

		/// <summary>
		/// <para>Gets the default active repository instance name.</para>
		/// </summary>
		/// <returns>
		/// <para>The default repository instance name.</para>
		/// </returns>
		protected string GetDefaultInstanceName()
		{
            return RepositoryConfiguration.Current.ActiveInstance;
		}

        /// <summary>
        ///     Loads the repository configuration from the config file and using the settings, creates an instance of the active
        ///     repository assembly of type IRepository and returns it to the caller.
        /// </summary>
        /// <returns>An instance of an object implementing IRepository.</returns>
        private IRepository CreateActiveInstance()
        {
            IRepository repository;

            // Get the type of the active instance
            RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();            
            RepositoryType repositoryType = repositoryConfigurationView.GetActiveInstanceType();
                        
            // Load the respoitory instance and return it
            repository = LoadRepositoryType(repositoryType);
            return repository;
        }

        /// <summary>
        ///     Loads the given repository type. Checking is then performed before returning to the 
        ///     calling function.
        /// </summary>
        /// <param name="type">Repository Type object used load the given type.</param>
        /// <returns>An instance of type IRepositry.</returns>
        private IRepository LoadRepositoryType(RepositoryType repositoryType)
        {
            int classIndex = 0;
            int assemblyIndex = 1;
            string[] typeInfo = new string[1];

            // Using Reflection to get information from an Assembly:
            try
            {
                // Load the repository assembly
                typeInfo = repositoryType.Type.Split((",").ToCharArray());
                Assembly assembly = Assembly.Load(typeInfo[assemblyIndex]);
                
                if (assembly != null)
                {
                    // Create the instance of the repository type
                    IRepository repository = (IRepository)assembly.CreateInstance(typeInfo[classIndex]);
                    return repository;
                }
                else
                {
                    //TODO - need to provide this in a resource file
                    throw new NullReferenceException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.RepositoryProviderFactory.LoadingError") + repositoryType.Name +
                                                     ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.RepositoryProviderFactory.CheckConfig"));
                }
            }
            catch
            {
                //TODO - reflection - Need to log the error
                //TODO - need to provide this in a resource file
                throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.RepositoryProviderFactory.LoadingError") + repositoryType.Name +
                                    ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.RepositoryProviderFactory.CheckConfig2"));
            }
        }
    }
}