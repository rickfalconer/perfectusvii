using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.SharedLibrary.Configuration;

namespace Perfectus.Common.SharedLibrary
{
    public class RepositoryConfigurationView
    {
        private bool isActive;
        private string activeInstanceName;
        private RepositoryConfiguration repositoryConfiguration;

        /// <summary>
        ///     Accessor for the deserialized instance of the RepositoryConfiguation section of the config
        ///     file.
        /// </summary>
        public RepositoryConfiguration RepositoryConfiguration
        {
            get { return RepositoryConfiguration.Current; }
        }

        /// <summary>
        ///     Accessor for the name of the active repository.
        /// </summary>
        public string ActiveInstanceName
        {
            get { return activeInstanceName; }
        }

        /// <summary>
        ///     Accessor that indicates whether the default repository is activated.
        /// </summary>
        public bool RepositoryActivated
        {
            get { return isActive; }
        }

        /// <summary>
        ///     Constructor, which ensures the active repository instance name is set, for quick 
        ///     access for clients.
        /// </summary>
        public RepositoryConfigurationView()
        {
            // Get the repository configuration & the name of the active instance
            repositoryConfiguration = RepositoryConfiguration.Current;
            activeInstanceName = repositoryConfiguration.ActiveInstance;
            isActive = repositoryConfiguration.Active;
        }

        /// <summary>
        ///     Retrieves the active repository instance type as defined in the config file
        /// </summary>
        /// <returns></returns>
        public RepositoryType GetActiveInstanceType()
        {
            RepositoryInstance instance = GetInstance(activeInstanceName);
            return repositoryConfiguration.Types[instance.Type];
        }

        /// <summary>
        ///     Retieves the connections settings for the active repository
        /// </summary>
        /// <returns></returns>
        public ConnectionSetting GetActiveConnectionSettings()
        {
            // Get the connection settings of the active instance
            RepositoryInstance instance = GetInstance(activeInstanceName);
            return repositoryConfiguration.Settings[instance.ConnectionSetting];
        }        

        /// <summary>
        ///     Retieve a repository instance as given by it's name
        /// </summary>
        /// <param name="name">The name of the repository instance to retieve.</param>
        /// <returns>An instance of a repository instance.</returns>
        public RepositoryInstance GetInstance(string name)
        {
            return repositoryConfiguration.Instances[name];
        }
    }
}