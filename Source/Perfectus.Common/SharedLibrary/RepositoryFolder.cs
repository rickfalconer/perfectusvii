using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary
{
	/// <summary>
	/// Summary description for RepositoryFolder.
	/// </summary>
	/// 
	[Serializable]
	public class RepositoryFolder
	{	
		protected List<RepositoryItem> mRepositoryItems = new List<RepositoryItem>();
		protected List<RepositoryFolder> m_Subfolders = new List<RepositoryFolder>();
		protected RepositoryFolder m_ParentRepositoryFolder;
		protected string mFolderName;
		protected string mRelativePath;       

		public RepositoryFolder(){}

		public RepositoryFolder(string name, string relativePath)
		{
			this.Name = name;
			this.RelativePath = relativePath;
		}

		public string Name
		{
			get { return mFolderName;  }
			set { mFolderName = value; }
		}

		public string RelativePath
		{
			get { return mRelativePath;  }
			set { mRelativePath = value; }
		}

		public List<RepositoryItem> RepositoryItems
		{
			get { return mRepositoryItems;  }
			set { mRepositoryItems = value; }
		}

		/// <summary>
		/// The children RepositoryFolders this RepositoryFolder contains, read only
		/// </summary>
		[Browsable(false)]		
		public List<RepositoryFolder> Subfolders
		{
			get { return m_Subfolders; }			
		}
	
		/// <summary>
		/// The RepositoryFolders parent RepositoryFolder, read only.
		/// </summary>
		public RepositoryFolder ParentRepositoryFolder
		{
			get {return m_ParentRepositoryFolder;}
		}

		/// <summary>
		/// Returns all child RepositoryFolders AND the current RepositoryFolder into a single RepositoryFolder collection, 
		/// </summary>
		/// <returns>A list of RepositoryFolders</returns>		
		public List<RepositoryFolder> GetAllRepositoryFolders()
		{
			List<RepositoryFolder> returnCollection = new List<RepositoryFolder>();
			returnCollection.Add(this);
			RecurseSubfolders(m_Subfolders, returnCollection);
			return returnCollection;
		}

		/// <summary>
		/// Returns all child RepositoryFolders of the current RepositoryFolder into a single RepositoryFolder collection
		/// </summary>
		/// <returns>List<RepositoryFolder></returns>		
		public List<RepositoryFolder> GetSubfolders()
		{
			List<RepositoryFolder> returnCollection = new List<RepositoryFolder>();
			RecurseSubfolders(m_Subfolders, returnCollection);
			return returnCollection;
		}

		protected void MakeChildOf(RepositoryFolder argParentRepositoryFolder)
		{
			this.m_ParentRepositoryFolder = argParentRepositoryFolder;
		}

		protected bool RepositoryFolderMoveValid(RepositoryFolder targetRepositoryFolder)
		{
			foreach(RepositoryFolder RepositoryFolder in this.GetAllRepositoryFolders())
			{
				if(RepositoryFolder == targetRepositoryFolder)
				{
					return false;
				}
			}

			return true;
		}

		private void RecurseSubfolders(List<RepositoryFolder> fc, List<RepositoryFolder> returnCollection)
		{
			foreach (RepositoryFolder f in fc)
			{
				// add to our new collection before the recursive call, so the collection that is built up as any parents before children.
				returnCollection.Add(f);
				
				if (f.m_Subfolders.Count > 0)
				{
					RecurseSubfolders(f.m_Subfolders, returnCollection);
				}						
			}
		}		
	}
}