using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary.Configuration;

namespace Perfectus.Common.SharedLibrary
{
	/// <summary>
	///		Provides the functionality to serialize LibraryItem objects into a memory stream containing Xml data of Library Item object.
	/// </summary>
	public static class Serializer
	{
        // Sharepoint folder variables
        internal static string msSimpleOutcomeFolder;
        internal static string msInterviewPageFolder;
        internal static string msFunctionFolder;
        internal static string msOutcomeFolder;
        internal static string msQuestionFolder;
        internal static string msTemplateFolder;
        internal const string csSIMPLE_OUTCOME_FOLDER = "SimpleOutcomeFolder";
        internal const string csINTERVIEW_PAGE_FOLDER = "InterviewPageFolder";
        internal const string csFUNCTION_FOLDER = "FunctionFolder";
        internal const string csOUTCOME_FOLDER = "OutcomeFolder";
        internal const string csQUESTION_FOLDER = "QuestionFolder";
        internal const string csTEMPLATE_FOLDER = "TemplateFolder";
        internal const string csEXTENSION = ".xml";

		// Constants for Xml Serialization info
		private static string msDATATYPE = "DataType";
		private static string msDEPENDENCIES = "Dependencies";
		private static string msDEPENDENCY = "Dependancy";
		private static string msNAME = "Name";
		private static string msPARAMETER1 = "Parameter1";
		private static string msPARAMETER2 = "Parameter2";
		private static string msTYPE = "Type";
		private static string msUNIQUE_IDENTIFIER = "UniqueIdentifier";
		private static string msRELATIVE_PATH = "RelativePath";
		private static string msVALUE = "Value";
		private static string msVISIBLE_KEY = "Visible";
        private static string msSHOW_PAGE_IN_HISTORY = "ShowPageInHistory";
		private static string msVALUE_TYPE_KEY = "ValueType";
		private static string msYES_NO_VALUE_KEY = "YesNoValue";
		private static string msTEXT_QUESTION_VALUE_KEY = "TextQuestionValue";
		private static string msQUESTION_VALUE_KEY = "QuestionValue";
		private static string msTEXT_VALUE_KEY = "TextValue";
		private static string msQUESTION_VALUE_TYPE_KEY = "Question";
		private static string msTEXT_VALUE_TYPE_KEY = "Text";
		private static string msNONE_VALUE_TYPE_KEY = "None";
        private static string msWORDML_DEPENDENCIES_KEY = "WordMLDependencies";
		private static int miUNIQUE_IDENTIFIER = 0;
		private static int miNAME = 1;
		private static int miVALUE = 2;
		private static int miRELATIVE_PATH = 3;
		private static int miDATA_TYPE = 4;
		private static int miTYPE = 5;
		private static int miINDEX_COUNT = 6;

		public enum PropertyType
		{
			ParameterOne,
			ParameterTwo,
			OperandOne,
			OperandTwo
		}

		public static string DataTypeKey { get { return msDATATYPE; } }
		public static string DependenciesKey { get { return msDEPENDENCIES; } }
		public static string DependencyKey { get { return msDEPENDENCY; } }
		public static string NameKey { get { return msNAME; } }
		public static string Parameter1Key { get { return msPARAMETER1; } }
		public static string Parameter2Key { get { return msPARAMETER2; } }
		public static string TypeKey { get { return msTYPE; } }
		public static string UniqueIdentifierKey { get { return msUNIQUE_IDENTIFIER; } }
		public static string RelativePath { get { return msRELATIVE_PATH; } }
		public static string ValueKey { get { return msVALUE; } }
		public static string TextQuestionValueKey { get { return msTEXT_QUESTION_VALUE_KEY; } }
		public static string ValueTypeKey { get { return msVALUE_TYPE_KEY; } }
		public static string QuestionValueKey { get { return msQUESTION_VALUE_KEY; } }
		public static string TextValueKey { get { return msTEXT_VALUE_KEY; } }
		public static string QuestionValueTypeKey { get { return msQUESTION_VALUE_TYPE_KEY; } }
		public static string TextValueTypeKey { get { return msTEXT_VALUE_TYPE_KEY; } }
		public static string NoneValueTypeKey { get { return msNONE_VALUE_TYPE_KEY; } }
        public static string WordMLDependenciesKey { get { return msWORDML_DEPENDENCIES_KEY; } }
		public static int TypeIndex { get { return miTYPE; } }
		public static int UniqueIdentifierIndex { get { return miUNIQUE_IDENTIFIER; } }
		public static int RelativePathIndex { get { return miRELATIVE_PATH; } }
		public static int NameIndex { get { return miNAME; } }
		public static int ValueIndex { get { return miVALUE; } }
		public static int DataTypeIndex { get { return miDATA_TYPE; } }
		public static int IndexCount { get { return miINDEX_COUNT; } }

        /// <summary>
        ///     Serializes a RepositoryItem to a memoryStream.
        /// </summary>
        /// <param name="repositoryItem">The RepositoryItem to be serialized.</param>
        /// <returns>A MemoryStream containing the serialized RepositoryItems contents.</returns>
        public static MemoryStream ToStream(RepositoryItem repositoryItem)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Context = new StreamingContext(StreamingContextStates.Remoting);
            bformatter.Serialize(stream, repositoryItem);

            return stream;
        }

        /// <summary>
        ///     Serializes a RepositoryItem list to a memoryStream.
        /// </summary>
        /// <param name="repositoryItems">The RepositoryItem list to be serialized.</param>
        /// <returns>A MemoryStream containing the serialized RepositoryItem lists contents.</returns>
        public static MemoryStream ToStream(List<RepositoryItem> repositoryItems)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter bformatter = new BinaryFormatter();
            bformatter.Context = new StreamingContext(StreamingContextStates.Remoting);
            bformatter.Serialize(stream, repositoryItems);

            return stream;
        }

		/// <summary>
		///		Forwards the request to serialize a LibraryItem Object into a memory stream containing xml of the library item object 
		///		to the appropriate function, using the given LibraryItemType.
		/// </summary>
		/// <param name="libraryItem">A populated library item of the specific type as given by the LibraryItemType.</param>
		/// <returns>A memory stream containing the Xml of the object.</returns>
		public static MemoryStream SerializeLibraryItem(LibraryItem libraryItem)
		{
			MemoryStream stream = null;

            SetLibraryInformation();

			try
			{
				// Serialize the appropriate item type
				switch (libraryItem.LibraryType)
				{
					case  LibraryItemType.Function:
						stream = SerializeFunction(libraryItem);
						break;
					case LibraryItemType.InterviewPage:
						stream = SerializeInterviewPage(libraryItem);
						break;
					case LibraryItemType.Outcome:
						stream = SerializeOutcome(libraryItem);
						break;
					case LibraryItemType.Question:
						stream = SerializeQuestion(libraryItem);
						break;				
					case LibraryItemType.SimpleOutcome:
						stream = SerializeSimpleOutcome(libraryItem);
						break;
					case LibraryItemType.Template:
						stream = SerializeTemplate(libraryItem);
						break;				
					default:
						throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                             GetString("Perfectus.Common.SharedLibrary.Serializer.SerializationTypeInvalid"));
				}
			}
			catch(Exception ex)
			{
				bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				if (rethrow)
					throw ex;
			}

			return stream;
		}

        private static void SetLibraryInformation()
        {
            // Exit if the folder values are already set
            if (msSimpleOutcomeFolder != null && msInterviewPageFolder != null && msFunctionFolder != null &&
                msOutcomeFolder != null && msQuestionFolder != null && msTemplateFolder != null)
            {
                return;
            }

            try
            {
                RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();
                ConnectionSetting connectionSetting = repositoryConfigurationView.GetActiveConnectionSettings();

                // Populate the Sharepoint folder setings
                msSimpleOutcomeFolder = connectionSetting.ConnectionParameters[csSIMPLE_OUTCOME_FOLDER].Value;
                msInterviewPageFolder = connectionSetting.ConnectionParameters[csINTERVIEW_PAGE_FOLDER].Value;
                msFunctionFolder = connectionSetting.ConnectionParameters[csFUNCTION_FOLDER].Value;
                msOutcomeFolder = connectionSetting.ConnectionParameters[csOUTCOME_FOLDER].Value;
                msQuestionFolder = connectionSetting.ConnectionParameters[csQUESTION_FOLDER].Value;
                msTemplateFolder = connectionSetting.ConnectionParameters[csTEMPLATE_FOLDER].Value;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw ex;
            }
        }

		/// <summary>
		///		Serializes a PFunction object into a memory stream, 			
		///		which contains the PFunction object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeFunction(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(PFunction), new Type[]{typeof(AnswerProvider), 
																						  typeof(DatePFunction),
																						  typeof(ArithmeticPFunction),
																						  typeof(Question),
																						  typeof(PackageConstant)});
            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, (PFunction)item);
            
			return stream;
		}

		/// <summary>
		///		Serializes a InterviewPage object into a memory stream, 			
		///		which contains the InterviewPage object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeInterviewPage(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(InternalPage), new Type[]{typeof(InterviewPage),
																							 typeof(PageItemCollection),
																							 typeof(ActionBase),
																							 typeof(OutcomeAction), 
																							 typeof(WordTemplateDocument2)});
            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, item);

			return stream;
		}

		/// <summary>
		///		Serializes a Outcome object into a memory stream, 			
		///		which contains the Outcome object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeOutcome(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(Outcome), new Type[]{typeof(Outcome), 
																						typeof(ActionBase), 
																						typeof(OutcomeAction), 
																						typeof(WordTemplateDocument2)});
            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, item);

			return stream;
		}

		/// <summary>
		///		Serializes a Question object into a memory stream, 			
		///		which contains the Question object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeQuestion(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Question), new Type[] { typeof(TextQuestionQueryValue), 
                                                                                           typeof(ItemsItemCollection) } );
                                                                                           //typeof(data),
                                                                                           //typeof(),
        //});

            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, (Question)item);

			return stream;
		}

		/// <summary>
		///		Serializes a SimpleOutcome object into a memory stream, 			
		///		which contains the SimpleOutcome object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeSimpleOutcome(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(SimpleOutcome));
            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, item);
			return stream;
		}

		/// <summary>
		///		Serializes a TemplateDocument object into a memory stream, 			
		///		which contains the TemplateDocument object as xml.
		/// </summary>
		/// <param name="item">A populated LibraryItem object.</param>
		/// <returns>A stream that contains the objects Xml.</returns>
		private static MemoryStream SerializeTemplate(LibraryItem item)
		{
			MemoryStream stream = new MemoryStream();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(WordTemplateDocument2), new Type[]{typeof(TextQuestionQueryValue), 
																									  typeof(TemplateDocument),
                                                                                                      typeof(WordMLDependentPackageItems)});

            StreamWriter encodingWriter = new StreamWriter(stream, System.Text.Encoding.UTF8);
            xmlSerializer.Serialize(encodingWriter, item);

            stream.Position = 0; 
			return stream;
		}

        public static string GetRelativePath(RepositoryFolder folder, string name, LibraryItemType libraryItemType)
        {
            SetLibraryInformation();
            name = CleanName(name);
            return string.Format("{0}/{1}{2}", folder.RelativePath, name, csEXTENSION);
        }

        public static string GetDefaultRelativePath(string name, LibraryItemType libraryItemType)
        {
            SetLibraryInformation();
            string rootFolder = GetRootFolderByType(libraryItemType, name);
            name = CleanName(name);
            return string.Format("{0}/{1}{2}", rootFolder, name, csEXTENSION);
        }

        private static string CleanName(string name)
        {
            name = name.Replace("\"", "");
            name = name.Replace("'", "");

            name = name.Replace("/", "@");
            name = name.Replace("\\", "@");

            name = name.Replace("~", "@");
            name = name.Replace("<", "@");
            name = name.Replace(">", "@");
            name = name.Replace(":", "@");
            name = name.Replace("!", "@");

            name = HttpUtility.UrlEncode(name);
            return name;
        }

        private static string GetRootFolderByType(LibraryItemType libraryItemType, string name)
        {
            switch (libraryItemType)
            {
                case LibraryItemType.Question:
                    return msQuestionFolder;
                case LibraryItemType.Outcome:
                    return msOutcomeFolder;
                case LibraryItemType.Template:
                    return msTemplateFolder;
                case LibraryItemType.Function:
                    return msFunctionFolder;
                case LibraryItemType.SimpleOutcome:
                    return msSimpleOutcomeFolder;
                case LibraryItemType.InterviewPage:
                    return msInterviewPageFolder;
                default:
                    string message = string.Format("The default path for the item cannot be set. The item {0}, has an invalid type {1}",
                                                   name, libraryItemType.ToString());
                    throw new InvalidOperationException(message);
            }
        }

		#region IXmlSerilizable Read & Write

		public static void WriteDependentInformation(XmlWriter w, PackageItem packageItem, string elementName)
		{
			WriteDependentInformation(w, packageItem, elementName, true);
		}

		public static void WriteDependentInformation(XmlWriter w, PackageItem packageItem, string elementName, bool writeStartingElement)
		{
			// Write out the starting element
            if (writeStartingElement)
            {
                w.WriteStartElement(elementName);
            }

			// Write out the element marking a dependant object
			w.WriteStartElement(msDEPENDENCY);

            try
            {
                // e.g. <Type>Perfectus.Common.PackageObjects.Question</Type>
                switch (packageItem.GetType().ToString())
                {
                    case "Perfectus.Common.PackageObjects.Question":
                        w.WriteElementString(msTYPE, typeof(Question).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.Outcome":
                        w.WriteElementString(msTYPE, typeof(Outcome).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.SimpleOutcome":
                        w.WriteElementString(msTYPE, typeof(SimpleOutcome).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.ArithmeticPFunction":
                        w.WriteElementString(msTYPE, typeof(ArithmeticPFunction).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.DatePFunction":
                        w.WriteElementString(msTYPE, typeof(DatePFunction).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.InternalPage":
                        w.WriteElementString(msTYPE, typeof(InternalPage).ToString());
                        break;
                    case "Perfectus.Common.PackageObjects.PackageConstant":
                        w.WriteElementString(msTYPE, typeof(PackageConstant).ToString());
                        break;
                    default:
                        throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                      GetString("Perfectus.Common.SharedLibrary.Serializer.SerializationTypeInvalid"));
                }

                if (!(packageItem is PackageConstant))
                {
                    // e.g. <Name>page_name</Name>
                    w.WriteElementString(msNAME, ((LibraryItem)packageItem).Name);

                    // e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
                    if (((LibraryItem)packageItem).LibraryUniqueIdentifier != null &&
                        ((LibraryItem)packageItem).LibraryUniqueIdentifier != Guid.Empty)
                    {
                        w.WriteElementString(msUNIQUE_IDENTIFIER, ((LibraryItem)packageItem).LibraryUniqueIdentifier.ToString());
                    }
                    else
                    {
                        throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                      GetString("Perfectus.Common.SharedLibrary.Serializer.SerializationInvalidGuid"));
                    }

                    // e.g. <RelativePath>folder/subfolder/internalpage.xml</RelativePath>
                    if (((LibraryItem)packageItem).RelativePath != null && ((LibraryItem)packageItem).RelativePath != string.Empty &&
                        ((LibraryItem)packageItem).RelativePath.Length > 0)
                    {
                        w.WriteElementString(msRELATIVE_PATH, ((LibraryItem)packageItem).RelativePath);
                    }
                    else
                    {
                        // Write the default path
                        string relativePath = GetDefaultRelativePath(packageItem.Name, ((LibraryItem)packageItem).LibraryType);

                        if (relativePath != null && relativePath != string.Empty && relativePath.Length > 0)
                        {
                            w.WriteElementString(msRELATIVE_PATH, relativePath);
                        }
                        else
                        {
                            throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                          GetString("Perfectus.Common.SharedLibrary.Serializer.SerializationInvalidRelativePath"));
                        }
                    }
                }
                else
                { 
                    // Handle package constant

                    // e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
                    w.WriteElementString(msUNIQUE_IDENTIFIER, ((PackageConstant)packageItem).UniqueIdentifier.ToString());

                    if (((PackageConstant)packageItem).Value is System.DateTime)
                    {
                        // e.g. <Value>DateTime.Now</Value>
                        w.WriteElementString(msVALUE, ((DateTime)((PackageConstant)packageItem).Value).ToLongDateString().ToString());
                    }
                    else
                    {
                        if (((PackageConstant)packageItem).Value != null)
                        {
                            // e.g. <Value>value</Value>
                            w.WriteElementString(msVALUE, ((PackageConstant)packageItem).Value.ToString());					
                        }
                        else
                        {
                            // e.g. <Value></Value>
                            w.WriteElementString(msVALUE, "");																
                        }
                    }

                    // e.g. <DataType>Text</DataType>
                    w.WriteElementString(msDATATYPE, ((PackageConstant)packageItem).DataType.ToString());					
                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2))
                {
                    throw ex;
                }
            }

			// Write out the ending elements
			w.WriteEndElement();

            if (writeStartingElement)
            {
                w.WriteEndElement();
            }
		}

		public static void WriteVisibleSection(XmlWriter w, YesNoQueryValue yesNoQueryValue)
		{
			WriteYesNoQuerySection(w, yesNoQueryValue, msVISIBLE_KEY);
		}

        public static void WriteShowPageInHistorySection(XmlWriter w, YesNoQueryValue yesNoQueryValue)
        {
            WriteYesNoQuerySection(w, yesNoQueryValue, msSHOW_PAGE_IN_HISTORY);
        }
		
		public static void WriteYesNoQuerySection(XmlWriter w, YesNoQueryValue yesNoQueryValue, string key)
		{

			w.WriteStartElement(key);

            if (yesNoQueryValue != null)
            {
                if (yesNoQueryValue.QueryValue == null)
                {
                    w.WriteStartElement(msVALUE_TYPE_KEY);
                    w.WriteString(yesNoQueryValue.ValueType.ToString());
                    w.WriteEndElement();
                    w.WriteStartElement(msYES_NO_VALUE_KEY);
                    w.WriteString(yesNoQueryValue.YesNoValue.ToString());
                    w.WriteEndElement();
                }
                else 
                {
                    yesNoQueryValue.QueryValue.WriteXml(w);
                }
            }

			w.WriteEndElement();
		}

		public static string[] ReadLibraryItem(XmlReader r)
		{
			string[] values = new string[miINDEX_COUNT];

			r.ReadStartElement(msNAME);					// Name
			values[miNAME] = r.ReadString();
			r.ReadEndElement();
			r.ReadStartElement(msUNIQUE_IDENTIFIER);	// Unique Identifier
			values[miUNIQUE_IDENTIFIER] = r.ReadString();
			r.ReadEndElement();
			r.ReadStartElement(msRELATIVE_PATH);	    // Relative Path
			values[miRELATIVE_PATH] = r.ReadString();
			r.ReadEndElement();
			
			return values;
		}

		public static string[] ReadPackageConstant(XmlReader r)
		{
			string[] values = new string[miINDEX_COUNT];

			r.ReadStartElement(msUNIQUE_IDENTIFIER);	// Unique Identifier
			values[miUNIQUE_IDENTIFIER] = r.ReadString();
			r.ReadEndElement();
            if (!r.IsEmptyElement)
            {
                r.ReadStartElement(msVALUE);				// Value
                values[miVALUE] = r.ReadString();
                r.ReadEndElement();
            }
            else
            {
                r.Read();
                values[miVALUE] = "";
            }
			r.ReadStartElement(msDATATYPE);				// DataType
			values[miDATA_TYPE] = r.ReadString();
			r.ReadEndElement();

			return values;
		}

		public static YesNoQueryValue ReadVisibleSection(XmlReader r)
		{
			return ReadYesNoQuerySection(r, msVISIBLE_KEY);
		}

        public static YesNoQueryValue ReadShowPageInHistorySection(XmlReader r)
        {
            return ReadYesNoQuerySection(r, msSHOW_PAGE_IN_HISTORY);
        }

		public static YesNoQueryValue ReadYesNoQuerySection(XmlReader r, string key)
		{
			YesNoQueryValue yesNoQueryValue = new YesNoQueryValue();

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(key);

                if (r.Name == Serializer.msDEPENDENCIES)
                {
                    yesNoQueryValue.QueryValue = new Query();
                    yesNoQueryValue.QueryValue.ReadXml(r);
                }
                else
                {

                    r.ReadStartElement(msVALUE_TYPE_KEY);
                    yesNoQueryValue.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
                    r.Read();
                    r.ReadEndElement();
                    r.ReadStartElement(msYES_NO_VALUE_KEY);
                    yesNoQueryValue.YesNoValue = Convert.ToBoolean(r.ReadString());
                    r.ReadEndElement();
                }

				r.ReadEndElement();
			}

			return yesNoQueryValue;
		}

		#endregion
	}
}
