using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.SharedLibrary
{
    public class SharedLibraryException : ApplicationException
    {
        RepositoryItem repositoryItem;
        LibraryItem libraryItem;
        Exception innerException;
        string message;

        /// <summary>
        ///     The RepositoryItem which was being processed when the error occurred
        /// </summary>
        public RepositoryItem RepositoryItem
        {
            get { return repositoryItem; }
            set { repositoryItem = value; }
        }

        /// <summary>
        ///     The libraryItem that was being processed when the error occurred
        /// </summary>
        public LibraryItem LibraryItem
        {
            get { return libraryItem; }
            set { libraryItem = value; }
        }

        /// <summary>
        ///     Hold a reference to the original exception that was thrown in the shared library
        /// </summary>
        public new Exception InnerException
        {
            get { return innerException; }
        }

        /// <summary>
        ///     Hold the error message of the inner exception
        /// </summary>
        public new string Message
        {
            get { return message; }
        }

        /// <summary>
        ///     Contructor
        /// </summary>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        public SharedLibraryException(string message, Exception ex)
        {
            this.message = message;
            innerException = ex;
        }
    }
}