﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GemBox.Document;
using GemBox.Document.Tables;
using System.IO;

namespace Perfectus.Common.SharedLibrary
{
    public class GemBoxAPI
    {
        static GemBoxAPI()
        {
            //VERSION 2.7 D3JP-LSI7-8888-W0XO
            //Version 2.5 as used in v6.0.4.931
            ComponentInfo.SetLicense("DUZ7-YWKX-BD2Z-MNI2");
            //Trial
            //ComponentInfo.SetLicense("FREE-LIMITED-KEY");
            //ComponentInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = FreeLimitReachedAction.ContinueAsTrial;
        }

        [STAThread]
        public static void ConvertWordToHtml(MemoryStream msDocx, ref MemoryStream msHtml)
        {
            DocumentModel doc = DocumentModel.Load(msDocx, new DocxLoadOptions() { });
            //doc.Save("c:\testing\testdoc.docx");
            //doc.Save(msHtml, new HtmlSaveOptions() { EmbedImages = true, Encoding = UTF8Encoding.UTF8, HtmlType = HtmlType.HtmlInline });
            HtmlSaveOptions hh = new HtmlSaveOptions();
            hh.HtmlType = HtmlType.HtmlInline;
            hh.Encoding = UTF8Encoding.UTF8;
            hh.EmbedImages = true;
            doc.Save(msHtml, hh);
        }

        [STAThread]
        public static void PrintDocument(string fileName)
        {
            DocumentModel doc = DocumentModel.Load(fileName);

            var toc = (TableOfEntries)doc.GetChildElements(true, ElementType.TableOfEntries).First();
            toc.Update();

            // Update TOC's page numbers.
            // NOTE: This is not necessary when printing and saving to PDF, XPS or an image format.
            // Page numbers are automatically updated in that case.
            doc.GetPaginator(new PaginatorOptions() { UpdateFields = true });

            doc.Print();
        }

        [STAThread]
        public static void ConvertWord(Stream msDocx, string convertToFileName)
        {
            DocumentModel doc = DocumentModel.Load(msDocx, new DocxLoadOptions() { });
            doc.Save(convertToFileName);
        }

    }
}
