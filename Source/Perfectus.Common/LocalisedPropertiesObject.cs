using System;
using System.ComponentModel;
using System.Collections.Generic;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common
{
    /// <summary>
    /// Custom property attribute to specify the display ranking (order). Optional. Classes that derive from the 
    /// class 'LocalisedPropertiesObject' will have their properties sorted alphabetical on the property grid. This
    /// attribute overides that, low numbers are placed on the top and higher numbers lower down. All properties have
    /// a default ranking of 100, hence one should specify a number before 100 if you wish the property to be placed 
    /// before properties that have no ranking attribute assigned.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayRankingAttribute : System.Attribute
    {
        private int _rank;
	    public int Ranking
	    {
		    get { return _rank;}
	    }
	
        public DisplayRankingAttribute(int ranking)
        {
            this._rank = ranking;
        }
    }

    /// <summary>
    /// Custom property attribute to specify the localised display name. Optional. Classes that derive from the 
    /// class 'LocalisedPropertiesObject' will have a collection of 'LocalisedPropertyDescriptor' objects 
    /// which will in turn detect this attribute and use it to extract the laclised string value.
    /// </summary>
    [AttributeUsage( AttributeTargets.Property )]
    public class DisplayNameAttribute : System.Attribute
    {
        private string _namekey;
        public string NameKey
        {
            get { return _namekey; }
        }

        public DisplayNameAttribute( string key )
        {
            this._namekey = key;
        }
    }

    /// <summary>
    /// Custom property attribute to specify the localised display description. Optional. Classes that derive from the 
    /// class 'LocalisedPropertiesObject' will have a collection of 'LocalisedPropertyDescriptor' objects 
    /// which will in turn detect this attribute and use it to extract the laclised string value.
    /// </summary>
    [AttributeUsage( AttributeTargets.Property )]
    public class DisplayDescriptionAttribute : System.Attribute
    {
        private string _desckey;
        public string DescKey
        {
            get { return _desckey; }
        }

        public DisplayDescriptionAttribute( string key )
        {
            this._desckey = key;
        }
    }

    
    /// <summary>
    /// This class allows you to select and sort the properties displayed on the property grid. The System.Component.CustomTypeDescriptor class 
    /// allows one to control how the properties of a type are displayed. It's main task is to build a collection of 'LocalisedPropertyDescriptor's
	/// </summary>
	public class LocalisedPropertiesObject : ICustomTypeDescriptor
	{
		protected PropertyDescriptorCollection localisedProperties;

		public LocalisedPropertiesObject(){}

		#region ICustomTypeDescriptor Members

		public TypeConverter GetConverter()
		{
			return (TypeDescriptor.GetConverter(this, true));
		}

		public EventDescriptorCollection GetEvents(Attribute[] attributes)
		{
			return (TypeDescriptor.GetEvents(this, attributes, true));
		}

		EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
		{
			return (TypeDescriptor.GetEvents(this, true));
		}

		public string GetComponentName()
		{
			return (TypeDescriptor.GetComponentName(this, true));
		}

		public object GetPropertyOwner(PropertyDescriptor pd)
		{
			return this;
		}

		public AttributeCollection GetAttributes()
		{
			return (TypeDescriptor.GetAttributes(this, true));
		}

		public virtual PropertyDescriptorCollection GetProperties(Attribute[] attributes)
		{
			if ( localisedProperties == null) 
			{
				// Get the collection of properties
				PropertyDescriptorCollection baseProps = TypeDescriptor.GetProperties(this, attributes, true);

                // Finally copy the sorted properties into our local properties collection member to return.
                localisedProperties = this.GetFilteredAndSortedProperties( baseProps );
			}
			return localisedProperties;
		}

		public virtual PropertyDescriptorCollection GetProperties()
		{
			// Only do once
			if ( localisedProperties == null) 
			{
				// Get the collection of properties
				PropertyDescriptorCollection baseProps = TypeDescriptor.GetProperties(this, true);

                // Finally copy the sorted properties into our local properties collection member to return.
                localisedProperties = this.GetFilteredAndSortedProperties( baseProps );
			}
			return localisedProperties;
		}

        /// <summary>
        /// This routine allows the inherited class override the final structure of the properties for this object.
        /// This allows one to change the properties this class provides only during runtime.
        /// </summary>
        /// <param name="properties">Alter this list of 'LocalisedPropertyDescriptor' objects</param>
        public virtual void AlterProperties( PropertyDescriptorCollection properties )
        {
            return;
        }

        /// <summary>
        /// Called by the GetProperties() overloaded functions. Performs common funtionality to filter and sort the properties.
        /// </summary>
        /// <param name="baseProps"></param>
        /// <returns></returns>
        private PropertyDescriptorCollection GetFilteredAndSortedProperties( PropertyDescriptorCollection baseProps )
        {
            // Create a generic List<> of the properties for later sorting. (Sorting the 'PropertyDescriptorCollection' 
            // by passing a custom IComparer somehow doesn't work)
            List<LocalisedPropertyDescriptor> propertyList = new List<LocalisedPropertyDescriptor>( );

            // For each property use a property descriptor of our own that is able to be globalized
            foreach( PropertyDescriptor oProp in baseProps )
            {
                // Only include Browserable properties.
                if( !oProp.IsBrowsable )
                    continue;

                // Get instance of the ranking attribute for display sort order.    
                Attribute rankattribute = oProp.Attributes[ typeof( DisplayRankingAttribute ) ];
                if( rankattribute != null )
                    propertyList.Add( new LocalisedPropertyDescriptor( oProp, ( (DisplayRankingAttribute)rankattribute ).Ranking ) );
                else
                    propertyList.Add( new LocalisedPropertyDescriptor( oProp ) );
            }

            // Sort the property list.
            propertyList.Sort( );

            // Finally return the sorted properties in the PropertyDescriptorCollection collection class.
            PropertyDescriptorCollection returncollection = new PropertyDescriptorCollection( null );
            foreach( LocalisedPropertyDescriptor localProp in propertyList )
                returncollection.Add( localProp );

            // Maybe change the properties.
            AlterProperties( returncollection );

            return returncollection;
        }

        public object GetEditor( Type editorBaseType )
		{
			return (TypeDescriptor.GetEditor(this, editorBaseType, true));
		}

		public PropertyDescriptor GetDefaultProperty()
		{
			return (TypeDescriptor.GetDefaultProperty(this, true));
		}

		public EventDescriptor GetDefaultEvent()
		{
			return (TypeDescriptor.GetDefaultEvent(this, true));
		}

		public string GetClassName()
		{
			return (TypeDescriptor.GetClassName(this, true));
		}

		#endregion

	}
}
