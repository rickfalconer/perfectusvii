using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web.Services.Description;
using System.Web.Services.Discovery;
using Microsoft.CSharp;

namespace Perfectus.Common.WebServiceSystem
{
	/// <summary>
	/// Summary description for DynamicProxy.
	/// </summary>
	public sealed class DynamicProxy
	{
		private Uri wsdlLocation;
		private Assembly assembly;
		private string wsdlHash;
		private string urlHash;
        
        public DynamicProxy(Uri wsdlLocation)
		{
			this.wsdlLocation = wsdlLocation;
			WebClient wc = new WebClient();
			wc.Credentials = CredentialCache.DefaultCredentials;

			byte[] wsdl = wc.DownloadData(wsdlLocation.ToString());
			wsdlHash = GetMD5Sum(wsdl);
			urlHash = GetMD5Sum(Encoding.UTF8.GetBytes(wsdlLocation.ToString()));

			BuildProxy(true);
		}
        
        public DynamicProxy(Uri wsdlLocation, bool checkCachedProxy)
        {
            this.wsdlLocation = wsdlLocation;
            WebClient wc = new WebClient();
            wc.Credentials = CredentialCache.DefaultCredentials;

            byte[] wsdl = wc.DownloadData(wsdlLocation.ToString());
            wsdlHash = GetMD5Sum(wsdl);
            urlHash = GetMD5Sum(Encoding.UTF8.GetBytes(wsdlLocation.ToString()));
        
            BuildProxy(checkCachedProxy);
        }

        private void BuildProxy(Boolean checkCachedProxy)
		{
            if (checkCachedProxy && urlHash != null && wsdlHash != null && CachedAssemblyExists())
			{
				ProxyFromCache();
			}
			else
			{
				CreateProxyAssembly();
			}
		}


		private string GenerateCacheFilename()
		{
			return string.Format("{0}{1}_{2}_service_proxy.ipsvr.dll", Path.GetTempPath(), urlHash, wsdlHash);
		}

		private bool CachedAssemblyExists()
		{
            return (File.Exists(GenerateCacheFilename()));
		}


		private string GetMD5Sum(byte[] wsdl)
		{
			// First we need to convert the string into bytes, which
			// Now that we have a byte array we can ask the CSP to hash it
			MD5 md5 = new MD5CryptoServiceProvider();

			byte[] result = md5.ComputeHash(wsdl);

			// Build the final string by converting each byte
			// into hex and appending it to a StringBuilder
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < result.Length; i++)
			{
				sb.Append(result[i].ToString("X2"));
			}

			return sb.ToString();
		}


		private void ProxyFromCache()
		{
			this.assembly = Assembly.LoadFrom(GenerateCacheFilename());
		}

		public Assembly Assembly
		{
			get { return assembly; }
		}

		private void CreateProxyAssembly()
		{
			string sNamespace = "Perfectus.Common.WebServiceSystem.DynamicProxy";


			ServiceDescription description = null;
			// Get the wsdl file
			// Read it into a service description and create a compile unit with ServiceImporter
			DiscoveryClientProtocol client = new DiscoveryClientProtocol();

			// Get the default web proxy from the system
			client.Proxy = WebProxy.GetDefaultProxy();

			//TODO: Should there be any other credentials here?  If so, where to store them?
			client.Credentials = CredentialCache.DefaultCredentials;

			IList referenceList = null;
			referenceList = client.DiscoverAny(wsdlLocation.AbsoluteUri).References;

			if (referenceList != null)
				foreach (object o in referenceList)
				{
					if (o is ContractReference)
					{
						description = ((ContractReference) o).Contract;
						break;
					}
				}

			if (description != null)
			{
				// Create an importer
				ServiceDescriptionImporter importer = new ServiceDescriptionImporter();
				importer.AddServiceDescription(description, null, null);

				// Do the import
				CodeNamespace codeNameSpace = new CodeNamespace(sNamespace);
				CodeCompileUnit codeCompileUnit = new CodeCompileUnit();

				codeCompileUnit.Namespaces.Add(codeNameSpace);

				ServiceDescriptionImportWarnings warnings =
					importer.Import(codeNameSpace, codeCompileUnit);

				// Compile the compile unit and create an assembly
				CSharpCodeProvider csprov = new CSharpCodeProvider();
				ICodeCompiler compiler = csprov.CreateCompiler();
				CompilerParameters options = new CompilerParameters();
				options.ReferencedAssemblies.Add("System.dll");
				options.ReferencedAssemblies.Add("System.XML.dll");
				options.ReferencedAssemblies.Add("System.Web.Services.dll");

#if  DEBUG
				options.IncludeDebugInformation = true;
				options.TreatWarningsAsErrors = true;
#endif	

				CompilerResults results = compiler.CompileAssemblyFromDom(options,
				                                                          codeCompileUnit);

				this.assembly = results.CompiledAssembly;

				// Cache this assembly
				File.Copy(results.PathToAssembly, GenerateCacheFilename(), true);
			}
		}
	}
}
