﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace Perfectus.Common.Helper
{
    public static class CompressionHelper
    {
        #region compression


        public static byte[] CompressIfSmaller(byte[] byteArray)
        {
            // return the smaller byte array
            byte[] compresssedArray = Compress(byteArray);
            if (compresssedArray.Length < byteArray.Length)
                return compresssedArray;
            return byteArray;
        }
        public static byte[] CompressIfSmaller(MemoryStream streamedPackage)
        {
            return CompressIfSmaller ( streamedPackage.ToArray());
        }

        private static byte[] Compress(byte[] input)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (GZipStream zip = new GZipStream(output, CompressionMode.Compress))
                    zip.Write(input, 0, input.Length);
                return output.ToArray();
            }
        }

        public static byte[] DecompressIfNecessary(byte[] input)
        {
            if (input[0] == 0x1F &&
                input[1] == 0x8b &&
                input[2] <= 0x08)
                return Decompress(input);
            return input;
        }


        private static byte[] Decompress(byte[] input)
        {
            using (MemoryStream output = new MemoryStream(input))
            using (GZipStream zip = new GZipStream(output, CompressionMode.Decompress))
            {
                List<byte> bytes = new List<byte>();
                int b = zip.ReadByte();
                while (b != -1)
                {
                    bytes.Add((byte)b);
                    b = zip.ReadByte();
                }
                return bytes.ToArray();
            }
        }

        public static bool IsCompressed(Stream s)
        {
            s.Seek(0, SeekOrigin.Begin);
            if (s.Length < 3)
            {
                return false;
            }

            byte[] sig = new byte[3];
            s.Read(sig, 0, 3);

            s.Seek(0, SeekOrigin.Begin);
            return sig[0] == 0x1F && sig[1] == 0x8b && sig[2] <= 0x08;
        }
        /*
        internal static bool IsCompressed(byte[] data)
        {
            if (data.Length < 3)
                return false;
            return data[0] == 0x1F && data[1] == 0x8b && data[2] <= 0x08; ;
        }
         */ 
        #endregion

    }
}