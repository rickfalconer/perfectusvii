using System;

namespace Perfectus.Common
{
	/// <summary>
	/// Summary description for PackageItemDeletedEventArgs.
	/// </summary>
	public class PackageItemDeletedEventArgs : EventArgs
	{
		private Guid id;

		public Guid Id
		{
			get { return id; }
		}

		public PackageItemDeletedEventArgs(Guid id)
		{
			this.id = id;
		}
	}
}