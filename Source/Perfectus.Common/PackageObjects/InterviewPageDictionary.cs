using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type InterviewPage
	/// </summary>
	[Serializable]
	public class InterviewPageDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the InterviewPageDictionary class
		/// </summary>
		public InterviewPageDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the InterviewPage associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual InterviewPage this[Guid key]
		{
			get { return (InterviewPage) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this InterviewPageDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The InterviewPage value of the element to add.
		/// </param>
		public virtual void Add(Guid key, InterviewPage value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this InterviewPageDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this InterviewPageDictionary.
		/// </param>
		/// <returns>
		/// true if this InterviewPageDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this InterviewPageDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this InterviewPageDictionary.
		/// </param>
		/// <returns>
		/// true if this InterviewPageDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this InterviewPageDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The InterviewPage value to locate in this InterviewPageDictionary.
		/// </param>
		/// <returns>
		/// true if this InterviewPageDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(InterviewPage value)
		{
			foreach (InterviewPage item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this InterviewPageDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this InterviewPageDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this InterviewPageDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}