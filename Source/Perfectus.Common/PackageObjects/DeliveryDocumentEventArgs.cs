using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for RuleTemplateEventArgs.
	/// </summary>
	public sealed class DeliveryDistributorEventArgs : EventArgs
	{
		private Delivery delivery;
        private Distributor distributor;

        public Distributor Distributor
		{
            get { return distributor; }
		}

        public Delivery Delivery
		{
			get { return delivery; }
		}

        public DeliveryDistributorEventArgs(Delivery delivery, Distributor distributor)
		{
            this.distributor = distributor;
			this.delivery = delivery ;
		}
	}
}

