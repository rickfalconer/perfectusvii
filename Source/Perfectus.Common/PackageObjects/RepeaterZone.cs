using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for HorizontalLayoutZone.
	/// </summary>
	[Serializable]
	public sealed class RepeaterZone : PackageItem, IPageItem, ISerializable
	{
		private PageItemCollection items = new PageItemCollection();
		private YesNoQueryValue visible;
		private IntQuestionValue maxRows = new IntQuestionValue();


		public YesNoQueryValue Visible
		{
			get { return visible; }
		}
				
		public IntQuestionValue MaxRows
		{
			get { return maxRows; }
			set { maxRows = value; }
		}

		public PageItemCollection Items
		{
			get { return items;  }
			set { items = value; }
		}

		public RepeaterZone(Guid uniqueIdentifier, YesNoQueryValue visible, IntQuestionValue maxRows)
		{
			this.UniqueIdentifier = uniqueIdentifier;
			this.visible = visible;
			this.maxRows = maxRows;
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		public RepeaterZone(){}//SL

		#region ISerializable Members

		public RepeaterZone(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			items = (PageItemCollection) info.GetValue("items", typeof (PageItemCollection));
			try
			{
				visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}
			try
			{
				maxRows = (IntQuestionValue) info.GetValue("maxRows", typeof (IntQuestionValue));
				if(maxRows == null)
				{
					maxRows = new IntQuestionValue();
				}
			}
			catch
			{
				maxRows = new IntQuestionValue();
			}

			object o = null;

			try
			{
				o = info.GetValue("uniqueIdentifier", typeof (Guid));
			}
			catch
			{
			}
			if (o == null)
			{
				UniqueIdentifier = Guid.NewGuid();
			}
			else
			{
				UniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			}

		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("items", items);
			info.AddValue("visible", visible);
			info.AddValue("maxRows", maxRows);
			//info.AddValue("uniqueIdentifier", uniqueIdentifier);
		}

		#endregion
	}
}