using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
    public enum LibraryItemType
    {
        None,
        Function,
        InterviewPage,
        Outcome,
        Question,
        SimpleOutcome,
        Template
    }

	/// <summary>
	/// Summary description for PackageItem.
	/// </summary>
	[Serializable]
	public abstract class LibraryItem : PackageItem, ISerializable, IDesignable
	{
		#region Private Member Variables

		// Private member properties
		private Guid _LibraryUniqueIdentifier;
		private int _Version;
		private LibraryItemType _LibraryItemType;
		private bool _Linked;
		private string _CreatedBy = null;
		private DateTime _CreatedDateTime = DateTime.MinValue;
		private string _CheckedOutBy = null;
		private string _CheckedOutByLogInName = null;
		private DateTime _CheckedOutDateTime = DateTime.MinValue;
		private CheckOutStatus _CheckOutStatus = CheckOutStatus.CheckedIn;
		private string _ModifiedBy = null;
		private DateTime _ModifiedDateTime = DateTime.MinValue;
		private string _CheckInComment;
		private string _Path;
		private string _RelativePath;
		private string _Filename;
		private RepositoryFolder _RepositoryFolder;
		private bool _DependentObject = false;
		private List<LibraryItem> _DependentLibraryItems;
		private string _StringValue;
		private string _DataType;
        private bool _IsNewLibraryItem = true;

        [OptionalField(VersionAdded = 5)]
        private Guid _OriginalUniqueIdentifier = Guid.Empty;

		#endregion
		#region Properties & Constructor 

		[Browsable(false)]
		public Guid LibraryUniqueIdentifier
		{
			get { return _LibraryUniqueIdentifier;  }
			set { _LibraryUniqueIdentifier = value; }
		}

        [Browsable(false)]
        public Guid OriginalUniqueIdentifier
        {
            get { return _OriginalUniqueIdentifier; }
            set { _OriginalUniqueIdentifier = value; }
        }

		[Browsable(false)]
		public int Version
		{
			get { return _Version;  }
			set { _Version = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public virtual LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public bool Linked
		{
			get { return _Linked;  }
			set { _Linked = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string CreatedBy
		{
			get { return _CreatedBy;  }
			set { _CreatedBy = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public DateTime CreatedDateTime
		{
			get { return _CreatedDateTime;  }
			set { _CreatedDateTime = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string CheckedOutBy
		{
			get { return _CheckedOutBy;  }
			set { _CheckedOutBy = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string CheckedOutByLogInName
		{
			get { return _CheckedOutByLogInName;  }
			set { _CheckedOutByLogInName = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public DateTime CheckedOutDateTime
		{
			get { return _CheckedOutDateTime;  }
			set { _CheckedOutDateTime = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public CheckOutStatus CheckedOutStatus
		{
			get { return _CheckOutStatus;  }
			set { _CheckOutStatus = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string ModifiedBy
		{
			get { return _ModifiedBy;  }
			set { _ModifiedBy = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public DateTime ModifiedDateTime
		{
			get { return _ModifiedDateTime;  }
			set { _ModifiedDateTime = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string CheckInComment
		{
			get { return _CheckInComment;  }
			set { _CheckInComment = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string RelativePath
		{
			get { return _RelativePath;  }
			set { _RelativePath = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string LocationPath
		{
			get { return _Path;  }
			set { _Path = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public string Filename
		{
			get { return _Filename;  }
			set { _Filename = value; }
		}

		[Category("SharedLibrary")]
		[Browsable(false)]
		public virtual string PerfectusType
		{
			get { return this.GetType().ToString(); }
			set {}
		}

		[Category("Shared Library")]
		[Browsable(false)]
		public byte[] SerializedContents
		{
			get { return GetSerializedContents(); }
		}

		[Category("Shared Library")]
		[Browsable(false)]
        [XmlIgnore]
		public RepositoryFolder RepositoryFolder
		{
			get { return _RepositoryFolder;  }
			set { _RepositoryFolder = value; }
		}		

		/// <summary>
		///		Accessor for the value of the library item. Used for Xml Deserialization of Package Constants and other such items.
		/// </summary>
		[Category("SharedLibrary")]
		[Browsable(false)]
		public string StringValue
		{
			get { return _StringValue;  }
			set { _StringValue = value; }
		}

		/// <summary>
		///		Accessor for the type of data being held in the library item. Used for Xml Deserialization of Package Constants and other such items.
		/// </summary>
		[Category("SharedLibrary")]
		[Browsable(false)]
		public virtual string DataTypeString
		{
			get { return _DataType;  }
			set { _DataType = value; }
		}

		/// <summary>
		///		Accessor that indicates whether the library item is a dependent on another library item.
		///		Used for handling dependent library items.
		/// </summary>
		[Category("SharedLibrary")]
		[Browsable(false)]
		public bool DependentObject
		{
			get { return _DependentObject;  }
			set { _DependentObject = value; }
		}

		/// <summary>
		///		Accessor that gets and sets the library items dependent items collection.
		/// </summary>
		[Category("SharedLibrary")]
		[Browsable(false)]
		[XmlIgnore]
        public List<LibraryItem> DependentLibraryItems
		{
			get { return _DependentLibraryItems;  }
			set { _DependentLibraryItems = value; }
		}

        /// <summary>
        ///		Accessor that indicates whether the library item is new to the library or not.
        ///		Used for handling errors when adding / checking in library items.
        /// </summary>
        [Category("SharedLibrary")]
        [Browsable(false)]
        public bool IsNewLibraryItem
        {
            get { return _IsNewLibraryItem; }
            set { _IsNewLibraryItem = value; }
        }

		public LibraryItem() : base(){}

		#endregion
		#region Methods

        public virtual Boolean IsValid(ref String message)
        {
            return true;
        }

		public PerfectusDataType GetPerfectusDataTypeFromStringProperty()
		{
			if (_DataType == PerfectusDataType.AnswerSet.ToString())
				return PerfectusDataType.AnswerSet;
			if (_DataType == PerfectusDataType.Date.ToString())
				return PerfectusDataType.Date;
			if (_DataType == PerfectusDataType.File.ToString() ||
               _DataType == PerfectusDataType.Attachment.ToString())
				return PerfectusDataType.File;
			if (_DataType == PerfectusDataType.HTML.ToString())
				return PerfectusDataType.HTML;
			if (_DataType == PerfectusDataType.Number.ToString())
				return PerfectusDataType.Number;
			if (_DataType == PerfectusDataType.Text.ToString())
				return PerfectusDataType.Text;
			if (_DataType == PerfectusDataType.YesNo.ToString())
				return PerfectusDataType.YesNo;
			else
				return PerfectusDataType.Text; // Default
		}

		/// <summary>
		///		Called by the definition editors of queries to ensure a precheck out property changed event is fired
		/// </summary>
		/// <param name="propertyName">The name of the property being edited. Can also include the global markers for unlink and checkout.</param>
		public void DefinitionBeingEdited(string propertyName)
		{
			OnPropertyChanged(propertyName);
		}

        public List<LibraryItem> GetUnlinkedDependencies()
        {
            List<LibraryItem> unlinkedLibraryItems = new List<LibraryItem>();

            if (this.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in this.DependentLibraryItems)
                {
                    if (this.ParentPackage != null)
                    {
                        dependentLibraryItem.ParentPackage = this.ParentPackage;
                    }
                    
                    List<LibraryItem> dependentUnlinkedLibraryItems = null;

                    if (dependentLibraryItem.DependentLibraryItems != null)
                    {
                        dependentUnlinkedLibraryItems = dependentLibraryItem.GetUnlinkedDependencies();
                    }

                    if (this.ParentPackage != null)
                    {
                    LibraryItem packageitem = this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(dependentLibraryItem);

                    if (packageitem != null && !packageitem.Linked && packageitem.LibraryUniqueIdentifier != Guid.Empty)
                    {
                        unlinkedLibraryItems.Add(packageitem);

                        if (dependentUnlinkedLibraryItems != null)
                        {
                            unlinkedLibraryItems.AddRange(dependentUnlinkedLibraryItems);
                        }
                    }
                }                
            }
            }

            return unlinkedLibraryItems;
        }

		// Abstract methods defined by the individual library item classes of what objects are dependent
		public abstract List<LibraryItem> GetDependentLibraryItems();
		public abstract List<LibraryItem> SetDependentLibraryItems();

		/// <summary>
		///		Serializes a LibraryItem into xml and returns it as a byte array
		/// </summary>
		/// <param name="stream">A memory stream that contains the serialized library item.</param>
		/// <returns>The Library Item as an Xml byte array.</returns>
		private byte[] GetSerializedContents()
		{
			MemoryStream stream;
			byte[] contents = null;
			
			try
			{
				// Serialize the item
				stream = Serializer.SerializeLibraryItem(this);

				// Read the stream into a byte array
				contents = stream.ToArray();
			}
			catch(Exception ex)
			{
				throw ex; 
			}

			return contents;
		}

		/// <summary>
		///		Drills down a library items dependency hierarchy adding the dependent items to the library item collection.
		/// </summary>
        /// <param name="handledLibraryItems">A collection of library items which have already been handled.</param>
        public void HandleDependencies(List<LibraryItem> handledLibraryItems) {
            
            // Ensure the collection is null to start
            this.DependentLibraryItems = null;

            HandleRecursiveDependencies(handledLibraryItems);
        }

        /// <summary>
		///		Drills down a library items dependency hierarchy adding the dependent items to the library item collection.
		/// </summary>
        /// <param name="handledLibraryItems">A collection of library items which have already been handled.</param>
        public void HandleRecursiveDependencies(List<LibraryItem> handledLibraryItems) {

            List<LibraryItem> dependentLibraryItems = this.GetDependentLibraryItems();

            // Keep a reference to this item to stop dependent cyclic references
            handledLibraryItems.Add(this);

            if (dependentLibraryItems != null) {

                foreach (LibraryItem dependency in dependentLibraryItems) {

                    dependency.DependentObject = true;

                    if (!handledLibraryItems.Contains(dependency)) {

                        if (this.DependentLibraryItems == null) {
                            this.DependentLibraryItems = new List<LibraryItem>();
                        }

                        this.DependentLibraryItems.Add(dependency);
                        dependency.HandleDependencies(handledLibraryItems);
                    }
                }
            }

            // Ensure the item has a library UID
            if (this.LibraryUniqueIdentifier == Guid.Empty) {
                this.LibraryUniqueIdentifier = Guid.NewGuid();
                this.IsNewLibraryItem = true;
                this.Linked = true; // (default)
            }
            else
                this.IsNewLibraryItem = false;
        }

		protected virtual PackageItem GetDependentPackageItem(LibraryItem libraryItem)
		{
			object itemValue;
			PerfectusDataType itemDataType;
			PackageItem packageItem = null;

			// Attempt to get the item from the package
			packageItem = this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);

			if (libraryItem.LibraryType == LibraryItemType.None)
			{
				if (((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty() == PerfectusDataType.Date)
				{
					itemDataType = PerfectusDataType.Date;
					itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value.ToString();

                    if (Globals.IsDate(itemValue.ToString()))
                    {
                        itemValue = DateTime.Parse(itemValue.ToString()).ToLocalTime().ToShortDateString();
                    }
                    else
                    {
                        itemValue = itemValue.ToString();
                    }
				}
				else
				{
					itemDataType = ((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty();
					itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value;
				}

				// Set the packge constant
				packageItem = new PackageConstant(itemDataType, itemValue);
			}

			if (packageItem == null)
			{
                Exception ex = new Exception(libraryItem.Name + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage"));
				throw new NullReferenceException(libraryItem.Name); 
			}
		
			return packageItem;
		}

        public void DisassociateRootAndDependents(bool onlyNewItems)
        {
            if (this.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in this.DependentLibraryItems)
                {
                    dependentLibraryItem.DisassociateRootAndDependents(onlyNewItems);
                }
            }

            DisassociateFromSharedLibrary(onlyNewItems);
        }

        public void DisassociateFromSharedLibrary()
        {
            DisassociateFromSharedLibrary(false);
        }

        public void DisassociateFromSharedLibrary(bool onlyNewItems)
        {
            if (onlyNewItems && !this.IsNewLibraryItem)
            {
                return;
            }

            this.Linked = false;
            this.RelativePath = null;
            this.DependentObject = false;
            this.DependentLibraryItems = null;
            this.LibraryUniqueIdentifier = Guid.Empty;
        }

		#endregion
		#region ISerializable Members

		// Serialization property constants
		private const string mcLIBRARY_ITEM_UNIQUE_IDENTIFIER = "libraryUniqueIdentifier";
		private const string mcVERSION = "version";
		private const string mcUI_VERSION = "uiversion";
		private const string mcLIBRARY_ITEM = "libraryitem";
		private const string mcLINKED = "linked";
		private const string mcCREATED_BY = "createdby";
		private const string mcCREATED_DATETIME = "createddatetime";
		private const string mcCHECKOUT_BY = "checkedoutby";
		private const string mcCHECKOUT_BY_LOGIN_NAME = "checkedoutbyloginname";
		private const string mcCHECKOUT_DATETIME = "checkedoutdatetime";
		private const string mcCHECKOUT_STATUS = "checkedoutstatus";
		private const string mcMODIFIED_BY = "modifiedby";
		private const string mcMODIFIED_DATETIME = "modifeddatetime";
		private const string mcCHECKIN_COMMENT = "checkincomment";
		private const string mcRELATIVE_PATH = "relativepath";
		private const string mcPATH = "path";
		private const string mcFILE_NAME = "filename";
		private const string mcDEPENDENCIES = "dependencies";
		private const string mcFOLDER = "folder";
		private const string mcDATA_TYPE_STRING = "datatypestring";
		private const string mcSTRING_VALUE = "stringvalue";
		private const string mcDEPENDENT_OBJECT = "dependentobject";
        private const string mcIS_NEW_LIBRARY_ITEM = "isnewlibraryitem";

		public LibraryItem(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			try
			{
				this.LibraryUniqueIdentifier = new Guid((string)info.GetString(mcLIBRARY_ITEM_UNIQUE_IDENTIFIER));
				this.Version = (int)info.GetValue(mcVERSION, typeof(int));
				this.Linked = (bool)info.GetValue(mcLINKED, typeof(bool));
				this.CreatedBy = (string)info.GetString(mcCREATED_BY);
				this.CreatedDateTime = (DateTime)info.GetValue(mcCREATED_DATETIME, typeof(DateTime));
				this.CheckedOutBy = (string)info.GetString(mcCHECKOUT_BY);
				this.CheckedOutByLogInName = (string) info.GetString(mcCHECKOUT_BY_LOGIN_NAME);
				this.CheckedOutDateTime = (DateTime)info.GetValue(mcCHECKOUT_DATETIME, typeof(DateTime));
				this.CheckedOutStatus = (CheckOutStatus)info.GetValue(mcCHECKOUT_STATUS, typeof(CheckOutStatus));
				this.ModifiedBy = (string)info.GetString(mcMODIFIED_BY);
				this.ModifiedDateTime = (DateTime)info.GetValue(mcMODIFIED_DATETIME, typeof(DateTime));
				this.CheckInComment = (string)info.GetString(mcCHECKIN_COMMENT);
				this.RelativePath = (string)info.GetString(mcRELATIVE_PATH);
				this.LocationPath = (string)info.GetString(mcPATH);
				this.Filename = (string)info.GetString(mcFILE_NAME);
				this.DependentLibraryItems = (List<LibraryItem>) info.GetValue(mcDEPENDENCIES, typeof(List<LibraryItem>));
				this.RepositoryFolder = (RepositoryFolder) info.GetValue(mcFOLDER, typeof(RepositoryFolder));
				this.StringValue = (string)info.GetString(mcSTRING_VALUE);
				this.DataTypeString = (string)info.GetString(mcDATA_TYPE_STRING);
				this.DependentObject = (bool)info.GetValue(mcDEPENDENT_OBJECT, typeof(bool));
                this.IsNewLibraryItem = (bool)info.GetValue(mcIS_NEW_LIBRARY_ITEM, typeof(bool));

                try
                {
                    this.LibraryType = (LibraryItemType)info.GetValue(mcLIBRARY_ITEM, typeof(LibraryItemType));
                }
                catch
                { 
                    if (this.GetType() == typeof(Question))
                    {
                        this.LibraryType = LibraryItemType.Question;
                    }
                    else if (this.GetType() == typeof(Outcome))
                    {
                        this.LibraryType = LibraryItemType.Outcome;
                    }
                    else if (this.GetType() == typeof(WordTemplateDocument2) || this.GetType() == typeof(TemplateDocument))
                    {
                        this.LibraryType = LibraryItemType.Template;
                    }
                    else if (this.GetType() == typeof(PFunction) || this.GetType() == typeof(ArithmeticPFunction) ||
                             this.GetType() == typeof(DatePFunction))
                    {
                        this.LibraryType = LibraryItemType.Function;
                    }
                    else if (this.GetType() == typeof(InterviewPage) || this.GetType() == typeof(ExternalPage) ||
                             this.GetType() == typeof(InternalPage))
                    {
                        this.LibraryType = LibraryItemType.InterviewPage;
                    }
                    else if (this.GetType() == typeof(SimpleOutcome))
                    {
                        this.LibraryType = LibraryItemType.SimpleOutcome;
                    }
                    else
                    {
                        //TODO - RESOURCE
                        string message = "Unable to deserialize {0} correctly. The library item type cannot be discovered.";
                        throw new SerializationException(string.Format(message, this.Name));
                    }
                }
			}
			catch
			{
				// Retro version - swallow the error
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			try
			{
				base.GetObjectData(info, context);
				
				info.AddValue(mcLIBRARY_ITEM_UNIQUE_IDENTIFIER, this.LibraryUniqueIdentifier.ToString());
				info.AddValue(mcVERSION, this.Version);
				info.AddValue(mcLIBRARY_ITEM, this.LibraryType);
				info.AddValue(mcLINKED, this.Linked);
				info.AddValue(mcCREATED_BY, this.CreatedBy);
				info.AddValue(mcCREATED_DATETIME, this.CreatedDateTime);
				info.AddValue(mcCHECKOUT_BY, this.CheckedOutBy);
				info.AddValue(mcCHECKOUT_BY_LOGIN_NAME, this.CheckedOutByLogInName);
				info.AddValue(mcCHECKOUT_DATETIME, this.CheckedOutDateTime);
				info.AddValue(mcCHECKOUT_STATUS, this.CheckedOutStatus);
				info.AddValue(mcMODIFIED_BY, this.ModifiedBy);
				info.AddValue(mcMODIFIED_DATETIME, this.ModifiedDateTime);
				info.AddValue(mcCHECKIN_COMMENT, this.CheckInComment);
				info.AddValue(mcRELATIVE_PATH, this.RelativePath);
				info.AddValue(mcPATH, this.LocationPath);
				info.AddValue(mcFILE_NAME, this.Filename);
				info.AddValue(mcDEPENDENCIES, this.DependentLibraryItems);
				info.AddValue(mcFOLDER, this.RepositoryFolder);
				info.AddValue(mcSTRING_VALUE, this.StringValue);
				info.AddValue(mcDATA_TYPE_STRING, this.DataTypeString);
				info.AddValue(mcDEPENDENT_OBJECT, this.DependentObject);
                info.AddValue(mcIS_NEW_LIBRARY_ITEM, this.IsNewLibraryItem);
			}
			catch
			{
				// Retro version - Swallow the error
			}
		}

		#endregion
    }
}
