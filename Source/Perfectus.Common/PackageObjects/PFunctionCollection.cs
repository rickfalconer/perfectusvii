using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Function
	/// </summary>
	[Serializable]
	public sealed class PFunctionCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FunctionCollection class.
		/// </summary>
		internal PFunctionCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the FunctionCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new FunctionCollection.
		/// </param>
		internal PFunctionCollection(PFunction[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the FunctionCollection class, containing elements
		/// copied from another instance of FunctionCollection
		/// </summary>
		/// <param name="items">
		/// The FunctionCollection whose elements are to be added to the new FunctionCollection.
		/// </param>
		internal PFunctionCollection(PFunctionCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this FunctionCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this FunctionCollection.
		/// </param>
		internal void AddRange(PFunction[] items)
		{
			foreach (PFunction item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another FunctionCollection to the end of this FunctionCollection.
		/// </summary>
		/// <param name="items">
		/// The FunctionCollection whose elements are to be added to the end of this FunctionCollection.
		/// </param>
		internal void AddRange(PFunctionCollection items)
		{
			foreach (PFunction item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Function to the end of this FunctionCollection.
		/// </summary>
		/// <param name="value">
		/// The Function to be added to the end of this FunctionCollection.
		/// </param>
		public void Add(PFunction value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Function value is in this FunctionCollection.
		/// </summary>
		/// <param name="value">
		/// The Function value to locate in this FunctionCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this FunctionCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(PFunction value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this FunctionCollection
		/// </summary>
		/// <param name="value">
		/// The Function value to locate in the FunctionCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(PFunction value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the FunctionCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Function is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Function to insert.
		/// </param>
		internal void Insert(int index, PFunction value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Function at the given index in this FunctionCollection.
		/// </summary>
		public PFunction this[int index]
		{
			get { return (PFunction) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Function from this FunctionCollection.
		/// </summary>
		/// <param name="value">
		/// The Function value to remove from this FunctionCollection.
		/// </param>
		internal void Remove(PFunction value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by FunctionCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(PFunctionCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public PFunction Current
			{
				get { return (PFunction) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (PFunction) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this FunctionCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}