using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for PackageItemEventArgs.
	/// </summary>
	public sealed class PackageItemEventArgs : EventArgs
	{
		private PackageItem _item;

		public PackageItem Item
		{
			get { return _item; }
		}

		public PackageItemEventArgs(PackageItem item)
		{
			this._item = item;
		}
	}
}