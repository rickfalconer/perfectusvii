using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type QuestionFolder
	/// </summary>
	[Serializable]
	public sealed class QuestionFolderCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the QuestionFolderCollection class.
		/// </summary>
		internal QuestionFolderCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the QuestionFolderCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new QuestionFolderCollection.
		/// </param>
		internal QuestionFolderCollection(QuestionFolder[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the QuestionFolderCollection class, containing elements
		/// copied from another instance of QuestionFolderCollection
		/// </summary>
		/// <param name="items">
		/// The QuestionFolderCollection whose elements are to be added to the new QuestionFolderCollection.
		/// </param>
		internal QuestionFolderCollection(QuestionFolderCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this QuestionFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this QuestionFolderCollection.
		/// </param>
		internal void AddRange(QuestionFolder[] items)
		{
			foreach (QuestionFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another QuestionFolderCollection to the end of this QuestionFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The QuestionFolderCollection whose elements are to be added to the end of this QuestionFolderCollection.
		/// </param>
		internal void AddRange(QuestionFolderCollection items)
		{
			foreach (QuestionFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type QuestionFolder to the end of this QuestionFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionFolder to be added to the end of this QuestionFolderCollection.
		/// </param>
		public void Add(QuestionFolder value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic QuestionFolder value is in this QuestionFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionFolder value to locate in this QuestionFolderCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this QuestionFolderCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(QuestionFolder value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this QuestionFolderCollection
		/// </summary>
		/// <param name="value">
		/// The QuestionFolder value to locate in the QuestionFolderCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(QuestionFolder value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the QuestionFolderCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the QuestionFolder is to be inserted.
		/// </param>
		/// <param name="value">
		/// The QuestionFolder to insert.
		/// </param>
		internal void Insert(int index, QuestionFolder value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the QuestionFolder at the given index in this QuestionFolderCollection.
		/// </summary>
		public QuestionFolder this[int index]
		{
			get { return (QuestionFolder) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific QuestionFolder from this QuestionFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The QuestionFolder value to remove from this QuestionFolderCollection.
		/// </param>
		internal void Remove(QuestionFolder value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by QuestionFolderCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(QuestionFolderCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public QuestionFolder Current
			{
				get { return (QuestionFolder) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (QuestionFolder) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this QuestionFolderCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}
