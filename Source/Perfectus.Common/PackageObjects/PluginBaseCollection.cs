using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type PluginBase
	/// </summary>
	[Serializable]
	public sealed class PluginBaseCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the PluginBaseCollection class.
		/// </summary>
		public PluginBaseCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PluginBaseCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PluginBaseCollection.
		/// </param>
		public PluginBaseCollection(PluginBase[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PluginBaseCollection class, containing elements
		/// copied from another instance of PluginBaseCollection
		/// </summary>
		/// <param name="items">
		/// The PluginBaseCollection whose elements are to be added to the new PluginBaseCollection.
		/// </param>
		public PluginBaseCollection(PluginBaseCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this PluginBaseCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PluginBaseCollection.
		/// </param>
		public void AddRange(PluginBase[] items)
		{
			foreach (PluginBase item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PluginBaseCollection to the end of this PluginBaseCollection.
		/// </summary>
		/// <param name="items">
		/// The PluginBaseCollection whose elements are to be added to the end of this PluginBaseCollection.
		/// </param>
		public void AddRange(PluginBaseCollection items)
		{
			foreach (PluginBase item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type PluginBase to the end of this PluginBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginBase to be added to the end of this PluginBaseCollection.
		/// </param>
		public void Add(PluginBase value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic PluginBase value is in this PluginBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginBase value to locate in this PluginBaseCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PluginBaseCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(PluginBase value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PluginBaseCollection
		/// </summary>
		/// <param name="value">
		/// The PluginBase value to locate in the PluginBaseCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(PluginBase value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PluginBaseCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the PluginBase is to be inserted.
		/// </param>
		/// <param name="value">
		/// The PluginBase to insert.
		/// </param>
		public void Insert(int index, PluginBase value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the PluginBase at the given index in this PluginBaseCollection.
		/// </summary>
		public PluginBase this[int index]
		{
			get { return (PluginBase) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific PluginBase from this PluginBaseCollection.
		/// </summary>
		/// <param name="value">
		/// The PluginBase value to remove from this PluginBaseCollection.
		/// </param>
		public void Remove(PluginBase value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PluginBaseCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(PluginBaseCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public PluginBase Current
			{
				get { return (PluginBase) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (PluginBase) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PluginBaseCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}