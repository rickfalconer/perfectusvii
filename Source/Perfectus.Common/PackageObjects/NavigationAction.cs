using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for NavigationAction.
	/// </summary>
	[Serializable]
	public sealed class NavigationAction : ActionBase, IPackageItemContainer, ISerializable
	{
		private InterviewPage page = null;

		public InterviewPage Page
		{
			get { return page; }
			set { page = value; }
		}

		public NavigationAction():base()
		{}

		public NavigationAction(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			page = (InterviewPage)info.GetValue("page", typeof(InterviewPage));	
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("page", page);
		}


		#region IPackageItemContainer Members

		public override bool ContainsReferenceTo(PackageItem item)
		{
			bool b = base.ContainsReferenceTo(item);

			return b || (page == item);
		}

		#endregion

//		public override object Clone()
//		{
//			return this.MemberwiseClone();			
//		}

	}
}