using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Function.
	/// </summary>
	[Serializable]
	[SoapInclude(typeof(ArithmeticPFunction))]
	[SoapInclude(typeof(DatePFunction))]
	public abstract class PFunction : AnswerProvider, ITemplateItem, ISerializable, IPackageItemContainer, IStringBindingItem
	{
		private string formatString = string.Empty;
		private string formatStringParams = string.Empty;
		private string previewHoverText = string.Empty;
        private string interviewFormatString = string.Empty;
        private string interviewFormatStringParams = string.Empty;

		// Defines the Shared Library type for this class
		private LibraryItemType _LibraryItemType = LibraryItemType.Function;

		[Category("SharedLibrary")]
		[Browsable(false)]
		public override LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

		protected PFunction()
		{
			Name = GetUniqueName(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.PFunction.FunctionName"),null);
			
		}

		[Category("Display")]
		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.FormatStringEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FormatStringConverter, Studio")]
        [DisplayRanking( 6 )]
        public string FormatString
		{
			get { return formatString; }
			set { formatString = value; }
		}

		[Category("Display")]
		[Browsable(false)]
		public string FormatStringParams
		{
			get { return formatStringParams; }
			set
			{
				formatStringParams = value;
				OnPropertyChanged("FormatStringParams");
			}
		}

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.FormatStringEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FormatStringConverter, Studio")]
        [DisplayRanking( 4 )]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string InterviewFormatString
        {
            get { return interviewFormatString; }
            set { interviewFormatString = value; }
        }

        [Category("Display")]
        [Browsable(false)]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string InterviewFormatStringParams
        {
            get { return interviewFormatStringParams; }
            set { interviewFormatStringParams = value; }
        }

		public void AddWordMLTagToObject(DataObject inObj)
		{
			WordTemplateDocument2.AddWordMLTagToDataObject(inObj, this, SafeTagName);
		}

		#region ISerializable Members

		public PFunction(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			try
			{
				formatString = info.GetString("formatString");
			}
			catch
			{
				formatString = string.Empty;
			}
			try
			{
				formatStringParams = info.GetString("formatStringParams");
			}
			catch
			{
				formatStringParams = string.Empty;
			}
			try
			{
				previewHoverText = info.GetString("previewHoverText");
			}
			catch
			{
				previewHoverText = string.Empty;
			}

            try
            {
                interviewFormatStringParams = info.GetString("interviewFormatStringParams");
            }
            catch
            {
                interviewFormatStringParams = string.Empty;
            }

            try
            {
                interviewFormatString = info.GetString("interviewFormatString");
            }
            catch
            {
                interviewFormatString = string.Empty;
            }
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("formatString", formatString);
			info.AddValue("formatStringParams", formatStringParams);
			info.AddValue("previewHoverText", previewHoverText);
            info.AddValue("interviewFormatString", interviewFormatString);
            info.AddValue("interviewFormatStringParams", interviewFormatStringParams);
		}

		#endregion
		#region ITemplateItem Members

		public string GetTagName()
		{
			return SafeTagName;
		}

		public string GetTagContents()
		{
			return m_name;
		}

		[Category("Display")]
		[Browsable(true)]
        [DisplayRanking( 20 )]
        public string PreviewHoverText
		{
			get { return previewHoverText; }
			set
			{
				previewHoverText = value;
				OnPropertyChanged("PreviewHoverText");
			}
		}


		#endregion
		#region IPackageItemContainer Members

		public abstract bool ContainsReferenceTo(PackageItem item);

		#endregion
		#region IStringBindingItem Members

		public void AddHTMLTagToObject(DataObject inObj, string fieldName)
		{	
			StringBinding.AddHTMLTagToDataObject(inObj, this, fieldName);
		}

		#endregion
	}
}
