using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Template.
	/// </summary>
	[Serializable]
	public class TemplateDocumentLink : TemplateDocument
	{
        internal TemplateDocumentLink(TemplateDocument the_document_that_we_link_to)
            : base()
        {
            base.PublishedFileName = (TextQuestionQueryValue) the_document_that_we_link_to.PublishedFileName.Clone();
            UniqueIdentifier = the_document_that_we_link_to.UniqueIdentifier;
        }

		#region ISerializable Members

        public TemplateDocumentLink(SerializationInfo info, StreamingContext context)
            : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		#endregion

        public override System.Collections.Generic.List<LibraryItem> GetDependentLibraryItems()
        {
            return null;
        }

        public override System.Collections.Generic.List<LibraryItem> SetDependentLibraryItems()
        {
            return null;
        }
    }
}
