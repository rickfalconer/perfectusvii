using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for RuleTemplateEventArgs.
	/// </summary>
	public sealed class RuleDeliveryEventArgs : EventArgs
	{
		private Rule2 r;
		private Delivery delivery;

		public Rule2 Rule2
		{
			get { return r; }
		}

        public Delivery Delivery
		{
			get { return delivery; }
		}

        public RuleDeliveryEventArgs(Rule2 r, Delivery delivery)
		{
			this.r = r;
			this.delivery = delivery ;
		}
	}
}

