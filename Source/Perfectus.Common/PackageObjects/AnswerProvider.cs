using System;
using System.Globalization;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for AnswerProvider.
    /// </summary>
    public abstract class AnswerProvider : LibraryItem, ISerializable
    {
        protected PerfectusDataType m_dataType;
        private StringBindingMetaData stringBindingMetaData;

        public enum Caller
        {
            Assembly,
            Interview,
            System
        }

        [Browsable(false)]
        public StringBindingMetaData StringBindingMetaData
        {
            get { return stringBindingMetaData; }
            set { stringBindingMetaData = value; }
        }

        public abstract PerfectusDataType DataType { get; set; }

        public abstract object GetSingleAnswer(Caller c);

        public AnswerProvider() : base() { }

        ///// <summary>
        ///// Method name changed to ConvertToBestType.
        ///// </summary>
        ///// <param name="answer"></param>
        ///// <param name="dataType"></param>
        ///// <param name="caller"></param>
        ///// <returns></returns>
        //[Obsolete]
        //public static object GetBestType(object answer, PerfectusDataType dataType, Caller caller)
        //{
        //    bool success;
        //    return ConvertToBestType(answer, dataType, out success, caller);
        //}

        ///// <summary>
        ///// Method name changed to ConvertToBestType.
        ///// </summary>
        ///// <param name="answer"></param>
        ///// <param name="dataType"></param>
        ///// <param name="caller"></param>
        ///// <returns></returns>
        //[Obsolete]
        //public static object GetBestType(object answer, PerfectusDataType dataType, out bool success, Caller caller)
        //{
        //    if (answer is TextQuestionValue)
        //    {
        //        answer = ((TextQuestionValue)answer).Evaluate(caller);

        //        if (answer == null)
        //        {
        //            answer = "";
        //        }
        //    }
        //    success = true;
        //    try
        //    {
        //        switch (dataType)
        //        {
        //            case PerfectusDataType.Date:
        //                if (answer is DateTime)
        //                {
        //                    return answer;
        //                }
        //                return DateTime.Parse(answer.ToString());
        //            case PerfectusDataType.Number:
        //                if (answer is double)
        //                {
        //                    return answer;
        //                }
        //                else
        //                {
        //                    double d;
        //                    if (double.TryParse(answer.ToString(), NumberStyles.Any, CultureInfo.CurrentUICulture, out d))
        //                    {
        //                        return d;
        //                    }
        //                    else
        //                    {
        //                        success = false;
        //                        return answer;
        //                    }
        //                }
        //            case PerfectusDataType.YesNo:
        //                if (answer is bool)
        //                {
        //                    return answer;
        //                }
        //                else
        //                {
        //                    string s = answer.ToString().Trim().ToLower(CultureInfo.InvariantCulture);
        //                    if (s == "true" || s == "yes" || s == "1")
        //                    {
        //                        return true;
        //                    }
        //                    if (s == "false" || s == "no" || s == "0")
        //                    {
        //                        return false;
        //                    }
        //                    if (s == string.Empty)
        //                    {
        //                        success = false;
        //                        return answer;
        //                    }
        //                    return bool.Parse(s);
        //                }
        //            default:
        //                return answer;
        //        }
        //    }
        //    catch
        //    {
        //        success = false;
        //        return answer;
        //    }
        //}

        public static object ConvertToBestType(object answer, PerfectusDataType dataType, Caller caller, StringBindingMetaData stringBindingMetaData)
        {
            bool success;
            return ConvertToBestType(answer, dataType, out success, caller, stringBindingMetaData);
        }

        public static object ConvertToBestType(object answer, PerfectusDataType dataType, out bool success, Caller caller, StringBindingMetaData stringBindingMetaData)
        {
            if (answer is TextQuestionQueryValue)
            {
                answer = ((TextQuestionQueryValue)answer).Evaluate(caller, stringBindingMetaData);

                if (answer == null)
                {
                    answer = "";
                }
            }
            success = true;
            try
            {
                switch (dataType)
                {
                    case PerfectusDataType.Date:
                        if (answer is DateTime)
                        {
                            return answer;
                        }
                        return DateTime.Parse(answer.ToString());
                    case PerfectusDataType.Number:
                        if (answer is decimal)
                        {
                            return answer;
                        }
                        else
                        {
                            decimal d;
                            if (decimal.TryParse(answer.ToString(), NumberStyles.Any, CultureInfo.CurrentUICulture, out d))
                            {
                                return d;
                            }
                            else
                            {
                                success = false;
                                return answer;
                            }
                        }
                    case PerfectusDataType.YesNo:
                        if (answer is bool)
                        {
                            return answer;
                        }
                        else
                        {
                            string s = answer.ToString().Trim().ToLower(CultureInfo.InvariantCulture);
                            if (s == "true" || s == "yes" || s == "1")
                            {
                                return true;
                            }
                            if (s == "false" || s == "no" || s == "0")
                            {
                                return false;
                            }
                            if (s == string.Empty)
                            {
                                success = false;
                                return answer;
                            }
                            return bool.Parse(s);
                        }
                    default:
                        return answer;
                }
            }
            catch
            {
                success = false;
                return answer;
            }
        }

        #region ISerializable Members

        public AnswerProvider(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            m_dataType = (PerfectusDataType)info.GetValue("dataType", typeof(PerfectusDataType));
        }


        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("dataType", m_dataType);
        }

        #endregion
    }

}
