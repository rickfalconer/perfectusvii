using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for HtmlBlock.
	/// </summary>
	[Serializable]
	public sealed class ImageBlock : IPageItem, ISerializable
	{
		private Image image = null;
		private int designerHeight;
		private Guid uniqueIdentifier;
		private YesNoQueryValue visible;

		public YesNoQueryValue Visible
		{
			get { return visible; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
		}

		public int DesignerHeight
		{
			get { return designerHeight; }
			set { designerHeight = value; }
		}

		[XmlIgnore] //SL
		public Image Image
		{
			get { return image; }
			set
			{
				image = value;
			}
		}

		public string GetImageAsHexString(ImageFormat imageFormat)
		{
			if (image == null)
			{
				throw new NullReferenceException("Image property is null");
			}
			else
			{
				using (MemoryStream ms = new MemoryStream())
				{					
					image.Save(ms, imageFormat);
					ms.Flush();
					ms.Seek(0, SeekOrigin.Begin);
					byte[] bytes = ms.ToArray();
					StringBuilder sb = new StringBuilder(bytes.Length * 2);

					foreach(byte b in bytes)
					{
						sb.Append(b.ToString("X2"));
					}
					return sb.ToString();
				}				
			}
		}

		public string[] GetImageAsHexLines()
		{
			string[] lines = new string[image.Height];
			if (image == null)
			{
				throw new NullReferenceException("Image property is null");
			}
			else
			{
				Bitmap bmp = (Bitmap)image;
				for(int y=0; y<bmp.Height; y++)
				{
					StringBuilder lineBuilder = new StringBuilder(bmp.Width * 8);
					for (int x=0; x<bmp.Width; x++)
					{
						Color c = bmp.GetPixel(x, y);
						lineBuilder.Append(string.Format("{0:X2}{1:X2}{2:X2}{3:X2}", c.A,  c.R, c.G, c.B));
					}
					lines[y] = lineBuilder.ToString();
				}	
			}
			return lines;
		}

		
		public string GetImageAsJpegBase64()
		{
			if (image == null)
			{
				throw new NullReferenceException("Image property is null");
			}
			else
			{
				using (MemoryStream ms = new MemoryStream())
				{
					image.Save(ms, ImageFormat.Jpeg);
					ms.Flush();
					ms.Seek(0, SeekOrigin.Begin);
					byte[] bytes = ms.ToArray();
					return Convert.ToBase64String(bytes);
				}
			}
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		public ImageBlock(){}

		public ImageBlock(Image image, int designerHeight, Guid uniqueIdentifier, YesNoQueryValue visible) : base()
		{
			this.image = image;
			this.designerHeight = designerHeight;
			this.uniqueIdentifier = uniqueIdentifier;
			this.visible = visible;
		}

		#region ISerializable Members

		public ImageBlock(SerializationInfo info, StreamingContext context)
		{
			image = (Image)info.GetValue("image", typeof(Image));
			designerHeight = info.GetInt32("designerHeight");

			// Unique identifer was added prior to beta, but after the format got locked.
			object o = null;

			try
			{
				o = info.GetValue("uniqueIdentifier", typeof (Guid));
			}
			catch
			{
			}
			if (o == null)
			{
				uniqueIdentifier = Guid.NewGuid();
			}
			else
			{
				uniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			}

			try
			{
				visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("image", image);
			info.AddValue("designerHeight", designerHeight);
			info.AddValue("uniqueIdentifier", uniqueIdentifier);
			info.AddValue("visible", visible);
		}

		#endregion
	}
}