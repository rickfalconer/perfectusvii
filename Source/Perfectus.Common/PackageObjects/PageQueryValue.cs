using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	[Serializable]
	public sealed class PageQueryValue : IPackageItemContainer, ISerializable, IXmlSerializable
	{
		public enum PageQueryValueType
		{
			Page,
			Query,
			NoValue
		}

		private PageQueryValueType valueType;
		private InterviewPage pageValue;
		private Query queryValue;

		[field : NonSerialized]
		public event EventHandler ValueChanged;

		public PageQueryValue()
		{
			valueType = PageQueryValueType.NoValue;
			pageValue = null;
			queryValue = null;
		}

		[Obsolete]
		public InterviewPage Evaluate()
		{
			return Evaluate(AnswerProvider.Caller.Interview);
		}

		public InterviewPage Evaluate(AnswerProvider.Caller caller)
		{
			if (valueType == PageQueryValueType.NoValue)
			{
				return null;
			} 
			else if (valueType == PageQueryValueType.Page)
			{
				return pageValue;
			} 
			else if (valueType == PageQueryValueType.Query && queryValue != null)
			{
				return ((NavigationAction)queryValue.Evaluate(caller)).Page;
			} 
			else
			{
				return null;
			}
			
		}

		public PageQueryValueType ValueType
		{
			get { return valueType; }
//			set
//			{
//				switch (value)
//				{
//					case PageQueryValueType.Query:
//						pageValue = null;
//						OnValueChanged();
//						break;
//					case PageQueryValueType.Page:
//						queryValue = null;
//						OnValueChanged();
//						break;
//					case PageQueryValueType.NoValue:
//						pageValue = null;
//						queryValue = null;
//						OnValueChanged();
//						break;
//				}
//				valueType = value;
//			}
		}

		public void SetNoValue()
		{
			this.valueType = PageQueryValueType.NoValue;
			queryValue = null;
			pageValue = null;
			OnValueChanged();
		}

		public InterviewPage PageValue
		{
			get { return pageValue; }
			set
			{
				pageValue = value;
				valueType = PageQueryValueType.Page;
				OnValueChanged();

			}
		}

		public Query QueryValue
		{
			get 
			{ 				
				return queryValue; 
			}
			set
			{
				queryValue = value;
				valueType = PageQueryValueType.Query;
				queryValue.PropertyChanged += new PropertyChangedEventHandler(queryValue_PropertyChanged);
				//TODO: Bubble up changes to the query itself, not just its ref.  The diagram will need to know
				OnValueChanged();
			}
		}

		private void OnValueChanged()
		{
			if (ValueChanged != null)
			{
				ValueChanged(this, new EventArgs());
			}
		}

		private void queryValue_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			OnValueChanged();
		}

		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			switch (valueType)
			{
				case PageQueryValueType.NoValue:
					return false;
				case PageQueryValueType.Page:
					if (pageValue == item)
					{
						return true;
					}
					else
					{
						return false;
					}
				case PageQueryValueType.Query:
				default:
					if (queryValue != null)
					{
						return queryValue.ContainsReferenceTo(item);
					}
					else
					{
						return false;
					}
			}
		}

		#endregion		
		#region ISerializable Members
		
		public PageQueryValue(SerializationInfo info, StreamingContext context)
		{
			try 
			{
				valueType = (PageQueryValueType) info.GetValue("valueType", typeof (PageQueryValueType));
			}
			catch
			{
			}
			try 
			{
				if(valueType == PageQueryValueType.Query)
				{
					QueryValue = (Query) info.GetValue("queryValue", typeof (Query));
				}
			}
			catch
			{
			}			
			
			try 
			{
				if(valueType == PageQueryValueType.Page)
				{
					PageValue = (InterviewPage) info.GetValue("pageValue", typeof (InterviewPage));
				}
			}
			catch
			{
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("queryValue", queryValue);
			info.AddValue("valueType", valueType);
			info.AddValue("pageValue", pageValue);
		}

		#endregion
		#region IXmlSerializable Members

		public XmlSchema GetSchema()
		{
			return null;
		}

		const string mcVALUE_TYPE = "ValueType";
		const string mcPAGE_VALUE = "PageValue";
		const string mcQUERY_VALUE = "QueryValue";
		
		public void WriteXml(XmlWriter w)
		{
			// Value Type
			w.WriteStartElement(mcVALUE_TYPE);
			w.WriteString(Convert.ToInt32(this.ValueType).ToString());
			w.WriteEndElement();

			// Page Value
			/*	Not used for the initial release of 5.2 - uncomment this to utilise the next page functionality
			  if (this.PageValue != null)
				Serializer.WriteDependentInformation(w, this.PageValue, mcPAGE_VALUE);
			else
			*/
			w.WriteElementString(mcPAGE_VALUE, "");

			// Query Value
			/*	Not used for the initial release of 5.2 - uncomment this to utilise the next page functionality
			 if (this.QueryValue != null)
				this.QueryValue.WriteXml(w);
			else
			*/
				w.WriteElementString(mcQUERY_VALUE, "");
		}

		public void ReadXml(XmlReader r)
		{
			string[] values;

			// Value type
			r.ReadStartElement(mcVALUE_TYPE);
			this.valueType = (PageQueryValueType) Convert.ToInt32(r.ReadString());
			r.ReadEndElement();

			// Page Value
			if (!r.IsEmptyElement)
			{
				// Read the page value
				r.ReadStartElement(mcPAGE_VALUE);
				r.ReadStartElement(Serializer.DependencyKey);

				// Type
				r.ReadStartElement(Serializer.TypeKey);
				r.Read();
				r.ReadEndElement();

				// Values
				values = Serializer.ReadLibraryItem(r);
				r.ReadEndElement();
				r.ReadEndElement();

				// Set the new library item
				InternalPage internalPage = new InternalPage();
				internalPage.Name = values[Serializer.ValueIndex];
				internalPage.LibraryUniqueIdentifier = new Guid(values[Serializer.UniqueIdentifierIndex]);
				internalPage.RelativePath = values[Serializer.RelativePathIndex];
				this.PageValue = internalPage;

			}
			else
			{
				r.Read();
			}

			// Query Value
			if (!r.IsEmptyElement)
			{
				this.QueryValue.ReadXml(r);
			}
			else
			{
				r.Read();
			}
		}

		#endregion
	}
}