using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Rule2TemplateEventArgs.
	/// </summary>
	public sealed class Rule2TemplateEventArgs : EventArgs
	{
		private Rule2 r;
		private TemplateDocument template;

		public Rule2 Rule2
		{
			get { return r; }
		}

		public TemplateDocument Template
		{
			get { return template; }
		}

		public Rule2TemplateEventArgs(Rule2 r, TemplateDocument template)
		{
			this.r = r;
			this.template = template;
		}
	}
}