using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Rule2.
	/// </summary>
	[Serializable]
	public sealed class Rule2 : PackageItem, ISerializable
	{
		private Server parentServer;

        private List<Delivery> deliveries;
            private YesNoQueryValue deliveryRule = new YesNoQueryValue();
		private YesNoQueryValue doRuleAtFinish = new YesNoQueryValue();

        [Browsable( true )]
        [Editor( "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof( UITypeEditor ) )]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 4 )]
        public YesNoQueryValue DeliveryRule
		{
			get { return deliveryRule; }
			set
			{
				deliveryRule = value;
				OnPropertyChanged("DeliveryRule");
			}
		}

        [Browsable( true )]
        [Editor( "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof( UITypeEditor ) )]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 2 )]
        public YesNoQueryValue DoRuleAtFinish
		{
			get { return doRuleAtFinish; }
			set
			{
				doRuleAtFinish = value;
				OnPropertyChanged("DoRuleAtFinish");
			}
		}


		[Browsable(false)]
		public List<Delivery> Deliveries
		{
            get { return deliveries; }
		}

        public void Add(Delivery delivery)
        {
            deliveries.Add(delivery);
        }

        [Browsable(false)]
		public Server ParentServer
		{
			get { return parentServer; }
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		private Rule2(){} //SL

		public Rule2(Server parentServer) : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.Rule.NewItemName");
			this.parentServer = parentServer;
            deliveries = new List<Delivery>();
			this.deliveryRule.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
			this.deliveryRule.YesNoValue = true;
			this.doRuleAtFinish.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
			this.doRuleAtFinish.YesNoValue = true;
		}

        public override bool Valid(out string message)
        {
            message = default(String);
            bool valid = base.Valid(out message);
            StringBuilder sb = new StringBuilder(message);

            foreach (Delivery delivery in Deliveries)
                if (!delivery.Valid(out message))
                {
                    sb.AppendLine(" " + message);
                    valid = false;
                }
            if (!valid)
            {
                sb.Insert(0, new StringBuilder().AppendLine(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Rule.NotValid"), Name)).ToString());
                message = sb.ToString();
            }
            return valid;
        }

		#region ISerializable Members

		public Rule2(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            /*
			templates = (TemplateDocumentCollection) info.GetValue("templates", typeof (TemplateDocumentCollection));
			distributors = (PluginBaseCollection) info.GetValue("distributors", typeof (PluginBaseCollection));
			converters = (PluginBaseCollection) info.GetValue("converters", typeof (PluginBaseCollection));
			*/
            deliveries = (List<Delivery>)info.GetValue("deliveries", typeof(List<Delivery>));

            deliveryRule = (YesNoQueryValue) info.GetValue("deliveryRule", typeof (YesNoQueryValue));
			parentServer = (Server) info.GetValue("parentServer", typeof (Server));
			try 
			{
				doRuleAtFinish = (YesNoQueryValue) info.GetValue("doRuleAtFinish", typeof (YesNoQueryValue));
			}
			catch
			{
				doRuleAtFinish = new YesNoQueryValue();
				doRuleAtFinish.YesNoValue = true;
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
            /*
			info.AddValue("templates", templates);
			info.AddValue("distributors", distributors);
			info.AddValue("converters", this.converters);
             */
            info.AddValue("deliveries", deliveries);

			info.AddValue("deliveryRule", this.deliveryRule);
			info.AddValue("parentServer", this.parentServer);
			info.AddValue("doRuleAtFinish",this.doRuleAtFinish);
		}

		#endregion
	}
}