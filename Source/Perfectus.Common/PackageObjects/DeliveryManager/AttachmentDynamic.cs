using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    public class AttachmentDynamic : AttachmentBase, ISerializable, ICloneable, IPackageItemRequiresUniqueName
    {
        Guid _hostQuestion;

        public AttachmentDynamic()
            : this("New Attachment")
        {
        }

        public AttachmentDynamic(String param_name)
            : base(param_name)
        {
            _hostQuestion = Guid.Empty;
        }

        public void SetPayload(Guid guid)
        {
            _hostQuestion = guid;
        }

        public Guid GetPayload()
        {
            return _hostQuestion;
        }

        #region ISerializable Members

        public AttachmentDynamic(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            try
            {
                _hostQuestion = (Guid)info.GetValue("hostQuestion", typeof(Guid));
            }
            catch
            {
                _hostQuestion = Guid.Empty;
            }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("hostQuestion", _hostQuestion, typeof(Object));
        }

        #endregion


        #region ICloneable Members

        public override object Clone()
        {
            object o = base.Clone();
            //((AttachmentDynamic)o).SetPayload();
            return o;
        }

        #endregion


        #region LocalisedPropertiesObject Members
        public override void AlterProperties( PropertyDescriptorCollection properties )
        {
            // FileName will be set defined by user or integration.
            // No point of showing the property to the designer
            for( int i = properties.Count - 1; i >= 0; i-- )
            {
                // Remove the Condition property
                if( properties[ i ].Name == "FileName" )
                {
                    //pf-3235 - we need this filename property for attachments
                    //properties.RemoveAt( i );
                    continue;
                }
            }

            // Do our AttachmentBase class actions next for further changes.
            base.AlterProperties( properties );
        }
        #endregion

        #region IPackageItemRequiresUniqueName Members
        [Browsable(false)]
        public new bool RequiresUniqueName
        {
            get { return false; }
        }

        #endregion
    }
}