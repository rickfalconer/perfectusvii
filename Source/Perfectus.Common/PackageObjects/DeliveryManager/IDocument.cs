using System;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IConverter.
	/// </summary>
    public interface IDocument : IPackageItemCommonProperties, ICloneable
	{
        YesNoQueryValue Condition { get; set; }
        TextQuestionQueryValue FileName { get; set; }
        Delivery ParentDelivery { get; set; }
	}
}
