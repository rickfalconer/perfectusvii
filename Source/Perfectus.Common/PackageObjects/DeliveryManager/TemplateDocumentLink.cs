using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Template.
	/// </summary>
	[Serializable]
    public class TemplateDocumentLink : TemplateDocument, IPackageItemRequiresUniqueName
	{
        internal TemplateDocumentLink(TemplateDocument the_document_that_we_link_to)
            : base()
        {
            UniqueIdentifier = the_document_that_we_link_to.UniqueIdentifier;
            this.Name = the_document_that_we_link_to.Name;
        }

		#region ISerializable Members

        public TemplateDocumentLink(SerializationInfo info, StreamingContext context)
            : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		#endregion

        public override System.Collections.Generic.List<LibraryItem> GetDependentLibraryItems()
        {
            return null;
        }

        public override System.Collections.Generic.List<LibraryItem> SetDependentLibraryItems()
        {
            return null;
        }

        #region IPackageItemRequiresUniqueName Members

        public bool RequiresUniqueName
        {
            get { return false; }
        }

        #endregion
    }
}
