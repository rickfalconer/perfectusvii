using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using Perfectus.Common.Helper;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    public class AttachmentStatic : AttachmentBase, ISerializable, ICloneable
    {
        byte[] _payload = null;

        public AttachmentStatic()
            : this("New Attachment")
        {
        }

        public AttachmentStatic(String param_name)
            : base(param_name)
        {
            // FB 2328, having a 0 length byte array causes the compression deserialisation to fail
            // _payload = new byte[0];
        }

        public void SetPayload(byte[] stream)
        {
            // FB 2328, having a 0 length byte array causes the compression deserialisation to fail
            if( stream == null || stream.Length == 0 )
                _payload = null;
            else
                _payload = (byte[])stream.Clone();
        }

        public byte[] GetPayload()
        {
            return _payload;
        }


        #region ISerializable Members

        public AttachmentStatic(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            try
            {
                _payload = null;
                if (context.State != StreamingContextStates.Persistence &&
                    context.State != StreamingContextStates.All)
                {
                    _payload = CompressionHelper.DecompressIfNecessary((byte[])info.GetValue("payload", typeof(byte[])));
                }
            }
            catch
            {
                _payload = null;
            }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            try
            {
                switch (context.State)
                {
                    case StreamingContextStates.Persistence:
                    case StreamingContextStates.All: 
                    break;
                    default:
                        info.AddValue("payload", CompressionHelper.CompressIfSmaller(_payload), typeof(Object));
                        break;
                }
            }
            catch { }
        }

        #endregion


        #region ICloneable Members

        public override object Clone()
        {
            object o = base.Clone();
            ((AttachmentStatic)o).SetPayload(null);
            return o;
        }

        #endregion
    }
}