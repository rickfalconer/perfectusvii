using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    public class Document : PackageItem, IDocument, ISerializable, IPackageItemRequiresUniqueName, ICloneable
    {
        private TemplateDocumentLink template;
        private Converter converter;

        [Browsable(false)]
        public Converter Converter
        {
            get { return converter; }
        }
        [Browsable(false)]
        public TemplateDocument Template
        {
            get { return template; }
        }
        [Browsable(false)]
        public bool HasTemplate
        {
            get { return template != null; }
        }

        public Document()
        {
            condition = new YesNoQueryValue();
            condition.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
            condition.YesNoValue = true;

            Name = "New Document";
        }

        public Document(Converter param_converter)
            : this()
        {
            if (param_converter == null || param_converter.Descriptor.PreferredDoc != "AnswerSetXml")
                throw new Exception("Expect valid converter of type 'AnswerSetXml'");
            SetConverter(param_converter);
        }

        public Document(TemplateDocument param_template, Converter param_converter)
            : this()
        {
            SetTemplate(param_template);
            SetConverter(param_converter);
        }

        public void SetTemplate(TemplateDocument param_template)
        {
            if (param_template == null)
                ClearTemplate();
            else
            {
                if (converter != null && converter.Descriptor.PreferredDoc == "AnswerSetXml")
                    throw new Exception("Document is of type AnswerSetXml");

                template = new TemplateDocumentLink(param_template);
            }
        }

        public void SetConverter(Converter param_converter)
        {
            if (converter != null && converter.Descriptor.PreferredDoc == "AnswerSetXml")
                ClearTemplate();
            ClearConverter();
            converter = param_converter;
            if (converter != null && ParentPackage != null)
                converter.ParentPackage = this.ParentPackage;
        }

        private void ClearTemplate()
        {
            template = null;
        }

        private void ClearConverter()
        {
            converter = null;
        }

        public override bool Valid()
        {
            string message;
            return Valid(out message);
        }

        public bool converterFoundAndValid()
        {
            if (null == Converter)
                return false;

            // Cannot test, so assume correctness
            if (ParentDelivery == null || ParentDelivery.ParentRule == null || ParentDelivery.ParentRule.ParentServer == null)
                return true;

            Server server = ParentDelivery.ParentRule.ParentServer;
            foreach (Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins.PluginDescriptor p in server.AvailablePlugins)
                if (p.UniqueIdentifier == Converter.Descriptor.UniqueIdentifier)
                    return true;
            return false;
        }

        public override bool Valid(out string message)
        {
            message = default(string);
            bool valid = base.Valid(out message);
            StringBuilder sb = new StringBuilder(message);

            if (valid && (Name == null || Name.Length == 0))
            {
                valid = false;
                sb.AppendLine(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.NamePropertyNull"));
            }

            if (valid && converter == null)
            {
                valid = false;
                sb.AppendLine(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.ConverterPropertyNull"));
            }

            if (valid && converter.Descriptor.PreferredDoc != "AnswerSetXml" && !HasTemplate)
            {
                valid = false;
                sb.AppendLine(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.TemplatePropertyNull"));
            }

            if (valid &&! converterFoundAndValid())
            {
                valid = false;
                sb.AppendLine(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.ConverterNotFound"), Converter.Name));
            }
            if (!valid)
            {
                sb.Insert(0, new StringBuilder().AppendLine(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.NotValid"), Name)).ToString());
message = sb.ToString();
            }
            return valid;
        }
        #region IDocument Members

        Delivery parentDelivery;
        [Browsable(false)]
        public Delivery ParentDelivery
        {
            get { return parentDelivery; }
            set { parentDelivery = value; }
        }


        private YesNoQueryValue condition;
        private TextQuestionQueryValue fileName;

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking(2)]
        [DisplayName( "Perfectus.Common.PackageObjects.PropertyNames.PublishedFileName" )]
        [DisplayDescription( "Perfectus.Common.PackageObjects.Document.FileName.Description" )]
        public TextQuestionQueryValue FileName
        {
            get
            {
                if ((fileName == null ||
                    fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.NoValue)
                    && template != null)
                {
                    // return the document name, if the template has no PFN
                    fileName = new TextQuestionQueryValue();
                    fileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    fileName.TextValue = this.Name;
                }
                return fileName;
            }
            set { if (fileName == null)fileName = new TextQuestionQueryValue(); fileName = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking(25)]
        [DisplayDescription( "Perfectus.Common.PackageObjects.Document.Condition.Description" )]
        public YesNoQueryValue Condition
        {
            get { return condition; }
            set { condition = value; }
        }

        #endregion

        #region ISerializable Members

        public Document(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            template = (TemplateDocumentLink)info.GetValue("template", typeof(TemplateDocumentLink));
            converter = (Converter)info.GetValue("converter", typeof(Converter));
            condition = (YesNoQueryValue)info.GetValue("condition", typeof(YesNoQueryValue));
            fileName = (TextQuestionQueryValue)info.GetValue("fileName", typeof(TextQuestionQueryValue));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("template", template);
            info.AddValue("converter", converter);
            info.AddValue("condition", condition);
            info.AddValue("fileName", fileName);
        }

        #endregion
        
        #region IPackageItemRequiresUniqueName Members

        [Browsable(false)]
        public bool RequiresUniqueName
        {
            get { return false; }
        }

        #endregion

        #region ICloneable Members

        public override object Clone()
        {
            //Document newDoc = base.Clone() as Document;
            Document newDoc = new Document();
            newDoc.SetTemplate(Template);

            if (FileName != null)
                newDoc.FileName = FileName.Clone() as TextQuestionQueryValue;
            newDoc.Condition = Condition.Clone() as YesNoQueryValue;
            newDoc.Name = Name;

            newDoc.ParentDelivery = ParentDelivery;
            newDoc.ParentPackage = ParentPackage;

            // Having some unexplainable serialisation issues with the Word2007 converter
            // Hence will copy all values manually and do not rely on PackageItem.Clone()
            if (this.Converter != null)
            {
                Converter newConverter = new Converter(this.converter.Descriptor);
                PropertyDescriptorCollection col = this.converter.GetProperties();
                foreach (PropertyDescriptor pd in col)
                {
                    object val = this.converter[pd.Name];
                    newConverter[pd.Name] = val;
                }
                newDoc.SetConverter(newConverter);
            }
            return newDoc;
        }

        #endregion
    }
}
