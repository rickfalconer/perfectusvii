using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Rule2
	/// </summary>
	[Serializable]
	public sealed class Rule2Collection : CollectionBase, IXmlSerializable
	{
		#region Private Member Variables

		[NonSerialized] private const string msRule2S_KEY = "Rule2s";

		#endregion
		#region Properties & Constructor

		/// <summary>
		/// Gets or sets the Rule2 at the given index in this Rule2Collection.
		/// </summary>
		public Rule2 this[int index]
		{
			get { return (Rule2) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Initializes a new empty instance of the Rule2Collection class.
		/// </summary>
		public Rule2Collection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the Rule2Collection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new Rule2Collection.
		/// </param>
		public Rule2Collection(Rule2[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the Rule2Collection class, containing elements
		/// copied from another instance of Rule2Collection
		/// </summary>
		/// <param name="items">
		/// The Rule2Collection whose elements are to be added to the new Rule2Collection.
		/// </param>
		public Rule2Collection(Rule2Collection items)
		{
			this.AddRange(items);
		}

		#endregion
		#region Methods

		/// <summary>
		/// Adds the elements of an array to the end of this Rule2Collection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this Rule2Collection.
		/// </param>
		internal void AddRange(Rule2[] items)
		{
			foreach (Rule2 item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another Rule2Collection to the end of this Rule2Collection.
		/// </summary>
		/// <param name="items">
		/// The Rule2Collection whose elements are to be added to the end of this Rule2Collection.
		/// </param>
		internal void AddRange(Rule2Collection items)
		{
			foreach (Rule2 item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Rule2 to the end of this Rule2Collection.
		/// </summary>
		/// <param name="value">
		/// The Rule2 to be added to the end of this Rule2Collection.
		/// </param>
		public void Add(Rule2 value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Rule2 value is in this Rule2Collection.
		/// </summary>
		/// <param name="value">
		/// The Rule2 value to locate in this Rule2Collection.
		/// </param>
		/// <returns>
		/// true if value is found in this Rule2Collection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Rule2 value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this Rule2Collection
		/// </summary>
		/// <param name="value">
		/// The Rule2 value to locate in the Rule2Collection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Rule2 value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the Rule2Collection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Rule2 is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Rule2 to insert.
		/// </param>
		internal void Insert(int index, Rule2 value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Removes the first occurrence of a specific Rule2 from this Rule2Collection.
		/// </summary>
		/// <param name="value">
		/// The Rule2 value to remove from this Rule2Collection.
		/// </param>
		public void Remove(Rule2 value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by Rule2Collection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(Rule2Collection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Rule2 Current
			{
				get { return (Rule2) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Rule2) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this Rule2Collection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		#endregion
		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		/// <summary>
		///		Serializes the Query into Xml.
		/// </summary>
		/// <param name="w">Writes the object to Xml.</param>
		public void WriteXml(XmlWriter w)
		{	
			
			w.WriteStartElement(msRule2S_KEY);


			w.WriteEndElement();
		}
				
		public void ReadXml(XmlReader r)
		{	
			// Read the starting element
			r.ReadStartElement(msRule2S_KEY);

			// Read the end element
			r.ReadEndElement();
		}

		#endregion
	}
}