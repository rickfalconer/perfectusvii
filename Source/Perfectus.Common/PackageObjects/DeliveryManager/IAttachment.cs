using System;
using System.IO;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IConverter.
	/// </summary>
    public interface IAttachment : IDocument
	{
        // Somewhere in the object tree is the original version of the 
        // attachment. This is its GUID
        Guid OriginalUniqueueIdentifier { get; }
	}
}
