using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    public class Delivery : PackageItem, ISerializable
    {
        List<IDocument> documents;
        Distributor distributor;
        Rule2 parentRule;

        [Browsable(false)]
        public Rule2 ParentRule
        { get { return parentRule; } }

        [Browsable(false)]
        public Distributor Distributor
        { get { return distributor; } }

        [Browsable(false)]
        public List<IDocument> Documents
        { get { return documents; } }


        public Delivery()
        {
            documents = new List<IDocument>();
            condition = new YesNoQueryValue();
            condition.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
            condition.YesNoValue = true;
        }

        public Delivery(Rule2 param_parent, String param_name)
            : this()
        {
            parentRule = param_parent;
            base.Name = param_name;

        }
        public Delivery(Rule2 param_parent, String param_name, IDocument param_document, Distributor param_distributor)
            : this(param_parent, param_name)
        {
            AddDocument(param_document);
            SetDistributor(param_distributor);
        }

        private YesNoQueryValue condition;

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking(25)]
        [DisplayDescription( "Perfectus.Common.PackageObjects.Delivery.Condition.Description" )]
        public YesNoQueryValue Condition
        {
            get { return condition; }
            set { condition = value; }
        }

        public void AddDocument(IDocument param_document)
        {
            param_document.ParentDelivery = this;
            documents.Add(param_document);
        }
        public void SetDistributor(Distributor param_distributor)
        {
            distributor = param_distributor;
        }
        public void RemoveDistributor()
        {
            distributor = null;
        }
        //
        internal void RemoveDocument(IDocument param_document)
        {
            documents.Remove(param_document);
        }
        internal void RemoveDocument(int param_index)
        {
            documents.RemoveAt(param_index);
        }

        public bool distributerFoundAndValid()
        {
            if (null == Distributor)
                return false;
            Server server = ParentRule.ParentServer;
            foreach (Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins.PluginDescriptor p in server.AvailablePlugins)
                if (p.UniqueIdentifier == Distributor.Descriptor.UniqueIdentifier)
                    return true;
            return false;
        }

        public override bool Valid(out string message)
        {
            message = default(String);
            bool valid = base.Valid(out message);
            StringBuilder sb = new StringBuilder(message);

            foreach (PackageItem document in Documents)
                if (!document.Valid(out message))
                {
                    sb.AppendLine(" " + message);
                    valid = false;
                }

            if (Distributor == null)
            {
                sb.AppendLine("Delivery has no distributor");
                valid = false;
            }
            else if (!distributerFoundAndValid())
            {
                sb.AppendLine(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Document.DistributorNotFound"), Distributor.Name));
                valid = false;
            }

            if (!valid)
            {
                sb.Insert(0, new StringBuilder().AppendLine(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Delivery.NotValid"), Name)).ToString());
                message = sb.ToString();
            }
            return valid;
        }

		#region ISerializable Members

        public Delivery(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            documents = (List<IDocument>)info.GetValue("documents", typeof(List<IDocument>));
            distributor = (Distributor)info.GetValue("distributor", typeof(Distributor));
            condition = (YesNoQueryValue)info.GetValue("condition", typeof(YesNoQueryValue));
            parentRule = (Rule2)info.GetValue("parentRule", typeof(Rule2));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("documents", documents);
            info.AddValue("distributor", distributor);
            info.AddValue("condition", this.condition);
            info.AddValue("parentRule", parentRule);
        }
		#endregion

    }
}
