using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for RuleItemEventArgs.
	/// </summary>
	public sealed class Rule2PluginEventArgs : EventArgs
	{
		private Rule2 r;

		private PluginBase plugin;

		public Rule2 Rule2
		{
			get { return r; }

		}

		public PluginBase Plugin
		{
			get { return plugin; }
		}

		public Rule2PluginEventArgs(Rule2 r, PluginBase plugin)
		{
			this.r = r;
			this.plugin = plugin;
		}
	}
}