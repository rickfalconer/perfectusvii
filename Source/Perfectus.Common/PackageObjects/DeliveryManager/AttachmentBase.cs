using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    abstract public class AttachmentBase : PackageItem, IAttachment, ISerializable, IPackageItemRequiresUniqueName
    {
        internal protected Guid originalUniqueueIdentifier;

        public AttachmentBase()
        {
            _condition = new YesNoQueryValue();
            _condition.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
            _condition.YesNoValue = true;

            originalUniqueueIdentifier = Guid.Empty;
        }

        public AttachmentBase(String param_name)
            : this()
        {
            base.Name = param_name;
        }


        public AttachmentBase(Delivery param_parentDelivery)
            : this()
        {
            parentDelivery = param_parentDelivery;
        }

        // This returns true if this object is the source attachment. A source attachment is an attachment object 
        // attached to the 'Attachments' folder, instead of a dellivery folder.
        [Browsable( false )]
        public bool AttachmentSource
        {
            get { return ( originalUniqueueIdentifier == Guid.Empty ); }
        }

        #region IDocument Members

        Delivery parentDelivery;
        [Browsable(false)]
        public Delivery ParentDelivery
        {
            get { return parentDelivery; }
            set { parentDelivery = value; }
        }

        private YesNoQueryValue _condition;
        private TextQuestionQueryValue _fileName;

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking(2)]
        [DisplayName( "Perfectus.Common.PackageObjects.PropertyNames.PublishedFileName" )]
        public TextQuestionQueryValue FileName
        {
            get
            {
                if ((_fileName == null ||
                    _fileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.NoValue))
                {
                    // return the document name, if the template has no PFN
                    _fileName = new TextQuestionQueryValue();
                    _fileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    _fileName.TextValue = this.Name;
                }
                return _fileName;
            }
            set { if (_fileName == null)_fileName = new TextQuestionQueryValue(); _fileName = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking(25)]
        [DisplayDescription( "Perfectus.Common.PackageObjects.Document.Condition.Description" )]
        public YesNoQueryValue Condition
        {
            get { return _condition; }
            set { _condition = value; }
        }

        #endregion

        #region LocalisedPropertiesObject Members
        public override void AlterProperties( PropertyDescriptorCollection properties )
        {
            // As this is attached to the attachment source folder, we change the properties slightly.
            for( int i = properties.Count - 1; i >= 0; i-- )
            {
                // Remove the Condition property
                if( AttachmentSource && properties[ i ].Name == "Condition" )
                {
                    properties.RemoveAt( i );
                    continue;
                }

                if( properties[ i ].Name == "FileName" )
                {
                    // File Name is read only.
                    if( AttachmentSource )
                        ( (LocalisedPropertyDescriptor)properties[ i ] ).ReadOnly = true;

                    if( AttachmentSource )
                    {
                        ( (LocalisedPropertyDescriptor)properties[ i ] ).SetDisplayNameKey( "Perfectus.Common.PackageObjects.AttachmentBase.Source.FileName.DisplayName" );
                        ( (LocalisedPropertyDescriptor)properties[ i ] ).SetDescriptionKey( "Perfectus.Common.PackageObjects.AttachmentBase.Source.FileName.Description" );
                    }
                    else
                    {
                        ( (LocalisedPropertyDescriptor)properties[ i ] ).SetDescriptionKey( "Perfectus.Common.PackageObjects.AttachmentBase.Delivery.FileName.Description" );
                    }
                }
            }
        }
        #endregion


        #region ISerializable Members

        public AttachmentBase(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            originalUniqueueIdentifier = (Guid)info.GetValue("originalUniqueueIdentifier", typeof(Guid));
            try
            {
                _condition = (YesNoQueryValue)info.GetValue("condition", typeof(YesNoQueryValue));
            }
            catch
            {
                _condition = new YesNoQueryValue();
                _condition.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
                _condition.YesNoValue = true;
            }
            _fileName = (TextQuestionQueryValue)info.GetValue("fileName", typeof(TextQuestionQueryValue));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("condition", _condition);
            info.AddValue("originalUniqueueIdentifier", originalUniqueueIdentifier);
            info.AddValue("fileName", _fileName);
        }

        #endregion

        #region IAttachment Members

        [Browsable(false)]
        public Guid OriginalUniqueueIdentifier
        {
            get { return originalUniqueueIdentifier; }
        }

        #endregion

        public override object Clone()
        {
            AttachmentBase o = (AttachmentBase)base.Clone();
            o.FileName = this.FileName.Clone() as TextQuestionQueryValue;
            o.originalUniqueueIdentifier = UniqueIdentifier;
            return o;
        }

        #region IPackageItemRequiresUniqueName Members
        [Browsable(false)]
        public bool RequiresUniqueName
        {
            // If we don't have the Guid to the original attachment, then
            // this is the master copy. The master must have a unique name in the tree
            get { return originalUniqueueIdentifier == Guid.Empty; }
        }
        #endregion
    }


    /// <summary>
    /// Class that contains a list of AttachmentBase objects. Its sole purpose is to 
    /// define a type converter to overide the display properties, as we wish to hide the 
    /// properties of the generic List<> class.
    /// </summary>
    [Serializable]
    [TypeConverter(typeof(AttachmentListConverter))]
    public class AttachmentCollection : List<AttachmentBase>
    {
        public AttachmentCollection( )
            : base( )
        { }

        public AttachmentCollection( int capacity )
            : base( capacity )
        { }
    }

    /// <summary>
    /// This class defines the type converter for the class AttachmentCollection. We created this class to override
    /// the description of the properties.
    /// </summary>
    public class AttachmentListConverter : TypeConverter
    {
        public override bool GetPropertiesSupported( ITypeDescriptorContext context )
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties( ITypeDescriptorContext context, object value, Attribute[ ] attributes )
        {
            // At the moment we don't want this class to display any properties.
            return null;
        }
    }
}
