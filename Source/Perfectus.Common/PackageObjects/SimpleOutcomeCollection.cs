using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type SimpleOutcome
	/// </summary>
	[Serializable]
	public class SimpleOutcomeCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the SimpleOutcomeCollection class.
		/// </summary>
		public SimpleOutcomeCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the SimpleOutcomeCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new SimpleOutcomeCollection.
		/// </param>
		public SimpleOutcomeCollection(SimpleOutcome[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the SimpleOutcomeCollection class, containing elements
		/// copied from another instance of SimpleOutcomeCollection
		/// </summary>
		/// <param name="items">
		/// The SimpleOutcomeCollection whose elements are to be added to the new SimpleOutcomeCollection.
		/// </param>
		public SimpleOutcomeCollection(SimpleOutcomeCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this SimpleOutcomeCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this SimpleOutcomeCollection.
		/// </param>
		public virtual void AddRange(SimpleOutcome[] items)
		{
			foreach (SimpleOutcome item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another SimpleOutcomeCollection to the end of this SimpleOutcomeCollection.
		/// </summary>
		/// <param name="items">
		/// The SimpleOutcomeCollection whose elements are to be added to the end of this SimpleOutcomeCollection.
		/// </param>
		public virtual void AddRange(SimpleOutcomeCollection items)
		{
			foreach (SimpleOutcome item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type SimpleOutcome to the end of this SimpleOutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcome to be added to the end of this SimpleOutcomeCollection.
		/// </param>
		public virtual void Add(SimpleOutcome value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic SimpleOutcome value is in this SimpleOutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcome value to locate in this SimpleOutcomeCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this SimpleOutcomeCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(SimpleOutcome value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this SimpleOutcomeCollection
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcome value to locate in the SimpleOutcomeCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(SimpleOutcome value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the SimpleOutcomeCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the SimpleOutcome is to be inserted.
		/// </param>
		/// <param name="value">
		/// The SimpleOutcome to insert.
		/// </param>
		public virtual void Insert(int index, SimpleOutcome value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the SimpleOutcome at the given index in this SimpleOutcomeCollection.
		/// </summary>
		public virtual SimpleOutcome this[int index]
		{
			get { return (SimpleOutcome) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific SimpleOutcome from this SimpleOutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcome value to remove from this SimpleOutcomeCollection.
		/// </param>
		public virtual void Remove(SimpleOutcome value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by SimpleOutcomeCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(SimpleOutcomeCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public SimpleOutcome Current
			{
				get { return (SimpleOutcome) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (SimpleOutcome) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this SimpleOutcomeCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}