using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	public enum DateFunctionMethod
	{
		DayDiff,
		DayAdd,
		MonthDiff,
		MonthAdd,
		YearDiff,
		YearAdd,
		Now
	} ;

	/// <summary>
	/// Summary description for DatePFunction.
	/// </summary>
	[Serializable]
	public sealed class DatePFunction : PFunction, ISerializable, IPackageItemContainer
	{
		#region Private Member Variables

		private DateExpression definition;

		#endregion
		#region Properties & Constructor

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.DatePFunctionEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FunctionTypeConverter, Studio")]
        [DisplayDescriptionAttribute("Perfectus.Common.PackageObjects.PropertyDescriptions.FunctionDefinition")]
        [DisplayRanking( 2 )]
        public DateExpression Definition
		{
			get { return definition; }
			set
			{
				definition = value;
			}
		}

		[Browsable(true)]
		[ReadOnly(true)]
		[Category("Data")]
        public override PerfectusDataType DataType
		{
			get { return DiscoverDataType(); }
			set 
			{
				// don't allow setting of data types for an date function
				if (value.ToString() == "")
					throw new NotSupportedException();
			}
		}

		public DatePFunction()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.NewItemName");
		}

		#endregion
		#region Methods

        public override Boolean IsValid(ref String message)
        {
            if (Definition == null)
            {
                message = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DateFunction.NoDefinition");
                return false;
            }
            return true;
        }

		/// <summary>
		///		Gets any depenendent library item objects of this DatePFunction and returns
		///		a reference to them in a LibraryItem list. Dependent objects for a DatePFunction
		///		only include Question objects and not package constants.
		/// </summary>
		/// <returns>A LibraryItem list containing any dependent LibraryItem objects.</returns>
        public override List<LibraryItem> GetDependentLibraryItems()
		{
            List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();

			// Add dependent parameter 1 if it is a question object
			if (this.definition != null)
			{
				if (this.definition.Parameter1 is Question)
				{
                    if ((this.definition.Parameter1.RelativePath == null || this.definition.Parameter1.RelativePath == string.Empty) &&
                        !dependentLibraryItems.Contains((LibraryItem) this.definition.Parameter1))
						dependentLibraryItems.Add((Question) this.definition.Parameter1);
				}
				else if (this.definition.Parameter1 is DatePFunction)
				{
                    if ((this.definition.Parameter1.RelativePath == null || this.definition.Parameter1.RelativePath == string.Empty) &&
                        !dependentLibraryItems.Contains((LibraryItem) this.definition.Parameter1))
						dependentLibraryItems.Add((DatePFunction) this.definition.Parameter1);
				}

				// Add dependent parameter 2 if it is a question object 
				if (this.definition.Parameter2 is Question)
				{
                    if ((this.definition.Parameter2.RelativePath == null || this.definition.Parameter2.RelativePath == string.Empty) &&
                        !dependentLibraryItems.Contains((LibraryItem) this.definition.Parameter2))
						dependentLibraryItems.Add((Question) this.definition.Parameter2);
				}
				else if (this.definition.Parameter2 is DatePFunction)
				{
                    if ((this.definition.Parameter2.RelativePath == null || this.definition.Parameter2.RelativePath == string.Empty) &&
                        !dependentLibraryItems.Contains((LibraryItem) this.definition.Parameter2))
						dependentLibraryItems.Add((DatePFunction) this.definition.Parameter2);
				}
			}

			return dependentLibraryItems;
		}

        public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

		public override bool ContainsReferenceTo(PackageItem pi)
		{
			bool parm1;
			bool parm2;
			if (definition == null)
			{
				return false;
			}

			if (definition.Parameter1 is PFunction)
			{
				if (((PFunction) definition.Parameter1) == pi)
				{
					return true;
				}

				parm1 = ((PFunction) definition.Parameter1).ContainsReferenceTo(pi);
				if (parm1)
				{
					return true;
				}
			}
			if (definition.Parameter2 is PFunction)
			{
				if (((PFunction) definition.Parameter2) == pi)
				{
					return true;
				}

				parm2 = ((PFunction) definition.Parameter2).ContainsReferenceTo(pi);
				if (parm2)
				{
					return true;
				}
			}
			if (definition.Parameter1 is Question)
			{
				if (((Question) definition.Parameter1) == pi)
				{
					return true;
				}			
			}
			if (definition.Parameter2 is Question)
			{
				if (((Question) definition.Parameter2) == pi)
				{
					return true;
				}				
			}
			return false;
		}

		public override object GetSingleAnswer(Caller caller)
		{
			if (definition != null)
			{
				return definition.Evaluate(caller);
			}
			else
			{
				return null;
			}

		}


		private PerfectusDataType DiscoverDataType()
		{
			if (definition != null)
			{
				// DayDiff, MonthDiff & YearDiff are considered number type, unless the parmeters are null (SL possible)
				if ((definition.Method == DateFunctionMethod.DayDiff ||
					definition.Method == DateFunctionMethod.MonthDiff ||
					definition.Method == DateFunctionMethod.YearDiff) && 
					definition.Parameter1 != null &&
					definition.Parameter2 != null)
				{
					return PerfectusDataType.Number;
				}
				else
				{
					return PerfectusDataType.Date;
				}
			}
			else
			{
				return PerfectusDataType.Date;
			}
		}

		public override object Clone()
		{
			object newObj = this.MemberwiseClone();
			DatePFunction dpf = (DatePFunction)newObj;
			if (dpf.definition != null)
			{
				dpf.definition = (DateExpression)(definition.Clone());
			}
			return dpf;
		}

		#endregion
		#region ISerializable Members

		public DatePFunction(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			definition = (DateExpression) info.GetValue("definition", typeof (DateExpression));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("definition", definition);
		}

		#endregion
		#region Shared Library

		public void Update(DatePFunction datePFunction)
		{
            if (this.Name != datePFunction.Name)
            {
                this.Name = datePFunction.Name;
            }
            
			// Shared Library Item Properties
			this.LibraryUniqueIdentifier = datePFunction.LibraryUniqueIdentifier;	
			this.Version = datePFunction.Version;
			this.LibraryType = datePFunction.LibraryType;
			this.Linked = datePFunction.Linked;
			this.CreatedBy = datePFunction.CreatedBy;		
			this.CreatedDateTime = datePFunction.CreatedDateTime;
			this.CheckedOutBy = datePFunction.CheckedOutBy;
			this.CheckedOutByLogInName = datePFunction.CheckedOutByLogInName;
			this.CheckedOutDateTime = datePFunction.CheckedOutDateTime;
			this.CheckedOutStatus = datePFunction.CheckedOutStatus;
			this.ModifiedBy = datePFunction.ModifiedBy;
			this.ModifiedDateTime = datePFunction.ModifiedDateTime;
			this.CheckInComment = datePFunction.CheckInComment;
			this.LocationPath = datePFunction.LocationPath;
			this.RelativePath = datePFunction.RelativePath;
			this.Filename = datePFunction.Filename;
			this.Touch = datePFunction.Touch;
			this.Notes = datePFunction.Notes;
			this.FormatString = datePFunction.FormatString;
			this.FormatStringParams = datePFunction.FormatStringParams;
			this.DataType = datePFunction.DataType;
			this.DataTypeString = datePFunction.DataTypeString;
            this.DependentLibraryItems = datePFunction.DependentLibraryItems;
            this.InterviewFormatString = datePFunction.InterviewFormatString;
            this.InterviewFormatStringParams = datePFunction.InterviewFormatStringParams;
            this.PreviewHoverText = datePFunction.PreviewHoverText;

			this.Definition = new DateExpression();
			
			if (datePFunction.Definition != null)
			{
				this.Definition.Method = datePFunction.Definition.Method;

				// Reconnect the dependent objects
				ReconnectDependentObjects(datePFunction);
			}
		}

		private void ReconnectDependentObjects(DatePFunction datePFunction)
		{
			string type1, uniqueIdentifier1, name1, dataType1, value1;
			string type2, uniqueIdentifier2, name2, dataType2, value2;
			
			name1 = datePFunction.Definition.Parameter1Name;
			type1 = datePFunction.Definition.Parameter1Type;
			uniqueIdentifier1 = datePFunction.Definition.Parameter1UniqueIdentifier;
			dataType1 = datePFunction.Definition.Parameter1DataType;
			value1 = datePFunction.Definition.Parameter1Value;

            if (type1 != null && uniqueIdentifier1 != null) {
                this.Definition.Parameter1 = FindAnswerProviderReferencedObject(type1, uniqueIdentifier1, name1, dataType1, value1);
            }

			name2 = datePFunction.Definition.Parameter2Name;
			type2 = datePFunction.Definition.Parameter2Type;
			uniqueIdentifier2 = datePFunction.Definition.Parameter2UniqueIdentifier;
			dataType2 = datePFunction.Definition.Parameter2DataType;
			value2 = datePFunction.Definition.Parameter2Value;

            if (type2 != null && uniqueIdentifier2 != null) {
                this.Definition.Parameter2 = FindAnswerProviderReferencedObject(type2, uniqueIdentifier2, name2, dataType2, value2);
            }
		}

		private AnswerProvider FindAnswerProviderReferencedObject(string type, string uniqueIdentifier, string name, string dataType, string dataValue)
		{
			LibraryItem libraryItem;
			AnswerProvider answerProvider = null;

			// Handle the different answer provider types
			if (typeof(Question).ToString() == type)
			{
				libraryItem = new Question();
				libraryItem.LibraryUniqueIdentifier = new Guid(uniqueIdentifier);
				answerProvider = (AnswerProvider)this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
			}
			else if (typeof(DatePFunction).ToString() == type)
			{
				libraryItem = new DatePFunction();
				libraryItem.LibraryUniqueIdentifier = new Guid(uniqueIdentifier);
				answerProvider = (AnswerProvider)this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
			}
			else if (typeof(ArithmeticPFunction).ToString() == type)
			{
				libraryItem = new ArithmeticPFunction();
				libraryItem.LibraryUniqueIdentifier = new Guid(uniqueIdentifier);
				answerProvider = (AnswerProvider)this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
			}
			else if (typeof(PackageConstant).ToString() == type)
			{
				if (dataType == PerfectusDataType.Date.ToString())
				{
					if (dataValue == "DateTime.Now")
						answerProvider = (AnswerProvider)new PackageConstant(PerfectusDataType.Date, dataValue);
					else
						answerProvider = (AnswerProvider)new PackageConstant(PerfectusDataType.Date, DateTime.Parse(dataValue));
				}
				else if (dataType == PerfectusDataType.Number.ToString())
				{
					answerProvider = (AnswerProvider)new PackageConstant(PerfectusDataType.Number, dataValue);
				}
				else
				{
					answerProvider = (AnswerProvider)new PackageConstant(PerfectusDataType.Text, dataValue);
				}
			}

			if (answerProvider == null && type != typeof(PackageConstant).ToString())
			{
				Exception ex = new Exception(name + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage")); //TODO - SL - RESOURCE
				ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				throw new NullReferenceException(name);
			}
		
			return answerProvider;
		}

		#endregion
	}
}
