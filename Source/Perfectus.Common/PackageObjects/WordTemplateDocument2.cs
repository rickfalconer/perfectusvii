using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using System.Drawing.Design;
using Perfectus.Common.SharedLibrary;
using Microsoft.Win32;

using System.IO.Packaging;
//using DocumentFormat.OpenXml.Packaging;
//using OpenXmlWordProcessing = DocumentFormat.OpenXml.Wordprocessing;
//using System.Xml.XPath;
using System.Linq;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for WordTemplateDocument2.
	/// </summary>
	[Serializable]
	public sealed class WordTemplateDocument2 : TemplateDocument, IDesignable, IDisposable, ISerializable
	{
        // Office Word Error Codes.
        [NonSerialized]
        public const int FILE_IN_USE_SAVE_ERROR = -2146823135;

        public const string CUSTOM_PROP_NS = "http://schemas.openxmlformats.org/officeDocument/2006/custom-properties";
        public const string CUSTOM_RELS_NS = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties";
        public const string PACKAGE_NS = "http://schemas.microsoft.com/office/2006/xmlPackage";
        public const string VTYPES_NS = "http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes";
        public const string EXT_PROPS_NS = "http://schemas.openxmlformats.org/officeDocument/2006/extended-properties";
        public const string REL_NS = "http://schemas.openxmlformats.org/package/2006/relationships";
        public const string CP_NS = "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
        public const string DC_NS = "http://purl.org/dc/elements/1.1/";
        public const string DCTERMS_NS = "http://purl.org/dc/terms/";
        public const string DCMITYPE_NS = "http://purl.org/dc/dcmitype/";
        public const string XSI_NS = "http://www.w3.org/2001/XMLSchema-instance";

		#region Private Member Variables

		[NonSerialized] private string filePath = null;
		[NonSerialized] private bool officeOpenXMLBytesAccessible;
		[NonSerialized] private byte[] officeOpenXMLBytes;
        [NonSerialized] private bool wordMlBytesPresent;
        [NonSerialized] private string wordMLToString;

        private WordMLDependentPackageItems wordMLDependentPackageItems;
		private WordDocumentPropertyCollection documentProperties = new WordDocumentPropertyCollection();

		// Defines the Shared Library type for this class
		private LibraryItemType _LibraryItemType = LibraryItemType.Template;

		#endregion
		#region Properties & Constructor
        
		[Category("SharedLibrary")]
		[Browsable(false)]
		public override LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

        [Browsable(false)]
        [XmlIgnore]
        public bool WordMlBytesPresent
        {
            get { return wordMlBytesPresent; }
        }

		[Browsable(false)]
		[XmlIgnore]
		public string FilePath
		{
			get { return filePath; }
            
            #if DEBUG
                // Used for unit testing only
                set { filePath = value; }
            #endif
		}

        [Browsable(false)]
        public string OfficeOpenXMLToString
        {
            get
            {
                if (this.officeOpenXMLBytesAccessible)
                {
                    return Encoding.UTF8.GetString(officeOpenXMLBytes);
                }
                else
                {
                    byte[] wordMLBytes = null;
                    wordMLBytes = DiscToBytes();

                    if (wordMLBytes != null)
                        //pf-3020
                        //return Encoding.UTF8.GetString(officeOpenXMLBytes);
                        return Encoding.UTF8.GetString(wordMLBytes);
                    else
                        return string.Empty;
                }
            }
            set
            {
                this.officeOpenXMLBytesAccessible = true;
                officeOpenXMLBytes = Encoding.UTF8.GetBytes(CleanWordml(value));
            }
        }

        [Browsable(false)]
        public WordMLDependentPackageItems WordMLDependentLibraryItems
        {
            get { return wordMLDependentPackageItems; }
            set { wordMLDependentPackageItems = value; }
        }

		[Browsable(true)]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.WordDocumentPropertyCollectionConverter, Studio")]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.WordDocumentPropertyCollectionEditor, Studio", typeof(UITypeEditor))]
        [DisplayRanking( 4 )]
        public WordDocumentPropertyCollection DocumentProperties
		{
			get 
			{
				return documentProperties; 
			}
			set 
			{ 
				documentProperties = value; 
			}
		}

		[Browsable(false)]
		public bool FileHasContent
		{
			get
			{
				if( filePath != null )
				{
                    FileInfo fi = new FileInfo( filePath );
                    if( fi.Exists )
                        return ( fi.Length > 0 );
				}
				return false;
			}
		}

		[Browsable(false)]
		[XmlIgnore]
		public byte[] OfficeOpenXMLBytes
		{
			get
			{
                if (officeOpenXMLBytes != null)
                {
                    return officeOpenXMLBytes;
                }
                else
                {
                    return DiscToBytes();
                }
			}
            set
            {
                officeOpenXMLBytes = value;
                officeOpenXMLBytesAccessible = true;
            }
		}

		public WordTemplateDocument2()
		{
			filePath = GenerateFilePath();
		}

		#endregion
		#region IDisposable Members

        /// <summary>
        /// Disposes of the file in the temp directory.
        /// </summary>
        /// <param name="disposing">Not currently used.</param>
        private void Dispose( bool disposing )
		{
            if( !String.IsNullOrEmpty( filePath ) )
            {
                File.Delete( filePath );
                filePath = null;
            }
		}

        /// <summary>
        /// Standard Dispose function. Prevents further calls to this function by suppressing calls to the destructor.
        /// </summary>
        public void Dispose( )
        {
            Dispose( true );
            System.GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Our object destructor. Primary used to remove the template document.
        /// </summary>
        ~WordTemplateDocument2( )
        {
            Dispose( false );
        }


		#endregion
		#region Methods

		private string GenerateFilePath( )
		{
            return Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) ) + ".xml";
		}

        public void GenerateNewFilePath( )
        {
			filePath = GenerateFilePath();
        }
        
        public void CreateBlankWordMLFile( )
		{
			//string blankWordML = @"<?xml version=""1.0"" encoding=""utf-8"" standalone=""yes""?><?mso-application progid=""Word.Document""?><w:wordDocument xmlns:w=""http://schemas.microsoft.com/office/word/2003/wordml"" xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:w10=""urn:schemas-microsoft-com:office:word"" xmlns:sl=""http://schemas.microsoft.com/schemaLibrary/2003/core"" xmlns:aml=""http://schemas.microsoft.com/aml/2001/core"" xmlns:wx=""http://schemas.microsoft.com/office/word/2003/auxHint"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:dt=""uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"" w:macrosPresent=""no"" w:embeddedObjPresent=""no"" w:ocxPresent=""no"" xml:space=""preserve""><w:body></w:body></w:wordDocument>";
			
			//wordMlBytes = Encoding.UTF8.GetBytes(blankWordML);
			//BytesToDisc(wordMlBytes);
		}

		new public object Clone()
		{
			return null;
		}

		public void Export(string path)
		{
			byte[] bytes = GetBytesWithoutPropsByAnswer();
		
            //pf-3065 stop writing plain old flat ooxml xml format, offer docx package too
            if (path.EndsWith(".xml"))
            {
                using (FileStream fs = new FileStream(path, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
            }
            else
            {
                if (!path.EndsWith(".docx"))
                {
                    path += ".docx";
                }
                // create docx package from flat xml
                MemoryStream msInput = new MemoryStream(bytes);
                XmlTextReader reader = new XmlTextReader(msInput);
                XDocument flatXmlDoc = XDocument.Load(reader);
                MemoryStream docxOutput = new MemoryStream();
                FlatToOpc(flatXmlDoc, ref docxOutput);

                docxOutput.Position = 0;
                byte[] outbytes = new byte[docxOutput.Length];
                docxOutput.Read(outbytes, 0, outbytes.Length);

                using (FileStream fs = File.OpenWrite(path))
                {
                    fs.Write(outbytes, 0, outbytes.Length);
                    fs.Close();
                }
            }
		}

		public static void AddWordMLTagToDataObject(DataObject inObj, PackageItem item, string tagName)
		{
			AddWordMLTagToDataObject(inObj, item, tagName, item.Name);
		}

		public static void AddWordMLTagToDataObject(DataObject inObj, PackageItem item, string tagName, string content)
		{
            string header = "Version:1.0\n\rStartHTML:aaaaaaaaaa\n\rEndHTML:bbbbbbbbbb\n\rStartFragment:cccccccccc\n\rEndFragment:dddddddddd\n\r";
            string start = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\" xmlns=\"http://www.w3.org/TR/REC-html40\"><body>";
            //string start = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas.openxmlformats.org/wordprocessingml/2006/main\"><body>";
            string uid = System.Guid.NewGuid().ToString();
            string guidWordFormat = string.Format("X_{0}", uid.ToUpper());

            string fragment = string.Empty;

            // pf-3037 guid shows in w15 content controls, pre w15 title was {0}, tag was {1}
            if (GetWordVersion() < 15)
            {
                fragment = string.Format("<w:Sdt Text=\"t\" Title=\"{0}\" SdtTag=\"{1}\" >{1}</w:Sdt>", item.UniqueIdentifier.ToString(), tagName);
            }
            else
            {
                // w15 shows the title, so switch em. Same logic in merge.xslt and packageupgrader.
                fragment = string.Format("<w:Sdt Text=\"t\" Title=\"{1}\" SdtTag=\"{0}\" >{1}</w:Sdt>", item.UniqueIdentifier.ToString(), tagName);
            }
            //string fragment = string.Format("<w:sdt><w:sdtPr><w:alias w:val=\"{1}\"/><w:tag w:val=\"{0}\"/></w:sdtPr><w:sdtContent><w:r><w:t>{1}</w:t></w:r></w:sdtContent></w:sdt>", item.UniqueIdentifier.ToString(), tagName);
            
            string end = "</BODY></HTML>";
            string output = header + start + fragment + end;
            
            output = output.Replace("aaaaaaaaaa", header.Length.ToString("d10"));
            output = output.Replace("bbbbbbbbbb", output.Length.ToString("d10"));
            output = output.Replace("cccccccccc", (header.Length + start.Length).ToString("d10"));
            output = output.Replace("dddddddddd", (header.Length + start.Length + fragment.Length).ToString("d10"));

            inObj.SetData(DataFormats.Html, output);
		}

        private static decimal GetWordVersion()
        {
            try
            {
                // get word version from interop, same technique as OOXML Upgrader
                //Microsoft.Office.Interop.Word.Application app;
                //app = new Microsoft.Office.Interop.Word.Application();
                decimal version = 15;

                //try
                //{
                //    version = decimal.Parse(app.Version);
                //}
                //finally
                //{
                //    if (app != null)
                //    {
                //        System.Object oMissing = System.Reflection.Missing.Value;
                //        app.Quit(ref oMissing, ref oMissing, ref oMissing);
                //        app = null;
                //    }
                //}

                //GC.Collect();
                //GC.WaitForPendingFinalizers();

                return version;
            }
            catch (Exception eIgnoreMe)
            {
                // PF-3322 word 2016 doesn't use the reg key
                return 16;
            }

        }

        public void SwitchSdtAliasTagWord2013()
        {
            XmlDocument xmlDoc = new XmlDocument();
            MemoryStream msTemplate = new MemoryStream(OfficeOpenXMLBytes);
            xmlDoc.Load(msTemplate);

            XmlNamespaceManager nsm = new XmlNamespaceManager(xmlDoc.NameTable);
            nsm.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            string matchSdtPr = "//w:sdt/w:sdtPr";
            XmlNodeList mySdtPrs = xmlDoc.SelectNodes(matchSdtPr, nsm);
            foreach (XmlNode mySdt in mySdtPrs)
            {
                if (mySdt != null)
                {
                    string title = mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value;
                    string tag = mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value;
                    if (IsGuid(title))
                    {
                        mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value = tag;
                        mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value = title;
                    }
                }
            }

            MemoryStream msOutput = new MemoryStream();
            xmlDoc.Save(msOutput);
            msOutput.Seek(0, SeekOrigin.Begin);
            msOutput.Position = 0;
            OfficeOpenXMLBytes = msOutput.ToArray();
            BytesToDisc(officeOpenXMLBytes);
        }

        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        private static bool IsGuid(string candidate)
        {
            if (candidate != null)
            {
                if (isGuid.IsMatch(candidate))
                {
                    return true;
                }
            }

            return false;
        }


        public static string GenerateTag(PackageItem item, bool includeNamespace)
		{
			if (! (item is ITemplateItem))
			{
				return null;
			}
			else
			{
				ITemplateItem iti = (ITemplateItem) item;
				string tag = null;
				if (includeNamespace)
				{
                    tag = string.Format( "<perfectus:{0} perfectus:uid='{2}' xmlns='http://www.perfectus.net/adhoc' >{1}</perfectus:{0}>", 
                            iti.GetTagName( ), 
                            System.Web.HttpUtility.HtmlEncode( iti.GetTagContents( ) ), 
                            item.UniqueIdentifier );
                }
				else
				{
                    tag = string.Format("<{0} uid='{2}' xmlns='http://www.perfectus.net/adhoc' >{1}</{0}>", 
                            iti.GetTagName(), 
                            System.Web.HttpUtility.HtmlEncode(iti.GetTagContents()), 
                            item.UniqueIdentifier);
				}
				return tag;
			}
		}

		private byte[] DiscToBytes()
		{
			if (filePath != null)
			{
				using (FileStream fs = File.OpenRead(filePath))
				{
					byte[] retVal = new byte[fs.Length];
					fs.Read(retVal, 0, retVal.Length);

					// Since the last time we got the wordML the document property collection might of been updated in memory, so update the wordML.
					retVal = GetBytesWithDocumentProperties(retVal);
					
					fs.Close();
					return retVal;
				}
			}
			else
			{
				return null;
			}
		}

		private void BytesToDisc(byte[] bytes)
		{
			if (filePath == null)
			{
				filePath = GenerateFilePath();
			}

			using (FileStream fs = new FileStream(filePath, FileMode.Create))
			{
				fs.Write(bytes, 0, bytes.Length);
				fs.Close();
			}

			ForceDocumentPropertiesUpdate();
		}

        private string CleanWordml(string officeOpenXML)
        {
            string rougeStartingChars = "???";

            if (officeOpenXML.StartsWith(rougeStartingChars))
            {
                return officeOpenXML.Substring(rougeStartingChars.Length);
            }
            else
            {
                return officeOpenXML;
            }
        }

		#endregion
		#region ISerializable Members

		public WordTemplateDocument2(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            try
            {
                documentProperties = (WordDocumentPropertyCollection)info.GetValue("documentProperties", typeof(WordDocumentPropertyCollection));
            }
            catch { }

            try
            {
                wordMLDependentPackageItems = (WordMLDependentPackageItems) info.GetValue("wordMLDependentLibraryItems", typeof(WordMLDependentPackageItems));
            }
            catch { }

			switch (context.State)
			{
				case StreamingContextStates.File:
					filePath = GenerateFilePath();
                    try
                    {
                        BytesToDisc((byte[])info.GetValue("officeOpenXML", typeof(byte[])));
                    }
                    catch
                    {
                    }

                    try
                    {
                        BytesToDisc((byte[])info.GetValue("wordMl", typeof(byte[])));
                        wordMlBytesPresent = true;
                    }
                    catch
                    {
                        wordMlBytesPresent = false;
                    }

                    officeOpenXMLBytesAccessible = false;

					break;
				case StreamingContextStates.CrossMachine:
                    officeOpenXMLBytesAccessible = true;

                    try
                    {
                        officeOpenXMLBytes = (byte[])info.GetValue("officeOpenXML", typeof(byte[]));
                    }
                    catch
                    {
                        officeOpenXMLBytes = null;
                    }

					filePath = null;
					break;
				case StreamingContextStates.Persistence:
                    officeOpenXMLBytesAccessible = false;
                    officeOpenXMLBytes = null;
					filePath = null;
					break;
				case StreamingContextStates.Remoting: // (Web service)
                case StreamingContextStates.Clone:
                    officeOpenXMLBytesAccessible = false;
					officeOpenXMLBytes = null;
					filePath = null;

                    try
                    {
                        OfficeOpenXMLToString = info.GetString("officeOpenXMLToString");
                    }
                    catch 
                    {
                        OfficeOpenXMLToString = null;
                    }


                    try
                    {
                        wordMLToString = info.GetString("wordMlToString");
                    }
                    catch
                    {
                        wordMLToString = null;
                    }

					break;
				default:
					throw new NotSupportedException("Only File (client), CrossMachine (server API) or Persistence (server data tier) StreamingContextStates are supported.");
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("documentProperties",documentProperties);
            info.AddValue("wordMLDependentLibraryItems", wordMLDependentPackageItems);

			switch (context.State)
			{
				case StreamingContextStates.File:
				case StreamingContextStates.CrossMachine:
                    info.AddValue("officeOpenXML", DiscToBytes());
					break;
				case StreamingContextStates.Persistence:
				case StreamingContextStates.All: // (All is passed by the MessageQueue Send method)
					// Don't serialise the wordml - the context should already have these extracted
					break;
				case StreamingContextStates.Remoting: // (Web Service)
                case StreamingContextStates.Clone:
                    info.AddValue("officeOpenXMLToString", OfficeOpenXMLToString);
					break;
				default:
					throw new NotSupportedException("Only File (client) or Persistence (server data tier) StreamingContextStates are supported.");
			}
		}

		#endregion
		#region Shared Library

		public void Update(WordTemplateDocument2 template)
		{
			// Shared Library Item Properties
			this.LibraryUniqueIdentifier = template.LibraryUniqueIdentifier;	
			this.Version = template.Version;
			this.LibraryType = template.LibraryType;
			this.Linked = template.Linked;
			this.CreatedBy = template.CreatedBy;		
			this.CreatedDateTime = template.CreatedDateTime;
			this.CheckedOutBy = template.CheckedOutBy;
			this.CheckedOutByLogInName = template.CheckedOutByLogInName;
			this.CheckedOutDateTime = template.CheckedOutDateTime;
			this.CheckedOutStatus = template.CheckedOutStatus;
			this.ModifiedBy = template.ModifiedBy;
			this.ModifiedDateTime = template.ModifiedDateTime;
			this.CheckInComment = template.CheckInComment;
			this.LocationPath = template.LocationPath;
			this.RelativePath = template.RelativePath;
			this.Filename = template.Filename;
			this.Name = template.Name;
			this.Touch = template.Touch;
			this.Notes = template.Notes;
			this.DocumentProperties = template.DocumentProperties;
            this.DependentLibraryItems = template.DependentLibraryItems;
            
			// Update the word ML and save the bytes to disc
			this.officeOpenXMLBytes = template.officeOpenXMLBytes;
			this.officeOpenXMLBytesAccessible = true;
			this.BytesToDisc(officeOpenXMLBytes);
		}

		public override List<LibraryItem> GetDependentLibraryItems()
		{
            List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();

            // Ensure the template designer is in a saved state
            if (this.ParentPackage != null)
            {
                this.ParentPackage.EnsureUIDesignerSaved(new PackageItemEventArgs(this));
            }

            officeOpenXMLBytesAccessible = false;

            // Set the depenendent wordml library items
            SetDependentItems(dependentLibraryItems);
  //          this.PublishedFileName.SetDependentLibraryItems(dependentLibraryItems, this.ParentPackage);
                                    
			return dependentLibraryItems;
		}

        /// <summary>
        ///     Gets a list of package items from the WordML and populates these into a LibraryItem list.
        /// </summary>
        /// <returns>A list of LibraryItems that are referenced in the WordML.</returns>
        public override List<LibraryItem> SetDependentLibraryItems()
		{
            return null;
        }

        private void SetDependentItems(List<LibraryItem> dependentLibraryItems)
        {
            Dictionary<string, Guid> orphanedItems;

            // Get the list of referenced package item guids in the WordML
            Dictionary<string, Guid> referencedItems = WordML.WordMLUtilities.GetDistinctPackageItemListFromWordMLPath(filePath);
            wordMLDependentPackageItems = ParentPackage.PopulateLibraryItems(referencedItems, out orphanedItems);

            // Populate the colleciton of depenendent items
            foreach (LibraryItem libraryItem in wordMLDependentPackageItems)
            {
                dependentLibraryItems.Add(libraryItem);
            }
            
            //TODO - orphanedItems - need to decide what to do here to handle orphaned items correctly
            if (orphanedItems != null && orphanedItems.Count > 0)
            {
                //throw new System.InvalidOperationException("Orphaned Referenced Package Item Exception");
            }
		}

        /// <summary>
        /// Converts flat word OOXML to a docx package.
        /// Originally used in Server ProcessCoordinator agent, moved here to share with Client for Export Template.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="docxOutput"></param>
        public void FlatToOpc(XDocument doc, ref MemoryStream docxOutput)
        {
            XNamespace pkg =
                "http://schemas.microsoft.com/office/2006/xmlPackage";
            XNamespace rel =
                "http://schemas.openxmlformats.org/package/2006/relationships";

            using (System.IO.Packaging.Package package = System.IO.Packaging.Package.Open(docxOutput, FileMode.Create))
            {
                // add all parts (but not relationships)
                foreach (var xmlPart in doc.Root
                    .Elements()
                    .Where(p =>
                        (string)p.Attribute(pkg + "contentType") !=
                        "application/vnd.openxmlformats-package.relationships+xml"))
                {
                    string name = (string)xmlPart.Attribute(pkg + "name");
                    string contentType = (string)xmlPart.Attribute(pkg + "contentType");
                    if (name != null)
                    {
                        if (contentType.EndsWith("xml"))
                        {
                            Uri u = new Uri(name, UriKind.Relative);
                            PackagePart part = package.CreatePart(u, contentType,
                                CompressionOption.SuperFast);
                            using (Stream str = part.GetStream(FileMode.Create))
                            using (XmlWriter xmlWriter = XmlWriter.Create(str))
                                xmlPart.Element(pkg + "xmlData")
                                    .Elements()
                                    .First()
                                    .WriteTo(xmlWriter);
                        }
                        else
                        {
                            Uri u = new Uri(name, UriKind.Relative);
                            PackagePart part = package.CreatePart(u, contentType,
                                CompressionOption.SuperFast);
                            using (Stream str = part.GetStream(FileMode.Create))
                            using (BinaryWriter binaryWriter = new BinaryWriter(str))
                            {
                                string base64StringInChunks =
                                    (string)xmlPart.Element(pkg + "binaryData");
                                char[] base64CharArray = base64StringInChunks
                                    .Where(c => c != '\r' && c != '\n').ToArray();
                                byte[] byteArray =
                                    System.Convert.FromBase64CharArray(base64CharArray,
                                    0, base64CharArray.Length);
                                binaryWriter.Write(byteArray);
                            }
                        }
                    }
                }

                foreach (var xmlPart in doc.Root.Elements())
                {
                    string name = (string)xmlPart.Attribute(pkg + "name");
                    string contentType = (string)xmlPart.Attribute(pkg + "contentType");
                    if (contentType ==
                        "application/vnd.openxmlformats-package.relationships+xml")
                    {
                        // add the package level relationships
                        if (name == "/_rels/.rels")
                        {
                            foreach (XElement xmlRel in
                                xmlPart.Descendants(rel + "Relationship"))
                            {
                                string id = (string)xmlRel.Attribute("Id");
                                string type = (string)xmlRel.Attribute("Type");
                                string target = (string)xmlRel.Attribute("Target");
                                string targetMode =
                                    (string)xmlRel.Attribute("TargetMode");
                                if (targetMode == "External")
                                    package.CreateRelationship(
                                        new Uri(target, UriKind.Absolute),
                                        TargetMode.External, type, id);
                                else
                                    package.CreateRelationship(
                                        new Uri(target, UriKind.Relative),
                                        TargetMode.Internal, type, id);
                            }
                        }
                        else
                        // add part level relationships
                        {
                            string directory = name.Substring(0, name.IndexOf("/_rels"));
                            string relsFilename = name.Substring(name.LastIndexOf('/'));
                            string filename =
                                relsFilename.Substring(0, relsFilename.IndexOf(".rels"));
                            PackagePart fromPart = package.GetPart(
                                new Uri(directory + filename, UriKind.Relative));
                            foreach (XElement xmlRel in
                                xmlPart.Descendants(rel + "Relationship"))
                            {
                                string id = (string)xmlRel.Attribute("Id");
                                string type = (string)xmlRel.Attribute("Type");
                                string target = (string)xmlRel.Attribute("Target");
                                string targetMode =
                                    (string)xmlRel.Attribute("TargetMode");
                                if (targetMode == "External")
                                    fromPart.CreateRelationship(
                                        new Uri(target, UriKind.Absolute),
                                        TargetMode.External, type, id);
                                else
                                    fromPart.CreateRelationship(
                                        new Uri(target, UriKind.Relative),
                                        TargetMode.Internal, type, id);
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// pf-2944 
        /// XSLT did not copy the namespaces needed by w:numbering, as they already existed in the scope.
        /// Mx Word needs the w:numbering namespaces, so graft them back in.
        /// </summary>
        /// <param name="msDoc"></param>
        /// <param name="docxOutput"></param>
        public void PatchNumberingNamespace(MemoryStream msDoc, ref MemoryStream docxOutput)
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(msDoc);

            XmlNamespaceManager nsmgr1 = new XmlNamespaceManager(new NameTable());
            nsmgr1.AddNamespace("pkg", "http://schemas.microsoft.com/office/2006/xmlPackage");
            nsmgr1.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            XmlElement elNumbering = (XmlElement)doc.SelectSingleNode("/pkg:package/pkg:part[@pkg:name='/word/numbering.xml']/pkg:xmlData/w:numbering", nsmgr1);
            if (elNumbering != null)
            {
                elNumbering.SetAttribute("xmlns:wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
                elNumbering.SetAttribute("xmlns:mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
                elNumbering.SetAttribute("xmlns:o", "urn:schemas-microsoft-com:office:office");
                elNumbering.SetAttribute("xmlns:r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
                elNumbering.SetAttribute("xmlns:m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
                elNumbering.SetAttribute("xmlns:v", "urn:schemas-microsoft-com:vml");
                elNumbering.SetAttribute("xmlns:wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
                elNumbering.SetAttribute("xmlns:wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
                elNumbering.SetAttribute("xmlns:w10", "urn:schemas-microsoft-com:office:word");
                elNumbering.SetAttribute("xmlns:w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
                elNumbering.SetAttribute("xmlns:w14", "http://schemas.microsoft.com/office/word/2010/wordml");
                elNumbering.SetAttribute("xmlns:wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
                elNumbering.SetAttribute("xmlns:wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
                elNumbering.SetAttribute("xmlns:wne", "http://schemas.microsoft.com/office/word/2006/wordml");
                elNumbering.SetAttribute("xmlns:wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");
            }
            docxOutput = new MemoryStream();
            doc.Save(docxOutput);
        }


		#endregion		
		#region Word Document Properties
		private byte[] GetBytesWithDocumentProperties(byte[] bytes)
		{
            XmlDocument doc = new XmlDocument();
            MemoryStream ms = new MemoryStream(bytes);
            doc.Load(ms);
            ms.Close();

            XmlNodeList partsList = doc.GetElementsByTagName("pkg:part");

            if (partsList.Count > 0)//check for OOXML (old deleted WordML templates may still be present)
            {
                XmlNodeList lstStandardCoreDocumentProperties = doc.GetElementsByTagName("cp:coreProperties");

                // Make sure any properties in the wordMl but not memory get into memory.
                UpdateStandardCoreMemoryDocProperties(doc, lstStandardCoreDocumentProperties);

                XmlNodeList lstStandardAppDocumentProperties = doc.GetElementsByTagName("Properties", EXT_PROPS_NS);

                if (lstStandardAppDocumentProperties != null && lstStandardAppDocumentProperties.Count > 0)//this is our standard app props
                {
                    UpdateStandardAppMemoryDocProperties(doc, lstStandardAppDocumentProperties);
                }

                // Make sure any custom properties in the wordMl but not memory get into memory.
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                nsmgr.AddNamespace("cp", CP_NS);
                nsmgr.AddNamespace("dc", DC_NS);
                nsmgr.AddNamespace("dcterms", DCTERMS_NS);
                nsmgr.AddNamespace("dcmitype", DCMITYPE_NS);
                nsmgr.AddNamespace("xsi", XSI_NS);
                nsmgr.AddNamespace("pkg", PACKAGE_NS);
                nsmgr.AddNamespace("vt", VTYPES_NS);
                nsmgr.AddNamespace("rel", REL_NS);
                nsmgr.AddNamespace("pro", CUSTOM_PROP_NS);

                XmlNodeList lstCustomDocumentProperties = doc.GetElementsByTagName("Properties", CUSTOM_PROP_NS);

                if (lstCustomDocumentProperties != null && lstCustomDocumentProperties.Count > 0)//this is our custom props
                {
                    UpdateCustomMemoryDocProperties(doc, lstCustomDocumentProperties);
                }

                // Make sure any properties in memory but not wordMl get into the wordMl. Any values in the WordMl already will be updated by what is in memory.
                UpdateStandardCoreWordMlDocProperties(doc, lstStandardCoreDocumentProperties);

                UpdateStandardAppWordMlDocProperties(doc, lstStandardAppDocumentProperties);

                //If we have any custom properties, use them, if not create the part + ref from scratch, so that it can be added to
                if (lstCustomDocumentProperties != null && lstCustomDocumentProperties.Count == 0)
                {
                    //create the reference and the part
                    XmlElement customPartNode = doc.CreateElement("pkg:part", PACKAGE_NS);

                    customPartNode.SetAttribute("name", PACKAGE_NS, "/docProps/custom.xml");
                    XmlAttribute contentTypeAttr = (XmlAttribute)doc.CreateNode(XmlNodeType.Attribute, "pkg:contentType", PACKAGE_NS);
                    contentTypeAttr.Value = "application/vnd.openxmlformats-officedocument.custom-properties+xml";

                    customPartNode.Attributes.Append(contentTypeAttr);

                    XmlNode customXmlDataNode = doc.CreateElement("pkg:xmlData", PACKAGE_NS);

                    XmlElement propertiesNode = doc.CreateElement("Properties", CUSTOM_PROP_NS);
                    propertiesNode.SetAttribute("xmlns:vt", VTYPES_NS);

                    customPartNode.AppendChild(customXmlDataNode);
                    customXmlDataNode.AppendChild(propertiesNode);

                    //add new custom props part to package
                    XmlNode packageNode = doc.SelectSingleNode("//pkg:package", nsmgr);
                    packageNode.AppendChild(customPartNode);

                    XmlNode relationshipsNode = doc.SelectSingleNode("//rel:Relationships[ancestor::pkg:part[@pkg:name='/_rels/.rels']]", nsmgr);
                    int refCount = relationshipsNode.ChildNodes.Count + 1;

                    XmlElement customPropsRelationshipNode = doc.CreateElement("Relationship", REL_NS);
                    customPropsRelationshipNode.SetAttribute("Id", string.Format("rId{0}", refCount.ToString()));
                    customPropsRelationshipNode.SetAttribute("Type", CUSTOM_RELS_NS);
                    customPropsRelationshipNode.SetAttribute("Target", "docProps/custom.xml");
                    relationshipsNode.AppendChild(customPropsRelationshipNode);

                    lstCustomDocumentProperties = doc.GetElementsByTagName("Properties", CUSTOM_PROP_NS);
                }

                UpdateCustomWordMlDocProperties(doc, lstCustomDocumentProperties);

                MemoryStream ms2 = new MemoryStream();
                doc.Save(ms2);
                byte[] retVal = ms2.ToArray();
                ms2.Close();

                return retVal;
            }
            else
            {
                return bytes;
            }
		}

		private void UpdateStandardCoreMemoryDocProperties(XmlDocument doc, XmlNodeList list)
		{
			for(int i=0;i<list[0].ChildNodes.Count;i++)
			{	
				if(list[0].ChildNodes[i] is XmlSignificantWhitespace)
				{
					return;
				}

				bool existsInCol = false;
				XmlNode docPropertyNode = list[0].ChildNodes[i];
                string nodeName = docPropertyNode.Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).ToLower().Trim();

				if(WordDocumentProperty.IsStandardValue(nodeName) != true)
				{
					continue;
				}			

				foreach(WordDocumentProperty dp in documentProperties)
				{					
					if(dp.DocumentPropertyName.ToLower().Trim() == nodeName)// remove the "o:" at the start.
					{
						existsInCol = true;
						break;
					}
                    else if (dp.DocumentPropertyName.ToLower().Trim() == "author" && nodeName == "creator")
                    {
                        existsInCol = true;
                        break;
                    }
				}

				// If something is in the wordml but doesn't exist in memory then update our document property collection.
				if(! existsInCol)
				{
					WordDocumentProperty newProp = new WordDocumentProperty();

                    string docNodeName = list[0].ChildNodes[i].Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).Trim();
                    if (docNodeName == "creator")//special case for creator, as of new xml format, there are different names used for this prop in markup
                    {
                        newProp.DocumentPropertyName = "author";
                    }
                    else 
                    {
                        newProp.DocumentPropertyName = docNodeName;
                    }
                    
					newProp.DocumentPropertyValue.TextValue = list[0].ChildNodes[i].InnerText.Trim();
					newProp.DocumentPropertyValue.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    newProp.PropertyType = WordDocumentProperty.WordDocumentPropertyType.Standard;

					documentProperties.Add(newProp);
				}
			}	
		}

        private void UpdateStandardAppMemoryDocProperties(XmlDocument doc, XmlNodeList list)
        {
            for (int i = 0; i < list[0].ChildNodes.Count; i++)
            {
                if (list[0].ChildNodes[i] is XmlSignificantWhitespace)
                {
                    return;
                }

                bool existsInCol = false;
                XmlNode docPropertyNode = list[0].ChildNodes[i];
                string nodeName = docPropertyNode.Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).ToLower().Trim();

                if (WordDocumentProperty.IsStandardValue(nodeName) != true)
                {
                    continue;
                }

                foreach (WordDocumentProperty dp in documentProperties)
                {
                    if (dp.DocumentPropertyName.ToLower().Trim() == nodeName)// remove the "o:" at the start.
                    {
                        existsInCol = true;
                        break;
                    }
                }

                // If something is in the wordml but doesn't exist in memory then update our document property collection.
                if (!existsInCol)
                {
                    WordDocumentProperty newProp = new WordDocumentProperty();

                    string docNodeName = list[0].ChildNodes[i].Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).Trim();
                    newProp.DocumentPropertyName = docNodeName;
                    newProp.DocumentPropertyValue.TextValue = list[0].ChildNodes[i].InnerText.Trim();
                    newProp.DocumentPropertyValue.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    newProp.PropertyType = WordDocumentProperty.WordDocumentPropertyType.Standard;

                    documentProperties.Add(newProp);
                }
            }
        }

        private void UpdateCustomMemoryDocProperties(XmlDocument doc, XmlNodeList list)
        {
            for (int i = 0; i < list[0].ChildNodes.Count; i++)
            {
                if (list[0].ChildNodes[i] is XmlSignificantWhitespace)
                {
                    return;
                }

                bool existsInCol = false;
                XmlNode docPropertyNode = list[0].ChildNodes[i];
                string nodeName = docPropertyNode.Attributes["name"].Value;

                foreach (WordDocumentProperty dp in documentProperties)
                {
                    if (dp.DocumentPropertyName.ToLower().Trim() == nodeName)
                    {
                        existsInCol = true;
                        break;
                    }
                }

                // If something is in the wordml but doesn't exist in memory then update our document property collection.
                if (!existsInCol)
                {
                    WordDocumentProperty newProp = new WordDocumentProperty();

                    string docNodeName = docPropertyNode.Attributes["name"].Value;
                    newProp.DocumentPropertyName = docNodeName;

                    newProp.DocumentPropertyValue.TextValue = docPropertyNode.FirstChild.InnerText;

                    //find type
                    newProp.DocumentPropertyValue.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    newProp.PropertyType = WordDocumentProperty.WordDocumentPropertyType.Custom;

                    string pType = list[0].ChildNodes[i].FirstChild.Name;
                    newProp.CustomDocumentPropertyType = WordDocumentProperty.WordAttributeTypeToEnum(pType);

                    documentProperties.Add(newProp);
                }
            }
        }

        private void UpdateStandardCoreWordMlDocProperties(XmlDocument doc, XmlNodeList list)
		{
			// If a property is in Memory but not in WordMl then update wordMl.
			foreach(WordDocumentProperty dp in documentProperties)
			{
                if (dp.PropertyType != WordDocumentProperty.WordDocumentPropertyType.Standard)
				{
					continue;
				}

				bool inWordML = false;               
			
				XmlNode docPropertyNode = null;

				if(list[0] != null)
				{
					for(int i=0;i<list[0].ChildNodes.Count;i++)
					{	
						docPropertyNode = list[0].ChildNodes[i];
                        if (dp.DocumentPropertyName.ToLower().Trim() == docPropertyNode.Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).ToLower().Trim())
						{
							inWordML = true;
							break;
						}
                        else if (dp.DocumentPropertyName.ToLower().Trim() == "author" && docPropertyNode.Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).ToLower().Trim() == "creator")//word has a different name in its UI than it does in its xml since word 2007		
                        {
                            inWordML = true;
                            break;
                        }
					}
				}

                if (!inWordML && !WordDocumentProperty.IsStandardAppValue(dp.DocumentPropertyName)) // Add document property to wordML from scratch, using what is in memory.
				{
                    string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
                    val = val.Replace("<text>", "");
                    val = val.Replace("</text>", "");
                    val = val.Replace("<item uId=\"", "answerTo");
                    val = val.Replace("\">", "");
                    val = val.Replace("</item>", "");

                    string namespaceString = WordDocumentProperty.GetStandardCorePropNamespace(dp.DocumentPropertyName);

                    XmlElement newPropertyNode = doc.CreateElement("cp", dp.DocumentPropertyName.ToLower(), namespaceString);
                    newPropertyNode.InnerText = val;
                    list[0].AppendChild(newPropertyNode);
				}
                else if (docPropertyNode != null && !WordDocumentProperty.IsStandardAppValue(dp.DocumentPropertyName)) // Update the document property in the WordML with what is stored in memory. 
				{											
					string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
					val= val.Replace("<text>","");
					val = val.Replace("</text>","");
					val = val.Replace("<item uId=\"","answerTo");
					val = val.Replace("\">","");
					val = val.Replace("</item>","");
					docPropertyNode.InnerText = val;
				}
			}
		}

        private void UpdateStandardAppWordMlDocProperties(XmlDocument doc, XmlNodeList list)
        {
            // If a property is in Memory but not in WordMl then update wordMl.
            foreach (WordDocumentProperty dp in documentProperties)
            {
                if (dp.PropertyType != WordDocumentProperty.WordDocumentPropertyType.Standard)
                {
                    continue;
                }

                bool inWordML = false;

                XmlNode docPropertyNode = null;

                if (list[0] != null)
                {
                    for (int i = 0; i < list[0].ChildNodes.Count; i++)
                    {
                        docPropertyNode = list[0].ChildNodes[i];
                        if (dp.DocumentPropertyName.ToLower().Trim() == docPropertyNode.Name.Remove(0, docPropertyNode.Name.IndexOf(":") + 1).ToLower().Trim())
                        {
                            inWordML = true;
                            break;
                        }
                    }
                }

                if (!inWordML && WordDocumentProperty.IsStandardAppValue(dp.DocumentPropertyName)) // Add document property to wordML from scratch, using what is in memory.
                {
                    string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
                    val = val.Replace("<text>", "");
                    val = val.Replace("</text>", "");
                    val = val.Replace("<item uId=\"", "answerTo");
                    val = val.Replace("\">", "");
                    val = val.Replace("</item>", "");

                    XmlElement newPropertyNode = doc.CreateElement(dp.DocumentPropertyName, EXT_PROPS_NS);
                    newPropertyNode.InnerText = val;
                    list[0].AppendChild(newPropertyNode);
                }
                else if (docPropertyNode != null && WordDocumentProperty.IsStandardAppValue(dp.DocumentPropertyName)) // Update the document property in the WordML with what is stored in memory. 
                {
                    string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
                    val = val.Replace("<text>", "");
                    val = val.Replace("</text>", "");
                    val = val.Replace("<item uId=\"", "answerTo");
                    val = val.Replace("\">", "");
                    val = val.Replace("</item>", "");
                    docPropertyNode.InnerText = val;
                }
            }
        }

        private void UpdateCustomWordMlDocProperties(XmlDocument doc, XmlNodeList list)
        {
            // If a property is in Memory but not in WordMl then update wordMl.
            foreach (WordDocumentProperty dp in documentProperties)
            {
                if (dp.PropertyType != WordDocumentProperty.WordDocumentPropertyType.Custom)
                {
                    continue;
                }

                bool inWordML = false;

                XmlNode docPropertyNode = null;

                if (list[0] != null)
                {
                    for (int i = 0; i < list[0].ChildNodes.Count; i++)
                    {
                        docPropertyNode = list[0].ChildNodes[i];
                        if (dp.DocumentPropertyName.ToLower().Trim() == docPropertyNode.Attributes["name"].Value.ToLower().Trim())
                        {
                            inWordML = true;
                            break;
                        }
                    }
                }

                if (!inWordML) // Add document property to wordML from scratch, using what is in memory.
                {
                    string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
                    val = val.Replace("<text>", "");
                    val = val.Replace("</text>", "");
                    val = val.Replace("<item uId=\"", "answerTo");
                    val = val.Replace("\">", "");
                    val = val.Replace("</item>", "");

                    //create new elements
                    XmlElement newPropertyNode = doc.CreateElement("property", CUSTOM_PROP_NS);
                    newPropertyNode.SetAttribute("fmtid", "{D5CDD505-2E9C-101B-9397-08002B2CF9AE}");
                    int pidCount = list[0].ChildNodes.Count + 2;
                    newPropertyNode.SetAttribute("pid", pidCount.ToString());
                    newPropertyNode.SetAttribute("name", dp.DocumentPropertyName);
                    string customPropertyWordType = WordDocumentProperty.CustomPropertyTypeToWordAtt(dp.CustomDocumentPropertyType);
                    XmlElement newPropertyValueNode = doc.CreateElement("vt", customPropertyWordType, VTYPES_NS);
                    newPropertyValueNode.InnerText = val;
                    newPropertyNode.AppendChild(newPropertyValueNode);
                    list[0].AppendChild(newPropertyNode);
                }
                else if (docPropertyNode != null) // Update the document property in the WordML with what is stored in memory. 
                {
                    string val = dp.DocumentPropertyValue.EvaluateAndKeepXml();
                    val = val.Replace("<text>", "");
                    val = val.Replace("</text>", "");
                    val = val.Replace("<item uId=\"", "answerTo");
                    val = val.Replace("\">", "");
                    val = val.Replace("</item>", "");
                    docPropertyNode.FirstChild.InnerText = val;
                }
            }
        }

		public void ForceDocumentPropertiesUpdate()
		{
			// The call to DiscToBytes() will force the WordDocumentProperty Collection to be updated.			
			if(documentProperties.Count == 0)
			{
				byte[] bytes = DiscToBytes();
				bytes = null; // Dont persist anything in memory.
			}
		}

		private byte[] GetBytesWithoutPropsByAnswer()
		{			

			using (FileStream fs = File.OpenRead(filePath))
			{
				byte[] retVal = new byte[fs.Length];
				fs.Read(retVal, 0, retVal.Length);

				// update what is on disc with document properties.
				retVal = GetBytesWithDocumentProperties(retVal);

				// remove properties by answer.
				retVal = GetBytesWithoutPropsByAnswer(retVal);
					
				fs.Close();
				return retVal;
			}
			}

		private byte[] GetBytesWithoutPropsByAnswer(byte[] bytes)
		{
			XmlDocument doc = new XmlDocument();
			MemoryStream ms = new MemoryStream(bytes);
			doc.Load(ms);
			ms.Close();
	
            XmlNodeList lstStandardCoreProperties = doc.GetElementsByTagName("cp:coreProperties");
            XmlNodeList lstStandardAppProperties = doc.GetElementsByTagName("Properties", EXT_PROPS_NS);
            XmlNodeList lstCustomDocumentProperties = doc.GetElementsByTagName("Properties", CUSTOM_PROP_NS);

            //props that are going to get answers from questions are removed and added at merge
            RemoveStandardDocPropsByAnswer(lstStandardCoreProperties);
            RemoveStandardDocPropsByAnswer(lstStandardAppProperties);

			if(lstCustomDocumentProperties != null && lstCustomDocumentProperties.Count > 0)
			{
                RemoveCustomPropsByAnswer(lstCustomDocumentProperties);
		    }
					
			MemoryStream ms2 = new MemoryStream();
			doc.Save(ms2);
			byte[] retVal = ms2.ToArray();
			ms2.Close();

			return retVal;
		}
        
		private void RemoveStandardDocPropsByAnswer(XmlNodeList list)
		{
			for(int i=0;i<list[0].ChildNodes.Count;i++)
			{	
				XmlNode docPropertyNode = list[0].ChildNodes[i];		
				if(docPropertyNode.InnerText.IndexOf("answerTo") >= 0)
				{
					list[0].RemoveChild(list[0].ChildNodes[i]);
                    i--;
				}								
			}
		}

        private void RemoveCustomPropsByAnswer(XmlNodeList list)
        {
            for (int i = 0; i < list[0].ChildNodes.Count; i++)
            {
                XmlNode docPropertyNode = list[0].ChildNodes[i];
                if (docPropertyNode.FirstChild.InnerText.IndexOf("answerTo") >= 0)
                {
                    list[0].RemoveChild(list[0].ChildNodes[i]);
                    i--;
                }
            }
        }

		#endregion
    }
}