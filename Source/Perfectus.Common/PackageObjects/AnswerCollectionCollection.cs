using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type AnswerCollection
	/// </summary>
	[Serializable]
	public class AnswerCollectionCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the AnswerCollectionCollection class.
		/// </summary>
		public AnswerCollectionCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the AnswerCollectionCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new AnswerCollectionCollection.
		/// </param>
		public AnswerCollectionCollection(AnswerCollection[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the AnswerCollectionCollection class, containing elements
		/// copied from another instance of AnswerCollectionCollection
		/// </summary>
		/// <param name="items">
		/// The AnswerCollectionCollection whose elements are to be added to the new AnswerCollectionCollection.
		/// </param>
		public AnswerCollectionCollection(AnswerCollectionCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this AnswerCollectionCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this AnswerCollectionCollection.
		/// </param>
		public virtual void AddRange(AnswerCollection[] items)
		{
			foreach (AnswerCollection item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another AnswerCollectionCollection to the end of this AnswerCollectionCollection.
		/// </summary>
		/// <param name="items">
		/// The AnswerCollectionCollection whose elements are to be added to the end of this AnswerCollectionCollection.
		/// </param>
		public virtual void AddRange(AnswerCollectionCollection items)
		{
			foreach (AnswerCollection item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type AnswerCollection to the end of this AnswerCollectionCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerCollection to be added to the end of this AnswerCollectionCollection.
		/// </param>
		public virtual void Add(AnswerCollection value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic AnswerCollection value is in this AnswerCollectionCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerCollection value to locate in this AnswerCollectionCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this AnswerCollectionCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(AnswerCollection value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this AnswerCollectionCollection
		/// </summary>
		/// <param name="value">
		/// The AnswerCollection value to locate in the AnswerCollectionCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(AnswerCollection value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the AnswerCollectionCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the AnswerCollection is to be inserted.
		/// </param>
		/// <param name="value">
		/// The AnswerCollection to insert.
		/// </param>
		public virtual void Insert(int index, AnswerCollection value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the AnswerCollection at the given index in this AnswerCollectionCollection.
		/// </summary>
		public virtual AnswerCollection this[int index]
		{
			get
			{
				return (AnswerCollection) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific AnswerCollection from this AnswerCollectionCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerCollection value to remove from this AnswerCollectionCollection.
		/// </param>
		public virtual void Remove(AnswerCollection value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by AnswerCollectionCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(AnswerCollectionCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public AnswerCollection Current
			{
				get
				{
					return (AnswerCollection) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (AnswerCollection) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this AnswerCollectionCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual AnswerCollectionCollection.Enumerator GetEnumerator()
		{
			return new AnswerCollectionCollection.Enumerator(this);
		}

		public bool IsEquivalentTo(AnswerCollectionCollection collCollB)
		{
			if (this.Count != collCollB.Count)
			{
				return false;
			}

			foreach(AnswerCollection collA in this)
			{
				foreach(AnswerCollection collB in collCollB)
				{
					if (collA.Count != collB.Count)
					{
						return false;
					}

					for (int i = 0; i < collA.Count; i++)
					{
						object oA = collA[i];
						object oB = collB[i];
						if (! oA.Equals(oB))
						{
							return false;
						}
					}
				}
			}

			return true;
		}
	}

}
