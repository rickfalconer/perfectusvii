using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Question
	/// </summary>
	[Serializable]
	public sealed class QuestionCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the QuestionCollection class.
		/// </summary>
		internal QuestionCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the QuestionCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new QuestionCollection.
		/// </param>
		internal QuestionCollection(Question[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the QuestionCollection class, containing elements
		/// copied from another instance of QuestionCollection
		/// </summary>
		/// <param name="items">
		/// The QuestionCollection whose elements are to be added to the new QuestionCollection.
		/// </param>
		internal QuestionCollection(QuestionCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this QuestionCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this QuestionCollection.
		/// </param>
		internal void AddRange(Question[] items)
		{
			foreach (Question item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another QuestionCollection to the end of this QuestionCollection.
		/// </summary>
		/// <param name="items">
		/// The QuestionCollection whose elements are to be added to the end of this QuestionCollection.
		/// </param>
		internal void AddRange(QuestionCollection items)
		{
			foreach (Question item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Question to the end of this QuestionCollection.
		/// </summary>
		/// <param name="value">
		/// The Question to be added to the end of this QuestionCollection.
		/// </param>
		public void Add(Question value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Question value is in this QuestionCollection.
		/// </summary>
		/// <param name="value">
		/// The Question value to locate in this QuestionCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this QuestionCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Question value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this QuestionCollection
		/// </summary>
		/// <param name="value">
		/// The Question value to locate in the QuestionCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Question value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the QuestionCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Question is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Question to insert.
		/// </param>
		internal void Insert(int index, Question value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Question at the given index in this QuestionCollection.
		/// </summary>
		public Question this[int index]
		{
			get { return (Question) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Question from this QuestionCollection.
		/// </summary>
		/// <param name="value">
		/// The Question value to remove from this QuestionCollection.
		/// </param>
		internal void Remove(Question value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by QuestionCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(QuestionCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Question Current
			{
				get { return (Question) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Question) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this QuestionCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}