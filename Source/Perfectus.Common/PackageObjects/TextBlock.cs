using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for HtmlBlock.
	/// </summary>
	[Serializable]
	public sealed class TextBlock : IPageItem, ISerializable
	{
		private string text = null;
		private int designerHeight;
		private Guid uniqueIdentifier;
		private YesNoQueryValue visible;

		public YesNoQueryValue Visible
		{
			get { return visible; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
		}

		public int DesignerHeight
		{
			get { return designerHeight; }
			set { designerHeight = value; }
		}

		public string Text
		{
			get { return text; }
			set { text = value; }
		}

		public TextBlock(string body, int designerHeight, Guid uniqueIdentifier, YesNoQueryValue visible) : base()
		{
			text = body;
			this.designerHeight = designerHeight;
			this.uniqueIdentifier = uniqueIdentifier;
			this.visible = visible;
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		public TextBlock(){}//SL

		#region ISerializable Members

		public TextBlock(SerializationInfo info, StreamingContext context)
		{
			text = info.GetString("text");
			designerHeight = info.GetInt32("designerHeight");

			// Unique identifer was added prior to beta, but after the format got locked.
			object o = null;

			try
			{
				o = info.GetValue("uniqueIdentifier", typeof (Guid));
			}
			catch
			{
			}
			if (o == null)
			{
				uniqueIdentifier = Guid.NewGuid();
			}
			else
			{
				uniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			}

			try
			{
				visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("text", text);
			info.AddValue("designerHeight", designerHeight);
			info.AddValue("uniqueIdentifier", uniqueIdentifier);
			info.AddValue("visible", visible);
		}

		#endregion
	}
}