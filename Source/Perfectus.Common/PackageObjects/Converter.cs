using System;
using System.Runtime.Serialization;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Converter.
	/// </summary>
	[Serializable]
	public sealed class Converter : PluginBase, ISerializable
	{
		private Converter()
		{
		}

		public Converter(PluginDescriptor descriptorFromServer) : base(descriptorFromServer)
		{
		}

		public Converter(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
}