using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type OutcomeAction
	/// </summary>
	[Serializable]
	public sealed class OutcomeActionCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the OutcomeActionCollection class.
		/// </summary>
		internal OutcomeActionCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeActionCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new OutcomeActionCollection.
		/// </param>
		internal OutcomeActionCollection(OutcomeAction[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeActionCollection class, containing elements
		/// copied from another instance of OutcomeActionCollection
		/// </summary>
		/// <param name="items">
		/// The OutcomeActionCollection whose elements are to be added to the new OutcomeActionCollection.
		/// </param>
		internal OutcomeActionCollection(OutcomeActionCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this OutcomeActionCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this OutcomeActionCollection.
		/// </param>
		internal void AddRange(OutcomeAction[] items)
		{
			foreach (OutcomeAction item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another OutcomeActionCollection to the end of this OutcomeActionCollection.
		/// </summary>
		/// <param name="items">
		/// The OutcomeActionCollection whose elements are to be added to the end of this OutcomeActionCollection.
		/// </param>
		internal void AddRange(OutcomeActionCollection items)
		{
			foreach (OutcomeAction item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type OutcomeAction to the end of this OutcomeActionCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeAction to be added to the end of this OutcomeActionCollection.
		/// </param>
		public void Add(OutcomeAction value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic OutcomeAction value is in this OutcomeActionCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeAction value to locate in this OutcomeActionCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this OutcomeActionCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(OutcomeAction value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this OutcomeActionCollection
		/// </summary>
		/// <param name="value">
		/// The OutcomeAction value to locate in the OutcomeActionCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(OutcomeAction value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the OutcomeActionCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the OutcomeAction is to be inserted.
		/// </param>
		/// <param name="value">
		/// The OutcomeAction to insert.
		/// </param>
		internal void Insert(int index, OutcomeAction value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the OutcomeAction at the given index in this OutcomeActionCollection.
		/// </summary>
		public OutcomeAction this[int index]
		{
			get { return (OutcomeAction) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific OutcomeAction from this OutcomeActionCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeAction value to remove from this OutcomeActionCollection.
		/// </param>
		internal void Remove(OutcomeAction value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by OutcomeActionCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(OutcomeActionCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public OutcomeAction Current
			{
				get { return (OutcomeAction) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (OutcomeAction) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this OutcomeActionCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}