﻿using System;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// RealQuestionValue. Added 4 March 2013. 'Real' is .net Double
    /// pf-3035 New mapping category Real for livelink 
    /// </summary>
    [Serializable]
    public sealed class RealQuestionValue : IPackageItemContainer
    {
        public enum RealQuestionValueType
        {
            Real,
            Question,
            NoValue
        }


        private RealQuestionValueType valueType;
        private double dblValue;
        private Question questionValue;

        public RealQuestionValue()
        {
            valueType = RealQuestionValueType.NoValue;
            dblValue = 0;
            questionValue = null;
        }

        public double Evaluate(AnswerProvider.Caller caller)
        {
            switch (valueType)
            {
                case RealQuestionValueType.Real:
                    return dblValue;
                default:
                    Question q = questionValue;
                    if (q != null)
                    {
                        object a = q.GetSingleAnswer(caller);
                        if (a != null)
                        {
                            try
                            {
                                return double.Parse(a.ToString());
                            }
                            catch
                            {
                                return 0;
                            }

                        }
                    }
                    return 0;
                /*
                if (q != null && q.Answers.Count > 0)
                {
                    return q.GetSingleAnswer(caller).ToString();
                }
                else
                {
                    return null;
                }*/
            }
        }

        public RealQuestionValueType ValueType
        {
            get { return valueType; }
            set
            {
                switch (value)
                {
                    case RealQuestionValueType.Real:
                        questionValue = null;
                        break;
                    case RealQuestionValueType.Question:
                        dblValue = 0;
                        break;
                    case RealQuestionValueType.NoValue:
                        dblValue = 0;
                        questionValue = null;
                        break;
                }
                valueType = value;
            }
        }

        public double DblValue
        {
            get { return dblValue; }
            set
            {
                dblValue = value;
                ValueType = RealQuestionValueType.Real;
            }
        }

        public Question QuestionValue
        {
            get { return questionValue; }
            set
            {
                questionValue = value;
                ValueType = RealQuestionValueType.Question;
            }
        }

        #region ICloneable Members

        public object Clone()
        {
            object newObj = this.MemberwiseClone();
            RealQuestionValue newRqv = (RealQuestionValue)newObj;

            return newRqv;

        }

        #endregion

        #region IPackageItemContainer Members

        public bool ContainsReferenceTo(PackageItem item)
        {
            return questionValue == item;
        }

        #endregion
    }
}