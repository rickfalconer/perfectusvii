using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for HorizontalLayoutZone.
	/// </summary>
	[Serializable]
	public sealed class HorizontalLayoutZone : PackageItem, IPageItem, ISerializable
	{
		private PageItemCollection items = new PageItemCollection();
		private YesNoQueryValue visible;

		public YesNoQueryValue Visible
		{
			get { return visible; }
		}

		public PageItemCollection Items
		{
			get { return items;  }
			set { items = value; }
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		public HorizontalLayoutZone() //SL - changed from private to public
		{
		}

		public HorizontalLayoutZone(Guid uniqueIdentifier, YesNoQueryValue visible) : base()
		{
			this.UniqueIdentifier = uniqueIdentifier;
			this.visible = visible;
		}

		#region ISerializable Members

		public HorizontalLayoutZone(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			items = (PageItemCollection) info.GetValue("items", typeof (PageItemCollection));
			try
			{
				visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}

			object o = null;

			try
			{
				o = info.GetValue("uniqueIdentifier", typeof (Guid));
			}
			catch
			{
			}
			if (o == null)
			{
				UniqueIdentifier = Guid.NewGuid();
			}
			else
			{
				UniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			}

		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("items", items);
			info.AddValue("visible", visible);
			//info.AddValue("uniqueIdentifier", uniqueIdentifier);
		}

		#endregion
	}
}