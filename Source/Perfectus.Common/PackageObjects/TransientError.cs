using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for AnswerAcquisitionError.
	/// </summary>
	public sealed class TransientError
	{
		private bool isShowStopper = false;

		public bool IsShowStopper
		{
			get { return isShowStopper; }
		}

		private Exception innerException;

		public Exception InnerException
		{
			get { return innerException; }
		}

		private PackageItem packageItem;

		public PackageItem PackageItem
		{
			get { return packageItem; }
		}


		private string message;

		public string Message
		{
			get { return message; }
		}


		public TransientError(bool isShowStopper, PackageItem relatedItem, string message, Exception innerException)
		{
			this.isShowStopper = IsShowStopper;
			packageItem = relatedItem;
			this.message = message;
			this.innerException = innerException;
		}

		private TransientError()
		{
		}
	}
}