namespace Perfectus.Common.PackageObjects
{
	public delegate void PackageItemCancelEventHandler(object sender, PackageItemCancelEventArgs e);
}