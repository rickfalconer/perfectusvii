using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IPageItem.
	/// </summary>
	public interface IPageItem
	{
		YesNoQueryValue Visible { get; }

		Guid UniqueIdentifier { get; }
	}
}