using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type IPageItem
	/// </summary>
	[Serializable]
	public sealed class PageItemCollection : CollectionBase, IXmlSerializable
	{
		#region Private Member Variables

		// Items colleciton keys
		[NonSerialized] private const string msITEMS_KEY = "Items";
		[NonSerialized] private const string msITEM_KEY = "Item";

		// Item type keys
		[NonSerialized] private const string msREPEATER_ZONE_KEY = "RepeaterZone";
		[NonSerialized] private const string msTEXT_BLOCK_KEY = "TextBlock";
		[NonSerialized] private const string msIMAGE_BLOCK_KEY = "ImageBlock";
		[NonSerialized] private const string msHTML_BLOCK_KEY = "HtmlBlock";
		[NonSerialized] private const string msHORIZONTAL_LAYOUT_BLOCK_KEY = "HorizontalLayoutBlock";
		[NonSerialized] private const string msQUESTION_KEY = "Question";

		// Item property keys
		[NonSerialized] private const string msUNIQUE_IDENTIFIER_KEY = "UniqueIdentifier";
		[NonSerialized] private const string msDESIGNER_HEIGHT_KEY = "DesignerHeight";
		[NonSerialized] private const string msHTML_KEY = "Html";
		[NonSerialized] private const string msTEXT_KEY = "Text";
		[NonSerialized] private const string msNOTES_KEY = "Notes";
		[NonSerialized] private const string msNAME_KEY = "Name";
		[NonSerialized] private const string msMAX_ROWS = "MaxRows";

		#endregion
		#region Properties & Constructor

		/// <summary>
		/// Initializes a new empty instance of the PageItemCollection class.
		/// </summary>
		public PageItemCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PageItemCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PageItemCollection.
		/// </param>
		public PageItemCollection(IPageItem[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PageItemCollection class, containing elements
		/// copied from another instance of PageItemCollection
		/// </summary>
		/// <param name="items">
		/// The PageItemCollection whose elements are to be added to the new PageItemCollection.
		/// </param>
		public PageItemCollection(PageItemCollection items)
		{
			this.AddRange(items);
		}

		#endregion
		#region Methods

		/// <summary>
		/// Adds the elements of an array to the end of this PageItemCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PageItemCollection.
		/// </param>
		public void AddRange(IPageItem[] items)
		{
			foreach (IPageItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PageItemCollection to the end of this PageItemCollection.
		/// </summary>
		/// <param name="items">
		/// The PageItemCollection whose elements are to be added to the end of this PageItemCollection.
		/// </param>
		public void AddRange(PageItemCollection items)
		{
			foreach (IPageItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type IPageItem to the end of this PageItemCollection.
		/// </summary>
		/// <param name="value">
		/// The IPageItem to be added to the end of this PageItemCollection.
		/// </param>
		public void Add(IPageItem value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic IPageItem value is in this PageItemCollection.
		/// </summary>
		/// <param name="value">
		/// The IPageItem value to locate in this PageItemCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PageItemCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(IPageItem value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PageItemCollection
		/// </summary>
		/// <param name="value">
		/// The IPageItem value to locate in the PageItemCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(IPageItem value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PageItemCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the IPageItem is to be inserted.
		/// </param>
		/// <param name="value">
		/// The IPageItem to insert.
		/// </param>
		public void Insert(int index, IPageItem value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the IPageItem at the given index in this PageItemCollection.
		/// </summary>
		public IPageItem this[int index]
		{
			get { return (IPageItem) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific IPageItem from this PageItemCollection.
		/// </summary>
		/// <param name="value">
		/// The IPageItem value to remove from this PageItemCollection.
		/// </param>
		public void Remove(IPageItem value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PageItemCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(PageItemCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public IPageItem Current
			{
				get { return (IPageItem) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (IPageItem) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PageItemCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		#endregion
		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		public void WriteXml(XmlWriter w)
		{	
			w.WriteStartElement(msITEMS_KEY);
			WriteItemXml(w, this);
			w.WriteEndElement();
		}

		private void WriteItemXml(XmlWriter w, PageItemCollection items)
		{
			for (Int32 i = 0; i < items.Count; i++)
			{
				w.WriteStartElement(msITEM_KEY);

				if (items[i].GetType() == typeof(Question))
				{
					Serializer.WriteDependentInformation(w, (Question)items[i], "", false);
				}
				else if (items[i].GetType() == typeof(RepeaterZone))
				{
					WriteRepeaterZoneXml(w, (RepeaterZone)items[i]);
				}
				else if (items[i].GetType() == typeof(HorizontalLayoutZone))
				{
					WriteHorizontalLayoutZoneXml(w, (HorizontalLayoutZone)items[i]);
				}
				else if (items[i].GetType() == typeof(HtmlBlock))
				{
					WriteHtmlBlockXml(w, (HtmlBlock)items[i]);
				}
				else if (items[i].GetType() == typeof(TextBlock))
				{
					WriteTextBlockXml(w, (TextBlock)items[i]);				
				}
				else if (items[i].GetType() == typeof(ImageBlock))
				{
					WriteImageBlockXml(w, (ImageBlock)items[i]);					
				}
			
				w.WriteEndElement();
			}
		}

		private void WriteRepeaterZoneXml(XmlWriter w, RepeaterZone repeaterZone)
		{
			w.WriteStartElement(msREPEATER_ZONE_KEY);

			w.WriteStartElement(msUNIQUE_IDENTIFIER_KEY);
			w.WriteString(repeaterZone.UniqueIdentifier.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msNAME_KEY);
			w.WriteString(repeaterZone.Name);
			w.WriteEndElement();
			w.WriteStartElement(msNOTES_KEY);
			w.WriteString(repeaterZone.Notes);
			w.WriteEndElement();
			w.WriteStartElement(msMAX_ROWS);
            w.WriteString(repeaterZone.MaxRows.Evaluate(AnswerProvider.Caller.System).ToString()); // IntValue.ToString();
			w.WriteEndElement();
			w.WriteStartElement(msITEMS_KEY);
			WriteItemXml(w, repeaterZone.Items); // Recursive Call
			w.WriteEndElement();

			// Visible section
			Serializer.WriteVisibleSection(w, repeaterZone.Visible);

			w.WriteEndElement();
		}

		private void WriteHorizontalLayoutZoneXml(XmlWriter w, HorizontalLayoutZone horizontalLayoutZone)
		{
			w.WriteStartElement(msHORIZONTAL_LAYOUT_BLOCK_KEY);

			w.WriteStartElement(msUNIQUE_IDENTIFIER_KEY);
			w.WriteString(horizontalLayoutZone.UniqueIdentifier.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msNAME_KEY);
			w.WriteString(horizontalLayoutZone.Name);
			w.WriteEndElement();
			w.WriteStartElement(msNOTES_KEY);
			w.WriteString(horizontalLayoutZone.Notes);
			w.WriteEndElement();
			w.WriteStartElement(msITEMS_KEY);
			WriteItemXml(w, horizontalLayoutZone.Items); // Recursive Call
			w.WriteEndElement();
										
			// Visible section
			Serializer.WriteVisibleSection(w, horizontalLayoutZone.Visible);

			w.WriteEndElement();
		}

		private void WriteHtmlBlockXml(XmlWriter w, HtmlBlock htmlBlock)
		{
			//TODO - SL - WriteHtmlBlockXml - need to handle the parsing of objects in the html - dependencies
			w.WriteStartElement(msHTML_BLOCK_KEY);

			w.WriteStartElement(msUNIQUE_IDENTIFIER_KEY);
			w.WriteString(htmlBlock.UniqueIdentifier.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msDESIGNER_HEIGHT_KEY);
			w.WriteString(htmlBlock.DesignerHeight.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msHTML_KEY);

            if (htmlBlock.Html != null)
            {
			w.WriteString(htmlBlock.Html.ToString());
            }
            else
            {
                w.WriteString("");
            }

			w.WriteEndElement();

			// Visible section
			Serializer.WriteVisibleSection(w, htmlBlock.Visible);

			w.WriteEndElement();
		}

		private void WriteTextBlockXml(XmlWriter w, TextBlock textBlock)
		{
			w.WriteStartElement(msTEXT_BLOCK_KEY);

			w.WriteStartElement(msUNIQUE_IDENTIFIER_KEY);
			w.WriteString(textBlock.UniqueIdentifier.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msDESIGNER_HEIGHT_KEY);
			w.WriteString(textBlock.DesignerHeight.ToString());
			w.WriteEndElement();
			w.WriteStartElement(msTEXT_KEY);
			w.WriteString(textBlock.Text.ToString());
			w.WriteEndElement();

			// Visible section
			Serializer.WriteVisibleSection(w, textBlock.Visible);

			w.WriteEndElement();
		}

		private void WriteImageBlockXml(XmlWriter w, ImageBlock imageBlock)
		{
			w.WriteStartElement(msIMAGE_BLOCK_KEY);

			w.WriteStartElement(msUNIQUE_IDENTIFIER_KEY);
			w.WriteString(imageBlock.UniqueIdentifier.ToString());
			w.WriteEndElement();

			w.WriteEndElement();
		}

		public void ReadXml(XmlReader r)
		{
			if (!r.IsEmptyElement)
			{
				r.ReadStartElement(msITEMS_KEY);
				ReadItemXml(r, this);
				r.ReadEndElement();
			}
			else
			{
				r.Read();
			}
		}

		private void ReadItemXml(XmlReader r, PageItemCollection items)
		{
			string[] values;

			Question question;

			while (r.IsStartElement(msITEM_KEY))
			{
				r.ReadStartElement(msITEM_KEY);

				if (r.IsStartElement(Serializer.DependencyKey))
				{
					r.ReadStartElement(Serializer.DependencyKey);

					// Read the type data, but don't keep it, this can only be a question object
					r.ReadStartElement(Serializer.TypeKey);
					r.ReadString();
					r.ReadEndElement();

					// Get the Questions info, so that it can later be reconnected
					values = Serializer.ReadLibraryItem(r);
					question = new Question(values[Serializer.NameIndex]);
					question.LibraryUniqueIdentifier = new Guid(values[Serializer.UniqueIdentifierIndex]);
					items.Add(question);

					r.ReadEndElement();
				}
				else if (r.IsStartElement(msREPEATER_ZONE_KEY))
				{
					items.Add(ReadRepeaterZoneXml(r));
				}
				else if (r.IsStartElement(msHORIZONTAL_LAYOUT_BLOCK_KEY))
				{
					items.Add(ReadHorizontalLayoutZoneXml(r));
				}
				else if (r.IsStartElement(msHTML_BLOCK_KEY))
				{
					items.Add(ReadHtmlBlockXml(r));
				}
				else if (r.IsStartElement(msTEXT_BLOCK_KEY))
				{
					items.Add(ReadTextBlockXml(r));
				}
				else if (r.IsStartElement(msIMAGE_BLOCK_KEY))
				{
					items.Add(ReadImageBlockXml(r));		
				}
			
				r.ReadEndElement();
			}
		}

		private IPageItem ReadRepeaterZoneXml(XmlReader r)
		{
			string uniqueIdentifier;
			RepeaterZone repeaterZone;
			string name = string.Empty;
			PageItemCollection items = new PageItemCollection();
			YesNoQueryValue yesNoQueryValue = new YesNoQueryValue();
			IntQuestionValue intQuestionValue = new IntQuestionValue();
			intQuestionValue.ValueType = IntQuestionValue.IntQuestionValueType.Int;

			// Read & populate the RepeaterZone
			r.ReadStartElement(msREPEATER_ZONE_KEY);

			r.ReadStartElement(msUNIQUE_IDENTIFIER_KEY);
			uniqueIdentifier = r.ReadString();
			r.ReadEndElement();

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msNAME_KEY);
				name = r.ReadString();
				r.ReadEndElement();
			}

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msNOTES_KEY);
				intQuestionValue.IntValue = Convert.ToInt32(r.ReadString());
				r.ReadEndElement();
			}

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msMAX_ROWS);
				intQuestionValue.IntValue = Convert.ToInt32(r.ReadString());
				r.ReadEndElement();
			}

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msITEMS_KEY);
				ReadItemXml(r, items); // Recursive Call
				r.ReadEndElement();
			}

			// Visible section
			yesNoQueryValue = Serializer.ReadVisibleSection(r);

			r.ReadEndElement();

			// Set the RepeaterZone object and return it
			repeaterZone = new RepeaterZone(new Guid(uniqueIdentifier), yesNoQueryValue, intQuestionValue);
			repeaterZone.Name = name;
			repeaterZone.Items = items;
			return repeaterZone;
		}

		private IPageItem ReadHorizontalLayoutZoneXml(XmlReader r)
		{
			string uniqueIdentifier;
			string name = string.Empty;
			string notes = string.Empty;
			HorizontalLayoutZone horizontalLayoutZone;
			PageItemCollection items = new PageItemCollection();
			YesNoQueryValue yesNoQueryValue = new YesNoQueryValue();

			// Read & populate the horizontal block
			r.ReadStartElement(msHORIZONTAL_LAYOUT_BLOCK_KEY);

			r.ReadStartElement(msUNIQUE_IDENTIFIER_KEY);
			uniqueIdentifier = r.ReadString();
			r.ReadEndElement();

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msNAME_KEY);
				name = r.ReadString();
				r.ReadEndElement();
			}
			
			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msNOTES_KEY);
				notes = r.ReadString();
				r.ReadEndElement();
			}

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msITEMS_KEY);
				ReadItemXml(r, items); // Recursive Call
				r.ReadEndElement();
			}
						
			// Visible section
			yesNoQueryValue = Serializer.ReadVisibleSection(r);

			r.ReadEndElement();

			horizontalLayoutZone = new HorizontalLayoutZone(new Guid(), yesNoQueryValue);
			horizontalLayoutZone.Name = name;
			horizontalLayoutZone.Notes = notes;
			horizontalLayoutZone.Items = items;

			return horizontalLayoutZone;
		}

		private IPageItem ReadHtmlBlockXml(XmlReader r)
		{
			YesNoQueryValue yesNoQueryValue = new YesNoQueryValue();
			string uniqueIdentifier, html;
			Int32 designerheight = 0;

			//TODO - SL - ReadHtmlBlockXml - need to handle the parsing of objects in the html - dependencies
			r.ReadStartElement(msHTML_BLOCK_KEY);

			r.ReadStartElement(msUNIQUE_IDENTIFIER_KEY);
			uniqueIdentifier = r.ReadString();
			r.ReadEndElement();

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msDESIGNER_HEIGHT_KEY);
				designerheight = Convert.ToInt32(r.ReadString());
				r.ReadEndElement();
			}

            html = String.Empty;
            
			if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
			r.ReadStartElement(msHTML_KEY);
			html = r.ReadString();
			r.ReadEndElement();
            }
			// Visible section
			yesNoQueryValue = Serializer.ReadVisibleSection(r);

			r.ReadEndElement();

			return new HtmlBlock(html, designerheight, new Guid(uniqueIdentifier), yesNoQueryValue);
		}

		private IPageItem ReadTextBlockXml(XmlReader r)
		{
			YesNoQueryValue yesNoQueryValue = new YesNoQueryValue();
			string uniqueIdentifier, text;
			Int32 designerheight = 0;

			r.ReadStartElement(msTEXT_BLOCK_KEY);
			r.ReadStartElement(msUNIQUE_IDENTIFIER_KEY);
			uniqueIdentifier = r.ReadString();
			r.ReadEndElement();

			if (r.IsEmptyElement)
			{
				r.Read();
			}
			else
			{
				r.ReadStartElement(msDESIGNER_HEIGHT_KEY);
				designerheight = Convert.ToInt32(r.ReadString());
				r.ReadEndElement();
			}

			r.ReadStartElement(msTEXT_KEY);
			text= r.ReadString();
			r.ReadEndElement();			

			// Visible section
			yesNoQueryValue = Serializer.ReadVisibleSection(r);

			r.ReadEndElement();

			return new TextBlock(text, designerheight, new Guid(uniqueIdentifier), yesNoQueryValue);
		}

		private IPageItem ReadImageBlockXml(XmlReader r)
		{
			string uniqueIdentifier;

			r.ReadStartElement(msIMAGE_BLOCK_KEY);
			r.ReadStartElement(msUNIQUE_IDENTIFIER_KEY);
			uniqueIdentifier = r.ReadString();
			r.ReadEndElement();
			r.ReadEndElement();

			return new ImageBlock(null, 0, new Guid(uniqueIdentifier), null);
		}

		#endregion
	}
}
