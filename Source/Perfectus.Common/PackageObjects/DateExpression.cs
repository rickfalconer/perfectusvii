using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for DatePFunctionDefinition.
	/// </summary>
	[Serializable]
	public sealed class DateExpression : IXmlSerializable, ISerializable
	{
		#region Member Variables
		
		// Property variables
		private DateFunctionMethod method;
		private AnswerProvider parameter1;
		private AnswerProvider parameter2;
		private static string msEXPRESSIONOPERATION = "ExpressionOperation";

		// Properties for Xml Serialization (not for binary)
		string _UniqueIdentifier1;
		string _Name1;
		string _Value1;
		string _DataType1;
		string _Type1;
		string _UniqueIdentifier2;
		string _Name2;
		string _Type2;
		string _Value2;
		string _DataType2;

		// Serialization key constants
		const string mcMETHOD = "method";
		const string mcPARAMETER1 = "parameter1";
		const string mcPARAMETER2 = "parameter2";
		const string mcEXPRESSIONOPERATION = "expressionoperation";

		// Properties for Xml Serialization (not for binary)
		const string mcUNIQUE_IDENTIFIER1 = "UniqueIdentifier1";
		const string mcNAME1 = "Name1";
		const string mcVALUE1 = "Value1";
		const string mcDATA_TYPE1 = "DataType1";
		const string mcTYPE1 = "Type1";
		const string mcUNIQUE_IDENTIFIER2 = "UniqueIdentifier2";
		const string mcNAME2 = "Name2";
		const string mcTYPE2 = "Type2";
		const string mcVALUE2 = "Value2";
		const string mcDATA_TYPE2 = "DataType2";

		
		#endregion
		#region Properties & Constructor

		public AnswerProvider Parameter1
		{
			get { return parameter1; }
			set { parameter1 = value; }
		}

		public AnswerProvider Parameter2
		{
			get { return parameter2; }
			set { parameter2 = value; }
		}

		public string Parameter1UniqueIdentifier
		{
			get { return _UniqueIdentifier1;  }
			set { _UniqueIdentifier1 = value; }
		}

		public string Parameter2UniqueIdentifier
		{
			get { return _UniqueIdentifier2;  }
			set { _UniqueIdentifier2 = value; }
		}

		public string Parameter1Name
		{
			get { return _Name1;  }
			set { _Name1 = value; }
		}

		public string Parameter2Name
		{
			get { return _Name2;  }
			set { _Name2 = value; }
		}

		public string Parameter1Type
		{
			get { return _Type1;  }
			set { _Type1 = value; }
		}

		public string Parameter2Type
		{
			get { return _Type2;  }
			set { _Type2 = value; }
		}

		public string Parameter1DataType
		{
			get { return _DataType1;  }
			set { _DataType1 = value; }
		}

		public string Parameter2DataType
		{
			get { return _DataType2;  }
			set { _DataType2 = value; }
		}

		public string Parameter1Value
		{
			get { return _Value1;  }
			set { _Value1 = value; }
		}

		public string Parameter2Value
		{
			get { return _Value2;  }
			set { _Value2 = value; }
		}

		/// <summary>
		///  Accessor for the Method propery. Depicts the given date method to apply on the paramters.
		/// </summary>
		public DateFunctionMethod Method
		{
			get { return method; }
			set { method = value; }
		}

		public DateExpression() {}

		#endregion
		#region Methods

		[Obsolete]
		public object Evaluate()
		{
			return Evaluate(AnswerProvider.Caller.System);
		}

	
		public object Evaluate(AnswerProvider.Caller caller)
		{
			
			DateTime param1;
			DateTime param2AsDate;
            decimal param2AsDecimal;

			try
			{
				if(method == DateFunctionMethod.Now)
				{
					return DateTime.Now;
				}
				
				if (parameter1 is PackageConstant && ((PackageConstant) parameter1).Value.ToString() == "DateTime.Now")
				{
					param1 = DateTime.Now.Date;
				}
				else
				{
					param1 = (DateTime) parameter1.GetSingleAnswer(caller);
				}

				if (parameter2 is PackageConstant && ((PackageConstant) parameter2).Value.ToString() == "DateTime.Now")
				{
					param2AsDate = DateTime.Now.Date;
				}
				else
				{
					param2AsDate = (parameter2.GetSingleAnswer(caller) is DateTime) ? ((DateTime) parameter2.GetSingleAnswer(caller)) : DateTime.MinValue;
				}

				param2AsDecimal = (parameter2.GetSingleAnswer(caller) is decimal) ? (decimal) (parameter2.GetSingleAnswer(caller)) : 0;

			}
			catch
			{
				return null;
			}
			switch (method)
			{
				case DateFunctionMethod.DayAdd:
					return param1.AddDays((int) param2AsDecimal);
				case DateFunctionMethod.MonthAdd:
					return param1.AddMonths((int) param2AsDecimal);
				case DateFunctionMethod.YearAdd:
					return param1.AddYears((int) param2AsDecimal);
				case DateFunctionMethod.DayDiff:
					return Math.Abs(DateDiff("d", param1, param2AsDate));
				case DateFunctionMethod.MonthDiff:
					return Math.Abs(DateDiff("m", param1, param2AsDate));
				case DateFunctionMethod.YearDiff:
					return Math.Abs(DateDiff("y", param1, param2AsDate));
				default:
					return null;

			}

		}


		/// <summary>
		///		An implementation of the Visual Bacis "DateDiff" Function
		///		The howtocompare parameter accepts the following values:
		///		Results can be of the following types:
		/// <list type="">
		///		<item>y - Years</item>
		///		<item>m - Months</item>
		///		<item>d - Days (Default)</item>
		///		<item>q - Quater</item>
		///		<item>mi - Minutes</item>
		///		<item>s - seconds</item>
		///		<item>ms = MilliSeconds</item>
		///		<item>t - Ticks</item>
		/// </list>
		/// </summary>
		/// <param name="howtocompare">The comparison method</param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <returns>The Difference between the two dates</returns>
		private decimal DateDiff(string howtocompare, DateTime startDate, DateTime endDate)
		{
			decimal diff = 0;

			if (howtocompare.ToLower() == "m")
			{
                return ProcessMonths(startDate, endDate);
			}

			TimeSpan TS = new TimeSpan(endDate.Ticks - startDate.Ticks);
			switch (howtocompare.ToLower())
			{
				case "mi":
					diff = Convert.ToDecimal(TS.TotalMinutes);
					break;
				case "s":
                    diff = Convert.ToDecimal(TS.TotalSeconds);
					break;
				case "t":
                    diff = Convert.ToDecimal(TS.Ticks);
					break;
				case "ms":
                    diff = Convert.ToDecimal(TS.TotalMilliseconds);
					break;
				case "y":
                    diff = GetYearDifference(startDate, endDate);
					break;
				case "q":
                    diff = Convert.ToDecimal((TS.TotalDays / 365) / 4);
					break;
				default:
					//d
                    diff = Convert.ToDecimal(TS.TotalDays);
					break;
			}

			return Math.Ceiling(diff);
		}

        /// <summary>
        /// Finds out how many months between two dates without any knowledge of which date is higher than the other
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        private decimal ProcessMonths(DateTime startDate, DateTime endDate)
        {
            if (endDate.Year == startDate.Year)
            {
                return endDate.Month - startDate.Month;
            }
            else if (endDate.Year > startDate.Year)
            {
                return GetMonths(startDate, endDate);
            }
            else if (startDate.Year > endDate.Year)
            {
                return GetMonths(endDate, startDate);
            }

            return 0;
        }

        /// <summary>
        /// Gets the months difference between two provided dates with the knowledge that date2 is larger than date1
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns>Months</returns>
        private decimal GetMonths(DateTime date1, DateTime date2)
        {
            decimal yearsDiff = 0;
            decimal monthsDiff = 0;

            if (date2.Year > (date1.Year + 1))
            {
                yearsDiff = DateDiff("y", date1, date2);

                //if date2's month is after date1's month or they are in the same month but date2's day is after date1's day we know that we need to subtract a year becuase the yeardiff method will 
                //already have accounted for the year (because it ignores the months).
                //The month diff will work out up to 23 months difference between two years
                if (date2.Month > date1.Month || (date2.Month == date1.Month && date2.Day >= date1.Day))
                {
                    yearsDiff--;
                }
            }

            monthsDiff = date2.Month;
            monthsDiff = monthsDiff + (12 - date1.Month);
            return (yearsDiff * 12) + monthsDiff;
        }

        /// <summary>
        ///     Gets the year difference between two dates handling leap years and centenial leap years
        /// </summary>
        /// <param name="startDate">Starting date to evaludate</param>
        /// <param name="endDate">Ending date to evaludate</param>
        /// <returns>A decimal indicating the year difference value</returns>
        public decimal GetYearDifference(DateTime startDate, DateTime endDate)
        {
            decimal yearDifference = 0;

            // Ensure the start date is the smallest date
            if (startDate > endDate)
            {
                DateTime temp;
                temp = startDate;
                startDate = endDate;
                endDate = temp;
            }

            // Get the year difference
            yearDifference = endDate.Year - startDate.Year;
            if (endDate.Year - startDate.Year != 0)
            {
                // Ensure the month and day has ticked over to ensure the year diff is correct
                if (startDate.Month > endDate.Month)
                {
                    yearDifference--;
                }
                else if(startDate.Month == endDate.Month && startDate.Day > endDate.Day)
                {
                    yearDifference--;
                }
            }

            return yearDifference;
        }
        
		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion
		#region IXmlSerializable Members

		/// <summary>rv3
        /// 
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		public void WriteXml(XmlWriter w)
		{	
			DateExpression dateExpression = this;

            // Start the dependencies section
            w.WriteStartElement(Serializer.DependenciesKey);

            if (dateExpression.Method != DateFunctionMethod.Now)
            {
                if (dateExpression.Parameter1 != null && dateExpression.Parameter2 != null)
                {
                    // Parameter1 - cast this object into a specific ExpressionNode Object and get its Operand1 property
                    Serializer.WriteDependentInformation(w, dateExpression.Parameter1, Serializer.Parameter1Key);

                    // Parameter2 - cast this object into a specific ExpressionNode Object and get its Operand2 property
                    Serializer.WriteDependentInformation(w, dateExpression.Parameter2, Serializer.Parameter2Key);
                }
                else if ((dateExpression.Parameter1 != null && dateExpression.Parameter2 == null) ||
                         (dateExpression.Parameter1 == null && dateExpression.Parameter2 != null))
                {
                    //TODO - resource
                    throw new InvalidOperationException("The Date function is not correctly defined, please check the date function definition");
                }
            }

			// ExpressionOperation
			w.WriteElementString(msEXPRESSIONOPERATION, this.Method.ToString());

			// End the dependencies section
			w.WriteEndElement();
		}

		public void ReadXml(XmlReader r)
		{
			string[] values = new string[Serializer.IndexCount];
			string expressionOperation;

			// Read the starting element
			r.ReadStartElement();

			// Open the definition element and get the first parameters info
			r.ReadStartElement(Serializer.DependenciesKey);

            if (r.LocalName == Serializer.Parameter1Key)
            {
                // Set the Parameter 1 details
                r.ReadStartElement(Serializer.Parameter1Key);
                r.ReadStartElement(Serializer.DependencyKey);

                if (!r.IsEmptyElement)
                {
                    r.ReadStartElement(Serializer.TypeKey);
                    _Type1 = r.ReadString();
                    r.ReadEndElement();

                    if (_Type1 == typeof(Question).ToString() || _Type1 == typeof(DatePFunction).ToString())
                    {
                        values = Serializer.ReadLibraryItem(r);
                        _Name1 = values[Serializer.NameIndex];
                        _UniqueIdentifier1 = values[Serializer.UniqueIdentifierIndex];
                    }
                    else if (_Type1 == typeof(PackageConstant).ToString())
                    {
                        values = Serializer.ReadPackageConstant(r);
                        _UniqueIdentifier1 = values[Serializer.UniqueIdentifierIndex];
                        _Value1 = values[Serializer.ValueIndex];
                        _DataType1 = values[Serializer.DataTypeIndex];
                    }

                    r.ReadEndElement();
                    r.ReadEndElement();
                }
                else
                {
                    r.Read();
                }

                // Set the Parameter 2 details
                r.ReadStartElement(Serializer.Parameter2Key);
                r.ReadStartElement(Serializer.DependencyKey);

                if (!r.IsEmptyElement)
                {
                    r.ReadStartElement(Serializer.TypeKey);
                    _Type2 = r.ReadString();
                    r.ReadEndElement();

                    if (_Type2 == typeof(Question).ToString() || _Type2 == typeof(DatePFunction).ToString())
                    {
                        values = Serializer.ReadLibraryItem(r);
                        _Name2 = values[Serializer.NameIndex];
                        _UniqueIdentifier2 = values[Serializer.UniqueIdentifierIndex];
                    }
                    else if (_Type2 == typeof(PackageConstant).ToString())
                    {
                        values = Serializer.ReadPackageConstant(r);
                        _UniqueIdentifier2 = values[Serializer.UniqueIdentifierIndex];
                        _Value2 = values[Serializer.ValueIndex];
                        _DataType2 = values[Serializer.DataTypeIndex];
                    }

                    r.ReadEndElement();
                    r.ReadEndElement();
                }
                else
                {
                    r.Read();
                }
            }

			// Set the Expression Operation
			r.ReadStartElement(msEXPRESSIONOPERATION);
			expressionOperation = r.ReadString();
			r.ReadEndElement();

			this.Method = GetDateFunctionMethod(expressionOperation);
		}

		private DateFunctionMethod GetDateFunctionMethod(string dateFunctionMethod)
		{
			if (dateFunctionMethod == DateFunctionMethod.DayAdd.ToString())
				return DateFunctionMethod.DayAdd;
			else if (dateFunctionMethod == DateFunctionMethod.DayDiff.ToString())
				return DateFunctionMethod.DayDiff;
			else if (dateFunctionMethod == DateFunctionMethod.MonthAdd.ToString())
				return DateFunctionMethod.MonthAdd;
			else if (dateFunctionMethod == DateFunctionMethod.MonthDiff.ToString())
				return DateFunctionMethod.MonthDiff;
			else if (dateFunctionMethod == DateFunctionMethod.Now.ToString())
				return DateFunctionMethod.Now;
			else if (dateFunctionMethod == DateFunctionMethod.YearAdd.ToString())
				return DateFunctionMethod.YearAdd;
			else if (dateFunctionMethod == DateFunctionMethod.YearDiff.ToString())
				return DateFunctionMethod.YearDiff;
			else 
				return DateFunctionMethod.DayAdd;
		}

		#endregion
		#region ISerializable Members

		public DateExpression(SerializationInfo info, StreamingContext context)
		{
			method = (DateFunctionMethod) info.GetValue(mcMETHOD, typeof(DateFunctionMethod));
			parameter1 = (AnswerProvider) info.GetValue(mcPARAMETER1, typeof(AnswerProvider));
			parameter2 = (AnswerProvider) info.GetValue(mcPARAMETER2, typeof(AnswerProvider));

			try
			{
				_UniqueIdentifier1 = info.GetString(mcUNIQUE_IDENTIFIER1);
				_Name1 = info.GetString(mcNAME1);
				_Value1 = info.GetString(mcVALUE1);
				_DataType1  = info.GetString(mcDATA_TYPE1);
				_Type1 = info.GetString(mcTYPE1);
				_UniqueIdentifier2 = info.GetString(mcUNIQUE_IDENTIFIER2);
				_Name2 = info.GetString(mcNAME2);
				_Value2 = info.GetString(mcVALUE2);
				_DataType2 = info.GetString(mcDATA_TYPE2);
				_Type2 = info.GetString(mcTYPE2);
			}
			catch
			{
				// Item cannot be linked to the library, or it is from 5.1.X and below version (only v 5.2+ supports shared library)
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue(mcMETHOD, method);
			info.AddValue(mcPARAMETER1, parameter1);
			info.AddValue(mcPARAMETER2, parameter2);
			info.AddValue(mcUNIQUE_IDENTIFIER1, _UniqueIdentifier1);
			info.AddValue(mcNAME1, _Name1);
			info.AddValue(mcVALUE1, _Value1);
			info.AddValue(mcDATA_TYPE1, _DataType1);
			info.AddValue(mcTYPE1, _Type1);
			info.AddValue(mcUNIQUE_IDENTIFIER2, _UniqueIdentifier2);
			info.AddValue(mcNAME2, _Name2);
			info.AddValue(mcVALUE2, _Value2);
			info.AddValue(mcDATA_TYPE2, _DataType2);
			info.AddValue(mcTYPE2, _Type2);
		}

		#endregion
	}
}