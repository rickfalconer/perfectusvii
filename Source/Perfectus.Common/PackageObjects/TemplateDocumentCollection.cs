using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Template
	/// </summary>
	[Serializable]
	public sealed class TemplateDocumentCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the TemplateCollection class.
		/// </summary>
		public TemplateDocumentCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the TemplateCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new TemplateCollection.
		/// </param>
		internal TemplateDocumentCollection(TemplateDocument[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the TemplateCollection class, containing elements
		/// copied from another instance of TemplateCollection
		/// </summary>
		/// <param name="items">
		/// The TemplateCollection whose elements are to be added to the new TemplateCollection.
		/// </param>
		internal TemplateDocumentCollection(TemplateDocumentCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this TemplateCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this TemplateCollection.
		/// </param>
		internal void AddRange(TemplateDocument[] items)
		{
			foreach (TemplateDocument item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another TemplateCollection to the end of this TemplateCollection.
		/// </summary>
		/// <param name="items">
		/// The TemplateCollection whose elements are to be added to the end of this TemplateCollection.
		/// </param>
		internal void AddRange(TemplateDocumentCollection items)
		{
			foreach (TemplateDocument item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Template to the end of this TemplateCollection.
		/// </summary>
		/// <param name="value">
		/// The Template to be added to the end of this TemplateCollection.
		/// </param>
		public void Add(TemplateDocument value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Template value is in this TemplateCollection.
		/// </summary>
		/// <param name="value">
		/// The Template value to locate in this TemplateCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this TemplateCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(TemplateDocument value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this TemplateCollection
		/// </summary>
		/// <param name="value">
		/// The Template value to locate in the TemplateCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(TemplateDocument value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the TemplateCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Template is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Template to insert.
		/// </param>
		internal void Insert(int index, TemplateDocument value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Template at the given index in this TemplateCollection.
		/// </summary>
		public TemplateDocument this[int index]
		{
			get { return (TemplateDocument) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Template from this TemplateCollection.
		/// </summary>
		/// <param name="value">
		/// The Template value to remove from this TemplateCollection.
		/// </param>
		internal void Remove(TemplateDocument value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by TemplateCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(TemplateDocumentCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public TemplateDocument Current
			{
				get { return (TemplateDocument) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (TemplateDocument) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this TemplateCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}