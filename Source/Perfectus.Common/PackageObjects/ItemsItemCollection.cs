using System;
using System.Collections;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type ItemsItem
	/// </summary>
	[Serializable]
	public sealed class ItemsItemCollection : CollectionBase, IXmlSerializable
	{
		/// <summary>
		/// Initializes a new empty instance of the ItemsItemCollection class.
		/// </summary>
		public ItemsItemCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the ItemsItemCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new ItemsItemCollection.
		/// </param>
		public ItemsItemCollection(ItemsItem[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the ItemsItemCollection class, containing elements
		/// copied from another instance of ItemsItemCollection
		/// </summary>
		/// <param name="items">
		/// The ItemsItemCollection whose elements are to be added to the new ItemsItemCollection.
		/// </param>
		public ItemsItemCollection(ItemsItemCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this ItemsItemCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this ItemsItemCollection.
		/// </param>
		public void AddRange(ItemsItem[] items)
		{
			foreach (ItemsItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another ItemsItemCollection to the end of this ItemsItemCollection.
		/// </summary>
		/// <param name="items">
		/// The ItemsItemCollection whose elements are to be added to the end of this ItemsItemCollection.
		/// </param>
		public void AddRange(ItemsItemCollection items)
		{
			foreach (ItemsItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type ItemsItem to the end of this ItemsItemCollection.
		/// </summary>
		/// <param name="value">
		/// The ItemsItem to be added to the end of this ItemsItemCollection.
		/// </param>
		public void Add(ItemsItem value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic ItemsItem value is in this ItemsItemCollection.
		/// </summary>
		/// <param name="value">
		/// The ItemsItem value to locate in this ItemsItemCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this ItemsItemCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(ItemsItem value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this ItemsItemCollection
		/// </summary>
		/// <param name="value">
		/// The ItemsItem value to locate in the ItemsItemCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(ItemsItem value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the ItemsItemCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the ItemsItem is to be inserted.
		/// </param>
		/// <param name="value">
		/// The ItemsItem to insert.
		/// </param>
		public void Insert(int index, ItemsItem value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the ItemsItem at the given index in this ItemsItemCollection.
		/// </summary>
		public ItemsItem this[int index]
		{
			get { return (ItemsItem) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific ItemsItem from this ItemsItemCollection.
		/// </summary>
		/// <param name="value">
		/// The ItemsItem value to remove from this ItemsItemCollection.
		/// </param>
		public void Remove(ItemsItem value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by ItemsItemCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(ItemsItemCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public ItemsItem Current
			{
				get { return (ItemsItem) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (ItemsItem) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this ItemsItemCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		#region IXmlSerializable Members

		public void WriteXml(System.Xml.XmlWriter w)
		{
			for(int i=0; i < this.Count; i++)
			{
				w.WriteStartElement("Item");
				
				w.WriteStartElement("Display");
				w.WriteString(this[i].Display);
				w.WriteEndElement();

				w.WriteStartElement("Value");
				w.WriteString(this[i].Value);
				w.WriteEndElement();

				w.WriteEndElement();
			}
		}

		public System.Xml.Schema.XmlSchema GetSchema()
		{
			// TODO:  Add ItemsItemCollection.GetSchema implementation
			return null;
		}

		public void ReadXml(System.Xml.XmlReader r)
		{
			ItemsItem item;

			if (!r.IsEmptyElement)
			{
				r.ReadStartElement("Items");

				if (!r.IsEmptyElement)
				{
					while (r.Name == "Item")
					{		
						r.ReadStartElement("Item");

						item = new ItemsItem();

						if (!r.IsEmptyElement)
						{
							r.ReadStartElement("Display");
							item.Display = r.ReadString();
							r.ReadEndElement();
						}
						else
						{
							r.Read();
						}

						if (!r.IsEmptyElement)
						{
							r.ReadStartElement("Value");
							item.Value = r.ReadString();
							r.ReadEndElement();
						}
						else
						{
							r.Read();
						}

						r.ReadEndElement();

						this.Add(item);
					}

					r.ReadEndElement();
				}
				else
				{
					r.Read();
				}
			}
			else
			{
				r.Read();
			}
		}

		#endregion
	}

}