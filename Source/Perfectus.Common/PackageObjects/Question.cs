using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for Question.
    /// </summary>
    [Serializable]
    public sealed class Question : AnswerProvider, ITemplateItem, IPageItem, ISerializable, IStringBindingItem
    {
        private string formatString = string.Empty;
        private string formatStringParams = string.Empty;
        private string interviewFormatString = string.Empty;
        private string interviewFormatStringParams = string.Empty;
        private TextQuestionQueryValue prompt = new TextQuestionQueryValue();
        private QuestionDisplayType displayType;
        private TextQuestionQueryValue parameter = new TextQuestionQueryValue();

        [XmlArrayItem("ItemsItem", typeof (ItemsItem))] [XmlArray("Items")] private ItemsItemCollection items;
        private TextQuestionQueryValue defaultAnswer = new TextQuestionQueryValue();
        private DataBindingInfo dataBindings;
        private TextQuestionQueryValue hint = new TextQuestionQueryValue();
        private TextQuestionQueryValue example = new TextQuestionQueryValue();
        private string help;
        private YesNoQueryValue mandatory = new YesNoQueryValue();
        private RegexUrlValue validation;
        private TextQuestionQueryValue validationErrorMessage = new TextQuestionQueryValue();
        private int dataSize = 255;
        private string repeatAnswerSeparator;
        private string repeatAnswerSeparatorFinal;
        private YesNoQueryValue visible = new YesNoQueryValue();
        private YesNoQueryValue enabled = new YesNoQueryValue();
        private int displayWidth = 50;
        private int displayHeight = 2;
        private string cssClassName;
        private AnswerCollectionCollection answersByPerson = new AnswerCollectionCollection();
        private AnswerCollectionCollection answersBySystem = new AnswerCollectionCollection();
        private AnswerCollectionCollection answersByAuthor = new AnswerCollectionCollection();
        private string serviceCallButtonText = null;
        private TextQuestionQueryValue prefix = new TextQuestionQueryValue();
        private TextQuestionQueryValue suffix = new TextQuestionQueryValue();
        private ListDirection direction = ListDirection.Vertical;
        private bool seen = false;
        private YesNoQueryValue defaultAnswerAppliesInAssembly = new YesNoQueryValue();
        private TextQuestionQueryValue mirrorMapping = new TextQuestionQueryValue();
        private YesNoQueryValue mirrorPrimaryInclude = new YesNoQueryValue();
        private YesNoQueryValue mirrorSecondaryInclude = new YesNoQueryValue();
        private string tag = null;
        private string previewHoverText = string.Empty;

        // Defines the Shared Library type for this class
        private LibraryItemType _LibraryItemType = LibraryItemType.Question;

        public enum RepeaterAnswerType
        {
            Complex = 0,
            NonComplex
        }

        #region Properties

        [Category("SharedLibrary")]
        [Browsable(false)]
        public override LibraryItemType LibraryType
        {
            get { return _LibraryItemType; }
            set { _LibraryItemType = value; }
        }

        public enum ListDirection
        {
            Horizontal,
            Vertical
        } ;

        [Browsable(false)]
        public bool Seen
        {
            get { return seen; }
            set { seen = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [DisplayRanking( 18 )]
        public string ServiceCallButtonText
        {
            get { return serviceCallButtonText; }
            set { serviceCallButtonText = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 2 )]
        public TextQuestionQueryValue Prompt
        {
            get { return prompt; }
            set { prompt = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [TypeConverter(
            "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.PerfectusDataTypeConverter, Studio")]
        [RefreshProperties(RefreshProperties.All)]
        [DisplayRanking( 4 )]
        public override PerfectusDataType DataType
        {
            get { return m_dataType; }
            set
            {
                m_dataType = value;

                if (!(PerfectusTypeSystem.IsValidDisplayType(this, displayType)))
                {
                    DisplayType = PerfectusTypeSystem.GetDefaultDisplayType(value);
                    FormatString = string.Empty;
                    InterviewFormatString = string.Empty;
                }
            }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.ItemsEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.ItemsConverter, Studio")]
        [RefreshProperties(RefreshProperties.All)]
        [DisplayRanking( 12 )]
        public ItemsItemCollection Items
        {
            get
            {
                if (items != null && items.Count > 0)
                {
                    return items;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                items = value;
                if (!(PerfectusTypeSystem.IsValidDisplayType(this, displayType)))
                {
                    DisplayType = PerfectusTypeSystem.GetDefaultDisplayType(m_dataType);
                }
            }
        }

        [Category("Data")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.QuestionDisplayTypeConverter, Studio")]
        [DisplayRanking( 6 )]
        public QuestionDisplayType DisplayType
        {
            get { return displayType; }
            set { displayType = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 8 )]
        public TextQuestionQueryValue DefaultAnswer
        {
            get { return defaultAnswer; }
            set
            {
                defaultAnswer = value;

                // Just one default answer allowed at the moment.
                AnswerCollection coll = new AnswerCollection(new object[] {defaultAnswer});
                answersByAuthor = new AnswerCollectionCollection(new AnswerCollection[] {coll});
            }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 10 )]
        public YesNoQueryValue DefaultAnswerAppliesInAssembly
        {
            get { return defaultAnswerAppliesInAssembly; }
            set { defaultAnswerAppliesInAssembly = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 27 )]
        public TextQuestionQueryValue Hint
        {
            get { return hint; }
            set { hint = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 28 )]
        public TextQuestionQueryValue Example
        {
            get { return example; }
            set { example = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.StringEditor, Studio", typeof (UITypeEditor))]
        [DisplayRanking( 30 )]
        public string Help
        {
            get { return help; }
            set { help = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 20 )]
        public YesNoQueryValue Mandatory
        {
            get { return mandatory; }
            set { mandatory = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.DataBindingsEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.BindingInfoConverter, Studio")]
        [DisplayRanking( 16 )]
        public DataBindingInfo DataBindings
        {
            get { return dataBindings; }
            set { dataBindings = value; }
        }

        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.RegexUrlEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.RegexUrlConverter, Studio")]
        [Category("Data")]
        [Browsable(true)]
        [DisplayRanking( 25 )]
        public RegexUrlValue Validation
        {
            get { return validation; }
            set { validation = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 26 )]
        public TextQuestionQueryValue ValidationErrorMessage
        {
            get { return validationErrorMessage; }
            set { validationErrorMessage = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [DisplayRanking( 43 )]
        public int DataSize
        {
            get { return dataSize; }
            set { dataSize = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 48 )]
        public TextQuestionQueryValue Parameter
        {
            get { return parameter; }
            set { parameter = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter, Studio")]
        [DisplayRanking( 36 )]
        public string RepeatAnswerSeparator
        {
            get { return repeatAnswerSeparator; }
            set { repeatAnswerSeparator = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter, Studio")]
        [DisplayRanking( 37 )]
        public string RepeatAnswerSeparatorFinal
        {
            get { return repeatAnswerSeparatorFinal; }
            set { repeatAnswerSeparatorFinal = value; }
        }

        [Category("Display")]
        [Browsable(false)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.CssNameConverter, Studio")]
        [Obsolete]
        public string CssClassName
        {
            get { return cssClassName; }
            set { cssClassName = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 22 )]
        public YesNoQueryValue Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 24 )]
        public YesNoQueryValue Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [DisplayRanking( 46 )]
        public int DisplayWidth
        {
            get { return displayWidth; }
            set { displayWidth = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 31 )]
        public TextQuestionQueryValue Prefix
        {
            get { return prefix; }
            set { prefix = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 32 )]
        public TextQuestionQueryValue Suffix
        {
            get { return suffix; }
            set { suffix = value; }
        }


        [Category("Display")]
        [Browsable(true)]
        [DisplayRanking( 44 )]
        public int DisplayHeight
        {
            get { return displayHeight; }
            set { displayHeight = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.FormatStringEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FormatStringConverter, Studio")]
        [DisplayRanking( 34 )]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string FormatString
        {
            get { return formatString; }
            set { formatString = value; }
        }

        [Category("Display")]
        [Browsable(false)]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string FormatStringParams
        {
            get { return formatStringParams; }
            set { formatStringParams = value; }
        }


        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.FormatStringEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FormatStringConverter, Studio")]
        [DisplayRanking( 33 )]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string InterviewFormatString
        {
            get { return interviewFormatString; }
            set { interviewFormatString = value; }
        }

        [Category("Display")]
        [Browsable(false)]
        // If you rename this property, remember that FormatStringEditorForm uses its name to know what its doing.
        public string InterviewFormatStringParams
        {
            get { return interviewFormatStringParams; }
            set { interviewFormatStringParams = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.ListDirectionConverter, Studio")]
        [DisplayRanking( 14 )]
        public ListDirection Direction
        {
            get { return direction; }
            set { direction = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 40 )]
        public TextQuestionQueryValue MirrorMapping
        {
            get { return mirrorMapping; }
            set { mirrorMapping = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 41 )]
        public YesNoQueryValue MirrorPrimaryInclude
        {
            get { return mirrorPrimaryInclude; }
            set { mirrorPrimaryInclude = value; }
        }

        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 42 )]
        public YesNoQueryValue MirrorSecondaryInclude
        {
            get { return mirrorSecondaryInclude; }
            set { mirrorSecondaryInclude = value; }
        }

        [Browsable(false)]
        public string Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        #endregion

        public Question()
        {
            Name =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Question.NewItemName");
            Init();
        }

        public Question(string name)
        {
            Name = name;
            Init();
        }

        private void Init()
        {
            visible = new YesNoQueryValue();
            mandatory = new YesNoQueryValue();
            enabled = new YesNoQueryValue();
            defaultAnswerAppliesInAssembly = new YesNoQueryValue();
            mirrorPrimaryInclude = new YesNoQueryValue();
            mirrorSecondaryInclude = new YesNoQueryValue();
            prompt = new TextQuestionQueryValue();
            hint = new TextQuestionQueryValue();
            validationErrorMessage = new TextQuestionQueryValue();
            prefix = new TextQuestionQueryValue();
            suffix = new TextQuestionQueryValue();
            example = new TextQuestionQueryValue();
            hint = new TextQuestionQueryValue();
            defaultAnswer = new TextQuestionQueryValue();
            validationErrorMessage = new TextQuestionQueryValue();

            visible.YesNoValue = true;
            mandatory.YesNoValue = false;
            enabled.YesNoValue = true;
            DefaultAnswerAppliesInAssembly.YesNoValue = true;
            mirrorPrimaryInclude.YesNoValue = true;
            mirrorSecondaryInclude.YesNoValue = true;

            m_dataType = PerfectusDataType.Text;
            DisplayType = QuestionDisplayType.TextBox;
        }

        //SL - Update Question
        #region Shared Library

        /// <summary>
        ///		Popluates a collection of library items which are dependent on the current question object instance 
        /// </summary>
        /// <returns>A populated collection of dependent library items of this question.</returns>
        public override List<LibraryItem> GetDependentLibraryItems()
        {
            List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();

            // Update TextQuestionQueryValue properties.
            Prompt.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            DefaultAnswer.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            Hint.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            Example.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            MirrorMapping.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            Prefix.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            Suffix.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            ValidationErrorMessage.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);
            Parameter.SetDependentLibraryItems(dependentLibraryItems, ParentPackage);

            // Update YesNoQuery properties.
            DefaultAnswerAppliesInAssembly.SetDependentLibraryItems(dependentLibraryItems);
            Mandatory.SetDependentLibraryItems(dependentLibraryItems);
            Enabled.SetDependentLibraryItems(dependentLibraryItems);
            MirrorPrimaryInclude.SetDependentLibraryItems(dependentLibraryItems);
            MirrorSecondaryInclude.SetDependentLibraryItems(dependentLibraryItems);
            Visible.SetDependentLibraryItems(dependentLibraryItems);

            return dependentLibraryItems;
        }

        public override List<LibraryItem> SetDependentLibraryItems()
        {
            return null;
        }

        public void Update(Question question)
        {
            // Shared Library Item Properties
            LibraryUniqueIdentifier = question.LibraryUniqueIdentifier;
            Version = question.Version;
            LibraryType = question.LibraryType;
            Linked = question.Linked;
            CreatedBy = question.CreatedBy;
            CreatedDateTime = question.CreatedDateTime;
            CheckedOutBy = question.CheckedOutBy;
            CheckedOutByLogInName = question.CheckedOutByLogInName;
            CheckedOutDateTime = question.CheckedOutDateTime;
            CheckedOutStatus = question.CheckedOutStatus;
            ModifiedBy = question.ModifiedBy;
            ModifiedDateTime = question.ModifiedDateTime;
            CheckInComment = question.CheckInComment;
            LocationPath = question.LocationPath;
            RelativePath = question.RelativePath;
            Filename = question.Filename;
            DependentLibraryItems = question.DependentLibraryItems;

            // Text Question Values
            question.Prompt.ReconnectLibraryItems(ParentPackage);
            Prompt = question.Prompt;
            question.Hint.ReconnectLibraryItems(ParentPackage);
            Hint = question.Hint;
            question.Parameter.ReconnectLibraryItems(ParentPackage);
            Parameter = question.Parameter;
            question.Prefix.ReconnectLibraryItems(ParentPackage);
            Prefix = question.Prefix;
            question.Suffix.ReconnectLibraryItems(ParentPackage);
            Suffix = question.Suffix;
            question.DefaultAnswer.ReconnectLibraryItems(ParentPackage);
            DefaultAnswer = question.DefaultAnswer;
            question.Example.ReconnectLibraryItems(ParentPackage);
            Example = question.Example;
            question.ValidationErrorMessage.ReconnectLibraryItems(ParentPackage);
            ValidationErrorMessage = question.ValidationErrorMessage;
            
            // Update YesNoQuery properties
            question.Mandatory.ReconnectLibraryItems(ParentPackage);
            Mandatory = question.Mandatory;
            question.Enabled.ReconnectLibraryItems(ParentPackage);
            Enabled = question.Enabled;
            question.Visible.ReconnectLibraryItems(ParentPackage);
            Visible = question.Visible;
            question.MirrorPrimaryInclude.ReconnectLibraryItems(ParentPackage);
            MirrorPrimaryInclude = question.MirrorPrimaryInclude;
            question.MirrorSecondaryInclude.ReconnectLibraryItems(ParentPackage);
            MirrorSecondaryInclude = question.MirrorSecondaryInclude;
            question.DefaultAnswerAppliesInAssembly.ReconnectLibraryItems(ParentPackage);
            DefaultAnswerAppliesInAssembly = question.DefaultAnswerAppliesInAssembly;
            
            // Base properties

            if (Name != question.Name)
            {
                Name = question.Name;
            }

            Touch = question.Touch;
            Notes = question.Notes;
            DataType = question.DataType;
            Seen = question.Seen;
            DisplayType = question.DisplayType;
            DataSize = question.DataSize;
            DisplayWidth = question.DisplayWidth;
            DisplayHeight = question.DisplayHeight;
            FormatString = question.FormatString;
            InterviewFormatString = question.InterviewFormatString;
            Direction = question.Direction;
            Help = question.Help;
            Validation = question.Validation;
            RepeatAnswerSeparator = question.RepeatAnswerSeparator;
            RepeatAnswerSeparatorFinal = question.RepeatAnswerSeparatorFinal;
            ServiceCallButtonText = question.ServiceCallButtonText;
            Items = question.Items;
            FormatStringParams = question.FormatStringParams;
            InterviewFormatStringParams = question.InterviewFormatStringParams;
            previewHoverText = question.previewHoverText;
            DataBindings = ReconnectDataBindings(question.DataBindings);
        }

        #endregion

        public void AddWordMLTagToObject(DataObject inObj)
        {
            WordTemplateDocument2.AddWordMLTagToDataObject(inObj, this, SafeTagName);
        }

        /// <summary>
        ///     Assesses whether the given question represents a complex repeater question (checkbox list & multi-select list) 
        ///     or a non-complex repeater question (textbox etc).
        ///     Questions are considered to be repeated regardless of their package settings. A question therefore can be 
        ///     repeated one time (basically non-repeated), the code does not make a distinction.
        /// </summary>
        public RepeaterAnswerType GetRepeaterAnswerComplexity()
        {
            switch (displayType)
            {
                case QuestionDisplayType.CheckBoxList:
                case QuestionDisplayType.MultiSelectListBox:
                    // Checkbox list & multi-select listbox are considered complex repeater questions
                    return RepeaterAnswerType.Complex;
                default:
                    // Everything else is considered non-complex repeater questions
                    return RepeaterAnswerType.NonComplex;
            }
        }

        #region ISerializable Members

        public Question(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            displayType = (QuestionDisplayType) info.GetValue("displayType", typeof (QuestionDisplayType));
            items = (ItemsItemCollection) info.GetValue("items", typeof (ItemsItemCollection));
            dataBindings = (DataBindingInfo) info.GetValue("dataBindings", typeof (DataBindingInfo));
            mandatory = (YesNoQueryValue) info.GetValue("mandatory", typeof (YesNoQueryValue));
            validation = (RegexUrlValue) info.GetValue("validation", typeof (RegexUrlValue));
            dataSize = info.GetInt32("dataSize");
            repeatAnswerSeparator = info.GetString("repeatAnswerSeparator");
            visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
            enabled = (YesNoQueryValue) info.GetValue("enabled", typeof (YesNoQueryValue));
            displayWidth = info.GetInt32("displayWidth");
            displayHeight = info.GetInt32("displayHeight");
            cssClassName = info.GetString("cssClassName");

            // handle strings that have been changed to TextQuestionQueryValue (in ver 5.1)
            prompt = DeserializeTQQV("prompt", info);
            hint = DeserializeTQQV("hint", info);
            example = DeserializeTQQV("example", info);
            validationErrorMessage = DeserializeTQQV("validationErrorMessage", info);
            prefix = DeserializeTQQV("prefix", info);
            suffix = DeserializeTQQV("suffix", info);
            defaultAnswer = DeserializeTQQV("defaultAnswer", info);
            parameter = DeserializeTQQV("parameter", info);

            try
            {
                help = info.GetString("help");
            }
            catch
            {
            }
            try
            {
                answersByPerson =
                    (AnswerCollectionCollection) info.GetValue("answers", typeof (AnswerCollectionCollection));
            }
            catch
            {
            }

            try
            {
                serviceCallButtonText = info.GetString("serviceCallButtonText");
            }
            catch
            {
            }
            try
            {
                formatString = info.GetString("formatString");
            }
            catch
            {
                formatString = string.Empty;
            }
            try
            {
                interviewFormatString = info.GetString("interviewFormatString");
            }
            catch
            {
                interviewFormatString = string.Empty;
            }
            try
            {
                formatStringParams = info.GetString("formatStringParams");
            }
            catch
            {
                formatStringParams = string.Empty;
            }
            try
            {
                interviewFormatStringParams = info.GetString("interviewFormatStringParams");
            }
            catch
            {
                interviewFormatStringParams = string.Empty;
            }
            try
            {
                direction = (ListDirection) info.GetValue("direction", typeof (ListDirection));
            }
            catch
            {
            }
            try
            {
                seen = info.GetBoolean("seen");
            }
            catch
            {
            }
            try
            {
                answersBySystem =
                    (AnswerCollectionCollection) info.GetValue("answersBySystem", typeof (AnswerCollectionCollection));
            }
            catch
            {
            }
            try
            {
                answersByAuthor =
                    (AnswerCollectionCollection) info.GetValue("answersByAuthor", typeof (AnswerCollectionCollection));
                if (answersByAuthor != null && answersByAuthor.Count == 0 && defaultAnswer != null)
                {
                    AnswerCollection coll = new AnswerCollection(new object[] {defaultAnswer});
                    answersByAuthor = new AnswerCollectionCollection(new AnswerCollection[] {coll});
                }
            }
            catch
            {
                if (defaultAnswer != null)
                {
                    AnswerCollection coll = new AnswerCollection(new object[] {defaultAnswer});
                    answersByAuthor = new AnswerCollectionCollection(new AnswerCollection[] {coll});
                }
            }

            try
            {
                defaultAnswerAppliesInAssembly =
                    (YesNoQueryValue) info.GetValue("defaultAnswerAppliesInAssembly", typeof (YesNoQueryValue));
            }
            catch
            {
                defaultAnswerAppliesInAssembly = new YesNoQueryValue();
                defaultAnswerAppliesInAssembly.YesNoValue = true;
            }
            try
            {
                mirrorMapping = (TextQuestionQueryValue) info.GetValue("mirrorMapping", typeof (TextQuestionQueryValue));
                if (mirrorMapping == null)
                {
                    mirrorMapping = new TextQuestionQueryValue();
                }
            }
            catch
            {
            }
            try
            {
                mirrorPrimaryInclude = (YesNoQueryValue) info.GetValue("mirrorPrimaryInclude", typeof (YesNoQueryValue));
                if (mirrorPrimaryInclude == null)
                {
                    mirrorPrimaryInclude = new YesNoQueryValue();
                }
            }
            catch
            {
                mirrorPrimaryInclude = new YesNoQueryValue();
            }
            try
            {
                mirrorSecondaryInclude =
                    (YesNoQueryValue) info.GetValue("mirrorSecondaryInclude", typeof (YesNoQueryValue));
                if (mirrorSecondaryInclude == null)
                {
                    mirrorSecondaryInclude = new YesNoQueryValue();
                }
            }
            catch
            {
                mirrorSecondaryInclude = new YesNoQueryValue();
            }
            try
            {
                repeatAnswerSeparatorFinal = info.GetString("repeatAnswerSeparatorFinal");
            }
            catch
            {
            }
            try
            {
                previewHoverText = info.GetString("previewHoverText");
            }
            catch
            {
                previewHoverText = string.Empty;
            }
        }

        private DataBindingInfo ReconnectDataBindings(DataBindingInfo dataBindingInfoSL)
        {
            if (dataBindingInfoSL == null)
            {
                return null;
            }

            string[] webRefernceInfo;

            if (dataBindingInfoSL.AnswerBinding != null)
            {
                webRefernceInfo = dataBindingInfoSL.AnswerBinding.Split("|".ToCharArray());
                EnsureBindings(dataBindingInfoSL, BindingProperty.Answer);
            }

            if (dataBindingInfoSL.ItemsValueBinding != null)
            {
                EnsureBindings(dataBindingInfoSL, BindingProperty.ItemsValue);
            }

            if (dataBindingInfoSL.ParameterBindings != null && dataBindingInfoSL.ParameterBindings.Length > 0)
            {
                EnsureBindings(dataBindingInfoSL, BindingProperty.Parameters);
            }

            return dataBindingInfoSL;
        }

        private void EnsureBindings(DataBindingInfo dataBindingInfoSL, BindingProperty bindingProperty)
        {
            WebReferenceComponent addressComponent = WebReferenceComponent.Address;
            WebReferenceComponent guidComponent = WebReferenceComponent.GUID;
            WebReferenceComponent bindingInfoComponent = WebReferenceComponent.BindingInformation;

            string address = string.Empty;
            string guidString = string.Empty;
            string bindingInfo = string.Empty;

            switch (bindingProperty)
            { 
                case BindingProperty.Answer:
                    address = dataBindingInfoSL.GetWebServiceComponentForAnswerBinding(addressComponent);
                    guidString = dataBindingInfoSL.GetWebServiceComponentForAnswerBinding(guidComponent);
                    bindingInfo = dataBindingInfoSL.GetWebServiceComponentForAnswerBinding(bindingInfoComponent);
                    break;
                case BindingProperty.ItemsDisplay:
                    address = dataBindingInfoSL.GetWebServiceComponentForItemsDisplayBinding(addressComponent);
                    guidString = dataBindingInfoSL.GetWebServiceComponentForItemsDisplayBinding(guidComponent);
                    bindingInfo = dataBindingInfoSL.GetWebServiceComponentForItemsDisplayBinding(bindingInfoComponent);
                    break;
                case BindingProperty.ItemsValue:
                    address = dataBindingInfoSL.GetWebServiceComponentForItemsValueBinding(addressComponent);
                    guidString = dataBindingInfoSL.GetWebServiceComponentForItemsValueBinding(guidComponent);
                    bindingInfo = dataBindingInfoSL.GetWebServiceComponentForItemsValueBinding(bindingInfoComponent);
                    break;
                case BindingProperty.Parameters:
                    address = dataBindingInfoSL.GetWebServiceComponentForParameterBindings(addressComponent, 0);
                    guidString = dataBindingInfoSL.GetWebServiceComponentForParameterBindings(guidComponent, 0);
                    bindingInfo = dataBindingInfoSL.GetWebServiceComponentForParameterBindings(bindingInfoComponent, 0);
                    break;
            }

            if (address.StartsWith("http://") || address.StartsWith("https://"))
            {
                Guid guid;
                if (Globals.IsGuid(guidString, out guid))
                {
                    Guid guidNew = this.ParentPackage.EnsureWebReferenceExists(address, guid);
                    // Did we find a service by name only?
                    if (guidNew.CompareTo(guid) != 0)
                    {
                        switch (bindingProperty)
                        {
                            case BindingProperty.Answer:
                                dataBindingInfoSL.AnswerBinding = dataBindingInfoSL.AnswerBinding.Replace(guid.ToString(), guidNew.ToString());
                                break;
                            case BindingProperty.ItemsDisplay:
                                dataBindingInfoSL.ItemsDisplayBinding=dataBindingInfoSL.ItemsDisplayBinding.Replace(guid.ToString(), guidNew.ToString());
                                break;
                            case BindingProperty.ItemsValue:
                                dataBindingInfoSL.ItemsValueBinding=dataBindingInfoSL.ItemsValueBinding.Replace(guid.ToString(), guidNew.ToString());
                                break;
                            case BindingProperty.Parameters:
                                for(int i = 0; i < dataBindingInfoSL.ParameterBindings.Length;i++ )
                                    dataBindingInfoSL.ParameterBindings[i]=dataBindingInfoSL.ParameterBindings[i].Replace(guid.ToString(), guidNew.ToString());
                                break;
                        }


                        // Then we have to rework the binding information
                    }
                }
            }
            else
            {
                // Raise event that the data binding cannot be reconnected.
                PackageItemEventArgs eventArgs = new PackageItemEventArgs(this);
                this.ParentPackage.RaiseBindingsCannotBeReconnected(eventArgs);
            }
        }

        private static TextQuestionQueryValue DeserializeTQQV(string valToGet, SerializationInfo info)
        {
            // Lots of strings were changed to be TextQuestionQueryValues in 5.1. putting these TQV's through this method ensures everything is happy.
            TextQuestionQueryValue tqv = null;

            try
            {
                tqv = (TextQuestionQueryValue)info.GetValue(valToGet, typeof(TextQuestionQueryValue));
            }
            catch
            {
                // Nested try catch because it is possible that there are packages made before the property was even a string.
                try
                {
                    string strValToGet = info.GetString(valToGet);
                    tqv = new TextQuestionQueryValue();
                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    tqv.TextValue = strValToGet;
                }
                catch
                {
                }
            }
            finally
            {
                if (tqv == null)
                {
                    tqv = new TextQuestionQueryValue();
                }
                if (tqv.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text && tqv.TextValue == null)
                {
                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.NoValue;
                }
            }

            return tqv;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("prompt", prompt);
            info.AddValue("displayType", displayType);
            info.AddValue("items", items);
            info.AddValue("defaultAnswer", defaultAnswer);
            info.AddValue("dataBindings", dataBindings);
            info.AddValue("hint", hint);
            info.AddValue("example", example);
            info.AddValue("help", help);
            info.AddValue("mandatory", mandatory);
            info.AddValue("validation", validation);
            info.AddValue("validationErrorMessage", validationErrorMessage);
            info.AddValue("dataSize", dataSize);
            info.AddValue("repeatAnswerSeparator", repeatAnswerSeparator);
            info.AddValue("repeatAnswerSeparatorFinal", repeatAnswerSeparatorFinal);
            info.AddValue("visible", visible);
            info.AddValue("enabled", enabled);
            info.AddValue("displayWidth", displayWidth);
            info.AddValue("displayHeight", displayHeight);
            info.AddValue("cssClassName", cssClassName);
            info.AddValue("answers", answersByPerson);
            info.AddValue("serviceCallButtonText", serviceCallButtonText);
            info.AddValue("formatString", formatString);
            info.AddValue("interviewFormatString", interviewFormatString);
            info.AddValue("formatStringParams", formatStringParams);
            info.AddValue("interviewFormatStringParams", interviewFormatStringParams);
            info.AddValue("prefix", prefix);
            info.AddValue("suffix", suffix);
            info.AddValue("direction", direction);
            info.AddValue("seen", seen);
            info.AddValue("answersBySystem", answersBySystem);
            info.AddValue("answersByAuthor", answersByAuthor);
            info.AddValue("defaultAnswerAppliesInAssembly", defaultAnswerAppliesInAssembly);
            info.AddValue("mirrorMapping", mirrorMapping);
            info.AddValue("mirrorPrimaryInclude", mirrorPrimaryInclude);
            info.AddValue("mirrorSecondaryInclude", mirrorSecondaryInclude);
            info.AddValue("previewHoverText", previewHoverText);
            info.AddValue("parameter", parameter);
        }

        #endregion

        #region ITemplateItem Members

        public string GetTagName()
        {
            return SafeTagName;
        }

        public string GetTagContents()
        {
            return m_name;
        }

        [Category("Display")]
        [Browsable(true)]
        [DisplayRanking( 38 )]
        public string PreviewHoverText
        {
            get { return previewHoverText; }
            set
            {
                previewHoverText = value;
                OnPropertyChanged("PreviewHoverText");
            }
        }

        #endregion

        #region IStringBindingItem Members

        public void AddHTMLTagToObject(DataObject inObj, string fieldName)
        {
            StringBinding.AddHTMLTagToDataObject(inObj, this, fieldName);
        }

        #endregion

        #region Localisation

        public static string LocaliseQuestionDirection(ListDirection direction)
        {
            string keyName =
                string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.ListDirection.{0}", direction);
            return ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
        }

        public static ListDirection DelocaliseDirection(string directionName)
        {
            foreach (ListDirection ld in (ListDirection[]) (Enum.GetValues(typeof (ListDirection))))
            {
                if (LocaliseQuestionDirection(ld) == directionName)
                    //				if (ld.ToString() == directionName)
                {
                    return ld;
                }
            }
            return ListDirection.Vertical;
        }

        #endregion

        #region ICloneable members

        public override object Clone()
        {
            object newObj = MemberwiseClone();
            Question newQ = (Question) newObj;
            newQ.Visible = (YesNoQueryValue) visible.Clone();
            newQ.Enabled = (YesNoQueryValue) enabled.Clone();
            newQ.Mandatory = (YesNoQueryValue) mandatory.Clone();
            newQ.defaultAnswerAppliesInAssembly = (YesNoQueryValue) defaultAnswerAppliesInAssembly.Clone();
            // Safe to do, as no answers exist in IPManager.
            newQ.answersByPerson = new AnswerCollectionCollection();
            newQ.AnswersBySystem = new AnswerCollectionCollection();

            newQ.MirrorMapping = (TextQuestionQueryValue) mirrorMapping.Clone();
            newQ.Prompt = (TextQuestionQueryValue) prompt.Clone();
            newQ.Hint = (TextQuestionQueryValue) hint.Clone();
            newQ.Example = (TextQuestionQueryValue) example.Clone();
            newQ.Prefix = (TextQuestionQueryValue) prefix.Clone();
            newQ.Suffix = (TextQuestionQueryValue) suffix.Clone();
            newQ.ValidationErrorMessage = (TextQuestionQueryValue) validationErrorMessage.Clone();
            newQ.DefaultAnswer = (TextQuestionQueryValue) defaultAnswer.Clone();
            newQ.Parameter = (TextQuestionQueryValue) parameter.Clone();

            newQ.MirrorPrimaryInclude = (YesNoQueryValue) mirrorPrimaryInclude.Clone();
            newQ.MirrorSecondaryInclude = (YesNoQueryValue) mirrorSecondaryInclude.Clone();


            if (dataBindings != null)
            {
                newQ.DataBindings = (DataBindingInfo) dataBindings.Clone();
            }

            if (validation != null)
            {
                newQ.Validation = (RegexUrlValue) validation.Clone();
            }
            return newQ;
        }

        #endregion

        #region Answers

        [Browsable(false)]
        public int GetAnswerCount(Caller caller)
        {
            AnswerCollectionCollection collColl = GetAnswerCollectionCollection(caller);
            if (collColl != null)
            {
                return collColl.Count;
            }
            else
            {
                return 0;
            }
        }


        public void SetAnswers(AnswerCollectionCollection coll, Caller caller)
        {
            answersByPerson = coll;
            if (caller == Caller.System)
            {
                answersBySystem = coll;
            }
        }

        private AnswerCollectionCollection GetAnswerCollectionCollectionForCaller(Caller caller)
        {
            switch (caller)
            {
                case Caller.Interview:
                    return CoalesceAnswerCollectionCollections(answersByPerson, answersBySystem, answersByAuthor);
                default:
                case Caller.Assembly:
                    bool useDefault = defaultAnswerAppliesInAssembly.Evaluate(true, Caller.System);
                    if (seen)
                    {
                        if (useDefault)
                        {
                            return
                                CoalesceAnswerCollectionCollections(answersByPerson, answersBySystem, answersByAuthor);
                        }
                        else
                        {
                            return CoalesceAnswerCollectionCollections(answersByPerson, answersBySystem);
                        }
                    }
                    else
                    {
                        if (useDefault)
                        {
                            return CoalesceAnswerCollectionCollections(answersBySystem, answersByAuthor);
                        }
                        else
                        {
                            return CoalesceAnswerCollectionCollections(answersBySystem);
                        }
                    }
                case Caller.System:
                    if (seen)
                    {
                        return CoalesceAnswerCollectionCollections(answersByPerson, answersBySystem, answersByAuthor);
                    }
                    else
                    {
                        return CoalesceAnswerCollectionCollections(answersBySystem, answersByAuthor);
                    }
            }
        }

        private static AnswerCollectionCollection CoalesceAnswerCollectionCollections(
            params AnswerCollectionCollection[] collections)
        {
            for (int i = 0; i < collections.Length; i++)
            {
                if (collections[i] != null && collections[i].Count > 0)
                {
                    return collections[i];
                }
            }
            // No collections had an answer.
            return null;
        }

        public AnswerCollection GetAnswer(int index, Caller caller)
        {
            AnswerCollectionCollection coll = GetAnswerCollectionCollectionForCaller(caller);
            if (coll == null || index < 0 || index > coll.Count - 1)
            {
                return null;
            }
            else
            {
                return coll[index];
            }
        }

        public override object GetSingleAnswer(Caller c)
        {
            AnswerCollection firstAnswer = GetAnswer(0, Caller.System);
            if (firstAnswer != null && firstAnswer.Count > 0)
            {
                return ConvertToBestType(firstAnswer[0], DataType, c, StringBindingMetaData);
            }
            else
            {
                return null;
            }
        }

        public AnswerCollectionCollection GetAnswerCollectionCollection(Caller caller)
        {
            return GetAnswerCollectionCollectionForCaller(caller);
        }

        [Browsable(false)]
        public AnswerCollectionCollection AnswersByPerson
        {
            get { return answersByPerson; }
            set { answersByPerson = value; }
        }

        [Browsable(false)]
        public AnswerCollectionCollection AnswersBySystem
        {
            get { return answersBySystem; }
            set { answersBySystem = value; }
        }

        #endregion
    }
}
