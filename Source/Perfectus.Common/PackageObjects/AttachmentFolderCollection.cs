using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type AttachmentFolder
	/// </summary>
	[Serializable]
	public sealed class AttachmentFolderCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the AttachmentFolderCollection class.
		/// </summary>
		internal AttachmentFolderCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the AttachmentFolderCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new AttachmentFolderCollection.
		/// </param>
		internal AttachmentFolderCollection(AttachmentFolder[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the AttachmentFolderCollection class, containing elements
		/// copied from another instance of AttachmentFolderCollection
		/// </summary>
		/// <param name="items">
		/// The AttachmentFolderCollection whose elements are to be added to the new AttachmentFolderCollection.
		/// </param>
		internal AttachmentFolderCollection(AttachmentFolderCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this AttachmentFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this AttachmentFolderCollection.
		/// </param>
		internal void AddRange(AttachmentFolder[] items)
		{
			foreach (AttachmentFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another AttachmentFolderCollection to the end of this AttachmentFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The AttachmentFolderCollection whose elements are to be added to the end of this AttachmentFolderCollection.
		/// </param>
		internal void AddRange(AttachmentFolderCollection items)
		{
			foreach (AttachmentFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type AttachmentFolder to the end of this AttachmentFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The AttachmentFolder to be added to the end of this AttachmentFolderCollection.
		/// </param>
		public void Add(AttachmentFolder value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic AttachmentFolder value is in this AttachmentFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The AttachmentFolder value to locate in this AttachmentFolderCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this AttachmentFolderCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(AttachmentFolder value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this AttachmentFolderCollection
		/// </summary>
		/// <param name="value">
		/// The AttachmentFolder value to locate in the AttachmentFolderCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(AttachmentFolder value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the AttachmentFolderCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the AttachmentFolder is to be inserted.
		/// </param>
		/// <param name="value">
		/// The AttachmentFolder to insert.
		/// </param>
		internal void Insert(int index, AttachmentFolder value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the AttachmentFolder at the given index in this AttachmentFolderCollection.
		/// </summary>
		public AttachmentFolder this[int index]
		{
			get { return (AttachmentFolder) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific AttachmentFolder from this AttachmentFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The AttachmentFolder value to remove from this AttachmentFolderCollection.
		/// </param>
		internal void Remove(AttachmentFolder value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by AttachmentFolderCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(AttachmentFolderCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public AttachmentFolder Current
			{
				get { return (AttachmentFolder) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (AttachmentFolder) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this AttachmentFolderCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}
