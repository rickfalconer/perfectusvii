using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for YesNoAction.
    /// </summary>
    [Serializable]
    public sealed class HtmlStringBindingAction : ActionBase, ISerializable
    {
        private String payload;

        public String Payload
        {
            get { return payload; }
            set { payload = value; }
        }

        public HtmlStringBindingAction()
            : base()
        { }

        public HtmlStringBindingAction(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            payload = info.GetString("payload");
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("payload", payload);
        }

        public override bool ContainsReferenceTo(PackageItem item)
        {
            // FB2047 : If this is a query with ActionIfFalse and the 'else' clause has nothing entered.
            // the payload could be null if the query was created in a previous version. In this case it should be
            // save to return false as there is obviously no reference then.
            if( payload == null )
                return false;
            
            return payload.IndexOf( item.UniqueIdentifier.ToString( ) ) > -1;
        }

        public override object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}