using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type AnswerAcquisitionError
	/// </summary>
	public class TransientErrorCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the AnswerAcquisitionErrorCollection class.
		/// </summary>
		public TransientErrorCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the AnswerAcquisitionErrorCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new AnswerAcquisitionErrorCollection.
		/// </param>
		public TransientErrorCollection(TransientError[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the AnswerAcquisitionErrorCollection class, containing elements
		/// copied from another instance of AnswerAcquisitionErrorCollection
		/// </summary>
		/// <param name="items">
		/// The AnswerAcquisitionErrorCollection whose elements are to be added to the new AnswerAcquisitionErrorCollection.
		/// </param>
		public TransientErrorCollection(TransientErrorCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this AnswerAcquisitionErrorCollection.
		/// </param>
		public virtual void AddRange(TransientError[] items)
		{
			foreach (TransientError item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another AnswerAcquisitionErrorCollection to the end of this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <param name="items">
		/// The AnswerAcquisitionErrorCollection whose elements are to be added to the end of this AnswerAcquisitionErrorCollection.
		/// </param>
		public virtual void AddRange(TransientErrorCollection items)
		{
			foreach (TransientError item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type AnswerAcquisitionError to the end of this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerAcquisitionError to be added to the end of this AnswerAcquisitionErrorCollection.
		/// </param>
		public virtual void Add(TransientError value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic AnswerAcquisitionError value is in this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerAcquisitionError value to locate in this AnswerAcquisitionErrorCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this AnswerAcquisitionErrorCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(TransientError value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this AnswerAcquisitionErrorCollection
		/// </summary>
		/// <param name="value">
		/// The AnswerAcquisitionError value to locate in the AnswerAcquisitionErrorCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(TransientError value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the AnswerAcquisitionErrorCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the AnswerAcquisitionError is to be inserted.
		/// </param>
		/// <param name="value">
		/// The AnswerAcquisitionError to insert.
		/// </param>
		public virtual void Insert(int index, TransientError value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the AnswerAcquisitionError at the given index in this AnswerAcquisitionErrorCollection.
		/// </summary>
		public virtual TransientError this[int index]
		{
			get { return (TransientError) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific AnswerAcquisitionError from this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <param name="value">
		/// The AnswerAcquisitionError value to remove from this AnswerAcquisitionErrorCollection.
		/// </param>
		public virtual void Remove(TransientError value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by AnswerAcquisitionErrorCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(TransientErrorCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public TransientError Current
			{
				get { return (TransientError) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (TransientError) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this AnswerAcquisitionErrorCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}