using System;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Expression.
	/// </summary>
	[Serializable]
	public sealed class QueryExpression : ExpressionBase, ISerializable, IXmlSerializable
	{
		public QueryExpression() : base()
		{
		}

		public QueryExpression(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}


		[Obsolete]
		public bool Evaluate()
		{
			return Evaluate(AnswerProvider.Caller.System);
		}

		public bool Evaluate(AnswerProvider.Caller caller)
		{
			if (rootNode == null)
			{
				return true;
			}
			else
			{
				return (bool) (rootNode.Evaluate(caller));
			}
		}

		public string ToPreviewString()
		{
			if (rootNode == null)
			{
				return null;
			}
			else
			{
				if (rootNode.Operand1 != null)
					return rootNode.ToPreviewString();
				else
					return null;
			}
		}

		public string ToJavaScriptForBrowserString()
		{
			if (rootNode != null)
			{
				return rootNode.ToJavascriptForBrowserString();
			}
			else
			{
				// fallthrough
				return "true";
			}
		}

		public string ToJavaScriptForFormsString()
		{
			return rootNode.ToJavascriptForFormsString();
		}

		public override bool ValidateFromTable(DataTable dt, out string Message)
		{
			bool isOkay = true;
			Message = string.Empty;
			StringBuilder messageBuilder = new StringBuilder();
			messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.ProblemsHeader"));
			// Make sure the table's of the right structure
			if (dt.Columns[0].DataType == typeof (OperatorDisplay) &&
				dt.Columns[1].DataType == typeof (string) &&
				dt.Columns[2].DataType == typeof (PackageItem) &&
				dt.Columns[3].DataType == typeof (OperatorDisplay) &&
				dt.Columns[4].DataType == typeof (PackageItem) &&
				dt.Columns[5].DataType == typeof (string))
			{
				// Make sure there are some rows
				if (dt.Rows.Count == 0)
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.NoExpressionToValidate"));
					isOkay = false;
				}


				// Remove a bool operator from the first row
				if (dt.Rows.Count > 0)
				{
					dt.Rows[0][0] = DBNull.Value;
				}

				// Check for balanced parentheses
				if (! CheckBalancedParentheses(dt))
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.ParenthesesNotBalanced"));
					isOkay = false;
				}

				// Check that each row has two operands
				foreach (DataRow dr in dt.Rows)
				{
					if (Convert.IsDBNull(dr[2]) || dr[2] == null || Convert.IsDBNull(dr[4]) || dr[4] == null)
					{
						messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.RowWithoutTwoOperands"));
						isOkay = false;
						break;
					}
				}

				// Check that all rows after the first one have Operator1
				if (! CheckOperator1Present(dt))
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.MissingLogicOperator"));
					isOkay = false;
				}

				// Check that each row's left and right operands are of the correct type
				foreach (DataRow dr in dt.Rows)
				{
					if (! (Convert.IsDBNull(dr[2]) || dr[2] == null || Convert.IsDBNull(dr[4]) || dr[4] == null))
					{
						PackageItem op1 = (PackageItem) dr[2];
						PackageItem op2 = (PackageItem) dr[4];
						
						if (((AnswerProvider) op1).DataType != ((AnswerProvider) op2).DataType && ((PackageConstant)op2).Value.ToString() != "Null" && ((PackageConstant)op2).Value.ToString() != "Not Answered" )
						{
							messageBuilder.Append(string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.BadComparison"), op1.ToString(), op2.ToString()));
							isOkay = false;
						}
					}
				}


				// Check that each row has an operator
				foreach (DataRow dr in dt.Rows)
				{
					if (Convert.IsDBNull(dr[3]) || dr[3] == null)
					{
						messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.RowHasNoOperator"));
						isOkay = false;
//						messageBuilder.Append("\t- At least one row does not have an operator.  (>, =, etc.)\n");
					}
				}

				// Check that each row's left operand and operator are compatible
				foreach (DataRow dr in dt.Rows)
				{
					if (! (Convert.IsDBNull(dr[2]) || dr[2] == null || Convert.IsDBNull(dr[3]) || dr[3] == null))
					{
						PackageItem op1 = (PackageItem) dr[2];
						//int operand = (int)dr[3];
						OperatorDisplay opdisp = (OperatorDisplay) dr[3];
						PerfectusDataType op1dt = ((AnswerProvider) op1).DataType;
						if (! PerfectusTypeSystem.IsValidOperand(op1dt, opdisp.Op))
						{
							messageBuilder.Append(string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.InvalidComparisonOperator"), op1.ToString()));
							isOkay = false;
						}
					}
				}

				// If the right operand is 'Null', or 'Not Answered' then check that the middle operand is = or #,
				foreach (DataRow dr in dt.Rows)
				{
					if (! (Convert.IsDBNull(dr[4]) || dr[4] == null || Convert.IsDBNull(dr[3]) || dr[3] == null))
					{
						PackageItem op2 = (PackageItem) dr[4];						
						OperatorDisplay opdisp = (OperatorDisplay) dr[3];
					
						if(op2 is PackageConstant)
						{
							if((((PackageConstant)op2).Value.ToString() == "Null" || ((PackageConstant)op2).Value.ToString() == "Not Answered") && PerfectusTypeSystem.IsValidNullCheckOperand(opdisp.Op) != true)
							{							
								messageBuilder.Append(string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.QueryExpression.InvalidComparisonOperator"), op2.ToString()));
								isOkay = false;				
							}
						}						
					}
				}

				if (!isOkay)
				{
					Message = messageBuilder.ToString();
				}

				return isOkay;

			}
			else
			{
				throw new ArgumentException("Table was an unexepcted structure.", "dt");
			}
		}
	}
}