using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Perfectus.Common.Property.Class;
using System.Xml.Serialization;
using System.IO;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Server.
	/// </summary>
	[Serializable]
	public sealed class Server : PackageItem, ISerializable
	{
		public ServerInfo serverInfo;
		private PluginDescriptor[] plugins;
		private Uri address;
		private string interviewAddress;
		private string myDocumentsURL;

		private RuleCollection rules = new RuleCollection();
        private Rule2Collection rule2 = new Rule2Collection();

		[Browsable(false)]
		public PluginDescriptor[] AvailablePlugins
		{
			get { return plugins; }
		}

		[Browsable(true)]
		[ReadOnly(false)]
        [DisplayRanking( 2 )]
        public string Address
		{
			get { return address == null ? null : address.AbsoluteUri; }
			set
			{
				address = new Uri(value);
				OnPropertyChanged("Address");
			}
		}

		[Browsable(false)]
		[Obsolete("Not required in 4.2")]
		public string InterviewAddress
		{
			get { return interviewAddress; }
			set
			{
				interviewAddress = value;
				OnPropertyChanged("InterviewAddress");
			}
		}

		[Browsable(false)]
		[ReadOnly(true)]
		public string MyDocumentsURL
		{
			get { return myDocumentsURL;} 
			set
			{
				myDocumentsURL = value;
				OnPropertyChanged("MyDocumentsURL");
			}
		}
		private Server()
		{
		}
        
		[Browsable(false)]
		public RuleCollection Rules
		{
			get { return rules; }
		}

        [Browsable(false)]
        public Rule2Collection Rule2
        {
            get { return rule2; }
            set { rule2 = value; rules = null; }
        }

        public void RefreshPluginsOnly(PluginDescriptor[] plugins)
        {
            this.plugins = plugins;
        }

        public void Refresh(ServerInfo info, PluginDescriptor[] plugins, Dictionary<String, DataTable> oldData)
        {
            this.serverInfo = info;
            this.plugins = plugins;
            this.Name = info.Name;
            this.UniqueIdentifier = info.UniqueIdentifier;
            this.interviewAddress = info.InterviewAddress;

            // Get the MyDocuments URL for mydocuments publish control
            this.MyDocumentsURL = info.MyDocumentsURL;


            // Have seen serialisation errors that cannot be explained. Maybe because packages
            // were of a different build.
            // Property are stored in a table [e.g. nameValueTable of document.Converter.Descriptor]
            // Three array lists are craeted from this table displayNameList, attributeList, dataTypeList
            // Sometimes these lists are not in sync with the table 
            // Have seen errors like: "There is no row at position 0."

            // Update - Dec 2016
            // During development of the OTCS plugins we identified the root cause of the above symptoms
            // Changes to the distributor objects (names/structures) can cause data to be 'lost'. 
            // The data is still present in the .ip package, however becomes lost if it is not compatible with the 'new' plugins available on the server.
            // The original refresh attempted to fix this by cloning the distributors and converters, but the clone was flawed - failed to refresh 
            // parentPackage and uniqueIdentifier... which made the refresh appear to work but in fact broke livelink category mappings.
            // Our solution is to clone the parentPackage and uniqueIdentifiers, and in the case of a livelink object, also clone the property data (for cat maps).
            // The refresh now serves as a livelink - otcs upgrade tool, reliying on property data sourced by the caller before the server was refreshed.
            // Future upgrades to plugins could be handled similarly - identify the existence of the old plugin in the package, identify the presence of a new plugin
            // newly available on the server, and migrate data from old to new.

            //check for OTCS plugins here
            bool OTCSPlugins = false;
            foreach (var plugin in this.plugins)
                if (plugin.TypeName.Contains("OTCS"))
                    OTCSPlugins = true;

            try
            {
                foreach (Rule2 rule in rule2)
                    foreach (Delivery delivery in rule.Deliveries)
                    {
                        foreach (IDocument doc in delivery.Documents)
                            if (doc is Document)
                            {
                                Document document = doc as Document;
                                if (document.Converter != null)
                                    foreach (PluginDescriptor p in plugins)
                                        if (p.UniqueIdentifier == document.Converter.Descriptor.UniqueIdentifier)
                                        {
                                            Converter c = new Converter(p);

                                            foreach (PluginPropertyDescriptor d in document.Converter.Descriptor.PropertyDescriptors)
                                            {
                                                c[d.Name] = document.Converter[d.Name];
                                                //pf-3266 replace word2007 .doc with .docx
                                                if (document.Converter[d.Name] != null)
                                                {
                                                    String text = document.Converter[d.Name].ToString();
                                                    PropertyList data = PropertyList.CreateFromXML(text);
                                                    if (data.CurrentElement.InternalName == "wdFormatDocument97")
                                                    {
                                                        data.CurrentElement.InternalName = "wdFormatXMLDocument";
                                                        data.CurrentElement.FriendlyName = "Office Open XML (*.docx)";
                                                        String newtext = String.Empty;
                                                        XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
                                                        using (MemoryStream m = new MemoryStream())
                                                        {
                                                            ser.Serialize(m, data);
                                                            newtext = Encoding.ASCII.GetString(m.ToArray(), 0, (int)m.Length);
                                                        }
                                                        c[d.Name] = newtext;
                                                    }
                                                }
                                            }

                                            //ensure important props are set correctly
                                            c.UniqueIdentifier = document.Converter.UniqueIdentifier;
                                            c.ParentPackage = rule.ParentPackage;

                                            document.SetConverter(c);
                                            break;
                                        }
                                ((Document)doc).Converter.MaybeRebuildProperties();
                            }

                        if (delivery.Distributor != null)
                        {
                            foreach (PluginDescriptor p in plugins)
                                if (p.UniqueIdentifier == delivery.Distributor.Descriptor.UniqueIdentifier)
                                {
                                    Distributor d = new Distributor(p);

                                    d.UniqueIdentifier = delivery.Distributor.UniqueIdentifier;
                                    d.ParentPackage = rule.ParentPackage;

                                    foreach (PluginPropertyDescriptor e in delivery.Distributor.Descriptor.PropertyDescriptors)
                                        d[e.Name] = delivery.Distributor[e.Name];

                                    // upgrade livelink to otcs - clone property data (cat mappings)
                                    //if (OTCSPlugins && delivery.Distributor.Descriptor.FriendlyName.Contains("Livelink"))
                                    //{
                                    //    DataTable tempData = oldData[delivery.UniqueIdentifier + "-" + p.UniqueIdentifier];
                                    //    d.SetPropertyValuesForPackageMigration(tempData);
                                    //}

                                    delivery.SetDistributor(d);
                                    break;
                                }
                            delivery.Distributor.MaybeRebuildProperties();
                        }
                    }
            }
            catch { }

            string message;
            if (!Valid(out message))
            {
                System.Windows.Forms.MessageBox.Show(
                    String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Server.RefreshFoundErrorText"), message),
                    ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.Server.RefreshFoundErrorCaption"),
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Hand);
            }
        }

        internal Server(ServerInfo info, PluginDescriptor[] plugins, Uri address)
            : base()
		{
			this.serverInfo = info;
			this.plugins = plugins;
			this.address = address;
			this.Name = info.Name;
			this.UniqueIdentifier = info.UniqueIdentifier;
            this.interviewAddress = info.InterviewAddress;

            // Get the MyDocuments URL for mydocuments publish control 
            this.MyDocumentsURL = info.MyDocumentsURL;
        }

        public override bool Valid(out string message)
        {
            message = default(String);
            bool valid = base.Valid(out message);
            StringBuilder sb = new StringBuilder(message);

            foreach (Rule2 rule in this.Rule2)
                if (!rule.Valid(out message))
                {
                    sb.AppendLine(" " + message);
                    valid = false;
                }
            if (!valid)
            {
                sb.Insert(0, new StringBuilder().AppendLine(String.Format("Server '{0}' is not valid.", Name)).ToString());
                message = sb.ToString();
            }
            return valid;
        }


		#region ISerializable Members

		public Server(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.serverInfo = (ServerInfo) info.GetValue("serverInfo", typeof (ServerInfo));
			this.plugins = (PluginDescriptor[]) info.GetValue("plugins", typeof (PluginDescriptor[]));
			this.address = (Uri) info.GetValue("address", typeof (Uri));
			this.rules = (RuleCollection) info.GetValue("rules", typeof (RuleCollection));
            try
            {
                this.rule2 = (Rule2Collection)info.GetValue("rule2", typeof(Rule2Collection));
            }
            catch { }

            this.Name = serverInfo.Name;
			this.UniqueIdentifier = serverInfo.UniqueIdentifier;
			
			try
			{
				this.interviewAddress = info.GetString("interviewAddress");
			}
			catch
			{ 
			}
			try
			{
                // Get the MyDocuments URL for mydocuments publish control 
                this.MyDocumentsURL = serverInfo.MyDocumentsURL;
			}
			catch
			{
				
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("serverInfo", this.serverInfo);
			info.AddValue("plugins", this.plugins);
			info.AddValue("address", this.address);
			info.AddValue("rules", this.rules);
            info.AddValue("rule2", this.rule2);
            info.AddValue("interviewAddress", this.interviewAddress);
		}

		#endregion
	}
}
