using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Server
	/// </summary>
	[Serializable]
	public sealed class ServerCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the ServerCollection class.
		/// </summary>
		public ServerCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the ServerCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new ServerCollection.
		/// </param>
		public ServerCollection(Server[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the ServerCollection class, containing elements
		/// copied from another instance of ServerCollection
		/// </summary>
		/// <param name="items">
		/// The ServerCollection whose elements are to be added to the new ServerCollection.
		/// </param>
		public ServerCollection(ServerCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this ServerCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this ServerCollection.
		/// </param>
		public void AddRange(Server[] items)
		{
			foreach (Server item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another ServerCollection to the end of this ServerCollection.
		/// </summary>
		/// <param name="items">
		/// The ServerCollection whose elements are to be added to the end of this ServerCollection.
		/// </param>
		public void AddRange(ServerCollection items)
		{
			foreach (Server item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Server to the end of this ServerCollection.
		/// </summary>
		/// <param name="value">
		/// The Server to be added to the end of this ServerCollection.
		/// </param>
		public void Add(Server value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Server value is in this ServerCollection.
		/// </summary>
		/// <param name="value">
		/// The Server value to locate in this ServerCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this ServerCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Server value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this ServerCollection
		/// </summary>
		/// <param name="value">
		/// The Server value to locate in the ServerCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Server value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the ServerCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Server is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Server to insert.
		/// </param>
		public void Insert(int index, Server value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Server at the given index in this ServerCollection.
		/// </summary>
		public Server this[int index]
		{
			get { return (Server) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Server from this ServerCollection.
		/// </summary>
		/// <param name="value">
		/// The Server value to remove from this ServerCollection.
		/// </param>
		public void Remove(Server value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by ServerCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(ServerCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Server Current
			{
				get { return (Server) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Server) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this ServerCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}