namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IClipboardItem.
	/// </summary>
	public interface IClipboardItem
	{
		void Copy();
		void Paste();
		void Cut();
	}
}