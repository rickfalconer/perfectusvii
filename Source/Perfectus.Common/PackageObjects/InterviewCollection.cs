using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Interview
	/// </summary>
	[Serializable]
	public sealed class InterviewCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the InterviewCollection class.
		/// </summary>
		internal InterviewCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the InterviewCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new InterviewCollection.
		/// </param>
		internal InterviewCollection(Interview[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the InterviewCollection class, containing elements
		/// copied from another instance of InterviewCollection
		/// </summary>
		/// <param name="items">
		/// The InterviewCollection whose elements are to be added to the new InterviewCollection.
		/// </param>
		internal InterviewCollection(InterviewCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this InterviewCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this InterviewCollection.
		/// </param>
		internal void AddRange(Interview[] items)
		{
			foreach (Interview item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another InterviewCollection to the end of this InterviewCollection.
		/// </summary>
		/// <param name="items">
		/// The InterviewCollection whose elements are to be added to the end of this InterviewCollection.
		/// </param>
		public void AddRange(InterviewCollection items)
		{
			foreach (Interview item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Interview to the end of this InterviewCollection.
		/// </summary>
		/// <param name="value">
		/// The Interview to be added to the end of this InterviewCollection.
		/// </param>
		public void Add(Interview value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Interview value is in this InterviewCollection.
		/// </summary>
		/// <param name="value">
		/// The Interview value to locate in this InterviewCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this InterviewCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Interview value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this InterviewCollection
		/// </summary>
		/// <param name="value">
		/// The Interview value to locate in the InterviewCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Interview value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the InterviewCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Interview is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Interview to insert.
		/// </param>
		internal void Insert(int index, Interview value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Interview at the given index in this InterviewCollection.
		/// </summary>
		public Interview this[int index]
		{
			get { return (Interview) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Interview from this InterviewCollection.
		/// </summary>
		/// <param name="value">
		/// The Interview value to remove from this InterviewCollection.
		/// </param>
		internal void Remove(Interview value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by InterviewCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(InterviewCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Interview Current
			{
				get { return (Interview) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Interview) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this InterviewCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}