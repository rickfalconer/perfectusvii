namespace Perfectus.Common.PackageObjects
{
	public delegate void RulePluginEventHandler(object sender, RulePluginEventArgs e);
}