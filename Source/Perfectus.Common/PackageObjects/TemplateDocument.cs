using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Template.
	/// </summary>
	[Serializable]
	public abstract class TemplateDocument : LibraryItem, IDesignable, ISerializable
	{
        
        private TextQuestionQueryValue publishedFileName = new TextQuestionQueryValue();

		[Browsable(false)]
        /*
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayRanking( 2 )]
         */ 
        public TextQuestionQueryValue PublishedFileName
		{
			get { return publishedFileName; }
            /*
			set 
			{ 
				publishedFileName = value; 
                OnPropertyChanged("PublishedFileName");
			}
             */ 
		}
        
		protected TemplateDocument() : base()
		{
			Name = GetUniqueName(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.TemplateDocument.NewItemName"),null);
		}

		public override bool AllowDuplicatesInPackage
		{
			get { return true; }
		}

		#region ISerializable Members

		public TemplateDocument(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            // Only used during upgrade.
            // Perfectus 6 doesn't use this property anymore
            try
			{
				publishedFileName = (TextQuestionQueryValue)info.GetValue("publishedFileName", typeof(TextQuestionQueryValue));
			}
			catch
			{
				publishedFileName = new TextQuestionQueryValue();
			}
		}
        /*
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("publishedFileName", publishedFileName);
		}
        */

		#endregion
	}
}
