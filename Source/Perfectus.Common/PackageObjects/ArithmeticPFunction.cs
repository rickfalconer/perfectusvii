using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ArithmeticPFunction.
	/// </summary>
	[Serializable]
	[XmlRoot("ArithmeticPFunction")]
	public sealed class ArithmeticPFunction : PFunction, ISerializable, IPackageItemContainer
	{
		#region Private Member Variables

		private PerfectusDataType dataType = PerfectusDataType.Number;
		private ArithmeticExpression expression;
        private object functionQueryRounding = null;

		#endregion
		#region Properties & Contructor

        [Browsable(false)]
        [ReadOnly(true)]
        [Category("Data")]
        public override PerfectusDataType DataType
        {
            get { return dataType; }
            set
            {
                // Only allow number data types for an arithmetic function
                if (value == PerfectusDataType.Number)
                    this.dataType = value;
                else
                    throw new NotSupportedException();
            }
        }

        [Browsable(true)]
        [Category("Data")]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.ArithmeticPFunctionQueryRoundingConverter, Studio")]
        [DisplayRanking( 8 )]
        public object FunctionQueryRounding
        {
            get { return functionQueryRounding; }
            set
            {
                this.functionQueryRounding = value;
            }
        }

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.ArithmeticFunctionEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.FunctionTypeConverter, Studio")]
        [DisplayDescriptionAttribute("Perfectus.Common.PackageObjects.PropertyDescriptions.FunctionDefinition")]
        [DisplayRanking( 2 )]
        public ArithmeticExpression Definition
		{
			get { return expression; }
			set
			{
				expression = value;
			}
		}

		public ArithmeticPFunction()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.AritnmeticPFunction.NewItemName");
		}

		#endregion
		#region Methods

        public override Boolean IsValid(ref String message)
        {
            if (Definition == null || Definition.RootExpressionNode == null)
            {
                message = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticPFunction.NoDefinition");
                return false;
            }
            return true;
        }

        public override List<LibraryItem> GetDependentLibraryItems()
		{
            List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();

			if (this.Definition != null && this.Definition.RootExpressionNode != null)
				SetDependentObjects(dependentLibraryItems, this.Definition.RootExpressionNode.Operand1);
			if (this.Definition != null && this.Definition.RootExpressionNode != null)
				SetDependentObjects(dependentLibraryItems, this.Definition.RootExpressionNode.Operand2);
			
			return dependentLibraryItems;
		}

        public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

        private void SetDependentObjects(List<LibraryItem> dependentLibraryItems, PackageItem packageItem)
		{
			if (packageItem is ExpressionNode)
			{
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand1);
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand2);
			}
			else
			{
				if (!(packageItem is PackageConstant))
				{
					// Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
					if (!dependentLibraryItems.Contains((LibraryItem) packageItem))
					{
                        if ((((LibraryItem)packageItem).RelativePath == null || ((LibraryItem)packageItem).RelativePath == string.Empty) &&
                            !dependentLibraryItems.Contains((LibraryItem)packageItem))
                        {
                            dependentLibraryItems.Add((LibraryItem)packageItem);
                        }
					}
				}
			}
		}


		public override bool ContainsReferenceTo(PackageItem pi)
		{
			if (expression == null)
			{
				return false;
			}
			else
			{
				return expression.ContainsRefTo(pi);
			}
		}

		public override object GetSingleAnswer(Caller caller)
		{
			if (expression != null)
			{
				return expression.Evaluate(caller);
			}
			else
			{
				return null;
			}		
		}

		public override object Clone()
		{
			object newObj = this.MemberwiseClone();
			ArithmeticPFunction apf = (ArithmeticPFunction)newObj;
			if (apf.expression != null)
			{
				apf.expression = (ArithmeticExpression)apf.expression.Clone();
				apf.expression.ParentPackage = ParentPackage;
			}
			apf.ParentPackage = ParentPackage;
			return apf;
		}

#endregion
		#region ISerializable Members

		public ArithmeticPFunction(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			expression = (ArithmeticExpression) info.GetValue("expression", typeof (ArithmeticExpression));
			dataType = (PerfectusDataType) info.GetValue("dataType", typeof (PerfectusDataType));

            try
            {
                functionQueryRounding = info.GetValue("functionQueryRounding", typeof(object));
            }
            catch
            {
                functionQueryRounding = null;
            }
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("expression", expression);
            info.AddValue("functionQueryRounding", functionQueryRounding);
		}

		#endregion
		#region Shared Library

		public void Update(ArithmeticPFunction arithmeticPFunction)
		{
            if (Name != arithmeticPFunction.Name)
            {
                this.Name = arithmeticPFunction.Name;
            }

			// Shared Library Item Properties
			this.LibraryUniqueIdentifier = arithmeticPFunction.LibraryUniqueIdentifier;	
			this.Version = arithmeticPFunction.Version;
			this.LibraryType = arithmeticPFunction.LibraryType;
			this.Linked = arithmeticPFunction.Linked;
			this.CreatedBy = arithmeticPFunction.CreatedBy;		
			this.CreatedDateTime = arithmeticPFunction.CreatedDateTime;
			this.CheckedOutBy = arithmeticPFunction.CheckedOutBy;
			this.CheckedOutByLogInName = arithmeticPFunction.CheckedOutByLogInName;
			this.CheckedOutDateTime = arithmeticPFunction.CheckedOutDateTime;
			this.CheckedOutStatus = arithmeticPFunction.CheckedOutStatus;
			this.ModifiedBy = arithmeticPFunction.ModifiedBy;
			this.ModifiedDateTime = arithmeticPFunction.ModifiedDateTime;
			this.CheckInComment = arithmeticPFunction.CheckInComment;
			this.LocationPath = arithmeticPFunction.LocationPath;
			this.RelativePath = arithmeticPFunction.RelativePath;
			this.Filename = arithmeticPFunction.Filename;
			this.FormatString = arithmeticPFunction.FormatString;
			this.FormatStringParams = arithmeticPFunction.FormatStringParams;
			this.Touch = arithmeticPFunction.Touch;
			this.Notes = arithmeticPFunction.Notes;
            this.InterviewFormatString = arithmeticPFunction.InterviewFormatString;
            this.InterviewFormatStringParams = arithmeticPFunction.InterviewFormatStringParams;
            this.FunctionQueryRounding = arithmeticPFunction.FunctionQueryRounding;
            this.DependentLibraryItems = arithmeticPFunction.DependentLibraryItems;
            this.PreviewHoverText = arithmeticPFunction.PreviewHoverText;

			// Reconnect the dependent objects
			this.Definition = new ArithmeticExpression();

			// Set the definition if the shared item had one defined
			if (arithmeticPFunction.Definition != null)
			{
                if (null != arithmeticPFunction.Definition.RootExpressionNode &&
                    arithmeticPFunction.Definition.RootExpressionNode.Operand1 != null)
                {
                    this.Definition.RootExpressionNode = ReconnectDependentObjects(arithmeticPFunction.Definition.RootExpressionNode);
                }

				((ExpressionBase)this.Definition).CreateTableFromExpression();
			}
		}

		private ExpressionNode ReconnectDependentObjects(ExpressionNode expressionNode)
		{
			// Handle Operand 1 - nested or not
			if (expressionNode.Operand1.GetType() == typeof(ExpressionNode))
            {
				ReconnectDependentObjects((ExpressionNode)expressionNode.Operand1);
			}
			else
			{
				// Connect library item
				expressionNode.Operand1 = GetDependentPackageItem((LibraryItem)expressionNode.Operand1);
			}
		
			// Handle Operand 2 - nested or not
			if (expressionNode.Operand2.GetType() == typeof(ExpressionNode))
			{
				ReconnectDependentObjects((ExpressionNode)expressionNode.Operand2);
			}
			else
			{
				// Connect library item
				expressionNode.Operand2 = GetDependentPackageItem((LibraryItem)expressionNode.Operand2);
			}

			return expressionNode;
		}

		#endregion
		
        public object GetRoundedValueForQuery(AnswerProvider.Caller caller)
        {
            if (functionQueryRounding != null)
            {
                return System.Math.Round((decimal)GetSingleAnswer(caller), int.Parse(functionQueryRounding.ToString()), MidpointRounding.AwayFromZero);
            }
            else
            {
                return GetSingleAnswer(caller);
            }
        }
	}
}
