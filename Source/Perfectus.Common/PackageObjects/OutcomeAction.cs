using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using System.Text;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for OutcomeAction.
	/// </summary>
	[Serializable]
	public sealed class OutcomeAction : ActionBase, ISerializable, ICloneable
	{
		private string wordML;
        private string officeOpenXML;
		private PFunction function;
		private string libraryKey;
		private string fetcherId;
		private string displayName;
		private string libraryName;
		private string reportingId;

        public string OfficeOpenXML
        {
            get { return officeOpenXML; }
            set
            {
                officeOpenXML = value;
                function = null;
                fetcherId = null;
                libraryKey = null;
                libraryName = null;
                reportingId = null;
            }
        }

		public string LibraryKey
		{
			get { return libraryKey; }
			set
			{
				wordML = null;
				function = null;
				libraryKey = value;
			}
		}

		public string DisplayName
		{
			get { return displayName; }
			set
			{
				function = null;
				displayName = value;
			}
		}

		public string LibraryName
		{
			get { return libraryName; }
			set
			{
				libraryName = value;
			}
		}

		public string ReportingId
		{
			get { return reportingId; }
			set
			{
				reportingId = value;
			}
		}

		public string FetcherId
		{
			get { return fetcherId; }
			set { 
				wordML = null;
				function = null;
				fetcherId = value;
			}
		}

        public string WordML
        {
            get { return wordML; }
        }

		public PFunction Function
		{
			get { return function; }
			set
			{
				function = value;
				wordML = null;
			}
		}

		public OutcomeAction() : base()
		{}

        public string DocumentPartOfficeOpenXml()
        {
            if (officeOpenXML != null)
            {
                byte[] outcomeBytes = Encoding.UTF8.GetBytes(officeOpenXML);
                MemoryStream outcomeStream = new MemoryStream(outcomeBytes);

                using (WordprocessingDocument docPackage = WordprocessingDocument.Open(outcomeStream, false))
                {
                    return docPackage.MainDocumentPart.Document.OuterXml;
                }
            }
            else
            {
                return null;
            }

            //if (officeOpenXML != null)
            //{
            //    XmlDocument documentPart = new XmlDocument();
            //    documentPart.LoadXml(officeOpenXML);

            //    using (WordprocessingDocument docPackage = WordprocessingDocument.Open(ms, false))
            //    {
            //        return docPackage.MainDocumentPart.Document.OuterXml;
            //    }
            //}
            //else
            //{
            //    return null;
            //}
        }

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

            if (context.State != StreamingContextStates.Persistence)
            {
                info.AddValue("officeOpenXML", officeOpenXML);
            }

			if (context.State != StreamingContextStates.Persistence)
			{
				info.AddValue("wordML", wordML);
			}

			info.AddValue("function", function);
			info.AddValue("fetcherId", fetcherId);
			info.AddValue("libraryKey", libraryKey);
			info.AddValue("displayName", displayName);
			info.AddValue("libraryName", libraryName);
			info.AddValue("reportingId", reportingId);
		}

        public OutcomeAction(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            if (context.State != StreamingContextStates.Persistence)
            {
                try
                {
                    officeOpenXML = info.GetString("officeOpenXML");
                }
                catch
                {
                    officeOpenXML = null;
                }
            }
            else
            {
                officeOpenXML = null;
            }

            if (context.State != StreamingContextStates.Persistence)
            {
                try
                {
                    wordML = info.GetString("wordML");
                }
                catch
                {
                    wordML = null;
                }
            }
            else
            {
                wordML = null;
            }

            function = (PFunction)info.GetValue("function", typeof(PFunction));
            try
            {
                fetcherId = info.GetString("fetcherId");
                libraryKey = info.GetString("libraryKey");
            }
            catch
            {
            }
            try
            {
                displayName = info.GetString("displayName");
                libraryName = info.GetString("libraryName");
            }
            catch
            {
            }

            try
            {
                reportingId = info.GetString("reportingId");
            }
            catch
            {
            }

        }        
    }
}