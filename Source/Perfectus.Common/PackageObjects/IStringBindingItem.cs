using System.Windows.Forms;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IStringBindingItem.
	/// </summary>
	public interface IStringBindingItem
	{
		void AddHTMLTagToObject(DataObject inObj, string fieldName);

		string GetTagName();
		//string GetTagContents();
		//string FormatString { get; set; }
	}
}
