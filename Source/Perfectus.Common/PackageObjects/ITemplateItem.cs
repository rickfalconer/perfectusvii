using System.Windows.Forms;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ITemplateItem.
	/// </summary>
	public interface ITemplateItem
	{
		void AddWordMLTagToObject(DataObject inObj);

		string GetTagName();
		string GetTagContents();
		string FormatString { get; set; }
		string FormatStringParams { get; set; }
		string PreviewHoverText {get; set; }
	}
}
