using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type Question
	/// </summary>
	[Serializable]
	public class QuestionDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the QuestionDictionary class
		/// </summary>
		public QuestionDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the Question associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual Question this[Guid key]
		{
			get { return (Question) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this QuestionDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The Question value of the element to add.
		/// </param>
		public virtual void Add(Guid key, Question value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this QuestionDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this QuestionDictionary.
		/// </param>
		/// <returns>
		/// true if this QuestionDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this QuestionDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this QuestionDictionary.
		/// </param>
		/// <returns>
		/// true if this QuestionDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this QuestionDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The Question value to locate in this QuestionDictionary.
		/// </param>
		/// <returns>
		/// true if this QuestionDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(Question value)
		{
			foreach (Question item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this QuestionDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this QuestionDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this QuestionDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}

}