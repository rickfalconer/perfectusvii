using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Folder.
	/// </summary>
	/// 
	[Serializable]
	public abstract class Folder : PackageItem, IPackageItemRequiresUniqueName
	{			
		public Folder()
		{		
		}

		protected FolderCollection m_childFolders = new FolderCollection();	

		/// <summary>
		/// The children folders this folder contains, read only
		/// </summary>
		[Browsable(false)]		
		public FolderCollection ChildFolders
		{
			get { return m_childFolders; }			
		}

		protected Folder m_ParentFolder; 
		/// <summary>
		/// The folders parent folder, read only.
		/// </summary>
		public override Folder ParentFolder
		{
			get {return m_ParentFolder;}
		}


        private System.Collections.CollectionBase RootFoldersByFolderType()
        {
            if (ParentPackage != null)
                if (this is AttachmentFolder)
                    return this.ParentPackage.AttachmentFolders;
                else if (this is QuestionFolder)
                    return this.ParentPackage.QuestionsFolders;
                else if (this is OutcomeFolder)
                    return this.ParentPackage.OutcomeFolders;
                else if (this is SimpleOutcomeFolder)
                    return this.ParentPackage.SimpleOutcomeFolders;
                else if (this is PFunctionFolder)
                    return this.ParentPackage.FunctionFolders;
            return null;
        }


        public virtual String suggestedName(string proposedName)
        {
            int i = 0;
            // Names in the tree may have a leading space, which is not part of the name.
            System.Collections.CollectionBase folders = null;
            proposedName = proposedName.Trim();

            if (null != m_ParentFolder)
                folders = m_ParentFolder.ChildFolders;
            else if (ParentPackage != null)
                folders = RootFoldersByFolderType();

            while (null != folders)
            {
                int j = i;
                string resultingName = proposedName;

                if (i > 0)
                    resultingName += String.Format("_{0}", i);

                foreach (Folder f in folders)
                    if (f == this)
                        continue;
                    else if (0==String.Compare(f.Name,resultingName,true))
                    {
                        i++;
                        break;
                    }
                if (j == i)
                    return resultingName;
            }
            return proposedName; 
        }

		/// <summary>
		/// Returns all child folders AND the current folder into a single folder collection, 
		/// </summary>
		/// <returns>FolderCollection</returns>		
		public FolderCollection GetAllFolders()
		{
			FolderCollection returnCollection = new FolderCollection();
			returnCollection.Add(this);
			RecurseChildrenFolders(m_childFolders, returnCollection);
			return returnCollection;
		}

		/// <summary>
		/// Returns all child folders of the current folder into a single folder collection
		/// </summary>
		/// <returns>FolderCollection</returns>		
		public FolderCollection GetChildFolders()
		{
			FolderCollection returnCollection = new FolderCollection();
			RecurseChildrenFolders(m_childFolders, returnCollection);
			return returnCollection;
		}

		protected void MakeChildOf(Folder argParentFolder)
		{
			this.m_ParentFolder = argParentFolder;
		}

		protected bool FolderMoveValid(Folder targetFolder)
		{
			foreach(Folder folder in this.GetAllFolders())
			{
				if(folder == targetFolder)
				{
					return false;
				}
			}

			return true;
		}

		private void RecurseChildrenFolders(FolderCollection fc, FolderCollection returnCollection)
		{
			foreach (Folder f in fc)
			{
				// add to our new collection before the recursive call, so the collection that is built up as any parents before children.
				returnCollection.Add(f);
				
				if (f.m_childFolders.Count > 0)
				{
					RecurseChildrenFolders(f.m_childFolders, returnCollection);
				}						
			}
		}


		public abstract void CreateChild();	
		public abstract void ChangeToRootFolder();
		public abstract bool MoveToFolder(Folder targetFolder);
	
		#region Serialization members

		public Folder(SerializationInfo info, StreamingContext context) : base (info, context)
		{	
			if (context.State != StreamingContextStates.Persistence)
			{
				try 
				{
					m_childFolders = (FolderCollection) info.GetValue("childFolders", typeof(FolderCollection));	
				}
				catch
				{
				}				

				try 
				{
					m_ParentFolder = (Folder) info.GetValue("folderParentFolder", typeof(Folder));	
				}
				catch
				{
				}
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);	
			if (context.State != StreamingContextStates.Persistence)
			{
				info.AddValue("childFolders", m_childFolders);
				info.AddValue("folderParentFolder", m_ParentFolder);
			}
		}
		#endregion

        #region IPackageItemRequiresUniqueName Members

        [Browsable( false )]
        public virtual bool RequiresUniqueName
        {
            get { return true; }
        }

        #endregion
    }
}
