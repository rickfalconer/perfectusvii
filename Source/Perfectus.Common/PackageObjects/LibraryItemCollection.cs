using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type LibraryItem
	/// </summary>
	[Serializable]
	public sealed class LibraryItemCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the LibraryItemCollection class.
		/// </summary>
		public LibraryItemCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the LibraryItemCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new LibraryItemCollection.
		/// </param>
		internal LibraryItemCollection(LibraryItem[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the LibraryItemCollection class, containing elements
		/// copied from another instance of LibraryItemCollection
		/// </summary>
		/// <param name="items">
		/// The LibraryItemCollection whose elements are to be added to the new LibraryItemCollection.
		/// </param>
		internal LibraryItemCollection(LibraryItemCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this LibraryItemCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this LibraryItemCollection.
		/// </param>
		internal void AddRange(LibraryItem[] items)
		{
			foreach (LibraryItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another LibraryItemCollection to the end of this LibraryItemCollection.
		/// </summary>
		/// <param name="items">
		/// The LibraryItemCollection whose elements are to be added to the end of this LibraryItemCollection.
		/// </param>
		internal void AddRange(LibraryItemCollection items)
		{
			foreach (LibraryItem item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type LibraryItem to the end of this LibraryItemCollection.
		/// </summary>
		/// <param name="value">
		/// The LibraryItem to be added to the end of this LibraryItemCollection.
		/// </param>
		public void Add(LibraryItem value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic LibraryItem value is in this LibraryItemCollection.
		/// </summary>
		/// <param name="value">
		/// The LibraryItem value to locate in this LibraryItemCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this LibraryItemCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(LibraryItem value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this LibraryItemCollection
		/// </summary>
		/// <param name="value">
		/// The LibraryItem value to locate in the LibraryItemCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(LibraryItem value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the LibraryItemCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the LibraryItem is to be inserted.
		/// </param>
		/// <param name="value">
		/// The LibraryItem to insert.
		/// </param>
		internal void Insert(int index, LibraryItem value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the LibraryItem at the given index in this LibraryItemCollection.
		/// </summary>
		public LibraryItem this[int index]
		{
			get { return (LibraryItem) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific LibraryItem from this LibraryItemCollection.
		/// </summary>
		/// <param name="value">
		/// The LibraryItem value to remove from this LibraryItemCollection.
		/// </param>
		internal void Remove(LibraryItem value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by LibraryItemCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(LibraryItemCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public LibraryItem Current
			{
				get { return (LibraryItem) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (LibraryItem) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this LibraryItemCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}