using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ActionBase.
	/// </summary>
	[Serializable]
	public abstract class ActionBase : IPackageItemContainer, ISerializable, ICloneable
	{
		#region Private Member Variables

		private Query subQuery = null;
		private Guid uniqueIdentifier = Guid.Empty;

		#endregion
		#region Properties & Constructor

		public Query SubQuery
		{
			get { return subQuery; }
			set { subQuery = value; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
		}


		protected ActionBase()
		{
			if (uniqueIdentifier == Guid.Empty)
			{
				uniqueIdentifier = Guid.NewGuid();
			}
		}

		#endregion
		#region Methods

		public virtual bool ContainsReferenceTo(PackageItem item)
		{
			if (subQuery != null)
			{
				return subQuery.ContainsReferenceTo(item);
			}
			else
			{
				return false;
			}

		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("subQuery", subQuery);
			info.AddValue("uniqueIdentifier", uniqueIdentifier);
		}

		public ActionBase(SerializationInfo info, StreamingContext context) : this()
		{
			try
			{
				subQuery = (Query)info.GetValue("subQuery", typeof(Query));
			}
			catch
			{
				subQuery = (Query)info.GetValue("ActionBase+subQuery", typeof(Query));
			}

			try
			{

				uniqueIdentifier = (Guid)info.GetValue("uniqueIdentifier", typeof(Guid));
				if (uniqueIdentifier == Guid.Empty)
				{
					uniqueIdentifier = Guid.NewGuid();
				}
			}
			catch
			{
				uniqueIdentifier = Guid.NewGuid();
			}
		}

		#endregion
		#region ICloneable Members

		public virtual object Clone()
		{
			object newObj = this.MemberwiseClone();
			if (subQuery != null)
			{
				ActionBase newA = (ActionBase)newObj;
				newA.subQuery = (Query)(subQuery.Clone());
				return newA;
			}
			else
			{
				return newObj;
			}
		}
	

		#endregion
	}
}