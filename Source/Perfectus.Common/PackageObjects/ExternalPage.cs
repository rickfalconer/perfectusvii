using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ExternalPage.
	/// </summary>
	[Serializable]
	public class ExternalPage : InterviewPage, ISerializable	
    {
		private TextQuestionQueryValue address = new TextQuestionQueryValue();

		[Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
		public TextQuestionQueryValue Address
		{
			get { return address; }
			set
			{
				address = value;
				OnPropertyChanged("Address");
			}
		}

		public ExternalPage() : base()
		{
		}

		public ExternalPage(Interview parentInterview) : this()
		{
			this.ParentInterview = parentInterview;	
		}

		/// <summary>
		///		Returns null. External Pages are not allowed in the Shared Library.
		/// </summary>
		/// <returns>null</returns>
        public override List<LibraryItem> GetDependentLibraryItems()
		{
			return null;
		}

		/// <summary>
		///		Returns null. External Pages are not allowed in the Shared Library and therefore do not have dependent items.
		/// </summary>
		/// <returns>null</returns>
        public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

		#region ISerializable Members

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("address", address);
		}

		public ExternalPage(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			address = (TextQuestionQueryValue) (info.GetValue("address", typeof(TextQuestionQueryValue)));
		}

		#endregion
	}
}
