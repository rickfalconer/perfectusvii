using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for WordDocumentProperty.
	/// </summary>
	[Serializable]
	public class WordDocumentProperty : PackageItem, ISerializable
	{
		public enum WordDocumentPropertyType
		{
			Standard,
			Custom
		}
		public enum WordDocumentCustomPropertyType
		{
			Text,
			Date,
			Number,
			YesNo
		}

		private string documentPropertyName = null;
		private TextQuestionQueryValue documentPropertyValue = new TextQuestionQueryValue();
		private WordDocumentPropertyType propertyType = WordDocumentPropertyType.Standard;
		private WordDocumentCustomPropertyType customDocumentPropertyType = WordDocumentCustomPropertyType.Text;

		public string DocumentPropertyName 
		{
			get { return documentPropertyName;}
			set { documentPropertyName = value;}
		}

		public TextQuestionQueryValue DocumentPropertyValue
		{
			get { return documentPropertyValue;}
			set {documentPropertyValue = value;}
		}

		public WordDocumentPropertyType PropertyType
		{
			get {return propertyType;}
			set {propertyType = value;}
		}

		public WordDocumentCustomPropertyType CustomDocumentPropertyType
		{
			get {return customDocumentPropertyType;}
			set {customDocumentPropertyType = value;}
		}

		public WordDocumentProperty() : base()
		{	
		}

		public static bool IsStandardValue(string valueToCheck)
		{
			valueToCheck = valueToCheck.ToLower();
			string[] standardValues = GetStandardValues();
			foreach (string s in standardValues)
			{
				if (s == valueToCheck)
				{
					return true;
				}
			}
			return false;			
		}

        public static bool IsStandardAppValue(string valueToCheck)
        {
            valueToCheck = valueToCheck.ToLower();
            string[] standardAppValues = GetStandardAppValues();
            foreach (string s in standardAppValues)
            {
                if (s == valueToCheck)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetStandardCorePropNamespace(string propName)
        {
            propName = propName.ToLower();

            if (propName == "title" || propName == "subject" || propName == "creator" || propName == "description")
            {
                return "http://purl.org/dc/elements/1.1/";
            }
            else if (propName == "keywords" || propName == "category")
            {
                return "http://schemas.openxmlformats.org/package/2006/metadata/core-properties";
            }
            else 
            {
                return null;
            }
        }

        public static string[] GetStandardAppValues()
        {
            return new string[] { "manager", "company", "hyperlinkbase" };
        }

		public static string[] GetStandardValues()
		{
            return new string[] { "title", "subject", "creator", "keywords", "description", "category", "contentStatus", "manager", "company", "hyperlinkbase" };
		}

		public static WordDocumentCustomPropertyType WordAttributeTypeToEnum(string valueToConvert)
		{
			switch(valueToConvert)			
			{
                case "vt:filetime":
					return WordDocumentCustomPropertyType.Date;
                case "vt:i4":
					return WordDocumentCustomPropertyType.Number;
                case "vt:bool":
					return WordDocumentCustomPropertyType.YesNo;
				default :
					return WordDocumentCustomPropertyType.Text;			
			}
			
		}

		public static string CustomPropertyTypeToWordAtt(WordDocumentCustomPropertyType valueToConvert)
		{
			switch(valueToConvert)			
			{
				case WordDocumentCustomPropertyType.Date :
                    return "filetime";
				case WordDocumentCustomPropertyType.Number :
                    return "i4";
				case WordDocumentCustomPropertyType.YesNo :
                    return "bool";
				default :
                    return "lpwstr";				
			}			
		}

		public bool ContainsReferenceTo(PackageItem item)
		{
			if(this.documentPropertyValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
			{
				if(this.documentPropertyValue.QuestionValue == item)
				{
					return true;
				}
			}			

			return false;
		}

		#region ISerializable Members

		public WordDocumentProperty(SerializationInfo info, StreamingContext context) : base(info, context)
		{	
			try
			{
				documentPropertyName = info.GetString("documentPropertyName");	
			}
			catch
			{
			}

			try
			{
				documentPropertyValue = (TextQuestionQueryValue) info.GetValue("documentPropertyValue", typeof (TextQuestionQueryValue));	
			}
			catch
			{
			}

			try
			{
				propertyType = (WordDocumentPropertyType) info.GetValue("propertyType", typeof (WordDocumentPropertyType));	
			}
			catch
			{
			}
			try
			{
				customDocumentPropertyType = (WordDocumentCustomPropertyType) info.GetValue("customDocumentPropertyType", typeof (WordDocumentCustomPropertyType));	
			}
			catch
			{
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("documentPropertyName", documentPropertyName);
			info.AddValue("documentPropertyValue", documentPropertyValue);
			info.AddValue("propertyType", propertyType);
			info.AddValue("customDocumentPropertyType",customDocumentPropertyType);
		}

		#endregion
	}
}
