using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for PackageNotSavedException.
	/// </summary>
	[Serializable]
	public class PackageNotSavedException : Exception
	{
		public PackageNotSavedException() : base()
		{
		}

		public PackageNotSavedException(string Message, Exception InnerException) : base(Message, InnerException)
		{
		}

		public PackageNotSavedException(string Message) : base(Message)
		{
		}

		protected PackageNotSavedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

	}
}