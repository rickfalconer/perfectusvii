using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for HtmlBlock.
	/// </summary>
	[Serializable]
	public sealed class HtmlBlock : IPageItem, ISerializable
	{
		private string html = null;
		private int designerHeight;
		private Guid uniqueIdentifier;
		private YesNoQueryValue visible;

		public YesNoQueryValue Visible
		{
			get { return visible; }
		}

		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
		}

		public int DesignerHeight
		{
			get { return designerHeight; }
			set { designerHeight = value; }
		}

		public string Html
		{
			get { return html; }
			set { html = value; }
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		public HtmlBlock(){} //SL - changed from private to public

		public HtmlBlock(string body, int designerHeight, Guid uniqueIdentifier, YesNoQueryValue visible) : base()
		{
			html = body;
			this.designerHeight = designerHeight;
			this.uniqueIdentifier = uniqueIdentifier;
			this.visible = visible;
		}

		#region ISerializable Members

		public HtmlBlock(SerializationInfo info, StreamingContext context)
		{
			html = info.GetString("html");
			designerHeight = info.GetInt32("designerHeight");

			// Unique identifer was added prior to beta, but after the format got locked.
			object o = null;

			try
			{
				o = info.GetValue("uniqueIdentifier", typeof (Guid));
			}
			catch
			{
			}
			if (o == null)
			{
				uniqueIdentifier = Guid.NewGuid();
			}
			else
			{
				uniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			}

			try
			{
				visible = (YesNoQueryValue) info.GetValue("visible", typeof (YesNoQueryValue));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}
		}

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("html", html);
			info.AddValue("designerHeight", designerHeight);
			info.AddValue("uniqueIdentifier", uniqueIdentifier);
			info.AddValue("visible", visible);
		}

		#endregion
	}
}