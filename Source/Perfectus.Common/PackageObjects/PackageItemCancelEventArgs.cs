using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for PackageItemCancelEventArgs.
	/// </summary>
	public sealed class PackageItemCancelEventArgs : EventArgs
	{
		private PackageItem item;
		private bool cancel = false;

		public PackageItem Item
		{
			get { return item; }
			set { item = value; }
		}

		public bool Cancel
		{
			get { return cancel; }
			set { cancel = value; }
		}

		public PackageItemCancelEventArgs(PackageItem item)
		{
			this.item = item;
		}
	}
}