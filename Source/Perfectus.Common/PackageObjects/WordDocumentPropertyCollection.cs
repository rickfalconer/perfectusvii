using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type WordDocumentProperty
	/// </summary>
	[Serializable]
	public sealed class WordDocumentPropertyCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the WordDocumentPropertyCollection class.
		/// </summary>
		public WordDocumentPropertyCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the WordDocumentPropertyCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new WordDocumentPropertyCollection.
		/// </param>
		public WordDocumentPropertyCollection(WordDocumentProperty[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the WordDocumentPropertyCollection class, containing elements
		/// copied from another instance of WordDocumentPropertyCollection
		/// </summary>
		/// <param name="items">
		/// The WordDocumentPropertyCollection whose elements are to be added to the new WordDocumentPropertyCollection.
		/// </param>
		public WordDocumentPropertyCollection(WordDocumentPropertyCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this WordDocumentPropertyCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this WordDocumentPropertyCollection.
		/// </param>
		public void AddRange(WordDocumentProperty[] items)
		{
			foreach (WordDocumentProperty item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another WordDocumentPropertyCollection to the end of this WordDocumentPropertyCollection.
		/// </summary>
		/// <param name="items">
		/// The WordDocumentPropertyCollection whose elements are to be added to the end of this WordDocumentPropertyCollection.
		/// </param>
		public void AddRange(WordDocumentPropertyCollection items)
		{
			foreach (WordDocumentProperty item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type WordDocumentProperty to the end of this WordDocumentPropertyCollection.
		/// </summary>
		/// <param name="value">
		/// The WordDocumentProperty to be added to the end of this WordDocumentPropertyCollection.
		/// </param>
		public void Add(WordDocumentProperty value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic WordDocumentProperty value is in this WordDocumentPropertyCollection.
		/// </summary>
		/// <param name="value">
		/// The WordDocumentProperty value to locate in this WordDocumentPropertyCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this WordDocumentPropertyCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(WordDocumentProperty value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this WordDocumentPropertyCollection
		/// </summary>
		/// <param name="value">
		/// The WordDocumentProperty value to locate in the WordDocumentPropertyCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(WordDocumentProperty value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the WordDocumentPropertyCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the WordDocumentProperty is to be inserted.
		/// </param>
		/// <param name="value">
		/// The WordDocumentProperty to insert.
		/// </param>
		public void Insert(int index, WordDocumentProperty value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the WordDocumentProperty at the given index in this WordDocumentPropertyCollection.
		/// </summary>
		public WordDocumentProperty this[int index]
		{
			get { return (WordDocumentProperty) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific WordDocumentProperty from this WordDocumentPropertyCollection.
		/// </summary>
		/// <param name="value">
		/// The WordDocumentProperty value to remove from this WordDocumentPropertyCollection.
		/// </param>
		public void Remove(WordDocumentProperty value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by WordDocumentPropertyCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(WordDocumentPropertyCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public WordDocumentProperty Current
			{
				get { return (WordDocumentProperty) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (WordDocumentProperty) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this WordDocumentPropertyCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}