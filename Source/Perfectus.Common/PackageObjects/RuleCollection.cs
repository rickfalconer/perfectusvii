using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Rule
	/// </summary>
	[Serializable]
	public sealed class RuleCollection : CollectionBase, IXmlSerializable
	{
		#region Private Member Variables

		[NonSerialized] private const string msRULES_KEY = "Rules";

		#endregion
		#region Properties & Constructor

		/// <summary>
		/// Gets or sets the Rule at the given index in this RuleCollection.
		/// </summary>
		public Rule this[int index]
		{
			get { return (Rule) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Initializes a new empty instance of the RuleCollection class.
		/// </summary>
		public RuleCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the RuleCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new RuleCollection.
		/// </param>
		public RuleCollection(Rule[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the RuleCollection class, containing elements
		/// copied from another instance of RuleCollection
		/// </summary>
		/// <param name="items">
		/// The RuleCollection whose elements are to be added to the new RuleCollection.
		/// </param>
		public RuleCollection(RuleCollection items)
		{
			this.AddRange(items);
		}

		#endregion
		#region Methods

		/// <summary>
		/// Adds the elements of an array to the end of this RuleCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this RuleCollection.
		/// </param>
		internal void AddRange(Rule[] items)
		{
			foreach (Rule item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another RuleCollection to the end of this RuleCollection.
		/// </summary>
		/// <param name="items">
		/// The RuleCollection whose elements are to be added to the end of this RuleCollection.
		/// </param>
		internal void AddRange(RuleCollection items)
		{
			foreach (Rule item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Rule to the end of this RuleCollection.
		/// </summary>
		/// <param name="value">
		/// The Rule to be added to the end of this RuleCollection.
		/// </param>
		public void Add(Rule value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Rule value is in this RuleCollection.
		/// </summary>
		/// <param name="value">
		/// The Rule value to locate in this RuleCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this RuleCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Rule value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this RuleCollection
		/// </summary>
		/// <param name="value">
		/// The Rule value to locate in the RuleCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Rule value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the RuleCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Rule is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Rule to insert.
		/// </param>
		internal void Insert(int index, Rule value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Removes the first occurrence of a specific Rule from this RuleCollection.
		/// </summary>
		/// <param name="value">
		/// The Rule value to remove from this RuleCollection.
		/// </param>
		public void Remove(Rule value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by RuleCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(RuleCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Rule Current
			{
				get { return (Rule) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Rule) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this RuleCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		#endregion
		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		/// <summary>
		///		Serializes the Query into Xml.
		/// </summary>
		/// <param name="w">Writes the object to Xml.</param>
		public void WriteXml(XmlWriter w)
		{	
			
			w.WriteStartElement(msRULES_KEY);


			w.WriteEndElement();
		}
				
		public void ReadXml(XmlReader r)
		{	
			// Read the starting element
			r.ReadStartElement(msRULES_KEY);

			// Read the end element
			r.ReadEndElement();
		}

		#endregion
	}
}