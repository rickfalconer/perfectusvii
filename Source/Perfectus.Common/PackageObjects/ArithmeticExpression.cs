using System;
using System.Data;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ArithmeticExpression.
	/// </summary>
	[Serializable]
	public class ArithmeticExpression : ExpressionBase, ISerializable
	{
		public ArithmeticExpression()
		{
			//
			//TODO: Add constructor logic here
			//
		}

		[Obsolete]
		public object Evaluate()
		{
			return Evaluate(AnswerProvider.Caller.System);
		}

		public object Evaluate(AnswerProvider.Caller caller)
		{
			if (rootNode != null)
			{
				return rootNode.Evaluate(caller);
			}
			else
			{
				return null;
			}

		}

		public override bool ValidateFromTable(DataTable dt, out string Message)
		{
			bool isOkay = true;
			Message = string.Empty;
			StringBuilder messageBuilder = new StringBuilder();
			messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.ProblemsHeader"));
			// Make sure the table's of the right structure
			if (dt.Columns[0].DataType == typeof (OperatorDisplay) &&
				dt.Columns[1].DataType == typeof (string) &&
				dt.Columns[2].DataType == typeof (PackageItem) &&
				dt.Columns[3].DataType == typeof (OperatorDisplay) &&
				dt.Columns[4].DataType == typeof (PackageItem) &&
				dt.Columns[5].DataType == typeof (string))
			{
				// Make sure there are some rows
				if (dt.Rows.Count == 0)
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.NoExpressionToValidate"));
					isOkay = false;
				}


				// Remove any operator from the first cell of the first row
				if (dt.Rows.Count > 0)
				{
					dt.Rows[0][0] = DBNull.Value;
				}

				// Check for balanced parentheses
				if (! CheckBalancedParentheses(dt))
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.UnbalancedParentheses"));
					isOkay = false;
				}


				// If there's only one row, treat it like a normal one.  If there's more than one, the last row is a special case (can have only one operator/operand)
				int rowsToCheckForTwoOperators = dt.Rows.Count > 1 ? dt.Rows.Count - 1 : 1;

				for (int i = 0; i < rowsToCheckForTwoOperators; i++)
				{
					DataRow dr = dt.Rows[i];

					// Check that each row has two operands (except the last one) and two operators
					if (Convert.IsDBNull(dr[2]) || dr[2] == null || Convert.IsDBNull(dr[4]) || dr[4] == null || Convert.IsDBNull(dr[3]) || dr[3] == null)
					{
						messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.RowDoesNotHaveTwoOperators"));
						isOkay = false;
						break;
					}
				}


				// Check that all rows after the first one have Operator1
				if (! CheckOperator1Present(dt))
				{
					messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.RowMissingFirstOperator"));
					isOkay = false;
				}

				// Check that the last row has either: two operators and two operands, or the first operator and the first operand.

				if (dt.Rows.Count > 1)
				{
					DataRow lastRow = dt.Rows[dt.Rows.Count - 1];

					bool lastRowOkay = (!(Convert.IsDBNull(lastRow[2]))) &&
						(
							(!(Convert.IsDBNull(lastRow[3])) && !(Convert.IsDBNull(lastRow[4]))) || ((Convert.IsDBNull(lastRow[3])) && Convert.IsDBNull(lastRow[4]))
							);

					if (!lastRowOkay)
					{
						messageBuilder.Append(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ArithmeticExpression.LastRowMissingOperator"));
						//messageBuilder.Append("\t - The last row has a missing operator or operand.");
						isOkay = false;
					}
				}

				if (!isOkay)
				{
					Message = messageBuilder.ToString();
				}
				return isOkay;

			}
			else
			{
				throw new ArgumentException("Table was an unexepcted structure.", "dt");
			}
		}

		#region ISerializable Members

		public ArithmeticExpression(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		#endregion
	}
}