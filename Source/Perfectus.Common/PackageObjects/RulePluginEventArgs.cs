using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for RuleItemEventArgs.
	/// </summary>
	public sealed class RulePluginEventArgs : EventArgs
	{
		private Rule r;

		private PluginBase plugin;

		public Rule Rule
		{
			get { return r; }

		}

		public PluginBase Plugin
		{
			get { return plugin; }
		}

		public RulePluginEventArgs(Rule r, PluginBase plugin)
		{
			this.r = r;
			this.plugin = plugin;
		}
	}
}