using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ExpressionBase.
	/// </summary>
	public abstract class ExpressionBase : PackageItem, ISerializable, IXmlSerializable
	{
		#region Private Member Variables

		protected DataTable internalTable;
		protected ExpressionNode rootNode;

		// Xml serialization variables for shared library
		[NonSerialized] private const string msOPERAND1 = "Operand1";
		[NonSerialized] private const string msOPERAND2 = "Operand2";
		[NonSerialized] private const string msEXPRESSIONOPERATION = "ExpressionOperation";
		[NonSerialized] private const string msQUERY_EXPRESSION = "QueryExpression";
		[NonSerialized] private bool mbDependenciesSectionExists = false;
        [NonSerialized] private const string msTABLE_ROW = "Row";
        [NonSerialized] private const string msTABLE = "Table";
        [NonSerialized] private const string msDummyGUID = "00000000-AAAA-0000-AAAA-000000000000";

		public static string Operand1Key { get { return msOPERAND1; } }
		public static string Operand2Key { get { return msOPERAND2; } }
		public static string ExpressionOperationKey { get { return msEXPRESSIONOPERATION; } }

		#endregion
		#region Properties & Constructor

		public ExpressionNode RootExpressionNode
		{
			get { return rootNode; }
			set { rootNode = value; }
		}

		public DataTable Table
		{
			get { return internalTable; }
		}

		public ExpressionBase()
		{
		}

		#endregion
		#region Methods

		public void CreateTableFromExpression()
		{
			// Create the table structure

            // Items in the SL have the query stored in <Dependencies> and the <Table>
            // This must be merged together. 
            // We do(!) assume that the number of table rows matches the number of dependencies
            // and that the order is the same.
            if (null == internalTable)
                internalTable = CreateQueryTable();
            else
                foreach (DataRow _row in internalTable.Rows)
                {
                    PackageConstant c = new PackageConstant();
                    c.UniqueIdentifier = new Guid(msDummyGUID);
                    _row[2] = c;
                }
			
            // Set the root row
            if ((ExpressionNode)this.rootNode != null)
            {
                HandleExpressionNode((ExpressionNode)this.rootNode, DBNull.Value);
            }
		}

		private void HandleExpressionNode(ExpressionNode expressionNode, object initialExpressionOperator)
		{
			// Operand 1 & 2
			if (expressionNode.Operand1 != null && expressionNode.Operand1.GetType() == typeof(ExpressionNode))
			{
				HandleExpressionNode((ExpressionNode)expressionNode.Operand1, initialExpressionOperator);

				// Set the expression operator
				initialExpressionOperator = expressionNode.ExpressionOperator;
				HandleExpressionNode((ExpressionNode)expressionNode.Operand2, initialExpressionOperator);
			}
			else
			{
				CreateDataRow(expressionNode, initialExpressionOperator);
			}
		}

		private void CreateDataRow(ExpressionNode expressionNode, object initialExpressionOperator)
		{
			DataRow row = null;
            
            foreach ( DataRow _row in internalTable.Rows )
            {
                try {
                    PackageItem item = (PackageItem) _row[2];
                    if (item.UniqueIdentifier == new Guid(msDummyGUID))
                    {
                        row = _row;
                        break;
                    }
                }
                catch (Exception)
                {
                    row = null;
                }
            }

            if ( row == null )
                row = internalTable.Rows.Add();
            


			// Define the datarow and add to the internal table
			if (initialExpressionOperator == DBNull.Value)
                row[0] = (DBNull)initialExpressionOperator; 
			else
                row[0] = new OperatorDisplay((Operator)initialExpressionOperator); // If more than one expression node - joins the two expressions together

            row[2] = expressionNode.Operand1;				// Package Item
            row[3] = new OperatorDisplay(expressionNode.ExpressionOperator);
            row[4] = expressionNode.Operand2;				// Package Item
		}
		
		protected bool CheckBalancedParentheses(DataTable dt)
		{
			int brackCount = 0;
			foreach (DataRow dr in dt.Rows)
			{
				if (dr[1].ToString() == "(")
					brackCount ++;
				if (dr[5].ToString() == ")")
					brackCount --;
				if (brackCount < 0)
				{
					break;
				}
			}

			return brackCount == 0;
		}


		public bool ContainsRefTo(PackageItem pi)
		{
			if (rootNode == null)
			{
				return false;
			}
			else
			{
				return rootNode.ContainsRefTo(pi);
			}
		}

		protected bool CheckOperator1Present(DataTable dt)
		{
			// Check that all rows other than the first one have operator1
			if (dt.Rows.Count > 1)
			{
				for (int i = 1; i < dt.Rows.Count; i++)
				{
					if (Convert.IsDBNull(dt.Rows[i][0]) || dt.Rows[i][0] == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		public virtual void SaveDataTable(DataTable dt)
		{
			SaveDataTable(dt, true);
		}

		public virtual void SaveDataTable(DataTable dt, bool doValidate)
		{
			string message;
			if (doValidate && !ValidateFromTable(dt, out message))
			{
				throw new ArgumentException(message, "dt");
			}

			rootNode = SaveDataRows(dt.Select(), false);

			internalTable = dt;
			OnPropertyChanged("table");	
		}

		public static DataTable CreateQueryTable()
		{
			DataTable outTable;
			outTable = new DataTable("Expression");
			outTable.Columns.Add("Operator1", typeof (OperatorDisplay));
			outTable.Columns.Add("LeftParenthesis", typeof (string));
			outTable.Columns.Add("LeftOperand", typeof (PackageItem));
			outTable.Columns.Add("Operator2", typeof (OperatorDisplay));
			outTable.Columns.Add("RightOperand", typeof (PackageItem));
			outTable.Columns.Add("RightParenthesis", typeof (string));

			return outTable;
		}

		protected ExpressionNode SaveDataRows(DataRow[] rows, bool ignoreParensOnFirstRow)
		{
			ExpressionNode thisNode = null;
			ExpressionNode previousNode = null;

			for (int i = 0; i < rows.Length; i++)
			{
				DataRow dr = rows[i];


				// If the row is the start of a bracket, make a junior collection of the parenthesised rows and get them as our node.

				if (!(ignoreParensOnFirstRow && i == 0) && i < rows.Length - 1 && dr[1].ToString() == "(" && dr[5].ToString() != ")")
					// Don't bother recursing if the brackets open and close on the same row
				{
					ArrayList parenRows = new ArrayList();
					int openParenCount = 0;
					ExpressionNode rootNodeForParens;
					for (int par = i; par < rows.Length; par++)
					{
						if (rows[par][1].ToString() == "(")
						{
							openParenCount ++;
						}
						if (rows[par][5].ToString() == ")")
						{
							openParenCount --;
						}

						parenRows.Add(rows[par]);

						if (openParenCount == 0)
						{
							// Get the first first operator of the sub expression:
							if (((DataRow) parenRows[0])[0] == null || Convert.IsDBNull(((DataRow) parenRows[0])[0]))
							{
								ExpressionNode parensNode = SaveDataRows((DataRow[]) parenRows.ToArray(typeof (DataRow)), true);
								thisNode = parensNode;

							}
							else
							{
								OperatorDisplay subOp1 = (OperatorDisplay) (((DataRow) parenRows[0])[0]);
								rootNodeForParens = new ExpressionNode(ParentPackage, null, subOp1.Op, null);
								ExpressionNode parensNode = SaveDataRows((DataRow[]) parenRows.ToArray(typeof (DataRow)), true);
								rootNodeForParens.Operand2 = parensNode;
								thisNode = rootNodeForParens;
							}


							// Fast forward our outer index to the end of the bracket.							
							i = par; // + 1;				
							break;
						}
					}
				}
					// Otherwise, make a node from the row, as usual.
				else
				{
					if (! Convert.IsDBNull(dr[3]))
					{
						thisNode = ConstructExpressionNode(dr, i == 0);
					}
					else
					{
						thisNode = ConstructOrphanNode(dr, i == 0);
					}
				}

				if (previousNode != null)
				{
					thisNode.Operand1 = previousNode;
				}
				previousNode = thisNode;

			}
			return thisNode;
		}

		protected ExpressionNode ConstructExpressionNode(DataRow dr, bool ignoreFirstOperator)
		{
			Operator operator1 = Operator.And; // Needs initialising, but will always be set.
			bool hasOperator1 = false;
			if (! Convert.IsDBNull(dr[0]))
			{
				OperatorDisplay operator1Disp = (OperatorDisplay) dr[0];
				operator1 = operator1Disp.Op;
				hasOperator1 = true;
			}
			else
			{
				hasOperator1 = false;
			}

			OperatorDisplay operand2display = (OperatorDisplay) dr[3];
			Operator operator2 = operand2display.Op;
			PackageItem item1 = (PackageItem) dr[2];
			PackageItem item2 = (PackageItem) dr[4];

			ExpressionNode enMain = new ExpressionNode(ParentPackage, item1, operator2, item2);
			if (hasOperator1 && !ignoreFirstOperator)
			{
				ExpressionNode enAbove = new ExpressionNode(ParentPackage, null, operator1, enMain);
				return enAbove;
			}
			else
			{
				return enMain;
			}
		}

		protected ExpressionNode ConstructOrphanNode(DataRow dr, bool ignoreFirstOperator)
		{
			Operator operator1 = Operator.And; // Needs initialising, but will always be set.
			if (! Convert.IsDBNull(dr[0]))
			{
				OperatorDisplay operator1Disp = (OperatorDisplay) dr[0];
				operator1 = operator1Disp.Op;
			}
			PackageItem item1 = (PackageItem) dr[2];

			ExpressionNode enMain = new ExpressionNode(ParentPackage, null, operator1, item1);
			return enMain;


		}

		public abstract bool ValidateFromTable(DataTable dt, out string Message);
		public override object Clone()
		{
			ExpressionBase newExpression = (ExpressionBase)(this.MemberwiseClone());
			return this.MemberwiseClone();
		}
		#endregion
		#region ISerializable Members

		public ExpressionBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{

			rootNode = (ExpressionNode) info.GetValue("rootNode", typeof (ExpressionNode));
			internalTable = CreateQueryTable();
			object[] r = (object[]) info.GetValue("rows", typeof (object[]));
			if (r != null)
			{
				foreach (object[] o in r)
				{
					internalTable.Rows.Add(o);
				}
			}
		}


		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("rootNode", rootNode);

			ArrayList rows = new ArrayList();
			if (internalTable != null && internalTable.Rows != null) 
			{
				foreach (DataRow dr in internalTable.Rows)
				{
					rows.Add(dr.ItemArray);
				}

				info.AddValue("rows", rows.ToArray());
			}
			else
			{
				info.AddValue("rows", null);
			}
		}

		#endregion
		#region XmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		public void WriteXml(XmlWriter w, bool dependenciesSectionExists)
		{
			mbDependenciesSectionExists = dependenciesSectionExists;
			WriteXml(w);
		}

		public void WriteXml(XmlWriter w)
		{	
			// Write out the expressions dependencies

			if (!mbDependenciesSectionExists)
				w.WriteStartElement(Serializer.DependenciesKey);

			w.WriteStartElement(msQUERY_EXPRESSION);
			WriteExpressionNodeDependentInformation(w, this.rootNode);
            w.WriteEndElement();

            // Quick solution
            // Write out the node table (to support brackets)
            // We are only interested in the brackets 'per row'
            w.WriteStartElement(msTABLE);
            if (internalTable != null && internalTable.Rows != null)
                foreach (DataRow dr in internalTable.Rows)
        		{
                    w.WriteStartElement(msTABLE_ROW);
                    w.WriteAttributeString("LeftParenthesis", dr[1].ToString());
                    w.WriteAttributeString("RightParenthesis", dr[5].ToString());
                    w.WriteEndElement();
                }
            
            w.WriteEndElement();

            if (!mbDependenciesSectionExists)
				w.WriteEndElement();
		}

		/// <summary>
		///		Writes out the dependent ExpressionNodes for an expression (normally arithmetic).
		///		Note this method uses recursion, as marked in the comments.
		/// </summary>
		/// <param name="w">XmlWriter to write the information to.</param>
		/// <param name="expressionNode">The expression node whose state is being serialized.</param>
		private static void WriteExpressionNodeDependentInformation(XmlWriter w, ExpressionNode expressionNode)
		{
			// Write out the Operand 1
			w.WriteStartElement(msOPERAND1);

            if (expressionNode != null && expressionNode.Operand1 != null)
			{
                if (expressionNode.Operand1.GetType() == typeof(ExpressionNode))
                    WriteExpressionNodeDependentInformation(w, (ExpressionNode)expressionNode.Operand1); // Recursive call
                else
                    Serializer.WriteDependentInformation(w, expressionNode.Operand1, msOPERAND1, false);
			}

			w.WriteEndElement();

			// Write out the Operand 2
			w.WriteStartElement(msOPERAND2);

			if (expressionNode != null && expressionNode.Operand2 != null)
			{
				if (expressionNode.Operand2.GetType() == typeof(ExpressionNode))
					WriteExpressionNodeDependentInformation(w, (ExpressionNode)expressionNode.Operand2); // Recursive call
				else
					Serializer.WriteDependentInformation(w, expressionNode.Operand2, msOPERAND2, false);
			}

			w.WriteEndElement();

			// Write out the Expression Operator
			if (expressionNode != null)
				w.WriteElementString(ExpressionOperationKey, expressionNode.ExpressionOperator.ToString());
			else
				w.WriteElementString(ExpressionOperationKey, "");
		}
		
		public void ReadXml(XmlReader r)
		{
			ReadXml(r, false);
		}

		public ExpressionBase ReadXml(XmlReader r, bool returnValue)
		{
			this.rootNode = new ExpressionNode();

			// Read the starting element - (when using sub queries and outcomes)
			if (r.Name != msQUERY_EXPRESSION)
			{
				// Open the definition element and get the first parameters info
				r.ReadStartElement();
				r.ReadStartElement(Serializer.DependenciesKey);
			}
			
			r.ReadStartElement();

			// Read the expression nodes and return the populated root ExpressionNode
			ReadExpressionNodeDependentInformation(r, this.rootNode);

			// Read the end element
			if (r.Name != msQUERY_EXPRESSION)
			{
				//r.ReadEndElement();
				//r.ReadEndElement();
			}

			r.ReadEndElement();

            // Read the table

            internalTable = CreateQueryTable();

            if (r.Name == msTABLE)
            {
                r.ReadStartElement();
                while (r.IsStartElement(msTABLE_ROW))
                {
                    object[] dr = new object[6];

                    dr[0] = DBNull.Value;
                    dr[1] = r.GetAttribute(0);

                    PackageConstant c = new PackageConstant();
                    c.UniqueIdentifier = new Guid(msDummyGUID);
                    dr[2] = c;

                    dr[3] = null;
                    dr[4] = null;
                    dr[5] = r.GetAttribute(1);
                    r.ReadToNextSibling(msTABLE_ROW);

                    internalTable.Rows.Add(dr);
                }
            }
            
			r.ReadEndElement();
			return this;
		}

		private void ReadExpressionNodeDependentInformation(XmlReader r, ExpressionNode expressionNode)
		{
			string expressionOperation;
			ExpressionNode subExpressionNode;

			// Read the Operand 1
			if (!r.IsEmptyElement)
			{
				r.ReadStartElement(msOPERAND1);

				// Is the Expression Nested?
				if (r.IsStartElement(msOPERAND1))
				{
					// Nested Expression - Recursive call to drill down into nested expresion
					subExpressionNode = new ExpressionNode();
					ReadExpressionNodeDependentInformation(r, subExpressionNode);
					expressionNode.Operand1 = subExpressionNode;
				}
				else
				{
					if (!r.IsEmptyElement)
					{
						// Read the dependent library item node
						expressionNode.Operand1 = ReadLibraryItemInformation(r);
					}
					else
					{
						r.Read();
					}
				}				

				r.ReadEndElement();
			}
			else
			{
				r.Read();
			}			

			if (!r.IsEmptyElement)
			{
				// Read the Operand 2
				r.ReadStartElement(msOPERAND2);

				// Is the Expression Nested? (If nested the first nested operand will be operand 1)
				if (r.IsStartElement(msOPERAND1))
				{
					// Nested Expression - Recursive call to drill down into nested expresion
					subExpressionNode = new ExpressionNode();
					ReadExpressionNodeDependentInformation(r, subExpressionNode);
					expressionNode.Operand2 = subExpressionNode;
				}
				else
				{
					if (!r.IsEmptyElement)
					{
						// Read the dependent library item node
						expressionNode.Operand2 = ReadLibraryItemInformation(r);
					}
					else
					{
						r.Read();
					}
				}

				r.ReadEndElement();
			}
			else
			{
				r.Read();
			}
			
			// Set the Expression Operation
			if (!r.IsEmptyElement)
			{
				r.ReadStartElement(msEXPRESSIONOPERATION);
				expressionOperation = r.ReadString();
				r.ReadEndElement();

				// Read the Expression Operator
				expressionNode.ExpressionOperator = GetExpressionOperator(expressionOperation);
			}
			else
			{
				r.Read();
			}
		}

		private LibraryItem ReadLibraryItemInformation(XmlReader r)
		{
			string type;
			string[] values;
			LibraryItem libraryItem = null;

			// Dependent Item - (Not a nested expression)
			r.ReadStartElement(Serializer.DependencyKey);

			// Get the type
			r.ReadStartElement(Serializer.TypeKey);
			type = r.ReadString();
			r.ReadEndElement();

			// Handle the specific library item type
			if (type == typeof(PackageConstant).ToString())
			{
				libraryItem = new PackageConstant();
				values = Serializer.ReadPackageConstant(r);
			}
			else
			{	
				if (type == typeof(Question).ToString())
					libraryItem = new Question();
				else if (type == typeof(ArithmeticPFunction).ToString())
					libraryItem = new ArithmeticPFunction();
				else if (type == typeof(DatePFunction).ToString())
					libraryItem = new DatePFunction();
				else
					throw new InvalidCastException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ExpressionBase.DependantObjectCast")); //TODO - SL - RESOURCE - ReadLibraryItemInformation, invalid cast error message 

				// Get the values for the item
				values = Serializer.ReadLibraryItem(r);
			}

			// Assign the read values to the operand1 properties
			libraryItem.LibraryUniqueIdentifier = new Guid(values[Serializer.UniqueIdentifierIndex]);
			libraryItem.Name = values[Serializer.NameIndex];
			libraryItem.StringValue = values[Serializer.ValueIndex];
			libraryItem.DataTypeString = values[Serializer.DataTypeIndex];

			if (r.Name == Serializer.DependencyKey)
				r.ReadEndElement();

			return libraryItem;
		}

		private Operator GetExpressionOperator(string expressionOperator)
		{
			if (expressionOperator == Operator.And.ToString())
				return Operator.And;
			if (expressionOperator == Operator.Divide.ToString())
				return Operator.Divide;
			if (expressionOperator == Operator.Eq.ToString())
				return Operator.Eq;
			if (expressionOperator == Operator.Ge.ToString())
				return Operator.Ge;
			if (expressionOperator == Operator.Gt.ToString())
				return Operator.Gt;
			if (expressionOperator == Operator.Le.ToString())
				return Operator.Le;
			if (expressionOperator == Operator.Lt.ToString())
				return Operator.Lt;
			if (expressionOperator == Operator.Minus.ToString())
				return Operator.Minus;
			if (expressionOperator == Operator.Multiply.ToString())
				return Operator.Multiply;
			if (expressionOperator == Operator.Ne.ToString())
				return Operator.Ne;
			if (expressionOperator == Operator.Or.ToString())
				return Operator.Or;
			if (expressionOperator == Operator.Plus.ToString())
				return Operator.Plus;
			else
				return Operator.Plus;
		}
				
		#endregion
	}
}
