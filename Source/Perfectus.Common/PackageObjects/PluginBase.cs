using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Text;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for PluginBase.
	/// </summary>
	[Serializable]
	public class PluginBase : PackageItem, ICustomTypeDescriptor, ISerializable
	{
		[NonSerialized] private PropertyDescriptorCollection privateProp;

		private PluginDescriptor descriptorFromServer;

		// We'll expose our properties to the outside world in an indexer, but that indexer will get it's values from this datatable.

		private DataTable nameValueTable;

		protected PluginBase()
		{
		}

		public override bool AllowDuplicatesInPackage
		{
			get { return true; }
		}


		public PluginBase(PluginDescriptor descriptorFromServer) : base()
		{
			this.descriptorFromServer = descriptorFromServer;
			this.Name = descriptorFromServer.FriendlyName;
			BuildProperties();
		}

		public PluginDescriptor Descriptor
		{
			get { return descriptorFromServer; }
			set
			{
				descriptorFromServer = value;
				BuildProperties();
			}
		}

        public void MaybeRebuildProperties()
        {
            // BUG 
            if (descriptorFromServer != null &&
                descriptorFromServer.PropertyDescriptors != null &&
                 nameValueTable != null &&
                 descriptorFromServer.PropertyDescriptors.Length > 0 &&
                 nameValueTable.Rows.Count == 0)
                BuildProperties();
        }
			

		private void BuildProperties()
		{
            nameValueTable = new DataTable("Properties");

			foreach (PluginPropertyDescriptor pd in descriptorFromServer.PropertyDescriptors)
			{
				if (pd != null )
				{
                    bool readOnly = pd.ReadOnly;
                    //pd.ReadOnly = false;

					ArrayList attributesToAddList = new ArrayList();
					Attribute[] attributes = null;

					if (pd.Editor != null && pd.Editor.Length > 0)
					{
						Type t = Type.GetType(pd.Editor);
                        
                        // Work around for FB1507
                        if (t == null)
                        {
                            if (pd.Editor.Contains("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionEditor"))
                            {
                                PackageSerialisationBinder binder = new PackageSerialisationBinder();
                                pd.TypeName = binder.BindToType(System.Reflection.Assembly.GetExecutingAssembly().FullName, "Perfectus.Common.PackageObjects.TextQuestionValue").FullName; 
                                pd.Converter = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio";
                                pd.Editor = "Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio";
                                t = Type.GetType(pd.Editor);
                            }
                        }
                        // <- FB1507

						if (t != null)
						{
							attributesToAddList.Add(new EditorAttribute(t, typeof (UITypeEditor)));
						}
					}

					if (pd.Converter != null && pd.Converter.Length > 0)
					{
                        Type t = Type.GetType(pd.Converter);

                        if (t == null)
                        {
                            if (pd.Converter.Contains("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionConverter"))
                            {
                                PackageSerialisationBinder binder = new PackageSerialisationBinder();
                                pd.TypeName = binder.BindToType(System.Reflection.Assembly.GetExecutingAssembly().FullName, "Perfectus.Common.PackageObjects.TextQuestionValue").FullName; 
                                pd.Converter = "Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio";
                                t = Type.GetType(pd.Converter);
                            }
                        }
                        
						if (t != null)
						{
							attributesToAddList.Add(new TypeConverterAttribute(t));
						}
					}

					if (attributesToAddList.Count > 0)
					{
						attributes = (Attribute[]) (attributesToAddList.ToArray(typeof (Attribute)));
					}

					string typeNameToLoad = pd.TypeName;

                    if (typeNameToLoad.IndexOf(",") > 0)
                    {
                        while (typeNameToLoad.IndexOf("Version") > 0)
                        {
                            int indexOfVersionString = typeNameToLoad.IndexOf("Version");

                            if (typeNameToLoad.Substring(indexOfVersionString, typeNameToLoad.Length - (indexOfVersionString + 1)).IndexOf(']') > 0)
                            {
                                string stringToReplace = typeNameToLoad.Substring(indexOfVersionString - 2, (typeNameToLoad.IndexOf(']') + 2) - indexOfVersionString);
                                typeNameToLoad = typeNameToLoad.Replace(stringToReplace, "");
                            }
                            else
                            {
                                string stringToReplace = typeNameToLoad.Substring(indexOfVersionString - 2, ((typeNameToLoad.Length + 2) - indexOfVersionString));
                                typeNameToLoad = typeNameToLoad.Replace(stringToReplace, "");
                            }
                        }
                    }
                    else
                    {
                        typeNameToLoad = pd.TypeName;
                    }

					Type propertyType = Type.GetType(typeNameToLoad);
					if (propertyType != null)
					{
                        //AddProperty(pd.Name, pd.DisplayName, propertyType, pd.DefaultValue, readOnly, pd.Visible, attributes, pd.Description);
                        //dn461
                        AddProperty(pd.Name, pd.Name, propertyType, pd.DefaultValue, readOnly, pd.Visible, attributes, pd.Name);
                    }
                }
			}

		}

		[NonSerialized] private ArrayList displayNameList = new ArrayList();
		[NonSerialized] private ArrayList attributeList = new ArrayList();
		[NonSerialized] private ArrayList dataTypeList = new ArrayList();
        [NonSerialized] private ArrayList readOnlyList = new ArrayList();
        [NonSerialized] private ArrayList visibleList = new ArrayList();

		private void AddProperty(string propName, string displayName, Type propType, object propValue, bool readOnly, bool visible, Attribute[] attributes, string description)
		{
			DataColumn myDC = new DataColumn(propName, propType);
			nameValueTable.Columns.Add(myDC);
			if (nameValueTable.Rows.Count == 0)
			{
				DataRow myDR = nameValueTable.NewRow();
				nameValueTable.Rows.Add(myDR);
			}

			this[propName] = propValue;
			displayNameList.Add(displayName);
			attributeList.Add(attributes);
			dataTypeList.Add(propType.IsArray);
            readOnlyList.Add(readOnly);
            visibleList.Add(visible);
            myDC.Caption = description;
		}

        public object this[string name]
        {
            get
            {
                object v = nameValueTable.Rows[0][name];
                if (Convert.IsDBNull(v))
                {
                    return null;
                }
                else
                {
                    return v;
                }
            }
            set
            {
                if (value == null)
                {
                    nameValueTable.Rows[0][name] = DBNull.Value;
                }
                else
                {
                    // This is the most common case. The underlying editor created 
                    // the correct type...
                    DataColumn dc = nameValueTable.Columns[name];
                    if (dc.DataType == value.GetType())
                        nameValueTable.Rows[0][name] = value;
                    else 

                        // Values can be defaulted like:
                        // -> [DefaultValue("c:\\temp")]
                        // -> public TextQuestionQueryValue FileSystemFolder
		                // That requires a cast from a generic string to whatever type
                        // the property implements
                        try
                        {
                            if (dc.DataType == typeof(TextQuestionQueryValue))
                            {
                                TextQuestionQueryValue tqv = new TextQuestionQueryValue();
                                tqv.TextValue = value.ToString();
                                nameValueTable.Rows[0][name] = tqv;
                            }
                            else if (dc.DataType == typeof(YesNoQueryValue))
                            {
                                YesNoQueryValue yqv = new YesNoQueryValue();
                                yqv.YesNoValue = bool.Parse(value.ToString());
                                nameValueTable.Rows[0][name] = yqv;
                            }
                            else if (dc.DataType == typeof(IntQuestionValue))
                            {
                                IntQuestionValue iqv = new IntQuestionValue();
                                iqv.IntValue = int.Parse(value.ToString());
                                nameValueTable.Rows[0][name] = iqv;
                            }
                            else if ( dc.DataType == typeof(InterviewPage))
                                nameValueTable.Rows[0][name] = value;
                            else
                                // Maybe need more types here...
                                try
                                {
                                    nameValueTable.Rows[0][name] = value;
                                }
                                catch
                                {
                                    nameValueTable.Rows[0][name] = DBNull.Value;
                                }
                        }
                        catch
                        {
                            nameValueTable.Rows[0][name] = DBNull.Value;
                        }
                }
            }
        }
		#region ICustomTypeDescriptor Members

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the name of this class.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new string GetClassName()
		{
			return (TypeDescriptor.GetClassName(this, true));
		}

		public new PropertyDescriptor GetDefaultProperty()
		{
			PropertyDescriptorCollection myProps = GetAllProperties();

			if (myProps.Count > 0)
			{
				return myProps[0];
			}
			else
			{
				return null;
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>
		///		Get the attributes on this class.
		/// </returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new AttributeCollection GetAttributes()
		{
			return (TypeDescriptor.GetAttributes(this, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the name of this component.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new string GetComponentName()
		{
			return (TypeDescriptor.GetComponentName(this, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the type converter for this class.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new TypeConverter GetConverter()
		{
			return (TypeDescriptor.GetConverter(this, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the default event on this class.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new EventDescriptor GetDefaultEvent()
		{
			return (TypeDescriptor.GetDefaultEvent(this, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the editor for a given type.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new object GetEditor(Type editorBaseType)
		{
			return (TypeDescriptor.GetEditor(this, editorBaseType, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// 
		/// </summary>
		/// <param name="attributes"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new EventDescriptorCollection GetEvents(Attribute[] attributes)
		{
			return (TypeDescriptor.GetEvents(this, attributes, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public EventDescriptorCollection GetEvents()
		{
			return (TypeDescriptor.GetEvents(this, true));
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the properties for this class (the ones we've made earlier)</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new PropertyDescriptorCollection GetProperties(Attribute[]
			attributes)
		{
			return (GetAllProperties());
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <returns>Returns the properties for this class (the ones we've made earlier)</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new PropertyDescriptorCollection GetProperties()
		{
			return (GetAllProperties());
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// </summary>
		/// <param name="pd"></param>
		/// <returns>Always returns this class.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public new object GetPropertyOwner(PropertyDescriptor pd)
		{
			return (this);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Turns the internal DataTable of properties into a PropertyDescriptorCollection, as used by the grid.
		/// </summary>
		/// <returns>Returns this class's custom properties, as stored in the internal data table.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		private PropertyDescriptorCollection GetAllProperties()
		{
            if (privateProp == null)
            {
                privateProp = new PropertyDescriptorCollection(new
                    PropertyDescriptor[] { });
                int i = 0;
                foreach (DataColumn myDC in nameValueTable.Columns)
                {
                    if ((bool)visibleList[i])
                    {
                        AddNewColumn myANC = new AddNewColumn(
                            myDC,
                            displayNameList[i].ToString(),
                            (bool)dataTypeList[i],
                            (bool)readOnlyList[i],
                            (Attribute[])attributeList[i]);
                        privateProp.Add(myANC);
                        PropertyDescriptorCollection myPDC = myANC.GetChildProperties();
                    } i++;
                }
            }
			return privateProp;
		}

		/// <summary>
		///		A custom propertyDescriptor for describing our dynamically built properties.
		/// </summary>
		public class AddNewColumn : PropertyDescriptor
		{
			private DataColumn myDC;
			private Type myType;
			private bool isExpandable;
            private bool isReadOnly;
            private string displayName;
			/// <summary>
			///		Constructor.  Maps parameters to private fields for use elsewhere.
			/// </summary>
			/// <param name="thisDC">The data column that describes the property.</param>
			/// <param name="displayName">The displayName of the property.</param>
			/// <param name="expandable">True if the property is an expandable one in the grid.</param>
			/// <param name="attributes">An array of attributes that decorate this property.</param>
			public AddNewColumn(DataColumn thisDC, string displayName, bool expandable, bool readOnly, Attribute[] attributes) : base(thisDC.ColumnName, attributes)
			{
				myDC = thisDC;
				myType = thisDC.GetType();
				isExpandable = expandable;
				this.displayName = displayName;
                isReadOnly = readOnly;
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Returns the display name passed into the constructor.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override string DisplayName
			{
				get { return displayName; }
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Always returns null.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override Type ComponentType
			{
				get { return null; }
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Returns the category name passed into the constructor.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override string Category
			{
				get { return base.Category; }
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Returns true if the datacolumn is marked as readonly.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override bool IsReadOnly
			{
                get { return isReadOnly; }
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Returns either the type of property.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override Type PropertyType
			{
				get
				{
					if (isExpandable)
						return myType;
					else
						return myDC.DataType;
				}
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>Always returns true.</returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override bool CanResetValue(object component)
			{
				return true;
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <summary>
			///		Extracts the value of the property from the data column, via our wrapper class.
			/// </summary>
			/// <param name="component">The component whose property we want to see.</param>
			/// <returns>Returns the value of the property, via our wrapper class's indexer.</returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override object GetValue(object component)
			{
				PluginBase myCC = component as PluginBase;

				if (myCC != null)
					return myCC[myDC.ColumnName];
				else
					throw new ArgumentException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.PluginBase.DynamicProperties"));
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <summary>
			///		Sets the value of the property in the data column, via our wrapper class.
			/// </summary>
			/// <param name="component">The component whose property we want to set.</param>
			/// <param name="value">The value to be set, via our wrapper's indexer.</param>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override void SetValue(object component, object value)
			{
				PluginBase myCC = component as PluginBase;

				if (myCC != null)
					myCC[myDC.ColumnName] = value;
				else
                    throw new ArgumentException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.PluginBase.DynamicProperties"));
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <summary>
			///		Sets the value of the property to null.
			/// </summary>
			/// <param name="component">The class whose property we are resetting.</param>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override void ResetValue(object component)
			{
				PluginBase myCC = component as PluginBase;

				if (myCC != null)
					myCC[myDC.ColumnName] = null;
				else
                    throw new ArgumentException(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.SharedLibrary.PluginBase.DynamicProperties"));
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <summary>
			///		Always returns false.
			/// </summary>
			/// <param name="component"></param>
			/// <returns>Returns false.</returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override bool ShouldSerializeValue(object component)
			{
				return false;
			}

			// ------------------------------------------------------------------------------------------------------------------------------
			/// <returns>
			///		Returns the description of the property, as passed to the constructor.
			/// </returns>
			// ------------------------------------------------------------------------------------------------------------------------------
			public override string Description
			{
				get { return myDC.Caption; }
			}
		}

		#endregion
		#region ISerializable Members

		public PluginBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
            PluginDescriptor d = (PluginDescriptor) info.GetValue("descriptor", typeof (PluginDescriptor));
			this.descriptorFromServer = d;
            this.Name = descriptorFromServer.FriendlyName;
            BuildProperties();

			object oValKeys = info.GetValue("valKeys", typeof (string));
			if (oValKeys != null)
			{
				string valKeys = oValKeys.ToString();
				string[] keys = valKeys.Split('|');
				foreach (string key in keys)
				{
					if (key.Length > 0 && nameValueTable.Rows.Count > 0)
					{
						nameValueTable.Rows[0][key] = info.GetValue(key, typeof (object));
					}
				}
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("descriptor", descriptorFromServer);

			if (nameValueTable.Rows.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				foreach (DataColumn dc in nameValueTable.Columns)
				{
					sb.Append(dc.ColumnName);
					sb.Append("|");
					info.AddValue(dc.ColumnName, nameValueTable.Rows[0][dc]);
				}
				info.AddValue("valKeys", sb.ToString().Trim('|'));
			}
			else
			{
				info.AddValue("valKeys", null);
			}
		}

		#endregion
	}
}