#if UNIT_TESTS

using System;
using System.IO;
using System.Xml;
using NUnit.Framework;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects.Tests
{
	[TestFixture]
	public class QueryFixture
	{
		private const string wordML = "&lt;?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?&gt; &lt;?mso-application progid=\"Word.Document\"?&gt; &lt;w:wordDocument xmlns:w=\"http://schemas.microsoft.com/office/word/2003/wordml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w10=\"urn:schemas-microsoft-com:office:word\" xmlns:sl=\"http://schemas.microsoft.com/schemaLibrary/2003/core\" xmlns:aml=\"http://schemas.microsoft.com/aml/2001/core\" xmlns:wx=\"http://schemas.microsoft.com/office/word/2003/auxHint\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:dt=\"uuid:C2F41010-65B3-11d1-A29F-00AA00C14882\" w:macrosPresent=\"no\" w:embeddedObjPresent=\"no\" w:ocxPresent=\"no\" xml:space=\"preserve\"&gt;&lt;o:DocumentProperties&gt;&lt;o:Title&gt;This came out true&lt;/o:Title&gt;" +
			"&lt;o:Author&gt;Andre Strik&lt;/o:Author&gt;&lt;o:LastAuthor&gt;Andre Strik&lt;/o:LastAuthor&gt;&lt;o:Revision&gt;1&lt;/o:Revision&gt;&lt;o:TotalTime&gt;0&lt;/o:TotalTime&gt;&lt;o:Created&gt;2005-12-07T00:11:00Z&lt;/o:Created&gt;&lt;o:LastSaved&gt;2005-12-07T00:11:00Z&lt;/o:LastSaved&gt;&lt;o:Pages&gt;1&lt;/o:Pages&gt;&lt;o:Words&gt;3&lt;/o:Words&gt;&lt;o:Characters&gt;18&lt;/o:Characters&gt;&lt;o:Lines&gt;1&lt;/o:Lines&gt;&lt;o:Paragraphs&gt;1&lt;/o:Paragraphs&gt;&lt;o:CharactersWithSpaces&gt;20&lt;/o:CharactersWithSpaces&gt;&lt;o:Version&gt;11.6359&lt;/o:Version&gt;&lt;/o:DocumentProperties&gt;&lt;w:fonts&gt;&lt;w:defaultFonts w:ascii=\"Times New Roman\" w:fareast=\"Times New Roman\" w:h-ansi=\"Times New Roman\" w:cs=\"Times New Roman\"/&gt;&lt;/w:fonts&gt;&lt;w:styles&gt;&lt;w:versionOfBuiltInStylenames w:val=\"4\"/&gt;&lt;w:latentStyles w:defLockedState=\"off\" w:latentStyleCount=\"156\"/&gt;&lt;w:style w:type=\"paragraph\" w:default=\"on\" w:styleId=\"Normal\"&gt;" +
			"&lt;w:name w:val=\"Normal\"/&gt;&lt;w:rsid w:val=\"00403C45\"/&gt;&lt;w:rPr&gt;&lt;wx:font wx:val=\"Times New Roman\"/&gt;&lt;w:sz w:val=\"24\"/&gt;&lt;w:sz-cs w:val=\"24\"/&gt;&lt;w:lang w:val=\"EN-NZ\" w:fareast=\"EN-US\" w:bidi=\"AR-SA\"/&gt;&lt;/w:rPr&gt;&lt;/w:style&gt;&lt;w:style w:type=\"character\" w:default=\"on\" w:styleId=\"DefaultParagraphFont\"&gt;&lt;w:name w:val=\"Default Paragraph Font\"/&gt;&lt;w:semiHidden/&gt;&lt;/w:style&gt;&lt;w:style w:type=\"table\" w:default=\"on\" w:styleId=\"TableNormal\"&gt;&lt;w:name w:val=\"Normal Table\"/&gt;&lt;wx:uiName wx:val=\"Table Normal\"/&gt;&lt;w:semiHidden/&gt;&lt;w:rPr&gt;&lt;wx:font wx:val=\"Times New Roman\"/&gt;&lt;/w:rPr&gt;&lt;w:tblPr&gt;&lt;w:tblInd w:w=\"0\" w:type=\"dxa\"/&gt;&lt;w:tblCellMar&gt;&lt;w:top w:w=\"0\" w:type=\"dxa\"/&gt;&lt;w:left w:w=\"108\" w:type=\"dxa\"/&gt;&lt;w:bottom w:w=\"0\" w:type=\"dxa\"/&gt;&lt;w:right w:w=\"108\" w:type=\"dxa\"/&gt;&lt;/w:tblCellMar&gt;&lt;/w:tblPr&gt;&lt;/w:style&gt;" +
			"&lt;w:style w:type=\"list\" w:default=\"on\" w:styleId=\"NoList\"&gt;&lt;w:name w:val=\"No List\"/&gt;&lt;w:semiHidden/&gt;&lt;/w:style&gt;&lt;/w:styles&gt;&lt;w:docPr&gt;&lt;w:view w:val=\"normal\"/&gt;&lt;w:zoom w:val=\"best-fit\" w:percent=\"95\"/&gt;&lt;w:doNotEmbedSystemFonts/&gt;&lt;w:attachedTemplate w:val=\"\"/&gt;&lt;w:defaultTabStop w:val=\"720\"/&gt;&lt;w:punctuationKerning/&gt;&lt;w:characterSpacingControl w:val=\"DontCompress\"/&gt;&lt;w:optimizeForBrowser/&gt;&lt;w:validateAgainstSchema/&gt;&lt;w:saveInvalidXML w:val=\"off\"/&gt;&lt;w:ignoreMixedContent w:val=\"off\"/&gt;&lt;w:alwaysShowPlaceholderText w:val=\"off\"/&gt;&lt;w:compat&gt;&lt;w:breakWrappedTables/&gt;&lt;w:snapToGridInCell/&gt;&lt;w:wrapTextWithPunct/&gt;&lt;w:useAsianBreakRules/&gt;&lt;w:dontGrowAutofit/&gt;&lt;/w:compat&gt;&lt;/w:docPr&gt;&lt;w:body&gt;&lt;wx:sect&gt;&lt;w:p&gt;&lt;w:r&gt;&lt;w:t&gt;This came out true!!&lt;/w:t&gt;&lt;/w:r&gt;&lt;/w:p&gt;&lt;w:sectPr&gt;&lt;w:pgSz w:w=\"12240\" w:h=\"15840\"/&gt;" +
			"&lt;w:pgMar w:top=\"1440\" w:right=\"1800\" w:bottom=\"1440\" w:left=\"1800\" w:header=\"720\" w:footer=\"720\" w:gutter=\"0\"/&gt;&lt;w:cols w:space=\"720\"/&gt;&lt;w:docGrid w:line-pitch=\"360\"/&gt;&lt;/w:sectPr&gt;&lt;/wx:sect&gt;&lt;/w:body&gt;&lt;/w:wordDocument&gt;&lt;/WordML&gt;";

		private const string testXmlSerializedSimpleData = "" +
			"<?xml version=\"1.0\" ?>" + 
			"<Definition>" + 
				"<Dependencies>" + 
					"<ActionIfTrue>" + 
						"<WordML>" + wordML + "</WordML>" + 
						"<SubQuery />" + 
					"</ActionIfTrue>" + 
					"<ActionIfFalse>" + 
						"<WordML>" + wordML + "</WordML>" + 
						"<SubQuery />" + 
					"</ActionIfFalse>" + 
					"<QueryExpression>" + 
						"<Operand1>" + 
							"<Operand1>" + 
								"<Dependancy>" + 
									"<Type>Perfectus.Common.PackageObjects.Question</Type>" + 
									"<Name>Dependent Question</Name>" + 
									"<UniqueIdentifier>a4b28f79-1fd1-48a6-bcbc-8270351fc8a1</UniqueIdentifier>" + 
								"</Dependancy>" + 
							"</Operand1>" + 
							"<Operand2>" + 
								"<Dependancy>" + 
									"<Type>Perfectus.Common.PackageObjects.Question</Type>" + 
									"<Name>Dependent Question</Name>" + 
									"<UniqueIdentifier>a4b28f79-1fd1-48a6-bcbc-8270351fc8a1</UniqueIdentifier>" + 
								"</Dependancy>" + 
							"</Operand2>" + 
							"<ExpressionOperation>Eq</ExpressionOperation>" + 
						"</Operand1>" + 
						"<Operand2>" + 
							"<Operand1>" + 
								"<Dependancy>" + 
									"<Type>Perfectus.Common.PackageObjects.Question</Type>" + 
									"<Name>Dependent Question</Name>" + 
									"<UniqueIdentifier>a4b28f79-1fd1-48a6-bcbc-8270351fc8a1</UniqueIdentifier>" + 
								"</Dependancy>" + 
							"</Operand1>" + 
							"<Operand2>" + 
								"<Dependancy>" + 
									"<Type>Perfectus.Common.PackageObjects.Question</Type>" + 
									"<Name>Dependent Question</Name>" + 
									"<UniqueIdentifier>a4b28f79-1fd1-48a6-bcbc-8270351fc8a1</UniqueIdentifier>" + 
								"</Dependancy>" + 
							"</Operand2>" + 
							"<ExpressionOperation>Ne</ExpressionOperation>" + 
						"</Operand2>" + 
						"<ExpressionOperation>And</ExpressionOperation>" + 
					"</QueryExpression>" + 
				"</Dependencies>" + 
			"</Definition>";

		public QueryFixture()
		{}

		[SetUp]
		public void SetUp()
		{}

		[Test]
		public void SerializeSimpleFixture()
		{
			string operand1Name = "Question Operand 1";
			string operand2Name = "Question Operand 2";
			string wordMLElementName = "<WordML>";
			string queryExpresionElementName = "<QueryExpression>";
			Query query = new Query();
			StringWriter writer = new StringWriter();
			XmlTextWriter w = new XmlTextWriter(writer);

			// set the true false action
			query.ActionIfTrue = new OutcomeAction();
			query.ActionIfFalse = new OutcomeAction();
			((OutcomeAction)query.ActionIfTrue).WordML = wordML;
			((OutcomeAction)query.ActionIfFalse).WordML = wordML;
			
			// Set the query expression
			query.QueryExpression = new QueryExpression();
			query.QueryExpression.RootExpressionNode = new ExpressionNode();
			query.QueryExpression.RootExpressionNode.Operand1 = new Question(operand1Name);
			query.QueryExpression.RootExpressionNode.Operand2 = new Question(operand2Name);
			query.QueryExpression.RootExpressionNode.ExpressionOperator = Operator.Eq;

			// Serialize the object to Xml
			query.WriteXml(w);

			// Assert the serialization
			Assert.IsNotNull(writer);

			// Assert writer information
			Assert.IsTrue(writer.ToString().IndexOf(operand1Name, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(operand2Name, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(wordMLElementName, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(queryExpresionElementName, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf("&lt;o:LastAuthor&gt;Andre Strik&lt;/o:LastAuthor&gt;", 0) > -1);
		}

		/// <summary>
		///		Tests a simple expression - no nesting - actions are simple word ml and a query expression is used
		/// </summary>
		[Test]
		public void DeserializeXmlReaderSimpleTest()
		{
			Query query = new Query();
			StringReader stream = new StringReader(testXmlSerializedSimpleData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			query.ReadXml((XmlReader)r);

			// Test the read xml
			Assert.IsNotNull(query);

			// Ensure the first level of operands are simply expression nodes hold their own expressions

			// Assert Operand 1 Expression
			//------------------------------------

			// Question
			//Assert.AreEqual("Perfectus.Common.PackageObjects.Question", arithmeticExpression.RootExpressionNode.Operand1.GetType().ToString());
			//Assert.AreEqual("Numeric", arithmeticExpression.RootExpressionNode.Operand1.Name);
			//Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", arithmeticExpression.RootExpressionNode.Operand1.UniqueIdentifier.ToString());

			// Package Constant
			//Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", arithmeticExpression.RootExpressionNode.Operand2.GetType().ToString());
			//Assert.AreEqual("27", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).StringValue);
			//Assert.AreEqual("Number", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).DataTypeString);
			//Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", arithmeticExpression.RootExpressionNode.Operand2.UniqueIdentifier.ToString());
				
			// Divide Expression Operator
			//Assert.AreEqual("Plus", arithmeticExpression.RootExpressionNode.ExpressionOperator.ToString());
		}

		[TearDown]
		public void TearDown()
		{}
	}
}

#endif