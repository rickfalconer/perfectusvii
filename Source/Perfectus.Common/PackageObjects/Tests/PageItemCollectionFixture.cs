#if UNIT_TESTS

using System;
using System.IO;
using System.Xml;
using NUnit.Framework;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects.Tests
{
	[TestFixture]
	public class PageItemCollectionFixture
	{
		#region Private Member Variables

		private const string testXmlSerializedSimpleData = "" +
			"<Items>" +
			"	<Item>" +
			"		<RepeaterZone>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"			<Name>Test Repeater Zone</Name>" +
			"			<MaxRows>5</MaxRows>" +
			"			<Items>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 1</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 2</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"			</Items>" +
			"			<Visible />" +
			"		</RepeaterZone>" +
			"	</Item>" +
			"	<Item>" +
			"		<HorizontalLayoutBlock>" +
			"			<UniqueIdentifier>c60796f0-9a97-4f9d-8f29-ca75223cbe72</UniqueIdentifier>" +
			"			<Name>HorizontalLayoutZone Name 1</Name>" +
			"			<Items>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 1</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 2</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 3</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"				<Item>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Question 4</Name>" +
			"						<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Item>" +
			"			</Items>" +
			"			<Visible />" +
			"		</HorizontalLayoutBlock>" +
			"	</Item>" +
			"	<Item>" +
			"		<HtmlBlock>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"			<DesignerHeight>12</DesignerHeight>" +
			"			<Html>this is the body text.</Html>" +
			"			<Visible>" +
			"				<ValueType>Query</ValueType>" +
			"				<YesNoValue>False</YesNoValue>" +
			"			</Visible>" +
			"		</HtmlBlock>" +
			"	</Item>" +
			"	<Item>" +
			"		<Dependancy>" +
			"			<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"			<Name>Question 1</Name>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	</Item>" +
			"	<Item>" +
			"		<Dependancy>" +
			"			<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"			<Name>Question 2</Name>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	</Item>" +
			"	<Item>" +
			"		<Dependancy>" +
			"			<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"			<Name>Question 3</Name>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	</Item>" +
			"	<Item>" +
			"		<Dependancy>" +
			"			<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"			<Name>Question 4</Name>" +
			"			<UniqueIdentifier>00000000-0000-0000-0000-000000000000</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	</Item>" +
			"</Items>";

		#endregion

		public PageItemCollectionFixture()
		{}

		[SetUp]
		public void SetUp()
		{}

		[Test]
		public void SerializeSimpleFixture()
		{
			string horizontalLayoutZoneName = "HorizontalLayoutZone Name 1";
			string repeaterZoneName = "Repeater Zone Name";
			string question1Name = "Question 1 Name";
			string question2Name = "Question 2 Name";
			string question3Name = "Question 3 Name";
			string question4Name = "Question 4 Name";

			PageItemCollection items = new PageItemCollection();
			StringWriter writer = new StringWriter();
			XmlTextWriter w = new XmlTextWriter(writer);

			// Dependent Objects
			Question question1 = new Question(question1Name);
			question1.UniqueIdentifier = new Guid();
			Question question2 = new Question(question2Name);
			question2.UniqueIdentifier = new Guid();
			Question question3 = new Question(question3Name);
			question3.UniqueIdentifier = new Guid();
			Question question4 = new Question(question4Name);
			question4.UniqueIdentifier = new Guid();

			QueryExpression qe = new QueryExpression();
			qe.RootExpressionNode = new ExpressionNode();
			qe.RootExpressionNode.Operand1 = question1;
			qe.RootExpressionNode.Operand2 = question2;
			qe.RootExpressionNode.ExpressionOperator = Operator.Ne;
			Query query = new Query();
			query.QueryExpression = qe;
			YesNoQueryValue yesNoQueryValue1 = new YesNoQueryValue();
			yesNoQueryValue1.YesNoValue = true;
			yesNoQueryValue1.QueryValue = query;

			RepeaterZone repeaterZone = new RepeaterZone();
			repeaterZone.Name = repeaterZoneName;
			repeaterZone.UniqueIdentifier = new Guid();
			repeaterZone.Items.Add(question1);
			repeaterZone.Items.Add(question2);

			HorizontalLayoutZone horizontalLayoutZone = new HorizontalLayoutZone();
			horizontalLayoutZone.Name = horizontalLayoutZoneName;
			horizontalLayoutZone.Items.Add(question1);
			horizontalLayoutZone.Items.Add(question2);
			horizontalLayoutZone.Items.Add(question3);
			horizontalLayoutZone.Items.Add(question4);

			HtmlBlock htmlBlock = new HtmlBlock("this is the body text.", 12, new Guid(), yesNoQueryValue1);

			// set the items
			items.Add(repeaterZone);
			items.Add(horizontalLayoutZone);
			items.Add(htmlBlock);
			items.Add(question1);
			items.Add(question2);
			items.Add(question3);
			items.Add(question4);

			// Serialize the object to Xml
			items.WriteXml(w);

			// Assert the serialization
			Assert.IsNotNull(writer);

			// Assert writer information
			Assert.IsTrue(writer.ToString().IndexOf(horizontalLayoutZoneName, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(repeaterZoneName, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(question1Name, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(question2Name, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(question3Name, 0) > -1);
			Assert.IsTrue(writer.ToString().IndexOf(question4Name, 0) > -1);			
		}

		/// <summary>
		///		Tests a simple expression - no nesting - actions are simple word ml and a query expression is used
		/// </summary>
		[Test]
		public void DeserializeXmlReaderSimpleTest()
		{
			PageItemCollection items = new PageItemCollection();
			StringReader stream = new StringReader(testXmlSerializedSimpleData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			items.ReadXml((XmlReader)r);

			// Test the read xml
			Assert.IsNotNull(items);

			// Ensure the first level of operands are simply expression nodes hold their own expressions

			// Assert Operand 1 Expression
			//------------------------------------

			// Question
			//Assert.AreEqual("Perfectus.Common.PackageObjects.Question", arithmeticExpression.RootExpressionNode.Operand1.GetType().ToString());
			//Assert.AreEqual("Numeric", arithmeticExpression.RootExpressionNode.Operand1.Name);
			//Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", arithmeticExpression.RootExpressionNode.Operand1.UniqueIdentifier.ToString());

			// Package Constant
			//Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", arithmeticExpression.RootExpressionNode.Operand2.GetType().ToString());
			//Assert.AreEqual("27", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).StringValue);
			//Assert.AreEqual("Number", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).DataTypeString);
			//Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", arithmeticExpression.RootExpressionNode.Operand2.UniqueIdentifier.ToString());
				
			// Divide Expression Operator
			//Assert.AreEqual("Plus", arithmeticExpression.RootExpressionNode.ExpressionOperator.ToString());
		}

		[TearDown]
		public void TearDown()
		{}
	}
}

#endif