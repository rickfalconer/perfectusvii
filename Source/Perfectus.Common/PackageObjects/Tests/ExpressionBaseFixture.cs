#if UNIT_TESTS

using System;
using System.IO;
using System.Xml;
using NUnit.Framework;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects.Tests
{
	[TestFixture]
	public class ExpressionBaseFixture
	{
		#region Properties & Constructor

		private string testXmlSerializedSimpleData = "" +
			"<?xml version=\"1.0\" ?>" +
			"<Definition>" +
			"  <Dependencies>" +
			"	 <QueryExpression>" +
			"      <Operand1>" +
			"	    <Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"		  <Name>Numeric</Name>" +
			"		  <UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	  </Operand1>" +
			"	  <Operand2>" +
			"		<Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.PackageConstant</Type>" +
			"		  <UniqueIdentifier>4b95bc22-db7e-4d1f-8675-0aca247ffcad</UniqueIdentifier>" +
			"		  <Value>27</Value>" +
			"		  <DataType>Number</DataType>" +
			"		</Dependancy>" +
			"	  </Operand2>" +
			"	  <ExpressionOperation>Plus</ExpressionOperation>" +
			"	 </QueryExpression>" +
			"  </Dependencies>" +
			"</Definition>";

		private string testXmlSerializedComplexData = "" +
			"<?xml version=\"1.0\" ?>" +
			"<Definition>" +
			"  <Dependencies>" +
			"	 <QueryExpression>" +
			"    <Operand1>" +
			"      <Operand1>" +
			"	    <Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"		  <Name>Numeric</Name>" +
			"		  <UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	  </Operand1>" +
			"	  <Operand2>" +
			"		<Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.PackageConstant</Type>" +
			"		  <UniqueIdentifier>4b95bc22-db7e-4d1f-8675-0aca247ffcad</UniqueIdentifier>" +
			"		  <Value>27</Value>" +
			"		  <DataType>Number</DataType>" +
			"		</Dependancy>" +
			"	  </Operand2>" +
			"	  <ExpressionOperation>Plus</ExpressionOperation>" +
			"	</Operand1>" +
			"	<Operand2>" +
			"	  <Operand1>" +
			"		<Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.ArithmeticPFunction</Type>" +
			"		  <Name>New Arithmetic Function_1</Name>" +
			"		  <UniqueIdentifier>3bdca67a-9430-442d-8802-0a0938bbb030</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	  </Operand1>" +
			"	  <Operand2>" +
			"		<Dependancy>" +
			"		  <Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"		  <Name>Numeric</Name>" +
			"		  <UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"		</Dependancy>" +
			"	  </Operand2>" +
			"	  <ExpressionOperation>Minus</ExpressionOperation>" +
			"	</Operand2>" +
			"	<ExpressionOperation>Divide</ExpressionOperation>" +
			"	 </QueryExpression>" +
			"  </Dependencies>" +
			"</Definition>";

		private string testXmlSerialized3NestedComplexData = "" +
			"<?xml version=\"1.0\" ?>" +
			"<Definition>" +
			"	<Dependencies>" +
			"	 <QueryExpression>" +
			"	    <Operand1>" +
			"			<Operand1>" +
			"				<Operand1>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Numeric</Name>" +
			"						<UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Operand1>" +
			"				<Operand2>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.PackageConstant</Type>" +
			"						<UniqueIdentifier>4b95bc22-db7e-4d1f-8675-0aca247ffcad</UniqueIdentifier>" +
			"						<Value>27</Value>" +
			"						<DataType>Number</DataType>" +
			"					</Dependancy>" +
			"				</Operand2>" +
			"				<ExpressionOperation>Plus</ExpressionOperation>" +
			"			</Operand1>" +
			"			<Operand2>" +
			"				<Operand1>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Numeric</Name>" +
			"						<UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Operand1>" +
			"				<Operand2>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.PackageConstant</Type>" +
			"						<UniqueIdentifier>4b95bc22-db7e-4d1f-8675-0aca247ffcad</UniqueIdentifier>" +
			"						<Value>27</Value>" +
			"						<DataType>Number</DataType>" +
			"					</Dependancy>" +
			"				</Operand2>" +
			"				<ExpressionOperation>Plus</ExpressionOperation>" +
			"			</Operand2>" +
			"			<ExpressionOperation>Plus</ExpressionOperation>" +
			"		</Operand1>" +
			"		<Operand2>" +
			"			<Operand1>" +
			"				<Operand1>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.ArithmeticPFunction</Type>" +
			"						<Name>New Arithmetic Function_1</Name>" +
			"						<UniqueIdentifier>3bdca67a-9430-442d-8802-0a0938bbb030</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Operand1>" +
			"				<Operand2>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"						<Name>Numeric</Name>" +
			"						<UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Operand2>" +
			"				<ExpressionOperation>Minus</ExpressionOperation>" +
			"			</Operand1>" +
			"			<Operand2>" +
			"				<Operand1>" +
			"					<Dependancy>" +
			"						<Type>Perfectus.Common.PackageObjects.ArithmeticPFunction</Type>" +
			"						<Name>New Arithmetic Function_1</Name>" +
			"						<UniqueIdentifier>3bdca67a-9430-442d-8802-0a0938bbb030</UniqueIdentifier>" +
			"					</Dependancy>" +
			"				</Operand1>" +
			"				<Operand2>" +
			"					<Dependancy>" +
			"					<Type>Perfectus.Common.PackageObjects.Question</Type>" +
			"					<Name>Numeric</Name>" +
			"					<UniqueIdentifier>893dac1b-4610-4cd2-8018-dbb339c9cc33</UniqueIdentifier>" +
			"				</Dependancy>" +
			"			</Operand2>" +
			"			<ExpressionOperation>Minus</ExpressionOperation>" +
			"		</Operand2>" +
			"		<ExpressionOperation>Plus</ExpressionOperation>" +
			"	  </Operand2>" +
			"     <ExpressionOperation>Plus</ExpressionOperation>" +
			"	 <QueryExpression>" +
			"	</Dependencies>" +
			"</Definition>";

		public ExpressionBaseFixture() { }

		#endregion

		[SetUp]
		public void SetUp()
		{
		}

		/// <summary>
		///		Tests a simple expression - no nesting - expression = operand1, operand2, expressionoperator
		/// </summary>
		[Test]
		public void DeserializeXmlReaderSimpleTest()
		{
			ArithmeticExpression arithmeticExpression = new ArithmeticExpression();
			StringReader stream = new StringReader(testXmlSerializedSimpleData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			arithmeticExpression.ReadXml((XmlReader)r);

			// Test the read xml
			Assert.IsNotNull(arithmeticExpression.RootExpressionNode);

			// Ensure the first level of operands are simply expression nodes hold their own expressions

			// Assert Operand 1 Expression
			//------------------------------------

			// Question
			Assert.AreEqual("Perfectus.Common.PackageObjects.Question", arithmeticExpression.RootExpressionNode.Operand1.GetType().ToString());
			Assert.AreEqual("Numeric", arithmeticExpression.RootExpressionNode.Operand1.Name);
			Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", arithmeticExpression.RootExpressionNode.Operand1.UniqueIdentifier.ToString());

			// Package Constant
			Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", arithmeticExpression.RootExpressionNode.Operand2.GetType().ToString());
			Assert.AreEqual("27", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).StringValue);
			Assert.AreEqual("Number", ((LibraryItem)arithmeticExpression.RootExpressionNode.Operand2).DataTypeString);
			Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", arithmeticExpression.RootExpressionNode.Operand2.UniqueIdentifier.ToString());
				
			// Divide Expression Operator
			Assert.AreEqual("Plus", arithmeticExpression.RootExpressionNode.ExpressionOperator.ToString());
		}

		/// <summary>
		///		Tests a complex expression - expression node nesting 
		///		expression = 
		///			operand1 (operand1, operand2, expressionoperator), 
		///			operand2 (operand1, operand2, expressionoperator), expressionoperator
		/// </summary>
		[Test]
		public void DeserializeXmlReaderComplexTest()
		{
			ArithmeticExpression arithmeticExpression = new ArithmeticExpression();
			StringReader stream = new StringReader(testXmlSerializedComplexData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			arithmeticExpression.ReadXml((XmlReader)r);

			// Test the read xml
			Assert.IsNotNull(arithmeticExpression.RootExpressionNode);

			// Ensure the first level of operands are simply expression nodes hold their own expressions

			// Assert Operand 1 Expression
			//------------------------------------

			// Question
			Assert.AreEqual("Perfectus.Common.PackageObjects.Question", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1.GetType().ToString());
			Assert.AreEqual("Numeric", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1.Name);
			Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1.UniqueIdentifier.ToString());

			// Package Constant
			Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2.GetType().ToString());
			Assert.AreEqual("27", ((LibraryItem)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).StringValue);
			Assert.AreEqual("Number", ((LibraryItem)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).DataTypeString);
			Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2.UniqueIdentifier.ToString());
				
			// Plus Expression Operator
			Assert.AreEqual("Plus", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).ExpressionOperator.ToString());
			
			// Assert Operand 2 Expression
			//------------------------------------

			// Arithmetic PFunction
			Assert.AreEqual("Perfectus.Common.PackageObjects.ArithmeticPFunction", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand1.GetType().ToString());
			Assert.AreEqual("New Arithmetic Function_1", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand1.Name);
			Assert.AreEqual("3bdca67a-9430-442d-8802-0a0938bbb030", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand1.UniqueIdentifier.ToString());

			// Question
			Assert.AreEqual("Perfectus.Common.PackageObjects.Question", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand2.GetType().ToString());
			Assert.AreEqual("Numeric", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand2.Name);
			Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).Operand2.UniqueIdentifier.ToString());

			// Minus Expression Operator
			Assert.AreEqual("Minus", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).ExpressionOperator.ToString());

			// Combined Expression Operator
			Assert.AreEqual("Divide", arithmeticExpression.RootExpressionNode.ExpressionOperator.ToString());
		}

	
		[Test]
		public void DeserializeXmlReader3NestedComplexTest()
		{
			ArithmeticExpression arithmeticExpression = new ArithmeticExpression();
			StringReader stream = new StringReader(testXmlSerialized3NestedComplexData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			arithmeticExpression.ReadXml((XmlReader)r);

			// Test the read xml
			Assert.IsNotNull(arithmeticExpression.RootExpressionNode);

			// Ensure the first level of operands are simply expression nodes hold their own expressions

			// Assert Operand 1 Expression
			//------------------------------------

			// Question
			Assert.AreEqual("Perfectus.Common.PackageObjects.Question", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand1.GetType().ToString());
			Assert.AreEqual("Numeric", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand1.Name);
			Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand1.UniqueIdentifier.ToString());

			// Package Constant
			Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand2.GetType().ToString());
			Assert.AreEqual("27", ((LibraryItem)((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand2).StringValue);
			Assert.AreEqual("Number", ((LibraryItem)((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand2).DataTypeString);
			Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand1).Operand2.UniqueIdentifier.ToString());
				
			// Plus Expression Operator
			Assert.AreEqual("Plus", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).ExpressionOperator.ToString());
			
			// Assert Operand 2 Expression
			//------------------------------------

			// Arithmetic PFunction
			Assert.AreEqual("Perfectus.Common.PackageObjects.Question", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand1.GetType().ToString());
			Assert.AreEqual("Numeric", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand1.Name);
			Assert.AreEqual("893dac1b-4610-4cd2-8018-dbb339c9cc33", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand1.UniqueIdentifier.ToString());

			// Question
			Assert.AreEqual("Perfectus.Common.PackageObjects.PackageConstant", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand2.GetType().ToString());
			Assert.AreEqual("27", ((LibraryItem)((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand2).StringValue);
			Assert.AreEqual("Number", ((LibraryItem)((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand2).DataTypeString);
			Assert.AreEqual("4b95bc22-db7e-4d1f-8675-0aca247ffcad", ((ExpressionNode)((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand1).Operand2).Operand2.UniqueIdentifier.ToString());

			// Plus Expression Operator
			Assert.AreEqual("Plus", ((ExpressionNode)arithmeticExpression.RootExpressionNode.Operand2).ExpressionOperator.ToString());

			// Combined Expression Operator
			Assert.AreEqual("Plus", arithmeticExpression.RootExpressionNode.ExpressionOperator.ToString());
		}
	
		[Test]
		public void CreateTableFromExpressionSimpleFixture()
		{
			ArithmeticExpression arithmeticExpression = new ArithmeticExpression();
			StringReader stream = new StringReader(testXmlSerializedSimpleData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			arithmeticExpression.ReadXml((XmlReader)r);

			// Populate the internal table from the root expression node
			arithmeticExpression.CreateTableFromExpression();

			// Asserts
			Assert.IsNotNull(arithmeticExpression.Table);

			Assert.AreEqual(arithmeticExpression.Table.Rows[0][0].ToString(), "");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][1].ToString(), "(");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][2].GetType().ToString(), "Perfectus.Common.PackageObjects.Question");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][3].ToString(), "+");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][4].GetType().ToString(), "Perfectus.Common.PackageObjects.PackageConstant");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][5].ToString(), ")");
		}
		
		[Test]
		public void CreateTableFromExpressionComplexFixture()
		{
			ArithmeticExpression arithmeticExpression = new ArithmeticExpression();
			StringReader stream = new StringReader(testXmlSerializedComplexData);
			XmlTextReader r = new XmlTextReader(stream);
			
			// Read the xml 
			arithmeticExpression.ReadXml((XmlReader)r);

			// Populate the internal table from the root expression node
			arithmeticExpression.CreateTableFromExpression();

			// Asserts
			Assert.IsNotNull(arithmeticExpression.Table);

			Assert.AreEqual(arithmeticExpression.Table.Rows[0][0].ToString(), "");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][1].ToString(), "(");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][2].GetType().ToString(), "Perfectus.Common.PackageObjects.Question");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][3].ToString(), "+");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][4].GetType().ToString(), "Perfectus.Common.PackageObjects.PackageConstant");
			Assert.AreEqual(arithmeticExpression.Table.Rows[0][5].ToString(), ")");

			Assert.AreEqual(arithmeticExpression.Table.Rows[1][0].ToString(), "/");
			Assert.AreEqual(arithmeticExpression.Table.Rows[1][1].ToString(), "(");
			Assert.AreEqual(arithmeticExpression.Table.Rows[1][2].GetType().ToString(), "Perfectus.Common.PackageObjects.ArithmeticPFunction");
			Assert.AreEqual(arithmeticExpression.Table.Rows[1][3].ToString(), "-");
			Assert.AreEqual(arithmeticExpression.Table.Rows[1][4].GetType().ToString(), "Perfectus.Common.PackageObjects.Question");
			Assert.AreEqual(arithmeticExpression.Table.Rows[1][5].ToString(), ")");
		}

		[TearDown]
		public void TearDown()
		{}
	}
}

#endif