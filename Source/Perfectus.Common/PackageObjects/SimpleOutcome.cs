using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for SimpleOutcome.
	/// </summary>
	[Serializable]
	public class SimpleOutcome : LibraryItem, ISerializable, IPackageItemContainer
	{
		#region Private Member Variables

		private Query definition;

		// Defines the Shared Library type for this class
		private LibraryItemType _LibraryItemType = LibraryItemType.SimpleOutcome;

		#endregion
		#region Properties & Constructor

		[Category("SharedLibrary")] //TODO - need to define the category
		[Browsable(false)]
		public override LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.OutcomeConverter, Studio")]
        [DisplayRanking( 2 )]
        public Query Definition
		{
			get { return definition;  }
			set { definition = value; }
		}

		public SimpleOutcome() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.SimpleOutcome.NewItemName");
		}

		#endregion
		#region Methods

        public override Boolean IsValid(ref String message)
        {
            if (Definition == null)
            {
                message = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ConditionalText.QueryNotValid");
                return false;
            }
            return true;
        }

        public override List<LibraryItem> GetDependentLibraryItems()
		{
            List<LibraryItem> dependentLibrayrItems = new List<LibraryItem>();

			// Only set if a definition has been defined
			if (this.Definition != null)
				SetDependentObjects(dependentLibrayrItems, this.Definition.QueryExpression.RootExpressionNode);
			
			return dependentLibrayrItems;
		}

        public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

        private void SetDependentObjects(List<LibraryItem> dependentLibraryItems, PackageItem packageItem)
		{
			if (packageItem is ExpressionNode)
			{
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand1);
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand2);
			}
			else
			{
				if (packageItem != null && !(packageItem is PackageConstant))
				{
					// Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
					if (((LibraryItem)packageItem).GetType() != typeof(PackageConstant) && 
                        !dependentLibraryItems.Contains((LibraryItem) packageItem))
					{
						dependentLibraryItems.Add((LibraryItem) packageItem);
					}
				}
			}
		}

		public string GetTagName()
		{
			return SafeTagName;
		}

		public override object Clone()
		{
			SimpleOutcome o = (SimpleOutcome)this.MemberwiseClone();
			if (o.Definition != null)
			{
				o.Definition = (Query)(definition.Clone());
			}
			return o;
		}

		#endregion
		#region ISerializable Members

		public SimpleOutcome(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			definition = (Query) info.GetValue("definition", typeof (Query));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("definition", definition);
		}

		#endregion
		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			if (definition != null)
			{
				return definition.ContainsReferenceTo(item);
			}
			else
			{
				return false;
			}
		}

		#endregion
		#region Shared Library

		public void Update(SimpleOutcome simpleOutcome)
		{
            if (this.Name != simpleOutcome.Name)
            {
                this.Name = simpleOutcome.Name;
            }

			// Shared Library Item Properties
			this.LibraryUniqueIdentifier = simpleOutcome.LibraryUniqueIdentifier;	
			this.Version = simpleOutcome.Version;
			this.LibraryType = simpleOutcome.LibraryType;
			this.Linked = simpleOutcome.Linked;
			this.CreatedBy = simpleOutcome.CreatedBy;		
			this.CreatedDateTime = simpleOutcome.CreatedDateTime;
			this.CheckedOutBy = simpleOutcome.CheckedOutBy;
			this.CheckedOutByLogInName = simpleOutcome.CheckedOutByLogInName;
			this.CheckedOutDateTime = simpleOutcome.CheckedOutDateTime;
			this.CheckedOutStatus = simpleOutcome.CheckedOutStatus;
			this.ModifiedBy = simpleOutcome.ModifiedBy;
			this.ModifiedDateTime = simpleOutcome.ModifiedDateTime;
			this.CheckInComment = simpleOutcome.CheckInComment;
			this.LocationPath = simpleOutcome.LocationPath;
			this.RelativePath = simpleOutcome.RelativePath;
			this.Filename = simpleOutcome.Filename;
			this.Touch = simpleOutcome.Touch;
			this.Notes = simpleOutcome.Notes;
			this.Definition = new Query();
            this.Definition.ParentPackage = this.ParentPackage;
			this.Definition.QueryExpression = new QueryExpression();
            this.DependentLibraryItems = simpleOutcome.DependentLibraryItems;

			// Reconnect the query
			if (simpleOutcome.Definition != null)
			{
				this.Definition = simpleOutcome.Definition;
                QueryExpression queryExpression = simpleOutcome.definition.QueryExpression;
                ExpressionNode expressionNode = (queryExpression != null) ? queryExpression.RootExpressionNode : null;

                if (expressionNode != null)
                {
                    this.Definition.QueryExpression.RootExpressionNode = ReconnectDependentObjects(expressionNode);
                }
			}

            this.Definition.QueryExpression.CreateTableFromExpression();
            this.Definition.QueryExpression.ParentPackage = this.ParentPackage;
		}

		private ExpressionNode ReconnectDependentObjects(ExpressionNode expressionNode)
		{
			if (expressionNode.Operand1 != null && expressionNode.Operand2 != null)
			{
				// Handle Operand 1 - nested or not
				if (expressionNode.Operand1.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand1);
				}
				else
				{
					// Connect library item
					expressionNode.Operand1 = GetDependentPackageItem((LibraryItem)expressionNode.Operand1);
				}
		
				// Handle Operand 2 - nested or not
				if (expressionNode.Operand2.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand2);
				}
				else
				{
					// Connect library item
					expressionNode.Operand2 = GetDependentPackageItem((LibraryItem)expressionNode.Operand2);
				}
			}

			return expressionNode;
		}

		#endregion
	}
}
