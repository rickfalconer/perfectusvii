using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type object
	/// </summary>
	[Serializable]
	public sealed class AnswerCollection : CollectionBase
	{
		private Question parentQuestion;

		public Question ParentQuestion
		{
			get { return parentQuestion; }
			set { parentQuestion = value; }
		}

		/// <summary>
		/// Initializes a new empty instance of the AnswerCollection class.
		/// </summary>
		public AnswerCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the AnswerCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new AnswerCollection.
		/// </param>
		public AnswerCollection(object[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the AnswerCollection class, containing elements
		/// copied from another instance of AnswerCollection
		/// </summary>
		/// <param name="items">
		/// The AnswerCollection whose elements are to be added to the new AnswerCollection.
		/// </param>
		public AnswerCollection(AnswerCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this AnswerCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this AnswerCollection.
		/// </param>
		public void AddRange(object[] items)
		{
			foreach (object item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another AnswerCollection to the end of this AnswerCollection.
		/// </summary>
		/// <param name="items">
		/// The AnswerCollection whose elements are to be added to the end of this AnswerCollection.
		/// </param>
		public void AddRange(AnswerCollection items)
		{
			foreach (object item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type object to the end of this AnswerCollection.
		/// </summary>
		/// <param name="value">
		/// The object to be added to the end of this AnswerCollection.
		/// </param>
		public void Add(object value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic object value is in this AnswerCollection.
		/// </summary>
		/// <param name="value">
		/// The object value to locate in this AnswerCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this AnswerCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(object value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this AnswerCollection
		/// </summary>
		/// <param name="value">
		/// The object value to locate in the AnswerCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(object value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the AnswerCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the object is to be inserted.
		/// </param>
		/// <param name="value">
		/// The object to insert.
		/// </param>
		public void Insert(int index, object value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the object at the given index in this AnswerCollection.
		/// </summary>
		public object this[int index]
		{
			get { return (object) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific object from this AnswerCollection.
		/// </summary>
		/// <param name="value">
		/// The object value to remove from this AnswerCollection.
		/// </param>
		public void Remove(object value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by AnswerCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(AnswerCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public object Current
			{
				get { return (object) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (object) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this AnswerCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}