using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Web;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Provides static methods useful for string binding related tasks.
    /// </summary>
    public abstract class StringBinding
    {
        #region Drag and drop data creation

        public static string MakeHTMLTags(Package package, Guid uId, string defaultDisplay, string fieldName)
        {
            if (package.StringBindingItemByGuid.Contains(uId))
            {
                IStringBindingItem item = package.StringBindingItemByGuid[uId];
                return GenerateHTMLItemTags(item, defaultDisplay, fieldName);
            }
            else
            {
                return "";
            }
        }


        public static string MakeSimpleHTMLTags(Package package, Guid uId, string defaultDisplay)
        {
            if (package.StringBindingItemByGuid.Contains(uId))
            {
                IStringBindingItem item = package.StringBindingItemByGuid[uId];
                return string.Format("�{0}�", item.GetTagName());
            }
            else
            {
                return "";
            }
        }


        public static void AddHTMLTagToDataObject(DataObject inObj, IStringBindingItem item, string fieldName)
        {
            string output = GenerateHTMLItemTags(item, "", fieldName);
            inObj.SetData(DataFormats.Html, output);
        }

        public static string GenerateHTMLItemTags(IStringBindingItem item, string defaultText, string fieldName)
        {
            
            if (fieldName == null)
            {
                return string.Format("<span type=\"{0}\" id=\"{1}\" contentEditable=\"false\" class=\"{2}\">{3}{4}{5}{6}{7}</span>", "stringItem", ((PackageItem)item).UniqueIdentifier, "StringBindingDiv", GenerateFirstImageTag(), GenerateNameTag(item, null), GenerateSecondImageTag(), GenerateDefaultDisplay(defaultText), GenerateFinalImageTag());
            }
            else
            {
                return string.Format("<span type=\"{0}\" id=\"{1}\" fieldId=\"{8}\" contentEditable=\"false\" class=\"{2}\">{3}{4}{5}{6}{7}</span>", "stringItem", ((PackageItem)item).UniqueIdentifier, "StringBindingDiv", GenerateFirstImageTag(), GenerateNameTag(item, fieldName), GenerateSecondImageTag(), GenerateDefaultDisplay(defaultText), GenerateFinalImageTag(), fieldName);                
            }
        }

        private static string GenerateFirstImageTag()
        {
            return "<span class=\"Image1\"></span>";
        }

        private static string GenerateNameTag(IStringBindingItem item, string fieldName)
        {
            IStringBindingItem sbItem = item;
            return string.Format("<span type=\"itemName\" class=\"{0}\">{1}{2}</span>", "stringBindingSpan", sbItem.GetTagName(), fieldName == null ? null : "." + fieldName);
        }

        private static string GenerateSecondImageTag()
        {
            return "<span class=\"Image2\"></span>";
        }

        private static string GenerateDefaultDisplay(string defaultText)
        {
            return string.Format("<span type=\"defaultDisplay\" contentEditable=\"true\" class=\"{0}\">{1}</span>", "StringBindingDefault", defaultText);
        }

        private static string GenerateFinalImageTag()
        {
            string tag = null;
            tag = "<span class=\"Image3\"></span>";
            return tag;
        }


        #endregion

        #region helper methods

        public static string GetItemNodeXml(PackageItem packageItem, string defaultText)
        {
            string retVal = null;
            string uId = packageItem.UniqueIdentifier.ToString();
            retVal = string.Format("<item uId=\"{0}\">{1}</item>", uId, defaultText);
            return retVal;
        }

        public static PackageItem[] GetPackageItemsFromXml(string xml, StringBindingItemDictionary itemDict)
        {
            if (xml == null || xml.IndexOf("<text>") == -1)
            {
                return null;
            }
            
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(EncodeXml(xml));

            XmlNodeList list = doc.GetElementsByTagName("item");
            PackageItem[] retVal = new PackageItem[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                string strUId = list[i].Attributes["uId"].Value;

                if (strUId != string.Empty && strUId.Length != 0)
                {
                    Guid uId;

                    try
                    {
                        uId = new Guid(strUId);
                    }
                    catch (Exception ex)
                    {
                        //TODO - need to handle this
                        return null;
                    }

                    AnswerProvider item = (AnswerProvider)itemDict[uId];
                    retVal[i] = item;
                }
            }

            return retVal;
        }

        #endregion

        #region server evaluation methods

        /// <summary>
        /// Takes a string that may contain xml with string binding info, and performs any neccessary bindings.
        /// </summary>
        /// <param name="xmlBody">The string to perform string binding operations on</param>
        ///  <param name="caller">caller</param>
        ///  <param name="itemDict">item dict</param>
        /// <returns>The modified string</returns>
        //public static string EvaluateStringBindingXml(string xmlBody, AnswerProvider.Caller caller, StringBindingItemDictionary itemDict)
        //{
        //    return EvaluateStringBindingXml(xmlBody, caller, itemDict, false);
        //}

        /// <summary>
        /// Takes a string that may contain xml with string binding info, and performs any neccessary bindings.
        /// </summary>
        /// <param name="xmlBody">The string to perform string binding operations on</param>
        ///  <param name="caller">caller</param>
        ///  <param name="itemDict">item dict</param>
        /// <returns>The modified string</returns>
        public static string EvaluateStringBindingXml(string xmlBody, AnswerProvider.Caller caller, StringBindingItemDictionary itemDict, bool encodeNonTopLevelAnswers, StringBindingMetaData stringBindingMetaData)
        {
            if (xmlBody == null || xmlBody.IndexOf("<text>") == -1)
            {
                return xmlBody;
            }

            Regex itemReg = new Regex("<item.*</item>");
            Match itemMatch = itemReg.Match(xmlBody, 0, xmlBody.IndexOf("</item>") + 8); // we only want to get one at a time.

            while (itemMatch.Success)
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(itemMatch.ToString());

                string strUId = doc.SelectSingleNode("/item/@uId").Value;
                string fieldId = null;
                XmlNode fieldNode = doc.SelectSingleNode("/item/@fieldId");
                if (fieldNode != null)
                {
                    fieldId = fieldNode.InnerText;
                }

                Guid uId;

                try
                {
                    uId = new Guid(strUId);
                }
                catch (Exception ex)
                {
                    //TODO - need to handle this
                    return string.Empty;
                }

                AnswerProvider item = (AnswerProvider)itemDict[uId];

                object answer = item.GetSingleAnswer(caller);
                string stringWithBinding = string.Empty;

                if (answer != null)
                {
                    if (fieldId != null && answer.ToString().Trim().StartsWith("<multiValue>"))
                    {
                        XmlDocument multiDoc = new XmlDocument();
                        try
                        {
                            //RDF 14 May 2013 PF-402 (FB 2523): & in partner picker.  multiValue is not always valid xml, need to escape the &, and replace it when done.
                            multiDoc.LoadXml(answer.ToString().Replace("&", "&amp;"));
                            XmlNode valueNode = multiDoc.SelectSingleNode(string.Format("/multiValue/field[@id='{0}']", fieldId));
                            if (valueNode != null)
                            {
                                answer = valueNode.InnerXml.Replace("&amp;", "&");
                            }
                        }
                        catch
                        {
                            // Do nothing, leave answer alone and bind normally ignoring the field stuff.
                        }
                    }
                    stringWithBinding = EvaluateStringBindingXml(answer.ToString(), caller, itemDict, encodeNonTopLevelAnswers, stringBindingMetaData);
                }

                if (stringWithBinding.Length == 0)
                {
                    stringWithBinding = doc.SelectSingleNode("/item").InnerXml;
                }

                if (encodeNonTopLevelAnswers)
                {
                    stringWithBinding = HttpUtility.UrlEncode(stringWithBinding);
                }

                xmlBody = xmlBody.Remove(itemMatch.Index, itemMatch.Length);
                xmlBody = xmlBody.Insert(itemMatch.Index, stringWithBinding);

                itemMatch = itemReg.Match(xmlBody, 0, xmlBody.IndexOf("</item>") + 8);
            }

            xmlBody = xmlBody.Replace("<text>", "");
            xmlBody = xmlBody.Replace("</text>", "");

            return xmlBody;
        }

        /// <summary>
        /// Takes a string that may contain xml with string binding info, and performs any neccessary bindings.
        /// </summary>
        /// <param name="xmlBody">The string to perform string binding operations on</param>
        /// <returns>The modified string</returns>
        public static string EvaluateStringBindingMetaData(string xmlBody, StringBindingMetaData stringBindingMetaData)
        {
            if (xmlBody.IndexOf("{") == -1 || xmlBody.IndexOf("}") == -1)
            {
                return xmlBody;
            }

            ReplaceMetadataSections(ref xmlBody, ref stringBindingMetaData);
            return xmlBody;
        }

        public static void ReplaceMetadataSections(ref string xmlBody, ref StringBindingMetaData stringBindingMetaData)
        {
            string[] metadataTypes = new string[5]{"{InstanceId}", "{PackageName}", "{InstanceName}", "{PackageVersion}", "{PackageRevision}"};

            foreach (string s in metadataTypes)
            {
                string metadata = string.Empty;

                switch (s)
                {
                    case "{InstanceId}":
                        metadata = stringBindingMetaData.PackageInstanceId.ToString();
                        break;
                    case "{PackageName}":
                        metadata = stringBindingMetaData.PackageName;
                        break;
                    case "{InstanceName}":
                        metadata = stringBindingMetaData.InstanceName;
                        break;
                    case "{PackageVersion}":
                        metadata = stringBindingMetaData.PackageVersion.ToString();
                        break;
                    case "{PackageRevision}":
                        metadata = stringBindingMetaData.PackageRevision.ToString();
                        break;
                }

                xmlBody = ReplaceSection(xmlBody, 0, s, metadata);
            }
        }

        private static string ReplaceSection(string xmlBody, int placeMark, string s, string metadata)
        {
            int indexOfMetaSection = xmlBody.IndexOf(s, placeMark, xmlBody.Length - placeMark);

            if (indexOfMetaSection > -1)
            {
                int indexOfEscapeChar = indexOfMetaSection - 1;
                if (indexOfEscapeChar > -1)
                {
                    if (xmlBody[indexOfEscapeChar] != '\\')
                    {
                        xmlBody = xmlBody.Remove(indexOfMetaSection, s.Length);
                        if (null != metadata)
                        {
                            xmlBody = xmlBody.Insert(indexOfMetaSection, metadata);

                            if (xmlBody.IndexOf(s, placeMark) > -1)
                            {
                                xmlBody = ReplaceSection(xmlBody, placeMark, s, metadata);
                            }
                        }
                    }
                    else
                    {
                        xmlBody = xmlBody.Remove(indexOfEscapeChar, 1);
                        placeMark = indexOfMetaSection;
                        xmlBody = ReplaceSection(xmlBody, placeMark, s, metadata);
                    }
                }
                else
                {
                    xmlBody = xmlBody.Remove(indexOfMetaSection, s.Length);

                    if (metadata != null)
                    {
                        xmlBody = xmlBody.Insert(indexOfMetaSection, metadata);
                        if (null != metadata)
                        {

                            if (xmlBody.IndexOf(s) > 0)
                            {
                                xmlBody = ReplaceSection(xmlBody, xmlBody.IndexOf(s), s, metadata);
                            }
                        }
                    }
                }
            }
            return xmlBody;
        }

        #endregion

        #region SharedLibrary specific

        /// <summary>
        /// Takes a string such as "<text><item uId="abc" /></text>" and turns it into "<text><item lId="123" uId="abc" /></text>" 
        /// This is used in the sharedlibrary, so that the uniqueID can be restored to the new one later on based on the libraryId.
        /// </summary>
        /// <param name="xmlString">The string to convert.</param>\
        /// <param name="libraryValues">Values to get lId from.</param>
        /// <returns>The string containing lID attributes.</returns>
        public static string AppendLibraryIdAttributes(string xmlBody, PackageItem[] libraryValues)
        {
            if (xmlBody == null || xmlBody.IndexOf("<text>") == -1)
            {
                return xmlBody;
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(EncodeXml(xmlBody));

            XmlNodeList nodes = doc.GetElementsByTagName("item");

            for (int i = 0; i < nodes.Count; i++)
            {
                XmlNode node = nodes[i].SelectSingleNode("@uId");
                string uId = string.Empty;

                if (node != null)
                {
                    uId = node.Value;
                }

                XmlAttribute att = doc.CreateAttribute("lId");

                for (int y = 0; y < libraryValues.Length; y++)
                {
                    if (libraryValues[y] != null && libraryValues[y].UniqueIdentifier.ToString() == uId)
                    {
                        att.Value = ((LibraryItem)libraryValues[y]).LibraryUniqueIdentifier.ToString();
                    }
                }

                nodes[i].Attributes.Append(att);
            }

            return doc.OuterXml;
        }

        /// <summary>
        /// Takes a string such as "<text><item lId="123" uId="abc" /></text>" and turns it into "<text><item uId="def" /></text>" based by
        /// matching an item in the libraryValues and the lId attribute in the XML.
        /// </summary>
        /// <returns>The string containing the updated Xml.</returns>
        public static string CorrectUidByLibraryIds(string xmlBody, Package package)
        {
            if (xmlBody == null || xmlBody.IndexOf("<text>") == -1)
            {
                return xmlBody;
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlBody);

            XmlNodeList nodes = doc.GetElementsByTagName("item");

            for (int i = 0; i < nodes.Count; i++)
            {
                XmlNode node = nodes[i].SelectSingleNode("@lId");
                string lId = string.Empty;
                string uId = string.Empty;

                if (node != null)
                {
                    lId = node.Value;
                }
                
                foreach (Question q in package.Questions)
                {
                    if (q.LibraryUniqueIdentifier.ToString() == lId)
                    {
                        uId = q.UniqueIdentifier.ToString();
                        break;
                    }
                }

                //TODO - String Binding - Loop Functions
                nodes[i].Attributes.Remove((XmlAttribute)nodes[i].SelectSingleNode("@lId"));
                nodes[i].Attributes["uId"].Value = uId;
            }

            return doc.OuterXml;
        }

        private static string EncodeXml(string xml)
        {
            xml = xml.Replace("&", "&amp;");
            xml = xml.Replace("'", "&apos;");
            xml = xml.Replace("\"", "&quot;");
            xml = xml.Replace("<", "&lt;");
            xml = xml.Replace(">", "&gt;");

            // Ensure the item xml is correct
            xml = xml.Replace("&lt;text&gt;", "<text>");
            xml = xml.Replace("&lt;/text&gt;", "</text>");
            xml = xml.Replace("&lt;item uId=&quot;", "<item uId=\"");
            //xml = xml.Replace("&quot;&gt;&lt;/item&gt;", "\"></item>");
            // Had the case:
            // <item uID="99999999-xxxxxx">your spouse/partner's</item>
            xml = xml.Replace("&lt;/item&gt;", "</item>");
            xml = xml.Replace("&quot;&gt;", "\">");

            // LiveLink picker have pink bits like <item uId=\"xxx-xxx-xx-xxx" fieldname="name" 
            // which became 
            // <item uId=\"xxx-xxx-xx-xxx&quot; fieldname
            // GUID couldn't be evaluated - need to restore string, but do not touch anything 
            // outside of the <item/> tag
            if (xml.Contains("<item uId=\""))
            {
                int start = xml.IndexOf("<item uId=\"");
                int end = xml.IndexOf("</item>");
                String middelString = xml.Substring(start, end - start);
                if (middelString.Contains("&quot;"))
                {
                    middelString = middelString.Replace("&quot;", "\"");
                    xml = xml.Substring(0, start) + middelString + xml.Substring(end);
                }
            }
            return xml;
        }

        #endregion
    }
}
