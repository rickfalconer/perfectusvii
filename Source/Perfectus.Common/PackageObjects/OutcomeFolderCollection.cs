using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type OutcomeFolder
	/// </summary>
	[Serializable]
	public sealed class OutcomeFolderCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the OutcomeFolderCollection class.
		/// </summary>
		internal OutcomeFolderCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeFolderCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new OutcomeFolderCollection.
		/// </param>
		internal OutcomeFolderCollection(OutcomeFolder[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeFolderCollection class, containing elements
		/// copied from another instance of OutcomeFolderCollection
		/// </summary>
		/// <param name="items">
		/// The OutcomeFolderCollection whose elements are to be added to the new OutcomeFolderCollection.
		/// </param>
		internal OutcomeFolderCollection(OutcomeFolderCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this OutcomeFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this OutcomeFolderCollection.
		/// </param>
		internal void AddRange(OutcomeFolder[] items)
		{
			foreach (OutcomeFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another OutcomeFolderCollection to the end of this OutcomeFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The OutcomeFolderCollection whose elements are to be added to the end of this OutcomeFolderCollection.
		/// </param>
		internal void AddRange(OutcomeFolderCollection items)
		{
			foreach (OutcomeFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type OutcomeFolder to the end of this OutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeFolder to be added to the end of this OutcomeFolderCollection.
		/// </param>
		public void Add(OutcomeFolder value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic OutcomeFolder value is in this OutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeFolder value to locate in this OutcomeFolderCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this OutcomeFolderCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(OutcomeFolder value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this OutcomeFolderCollection
		/// </summary>
		/// <param name="value">
		/// The OutcomeFolder value to locate in the OutcomeFolderCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(OutcomeFolder value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the OutcomeFolderCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the OutcomeFolder is to be inserted.
		/// </param>
		/// <param name="value">
		/// The OutcomeFolder to insert.
		/// </param>
		internal void Insert(int index, OutcomeFolder value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the OutcomeFolder at the given index in this OutcomeFolderCollection.
		/// </summary>
		public OutcomeFolder this[int index]
		{
			get { return (OutcomeFolder) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific OutcomeFolder from this OutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The OutcomeFolder value to remove from this OutcomeFolderCollection.
		/// </param>
		internal void Remove(OutcomeFolder value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by OutcomeFolderCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(OutcomeFolderCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public OutcomeFolder Current
			{
				get { return (OutcomeFolder) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (OutcomeFolder) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this OutcomeFolderCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}
