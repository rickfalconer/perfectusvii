using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
    // avoid casting in derived interfaces
    public interface IPackageItemCommonProperties
    {
        Guid UniqueIdentifier { get; set; }
        String Name { get; set; }
        Package ParentPackage { get; set; }
    }

    public interface IPackageItemRequiresUniqueName
    {
        bool RequiresUniqueName { get; }
    }
    

	/// <summary>
	/// Summary description for PackageItem.
	/// </summary>
	[Serializable]
    public abstract class PackageItem : LocalisedPropertiesObject, IPackageItemCommonProperties, ISerializable, ICloneable
	{
		private Package parentPackage;
		private Guid uniqueIdentifier;
		protected string m_name;
		protected string altName1;
		private Folder parentFolder;
		[NonSerialized] public Guid Touch;
		protected string m_notes = "";		

		[field : NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		[Browsable(false)]
		public string AlternateName1
		{
			get { return altName1; }
			set
			{
				altName1 = value;
			}
		}

		protected string GetUniqueName(string proposedName,string itemId)
		{
			if (parentPackage != null)
			{
                return parentPackage.EnsureUniqueName(proposedName, itemId);
			}
			else
			{
				return proposedName;
			}
		}

		protected void FreeName(string oldName)
		{
			if (parentPackage != null)
			{
				parentPackage.FreeName(oldName);
			}
		}

		[Browsable(false)]
		public virtual bool AllowDuplicatesInPackage
		{
			get { return false; }
		}		

		[Browsable(false)]
		[XmlIgnore] //SL
		public Package ParentPackage
		{
			get { return parentPackage; }
			set { parentPackage = value; }
		}

		//TODO: Provide English description/help text for all browsable properties.
        [Browsable(true)]
        [Category("Data")]
        [DisplayRanking(0)]
        public string Name
        {
            get { return m_name; }
            set
            {
                if (this is Folder)
                {
                    m_name = ((Folder)this).suggestedName(value);
                }
                else
                {
                    bool wantUniqueName = true;
                    if (this is IPackageItemRequiresUniqueName)
                    {
                        wantUniqueName = ((IPackageItemRequiresUniqueName)this).RequiresUniqueName;
                    }
                 
                    if (value != m_name)
                        FreeName(m_name);
                    
                    // Place the name into the naming table - even if it shall not be unique
                    m_name = GetUniqueName(value, uniqueIdentifier.ToString());

                    if (!wantUniqueName)
                    {
                        if (value != m_name)
                            FreeName(m_name);
                        m_name = value;
                    }
                }
                OnPropertyChanged("Name");
            }
        }
		
		[Category("Data")]
		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.StringEditor, Studio", typeof (UITypeEditor))]
        [DisplayRanking( 50 )]
        public string Notes
		{
			get { return m_notes;  }
			set { m_notes = value; }
		}

		public void NewName(string n)
		{
			m_name = GetUniqueName(n,uniqueIdentifier.ToString());
		}

        // Only include in release versions
        #if !DEBUG
		[Browsable(false)]
        #endif
		public Guid UniqueIdentifier
		{
			get { return uniqueIdentifier; }
			set
			{
				uniqueIdentifier = value;
			}
		}		
 
		[Browsable(false)]
        [XmlIgnore] //SL
		public virtual Folder ParentFolder 
		{
			get { return parentFolder; }
			set { parentFolder = value; }
		}

		//TODO: improve rule for generating tag name?  Maybe use <Perfectus /> for all and rely on place-holder or innerText for the name?
		protected virtual string SafeTagName
		{
			get
			{
				return GetSafeTagValue(m_name.Trim());
			}
		}

		public override string ToString()
		{
			return this.Name;
		}

		public string GetSafeTagValue(string tagValue)
		{
			tagValue = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(tagValue);

			tagValue = Regex.Replace(tagValue, @"\W", "_");
			tagValue = Regex.Replace(tagValue, "^[^A-z]", "_");
			tagValue = Regex.Replace(tagValue, "[^A-z0-9]", "");
			return tagValue;
		}

		public PackageItem()
		{
			uniqueIdentifier = Guid.NewGuid();
		}

		protected void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
        }

        # region Valid check
        public virtual bool Valid()
        {
            string message;
            return Valid(out message);
        }
        public virtual bool Valid(out string message)
        {
            message = default(String);
            return true;
        }
#endregion

		#region ISerializable Members

		public PackageItem(SerializationInfo info, StreamingContext context)
		{
			this.uniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
			this.m_name = info.GetString("name");
			this.parentPackage = (Package) info.GetValue("parentPackage", typeof (Package));
			this.Touch = (Guid) info.GetValue("Touch", typeof (Guid));

			try
			{
				altName1 = info.GetString("alternateName1");
			}
			catch
			{
			}

			try
			{
				m_notes = info.GetString("notes");
			}
			catch
			{
			}

			try
			{
				parentFolder = (Folder) info.GetValue("parentFolder", typeof (Folder));
			}
			catch
			{
			}
		}

		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("uniqueIdentifier", this.uniqueIdentifier);
			info.AddValue("name", this.m_name);
			info.AddValue("parentPackage", this.parentPackage);
			info.AddValue("Touch", this.Touch);
			info.AddValue("alternateName1", altName1);
			info.AddValue("notes",m_notes);
			info.AddValue("parentFolder", this.parentFolder);
		}

		#endregion

		#region ICloneable Members

		public virtual object Clone()
		{
			using (MemoryStream ms = new MemoryStream())
			{
				BinaryFormatter bf = new BinaryFormatter();
				bf.Context = new StreamingContext(StreamingContextStates.Clone);
				bf.Serialize(ms, this);
				ms.Seek(0, SeekOrigin.Begin);
				object o = bf.Deserialize(ms);
				ms.Flush();
				((PackageItem)o).uniqueIdentifier = Guid.NewGuid();
				return o;
			}
		}

		#endregion
	}
}
