namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for IPackageItemContainer.
	/// </summary>
	public interface IPackageItemContainer
	{
		bool ContainsReferenceTo(PackageItem item);
	}
}