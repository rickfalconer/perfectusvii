using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Rule.
	/// </summary>
	[Serializable]
	public sealed class Rule : PackageItem, ISerializable
	{
		private Server parentServer;

		private TemplateDocumentCollection templates = new TemplateDocumentCollection();
		private PluginBaseCollection converters = new PluginBaseCollection();
		private PluginBaseCollection distributors = new PluginBaseCollection();
		private YesNoQueryValue deliveryRule = new YesNoQueryValue();
		private YesNoQueryValue doRuleAtFinish = new YesNoQueryValue();

        [Browsable( true )]
        [Editor( "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof( UITypeEditor ) )]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 4 )]
        public YesNoQueryValue DeliveryRule
		{
			get { return deliveryRule; }
			set
			{
				deliveryRule = value;
				OnPropertyChanged("DeliveryRule");
			}
		}

        [Browsable( true )]
        [Editor( "Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof( UITypeEditor ) )]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 2 )]
        public YesNoQueryValue DoRuleAtFinish
		{
			get { return doRuleAtFinish; }
			set
			{
				doRuleAtFinish = value;
				OnPropertyChanged("DoRuleAtFinish");
			}
		}


		[Browsable(false)]
		public TemplateDocumentCollection Templates
		{
			get { return templates; }
		}

		[Browsable(false)]
		public PluginBaseCollection Converters
		{
			get { return converters; }
		}

		[Browsable(false)]
		public PluginBaseCollection Distributors
		{
			get { return distributors; }
		}

		[Browsable(false)]
		public Server ParentServer
		{
			get { return parentServer; }
		}

		/// <summary>
		///  Constructor - Indended for use by the XmlSerialization Only!
		/// </summary>
		private Rule(){} //SL

		public Rule(Server parentServer) : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.Rule.NewItemName");
			this.parentServer = parentServer;
			this.deliveryRule.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
			this.deliveryRule.YesNoValue = true;
			this.doRuleAtFinish.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
			this.doRuleAtFinish.YesNoValue = true;
		}

		#region ISerializable Members

		public Rule(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			templates = (TemplateDocumentCollection) info.GetValue("templates", typeof (TemplateDocumentCollection));
			distributors = (PluginBaseCollection) info.GetValue("distributors", typeof (PluginBaseCollection));
			converters = (PluginBaseCollection) info.GetValue("converters", typeof (PluginBaseCollection));
			deliveryRule = (YesNoQueryValue) info.GetValue("deliveryRule", typeof (YesNoQueryValue));
			parentServer = (Server) info.GetValue("parentServer", typeof (Server));
			try 
			{
				doRuleAtFinish = (YesNoQueryValue) info.GetValue("doRuleAtFinish", typeof (YesNoQueryValue));
			}
			catch
			{
				doRuleAtFinish = new YesNoQueryValue();
				doRuleAtFinish.YesNoValue = true;
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("templates", templates);
			info.AddValue("distributors", distributors);
			info.AddValue("converters", this.converters);
			info.AddValue("deliveryRule", this.deliveryRule);
			info.AddValue("parentServer", this.parentServer);
			info.AddValue("doRuleAtFinish",this.doRuleAtFinish);
		}

		#endregion
	}
}