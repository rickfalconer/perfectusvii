using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for YesNoAction.
	/// </summary>
	[Serializable]
	public sealed class YesNoAction : ActionBase, ISerializable
	{
		private bool action;

		public bool Action
		{
			get { return action; }
			set { action = value; }
		}

		public YesNoAction() : base()
		{}

		public YesNoAction(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			action = info.GetBoolean("action");
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("action", action);
		}

		public override object Clone()
		{
			return this.MemberwiseClone();
		}


	}
}