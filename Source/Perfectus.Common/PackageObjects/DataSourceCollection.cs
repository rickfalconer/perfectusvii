using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type DataSource
    /// This class represents the WebReference tree node in IPManager
	/// </summary>
	[Serializable]
	public sealed class DataSourceCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the DataSourceCollection class.
		/// </summary>
		internal DataSourceCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the DataSourceCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new DataSourceCollection.
		/// </param>
		internal DataSourceCollection(DataSource[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the DataSourceCollection class, containing elements
		/// copied from another instance of DataSourceCollection
		/// </summary>
		/// <param name="items">
		/// The DataSourceCollection whose elements are to be added to the new DataSourceCollection.
		/// </param>
		internal DataSourceCollection(DataSourceCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this DataSourceCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this DataSourceCollection.
		/// </param>
		internal void AddRange(DataSource[] items)
		{
			foreach (DataSource item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another DataSourceCollection to the end of this DataSourceCollection.
		/// </summary>
		/// <param name="items">
		/// The DataSourceCollection whose elements are to be added to the end of this DataSourceCollection.
		/// </param>
		internal void AddRange(DataSourceCollection items)
		{
			foreach (DataSource item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type DataSource to the end of this DataSourceCollection.
		/// </summary>
		/// <param name="value">
		/// The DataSource to be added to the end of this DataSourceCollection.
		/// </param>
		public void Add(DataSource value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic DataSource value is in this DataSourceCollection.
		/// </summary>
		/// <param name="value">
		/// The DataSource value to locate in this DataSourceCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this DataSourceCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(DataSource value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this DataSourceCollection
		/// </summary>
		/// <param name="value">
		/// The DataSource value to locate in the DataSourceCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(DataSource value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the DataSourceCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the DataSource is to be inserted.
		/// </param>
		/// <param name="value">
		/// The DataSource to insert.
		/// </param>
		internal void Insert(int index, DataSource value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the DataSource at the given index in this DataSourceCollection.
		/// </summary>
		public DataSource this[int index]
		{
			get { return (DataSource) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific DataSource from this DataSourceCollection.
		/// </summary>
		/// <param name="value">
		/// The DataSource value to remove from this DataSourceCollection.
		/// </param>
		internal void Remove(DataSource value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by DataSourceCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(DataSourceCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public DataSource Current
			{
				get { return (DataSource) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (DataSource) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this DataSourceCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}