using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Security.Principal;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for Page.
    /// </summary>
    [Serializable]
    public sealed class InternalPage : InterviewPage, ISerializable, IDesignable, IXmlSerializable
    {
        // Enum for previewstyle
        public enum ListPreviewStyle
        {
            HTML,
            PDF
        } ;

        // Enum for refreshServices
        public enum ListRefreshServices
        {
            Yes,
            No,
            Ask
        } ;

        /// <summary>
        /// Localise the previewstyle words
        /// </summary>
        /// <param name="styleValue"></param>
        /// <returns></returns>
        public static string LocalisePreviewStyle(ListPreviewStyle styleValue)
        {
            string keyName =
                string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.ListPreviewStyle.{0}", styleValue);
            return ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
        }

        /// <summary>
        /// DeLocalise the previewstyle words
        /// </summary>
        /// <param name="styleValue"></param>
        /// <returns></returns>
        public static ListPreviewStyle DelocalisePreviewStyle(string styleValue)
        {
            foreach (ListPreviewStyle lps in (ListPreviewStyle[]) (Enum.GetValues(typeof (ListPreviewStyle))))
            {
                if (LocalisePreviewStyle(lps) == styleValue)
                {
                    return lps;
                }
            }
            return ListPreviewStyle.HTML;
        }


        private YesNoQueryValue allowSaveAnswerSet;
        private YesNoQueryValue allowSuspend;
        private YesNoQueryValue showPreviewButton;
        private YesNoQueryValue showHistory;
        private RuleCollection pageRules;
        private Rule2Collection pageRules2;

        // Defines the Shared Library type for this class
        private LibraryItemType _LibraryItemType = LibraryItemType.InterviewPage;

        // Declear previewstyle
        private ListPreviewStyle previewStyle;

        // Declare refreshServices
        private ListRefreshServices refreshServices;

        // IXmlSerializable variables
        [NonSerialized] private const string msINTERNAL_PAGE = "InternalPage";
        [NonSerialized] private const string msINTERVIEW_KEY = "ParentInterview";
        [NonSerialized] private const string msPROGRESS_KEY = "Progress";
        [NonSerialized] private const string msSEEN_KEY = "Seen";
        [NonSerialized] private const string msNEXT_KEY = "Next";
        [NonSerialized] private const string msVALUE_TYPE_KEY = "ValueType";
        [NonSerialized] private const string msTOUCH_KEY = "Touch";
        [NonSerialized] private const string msNAME_KEY = "Name";
        [NonSerialized] private const string msNOTES_KEY = "Notes";
        [NonSerialized] private const string msLIBRARY_UNIQUE_IDENTIFIER_KEY = "LibraryUniqueIdentifier";
        [NonSerialized] private const string msVERSION_KEY = "Version";
        [NonSerialized] private const string msLIBRARY_TYPE_KEY = "LibraryType";
        [NonSerialized] private const string msLINKED_KEY = "Linked";
        [NonSerialized] private const string msCREATED_BY_KEY = "CreatedBy";
        [NonSerialized] private const string msCREATED_DATETIME_KEY = "CreatedDateTime";
        [NonSerialized] private const string msCHECKED_OUT_DATETIME_KEY = "CheckedOutDateTime";
        [NonSerialized] private const string msCHECKED_OUT_STATUS_KEY = "CheckedOutStatus";
        [NonSerialized] private const string msMODIFIED_BY_KEY = "ModifiedBy";
        [NonSerialized] private const string msMODIFIED_DATETIME_KEY = "ModifiedDateTime";
        [NonSerialized] private const string msCHECK_IN_COMMENT_KEY = "CheckInComment";
        [NonSerialized] private const string msRELATIVE_PATH_KEY = "RelativePath";
        [NonSerialized] private const string msLOCATION_KEY = "LocationPath";
        [NonSerialized] private const string msFILENAME_KEY = "Filename";
        [NonSerialized] private const string msPREFECTUS_TYPE_KEY = "PerfectusType";
        [NonSerialized] private const string msDATA_TYPE_KEY = "DataType";
        [NonSerialized] private const string msSHOW_PREVIEW_BUTTON = "ShowPreviewButton";
        [NonSerialized] private const string msALLOW_SAVE_ANSWER_SET = "AllowSaveAnswerSet";
        [NonSerialized] private const string msALLOW_SUSPEND = "AllowSuspend";
        [NonSerialized] private const string msSHOW_HISTORY = "ShowHistory";
        [NonSerialized] private const string msPREVIEW_STYLE = "PreviewStyle";
        [NonSerialized] private static string msVISIBLE = "Visible";
        [NonSerialized] private static string msSHOW_PAGE_IN_HISTORY = "ShowPageInHistory";
        [NonSerialized] private static string msREFRESH_SERVICES = "RefreshServices";

        /// <summary>
        ///		Public accessor for the Shared Library type for the class
        /// </summary>
        [Category("SharedLibrary")]
        [Browsable(false)]
        public override LibraryItemType LibraryType
        {
            get { return _LibraryItemType; }
            set { _LibraryItemType = value; }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 14 )]
        public YesNoQueryValue AllowSaveAnswerSet
        {
            get { return allowSaveAnswerSet; }
            set
            {
                allowSaveAnswerSet = value;
                OnPropertyChanged("AllowSaveAnswerSet");
            }
        } 
        
        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 16 )]
        public YesNoQueryValue AllowSuspend
        {
            get { return allowSuspend; }
            set
            {
                allowSuspend = value;
                OnPropertyChanged("AllowSuspend");
            }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 12 )]
        public YesNoQueryValue ShowPreviewButton
        {
            get { return showPreviewButton; }
            set
            {
                showPreviewButton = value;
                OnPropertyChanged("ShowPreviewButton");
            }
        }

        /// <summary>
        ///     previewstyle property
        /// </summary>
        [Category("Display")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.ListPreviewStyleConverter, Studio")]
        [DisplayRanking( 18 )]
        public ListPreviewStyle PreviewStyle
        {
            get { return previewStyle; }
            set
            {
                previewStyle = value;
                OnPropertyChanged("PreviewStyle");
            }
        }

        [Category("Display")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 10 )]
        public YesNoQueryValue ShowHistory
        {
            get { return showHistory; }
            set
            {
                showHistory = value;
                OnPropertyChanged("ShowHistory");
            }
        }

        [Browsable(false)]
        public RuleCollection PageRules
        {
            get { return pageRules; }
            set
            {
                pageRules = value;
                OnPropertyChanged("PageRules");
            }
        }

        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.RuleCollectionEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.RuleCollectionConverter, Studio")]
        [XmlIgnore]
        [DisplayRanking(20)]
        public Rule2Collection PageRules2
        {
            get { return pageRules2; }
            set
            {
                pageRules2 = value;
                OnPropertyChanged("PageRules");
            }
        }

        /// <summary>
        ///     refreshServices property
        /// </summary>
        [Category("Display")]
        [Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.ListResfreshServicesConverter, Studio")]
        [DisplayRanking(21)]
        public ListRefreshServices RefreshServices
        {
            get { return refreshServices; }
            set
            {
                refreshServices = value;
                OnPropertyChanged("RefreshServices");
            }
        }

        public InternalPage()
        {
            allowSaveAnswerSet = new YesNoQueryValue();
            allowSuspend = new YesNoQueryValue();
            allowSuspend.YesNoValue = false;
            allowSaveAnswerSet.YesNoValue = false;
            showPreviewButton = new YesNoQueryValue();
            showPreviewButton.YesNoValue = false;
            showHistory = new YesNoQueryValue();
            showHistory.YesNoValue = true;
            pageRules = new RuleCollection();
            pageRules2 = new Rule2Collection();

            // initial previewstyle value
            previewStyle = ListPreviewStyle.HTML;

            // initial refreshServices value
            refreshServices = ListRefreshServices.Ask;
        }

        public InternalPage(Interview parentInterview) : this()
        {
            ParentInterview = parentInterview;
        }

        public override List<LibraryItem> GetDependentLibraryItems()
        {
            List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();

            // Next Page
            // Commented out for initial 5.2 release - simply uncomment to have next page dependencies picked up again
            //SetNextPageDependencies(dependentLibrayrItems, this.NextPage);

            // Items
            SetPageItemDependencies(dependentLibraryItems, Items);

            // Update YesNoQuery properties.
            Visible.SetDependentLibraryItems(dependentLibraryItems);
            AllowSuspend.SetDependentLibraryItems(dependentLibraryItems);
            ShowPreviewButton.SetDependentLibraryItems(dependentLibraryItems);
            AllowSaveAnswerSet.SetDependentLibraryItems(dependentLibraryItems);
            ShowPageInHistory.SetDependentLibraryItems(dependentLibraryItems);
            ShowHistory.SetDependentLibraryItems(dependentLibraryItems);

            return dependentLibraryItems;
        }

        public override List<LibraryItem> SetDependentLibraryItems()
        {
            return null;
        }

        // Commented out for initial 5.2 release - simply uncomment to have next page dependencies picked up again
        /*        
        private void SetNextPageDependencies(List<LibraryItem> dependentLibraryItems, PageQueryValue pageQueryValue)
        {
            if (pageQueryValue.PageValue != null)
            {
                SetDependentObjects(dependentLibraryItems, pageQueryValue.PageValue);
            }
            else
            {
                // Set the next page dependencies if set
                if (pageQueryValue.QueryValue != null)
                {
                    // Action if true
                    SetNextPageQueryDependencies(dependentLibraryItems,
                                                 (NavigationAction) pageQueryValue.QueryValue.ActionIfTrue);

                    // Action if false
                    SetNextPageQueryDependencies(dependentLibraryItems,
                                                 (NavigationAction) pageQueryValue.QueryValue.ActionIfFalse);

                    // Query Expression
                    SetDependentObjects(dependentLibraryItems, pageQueryValue.QueryValue.QueryExpression);
                }
            }
        }
        */
        // Commented out for initial 5.2 release - simply uncomment to have next page dependencies picked up again
        /*
        private void SetNextPageQueryDependencies(List<LibraryItem> dependentLibraryItems,
                                                  NavigationAction navigationAction)
        {
            if (navigationAction != null)
            {
                SetDependentObjects(dependentLibraryItems, navigationAction.Page);

                if (navigationAction.SubQuery != null)
                {
                    // Sub query action if true
                    SetNextPageQueryDependencies(dependentLibraryItems,
                                                 (NavigationAction) navigationAction.SubQuery.ActionIfTrue);

                    // Sub query action if false
                    SetNextPageQueryDependencies(dependentLibraryItems,
                                                 (NavigationAction) navigationAction.SubQuery.ActionIfFalse);

                    // Sub query query expression
                    SetDependentObjects(dependentLibraryItems,
                                        navigationAction.SubQuery.QueryExpression.RootExpressionNode);
                }
            }
        }
        */

        private void SetPageItemDependencies(List<LibraryItem> dependentLibraryItems, PageItemCollection items)
        {
            if (items != null)
            {
                foreach (IPageItem item in items)
                {
                    if (item is RepeaterZone)
                    {
                        if (item.Visible.ValueType == YesNoQueryValue.YesNoQueryValueType.Query)
                            throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                GetString("Perfectus.Common.SharedLibrary.InternalPage.QueryNotSupported"));
                        SetPageItemDependencies(dependentLibraryItems, ((RepeaterZone)item).Items);
                    }
                    else if (item is HorizontalLayoutZone)
                        SetPageItemDependencies(dependentLibraryItems, ((HorizontalLayoutZone)item).Items);
                    else if (item is HtmlBlock)
                    {
                        if (item.Visible.ValueType == YesNoQueryValue.YesNoQueryValueType.Query)
                            throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                GetString("Perfectus.Common.SharedLibrary.InternalPage.QueryNotSupported"));
                        ScanForDepandentItems(dependentLibraryItems, (HtmlBlock)item);
                    }
                    else if (item is Question)
                        SetDependentObjects(dependentLibraryItems, (Question)item);
                }
            }
        }

        private void ScanForDepandentItems(List<LibraryItem> dependentLibraryItems, HtmlBlock item)
        {
            StringBindingItemDictionary stringBindingItems;
            stringBindingItems = new StringBindingItemDictionary();

            PackageItem[] pp = StringBinding.GetPackageItemsFromXml(item.Html, ParentPackage.StringBindingItemByGuid);
        
            if ( null != pp )
            foreach ( PackageItem packageItem in pp )
                if (packageItem is Question)
                {
                    System.Windows.Forms.MessageBox.Show(String.Format(
                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                            GetString("Perfectus.Common.SharedLibrary.InternalPage.UnsupportedItem"), packageItem.Name),
                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                            GetString("Perfectus.Common.Globals.SL"),System.Windows.Forms.MessageBoxButtons.OK,System.Windows.Forms.MessageBoxIcon.Warning);

                    //SetDependentObjects(dependentLibraryItems, (Question)packageItem);
                }
        }


        private static void SetDependentObjects(List<LibraryItem> dependentLibraryItems, PackageItem packageItem)
        {
            if (packageItem is ExpressionNode)
            {
                SetDependentObjects(dependentLibraryItems, ((ExpressionNode) packageItem).Operand1);
                SetDependentObjects(dependentLibraryItems, ((ExpressionNode) packageItem).Operand2);
            }
            else
            {
                if (packageItem != null && !(packageItem is PackageConstant))
                {
                    // Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
                    if (!dependentLibraryItems.Contains((LibraryItem) packageItem))
                    {
                        if ((((LibraryItem)packageItem).RelativePath == null || ((LibraryItem)packageItem).RelativePath == string.Empty) &&
                            !dependentLibraryItems.Contains((LibraryItem) packageItem))
                            dependentLibraryItems.Add((LibraryItem) packageItem);
                    }
                }
            }
        }

        public QuestionCollection GetQuestionsOnPage()
        {
            QuestionCollection col = new QuestionCollection();
            PopulateQuestionCollection(col, Items);
            return col;
        }

        private static void PopulateQuestionCollection(QuestionCollection destCol, PageItemCollection sourceCol)
        {
            foreach (IPageItem pi in sourceCol)
            {
                if (pi is Question)
                {
                    destCol.Add((Question) pi);
                }
                else if (pi is HorizontalLayoutZone)
                {
                    PopulateQuestionCollection(destCol, ((HorizontalLayoutZone) pi).Items);
                }
                else if (pi is RepeaterZone)
                {
                    PopulateQuestionCollection(destCol, ((RepeaterZone) pi).Items);
                }
            }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("allowSaveAnswerSet", allowSaveAnswerSet);
            info.AddValue("allowSuspend", allowSuspend);
            info.AddValue("showPreviewButton", showPreviewButton);
            info.AddValue("pageRules", pageRules);
            info.AddValue("pageRules2", pageRules2);
            info.AddValue("showHistory", showHistory);

            //Add previewstyle into serializable info
            info.AddValue("previewStyle", previewStyle);

            //Add refreshServices into serializable info
            info.AddValue("refreshServices", refreshServices);
        }

        public InternalPage(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            try
            {
                allowSaveAnswerSet = (YesNoQueryValue) (info.GetValue("allowSaveAnswerSet", typeof (YesNoQueryValue)));
            }
            catch
            {
                allowSaveAnswerSet = new YesNoQueryValue();
                allowSaveAnswerSet.YesNoValue = false;
            }
            try
            {
                allowSuspend = (YesNoQueryValue) (info.GetValue("allowSuspend", typeof (YesNoQueryValue)));
            }
            catch
            {
                allowSuspend = new YesNoQueryValue();
                allowSuspend.YesNoValue = true;
            }

            try
            {
                showPreviewButton = (YesNoQueryValue) (info.GetValue("showPreviewButton", typeof (YesNoQueryValue)));
            }
            catch
            {
                showPreviewButton = new YesNoQueryValue();
                showPreviewButton.YesNoValue = true;
            }

            try
            {
                showHistory = (YesNoQueryValue) (info.GetValue("showHistory", typeof (YesNoQueryValue)));
            }
            catch
            {
                showHistory = new YesNoQueryValue();
                showHistory.YesNoValue = true;
            }

            try
            {
                pageRules = (RuleCollection) (info.GetValue("pageRules", typeof (RuleCollection)));
            }
            catch
            {
                pageRules = new RuleCollection();
            }

            try
            {
                pageRules2 = (Rule2Collection)(info.GetValue("pageRules2", typeof(Rule2Collection)));
            }
            catch
            {
                pageRules2 = new Rule2Collection();
            }


            // Save preview style input value
            try
            {
                previewStyle = (ListPreviewStyle) (info.GetValue("previewStyle", typeof (ListPreviewStyle)));
            }
            catch
            {
                previewStyle = ListPreviewStyle.HTML;
            }

            // Save refresh services input value
            try
            {
                refreshServices = (ListRefreshServices)(info.GetValue("refreshServices", typeof(ListRefreshServices)));
            }
            catch
            {
                refreshServices = ListRefreshServices.Ask;
            }
        }

        public bool ContainsReferenceTo(PackageItem item, out string errorMessage)
        {
            errorMessage = string.Empty;

            if (item is Rule2 && pageRules2.Contains(item as Rule2))
            {
                errorMessage =
                    ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                        "Perfectus.Common.PackageObjects.Package.ReferenceRuleDeleteErrorMsg");
                return true;
            }
            else if (item is Server)
            {
                if (CheckServerExists(item as Server))
                {
                    errorMessage =
                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                            "Perfectus.Common.PackageObjects.Package.ReferenceProcessDeleteErrorMsg");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (ColContainsRef(Items, item))
                {
                    errorMessage =
                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                            "Perfectus.Common.PackageObjects.Package.ReferencepropertyDeleteErrorMsg");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool ContainsReferenceTo(PackageItem item)
        {
            if (item is Rule2 && pageRules2.Contains(item as Rule2))
            {
                return true;
            }
            else if (item is Server)
            {
                return CheckServerExists(item as Server);
            }
            else
            {
                return ColContainsRef(Items, item);
            }
        }

        private bool CheckServerExists(Server item)
        {
            if (pageRules2 != null)
            {
                foreach (Rule2 r in pageRules2)
                {
                    if (item == r.ParentServer)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool ColContainsRef(PageItemCollection col, PackageItem item)
        {
            foreach (IPageItem ipi in col)
            {
                if (ipi == item)
                {
                    return true;
                }
                if (ipi is HorizontalLayoutZone)
                {
                    bool hzoneContainsRef = ColContainsRef(((HorizontalLayoutZone) ipi).Items, item);
                    if (hzoneContainsRef)
                    {
                        return true;
                    }
                }
                else if (ipi is RepeaterZone)
                {
                    bool rzoneContainsRef = ColContainsRef(((RepeaterZone) ipi).Items, item);
                    if (rzoneContainsRef)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool ColContainsRefToImportantQuestion(PageItemCollection col, Question q)
        {
            foreach (IPageItem ipi in col)
            {
                if (ipi.Visible.ContainsReferenceTo(q))
                {
                    return true;
                }
                if (ipi is HorizontalLayoutZone)
                {
                    bool hzoneContainsRef = ColContainsRefToImportantQuestion(((HorizontalLayoutZone) ipi).Items, q);
                    if (hzoneContainsRef)
                    {
                        return true;
                    }
                }
                else if (ipi is RepeaterZone)
                {
                    bool rzoneContainsRef = ColContainsRefToImportantQuestion(((RepeaterZone) ipi).Items, q);
                    if (rzoneContainsRef)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void WriteXml(XmlWriter w)
        {
            WriteLibraryItemInformation(w);

            // Progress
            w.WriteStartElement(msPROGRESS_KEY);
            w.WriteString(Progress.ToString());
            w.WriteEndElement();

            // Seen
            w.WriteStartElement(msSEEN_KEY);
            w.WriteString(Seen.ToString());
            w.WriteEndElement();

            // PreviewStyle
            w.WriteStartElement(msPREVIEW_STYLE);
            w.WriteString(PreviewStyle.ToString());
            w.WriteEndElement();

            // RefreshServices
            w.WriteStartElement(msREFRESH_SERVICES);
            w.WriteString(RefreshServices.ToString());
            w.WriteEndElement();

            // Parent Interview
            w.WriteStartElement(msINTERVIEW_KEY);
            w.WriteElementString(Serializer.NameKey, ParentInterview.Name); // e.g. <Name>question_name</Name>
            w.WriteElementString(Serializer.UniqueIdentifierKey, ParentInterview.UniqueIdentifier.ToString());
                // e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
            w.WriteEndElement();

            // Items
            Items.WriteXml(w);

            // Next Page
            NextPage.WriteXml(w);

            // Visible
            Serializer.WriteYesNoQuerySection(w, Visible, msVISIBLE);

            // Show Page In History
            Serializer.WriteYesNoQuerySection(w, ShowPageInHistory, msSHOW_PAGE_IN_HISTORY);

            // Show PreviewButton
            Serializer.WriteYesNoQuerySection(w, ShowPreviewButton, msSHOW_PREVIEW_BUTTON);

            // Allow Save Answer Set
            Serializer.WriteYesNoQuerySection(w, AllowSaveAnswerSet, msALLOW_SAVE_ANSWER_SET);

            // Allow Suspend Interview
            Serializer.WriteYesNoQuerySection(w, AllowSuspend, msALLOW_SUSPEND);

            // Show History
            Serializer.WriteYesNoQuerySection(w, ShowHistory, msSHOW_HISTORY);
        }

        private void WriteLibraryItemInformation(XmlWriter w)
        {
            w.WriteStartElement(msTOUCH_KEY);
            w.WriteString(Touch.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msNAME_KEY);
            w.WriteString(Name);
            w.WriteEndElement();

            w.WriteStartElement(msNOTES_KEY);
            if (Notes != null)
                w.WriteString(Notes);
            w.WriteEndElement();

            w.WriteStartElement(Serializer.UniqueIdentifierKey);
            w.WriteString(UniqueIdentifier.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msLIBRARY_UNIQUE_IDENTIFIER_KEY);
            w.WriteString(LibraryUniqueIdentifier.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msVERSION_KEY);
            w.WriteString(Version.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msLIBRARY_TYPE_KEY);
            w.WriteString(Convert.ToInt32(LibraryType).ToString());
            w.WriteEndElement();

            w.WriteStartElement(msLINKED_KEY);
            w.WriteString(Linked.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msCREATED_BY_KEY);
            if (CreatedBy == null)
                CreatedBy = WindowsIdentity.GetCurrent().Name;
            w.WriteString(CreatedBy);
            w.WriteEndElement();

            w.WriteStartElement(msCREATED_DATETIME_KEY);
            if (CreatedDateTime == DateTime.MinValue)
                CreatedDateTime = DateTime.Now;
            w.WriteString(CreatedDateTime.ToString("s")); // Sortable date/time pattern
            w.WriteEndElement();

            w.WriteStartElement(msCHECKED_OUT_DATETIME_KEY);
            if (CheckedOutDateTime != DateTime.MinValue)
                w.WriteString(CheckedOutDateTime.ToString("s")); // Sortable date/time pattern
            w.WriteEndElement();

            w.WriteStartElement(msCHECKED_OUT_STATUS_KEY);
            w.WriteString(Convert.ToInt32(CheckedOutStatus).ToString());
            w.WriteEndElement();

            w.WriteStartElement(msMODIFIED_BY_KEY);
            if (ModifiedBy != null)
                w.WriteString(ModifiedBy.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msMODIFIED_DATETIME_KEY);
            if (ModifiedDateTime != DateTime.MinValue)
                w.WriteString(ModifiedDateTime.ToString("s")); // Sortable date/time pattern
            w.WriteEndElement();

            w.WriteStartElement(msCHECK_IN_COMMENT_KEY);
            if (CheckInComment != null)
                w.WriteString(CheckInComment.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msRELATIVE_PATH_KEY);
            if (RelativePath != null)
                w.WriteString(RelativePath.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msLOCATION_KEY);
            if (LocationPath != null)
                w.WriteString(LocationPath.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msFILENAME_KEY);
            if (Filename != null)
                w.WriteString(Filename.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msPREFECTUS_TYPE_KEY);
            w.WriteString(PerfectusType.ToString());
            w.WriteEndElement();

            w.WriteStartElement(msDATA_TYPE_KEY);
            if (DataTypeString != null)
                w.WriteString(DataTypeString.ToString());
            w.WriteEndElement();
        }

        public void ReadXml(XmlReader r)
        {
            //string[] values;

            r.ReadStartElement(msINTERNAL_PAGE);

            ReadLibraryItemInformation(r);

            // Progress
            r.ReadStartElement(msPROGRESS_KEY);
            Progress = Convert.ToInt32(r.ReadString());
            r.ReadEndElement();

            // Seen
            r.ReadStartElement(msSEEN_KEY);
            Seen = Convert.ToBoolean(r.ReadString());
            r.ReadEndElement();

            // PreviewStyle
            r.ReadStartElement(msPREVIEW_STYLE);
            string previewStyleValue = r.ReadString();
            if (previewStyleValue == ListPreviewStyle.PDF.ToString())
            {
                PreviewStyle = ListPreviewStyle.PDF;
            }
            else
            {
                PreviewStyle = ListPreviewStyle.HTML;
            }
            r.ReadEndElement();

            // RefreshServices
            r.ReadStartElement(msREFRESH_SERVICES);
            string refreshServicesValue = r.ReadString();
            if (refreshServicesValue == ListRefreshServices.Ask.ToString())
            {
                RefreshServices = ListRefreshServices.Ask;
            }
            else if (refreshServicesValue == ListRefreshServices.Yes.ToString())
            {
                RefreshServices = ListRefreshServices.Yes;
            }
            else
            {
                RefreshServices = ListRefreshServices.No;
            }

            // Parent Interview
            r.ReadStartElement(msINTERVIEW_KEY);
            r.ReadStartElement(Serializer.NameKey);
            ParentInterview = new Interview();
            ParentInterview.Name = r.ReadString();
            r.ReadEndElement();
            r.ReadStartElement(Serializer.UniqueIdentifierKey);
            ParentInterview.UniqueIdentifier = new Guid(r.ReadString());
            r.ReadEndElement();
            r.ReadEndElement();

            // Items
            Items.ReadXml(r);

            // Next Page
            NextPage.ReadXml(r);

            // Visible
            Visible = Serializer.ReadVisibleSection(r);

            // Show Page In History
            ShowPageInHistory = Serializer.ReadShowPageInHistorySection(r);

            // Show PreviewButton
            ShowPreviewButton = Serializer.ReadYesNoQuerySection(r, msSHOW_PREVIEW_BUTTON);

            // Allow Save Answer Set
            AllowSaveAnswerSet = Serializer.ReadYesNoQuerySection(r, msALLOW_SAVE_ANSWER_SET);

            // Allow Save Answer Set
            AllowSuspend = Serializer.ReadYesNoQuerySection(r, msALLOW_SUSPEND);

            // Show History
            ShowHistory = Serializer.ReadYesNoQuerySection(r, msSHOW_HISTORY);

            r.ReadEndElement();
        }

        private void ReadLibraryItemInformation(XmlReader r)
        {
            r.ReadStartElement(msTOUCH_KEY);
            Touch = new Guid(r.ReadString());
            r.ReadEndElement();

            r.ReadStartElement(msNAME_KEY);
            Name = r.ReadString();
            r.ReadEndElement();

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msNOTES_KEY);
                Notes = r.ReadString();
                r.ReadEndElement();
            }

            r.ReadStartElement(Serializer.UniqueIdentifierKey);
            UniqueIdentifier = new Guid(r.ReadString());
            r.ReadEndElement();

            r.ReadStartElement(msLIBRARY_UNIQUE_IDENTIFIER_KEY);
            LibraryUniqueIdentifier = new Guid(r.ReadString());
            r.ReadEndElement();

            r.ReadStartElement(msVERSION_KEY);
            Version = Convert.ToInt32(r.ReadString());
            r.ReadEndElement();

            r.ReadStartElement(msLIBRARY_TYPE_KEY);
            LibraryType = (LibraryItemType) Convert.ToInt32(r.ReadString());
            r.ReadEndElement();

            r.ReadStartElement(msLINKED_KEY);
            Linked = Convert.ToBoolean(r.ReadString());
            r.ReadEndElement();

            if (r.IsEmptyElement)
            {
                r.ReadString();
            }
            else
            {
                r.ReadStartElement(msCREATED_BY_KEY);
                CreatedBy = r.ReadString();
                r.ReadEndElement();
            }

            r.ReadStartElement(msCREATED_DATETIME_KEY);
            CreatedDateTime = Convert.ToDateTime(r.ReadString());
            r.ReadEndElement();

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msCHECKED_OUT_DATETIME_KEY);
                CheckedOutDateTime = Convert.ToDateTime(r.ReadString());
                r.ReadEndElement();
            }

            r.ReadStartElement(msCHECKED_OUT_STATUS_KEY);
            CheckedOutStatus = (CheckOutStatus) Convert.ToInt32(r.ReadString());
            r.ReadEndElement();

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msMODIFIED_BY_KEY);
                ModifiedBy = r.ReadString();
                r.ReadEndElement();
            }

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msMODIFIED_DATETIME_KEY);
                ModifiedDateTime = Convert.ToDateTime(r.ReadString());
                r.ReadEndElement();
            }

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msCHECK_IN_COMMENT_KEY);
                CheckInComment = r.ReadString();
                r.ReadEndElement();
            }

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msRELATIVE_PATH_KEY);
                RelativePath = r.ReadString();
                r.ReadEndElement();
            }

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msLOCATION_KEY);
                LocationPath = r.ReadString();
                r.ReadEndElement();
            }

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msFILENAME_KEY);
                Filename = r.ReadString();
                r.ReadEndElement();
            }

            r.ReadStartElement(msPREFECTUS_TYPE_KEY);
            PerfectusType = r.ReadString();
            r.ReadEndElement();

            if (r.IsEmptyElement)
            {
                r.Read();
            }
            else
            {
                r.ReadStartElement(msDATA_TYPE_KEY);
                DataTypeString = r.ReadString();
                r.ReadEndElement();
            }
        }

        public void Update(InternalPage internalPage)
        {
            if (Name != internalPage.Name)
            {
                Name = internalPage.Name;
            }

            // Shared Library Item Properties
            LibraryUniqueIdentifier = internalPage.LibraryUniqueIdentifier;
            Version = internalPage.Version;
            LibraryType = internalPage.LibraryType;
            Linked = internalPage.Linked;
            CreatedBy = internalPage.CreatedBy;
            CreatedDateTime = internalPage.CreatedDateTime;
            CheckedOutBy = internalPage.CheckedOutBy;
            CheckedOutByLogInName = internalPage.CheckedOutByLogInName;
            CheckedOutDateTime = internalPage.CheckedOutDateTime;
            CheckedOutStatus = internalPage.CheckedOutStatus;
            ModifiedBy = internalPage.ModifiedBy;
            ModifiedDateTime = internalPage.ModifiedDateTime;
            CheckInComment = internalPage.CheckInComment;
            LocationPath = internalPage.LocationPath;
            RelativePath = internalPage.RelativePath;
            Filename = internalPage.Filename;
            Touch = internalPage.Touch;
            Notes = internalPage.Notes;
            PreviewStyle = internalPage.PreviewStyle;
            RefreshServices = internalPage.RefreshServices;
            DependentLibraryItems = internalPage.DependentLibraryItems;
            
            // Update YesNoQuery properties
            internalPage.ShowPageInHistory.ReconnectLibraryItems(ParentPackage);
            ShowPageInHistory = internalPage.ShowPageInHistory;
            internalPage.Visible.ReconnectLibraryItems(ParentPackage);
            Visible = internalPage.Visible;
            internalPage.ShowPreviewButton.ReconnectLibraryItems(ParentPackage);
            ShowPreviewButton = internalPage.ShowPreviewButton;
            internalPage.ShowHistory.ReconnectLibraryItems(ParentPackage);
            ShowHistory = internalPage.ShowHistory;
            internalPage.AllowSaveAnswerSet.ReconnectLibraryItems(ParentPackage);
            AllowSaveAnswerSet = internalPage.AllowSaveAnswerSet;
            internalPage.AllowSuspend.ReconnectLibraryItems(ParentPackage);
            AllowSuspend = internalPage.AllowSuspend;

            // Reconnect the package objects
            foreach (Interview interview in ParentPackage.Interviews)
            {
                if (interview.UniqueIdentifier == ParentInterview.UniqueIdentifier)
                    ParentInterview = interview;
            }

            // Hook up the next page
            NextPage = internalPage.NextPage;

            // Hook up the interview page (as the next page reference)
            if (NextPage.PageValue != null)
            {
                NextPage.PageValue = (InternalPage) ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(internalPage.NextPage.PageValue);
            }

            Items = new PageItemCollection();
            Items = ReconnectDependentObjects(internalPage.Items);
        }

        private PageItemCollection ReconnectDependentObjects(PageItemCollection items)
        {
            for (Int32 i = 0; i < items.Count; i++)
            {
                if (items[i] is RepeaterZone)
                {
                    ReconnectDependentObjects(((RepeaterZone) items[i]).Items);
                }
                else if (items[i] is HorizontalLayoutZone)
                {
                    ReconnectDependentObjects(((HorizontalLayoutZone) items[i]).Items);
                }
                else if (items[i] is Question)
                {
                    items[i] = (IPageItem) GetDependentPackageItem((LibraryItem) items[i]);
                }
            }

            return items;
        }

        protected override PackageItem GetDependentPackageItem(LibraryItem libraryItem)
        {
            PackageItem packageItem = ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);

            if (packageItem == null)
            {
                Exception ex = new Exception(libraryItem.Name + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                                                    "Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage"));
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw new NullReferenceException(libraryItem.Name);
            }

            return packageItem;
        }
    }
}
