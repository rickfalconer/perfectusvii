using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for AttachmentFolder.
	/// </summary>	 
	[Serializable]
	public class AttachmentFolder : Folder
	{

		public AttachmentFolder() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.AttachmentFolder.NewItemName");			
		}

		public AttachmentFolder(string name) : base()
		{
			this.Name = name;			
		}

		/// <summary>
		/// The folders parent folder, read only.
		/// </summary>
		public new Folder ParentFolder
		{
			get {return m_ParentFolder;}
		}

		/// <summary>
		/// Creates a new folder and adds it to this folders ChildFolders collection.
		/// </summary>		
		public override void CreateChild()
		{			
			AttachmentFolder newChildFolder = new AttachmentFolder();
			newChildFolder.MakeChildOf(this);
			this.m_childFolders.Add(newChildFolder);
			this.ParentPackage.AddFolderToPackage(newChildFolder);
		}

		/// <summary>
		/// Moves a Folder to its very root node.
		/// </summary>
		public override void ChangeToRootFolder()
		{
			if(m_ParentFolder != null)
			{
				m_ParentFolder.ChildFolders.Remove(this);
				m_ParentFolder = null;				
				this.ParentPackage.AttachmentFolders.Add(this);
                this.Name = this.Name;
			}
		}

		/// <summary>
		/// Moves folder to a different Folder. Returns True if operation successful.	
		/// </summary>
		public override bool MoveToFolder(Folder targetFolder)
		{
			if(targetFolder != null)
			{				
				if(FolderMoveValid(targetFolder))
				{
					if(m_ParentFolder != null)
					{
						m_ParentFolder.ChildFolders.Remove(this);
						m_ParentFolder = null;
					}
					else
					{
						this.ParentPackage.AttachmentFolders.Remove(this);
					}

					this.m_ParentFolder = targetFolder;
					targetFolder.ChildFolders.Add(this);
                    this.Name = this.Name;

					return true;
				}
				else
				{
					//TODO, how to handle?
					//throw new Exception("This operation is invalid, You can not move a folder to one its childrent folders.");
					return false;
				}
			}	
			return false;
		}	
		
		#region ISerializable Members

        public AttachmentFolder(SerializationInfo info, StreamingContext context)
            : base(info, context)
		{			
						
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		base.GetObjectData(info, context);			
		}

		#endregion
	}
}
