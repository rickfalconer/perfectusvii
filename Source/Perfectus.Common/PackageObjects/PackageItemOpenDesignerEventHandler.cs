namespace Perfectus.Common.PackageObjects
{
	// This delegate provides a means for package items to be assocaiated with Multiple Designers,and have the preffered designer set at run time.
	public delegate void PackageItemOpenDesignerEventHandler(object sender, PackageItemEventArgs e, PrefferedDesigner prefferedDesigner);

	public enum PrefferedDesigner
	{
		Default,
		ReportViewer
	}
}
