using System;
using System.Collections;
using System.Windows.Forms;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A dictionary with keys of type String and values of type TreeNode
	/// </summary>
	public sealed class TagTreeNodeDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the TagTreeNodeDictionary class
		/// </summary>
		public TagTreeNodeDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the TreeNode associated with the given String
		/// </summary>
		/// <param name="key">
		/// The String whose value to get or set.
		/// </param>
		public TreeNode this[String key]
		{
			get { return (TreeNode) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this TagTreeNodeDictionary.
		/// </summary>
		/// <param name="key">
		/// The String key of the element to add.
		/// </param>
		/// <param name="value">
		/// The TreeNode value of the element to add.
		/// </param>
		public void Add(String key, TreeNode value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this TagTreeNodeDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The String key to locate in this TagTreeNodeDictionary.
		/// </param>
		/// <returns>
		/// true if this TagTreeNodeDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public bool Contains(String key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this TagTreeNodeDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The String key to locate in this TagTreeNodeDictionary.
		/// </param>
		/// <returns>
		/// true if this TagTreeNodeDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public bool ContainsKey(String key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this TagTreeNodeDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The TreeNode value to locate in this TagTreeNodeDictionary.
		/// </param>
		/// <returns>
		/// true if this TagTreeNodeDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public bool ContainsValue(TreeNode value)
		{
			foreach (TreeNode item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this TagTreeNodeDictionary.
		/// </summary>
		/// <param name="key">
		/// The String key of the element to remove.
		/// </param>
		public void Remove(String key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this TagTreeNodeDictionary.
		/// </summary>
		public ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this TagTreeNodeDictionary.
		/// </summary>
		public ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}