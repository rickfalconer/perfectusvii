using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Design;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Interview.
	/// </summary>
	[Serializable]
	public sealed class Interview : PackageItem, IDesignable, ISerializable, IPackageItemContainer, IXmlSerializable
	{
		#region Private Member Variables

		private InterviewPage startPage;
		private InterviewPageCollection m_pages;
		private Uri destinationUrl;
		private TextQuestionQueryValue reference;
		private int progressMax = -1;
		private MemoryStream diagram;

		// IXmlSerializable
		[NonSerialized] private const string msINTERVIEW_KEY = "Interview";

		#endregion
		#region Properties & Constructor

		[Browsable(false)]
		public int ProgressMax
		{
			get { return progressMax;}
		}

		[Browsable(false)]
		public bool IsProgressInformationCalculated
		{
			get { return progressMax != -1; }
		}

		public Interview() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.Interview.NewItemName");
			m_pages = new InterviewPageCollection();
			startPage = null;
			destinationUrl = null;
			reference = new TextQuestionQueryValue();			
		}


		[Browsable(false)]
		public InterviewPageCollection Pages
		{
			get { return m_pages; }
		}

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.PageListEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.PageListConverter, Studio")]
        [DisplayRanking( 2 )]
        public InterviewPage StartPage
		{
			get { return startPage; }
			set
			{
				startPage = value;
				OnPropertyChanged("StartPage");
			}
		}

		[Browsable(true)]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [DisplayRanking( 6 )]
        public TextQuestionQueryValue Reference
		{
			get { return reference; }

			set { reference = value; }
		}

		[Browsable(true)]
        [DisplayRanking( 4 )]
        //TODO: Uri editor for HTTP, HTTPS maybe?
		public string DestinationUrl
		{
			get { return destinationUrl == null ? null : destinationUrl.AbsoluteUri; }
			set
			{
                if (!(value.Equals("")))
                {
                    destinationUrl = new Uri(value);
                }
                else
                {
                    destinationUrl = null;
                }
                
				OnPropertyChanged("DestinationUrl");
			}
		}
		
		[Browsable(false)]
		public MemoryStream Diagram
		{
			get { return diagram;}
			set { diagram = value;}
		}

		#endregion
		#region ISerializable Members

		public Interview(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			startPage = (InternalPage) info.GetValue("startPage", typeof (InternalPage));
			m_pages = (InterviewPageCollection) info.GetValue("pages", typeof (InterviewPageCollection));
			destinationUrl = (Uri) info.GetValue("destinationUrl", typeof (Uri));
			try
			{
				reference = (TextQuestionQueryValue) info.GetValue("reference", typeof (TextQuestionQueryValue));
			}
			catch
			{
			}
			try
			{
				if (context.State != StreamingContextStates.Persistence)
				{
				diagram = (MemoryStream) info.GetValue("diagram", typeof (MemoryStream));
			}
				else
				{
					diagram = null;
				}
			}
			catch
			{
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("startPage", startPage);
			info.AddValue("pages", m_pages);
			info.AddValue("destinationUrl", destinationUrl);
			info.AddValue("reference", reference);
			info.AddValue("progressMax", progressMax);
			if (context.State != StreamingContextStates.Persistence)
			{
				info.AddValue("diagram", diagram);
			}
			else
			{
				info.AddValue("diagram", null);
			}
		}

		public void CalculatePageRanks()
		{
			// Reset all pages
			foreach(InterviewPage p in Pages)
			{
				p.Progress = -1;
			}
			
		// Build the diagram from the StartPage down to the end.
		if (startPage != null)
			{
				progressMax = -1;
				CalculateRank(startPage, 1, ref progressMax);
			
			}
			else
			{
				return;
			}

		}

		private void CalculateRank(InterviewPage page, int rank, ref int interviewMaxRank)
		{
			
			if (page.Progress == -1)
			{
				page.Progress = rank;

				switch (page.NextPage.ValueType)
				{
					case PageQueryValue.PageQueryValueType.Page:
						if (page.NextPage.PageValue != null)
						{
							CalculateRank(page.NextPage.PageValue, ++rank, ref interviewMaxRank);
						}
						break;
					case PageQueryValue.PageQueryValueType.Query:
						rank++;

						ActionBase[] possibleActions = page.NextPage.QueryValue.AllPossibleActions;

						foreach (ActionBase a in possibleActions)
						{
							NavigationAction na = a as NavigationAction;
							if (na != null)
							{
								if (na.Page != null)
								{
									CalculateRank(na.Page, rank, ref interviewMaxRank);
								}
							}
						}
						break;
				}
			}

			if (rank > interviewMaxRank)
			{
				interviewMaxRank = rank;
			}
		}

		#endregion
		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			return startPage != null && startPage == item;
		}

		#endregion
	
		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		/// <summary>
		///		Serializes the Query into Xml.
		/// </summary>
		/// <param name="w">Writes the object to Xml.</param>
		public void WriteXml(XmlWriter w)
		{	
			w.WriteStartElement(msINTERVIEW_KEY);
			w.WriteElementString(Serializer.NameKey, ((Interview)this).Name);										// e.g. <Name>question_name</Name>
			w.WriteElementString(Serializer.UniqueIdentifierKey, ((Interview)this).UniqueIdentifier.ToString());	// e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
			w.WriteEndElement();
		}
				
		public void ReadXml(XmlReader r)
		{	
			// Read the starting element
			r.ReadStartElement(msINTERVIEW_KEY);

			r.ReadStartElement(Serializer.NameKey);
			this.Name = r.ReadString();
			r.ReadEndElement();
			r.ReadStartElement(Serializer.UniqueIdentifierKey);
			this.UniqueIdentifier = new Guid(r.ReadString());
			r.ReadEndElement();

			// Read the end element
			r.ReadEndElement();
		}

		#endregion
	}
}
