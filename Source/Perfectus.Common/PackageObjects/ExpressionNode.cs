using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for ExpressionNode.
    /// </summary>
    [Serializable]
    public class ExpressionNode : PackageItem, ISerializable
    {
        #region Private Member Variables

        private PackageItem operand1;
        private Operator expressionOperator;
        private PackageItem operand2;

        #endregion
        #region Properties & Constructor

        public PackageItem Operand1
        {
            get { return operand1; }
            set { operand1 = value; }
        }

        public Operator ExpressionOperator
        {
            get { return expressionOperator; }
            set { expressionOperator = value; }
        }

        public PackageItem Operand2
        {
            get { return operand2; }
            set { operand2 = value; }
        }

        /// <summary>
        ///  Constructor - Indended for use by the XmlSerialization Only!
        /// </summary>
        public ExpressionNode() { } //SL

        public ExpressionNode(Package parentPackage, PackageItem operand1, Operator expressionOperator, PackageItem operand2)
        {
            this.ParentPackage = parentPackage;
            this.operand1 = operand1;
            this.expressionOperator = expressionOperator;
            this.operand2 = operand2;
        }

        #endregion
        #region Methods

        public bool ContainsRefTo(PackageItem pi)
        {
            if (operand1 == pi || operand2 == pi)
            {
                return true;
            }
            else
            {
                bool ref1 = false;
                bool ref2 = false;
                if (operand1 is PFunction)
                {
                    ref1 = ((PFunction)operand1).ContainsReferenceTo(pi);
                    if (ref1)
                    {
                        return true;
                    }
                }
                if (operand2 is PFunction)
                {
                    ref2 = ((PFunction)operand2).ContainsReferenceTo(pi);
                    if (ref2)
                    {
                        return true;
                    }
                }

                // The following 2 recursive calls ensure that every row is checked.
                if (operand1 is ExpressionNode)
                {
                    ref1 = ((ExpressionNode)operand1).ContainsRefTo(pi);
                    if (ref1)
                    {
                        return true;
                    }
                }
                if (operand2 is ExpressionNode)
                {
                    ref2 = ((ExpressionNode)operand2).ContainsRefTo(pi);
                    if (ref2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        [Obsolete]
        public object Evaluate()
        {
            return Evaluate(AnswerProvider.Caller.System);
        }

        public object Evaluate(AnswerProvider.Caller caller)
        {
            AnswerProvider left;
            AnswerProvider right;
            Operator op = expressionOperator;

            if (operand1 is ExpressionNode)
            {
                object oLeft = ((ExpressionNode)operand1).Evaluate(caller);
                if (oLeft is decimal)
                {
                    left = new PackageConstant(PerfectusDataType.Number, (decimal)oLeft);
                }
                else if (oLeft is bool)
                {
                    left = new PackageConstant(PerfectusDataType.YesNo, (bool)oLeft);
                }
                else if (oLeft is string)
                {
                    left = new PackageConstant(PerfectusDataType.Text, oLeft.ToString());
                }
                else if (oLeft is DateTime)
                {
                    left = new PackageConstant(PerfectusDataType.Date, oLeft.ToString());
                }

                else
                {
                    left = (AnswerProvider)((ExpressionNode)operand1).Evaluate(caller);
                }
            }
            else
            {
                left = (AnswerProvider)operand1;
            }
            if (operand2 is ExpressionNode)
            {
                object oRight = ((ExpressionNode)operand2).Evaluate(caller);
                if (oRight is decimal)
                {
                    right = new PackageConstant(PerfectusDataType.Number, (decimal)oRight);
                }
                else if (oRight is bool)
                {
                    right = new PackageConstant(PerfectusDataType.YesNo, (bool)oRight);
                }
                else if (oRight is string)
                {
                    right = new PackageConstant(PerfectusDataType.Text, oRight.ToString());
                }
                else if (oRight is DateTime)
                {
                    right = new PackageConstant(PerfectusDataType.Date, oRight.ToString());
                }


                else
                {
                    right = (AnswerProvider)((ExpressionNode)operand2).Evaluate(caller);
                }
            }
            else
            {
                right = (AnswerProvider)operand2;
            }

            switch (op)
            {
                case Operator.And:
                    return And(left, right, caller);
                case Operator.Or:
                    return Or(left, right, caller);
                case Operator.Eq:
                    return Eq(left, right, caller);
                case Operator.Divide:
                    return Divide(left, right, caller);
                case Operator.Ge:
                    return Ge(left, right, caller);
                case Operator.Gt:
                    return Gt(left, right, caller);
                case Operator.Le:
                    return Le(left, right, caller);
                case Operator.Lt:
                    return Lt(left, right, caller);
                case Operator.Minus:
                    return Minus(left, right, caller);
                case Operator.Multiply:
                    return Multiply(left, right, caller);
                case Operator.Ne:
                    return Ne(left, right, caller);
                case Operator.Plus:
                    return Plus(left, right, caller);
                default:
                    throw new NotSupportedException("Invalid operator in an expression node");
            }
        }

        private decimal Plus(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object l = left.GetSingleAnswer(caller);
            object r = right.GetSingleAnswer(caller);
            if (l != null && l.ToString().Length == 0)
            {
                l = null;
            }
            if (r != null && r.ToString().Length == 0)
            {
                r = null;
            }
            try
            {
                if (l == null && r != null)
                {
                    return (decimal)r;
                }
                else if (r == null && l != null)
                {
                    return (decimal)l;
                }
                else if (l == null && r == null)
                {
                    return 0;
                }
                else
                {
                    return (decimal)left.GetSingleAnswer(caller) + (decimal)right.GetSingleAnswer(caller);
                }
            }
            catch
            {
                return 0;
            }
        }

        private decimal Minus(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object l = left.GetSingleAnswer(caller);
            object r = right.GetSingleAnswer(caller);
            if (l != null && l.ToString().Length == 0)
            {
                l = null;
            }
            if (r != null && r.ToString().Length == 0)
            {
                r = null;
            }
            try
            {
                if (l == null && r != null)
                {
                    return (decimal)r;
                }
                else if (r == null && l != null)
                {
                    return (decimal)l;
                }
                else if (l == null && r == null)
                {
                    return 0;
                }
                else
                {
                    return (decimal)left.GetSingleAnswer(caller) - (decimal)right.GetSingleAnswer(caller);
                }
            }
            catch
            {
                return 0;
            }
        }

        private decimal Multiply(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            try
            {
                return (decimal)left.GetSingleAnswer(caller) * (decimal)right.GetSingleAnswer(caller);
            }
            catch
            {
                return 0;
            }
        }

        private decimal Divide(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            try
            {
                return (decimal)left.GetSingleAnswer(caller) / (decimal)right.GetSingleAnswer(caller);
            }
            catch
            {
                return 1;
            }
        }

        private bool Gt(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object leftVal = left == null ? null : CheckForArithmeticRounding(left, caller);
            object rightVal = right == null ? null : CheckForArithmeticRounding(right, caller);

            if (leftVal == null || rightVal == null)
            {
                return false;
            }

            switch (left.DataType)
            {
                case PerfectusDataType.Date:
                    if (leftVal is DateTime && rightVal is DateTime)
                    {
                        return (DateTime)leftVal > (DateTime)rightVal;
                    }
                    else
                    {
                        return false;
                    }
                case PerfectusDataType.Number:
                default:
                    try
                    {
                        return (decimal)leftVal > (decimal)rightVal;
                    }
                    catch
                    {
                        return false;
                    }
            }
        }

        private bool Ge(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object leftVal = left == null ? null : CheckForArithmeticRounding(left, caller);
            object rightVal = right == null ? null : CheckForArithmeticRounding(right, caller);

            if (leftVal == null || rightVal == null)
            {
                return false;
            }

            switch (left.DataType)
            {
                case PerfectusDataType.Date:
                    if (leftVal is DateTime && rightVal is DateTime)
                    {
                        return (DateTime)leftVal >= (DateTime)rightVal;
                    }
                    else
                    {
                        return false;
                    }
                case PerfectusDataType.Number:
                default:
                    try
                    {
                        return (decimal)leftVal >= (decimal)rightVal;
                    }
                    catch
                    {
                        return false;
                    }

            }
        }

        private bool Lt(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object leftVal = left == null ? null : CheckForArithmeticRounding(left, caller);
            object rightVal = right == null ? null : CheckForArithmeticRounding(right, caller);

            if (leftVal == null || rightVal == null)
            {
                return false;
            }

            switch (left.DataType)
            {
                case PerfectusDataType.Date:
                    if (leftVal is DateTime && rightVal is DateTime)
                    {

                        return (DateTime)leftVal < (DateTime)rightVal;
                    }
                    else
                    {
                        return false;
                    }
                case PerfectusDataType.Number:
                default:
                    try
                    {
                        return (decimal)leftVal < (decimal)rightVal;
                    }
                    catch
                    {
                        return false;
                    }

            }
        }

        private bool Le(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object leftVal = left == null ? null : CheckForArithmeticRounding(left, caller);
            object rightVal = right == null ? null : CheckForArithmeticRounding(right, caller);

            if (leftVal == null || rightVal == null)
            {
                return false;
            }

            switch (left.DataType)
            {
                case PerfectusDataType.Date:
                    if (leftVal is DateTime && rightVal is DateTime)
                    {

                        return (DateTime)leftVal <= (DateTime)rightVal;
                    }
                    else
                    {
                        return false;
                    }
                case PerfectusDataType.Number:
                default:
                    try
                    {
                        return (decimal)leftVal <= (decimal)rightVal;
                    }
                    catch
                    {
                        return false;
                    }
            }
        }

        private object CheckForArithmeticRounding(AnswerProvider answerProvider, AnswerProvider.Caller caller)
        {
            if (answerProvider is ArithmeticPFunction)
            {
                return ((ArithmeticPFunction)answerProvider).GetRoundedValueForQuery(caller);
            }
            else
            {
                return answerProvider.GetSingleAnswer(caller);
            }
        }

        private bool Eq(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object rightVal = right.GetSingleAnswer(caller);

            if (left is Question)
            {
                Question leftQ = (Question)left;
                if (right is PackageConstant)
                {
                    string rightQueryVal = ((PackageConstant)(right)).Value.ToString();

                    if (rightQueryVal == "Not Answered")
                    {
                        return IsNotAnswered(leftQ, caller);
                    }
                    else if (rightQueryVal == "Null")
                    {
                        return IsNull(leftQ, caller);
                    }
                }

                AnswerCollection lColl;
                int collCount = leftQ.GetAnswerCount(caller);

                for (int i = 0; i < collCount; i++)
                {
                    lColl = leftQ.GetAnswer(i, caller);
                    for (int j = 0; j < lColl.Count; j++)
                    {
                        object leftVal = AnswerProvider.ConvertToBestType(lColl[j], left.DataType, caller, left.StringBindingMetaData);
                        object rightValueChecked = CheckForArithmeticRounding(right, caller);

                        if (Eq(leftVal, rightValueChecked))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            else
            {
                object leftValueChecked = CheckForArithmeticRounding(left, caller);
                object rightValueChecked = CheckForArithmeticRounding(right, caller);

                return Eq(leftValueChecked, rightValueChecked);
            }
        }

        private bool Eq(object leftVal, object rightVal)
        {
            if (leftVal is string && rightVal is string)
            {
                return leftVal.ToString().ToUpper(CultureInfo.InvariantCulture) == rightVal.ToString().ToUpper(CultureInfo.InvariantCulture);
            }
            else if (leftVal == null && rightVal == null)
            {
                return true;
            }
            else if (leftVal == null && rightVal != null)
            {
                return false;
            }
            else if (leftVal is DateTime && rightVal is DateTime)
            {
                return ((DateTime)leftVal).ToShortDateString().ToUpper(CultureInfo.InvariantCulture) == ((DateTime)rightVal).ToShortDateString().ToUpper(CultureInfo.InvariantCulture);
            }
            else
            {
                return leftVal.Equals(rightVal);
            }
        }

        private bool Ne(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            object rightVal = right.GetSingleAnswer(caller);
            if (left is Question)
            {
                Question leftQ = (Question)left;

                if (right is PackageConstant)
                {
                    string rightQueryVal = ((PackageConstant)(right)).Value.ToString();
                    if (rightQueryVal == "Not Answered")
                    {
                        return IsNotAnswered(leftQ, caller) ? false : true;
                    }
                    else if (rightQueryVal == "Null")
                    {
                        return IsNull(leftQ, caller) ? false : true;
                    }
                }

                AnswerCollection lColl;
                int collCount = leftQ.GetAnswerCount(caller);
                for (int i = 0; i < collCount; i++)
                {
                    lColl = leftQ.GetAnswer(i, caller);
                    for (int j = 0; j < lColl.Count; j++)
                    {
                        object leftVal = AnswerProvider.ConvertToBestType(lColl[j], left.DataType, caller, left.StringBindingMetaData);
                        object rightValueChecked = CheckForArithmeticRounding(right, caller);

                        if (Eq(leftVal, rightValueChecked))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            else
            {
                object leftValueChecked = CheckForArithmeticRounding(left, caller);
                object rightValueChecked = CheckForArithmeticRounding(right, caller);

                return Ne(leftValueChecked, rightValueChecked);
            }
        }

        private bool Ne(object leftVal, object rightVal)
        {
            if (leftVal == null && rightVal == null)
            {
                return true;
            }
            else if (leftVal == null)
            {
                return false;
            }
            else
            {
                return !(leftVal.Equals(rightVal));
            }
        }

        private bool And(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            bool leftVal = (bool)left.GetSingleAnswer(caller);
            bool rightVal = (bool)right.GetSingleAnswer(caller);

            return leftVal && rightVal;
        }

        private bool Or(AnswerProvider left, AnswerProvider right, AnswerProvider.Caller caller)
        {
            bool leftVal = (bool)left.GetSingleAnswer(caller);
            bool rightVal = (bool)right.GetSingleAnswer(caller);

            return leftVal || rightVal;
        }

        /// <summary>
        ///     Indicates whether a given question has not been answered during the interview
        /// </summary>
        /// <param name="q">A reference to a question.</param>
        /// <param name="caller">The given AnswerProvider context.</param>
        /// <returns>A boolean indicating whether the given question is unanswered (true), or answered (false).</returns>
        private bool IsNotAnswered(Question q, AnswerProvider.Caller caller)
        {
            if (q.Seen && q.GetSingleAnswer(caller) == null)
            {
                return true; // q is not answered
            }
            else if (q.Seen && q.GetSingleAnswer(caller).ToString().Length == 0)
            {
                return true; // q is not answered
            }
            else if (!q.Seen && (q.DefaultAnswer == null || q.DefaultAnswer.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.NoValue))
            {
                return true; // q is not answered
            }
            else
            {
                return false; // q is answered
            }
        }

        /// <summary>
        ///     Indicates whether a given question's answer is null, or not.
        /// </summary>
        /// <param name="q">A reference to a question.</param>
        /// <param name="caller">The given AnswerProvider context.</param>
        /// <returns>A boolean indicating whether the given question's answer is null (true), or not (false).</returns>
        private bool IsNull(Question q, AnswerProvider.Caller caller)
        {
            if (q.GetSingleAnswer(caller) == null)
            {
                return true; // q's answer is null
            }
            else if (q.GetSingleAnswer(caller).ToString().Length == 0)
            {
                return true; // q's answer is null
            }
            else
            {
                return false; // q's answer is not null
            }
        }

        public override string ToString()
        {
            return string.Format("[{0}] [{1}] [{2}]", operand1, Enum.GetName(typeof(Operator), expressionOperator), operand2);
        }

        public string ToJavascriptForFormsString()
        {
            string formatString = "({0} {1} {2})";
            string jsOperator = " ";
            string jsOperand1 = " ";
            string jsOperand2 = " ";
            switch (expressionOperator)
            {
                case Operator.And:
                    jsOperator = "&&";
                    break;
                case Operator.Divide:
                    jsOperator = "/";
                    break;
                case Operator.Eq:
                    jsOperator = "==";
                    break;
                case Operator.Ge:
                    jsOperator = ">=";
                    break;
                case Operator.Gt:
                    jsOperator = ">";
                    break;
                case Operator.Le:
                    jsOperator = "<=";
                    break;
                case Operator.Lt:
                    jsOperator = "<";
                    break;
                case Operator.Minus:
                    jsOperator = "-";
                    break;
                case Operator.Multiply:
                    jsOperator = "*";
                    break;
                case Operator.Ne:
                    jsOperator = "!=";
                    break;
                case Operator.Or:
                    jsOperator = "||";
                    break;
                case Operator.Plus:
                    jsOperator = "+";
                    break;
            }


            if (operand1 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand1;
                jsOperand1 = c.ToJavascriptForBrowserString();
            }
            else if (operand1 is ExpressionNode)
            {
                jsOperand1 = ((ExpressionNode)operand1).ToJavascriptForFormsString();
            }
            else
            {
                jsOperand1 = operand1.AlternateName1;
            }

            if (operand2 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand2;
                jsOperand2 = c.ToJavascriptForBrowserString();
            }
            else if (operand2 is ExpressionNode)
            {
                jsOperand2 = ((ExpressionNode)operand2).ToJavascriptForFormsString();
            }
            else
            {
                jsOperand2 = operand2.AlternateName1;
            }

            return string.Format(formatString, jsOperand1, jsOperator, jsOperand2);
        }

        public string ToJavascriptForBrowserString()
        {
            string formatStringL2R = "( {0} {1} {2} )";
            string formatStringFunc = "{1}({0}, {2})";
            string formatString = formatStringFunc;
            string jsOperator = " ";
            string jsOperand1 = " ";
            string jsOperand2 = " ";
            switch (expressionOperator)
            {
                case Operator.And:
                    jsOperator = "&&";
                    formatString = formatStringL2R;
                    break;
                case Operator.Divide:
                    jsOperator = "Div";
                    formatString = formatStringFunc;
                    break;
                case Operator.Eq:
                    jsOperator = "Eq";
                    formatString = formatStringFunc;
                    break;
                case Operator.Ge:
                    jsOperator = "Ge";
                    formatString = formatStringFunc;
                    break;
                case Operator.Gt:
                    jsOperator = "Gt";
                    formatString = formatStringFunc;
                    break;
                case Operator.Le:
                    jsOperator = "Le";
                    formatString = formatStringFunc;
                    break;
                case Operator.Lt:
                    jsOperator = "Lt";
                    formatString = formatStringFunc;
                    break;
                case Operator.Minus:
                    jsOperator = "Minus";
                    formatString = formatStringFunc;
                    break;
                case Operator.Multiply:
                    jsOperator = "Mult";
                    formatString = formatStringFunc;
                    break;
                case Operator.Ne:
                    jsOperator = "Ne";
                    formatString = formatStringFunc;
                    break;
                case Operator.Or:
                    jsOperator = "||";
                    formatString = formatStringL2R;
                    break;
                case Operator.Plus:
                    jsOperator = "Plus";
                    formatString = formatStringFunc;
                    break;
            }


            if (operand1 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand1;
                jsOperand1 = c.ToJavascriptForBrowserString();
            }
            else if (operand1 is ExpressionNode)
            {
                jsOperand1 = ((ExpressionNode)operand1).ToJavascriptForBrowserString();
            }
            else
            {
                jsOperand1 = string.Format("answerTo{0:N}", operand1.UniqueIdentifier);
            }

            if (operand2 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand2;
                jsOperand2 = c.ToJavascriptForBrowserString();
            }
            else if (operand2 is ExpressionNode)
            {
                jsOperand2 = ((ExpressionNode)operand2).ToJavascriptForBrowserString();
            }
            else
            {
                jsOperand2 = string.Format("answerTo{0:N}", operand2.UniqueIdentifier);
            }

            return string.Format(formatString, jsOperand1, jsOperator, jsOperand2);
        }

        public string ToPreviewString()
        {
            string formatString = "({0} {1} {2})";
            string jsOperator = " ";
            string jsOperand1 = " ";
            string jsOperand2 = " ";
            switch (expressionOperator)
            {
                case Operator.And:
                    jsOperator = "AND";
                    break;
                case Operator.Divide:
                    jsOperator = "/";
                    break;
                case Operator.Eq:
                    jsOperator = "=";
                    break;
                case Operator.Ge:
                    jsOperator = ">=";
                    break;
                case Operator.Gt:
                    jsOperator = ">";
                    break;
                case Operator.Le:
                    jsOperator = "<=";
                    break;
                case Operator.Lt:
                    jsOperator = "<";
                    break;
                case Operator.Minus:
                    jsOperator = "-";
                    break;
                case Operator.Multiply:
                    jsOperator = "*";
                    break;
                case Operator.Ne:
                    jsOperator = "<>";
                    break;
                case Operator.Or:
                    jsOperator = "OR";
                    break;
                case Operator.Plus:
                    jsOperator = "+";
                    break;
            }

            if (operand1 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand1;
                jsOperand1 = c.ToPreviewString();
            }
            else if (operand1 is ExpressionNode)
            {
                jsOperand1 = ((ExpressionNode)operand1).ToPreviewString();
            }
            else
            {
                jsOperand1 = string.Format("�{0}�", operand1.Name);
            }

            if (operand2 is PackageConstant)
            {
                PackageConstant c = (PackageConstant)operand2;
                jsOperand2 = c.ToPreviewString();
            }
            else if (operand2 is ExpressionNode)
            {
                jsOperand2 = ((ExpressionNode)operand2).ToPreviewString();
            }
            else
            {
                jsOperand2 = string.Format("�{0}�", operand2.Name);
            }

            return string.Format(formatString, jsOperand1, jsOperator, jsOperand2);
        }


        #endregion
        #region ISerializable Members

        public ExpressionNode(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            operand1 = (PackageItem)info.GetValue("operand1", typeof(PackageItem));
            expressionOperator = (Operator)info.GetValue("expressionOperator", typeof(Operator));
            operand2 = (PackageItem)info.GetValue("operand2", typeof(PackageItem));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("operand1", operand1);
            info.AddValue("expressionOperator", expressionOperator);
            info.AddValue("operand2", operand2);
        }

        #endregion
    }
}