using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects.WordML
{
    public static class WordMLUtilities
    {
        private const string PERFECTUS_MARKER = "uid";

        public static Dictionary<string, Guid> GetDistinctPackageItemListFromWordML(string wordML)
        {
            Dictionary<string, Guid> dependentPackageItems = new Dictionary<string, Guid>();
            return GetDistinctPackageItemListFromWordML(wordML, dependentPackageItems);
        }

        public static Dictionary<string, Guid> GetDistinctPackageItemListFromWordML(string wordML, Dictionary<string, Guid> dependentPackageItems)
        {
            MemoryStream stream;

            if (dependentPackageItems == null)
            {
                dependentPackageItems = new Dictionary<string, Guid>();
            }

            if (wordML != null)
            {
                stream = new MemoryStream(Encoding.ASCII.GetBytes(wordML));
                XmlTextReader xmlTextReader = new XmlTextReader(stream);
                return GetDistinctPackageItemList(xmlTextReader, dependentPackageItems);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///     Iterates the WordML for referenced Package Items and places them into a distinct list
        /// </summary>
        /// <returns>A distinct list of referenced package items from the WordML.</returns>
        public static Dictionary<string, Guid> GetDistinctPackageItemListFromWordMLPath(string path)
        {
            FileStream stream;
            Dictionary<string, Guid> dependentPackageItems = new Dictionary<string, Guid>();

            // Path Validation
            if (path == null || path == string.Empty || path.Length == 0)
            {
                throw new ArgumentNullException("The path to the WordML file must provided."); //TODO - RESOURCE
            }

            if (!File.Exists(path))
            {
                throw new ArgumentException("The path to the WordML must be a valid file path."); //TODO - RESOURCE
            }

            if (path != null)
            {
                // Open the file & get the dependent items
                using (stream = File.OpenRead(path))
                {
                    XmlTextReader xmlTextReader = new XmlTextReader(stream);
                    return GetDistinctPackageItemList(xmlTextReader, dependentPackageItems);
                }                
            }
            else
            {
                return null;
            }
        }

        private static Dictionary<string, Guid> GetDistinctPackageItemList(XmlTextReader wordMLReader,
                                                                           Dictionary<string, Guid> dependentPackageItems) {            
            
            try
            {
                // Read the files contents
                while (wordMLReader.Read())
                {
                    // Locate package items and place them into the distinct package item list
                    switch (wordMLReader.NodeType)
                    {
                        case XmlNodeType.Element:

                            string uid = string.Empty;
                            uid = wordMLReader.GetAttribute(PERFECTUS_MARKER);

                            if (uid != null && uid != string.Empty)
                            {
                                Guid guid;
                                Globals.IsGuid(uid, out guid);

                                if (!dependentPackageItems.ContainsValue(guid))
                                {
                                    dependentPackageItems.Add(wordMLReader.Name, guid);
                                }
                            }

                            break;
                    }
                }
                
                return dependentPackageItems;
            }
            catch (XmlException ex)
            {
                if (ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2))
                {
                    //TODO - Resource
                    throw new XmlException("The file is not a valid xml file.", ex);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                if (ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2))
                {
                    //TODO - Resource
                    throw new Exception("There was an error in parsing the WordML file.", ex);
                }
                else
                {
                    return null;
                }
            }
            finally
            {
                wordMLReader = null;
            }
        }

        public static byte[] EnsureWordMLReferencedItems(Package package, byte[] wordMLBytes)
        {
            XmlDocument wordMLDocument = new XmlDocument();
            string wordML = ASCIIEncoding.ASCII.GetString(wordMLBytes);
            wordMLDocument.LoadXml(wordML);
            wordML = EnsureWordMLPackageGUIDS(package, wordMLDocument);

            return ASCIIEncoding.ASCII.GetBytes(wordML);
        }

        /// <summary>
        ///		Validates the given WordML to ensure the referenced package items (pink bits) have valid GUIDs
        /// </summary>
        /// <param name="package">The given package being processed.</param>
        /// <param name="wordML">The WordML document being validated.</param>
        /// <returns>Flag indicating whether an update has been made.</returns>
        private static string EnsureWordMLPackageGUIDS(Package package, XmlDocument wordMLDocument)
        {
            foreach (XmlNode node in wordMLDocument.SelectNodes("//*[@uid]"))
            {
                // see if @uID exists. check outcomes, questions, and conditional texts
                Guid uid = new Guid(node.Attributes["uid"].Value);
                string name = node.LocalName;

                bool found = false;

                if (package.TemplateItemByGuid.Contains(uid))
                {
                    found = true;
                }

                foreach (SimpleOutcome so in package.SimpleOutcomes)
                {
                    if (so.UniqueIdentifier == uid)
                    {
                        found = true;
                    }
                }

                if (!found)
                {
                    // try to fix.

                    int matches = 0;
                    string newId = string.Empty;

                    foreach (SimpleOutcome so in package.SimpleOutcomes)
                    {
                        if (so.Name.ToLower() == name.ToLower())
                        {
                            matches ++;
                            newId = so.UniqueIdentifier.ToString();
                        }
                    }

                    foreach (ITemplateItem ti in package.TemplateItemByGuid.Values)
                    {
                        if (((PackageItem)ti).Name.ToLower() == name.ToLower())
                        {
                            matches ++;
                            newId = ((PackageItem)ti).UniqueIdentifier.ToString();
                            break;
                        }
                    }

                    if (matches == 1)
                    {
                        node.Attributes["uid"].Value = newId;
                    }
                }
            }

            return wordMLDocument.InnerXml;
        }
    }
}