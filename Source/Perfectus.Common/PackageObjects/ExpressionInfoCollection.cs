using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type ExpressionInfo
	/// </summary>
	public class ExpressionInfoCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the ExpressionInfoCollection class.
		/// </summary>
		public ExpressionInfoCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the ExpressionInfoCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new ExpressionInfoCollection.
		/// </param>
		public ExpressionInfoCollection(ExpressionInfo[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the ExpressionInfoCollection class, containing elements
		/// copied from another instance of ExpressionInfoCollection
		/// </summary>
		/// <param name="items">
		/// The ExpressionInfoCollection whose elements are to be added to the new ExpressionInfoCollection.
		/// </param>
		public ExpressionInfoCollection(ExpressionInfoCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this ExpressionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this ExpressionInfoCollection.
		/// </param>
		public virtual void AddRange(ExpressionInfo[] items)
		{
			foreach (ExpressionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another ExpressionInfoCollection to the end of this ExpressionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The ExpressionInfoCollection whose elements are to be added to the end of this ExpressionInfoCollection.
		/// </param>
		public virtual void AddRange(ExpressionInfoCollection items)
		{
			foreach (ExpressionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type ExpressionInfo to the end of this ExpressionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The ExpressionInfo to be added to the end of this ExpressionInfoCollection.
		/// </param>
		public virtual void Add(ExpressionInfo value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic ExpressionInfo value is in this ExpressionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The ExpressionInfo value to locate in this ExpressionInfoCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this ExpressionInfoCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(ExpressionInfo value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this ExpressionInfoCollection
		/// </summary>
		/// <param name="value">
		/// The ExpressionInfo value to locate in the ExpressionInfoCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(ExpressionInfo value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the ExpressionInfoCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the ExpressionInfo is to be inserted.
		/// </param>
		/// <param name="value">
		/// The ExpressionInfo to insert.
		/// </param>
		public virtual void Insert(int index, ExpressionInfo value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the ExpressionInfo at the given index in this ExpressionInfoCollection.
		/// </summary>
		public virtual ExpressionInfo this[int index]
		{
			get { return (ExpressionInfo) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific ExpressionInfo from this ExpressionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The ExpressionInfo value to remove from this ExpressionInfoCollection.
		/// </param>
		public virtual void Remove(ExpressionInfo value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by ExpressionInfoCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(ExpressionInfoCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public ExpressionInfo Current
			{
				get { return (ExpressionInfo) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (ExpressionInfo) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this ExpressionInfoCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}