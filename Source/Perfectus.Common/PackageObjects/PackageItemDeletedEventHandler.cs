namespace Perfectus.Common.PackageObjects
{
	public delegate void PackageItemDeletedEventHandler(object sender, PackageItemDeletedEventArgs e);
}