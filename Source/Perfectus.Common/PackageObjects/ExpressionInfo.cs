using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ExpressionInfo.
	/// </summary>
	public class ExpressionInfo
	{
		private QueryExpression expression;
		private PackageItem parentItem;
		private string parentPropertyName;
		private string name;

		public string Name
		{
			get { return name; }
		}

		public string PreviewString
		{
			get { return previewString; }
		}

		private string previewString;

		public QueryExpression Expression
		{
			get { return expression; }
		}


		public PackageItem ParentItem
		{
			get { return parentItem; }
		}

		public string ParentPropertyName
		{
			get { return parentPropertyName; }
		}

		private ExpressionInfo()
		{
		}

		public ExpressionInfo(QueryExpression expression, PackageItem parentItem, string parentPropertyName)
		{
			if (expression == null)
			{
				throw new ArgumentNullException("expression", "expression cannot be null.");
			}

			if (parentItem == null)
			{
				throw new ArgumentNullException("parentItem", "parentItem cannot be null.");
			}
			this.expression = expression;
			this.parentItem = parentItem;
			this.parentPropertyName = parentPropertyName;
			this.previewString = expression.ToPreviewString();

			if (parentPropertyName != null)
			{
				name = string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ExpressionInfo.PropertyOfItemDescription"), ParentPropertyName, parentItem.Name);
			}
			else
			{
				name = string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.ExpressionInfo.DefinitionOfItemDescription"), parentItem.Name);
			}
		}


	}
}