using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
    [Serializable]
    public class WordMLDependentPackageItems : List<LibraryItem>, IXmlSerializable
    {
        #region IXmlSerializable Members

        /// <summary>
        /// This is required for the interface, but producing an XmlSchema is not required for our purposes,
        /// and therefore we simply return null.
        /// </summary>
        /// <returns>null</returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Deserializes into object from XML.
        /// </summary>
        /// <param name="r">The reader to read XML from.</param>
        public void ReadXml(System.Xml.XmlReader reader)
        {
            // read the element - don't worry about reading the dependencies
            reader.Read();
        }

        /// <summary>
        ///	    Serializes each LibraryItem in the collection
        /// </summary>
        /// <param name="w">Writes the object to Xml.</param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement(Serializer.WordMLDependenciesKey);

            Enumerator enumerator = this.GetEnumerator();

            foreach(LibraryItem libraryItem in this)
            {
                Serializer.WriteDependentInformation(writer, libraryItem, Serializer.QuestionValueTypeKey, false);
            }

            // end dependencies.
            writer.WriteEndElement();
        }

        #endregion
    }
}