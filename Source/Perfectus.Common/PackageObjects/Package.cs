using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing.Design;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;
using Perfectus.Common.SharedLibrary;
using Perfectus.Common.Helper;


namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for Package.
    /// </summary>
    [Serializable]
    public sealed class Package : LocalisedPropertiesObject, IDisposable, ISerializable, ICloneable
    {
        private string instanceReference = null;
        private DisplayTypeExtensionInfoDictionary extensionMapping;
        private FormatStringExtensionInfoDictionary formatStringExtensionMapping;
        private Hashtable namesTable = new Hashtable();
        private byte[] hashAtLastSave;
        [NonSerialized] private TransientErrorCollection transientErrors = new TransientErrorCollection();
        [NonSerialized] private bool sharedLibraryEventsDisabled = true;

        public event EventHandler<PackageItemEventArgs> EnsureDesignerSaved;
        public event EventHandler<PackageItemEventArgs> BindingsCannotBeReconnected;

        // Dictionarys
        private QuestionDictionary questionDictionary;
        private TemplateItemDictionary templateItemDictionary;
        private StringBindingItemDictionary stringBindingItemDictionary;

        [Browsable(false)]
        public Hashtable NamesTable
        {
            get { return namesTable; }
            set { namesTable=value; }
        }
        
        [Browsable(false)]
        public string InstanceReference
        {
            get { return instanceReference; }
            set { instanceReference = value; }
        }

        [Browsable(false)]
        public DisplayTypeExtensionInfoDictionary ExtensionMapping
        {
            get { return extensionMapping; }
        }

        [Browsable(false)]
        public FormatStringExtensionInfoDictionary FormatStringExtensionMapping
        {
            get { return formatStringExtensionMapping; }
        }

        [Browsable(false)]
        public TransientErrorCollection TransientErrors
        {
            get { return transientErrors; }
        }

        [Browsable(false)]
        public QuestionDictionary QuestionByGuid
        {
            get { return questionDictionary; }
        }

        [Browsable(false)]
        public TemplateItemDictionary TemplateItemByGuid
        {
            get { return templateItemDictionary; }
        }

        private InterviewPageDictionary interviewPageDictionary;

        [Browsable(false)]
        public InterviewPageDictionary InterviewPageByGuid
        {
            get { return interviewPageDictionary; }
        }

        [Browsable(false)]
        public StringBindingItemDictionary StringBindingItemByGuid
        {
            get { return stringBindingItemDictionary; }
        }

        [Browsable(false)]
        public bool SharedLibraryEventsDisabled
        {
            get { return sharedLibraryEventsDisabled; }
            set { sharedLibraryEventsDisabled = value; }
        }

        public void Dispose()
        {
            foreach (TemplateDocument t in templates)
            {
                if (t is IDisposable)
                {
                    ((IDisposable) t).Dispose();
                }
            }
        }

        [field : NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        [field: NonSerialized]
        public event EventHandler<RuleDeliveryEventArgs> RuleDeliveryAdded;

        [field: NonSerialized]
        public event EventHandler<DeliveryDistributorEventArgs> DeliveryDistributorAdded;

        [field: NonSerialized]
        public event EventHandler<DeliveryDocumentEventArgs> DeliveryDocumentAdded;

        [field : NonSerialized]
        public event EventHandler<PackageItemEventArgs> ItemCreated;

        [field : NonSerialized]
        public event EventHandler<PackageItemDeletedEventArgs> ItemDeleted;

        [field: NonSerialized]
        public event EventHandler<LibraryItemEventArgs> ItemUIDChanged;

        [field: NonSerialized]
        public event EventHandler<LibraryItemEventArgs> LibraryItemUpdated;

        [field : NonSerialized]
        public event EventHandler<PackageItemCancelEventArgs> ItemDeleting;

        [field : NonSerialized]
        public event EventHandler<ProgressEventArgs> Progress;

        [field : NonSerialized]
        public event EventHandler BulkLoadStarted;

        [field : NonSerialized]
        public event EventHandler BulkLoadDone;

        public void ResetName()
        {
            name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                "Perfectus.Common.PackageObjects.Package.NewItemName");
        }

        public Package()
        {
            schemaVersionMajor = 5;
            schemaVersionMinor = 5;
            name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                "Perfectus.Common.PackageObjects.Package.NewItemName");
            creationDateTime = DateTime.UtcNow;
            modificationDateTime = creationDateTime;
            createdBy = WindowsIdentity.GetCurrent().Name;
            modifiedBy = createdBy;
            uniqueIdentifier = Guid.NewGuid();

            interviews = new InterviewCollection();
            templates = new TemplateDocumentCollection();
            attachments = new AttachmentCollection( );
            questions = new QuestionCollection();
            outcomes = new OutcomeCollection();
            simpleOutcomes = new SimpleOutcomeCollection();
            pFunctions = new PFunctionCollection();
            dataSources = new DataSourceCollection();
            servers = new ServerCollection();
            simpleOutcomeFolders = new SimpleOutcomeFolderCollection();
            outcomeFolders = new OutcomeFolderCollection();
            attachmentFolders = new AttachmentFolderCollection();
            questionFolders = new QuestionFolderCollection();
            pFunctionFolders = new PFunctionFolderCollection();
            interviewPageDictionary = new InterviewPageDictionary();
            questionDictionary = new QuestionDictionary();
            templateItemDictionary = new TemplateItemDictionary();
            stringBindingItemDictionary = new StringBindingItemDictionary();
            webReferencesDictionary = new WebReferenceDictionary();
            extensionMapping = new DisplayTypeExtensionInfoDictionary();
            formatStringExtensionMapping = new FormatStringExtensionInfoDictionary();

            makeMirror = new YesNoQueryValue();
            makeMirror.YesNoValue = false;
        }

        public void SetNoChanges()
        {
            hashAtLastSave = GetHash(this);
        }

        private WebReferenceDictionary webReferencesDictionary;

        private Guid uniqueIdentifier;
        private string currentFilePath = null;
        private DateTime creationDateTime;
        private DateTime modificationDateTime;
        private DateTime publishedDateTime;
        private int schemaVersionMajor;
        private int schemaVersionMinor;
        private string createdBy;
        private string modifiedBy;
        private string publishedBy;
        private string description;
        private string versionXml;
        private InterviewCollection interviews;
        private TemplateDocumentCollection templates;
        private AttachmentCollection attachments;
        private QuestionCollection questions;
        private OutcomeCollection outcomes;
        private PFunctionCollection pFunctions;
        private DataSourceCollection dataSources;
        private ServerCollection servers;
        private SimpleOutcomeCollection simpleOutcomes;
        private SimpleOutcomeFolderCollection simpleOutcomeFolders;
        private OutcomeFolderCollection outcomeFolders;
        private AttachmentFolderCollection attachmentFolders;
        private QuestionFolderCollection questionFolders;
        private PFunctionFolderCollection pFunctionFolders;
        private string name;
        private string notes = "";
        private string mirrorDocType = "Master";
        private YesNoQueryValue makeMirror;
        private Guid tempOfflineId;

        [Browsable(true)]
        [Category("Details")]
        [DisplayRanking(0)]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }


        [Browsable(true)]
        [Category("Details")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.StringEditor, Studio", typeof (UITypeEditor))]
        [DisplayRanking(2)]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [Browsable(false)]
        public string PublishedBy
        {
            get { return publishedBy; }
            set { publishedBy = value; }
        }

        [Browsable(false)]
        public DateTime PublishedDateTime
        {
            get { return publishedDateTime; }
            set { publishedDateTime = value; }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 4 )]
        public string CurrentFilePath
        {
            get { return currentFilePath; }
            set
            {
                currentFilePath = value;
                OnPropertyChanged("CurrentFilePath");
            }
        }


        [Browsable(false)]
        public InterviewCollection Interviews
        {
            get { return interviews; }
        }

        [Browsable(false)]
        public ServerCollection Servers
        {
            get { return servers; }
        }

        [Browsable(false)]
        public List<IDocument> DocumentsInUse
        {
            get
            {
                List<IDocument> documents = new List<IDocument>();
                
                foreach (Server server in Servers)
                    foreach (Rule2 rule in server.Rule2)
                        foreach (Delivery delivery in rule.Deliveries)
                            foreach (IDocument document in delivery.Documents)
                                if ( !(document is IAttachment) && !documents.Contains(document))
                                    documents.Add(document);
                return documents;
            }
        }

        [Browsable(false)]
        public List<IDocument> DocumentsAndAttachmentsInUse
        {
            get
            {
                List<IDocument> documents = new List<IDocument>();

                foreach (Server server in Servers)
                    foreach (Rule2 rule in server.Rule2)
                        foreach (Delivery delivery in rule.Deliveries)
                            foreach (IDocument document in delivery.Documents)
                                if (!documents.Contains(document))
                                    documents.Add(document);
                return documents;
            }
        }

        [Browsable(false)]
        public AttachmentCollection Attachments
        {
            get { return attachments;}
        }
        

        [Browsable(false)]
        public TemplateDocumentCollection Templates
        {
            get { return templates; }
        }

        [Browsable(false)]
        public QuestionCollection Questions
        {
            get { return questions; }
        }

        [Browsable(false)]
        public OutcomeCollection Outcomes
        {
            get { return outcomes; }
        }

        [Browsable(false)]
        public PFunctionCollection Functions
        {
            get { return pFunctions; }
        }

        [Browsable(false)]
        public SimpleOutcomeCollection SimpleOutcomes
        {
            get { return simpleOutcomes; }
        }

        [Browsable(false)]
        public DataSourceCollection DataSources
        {
            get { return dataSources; }
        }

        [Browsable(false)]
        public SimpleOutcomeFolderCollection SimpleOutcomeFolders
        {
            get { return simpleOutcomeFolders; }
        }

        [Browsable(false)]
        public OutcomeFolderCollection OutcomeFolders
        {
            get { return outcomeFolders; }
        }

        [Browsable(false)]
        public AttachmentFolderCollection AttachmentFolders
        {
            get { return attachmentFolders; }
        }

        [Browsable(false)]
        public QuestionFolderCollection QuestionsFolders
        {
            get { return questionFolders; }
        }

        [Browsable(false)]
        public PFunctionFolderCollection FunctionFolders
        {
            get { return pFunctionFolders; }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 8 )]
        public string CreatedBy
        {
            get { return createdBy; }
            set
            {
                createdBy = value;
                OnPropertyChanged("CreatedBy");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 12 )]
        public string ModifiedBy
        {
            get { return modifiedBy; }
            set
            {
                modifiedBy = value;
                OnPropertyChanged("ModifiedBy");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 6 )]
        public DateTime CreationDateTime
        {
            get { return creationDateTime; }
            set
            {
                creationDateTime = value;
                OnPropertyChanged("CreationDateTime");
            }
        }

        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 10 )]
        public DateTime ModificationDateTime
        {
            get { return modificationDateTime; }
            set
            {
                modificationDateTime = value;
                OnPropertyChanged("ModificationDateTime");
            }
        }


        [Browsable(true)]
        [ReadOnly(true)]
        [DisplayRanking( 16 )]
        public Guid UniqueIdentifier
        {
            get { return uniqueIdentifier; }
            set
            {
                uniqueIdentifier = value;
                OnPropertyChanged("UniqueIdentifier");
            }
        }


        [Browsable(false)]
        public int SchemaVersionMajor
        {
            get { return schemaVersionMajor; }
            set
            {
                schemaVersionMajor = value;
                OnPropertyChanged("SchemaVersionMajor");
            }
        }


        [Browsable(false)]
        public int SchemaVersionMinor
        {
            get { return schemaVersionMinor; }
            set
            {
                schemaVersionMinor = value;
                OnPropertyChanged("SchemaVersionMinor");
            }
        }

        [Browsable(false)]
        public String VersionXml
        {
            get { return versionXml; }
            set { versionXml = value; }
        }

        [Browsable(false)]
        public WebReferenceDictionary WebReferenceByGuid
        {
            get { return webReferencesDictionary; }
        }


        [Category("Data")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.StringEditor, Studio", typeof (UITypeEditor))]
        [DisplayRanking( 50 )]
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        [Browsable(false)]
        public string MirrorDocType
        {
            get { return mirrorDocType; }
            set
            {
                mirrorDocType = value;
                OnPropertyChanged("MirrorDocType");
            }
        }

        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 14 )]
        public YesNoQueryValue MakeMirror
        {
            get { return makeMirror; }
            set
            {
                makeMirror = value;
                OnPropertyChanged("MakeMirror");
            }
        }

        // Used by offline publishing.
        [Browsable(false)]
        public Guid TempOfflineId
        {
            get { return tempOfflineId; }
            set { tempOfflineId = value; }
        }

        public Interview CreateInterview(bool createStartPage)
        {
            Interview interview = CreateInterview();
            InterviewPage interviewPage = CreatePage(interview);
            
            interviewPage.Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                    GetString("Perfectus.Common.Package.NewInterviewStartPageName");
            interview.StartPage = interviewPage;
            
            return interview;
        }

        public Interview CreateInterview()
        {
            Interview interview = new Interview();
            AddNewInterview(interview);
            return interview;
        }

        private void AddNewInterview(Interview i)
        {
            i.ParentPackage = this;
            interviews.Add(i);

            i.Name = i.Name;
            OnItemCreated(i);
        }

        public WordTemplateDocument2 CreateWordTemplate()
        {
            return CreateWordTemplate(false);
        }

        public WordTemplateDocument2 CreateWordTemplate(bool createBlankFile)
        {
            return CreateWordTemplate(createBlankFile, Guid.Empty);
        }

        public WordTemplateDocument2 CreateWordTemplate(Guid uniqueIdentfier)
        {
            return CreateWordTemplate(false, uniqueIdentfier);
        }

        public WordTemplateDocument2 CreateWordTemplate(bool createBlankFile, Guid uniqueIdentifier)
        {
            WordTemplateDocument2 t = new WordTemplateDocument2();

            if (uniqueIdentifier != Guid.Empty)
            {
                t.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewWordTemplate(t);
            if (createBlankFile)
            {
                t.CreateBlankWordMLFile();
            }
            return t;
        }

        private void AddNewWordTemplate(WordTemplateDocument2 t)
        {
            t.ParentPackage = this;
            templates.Add(t);
            t.Name = t.Name;
            OnItemCreated(t);
        }

        public Question CreateQuestion()
        {
            return CreateQuestion(null, null);
        }

        public Question CreateQuestion(Guid uniqueIdentifier)
        {
            return CreateQuestion(null, null, uniqueIdentifier);
        }

        public Question CreateQuestion(Folder parentFolder)
        {
            return CreateQuestion(null, parentFolder);
        }

        public Question CreateQuestion(string questionName)
        {
            Question q;
            if (questionName == null)
            {
                q = new Question();
            }
            else
            {
                q = new Question(questionName);
            }
            AddNewQuestion(q, true);
            return q;
        }

        public Question CreateQuestion(string questionName, Folder parentFolder)
        {
            return CreateQuestion(questionName, parentFolder, Guid.Empty);
        }

        public Question CreateQuestion(string questionName, Folder parentFolder, Guid uniqueIdentifier)
        {
            Question q;
            if (questionName == null)
            {
                q = new Question();
            }
            else
            {
                q = new Question(questionName);
            }
            q.ParentFolder = parentFolder;

            // Force the unique identifier
            if (uniqueIdentifier != Guid.Empty)
            {
                q.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewQuestion(q, true);
            return q;
        }

        private void AddNewQuestion(Question q, bool checkName)
        {
            q.ParentPackage = this;
            questions.Add(q);
            questionDictionary.Add(q.UniqueIdentifier, q);
            templateItemDictionary.Add(q.UniqueIdentifier, q);
            stringBindingItemDictionary.Add(q.UniqueIdentifier, q);
            if (checkName)
            {
                q.Name = q.Name;
            }
            OnItemCreated(q);
        }

        public OutcomeFolder CreateOutcomeFolder()
        {
            return CreateOutcomeFolder(null);
        }

        /// <summary>
        /// Used for creating Root Outcome Folders (Subfolders should be created using methods on an existing Folder).
        /// </summary>		
        public OutcomeFolder CreateOutcomeFolder(string outcomeName)
        {
            OutcomeFolder of;
            if (outcomeName == null)
            {
                of = new OutcomeFolder();
            }
            else
            {
                of = new OutcomeFolder(outcomeName);
            }
            OutcomeFolders.Add(of);
            AddNewFolder(of, true);
            return of;
        }

        public AttachmentFolder CreateAttachmentFolder()
        {
            return CreateAttachmentFolder(null);
        }
        
        public AttachmentFolder CreateAttachmentFolder(string attachmentName)
        {
            AttachmentFolder af;
            if (attachmentName == null)
            {
                af = new AttachmentFolder();
            }
            else
            {
                af = new AttachmentFolder(attachmentName);
            }
            AttachmentFolders.Add(af);
            AddNewFolder(af, true);
            return af;
        }


        public SimpleOutcomeFolder CreateSimpleOutcomeFolder()
        {
            return CreateSimpleOutcomeFolder(null);
        }

        /// <summary>
        /// Used for creating Root SimpleOutcome Folders (Subfolders should be created using methods on an existing Folder).
        /// </summary>		
        public SimpleOutcomeFolder CreateSimpleOutcomeFolder(string simpleOutcomeName)
        {
            SimpleOutcomeFolder sof;
            if (simpleOutcomeName == null)
            {
                sof = new SimpleOutcomeFolder();
            }
            else
            {
                sof = new SimpleOutcomeFolder(simpleOutcomeName);
            }
            SimpleOutcomeFolders.Add(sof);
            AddNewFolder(sof, true);
            return sof;
        }

        public QuestionFolder CreateQuestionFolder()
        {
            return CreateQuestionFolder(null);
        }

        /// <summary>
        /// Used for creating Root Question Folders (Subfolders should be created using methods on an existing Folder).
        /// </summary>		
        public QuestionFolder CreateQuestionFolder(string questionFolderName)
        {
            QuestionFolder qf;
            if (questionFolderName == null)
            {
                qf = new QuestionFolder();
            }
            else
            {
                qf = new QuestionFolder(questionFolderName);
            }
            questionFolders.Add(qf);
            AddNewFolder(qf, true);
            return qf;
        }

        public PFunctionFolder CreatePFunctionFolder()
        {
            return CreatePFunctionFolder(null);
        }

        /// <summary>
        /// Used for creating Root Question Folders (Subfolders should be created using methods on an existing Folder).
        /// </summary>		
        public PFunctionFolder CreatePFunctionFolder(string pFunctionFolderName)
        {
            PFunctionFolder ff;
            if (pFunctionFolderName == null)
            {
                ff = new PFunctionFolder();
            }
            else
            {
                ff = new PFunctionFolder(pFunctionFolderName);
            }
            pFunctionFolders.Add(ff);
            AddNewFolder(ff, true);
            return ff;
        }

        internal void AddFolderToPackage(Folder f)
        {
            AddNewFolder(f, true);
        }

        private void AddNewFolder(Folder f, bool checkName)
        {
            f.ParentPackage = this;
            if (checkName)
            {
                f.Name = f.Name;
            }
            OnItemCreated(f);
        }

        public Query CreateQuery()
        {
            Query q = new Query(this);
            return q;
        }

        //Attachment ->
        public AttachmentBase CreateStaticAttachment()
        {
            return CreateStaticAttachment(null);
        }
        public AttachmentBase CreateStaticAttachment(Folder parentFolder)
        {
            return CreateAttachment(new  AttachmentStatic(), null, parentFolder, Guid.Empty);
        }
        //--
        public AttachmentBase CreateDynamicAttachment()
        {
            return CreateDynamicAttachment(null);
        }
        public AttachmentBase CreateDynamicAttachment(Folder parentFolder)
        {
            return CreateAttachment(new AttachmentDynamic(), null, parentFolder, Guid.Empty);
        }
        //--
        public AttachmentBase CreateAttachment(AttachmentBase o,string attachmentName, Folder parentFolder, Guid uniqueIdentifier)
        {
            if (attachmentName != null && attachmentName.Length > 0)
                o.Name = attachmentName;

            o.ParentFolder = parentFolder;
            if (uniqueIdentifier != Guid.Empty)
            {
                o.UniqueIdentifier = uniqueIdentifier;
            }
            AddNewAttachment(o);
            return o;
        }

        private void AddNewAttachment(AttachmentBase o)
        {
            AddNewAttachment(o, true, true);
        }

        private void AddNewAttachment(AttachmentBase o, bool doReset, bool checkName)
        {
            o.ParentPackage = this;
            Attachments.Add(o);
            if (checkName)
            {
                o.Name = o.Name;
            }
            if (doReset)
            {
                o.Condition = new YesNoQueryValue();
                o.Condition.ValueType = YesNoQueryValue.YesNoQueryValueType.YesNo;
                o.Condition.YesNoValue = true; 
            }
            OnItemCreated(o);
        }
        // <- Attachment
        
        public Outcome CreateOutcome()
        {
            return CreateOutcome(null, null);
        }

        public Outcome CreateOutcome(Guid uniqueIdentifier)
        {
            return CreateOutcome(null, null, uniqueIdentifier);
        }

        public Outcome CreateOutcome(Folder parentFolder)
        {
            return CreateOutcome(null, parentFolder);
        }

        public Outcome CreateOutcome(string outcomeName)
        {
            Outcome o;
            if (outcomeName == null)
            {
                o = new Outcome();
            }
            else
            {
                o = new Outcome(outcomeName);
            }
            AddNewOutcome(o);
            return o;
        }

        public Outcome CreateOutcome(string outcomeName, Folder parentFolder)
        {
            return CreateOutcome(outcomeName, parentFolder, Guid.Empty);
        }

        public Outcome CreateOutcome(string outcomeName, Folder parentFolder, Guid uniqueIdentifier)
        {
            Outcome o;
            if (outcomeName == null)
            {
                o = new Outcome();
            }
            else
            {
                o = new Outcome(outcomeName);
            }
            o.ParentFolder = parentFolder;

            if (uniqueIdentifier != Guid.Empty)
            {
                o.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewOutcome(o);
            return o;
        }

        private void AddNewOutcome(Outcome o)
        {
            AddNewOutcome(o, true, true);
        }

        private void AddNewOutcome(Outcome o, bool doReset, bool checkName)
        {
            o.ParentPackage = this;
            outcomes.Add(o);
            templateItemDictionary.Add(o.UniqueIdentifier, o);
            if (checkName)
            {
                o.Name = o.Name;
            }
            if (doReset)
            {
                o.Definition = CreateQuery();
                o.Definition.ActionIfTrue = new OutcomeAction();
                o.Definition.ActionIfFalse = new OutcomeAction();
            }
            OnItemCreated(o);
        }

        public SimpleOutcome CreateSimpleOutcome()
        {
            return CreateSimpleOutcome(null, Guid.Empty);
        }

        public SimpleOutcome CreateSimpleOutcome(Guid uniqueIdentifier)
        {
            return CreateSimpleOutcome(null, uniqueIdentifier);
        }

        public SimpleOutcome CreateSimpleOutcome(Folder parentFolder)
        {
            return CreateSimpleOutcome(parentFolder, Guid.Empty);
        }

        public SimpleOutcome CreateSimpleOutcome(Folder parentFolder, Guid uniqueIdentifier)
        {
            SimpleOutcome s = new SimpleOutcome();
            s.ParentFolder = parentFolder;

            if (uniqueIdentifier != Guid.Empty)
            {
                s.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewSimpleOutcome(s, true);
            return s;
        }

        private void AddNewSimpleOutcome(SimpleOutcome s, bool checkName)
        {
            s.ParentPackage = this;
            simpleOutcomes.Add(s);
            if (checkName)
            {
                s.Name = s.Name;
            }
            OnItemCreated(s);
        }


        public InterviewPage CreatePage(Interview parentInterview)
        {
            return CreatePage(parentInterview, Guid.Empty);
        }

        public InterviewPage CreatePage(Interview parentInterview, Guid uniqueIdentifier)
        {
            if (parentInterview == null)
            {
                throw new ArgumentNullException("parentInterview");
            }
            else
            {
                InternalPage p = new InternalPage(parentInterview);

                if (uniqueIdentifier != Guid.Empty)
                {
                    p.UniqueIdentifier = uniqueIdentifier;
                }

                AddNewPage(p, parentInterview);
                return p;
            }
        }

        public InterviewPage CreateExternalPage(Interview parentInterview)
        {
            if (parentInterview == null)
            {
                throw new ArgumentNullException("parentInterview");
            }
            else
            {
                ExternalPage p = new ExternalPage(parentInterview);
                AddNewPage(p, parentInterview);
                return p;
            }
        }

        private void AddNewPage(InterviewPage p, Interview parentInterview)
        {
            p.ParentPackage = this;
            parentInterview.Pages.Add(p);
            interviewPageDictionary.Add(p.UniqueIdentifier, p);
            if (p.ParentInterview.StartPage == null || p.ParentInterview.Pages.Count == 1)
            {
                p.ParentInterview.StartPage = p;
            }
            p.Name = p.Name;
            OnItemCreated(p);
        }

        public Rule2 CreateRule(Server parentServer)
        {
            if (parentServer == null)
            {
                throw new ArgumentNullException("parentServer");
            }
            else
            {
                Rule2 r = new Rule2(parentServer);
                r.ParentPackage = this;
                parentServer.Rule2.Add(r);
                r.Name = r.Name;
                OnItemCreated(r);
                return r;
            }
        }

        public void AddDeliveryToRule(Rule2 r, Delivery d) {
            d.ParentPackage = this;
            d.Name = d.Name;
            r.Deliveries.Add(d);
            OnRuleDeliveryAdded(r, d);
        }
        
        public void AddDistributorToDelivery(Delivery d, Distributor dist) {
            dist.ParentPackage = this;
            dist.Name = dist.Name;
            d.SetDistributor(dist);
            OnDeliveryDistributorAdded(d, dist);
        }

        public void AddDocumentToDelivery(Delivery d, IDocument doc)
        {
            doc.ParentPackage = this;
            doc.Name = doc.Name;
            doc.ParentDelivery = d;
            d.AddDocument (doc);
            OnDeliveryDocumentAdded(d, doc);
        }


        public PackageItem DuplicateItem(PackageItem i)
        {
            sharedLibraryEventsDisabled = true;

            PackageItem retVal = (PackageItem) (i.Clone());
            retVal.UniqueIdentifier = Guid.NewGuid();
            string newName = string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                GetString("Perfectus.Common.PackageObjects.Package.CopyOf"), i.Name);
            
            retVal.NewName(newName);

            // Ensure the item is not linked to the shared library
            ((LibraryItem) retVal).Linked = false;
            ((LibraryItem) retVal).LibraryUniqueIdentifier = Guid.Empty;
            ((LibraryItem) retVal).RelativePath = null;

            if (retVal is DatePFunction)
            {
                AddNewDatePFunction((DatePFunction) retVal, false);
            }
            else if (retVal is ArithmeticPFunction)
            {
                AddNewArithmeticPFunction((ArithmeticPFunction) retVal, false);
            }
            else if (retVal is Outcome)
            {
                AddNewOutcome((Outcome) retVal, false, false);
            }
            else if (retVal is Question)
            {
                AddNewQuestion((Question) retVal, false);
            }
            else
            {
                if (retVal is SimpleOutcome)
                {
                    AddNewSimpleOutcome((SimpleOutcome) retVal, false);
                }
            }

            sharedLibraryEventsDisabled = false;

            return retVal;
        }

        public void DeleteItem(PackageItem i, bool forceDelete)
        {
            if (i == null)
            {
                throw new ArgumentNullException("i");
            }

            string msg;
            if (!forceDelete && ! IsOkayToDelete(i, out msg))
            {
                throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                        GetString("Perfectus.Common.PackageObjects.Package.CannotBeDeleted") + msg);
            }

            string nameKey = i.Name.ToUpper(CultureInfo.InvariantCulture);
            PackageItemCancelEventArgs e = OnItemDeleting(i);
            if (!e.Cancel)
            {
                if (i is ITemplateItem)
                {
                    templateItemDictionary.Remove(i.UniqueIdentifier);
                }

                if (i is IStringBindingItem)
                {
                    stringBindingItemDictionary.Remove(i.UniqueIdentifier);
                }

                if (i is TemplateDocument)
                {
                    templates.Remove(i as TemplateDocument);
                }
                else if (i is Question)
                {
                    questions.Remove(i as Question);
                    questionDictionary.Remove(i.UniqueIdentifier);
                    RemoveQuestionFromPages(i as Question);
                }
                else if (i is Outcome)
                {
                    outcomes.Remove(i as Outcome);
                }
                else if (i is SimpleOutcome)
                {
                    simpleOutcomes.Remove(i as SimpleOutcome);
                }
                else if (i is PFunction)
                {
                    pFunctions.Remove(i as PFunction);
                }
                else if (i is Server)
                {
                    servers.Remove(i as Server);
                }
                else if (i is Rule2)
                {
                    ((Rule2) i).ParentServer.Rule2.Remove(i as Rule2);
                }
                else if (i is Delivery)
                {
                    Delivery d = (i as Delivery);
                    if (d.ParentRule != null)
                        d.ParentRule .Deliveries.Remove(d);
                }
                else if (i is Distributor)
                {
                    Distributor d = (i as Distributor);
                    foreach (Server server in Servers)
                        foreach (Rule2 rule in server.Rule2)
                            foreach (Delivery delivery in rule.Deliveries)
                                if (delivery.Distributor!=null &&
                                    delivery.Distributor.UniqueIdentifier == d.UniqueIdentifier)
                                    {
                                        delivery.RemoveDistributor();
                                        break;
                                    }
                }
                else if (i is IDocument)
                {
                    // Attachments can be linked to deliveries, or 
                    // exist independently in the tree.
                    IDocument d = (i as IDocument);

                    // Remove only the copy?
                    if (d.ParentDelivery != null && d.ParentDelivery is Delivery)
                        d.ParentDelivery.RemoveDocument(i as IDocument);

                    // No, then maybe remove the global object
                    else if (i is IAttachment)
                        Attachments.Remove(i as AttachmentBase);
                }
                else if (i is DataSource)
                {
                    dataSources.Remove(i as DataSource);
                }
                else if (i is InterviewPage)
                {
                    ((InterviewPage)i).ParentInterview.Pages.Remove(i as InterviewPage);
                    if (((InterviewPage)i).ParentInterview.StartPage == i)
                    {
                        ((InterviewPage)i).ParentInterview.StartPage = null;
                    }
                    interviewPageDictionary.Remove(i.UniqueIdentifier);
                }
                else if (i is Interview)
                {
                    Interview iv = i as Interview;
                    for (int idx = iv.Pages.Count - 1; idx >= 0; idx--)
                    {
                        DeleteItem(iv.Pages[idx], true);
                    }
                    interviews.Remove(iv);
                }
                else if (i is QuestionFolder)
                {
                    QuestionFolder f = i as QuestionFolder;
                    // Delete items in folder
                    for (int idx = questions.Count - 1; idx >= 0; idx--)
                    {
                        if (questions[idx].ParentFolder != null)
                        {
                            if (questions[idx].ParentFolder == f)
                            {
                                string failMsg;
                                if (!IsOkayToDelete((questions[idx]), out failMsg))
                                {
                                    // terminate folder delete.
                                    throw new Exception(failMsg + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                                    GetString("Perfectus.Common.PackageObjects.Package.DeleteTerminate"));
                                }
                                else
                                {
                                    DeleteItem(questions[idx], false);
                                }
                            }
                        }
                    }
                    // Delete Subfolders
                    while (f.ChildFolders.Count > 0)
                    {
                        DeleteItem(f.ChildFolders[0], true);
                    }
                    // Keep integrity in parents child collection
                    if (f.ParentFolder != null)
                    {
                        f.ParentFolder.ChildFolders.Remove(f);
                    }
                    else // it must be in the package folder collection
                    {
                        if (questionFolders.Contains(f))
                        {
                            questionFolders.Remove(f);
                        }
                    }
                }
                else if (i is AttachmentFolder)
                {
                    AttachmentFolder f = i as AttachmentFolder;
                    for (int idx = attachments.Count - 1; idx >= 0; idx--)
                    {
                        if (attachments[idx].ParentFolder != null)
                        {
                            if (attachments[idx].ParentFolder == f)
                            {
                                string failMsg;
                                if (!IsOkayToDelete((attachments[idx]), out failMsg))
                                {
                                    // terminate folder delete.
                                    throw new Exception(failMsg + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                                    GetString("Perfectus.Common.PackageObjects.Package.DeleteTerminate"));
                                }
                                else
                                {
                                    DeleteItem(attachments[idx], false);
                                }
                            }
                        }
                    }
                    // Delete Subfolders
                    while( f.ChildFolders.Count > 0 )
                    {
                        DeleteItem( f.ChildFolders[ 0 ], true );
                    }
                    // Keep integrity in parents child collection
                    if (f.ParentFolder != null)
                    {
                        f.ParentFolder.ChildFolders.Remove(f);
                    }
                    else // it must be in one of the package folder collection
                    {
                        if (attachmentFolders.Contains(f))
                        {
                            attachmentFolders.Remove(f);
                        }
                    }
                }
                else if (i is OutcomeFolder)
                {
                    OutcomeFolder f = i as OutcomeFolder;
                    for (int idx = outcomes.Count - 1; idx >= 0; idx--)
                    {
                        if (outcomes[idx].ParentFolder != null)
                        {
                            if (outcomes[idx].ParentFolder == f)
                            {
                                string failMsg;
                                if (!IsOkayToDelete((outcomes[idx]), out failMsg))
                                {
                                    // terminate folder delete.
                                    throw new Exception(failMsg + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                                    GetString("Perfectus.Common.PackageObjects.Package.DeleteTerminate"));
                                }
                                else
                                {
                                    DeleteItem(outcomes[idx], false);
                                }
                            }
                        }
                    }
                    // Delete Subfolders
                    while( f.ChildFolders.Count > 0 )
                    {
                        DeleteItem( f.ChildFolders[ 0 ], true );
                    }
                    // Keep integrity in parents child collection
                    if (f.ParentFolder != null)
                    {
                        f.ParentFolder.ChildFolders.Remove(f);
                    }
                    else // it must be in one of the package folder collection
                    {
                        if (outcomeFolders.Contains(f))
                        {
                            outcomeFolders.Remove(f);
                        }
                    }
                }
                else if (i is SimpleOutcomeFolder)
                {
                    SimpleOutcomeFolder f = i as SimpleOutcomeFolder;
                    for (int idx = simpleOutcomes.Count - 1; idx >= 0; idx--)
                    {
                        if (simpleOutcomes[idx].ParentFolder != null)
                        {
                            if (simpleOutcomes[idx].ParentFolder == f)
                            {
                                string failMsg;
                                if (!IsOkayToDelete((simpleOutcomes[idx]), out failMsg))
                                {
                                    // terminate folder delete.
                                    throw new Exception(failMsg + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                                    GetString("Perfectus.Common.PackageObjects.Package.DeleteTerminate"));
                                }
                                else
                                {
                                    DeleteItem(simpleOutcomes[idx], false);
                                }
                            }
                        }
                    }
                    // Delete Subfolders
                    while( f.ChildFolders.Count > 0 )
                    {
                        DeleteItem( f.ChildFolders[ 0 ], true );
                    }
                    // Keep integrity in parents child collection
                    if (f.ParentFolder != null)
                    {
                        f.ParentFolder.ChildFolders.Remove(f);
                    }
                    else // it must be in the package folders collection
                    {
                        if (simpleOutcomeFolders.Contains(f))
                        {
                            simpleOutcomeFolders.Remove(f);
                        }
                    }
                }
                else if (i is PFunctionFolder)
                {
                    PFunctionFolder f = i as PFunctionFolder;
                    for (int idx = pFunctions.Count - 1; idx >= 0; idx--)
                    {
                        if (pFunctions[idx].ParentFolder != null)
                        {
                            if (pFunctions[idx].ParentFolder == f)
                            {
                                string failMsg;
                                if (!IsOkayToDelete((pFunctions[idx]), out failMsg))
                                {
                                    // terminate folder delete.
                                    throw new Exception(failMsg + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                                    GetString("Perfectus.Common.PackageObjects.Package.DeleteTerminate"));
                                }
                                else
                                {
                                    DeleteItem(pFunctions[idx], false);
                                }
                            }
                        }
                    }
                    // Delete Subfolders
                    while( f.ChildFolders.Count > 0 )
                    {
                        DeleteItem( f.ChildFolders[ 0 ], true );
                    }
                    // Keep integrity in parents child collection
                    if (f.ParentFolder != null)
                    {
                        f.ParentFolder.ChildFolders.Remove(f);
                    }
                    else // it must be in the package folders collection
                    {
                        if (pFunctionFolders.Contains(f))
                        {
                            pFunctionFolders.Remove(f);
                        }
                    }
                }
                else if (i is Folder)
                {
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                            GetString("Perfectus.Common.PackageObjects.Package.AbstractItemFolderCannotBeDeleted"));
                }

                FreeName(nameKey);

                OnItemDeleted(i.UniqueIdentifier);

                if (i is IDisposable)
                {
                    ((IDisposable) i).Dispose();
                }
            }
        }

        #region ICloneable Members

        public object Clone()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                SaveToStream(this, ms, StreamingContextStates.Persistence);
                return OpenFromStream( ms, StreamingContextStates.Persistence );
            }
        }

        #endregion

        private PackageItemCancelEventArgs OnItemDeleting(PackageItem i)
        {
            PackageItemCancelEventArgs e = new PackageItemCancelEventArgs(i);
            if (ItemDeleting != null)
            {
                ItemDeleting(this, e);
            }
            return e;
        }

        private void RemoveQuestionFromPages(Question q)
        {
            foreach (Interview i in interviews)
            {
                foreach (InterviewPage p in i.Pages)
                {
                    if (p is InternalPage)
                    {
                        RemoveQuestionFromItemList(q, p.Items);
                    }
                }
            }
        }

        private static void RemoveQuestionFromItemList(Question q, PageItemCollection col)
        {
            for (int i = col.Count - 1; i >= 0; i--)
            {
                if (col[i] is Question && ((Question) col[i]) == q)
                {
                    col.RemoveAt(i);
                }
                else if (col[i] is RepeaterZone)
                {
                    RemoveQuestionFromItemList(q, ((RepeaterZone) col[i]).Items);
                }
                else if (col[i] is HorizontalLayoutZone)
                {
                    RemoveQuestionFromItemList(q, ((HorizontalLayoutZone) col[i]).Items);
                }
            }
        }

        private void OnItemUniqueIdentifierChange(LibraryItem libraryItem)
        {
            if (ItemUIDChanged != null)
            {
                ItemUIDChanged(this, new LibraryItemEventArgs(libraryItem));
            }
        }

        private void OnItemDeleted(Guid deletedItemGuid)
        {
            if (ItemDeleted != null)
            {
                ItemDeleted(this, new PackageItemDeletedEventArgs(deletedItemGuid));
            }
        }

        private void OnRuleDeliveryAdded(Rule2 r, Delivery d)
        {
            if (RuleDeliveryAdded!= null)
            {
                RuleDeliveryAdded(this, new RuleDeliveryEventArgs(r, d));
            }
        }
        private void OnDeliveryDistributorAdded(Delivery d, Distributor dist)
        {
            if (DeliveryDistributorAdded != null)
            {
                DeliveryDistributorAdded(this, new DeliveryDistributorEventArgs(d,dist));
            }
        }

        private void OnDeliveryDocumentAdded(Delivery d, IDocument doc)
        {
            if (DeliveryDocumentAdded != null)
            {
                DeliveryDocumentAdded(this, new DeliveryDocumentEventArgs(d, doc));
            }
        }

        public Server CreateServer(ServerInfo info, PluginDescriptor[] plugins, Uri address)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }
            if (plugins == null)
            {
                throw new ArgumentNullException("plugins");
            }

            Server s = new Server(info, plugins, address);
            s.UniqueIdentifier = Guid.NewGuid();
            s.ParentPackage = this;
            s.Name = s.Name;
            info.Name = s.Name;
            info.UniqueIdentifier = s.UniqueIdentifier;

            servers.Add(s);
            OnItemCreated(s);
            return s;
        }

        public DatePFunction CreateDateFunction()
        {
            return CreateDateFunction(null, Guid.Empty);
        }

        public DatePFunction CreateDateFunction(Guid uniqueIdentifier)
        {
            return CreateDateFunction(null, uniqueIdentifier);
        }

        public DatePFunction CreateDateFunction(Folder parentFolder)
        {
            return CreateDateFunction(parentFolder, Guid.Empty);
        }

        public DatePFunction CreateDateFunction(Folder parentFolder, Guid uniqueIdentifier)
        {
            DatePFunction d = new DatePFunction();
            d.ParentFolder = parentFolder;

            if (uniqueIdentifier != Guid.Empty)
            {
                d.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewDatePFunction(d, true);
            return d;
        }

        private void AddNewDatePFunction(DatePFunction d, bool checkName)
        {
            d.ParentPackage = this;
            pFunctions.Add(d);
            templateItemDictionary.Add(d.UniqueIdentifier, d);
            stringBindingItemDictionary.Add(d.UniqueIdentifier, d);
            if (checkName)
            {
                d.Name = d.Name;
            }
            OnItemCreated(d);
        }


        public ArithmeticPFunction CreateArithmeticFunction()
        {
            return CreateArithmeticFunction(null, Guid.Empty);
        }

        public ArithmeticPFunction CreateArithmeticFunction(Guid uniqueIdentifier)
        {
            return CreateArithmeticFunction(null, uniqueIdentifier);
        }

        public ArithmeticPFunction CreateArithmeticFunction(Folder parentFolder)
        {
            return CreateArithmeticFunction(parentFolder, Guid.Empty);
        }

        public ArithmeticPFunction CreateArithmeticFunction(Folder parentFolder, Guid uniqueIdentifier)
        {
            ArithmeticPFunction a = new ArithmeticPFunction();
            a.ParentFolder = parentFolder;

            if (uniqueIdentifier != Guid.Empty)
            {
                a.UniqueIdentifier = uniqueIdentifier;
            }

            AddNewArithmeticPFunction(a, true);
            return a;
        }

        private void AddNewArithmeticPFunction(ArithmeticPFunction a, bool checkName)
        {
            a.ParentPackage = this;
            pFunctions.Add(a);
            templateItemDictionary.Add(a.UniqueIdentifier, a);
            stringBindingItemDictionary.Add(a.UniqueIdentifier, a);
            if (checkName)
            {
                a.Name = a.Name;
            }
            OnItemCreated(a);
        }

        public WebReference CreateWebReference(Uri address, string layoutXml)
        {
            return CreateWebReference(address, layoutXml, Guid.Empty);
        }

        public WebReference CreateWebReference(Uri address, string layoutXml, Guid ID)
        {
            WebReference webReference = new WebReference(address, layoutXml);

            // Force the ID if provided
            if (ID != Guid.Empty)
            {
                webReference.UniqueIdentifier = ID;
            }

            webReference.ParentPackage = this;
            dataSources.Add(webReference);
            webReferencesDictionary.Add(webReference.UniqueIdentifier, webReference);
            webReference.Name = webReference.Name;
            OnItemCreated(webReference);
            return webReference;
        }

        private void OnItemCreated(PackageItem item)
        {
            if (ItemCreated != null)
            {
                PackageItemEventArgs e = new PackageItemEventArgs(item);
                ItemCreated(this, e);
            }
        }

        /// <summary>
        /// Will create empty ip file on disk.
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <returns>the file stream</returns>
        public static FileStream CreateOnDisc( string filePath )
        {
            // Create file with Read & Write access, allowing others to get read access.
            return File.Open( filePath, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.Read );
        }

        /// <summary>
        /// Will simply open file and return filestream to file
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <returns>the file stream</returns>
        public static FileStream OpenOnDisc( string filePath )
        {
            // Create file with Read & Write access, allowing others to get read access.
            return File.Open( filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read );
        }

        /// <summary>
        /// Will create ip file on disk by moving from existing file.
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <returns>the file stream of the new file</returns>
        public static FileStream MoveOnDisc( string sourceFile, string destinationFile )
        {
            // Delete file if it exists
            if( File.Exists( destinationFile ) )
                File.Delete( destinationFile );

            // Move source to destination
            File.Move( sourceFile, destinationFile );

            // Finally open new file and return stream
            return File.Open( destinationFile, FileMode.Open, FileAccess.ReadWrite, FileShare.Read );
        }

        /// <summary>
        /// Will open ip file from disk.
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <param name="fs">filestream, it is the reponsibility of the calling routine to close it when needed</param>
        /// <param name="fileAccess">the requested file access</param>
        /// <param name="fileShare">the requested file share, which implies the access level we are allowed to share the file with others, both future and past.</param>
        /// <returns></returns>
        public static Package ReadFromDisc( string filePath, ref FileStream fs, FileAccess fileAccess, FileShare fileShare )
        {
            // First ensure the current file stream is closed
            if( fs != null )
            { fs.Flush( ); fs.Close( ); }

            // Open file with Read & Write access, allowing others to get read access.
            try
            {
                fs = File.Open( filePath, FileMode.Open, fileAccess, fileShare );

                // Extract data from file and serialise into a package object
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                  
                Package p = (Package)OpenFromStream(fs);

                p.currentFilePath = filePath;
                p.hashAtLastSave = GetHash( p );

                return p;
            }
            catch(Exception e)
            {
                // Seeing as we created the file stream, we close it here in case the calling routine doesn't
                if( fs != null )
                { fs.Close( ); fs = null; }

                // Rethrow exception for the UI. The calling UI needs to catch exceptions with file opening and report to user
                throw;
            }
        }

        public static Package OpenFromStream( Stream s )
        {
            return OpenFromStream( s, StreamingContextStates.File );
        }

        public static Package OpenFromStream( Stream s, StreamingContextStates state )
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
                
            // Ensure file stream is positioned at the beginning.
            if( s.CanSeek )
                s.Seek( 0, SeekOrigin.Begin );

            BinaryFormatter formatter = new BinaryFormatter( );

            //formatter.FilterLevel = TypeFilterLevel.Full;
            formatter.Binder = new PackageSerialisationBinder();
            formatter.Context = new StreamingContext(state);

            object o = null;

            if (CompressionHelper.IsCompressed(s))
            {
                // What kind of stream do we have ?!?
                byte[] fBytes = new byte[s.Length];
                int cnt = 0;
                int k = s.ReadByte();
                while ( k != -1 )
                {
                    fBytes[cnt] = (byte)k;
                    k = s.ReadByte();
                    cnt++;
                }
                MemoryStream m = new MemoryStream(CompressionHelper.DecompressIfNecessary(fBytes));
                o = formatter.UnsafeDeserialize(m, null);
            }
            else
            {
                o = formatter.UnsafeDeserialize(s, null);
            }

            ((Package) o).Upgrade();

            ((Package)o).RecreateNamingTable();

            return o as Package;
        }


        private void RecreateNamingTable()
        {
            namesTable = new Hashtable(50);

            // Objects
            foreach (PackageItem item in Templates)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in Servers)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in Interviews)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in DataSources)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in WebReferenceByGuid.Values)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());


            // Items
            foreach (PackageItem item in Questions)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in Outcomes)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in SimpleOutcomes)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in Functions)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

            foreach (PackageItem item in Attachments)
                EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());
            
            // Folders
            foreach (Folder parentFolder in QuestionsFolders)
                if (parentFolder != null)
                    foreach (Folder anyFolder in parentFolder.GetAllFolders())
                        EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

            foreach (Folder parentFolder in AttachmentFolders)
                if (parentFolder != null)
                    foreach (Folder anyFolder in parentFolder.GetAllFolders())
                        EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

            foreach (Folder parentFolder in OutcomeFolders)
                if (parentFolder != null)
                    foreach (Folder anyFolder in parentFolder.GetAllFolders())
                        EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

            foreach (Folder parentFolder in FunctionFolders)
                if (parentFolder != null)
                    foreach (Folder anyFolder in parentFolder.GetAllFolders())
                        EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

            foreach (Folder parentFolder in SimpleOutcomeFolders)
                if (parentFolder != null)
                    foreach (Folder anyFolder in parentFolder.GetAllFolders())
                        EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

            webReferencesDictionary.Clear();
            foreach (DataSource ds in dataSources)
                if (ds is WebReference )
                    webReferencesDictionary.Add(ds.UniqueIdentifier, ds as WebReference);
        }

        public void Upgrade()
        {
            string version = string.Format("{0}.{1}", schemaVersionMajor, schemaVersionMinor);
            switch (version)
            {
                case "4.0":
                    // Touch the names of all items to make sure they are unique - this wasn't a requirement before format 4.1 (pre Beta 1)
                    foreach (Interview i in interviews)
                    {
                        i.Name = i.Name;
                        foreach (InterviewPage p in i.Pages)
                        {
                            p.Name = p.Name;
                        }
                    }

                    foreach (Question i in questions)
                    {
                        i.Name = i.Name;
                    }
                    foreach (TemplateDocument i in templates)
                    {
                        i.Name = i.Name;
                    }
                    foreach (Outcome i in outcomes)
                    {
                        i.Name = i.Name;
                    }
                    foreach (PFunction i in pFunctions)
                    {
                        i.Name = i.Name;
                    }
                    foreach (DataSource i in dataSources)
                    {
                        i.Name = i.Name;
                    }
                    foreach (Server i in servers)
                    {
                        i.Name = i.Name;
                    }

                    // Build an index of all items that could be placed on a template page.
                    templateItemDictionary.Clear();
                    foreach (Question q in questions)
                    {
                        templateItemDictionary.Add(q.UniqueIdentifier, q);
                    }
                    foreach (PFunction pf in pFunctions)
                    {
                        templateItemDictionary.Add(pf.UniqueIdentifier, pf);
                    }
                    foreach (Outcome o in outcomes)
                    {
                        templateItemDictionary.Add(o.UniqueIdentifier, o);
                    }

                    // Build an index of all items that could be placed on a string binding editor.
                    stringBindingItemDictionary.Clear();
                    foreach (Question q in questions)
                    {
                        stringBindingItemDictionary.Add(q.UniqueIdentifier, q);
                    }
                    foreach (PFunction pf in pFunctions)
                    {
                        stringBindingItemDictionary.Add(pf.UniqueIdentifier, pf);
                    }

                    schemaVersionMinor = 1;
                    break;
            }

            UpgradeDataBindings();

            MaybeUpgradeRule_56_to_6();
        }

        private void MaybeUpgradeRule_56_to_6()
        {
            foreach (Perfectus.Common.PackageObjects.Server s in Servers)
            {
                if (s.Rules !=  null && s.Rule2.Count > 0 && s.Rules.Count == 0)
                    continue;

                foreach (Rule r in s.Rules)
                {
                    System.Collections.Generic.List<Document> documents = new System.Collections.Generic.List<Document>();
                    Rule2 rule2 = new Rule2(s);

                    rule2.Name = r.Name;
                    rule2.DoRuleAtFinish = r.DoRuleAtFinish;
                    rule2.DeliveryRule = r.DeliveryRule;
                    rule2.ParentPackage = this;

                    foreach (Converter c in r.Converters)
                    {
                        if (c.Descriptor.PreferredDoc == "AnswerSetXml")
                        {
                            Document d = new Document((Converter)c);
                            d.Name = String.Format("{0}", c.Name);
                            d.ParentPackage = this;
                            documents.Add(d);
                        }
                        else foreach (TemplateDocument t in r.Templates)
                            {
                            // Document PFN shall be Template PFN or Template name
                            // (the behavior changed in v6, but the upgrade shall preserve the 5.6 default)
                                Document d = new Document(t, (Converter)c);
                                d.Name = String.Format("{0} as {1}", t.Name, c.Name);

                                if (t.PublishedFileName != null &&
                                        t.PublishedFileName.ValueType != TextQuestionQueryValue.TextQuestionQueryValueType.NoValue)
                                    d.FileName = t.PublishedFileName;
                                else
                                {
                                    d.FileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                                    d.FileName.TextValue = t.Name;
                                }

                                d.ParentPackage = this;
                                documents.Add(d);
                            }
                    }
                    int i = 0;
                    foreach (Distributor d in r.Distributors)
                    {
                        Delivery delivery = new Delivery(rule2, String.Format("Delivery {0}", ++i));
                        delivery.SetDistributor(d);
                        delivery.ParentPackage = this;
                        foreach (IDocument e in documents)
                        {
                            IDocument ne = e.Clone() as IDocument;
                            delivery.AddDocument(ne);
                        }
                        rule2.Add(delivery);
                    }
                    s.Rule2.Add(rule2);
                    foreach (Perfectus.Common.PackageObjects.Interview interview in Interviews)
                        foreach (InterviewPage page in interview.Pages)
                        {
                            if (!(page is InternalPage))
                                continue;
                            InternalPage ipage = ((InternalPage)page);

                            foreach (Rule r2 in ipage.PageRules)
                            {
                                if (r2 == r)
                                {
                                    if (ipage.PageRules2 == null)
                                        ipage.PageRules2 = new Rule2Collection();
                                    ipage.PageRules2.Add(rule2);
                                }
                            }
                        }
                }

                if (s.Rule2.Count > 0 && s.Rules.Count > 0)
                {
                    while (s.Rules.Count > 0)
                        s.Rules.RemoveAt(0);
                    continue;
                }
            }
        }

        private void UpgradeDataBindings()
        {
            foreach (Question question in questions)
            {
                if (question.DataBindings != null)
                {
                    // If there is a new 'default answer style' defined, then quietly remove the old-style binding.
                    if (question.DefaultAnswer != null && question.DataBindings.AnswerBinding != null)
                    {
                        if (question.DefaultAnswer.ValueType != TextQuestionQueryValue.TextQuestionQueryValueType.NoValue &&
                            question.DataBindings.AnswerBinding.StartsWith("Q"))
                        {
                            question.DataBindings = null;
                        }
                    }

                    if ( question.DataBindings != null )
                        question.DataBindings.Upgrade(question, this);
                }
            }
        }

        public XmlDocument ToPreviewXml()
        {
            MemoryStream ms = new MemoryStream();
            XmlTextWriter xw = new XmlTextWriter(ms, Encoding.UTF8);

            try
            {
                xw.WriteStartDocument();
                xw.WriteStartElement("templateItems");

                foreach (Guid g in templateItemDictionary.Keys)
                {
                    ITemplateItem iti = templateItemDictionary[g];
                    xw.WriteStartElement(iti.GetTagName());
                    xw.WriteAttributeString("uid", ((PackageItem) iti).UniqueIdentifier.ToString());
                    xw.WriteRaw(iti.GetTagContents());
                    xw.WriteEndElement();
                }

                xw.WriteEndElement();
                xw.WriteEndDocument();

                xw.Flush();
                XmlDocument doc = new XmlDocument();
                ms.Seek(0, SeekOrigin.Begin);
                doc.Load(ms);
                return doc;
            }
            finally
            {
                xw.Close();
                ms.Close();
            }
        }

        public static void SaveToDisc( ref FileStream fs, Package p )
        {
            if(fs == null || p.CurrentFilePath == null || p.CurrentFilePath.Length == 0)
            {
                throw new PackageNotSavedException();
            }
            else
            {
                SaveToDisc( ref fs, p, p.CurrentFilePath, false );
            }
        }

        public static void SaveToDisc( ref FileStream fs, Package p, string filePath, bool GenerateNewGuid)
        {
            SaveToDisc( ref fs, p, filePath, GenerateNewGuid, true );
        }

        public static void SaveToDisc( ref FileStream fs, Package p, string filePath, bool GenerateNewGuid, bool resetLocationProperty )
        {
            // This should never happen....
            System.Diagnostics.Debug.Assert( fs != null );

            // Must have a FileStream. Simply ignore if the case.
            if( fs == null )
                return;

            fs.SetLength(0);

            Guid oldGuid = p.UniqueIdentifier;
            DateTime originalModTime = p.ModificationDateTime;
            string originalModifier = p.ModifiedBy;
            string originalFilePath = p.CurrentFilePath;

            try
            {
                if( GenerateNewGuid )
                    p.UniqueIdentifier = Guid.NewGuid( );

                if( resetLocationProperty )
                    p.currentFilePath = filePath;

                // Serialize package, compress and save
                SaveToStream( p, fs, true );
            }
            catch (Exception ex)
            {
                p.ModifiedBy = originalModifier;
                p.ModificationDateTime = originalModTime;
                p.UniqueIdentifier = oldGuid;
                p.currentFilePath = originalFilePath;
                throw new PackageNotSavedException(ex.Message, ex);
            }
        }

        public static void SaveToStream( Package p, Stream s )
        {
            SaveToStream(p, s, StreamingContextStates.File, false);
        }

        public static void SaveToStream(Package p, Stream s, bool snapshotHash)
        {
            SaveToStream(p, s, StreamingContextStates.File, snapshotHash);
        }

        public static void SaveToStream(Package p, Stream s, StreamingContextStates state)
        {
            SaveToStream(p, s, state, false);
        }

        public static void SaveToStream(Package p, Stream s, StreamingContextStates state, bool snapshotHash)
        {
            // Ensure file stream is positioned at the beginning.
            if( s.CanSeek )
                s.Seek( 0, SeekOrigin.Begin );

            BinaryFormatter formatter = new BinaryFormatter();
            //			SoapFormatter formatter = new SoapFormatter();
            //formatter.FilterLevel = TypeFilterLevel.Full;
            formatter.Context = new StreamingContext(state);

            p.ModifiedBy = WindowsIdentity.GetCurrent().Name;
            p.ModificationDateTime = DateTime.UtcNow;

            if (snapshotHash)
            {
                p.hashAtLastSave = GetHash(p);
            }

            string useCompression = ConfigurationSettings.AppSettings["useCompression"];
            bool isCompressionEnabled = (useCompression == null) || (useCompression.ToUpper() != "FALSE");

            if (isCompressionEnabled)
            {
                MemoryStream streamedPackage = new MemoryStream( );
                formatter.Serialize(streamedPackage, p);

                byte[] packageBytes = CompressionHelper.CompressIfSmaller(streamedPackage);
                s.Write(packageBytes, 0, packageBytes.Length);
            }
            else
            {
                formatter.Serialize(s, p);
            }
        }

        private static byte[] GetHash(Package p)
        {
            using (Stream sHash = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(sHash, p);
                sHash.Seek(0, SeekOrigin.Begin);
                MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
                return provider.ComputeHash(sHash);
            }
        }


        public bool HasChanges()
        {
            byte[] currentHash = GetHash(this);

            return ! CompareHashes(currentHash, hashAtLastSave);
        }


        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        private void OnTaskProgress(string taskName, int percentComplete)
        {
            if (Progress != null)
            {
                Progress(this, new ProgressEventArgs(taskName, percentComplete));
            }
        }

        private void OnBulkLoadStarted()
        {
            if (BulkLoadStarted != null)
            {
                BulkLoadStarted(this, new EventArgs());
            }
        }

        private void OnBulkLoadDone()
        {
            if (BulkLoadDone != null)
            {
                BulkLoadDone(this, new EventArgs());
            }
        }


        /*
				public void ImportQuestionsFromXdp(string path)
				{
					OnBulkLoadStarted();
					XmlProgressReader rdr;
					System.Collections.Stack subforms = new Stack();
					Question currentQuestion = null;

					try
					{
						using (FileStream fs = new FileStream(path, FileMode.Open))
						{
							rdr = new XmlProgressReader(fs);
							while (rdr.Read())
							{
								if (rdr.NodeType == XmlNodeType.Element && rdr.Name == "subform")
								{
									string subformName = rdr.GetAttribute("name");
									subforms.Push(subformName);
								}
								else if (rdr.NodeType == XmlNodeType.Element && rdr.Name == "field")
								{
									string hidden = rdr.GetAttribute("presence");
										rdr.MoveToAttribute("name");
										string name = rdr.Value;
										Question q = CreateQuestion();
										StringBuilder nameBuilder = new StringBuilder(subforms.Count);
								
										object[] arr = subforms.ToArray();
										for (int i = arr.Length-1; i >= 0; i--)
										{
											object o = arr[i];
											nameBuilder.Append(o.ToString());
											nameBuilder.Append(".");
										}
										nameBuilder.Append(name);
										q.Name = nameBuilder.ToString();
										q.Prompt = name;
									if (hidden == null || hidden != "hidden")
									{
										q.Visible.YesNoValue = false;
									}	
					
									currentQuestion = q;
						
								}
								else if (rdr.NodeType == XmlNodeType.EndElement && rdr.Name == "subform")
								{
									subforms.Pop();
								}
								else if (rdr.NodeType == XmlNodeType.Element && rdr.Name == "caption" && currentQuestion != null)
								{
									currentQuestion.Prompt = rdr.Value;
								}
							}
						}
					}
					finally
					{
						OnBulkLoadDone();
					}
				}
		*/

        public void ImportQuestionsFromXdp(string path)
        {
            OnBulkLoadStarted();
            //string xdpns = null;
            try
            {
                /*
				// Step 1: Open the file with an XmlTextReader to find the namespace of the first <template> node:
				XmlTextReader rdr = null;
				try
				{
					using (FileStream fs = new FileStream(path, FileMode.Open))
					{
						rdr = new XmlTextReader(fs);
						while (rdr.Read())
						{
							if (rdr.NodeType == XmlNodeType.Element && rdr.Name == "subform" && rdr.GetAttribute("xmlns") != null)
							{
								xdpns = rdr.GetAttribute("xmlns");
								break;
							}
						}
					}
			
					if (xdpns == null)
					{
						rdr.Close();

						using (FileStream fs = new FileStream(path, FileMode.Open))
						{
							rdr = new XmlTextReader(fs);
							while (rdr.Read())
							{
								if (rdr.NodeType == XmlNodeType.Element && rdr.Name == "template" && rdr.GetAttribute("xmlns") != null)
								{
									xdpns = rdr.GetAttribute("xmlns");
									break;
								}
							}
						}
						if (xdpns == null)
						{
							throw new Exception("The namespace of the <template> or <subform> nodes could not be determined.");
						}
					}
				}
				finally
				{
					if (rdr != null)
					{
						rdr.Close();
					}
				}
	*/

                // Step 2: Having determined the namespace we need, load the xml into a DOM so we can xpath it.
                XmlDocument doc = new XmlDocument();

                doc.Load(path);

                // Strip out all default namespaces.  This will put ev

                string xml = doc.InnerXml;
                Regex reg = new Regex(@"(xmlns=[\'\""].*?[\'\""])");
                xml = reg.Replace(xml, @"xmlns=""http://schemata.perfectus.net/xdpimporttempuri""");

                doc.InnerXml = xml;

                XmlNameTable nt = doc.NameTable;
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);
                nsmgr.AddNamespace("ns1", "http://schemata.perfectus.net/xdpimporttempuri");

                foreach (XmlNode n in doc.GetElementsByTagName("subform")) // SelectNodes("//ns1:subform", nsmgr))
                {
                    XmlNode pn = n.ParentNode;
                    XmlNode nn = n.Attributes["name"];
                    string qName = string.Empty;
                    if (nn != null)
                    {
                        qName = n.Attributes["name"].Value;
                    }
                    while (pn != null)
                    {
                        if (pn.Name == "subform")
                        {
                            if (pn.Attributes["name"] != null && pn.Attributes["name"].Value.Trim().Length > 0)
                            {
                                if (qName.Length > 0)
                                {
                                    qName = pn.Attributes["name"].Value + "." + qName;
                                }
                                else
                                {
                                    qName = pn.Attributes["name"].Value;
                                }
                            }
                        }
                        pn = pn.ParentNode;
                    }

                    //				foreach(XmlNode groupN in n.SelectNodes("ns1:exclGroup | ns1:area/ns1:exclGroup", nsmgr))
                    //				{
                    //					// Loop through the <field> tags - these contain the options for a radiolist.
                    //					ItemsItemCollection iiColl = new ItemsItemCollection();
                    //					foreach(XmlNode groupFieldN in groupN.SelectNodes("ns1:field", nsmgr))
                    //					{
                    //						ItemsItem ii = new ItemsItem();
                    //						
                    //					}
                    //
                    //				}

                    foreach (
                        XmlNode qn in n.SelectNodes("ns1:field[@name != ''] | ns1:area/ns1:field[@name != ''] ", nsmgr))
                    {
                        Question q = CreateQuestion();
                        q.DisplayWidth = 10;
                        if (qName.Trim().Length > 0)
                        {
                            q.Name = qName + "." + qn.Attributes["name"].Value;
                        }
                        else
                        {
                            q.Name = qn.Attributes["name"].Value;
                        }
                        XmlNode cn = qn.SelectSingleNode("ns1:caption/ns1:value", nsmgr);
                        if (cn != null && cn.InnerText.Trim().Length > 0)
                        {
                            //q.Prompt = cn.InnerText;
                            q.Prompt.TextValue = cn.InnerText;
                        }
                        else
                        {
                            //q.Prompt = qn.Attributes["name"].Value;
                            q.Prompt.TextValue = qn.Attributes["name"].Value;
                        }

                        if (qn.Attributes["presence"] != null && qn.Attributes["presence"].Value == "hidden")
                        {
                            q.Visible.YesNoValue = false;
                        }

                        if (qn.SelectSingleNode("ns1:ui/ns1:checkButton", nsmgr) != null)
                        {
                            q.DataType = PerfectusDataType.YesNo;
                        }
                        else if (qn.SelectSingleNode("ns1:ui/ns1:dateTimeEdit", nsmgr) != null)
                        {
                            q.DataType = PerfectusDataType.Date;
                        }
                        else if (qn.SelectSingleNode("ns1:ui/ns1:numericEdit", nsmgr) != null)
                        {
                            q.DataType = PerfectusDataType.Number;
                        }
                        else if (qn.SelectSingleNode("ns1:ui/ns1:textEdit[@multiLine = '1']", nsmgr) != null)
                        {
                            q.DataType = PerfectusDataType.Text;
                            q.DisplayType = QuestionDisplayType.TextArea;
                        }
                        else if (qn.SelectSingleNode("ns1:ui/ns1:choiceList", nsmgr) != null &&
                                 qn.SelectSingleNode("ns1:items", nsmgr) != null)
                        {
                            XmlNodeList itemNodes = qn.SelectNodes("ns1:items/*", nsmgr);

                            if (itemNodes != null && itemNodes.Count > 0)
                            {
                                // See what type of node the first item is.  This will determine the type for the whole question.
                                switch (itemNodes[0].Name)
                                {
                                    case "text":
                                        q.DataType = PerfectusDataType.Text;
                                        break;
                                    case "interger":
                                        q.DataType = PerfectusDataType.Number;
                                        break;
                                    default:
                                        q.DataType = PerfectusDataType.Text;
                                        break;
                                }

                                ItemsItemCollection iiColl = new ItemsItemCollection();
                                foreach (XmlNode iNode in itemNodes)
                                {
                                    ItemsItem ii = new ItemsItem();
                                    ii.Display = iNode.InnerText;
                                    ii.Value = iNode.InnerText;
                                    iiColl.Add(ii);
                                    q.Items = iiColl;
                                }
                            }

                            if (qn.SelectSingleNode("ns1:ui/ns1:choiceList[@open='always']", nsmgr) != null)
                            {
                                q.DisplayType = QuestionDisplayType.ListBox;
                            }
                            else
                            {
                                q.DisplayType = QuestionDisplayType.DropDownList;
                            }
                        }
                        else
                        {
                            q.DataType = PerfectusDataType.Text;
                            q.DisplayType = QuestionDisplayType.TextBox;
                        }

                        XmlNode vn = qn.SelectSingleNode("ns1:value", nsmgr);
                        if (vn != null)
                        {
                            q.DefaultAnswer.TextValue = vn.InnerText;
                        }
                    }
                }
            }
            finally
            {
                OnBulkLoadDone();
            }
        }

        private static bool CompareHashes(byte[] h1, byte[] h2)
        {
            int i = 0;
            if (h1.Length == 0 || h2.Length == 0)
            {
                return false;
            }
            while ((i < h1.Length) && h1[i] == h2[i])
            {
                i++;
            }
            if (i == h1.Length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void UpdatePackageItem(PackageItem packageItem)
        {
            if (packageItem.GetType() == typeof (Question))
            {
                UpdateQuestion((Question) packageItem);
            }
        }

        private void UpdateQuestion(Question sharedQuestion)
        {
            // Find the given question
            Question question = QuestionByGuid[sharedQuestion.UniqueIdentifier];

            // Update it
            Questions[Questions.IndexOf(question)].Update(sharedQuestion);
        }

        public void ImportQuestionsFromPackage(string path)
        {
            // Create a file stream object to use
            FileStream fs = null;

            OnBulkLoadStarted( );
            string taskName = string.Empty;
            try
            {
                // Open package passing in our filestream. We only need read access, and other processes can only have either read or write access..
                Package p = ReadFromDisc( path, ref fs, FileAccess.Read, FileShare.ReadWrite );

                // We need to remember the current package folders, to check against before we add folders below.
                ArrayList OriginalQuestionFolders = new ArrayList();
                foreach (QuestionFolder qf in questionFolders)
                {
                    FolderCollection qfc = qf.GetAllFolders();

                    foreach (QuestionFolder qf2 in qfc)
                    {
                        OriginalQuestionFolders.Add(qf2.UniqueIdentifier.ToString().ToLower());
                    }
                }

                // Add folders.
                foreach (QuestionFolder qf in p.questionFolders)
                {
                    FolderCollection qfc = qf.GetAllFolders();
                        // gets all sub folders and the current folder into one collection.

                    foreach (QuestionFolder qf2 in qfc)
                    {
                        if (! OriginalQuestionFolders.Contains(qf2.UniqueIdentifier.ToString().ToLower()))
                        {
                            if (qf2.ParentFolder == null)
                            {
                                questionFolders.Add(qf2);
                            }

                            AddNewFolder(qf2, true);
                        }
                    }
                }

                // Add questions
                foreach (Question q in p.Questions)
                {
                    if (!questionDictionary.ContainsKey(q.UniqueIdentifier))
                    {
                        AddNewQuestion(q, true);
                    }
                }
            }
            finally
            {
                // Ensure file stream is closed.
                if( fs != null )
                { fs.Flush( ); fs.Close( ); }

                OnTaskProgress(taskName, 101);
                OnBulkLoadDone();
            }
        }

        #region ISerializable Members


        public Package(SerializationInfo info, StreamingContext context)
        {
            UniqueIdentifier = (Guid) info.GetValue("uniqueIdentifier", typeof (Guid));
            currentFilePath = info.GetString("currentFilePath");
            creationDateTime = info.GetDateTime("creationDateTime");
            modificationDateTime = info.GetDateTime("modificationDateTime");
            publishedDateTime = info.GetDateTime("publishedDateTime");
            schemaVersionMajor = info.GetInt32("schemaVersionMajor");
            schemaVersionMinor = info.GetInt32("schemaVersionMinor");
            publishedBy = info.GetString("publishedBy");
            createdBy = info.GetString("createdBy");
            modifiedBy = info.GetString("modifiedBy");
            description = info.GetString("description");
            try { 
                versionXml = info.GetString("versionXml"); }
            catch { }
            interviews = (InterviewCollection) info.GetValue("interviews", typeof (InterviewCollection));
            templates = (TemplateDocumentCollection) info.GetValue("templates", typeof (TemplateDocumentCollection));
            try {
                attachments = (AttachmentCollection)info.GetValue( "attachments", typeof( AttachmentCollection ) );
            }
            catch {
                attachments = new AttachmentCollection( 0 );
            }
            questions = (QuestionCollection) info.GetValue("questions", typeof (QuestionCollection));
            outcomes = (OutcomeCollection) info.GetValue("outcomes", typeof (OutcomeCollection));
            pFunctions = (PFunctionCollection) info.GetValue("pFunctions", typeof (PFunctionCollection));
            dataSources = (DataSourceCollection) info.GetValue("dataSources", typeof (DataSourceCollection));
            servers = (ServerCollection) info.GetValue("servers", typeof (ServerCollection));
            name = info.GetString("name");
            //pf-3030
            uniqueIdentifier = (Guid)info.GetValue("uniqueIdentifier", typeof(Guid));

            questionDictionary = (QuestionDictionary) info.GetValue("questionDictionary", typeof (QuestionDictionary));
            interviewPageDictionary =
                (InterviewPageDictionary) info.GetValue("interviewPageDictionary", typeof (InterviewPageDictionary));
            webReferencesDictionary =
                (WebReferenceDictionary) info.GetValue("webReferencesDictionary", typeof (WebReferenceDictionary));

            try
            {
                namesTable = (Hashtable)info.GetValue("namesTable", typeof(Hashtable));
            }
            catch
            {
                namesTable = new Hashtable();
            }

            try
            {
                templateItemDictionary =
                    (TemplateItemDictionary) info.GetValue("templateItemDictionary", typeof (TemplateItemDictionary));
            }
            catch
            {
                templateItemDictionary = new TemplateItemDictionary();
            }
            try
            {
                stringBindingItemDictionary =
                    (StringBindingItemDictionary)
                    info.GetValue("stringBindingItemDictionary", typeof (StringBindingItemDictionary));
            }
            catch
            {
                stringBindingItemDictionary = new StringBindingItemDictionary();
            }
            try
            {
                instanceReference = info.GetString("instanceReference");
            }
            catch
            {
            }
            try
            {
                simpleOutcomes =
                    (SimpleOutcomeCollection) info.GetValue("simpleOutcomes", typeof (SimpleOutcomeCollection));
            }
            catch
            {
                simpleOutcomes = new SimpleOutcomeCollection();
            }
            try
            {
                extensionMapping =
                    (DisplayTypeExtensionInfoDictionary)
                    info.GetValue("extensionMapping", typeof (DisplayTypeExtensionInfoDictionary));
            }
            catch
            {
                extensionMapping = new DisplayTypeExtensionInfoDictionary();
            }
            try
            {
                formatStringExtensionMapping =
                    (FormatStringExtensionInfoDictionary)
                    info.GetValue("formatStringExtensionMapping", typeof (FormatStringExtensionInfoDictionary));
            }
            catch
            {
                formatStringExtensionMapping = new FormatStringExtensionInfoDictionary();
            }
            try
            {
                notes = info.GetString("notes");
            }
            catch
            {
            }
            try
            {
                simpleOutcomeFolders =
                    (SimpleOutcomeFolderCollection)
                    info.GetValue("simpleOutcomeFolders", typeof (SimpleOutcomeFolderCollection));
            }
            catch
            {
                simpleOutcomeFolders = new SimpleOutcomeFolderCollection();
            }
            try
            {
                outcomeFolders =
                    (OutcomeFolderCollection) info.GetValue("outcomeFolders", typeof (OutcomeFolderCollection));
            }
            catch
            {
                outcomeFolders = new OutcomeFolderCollection();
            }
            try
            {
                attachmentFolders =
                    (AttachmentFolderCollection)info.GetValue("attachmentFolders", typeof(AttachmentFolderCollection));
            }
            catch
            {
                attachmentFolders = new AttachmentFolderCollection();
            }
            try
            {
                questionFolders =
                    (QuestionFolderCollection) info.GetValue("questionFolders", typeof (QuestionFolderCollection));
            }
            catch
            {
                questionFolders = new QuestionFolderCollection();
            }
            try
            {
                pFunctionFolders =
                    (PFunctionFolderCollection) info.GetValue("pFunctionFolders", typeof (PFunctionFolderCollection));
            }
            catch
            {
                pFunctionFolders = new PFunctionFolderCollection();
            }
            try
            {
                mirrorDocType = info.GetString("mirrorDocType");
                if (mirrorDocType == null)
                {
                    mirrorDocType = "Master";
                }
            }
            catch
            {
                mirrorDocType = "Master";
            }
            try
            {
                makeMirror = (YesNoQueryValue) info.GetValue("makeMirror", typeof (YesNoQueryValue));
            }
            catch
            {
                makeMirror = new YesNoQueryValue();
                makeMirror.YesNoValue = false;
            }
            try
            {
                tempOfflineId = (Guid) info.GetValue("tempOfflineId", typeof (Guid));
            }
            catch
            {
            }
        }


        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("uniqueIdentifier", uniqueIdentifier);
            info.AddValue("currentFilePath", currentFilePath);
            info.AddValue("creationDateTime", creationDateTime);
            info.AddValue("modificationDateTime", modificationDateTime);
            info.AddValue("publishedDateTime", publishedDateTime);
            info.AddValue("schemaVersionMajor", schemaVersionMajor);
            info.AddValue("schemaVersionMinor", schemaVersionMinor);
            info.AddValue("createdBy", createdBy);
            info.AddValue("modifiedBy", modifiedBy);
            info.AddValue("publishedBy", publishedBy);
            info.AddValue("description", description);
            info.AddValue("versionXml", versionXml);
            info.AddValue("interviews", interviews);
            info.AddValue("templates", templates);
            info.AddValue("attachments", attachments);
            info.AddValue("questions", questions);
            info.AddValue("outcomes", outcomes);
            info.AddValue("pFunctions", pFunctions);
            info.AddValue("dataSources", dataSources);
            info.AddValue("servers", servers);
            info.AddValue("name", name);

            info.AddValue("questionDictionary", questionDictionary);
            info.AddValue("interviewPageDictionary", interviewPageDictionary);
            info.AddValue("webReferencesDictionary", webReferencesDictionary);
            info.AddValue("templateItemDictionary", templateItemDictionary);
            info.AddValue("stringBindingItemDictionary", stringBindingItemDictionary);
            info.AddValue("namesTable", namesTable);
            info.AddValue("instanceReference", instanceReference);

            info.AddValue("simpleOutcomes", simpleOutcomes);
            info.AddValue("extensionMapping", extensionMapping);
            info.AddValue("formatStringExtensionMapping", formatStringExtensionMapping);

            info.AddValue("notes", notes);
            info.AddValue("simpleOutcomeFolders", simpleOutcomeFolders);
            info.AddValue("outcomeFolders", outcomeFolders);
            info.AddValue("attachmentFolders", attachmentFolders);
            info.AddValue("questionFolders", questionFolders);
            info.AddValue("pFunctionFolders", pFunctionFolders);
            info.AddValue("mirrorDocType", mirrorDocType);
            info.AddValue("makeMirror", makeMirror);
            info.AddValue("tempOfflineId", tempOfflineId);
        }

        #endregion

        public string EnsureUniqueName(string proposedName, string itemId)
        {
            bool existItem = false;
            if (namesTable == null || namesTable.Count == 0)
            {
                namesTable = new Hashtable(50);
                namesTable.Add(proposedName.ToUpper(CultureInfo.InvariantCulture).Trim(), itemId);
                return proposedName;
            }

            if (namesTable != null && proposedName != null)
            {
                if (namesTable.Count > 0 &&
                    namesTable.ContainsKey(proposedName.ToUpper(CultureInfo.InvariantCulture).Trim()))
                {
                    string baseName = proposedName;
                    int i = 1;
                    while (namesTable.ContainsKey(proposedName.ToUpper(CultureInfo.InvariantCulture).Trim()))
                    {
                        object itemValue = namesTable[proposedName.ToUpper(CultureInfo.InvariantCulture).Trim()];
                        if (itemValue != null && itemValue.ToString() != itemId)
                        {
                            proposedName = string.Format("{0}_{1}", baseName, i);
                            i++;
                        }
                        else
                        {
                            existItem = true;
                            break;
                        }
                    }
                }

                if (!existItem)
                {
                    namesTable.Add(proposedName.ToUpper(CultureInfo.InvariantCulture).Trim(), itemId);
                }
            }
            return proposedName;
        }

        public void FreeName(string oldName)
        {
            if (namesTable != null && namesTable.Count > 0)
            {
                if (namesTable.ContainsKey(oldName.ToUpper(CultureInfo.InvariantCulture).Trim()))
                {
                    namesTable.Remove(oldName.ToUpper(CultureInfo.InvariantCulture).Trim());
                }
            }
        }


        public bool IsOkayToDelete(PackageItem item, out string Message)
        {
            bool isOkay = true;
            Message = null;
            StringBuilder messageBuilder = new StringBuilder();

            messageBuilder.Append(
                string.Format("\"{0}\" {1}\n", item.Name,
                              ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                  "Perfectus.Common.PackageObjects.Package.DeleteErrorMsg")));

            isOkay = FindIfDependentsExist(item, ref messageBuilder);


            if (isOkay)
            {
                Message = null;
                return true;
            }
            else
            {
                Message = messageBuilder.ToString();
                return false;
            }
        }

        public bool FindIfDependentsExist(PackageItem item, ref StringBuilder messageBuilder)
        {
            bool noDependencies = true;

            foreach (Interview iv in interviews)
            {
                if (iv.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.StartPageDeleteErrorMsg"), iv.Name));
                }

                if (iv.Reference != null && iv.Reference.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferencepropertyDeleteErrorMsg"),
                                      iv.Name));
                }


                foreach (InterviewPage pg in iv.Pages)
                {
                    if (pg is InternalPage)
                    {
                        string errorMessage = String.Empty;
                        if (((InternalPage)pg).ContainsReferenceTo(item, out errorMessage))
                        {
                            noDependencies = false;
                            messageBuilder.Append(
                                string.Format("\t{0} \"{1}\"\n",
                                              ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                  GetString("Perfectus.Common.PackageObjects.Package.PageDeleteErrorMsg"),
                                              pg.Name));
                        }
                    }

                    if (pg.NextPage.ContainsReferenceTo(item))
                    {
                        noDependencies = false;
                        messageBuilder.Append(
                            string.Format("\t{0} \"{1}\"\n",
                                          ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                              "Perfectus.Common.PackageObjects.Package.ReferenceNextPagepropertyDeleteErrorMsg"),
                                          pg.Name));
                    }
                    if (pg.Visible.ContainsReferenceTo(item))
                    {
                        noDependencies = false;
                        messageBuilder.Append(
                            string.Format("\t{0} \"{1}\"\n",
                                          ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                              "Perfectus.Common.PackageObjects.Package.ReferenceVisiblepropertyDeleteErrorMsg"),
                                          pg.Name));
                    }
                }
            }

            foreach (Question q in questions)
            {
                if (q.Mandatory.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceMandatorypropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Enabled.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceEnabledpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Visible.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionVisiblepropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.MirrorMapping.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionMirrorpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Prompt.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionPromptpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.DefaultAnswer.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionDefaultAnswerpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Hint.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionHintpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Example.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionExamplepropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Prefix.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionPrefixpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.Suffix.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionSuffixpropertyDeleteErrorMsg"),
                                      q.Name));
                }
                if (q.ValidationErrorMessage.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceQuestionValidationErrorMessagepropertyDeleteErrorMsg"),
                                      q.Name));
                }
            }


            foreach (Outcome o in outcomes)
            {
                if (o.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        string.Format("\t{0} \"{1}\"\n",
                                      ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                          "Perfectus.Common.PackageObjects.Package.ReferenceOutcomeDeleteErrorMsg"),
                                      o.Name));
                }
            }

            foreach (SimpleOutcome so in simpleOutcomes)
            {
                if (so.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        (string.Format("\t{0} \"{1}\"\n",
                                       ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                           "Perfectus.Common.PackageObjects.Package.ReferenceConditionalTextDeleteErrorMsg"),
                                       so.Name)));
                }
            }

            foreach (PFunction f in pFunctions)
            {
                if (f.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    string mystring =
                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                            "Perfectus.Common.PackageObjects.Package.ReferenceFunctionDeleteErrorMsg");
                    messageBuilder.Append(string.Format("\t{0} \"{1}\"\n", mystring, f.Name));
                }
            }

            foreach (TemplateDocument t in Templates)
            {
                if (t is WordTemplateDocument2)
                {
                    foreach (WordDocumentProperty dp in ((WordTemplateDocument2)t).DocumentProperties)
                    {
                        if (dp.ContainsReferenceTo(item))
                        {
                            noDependencies = false;
                            messageBuilder.Append(
                                string.Format("\t{0} \"{1}\"\n",
                                              ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                  GetString(
                                                  "Perfectus.Common.PackageObjects.Package.TemplateDocumentPropertyDeleteErrorMsg"),
                                              t.Name));
                        }
                    }
                }

               
            }

            // Properties 'FileName' and 'Condition' of Documents and Attachments
            foreach (IDocument doc in DocumentsAndAttachmentsInUse)
            {
                if (doc.Condition != null &&
                    doc.Condition.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        (string.Format("\t{0} \"{1}\"\n",
                                       ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                           "Perfectus.Common.PackageObjects.Package.ConditionIDocumentDeleteErrorMsg"),
                                       doc.Name)));
                }
                if (doc.FileName != null &&
                    doc.FileName.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        (string.Format("\t{0} \"{1}\"\n",
                                       ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                           "Perfectus.Common.PackageObjects.Package.FileNameIDocumentDeleteErrorMsg"),
                                       doc.Name)));
                }
            }
            foreach (IAttachment attachment in Attachments)
            {
                if (attachment.Condition != null &&
                    attachment.Condition.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        (string.Format("\t{0} \"{1}\"\n",
                                       ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                           "Perfectus.Common.PackageObjects.Package.ConditionIDocumentDeleteErrorMsg"),
                                       attachment.Name)));
                }
                if (attachment.FileName != null &&
                    attachment.FileName.ContainsReferenceTo(item))
                {
                    noDependencies = false;
                    messageBuilder.Append(
                        (string.Format("\t{0} \"{1}\"\n",
                                       ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                           "Perfectus.Common.PackageObjects.Package.FileNameIDocumentDeleteErrorMsg"),
                                       attachment.Name)));
                }
            }


            foreach (Server s in servers)
                foreach (Rule2 r in s.Rule2)
                {
                    if (r.DoRuleAtFinish.ContainsReferenceTo(item) )
                    {
                        noDependencies = false;
                        messageBuilder.Append(
                            (string.Format("\t{0} \"{1}\"\n",
                                           ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                               "Perfectus.Common.PackageObjects.Package.DoRuleAtFinishRuleDeleteErrorMsg"),
                                           r.Name)));
                    }

                    if (r.DeliveryRule.ContainsReferenceTo(item))
                    {
                        noDependencies = false;
                        messageBuilder.Append(
                            (string.Format("\t{0} \"{1}\"\n",
                                           ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                               "Perfectus.Common.PackageObjects.Package.DeliveryRuleRuleDeleteErrorMsg"),
                                           r.Name)));
                    }


                    foreach (Delivery d in r.Deliveries)
                        if (d.Condition.ContainsReferenceTo(item))
                        {
                            noDependencies = false;
                            messageBuilder.Append(
                                (string.Format("\t{0} \"{1}\"\n",
                                               ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                                   "Perfectus.Common.PackageObjects.Package.ConditionDeliveryDeleteErrorMsg"),
                                               d.Name)));

                        }
                }
                

            // Template used by documents
            if (item is TemplateDocument)
            {
                foreach (Server s in servers)
                    foreach (Rule2 r in s.Rule2)
                        foreach (Delivery d in r.Deliveries)
                            foreach (IDocument idoc in d.Documents)
                            {
                                if (!(idoc is Document))
                                    continue;

                                Document theDoc = idoc as Document;
                                if (theDoc.HasTemplate &&
                                    theDoc.Template.UniqueIdentifier == item.UniqueIdentifier)
                                {
                                    noDependencies = false;
                                    messageBuilder.Append(
                                        string.Format("\t{0}\n",
                                                      string.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                                          GetString(
                                                          "Perfectus.Common.PackageObjects.Package.TemplateDocumentDeleteErrorMsg"),
                                                      theDoc.Name)));

                                }
                            }
            }

            // Question that is actually used as attachment
            if (item is Question)
            {
                Question question = item as Question;
                if ( question.DataType == PerfectusDataType.Attachment )
                    foreach (AttachmentBase attachment in Attachments)
                        if (attachment is AttachmentDynamic &&
                        ((AttachmentDynamic)attachment).GetPayload() == question.UniqueIdentifier)
                        {
                            noDependencies = false;

                            string localAttachmentsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.AttachmentsNodeName");
                            string message = String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Attachment.CannotDeleteAttachmentQuestion"), localAttachmentsNodeName);
                            messageBuilder.Append(message);
                        }

            }

            // When deleting an Attachment from the tree, then ensure that it is not used in any
            // delivery
            if (item is IAttachment && ((IAttachment)item).ParentDelivery == null)
                foreach (Server s in servers)
                    foreach (Rule2 r in s.Rule2)
                        foreach (Delivery d in r.Deliveries)
                            foreach (IDocument idoc in d.Documents)
                            {
                                if (!(idoc is IAttachment))
                                    continue;

                                IAttachment iatt = item as IAttachment;

                                if (iatt.UniqueIdentifier == ((IAttachment)idoc).OriginalUniqueueIdentifier)
                                {
                                    noDependencies = false;
                                    string mystr =
                                        ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                                            "Perfectus.Common.PackageObjects.Package.ReferenceAttachmentDeleteErrorMsg");
                                    mystr = mystr.Replace("{0}", "\"" + d.Name + "\"");
                                    mystr = mystr.Replace("{1}", "\"" + r.Name + "\"");
                                    messageBuilder.Append(string.Format("\t{0} \"{1}\"\n", mystr, s.Name));
                                }
                            }

            return noDependencies;
        }


        public bool IsOkayToFF(PackageItem item)
        {
            bool isOkay = true;

            foreach (Interview iv in interviews)
            {
                if (iv.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
                if (iv.Reference != null && iv.Reference.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
            }

            foreach (Question q in questions)
            {
                if (q.Mandatory.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
                if (q.Enabled.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
                if (q.Visible.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
            }

            foreach (Outcome o in outcomes)
            {
                if (o.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
            }

            foreach (SimpleOutcome so in simpleOutcomes)
            {
                if (so.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
            }

            foreach (PFunction f in pFunctions)
            {
                if (f.ContainsReferenceTo(item))
                {
                    isOkay = false;
                }
            }


            if (isOkay)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void RemoveDocumentFromDelivery(IDocument td, Delivery d)
        {
            d.RemoveDocument (td);
        }

        public ExpressionInfoCollection GetAllExpressions()
        {
            ExpressionInfoCollection coll = new ExpressionInfoCollection();

            // Outcomes - Subqueries
            foreach (Outcome o in outcomes)
            {
                try
                {
                    if (o.Definition != null)
                    {
                        AddSubqueries(o.Definition, coll, o);
                    }
                }
                catch
                {
                }
            }

            // Simple Outcomes
            string localDefinition =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.DefinitionPropertyName");
            string localVisible =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.VisiblePropertyName");
            string localMandatory =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.MandatoryPropertyName");
            string localEnabled =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.EnabledPropertyName");
            string localNextPage =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.NextPagePropertyName");
            string localDeliveryRule =
                ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(
                    "Perfectus.Common.PackageObjects.Package.DeliveryRulePropertyName");

            foreach (SimpleOutcome so in simpleOutcomes)
            {
                try
                {
                    if (so.Definition != null && so.Definition.QueryExpression.ToPreviewString() != null)
                    {
                        coll.Add(new ExpressionInfo(so.Definition.QueryExpression, so, localDefinition));
                    }
                }
                catch
                {
                }
            }

            // Questions - Visible, Mandatory, Enabled
            foreach (Question q in questions)
            {
                try
                {
                    if (q.Visible != null && q.Visible.ValueType == YesNoQueryValue.YesNoQueryValueType.Query &&
                        q.Visible.QueryValue != null && q.Visible.QueryValue.QueryExpression.ToPreviewString() != null)
                    {
                        coll.Add(new ExpressionInfo(q.Visible.QueryValue.QueryExpression, q, localVisible));
                    }
                    if (q.Mandatory != null && q.Mandatory.ValueType == YesNoQueryValue.YesNoQueryValueType.Query &&
                        q.Mandatory.QueryValue != null &&
                        q.Mandatory.QueryValue.QueryExpression.ToPreviewString() != null)
                    {
                        coll.Add(new ExpressionInfo(q.Mandatory.QueryValue.QueryExpression, q, localMandatory));
                    }
                    if (q.Enabled != null && q.Enabled.ValueType == YesNoQueryValue.YesNoQueryValueType.Query &&
                        q.Enabled.QueryValue != null && q.Enabled.QueryValue.QueryExpression.ToPreviewString() != null)
                    {
                        coll.Add(new ExpressionInfo(q.Enabled.QueryValue.QueryExpression, q, localEnabled));
                    }
                }
                catch
                {
                }
            }

            // Pages - Visible, NextPage
            foreach (Interview iv in interviews)
            {
                try
                {
                    foreach (InterviewPage pg in iv.Pages)
                    {
                        if (pg.Visible != null && pg.Visible.ValueType == YesNoQueryValue.YesNoQueryValueType.Query &&
                            pg.Visible.QueryValue != null &&
                            pg.Visible.QueryValue.QueryExpression.ToPreviewString() != null)
                        {
                            coll.Add(new ExpressionInfo(pg.Visible.QueryValue.QueryExpression, pg, localVisible));
                        }

                        if (pg.NextPage != null && pg.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query &&
                            pg.NextPage.QueryValue != null &&
                            pg.NextPage.QueryValue.QueryExpression.ToPreviewString() != null)
                        {
                            coll.Add(new ExpressionInfo(pg.NextPage.QueryValue.QueryExpression, pg, localNextPage));
                        }
                    }
                }
                catch
                {
                }
            }

            // Rules - FireRule
            foreach (Server s in servers)
            {
                try
                {
                    foreach (Rule r in s.Rules)
                    {
                        if (r.DeliveryRule != null &&
                            r.DeliveryRule.ValueType == YesNoQueryValue.YesNoQueryValueType.Query &&
                            r.DeliveryRule.QueryValue != null &&
                            r.DeliveryRule.QueryValue.QueryExpression.ToPreviewString() != null)
                        {
                            coll.Add(new ExpressionInfo(r.DeliveryRule.QueryValue.QueryExpression, r, localDeliveryRule));
                        }
                    }
                }
                catch
                {
                }
            }
            return coll;
        }

        private static void AddSubqueries(Query query, ExpressionInfoCollection eic, PackageItem parentItem)
        {
            if (query != null && query.QueryExpression != null && query.QueryExpression.ToPreviewString() != null)
            {
                //TODO: Refactor out property name to caller.
                string localDefinition = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                                            GetString("Perfectus.Common.PackageObjects.Package.DefinitionPropertyName");
                eic.Add(new ExpressionInfo(query.QueryExpression, parentItem, localDefinition));
                if (query.ActionIfTrue.SubQuery != null)
                {
                    AddSubqueries(query.ActionIfTrue.SubQuery, eic, parentItem);
                }
                if (query.ActionIfFalse.SubQuery != null)
                {
                    AddSubqueries(query.ActionIfFalse.SubQuery, eic, parentItem);
                }
            }
        }

        public void ClearForPreview(Guid pageToKeep)
        {
            for (int idx = templates.Count - 1; idx >= 0; idx--)
            {
                DeleteItem(templates[idx], true);
            }

            for (int idx = servers.Count - 1; idx >= 0; idx--)
            {
                for (int idx2 = servers[idx].Rules.Count - 1; idx2 >= 0; idx2--)
                {
                    DeleteItem(servers[idx].Rules[idx2], true);
                }

                DeleteItem(servers[idx], true);
            }

            for (int idx = outcomes.Count - 1; idx >= 0; idx--)
            {
                DeleteItem(outcomes[idx], true);
            }

            for (int idx = simpleOutcomes.Count - 1; idx >= 0; idx--)
            {
                DeleteItem(simpleOutcomes[idx], true);
            }

            for (int idx = dataSources.Count - 1; idx >= 0; idx--)
            {
                DeleteItem(dataSources[idx], true);
            }

            foreach (Interview iv in interviews)
            {
                for (int idx = iv.Pages.Count - 1; idx >= 0; idx--)
                {
                    if (iv.Pages[idx].UniqueIdentifier != pageToKeep)
                        DeleteItem(iv.Pages[idx], true);
                }
            }
            templateItemDictionary.Clear();
            namesTable.Clear();
            templateItemDictionary = null;
            namesTable = null;
        }

        public void ReindexQuestions()
        {
            questionDictionary.Clear();
            stringBindingItemDictionary.Clear();

            foreach (Question q in questions)
            {
                questionDictionary.Add(q.UniqueIdentifier, q);
                stringBindingItemDictionary.Add(q.UniqueIdentifier, q);
            }

            foreach (PFunction f in pFunctions)
            {
                stringBindingItemDictionary.Add(f.UniqueIdentifier, f);
            }
        }

        /// <summary>
        ///		Update this package with all the latest versions of any linked shared library items, including
        ///		the addition of any new items that are dependent on those latest shared library versions.
        /// </summary>
        /// <returns>
        ///		A string that includes an messages that arise from the processing of getting the latest versions.
        /// </returns>
        public string UpdateAllLinkedSharedLibraryItems(SharedLibraryProxy sharedLibraryProxy)
        {
            // Shared Library non-connectable error messages
            ArrayList unConnectedSharedLibraryItems = new ArrayList();

            // Update all functions
            UpdateLinkedSharedLibraryFunctions(sharedLibraryProxy, unConnectedSharedLibraryItems);

            // Update all interview pages
            UpdateLinkedSharedLibraryInterviewPages(sharedLibraryProxy, unConnectedSharedLibraryItems);

            // Update all outcomes
            UpdateLinkedSharedLibraryOutcomes(sharedLibraryProxy, unConnectedSharedLibraryItems);

            // Update all questions
            UpdateLinkedSharedLibraryQuestions(sharedLibraryProxy, unConnectedSharedLibraryItems);

            // Update all simple outcomes
            UpdateLinkedSharedLibrarySimpleOutcomes(sharedLibraryProxy, unConnectedSharedLibraryItems);

            // Update all templates
            UpdateLinkedSharedLibraryTemplates(sharedLibraryProxy, unConnectedSharedLibraryItems);

            if (unConnectedSharedLibraryItems.Count == 0)
            {
                return string.Empty;
            }
            else
            {
                StringBuilder unConnectedItemNames = new StringBuilder();

                foreach (object item in unConnectedSharedLibraryItems)
                {
                    unConnectedItemNames.Append(((LibraryItem) item).Name + "\n");
                }

                return unConnectedItemNames.ToString();
            }
        }

        private void UpdateLinkedSharedLibraryFunctions(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            // Functions
            foreach (PFunction function in Functions)
            {
                if (function.LibraryUniqueIdentifier == Guid.Empty || !function.Linked)
                    continue;
                try
                {
                    PFunction functionToUpdate = (PFunction)GetLatestSharedLibraryItem(function, sharedLibraryProxy);
                    LibraryItem packageItem = GetLibraryItemByName(functionToUpdate);
                    if (packageItem != null &&
                        functionToUpdate.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                        continue;


                    // Handle the dependents
                    DrillDownDependentItems(functionToUpdate, sharedLibraryProxy, functionToUpdate.DependentObject);

                    // Update the specific function type
                    if (functionToUpdate != null && function is ArithmeticPFunction)
                    {
                        ((ArithmeticPFunction) function).Update((ArithmeticPFunction) functionToUpdate);

                        if (LibraryItemUpdated != null)
                        {
                            LibraryItemUpdated(this, new LibraryItemEventArgs(function));
                        }
                    }
                    else if (functionToUpdate != null)
                    {
                        ((DatePFunction) function).Update((DatePFunction) functionToUpdate);

                        if (LibraryItemUpdated != null)
                        { 
                            LibraryItemUpdated(this, new LibraryItemEventArgs(function));
                        }
                    }
                }
                catch
                {
                    function.Linked = false;
                    unConnectedSharedLibraryItems.Add(function);
                }
            }
        }

        private void UpdateLinkedSharedLibraryInterviewPages(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            InterviewPage pageToUpdate = null;

            // Interview Pages
            foreach (Interview interview in Interviews)
            {
                foreach (InterviewPage page in interview.Pages)
                {
                    if (page.LibraryUniqueIdentifier == Guid.Empty || !page.Linked)
                        continue;
                    try
                    {
                        pageToUpdate = (InterviewPage) GetLatestSharedLibraryItem(page, sharedLibraryProxy);

                        // Handle the dependents
                        DrillDownDependentItems(pageToUpdate, sharedLibraryProxy, pageToUpdate.DependentObject);

                        if (pageToUpdate != null && page is InternalPage)
                            ((InternalPage) page).Update((InternalPage) pageToUpdate);
                    }
                    catch
                    {
                        page.Linked = false;
                        unConnectedSharedLibraryItems.Add(page);
                    }
                }
            }
        }

        private void UpdateLinkedSharedLibraryOutcomes(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            Outcome outcomeToUpdate;

            // Outcomes
            foreach (Outcome outcome in Outcomes)
            {
                if (outcome.LibraryUniqueIdentifier == Guid.Empty || !outcome.Linked)
                    continue;

                try
                {
                    outcomeToUpdate = (Outcome)GetLatestSharedLibraryItem(outcome, sharedLibraryProxy);

                    LibraryItem packageItem = GetLibraryItemByName(outcomeToUpdate);
                    if (packageItem != null &&
                        outcomeToUpdate.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                        continue;

                    // Handle the dependents
                    DrillDownDependentItems(outcomeToUpdate, sharedLibraryProxy, outcomeToUpdate.DependentObject);

                    if (outcomeToUpdate != null)
                        outcome.Update(outcomeToUpdate);
                }
                catch
                {
                    outcome.Linked = false;
                    unConnectedSharedLibraryItems.Add(outcome);
                }
            }
        }

        private void UpdateLinkedSharedLibraryQuestions(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            Question questionToUpdate;

            // Questions
            for ( int index = 0; index < Questions.Count; index++)
            //foreach (Question question in Questions)
            {
                Question question = Questions[index];
                if (question.LibraryUniqueIdentifier == Guid.Empty || !question.Linked)
                    continue;
                try
                {
                    questionToUpdate = (Question)GetLatestSharedLibraryItem(question, sharedLibraryProxy);

                    LibraryItem packageItem =  GetLibraryItemByName(questionToUpdate);
                    if (packageItem != null &&
                        questionToUpdate.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                    continue;


                    // Handle the dependents
                    DrillDownDependentItems(questionToUpdate, sharedLibraryProxy, questionToUpdate.DependentObject);

                    if (questionToUpdate != null)
                        question.Update(questionToUpdate);
                }
                catch
                {
                    question.Linked = false;
                    unConnectedSharedLibraryItems.Add(question);
                }
            }
        }

        private void UpdateLinkedSharedLibrarySimpleOutcomes(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            SimpleOutcome simpleOutcomeToUpdate = null;

            // Simple Outcomes
            foreach (SimpleOutcome simpleOutcome in SimpleOutcomes)
            {
                if (simpleOutcome.LibraryUniqueIdentifier == Guid.Empty || !simpleOutcome.Linked)
                    continue;
                try
                {
                    simpleOutcomeToUpdate = (SimpleOutcome)GetLatestSharedLibraryItem(simpleOutcome, sharedLibraryProxy);

                    LibraryItem packageItem = GetLibraryItemByName(simpleOutcomeToUpdate);
                    if (packageItem != null &&
                        simpleOutcomeToUpdate.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                        continue;

                    // Handle the dependents
                    DrillDownDependentItems(simpleOutcomeToUpdate, sharedLibraryProxy, simpleOutcomeToUpdate.DependentObject);

                    if (simpleOutcomeToUpdate != null)
                        simpleOutcome.Update(simpleOutcomeToUpdate);
                }
                catch
                {
                    simpleOutcome.Linked = false;
                    unConnectedSharedLibraryItems.Add(simpleOutcome);
                }
            }
        }

        private void UpdateLinkedSharedLibraryTemplates(SharedLibraryProxy sharedLibraryProxy, ArrayList unConnectedSharedLibraryItems)
        {
            TemplateDocument templateToUpdate = null;

            // Templates
            foreach (TemplateDocument template in Templates)
            {
                if (template.LibraryUniqueIdentifier == Guid.Empty || !template.Linked)
                    continue;
                try
                {
                    templateToUpdate = (TemplateDocument)GetLatestSharedLibraryItem(template, sharedLibraryProxy);

                    LibraryItem packageItem = GetLibraryItemByName(templateToUpdate);
                    if (packageItem != null &&
                        templateToUpdate.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                        continue;


                    // Handle the dependents
                    DrillDownDependentItems(templateToUpdate, sharedLibraryProxy, templateToUpdate.DependentObject);

                    if (templateToUpdate != null)
                    {
                        ((WordTemplateDocument2) template).Update((WordTemplateDocument2) templateToUpdate);
                    }
                }
                catch
                {
                    template.Linked = false;
                    unConnectedSharedLibraryItems.Add(template);
                }
            }
        }

        #region create items

        /// <summary>
        ///		Creates a new ArithmeticPFunction and updates it to mirror that of the shared library item it is representing
        /// </summary>
        /// <param name="libraryItem">The shared library item to be created in the package.</param>
        public void AddLibraryItem(LibraryItem libraryItem, SharedLibraryProxy sharedLibraryProxy, bool drillDownDependents, 
                                   bool createCopy, bool replaceExisting)
        {
            LibraryItem packageItem = null;

            // Handle dependents
            if (drillDownDependents)
            {
                DrillDownDependentItems(libraryItem, sharedLibraryProxy, libraryItem.DependentObject);
            }

            // Attempt to get the item by name matching if we are replacing
            if (replaceExisting)
            {
                packageItem = GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
                if ( packageItem == null)
                packageItem = GetLibraryItemByName(libraryItem);
            }
            else
            {
                // Ensure there is not going to be a Guid clash
                if (GetLibraryItemByUniqueIdentifier(libraryItem.UniqueIdentifier) != null)
                {
                    libraryItem.OriginalUniqueIdentifier = libraryItem.UniqueIdentifier;
                    libraryItem.UniqueIdentifier = Guid.NewGuid();
                }
            }

            if (packageItem == null)
            {
                switch (libraryItem.LibraryType)
                {
                    case LibraryItemType.Question:
                        packageItem = CreateQuestion(libraryItem.UniqueIdentifier);
                        break;
                    case LibraryItemType.Outcome:
                        packageItem = CreateOutcome(libraryItem.UniqueIdentifier);
                        break;
                    case LibraryItemType.Function:
                        if (libraryItem is ArithmeticPFunction)
                        {
                            packageItem = CreateArithmeticFunction(libraryItem.UniqueIdentifier);
                        }
                        else if (libraryItem is DatePFunction)
                        {
                            packageItem = CreateDateFunction(libraryItem.UniqueIdentifier);
                        }
                        else
                        {
                            throw new InvalidOperationException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").
                                                     GetString("Perfectus.Client.UI.PackageExplorer.ExplorerControl.InvalidFunctionType"));
                        }
                        break;
                    case LibraryItemType.SimpleOutcome:
                        packageItem = CreateSimpleOutcome(libraryItem.UniqueIdentifier);
                        break;
                    case LibraryItemType.Template:
                        packageItem = CreateWordTemplate(libraryItem.UniqueIdentifier);
                        break;
                    case LibraryItemType.InterviewPage:
                        if (this.interviews == null || this.interviews.Count == 0)
                        {
                            // If the interview is being created - update the new start page with the item that was created
                            Interview interview = CreateInterview(true);
                            packageItem = interview.StartPage;
                            HandleLibraryItemUpdate(packageItem, libraryItem);
                        }
                        else
                        {
                            packageItem = CreatePage(this.Interviews[0], libraryItem.UniqueIdentifier);
                        }
                        break;
                    default:
                        //TODO - RESOURCE
                        throw new InvalidOperationException("Could not determine the type of package item type from the shared library.");
                }
            }

            // Update the new item with the details from the shared library
            HandleLibraryItemUpdate(packageItem, libraryItem);

            //	If the item is a copy, ensure it is not marked as a shared library item
            if (createCopy)
            {
                packageItem.Linked = false;
                packageItem.LibraryUniqueIdentifier = Guid.Empty;
                packageItem.IsNewLibraryItem = true;
            }

            // Ensure the parent package is set
            packageItem.ParentPackage = this;
        }

        /// <summary>
        ///		Drills down a populated library items depdendent library items, until it no longer has depdenencies
        /// </summary>
        /// <param name="libraryItem">The latest version of the package item from the shared library</param>        
        /// <param name="sharedLibraryProxy">
        ///     A reference to the Shared Library proxt object that connects to the Shared Library Web Service
        /// </param>
        /// <param name="replaceExisting">Boolean flag indicating whether to replace on matching or simple a new item.</param>
        public void DrillDownDependentItems(LibraryItem libraryItem, SharedLibraryProxy sharedLibraryProxy, bool isDependent)
        {
            libraryItem.DependentObject = isDependent;

            // Drill down the items dependents - only if there are dependents
            if (libraryItem != null && libraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    DrillDownDependentItems(dependentLibraryItem, sharedLibraryProxy, true);
                }

                // Only handle dependents - not the root item itself
                if (libraryItem.DependentObject)
                {
                    // Create the new depdentent library item
                    HandleDependentLibraryItem(libraryItem, sharedLibraryProxy);
                }
            }
        }

        /// <summary>
        ///		Creates the specific library item as given by its type, and ensure no depdendent drill down checking is performed.
        /// </summary>
        /// <param name="libraryItem">The library item to be added.</param>
        /// <param name="sharedLibraryProxy">A reference to the shared library object.</param>
        private void HandleDependentLibraryItem(LibraryItem libraryItem, SharedLibraryProxy sharedLibraryProxy)
        {
            LibraryItem packageItem = null;

            // Attempt to get the item by Uid
            packageItem = GetLibraryItemByUniqueIdentifier(libraryItem.UniqueIdentifier);

            if (packageItem != null && packageItem.Linked == false)
            {
                throw new Exception(String.Format(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").
                GetString("Perfectus.Common.SharedLibrary.CannotUpdateExistingUnlinkedDependent"), libraryItem.Name));
            }

            if (packageItem != null)
            {
                if (null == libraryItem.ParentPackage)
                    libraryItem.ParentPackage = packageItem.ParentPackage;
                switch (libraryItem.LibraryType)
                {
                    case LibraryItemType.Function:
                        if (libraryItem.GetType() == typeof(ArithmeticPFunction))
                        {
                            ((ArithmeticPFunction)packageItem).Update((ArithmeticPFunction)libraryItem);
                        }
                        else
                        {
                            ((DatePFunction)packageItem).Update((DatePFunction)libraryItem);
                        }
                        break;
                    case LibraryItemType.InterviewPage:
                        ((InternalPage)packageItem).Update((InternalPage)libraryItem);
                        break;
                    case LibraryItemType.Question:
                        ((Question)packageItem).Update((Question)libraryItem);
                        break;
                    case LibraryItemType.Outcome:
                        ((Outcome)packageItem).Update((Outcome)libraryItem);
                        break;
                    case LibraryItemType.SimpleOutcome:
                        ((SimpleOutcome)packageItem).Update((SimpleOutcome)libraryItem);
                        break;
                    case LibraryItemType.Template:
                        ((WordTemplateDocument2)packageItem).Update((WordTemplateDocument2)libraryItem);
                        break;
                }
            }
            else
            {
                AddLibraryItem(libraryItem, sharedLibraryProxy, false, false, false);
            }
        }

        /// <summary>
        ///		Updates a given library item to mirror that of the share library item.
        /// </summary>
        /// <param name="libraryItem">The package library item to be updated.</param>
        /// <param name="sharedLibraryItem">The shared library item that is to be mirrored.</param>
        public static void HandleLibraryItemUpdate(LibraryItem libraryItem, LibraryItem sharedLibraryItem)
        {
            switch (libraryItem.LibraryType)
            {
                case LibraryItemType.Function:
                    if (libraryItem.PerfectusType == typeof (ArithmeticPFunction).ToString())
                        ((ArithmeticPFunction) libraryItem).Update((ArithmeticPFunction) sharedLibraryItem);
                    else
                        ((DatePFunction) libraryItem).Update((DatePFunction) sharedLibraryItem);
                    break;
                case LibraryItemType.InterviewPage:
                    ((InternalPage) libraryItem).Update((InternalPage) sharedLibraryItem);
                    break;
                case LibraryItemType.Outcome:
                    ((Outcome) libraryItem).Update((Outcome) sharedLibraryItem);
                    break;
                case LibraryItemType.Question:
                    ((Question) libraryItem).Update((Question) sharedLibraryItem);
                    break;
                case LibraryItemType.SimpleOutcome:
                    ((SimpleOutcome) libraryItem).Update((SimpleOutcome) sharedLibraryItem);
                    break;
                case LibraryItemType.Template:
                    ((WordTemplateDocument2) libraryItem).Update((WordTemplateDocument2) sharedLibraryItem);
                    break;
            }

            // update the UID
            if (libraryItem.UniqueIdentifier != sharedLibraryItem.UniqueIdentifier)
            {
                Package package = libraryItem.ParentPackage;

                // Update the appropiate dictionarys
                if (libraryItem is ITemplateItem)
                {
                    package.templateItemDictionary.Remove(libraryItem.UniqueIdentifier);
                    package.templateItemDictionary.Add(sharedLibraryItem.UniqueIdentifier, (ITemplateItem)libraryItem);
                }

                if (libraryItem is IStringBindingItem)
                {
                    package.stringBindingItemDictionary.Remove(libraryItem.UniqueIdentifier);
                    package.stringBindingItemDictionary.Add(sharedLibraryItem.UniqueIdentifier, (IStringBindingItem)libraryItem);
                }

                if (libraryItem is Question)
                {
                    package.questionDictionary.Remove(libraryItem.UniqueIdentifier);
                    package.questionDictionary.Add(sharedLibraryItem.UniqueIdentifier, (Question)libraryItem);
                }

                if (libraryItem is InterviewPage)
                {
                    package.interviewPageDictionary.Remove(libraryItem.UniqueIdentifier);
                    package.interviewPageDictionary.Add(sharedLibraryItem.UniqueIdentifier, (InterviewPage)libraryItem);
                }

                libraryItem.OriginalUniqueIdentifier = libraryItem.UniqueIdentifier;
                libraryItem.UniqueIdentifier = sharedLibraryItem.UniqueIdentifier;
                package.OnItemUniqueIdentifierChange(libraryItem);
            }

            // Ensure the relative path matches that as in the repository
            libraryItem.RelativePath = sharedLibraryItem.RelativePath;
        }

        #endregion

        private LibraryItem GetLatestSharedLibraryItem(LibraryItem libraryItem, SharedLibraryProxy sharedLibraryProxy)
        {
            if (libraryItem.LibraryUniqueIdentifier != new Guid() && libraryItem.Linked &&
                libraryItem.CheckedOutByLogInName != WindowsIdentity.GetCurrent().Name)
            {
                
                LibraryItem latestLibraryItem = sharedLibraryProxy.GetLatestVersion(libraryItem);
                DrillDownDependentItems(latestLibraryItem, sharedLibraryProxy, latestLibraryItem.DependentObject);

                return latestLibraryItem;
            }
            else
            {
                return null;
            }
        }

        public LibraryItem GetLibraryItemByName(LibraryItem libraryItem)
        {
            // Enumerate through the appropriate library item type attempting to match the library item by name
            switch (libraryItem.LibraryType)
            {
                case LibraryItemType.Function:
                    foreach (PFunction packageLibraryItem in Functions)
                    {
                        if (packageLibraryItem.Name == libraryItem.Name)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    foreach (Interview interview in Interviews)
                    {
                        foreach (InterviewPage packageLibraryItem in interview.Pages)
                        {
                            if (packageLibraryItem.Name == libraryItem.Name)
                            {
                                packageLibraryItem.ParentPackage = this;
                                return packageLibraryItem;
                        }
                    }
                    }
                    break;
                case LibraryItemType.Outcome:
                    foreach (Outcome packageLibraryItem in Outcomes)
                    {
                        if (packageLibraryItem.Name == libraryItem.Name)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Question:
                    foreach (Question packageLibraryItem in Questions)
                    {
                        if (packageLibraryItem.Name == libraryItem.Name)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.SimpleOutcome:
                    foreach (SimpleOutcome packageLibraryItem in SimpleOutcomes)
                    {
                        if (packageLibraryItem.Name == libraryItem.Name)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Template:
                    foreach (TemplateDocument packageLibraryItem in Templates)
                    {
                        if (packageLibraryItem.Name == libraryItem.Name)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
            }

            // If there has been no match then false
            return null;
        }

        public LibraryItem GetLibraryItemByLibraryUniqueIdentifier(LibraryItem libraryItem)
        {
            // Enumerate through the appropriate library item type attempting to match the library item type unique identifiers
            switch (libraryItem.LibraryType)
            {
                case LibraryItemType.Function:
                    foreach (PFunction packageLibraryItem in Functions)
                    {
                        if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    foreach (Interview interview in Interviews)
                    {
                        foreach (InterviewPage packageLibraryItem in interview.Pages)
                        {
                            if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                            {
                                packageLibraryItem.ParentPackage = this;
                                return packageLibraryItem;
                        }
                    }
                    }
                    break;
                case LibraryItemType.Outcome:
                    foreach (Outcome packageLibraryItem in Outcomes)
                    {
                        if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Question:
                    foreach (Question packageLibraryItem in Questions)
                    {
                        if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.SimpleOutcome:
                    foreach (SimpleOutcome packageLibraryItem in SimpleOutcomes)
                    {
                        if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Template:
                    foreach (TemplateDocument packageLibraryItem in Templates)
                    {
                        if (packageLibraryItem.LibraryUniqueIdentifier == libraryItem.LibraryUniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
            }

            // If there has been no match then false
            return null;
        }

        public LibraryItem GetLibraryItemByUniqueIdentifier(Guid uniqueIdentifier)
        {
            LibraryItem libraryItem = null;

            libraryItem = GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.Question);
            if (libraryItem != null) { return libraryItem; }

            libraryItem = GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.Outcome);
            if (libraryItem != null) { return libraryItem; }

            libraryItem = GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.SimpleOutcome);
            if (libraryItem != null) { return libraryItem; }

            libraryItem = GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.Function);
            if (libraryItem != null) { return libraryItem; }

            libraryItem = GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.InterviewPage);
            if (libraryItem != null) { return libraryItem; }

            return GetLibraryItemByUniqueIdentifier(uniqueIdentifier, LibraryItemType.Template);
        }

        public LibraryItem GetLibraryItemByUniqueIdentifier(Guid uniqueIdentifier, LibraryItemType libraryItemType)
        {
            // Enumerate through the appropriate library item type attempting to match
            // the library item type unique identifiers
            switch (libraryItemType)
            {
                case LibraryItemType.Function:
                    foreach (PFunction packageLibraryItem in Functions)
                    {
                        if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    foreach (Interview interview in Interviews)
                    {
                        foreach (InterviewPage packageLibraryItem in interview.Pages)
                        {
                            if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                            {
                                packageLibraryItem.ParentPackage = this;
                                return packageLibraryItem;
                        }
                    }
                    }
                    break;
                case LibraryItemType.Outcome:
                    foreach (Outcome packageLibraryItem in Outcomes)
                    {
                        if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Question:
                    foreach (Question packageLibraryItem in Questions)
                    {
                        if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.SimpleOutcome:
                    foreach (SimpleOutcome packageLibraryItem in SimpleOutcomes)
                    {
                        if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
                case LibraryItemType.Template:
                    foreach (TemplateDocument packageLibraryItem in Templates)
                    {
                        if (packageLibraryItem.UniqueIdentifier == uniqueIdentifier)
                        {
                            packageLibraryItem.ParentPackage = this;
                            return packageLibraryItem;
                    }
                    }
                    break;
            }

            // If there has been no match then false
            return null;
        }

        public MemoryStream GetAnswerState()
        {
            MemoryStream ms = new MemoryStream();

            XmlTextWriter xw = new XmlTextWriter(ms, Encoding.UTF8);
            xw.WriteStartDocument();
            xw.WriteStartElement("state");
            xw.WriteStartElement("package");
            xw.WriteAttributeString("mirrorDocType", mirrorDocType);
            xw.WriteStartElement("mirror");

            BinaryFormatter bf1 = new BinaryFormatter();
            using (MemoryStream mirrorStream = new MemoryStream())
            {
                bf1.Serialize(mirrorStream, makeMirror);
                mirrorStream.Seek(0, SeekOrigin.Begin);
                byte[] bytes = mirrorStream.ToArray();
                xw.WriteBase64(bytes, 0, bytes.Length);
            }

            xw.WriteEndElement();
            xw.WriteEndElement();
            xw.WriteStartElement("questions");
            foreach (Question q in questions)
            {
                xw.WriteStartElement("q");
                //xw.WriteAttributeString("id", q.UniqueIdentifier.ToString("N"));
                xw.WriteAttributeString("seen", q.Seen.ToString());
                if (q.Items != null && q.Items.Count > 0 && q.DataBindings != null &&
                    q.DataBindings.ItemsValueBinding != null)
                {
                    xw.WriteStartElement("items");
                    foreach (ItemsItem ii in q.Items)
                    {
                        xw.WriteStartElement("ii");
                        xw.WriteAttributeString("d", ii.Display);
                        xw.WriteAttributeString("v", ii.Value);
                        xw.WriteEndElement(); // /item
                    }
                    xw.WriteEndElement(); // /items
                }
                if (q.AnswersByPerson != null && q.AnswersByPerson.Count > 0)
                {
                    xw.WriteStartElement("answersByPerson");
                    BinaryFormatter bf = new BinaryFormatter();
                    using (MemoryStream answerStream = new MemoryStream())
                    {
                        bf.Serialize(answerStream, q.AnswersByPerson);
                        answerStream.Seek(0, SeekOrigin.Begin);
                        byte[] bytes = answerStream.ToArray();
                        xw.WriteBase64(bytes, 0, bytes.Length);
                    }
                    xw.WriteEndElement(); // /answersByPerson
                }
                if (q.AnswersBySystem != null && q.AnswersBySystem.Count > 0)
                {
                    xw.WriteStartElement("answersBySystem");
                    BinaryFormatter bf = new BinaryFormatter();
                    using (MemoryStream answerStream = new MemoryStream())
                    {
                        bf.Serialize(answerStream, q.AnswersBySystem);
                        answerStream.Seek(0, SeekOrigin.Begin);
                        byte[] bytes = answerStream.ToArray();
                        xw.WriteBase64(bytes, 0, bytes.Length);
                    }
                    xw.WriteEndElement(); // /answersByPerson
                }
                xw.WriteEndElement(); // /question
            }
            xw.WriteEndElement(); // /questions

            xw.WriteStartElement("interviews");

            foreach (Interview interview in interviews)
            {
                xw.WriteStartElement("interview");
                //xw.WriteAttributeString("id", interview.UniqueIdentifier.ToString("N"));

                xw.WriteStartElement("pages");
                foreach (InterviewPage page in interview.Pages)
                {
                    xw.WriteStartElement("p");
                    //xw.WriteAttributeString("id", page.UniqueIdentifier.ToString("N"));
                    xw.WriteAttributeString("seen", page.Seen.ToString());
                    xw.WriteEndElement(); /// /page
                }
                xw.WriteEndElement(); // /pages
                xw.WriteEndElement(); // /interview
            }


            xw.WriteEndElement(); // /interviews


            xw.WriteEndElement(); // /state
            xw.WriteEndDocument();
            xw.Flush();
            ms.Seek(0, SeekOrigin.Begin);

            return ms;
        }

        public void ApplyAnswerState(Stream state)
        {
            if (state.Length > 0)
            {
                state.Seek(0, SeekOrigin.Begin);
                XmlTextReader rdr = new XmlTextReader(state);
                int interviewCount = -1;
                int pageCount = -1;
                int questionCount = -1;
                bool answersByPerson = false;
                bool answersBySystem = false;
                bool mirror = false;

                while (rdr.Read())
                {
                    if (rdr.Name == "mirror" && rdr.NodeType == XmlNodeType.Element)
                    {
                        mirror = true;
                    }
                    else if (rdr.Name == "package" && rdr.NodeType == XmlNodeType.Element)
                    {
                        rdr.MoveToAttribute("mirrorDocType");
                        mirrorDocType = rdr.Value;
                    }

                    else if (rdr.Name == "interview" && rdr.NodeType == XmlNodeType.Element)
                    {
                        interviewCount++;
                        pageCount = -1;
                    }

                    else if (rdr.Name == "p" && rdr.NodeType == XmlNodeType.Element)
                    {
                        pageCount++;
                        rdr.MoveToAttribute("seen");
                        bool seen = bool.Parse(rdr.Value);
                        interviews[interviewCount].Pages[pageCount].Seen = seen;
                    }
                    else if (rdr.Name == "q" && rdr.NodeType == XmlNodeType.Element)
                    {
                        answersByPerson = false;
                        answersBySystem = false;
                        questionCount++;
                        rdr.MoveToAttribute("seen");

                        bool seen = bool.Parse(rdr.Value);
                        questions[questionCount].Seen = seen;
                    }
                    else if (rdr.Name == "items" && rdr.NodeType == XmlNodeType.Element)
                    {
                        if (questions[questionCount].Items != null)
                        {
                            questions[questionCount].Items.Clear();
                        }
                    }
                    else if (rdr.Name == "ii" && rdr.NodeType == XmlNodeType.Element)
                    {
                        rdr.MoveToAttribute("d");
                        string display = rdr.Value;
                        rdr.MoveToAttribute("v");
                        string value = rdr.Value;
                        ItemsItem ii = new ItemsItem();
                        ii.Display = display;
                        ii.Value = value;
                        if (questions[questionCount].Items == null)
                        {
                            questions[questionCount].Items = new ItemsItemCollection(new ItemsItem[] {ii});
                        }
                        else
                        {
                            questions[questionCount].Items.Add(ii);
                        }
                    }

                    else if (rdr.Name == "answersByPerson" && rdr.NodeType == XmlNodeType.Element)
                    {
                        answersByPerson = true;
                        answersBySystem = false;
                    }
                    else if (rdr.Name == "answersBySystem" && rdr.NodeType == XmlNodeType.Element)
                    {
                        answersBySystem = true;
                        answersByPerson = false;
                    }
                    else if (rdr.NodeType == XmlNodeType.Text && (answersBySystem || answersByPerson))
                    {
                        string base64 = rdr.Value;
                        byte[] bytes = Convert.FromBase64String(base64);
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Context = new StreamingContext(StreamingContextStates.Persistence);

                        using (MemoryStream ms = new MemoryStream(bytes))
                        {
                            if (answersByPerson)
                            {
                                questions[questionCount].AnswersByPerson = (AnswerCollectionCollection) bf.UnsafeDeserialize(ms, null);
                            }
                            else
                            {
                                questions[questionCount].AnswersBySystem = (AnswerCollectionCollection) bf.UnsafeDeserialize(ms, null);
                            }
                        }
                    }

                    else if (rdr.NodeType == XmlNodeType.Text && mirror)
                    {
                        mirror = false;
                        string base64 = rdr.Value;
                        byte[] bytes = Convert.FromBase64String(base64);
                        BinaryFormatter bf = new BinaryFormatter();
                        bf.Context = new StreamingContext(StreamingContextStates.Persistence);
                        using (MemoryStream ms = new MemoryStream(bytes))
                        {
                            makeMirror = (YesNoQueryValue) bf.UnsafeDeserialize(ms, null);
                        }
                    }
                }

                rdr.Close();
            }
        }

        public Question FindQuestionByName(string searchBy)
        {
            searchBy = searchBy.ToLower();
            foreach (Question q in questions)
            {
                if (searchBy == q.Name.ToLower())
                {
                    return q;
                }
            }
            return null;
        }

        /// <summary>
        ///     Event to notifiy that any open designers should ensure their contents are saved.
        /// </summary>
        /// <param name="args"></param>
        public void EnsureUIDesignerSaved(PackageItemEventArgs args)
        {
            if (EnsureDesignerSaved != null)
            {
                EnsureDesignerSaved(this, args);
            }
        }

        /// <summary>
        ///     Event to notify that a given questions bindings could not be successfully reconnected to the package.
        /// </summary>
        /// <param name="args"></param>
        public void RaiseBindingsCannotBeReconnected(PackageItemEventArgs args)
        {
            if (BindingsCannotBeReconnected != null)
            {
                BindingsCannotBeReconnected(this, args);
            }                
        }

        public void RefreshWebReference(string url, Guid ID)
        {
            WebReference webReference = null;

            foreach (DataSource ds in this.dataSources)
            {
                if (ds is WebReference)
                {
                    webReference = (WebReference)ds;

                    if (webReference.WebServiceAddress == url && webReference.UniqueIdentifier == ID)
                    {
                        DeleteItem(ds, false);
                        webReferencesDictionary.Remove(webReference.UniqueIdentifier);
                        try
                        {
                            EnsureWebReferenceExists(url, ID);

                            EnsureQuestionsHaveUpdatedURL(url, ID);
                        }
                        catch (Exception ex)
                        {
                            dataSources.Add(webReference);
                            webReferencesDictionary.Add(webReference.UniqueIdentifier, webReference);
                            webReference.Name = webReference.Name;
                            OnItemCreated(webReference);
                            throw ex;
                        }
                        break;
                    }
                }
            }
        }

        private void EnsureQuestionsHaveUpdatedURL(string url, Guid ID)
        {
            foreach (Question q in Questions)
                if (q.DataBindings != null)
                    q.DataBindings.ReEvaluateUrlByGuid(this, ID);
        }

        public Guid EnsureWebReferenceExists(string url, Guid ID)
        {
            foreach(DataSource ds in this.dataSources)
            {
                if (ds is WebReference)
                { 
                    WebReference webReference = (WebReference)ds;

                    if (webReference.WebServiceAddress == url && (webReference.UniqueIdentifier == ID ))
                    {
                        return ID;
                    }
                }
            }

            foreach (DataSource ds in this.dataSources)
            {
                if (ds is WebReference)
                {
                    WebReference webReference = (WebReference)ds;

                    if (webReference.WebServiceAddress == url )
                    {
                        return webReference.UniqueIdentifier;
                    }
                }
            }


            // If we get here there the web reference does not exist and needs to be created
            try
            {
                Uri uri = new Uri(url);
                WebReference newWebReference = this.CreateWebReference(uri, WebReference.GetLayoutXml(uri, false), ID);
                return ID;
            }
            catch (Exception ex)
            {
                string message = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("AutoAddWebReference");
                message = string.Format(message, url, ex.Message);
                throw new Exception(message, ex);
            }
            return Guid.Empty;
        }

        public WordMLDependentPackageItems PopulateLibraryItems(Dictionary<string, Guid> packageItems, 
                                                                out Dictionary<string, Guid> orphanedItems)
        {
            orphanedItems = new Dictionary<string,Guid>();
            WordMLDependentPackageItems referencedPackageItems = new WordMLDependentPackageItems();

            foreach (KeyValuePair<string, Guid> packageItem in packageItems)
            { 
                LibraryItem locatedLibraryItem;

                // Try locate the item in the questions collection
                locatedLibraryItem = GetLibraryItemByUniqueIdentifier(packageItem.Value, LibraryItemType.Question);
                if (locatedLibraryItem != null)
                {
                    referencedPackageItems.Add(locatedLibraryItem);
                    continue;
                }

                // Try locate the item in the conditional text collection
                locatedLibraryItem = GetLibraryItemByUniqueIdentifier(packageItem.Value, LibraryItemType.SimpleOutcome);
                if (locatedLibraryItem != null)
                {
                    referencedPackageItems.Add(locatedLibraryItem);
                    continue;
                }

                // Try locate the item in the functions collection
                locatedLibraryItem = GetLibraryItemByUniqueIdentifier(packageItem.Value, LibraryItemType.Function);
                if (locatedLibraryItem != null)
                {
                    referencedPackageItems.Add(locatedLibraryItem);
                    continue;
                }

                // Try locate the item in the outcomes collection
                locatedLibraryItem = GetLibraryItemByUniqueIdentifier(packageItem.Value, LibraryItemType.Outcome);
                if (locatedLibraryItem != null)
                {
                    referencedPackageItems.Add(locatedLibraryItem);
                    continue;
                }

                // no matches to exists package items - Orphaned Item
                orphanedItems.Add(packageItem.Key, packageItem.Value);
            }

            // Return the populated collection of referenced package items.
            return referencedPackageItems;
        }
    }
}
