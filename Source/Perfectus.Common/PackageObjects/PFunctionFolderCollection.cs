using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// A collection of elements of type FunctionFolder
    /// </summary>
    [Serializable]
    public sealed class PFunctionFolderCollection : CollectionBase
    {
        /// <summary>
        /// Initializes a new empty instance of the FunctionFolderCollection class.
        /// </summary>
        internal PFunctionFolderCollection()
        {
            // empty
        }

        /// <summary>
        /// Initializes a new instance of the FunctionFolderCollection class, containing elements
        /// copied from an array.
        /// </summary>
        /// <param name="items">
        /// The array whose elements are to be added to the new FunctionFolderCollection.
        /// </param>
        internal PFunctionFolderCollection(PFunctionFolder[] items)
        {
            this.AddRange(items);
        }

        /// <summary>
        /// Initializes a new instance of the FunctionFolderCollection class, containing elements
        /// copied from another instance of FunctionFolderCollection
        /// </summary>
        /// <param name="items">
        /// The FunctionFolderCollection whose elements are to be added to the new FunctionFolderCollection.
        /// </param>
        internal PFunctionFolderCollection(PFunctionFolderCollection items)
        {
            this.AddRange(items);
        }

        /// <summary>
        /// Adds the elements of an array to the end of this FunctionFolderCollection.
        /// </summary>
        /// <param name="items">
        /// The array whose elements are to be added to the end of this FunctionFolderCollection.
        /// </param>
        internal void AddRange(PFunctionFolder[] items)
        {
            foreach (PFunctionFolder item in items)
            {
                this.List.Add(item);
            }
        }

        /// <summary>
        /// Adds the elements of another FunctionFolderCollection to the end of this FunctionFolderCollection.
        /// </summary>
        /// <param name="items">
        /// The FunctionFolderCollection whose elements are to be added to the end of this FunctionFolderCollection.
        /// </param>
        internal void AddRange(PFunctionFolderCollection items)
        {
            foreach (PFunctionFolder item in items)
            {
                this.List.Add(item);
            }
        }

        /// <summary>
        /// Adds an instance of type FunctionFolder to the end of this FunctionFolderCollection.
        /// </summary>
        /// <param name="value">
        /// The FunctionFolder to be added to the end of this FunctionFolderCollection.
        /// </param>
        public void Add(PFunctionFolder value)
        {
            this.List.Add(value);
        }

        /// <summary>
        /// Determines whether a specfic FunctionFolder value is in this FunctionFolderCollection.
        /// </summary>
        /// <param name="value">
        /// The FunctionFolder value to locate in this FunctionFolderCollection.
        /// </param>
        /// <returns>
        /// true if value is found in this FunctionFolderCollection;
        /// false otherwise.
        /// </returns>
        public bool Contains(PFunctionFolder value)
        {
            return this.List.Contains(value);
        }

        /// <summary>
        /// Return the zero-based index of the first occurrence of a specific value
        /// in this FunctionFolderCollection
        /// </summary>
        /// <param name="value">
        /// The FunctionFolder value to locate in the FunctionFolderCollection.
        /// </param>
        /// <returns>
        /// The zero-based index of the first occurrence of the _ELEMENT value if found;
        /// -1 otherwise.
        /// </returns>
        public int IndexOf(PFunctionFolder value)
        {
            return this.List.IndexOf(value);
        }

        /// <summary>
        /// Inserts an element into the FunctionFolderCollection at the specified index
        /// </summary>
        /// <param name="index">
        /// The index at which the FunctionFolder is to be inserted.
        /// </param>
        /// <param name="value">
        /// The FunctionFolder to insert.
        /// </param>
        internal void Insert(int index, PFunctionFolder value)
        {
            this.List.Insert(index, value);
        }

        /// <summary>
        /// Gets or sets the FunctionFolder at the given index in this FunctionFolderCollection.
        /// </summary>
        public PFunctionFolder this[int index]
        {
            get { return (PFunctionFolder)this.List[index]; }
            set { this.List[index] = value; }
        }

        /// <summary>
        /// Removes the first occurrence of a specific FunctionFolder from this FunctionFolderCollection.
        /// </summary>
        /// <param name="value">
        /// The FunctionFolder value to remove from this FunctionFolderCollection.
        /// </param>
        internal void Remove(PFunctionFolder value)
        {
            this.List.Remove(value);
        }

        /// <summary>
        /// Type-specific enumeration class, used by FunctionFolderCollection.GetEnumerator.
        /// </summary>
        public class Enumerator : IEnumerator
        {
            private IEnumerator wrapped;

            public Enumerator(PFunctionFolderCollection collection)
            {
                this.wrapped = ((CollectionBase)collection).GetEnumerator();
            }

            public PFunctionFolder Current
            {
                get { return (PFunctionFolder)(this.wrapped.Current); }
            }

            object IEnumerator.Current
            {
                get { return (PFunctionFolder)(this.wrapped.Current); }
            }

            public bool MoveNext()
            {
                return this.wrapped.MoveNext();
            }

            public void Reset()
            {
                this.wrapped.Reset();
            }
        }

        /// <summary>
        /// Returns an enumerator that can iterate through the elements of this FunctionFolderCollection.
        /// </summary>
        /// <returns>
        /// An object that implements System.Collections.IEnumerator.
        /// </returns>        
        new public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }
    }
}
