using System;
using System.Collections;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Query.
	/// </summary>
	[Serializable]
	public sealed class Query : PackageItem, IPackageItemContainer, ICloneable, ISerializable, IXmlSerializable
	{
		#region Private Member Vairalbes

		private QueryExpression queryExpression = null;
		private ActionBase actionIfTrue = null;
		private ActionBase actionIfFalse = null;

		// IXmlSerialiable variables
		private const string msACTION_IF_TRUE_KEY = "ActionIfTrue";
		private const string msACTION_IF_FALSE_KEY = "ActionIfFalse";
		private const string msQUERY_EXPRESSION_KEY = "QueryExpression";
		private const string msWORD_ML_KEY = "WordML";
        private const string msDISPLAY_NAME_KEY = "OutcomeName";
		private const string msSUB_QUERY_KEY = "SubQuery";

		#endregion
		#region Properties & Constructor

		public ActionBase ActionIfTrue
		{
			get { return actionIfTrue; }
			set { actionIfTrue = value; }
		}

		public ActionBase ActionIfFalse
		{
			get { return actionIfFalse; }
			set { actionIfFalse = value; }
		}

		public QueryExpression QueryExpression
		{
			get {  return queryExpression; }			
			set
			{
				if (queryExpression != null)
				{
					queryExpression.PropertyChanged -= new PropertyChangedEventHandler(expression_PropertyChanged);
				}

				queryExpression = value;
				queryExpression.PropertyChanged += new PropertyChangedEventHandler(expression_PropertyChanged);		
		
			}
		}
		
		public string AsJavaScript
		{
			get { return ToJavaScript(); }
		}

		public ActionBase[] AllPossibleActions
		{
			get { return FindAllPossibleActions(); }

		}

		// Public for SL serialization
		public Query() : base(){}

		public Query(Package parentPackage) : this()
		{
			this.ParentPackage = parentPackage;
			queryExpression = new QueryExpression();
			queryExpression.ParentPackage = parentPackage;
			queryExpression.PropertyChanged += new PropertyChangedEventHandler(expression_PropertyChanged);

		}

		#endregion
		#region Methods

		[Obsolete]
		public bool EvaluateBool()
		{
			return EvaluateBool(AnswerProvider.Caller.System);
		}

		public bool EvaluateBool(AnswerProvider.Caller caller)
		{
			if (queryExpression == null)
			{
				return true;
			}
			else
			{
				return queryExpression.Evaluate(caller);
			}
			
		}

		[Obsolete]
		public ActionBase Evaluate()
		{
			return Evaluate(AnswerProvider.Caller.System);
		}

		public ActionBase Evaluate(AnswerProvider.Caller caller)
		{
			if (queryExpression == null)
			{
				return null;
			}
			else
			{
				bool result = queryExpression.Evaluate(caller);
				if (result)
				{
					if (actionIfTrue.SubQuery != null)
					{
						return actionIfTrue.SubQuery.Evaluate(caller);
					}
					else
					{
						return actionIfTrue;
					}
				}
				else
				{
					if (actionIfFalse != null && actionIfFalse.SubQuery != null)
					{
						return actionIfFalse.SubQuery.Evaluate(caller);
					}
					else
					{
						return actionIfFalse;
					}
				}
			}
		}

		// Used by the diagram tool to show all possible 'next page' actions from a given page.
		private ActionBase[] FindAllPossibleActions()
		{
			if (queryExpression == null)
			{
				return null;
			}
			else
			{
				ArrayList l = new ArrayList();
				AddActionsToList(l, this);

				return (ActionBase[]) l.ToArray(typeof (ActionBase));
			}
		}


		private void AddActionsToList(ArrayList l, Query q)
		{
			ActionBase aTrue = q.actionIfTrue;
			ActionBase aFalse = q.actionIfFalse;
            if (aTrue != null && aTrue.SubQuery != null)
			{
				AddActionsToList(l, aTrue.SubQuery);
			}
			else
			{
				l.Add(aTrue);
			}

            if (aFalse != null && aFalse.SubQuery != null)
			{
				AddActionsToList(l, aFalse.SubQuery);
			}
			else
			{
				l.Add(aFalse);
			}

		}

		//TODO: Do
		private string ToJavaScript()
		{
			return null;
		}

		private void expression_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			OnPropertyChanged("Expression");
		}

		public override object Clone()
		{
			object newObj = this.MemberwiseClone();
			Query newQ = (Query)newObj;
			newQ.ParentPackage = ParentPackage;
			newQ.queryExpression = (QueryExpression)queryExpression.Clone();
		
			if(actionIfFalse != null)
			{
				newQ.actionIfFalse = (ActionBase)actionIfFalse.Clone();
			}
			
			if(actionIfTrue != null)
			{
				newQ.actionIfTrue = (ActionBase)actionIfTrue.Clone();				
			}

			return newQ;
		}

		#endregion
		#region ISerializable Members

		public Query(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			actionIfFalse = (ActionBase) info.GetValue("actionIfFalse", typeof (ActionBase));
			actionIfTrue = (ActionBase) info.GetValue("actionIfTrue", typeof (ActionBase));
			queryExpression = (QueryExpression) info.GetValue("queryExpression", typeof (QueryExpression));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("actionIfFalse", actionIfFalse);
			info.AddValue("actionIfTrue", actionIfTrue);
			info.AddValue("queryExpression", queryExpression);

		}

		#endregion
		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			if (queryExpression != null && queryExpression.ContainsRefTo(item))
			{
				return true;
			}
			if (actionIfTrue != null && actionIfTrue.ContainsReferenceTo(item))
			{
				return true;
			}
			if (actionIfFalse != null && actionIfFalse.ContainsReferenceTo(item))
			{
				return true;
			}
			return false;
		}

		#endregion		
		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		/// <summary>
		///		Serializes the Query into Xml.
		/// </summary>
		/// <param name="w">Writes the object to Xml.</param>
		public void WriteXml(XmlWriter w)
		{	
			// Start the dependencies section
			w.WriteStartElement(Serializer.DependenciesKey);

			// Write out the querys information
			WriteQueryData(w, this);

			// End the dependencies section
			w.WriteEndElement();
		}

		/// <summary>
		///		Serializes the data of the query, including the ActionIfTrue, ActionIfFalse, QueryExpression
		/// </summary>
		/// <param name="w">Writes the object to Xml.</param>
		/// <param name="query">The query being serialized.</param>
		private void WriteQueryData(XmlWriter w, Query query)
		{
			// ActionIfTrue
			if (query != null && query.actionIfTrue != null)
				WriteActionDependentInformation(w, query.actionIfTrue, msACTION_IF_TRUE_KEY);

			// ActionIfFalse
			if (query != null && query.actionIfFalse != null)
				WriteActionDependentInformation(w, query.actionIfFalse, msACTION_IF_FALSE_KEY);

			// QueryExpression
			WriteExpressionBaseDependentInformation(w, query.queryExpression);		
		}

		/// <summary>
		///		Writes out the dependent ActionBase information.
		/// </summary>
		/// <param name="w">XmlWriter to write the information to.</param>
		/// <param name="actionBase">The action base being serialized.</param>
		/// <param name="actionType">The type of action base being written. This is also used of the element name in the output Xml.</param>
		private void WriteActionDependentInformation(XmlWriter w, ActionBase actionBase, string actionType)
		{
			// Write out the starting element
			w.WriteStartElement(actionType);

			// Write out the Word ML element section
			if (actionBase is OutcomeAction)
			{
                w.WriteStartElement(msDISPLAY_NAME_KEY);

                if (((OutcomeAction)actionBase).DisplayName != null)
                {
                    w.WriteString(((OutcomeAction)actionBase).DisplayName);
                }

                w.WriteEndElement();

				w.WriteStartElement(msWORD_ML_KEY);

                if (((OutcomeAction)actionBase).OfficeOpenXML != null)
				{
                    w.WriteString(((OutcomeAction)actionBase).OfficeOpenXML);
				}

				w.WriteEndElement();
			}

			// Write out the Sub Query element section
			w.WriteStartElement(msSUB_QUERY_KEY);
			
			if (actionBase.SubQuery != null)
			{
				WriteQueryData(w, actionBase.SubQuery);
			}

			w.WriteEndElement();
			w.WriteEndElement();
		}

		/// <summary>
		///		Writes out the dependent ExpressionBase.
		/// </summary>
		/// <param name="w">XmlWriter to write the information to.</param>
		/// <param name="expressionNode">The expression base whose state is being serialized.</param>
		private void WriteExpressionBaseDependentInformation(XmlWriter w, ExpressionBase expressionBase)
		{
			expressionBase.WriteXml(w, true);
		}
		
		public void ReadXml(XmlReader r)
		{
            bool readEndElement = false;

			// Read the starting element
            if (r.Name != Serializer.DependenciesKey)
            {
                r.ReadStartElement();
                readEndElement = true;
            }

			// Start the dependencies section
			r.ReadStartElement(Serializer.DependenciesKey);

			// Read the and create queries information
			ReadQueryData(r, this);

			// End the dependencies section
			r.ReadEndElement();

			// Read the end element
            if (readEndElement)
            {
                r.ReadEndElement();
            }
		}

		/// <summary>
		///		Serializes the data of the query, including the ActionIfTrue, ActionIfFalse, QueryExpression
		/// </summary>
		/// <param name="r">Reads the object from Xml.</param>
		/// <param name="query">The query being deserialized.</param>
		private void ReadQueryData(XmlReader r, Query query)
		{
			query.actionIfTrue = (ActionBase) new OutcomeAction();
			query.actionIfFalse = (ActionBase) new OutcomeAction();
			query.queryExpression = new QueryExpression();

			// Deserialize the ActionIfTrue
			ReadActionDependentInformation(r, query.actionIfTrue, msACTION_IF_TRUE_KEY);

			// Deserialize the ActionIfFalse
			ReadActionDependentInformation(r, query.actionIfFalse, msACTION_IF_FALSE_KEY);

			// Deserialize the QueryExpression
			query.queryExpression = ReadExpressionBaseDependentInformation(r, query.queryExpression);
		}

		/// <summary>
		///		Reads the dependent ActionBase information and deserializes it from Xml to an object.
		/// </summary>
		/// <param name="r">XmlReader to read the information from.</param>
		/// <param name="actionBase">The action base being deserialized.</param>
		/// <param name="actionType">The type of action base being deserialized.</param>
		private void ReadActionDependentInformation(XmlReader r, ActionBase actionBase, string actionType)
		{
			// Skip this if the given action type is not defined in the xml
			if (r.Name == actionType)
			{
                r.ReadStartElement(actionType);

                if (r.Name == msDISPLAY_NAME_KEY)   // Only read Name if item is ver 5.5+
                {
                    // Read the Outcome label
                    if (r.IsEmptyElement)
                    {
                        r.Read();
                    }
                    else
                    {
                        r.ReadStartElement(msDISPLAY_NAME_KEY);
                        ((OutcomeAction)actionBase).DisplayName = r.ReadString();
                        r.ReadEndElement();
                    }
                }

                // Read the Word ML element section
				if (r.IsEmptyElement)
				{
					r.Read();
				}
				else
				{
					r.ReadStartElement(msWORD_ML_KEY);
					//((OutcomeAction)actionBase).WordML = r.ReadString();
					r.ReadEndElement();
				}

				// Read out the Sub Query element section
				if (r.IsEmptyElement)
				{
					r.Read();
				}
				else
				{
					// Create a new sub query and read the deserialized info into it
					actionBase.SubQuery = new Query();

					r.ReadStartElement(msSUB_QUERY_KEY);
					ReadQueryData(r, actionBase.SubQuery);
					r.ReadEndElement();
				}
			
                if ( r.NodeType == XmlNodeType.EndElement )
    				r.ReadEndElement();
			}
		}

		/// <summary>
		///		Reads the dependent ExpressionBase from Xml and popluates the object.
		/// </summary>
		/// <param name="r">XmlReader to write the information to.</param>
		/// <param name="expressionBase">The expression base whose state is being serialized.</param>
		private QueryExpression ReadExpressionBaseDependentInformation(XmlReader r, ExpressionBase expressionBase)
		{
			return (QueryExpression)expressionBase.ReadXml(r, true);
		}
				
		#endregion
	}
}
