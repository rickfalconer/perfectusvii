using System;
using System.Collections.Generic;
using System.Text;

namespace Perfectus.Common.PackageObjects
{
    public struct StringBindingMetaData
    {
        private Guid packageInstanceId;
        private int packageVersion;
        private int packageRevision;
        private string packageName;
        private string instanceName; //EasyInterview Reference property

        public string PackageName
        {
            get { return packageName; }
            set { packageName = value; }
        }

        /// <summary>
        /// This is the EasyInterview "Reference" property
        /// </summary>
        public string InstanceName
        {
            get { return instanceName; }
            set { instanceName = value; }
        }

        public Guid PackageInstanceId
        {
            get { return packageInstanceId; }
            set { packageInstanceId = value; }
        }

        public int PackageVersion
        {
            get { return packageVersion; }
            set { packageVersion = value; }
        }

        public int PackageRevision
        {
            get { return packageRevision; }
            set { packageRevision = value; }
        }

        public StringBindingMetaData(string packageName, string instanceName, Guid packageInstanceId, int packageVersion, int packageRevision)
        {
            this.packageName = packageName;
            this.instanceName = instanceName;
            this.packageInstanceId = packageInstanceId;
            this.packageVersion = packageVersion;
            this.packageRevision = packageRevision;
        }
    }
}
