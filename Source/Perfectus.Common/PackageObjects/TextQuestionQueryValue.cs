using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
    /// <summary>
    /// Summary description for TextQuestionValue.
    /// </summary>
    [Serializable]
    public sealed class TextQuestionQueryValue : IPackageItemContainer, ISerializable, IXmlSerializable
    {
        public enum TextQuestionQueryValueType
        {
            Text,
            Question,
            NoValue,
            Query
        }

        private TextQuestionQueryValueType valueType;
        private string textValue;
        private Question questionValue;
        private Package parentPackage;
        private Query queryValue;

        public Query QueryValue
        {
            get { return queryValue; }
            set 
            { 
                //show query form?
                queryValue = value; 
            }
        }

        public TextQuestionQueryValue()
        {
            valueType = TextQuestionQueryValueType.NoValue;
            textValue = null;
            questionValue = null;
            queryValue = null;
            parentPackage = null;
        }

        [Obsolete]
        public string Evaluate(StringBindingMetaData stringBindingMetaData)
        {
            return Evaluate(AnswerProvider.Caller.System, stringBindingMetaData);
        }

        public string Evaluate(AnswerProvider.Caller caller, StringBindingMetaData stringBindingMetaData)
        {
            return Evaluate(caller, false, stringBindingMetaData);
        }

        public string Evaluate(AnswerProvider.Caller caller, bool encodeNonTopLevelAnswers, StringBindingMetaData stringBindingMetaData)
        {
            switch (valueType)
            {
                case TextQuestionQueryValueType.Text:

                    if (parentPackage != null)
                    {
                        string s = StringBinding.EvaluateStringBindingXml(textValue, caller, parentPackage.StringBindingItemByGuid, encodeNonTopLevelAnswers, stringBindingMetaData);
                        s = StringBinding.EvaluateStringBindingMetaData(s, stringBindingMetaData);
                        return s;
                    }

                    return StringBinding.EvaluateStringBindingMetaData(textValue, stringBindingMetaData);

                case TextQuestionQueryValue.TextQuestionQueryValueType.Query:

                    if (parentPackage != null && queryValue != null)
                    {
                        //evaluate the query and then evaluate whatever stringbinding results
                        HtmlStringBindingAction stringAction = (HtmlStringBindingAction)queryValue.Evaluate(caller);

                        if (stringAction != null)
                        {
                            string s = StringBinding.EvaluateStringBindingXml(stringAction.Payload, caller, parentPackage.StringBindingItemByGuid, encodeNonTopLevelAnswers, stringBindingMetaData);

                            if (stringAction != null && stringAction.Payload != null)
                            {
                                return StringBinding.EvaluateStringBindingMetaData(s, stringBindingMetaData);
                            }
                        }
                    }
                    return null;
                default:
                    Question q = questionValue;
                    if (q != null)
                    {
                        object a = q.GetSingleAnswer(caller);
                        if (a != null)
                        {
                            if (a.ToString().IndexOf("</item>") > -1)
                            {
                                string s = StringBinding.EvaluateStringBindingXml(a.ToString(), caller, q.ParentPackage.StringBindingItemByGuid, encodeNonTopLevelAnswers, stringBindingMetaData);
                                s = StringBinding.EvaluateStringBindingMetaData(s, stringBindingMetaData);
                                return s;
                            }

                            return StringBinding.EvaluateStringBindingMetaData(a.ToString(), stringBindingMetaData);
                        }
                    }
                    return null;
                /*
                if (q != null && q.Answers.Count > 0)
                {
                    return q.GetSingleAnswer(caller).ToString();
                }
                else
                {
                    return null;
                }*/
            }
        }

        public string EvaluateAndKeepXml()
        {
            switch (valueType)
            {
                case TextQuestionQueryValueType.Text:
                    return textValue;
                case TextQuestionQueryValueType.Query:
                    HtmlStringBindingAction stringAction = (HtmlStringBindingAction)queryValue.Evaluate(AnswerProvider.Caller.System);
                    
                    if (stringAction != null && stringAction.Payload != null)
                    {
                        return stringAction.Payload;
                    }
                    else
                    {
                        return string.Empty;
                    }

                default:
                    Question q = questionValue;
                    if (q != null)
                    {
                        return String.Format("<text>{0}</text>", StringBinding.GetItemNodeXml(q, ""));
                    }
                    return "";
            }
        }

        public TextQuestionQueryValueType ValueType
        {
            get { return valueType; }
            set
            {
                switch (value)
                {
                    case TextQuestionQueryValueType.Text:
                        questionValue = null;
                        break;
                    case TextQuestionQueryValueType.Question:
                        textValue = null;
                        break;
                    case TextQuestionQueryValueType.Query:
                        queryValue = null;
                        break;
                    case TextQuestionQueryValueType.NoValue:
                        textValue = null;
                        questionValue = null;
                        break;
                }
                valueType = value;
            }
        }

        public string TextValue
        {
            get { return textValue; }
            set
            {
                textValue = value;
                ValueType = TextQuestionQueryValueType.Text;
            }
        }

        public Question QuestionValue
        {
            get { return questionValue; }
            set
            {
                questionValue = value;
                ValueType = TextQuestionQueryValueType.Question;
            }
        }

        [XmlIgnore]
        public Package ParentPackage
        {
            get { return parentPackage; }
            set { parentPackage = value; }
        }

        public override string ToString()
        {
            return EvaluateAndKeepXml();
        }


        #region Shared Library
        public void ReconnectLibraryItems(Package package)
        {
            if (valueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
            {
                parentPackage = package;
                questionValue = (Question)GetDependentPackageItem((LibraryItem)this.questionValue);
                questionValue.ParentPackage = package;
            }
            else if (this.valueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
            {
                parentPackage = package;
                // Redo xml, to reflect new uIds.
                textValue = StringBinding.CorrectUidByLibraryIds(this.textValue, this.parentPackage);
            }
        }

        public void SetDependentLibraryItems(List<LibraryItem> dependentLibraryItems, Package package)
        {
            if (this.valueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
            {
                if (this.questionValue.LibraryUniqueIdentifier == new Guid() &&
                    !dependentLibraryItems.Contains((LibraryItem)this.questionValue))
                {
                    dependentLibraryItems.Add(this.questionValue);
                }
            }
            else if (this.valueType == TextQuestionQueryValueType.Text && this.textValue.IndexOf("<text>") >= 0)
            {
                PackageItem[] items = StringBinding.GetPackageItemsFromXml(this.textValue, package.StringBindingItemByGuid);

                for (int i = 0; i < items.Length; i++)
                {
                    if (items[i] is LibraryItem)
                    {
                        if (((LibraryItem)items[i]).LibraryUniqueIdentifier == new Guid() &&
                            !dependentLibraryItems.Contains((LibraryItem)items[i]))
                        {
                            dependentLibraryItems.Add((LibraryItem)items[i]);
                        }
                    }
                }
            }
        }

        private PackageItem GetDependentPackageItem(LibraryItem libraryItem)
        {
            object itemValue;
            PerfectusDataType itemDataType;
            PackageItem packageItem = null;

            // Attempt to get the item from the package
            packageItem = this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);

            if (libraryItem.LibraryType == LibraryItemType.None)
            {
                if (((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty() == PerfectusDataType.Date)
                {
                    itemDataType = PerfectusDataType.Date;
                    itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value.ToString();
                    itemValue = DateTime.Parse(itemValue.ToString()).ToLocalTime().ToShortDateString();
                }
                else
                {
                    itemDataType = ((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty();
                    itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value;
                }

                // Set the packge constant
                packageItem = new PackageConstant(itemDataType, itemValue);
            }

            if (packageItem == null)
            {
                Exception ex = new Exception(libraryItem.Name + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage")); //TODO - SL - RESOURCE
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                throw new NullReferenceException(libraryItem.Name);
            }

            return packageItem;
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            object newObj = this.MemberwiseClone();
            TextQuestionQueryValue newTqv = (TextQuestionQueryValue)newObj;

            return newTqv;

        }

        #endregion

        #region IPackageItemContainer Members

        public bool ContainsReferenceTo(PackageItem item)
        {
            if (valueType == TextQuestionQueryValueType.Query)
            {
                if (queryValue.QueryExpression != null && queryValue.QueryExpression.ContainsRefTo(item))
                {
                    return true;
                }

                if (queryValue.ActionIfTrue != null && queryValue.ActionIfTrue.ContainsReferenceTo(item))
                {
                    return true;
                }

                if (queryValue.ActionIfFalse != null && queryValue.ActionIfFalse.ContainsReferenceTo(item))
                {
                    return true;
                }
            }

            if (questionValue == item)
            {
                return true;
            }
            else if (textValue != null && textValue.IndexOf(item.UniqueIdentifier.ToString()) > -1) // if the uID is contained in the xml then that is good enough to know its been used.
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region ISerializable Members

        public TextQuestionQueryValue(SerializationInfo info, StreamingContext context)
        {
            valueType = (TextQuestionQueryValueType)info.GetValue("valueType", typeof(TextQuestionQueryValueType));
            textValue = info.GetString("textValue");
            questionValue = (Question)info.GetValue("questionValue", typeof(Question));

            try
            {
                queryValue = (Query)info.GetValue("queryValue", typeof(Query));
            }
            catch
            {
                queryValue = null;
            }

            try
            {
                parentPackage = (Package)info.GetValue("parentPackage", typeof(Package));
            }
            catch
            {
                uint a = 0xdeadbeef;
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("valueType", valueType);
            info.AddValue("textValue", textValue);
            info.AddValue("questionValue", questionValue);
            info.AddValue("parentPackage", parentPackage);
            info.AddValue("queryValue", queryValue);
        }

        #endregion

        #region IXmlSerializable Members

        /// <summary>
        /// This is required for the interface, but producing an XmlSchema is not required for our purposes,
        /// and therefore we simply return null.
        /// </summary>
        /// <returns>null</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        ///	Serializes the TextQuestionValue into Xml.
        /// </summary>
        /// <param name="w">Writes the object to Xml.</param>
        public void WriteXml(XmlWriter w)
        {
            w.WriteStartElement(Serializer.TextQuestionValueKey);

            if (this.ValueType == TextQuestionQueryValueType.Question)
            {
                // write value type of question.
                w.WriteStartElement(Serializer.ValueTypeKey);
                w.WriteRaw(Serializer.QuestionValueTypeKey);
                w.WriteEndElement();
                // write the question value, and dependent info.
                w.WriteStartElement(Serializer.QuestionValueKey);
                Serializer.WriteDependentInformation(w, this.QuestionValue, Serializer.QuestionValueTypeKey, false);
                w.WriteEndElement();

            }
            else if (this.valueType == TextQuestionQueryValueType.Text)
            {
                // write value type of text
                w.WriteStartElement(Serializer.ValueTypeKey);
                w.WriteRaw(Serializer.TextValueTypeKey);
                w.WriteEndElement();

                // write dependencies.
                w.WriteStartElement(Serializer.DependenciesKey);

                // get dependencies from the xml stored in the text property.
                PackageItem[] items = null;
                if (this.ParentPackage != null)
                {
                    items = StringBinding.GetPackageItemsFromXml(this.TextValue, this.ParentPackage.StringBindingItemByGuid);

                    if (items != null && items.Length > 0)
                    {
                        for (int i = 0; i < items.Length; i++)
                        {
                            Serializer.WriteDependentInformation(w, items[i], Serializer.QuestionValueTypeKey, false);
                        }
                    }
                }

                // end dependencies.
                w.WriteEndElement();

                // write text value as is.
                w.WriteStartElement(Serializer.TextValueKey);

                // there may not be a parent package set if not using binding.
                if (this.ParentPackage != null)
                {
                    // add library id attributes to the xml stored in textValue, so its easy to reconnect later on.
                    w.WriteRaw(StringBinding.AppendLibraryIdAttributes(System.Web.HttpUtility.HtmlEncode(this.TextValue), items));
                }
                else
                {
                    w.WriteRaw(System.Web.HttpUtility.HtmlEncode(this.TextValue));
                }

                w.WriteEndElement();
            }
            else
            {
                w.WriteStartElement(Serializer.ValueTypeKey);
                w.WriteRaw(Serializer.NoneValueTypeKey);
                w.WriteEndElement();
            }

            w.WriteEndElement();
        }

        /// <summary>
        /// Deserializes into object from XML.
        /// </summary>
        /// <param name="r">The reader to read XML from.</param>
        public void ReadXml(XmlReader r)
        {
            r.ReadStartElement();
            // read TextQuestionValue element.
            r.ReadStartElement(Serializer.TextQuestionValueKey);

            // read value type (e.g Text or Question).
            r.ReadStartElement(Serializer.ValueTypeKey);
            string strValType = r.ReadString();
            r.ReadEndElement();

            // handle a question value.
            if (strValType == Serializer.QuestionValueTypeKey)
            {
                r.ReadStartElement(Serializer.QuestionValueKey);
                this.valueType = TextQuestionQueryValueType.Question;

                r.ReadStartElement(Serializer.DependencyKey);
                r.Skip(); // skip "Type" element.

                string[] values = Serializer.ReadLibraryItem(r);

                // Set the question item
                Question q = new Question();
                q.Name = values[Serializer.NameIndex];
                q.LibraryUniqueIdentifier = new Guid(values[Serializer.UniqueIdentifierIndex]);
                q.RelativePath = values[Serializer.RelativePathIndex];
                this.QuestionValue = q;

                r.ReadEndElement();

                if (r.Name != Serializer.TextQuestionValueKey)
                {
                    r.ReadEndElement();
                }
            }
            else if (strValType == Serializer.TextValueTypeKey) // handle a text value.
            {
                this.valueType = TextQuestionQueryValueType.Text;

                // Dependencies section. we dont need to do anything else with it here. The Text property items tags have all the references already.
                r.Skip();

                // Set the Text Value.
                r.ReadStartElement(Serializer.TextValueKey);

                if (!r.IsStartElement("text")) // "text" is the xml stored as outer element if string binding used on a text value.
                {
                    this.TextValue = r.ReadString();
                }
                else
                {
                    this.TextValue = r.ReadOuterXml();
                }
                r.ReadEndElement();
            }

            // read end TextQuestionValue element
            r.ReadEndElement();
            // Read the end element
            r.ReadEndElement();
        }

        #endregion
    }
}
