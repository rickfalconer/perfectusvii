using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type SimpleOutcomeFolder
	/// </summary>
	[Serializable]
	public sealed class SimpleOutcomeFolderCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the SimpleOutcomeFolderCollection class.
		/// </summary>
		internal SimpleOutcomeFolderCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the SimpleOutcomeFolderCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new SimpleOutcomeFolderCollection.
		/// </param>
		internal SimpleOutcomeFolderCollection(SimpleOutcomeFolder[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the SimpleOutcomeFolderCollection class, containing elements
		/// copied from another instance of SimpleOutcomeFolderCollection
		/// </summary>
		/// <param name="items">
		/// The SimpleOutcomeFolderCollection whose elements are to be added to the new SimpleOutcomeFolderCollection.
		/// </param>
		internal SimpleOutcomeFolderCollection(SimpleOutcomeFolderCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this SimpleOutcomeFolderCollection.
		/// </param>
		internal void AddRange(SimpleOutcomeFolder[] items)
		{
			foreach (SimpleOutcomeFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another SimpleOutcomeFolderCollection to the end of this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <param name="items">
		/// The SimpleOutcomeFolderCollection whose elements are to be added to the end of this SimpleOutcomeFolderCollection.
		/// </param>
		internal void AddRange(SimpleOutcomeFolderCollection items)
		{
			foreach (SimpleOutcomeFolder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type SimpleOutcomeFolder to the end of this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcomeFolder to be added to the end of this SimpleOutcomeFolderCollection.
		/// </param>
		public void Add(SimpleOutcomeFolder value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic SimpleOutcomeFolder value is in this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcomeFolder value to locate in this SimpleOutcomeFolderCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this SimpleOutcomeFolderCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(SimpleOutcomeFolder value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this SimpleOutcomeFolderCollection
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcomeFolder value to locate in the SimpleOutcomeFolderCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(SimpleOutcomeFolder value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the SimpleOutcomeFolderCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the SimpleOutcomeFolder is to be inserted.
		/// </param>
		/// <param name="value">
		/// The SimpleOutcomeFolder to insert.
		/// </param>
		internal void Insert(int index, SimpleOutcomeFolder value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the SimpleOutcomeFolder at the given index in this SimpleOutcomeFolderCollection.
		/// </summary>
		public SimpleOutcomeFolder this[int index]
		{
			get { return (SimpleOutcomeFolder) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific SimpleOutcomeFolder from this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <param name="value">
		/// The SimpleOutcomeFolder value to remove from this SimpleOutcomeFolderCollection.
		/// </param>
		internal void Remove(SimpleOutcomeFolder value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by SimpleOutcomeFolderCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(SimpleOutcomeFolderCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public SimpleOutcomeFolder Current
			{
				get { return (SimpleOutcomeFolder) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (SimpleOutcomeFolder) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this SimpleOutcomeFolderCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}
