using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type WebReference
	/// </summary>
	[Serializable]
	public class WebReferenceDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the WebReferenceDictionary class
		/// </summary>
		public WebReferenceDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the WebReference associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual WebReference this[Guid key]
		{
			get { return (WebReference) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this WebReferenceDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The WebReference value of the element to add.
		/// </param>
		public virtual void Add(Guid key, WebReference value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this WebReferenceDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this WebReferenceDictionary.
		/// </param>
		/// <returns>
		/// true if this WebReferenceDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this WebReferenceDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this WebReferenceDictionary.
		/// </param>
		/// <returns>
		/// true if this WebReferenceDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this WebReferenceDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The WebReference value to locate in this WebReferenceDictionary.
		/// </param>
		/// <returns>
		/// true if this WebReferenceDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(WebReference value)
		{
			foreach (WebReference item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this WebReferenceDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this WebReferenceDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this WebReferenceDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}