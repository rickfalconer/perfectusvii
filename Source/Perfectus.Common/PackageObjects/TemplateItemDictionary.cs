using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type ITemplateItem
	/// </summary>
	[Serializable]
	public class TemplateItemDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the TemplateItemDictionary class
		/// </summary>
		public TemplateItemDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the ITemplateItem associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual ITemplateItem this[Guid key]
		{
			get { return (ITemplateItem) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this TemplateItemDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The ITemplateItem value of the element to add.
		/// </param>
		public virtual void Add(Guid key, ITemplateItem value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this TemplateItemDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this TemplateItemDictionary.
		/// </param>
		/// <returns>
		/// true if this TemplateItemDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this TemplateItemDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this TemplateItemDictionary.
		/// </param>
		/// <returns>
		/// true if this TemplateItemDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this TemplateItemDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The ITemplateItem value to locate in this TemplateItemDictionary.
		/// </param>
		/// <returns>
		/// true if this TemplateItemDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(ITemplateItem value)
		{
			foreach (ITemplateItem item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this TemplateItemDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this TemplateItemDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this TemplateItemDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}