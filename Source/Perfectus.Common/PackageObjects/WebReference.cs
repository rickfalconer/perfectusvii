using System;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Common.WebServiceSystem;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for WebReference.
	/// </summary>
	[Serializable]
	public sealed class WebReference : DataSource, ISerializable
	{
		private Uri wsdlLocation = null;

		private string layout;

		private bool cacheResults = false;

		[Browsable(true)]
        [DisplayRanking( 4 )]
        public bool CacheResults
		{
			get { return cacheResults; }
			set { cacheResults = value; }
		}

		[Browsable(false)]
		public string LayoutXml
		{
			get { return layout; }
		}

		private WebReference() : base()
		{
			Name = GetUniqueName(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.WebReference.NewItemName"),null);
		}

		[Browsable(true)]
        [DisplayRanking( 2 )]
        //TODO: Uri editor for HTTP, HTTPS maybe?
		public string WebServiceAddress
		{
			get { return wsdlLocation == null ? null : wsdlLocation.AbsoluteUri; }
			set
			{
				wsdlLocation = new Uri(value);
				OnPropertyChanged("WebServiceAddress");
			}
		}

		public WebReference(Uri address, string layoutXml) : this()
		{
			this.wsdlLocation = address;
			if (layoutXml == null)
			{
				ResetObject();
			}
			else
			{
				this.layout = layoutXml;
			}

			Name = address.ToString();
		}

		public WebReference(Uri address) : this(address, null)
		{
		}


		public static string GetLayoutXml(Uri wsdlLocation)
		{
            return GetLayoutXml(wsdlLocation, true);
        }

        public static string GetLayoutXml(Uri wsdlLocation, bool checkCachedProxy)
        {
			if (wsdlLocation == null)
			{
				throw new ArgumentNullException("wsdlLocation");
			}

			Hashtable structTypes = new Hashtable();
			MemoryStream ms = null;
			XmlTextWriter xw = null;
			try
			{
				ms = new MemoryStream();
				xw = new XmlTextWriter(ms, Encoding.UTF8);

				XmlDocument doc = new XmlDocument();

				if (wsdlLocation != null)
				{
					xw.WriteStartDocument();

					xw.WriteStartElement("serviceLayout");

					DynamicProxy proxy = new DynamicProxy(wsdlLocation, checkCachedProxy);

					// Find the struct types
					foreach (Type t in proxy.Assembly.GetTypes())
					{
						if (IsStructType(t))
						{
							structTypes.Add(t, null);
						}
					}

					// Reflect on the proxy object, snapshotting its structure into our lighter model, checking for our supported types along the way.
					foreach (Type t in proxy.Assembly.GetTypes())
					{
						if (isServiceType(t))
						{
							xw.WriteStartElement("serviceType");
							xw.WriteAttributeString("name", t.Name);
                            xw.WriteAttributeString("address", wsdlLocation.ToString());

							foreach (MethodInfo mi in t.GetMethods())
							{
								if (isServiceMethod(mi))
								{
									xw.WriteStartElement("method");
									xw.WriteAttributeString("name", mi.Name);
									foreach (ParameterInfo pi in mi.GetParameters())
									{
										xw.WriteStartElement("parameter");
										WriteParamInfo(xw, pi.Name, pi.ParameterType, structTypes, 0);
										xw.WriteEndElement();
									}

									xw.WriteStartElement("returns");
									WriteParamInfo(xw, "RetVal", mi.ReturnType, structTypes, 0);
									xw.WriteEndElement();
									xw.WriteEndElement();
								}
							}
							xw.WriteEndElement();
						}
					}
					xw.WriteEndElement();
					xw.WriteEndDocument();


					xw.Flush();
					ms.Seek(0, SeekOrigin.Begin);
					doc.Load(ms);

#if  DEBUG
					Console.WriteLine(doc.OuterXml);
#endif
					return doc.OuterXml;
				}
			}
			finally
			{
				if (xw != null)
				{
					xw.Close();
				}
			}

			return null;
		}

		private void ResetObject()
		{
			this.layout = GetLayoutXml(wsdlLocation);
			OnPropertyChanged("LayoutXml");
		}

		private static void WriteParamInfo(XmlTextWriter xw, string name, Type t, Hashtable structTypes, int depth)
		{
			xw.WriteAttributeString("name", name);
			xw.WriteAttributeString("typeName", t.Name);
			xw.WriteAttributeString("isArray", t.IsArray.ToString());

			if (t.IsArray)
			{
				t = t.GetElementType();
			}

			if (structTypes.ContainsKey(t))
			{
				xw.WriteAttributeString("struct", "true");

				foreach (FieldInfo f in t.GetFields())
				{
					xw.WriteStartElement("field");
					xw.WriteAttributeString("name", f.Name);
					xw.WriteAttributeString("typeName", t.Name);
					xw.WriteAttributeString("isArray", t.IsArray.ToString());


//					if (IsStructType(f.FieldType))
//					{
//						if (depth <= 5)
//						{
//							depth ++;
//							WriteParamInfo(xw, f.Name, f.FieldType, structTypes, depth);
//						}
//						else
//						{
//							xw.WriteAttributeString("name", f.Name);
//						}
//					}

					xw.WriteEndElement();
				}
			}
			else
			{
			}

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Checks a type (class) to see if is a struct.
		/// </summary>
		/// <param name="t">The type (class) to check.</param>
		/// <returns>Returns true if the class is marked with XmlTypeAttribute</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		private static bool IsStructType(Type t)
		{
			return (t.GetCustomAttributes(typeof (XmlTypeAttribute), true).Length > 0);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Checks a type (class) to see if it is marked as a web service.
		/// </summary>
		/// <param name="t">The type (class) to check.</param>
		/// <returns>Returns true if the class is marked with WebServiceBindingAttribute</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		private static bool isServiceType(Type t)
		{
			return (t.GetCustomAttributes(typeof (WebServiceBindingAttribute), true).Length > 0);
		}

		private static bool isServiceMethod(MethodInfo mi)
		{
			return (mi.GetCustomAttributes(typeof (SoapDocumentMethodAttribute), true).Length > 0);
		}

		#region ISerializable Members

		public WebReference(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.wsdlLocation = (Uri) info.GetValue("wsdlLocation", typeof (Uri));
			this.layout = info.GetString("layout");
			this.cacheResults = info.GetBoolean("cacheResults");
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("wsdlLocation", this.wsdlLocation);
			info.AddValue("layout", this.layout);
			info.AddValue("cacheResults", this.cacheResults);

		}

		#endregion
	}
}
