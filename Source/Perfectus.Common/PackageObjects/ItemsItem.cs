using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for ItemsItem.
	/// </summary>
	[Serializable]
	public struct ItemsItem
	{
		private string _display;

		public string Display
		{
			get { return _display.Trim('\n','\r'); }
			set { _display = value.Trim('\n','\r'); }
		}

		private string _value;

		public string Value
		{
			get { return _value.Trim('\n','\r'); }
			set { _value = value.Trim('\n','\r'); }
		}
	}
}