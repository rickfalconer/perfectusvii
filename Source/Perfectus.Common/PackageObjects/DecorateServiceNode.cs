using System.Windows.Forms;
using System.Xml;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common
{
	public enum TreeIcons
	{
		URL,
		Method,
		Class,
		Param,
		Result,
		StructField,
		FolderOpen,
		FolderClosed,
		Page,
		DateFunction,
		Question,
		Outcome,
		Template,
		Interview,
		ArithmeticFunction,
		Server,
		Rule,
		Converter,
		Distributor,
		StartPage,
		SimpleOutcome,
		FolderSimpleOutcome,
		FolderEasyInterview,
		FolderFunctions,
		FolderOutcomes,
		FolderProcessManagers,
		FolderQuestions,
		FolderTemplates,
		FolderWebReferences,
		FolderPackage,
		ExternalPage,
		DateFunctionCheckedIn,							// Shared Library Images
		DateFunctionCheckedOut,
		DateFunctionCheckedOutOtherUser,				// Checked In
		DateFunctionUnlinked,							// Checked Out
		ArithmeticFunctionCheckedIn,					// Checked Out to another user (other than current user)
		ArithmeticFunctionCheckedOut,					// Unlinked
		ArithmeticFunctionCheckedOutOtherUser,			// Normal (as given above - question, outcome etc)
		ArithmeticFunctionUnlinked,
		InternalPageCheckedIn,
		InternalPageCheckedOut,
		InternalPageCheckedOutOtherUser,
		InternalPageUnlinked,
		OutcomeCheckedIn,
		OutcomeCheckedOut,
		OutcomeCheckedOutOtherUser,
		OutcomeUnlinked,
		QuestionCheckedIn,
		QuestionCheckedOut,
		QuestionCheckedOutOtherUser,
		QuestionUnlinked,
		SimpleOutcomeCheckedIn,
		SimpleOutcomeCheckedOut,
		SimpleOutcomeCheckedOutOtherUser,
		SimpleOutcomeUnlinked,
		TemplateCheckedIn,
		TemplateCheckedOut,
		TemplateCheckedOutOtherUser,
		TemplateUnlinked,
        StartPageUnlinked,
        StartPageCheckedIn,
        StartPageCheckedOut,
        StartPageCheckedOutOtherUser,
        DocumentIcon,
        AttachmentIcon,
        FolderAttachment,
        AttachmentInterviewIcon,
        DocumentFolderIcon,
        DeliveryIcon,
        BlankIcon,
	}

	public enum ServiceNodeFilter
	{
		ParametersOnly,
		ResultsOnly,
		All
	}

	/// <summary>
	/// Summary description for DecorateServiceNode.
	/// </summary>
	public sealed class DecorateServiceNode
	{
		private DecorateServiceNode()
		{
			//
			//TODO: Add constructor logic here
			//
		}

		public static TagTreeNodeDictionary Decorate(WebReference wr, TreeNode treeNode, string layoutXml, ServiceNodeFilter filter)
		{
			TagTreeNodeDictionary dict = new TagTreeNodeDictionary();
			return Decorate(wr, treeNode, layoutXml, filter, dict);
		}

		public static TagTreeNodeDictionary Decorate(WebReference wr, TreeNode treeNode, string layoutXml, ServiceNodeFilter filter, TagTreeNodeDictionary dict)
		{
			//TODO: Validate schema?

			if(dict == null)
			{
				dict = new TagTreeNodeDictionary();
			}

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(layoutXml);


			XmlNameTable nt = doc.NameTable;
			XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);

			foreach (XmlNode n in doc.SelectNodes(@"/serviceLayout/serviceType", nsmgr))
			{
				TreeNode newTn = new TreeNode();
				newTn.Text = n.Attributes["name"].Value;
				newTn.ImageIndex = (int) TreeIcons.Class;
				newTn.SelectedImageIndex = newTn.ImageIndex;
				string xpath = string.Format(@"/serviceLayout/serviceType[@name='{0}']", n.Attributes["name"].Value);
                string address = (n.Attributes["address"] == null) ? wr.WebServiceAddress : n.Attributes["address"].Value;

				if (wr == null)
				{
					newTn.Tag = xpath;
				}
				else
				{
                    newTn.Tag = string.Format("{0}|{1}|{2}", address, wr.UniqueIdentifier, xpath);
				}

				treeNode.Nodes.Add(newTn);
				dict.Add(newTn.Tag.ToString(), newTn);

				foreach (XmlNode mn in n.SelectNodes(@"method", nsmgr))
				{
					TreeNode newTnMethod = new TreeNode();
					newTnMethod.Text = mn.Attributes["name"].Value;
					newTnMethod.ImageIndex = (int) TreeIcons.Method;
					newTnMethod.SelectedImageIndex = newTnMethod.ImageIndex;
					xpath = string.Format("{0}/method[@name='{1}']", newTn.Tag.ToString(), mn.Attributes["name"].Value);
					newTnMethod.Tag = xpath;

					newTn.Nodes.Add(newTnMethod);
					dict.Add(newTnMethod.Tag.ToString(), newTnMethod);


					if (filter == ServiceNodeFilter.All || filter == ServiceNodeFilter.ParametersOnly)
					{
						foreach (XmlNode fn in mn.SelectNodes(@"parameter", nsmgr))
						{
							BuildFieldNode(fn, newTnMethod, xpath + "/parameter", nsmgr, (int) TreeIcons.Param, dict);
						}
					}

					if (filter == ServiceNodeFilter.All || filter == ServiceNodeFilter.ResultsOnly)
					{
						BuildFieldNode(mn.SelectSingleNode(@"returns"), newTnMethod, newTnMethod.Tag.ToString() + "/returns", nsmgr, (int) TreeIcons.Result, dict);
					}
				}
			}
			return dict;
		}

		private static void BuildFieldNode(XmlNode fn, TreeNode parentNode, string xpath, XmlNamespaceManager nsmgr, int imageIndex, TagTreeNodeDictionary dict)
		{
			TreeNode newPmNode = new TreeNode();
			newPmNode.Text = fn.Attributes["name"].Value;
			newPmNode.ImageIndex = imageIndex;
			newPmNode.SelectedImageIndex = newPmNode.ImageIndex;
			xpath = string.Format("{0}[@name='{1}']", xpath, fn.Attributes["name"].Value);
			newPmNode.Tag = xpath;
			parentNode.Nodes.Add(newPmNode);

			dict.Add(newPmNode.Tag.ToString(), newPmNode);

			if (fn.Attributes["struct"] != null && fn.Attributes["struct"].Value == "true")
			{
				foreach (XmlNode sfn in fn.SelectNodes(@"field", nsmgr))
				{
					BuildFieldNode(sfn, newPmNode, newPmNode.Tag.ToString() + "/field", nsmgr, (int) TreeIcons.StructField, dict);
				}
			}
		}

	}
}
