using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for DataSource.
	/// </summary>
	[Serializable]
	public abstract class DataSource : PackageItem, ISerializable
	{
		protected DataSource() : base()
		{
			Name = GetUniqueName("New datasource","");

		}

		#region ISerializable Members

		public DataSource(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		#endregion
	}
}
