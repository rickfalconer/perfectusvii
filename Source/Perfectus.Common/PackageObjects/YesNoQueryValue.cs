using System;
using System.Collections.Generic;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	[Serializable]
	public sealed class YesNoQueryValue : IPackageItemContainer, ICloneable
	{
		public enum YesNoQueryValueType
		{
			YesNo,
			Query,
			NoValue
		}


		private YesNoQueryValueType valueType;
		private bool boolValue;
		private Query queryValue;

		public YesNoQueryValue()
		{
			valueType = YesNoQueryValueType.NoValue;
			boolValue = false;
			queryValue = null;
		}

		public bool IsDefinitelyNo()
		{
			return (valueType == YesNoQueryValueType.YesNo && ! boolValue);
		}

		[Obsolete]
		public bool Evaluate(bool defaultValue)
		{
			return Evaluate(defaultValue, AnswerProvider.Caller.System);
		}

		public bool Evaluate(bool defaultValue, AnswerProvider.Caller caller)
		{
			switch (valueType)
			{
				case YesNoQueryValueType.YesNo:
					return YesNoValue;
				case YesNoQueryValueType.Query:
					return queryValue.EvaluateBool(caller);
				case YesNoQueryValueType.NoValue:
				default:
					return defaultValue;
			}
			
		}

		public YesNoQueryValueType ValueType
		{
			get { return valueType; }
			set
			{
				switch (value)
				{
					case YesNoQueryValueType.Query:
						boolValue = false;
						break;
					case YesNoQueryValueType.YesNo:
						queryValue = null;
						break;
					case YesNoQueryValueType.NoValue:
						boolValue = false;
						queryValue = null;
						break;
				}
				valueType = value;
			}
		}

		public bool YesNoValue
		{
			get { return boolValue; }
			set
			{
				boolValue = value;
				ValueType = YesNoQueryValueType.YesNo;
			}
		}

		public Query QueryValue
		{
			get { return queryValue; }
			set
			{
				queryValue = value;
				ValueType = YesNoQueryValueType.Query;
			}
		}

		public bool ContainsReferenceTo(PackageItem item)
		{
			if (queryValue != null)
			{
				return queryValue.ContainsReferenceTo(item);
			}
			else
			{
				return false;
			}
		}

		#region sharedlibrary

		public void SetDependentLibraryItems(List<LibraryItem> dependentLibraryItems)
		{	
			if(this.queryValue != null)
			{
				SetDependentObjects(dependentLibraryItems, queryValue.QueryExpression.RootExpressionNode);
			}		
		}

		private void SetDependentObjects(List<LibraryItem> dependentLibraryItems, PackageItem packageItem)
		{
			if (packageItem is ExpressionNode)
			{
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand1);
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand2);
			}
			else
			{
				if (packageItem != null && !(packageItem is PackageConstant))
				{
					// Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
                    if ((((LibraryItem)packageItem).RelativePath == null || ((LibraryItem)packageItem).RelativePath == string.Empty) &&
                        !dependentLibraryItems.Contains((LibraryItem) packageItem))
					{
						dependentLibraryItems.Add((LibraryItem) packageItem);
					}
				}
			}
		}

		public void ReconnectLibraryItems(Package package)
		{		
			if(this.queryValue != null)
			{
				this.queryValue.ParentPackage = package;

				if(queryValue.QueryExpression != null)
				{
					this.queryValue.QueryExpression.ParentPackage = package;
					ReconnectDependentObjects(this.queryValue.QueryExpression.RootExpressionNode, package);
					queryValue.QueryExpression.CreateTableFromExpression();
				}				
			}
		}

		private void ReconnectDependentObjects(ExpressionNode expressionNode, Package package)
		{
			expressionNode.ParentPackage = package;

			// only reconnect if the expression is valid
			if (expressionNode.Operand1 != null && expressionNode.Operand2 != null) 
			{
				// Handle Operand 1 - nested or not
				if (expressionNode.Operand1.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand1, package);
				}
				else
				{
					// Connect library item
					expressionNode.Operand1 = GetDependentPackageItem((LibraryItem)expressionNode.Operand1, package);
					expressionNode.Operand1.ParentPackage = package;
				}
		
				// Handle Operand 2 - nested or not
				if (expressionNode.Operand2.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand2, package);
				}
				else
				{
					// Connect library item
					expressionNode.Operand2 = GetDependentPackageItem((LibraryItem)expressionNode.Operand2, package);
					expressionNode.Operand2.ParentPackage = package;
				}
			}
		}

		private PackageItem GetDependentPackageItem(LibraryItem libraryItem, Package package)
		{
			object itemValue;
			PerfectusDataType itemDataType;
			PackageItem packageItem = null;

			// Attempt to get the item from the package
			packageItem = package.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);

			if (libraryItem.LibraryType == LibraryItemType.None)
			{
				if (((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty() == PerfectusDataType.Date)
				{
					itemDataType = PerfectusDataType.Date;
					itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value.ToString();
					
                    if (Globals.IsDate(itemValue.ToString()))
                    {
					itemValue = DateTime.Parse(itemValue.ToString()).ToLocalTime().ToShortDateString();
				}
				else
				{
                        itemValue = itemValue.ToString();
                    }
				}
				else
				{
					itemDataType = ((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty();
					itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value;
				}

				// Set the packge constant
				packageItem = new PackageConstant(itemDataType, itemValue);
			}

			if (packageItem == null)
			{
                Exception ex = new Exception(libraryItem.Name + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage")); //TODO - SL - RESOURCE
				ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				throw new NullReferenceException(libraryItem.Name); 
			}
		
			return packageItem;
		}

		#endregion

		#region ICloneable Members

		public object Clone()
		{
			object newObj = this.MemberwiseClone();
			YesNoQueryValue newYnqv = (YesNoQueryValue)newObj;

			if(newYnqv.queryValue != null)
			{
				newYnqv.queryValue = (Query)queryValue.Clone();
			}
			return newYnqv;
		}

		#endregion
	}
}
