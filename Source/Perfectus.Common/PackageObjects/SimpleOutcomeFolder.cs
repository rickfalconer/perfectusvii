using System;
using System.Runtime.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for SimpleOutcomeFolder.
	/// </summary>
	[Serializable]
	public class SimpleOutcomeFolder : Folder
	{

		public SimpleOutcomeFolder() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.SimpleOutcomeFolder.NewItemName");
		}

		public SimpleOutcomeFolder(string name) : base()
		{
			this.Name = name;			
		}

		/// <summary>
		/// The folders parent folder, read only.
		/// </summary>
		public new Folder ParentFolder
		{
			get {return m_ParentFolder;}
		}
			
		/// <summary>
		/// Creates a new folder and adds it to this folders ChildFolders collection.
		/// </summary>		
		public override void CreateChild()
		{			
			SimpleOutcomeFolder newChildFolder = new SimpleOutcomeFolder();
			newChildFolder.MakeChildOf(this);
			this.m_childFolders.Add(newChildFolder);
			this.ParentPackage.AddFolderToPackage(newChildFolder);
		}

		/// <summary>
		/// Moves a Folder to its very root node.
		/// </summary>
		public override void ChangeToRootFolder()
		{
			if(m_ParentFolder != null)
			{
				m_ParentFolder.ChildFolders.Remove(this);
				m_ParentFolder = null;				
				this.ParentPackage.SimpleOutcomeFolders.Add(this);
                this.Name = this.Name;
			}
		}
		/// <summary>
		/// Moves folder to a different Folder. Returns True if operation successful.		
		/// </summary>
		public override bool MoveToFolder(Folder targetFolder)
		{
			if(targetFolder != null)
			{				
				if(FolderMoveValid(targetFolder))
				{
					if(m_ParentFolder != null)
					{
						m_ParentFolder.ChildFolders.Remove(this);
						m_ParentFolder = null;
					}
					else
					{
						this.ParentPackage.SimpleOutcomeFolders.Remove(this);
					}

					this.m_ParentFolder = targetFolder;
					targetFolder.ChildFolders.Add(this);
                    this.Name = this.Name;

					return true;
				}
				else
				{
					//throw new Exception("This operation is invalid, You can not move a folder to one its childrent folders.");
					return false;
				}
			}
			return false;
		}		

		#region ISerializable Members

		public SimpleOutcomeFolder(SerializationInfo info, StreamingContext context) : base (info, context)
		{			
					
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);			
		}

		#endregion
	}
}
