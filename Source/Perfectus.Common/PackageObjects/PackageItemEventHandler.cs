namespace Perfectus.Common.PackageObjects
{
	public delegate void PackageItemEventHandler(object sender, PackageItemEventArgs e);
}