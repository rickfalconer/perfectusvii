using System;
using System.Collections;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Folder
	/// </summary>
	[Serializable]
	public sealed class FolderCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FolderCollection class.
		/// </summary>
		internal FolderCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the FolderCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new FolderCollection.
		/// </param>
		internal FolderCollection(Folder[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the FolderCollection class, containing elements
		/// copied from another instance of FolderCollection
		/// </summary>
		/// <param name="items">
		/// The FolderCollection whose elements are to be added to the new FolderCollection.
		/// </param>
		internal FolderCollection(FolderCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this FolderCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this FolderCollection.
		/// </param>
		internal void AddRange(Folder[] items)
		{
			foreach (Folder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another FolderCollection to the end of this FolderCollection.
		/// </summary>
		/// <param name="items">
		/// The FolderCollection whose elements are to be added to the end of this FolderCollection.
		/// </param>
		internal void AddRange(FolderCollection items)
		{
			foreach (Folder item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Folder to the end of this FolderCollection.
		/// </summary>
		/// <param name="value">
		/// The Folder to be added to the end of this FolderCollection.
		/// </param>	
		public void Add(Folder value)
		{					
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Folder value is in this FolderCollection.
		/// </summary>
		/// <param name="value">
		/// The Folder value to locate in this FolderCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this FolderCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Folder value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this FolderCollection
		/// </summary>
		/// <param name="value">
		/// The Folder value to locate in the FolderCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Folder value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the FolderCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Folder is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Folder to insert.
		/// </param>
		internal void Insert(int index, Folder value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Folder at the given index in this FolderCollection.
		/// </summary>
		public Folder this[int index]
		{
			get { return (Folder) this.List[index]; }
			//set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Folder from this FolderCollection.
		/// </summary>
		/// <param name="value">
		/// The Folder value to remove from this FolderCollection.
		/// </param>
		internal void Remove(Folder value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by FolderCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(FolderCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Folder Current
			{
				get { return (Folder) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Folder) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this FolderCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}
