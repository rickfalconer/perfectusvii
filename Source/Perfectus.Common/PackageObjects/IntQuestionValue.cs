using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for TextQuestionValue.
	/// </summary>
	[Serializable]
	public sealed class IntQuestionValue : IPackageItemContainer
	{
		public enum IntQuestionValueType
		{
			Int,
			Question,
			NoValue
		}


		private IntQuestionValueType valueType;
		private int intValue;
		private Question questionValue;

		public IntQuestionValue()
		{
			valueType = IntQuestionValueType.NoValue;
			intValue = 0;
			questionValue = null;
		}

		public int Evaluate(AnswerProvider.Caller caller)
		{
			switch (valueType)
			{
				case IntQuestionValueType.Int:
					return intValue;
				default:
					Question q = questionValue;
					if (q != null)
					{
						object a = q.GetSingleAnswer(caller);
						if (a != null)
						{
							try
							{
								return int.Parse(a.ToString());
							}
							catch
							{
								return 0;
							}
							
						}
					}
					return 0;
					/*
					if (q != null && q.Answers.Count > 0)
					{
						return q.GetSingleAnswer(caller).ToString();
					}
					else
					{
						return null;
					}*/
			}
		}

		public IntQuestionValueType ValueType
		{
			get { return valueType; }
			set
			{
				switch (value)
				{
					case IntQuestionValueType.Int:
						questionValue = null;
						break;
					case IntQuestionValueType.Question:
						intValue = 0;
						break;
					case IntQuestionValueType.NoValue:
						intValue = 0;
						questionValue = null;
						break;
				}
				valueType = value;
			}
		}

		public int IntValue
		{
			get { return intValue; }
			set
			{
				intValue = value;
				ValueType = IntQuestionValueType.Int;
			}
		}

		public Question QuestionValue
		{
			get { return questionValue; }
			set
			{
				questionValue = value;
				ValueType = IntQuestionValueType.Question;
			}
		}

		#region ICloneable Members

		public object Clone()
		{
			object newObj = this.MemberwiseClone();
			IntQuestionValue newIqv = (IntQuestionValue)newObj;

			return newIqv;

		}

		#endregion

		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			return questionValue == item;
		}

		#endregion
	}
}