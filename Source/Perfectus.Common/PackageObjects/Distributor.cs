using System;
using System.Runtime.Serialization;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Distributor.
	/// </summary>
	[Serializable]
    public sealed class Distributor : PluginBase, IPackageItemRequiresUniqueName
	{
		private Distributor()
		{
		}

		public Distributor(PluginDescriptor descriptorFromServer) : base(descriptorFromServer)
		{
		}

		public Distributor(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}



        #region IPackageItemRequiresUniqueName Members

        public bool RequiresUniqueName
        {
            get { return false; }
        }

        #endregion
    }
}