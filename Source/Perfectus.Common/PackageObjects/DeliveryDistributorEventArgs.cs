using System;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for RuleTemplateEventArgs.
	/// </summary>
    public sealed class DeliveryDocumentEventArgs : EventArgs
	{
		private Delivery delivery;
        private IDocument document;

        public IDocument Document
		{
            get { return document; }
		}

        public Delivery Delivery
		{
			get { return delivery; }
		}

        public DeliveryDocumentEventArgs(Delivery delivery, IDocument document)
		{
            this.document = document;
			this.delivery = delivery ;
		}
	}
}

