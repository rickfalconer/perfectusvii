using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	public abstract class InterviewPage : LibraryItem, ISerializable
	{
		#region Private Member Variables

		// Defines the Shared Library type for this class
		private LibraryItemType _LibraryItemType = LibraryItemType.None;

		private Interview parentInterview;
		private PageQueryValue m_nextPage;
		private YesNoQueryValue visible;
        private YesNoQueryValue showPageInHistory;
		private int progress = -1;
		private bool seen;
		private PageItemCollection items = new PageItemCollection();
        private TextQuestionQueryValue pageTitle;

		#endregion
		#region Properties & Constructor

		/// <summary>
		///		Public accessor for the Shared Library type for the class
		/// </summary>
		[Browsable(false)]
		public override LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

        [Category("Data")]
        [Browsable(true)]
        [DisplayName("Page Title")]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.TextQuestionQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter, Studio")]
        [DisplayRanking( 2 )]
        public TextQuestionQueryValue PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                OnPropertyChanged("PageTitle");
            }
        }

        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof(UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 8 )]
        public YesNoQueryValue ShowPageInHistory
        {
            get { return showPageInHistory; }
            set
            {
                showPageInHistory = value;
            }
        }

		[Browsable(false)]
        [XmlIgnore]
		public int Progress
		{
			get { return progress;  }
			set { progress = value; }
		}

		[Browsable(false)]
        [XmlIgnore]
		public bool Seen
		{
			get { return seen;}
			set { seen = value;}
		}

		[Browsable(false)]
        [XmlIgnore]
		public Interview ParentInterview
		{
			get { return parentInterview; }
			set { parentInterview = value; }
		}

		//TODO: Items is only used by the derived class internalPage, it's in the base class because of backwards compatability, 
		// there is a lot of code on the client and server which thinks Items is a property existing on an InterviewPage.
		[Browsable(false)]
		public PageItemCollection Items
		{
			get { return items; }
			set { items = value; }
		}

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.PageQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.PageQueryConverter, Studio")]
        [XmlIgnore]
        [DisplayRanking( 4 )]
        public PageQueryValue NextPage
		{
			get { return m_nextPage; }
			set 
			{
                m_nextPage = value;
				m_nextPage.ValueChanged += new EventHandler(m_nextPage_ValueChanged);
			}
		}

		[Category("Display")]
		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        [DisplayRanking( 6 )]
        public YesNoQueryValue Visible
		{
			get { return visible;  }
			set { visible = value; }
		}		

		public InterviewPage() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.InterviewPage.NewItemName");
			m_nextPage = new PageQueryValue();
			m_nextPage.ValueChanged += new EventHandler(m_nextPage_ValueChanged);
			visible = new YesNoQueryValue();
			visible.YesNoValue = true;
            showPageInHistory = new YesNoQueryValue();
            showPageInHistory.YesNoValue = true;
            pageTitle = new TextQuestionQueryValue();
		}

		#endregion
		#region Methods

		protected void m_nextPage_ValueChanged(object sender, EventArgs e)
		{
			OnPropertyChanged("NextPage");
		}

		#endregion
		#region ISerializable Members
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("items", items);
			info.AddValue("parentInterview", parentInterview);
            info.AddValue("showPageInHistory", showPageInHistory);
			info.AddValue("nextPage", m_nextPage);
			info.AddValue("visible", visible);
			info.AddValue("progress", progress);
			info.AddValue("seen", seen);
            info.AddValue("pageTitle", pageTitle);
		}

		public InterviewPage(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			items = (PageItemCollection) info.GetValue("items", typeof (PageItemCollection));
			parentInterview = (Interview) info.GetValue("parentInterview", typeof (Interview));
			m_nextPage = (PageQueryValue) info.GetValue("nextPage", typeof (PageQueryValue));

            try
            {
                pageTitle = (TextQuestionQueryValue)(info.GetValue("pageTitle", typeof(TextQuestionQueryValue)));
            }
            catch 
            {
                pageTitle = new TextQuestionQueryValue();
                pageTitle.TextValue = string.Empty;
            }

            try
            {
                showPageInHistory = (YesNoQueryValue)(info.GetValue("showPageInHistory", typeof(YesNoQueryValue)));
            }
            catch
            {
                showPageInHistory = new YesNoQueryValue();
                showPageInHistory.YesNoValue = true;
            }

			try
			{
				visible = (YesNoQueryValue) (info.GetValue("visible", typeof (YesNoQueryValue)));
			}
			catch
			{
				visible = new YesNoQueryValue();
				visible.YesNoValue = true;
			}

			m_nextPage.ValueChanged += new EventHandler(m_nextPage_ValueChanged);

			try
			{
				progress = info.GetInt32("progress");
			}
			catch
			{
			}

			try
			{
				seen = info.GetBoolean("seen");
			}
			catch
			{
			}
		}
		
		#endregion
	}
}
