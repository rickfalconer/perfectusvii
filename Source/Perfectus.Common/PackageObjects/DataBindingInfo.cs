using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
    public enum WebReferenceComponent
    {
        Address = 0,
        GUID = 1,
        BindingInformation = 2
    }

    public enum BindingProperty
    {
        Answer,
        ItemsValue,
        ItemsDisplay,
        Parameters
    }

	/// <summary>
	/// Summary description for DataBinding.
	/// </summary>
	[Serializable]
	public sealed class DataBindingInfo : ICloneable, ISerializable
	{
        public string AnswerBinding;
		public string ItemsValueBinding;
        public string ItemsDisplayBinding;

        [XmlElement(ElementName = "ParameterBindings")]
		public string[] ParameterBindings;

        public DataBindingInfo() { }

        public static Int32 GetWebComponentIndex(WebReferenceComponent webReferenceComponent)
        {
            return Convert.ToInt32(webReferenceComponent);
        }

        public void Upgrade(Question question, Package package)
        {
            // Maybe create a new 'Default Answer' from the old binding.
            if (MigrateQuestionBinding(package, AnswerBinding, question))
                return;

            if (AnswerBinding != null || AnswerBinding != string.Empty)
            {
                AnswerBinding = UpdateBinding(package, AnswerBinding, BindingProperty.Answer);
            }

            if (ItemsValueBinding != null || ItemsValueBinding != string.Empty)
            {
                ItemsValueBinding = UpdateBinding(package, ItemsValueBinding, BindingProperty.ItemsValue);
            }

            if (ItemsDisplayBinding != null || ItemsDisplayBinding != string.Empty)
            {
                ItemsDisplayBinding = UpdateBinding(package, ItemsDisplayBinding, BindingProperty.ItemsDisplay);
            }

            if (ParameterBindings != null && ParameterBindings.Length > 0)
            {
                for (int i=0; i < ParameterBindings.Length; i++)
                {
                    ParameterBindings[i] = UpdateBinding(package, ParameterBindings[i], BindingProperty.Parameters, i);
                }
            }
        }

        private Boolean MigrateQuestionBinding(Package package, string binding, Question question)
        {
            // Find the question and create a 'new style default answer',
            // then remove the old binding.
            if (binding != null && binding.StartsWith("Q"))
            {
                Question q = package.QuestionByGuid[new Guid(binding.Substring(1))];
                question.DefaultAnswer.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Question;
                question.DefaultAnswer.QuestionValue = q;
                question.DataBindings = null;
                return true;
            }
            return false;
        }

        public string GetWebServiceComponentForAnswerBinding(WebReferenceComponent webReferenceComponent)
        {
            if (AnswerBinding.Length > 0)
            {
                switch (webReferenceComponent)
                { 
                    case WebReferenceComponent.Address:
                        return GetAddress(AnswerBinding);
                    case WebReferenceComponent.GUID:
                        return GetGuid(AnswerBinding);
                    case WebReferenceComponent.BindingInformation:
                        return GetBindingInformation(AnswerBinding);
                }
            }

            return string.Empty;
        }

        public string GetWebServiceComponentForItemsValueBinding(WebReferenceComponent webReferenceComponent)
        {
            if (ItemsValueBinding.Length > 0)
            {
                switch (webReferenceComponent)
                {
                    case WebReferenceComponent.Address:
                        return GetAddress(ItemsValueBinding);
                    case WebReferenceComponent.GUID:
                        return GetGuid(ItemsValueBinding);
                    case WebReferenceComponent.BindingInformation:
                        return GetBindingInformation(ItemsValueBinding);
                }
            }

            return string.Empty;
        }

        public string GetWebServiceComponentForItemsDisplayBinding(WebReferenceComponent webReferenceComponent)
        {
            if (ItemsDisplayBinding.Length > 0)
            {
                switch (webReferenceComponent)
                {
                    case WebReferenceComponent.Address:
                        return GetAddress(ItemsDisplayBinding);
                    case WebReferenceComponent.GUID:
                        return GetGuid(ItemsDisplayBinding);
                    case WebReferenceComponent.BindingInformation:
                        return GetBindingInformation(ItemsDisplayBinding);
                }
            }

            return string.Empty;
        }

        public string GetWebServiceComponentForParameterBindings(WebReferenceComponent webReferenceComponent, int index)
        {
            for (int i = 0; i < ParameterBindings.Length; i++)
            {
                if (i == index)
                {
                    if (ParameterBindings[i].Length > 0)
                    {
                        switch (webReferenceComponent)
                        {
                            case WebReferenceComponent.Address:
                                return GetAddress(ParameterBindings[i]);
                            case WebReferenceComponent.GUID:
                                return GetGuid(ParameterBindings[i]);
                            case WebReferenceComponent.BindingInformation:
                                return GetBindingInformation(ParameterBindings[i]);
                        }
                    }
                }
            }

            return string.Empty;
        }

        private string GetAddress(string dataBinding)
        {
            if (dataBinding.Length > 0)
            {
                string[] webComponents = dataBinding.Split("|".ToCharArray());

                if (webComponents.Length == 3)
                {
                    return webComponents[GetWebComponentIndex(WebReferenceComponent.Address)];
                }
                else
                {
                    // Old value - does not include address
                    return string.Empty;
                }
            }

            return string.Empty;
        }

        private string GetGuid(string dataBinding)
        {
            if (dataBinding.Length > 0)
            {
                string[] webComponents = dataBinding.Split("|".ToCharArray());

                if (webComponents.Length == 3)
                {
                    return webComponents[GetWebComponentIndex(WebReferenceComponent.GUID)];
                }
                else
                {
                    // Old value - does not include address
                    return webComponents[GetWebComponentIndex(WebReferenceComponent.GUID) - 1];
                }
            }

            return string.Empty;
        }

        public void ReEvaluateUrlByGuid(Package package, Guid guid)
        {
            string url = GetWebServiceUrlByGuid(package, guid);

            if (AnswerBinding != null && AnswerBinding != string.Empty)
            {
                if (String.Compare(GetWebServiceComponentForAnswerBinding(WebReferenceComponent.Address), url, true) != 0 &&
                    new Guid(GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID)).CompareTo(guid) == 0)
                {
                    AnswerBinding = string.Format("{0}|{1}|{2}",
                        url,
                        GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID),
                        GetWebServiceComponentForAnswerBinding(WebReferenceComponent.BindingInformation));
                }
            }

            if (ItemsValueBinding != null && ItemsValueBinding != string.Empty && ItemsValueBinding[0] != 'Q')
            {
                if (String.Compare(GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.Address), url, true) != 0 &&
                    new Guid(GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.GUID)).CompareTo(guid) == 0)
                {
                    ItemsValueBinding = string.Format("{0}|{1}|{2}",
                        url,
                        GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.GUID),
                        GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.BindingInformation));
                }
            }

            if (ParameterBindings != null && ParameterBindings.Length > 0)
            {

                for (int i = 0; i < ParameterBindings.Length; i++)
                {
                    if (String.Compare(GetWebServiceComponentForParameterBindings(WebReferenceComponent.Address, i), url, true) != 0 &&
                    new Guid(GetWebServiceComponentForParameterBindings(WebReferenceComponent.GUID, i)).CompareTo(guid) == 0)
                    {
                        ParameterBindings[i] = string.Format("{0}|{1}|{2}",
                            url,
                            GetWebServiceComponentForParameterBindings(WebReferenceComponent.GUID, i),
                            GetWebServiceComponentForParameterBindings(WebReferenceComponent.BindingInformation, i));
                    }
                }
            }
        }

        private string GetBindingInformation(string dataBinding)
        {
            if (dataBinding.Length > 0)
            {
                string[] webComponents = dataBinding.Split("|".ToCharArray());

                if (webComponents.Length == 3)
                {
                    return webComponents[GetWebComponentIndex(WebReferenceComponent.BindingInformation)];
                }
                else
                {
                    // Old value - does not include address
                    return webComponents[GetWebComponentIndex(WebReferenceComponent.BindingInformation) - 1];
                }
            }

            return string.Empty;
        }

        private string UpdateBinding(Package package, string binding, BindingProperty bindingProperty)
        {
            return UpdateBinding(package, binding, bindingProperty, -1);
        }

        private string UpdateBinding(Package package, string binding, BindingProperty bindingProperty, int index)
        {
            if (binding != null && !binding.StartsWith("Q") && !binding.StartsWith("http"))
            { 
                // Upgrade

                string webReferenceGuid = string.Empty;
                switch (bindingProperty)
                {
                    case BindingProperty.Answer:
                        webReferenceGuid = GetWebServiceComponentForAnswerBinding(WebReferenceComponent.GUID);
                        break;
                    case BindingProperty.ItemsDisplay:
                        webReferenceGuid = GetWebServiceComponentForItemsDisplayBinding(WebReferenceComponent.GUID);
                        break;
                    case BindingProperty.ItemsValue:
                        webReferenceGuid = GetWebServiceComponentForItemsValueBinding(WebReferenceComponent.GUID);
                        break;
                    case BindingProperty.Parameters:
                        webReferenceGuid = GetWebServiceComponentForParameterBindings(WebReferenceComponent.GUID, index);
                        break;
                }

                Guid guid;
                bool isGuid = Globals.IsGuid(webReferenceGuid, out guid);

                if (isGuid)
                {
                    string url = GetWebServiceUrlByGuid(package, guid);

                    if (url != string.Empty && url.StartsWith("http"))
                    {
                        binding = string.Format("{0}|{1}", url, binding);
                    }
                }
            }

            // Return the binding value
            return binding;
        }

        private string GetWebServiceUrlByGuid(Package package, Guid guid)
        {
            foreach (DataSource dataSource in package.DataSources)
            {
                if (dataSource is WebReference && dataSource.UniqueIdentifier == guid)
                {
                    return ((WebReference)dataSource).WebServiceAddress;
                }
            }

            return string.Empty;
        }

		#region ICloneable Members

		public object Clone()
		{
			return this.MemberwiseClone();
		}

		#endregion

        #region ISerializable Members

        public DataBindingInfo(SerializationInfo info, StreamingContext context)
        {
            this.AnswerBinding = info.GetString("AnswerBinding");
            this.ItemsDisplayBinding = info.GetString("ItemsDisplayBinding");
            this.ItemsValueBinding = info.GetString("ItemsValueBinding");
            this.ParameterBindings = (string[])info.GetValue("ParameterBindings", typeof(string[]));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("AnswerBinding", this.AnswerBinding);
            info.AddValue("ItemsDisplayBinding", this.ItemsDisplayBinding);
            info.AddValue("ItemsValueBinding", this.ItemsValueBinding);
            info.AddValue("ParameterBindings", this.ParameterBindings);
        }

        #endregion
	}
}
