using System;
using System.Collections;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Page
	/// </summary>
	[Serializable]
	public sealed class InterviewPageCollection : CollectionBase, IXmlSerializable
	{
		/// <summary>
		/// Initializes a new empty instance of the PageCollection class.
		/// </summary>
		public InterviewPageCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PageCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PageCollection.
		/// </param>
		internal InterviewPageCollection(InterviewPage[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PageCollection class, containing elements
		/// copied from another instance of PageCollection
		/// </summary>
		/// <param name="items">
		/// The PageCollection whose elements are to be added to the new PageCollection.
		/// </param>
		internal InterviewPageCollection(InterviewPageCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this PageCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PageCollection.
		/// </param>
		internal void AddRange(InterviewPage[] items)
		{
			foreach (InterviewPage item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PageCollection to the end of this PageCollection.
		/// </summary>
		/// <param name="items">
		/// The PageCollection whose elements are to be added to the end of this PageCollection.
		/// </param>
		internal void AddRange(InterviewPageCollection items)
		{
			foreach (InterviewPage item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Page to the end of this PageCollection.
		/// </summary>
		/// <param name="value">
		/// The Page to be added to the end of this PageCollection.
		/// </param>
		public void Add(InterviewPage value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Page value is in this PageCollection.
		/// </summary>
		/// <param name="value">
		/// The Page value to locate in this PageCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PageCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(InterviewPage value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PageCollection
		/// </summary>
		/// <param name="value">
		/// The Page value to locate in the PageCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(InterviewPage value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PageCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Page is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Page to insert.
		/// </param>
		internal void Insert(int index, InterviewPage value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Page at the given index in this PageCollection.
		/// </summary>
		public InterviewPage this[int index]
		{
			get { return (InterviewPage) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Page from this PageCollection.
		/// </summary>
		/// <param name="value">
		/// The Page value to remove from this PageCollection.
		/// </param>
		internal void Remove(InterviewPage value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PageCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(InterviewPageCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public InterviewPage Current
			{
				get { return (InterviewPage) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (InterviewPage) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PageCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}

		#region IXmlSerializable Members

		/// <summary>
		/// This is required for the interface, but producing an XmlSchema is not required for our purposes,
		/// and therefore we simply return null.
		/// </summary>
		/// <returns>null</returns>
		public XmlSchema GetSchema()
		{
			return null;
		}

		public void WriteXml(XmlWriter w)
		{	
			//ExpressionNode expressionNode;

			// Start the dependencies section
			w.WriteStartElement("InterviewPageCollection");

			w.WriteElementString("Name", "Example_Name");
			w.WriteElementString("Value", "Example_Value");
			/*
			 * EXAMPLE CODE
			 * 
			
			// Unwrap the expression node if required
			if (this.Operand1.GetType() == typeof(ExpressionNode))
				expressionNode = ((ExpressionNode)(((PackageItem)(this.Operand2))));
			else
				expressionNode = this;

			// Operand1 - cast this object into a specific ExpressionNode Object and get its Operand1 property
			WriteDependantInformation(w, expressionNode.Operand1, msOPERAND1);

			// Operand2 - cast this object into a specific ExpressionNode Object and get its Operand2 property
			WriteDependantInformation(w, expressionNode.Operand2, msOPERAND2);

			// ExpressionOperation
			w.WriteElementString(msEXPRESSIONOPERATION, this.ExpressionOperator.ToString());
			
			*
			*
			*/

			// End the dependencies section
			w.WriteEndElement();
		}

		public void ReadXml(XmlReader r)
		{
			// Not Required
		}

		//XML_TEST - This should be refactored into a new class?
		private void WriteDependantInformation(XmlWriter w, PackageItem packageItem, string elementName)
		{
			/*
			// Write out the starting element
			w.WriteStartElement(elementName);
			
			// Question
			if(packageItem.GetType() == typeof(Question))
			{
				// e.g. <Name>question_name</Name>
				w.WriteElementString(msNAME, ((Question)packageItem).Name);

				// e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
				w.WriteElementString(msUNIQUE_IDENTIFIER, ((Question)packageItem).UniqueIdentifier.ToString());

				// e.g. <Type>Perfectus.Common.PackageObjects.Question</Type>
				w.WriteElementString(msTYPE, typeof(Question).ToString());
				
			}

			// PackageConstant
			if(packageItem.GetType() == typeof(PackageConstant))
			{
				// e.g. <UniqueIdentifier>7a66d7a7-4d29-462c-a34a-d9a143bb9872</UniqueIdentifier>
				w.WriteElementString(msUNIQUE_IDENTIFIER, ((PackageConstant)packageItem).UniqueIdentifier.ToString());

				// e.g. <Value>question_name</Value>
				w.WriteElementString(msVALUE, ((PackageConstant)packageItem).Value.ToString());

				// e.g. <DataType>Text</DataType>
				w.WriteElementString(msDATATYPE, ((PackageConstant)packageItem).DataType.ToString());

				// e.g. <Type>Perfectus.Common.PackageObjects.PackageConstant</Type>
				w.WriteElementString(msTYPE, typeof(PackageConstant).ToString());
			}

			// Write out the ending element
			w.WriteEndElement();
			*/
		}

		#endregion
	}

}