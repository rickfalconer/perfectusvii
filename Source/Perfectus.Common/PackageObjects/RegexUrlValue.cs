using System;
using System.Xml.Serialization;
using System.Text.RegularExpressions;


namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for TextQuestionValue.
	/// </summary>
	[Serializable]
	public sealed class RegexUrlValue : ICloneable
	{
		public enum RegexUrlValueType
		{
			Regex,
			Url,
			NoValue
		}

		private RegexUrlValueType valueType;
		private string regexValue;
		private Uri urlValue;

		public RegexUrlValue()
		{
			valueType = RegexUrlValueType.NoValue;
			regexValue = null;
			urlValue = null;
		}

		public RegexUrlValueType ValueType
		{
			get { return valueType; }
			set
			{
				switch (value)
				{
					case RegexUrlValueType.Regex:
						urlValue = null;
						break;
					case RegexUrlValueType.Url:
						regexValue = null;
						break;
					case RegexUrlValueType.NoValue:
						regexValue = null;
						urlValue = null;
						break;
				}
				valueType = value;
			}
		}

		public string RegexValue
		{
			get { return regexValue; }
			set
			{
				regexValue = value;
				ValueType = RegexUrlValueType.Regex;
			}
		}

		[XmlIgnore] //TODO - SL - Do I need this value - Uri UrlValue
		public Uri UrlValue
		{
			get { return urlValue; }
			set
			{
				urlValue = value;
				ValueType = RegexUrlValueType.Url;
			}
		}

        public bool IsValidRegex( string value, ref string msg )
        {
            // Moved from the RegexUrlEditorForm class to support validation within property grid itself FB2302.
            try
            {
                Regex r = new Regex( value );
                r.Match( "abc" );
            }
            catch
            {
                msg = ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString( "Perfectus.Client.UI.PackageObjectEditors.Dialogs.RegexUrlEditorForm.InvalidRegexError" );
                return false;
            }
            
            return true;
        }

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}