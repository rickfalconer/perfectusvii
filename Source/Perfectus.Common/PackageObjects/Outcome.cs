using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Runtime.Serialization;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Outcome.
	/// </summary>
	[Serializable]
	public sealed class Outcome : LibraryItem, ITemplateItem, ISerializable, IPackageItemContainer
	{
		#region Private Member Variables

		private Query definition;
		private string formatString = string.Empty;
		private string formatStringParams = string.Empty;
		private string previewHoverText = string.Empty;
        private WordMLDependentPackageItems wordMLDependentPackageItems;
        Dictionary<string, Guid> dependentPackageItems;

		// Defines the Shared Library type for this class
		private LibraryItemType _LibraryItemType = LibraryItemType.Outcome;

		#endregion
		#region Properties & Constructor

		[Category("SharedLibrary")]
		[Browsable(false)]
		public override LibraryItemType LibraryType
		{
			get { return _LibraryItemType;  }
			set { _LibraryItemType = value; }
		}

		[Category("Display")]
		[Browsable(false)]
		public string FormatString
		{
			get { return formatString;  }
			set { formatString = value; }
		}

		[Category("Display")]
		[Browsable(false)]
		public string FormatStringParams
		{
			get { return formatStringParams;  }
			set { formatStringParams = value; }
		}

		[Category("Display")]
		[Browsable(true)]
        [DisplayRanking( 4 )]
        public string PreviewHoverText
		{
			get { return previewHoverText; }
			set
			{
				previewHoverText = value;
				OnPropertyChanged("PreviewHoverText");
			}
		}

		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor, Studio", typeof(UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.OutcomeConverter, Studio")]
        [DisplayRanking( 2 )]
        public Query Definition
		{
			get { return definition; }
			set { definition = value; }
		}

        [Browsable(false)]
        public WordMLDependentPackageItems WordMLDependentLibraryItems
        {
            get { return wordMLDependentPackageItems; }
            set { wordMLDependentPackageItems = value; }
        }

		public Outcome() : base()
		{
			Name = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.Outcome.NewItemName");
		}

		public Outcome(string name) : base()
		{
			this.Name = name;
		}

		#endregion
		#region Methods

        public override Boolean IsValid(ref String message)
        {
            if (Definition != null && Definition.QueryExpression != null )
            {
                QueryExpression qe = Definition.QueryExpression;

                if (Definition.QueryExpression.Table == null)
                {
                    message = String.Format("[{0}] - {1}", this.Name,
                    ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.Outcome.QueryTableNotValid"));
                    return false;
                }
                else
                {
                    Boolean success = qe.ValidateFromTable(qe.Table, out message);
                    if (!success)
                    {
                        message = String.Format("[{0}] - {1}", this.Name, message);
                        message = message.Replace("\\n", System.Environment.NewLine);
                        message = message.Replace("\\t", " ");
                    }
                    return success;
                }

            }
            return true;
        }

		public override List<LibraryItem> GetDependentLibraryItems()
		{
            Dictionary<string, Guid> orphanedItems;
			List<LibraryItem> dependentLibraryItems = new List<LibraryItem>();
            dependentPackageItems = new Dictionary<string, Guid>();

			// Action if true dependencies
            SetOutcomeActionDependencies(dependentLibraryItems, (OutcomeAction)this.Definition.ActionIfTrue);

			// Action if false dependencies
            SetOutcomeActionDependencies(dependentLibraryItems, (OutcomeAction)this.Definition.ActionIfFalse);

			// Query expression dependencies
            SetDependentObjects(dependentLibraryItems, this.Definition.QueryExpression.RootExpressionNode);

            // Set the dependent WordML Items
            wordMLDependentPackageItems = ParentPackage.PopulateLibraryItems(dependentPackageItems, out orphanedItems);

            // Populate the colleciton of depenendent items
            foreach (LibraryItem libraryItem in wordMLDependentPackageItems)
            {
                dependentLibraryItems.Add(libraryItem);
            }

            //TODO - orphanedItems - need to decide what to do here to handle orphaned items correctly
            if (orphanedItems != null && orphanedItems.Count > 0)
            {
                //throw new System.InvalidOperationException("Orphaned Referenced Package Item Exception");
            }

            return dependentLibraryItems;
		}

		public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

		private void SetOutcomeActionDependencies(List<LibraryItem> dependentLibraryItems, OutcomeAction outcomeAction)
		{
			if (outcomeAction != null)
			{
				SetDependentObjects(dependentLibraryItems, outcomeAction.Function);

				if (outcomeAction.SubQuery != null)
				{
					// Sub query action if true
					SetOutcomeActionDependencies(dependentLibraryItems, (OutcomeAction)outcomeAction.SubQuery.ActionIfTrue);

					// Sub query action if false
					SetOutcomeActionDependencies(dependentLibraryItems, (OutcomeAction)outcomeAction.SubQuery.ActionIfFalse);

					// Sub query query expression
					SetDependentObjects(dependentLibraryItems, outcomeAction.SubQuery.QueryExpression.RootExpressionNode);
				}
                else if (outcomeAction.OfficeOpenXML != null)
                {
                    SetDependentWordMLItems(dependentLibraryItems, outcomeAction.OfficeOpenXML);
                }
			}
		}

		private void SetDependentObjects(List<LibraryItem> dependentLibraryItems, PackageItem packageItem)
		{
			if (packageItem is ExpressionNode)
			{
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand1);
				SetDependentObjects(dependentLibraryItems, ((ExpressionNode)packageItem).Operand2);
			}
			else
			{
				if (packageItem != null && !(packageItem is PackageConstant))
				{
					// Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
					if (!dependentLibraryItems.Contains((LibraryItem) packageItem))
					{
                        if ((((LibraryItem)packageItem).RelativePath == null || ((LibraryItem)packageItem).RelativePath == string.Empty) &&
                            !dependentLibraryItems.Contains((LibraryItem)packageItem))
                        {
                            dependentLibraryItems.Add((LibraryItem)packageItem);
                        }
					}
				}
			}
		}

        private void SetDependentWordMLItems(List<LibraryItem> dependentLibraryItems, string wordML)
        {
            // Get the list of referenced package item guids in the WordML
            WordML.WordMLUtilities.GetDistinctPackageItemListFromWordML(wordML, dependentPackageItems);
        }

		public void AddWordMLTagToObject(DataObject inObj)
		{
			string content = m_name;
			WordTemplateDocument2.AddWordMLTagToDataObject(inObj, this, SafeTagName, content);
		}

		public override object Clone()
		{
			Outcome o = (Outcome)this.MemberwiseClone();
			if (o.Definition != null)
			{
				o.Definition = (Query)(definition.Clone());
			}
			return o;
			
		}

		#endregion
		#region ISerializable Members


		public Outcome(SerializationInfo info, StreamingContext context) : base (info, context)
		{
			definition = (Query) info.GetValue("definition", typeof(Query));
			try
			{
				formatString = info.GetString("formatString");
			}
			catch
			{
				formatString = string.Empty;
			}
			try
			{
				previewHoverText = info.GetString("previewHoverText");
			}
			catch
			{
				previewHoverText = string.Empty;
			}
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("definition", definition);
			info.AddValue("formatString", formatString);
			info.AddValue("previewHoverText", previewHoverText);
		}

		#endregion
		#region ITemplateItem Members

		public string GetTagName()
		{
			return SafeTagName;
		}

		public string GetTagContents()
		{
            // FB112: Requested to display name only as done for questions. The practice of showing the 
            // true condition output caused problems with images, the output was crap anyway, it not reflect the
            // condition, and did not changed if the outcome itself changed.
            return m_name;
		}

		/// <summary>
		///		Ensure there is nothing in the front of the Xml definition
		/// </summary>
		/// <param name="wordML"></param>
		/// <returns></returns>
		private string GetValidWordML(string wordML)
		{
			// Ensure there is nothing before the start of the xml
			if (wordML.IndexOf("<?xml version=", 0) != 0)
			{
				int startPositionIndex = wordML.IndexOf("<?xml version=", 0);
				return wordML.Substring(startPositionIndex);
			}
			else
			{
				return wordML;
			}
		}

		#endregion
		#region IPackageItemContainer Members

		public bool ContainsReferenceTo(PackageItem item)
		{
			if (definition != null)
			{
				return definition.ContainsReferenceTo(item);
			}
			else
			{
				return false;
			}
		}

		#endregion
		#region Shared Library

		public void Update(Outcome outcome)
		{
            if (this.Name != outcome.Name)
            {
                this.Name = outcome.Name;
            }

			// Shared Library Item Properties
			this.LibraryUniqueIdentifier = outcome.LibraryUniqueIdentifier;	
			this.Version = outcome.Version;
			this.LibraryType = outcome.LibraryType;
			this.Linked = outcome.Linked;
			this.CreatedBy = outcome.CreatedBy;		
			this.CreatedDateTime = outcome.CreatedDateTime;
			this.CheckedOutBy = outcome.CheckedOutBy;
			this.CheckedOutByLogInName = outcome.CheckedOutByLogInName;
			this.CheckedOutDateTime = outcome.CheckedOutDateTime;
			this.CheckedOutStatus = outcome.CheckedOutStatus;
			this.ModifiedBy = outcome.ModifiedBy;
			this.ModifiedDateTime = outcome.ModifiedDateTime;
			this.CheckInComment = outcome.CheckInComment;
			this.LocationPath = outcome.LocationPath;
			this.RelativePath = outcome.RelativePath;
			this.Filename = outcome.Filename;
			this.Touch = outcome.Touch;
			this.Notes = outcome.Notes;
            this.previewHoverText = outcome.previewHoverText;
            this.DependentLibraryItems = outcome.DependentLibraryItems;
			
			// Reconnect the query
			this.Definition = outcome.Definition;
			ReconnectActionObjects(this.Definition);

            if (this.Definition.QueryExpression.RootExpressionNode != null)
            {
                ReconnectDependentObjects(this.Definition.QueryExpression.RootExpressionNode);
            }

			// Ensure the query objects have their parent package hooked up
			this.Definition.ParentPackage = this.ParentPackage;
			this.Definition.QueryExpression.ParentPackage = this.ParentPackage;
		}

        private void ReconnectActionObjects(Query query)
        {
            if (query.ActionIfTrue.SubQuery != null)
            {
                ReconnectActionObjects(query.ActionIfTrue.SubQuery);
            }

            if (query.ActionIfFalse.SubQuery != null)
            {
                ReconnectActionObjects(query.ActionIfFalse.SubQuery);
            }

            // Reconnect the query expression
            if (query.QueryExpression.RootExpressionNode != null)
            {
                ReconnectDependentObjects(query.QueryExpression.RootExpressionNode);
            }

            query.QueryExpression.CreateTableFromExpression();
            query.QueryExpression.ParentPackage = this.ParentPackage;
        }

		private void ReconnectDependentObjects(ExpressionNode expressionNode)
		{
			// only reconnect if the expression is valid
			if (expressionNode.Operand1 != null && expressionNode.Operand2 != null) 
			{
				// Handle Operand 1 - nested or not
				if (expressionNode.Operand1.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand1);
					expressionNode.ParentPackage = this.ParentPackage;
				}
				else
				{
					// Connect library item
					expressionNode.Operand1 = GetDependentPackageItem((LibraryItem)expressionNode.Operand1);
				}
		
				// Handle Operand 2 - nested or not
				if (expressionNode.Operand2.GetType() == typeof(ExpressionNode))
				{
					ReconnectDependentObjects((ExpressionNode)expressionNode.Operand2);
					expressionNode.ParentPackage = this.ParentPackage;
				}
				else
				{
					// Connect library item
					expressionNode.Operand2 = GetDependentPackageItem((LibraryItem)expressionNode.Operand2);

				}
			}
		}

		private PackageItem GetDependentPackageItem(LibraryItem libraryItem)
		{
			object itemValue;
			PerfectusDataType itemDataType;
			PackageItem packageItem = null;

			// Attempt to get the item from the package
			packageItem = this.ParentPackage.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);

            // Ensure all items except package constants have there parent pasckage values set
            if (packageItem != null)
            {
                packageItem.ParentPackage = this.ParentPackage;
            }

			if (libraryItem.LibraryType == LibraryItemType.None)
			{
				if (((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty() == PerfectusDataType.Date)
				{
					itemDataType = PerfectusDataType.Date;
                    itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value.ToString();

                    //try to cast the string value to a date, if this does not work (it might be "DateTime.Now" or other constant)
                    //we leave original value.
                    try
                    {
                        itemValue = DateTime.Parse(itemValue.ToString()).ToLocalTime().ToShortDateString();
                    }
                    catch
                    {
                    }
				}
				else
				{
					itemDataType = ((PackageConstant)libraryItem).GetPerfectusDataTypeFromStringProperty();
					itemValue = libraryItem.StringValue != null ? libraryItem.StringValue : ((PackageConstant)libraryItem).Value;
				}

				// Set the packge constant
				packageItem = new PackageConstant(itemDataType, itemValue);

                //TODO: The datatype string needs to be set back to original also for next sweep otherwise we lose data types of
                //query operands, is this the best way?
                ((LibraryItem)packageItem).DataTypeString = libraryItem.DataTypeString;
			}

			if (packageItem == null)
			{
                Exception ex = new Exception(libraryItem.Name + " " + ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction.ReconnectedPackage")); //TODO - SL - RESOURCE
				ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
				throw ex; 
			}
		
			return packageItem;
		}

		#endregion
	}
}
