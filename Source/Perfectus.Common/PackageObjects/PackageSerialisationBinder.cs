using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Perfectus.Common.SharedLibrary;
using System.Linq;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for PackageSerialisationBinder.
	/// </summary>
	public sealed class PackageSerialisationBinder : SerializationBinder
	{
        const string supportOldVersion = "5.4.0.0";
        const string supportOldAssembly = "Perfectus.Common";
        
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type type = null;

            // 1. Normal case; objects are serialized with the current assemblies
            try
            {
                //type = Type.GetType((System.Reflection.Assembly.CreateQualifiedName(assemblyName, typeName)));
                type = evaluatedType(assemblyName, typeName);
                if (null != type)
                    return type;
            }
            catch { }


            // 2. Perfectus object of version 5.4.0.0 (or whatever) might have been used in the typename(!)
            //    An example are embedded objects in Generics:
            //    "System.Collections.Generic.List`1[[Perfectus.Common.PackageObjects.Delivery, Perfectus.Common, Version=5.4.0.0, Culture=neutral, PublicKeyToken=cba912c898d07724]]"  
            try
            {
                int pos = -1;
                // Find a type of (let's say) "Perfectus.Common, Version=5.4.0.0"
                string serachString = String.Format("{0}, Version={1}", supportOldAssembly, supportOldVersion);
                if (typeName.Contains(serachString))
                {
                    // Create a type of (let's say) "Perfectus.Common, Version=6.0.0.0"
                    typeName = typeName.Replace(serachString, String.Format("{0}, Version={1}", supportOldAssembly, System.Windows.Forms.Application.ProductVersion));
                    pos = 1; // Just remember that we found something
                }
                else
                {
                    // Remove the Version string from the SharePoint intregration, as we cannot know which versions of SP are compatible
                    pos = typeName.IndexOf("Perfectus.SharePoint2007.Common, Version=");
                    if (pos > 0)
                        typeName = String.Format("{0}{1}",
                            typeName.Substring(0, pos + "Perfectus.Sharepoint2007.Common".Length),
                            typeName.Substring(pos + "Perfectus.Sharepoint2007.Common, Version=?.?.?.?".Length));
                }
                // If we haven't replaced anything then its pointless to try again
                if ( pos > 0)
                    type = Type.GetType((System.Reflection.Assembly.CreateQualifiedName(assemblyName, typeName)));
            }
            catch { }

            if (type == null)
            {
                string shortProductVersion = String.Format("{0}.{1}.{2}", 6,0,0);
                //A full name was passed, we need the simple one!
                string simpleAssemblyName = assemblyName.Split(',').FirstOrDefault();

                if (string.IsNullOrEmpty(simpleAssemblyName))
                    throw new InvalidOperationException("Cannot retrieve assembly name from string '" + assemblyName + "'");

                System.Reflection.Assembly currentAssembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault((System.Reflection.Assembly assembly) => { return (assembly.GetName().Name.Equals(simpleAssemblyName)); });

                if (currentAssembly == null)
                    throw new InvalidOperationException("The requested assembly '" + simpleAssemblyName + "' was not found");

                // We can do a sanity check on Perfectus.Common
                // (note that we do NOT test the version of an integration like Perfectus.SharePoint2007.Common)
                if (currentAssembly.FullName.Contains(supportOldAssembly) &&
                    !currentAssembly.FullName.Contains(String.Format("Version={0}", shortProductVersion)))
                    throw new InvalidOperationException("The requested assembly '" + simpleAssemblyName + "' is not of version '" + shortProductVersion + ".*'");

                type = evaluatedType(currentAssembly.FullName, typeName);

                if ( type == null )
                    throw new Exception (
                        String.Format(
                        "The type '{0}' of assembly '{1}' cannot be mapped."+System.Environment.NewLine+
                        "Upgrading of the package is not possible",
                        typeName,assemblyName ));

            }
            return type;
        }

        private Type evaluatedType(string assemblyName, string typeName)
        {
            Console.WriteLine(String.Format("ET: {0}, {1}", typeName, assemblyName));

            switch (typeName)
            {
                case "Perfectus.Common.PackageObjects.InterviewPage":
                    return typeof(InternalPage);
                case "Perfectus.Common.PackageObjects.Page":
                    return typeof(InternalPage);
                case "Perfectus.Common.PackageObjects.PageCollection":
                    return typeof(InterviewPageCollection);
                case "Perfectus.Common.PackageObjects.Repeater":
                    return typeof(RepeaterZone);
                case "Perfectus.Common.PackageObjects.Package":
                    return typeof(Package);
                case "Perfectus.Common.SharedLibrary.RepositoryItemCollection":
                    return typeof(List<RepositoryItem>);
                case "Perfectus.Common.SharedLibrary.RepositoryFolderCollection":
                    return typeof(List<RepositoryFolder>);
                case "Perfectus.Common.PackageObjects.TextQuestionValue":
                    return typeof(TextQuestionQueryValue);
                case "Perfectus.Common.PackageObjects.TextQuestionValue+TextQuestionValueType":
                    return typeof(TextQuestionQueryValue.TextQuestionQueryValueType);
                // Saw a package having that type - maybe a pre-release before 
                // SharedLibrary was finished?!?
                case "Perfectus.Common.SharedLibrary.LibraryItemType":
                    return typeof(LibraryItemType);
                // BEGIN // PF-3050 5.6.1 types have moved
                case "Perfectus.Common.PackageObjects.ServerInfo":
                    return typeof(Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server.ServerInfo);
                case "Perfectus.Common.PackageObjects.PluginDescriptor":
                    return typeof(Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins.PluginDescriptor);
                case "Perfectus.Common.PackageObjects.PluginPropertyDescriptor":
                    return typeof(Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins.PluginPropertyDescriptor);
                // END // PF-3050 5.6.1 types have moved
                default:
                    return Type.GetType(String.Format("{0}, {1}", typeName, assemblyName));
            }
        }
    }
}