using System;
using System.Collections.Generic;
using System.Resources;
using System.Runtime.Serialization;
using System.Globalization;
using System.Threading;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// Summary description for Constant.
	/// </summary>
	[Serializable]
	public sealed class PackageConstant : AnswerProvider, ISerializable
	{
		#region Private Member Variables

		private ResourceManager resources;
		private string localYes;
		private string localNo; 
		private string localNow;
		private string localNull;
		private string localNotAnswered;
		private object val;
		
		#endregion
		#region Properties & Constructor

		public object Value
		{
			get { return val; }
			// set { val = value;}
		}

		public PackageConstant() : base() //SL
		{
			GetLocalStrings();
		}

		public PackageConstant(PerfectusDataType dt, object value) : this()
		{
			m_dataType = dt;
			this.val = value;
		}

		#endregion
		#region Methods

		/// <summary>
		///		Returns null as package constants do not have dependencies.
		/// </summary>
		/// <returns>null</returns>
        public override List<LibraryItem> GetDependentLibraryItems()
		{
			return null;
		}

		/// <summary>
		///		Returns null as package constants do not have dependencies.
		/// </summary>
		/// <returns>null</returns>
        public override List<LibraryItem> SetDependentLibraryItems()
		{
			return null;
		}

		private void GetLocalStrings()
		{
			resources =  ResourceLoader.GetResourceManager("Perfectus.Common.Localisation");
			localYes = resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Yes");
			localNo = resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.No");
			localNow = resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Now");
			localNull = resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Null");
			localNotAnswered = resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.NotAnswered");
		}

		public void SetValue(object val, PerfectusDataType dataType)
		{
			this.val = val;
			m_dataType = dataType;
		}

		public override object GetSingleAnswer(Caller c)
		{
			if (val.ToString() == "DateTime.Now")
			{
				return DateTime.Now;
			}		
			else
			{
				return ConvertToBestType(val, DataType, c, this.StringBindingMetaData);
			}
		}

		public override string ToString()
		{
			if (val == null)
			{
				return null;
			}
			else
			{
				if (val is string)
				{
					if (val.ToString() == "Not Answered")
					{
						return localNotAnswered;
					}
					else if (val.ToString() == "Null")
					{
						return localNull;
					}
					else if (val.ToString() == "DateTime.Now")
					{
						return localNow;
					}
				}
				
				switch (m_dataType)
				{
					case PerfectusDataType.Date:
						return Convert.ToDateTime(val.ToString()).ToShortDateString();
					case PerfectusDataType.Text:
					case PerfectusDataType.Number:
						return val.ToString();
					case PerfectusDataType.YesNo:
                        try
                        {
                            return Convert.ToBoolean(val) == true ? localYes : localNo;
                        }
                        catch (Exception) // ToBoolean() failed
                        {
                            // Want to support Yes and No
                            object x = GetSingleAnswer(Caller.System);
                            return Convert.ToBoolean(x) == true ? localYes : localNo;
                        }
					default:
						return null;
				}				
			}
		}

		public string ToJavascriptForBrowserString()
		{
			string format = "new Array(new Array({0}))";
			if (val == null)
			{
				return "null";
			}
			else
			{
				if (val is string)
				{
					if (val.ToString() == "Not Answered")
					{
						return string.Format(format, "null");
					}
					else if (val.ToString() == "Null")
					{
						return string.Format(format, "null");
					}
					else if (val.ToString() == "DateTime.Now")
					{
						return string.Format(format, "[timeNow]");
					}
				}

				switch (m_dataType)
				{
					case PerfectusDataType.Date:
						string d = string.Format("[new Date('{0} 00:00:00').getTime()]", ((DateTime) val).ToShortDateString());
						return string.Format(format, d);
					case PerfectusDataType.Text:
						string t = string.Format("'{0}'", val);
						return string.Format(format, t);
					case PerfectusDataType.Number:
                        //we need to change the current culture to US english to ensure that the JavaScript friendly variables are written out
                        CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;//store our current culture for later
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");//set US
                        string t2 = string.Format("[{0}]", val);
                        Thread.CurrentThread.CurrentCulture = originalCulture;//set original culure back
						return string.Format(format, t2);
					case PerfectusDataType.YesNo:
						string b = (Convert.ToBoolean(val) == true ? "true" : "false");
						return string.Format(format, b);
					default:
						return "null";
				}
			}
		}

		public string ToPreviewString()
		{
			if (val == null)
			{
				return "null";
			}
			else
			{
				if (val is string)
				{
					if (val.ToString() == "Not Answered")
					{
						return localNotAnswered;
					}
					else if (val.ToString() == "Null")
					{
						return localNull;
					}
					else if (val.ToString() == "DateTime.Now")
					{
						return localNow;	
					}
				}
				
				switch (m_dataType)
				{
					case PerfectusDataType.Date:
						return string.Format("\"{0}\"", Convert.ToDateTime(val.ToString()).ToShortDateString());
					case PerfectusDataType.Text:
						return string.Format("\"{0}\"", val);
					case PerfectusDataType.Number:
						return val.ToString();
					case PerfectusDataType.YesNo:
						return Convert.ToBoolean(val) == true ? localYes : localNo;					
					default:
						return "null";
				}				
			}	
		}

		
		#endregion
		#region IAnswerProvider Members

		public override PerfectusDataType DataType
		{
			get { return m_dataType; }
			set { }

		}

		#endregion
		#region ISerializable Members

		public PackageConstant(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			GetLocalStrings();
			//this.dataType = (PerfectusDataType)info.GetValue("dataType", typeof(PerfectusDataType));
			this.val = info.GetValue("val", typeof (object));
		}

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("val", val);

		}

		#endregion
	}
}