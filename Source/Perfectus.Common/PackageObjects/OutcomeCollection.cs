using System;
using System.Collections;
using System.ComponentModel;

namespace Perfectus.Common.PackageObjects
{
	/// <summary>
	/// A collection of elements of type Outcome
	/// </summary>
	[Serializable]
	public sealed class OutcomeCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the OutcomeCollection class.
		/// </summary>
		internal OutcomeCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new OutcomeCollection.
		/// </param>
		internal OutcomeCollection(Outcome[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the OutcomeCollection class, containing elements
		/// copied from another instance of OutcomeCollection
		/// </summary>
		/// <param name="items">
		/// The OutcomeCollection whose elements are to be added to the new OutcomeCollection.
		/// </param>
		internal OutcomeCollection(OutcomeCollection items)
		{
			this.AddRange(items);
		}

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Capacity
        {
            get { return base.Capacity; }
            set { base.Capacity = value; }
        }

        /// <summary>
        /// Using hiding and setting Browsable = false on this base class prop because it was showing up in the IPManager property grid
        /// when manually altered it was causing IPManager to crash
        /// </summary>
        [Browsable(false)]
        public new int Count
        {
            get { return base.Count; }
        }

		/// <summary>
		/// Adds the elements of an array to the end of this OutcomeCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this OutcomeCollection.
		/// </param>
		internal void AddRange(Outcome[] items)
		{
			foreach (Outcome item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another OutcomeCollection to the end of this OutcomeCollection.
		/// </summary>
		/// <param name="items">
		/// The OutcomeCollection whose elements are to be added to the end of this OutcomeCollection.
		/// </param>
		internal void AddRange(OutcomeCollection items)
		{
			foreach (Outcome item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type Outcome to the end of this OutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The Outcome to be added to the end of this OutcomeCollection.
		/// </param>
		public void Add(Outcome value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic Outcome value is in this OutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The Outcome value to locate in this OutcomeCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this OutcomeCollection;
		/// false otherwise.
		/// </returns>
		public bool Contains(Outcome value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this OutcomeCollection
		/// </summary>
		/// <param name="value">
		/// The Outcome value to locate in the OutcomeCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public int IndexOf(Outcome value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the OutcomeCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the Outcome is to be inserted.
		/// </param>
		/// <param name="value">
		/// The Outcome to insert.
		/// </param>
		internal void Insert(int index, Outcome value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the Outcome at the given index in this OutcomeCollection.
		/// </summary>
		public Outcome this[int index]
		{
			get { return (Outcome) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific Outcome from this OutcomeCollection.
		/// </summary>
		/// <param name="value">
		/// The Outcome value to remove from this OutcomeCollection.
		/// </param>
		internal void Remove(Outcome value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by OutcomeCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(OutcomeCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public Outcome Current
			{
				get { return (Outcome) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (Outcome) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this OutcomeCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}

}