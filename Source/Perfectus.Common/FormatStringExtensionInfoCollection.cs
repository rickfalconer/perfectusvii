namespace Perfectus.Common
{
	/// <summary>
	/// A collection of elements of type FormatStringExtensionInfo
	/// </summary>
	public class FormatStringExtensionInfoCollection: System.Collections.CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FormatStringExtensionInfoCollection class.
		/// </summary>
		public FormatStringExtensionInfoCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the FormatStringExtensionInfoCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new FormatStringExtensionInfoCollection.
		/// </param>
		public FormatStringExtensionInfoCollection(FormatStringExtensionInfo[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the FormatStringExtensionInfoCollection class, containing elements
		/// copied from another instance of FormatStringExtensionInfoCollection
		/// </summary>
		/// <param name="items">
		/// The FormatStringExtensionInfoCollection whose elements are to be added to the new FormatStringExtensionInfoCollection.
		/// </param>
		public FormatStringExtensionInfoCollection(FormatStringExtensionInfoCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this FormatStringExtensionInfoCollection.
		/// </param>
		public virtual void AddRange(FormatStringExtensionInfo[] items)
		{
			foreach (FormatStringExtensionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another FormatStringExtensionInfoCollection to the end of this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <param name="items">
		/// The FormatStringExtensionInfoCollection whose elements are to be added to the end of this FormatStringExtensionInfoCollection.
		/// </param>
		public virtual void AddRange(FormatStringExtensionInfoCollection items)
		{
			foreach (FormatStringExtensionInfo item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type FormatStringExtensionInfo to the end of this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The FormatStringExtensionInfo to be added to the end of this FormatStringExtensionInfoCollection.
		/// </param>
		public virtual void Add(FormatStringExtensionInfo value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic FormatStringExtensionInfo value is in this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The FormatStringExtensionInfo value to locate in this FormatStringExtensionInfoCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this FormatStringExtensionInfoCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(FormatStringExtensionInfo value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this FormatStringExtensionInfoCollection
		/// </summary>
		/// <param name="value">
		/// The FormatStringExtensionInfo value to locate in the FormatStringExtensionInfoCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(FormatStringExtensionInfo value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the FormatStringExtensionInfoCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the FormatStringExtensionInfo is to be inserted.
		/// </param>
		/// <param name="value">
		/// The FormatStringExtensionInfo to insert.
		/// </param>
		public virtual void Insert(int index, FormatStringExtensionInfo value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the FormatStringExtensionInfo at the given index in this FormatStringExtensionInfoCollection.
		/// </summary>
		public virtual FormatStringExtensionInfo this[int index]
		{
			get
			{
				return (FormatStringExtensionInfo) this.List[index];
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Removes the first occurrence of a specific FormatStringExtensionInfo from this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <param name="value">
		/// The FormatStringExtensionInfo value to remove from this FormatStringExtensionInfoCollection.
		/// </param>
		public virtual void Remove(FormatStringExtensionInfo value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by FormatStringExtensionInfoCollection.GetEnumerator.
		/// </summary>
		public class Enumerator: System.Collections.IEnumerator
		{
			private System.Collections.IEnumerator wrapped;

			public Enumerator(FormatStringExtensionInfoCollection collection)
			{
				this.wrapped = ((System.Collections.CollectionBase)collection).GetEnumerator();
			}

			public FormatStringExtensionInfo Current
			{
				get
				{
					return (FormatStringExtensionInfo) (this.wrapped.Current);
				}
			}

			object System.Collections.IEnumerator.Current
			{
				get
				{
					return (FormatStringExtensionInfo) (this.wrapped.Current);
				}
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this FormatStringExtensionInfoCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		public new virtual FormatStringExtensionInfoCollection.Enumerator GetEnumerator()
		{
			return new FormatStringExtensionInfoCollection.Enumerator(this);
		}
	}
}
