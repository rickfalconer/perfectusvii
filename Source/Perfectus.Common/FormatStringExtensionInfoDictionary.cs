using System;

namespace Perfectus.Common
{
	/// <summary>
	/// A dictionary with keys of type string and values of type FormatStringExtensionInfo
	/// </summary>
	[Serializable]
	public class FormatStringExtensionInfoDictionary: System.Collections.DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the FormatStringExtensionInfoDictionary class
		/// </summary>
		public FormatStringExtensionInfoDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the FormatStringExtensionInfo associated with the given string
		/// </summary>
		/// <param name="key">
		/// The string whose value to get or set.
		/// </param>
		public virtual FormatStringExtensionInfo this[string key]
		{
			get
			{
				return (FormatStringExtensionInfo) this.Dictionary[key];
			}
			set
			{
				this.Dictionary[key] = value;
			}
		}

		/// <summary>
		/// Adds an element with the specified key and value to this FormatStringExtensionInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to add.
		/// </param>
		/// <param name="value">
		/// The FormatStringExtensionInfo value of the element to add.
		/// </param>
		public virtual void Add(string key, FormatStringExtensionInfo value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this FormatStringExtensionInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this FormatStringExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this FormatStringExtensionInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this FormatStringExtensionInfoDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The string key to locate in this FormatStringExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this FormatStringExtensionInfoDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(string key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this FormatStringExtensionInfoDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The FormatStringExtensionInfo value to locate in this FormatStringExtensionInfoDictionary.
		/// </param>
		/// <returns>
		/// true if this FormatStringExtensionInfoDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(FormatStringExtensionInfo value)
		{
			foreach (FormatStringExtensionInfo item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this FormatStringExtensionInfoDictionary.
		/// </summary>
		/// <param name="key">
		/// The string key of the element to remove.
		/// </param>
		public virtual void Remove(string key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this FormatStringExtensionInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Keys
		{
			get
			{
				return this.Dictionary.Keys;
			}
		}

		/// <summary>
		/// Gets a collection containing the values in this FormatStringExtensionInfoDictionary.
		/// </summary>
		public virtual System.Collections.ICollection Values
		{
			get
			{
				return this.Dictionary.Values;
			}
		}
	}
}
