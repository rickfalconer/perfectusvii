using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common
{
    public class LibraryItemEventArgs : EventArgs
    {
        private LibraryItem libraryItem;

		public LibraryItem CurrentLibraryItem
		{
			get { return libraryItem; }
		}

        public LibraryItemEventArgs(LibraryItem libraryItem)
		{
			this.libraryItem = libraryItem;
		}
    }
}