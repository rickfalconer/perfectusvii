using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.Win32;

namespace Perfectus.Common
{
	/// <summary>
	/// testing merge. delete this. ttt u
	/// Summary description for About.
	/// </summary>
	public sealed class About
	{
		private static string copyright = ((AssemblyCopyrightAttribute) AssemblyCopyrightAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyCopyrightAttribute))).Copyright;
		private static string title = ((AssemblyTitleAttribute) AssemblyTitleAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyTitleAttribute))).Title;
		private static string familyTitle = ((AssemblyProductAttribute) AssemblyProductAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyProductAttribute))).Product;
		private static string company = ((AssemblyCompanyAttribute) AssemblyCompanyAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyCompanyAttribute))).Company;
		private static string assemblyName = Assembly.GetEntryAssembly().GetName().Name;
		private static string productVersion = Assembly.GetEntryAssembly().GetName().Version.Major + "." + Assembly.GetEntryAssembly().GetName().Version.Minor;
		//private static string assemblyVersion = Assembly.GetEntryAssembly().GetName().Version.ToString();
		private static string assemblyVersion = ((AssemblyInformationalVersionAttribute) AssemblyInformationalVersionAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyInformationalVersionAttribute))).InformationalVersion;
		private static string trademark = ((AssemblyTrademarkAttribute) AssemblyTrademarkAttribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof (AssemblyTrademarkAttribute))).Trademark;
		private static string updates = GetUpdates();
        private const string packagecompatibility = "2";

        public static string PackageCompatibility
        {
            get { return packagecompatibility; }
        }
		public static string Updates
		{
			get
			{ return updates; }
		}
		public static string Copyright
		{
			get { return copyright; }
		}

		public static string Trademark
		{
			get { return trademark; }
		}

		public static string Title
		{
			get { return title; }
		}

		public static string FamilyTitle
		{
			get { return familyTitle; }
		}

		public static string Company
		{
			get { return company; }
		}

		public static string AssemblyName
		{
			get { return assemblyName; }
		}

		public static string ProductVersion
		{
			get { return productVersion; }
		}

		public static string AssemblyVersion
		{
			get { return assemblyVersion; }
		}

		public static string FormsTitle
		{
			get { return title; }
		}

		private About()
		{
		}

		public static string ConsoleString
		{
			get { return string.Format("{0}\n{1}\n================================================================================", FormsTitle, copyright); }
		}

		public static DataTable LoadedAssemblyTable
		{
			get
			{
				DataTable dt = new DataTable("Assemblies");
				dt.Locale = CultureInfo.InvariantCulture;

				dt.Columns.Add("Module", typeof (string));
				dt.Columns.Add("Version", typeof (string));
				dt.Columns.Add("Date", typeof (DateTime));
				dt.Columns.Add("Path", typeof (string));

				foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
				{
					DataRow dr = dt.NewRow();
					dr["Module"] = a.GetName(false).Name;
					dr["Version"] = a.GetName(false).Version.ToString();
					try
					{
						dr["Date"] = File.GetCreationTimeUtc(a.Location);
						dr["Path"] = a.Location;
					}
					catch
					{
					}
					dt.Rows.Add(dr);
				}
				return dt;
			}
		}

		private static string GetUpdates()
		{
            RegistryKey key = GetRootRegistry( );

            if( key == null )
                return null;

            else
			{
				object oVal = key.GetValue("Updates");
				string retVal = null;
				if (oVal != null)
				{
					retVal = String.Join(";",(string[])(key.GetValue("Updates")));

				}
				key.Close();
				return retVal;
			}
		}

        public static RegistryKey GetRootRegistry( )
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey( @"Software\Perfectus Technology Limited" );
            // There is the chance this is a 32 bit installed application running on a 64 bit machine. For that case we simply need to check for the key elsewhere.
            if( key == null )
                key = Registry.LocalMachine.OpenSubKey( @"Software\Wow6432Node\Perfectus Technology Limited" );

            return key;
        }
    }
}