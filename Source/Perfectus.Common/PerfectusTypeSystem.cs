using System;
using System.Collections;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Common
{
	public enum PerfectusDataType
	{
		Number,
		//DateTime,
		Date,
		Text,
		HTML, // Disabled in editor
		YesNo,
		File,
		AnswerSet,
		Attachment
	}

	public enum Operator
	{
		Eq,
		Ne,
		Lt,
		Gt,
		Le,
		Ge,
		And,
		Or,
		Plus,
		Minus,
		Divide,
		Multiply
	}

	public enum QuestionDisplayType
	{
		TextBox = 0,
		TextArea = 1,
		HtmlArea = 2,
		Password = 3,
		ListBox = 4,
		DropDownList = 5,
		MultiSelectListBox = 6,
		CheckBox = 7,
		CheckBoxList = 8,
		RadioList = 9,
		Calendar = 10,
//		InterwovenObjectPicker,
		FilePicker = 12,
		AnswerSetPicker = 13,
		Extension1 = 1001,
		Extension2 = 1002,
		Extension3 = 1003,
		Extension4 = 1004,
		Extension5 = 1005,
		Extension6 = 1006,
		Extension7 = 1007,
		Extension8 = 1008,
		Extension9 = 1009,
		Extension10 = 1010,
		Extension11 = 1011,
		Extension12 = 1012,
		Extension13 = 1013,
		Extension14 = 1014,
		Extension15 = 1015,
		Extension16 = 1016,
		Extension17 = 1017,
		Extension18 = 1018,
		Extension19 = 1019,
		Extension20 = 1020,
		Extension21 = 1021,
		Extension22 = 1022,
		Extension23 = 1023,
		Extension24 = 1024,
		Extension25 = 1025,
		Extension26 = 1026,
		Extension27 = 1027,
		Extension28 = 1028,
		Extension29 = 1029,
		Extension30 = 1030,
		Extension31 = 1031,
		Extension32 = 1032,
		Extension33 = 1033,
		Extension34 = 1034,
		Extension35 = 1035,
		Extension36 = 1036,
		Extension37 = 1037,
		Extension38 = 1038,
		Extension39 = 1039,
		Extension40 = 1040,
		Extension41 = 1041,
		Extension42 = 1042,
		Extension43 = 1043,
		Extension44 = 1044,
		Extension45 = 1045,
		Extension46 = 1046,
		Extension47 = 1047,
		Extension48 = 1048,
		Extension49 = 1049,
		Extension50 = 1050
	}

	[Serializable]
	public class OperatorDisplay
	{
		private string display;
		private Operator op;

		public OperatorDisplay(Operator op)
		{
			this.Op = op;
		}


		public Operator Op
		{
			get { return op; }
			set
			{
				op = value;
				switch (value)
				{
					case Operator.Eq:
						display = "=";
						break;
					case Operator.Ge:
						display = "≥";
						break;
					case Operator.Le:
						display = "≤";
						break;
					case Operator.Gt:
						display = ">";
						break;
					case Operator.Lt:
						display = "<";
						break;
					case Operator.Ne:
						display = "≠";
						break;
					case Operator.Plus:
						display = "+";
						break;
					case Operator.Minus:
						display = "-";
						break;
					case Operator.Divide:
						display = "/";
						break;
					case Operator.Multiply:
						display = "*";
						break;
					default:
						display = Enum.GetName(typeof (Operator), value);
						break;

				}
			}
		}

		public string Display
		{
			get { return display; }
		}

		public override string ToString()
		{
			return display;
		}

	}

	public class PerfectusTypeSystem
	{
		public static QuestionDisplayType[] GetAvailableDisplayTypes(Question q)
		{
			bool hasItems = HasItems(q);
			if (hasItems && 
                q.DataType != PerfectusDataType.YesNo &&
                q.DataType != PerfectusDataType.File &&
                q.DataType != PerfectusDataType.Attachment)
			{
				return new QuestionDisplayType[]
					{
						QuestionDisplayType.CheckBoxList,
						QuestionDisplayType.DropDownList,
						QuestionDisplayType.MultiSelectListBox,
						QuestionDisplayType.RadioList,
						QuestionDisplayType.ListBox
					};
			}
			else
			{
				ArrayList retVal = new ArrayList();
                switch (q.DataType)
                {
                    case PerfectusDataType.Attachment:
                        retVal.Add(QuestionDisplayType.FilePicker);
                        break;
                    case PerfectusDataType.File:
                        retVal.Add(QuestionDisplayType.FilePicker);
                        break;
                    case PerfectusDataType.Date:
                        retVal.Add(QuestionDisplayType.Calendar);
                        break;
                    case PerfectusDataType.Number:
                        retVal.Add(QuestionDisplayType.TextBox);
                        break;
                    case PerfectusDataType.HTML:
                        retVal.Add(QuestionDisplayType.HtmlArea);
                        break;
                    case PerfectusDataType.YesNo:
                        retVal.Add(QuestionDisplayType.CheckBox);
                        break;
                    case PerfectusDataType.AnswerSet:
                        retVal.Add(QuestionDisplayType.AnswerSetPicker);
                        break;
                    case PerfectusDataType.Text:
                    default:
                        retVal.AddRange(new QuestionDisplayType[] { QuestionDisplayType.TextBox, QuestionDisplayType.TextArea, QuestionDisplayType.Password });
                        break;

                }

				// Add extensions appropriate for this type
				Package p = q.ParentPackage;
				if (p != null && p.ExtensionMapping != null)
				{
					foreach (QuestionDisplayType qdt in p.ExtensionMapping.Keys)
					{
                        // do not use copies like 'Sharepoint Listitem Picker#;xxxx-xxxx-xxxx-xxxx'
                        if ( p.ExtensionMapping[qdt].ServerSideDisplayPluginId.Contains ("#;"))
                            continue;

                        string[] forDataTypes = p.ExtensionMapping[qdt].ForDataType.Split(';');
                        foreach(string forDataType in forDataTypes)
                        {
                            switch (forDataType.ToUpperInvariant())
                            {
                                case "TEXT":
                                    if (q.DataType == PerfectusDataType.Text)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;
                                case "DATE":
                                    if (q.DataType == PerfectusDataType.Date)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;
                                case "YESNO":
                                    if (q.DataType == PerfectusDataType.YesNo)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;
                                case "NUMBER":
                                    if (q.DataType == PerfectusDataType.Number)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;
                                case "ANSWERSET":
                                    if (q.DataType == PerfectusDataType.AnswerSet)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;
                                case "ATTACHMENT":
                                    if (q.DataType == PerfectusDataType.Attachment)
                                    {
                                        retVal.Add(qdt);
                                    }
                                    break;

                            }
                        }
					}
				}


				return (QuestionDisplayType[])retVal.ToArray(typeof(QuestionDisplayType));
			}
		}

		public static bool IsValidOperand(PerfectusDataType pdt, Operator op)
		{
			Operator[] ops = GetComparisonOperators(pdt);
			foreach (Operator o in ops)
			{
				if (o == op)
				{
					return true;
				}
			}
			return false;
		}

		public static bool IsValidNullCheckOperand(Operator op)
		{
			if(op == Operator.Eq || op == Operator.Ne)
			{
				return true;
			}
			else
			{			
				return false;
			}
		}

		public static Operator[] GetArithmeticOperators()
		{
			return new Operator[] {Operator.Plus, Operator.Minus, Operator.Divide, Operator.Multiply};
		}

		public static Operator[] GetBooleanOperators()
		{
			return new Operator[] {Operator.And, Operator.Or};
		}

		public static Operator[] GetComparisonOperators(PerfectusDataType leftOperandType)
		{
			switch (leftOperandType)
			{
				case PerfectusDataType.Date:
					//		case PerfectusDataType.DateTime:
					//allowedRightOperandTypes = new PerfectusDataType[] {PerfectusDataType.Date}; // PerfectusDataType.DateTime};
					return new Operator[] {Operator.Eq, Operator.Ne, Operator.Lt, Operator.Le, Operator.Gt, Operator.Ge};
				case PerfectusDataType.Number:
					//allowedRightOperandTypes = new PerfectusDataType[] {PerfectusDataType.Number};
					return new Operator[] {Operator.Eq, Operator.Ne, Operator.Lt, Operator.Le, Operator.Gt, Operator.Ge};
				case PerfectusDataType.YesNo:
					//allowedRightOperandTypes = new PerfectusDataType[] {PerfectusDataType.YesNo};
					return new Operator[] {Operator.Eq, Operator.Ne};
				case PerfectusDataType.Text:
				default:
					//allowedRightOperandTypes = new PerfectusDataType[] {PerfectusDataType.Text};
					return new Operator[] {Operator.Eq, Operator.Ne};
			}
		}

		public static bool HasItems(Question q)
		{
			return ((q.Items != null && q.Items.Count > 0) || (q.DataBindings != null && q.DataBindings.ItemsValueBinding != null && q.DataBindings.ItemsValueBinding.Length > 0));
		}

		public static bool IsValidDisplayType(Question q, QuestionDisplayType qdt)
		{
			QuestionDisplayType[] validTypes = GetAvailableDisplayTypes(q);
			foreach (QuestionDisplayType dt in validTypes)
			{
				if (dt == q.DisplayType)
				{
					return true;
				}
			}
			return false;
		}

		public static QuestionDisplayType GetDefaultDisplayType(PerfectusDataType pdt)
		{
			switch (pdt)
			{
                case PerfectusDataType.Attachment:
				case PerfectusDataType.File:
					return QuestionDisplayType.FilePicker;
				case PerfectusDataType.Date:
					//		case PerfectusDataType.DateTime:
					return QuestionDisplayType.Calendar;
				case PerfectusDataType.HTML:
					return QuestionDisplayType.HtmlArea;
				case PerfectusDataType.YesNo:
					return QuestionDisplayType.CheckBox;
				case PerfectusDataType.AnswerSet:
					return QuestionDisplayType.AnswerSetPicker;
				case PerfectusDataType.Number:
				case PerfectusDataType.Text:				
				default:
					return QuestionDisplayType.TextBox;
			}
		}

		public static PackageItem[] GetCompatibleItems(PerfectusDataType dataType, Package p)
		{
			ArrayList items = new ArrayList();
			foreach (Question q in p.Questions)
			{
				if (q.DataType == dataType)
				{
					items.Add(q);
				}
			}

			foreach (PFunction f in p.Functions)
			{
				if (f.DataType == dataType)
				{
					items.Add(f);
				}
			}

			return (PackageItem[]) items.ToArray(typeof (PackageItem));
		}

		public static PackageItem[] GetNonSelfReferentialItems(PFunction startingFrom, PerfectusDataType dataTypeFilter)
		{
			Package p = startingFrom.ParentPackage;

			// Return a list of items that do not reference 'startingFrom', or reference something that references something that references 'startingFrom'

			// Starting from will be  Function.

			// Walk the package, excluding items that eventually mnetion startingFrom
			ArrayList items = new ArrayList();
			foreach (Question q in p.Questions)
			{
				if (q.DataType == dataTypeFilter)
				{
					items.Add(q);
				}
			}

			foreach (PFunction f in p.Functions)
			{
				if (f != startingFrom && f.DataType == startingFrom.DataType && !(f.ContainsReferenceTo(startingFrom)))
				{
					items.Add(f);
				}
			}

			return (PackageItem[]) items.ToArray(typeof (PackageItem));
		}


		public static PerfectusDataType[] GetEnabledTypes()
		{
			return new PerfectusDataType[] {PerfectusDataType.Date, PerfectusDataType.Number, PerfectusDataType.Text, PerfectusDataType.YesNo, PerfectusDataType.File, PerfectusDataType.Attachment, PerfectusDataType.AnswerSet};
		}

		public static string[] GetEnabledTypeNamesLocalised()
		{
			PerfectusDataType[] enabledTypes = GetEnabledTypes();
			string[] localisedTypes = new string[enabledTypes.Length];
			int i=0;
			foreach(PerfectusDataType pdt in enabledTypes)
			{
				string keyName = string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.PerfectusDataType.{0}", pdt.ToString());
				localisedTypes[i] = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
				i++;
			}

			return localisedTypes;
		}

		public static string LocaliseType(PerfectusDataType type)
		{
			string keyName = string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.PerfectusDataType.{0}", type.ToString());
			return ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
		}

		public static PerfectusDataType DelocaliseTypeName(string typeName)
		{
			PerfectusDataType[] enabledTypes = GetEnabledTypes();
			foreach(PerfectusDataType pdt in enabledTypes)
			{
				string keyName = string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.PerfectusDataType.{0}", pdt.ToString());
				if(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName) == typeName)
				{
					return pdt;
				}
			}
			// Should never happen.
			return PerfectusDataType.Text;
		}

		public static string LocaliseDisplayType(QuestionDisplayType type, Question q)
		{
			if (q != null && q.ParentPackage != null && q.ParentPackage.ExtensionMapping.ContainsKey(type))
			{
				Package p = q.ParentPackage;
				return (p.ExtensionMapping[type].LocalisedDisplayName);
			}
			else
			{
				string keyName = string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.QuestionDisplayType.{0}", type.ToString());
				return ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
			}
		}

		public static QuestionDisplayType DelocaliseDisplayTypeName(string typeName, Question q)
		{		
			if (q != null && q.ParentPackage != null && q.ParentPackage.ExtensionMapping != null)
			foreach(QuestionDisplayType qdt in q.ParentPackage.ExtensionMapping.Keys)
			{
				if (q.ParentPackage.ExtensionMapping[qdt].LocalisedDisplayName == typeName)
				{
					return qdt;
				}
			}
			
			QuestionDisplayType[] enabledTypes = GetAvailableDisplayTypes(q);
			foreach(QuestionDisplayType qdt in enabledTypes)
			{
				string keyName = string.Format("Perfectus.Common.PerfectusTypeSystem.Enums.QuestionDisplayType.{0}", qdt.ToString());
				if(ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName) == typeName)
				{
					return qdt;
				}
			}
			// Should never happen.
			return QuestionDisplayType.TextBox;
		}

		public static bool IsValidQuestionBindingType(PerfectusDataType pdt)
		{
			switch(pdt)
			{
				case PerfectusDataType.Date: 
					return true;
				case PerfectusDataType.Number: 
					return true;
				case PerfectusDataType.Text: 
					return true;
				case PerfectusDataType.YesNo:
					return true;
				default :
					return false;				
			}			
		}
	}
}