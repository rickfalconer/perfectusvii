using System.Collections;
using System.Reflection;
using System.Resources;

namespace Perfectus.Common
{
	/// <summary>
	/// Summary description for Resources.
	/// </summary>
	public class ResourceLoader
	{
		private static Hashtable resourceManagersCache = new Hashtable();
		
		private ResourceLoader()
		{
		}

		public static ResourceManager GetResourceManager(string resourceFileName)
		{
            // We could be deserialised on multiple threads by the coordinator service
            lock (resourceManagersCache)
            {
                if (resourceManagersCache.ContainsKey(resourceFileName))
                {
                    return (ResourceManager) resourceManagersCache[resourceFileName];
                }
                else
                {
                    ResourceManager rm = new ResourceManager(resourceFileName, Assembly.GetCallingAssembly());
                    resourceManagersCache.Add(resourceFileName, rm);
                    return rm;
                }
            }
		}
	}
}
