using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Property.Dialogs;

namespace Perfectus.Common.Property.Editors
{
    /// <summary>
    /// Summary description for StringEditor.
    /// </summary>
    public class XmlEncodedEditor : UITypeEditor
    {
        private XmlEncodedEditorForm ui;

        // ------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///		Launch the LargeTextEditorForm.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sp"></param>
        /// <param name="value">The current value of the property.</param>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            // get the editor service.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)sp.GetService(typeof(IWindowsFormsEditorService));
            DialogResult result = DialogResult.Ignore;

            if (edSvc == null)
            {
                // uh oh.
                return value;
            }

            ui = new XmlEncodedEditorForm();
            if (value == null)
            {
                ui.Val = null;
            }
            else
            {
                ui.Val = value.ToString();
            }

            DialogResult dr = edSvc.ShowDialog(ui);
            string resultVal = ui.Val;

            ui.Dispose();
            if (dr == DialogResult.OK)
                return resultVal;
            else
                return value;
        }
    

        // ------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Specify that this editor is the popup style ([...] button)
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Modal</returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}
