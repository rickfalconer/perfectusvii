namespace Perfectus.Common
{
	public interface IProgressReporter
	{
		long Progress { get; }
	}
}