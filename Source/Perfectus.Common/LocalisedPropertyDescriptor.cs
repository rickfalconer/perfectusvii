using System;
using System.ComponentModel;

namespace Perfectus.Common
{
	/// <summary>
    /// This class tells the property grid how to describe a property. There are many methods and properties that need to be 
    /// overriden but the three most useful are the read only DisplayName, Category and the Description properties. This 
    /// descriptor class could be thought of as a class that describes how a single property is displayed in the property grid.
	/// </summary>
	public class LocalisedPropertyDescriptor : PropertyDescriptor, IComparable
	{
        // Contains the localised display name of the property.
		private string displayName = null;
        // Contains the localised description of the property
		private string description = null;
        // Contains the properties ranking.
        private int rank = 100;
        // Provides a read only flag that can be configured during runtime.
        private bool? readOnly = null;
        // Allows you to overide the localised display name key
        private string displaynamekey = null;
        // Allows you to overide the localised description key
        private string desckey = null;        
        
        private PropertyDescriptor basePropertyDescriptor; 

		public LocalisedPropertyDescriptor(PropertyDescriptor basePropertyDescriptor) : base(basePropertyDescriptor)
		{
			this.basePropertyDescriptor = basePropertyDescriptor;
		}

        public LocalisedPropertyDescriptor(PropertyDescriptor basePropertyDescriptor, int ranking )
            : base(basePropertyDescriptor)
        {
            this.basePropertyDescriptor = basePropertyDescriptor;
            this.rank = ranking;
        }
        
        public override object GetValue(object component)
		{
			return basePropertyDescriptor.GetValue(component);
		}

		public override void ResetValue(object component)
		{
			basePropertyDescriptor.ResetValue(component);
		}

		public override void SetValue(object component, object value)
		{
            //SetValue will throw "Invalid Property value" error when try to cast a string value to an object such as Direction or Perfectus Date Type etc.
            //However, the reason that don't use if (!(value is String)) to avoid the expection is because, this need to be called to set the Name property, and when set the Name property the value has to be a string.
            try
            {
                basePropertyDescriptor.SetValue(component, value);
            }
            catch
            { }

		}

		public override bool ShouldSerializeValue(object component)
		{
			return basePropertyDescriptor.ShouldSerializeValue(component);
		}

		public override Type ComponentType
		{
			get { return basePropertyDescriptor.ComponentType; }
		}

		public override bool IsReadOnly
		{
			get 
            { 
                // First check we don't have a local setting first.
                if( readOnly.HasValue )
                    return (bool)readOnly;

                return basePropertyDescriptor.IsReadOnly; 
            }
		}

		public override Type PropertyType
		{
			get { return basePropertyDescriptor.PropertyType; }
		}

		public override bool CanResetValue(object component)
		{
			return basePropertyDescriptor.CanResetValue(component);
		}

		public override string DisplayName
		{
			get
			{
				if (displayName == null)
				{
                    // This is the default localisation key
                    string keyName = null;

                    // On override take precedence.
                    if( !String.IsNullOrEmpty( displaynamekey ) )
                        keyName = displaynamekey;
                    else
                    {
                        // Check whether an alternative display name localisation key has been provided with the property attributes.
                        Attribute nameattribute = this.Attributes[ typeof( DisplayNameAttribute ) ];
                        if( nameattribute != null )
                            keyName = ( (DisplayNameAttribute)nameattribute ).NameKey;
                        else
                            keyName = string.Format( "Perfectus.Common.PackageObjects.PropertyNames.{0}", basePropertyDescriptor.Name );
                    }

                    // Extract the text from our localisation resource
					string localisedDisplayName = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
					if (localisedDisplayName != null)
					{
						displayName = localisedDisplayName;
					}
					else
					{
						displayName = basePropertyDescriptor.DisplayName;
					}
				}
				return displayName;
			}
		}

		public override string Description
		{
			get
			{
				if (description == null)
				{
                    // This is the default localisation key
                    string keyName = null;

                    // On override take precedence.
                    if( !String.IsNullOrEmpty( desckey ) )
                        keyName = desckey;
                    else
                    {
                        // Check whether an alternative description localisation key has been provided with the property attributes.
                        Attribute descattribute = this.Attributes[ typeof( DisplayDescriptionAttribute ) ];
                        if( descattribute != null )
                            keyName = ( (DisplayDescriptionAttribute)descattribute ).DescKey;
                        else
                            keyName = string.Format( "Perfectus.Common.PackageObjects.PropertyDescriptions.{0}", basePropertyDescriptor.Name );
                    }

                    // Extract the text from our localisation resource
					string localisedDescription = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString(keyName);
					if (localisedDescription != null)
					{
						description = localisedDescription;
					}
					else
					{
						description = basePropertyDescriptor.Description;
					}	
				}
				return description;
			}
		}

        /// <summary>
        /// Allows one to set the localised displayname key
        /// </summary>
        public void SetDisplayNameKey( string value )
        {
            displaynamekey = value;
        }

        /// <summary>
        /// Allows one to set the localised description key
        /// </summary>
        public void SetDescriptionKey( string value )
        {
            desckey = value;
        }

        /// <summary>
        /// Provides a readonly property to the object's property that can be dynamically changed during runtime.
        /// </summary>
        public bool? ReadOnly
        {
            get { return readOnly; }
            set { readOnly = value; }
        }

        /// <summary>
        /// The display ranking of this property. Default is 100 which should typically place the property on the 
        /// property grid at the bottom, unless all the other properties are also unspecified in which case the properties
        /// would be shown in alphabetical order.
        /// </summary>
        public int Ranking
        {
            get { return rank; }
            set { rank = value; }
        }

        /// <summary>
        /// Sorting of Property Descriptors. First use ranking and if the same, alphabetical.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if( obj is LocalisedPropertyDescriptor )
            {
                int comparison = this.Ranking.CompareTo( ((LocalisedPropertyDescriptor)obj).Ranking );
                if( comparison == 0 )
                    comparison = this.DisplayName.CompareTo( ((LocalisedPropertyDescriptor)obj).DisplayName );
                return comparison;
            }

            throw new ArgumentException("LocalisedPropertyDescriptor: Invalid type comparison.");
        }
	}
}
