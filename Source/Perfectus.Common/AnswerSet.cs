﻿using System;
using System.Xml.Serialization;

namespace Perfectus.Common {

    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType=true)]
    [System.Xml.Serialization.XmlRoot(ElementName="answerSet", Namespace="", IsNullable=false)]
    public partial class AnswerSet 
    {        
        private AnswerSetAnswer[] answer;
        private string packageName;
        private Guid packageId = Guid.Empty;    // 
        private int packageVersion = -1;        // Default for validation checks
        private int packageRevision = -1;       // 
        private Guid packageInstanceId;
        private string resumeURL;
        private string mirrorDocType;
        private Guid lastPageAnswered;
        
        [XmlElement("answer")]
        public AnswerSetAnswer[] Answer
        {
            get { return this.answer;  }
            set { this.answer = value; }
        }
        
        [XmlAttribute("packageName")]
        public string PackageName
        {
            get { return this.packageName;  }
            set { this.packageName = value; }
        }

        [XmlAttribute("packageId")]
        public Guid PackageId
        {
            get { return this.packageId; }
            set { this.packageId = new Guid(Convert.ToString(value)); }
        }
        
        [XmlAttribute("packageVersion", DataType="int")]
        public int PackageVersion 
        {
            get { return this.packageVersion;  }
            set { this.packageVersion = value; }
        }
        
        [XmlAttribute("packageRevision", DataType="int")]
        public int PackageRevision 
        {
            get { return this.packageRevision;  }
            set { this.packageRevision = value; }
        }

        [XmlAttribute("packageInstanceId")]
        public Guid PackageInstanceId
        {
            get { return this.packageInstanceId; }
            set { this.packageInstanceId = new Guid(Convert.ToString(value)); }
        }
        
        [XmlAttribute("resumeURL")]
        public string ResumeURL 
        {
            get { return this.resumeURL;  }
            set { this.resumeURL = value; }
        }
        
        [XmlAttribute("mirrorDocType")]
        public string MirrorDocType 
        {
            get { return this.mirrorDocType;  }
            set { this.mirrorDocType = value; }
        }

        [XmlAttribute("lastPageAnswered")]
        public Guid LastPageAnswered
        {
            get { return this.lastPageAnswered; }
            set { this.lastPageAnswered = new Guid(Convert.ToString(value)); }
        }

        /// <summary>
        ///     Property indicating whether the AnswerSet is valid or not.
        /// </summary>
        public bool IsValid
        {
            get { return Validate(); }
        }

        /// <summary>
        ///     Validates whether the given AnswerSet is valid or not.
        /// </summary>
        /// <returns>Indicator of whether the instance is value or not.</returns>
        public bool Validate()
        {
            // The package id, version and revision need to be set in order to be valid

            if (this.packageId == Guid.Empty)
            {
                return false;
            }

            if (this.packageVersion == -1)
            {
                return false;
            }

            if  (this.packageRevision == -1)
            {
                return false;
            }
            
            return true;
        }
    }
    
    [System.Serializable()]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType=true)]
    public partial class AnswerSetAnswer {
        
        private string name = string.Empty;         //
        private Guid answeredItemId = Guid.Empty;   // Default for validation checks
        private int sequenceNumber;
        private int repeaterRow;        
        private string repeatSeparator;        
        private string mirrorMapping;        
        private string mirrorPrimaryInclude;        
        private string mirrorSecondaryInclude;        
        private bool isCondition;        
        private string value;
        
        [XmlAttribute("name")]
        public string Name
        {
            get { return this.name;  }
            set { this.name = value; }
        }
        
        [XmlAttribute("answeredItemId")]
        public Guid AnsweredItemId
        {
            get { return this.answeredItemId; }
            set { this.answeredItemId = new Guid(Convert.ToString(value)); }
        }
        
        [XmlAttribute("sequenceNumber", DataType="int")]
        public int SequenceNumber 
        {
            get { return this.sequenceNumber;  }
            set { this.sequenceNumber = value; }
        }
        
        [XmlAttribute("repeaterRow", DataType="int")]
        public int RepeaterRow 
        {
            get { return this.repeaterRow;  }
            set { this.repeaterRow = value; }
        }
        
        [XmlAttribute("repeatSeparator")]
        public string RepeatSeparator 
        {
            get { return this.repeatSeparator;  }
            set { this.repeatSeparator = value; }
        }
        
        [XmlAttribute("mirrorMapping")]
        public string MirrorMapping 
        {
            get { return this.mirrorMapping;  }
            set { this.mirrorMapping = value; }
        }
        
        [XmlAttribute("mirrorPrimaryInclude")]
        public string MirrorPrimaryInclude 
        {
            get { return this.mirrorPrimaryInclude;  }
            set { this.mirrorPrimaryInclude = value; }
        }
        
        [XmlAttribute("mirrorSecondaryInclude")]
        public string MirrorSecondaryInclude 
        {
            get { return this.mirrorSecondaryInclude;  }
            set { this.mirrorSecondaryInclude = value; }
        }
        
        [XmlAttribute("isCondition")]
        public bool IsCondition 
        {
            get { return this.isCondition;  }
            set { this.isCondition = value; }
        }
                
        [XmlText()]
        public string Value 
        {
            get { return this.value;  }
            set { this.value = value; }
        }

        /// <summary>
        ///     Property indicating whether the given Answer is valid or not.
        /// </summary>
        public bool IsValid
        {
            get { return Validate(); }
        }

        /// <summary>
        ///     Validates whether the given Answer is valid or not.
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            // Either the name or the id must be set in order ot be valid
            if (this.name == string.Empty && this.answeredItemId == Guid.Empty)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}