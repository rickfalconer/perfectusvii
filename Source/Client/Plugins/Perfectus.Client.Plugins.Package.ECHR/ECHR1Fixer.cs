using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.SDK;
using Perfectus.Common.PackageObjects;
using System.Collections;

namespace Perfectus.IPManager.Package
{
    public partial class ECHR1Fixer : IPackageFixer
    {
        private Form m_openingForm = null;
        private Perfectus.Common.PackageObjects.Package m_package = null;

        const int m_versionNumber = 0;
        const int m_revisionNumber = 0;

        System.Guid m_packageFixerGuid = new Guid("{703286B7-75C2-49f8-B71F-3A550FC21B94}");

        StringBuilder FixNote;
        StringBuilder TestNote;

        public ECHR1Fixer()
        {
            FixNote = new StringBuilder();
            TestNote = new StringBuilder();
        }

        private void AddFixNote(String message)
        {
            FixNote.Append(System.Environment.NewLine + message);
        }
        private void ClearFixNote()
        {
            FixNote.Remove(0, FixNote.Length);
        }
        private void AddTestNote(String message)
        {
            TestNote.Append(System.Environment.NewLine + message);
        }
        private void ClearTestNote()
        {
            TestNote.Remove(0, TestNote.Length);
        }


        #region IStudioPlugin Members

        public string Author
        {
            get { return "Perfectus Solutions"; }
        }

        public void Init(Form openingForm)
        {
            m_openingForm = openingForm;
        }

        public string Message
        {
            get { return "Perfectus IPManager package integrity tool (ECHR:1)"; }
        }

        public string PluginName
        {
            get { return "IPPackageFixer"; }
        }

        public void Shutdown()
        {
        }

        #endregion

        #region IPackageFixer Members

        public Guid PluginId
        {
            get { return m_packageFixerGuid; }
        }

        public int RevisionNumber
        {
            get { return m_revisionNumber; }
        }

        public int VersionNumber
        {
            get { return m_versionNumber; }
        }

        /// <summary>
        /// NeedFixing
        /// Test the package and indicate whether an integrity issue was found.
        /// The parameter 'param_critical_only' is used to indicate that only those tests are relevant
        /// that prevent the package from being loaded.
        /// Otherwise the test is invoked by user input
        /// </summary>
        /// <param name="param_package"></param>
        /// <param name="param_critical_only"></param>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public bool NeedFixing(Perfectus.Common.PackageObjects.Package param_package, bool param_critical_only, ref string protocol)
        {
            bool no_fix_required = true;
            m_package = param_package;
            ClearTestNote();
            protocol = System.String.Empty;

            // Critcal issues
            no_fix_required = no_fix_required && !NeedFixingCriticalIssues();

            if (!param_critical_only)
                // None-critical issues
                no_fix_required = no_fix_required && !NeedFixingNormalIssues();

            protocol = TestNote.ToString();
            return !no_fix_required;
        }


        /// <summary>
        /// Fix
        /// The function shall run after 'NeedFixing'
        /// </summary>
        /// <param name="param_package"></param>
        /// <param name="param_critical_only"></param>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public bool Fix(ref Perfectus.Common.PackageObjects.Package param_package, bool param_critical_only, ref string protocol)
        {
            bool success = true;
            m_package = param_package;
            ClearFixNote();
            protocol = System.String.Empty;

            if (!param_critical_only)
                success = success && FixNormalIssues();

            protocol = FixNote.ToString();
            return success;
        }
        #endregion

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool NeedFixingCriticalIssues()
        {
            bool test_ok = true;
            return !test_ok;
        }



        private Boolean maybe_fixECHR(CollectionBase collection)
        {
            foreach (LibraryItem o in collection)
                if (o.RelativePath != null)
                {
                    int index = o.RelativePath.IndexOf("Perfectus%20SharedLibrary/");
                    int index2 = o.RelativePath.IndexOf("Perfectus SharedLibrary/");

                    if (-1 != index || -1 != index2)
                    {
                        AddTestNote("Shared Library referrences invalid");
                        return false;
                    }
                }
            return true;
        }


        private Boolean fixECHR(CollectionBase collection)
        {
            foreach (LibraryItem o in collection)
                if (o.RelativePath != null)
                {
                    int index = o.RelativePath.IndexOf("Perfectus%20SharedLibrary/");
                    if (index != -1)
                    {
                        o.RelativePath = o.RelativePath.Substring(index + 26);
                        continue;
                    }
                    index = o.RelativePath.IndexOf("Perfectus SharedLibrary/");
                    if (index != -1)
                    {
                        o.RelativePath = o.RelativePath.Substring(index + 24);
                        continue;
                    }
                }
            return true;
        }


        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool NeedFixingNormalIssues()
        {
            bool test_ok = true;

            test_ok = test_ok && maybe_fixECHR(m_package.Outcomes);
            test_ok = test_ok && maybe_fixECHR(m_package.Questions);
            test_ok = test_ok && maybe_fixECHR(m_package.Functions);
            test_ok = test_ok && maybe_fixECHR(m_package.SimpleOutcomes);
            test_ok = test_ok && maybe_fixECHR(m_package.Templates);

            foreach (Interview interview in m_package.Interviews)
                test_ok = test_ok && maybe_fixECHR(interview.Pages);

            return !test_ok;
        }

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool FixCriticalIssues()
        {
            bool success = true;
            return success;
        }

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool FixNormalIssues()
        {
            bool test_ok = true;

            test_ok = test_ok && fixECHR(m_package.Outcomes);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Outcomes");
            test_ok = test_ok && fixECHR(m_package.Questions);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Questions");
            test_ok = test_ok && fixECHR(m_package.Functions);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Functions");
            test_ok = test_ok && fixECHR(m_package.SimpleOutcomes);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Conditional Text");
            test_ok = test_ok && fixECHR(m_package.Templates);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Templates");

            foreach (Interview interview in m_package.Interviews)
                test_ok = test_ok && maybe_fixECHR(interview.Pages);
            if (test_ok)
                AddFixNote("Shared Library referrences adjusted for Interview Pages");

            return test_ok;
        }
    }
}
