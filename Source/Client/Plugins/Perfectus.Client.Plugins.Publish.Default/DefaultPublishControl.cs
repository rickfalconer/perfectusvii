using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Perfectus.Client.SDK;
using Perfectus.OdmaSupport;
using Perfectus.Common;

namespace Publish.Default
{
	/// <summary>
	/// Summary description for DefaultPublishControl.
	/// </summary>
	public class DefaultPublishControl : System.Windows.Forms.UserControl, IPublisher
	{
		private System.Windows.Forms.Button btnCopy;
		private System.Windows.Forms.Button btnSaveShortcut;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Label label1;

	
		//ODMA
		private Int32 odmaHandle = int.MinValue;
		private string odmaDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);



		private Guid packageId;
		private int versionNumber;
		private int revisionNumber;
		//private System.Windows.Forms.LinkLabel llblLink;
		//private System.Windows.Forms.Label lblBlurb;
		//private System.Windows.Forms.Button btnSaveLink;
		private string baseUiUrl;
		private string packageName;
		private string myDocumentsURL;
		private System.Windows.Forms.LinkLabel lblLink;
        private SaveFileDialog saveFileDialog1;
        private string suggestedFileName;


		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DefaultPublishControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			OdmaRegister();
			// TODO: Add any initialization after the InitializeComponent call

		}


		public void ResetUI()
		{
			//txtBoxLink.Text = string.Format("{0}/Interview.aspx?easyStart/{1}/{2}/{3}", baseUiUrl, packageId, versionNumber,  revisionNumber);
			lblLink.Text = string.Format("{0}/Interview.aspx?easyStart/{1}/{2}/{3}", baseUiUrl, packageId, versionNumber,  revisionNumber);
		}

		public void Init(System.Windows.Forms.Form openingForm)
		{
		}

		public void Shutdown()
		{
		}

		public string PluginName
		{
            get { return ResourceLoader.GetResourceManager("Publish.Default.Localisation").GetString("SaveEasyStartURL"); }
		}

        public string SuggestedFileName
        {
            get { return suggestedFileName; }
            set { suggestedFileName = value; }
        }

		public string Author
		{
			get { return "Perfectus Solutions"; }
		}

		public Guid PackageId
		{
			get { return packageId; }
			set { packageId = value; }
		}

		public int VersionNumber
		{
			get { return versionNumber; }
			set { versionNumber = value; }
		}

		public int RevisionNumber
		{
			get { return revisionNumber; }
			set { revisionNumber = value; }
		}

		public string BaseUiUrl
		{
			get { return baseUiUrl; }
			set { baseUiUrl = value; }
		}

		public string PackageName
		{
			get { return packageName; }
			set { packageName = value; }
		}

		public string MyDocumentsAddress
		{
			get { return myDocumentsURL; }
			set { myDocumentsURL = value; }
		}


		public string Message
		{
            get { return ResourceLoader.GetResourceManager("Publish.Default.Localisation").GetString("SaveEasyStartURL"); }
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultPublishControl));
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnSaveShortcut = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.LinkLabel();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCopy
            // 
            resources.ApplyResources(this.btnCopy, "btnCopy");
            this.btnCopy.BackColor = System.Drawing.SystemColors.Control;
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnSaveShortcut
            // 
            resources.ApplyResources(this.btnSaveShortcut, "btnSaveShortcut");
            this.btnSaveShortcut.BackColor = System.Drawing.SystemColors.Control;
            this.btnSaveShortcut.Name = "btnSaveShortcut";
            this.btnSaveShortcut.UseVisualStyleBackColor = false;
            this.btnSaveShortcut.Click += new System.EventHandler(this.btnSaveShortcut_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblLink
            // 
            resources.ApplyResources(this.lblLink, "lblLink");
            this.lblLink.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLink.Name = "lblLink";
            this.lblLink.TabStop = true;
            this.lblLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLink_LinkClicked);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "url";
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // DefaultPublishControl
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnSaveShortcut);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.lblLink);
            this.Name = "DefaultPublishControl";
            resources.ApplyResources(this, "$this");
            this.Load += new System.EventHandler(this.DefaultPublishControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private void btnClose_Click(object sender, System.EventArgs e)
		{
			(this.Parent as Form).Close();
		}

		private void DefaultPublishControl_Load(object sender, System.EventArgs e)
		{
            //TODO: Localise
            label1.Text = ResourceLoader.GetResourceManager("Publish.Default.Localisation").GetString("PackageSuccessfullyPublished");
            // Show the new line characters in the Label text
		    label1.Text = label1.Text.Replace("\\n", "\n");
		}

		private void btnSaveShortcut_Click(object sender, System.EventArgs e)
		{

			if (IsOdmaAvailable())
				DMSave();
			else
				StandardSave();
		}

		private bool IsOdmaAvailable()
		{
			return odmaHandle != int.MinValue && odmaHandle != 0;
		}


		private void DMSave()
		{
			int retval;

            // Generate temp file name.
            string urlFilename = Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) ) + ".htm";

            try
            {
                //save the URL file to a temp location on local disk
                try
                {
                    using( StreamWriter sw = new StreamWriter( urlFilename ) )
                    {
                        sw.WriteLine( "<html>" );
                        sw.WriteLine( "<head>" );
                        sw.WriteLine( "<title></title>" );
                        sw.WriteLine( "<META HTTP-EQUIV=Refresh CONTENT=\"0; URL={0}\"", lblLink.Text );
                        sw.WriteLine( "</head>" );
                        sw.WriteLine( "</html>" );
                    }
                }
                catch( Exception ex )
                {
                    MessageBox.Show( ex.Message );
                }



                string format = new string( ' ', OdmaImports.ODM_DOCID_MAX );
                string newDocid = new string( ' ', OdmaImports.ODM_DOCID_MAX );
                string location = new string( ' ', OdmaImports.ODM_FILENAME_MAX );

                retval = OdmaImports.ODMNewDoc( odmaHandle, ref odmaDocid, 0, ref format, ref location );

                if( retval == OdmaImports.ODM_E_CANCEL )
                {

                    StandardSave( );
                    return;
                }
                if( retval != 0 )
                {

                    StandardSave( );
                    return;
                }



                string oldDocid = odmaDocid;
                retval = OdmaImports.ODMSaveAs( odmaHandle, ref oldDocid, ref newDocid, ref format, 0, 0 );

                if( retval == OdmaImports.ODM_E_CANCEL )
                {

                    return;
                }
                if( retval != 0 )
                {

                    StandardSave( );
                    return;
                }

                retval = OdmaImports.ODMOpenDoc( odmaHandle, 0, ref newDocid, ref location );

                try
                {

                    if( location.Trim( ).Trim( '\0' ).Length > 0 )
                    {
                        File.Delete( location.Trim( ).Trim( '\0' ) );
                        File.Move( urlFilename.Trim( ).Trim( '\0' ), location.Trim( ).Trim( '\0' ) );
                    }

                }
                catch( Exception ex )
                {
                    StandardSave( );
                    MessageBox.Show( ex.Message );
                }


                odmaDocid = newDocid;
                retval = OdmaImports.ODMSaveDoc( odmaHandle, ref odmaDocid, ref newDocid );

                OdmaImports.ODMCloseDoc( odmaHandle, ref newDocid, 0, 0, 0, 0 );

                location.Trim( ).Trim( '\0' );
            }
            finally
            {
                // Remove working files in temp folder.
                if( !String.IsNullOrEmpty( urlFilename ) )
                    File.Delete( urlFilename );
            }
		}

		private void StandardSave()
		{
			DialogResult dr = saveFileDialog1.ShowDialog();

			if (dr == DialogResult.OK)
			{
				try
				{
					using(StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
					{
						sw.WriteLine("[InternetShortcut]");	
						sw.WriteLine("URL={0}", lblLink.Text);
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
			}
		}

		private void OdmaRegister()
		{
			Int32 handle = 0;
			string errCode = string.Empty;
			try
			{
				OdmaImports.RegisterDMS(this.Handle.ToInt32(), ref errCode, "HTM", ref handle);
				if (handle != 0 || errCode.ToLower() != "successful")
				{
					this.odmaHandle = handle;
				}
			}
			catch{} // Assume no ODMA support on this machine


		}

		private void btnCopy_Click(object sender, System.EventArgs e)
		{
			Clipboard.SetDataObject(lblLink.Text);
		}

		private void lblLink_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			try
			{
				Process.Start(lblLink.Text);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		


	}
}
