using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Perfectus.Common.PackageObjects;

namespace Perfectus.IPManager.Package
{
    public partial class PackageFixer
    {
        #region FB1019, FB702
        // There is nothing wrong to assign always the same ID to these specific folders
        System.Guid LostFoundQuestionFolderID = new Guid("F7D12D2C-1D2C-4da5-9B05-C7D728882DDC");
        System.Guid LostFoundSimpleOutcomeFolderID = new Guid("A6211CAE-DC8D-448e-A617-331AFB0A5129");
        System.Guid LostFoundOutcomeFolderID = new Guid("4DFDCC40-43E8-4b37-9E50-E05AFD629C98");
        System.Guid LostFoundFunctionFolderID = new Guid("782AAD32-8370-4fb4-8E51-171940F3A7C0");

        private bool FixMissingParentFolder()
        {
            bool success = true;
            bool overall_success = true;
            try
            {

                // Questions
                if (!CheckForMissingParentFolder(m_package.Questions, m_package.QuestionsFolders))
                    success = FixMissingParentFolder(m_package.Questions, m_package.QuestionsFolders);
                overall_success = success && overall_success;

                // Outcomes
                if (!CheckForMissingParentFolder(m_package.Outcomes, m_package.OutcomeFolders))
                    success = success && FixMissingParentFolder(m_package.Outcomes, m_package.OutcomeFolders);
                overall_success = success && overall_success;

                // Conditions
                if (!CheckForMissingParentFolder(m_package.SimpleOutcomes, m_package.SimpleOutcomeFolders))
                    success = success && FixMissingParentFolder(m_package.SimpleOutcomes, m_package.SimpleOutcomeFolders);
                overall_success = success && overall_success;

                // Functions
                if (!CheckForMissingParentFolder(m_package.Functions, m_package.FunctionFolders))
                    success = success && FixMissingParentFolder(m_package.Functions, m_package.FunctionFolders);
                overall_success = success && overall_success;
            }
            catch (Exception e)
            {
                AddFixNote(e.Message);
                return false;
            }

            if (!overall_success)
                AddFixNote("  Not all items can be assigned to folders");
            return overall_success;
        }

        /// <summary>
        /// Folder CreateOrReturnLostFoundFolder(ICollection param_collection)
        /// Add the Lost+Found folder if not already exists
        /// </summary>
        /// <param name="param_collection"></param>
        /// <returns></returns>
        private Folder CreateOrReturnLostFoundFolder(ICollection param_collection)
        {
            Guid guid = Guid.Empty;
            Folder folder = null;

            if (param_collection is QuestionFolderCollection)
                guid = LostFoundQuestionFolderID;
            else
                if (param_collection is SimpleOutcomeFolderCollection)
                    guid = LostFoundSimpleOutcomeFolderID;
                else
                    if (param_collection is OutcomeFolderCollection)
                        guid = LostFoundOutcomeFolderID;
                    else
                        if (param_collection is PFunctionFolderCollection)
                            guid = LostFoundFunctionFolderID;

            foreach (Folder _folder in (ICollection)param_collection)
                if (_folder.UniqueIdentifier == guid)
                    return _folder;

            if (param_collection is QuestionFolderCollection)
                (folder = m_package.CreateQuestionFolder("Lost+Found")).UniqueIdentifier = LostFoundQuestionFolderID;
            else
                if (param_collection is SimpleOutcomeFolderCollection)
                    (folder = m_package.CreateSimpleOutcomeFolder("Lost+Found")).UniqueIdentifier = LostFoundSimpleOutcomeFolderID;
                else
                    if (param_collection is OutcomeFolderCollection)
                        (folder = m_package.CreateOutcomeFolder("Lost+Found")).UniqueIdentifier = LostFoundOutcomeFolderID;
                    else
                        if (param_collection is PFunctionFolderCollection)
                            (folder = m_package.CreatePFunctionFolder("Lost+Found")).UniqueIdentifier = LostFoundFunctionFolderID;

            return folder;
        }

        /// <summary>
        /// AssignItemToFoundFolder(PackageItem item)
        /// Assign Lost+Found folder to item
        /// </summary>
        /// <param name="item"></param>
        private void AssignItemToFoundFolder(PackageItem item)
        {
            if (item is Question)
                item.ParentFolder = CreateOrReturnLostFoundFolder(m_package.QuestionsFolders);
            else if (item is Outcome)
                item.ParentFolder = CreateOrReturnLostFoundFolder(m_package.OutcomeFolders);
            else if (item is SimpleOutcome)
                item.ParentFolder = CreateOrReturnLostFoundFolder(m_package.SimpleOutcomeFolders);
            else if (item is PFunction)
                item.ParentFolder = CreateOrReturnLostFoundFolder(m_package.FunctionFolders);
        }

        /// <summary>
        /// FixMissingParentFolder(ICollection param_item_collection, ICollection param_folder_collection)
        /// </summary>
        /// <param name="param_item_collection"></param>
        /// <param name="param_folder_collection"></param>
        /// <returns></returns>
        private bool FixMissingParentFolder(ICollection param_item_collection, ICollection param_folder_collection)
        {
            foreach (PackageItem item in param_item_collection)
            {
                if (item.ParentFolder == null)
                    continue;

                bool found = false;
                Guid parentFolderID = item.ParentFolder.UniqueIdentifier;

                // There are no folder - bad
                if (param_folder_collection == null)
                    return false;

                foreach (Folder folder in param_folder_collection)
                {
                    FolderCollection folders = folder.GetAllFolders();

                    foreach (Folder anyFolder in folders)
                        if (anyFolder.UniqueIdentifier == parentFolderID)
                        {
                            found = true;
                            break;
                        }
                    if (found) break;
                }
                if (!found)
                {
                    AddFixNote("  Item [" + item.Name + "] parent folder missing");
                    AssignItemToFoundFolder(item);
                }
            }
            return true;
        }


        #endregion

    }
}
