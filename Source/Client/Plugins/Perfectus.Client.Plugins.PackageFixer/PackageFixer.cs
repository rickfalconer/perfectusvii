using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.SDK;
using Perfectus.Common.PackageObjects;
using System.Collections;

namespace Perfectus.IPManager.Package
{
    public partial class PackageFixer : IPackageFixer
    {
        private Form m_openingForm = null;
        private Perfectus.Common.PackageObjects.Package m_package = null;

        const int m_versionNumber = 0;
        const int m_revisionNumber = 0;

        System.Guid m_packageFixerGuid = new Guid("62FA6F22-C0FF-41d5-895E-C3559C3EF201");

        StringBuilder FixNote;
        StringBuilder TestNote;

        public PackageFixer()
        {
            FixNote = new StringBuilder();
            TestNote = new StringBuilder();
        }

        private void AddFixNote(String message)
        {
            FixNote.Append(System.Environment.NewLine + message);
        }
        private void ClearFixNote()
        {
            FixNote.Remove(0, FixNote.Length);
        }
        private void AddTestNote(String message)
        {
            TestNote.Append(System.Environment.NewLine + message);
        }
        private void ClearTestNote()
        {
            TestNote.Remove(0, TestNote.Length);
        }


        #region IStudioPlugin Members

        public string Author
        {
            get { return "Perfectus Solutions"; }
        }

        public void Init(Form openingForm)
        {
            m_openingForm = openingForm;
        }

        public string Message
        {
            get { return "Perfectus IPManager package integrity tool"; }
        }

        public string PluginName
        {
            get { return "IPPackageFixer"; }
        }

        public void Shutdown()
        {
        }

        #endregion

        #region IPackageFixer Members

        public Guid PluginId
        {
            get { return m_packageFixerGuid; }
        }

        public int RevisionNumber
        {
            get { return m_revisionNumber; }
        }

        public int VersionNumber
        {
            get { return m_versionNumber; }
        }

        /// <summary>
        /// NeedFixing
        /// Test the package and indicate whether an integrity issue was found.
        /// The parameter 'param_critical_only' is used to indicate that only those tests are relevant
        /// that prevent the package from being loaded.
        /// Otherwise the test is invoked by user input
        /// </summary>
        /// <param name="param_package"></param>
        /// <param name="param_critical_only"></param>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public bool NeedFixing(Perfectus.Common.PackageObjects.Package param_package, bool param_critical_only, ref string protocol)
        {
            bool no_fix_required = true;
            m_package = param_package;
            ClearTestNote();
            protocol = System.String.Empty;

            // Critcal issues
            no_fix_required = no_fix_required && !NeedFixingCriticalIssues();

            if (!param_critical_only)
                // None-critical issues
                no_fix_required = no_fix_required && !NeedFixingNormalIssues();

            protocol = TestNote.ToString();
            return !no_fix_required;
        }


        /// <summary>
        /// Fix
        /// The function shall run after 'NeedFixing'
        /// </summary>
        /// <param name="param_package"></param>
        /// <param name="param_critical_only"></param>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public bool Fix(ref Perfectus.Common.PackageObjects.Package param_package, bool param_critical_only, ref string protocol)
        {
            bool success = true;
            m_package = param_package;
            ClearFixNote();
            protocol = System.String.Empty;

            // Critcal issues
            success = success && FixCriticalIssues();

            if (!param_critical_only)
                success = success && FixNormalIssues();

            protocol = FixNote.ToString();
            return success;
        }
        #endregion

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool NeedFixingCriticalIssues()
        {
            bool test_ok = true;
            // FB702, FB1019
            test_ok = test_ok && CheckForMissingParentFolder();
            return !test_ok;
        }

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool NeedFixingNormalIssues()
        {
            bool test_ok = true;
            // FB1095
            if (!CheckNamingTable())
                test_ok = false;

            if (!CheckOutcome())
                test_ok = false;

            return !test_ok;
        }

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool FixCriticalIssues()
        {
            bool success = true;
            // FB702, FB1019
            success = success && FixMissingParentFolder();
            return success;
        }

        /// <summary>
        /// NeedFixingCriticalIssues()
        /// Run test for issues that would prevent the package load
        /// </summary>
        /// <returns></returns>
        private bool FixNormalIssues()
        {
            bool test_ok = true;
            // FB1095
            if (!FixNamingTable())
                test_ok = false;
            if (!FixOutcome())
                test_ok = false;

            return test_ok;
        }
    }
}
