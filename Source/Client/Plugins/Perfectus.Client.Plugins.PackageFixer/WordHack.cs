using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using AxDSOFramer;
using DSOFramer;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Word;
using Perfectus.Common;
using Application = Microsoft.Office.Interop.Word.Application;

namespace Perfectus.IPManager.Package
{
	/// <summary>
	/// Summary description for OutcomeWordDialog.
	/// </summary>
	public class Hack : Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private AxDSOFramer.AxFramerControl axFramerControl1;
		private string tempFilePath;
		

		public string GetPathToProperWordML(string wordML)
		{
			SendWordMLtoDisc(wordML);
			SaveWordDoc();
			return tempFilePath;
		}

		private void SendWordMLtoDisc(string wordML)
		{
			if (wordML != null)
			{
				tempFilePath = Path.GetTempFileName();
				using (FileStream fs = File.OpenWrite(tempFilePath))
				{
					byte[] bytes = Encoding.UTF8.GetBytes(wordML);
					fs.Write(bytes, 0, bytes.Length);
					fs.Close();
				}
				LoadWordDoc();
			}
		}


		public static string GetWordMLfromDisc(string tempFilePath, bool deleteFile)
		{
			string wordMl;
			using (StreamReader sr = File.OpenText(tempFilePath))
			{
				wordMl = sr.ReadToEnd();
				sr.Close();
			}

			if(deleteFile)
			{
				File.Delete(tempFilePath);
			}

			return wordMl;
		}

		public Hack()
		{
		
			InitializeComponent();

			Document doc = null;
			Application app = null;

			object oMissing = Missing.Value;
			axFramerControl1.Refresh();

			axFramerControl1.CreateNew("Word.Document");

			// If that worked and we have an active document...
			if (null != axFramerControl1.ActiveDocument)
			{
				doc = axFramerControl1.ActiveDocument as Document;
				if (doc != null)
				{
					// Turn on the tag view
					app = doc.Application;
					app.ActiveWindow.View.ShowXMLMarkup = 1;

					// Create some inline XML to put the document into 'XML Mode'.  Otherwise a Schema is required.
					string tempXml = "<perfectusTemporaryNode uid=\"123\">It is safe to delete this node.</perfectusTemporaryNode>";
					doc.Content.InsertXML(tempXml, ref oMissing);
					object oOnce = 1;
					doc.Undo(ref oOnce);
					app.AutomationSecurity = MsoAutomationSecurity.msoAutomationSecurityLow;
					app.ActiveWindow.View.Type = WdViewType.wdNormalView;
					app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitBestFit;
				}
			}

			axFramerControl1.Visible = true;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (axFramerControl1 != null)
				{
					axFramerControl1.Close();
					object oFalse = false;
					object oMissing = Missing.Value;
					axFramerControl1.Dispose();
					axFramerControl1 = null;
				}

				//	if (File.Exists(tempFilePath))
				//	{
				//		try
				//		{
				//			File.Delete(tempFilePath);
				//		}
				//		catch
				//		{
				//		}
				//	}
				
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}


		private void LoadWordDoc()
		{
			object oMissing = Missing.Value;

			object oFalse = false;
			axFramerControl1.Open(tempFilePath, oFalse, "Word.Document", oMissing, oMissing);

		}

		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Hack));
			this.axFramerControl1 = new AxDSOFramer.AxFramerControl();
			((System.ComponentModel.ISupportInitialize)(this.axFramerControl1)).BeginInit();
			this.SuspendLayout();
			// 
			// axFramerControl1
			// 
			this.axFramerControl1.Enabled = true;
			this.axFramerControl1.Location = new System.Drawing.Point(8, 8);
			this.axFramerControl1.Name = "axFramerControl1";
			this.axFramerControl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axFramerControl1.OcxState")));
			this.axFramerControl1.TabIndex = 0;
			// 
			// Hack
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 273);
			this.Controls.Add(this.axFramerControl1);
			this.Name = "Hack";
			((System.ComponentModel.ISupportInitialize)(this.axFramerControl1)).EndInit();
			this.ResumeLayout(false);

		}

		private void SaveWordDoc()
		{
			if (tempFilePath == null)
			{
				tempFilePath = Path.GetTempFileName();
			}
			object oTempFilePath = tempFilePath;

			object oFileFormat = WdSaveFormat.wdFormatXML;
			object oMissing = Missing.Value;

			Document doc = axFramerControl1.ActiveDocument as Document;
			if (doc != null)

			{
				doc.SaveAs(ref oTempFilePath, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
			}
		}

	}
}
