using System;
using System.Collections;
using System.Text;
using Perfectus.Common.PackageObjects;
using System.Xml;

namespace Perfectus.IPManager.Package
{
    public partial class PackageFixer
    {
        #region FB1095
        private bool FixNamingTable()
        {
            if ( CheckNamingTable () )
                return true;

            try
            {
                m_package.NamesTable = new Hashtable();

                // Objects
                foreach (PackageItem item in m_package.Templates)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.Servers)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.Interviews)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());
                        
                foreach (PackageItem item in m_package.DataSources)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.WebReferenceByGuid.Values)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                
                // Items
                foreach (PackageItem item in m_package.Questions)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.Outcomes)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.SimpleOutcomes)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                foreach (PackageItem item in m_package.Functions)
                    m_package.EnsureUniqueName(item.Name, item.UniqueIdentifier.ToString());

                // Folders
                foreach (Folder parentFolder in m_package.QuestionsFolders)
                    if (parentFolder != null)
                        foreach (Folder anyFolder in parentFolder.GetAllFolders())
                            m_package.EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

                foreach (Folder parentFolder in m_package.OutcomeFolders)
                    if (parentFolder != null)
                        foreach (Folder anyFolder in parentFolder.GetAllFolders())
                            m_package.EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

                foreach (Folder parentFolder in m_package.FunctionFolders)
                    if (parentFolder != null)
                        foreach (Folder anyFolder in parentFolder.GetAllFolders())
                            m_package.EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());

                foreach (Folder parentFolder in m_package.SimpleOutcomeFolders)
                    if (parentFolder != null)
                        foreach (Folder anyFolder in parentFolder.GetAllFolders())
                            m_package.EnsureUniqueName(anyFolder.Name, anyFolder.UniqueIdentifier.ToString());
            }
            catch (Exception e)
            {
                AddFixNote(e.Message);
                return false;
            }
            AddFixNote ("   Naming table recreated");
            return true;
        }
        #endregion

        #region FB1053
        /// <summary>
        /// Iterate through all outcomes and find/remove footers,
        /// find PackageItems in WordML. Maybe correct the Guid, if item
        /// exist, but cannoit be found by GUID.
        /// </summary>
        /// <returns></returns>
        private bool FixOutcome()
        {
            bool success = true;
            foreach (Outcome outcome in m_package.Outcomes)
            {
                if (outcome.Definition == null)
                    continue;
                ActionBase[] actions = outcome.Definition.AllPossibleActions;
                foreach (ActionBase action in actions)
                    if (action is OutcomeAction && action.SubQuery == null)
                        if (!FixAction((OutcomeAction)action, outcome.Name))
                            success = false;
            }
            return success;
        }

        /// <summary>
        /// private bool FixAction(PObjects.OutcomeAction action)
        /// Test one outcome
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        private bool FixAction(OutcomeAction action, String param_outcome)
        {
            bool success = true;
            XmlDocument wordML = new XmlDocument();
            if (action.WordML != null)
            {
                try
                {
                    wordML.LoadXml(action.WordML);
                }
                catch (Exception e)
                {
                    AddFixNote("Failed to read XML for Outcome\n" + e.Message);
                    return false;
                }

                try
                {
                    if (!FixBadFooterWordML(wordML, action, param_outcome)
                        ) success = false;

                    // Validate outcome action output - child package items have valid GUIDS
                    if (!FixWordMLMarkupGUIDS(ref wordML, param_outcome))
                        success = false;
                    action.WordML = wordML.OuterXml;
                }
                catch (Exception e)
                {
                    AddFixNote("Failed to process XML for Outcome " + e.Message);
                    return false;
                }
            }
            return success;
        }

        /// <summary>
        ///		Validates the given WordML to ensure the referenced package items (pink bits) have valid GUIDs
        /// </summary>
        /// <param name="package">The given package being processed.</param>
        /// <param name="wordML">The WordML being validated.</param>
        /// <returns>Flag indicating whether an update has been made.</returns>
        private bool FixWordMLMarkupGUIDS(ref XmlDocument wordML, String param_outcome)
        {
            bool success = true;
            foreach (XmlNode node in wordML.SelectNodes("//*[@uid]"))
            {
                // see if @uID exists. check outcomes, questions, and conditional texts
                Guid uid = new Guid(node.Attributes["uid"].Value);
                string name = node.LocalName;
                PackageItem item = FindItem(uid);
                if (item == null)
                {
                    item = FindItem(name);
                    if (item != null)
                    {
                        node.Attributes["uid"].Value = item.UniqueIdentifier.ToString();
                        AddFixNote(String.Format("Outcome {0} repaired",param_outcome));
                    }
                    else
                    {
                        // ToDo... Remove broken pink bits
                        //node.RemoveAll();
                        AddFixNote("Cannot remove now .....");
                        success = false;
                    }
                }
            }
            return success;
        }

        // Could probably make this change in the merge, e.g dont copy it! rather than having a fixer app.
        private bool FixBadFooterWordML(XmlDocument wordML, OutcomeAction action, String param_outcome)
        {
            foreach (XmlNode node in wordML.GetElementsByTagName("w:ftr"))
            {
                node.ParentNode.RemoveChild(node);
                AddFixNote(String.Format("Invalid footer removed in outcome {0}", param_outcome));

                string path;
                using (Hack hack = new Hack())
                {
                    // Opening and closing our 'fixed' wordML in word as otherwise there are a couple of errors (after in the interview, in the merge) i dont have time to investigate. Word is cleaning something up. dodgy.
                    path = hack.GetPathToProperWordML(wordML.OuterXml);
                }

                action.WordML = Hack.GetWordMLfromDisc(path, true);
                break;
            }
            return true;
        }
        #endregion

    }
}
