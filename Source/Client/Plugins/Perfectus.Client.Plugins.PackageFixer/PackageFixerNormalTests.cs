using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using System.Xml;
using Perfectus.Common.PackageObjects;

namespace Perfectus.IPManager.Package
{
    public partial class PackageFixer
    {
        #region FB1095
        private bool CheckNamingTable()
        {
            System.Collections.Hashtable testTable = new System.Collections.Hashtable();

            foreach (String str in m_package.NamesTable.Values)
            {
                if (str == null || str.Length == 0)
                {
                    AddTestNote("  Internal name table has invalid entries");
                    return false;
                }
                // Valid guid?
                try
                {
                    Guid guid = new Guid(str);
                    if (guid == Guid.Empty)
                    {
                        AddTestNote("  Internal name table has empty identifier");
                        return false;
                    }
                }
                catch (Exception)
                {
                    AddTestNote("  Internal name table has invalid identifier");
                    return false;
                }

                try
                {
                    testTable.Add(str, null);
                }
                catch (Exception)
                {
                    AddTestNote("  Internal name table contains duplicated names");
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region FB1053

        /// <summary>
        /// Iterate through all outcomes and find/remove footers,
        /// find PackageItems in WordML. Maybe correct the Guid, if item
        /// exist, but cannoit be found by GUID.
        /// </summary>
        /// <returns></returns>
        private bool CheckOutcome()
        {
            bool success = true;
            foreach (Outcome outcome in m_package.Outcomes)
            {
                if (outcome.Definition == null)
                    continue;
                ActionBase[] actions = outcome.Definition.AllPossibleActions;
                foreach (ActionBase action in actions)
                    if (action is OutcomeAction && action.SubQuery == null)
                        if (!CheckAction((OutcomeAction)action, outcome.Name))
                            success = false;
            }
            // return false ::= At least one issue can be fixed
            return success;
        }

        /// <summary>
        /// private bool CheckAction(PObjects.OutcomeAction action)
        /// Test one outcome
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        private bool CheckAction(OutcomeAction action, String param_OutcomeName)
        {
            bool success = true;
            XmlDocument wordML = new XmlDocument();
            if (action.WordML != null)
            {
                try
                {
                    wordML.LoadXml(action.WordML);
                }
                catch (Exception e )
                {
                    AddTestNote("Failed to read XML for Outcome\n" + e.Message);
                    return false;
                }

                try
                {
                    if (!CheckBadFooterWordML(wordML, param_OutcomeName)
                        ) success = false;

                    // Validate outcome action output - child package items have valid GUIDS
                    if (!CheckWordMLMarkupGUIDS(wordML, param_OutcomeName))
                        success = false;
                }
                catch (Exception e)
                {
                    AddTestNote("Failed to process XML for Outcome " + e.Message);
                    return false;
                }
            }
            return success;
        }

        /// <summary>
        /// Find a PackageItem in Questions, Conditions or Outcomes 
        /// by name or Guid
        /// </summary>
        /// <param name="param_guid"></param>
        /// <param name="param_collection"></param>
        /// <returns></returns>
        private PackageItem FindItem(Guid param_guid, ICollection param_collection)
        {
            foreach (PackageItem item in param_collection)
                if (item.UniqueIdentifier == param_guid)
                    return item;
            return null;
        }
        private PackageItem FindItem(String param_name, ICollection param_collection)
        {
            foreach (PackageItem item in param_collection)
                if (String.Compare(item.Name, param_name, true) == 0)
                    return item;
            return null;
        }
        private PackageItem FindItem(Guid param_guid)
        {
            PackageItem item = FindItem(param_guid, m_package.SimpleOutcomes);
            if (item == null)
                item = FindItem(param_guid, m_package.Questions);
            if (item == null)
                item = FindItem(param_guid, m_package.Outcomes);
            return item;
        }
        private PackageItem FindItem(String param_name)
        {
            PackageItem item = FindItem(param_name, m_package.SimpleOutcomes);
            if (item == null)
                item = FindItem(param_name, m_package.Questions);
            if (item == null)
                item = FindItem(param_name, m_package.Outcomes);
            return item;
        }
        /// <summary>
        ///		Validates the given WordML to ensure the referenced package items (pink bits) have valid GUIDs
        /// </summary>
        /// <param name="package">The given package being processed.</param>
        /// <param name="wordML">The WordML being validated.</param>
        /// <returns>Flag indicating whether an update has been made.</returns>
        private bool CheckWordMLMarkupGUIDS(XmlDocument wordML, String param_OutcomeName)
        {
            bool success = true;
            foreach (XmlNode node in wordML.SelectNodes("//*[@uid]"))
            {
                // see if @uID exists. check outcomes, questions, and conditional texts
                Guid uid = new Guid(node.Attributes["uid"].Value);
                string name = node.LocalName;
                PackageItem item = FindItem(uid);
                if (item == null)
                {
                    AddTestNote(String.Format("Item {0} in outcome {1} not found", name, param_OutcomeName));
                    success = false;
                }
            }
            return success;
        }

        // Could probably make this change in the merge, e.g dont copy it! rather than having a fixer app.
        private bool CheckBadFooterWordML(XmlDocument wordML, string param_OutcomeName)
        {
            foreach (XmlNode node in wordML.GetElementsByTagName("w:ftr"))
            {
                 AddTestNote(String.Format("Outcome {0} contains invalid footer", param_OutcomeName));
                return false;
            }
            return true;
        }
        #endregion
    }
}

