using System;
using System.Collections.Generic;
using System.Text;

using Perfectus.Client.SDK;
using Perfectus.Common.PackageObjects;
using System.Collections;

namespace Perfectus.IPManager.Package
{
    public partial class PackageFixer
    {
        #region FB1019, FB702
        /// <summary>
        /// bool CheckForMissingParentFolder(ICollection param_item_collection, ICollection param_folder_collection)
        /// Match the item in the first collection to the items in the second collection
        /// </summary>
        /// <param name="param_item_collection"></param>
        /// <param name="param_folder_collection"></param>
        /// <returns></returns>
        private bool CheckForMissingParentFolder(ICollection param_item_collection, ICollection param_folder_collection)
        {
            bool success = true;
            foreach (PackageItem item in param_item_collection)
            {
                // Is it a root object?
                if (item.ParentFolder == null)
                    continue;

                // There are no folder - bad
                if (param_folder_collection == null)
                {
                    AddTestNote ( "   missing folders for type " + item.GetType().ToString());
                    return false;
                }

                bool found = false;
                Guid parentFolderID = item.ParentFolder.UniqueIdentifier;

                foreach (Folder rootFolder in param_folder_collection)
                {
                    // A folder on root level can have many children
                    FolderCollection allFolders = rootFolder.GetAllFolders();
                    foreach (Folder folder in allFolders)
                        if (folder.UniqueIdentifier == parentFolderID)
                        {
                            found = true;
                            break;
                        }
                    if (found) break;
                }
                if (!found)
                {
                    AddTestNote ("  The item ["+item.Name+"] cannot be assigned to a parent folder");
                    success = false;
                }
            }
            return success;
        }

        /// <summary>
        /// CheckForMissingParentFolder()
        /// Break up per type of PackageItem
        /// </summary>
        /// <returns></returns>
        private bool CheckForMissingParentFolder()
        {
            bool success = true;
            success = success && CheckForMissingParentFolder(m_package.Questions, m_package.QuestionsFolders);
            success = success && CheckForMissingParentFolder(m_package.Outcomes, m_package.OutcomeFolders);
            success = success && CheckForMissingParentFolder(m_package.SimpleOutcomes, m_package.SimpleOutcomeFolders);
            success = success && CheckForMissingParentFolder(m_package.Functions, m_package.FunctionFolders);
            if (!success)
                AddFixNote("  Not all items can be assigned to folders");
            return success;
        }
#endregion
    }
}
