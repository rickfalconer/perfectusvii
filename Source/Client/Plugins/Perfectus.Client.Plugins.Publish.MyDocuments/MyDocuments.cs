using System;
using System.Net;
using System.Windows.Forms;
using System.Xml;
using Perfectus.Client.SDK;
using Publish.MyDocuments.PerfectusMyDocuments.Folders;


namespace Publish.MyDocuments
{
	/// <summary>
	/// Summary description for MyDocuments.
	/// </summary>
	public class MyDocuments : System.Windows.Forms.UserControl, IPublisher
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private Guid packageId;
		private int versionNumber;
		private int revisionNumber;
		private string baseUiUrl;
		private string packageName;
		private string myDocumentsURL;
        private string suggestedFileName;
		private System.Windows.Forms.Button btnSave;
		private Publish.Default.DefaultPublishControl defaultPublishControl2;
		private XmlDocument folders = null;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.Label label1;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public MyDocuments()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// 

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.btnSave = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.defaultPublishControl2 = new Publish.Default.DefaultPublishControl();
			this.label1 = new System.Windows.Forms.Label();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(448, 176);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.treeView1);
			this.tabPage1.Controls.Add(this.btnSave);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(440, 150);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "MyDocuments";
			this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
			// 
			// treeView1
			// 
			this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.treeView1.ImageIndex = -1;
			this.treeView1.Location = new System.Drawing.Point(7, 8);
			this.treeView1.Name = "treeView1";
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.Size = new System.Drawing.Size(426, 107);
			this.treeView1.TabIndex = 2;
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// btnSave
			// 
			this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.btnSave.Location = new System.Drawing.Point(359, 122);
			this.btnSave.Name = "btnSave";
			this.btnSave.TabIndex = 1;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.defaultPublishControl2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(440, 150);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Desktop";
			// 
			// defaultPublishControl2
			// 
			this.defaultPublishControl2.BackColor = System.Drawing.SystemColors.Control;
			this.defaultPublishControl2.BaseUiUrl = null;
			this.defaultPublishControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.defaultPublishControl2.Location = new System.Drawing.Point(0, 0);
			this.defaultPublishControl2.MyDocumentsAddress = null;
			this.defaultPublishControl2.Name = "defaultPublishControl2";
			this.defaultPublishControl2.PackageId = new System.Guid("00000000-0000-0000-0000-000000000000");
			this.defaultPublishControl2.PackageName = null;
			this.defaultPublishControl2.RevisionNumber = 0;
			this.defaultPublishControl2.Size = new System.Drawing.Size(440, 150);
			this.defaultPublishControl2.TabIndex = 0;
			this.defaultPublishControl2.VersionNumber = 0;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(10, 126);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(341, 19);
			this.label1.TabIndex = 3;
			this.label1.Text = "label1";
			// 
			// MyDocuments
			// 
			this.Controls.Add(this.tabControl1);
			this.Name = "MyDocuments";
			this.Size = new System.Drawing.Size(448, 176);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		public void Init(Form openingForm)
		{
			
		}

		public void Shutdown()
		{
		}

		public string PluginName
		{
			get { return "MyDocuments"; }
		}

		public string Author
		{
			get { return "Perfectus Solutions"; }
		}

        public string SuggestedFileName 
        { 
            get { return suggestedFileName;  } 
            set { suggestedFileName = value; } 
        }

		public void ResetUI()
		{
			label1.Text = "";

            try
            {
                addFolders();
            }
            catch (Exception ex)
            {
                label1.Text = "Error getting folders";
            }
			
			btnSave.Enabled = false;
			try
			{
				defaultPublishControl2.ResetUI();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void addFolders()
		{
           
			string serviceUrl = MyDocumentsAddress + "/publishing/folders.asmx";
			Folders f = new Folders();
			f.Url = serviceUrl;
			f.Credentials = CredentialCache.DefaultCredentials;

            string xml = f.GetFolders().Trim();
            folders = new XmlDocument();
            folders.LoadXml(xml);
            populateTreeControl(folders.DocumentElement, treeView1.Nodes);

            if (treeView1.Nodes.Count == 0)
            {
                label1.Text = "No matched folders for publishing";
            }
		}
		private void populateTreeControl(System.Xml.XmlNode document, System.Windows.Forms.TreeNodeCollection nodes)
		{
			foreach (XmlNode node in document.ChildNodes)
			{
				string text = node.Attributes[0].Value;
				string id = node.Attributes[1].Value;
				TreeNode new_child = new TreeNode(text);
				new_child.Tag = id.ToString();
				nodes.Add(new_child);
				populateTreeControl(node, new_child.Nodes);
			}
		}	

		private void btnSave_Click(object sender, System.EventArgs e)
		{
            try
            {
                string serviceUrl = MyDocumentsAddress + "/publishing/publish.asmx";
                PerfectusMyDocuments.Publish.Publish publisher = new PerfectusMyDocuments.Publish.Publish();
                publisher.Url = serviceUrl;
                publisher.Credentials = CredentialCache.DefaultCredentials;
                publisher.publishPackage(PackageId, VersionNumber, RevisionNumber, packageName, Convert.ToInt32(treeView1.SelectedNode.Tag.ToString()));
                label1.Text = "Package saved";
            }
            catch
            {
                label1.Text = "Error saving package";
            }
			
		}

		private void treeView1_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			btnSave.Enabled = true;
		}


		private void tabPage1_Click(object sender, System.EventArgs e)
		{
		
		}


		
		public Guid PackageId
		{
			get { return packageId; }
			set { packageId = value; 
			defaultPublishControl2.PackageId = value;}
		}

		public int VersionNumber
		{
			get { return versionNumber; }
			set { versionNumber = value;
			defaultPublishControl2.VersionNumber = value;}
		}

		public int RevisionNumber
		{
			get { return revisionNumber; }
			set { revisionNumber = value; 
			defaultPublishControl2.RevisionNumber = value;}
		}

		public string BaseUiUrl
		{
			get { return baseUiUrl; }
			set { baseUiUrl = value; 
			defaultPublishControl2.BaseUiUrl = value;}
		}

		public string PackageName
		{
			get { return packageName; }
			set 
			{
				packageName = value; 
				packageName = value;}
		}

		public string MyDocumentsAddress
		{
			get { return myDocumentsURL; }
			set { myDocumentsURL = value; }
		}


		public string Message
		{
			get { return "MyDocuments"; }
		}
	}
}
