﻿using System;
using System.Reflection;

// Assembly specific information
[assembly: AssemblyTitle("Perfectus.Client.Configuration")]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs