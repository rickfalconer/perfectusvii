using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Perfectus.Client.Configuration
{
    public static class ConfigHandler
    {
        static private object _l = new object();
        static System.Configuration.Configuration _configFile = null;
        private static System.Configuration.Configuration configFile
        {
            get
            {
                if (null == _configFile)
                {
                    ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                    fileMap.ExeConfigFilename = System.Windows.Forms.Application.ExecutablePath + ".config";
                    _configFile = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
                }
                return _configFile;
            }
        }

        public static ConfigurationSection GetConfiguration(String param_configuration_name)
        {
            lock (_l)
            {
                ConfigurationSection configuration = configFile.GetSection(param_configuration_name);
                return configuration;
            }
        }

        public static Boolean SaveConfiguration(ConfigurationSection param_configuration)
        {
            try
            {
                lock (_l)
                {
                    param_configuration.SectionInformation.ForceSave = true;
                    configFile.Save(ConfigurationSaveMode.Full);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static Boolean SetConfiguration(String param_configuration_name, ConfigurationSection param_configuration)
        {
            try
            {
                ConfigurationSection configuration = GetConfiguration(param_configuration_name);

                if (configuration != null)
                    return false;

                lock (_l)
                {
                    configFile.Sections.Add(param_configuration_name, param_configuration);
                    configuration = configFile.GetSection(param_configuration_name);

                    configuration.SectionInformation.ForceSave = true;
                    configFile.Save(ConfigurationSaveMode.Full);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}