using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.SDK;

namespace Perfectus.Client.Configuration
{
    public partial class OptionForm : Form
    {
        public OptionForm()
        {
            InitializeComponent();
        }

        public void AddConfiguration(String param_name, IConfigurationUserControl param_ctrl)
        {
            TabPage tabPage = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            tabPage.SuspendLayout();
            tabPage.Controls.Add(param_ctrl as UserControl);
            tabPage.Location = new System.Drawing.Point(4, 22);
            tabPage.Name = param_name;
            tabPage.Padding = new System.Windows.Forms.Padding(3);
            tabPage.Size = new System.Drawing.Size(260, 319);
            tabPage.TabIndex = 0;
            tabPage.Text = param_name;
            tabPage.UseVisualStyleBackColor = true;
            this.tabControl.Controls.Add(tabPage);
            this.tabControl.ResumeLayout(false);
            tabPage.ResumeLayout(false);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // Call each control in each tab...
            foreach (TabPage page in this.tabControl.TabPages)
                foreach (Control ctrl in page.Controls)
                    if (ctrl is IConfigurationUserControl)
                    {
                        IConfigurationUserControl configCtrl = ctrl as IConfigurationUserControl;
                        configCtrl.Commit();
                    }
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            // Call each control in each tab...
            foreach (TabPage page in this.tabControl.TabPages)
                foreach (Control ctrl in page.Controls)
                    if (ctrl is IConfigurationUserControl)
                    {
                        IConfigurationUserControl configCtrl = ctrl as IConfigurationUserControl;
                        configCtrl.Rollback();
                    }
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            // Call each control in each tab...
            foreach (TabPage page in this.tabControl.TabPages)
                foreach (Control ctrl in page.Controls)
                    if (ctrl is IConfigurationUserControl)
                    {
                        IConfigurationUserControl configCtrl = ctrl as IConfigurationUserControl;
                        if ( configCtrl.CanDefault )
                            configCtrl.Default();
                    }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is TabControl)
            {
                TabPage page = (sender as TabControl).SelectedTab;
                if (page.Controls[0] is IConfigurationUserControl)
                {
                    IConfigurationUserControl configCtrl = page.Controls[0] as IConfigurationUserControl;
                    this.btnDefault.Enabled = configCtrl.CanDefault;
                }
                else
                this.btnDefault.Enabled = false;
            }
        }
    }
}