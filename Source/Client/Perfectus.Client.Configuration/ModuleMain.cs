using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Windows.Forms;
using System.Reflection;
using Perfectus.Client.SDK;
using Perfectus.Client.Configuration.QueryEditConfiguration;

namespace Perfectus.Client.Configuration
{

    public static class Handler
    {
        private static List<IConfiguration> _plugins = new List<IConfiguration>();

        public static void AddPlugin(IConfiguration param_plugin)
        {
            _plugins.Add(param_plugin);
        }
        public static DialogResult ShowOptionForm()
        {
            if (_plugins.Count == 0)
                return DialogResult.Abort;

            OptionForm configForm = new OptionForm();

            // Loop through all configuration plugin's
            foreach (IConfiguration plugin in _plugins)
            {
                // Each plugin might have zero, one or many custom controls that
                // shall be inserted into the card
                String[] configNames = plugin.getConfigurations();
                foreach (String configName in configNames)
                {
                    // Add each control as a card
                    IConfigurationUserControl ctrl = plugin.getConfigCtrl(configName);
                    if (null != ctrl)
                        configForm.AddConfiguration(configName, ctrl);
                }
            }
            return configForm.ShowDialog();
        }

        /// The following section should lookup, load and return/modify values using 
        /// reflection
        // Object
        public static object GetSetting(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("Not implemented yet");
        }
        public static void SetSetting(string param_configuration_section_name, string param_setting, object param_value)
        {
            throw new Exception("Not implemented yet");
        }
        // Integer
        public static int GetSettingInteger(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("Not implemented yet");
        }
        public static void SetSettingInteger(string param_configuration_section_name, string param_setting, int param_value)
        {
            throw new Exception("Not implemented yet");
        }
        // Double
        public static double GetSettingDouble(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("Not implemented yet");
        }
        public static void SetSettingDouble(string param_configuration_section_name, string param_setting, double param_value)
        {
            throw new Exception("Not implemented yet");
        }
        // String
        public static string GetSettingString(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("Not implemented yet");
        }
        public static void SetSettingString(string param_configuration_section_name, string param_setting, string param_value)
        {
            throw new Exception("Not implemented yet");
        }
        // Boolean
        public static bool GetSettingBoolean(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("Not implemented yet");
        }
        public static void SetSettingBoolean(string param_configuration_section_name, string param_setting, bool param_value)
        {
            throw new Exception("Not implemented yet");
        }
    }

    /// <summary>
    /// used to initilaize and store configurations
    /// </summary>
    public class ModuleMain : IConfiguration
    {
        const String cName = "Query Editor";
        const String cConfigurationName = "QueryEditorConfigurationSection";
    

        public ModuleMain ()
        {
        }
        #region IConfiguration Members

        /// <summary>
        /// Return all configuration names
        /// </summary>
        /// <returns></returns>
        public string[] getConfigurations()
        {
            return new String[] { cName };
        }

        #region IConfiguration Members
        /// <summary>
        /// Create and return the UserControl defined by the name
        /// </summary>
        /// <param name="param_name">Configuration name</param>
        /// <returns>User control</returns>
        IConfigurationUserControl IConfiguration.getConfigCtrl(String param_name)
        {
            if (param_name.CompareTo(cName) == 0)
                return new QESetupUserControl(cConfigurationName);
            return null;
        }
        #endregion

        public ConfigurationSection GetConfig(string param_configuration_section_name)
        {
            if (param_configuration_section_name.CompareTo(cConfigurationName) == 0)
                return ConfigHandler.GetConfiguration(cConfigurationName);
            return null;
        }

        public void SaveConfig(ConfigurationSection param_configuration)
        {
            if (!(param_configuration is ConfigurationSection))
                throw new Exception("Bad type");
            ConfigHandler.SaveConfiguration(param_configuration);
        }

        #endregion

        #region IStudioPlugin Members

        public string Author
        {
            get { return "Perfectus Solutions"; }
        }

        public void Init(Form openingForm)
        {
        }

        public string Message
        {
            get { return "Perfectus IPManager Configuration helper"; }
        }

        public string PluginName
        {
            get { return "ConfigurationPlugin"; }
        }

        public void Shutdown()
        {
        }

        #endregion

        #region IConfiguration Members


        public object GetSetting(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSetting(string param_configuration_section_name, string param_setting, object param_value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int GetSettingInteger(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSettingInteger(string param_configuration_section_name, string param_setting, int param_value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public double GetSettingDouble(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSettingDouble(string param_configuration_section_name, string param_setting, double param_value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public string GetSettingString(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSettingString(string param_configuration_section_name, string param_setting, string param_value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public bool GetSettingBoolean(string param_configuration_section_name, string param_setting)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void SetSettingBoolean(string param_configuration_section_name, string param_setting, bool param_value)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        
    }
}
