namespace Perfectus.Client.Configuration.QueryEditConfiguration
{
    partial class QESetupUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QESetupUserControl));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbAsk = new System.Windows.Forms.RadioButton();
            this.rbBasicEditor = new System.Windows.Forms.RadioButton();
            this.rbAdvancedEditor = new System.Windows.Forms.RadioButton();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zoomComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSetColor = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbAsk);
            this.groupBox3.Controls.Add(this.rbBasicEditor);
            this.groupBox3.Controls.Add(this.rbAdvancedEditor);
            resources.ApplyResources(this.groupBox3, "groupBox3");
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.TabStop = false;
            // 
            // rbAsk
            // 
            resources.ApplyResources(this.rbAsk, "rbAsk");
            this.rbAsk.Checked = true;
            this.rbAsk.Name = "rbAsk";
            this.rbAsk.TabStop = true;
            this.rbAsk.UseVisualStyleBackColor = true;
            this.rbAsk.Click += new System.EventHandler(this.cbEditor_SelectedIndexChanged);
            // 
            // rbBasicEditor
            // 
            resources.ApplyResources(this.rbBasicEditor, "rbBasicEditor");
            this.rbBasicEditor.Name = "rbBasicEditor";
            this.rbBasicEditor.UseVisualStyleBackColor = true;
            this.rbBasicEditor.Click += new System.EventHandler(this.cbEditor_SelectedIndexChanged);
            // 
            // rbAdvancedEditor
            // 
            resources.ApplyResources(this.rbAdvancedEditor, "rbAdvancedEditor");
            this.rbAdvancedEditor.Name = "rbAdvancedEditor";
            this.rbAdvancedEditor.UseVisualStyleBackColor = true;
            this.rbAdvancedEditor.Click += new System.EventHandler(this.cbEditor_SelectedIndexChanged);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            resources.ApplyResources(this.listView1, "listView1");
            this.listView1.Name = "listView1";
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.zoomComboBox);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // zoomComboBox
            // 
            this.zoomComboBox.FormattingEnabled = true;
            this.zoomComboBox.Items.AddRange(new object[] {
            resources.GetString("zoomComboBox.Items"),
            resources.GetString("zoomComboBox.Items1"),
            resources.GetString("zoomComboBox.Items2"),
            resources.GetString("zoomComboBox.Items3"),
            resources.GetString("zoomComboBox.Items4"),
            resources.GetString("zoomComboBox.Items5"),
            resources.GetString("zoomComboBox.Items6"),
            resources.GetString("zoomComboBox.Items7"),
            resources.GetString("zoomComboBox.Items8")});
            resources.ApplyResources(this.zoomComboBox, "zoomComboBox");
            this.zoomComboBox.Name = "zoomComboBox";
            this.zoomComboBox.SelectedIndexChanged += new System.EventHandler(this.zoom_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetColor);
            resources.ApplyResources(this.groupBox2, "groupBox2");
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.TabStop = false;
            // 
            // btnSetColor
            // 
            resources.ApplyResources(this.btnSetColor, "btnSetColor");
            this.btnSetColor.Name = "btnSetColor";
            this.btnSetColor.UseVisualStyleBackColor = true;
            this.btnSetColor.Click += new System.EventHandler(this.btnSetColor_Click);
            // 
            // QESetupUserControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "QESetupUserControl";
            this.Load += new System.EventHandler(this.CodeEditUserControlSetup_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbAsk;
        private System.Windows.Forms.RadioButton rbBasicEditor;
        private System.Windows.Forms.RadioButton rbAdvancedEditor;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox zoomComboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSetColor;

    }
}
