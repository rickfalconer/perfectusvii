namespace Perfectus.Client.Configuration
{
    partial class QESetupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnDefault = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zoomComboBox = new System.Windows.Forms.ComboBox();
            this.btnSetColor = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbAsk = new System.Windows.Forms.RadioButton();
            this.rbBasicEditor = new System.Windows.Forms.RadioButton();
            this.rbAdvancedEditor = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(133, 325);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(52, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(202, 325);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(52, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnDefault
            // 
            this.btnDefault.Location = new System.Drawing.Point(61, 325);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(52, 23);
            this.btnDefault.TabIndex = 5;
            this.btnDefault.Text = "Default";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.zoomComboBox);
            this.groupBox1.Location = new System.Drawing.Point(6, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 50);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Text size";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Initial zoom";
            // 
            // zoomComboBox
            // 
            this.zoomComboBox.FormattingEnabled = true;
            this.zoomComboBox.Items.AddRange(new object[] {
            "50%",
            "75%",
            "90%",
            "95%",
            "100%",
            "110%",
            "125%",
            "150%",
            "200%"});
            this.zoomComboBox.Location = new System.Drawing.Point(69, 19);
            this.zoomComboBox.MaxDropDownItems = 6;
            this.zoomComboBox.Name = "zoomComboBox";
            this.zoomComboBox.Size = new System.Drawing.Size(121, 21);
            this.zoomComboBox.TabIndex = 0;
            this.zoomComboBox.Text = "100%";
            this.zoomComboBox.SelectedIndexChanged += new System.EventHandler(this.zoom_SelectedIndexChanged);
            // 
            // btnSetColor
            // 
            this.btnSetColor.Location = new System.Drawing.Point(196, 113);
            this.btnSetColor.Name = "btnSetColor";
            this.btnSetColor.Size = new System.Drawing.Size(52, 23);
            this.btnSetColor.TabIndex = 2;
            this.btnSetColor.Text = "Color";
            this.btnSetColor.UseVisualStyleBackColor = true;
            this.btnSetColor.Click += new System.EventHandler(this.btnSetColor_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView1.Location = new System.Drawing.Point(15, 188);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(181, 115);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 200;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSetColor);
            this.groupBox2.Location = new System.Drawing.Point(6, 167);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 142);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Colors";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbAsk);
            this.groupBox3.Controls.Add(this.rbBasicEditor);
            this.groupBox3.Controls.Add(this.rbAdvancedEditor);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 87);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Outcome Editor";
            // 
            // rbAsk
            // 
            this.rbAsk.AutoSize = true;
            this.rbAsk.Location = new System.Drawing.Point(9, 64);
            this.rbAsk.Name = "rbAsk";
            this.rbAsk.Size = new System.Drawing.Size(78, 17);
            this.rbAsk.TabIndex = 2;
            this.rbAsk.Text = "Always ask";
            this.rbAsk.UseVisualStyleBackColor = true;
            this.rbAsk.CheckedChanged += new System.EventHandler(this.cbEditor_SelectedIndexChanged);
            // 
            // rbBasicEditor
            // 
            this.rbBasicEditor.AutoSize = true;
            this.rbBasicEditor.Location = new System.Drawing.Point(9, 41);
            this.rbBasicEditor.Name = "rbBasicEditor";
            this.rbBasicEditor.Size = new System.Drawing.Size(112, 17);
            this.rbBasicEditor.TabIndex = 1;
            this.rbBasicEditor.Text = "Basic Query Editor";
            this.rbBasicEditor.UseVisualStyleBackColor = true;
            this.rbBasicEditor.CheckedChanged += new System.EventHandler(this.cbEditor_SelectedIndexChanged);
            // 
            // rbAdvancedEditor
            // 
            this.rbAdvancedEditor.AutoSize = true;
            this.rbAdvancedEditor.Checked = true;
            this.rbAdvancedEditor.Location = new System.Drawing.Point(9, 19);
            this.rbAdvancedEditor.Name = "rbAdvancedEditor";
            this.rbAdvancedEditor.Size = new System.Drawing.Size(135, 17);
            this.rbAdvancedEditor.TabIndex = 0;
            this.rbAdvancedEditor.TabStop = true;
            this.rbAdvancedEditor.Text = "Advanced Query Editor";
            this.rbAdvancedEditor.UseVisualStyleBackColor = true;
            // 
            // QESetupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 351);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "QESetupForm";
            this.Text = "Advanced Query Editor Configuration";
            this.Load += new System.EventHandler(this.CodeEditUserControlSetup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnDefault;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox zoomComboBox;
        private System.Windows.Forms.Button btnSetColor;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbAsk;
        private System.Windows.Forms.RadioButton rbBasicEditor;
        private System.Windows.Forms.RadioButton rbAdvancedEditor;
    }
}