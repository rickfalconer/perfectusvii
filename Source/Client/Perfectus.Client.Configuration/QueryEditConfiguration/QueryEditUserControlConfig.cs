using System;
using System.Configuration;
using System.Drawing;


//namespace Perfectus.Client.Studio.QueryEditControl
namespace Perfectus.Client.Configuration
{
    public class CTextAttr
    {
        //internal Font font;
        public Color color = Color.Black;
        public
        CTextAttr(Color param_color) { color = param_color; }
        public
        CTextAttr()
            : this(Color.Black) { }
    }
    /// <summary>
    /// <section name="ModelLibraries" type="Model.DB.Container.ModelLibrariesSection, ModelDBContainer" />
    /// </summary>
    public class CodeEditControlSection : ConfigurationSection
    {
        public CodeEditControlSection()
        {
        }

        public enum enumEditor { OLD, NEW, ASK }

        [ConfigurationProperty("zoom", DefaultValue = 100)]
        [IntegerValidator(MaxValue=200, MinValue=50)]
        public Int32 Zoom
        {
            get { return (Int32) this["zoom"]; }
            set { this["zoom"] = value; }
        }

        [ConfigurationProperty("editor", DefaultValue = "ASK")]
        public enumEditor Editor
        {
            get { return (enumEditor)this["editor"]; }
            set { this["editor"] = value; }
        }

        [ConfigurationProperty("codeformsize"/*, DefaultValue=new Size(480, 200)*/)]
        public Size CodeFormSize
        {
            get { return (Size)this["codeformsize"]; }
            set { this["codeformsize"] = value; }
        }
        [ConfigurationProperty("codeFormLocation")]
        public Point CodeFormLocation
        {
            get { return (Point)this["codeFormLocation"]; }
            set { this["codeFormLocation"] = value; }
        }
        [ConfigurationProperty("selectionformsize" /*, DefaultValue=(220, 270)*/)]
        public Size SelectionFormSize
        {
            get { return (Size)this["selectionformsize"]; }
            set { this["selectionformsize"] = value; }
        }
        [ConfigurationProperty("selectionFormLocation" )]
        public Point SelectionFormLocation
        {
            get { return (Point)this["selectionFormLocation"]; }
            set { this["selectionFormLocation"] = value; }
        }

        [ConfigurationProperty("pageListFormsize" /*, DefaultValue=(220, 270)*/)]
        public Size PageListFormsize
        {
            get { return (Size)this["pageListFormsize"]; }
            set { this["pageListFormsize"] = value; }
        }
        [ConfigurationProperty("pageListFormLocation")]
        public Point PageListFormLocation
        {
            get { return (Point)this["pageListFormLocation"]; }
            set { this["pageListFormLocation"] = value; }
        }


        [ConfigurationProperty("colors", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ColorCollection),
           AddItemName = "addColor",
           ClearItemsName = "clearColor",
           RemoveItemName = "removeColor")]
        public ColorCollection libraryList
        {
            get
            {
                ColorCollection colorCollection =
                (ColorCollection)base["colors"];
                return colorCollection;
            }
        }
    }

    public class FormLayout : ConfigurationElement
    {
        FormLayout(Size param_size) {
            this.Size = param_size;
        }

        [ConfigurationProperty("height", IsRequired = true)]
        public Size Size
        { get { return (Size)this["size"]; }
            set { this["size"] = value; }
        }
    }

    /// <summary>
    /// <libraries/>
    /// </summary>
    public class ColorCollection : ConfigurationElementCollection
    {
        public ColorCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ColorElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((ColorElement)element).Name;
        }

        public ColorElement this[int index]
        {
            get
            {
                return (ColorElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public ColorElement this[string Name]
        {
            get { return (ColorElement)BaseGet(Name); }
        }

        public int IndexOf(ColorElement library)
        {
            return BaseIndexOf(library);
        }

        public void Add(ColorElement library)
        {
            BaseAdd(library);
        }
        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(ColorElement library)
        {
            if (BaseIndexOf(library) >= 0)
                BaseRemove(library.Name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }

    public class ColorElement : ConfigurationElement
    {
        public ColorElement(String param_name, Color param_color, Font param_font)
        {
            this.Name = param_name;
            this.Font = param_font;
            this.Color = param_color;
        }

        public ColorElement(String param_name, Color param_color)
            : this ( param_name, param_color, null )
        {
        }

        public ColorElement()
        {
        }


        [ConfigurationProperty("name",IsRequired = true, IsKey = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("color", IsRequired = true)]
        public Color Color
        {
            get
            {
                return (Color)this["color"];
            }
            set
            {
                this["color"] = value;
            }
        }


        [ConfigurationProperty("font", IsRequired = false)]
        public Font Font
        {
            get
            {
                return (Font)this["font"];
            }
            set
            {
                this["font"] = value;
            }
        }
    }
    
}