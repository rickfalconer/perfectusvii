using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.Studio.ExpressionLibrary;

namespace Perfectus.Client.Configuration
{
    public partial class QESetupForm : Form
    {
        const String BackgroundName = "Background";
        const String ForegroundName = "Foreground";

        ConfigHandler handler = null;

        CodeEditControlSection _config = null;
        internal
        CodeEditControlSection Config { get { return _config; } }

        Dictionary<String, CTextAttr> defaultValues = new Dictionary<String, CTextAttr>();


        public QESetupForm(String param_config_name)
            : this()
        {
            _config = handler.GetConfiguration(param_config_name) as CodeEditControlSection;
            if (_config == null)
            {
                _config = new CodeEditControlSection();
                LoadConfigValues();
                handler.SetConfiguration(param_config_name, _config);
            }
        }

        private QESetupForm()
        {
            InitializeComponent();
            handler = new ConfigHandler();
        }

        private void SetupDefaultValues()
        {
            defaultValues.Clear();

            // Fore- and background
            defaultValues.Add(BackgroundName, new CTextAttr(Color.FromArgb(255, 255, 245)));

            // Other constants
            defaultValues.Add("Predefined constant", new CTextAttr(Color.Purple));
            defaultValues.Add("Invalid", new CTextAttr(Color.Red));

            // Atom types
            foreach (AtomType value in Enum.GetValues(typeof(AtomType)))
            {
                if (value == AtomType.ATOM
                     || value == AtomType.QUERY
                     || value == AtomType.CLAUSE
                     || value == AtomType.IFCLAUSE
                     || value == AtomType.BLOCK
                     || value == AtomType.VALUE
                     || value == AtomType.CONDITION
                     || value == AtomType.RUNBASE
                     || value == AtomType.QUERY
                     || value == AtomType.EXPRESSIONBASE
                    )
                    continue;
                switch (value)
                {
                    case AtomType.RUN:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Red));
                        break;

                    case AtomType.CONCATENATION:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Gray));
                        break;
                    case AtomType.STATEMENT:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Gray));
                        break;
                    case AtomType.ELSEEXPRESSION:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Gray));
                        break;
                    case AtomType.OPERATOR:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.FromArgb(0, 0, 64)));
                        break;
                    case AtomType.LVALUE:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.FromArgb(0, 128, 64)));
                        break;
                    case AtomType.RVALUE:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Purple));
                        break;
                    case AtomType.WORDTEXT:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.FromArgb(128, 128, 255)));
                        break;
                    case AtomType.PLACEHOLDER:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.Red));
                        break;
                    case AtomType.INTEGRATION:
                        defaultValues.Add(Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.DarkBlue));
                        break;
                    default:
                        defaultValues.Add("XXXX" + Perfectus.Client.Studio.ExpressionLibrary.Info.GetLocalizedName(value),
                            new CTextAttr(Color.White));
                        break;
                }
            }
        }

        private void CodeEditUserControlSetup_Load(object sender, EventArgs e)
        {
            if (_config == null)
            {
                MessageBox.Show("Failed to load configuration section");
                Close();
                return;
            }
            this.AcceptButton = btnOk;
            this.CancelButton = btnCancel;

            this.zoomComboBox.Text = String.Format("{0}%", _config.Zoom);

            if (_config.Editor == CodeEditControlSection.enumEditor.NEW)
                rbAdvancedEditor.Checked = true;
            else
                if (_config.Editor == CodeEditControlSection.enumEditor.OLD)
                    rbBasicEditor.Checked = true;
                else
                    rbAsk.Checked = true;

            SetupDefaultValues();

            listView1.Items.Clear();

            Color backgroundColor = defaultValues[BackgroundName].color;

            foreach (String name in defaultValues.Keys)
            {
                ColorElement cElement = Config.libraryList[name];
                if (null == cElement)
                {
                    Config.libraryList.Add(new ColorElement(name, defaultValues[name].color));
                    cElement = Config.libraryList[name];
                }


                ListViewItem item = new ListViewItem(name);
                if (name == BackgroundName)
                {
                    item.ForeColor = Color.Black;
                    backgroundColor = cElement.Color;
                }
                else
                    item.ForeColor = cElement.Color;

                item.BackColor = backgroundColor;
                this.listView1.Items.Add(item);
            }
            listView1.HeaderStyle = ColumnHeaderStyle.None;
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        #region Zoom button event
        private void zoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            String value = this.zoomComboBox.Text;
            int i = value.LastIndexOf("%");
            try
            {
                _config.Zoom = Int32.Parse(value.Substring(0, i));
            }
            catch (Exception) { }
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            handler.SaveConfiguration(_config);
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnSetColor_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count != 1)
                return;

            ListViewItem item = listView1.SelectedItems[0];
            ColorElement cElement = Config.libraryList[item.Text];

            ColorDialog colForm = new ColorDialog();
            colForm.Color = cElement.Color;

            if (colForm.ShowDialog() == DialogResult.OK)
            {
                Config.libraryList.Remove(item.Text);
                Config.libraryList.Add(new ColorElement(item.Text, colForm.Color));

                if (item.Text != BackgroundName)
                    item.ForeColor = colForm.Color;
                else
                    foreach (ListViewItem item2 in listView1.Items)
                        item2.BackColor = colForm.Color;
            }
        }

        public void LoadConfigValues()
        {
            SetupDefaultValues();
            foreach (String name in defaultValues.Keys)
            {
                ColorElement cElement = Config.libraryList[name];

                if (cElement == null)
                    Config.libraryList.Add(new ColorElement(name, defaultValues[name].color));
            }
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            _config.Editor = CodeEditControlSection.enumEditor.NEW;
            
            this.zoomComboBox.Text = String.Format("{0}%", 100);
            
            _config.CodeFormLocation = new Point(-1, -1);
            _config.SelectionFormLocation = new Point(-1, -1);
            _config.CodeFormSize = new Size (0,0);
            _config.SelectionFormSize= new Size(0,0);

            SetupDefaultValues();
            foreach (String name in defaultValues.Keys)
            {
                ColorElement cElement = Config.libraryList[name];

                if (cElement != null)
                    Config.libraryList.Remove(name);
                Config.libraryList.Add(new ColorElement(name, defaultValues[name].color));
            }

            CodeEditUserControlSetup_Load(null, EventArgs.Empty);
        }

        private void cbEditor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( rbAdvancedEditor.Checked )
                    _config.Editor = CodeEditControlSection.enumEditor.NEW;
            else 
            if ( this.rbBasicEditor.Checked )
                    _config.Editor = CodeEditControlSection.enumEditor.OLD;
            else 
                    _config.Editor = CodeEditControlSection.enumEditor.ASK;
        }
    }
}