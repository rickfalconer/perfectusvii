using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Perfectus.Client.Configuration.QueryEditConfiguration
{
    public partial class QueryEditorAskVersion : Form
    {
        public Boolean RememberSelection
        {
            get { return this.checkBox1.Checked; }
        }
	
        public QueryEditorAskVersion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
        }
    }
}