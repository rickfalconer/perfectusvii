using System;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using System.Configuration;

namespace Perfectus.Client.SDK
{

    public interface IConfigurationUserControl
    {
        Boolean CanDefault { get; }
        void Rollback();
        void Commit();
        void Default();
    }

	/// <summary>
	/// Summary description for IPublisher.
	/// </summary>
	public interface IConfiguration : IStudioPlugin
	{
        // Get the names of sections that can be used with 
        // GetConfig(...), SaveConfig (...), ...
        String[] getConfigurations();

        IConfigurationUserControl getConfigCtrl(String param_config_name);
        ConfigurationSection GetConfig(String param_configuration_section_name);
        void SaveConfig(ConfigurationSection param_configuration_section_name);

        // Get the (nice-) names of forms that can be used to invoke a configuration screen

        // Get/Set distinct values
        Object GetSetting(String param_configuration_section_name, String param_setting);
        void SetSetting(String param_configuration_section_name, String param_setting, Object param_value);
        Int32 GetSettingInteger(String param_configuration_section_name, String param_setting);
        void SetSettingInteger(String param_configuration_section_name, String param_setting, Int32 param_value);
        Double GetSettingDouble(String param_configuration_section_name, String param_setting);
        void SetSettingDouble(String param_configuration_section_name, String param_setting, Double param_value);
        String GetSettingString(String param_configuration_section_name, String param_setting);
        void SetSettingString(String param_configuration_section_name, String param_setting, String param_value);
        Boolean GetSettingBoolean(String param_configuration_section_name, String param_setting);
        void SetSettingBoolean(String param_configuration_section_name, String param_setting, Boolean param_value);
    }
}
