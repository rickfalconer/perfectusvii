using System;
using System.Windows.Forms;

namespace Perfectus.Client.SDK
{
	/// <summary>
	/// Summary description for IPublisher.
	/// </summary>
	public interface IPublisher : IStudioPlugin
	{
		Guid PackageId
		{
			get;
			set;
		}

		int VersionNumber
		{
			get;
			set;
		}

		int RevisionNumber
		{
			get;
			set;
		}

		string BaseUiUrl
		{
			get;
			set;
		}

		string PackageName
		{
			get;
			set;
		}

        string SuggestedFileName
        {
            get; 
            set;
        }

		string MyDocumentsAddress
		{
			get;
			set;
		}

		void ResetUI();

	}
}
