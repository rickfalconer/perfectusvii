using System.Drawing;
using System.IO;
using Perfectus.Client.SDK;

namespace Perfectus.Client.SDK
{
	/// <summary>
	/// Summary description for IOpenSaveHandler.
	/// </summary>
	public interface IOpenSaveHandler : IStudioPlugin
	{
		string OpenFileMenuName
		{
			get;
		}

//		string SaveFileMenuName
//		{
//			get;
//		}

	    void PackageUnloaded();

		string SaveAsFileMenuName
		{
			get;
		}

		string ImportTemplateMenuName
		{
			get;
		}

		string ExportTemplateMenuName
		{
			get;
		}

		Image FileMenuSaveAsIcon
		{
			get;
		}

        Image FileMenuOpenIcon
		{
			get;
		}

		bool SupportsOpen
		{
			get;
		}

//		bool SupportsSave
//		{
//			get;
//		}

		bool SupportsSaveAs
		{
			get;
		}

		bool SupportsImportTemplate
		{
			get;
		}

		bool SupportsExportTemplate
		{
			get;
		}
		
		string Name
		{
			get;
		}

		string IpManagerTitleText
		{
			get;			
		}


		bool SavePackage(Stream streamToSave);
		bool SavePackageAs(Stream streamToSave, string packageName, string suggestedFileName);
		void ImportTemplateCleanUp(string filename);
		Stream OpenPackage();
		bool ImportTemplate(out string fileToImport);
		bool ExportTemplate(string fileToExportPath, string templateName, string suggestedFileName);
	}
}
