namespace Perfectus.Client.SDK
{
	/// <summary>
	/// Summary description for IClauseLibrary.
	/// </summary>
	public interface IClauseLibrary : IStudioPlugin
	{
		// The text displayed on the plug-in's button in the Outcome editor.  Should be localised to the same languages as IPManager.
		string ButtonText
		{
			get;
		}

		// The ID of the server's Fetcher plug-in that will parse the document reference and retrieve the specified Clause.
		string ServerSideFetcherId
		{
			get;
		}
		
		/// <summary>
		/// Gets an arbitrary string that represents a reference to the Clause in a document management system.  Processed by the Fetcher plug-in
		//  on the server to retrieve the specified Clause.
		/// </summary>
		/// <param name="displayName">The friendly name of the clause. Displayed in IPM and Reportin.g</param>
		/// <param name="libraryName">The name of the Library the clause has been picked from. Displayed in Reporting.</param>
		/// <param name="libraryUniqueIdentifier">Allows an additional ID to be specified than just the ID the fetcher will use. Useful for Reporting, as sometimes the ID 
		/// used by the fetcher (the LibraryKey) returns big strings of XML, etc. Which would be overkill and a possible security concern to pass around as an identifier.</param>
		/// <returns>The docunentId that is used by the fetcher plug-in.</returns>
		bool PickClause(out string clauseId, out string displayName, out string libraryName, out string libraryUniqueIdentifier);
	}
}
