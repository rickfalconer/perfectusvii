using System.Windows.Forms;

namespace Perfectus.Client.SDK
{
	/// <summary>
	/// Summary description for IStudioPlugin.
	/// </summary>
	public interface IStudioPlugin
	{
		// The friendly name for this plug-in
		string PluginName
		{
			get;
		}

		// The comapny that wrote the plug-in.
		string Author
		{
			get;
		}

		// Any copyright information about the plug-in.
		string Message
		{
			get;
		}

		// Called when the plug-in is first loaded by the IPManager application.
		void Init(Form openingForm);

		// Called when the IPManager application is closing.
		void Shutdown();
	}
}
