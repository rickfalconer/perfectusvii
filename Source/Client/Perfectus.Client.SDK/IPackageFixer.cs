using System;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.SDK
{
	/// <summary>
	/// Summary description for IPublisher.
	/// </summary>
	public interface IPackageFixer : IStudioPlugin
	{
		Guid PluginId
		{
			get;
		}

		int VersionNumber
		{
			get;
		}

		int RevisionNumber
		{
			get;
		}

        bool NeedFixing(Package param_package, bool param_critical_only, ref String protocol);
        bool Fix(ref Package param_package, bool param_critical_only, ref String protocol);
    }
}
