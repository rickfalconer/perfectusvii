using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using Perfectus.Client.Studio.ExpressionLibrary;

using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.ExpressionLibrary
{
    /// <summary>
    /// Object and Object with children validation interface
    /// </summary>
    public interface IValidation
    {
        /// <summary>
        /// Object validation with children
        /// </summary>
        /// <param name="param_message">Error message on return</param>
        /// <returns></returns>
        Boolean IsValid(ref String param_message);
        /// <summary>
        /// Object validation without children
        /// </summary>
        /// <param name="param_message">Error message on return</param>
        /// <returns></returns>
        Boolean IsObjectValid(ref string param_message);
    }
    /// <summary>
    /// Common methods and properties
    /// </summary>
    public interface ICommon : IValidation
    {
        Boolean HasChildren();
        ICommon[] Children();
        void Remove();
        Guid Id { get; }
        Guid ParentId { get; }
    }
    /// <summary>
    /// Implemented by elements like Word text, which can be sourrounded
    /// by a new if .. then statement
    /// </summary>
    public interface ISupportOuterIF
    {
        void InsertOuterIF();
        void SubClassChild(CExpressionBase p);
    }

    #region CAtom
    /// <summary>
    /// CAtom
    /// Abstract base class for all expression elements
    /// </summary>
    public abstract class CAtom : ICommon, IDisposable
    {
        /// <summary>
        /// Unique element Id
        /// </summary>
        public Guid Id { get { return _Id; } }
        private Guid _Id;

        /// <summary>
        /// Unique element parent Id
        /// </summary>
        public Guid ParentId { get { return _parentId; } set { _parentId = value; } }
        Guid _parentId;

        /// <summary>
        /// Type identifier
        /// </summary>
        public virtual AtomType type { get { return AtomType.ATOM; } }

        /// <summary>
        /// Constructor
        /// </summary>
        public
        CAtom()
        {
            _Id = Guid.NewGuid();
            _parentId = Guid.Empty;
            ExpressionHelper.Register(this);
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">The logical parent</param>
        public
        CAtom(CAtom param_parent)
            : this(param_parent.Id)
        { }
        private
        CAtom(Guid param_parentId)
            : this()
        {
            _parentId = param_parentId;
        }

        #region Object destruction
        // One of our children is suicidal and wants us (the parent)
        // to remove it.
        internal virtual void RemoveChild(CAtom param_removal)
        {
            // sanity test
            if (!HasChildren())
                throw new ExpressionLibraryException(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorUnknownChild"),
                    "CAtom::RemoveChild",
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
        }
        /// <summary>
        /// Remove this object
        /// </summary>
        public virtual void Remove()
        {
            /*
            if (HasChildren())
                foreach (ICommon child in Children())
                    child.Remove();
             */ 
            Dispose();
        }

        #region IDisposable Members
        /// <summary>
        /// Dispose()
        /// Removes the object from the global registar
        /// </summary>
        public void Dispose()
        {
            ExpressionHelper.UnRegister(this);
        }
        #endregion

        #endregion

        #region Clone
        internal new CClause Clone(CAtom param_parent)
        {
            throw new ExpressionLibraryException(
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorCannotCloneAbstractClass"),
                "CAtom::RemoveChild",
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
        }
        #endregion

        #region Display
        /// <summary>
        /// Draw yourself into the code window using the pretty-printer callback
        /// </summary>
        /// <param name="_call_back"></param>
        public virtual void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            // nothing to write
        }
        /*
        /// <summary>
        /// Overriden
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Empty;
        }
         */
        #endregion

        #region IValidation Members
        /// <summary>
        /// Has child elements
        /// </summary>
        /// <returns></returns>
        public virtual Boolean HasChildren()
        {
            return false;
        }

        /// <summary>
        /// Array of child elemenst
        /// </summary>
        /// <returns></returns>
        public virtual ICommon[] Children()
        {
            return null;
        }

        /// <summary>
        /// Test if element is valid.
        /// That does not include testing of any child.
        /// </summary>
        /// <param name="param_message"></param>
        /// <returns></returns>
        public virtual bool IsObjectValid(ref string param_message)
        {
            // The guid's must be valid
            Boolean success = Id != null && Id != Guid.Empty && ParentId != null;
            if (!success)
                param_message += System.Environment.NewLine + "Guid's invalid";
            return success;
        }
        /// <summary>
        /// Test if element including the children are valid.
        /// </summary>
        /// <param name="param_message"></param>
        /// <returns></returns>
        /// /*
        public virtual bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (ICommon child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && this.IsObjectValid(ref param_message);
            return success;
        }
        #endregion

    }
    #endregion

    #region CQuery
    /// <summary>
    /// A CQuery is the root element of any query
    /// </summary>
    public class CQuery : CAtom, IDisposable
    {
        internal List<CBlock> _blocks;
        /// <summary>
        /// Constructor
        /// </summary>
        public
            CQuery()
        {
            _blocks = new List<CBlock>();
        }

        /// <summary>
        /// Create an empty query that is suiable for the code window
        /// (must have initial block and statement)
        /// </summary>
        /// <returns></returns>
        static public CQuery AsNewQuery()
        {
            CQuery query = new CQuery();
            CBlock firstBlock = query.AddBlock();
            firstBlock.Statement.if_clause.Run.AddCondition();
            return query;
        }

        /// <summary>
        /// Create a new block for the query
        /// <code>
        /// Example:
        /// CQuery new_query = new CQuery();
        /// CBlock firstBlock = new_query.addBlock("First Block");
        /// </code>
        /// </summary>
        /// <param name="param_name">Optional name for the block</param>
        /// <returns></returns>
        public CBlock AddBlock(String param_name)
        {
            if (_blocks.Count == 1)
                throw new ExpressionLibraryException(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorNotImplemented"),
                    "CQuery::AddBlock",
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
            CBlock block = new CBlock(param_name, this);
            _blocks.Add(block);
            return block;
        }
        /// <summary>
        /// Create a new block for the query
        /// </summary>
        /// <returns></returns>
        public CBlock AddBlock()
        {
            return AddBlock(String.Empty);
        }

        #region Clone
        public new CQuery Clone()
        {
            CQuery new_query = new CQuery();
            foreach (CBlock block in _blocks)
                new_query._blocks.Add(block.Clone(new_query));
            return new_query;
        }
        #endregion

        #region Atom override
        public override AtomType type { get { return AtomType.QUERY; } }
        #endregion

        #region IValidation Members
        /// <summary>
        /// Has child elements
        /// </summary>
        /// <returns></returns>
        public override Boolean HasChildren()
        {
            return _blocks.Count > 0;
        }

        /// <summary>
        /// Array of child elemenst
        /// </summary>
        /// <returns></returns>
        public override ICommon[] Children()
        {
            ICommon[] children = (ICommon[])_blocks.ToArray();
            return (children);
        }
        #endregion

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            // Each block shall display itself
            foreach (CBlock block in _blocks)
                block.DisplayYourself(_call_back);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            foreach (CBlock block in _blocks)
            {
                strBld.AppendLine(block.ToString());
            }
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CBlock
    /// <summary>
    /// A Block is owned by the CQuery instance.
    /// It contains exactly one statement
    /// </summary>
    public class CBlock : CAtom
    {
        /// <summary>
        /// Optional name fior the block
        /// </summary>
        public String Name { get { return _name; } }
        private String _name;

        /// <summary>
        /// Single statement
        /// </summary>
        internal CStatement Statement { get { return _statement; } }
        private CStatement _statement;

        /// <summary>
        /// Constructor
        /// Note that a client should not create a block directly,
        /// bute use CQuesry.AddBlock() instead.
        /// </summary>
        /// <param name="param_name"></param>
        /// <param name="param_parent"></param>
        internal
        CBlock(String param_name, CAtom param_parent)
            : base(param_parent)
        {
            _name = param_name;
            _statement = new CStatement(this);
        }

        /// <summary>
        /// Embrace this statement with a new statement
        /// <example>
        /// Before:
        /// if ( Q1 = 2) then
        /// After:    
        /// if ( 'new question' = 'new question or constant' ) then
        ///     if ( Q1 = 2) then
        /// </example>
        /// </summary>
        internal protected void SubClassIf()
        {
            CStatement stmt = new CStatement(this);
            stmt.if_clause.Run.AddCondition();
            stmt.IfStatement = _statement;
            _statement = stmt;
        }

        #region Clone
        internal new CBlock Clone(CAtom param_parent)
        {
            CBlock newBlock = new CBlock(_name, param_parent);
            newBlock._statement = Statement.Clone(newBlock) as CStatement;
            return newBlock;
        }
        #endregion

        public override AtomType type { get { return AtomType.BLOCK; } }

        #region IValidation Members
        /// <summary>
        /// Has child elements
        /// </summary>
        /// <returns></returns>
        public override Boolean HasChildren()
        {
            return true;
        }

        /// <summary>
        /// Array of child elemenst
        /// </summary>
        /// <returns></returns>
        public override ICommon[] Children()
        {
            return (new ICommon[] { _statement });
        }
        #endregion

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back("CBlock_BEGIN", type, Id, ParentId, true);
            _statement.DisplayYourself(_call_back);
            _call_back("CBlock_END", type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("Begin", Name));
            strBld.AppendLine(_statement.ToString());
            strBld.AppendLine(String.Format("End", Name));
            return strBld.ToString();
        }
        */
        #endregion
    }
    #endregion

    #region CExpression
    /// <summary>
    /// base class for CStatement and CWordText
    /// </summary>
    public abstract class CExpressionBase : CAtom//, ISupportOuterIF
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent"></param>
        public
        CExpressionBase(CAtom param_parent)
            : base(param_parent) { }
        public override AtomType type { get { return AtomType.EXPRESSIONBASE; } }

        public virtual CStatement AddStatement(CExpressionBase param_expression)
        {
            return null;
        }
        public virtual CWordText AddAction(CExpressionBase param_expression)
        {
            return null;
        }
        public virtual CIntegration AddIntegration(CExpressionBase param_expression)
        {
            return null;
        }
        public virtual CNavigationPage AddNavigationAction(CExpressionBase param_if_or_else)
        {
            return null;
        }
        public virtual CHtmlStringBinding AddHtmlStringBinding(CExpressionBase param_if_or_else)
        {
            return null;
        }
        public void InsertOuterIF()
        {
            ISupportOuterIF parent = (ISupportOuterIF)ExpressionHelper.Find(ParentId);
            parent.SubClassChild(this);
        }

        #region Clone
        internal virtual CExpressionBase Clone(CAtom param_parent)
        {
            throw new ExpressionLibraryException(
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorCannotCloneAbstractClass"),
                "CExpressionBase::RemoveChild",
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
        }
        #endregion
    }
    #endregion

    #region CPlaceholder
    /// <summary>
    /// Placeholder is sued to indicate an empty statement
    /// This class likely to be replaced with either another statement,
    /// or with an action
    /// </summary>
    public class CPlaceholder : CExpressionBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent"></param>
        public
        CPlaceholder(CAtom param_parent)
            : base(param_parent) { }

        public override void Remove()
        {
            // clean up the action
            CAtom parent = ExpressionHelper.Find(ParentId);
            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CPlaceholder newPlaceholder = new CPlaceholder(param_parent);
            return newPlaceholder;
        }
        #endregion

        public override AtomType type { get { return AtomType.PLACEHOLDER; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back("CPlaceholder_NewTextOrStatement", type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}�New Text or Statement�", CInsertion.PaddingSpaces(1)));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CWordText
    /// <summary>
    /// Linking object between the query and the actual outcome etc.
    /// </summary>
    public class CWordText : CExpressionBase
    {
        private Object _payload;
        public Object Payload
        {
            get { return _payload; }
            set { _payload = value; }
        }

        /// <summary>
        /// Name of the action
        /// </summary>
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private String _name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">parent</param>
        public
        CWordText(CAtom param_parent)
            : base(param_parent) { }

        #region IValidation Members

        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="param_message"></param>
        /// <returns>Error message if test fails</returns>
        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
            {
                success = this.Payload != null || (Name != null && Name.Length > 0);
                if (!success)
                    param_message +=
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CWordTextNotValid1");
            } return success;
        }

        #endregion

        /// <summary>
        /// Remove
        /// Tell the parent to remove this object
        /// </summary>
        public override void Remove()
        {
            // clean up the action
            CAtom parent = ExpressionHelper.Find(ParentId);
            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CWordText newAction = new CWordText(param_parent);
            newAction._payload = _payload;
            newAction._name = _name;
            return newAction;
        }
        #endregion

        public override AtomType type { get { return AtomType.WORDTEXT; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back(String.Format("{0} {1}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CWordText_TEXT"), Name), type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}Outcome {1}", CInsertion.PaddingSpaces(1), Name));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CNavigationPage
    /// <summary>
    /// Linking object between the query and the actual outcome etc.
    /// </summary>
    public class CNavigationPage : CExpressionBase
    {
        private Object _payload;
        public Object Payload
        {
            get { return _payload; }
            set { _payload = value; }
        }

        /// <summary>
        /// Name of the action
        /// </summary>
        public String Name
        {
            get { return Payload == null ? String.Empty :  _name; }
            set { _name = value; }
        }
        private String _name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">parent</param>
        public
        CNavigationPage(CAtom param_parent)
            : base(param_parent) { }

        /// <summary>
        /// Remove
        /// Tell the parent to remove this object
        /// </summary>
        public override void Remove()
        {
            // clean up the action
            CAtom parent = ExpressionHelper.Find(ParentId);
            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CNavigationPage newAction = new CNavigationPage(param_parent);
            newAction._payload = _payload;
            newAction._name = _name;
            return newAction;
        }
        #endregion

        public override AtomType type { get { return AtomType.NAVIGATIONPAGE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            String msg = 
                this.Payload != null ?
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CNavigationPage_TEXT") :
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CNavigationPage_EMPTY");
            _call_back(String.Format("{0} {1}", msg, Name), type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}Outcome {1}", CInsertion.PaddingSpaces(1), Name));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CHtmlStringBinding
    /// <summary>
    /// Linking object between the query and the HTML text etc.
    /// </summary>
    public class CHtmlStringBinding : CExpressionBase
    {
        private String _payload;
        public String Payload
        {
            get { return _payload; }
            set { _payload = value; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">parent</param>
        public
        CHtmlStringBinding(CAtom param_parent)
            : base(param_parent) { }

        /// <summary>
        /// Remove
        /// Tell the parent to remove this object
        /// </summary>
        public override void Remove()
        {
            // clean up the action
            CAtom parent = ExpressionHelper.Find(ParentId);
            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CHtmlStringBinding newAction = new CHtmlStringBinding(param_parent);
            newAction._payload = _payload;
            return newAction;
        }
        #endregion

        public override AtomType type { get { return AtomType.HTMLSTRINGBINDING; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            String msg;
            if (Payload != null && Payload.Length > 0)
            {
                if (Payload.StartsWith("<text"))
                    msg = "(Bound question)";
                else
                    msg = Payload.Length > 30 ?
                        Payload.Substring(0, 27) + "..." :
                        Payload;
            }
            else
                msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CHtmlStringBinding_TEXT");
            _call_back(String.Format("{0}", msg), type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}Outcome {1}", CInsertion.PaddingSpaces(1), Name));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion
    
    #region CIntegration
    /// <summary>
    /// Linking object between the query and the actual outcome etc.
    /// </summary>
    public class CIntegration : CExpressionBase
    {
        private Object _clauseLibrary;  // the plugin itself
        public Object ClauseLibrary
        {
            get { return _clauseLibrary; }
            set { _clauseLibrary = value; }
        }
        private String _clauseRef;
        public String ClauseRef
        {
            get { return _clauseRef; }
            set { _clauseRef = value; }
        }
        private String _fetcherId;
        public String FetcherId
        {
            get { return _fetcherId; }
            set { _fetcherId = value; }
        }
        private String _displayName;
        public String DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; }
        }
        private String _libraryName;
        public String LibraryName
        {
            get { return _libraryName; }
            set { _libraryName = value; }
        }
        private String _uniqueLibraryKey;
        public String UniqueLibraryKey
        {
            get { return _uniqueLibraryKey; }
            set { _uniqueLibraryKey = value; }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">parent</param>
        public
        CIntegration(CAtom param_parent)
            : base(param_parent) { }

        #region IValidation Members

        /// <summary>
        /// Validation
        /// </summary>
        /// <param name="param_message"></param>
        /// <returns>Error message if test fails</returns>
        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
            {
                success =
                    this._clauseRef != null && _clauseRef.Length > 0 &&
                    this._fetcherId != null && _fetcherId.Length > 0 &&
                    this._displayName != null && _displayName.Length > 0 &&
                    this._libraryName != null && _libraryName.Length > 0 &&
                    this._uniqueLibraryKey != null && _uniqueLibraryKey.Length > 0;
                if (!success)
                    param_message += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CIntegrationNotValid1");

            } return success;
        }

        #endregion

        /// <summary>
        /// Remove
        /// Tell the parent to remove this object
        /// </summary>
        public override void Remove()
        {
            // clean up the action
            CAtom parent = ExpressionHelper.Find(ParentId);
            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CIntegration newIntegration = new CIntegration(param_parent);
            newIntegration._clauseRef = _clauseRef;
            newIntegration._fetcherId = _fetcherId;
            newIntegration._displayName = _displayName;
            newIntegration._libraryName = _libraryName;
            newIntegration._uniqueLibraryKey = _uniqueLibraryKey;
            return newIntegration;
        }
        #endregion

        public override AtomType type { get { return AtomType.INTEGRATION; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back(String.Format("{0} {1}",
            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CIntegration_TEXT"),
            DisplayName),
            type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}Integration {1}", CInsertion.PaddingSpaces(1), DisplayName));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CStatement
    /// <summary>
    /// The Statement has one clause and two optional statements.
    /// The clause is a collection of conditions (and further clauses) that can be 
    /// evaluated to true or false. 
    /// The two optional statements represent the If/Else branch that shall be used.
    /// Note that a statement can be an action (like an outcome text)
    /// Example:
    /// The clause is the part (( Q1 =  .... Q2 ))
    /// The if-statement is set and actually an outcome
    /// The else-statement is set and another statement
    /// <example>
    ///   If (( Q1 = �Null�  or Q2 = "dsdsll" ) and( Q3 = Q1  or Q3 = Q2 )) Then
    ///            <<Outcome.txt>>
    ///    Else
    ///             If ( B1 = �Yes�  or N1 > 200 ) Then
    /// </example>
    /// </summary>
    public class CStatement : CExpressionBase, ISupportOuterIF
    {
        /// <summary>
        /// Clause holding the collection of clauses and condition
        /// </summary>
        public CClause if_clause
        {
            get { if (_if_clause == null) _if_clause = new CClause(this); return _if_clause; }
        }
        private CClause _if_clause;

        /// <summary>
        /// The if-statement shall not be empty
        /// It is anything that derives from CExpressionBase (statements, actions, placeholders)
        /// </summary>
        public CExpressionBase IfStatement
        {
            get { return _if_statement; }
            set
            {
                _if_statement = value;
                if (null != _if_statement)
                    _if_statement.ParentId = Id; // Set ourself as the parent
            }
        }
        private CExpressionBase _if_statement;


        /// <summary>
        /// Same as an if-statement, but can be 'null'
        /// </summary>
        public CExpressionBase ElseStatement
        {
            get { return _else_statement; }
            set
            {
                if (value != null)
                    _else_statement = new CElseExpression(this, value);
                else
                    _else_statement = null;
            }
        }
        private CExpressionBase _else_statement;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_parent">parent</param>
        public CStatement(CAtom param_parent)
            : base(param_parent)
        {
            _if_clause = new CClause(this);
            AddIfPlaceholderAction();
        }
        /// <summary>
        /// Add or replace the existing IF TRUE action with a placeholder
        /// </summary>
        /// <returns>Placeholder</returns>
        public CExpressionBase AddIfPlaceholderAction()
        {
            if (IfStatement != null) IfStatement.Remove();
            CExpressionBase placeholder = new CPlaceholder(this);
            IfStatement = placeholder;
            return placeholder;
        }
        /// <summary>
        /// Add or replace the existing IF FALSE action with a placeholder
        /// </summary>
        /// <returns>Placeholder</returns>
        public CExpressionBase AddElsePlaceholderAction()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CExpressionBase placeholder = new CPlaceholder(this);
            ElseStatement = placeholder;
            return placeholder;
        }
        public CWordText AddIfAction()
        {
            if (IfStatement != null) IfStatement.Remove();
            CWordText action = new CWordText(this);
            IfStatement = action;
            return action;
        }
        public CWordText AddElseAction()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CWordText action = new CWordText(this);
            ElseStatement = action;
            return action;
        }
        public CIntegration AddIfIntegration()
        {
            if (IfStatement != null) IfStatement.Remove();
            CIntegration integration = new CIntegration(this);
            IfStatement = integration;
            return integration;
        }
        public CIntegration AddElseIntegration()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CIntegration integration = new CIntegration(this);
            ElseStatement = integration;
            return integration;
        }

        public CNavigationPage AddIfNavigationAction()
        {
            if (IfStatement != null) IfStatement.Remove();
            CNavigationPage navigation = new CNavigationPage(this);
            IfStatement = navigation;
            return navigation;
        }
        public CNavigationPage AddElseNavigationAction()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CNavigationPage navigation = new CNavigationPage(this);
            ElseStatement = navigation;
            return navigation;
        }
        public CHtmlStringBinding AddIfHtmlStringBindingAction()
        {
            if (IfStatement != null) IfStatement.Remove();
            CHtmlStringBinding navigation = new CHtmlStringBinding(this);
            IfStatement = navigation;
            return navigation;
        }
        public CHtmlStringBinding AddElseHtmlStringBindingAction()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CHtmlStringBinding navigation = new CHtmlStringBinding(this);
            ElseStatement = navigation;
            return navigation;
        }
        internal CStatement AddIfStatement()
        {
            if (IfStatement != null) IfStatement.Remove();
            CStatement stmt = new CStatement(this);
            IfStatement = stmt;
            return stmt;
        }
        internal CStatement AddElseStatement()
        {
            if (ElseStatement != null) ElseStatement.Remove();
            CStatement stmt = new CStatement(this);
            ElseStatement = stmt;
            return stmt;
        }
        public override CStatement AddStatement(CExpressionBase param_if_or_else)
        {
            if (IfStatement == param_if_or_else)
                return AddIfStatement();
            if (ElseStatement == param_if_or_else)
                return AddElseStatement();
            return null;
        }
        public override CWordText AddAction(CExpressionBase param_if_or_else)
        {
            if (IfStatement == param_if_or_else)
                return AddIfAction();
            if (ElseStatement == param_if_or_else)
                return AddElseAction();
            return null;
        }
        public override CIntegration AddIntegration(CExpressionBase param_if_or_else)
        {
            if (IfStatement == param_if_or_else)
                return AddIfIntegration();
            if (ElseStatement == param_if_or_else)
                return AddElseIntegration();
            return null;
        }

        public override CNavigationPage AddNavigationAction(CExpressionBase param_if_or_else)
        {
            if (IfStatement == param_if_or_else)
                return AddIfNavigationAction();
            if (ElseStatement == param_if_or_else)
                return AddElseNavigationAction();
            return null;
        }
        public override CHtmlStringBinding AddHtmlStringBinding(CExpressionBase param_if_or_else)
        {
            if (IfStatement == param_if_or_else)
                return AddIfHtmlStringBindingAction();
            if (ElseStatement == param_if_or_else)
                return AddElseHtmlStringBindingAction();
            return null;
        }

        /// <summary>
        /// Insert new statement
        /// <example>
        /// Before:
        /// if ( q1 = 1 ) then
        /// After:
        /// if ( 'new question' = 'new question or constant' ) then
        ///     if ( q1 = 1 ) then
        /// </example>
        /// Note that this object tells its parent to perfoem the operation (see SubClassChild())
        /// </summary>
        public void InsertOuterIF()
        {
            CAtom parent = ExpressionHelper.Find(ParentId);
            if (parent is CBlock)
                (parent as CBlock).SubClassIf();
            else
                (parent as ISupportOuterIF).SubClassChild(this);
        }
        /// <summary>
        /// Insert new statement
        /// A child object wants us to embrace it with a new statement ( see InsertOuterIF())
        /// </summary>
        /// <param name="param_client"></param>
        public void SubClassChild(CExpressionBase param_client)
        {
            if (param_client == IfStatement)
                IfStatement = SubClassSpecificChild(param_client);
            else if (param_client == ElseStatement)
                ElseStatement = SubClassSpecificChild(param_client);
        }
        private CExpressionBase SubClassSpecificChild(CExpressionBase param_client)
        {
            CStatement stmt = new CStatement(this);
            stmt.if_clause.Run.AddCondition();
            stmt.IfStatement = param_client;
            return stmt;
        }

        internal override void RemoveChild(CAtom param_child)
        {
            if (ElseStatement == param_child)
            {
                ElseStatement = null;
                AddElsePlaceholderAction();
            }

            if (IfStatement == param_child)
            {
                IfStatement = null;
                AddIfPlaceholderAction();
            }
        }
        internal void RemoveElse()
        {
            ElseStatement = null;
        }


        public override void Remove()
        {
            CAtom parent = ExpressionHelper.Find(ParentId);
            if (parent is CBlock)
                throw new ExpressionLibraryException(
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CStatement_CannotRemove"),
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CStatement_RemoveNotPossible"));
            if (ElseStatement != null)
                ElseStatement.Remove();
            if (IfStatement != null)
                IfStatement.Remove();
            if (_if_clause != null)
                if_clause.Remove();

            ElseStatement = null;
            IfStatement = null;
            _if_clause = null;

            parent.RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CStatement newStatement = new CStatement(param_parent);

            newStatement._if_clause = _if_clause.Clone(newStatement);
            newStatement._if_statement = _if_statement.Clone(newStatement);
            if (null != _else_statement)
                newStatement._else_statement = _else_statement.Clone(newStatement);

            return newStatement;
        }
        #endregion

        public override AtomType type { get { return AtomType.STATEMENT; } }

        #region IValidation Members
        /// <summary>
        /// Has child elements
        /// </summary>
        /// <returns></returns>
        public override Boolean HasChildren()
        {
            return true;
        }

        /// <summary>
        /// Array of child elemenst
        /// </summary>
        /// <returns></returns>
        public override ICommon[] Children()
        {
            ICommon[] children = null;

            if (_else_statement == null)
                children = new ICommon[] { _if_clause, _if_statement };
            else
                children = new ICommon[] { _if_clause, _if_statement, _else_statement };
            return (children);
        }
        #endregion

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back("CStatement_IF", type, Id, ParentId, true);
            _if_clause.DisplayYourself(_call_back);
            _call_back("CStatement_THEN", type, Id, ParentId, true);

            if (null != _if_statement)
                _if_statement.DisplayYourself(_call_back);
            if (null != _else_statement)
                _else_statement.DisplayYourself(_call_back);

            _call_back("CStatement_ENDIF", type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            CInsertion.PaddingSpaces(1);

            StringBuilder strBld = new StringBuilder();
            // Condition
            strBld.AppendLine(String.Format("{0} {1} {2} {3}", CInsertion.PaddingSpaces(), "if", _if_clause.ToString(), "then"));
            //Action or statement
            if (null != _if_statement)
                strBld.AppendLine(_if_statement.ToString());
            if (null != _else_statement)
            {
                strBld.AppendLine(String.Format("{0}else", CInsertion.PaddingSpaces()));
                strBld.AppendLine(String.Format("{0}", _else_statement.ToString()));
            }
            strBld.Append(String.Format("{0}EndIf", CInsertion.PaddingSpaces()));
            CInsertion.PaddingSpaces(-1);
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CClause
    /// <summary>
    /// Object is used by a statement to hold all conditions (represented by CRun objects)
    /// CStatement := If CCLAUSE then CExpressionBase [else CExpressionBase]
    /// CClause itself only contains only a CRun object. However CClause
    /// is convinient for identifying the outermost CRun that cannot be removed.
    /// </summary>
    public class CClause : CAtom, ICommon
    {
        private CRun _run;
        public CRun Run { get { return _run; } }

        internal CClause(CAtom param_parent)
            : base(param_parent)
        {
            _run = new CRun(this);
        }

        public override void Remove()
        {
            Run.Remove(true);
            base.Remove();
        }

        internal override void RemoveChild(CAtom param_removal)
        {
            if (param_removal == _run)
                _run = null;
            base.RemoveChild(param_removal);
        }

        internal  void ReplaceRun(CRun param_run)
        {
            RemoveChild(_run);
            param_run.SetParent(this);
            _run = param_run;
        }

        #region Clone
        internal new CClause Clone(CAtom param_parent)
        {
            CClause newClause = new CClause(param_parent);
            newClause._run = _run.Clone(newClause) as CRun;
            return newClause;
        }
        #endregion

        public override AtomType type { get { return AtomType.CLAUSE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _run.DisplayYourself(_call_back);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(_run.ToString());
            if (null != _run_or_action)
                strBld.Append(_run_or_action.ToString());
            return strBld.ToString();
        }
        */
        #endregion

        #region IValidation Members

        public override ICommon[] Children()
        {
            return (new ICommon[] { _run });
        }

        public override Boolean HasChildren()
        {
            return true;
        }

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }

        #endregion

    }
    #endregion

    #region CElseExpression
    /// <summary>
    /// The CElseExpression expression was refactored out of the FALSE branch of CStatement to have an object 
    /// that is not part of a statement, but is a child of a statement with its own children.
    /// CStatement := If CCLAUSE then CExpressionBase [else CExpressionBase]
    /// The reason is to simply have an object that can be marked independently:
    /// The THEN keyword is part of the statement (same as the IF). Selecting THEN
    /// does select the statement. 
    /// The ELSE keyword is owned by CElseExpression and clicking would not mark the statement.
    /// (Note that TRUE and FALSE cases are slightly differrent and so are their marking and command behavior
    /// e.g. you can have a statement without FALSE case, but never without the TRUE case - here we must
    /// use a placeholder)
    /// </summary>
    public class CElseExpression : CExpressionBase, ISupportOuterIF
    {
        CExpressionBase expression;
        internal
        CExpressionBase Expression { get { return expression; } }

        /// <summary>
        /// Constructor
        /// Note that there is conceptionally always a parent and an expression that is 
        /// hosted by the ELSE.
        /// </summary>
        /// <param name="param_parent"></param>
        /// <param name="param_expression"></param>
        public CElseExpression(CAtom param_parent, CExpressionBase param_expression)
            : base(param_parent)
        {
            expression = param_expression;
            expression.ParentId = Id;
        }

        public override CStatement AddStatement(CExpressionBase param_expression)
        {
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            return parent_statement.AddStatement(this);
        }
        public override CWordText AddAction(CExpressionBase param_expression)
        {
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            return parent_statement.AddAction(this);
        }
        public override CIntegration AddIntegration(CExpressionBase param_expression)
        {
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            return parent_statement.AddIntegration(this);
        }
        public override CNavigationPage AddNavigationAction(CExpressionBase param_expression)
        {
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            return parent_statement.AddNavigationAction(this);
        }
        public override CHtmlStringBinding AddHtmlStringBinding(CExpressionBase param_expression)
        {
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            return parent_statement.AddHtmlStringBinding(this);
        }


        public void SubClassChild(CExpressionBase param_client)
        {
            CStatement stmt = new CStatement(this);
            stmt.if_clause.Run.AddCondition();
            stmt.IfStatement = param_client;
            expression = stmt;
        }
        internal override void RemoveChild(CAtom param_removal)
        {
            base.RemoveChild(param_removal);
            expression = null;
            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            parent_statement.AddElsePlaceholderAction();
        }

        public override void Remove()
        {
            if (null != expression)
                expression.Remove();

            CStatement parent_statement = (CStatement)ExpressionHelper.Find(ParentId);
            parent_statement.RemoveElse();

            base.Remove();
        }

        #region Clone
        internal override CExpressionBase Clone(CAtom param_parent)
        {
            CExpressionBase newExpressionBase = this.expression.Clone(param_parent);
            CElseExpression newElseExpression = new CElseExpression(param_parent, newExpressionBase);
            return newElseExpression;
        }
        #endregion


        public override AtomType type { get { return AtomType.ELSEEXPRESSION; } }

        #region IValidation Members
        /// <summary>
        /// Has child elements
        /// </summary>
        /// <returns></returns>
        public override Boolean HasChildren()
        {
            return true;
        }

        /// <summary>
        /// Array of child elemenst
        /// </summary>
        /// <returns></returns>
        public override ICommon[] Children()
        {
            return (new ICommon[] { expression });
        }
        #endregion

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back("CElse_ELSE", type, Id, ParentId, true);
            this.expression.DisplayYourself(_call_back);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();

            strBld.AppendLine(String.Format("{0}else", CInsertion.PaddingSpaces()));
            strBld.AppendLine(String.Format("{0}", expression.ToString()));
            return strBld.ToString();
        }
         */
        #endregion
    }
    #endregion

    #region CConcatenation
    /// <summary>
    /// Concatenation,
    /// used with conditions "Q1 = 1 OR Q2 = 2"
    /// and 'runs' like "Q4 = 3) AND ( Q1 = 3"
    /// </summary>
    public class CConcatenation : CAtom, ICommon
    {
        public
        enum concatenationType { UKNOWN, AND, OR }

        private concatenationType _concatenationType;

        public concatenationType Con { get { return _concatenationType; } set { _concatenationType = value; } }
        public
        CConcatenation(CAtom param_parent)
            : base(param_parent)
        { }

        private String englishString()
        {
            switch (_concatenationType)
            {
                case concatenationType.AND:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CConcatenation_AND");
                case concatenationType.OR:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CConcatenation_OR");
                default:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CConcatenation_UNKNOWN");
            }
        }

        public override AtomType type { get { return AtomType.CONCATENATION; } }

        public void Toggle()
        {
            // There is a NONE case...
            if (_concatenationType == concatenationType.AND)
                _concatenationType = concatenationType.OR;
            else
                if (_concatenationType == concatenationType.OR)
                    _concatenationType = concatenationType.AND;
        }

        #region Clone
        internal new CConcatenation Clone(CAtom param_parent)
        {
            CConcatenation newConcatenation = new CConcatenation(param_parent);
            newConcatenation._concatenationType = this._concatenationType;
            return newConcatenation;
        }
        #endregion


        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back(englishString(), type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            return " " + englishString();
        }
         */
        #endregion

        #region IValidation Members

        public override bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (ICommon child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && base.IsValid(ref param_message);
            success = success && _concatenationType != CConcatenation.concatenationType.UKNOWN;
            return success;
        }

        #endregion

    }
    #endregion

    #region CRunBase
    /// <summary>
    /// Abstarct base class for conditions and bracker terms
    /// </summary>
    public abstract class CRunBase : CAtom, ICommon
    {
        protected CAtom _parent;

        internal CConcatenation _concatenation;
        public CConcatenation.concatenationType concatenation
        {
            get { return _concatenation.Con; }
            set { _concatenation.Con = value; }
        }

        public
        CRunBase(CAtom param_parent)
            : base(param_parent)
        {
            _parent = param_parent;
            _concatenation = new CConcatenation(param_parent);
            _concatenation.Con = CConcatenation.concatenationType.UKNOWN;
        }

        public void SetParent(CAtom param_parent)
        {
            _concatenation.ParentId = param_parent.Id;
            ParentId = param_parent.Id;
            _parent = param_parent;
        }

        public abstract CRunBase AddCondition(CConcatenation.concatenationType param_conType);
        internal abstract CRunBase AddCondition(CRunBase param_child, CConcatenation.concatenationType param_conType);


        #region Clone
        internal virtual CRunBase Clone(CAtom param_parent)
        {
            throw new ExpressionLibraryException(
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorCannotCloneAbstractClass"),
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
        }
        #endregion

        public override AtomType type { get { return AtomType.RUNBASE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            if (_parent is CRun)
            {
                int index = ((CRun)_parent).IndexOf(this);
                if (index > 0)
                    _concatenation.DisplayYourself(_call_back);
            }
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            if (_parent is CRun)
            {
                int index = ((CRun)_parent).IndexOf(this);

                if (index > 0)
                    strBld.Append(_concatenation.ToString());

            }
            return strBld.ToString();
        }
         */
        #endregion

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
                success = _concatenation.IsObjectValid(ref param_message);
            return success;
        }
        #endregion
    }
    #endregion

    #region CRun
    /// <summary>
    /// Represents a bracket term that owns condition(s) and bracket term(s)
    /// </summary>
    public class CRun : CRunBase, ICommon
    {
        // List of children ( bracket term or condition)
        // Example of two conditions and one bracket term:
        // Child1-->|<------- Child2 ------->|<-Child3 ->|
        //CCondition            CRun          CCondition
        //   Q1 = 1  or ( Q2 = 2 and Q3 = 3 ) or Q4 = 4
        internal List<CRunBase> _run_or_condition;

        private Boolean _disposing = false;
        public
        CRun(CAtom param_parent)
            : base(param_parent)
        {
            _run_or_condition = new List<CRunBase>();
        }

        public int IndexOf(CRunBase param_run)
        {
            return this._run_or_condition.IndexOf(param_run);
        }
        private int Count { get { return _run_or_condition.Count; } }

        internal override void RemoveChild(CAtom param_removal)
        {
            if (_parent is CClause &&               // uppermost the_new_run of a TRUE case
                _run_or_condition.Count == 1 &&     // Only one condition 
                !_disposing)
                throw new ExpressionLibraryException(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_CannotRemove"),
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_RemoveNotPossible"));
            try
            {
                CRunBase child = param_removal as CRunBase;
                _run_or_condition.Remove(child);

                //Example:     ( A ) 
                //remove A     (  )
                // Remove the entire the_new_run if that was the last condition
                if (_run_or_condition.Count == 0)
                    Remove(true);

                //Example:     if ( ( A and B ) and C )
                //remove C     if ( ( A and B ) )
                //Need to elevate the (CRun-)child to remove double brackets
                //Note that the 'this' CRun might be the root of the statement, which makes 
                //an elevation of 'this' impossible (therefore we remove the child)
                if (_run_or_condition.Count == 1 && _run_or_condition[0] is CRun)
                    ((CRun)_run_or_condition[0]).ElevateRun();

                //Example:     if ( ( A and B ) and C )
                //remove B     if ( ( A ) and B )
                //Need to elevate 'this' to remove useless brackets around A
                if (_run_or_condition.Count == 1)
                    ElevateRun();
            }
            catch (System.Exception)
            {
                throw new ExpressionLibraryException(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_Logic1"),
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
            }
        }

        internal void Remove(Boolean param_enforce)
        {
            _disposing = true;
            if (_parent is CClause && !param_enforce)
                throw new ExpressionLibraryException(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_CannotRemove"),
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_RemoveNotPossible"));
            while (_run_or_condition.Count > 0)
                _run_or_condition[0].Remove();

            _parent.RemoveChild(this);
        }
        public override void Remove()
        {
            Remove(false);
            base.Remove();
        }

        /// <summary>
        /// Get the left condition or bracket term 
        /// </summary>
        /// <param name="param_condition">Position to start with</param>
        /// <returns>Condition or bracket term</returns>
        public CRunBase LeftOf(CCondition param_condition)
        {
            int index = _run_or_condition.IndexOf(param_condition);
            if (-1 == index || index < 1) return null;
            return _run_or_condition[index - 1];
        }
        /// <summary>
        /// Get the left condition or bracket term 
        /// </summary>
        /// <param name="param_condition">AND/OR to start with</param>
        /// <returns>Condition or bracket term</returns>
        public CRunBase LeftOf(CConcatenation param_and_or)
        {
            for (int idx = 1; idx < _run_or_condition.Count; idx++)
            {
                if (_run_or_condition[idx]._concatenation == param_and_or)
                    return _run_or_condition[idx - 1];
            }
            return null;
        }
        /// <summary>
        /// Get the right condition or bracket term 
        /// </summary>
        /// <param name="param_condition">Position to start with</param>
        /// <returns>Condition or bracket term</returns>
        public CRunBase RightOf(CCondition param_condition)
        {
            int index = _run_or_condition.IndexOf(param_condition);
            if (-1 == index || index >= _run_or_condition.Count - 1) return null;
            return _run_or_condition[index + 1];
        }
        /// <summary>
        /// Get the right condition or bracket term 
        /// </summary>
        /// <param name="param_condition">AND/OR to start with</param>
        /// <returns>Condition or bracket term</returns>
        public CRunBase RightOf(CConcatenation param_and_or)
        {
            for (int idx = 0; idx < _run_or_condition.Count - 1; idx++)
                if (_run_or_condition[idx]._concatenation == param_and_or)
                    return _run_or_condition[idx];
            return null;
        }

        /// <summary>
        /// Create a condition or bracket statement
        /// (default is AND)
        /// </summary>
        /// <returns></returns>
        public CRunBase AddRun()
        {
            return AddRun(CConcatenation.concatenationType.AND);
        }
        /// <summary>
        /// Create a condition or bracket statement
        /// </summary>
        /// <returns></returns>
        public CRunBase AddRun(CConcatenation.concatenationType param_concatenation)
        {
            CRunBase run = new CRun(this);
            _run_or_condition.Add(run);
            run.concatenation = param_concatenation;
            return run;
        }

        /// <summary>
        /// Add a contion or bracket statement
        /// </summary>
        /// <returns></returns>
        internal virtual CRunBase InsertRun(CRunBase param_run, CConcatenation.concatenationType param_concatenation)
        {
            return InsertRun(new CRunBase[] { param_run }, param_concatenation);
        }

        /// <summary>
        /// Add multiple conditions or bracket statements
        /// Problem:
        /// Need to splitt an existing CRun
        /// <code>
        /// Example:
        /// Current:   |<---------------- Run 1 -------------->|
        ///            (Q1 = 1 and Q2 = 2 and Q3 = 3 and Q4 = 4) 
        /// User wants to embrace the inner two conditions
        /// Result:   |<Run 1>|<----- Run 2 (new)----->|<- Run 1 ->|
        ///            (Q1 = 1 and ( Q2 = 2 and Q3 = 3 ) and Q4 = 4) 
        /// </code>
        /// </summary>
        /// <returns></returns>
        internal virtual CRunBase InsertRun(CRunBase[] param_runs, CConcatenation.concatenationType param_concatenation)
        {
            CRun the_new_run = new CRun(this);
            the_new_run.concatenation = param_concatenation;

            for (int idx = 0; idx < param_runs.Length; idx++)
            {
                CRunBase baseRun = param_runs[idx];
                int index = this._run_or_condition.IndexOf(baseRun);
                if (index == -1)
                    throw new ExpressionLibraryException(
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_Logic2"),
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));

                the_new_run.AddRun(baseRun);

                if (idx == 0)
                    // Replace the first element
                    this._run_or_condition[index] = the_new_run;
                else
                    // Remove others
                    this._run_or_condition.Remove(baseRun);
            }
            return the_new_run;
        }
        public virtual CRunBase InsertRun(CRunBase[] param_runs)
        {
            if (param_runs.Length == 0)
                throw new ExpressionLibraryException(
                                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_Logic3"),
                                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
            return InsertRun(param_runs, param_runs[0].concatenation);
        }
        public virtual CRunBase InsertRun(CRunBase param_run)
        {
            return InsertRun(new CRunBase[] { param_run }, param_run.concatenation);
        }

        // Inserted to support the replacement of code fragments
        // (late change, method might not survive in future versions)
        public void SubstituteRun(CRun run)
        {
            CAtom atom = ExpressionHelper.Find(ParentId);

            // If this is the outer most run, then our parent is the clause itself
            // e.g.: "if A=1 then"
            if (atom is CClause)
            {
                ((CClause)atom).ReplaceRun(run);
                return;
            }

            // Otherwise we have something like this...
            // "if A=B and (c=d) or e=f", where (c=d) is the run to be replaced
            if (!(atom is CRun))
                return;

            CRun parentRun = atom as CRun;

            // Keep the OR,..,AND
            run._run_or_condition[0].concatenation = this.concatenation;

            int myIndex = parentRun.IndexOf(this);
            if (-1 != myIndex)
            {
                parentRun.ReplaceRun(this, run._run_or_condition.ToArray());
                Dispose();
            }
        }


        // see: public void ElevateRun()
        private void ReplaceRun(CRunBase param_child, CRunBase[] param_runs)
        {
            int index = IndexOf(param_child);
            if (-1 == index)
                throw new ExpressionLibraryException(
                                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRun_Logic2"),
                                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));

            int currIndex = index;
            foreach (CRunBase run in param_runs)
            {
                run.SetParent(this);
                if (currIndex == index)
                    _run_or_condition[currIndex] = run;
                else
                    _run_or_condition.Insert(currIndex, run);
                currIndex++;
            }
        }
        /// <summary>
        /// Merge runs
        /// <code>
        /// Example:
        /// Current:  |<Run 1>|<----- Run 2 (this)---->|<- Run 1 ->|
        ///            (Q1 = 1 and ( Q2 = 2 and Q3 = 3 ) and Q4 = 4) 
        /// Result:   |<---------------- Run 1 -------------->|
        ///            (Q1 = 1 and Q2 = 2 and Q3 = 3 and Q4 = 4) 
        /// Note that Run 2 (this) calls its parent Run 1 to perform the copying
        /// </code>
        /// </summary>
        public void ElevateRun()
        {
            CAtom atom = ExpressionHelper.Find(ParentId);
            if (!(atom is CRun))
                return;

            CRun parentRun = atom as CRun;

            int myIndex = parentRun.IndexOf(this);

            if (-1 != myIndex)
            {
                parentRun.ReplaceRun(this, _run_or_condition.ToArray());
            }
            Dispose();
        }
        /// <summary>
        /// Determines if a given set of Runs (= conditions)
        /// can be converted to a bracket term
        // Method shall detect the following problems:
        // (A and B and C) -> ((A and B and C)) // wrong
        // (A and (B and C // wrong
        /// </summary>
        /// <param name="param_runs"></param>
        /// <returns></returns>
        public Boolean CanConvertToRun(CRunBase[] param_runs)
        {
            // Try to avoid the following
            // Example:  (A and B and C) -> ((A and B and C)) // wrong
            foreach (CRunBase run in param_runs)
            {
                // Must be a child of this the_new_run
                if (-1 == IndexOf(run))
                    return false;
            }
            // If all children match and there is the same number, then the arrays are identical.
            if (Count == param_runs.Length)
                return false;
            return true;
        }
        public Boolean CanRemoveRun()
        {
            CAtom atom = ExpressionHelper.Find(ParentId);
            return (atom is CRun);
        }
        public CRunBase AddRun(CRunBase param_run)
        {
            ((CRunBase)param_run).SetParent(this);
            _run_or_condition.Add(param_run);
            return param_run;
        }
        public CRunBase AddCondition()
        {
            return AddCondition(CConcatenation.concatenationType.AND);
        }
        public override CRunBase
        AddCondition(CConcatenation.concatenationType param_conType)
        {
            return AddCondition(null, param_conType);
        }
        internal CRunBase AddCondition(CRunBase param_child)
        {
            return AddCondition(param_child, CConcatenation.concatenationType.AND);
        }
        internal override CRunBase
        AddCondition(CRunBase param_child, CConcatenation.concatenationType param_conType)
        {
            int index = IndexOf(param_child);
            CCondition cond = new CCondition(this);
            cond.concatenation = param_conType;
            if (-1 == index)
                _run_or_condition.Add(cond);
            else
                _run_or_condition.Insert(index + 1, cond);
            return cond;
        }

        #region Clone
        internal override CRunBase Clone(CAtom param_parent)
        {
            CRun newRun = new CRun(param_parent);
            newRun._concatenation = this._concatenation.Clone(newRun);

            foreach (CRunBase baseRun in this._run_or_condition)
                newRun._run_or_condition.Add(baseRun.Clone(newRun));
            return newRun;
        }
        #endregion

        public override AtomType type { get { return AtomType.RUN; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            base.DisplayYourself(_call_back);
            _call_back("CRun_OpenBracket", type, Id, ParentId, true);
            foreach (CRunBase run in this._run_or_condition)
                run.DisplayYourself(_call_back);
            _call_back("CRun_CloseBracket", type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(base.ToString());
            strBld.Append("(");
            foreach (CRunBase run in this._run_or_condition)
                strBld.Append(run.ToString());
            strBld.Append(")");
            return strBld.ToString();
        }
         */
        #endregion

        #region IValidation Members

        public override ICommon[] Children()
        {
            ICommon[] children = (ICommon[])_run_or_condition.ToArray();
            return (children);
        }

        public override Boolean HasChildren()
        {
            return _run_or_condition.Count > 0;
        }
        #endregion
    }
    #endregion

    #region CCondition
    /// <summary>
    /// Simple condition
    /// Example:
    /// "and Q1 = 2"
    /// (the and/or is maintained by the base class CRunBase
    /// </summary>
    public class CCondition : CRunBase, ICommon
    {
        private CLValue _lValue;
        private CRValue _rValue;
        private COperand _operand;

        public AnswerProvider lValue
        {
            get { return _lValue.AnswerProviderItem; }
            set { _lValue.AnswerProviderItem = value; }
        }
        public AnswerProvider rValue
        {
            get { return _rValue.AnswerProviderItem; }
            set { _rValue.AnswerProviderItem = value; }
        }
        public OperatorDisplay Operator
        {
            get { return _operand.OperatorDisplay; }
            set { _operand.OperatorDisplay = value; }
        }

        // There is nothing like a NONE datatype. But we need that kind of state
        public Boolean hasDataType()
        {
            return (lValue != null || rValue != null);
        }

        // Test if the item represents a 'Perfectus constant' and support a specific type
        // For example: The item <<Not Answered>> is supported with all data types,
        //              whereas the "current date constant" is only valid for PerfectusDataType.Date
        private Boolean SupportDatatype(PerfectusDataType param_dataType, PackageConstant param_item)
        {
            Boolean success;

            if (param_item.Value is String)
            {
                if (String.Compare((String)param_item.Value, "Not Answered", true) == 0)
                    return true;
                if (String.Compare((String)param_item.Value, "Null", true) == 0)
                    return true;
                if (String.Compare((String)param_item.Value, "DateTime.Now", true) == 0)
                    return param_dataType == PerfectusDataType.Date;
            }

            AnswerProvider.ConvertToBestType(
                    param_item.Value, param_dataType, out success, AnswerProvider.Caller.System,
                    param_item.StringBindingMetaData);
            return success;
        }

        /// <summary>
        /// Determine all possible datatypes that the actual Values can hold
        /// Example:
        /// The right value is '100', then the left value can either
        /// be a number or text. This procedure returns all possible types so 
        /// that the call knows which questions are valid for selections
        /// </summary>
        /// <returns></returns>
        public PerfectusDataType[] GetDataTypes()
        {
            // simple case
            // Both values are 'empty' - all types are possible
            if (lValue == null &&
                rValue == null)
                return new PerfectusDataType[] { 
                PerfectusDataType.Text, 
                PerfectusDataType.Number,
                PerfectusDataType.YesNo,
                PerfectusDataType.Date };

            // simple cases
            // Either of the values is a question -> the type must be exact.
            if (lValue != null &&
                lValue is Question)
                return new PerfectusDataType[] { lValue.DataType };
            if (rValue != null &&
                rValue is Question)
                return new PerfectusDataType[] { rValue.DataType };

            // simple cases
            // If either value is a function, then the datatype has to be of NUMBER
            if (lValue != null &&
                (lValue is ArithmeticPFunction || lValue is DatePFunction))
                return new PerfectusDataType[] { lValue.DataType };
            if (rValue != null &&
                (rValue is ArithmeticPFunction || rValue is DatePFunction))
                return new PerfectusDataType[] { rValue.DataType };

            // complex cases
            List<PerfectusDataType> allPossibleTypes = new List<PerfectusDataType>();

            // Test both sides for constants
            // (older versions do not support constants as lValues,
            //  therefore this test is an extension)
            PackageConstant lConstant = null;
            PackageConstant rConstant = null;

            if (lValue != null &&
                lValue is PackageConstant)
                lConstant = (PackageConstant)lValue;

            if (rValue != null &&
                rValue is PackageConstant)
                rConstant = (PackageConstant)rValue;

            // Test for Text
            if ((lConstant == null || SupportDatatype(PerfectusDataType.Text, lConstant))
                && (rConstant == null || SupportDatatype(PerfectusDataType.Text, rConstant)))
                allPossibleTypes.Add(PerfectusDataType.Text);

            // Test for YesNo
            if ((lConstant == null || SupportDatatype(PerfectusDataType.YesNo, lConstant))
                && (rConstant == null || SupportDatatype(PerfectusDataType.YesNo, rConstant)))
                allPossibleTypes.Add(PerfectusDataType.YesNo);

            // Test for Number
            if ((lConstant == null || SupportDatatype(PerfectusDataType.Number, lConstant))
                && (rConstant == null || SupportDatatype(PerfectusDataType.Number, rConstant)))
                allPossibleTypes.Add(PerfectusDataType.Number);

            // Test for Date
            if ((lConstant == null || SupportDatatype(PerfectusDataType.Date, lConstant))
                && (rConstant == null || SupportDatatype(PerfectusDataType.Date, rConstant)))
                allPossibleTypes.Add(PerfectusDataType.Date);
            return allPossibleTypes.ToArray();
        }
        // A value of the condition changed and now determines exactly one possible
        // datatype. The other value and the operator might need to be set accordingly
        public void MaybeEnforceType(PerfectusDataType param_datatype)
        {
            if (lValue != null &&
                lValue is PackageConstant &&
                lValue.DataType != param_datatype)
                ((PackageConstant)lValue).SetValue(((PackageConstant)lValue).Value, param_datatype);

            if (rValue != null &&
                rValue is PackageConstant &&
                rValue.DataType != param_datatype)
                ((PackageConstant)rValue).SetValue(((PackageConstant)rValue).Value, param_datatype);

            // We might have a (let's say) GE, but changed the type to Text...
            if ((param_datatype == PerfectusDataType.YesNo ||
                param_datatype == PerfectusDataType.Text) &&
                (_operand.OperatorDisplay.Op != Perfectus.Common.Operator.Eq &&
                _operand.OperatorDisplay.Op != Perfectus.Common.Operator.Ne))
                _operand.OperatorDisplay.Op = Perfectus.Common.Operator.Eq;
        }

        public
        CCondition(CAtom param_parent)
            : base(param_parent)
        {
            _parent = param_parent;
            _lValue = new CLValue(this);
            _rValue = new CRValue(this);
            _operand = new COperand(this);
        }

        internal override CRunBase AddCondition(CRunBase param_child, CConcatenation.concatenationType param_conType)
        {
            throw new ExpressionLibraryException("not implemented");
        }
        public override CRunBase AddCondition(CConcatenation.concatenationType param_concatenation)
        {
            return ((CRun)_parent).AddCondition(this, param_concatenation);
        }
        public virtual CRunBase AddRun(CConcatenation.concatenationType param_concatenation)
        {
            return ((CRun)_parent).InsertRun(this, param_concatenation);
        }
        public virtual CRunBase AddRun()
        {
            return ((CRun)_parent).InsertRun(this);
        }

        public override void Remove()
        {
            ((CRun)_parent).RemoveChild(this);
            base.Remove();
        }

        #region Clone
        internal override CRunBase Clone(CAtom param_parent)
        {
            CCondition newCondition = new CCondition(param_parent);
            //newCondition._concatenation = this._concatenation.Clone(newCondition);
            newCondition.concatenation = this._concatenation.Con;

            newCondition._lValue = this._lValue.Clone(newCondition);
            newCondition._rValue = this._rValue.Clone(newCondition); ;
            newCondition._operand = this._operand.Clone(newCondition);
            return newCondition;
        }
        #endregion

        public override AtomType type { get { return AtomType.CONDITION; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            base.DisplayYourself(_call_back);
            _lValue.DisplayYourself(_call_back);
            _operand.DisplayYourself(_call_back);
            _rValue.DisplayYourself(_call_back);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(base.ToString());
            strBld.Append(String.Format(" {0} {1} {2} ",
                _lValue.ToString(),
                _operand.ToString(),
                _rValue.ToString()));
            return strBld.ToString();
        }
         */
        #endregion
        #region IValidation Members

        public override ICommon[] Children()
        {
            return (new ICommon[] { _lValue, _rValue, _operand });
        }
        public override Boolean HasChildren()
        {
            return true;
        }
        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }
        #endregion
    }
    #endregion

    #region CValue
    /// <summary>
    /// Base class for a left- and right question/constant
    /// Stores an AnswerProvide object from the Perfectus core
    /// </summary>
    public abstract class CValue : CAtom, ICommon
    {
        private AnswerProvider _answerProvider;
        public AnswerProvider AnswerProviderItem
        { set { _answerProvider = value; } get { return _answerProvider; } }

        protected String Name
        {
            get
            {
                if (_answerProvider == null)
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CValue_QuestionOrValue");
                else
                    if (_answerProvider is PackageConstant && _answerProvider.DataType == PerfectusDataType.Text)
                        return String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CValue_Constant"),
                            _answerProvider.ToString());
                return _answerProvider.ToString();
            }
        }

        private void Init()
        {
            _answerProvider = null;
        }
        public
        CValue(CAtom param_parent)
            : base(param_parent)
        {
            Init();
        }
        public
        CValue()
        {
            Init();
        }

        #region Clone
        internal new CValue Clone(CAtom param_parent)
        {
            throw new ExpressionLibraryException(
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorCannotCloneAbstractClass"),
                "CValue::Clone",
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorSoftwareDefect"));
        }
        #endregion

        public override AtomType type { get { return AtomType.VALUE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            String s = String.Empty;
            _call_back(String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CValue_Value"),
                Name), type, Id, ParentId, IsObjectValid(ref s));
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", Name));
            return strBld.ToString();
        }
         */
        #endregion

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
            {
                success = _answerProvider != null;
                if (!success)
                {
                    if (param_message.Length > 0)
                        param_message += ", ";
                    param_message += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CValue_MissingName");
                }
            }
            return success;
        }

        #endregion
    }
    #endregion

    #region CRValue

    /// <summary>
    /// Right value
    /// Allow to hold PackageConstant and Questions
    /// </summary>
    public class CRValue : CValue, ICommon
    {
        public new String Name
        {
            get { return base.Name; }
        }

        public
        CRValue(CAtom param_parent)
            : base(param_parent)
        {
        }

        #region Clone
        internal new CRValue Clone(CAtom param_parent)
        {
            CRValue newValue = new CRValue(param_parent);
            if (null != base.AnswerProviderItem)
                newValue.AnswerProviderItem = base.AnswerProviderItem.Clone() as AnswerProvider;
            return newValue;
        }
        #endregion

        public override AtomType type { get { return AtomType.RVALUE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            String s = String.Empty;
            _call_back(String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CRValue_Value"),
                Name), type, Id, ParentId, IsObjectValid(ref s));
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", base.ToString()));
            return strBld.ToString();
        }
         */
        #endregion

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            return base.IsObjectValid(ref param_message);
        }

        #endregion
    }
    #endregion

    #region CValue
    /// <summary>
    /// Can hold Questions only
    /// </summary>
    public class CLValue : CValue, ICommon
    {
        public new String Name
        {
            get
            {
                return (AnswerProviderItem == null ?
          ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CLValue_Question")
          : base.Name);
            }
        }

        public
        CLValue(CAtom param_parent)
            : base(param_parent)
        {
        }

        #region Clone
        internal new CLValue Clone(CAtom param_parent)
        {
            CLValue newValue = new CLValue(param_parent);
            if (null != base.AnswerProviderItem)
                newValue.AnswerProviderItem = base.AnswerProviderItem.Clone() as AnswerProvider;
            return newValue;
        }
        #endregion

        public override AtomType type { get { return AtomType.LVALUE; } }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            String s = String.Empty;
            _call_back(String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("CLValue_Value"),
                Name), type, Id, ParentId, IsObjectValid(ref s));
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", base.ToString()));
            return strBld.ToString();
        }
         */
        #endregion

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }

        #endregion
    }
    #endregion

    #region COperand
    /// <summary>
    /// Operand in a condition 
    /// </summary>
    public class COperand : CAtom, ICommon
    {
        private OperatorDisplay _operator;
        public OperatorDisplay OperatorDisplay
        {
            get { return _operator; }
            set { _operator = value; }
        }

        internal
        COperand(CAtom param_parent)
            : base(param_parent)
        {
            _operator = new OperatorDisplay(Operator.Eq);
        }

        public void Reset()
        {
            _operator.Op = Operator.Eq;
        }

        #region Clone
        internal new COperand Clone(CAtom param_parent)
        {
            COperand newValue = new COperand(param_parent);
            newValue.OperatorDisplay = new OperatorDisplay(_operator.Op);
            return newValue;
        }
        #endregion

        public override AtomType type { get { return AtomType.OPERATOR; } }

        private String LocalizedDisplay ( )
        {
            switch (_operator.Op)
            {
                case Operator.Eq:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Eq");
                case Operator.Ge:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Ge");
                case Operator.Le:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Le");
                case Operator.Gt:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Gt");
                case Operator.Lt:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Lt");
                case Operator.Ne:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Ne");
            }
            return OperatorDisplay.Display;
        }

        #region Display
        public override void DisplayYourself(DisplayYourselfCallback _call_back)
        {
            _call_back(String.Format("{0}", LocalizedDisplay ( )), type, Id, ParentId, true);
        }
        /*
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", OperatorDisplay.Display));
            return strBld.ToString();
        }
         */ 
        #endregion
        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);

            success = OperatorDisplay.Op != Operator.And &&
                OperatorDisplay.Op != Operator.Or;
            if (!success)
                param_message += 
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("COperator_Invalid");
            return success;
        }

        #endregion
    }
    #endregion
}
