using System;
using System.Collections.Generic;
using System.Text;

using Perfectus.Client.Studio.ExpressionLibrary;
using System.Resources;
using System.Reflection;

namespace Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter
{
    public delegate void WriteText(String param_text);
    public delegate void WriteElement(String param_text, Guid param_Id, Guid aram_parentId, AtomType param_type, Boolean param_valid);

    public interface IPrintFormatter
    {
        event WriteText writeText;
        event WriteElement writeElement;
        void Print(String Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid);
    }

    #region PrintFormatter
    public class CPrintFormatter : IPrintFormatter
    {
        public event WriteText writeText;
        public event WriteElement writeElement;

        protected void OnWriteText(String param_text)
        {
            if (writeText != null)
                writeText(param_text);
        }
        protected void OnWriteElement(String param_text, Guid param_Id, Guid param_parentId, AtomType param_type, Boolean param_valid)
        {
            if (writeElement != null)
                writeElement(param_text, param_Id, param_parentId, param_type, param_valid);
        }

        ResourceManager rm;
        const int _const_indentation = 4;
        protected int _iStmt = 0;
        protected int iStmt
        {
            get { return _iStmt; }
            set { _iStmt = value; }
        }
        protected int m_count_brackets = 0;

        public CPrintFormatter()
        {
            iStmt = 0;
            rm = Perfectus.Common.ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation");
            localized_words = new Dictionary<string, string>();
        }

        protected String indention()
        {
            if (iStmt > 2)
            {
                String retStr = System.String.Empty;
                return retStr.PadRight((iStmt - 2) * _const_indentation);
            }
            else
                return String.Empty;
        }

        #region IPrintFormatter Members

        Dictionary<String, String> localized_words;

        protected String MaybeGetLocalizedString(String param_str)
        {
            if (localized_words.ContainsKey(param_str))
                return localized_words[param_str];
            String retStr = null;

            retStr = rm.GetString(param_str);
            if (retStr == null)
                retStr = param_str;
            localized_words.Add(param_str, retStr);
            return retStr;
        }

        // ToDo
        // Name could be a token like 'CPlaceholder_NewTextOrStatement', or 
        // an identifier like 'Question_abc'. These cases should be handled differently, as 
        // the second would never be in a resource file.
        void IPrintFormatter.Print(string Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid)
        {
            Boolean want_new_line = false;
            Boolean suppress_write = false;

            // 


            int postdecrement = 0;

            switch (Type)
            {
                case AtomType.WORDTEXT:
                case AtomType.PLACEHOLDER:
                case AtomType.INTEGRATION:
                case AtomType.NAVIGATIONPAGE:
                    return;
                case AtomType.HTMLSTRINGBINDING:
                    want_new_line = true;
                    iStmt++;
                    iStmt++;
                    postdecrement--;
                    postdecrement--;
                    break;

                case AtomType.RUN:
                    if (String.Compare(Name, "CRun_OpenBracket", true) == 0)
                    {
                        if (m_count_brackets++ < 0) suppress_write = true;
                    }
                    else
                    {
                        if (--m_count_brackets < 0) suppress_write = true;
                    }
                    break;
                case AtomType.ELSEEXPRESSION:
                    want_new_line = true;
                    break;

                case AtomType.CONCATENATION:
                    iStmt++;
                    postdecrement--;
                    want_new_line = true;
                    break;


                case AtomType.BLOCK:
                    {
                        suppress_write = true;
                        iStmt = 0;
                        if (String.Compare(Name, "CBlock_END", true) == 0)
                            want_new_line = true;
                    }
                    break;
                case AtomType.STATEMENT:
                    if (String.Compare(Name, "CStatement_ENDIF", true) == 0)
                    {
                        want_new_line = true;
                        if (iStmt > 0)
                        {
                            postdecrement--;
                            postdecrement--;
                        }
                    }
                    else
                        if (String.Compare(Name, "CStatement_THEN", true) == 0)
                            m_count_brackets++;

                        else
                            if (String.Compare(Name, "CStatement_IF", true) == 0)
                            {
                                m_count_brackets--;
                                want_new_line = true;
                                iStmt++;
                                iStmt++;
                            }
                    break;
            }

            if (want_new_line && !suppress_write)
            {
                OnWriteText(System.Environment.NewLine);
                OnWriteText(indention());
            }

            iStmt += postdecrement;
            if (!suppress_write)
                OnWriteElement(" " + MaybeGetLocalizedString(Name), Id, ParentId, Type, valid);
        }

        #endregion
    }
    #endregion

    #region formatting for outcomes Queries
    public class CPrintOutcomeFormatter : CPrintFormatter, IPrintFormatter
    {
        #region IPrintFormatter Members


        // ToDo
        // Name could be a token like 'CPlaceholder_NewTextOrStatement', or 
        // an identifier like 'Question_abc'. These cases should be handled differently, as 
        // the second would never be in a resource file.
        void IPrintFormatter.Print(string Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid)
        {
            Boolean want_new_line = false;
            Boolean suppress_write = false;

            // 


            int postdecrement = 0;

            switch (Type)
            {
                case AtomType.WORDTEXT:
                case AtomType.PLACEHOLDER:
                case AtomType.INTEGRATION:
                    want_new_line = true;
                    iStmt++;
                    iStmt++;
                    postdecrement--;
                    postdecrement--;
                    break;

                case AtomType.RUN:
                    if (String.Compare(Name, "CRun_OpenBracket", true) == 0)
                    {
                        if (m_count_brackets++ < 0) suppress_write = true;
                    }
                    else
                    {
                        if (--m_count_brackets < 0) suppress_write = true;
                    }
                    break;
                case AtomType.ELSEEXPRESSION:
                    want_new_line = true;
                    break;

                case AtomType.CONCATENATION:
                    iStmt++;
                    postdecrement--;
                    want_new_line = true;
                    break;


                case AtomType.BLOCK:
                    {
                        suppress_write = true;
                        iStmt = 0;
                        if (String.Compare(Name, "CBlock_END", true) == 0)
                            want_new_line = true;
                    }
                    break;
                case AtomType.STATEMENT:
                    if (String.Compare(Name, "CStatement_ENDIF", true) == 0)
                    {
                        want_new_line = true;
                        if (iStmt > 0)
                        {
                            postdecrement--;
                            postdecrement--;
                        }
                    }
                    else
                        if (String.Compare(Name, "CStatement_THEN", true) == 0)
                            m_count_brackets++;

                        else
                            if (String.Compare(Name, "CStatement_IF", true) == 0)
                            {
                                m_count_brackets--;
                                want_new_line = true;
                                iStmt++;
                                iStmt++;
                            }
                    break;
            }

            if (want_new_line && !suppress_write)
            {
                OnWriteText(System.Environment.NewLine);
                OnWriteText(indention());
            }

            iStmt += postdecrement;
            if (!suppress_write)
                OnWriteElement(" " + MaybeGetLocalizedString(Name), Id, ParentId, Type, valid);
        }

        #endregion
    }
    #endregion

    #region formatting for Navigation pages Queries
    public class CPrintNavigationFormatter : CPrintFormatter, IPrintFormatter
    {
        void IPrintFormatter.Print(string Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid)
        {
            Boolean want_new_line = false;
            Boolean suppress_write = false;
            int postdecrement = 0;

            switch (Type)
            {
                case AtomType.WORDTEXT:
                case AtomType.PLACEHOLDER:
                case AtomType.INTEGRATION:
                    return;
                case AtomType.NAVIGATIONPAGE:
                    want_new_line = true;
                    iStmt++;
                    iStmt++;
                    postdecrement--;
                    postdecrement--;
                    break;

                case AtomType.RUN:
                    if (String.Compare(Name, "CRun_OpenBracket", true) == 0)
                    {
                        if (m_count_brackets++ < 0) suppress_write = true;
                    }
                    else
                    {
                        if (--m_count_brackets < 0) suppress_write = true;
                    }
                    break;
                case AtomType.ELSEEXPRESSION:
                    want_new_line = true;
                    break;

                case AtomType.CONCATENATION:
                    iStmt++;
                    postdecrement--;
                    want_new_line = true;
                    break;


                case AtomType.BLOCK:
                    {
                        suppress_write = true;
                        iStmt = 0;
                        if (String.Compare(Name, "CBlock_END", true) == 0)
                            want_new_line = true;
                    }
                    break;
                case AtomType.STATEMENT:
                    if (String.Compare(Name, "CStatement_ENDIF", true) == 0)
                    {
                        want_new_line = true;
                        if (iStmt > 0)
                        {
                            postdecrement--;
                            postdecrement--;
                        }
                    }
                    else
                        if (String.Compare(Name, "CStatement_THEN", true) == 0)
                            m_count_brackets++;

                        else
                            if (String.Compare(Name, "CStatement_IF", true) == 0)
                            {
                                m_count_brackets--;
                                want_new_line = true;
                                iStmt++;
                                iStmt++;
                            }
                    break;
            }

            if (want_new_line && !suppress_write)
            {
                OnWriteText(System.Environment.NewLine);
                OnWriteText(indention());
            }

            iStmt += postdecrement;
            if (!suppress_write)
                OnWriteElement(" " + MaybeGetLocalizedString(Name), Id, ParentId, Type, valid);
        }
    }
    #endregion

    #region formatting for Navigation pages Queries
    public class CPrintHtmlStringBindingFormatter : CPrintFormatter, IPrintFormatter
    {
        void IPrintFormatter.Print(string Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid)
        {
            Boolean want_new_line = false;
            Boolean suppress_write = false;
            int postdecrement = 0;

            switch (Type)
            {
                case AtomType.WORDTEXT:
                case AtomType.PLACEHOLDER:
                case AtomType.INTEGRATION:
                case AtomType.NAVIGATIONPAGE:
                    suppress_write = true;
                    break;

                case AtomType.HTMLSTRINGBINDING:
                    want_new_line = true;
                    iStmt++;
                    iStmt++;
                    postdecrement--;
                    postdecrement--;
                    break;

                case AtomType.RUN:
                    if (String.Compare(Name, "CRun_OpenBracket", true) == 0)
                    {
                        if (m_count_brackets++ < 0) suppress_write = true;
                    }
                    else
                    {
                        if (--m_count_brackets < 0) suppress_write = true;
                    }
                    break;
                case AtomType.ELSEEXPRESSION:
                    want_new_line = true;
                    break;

                case AtomType.CONCATENATION:
                    iStmt++;
                    postdecrement--;
                    want_new_line = true;
                    break;


                case AtomType.BLOCK:
                    {
                        suppress_write = true;
                        iStmt = 0;
                        if (String.Compare(Name, "CBlock_END", true) == 0)
                            want_new_line = true;
                    }
                    break;
                case AtomType.STATEMENT:
                    if (String.Compare(Name, "CStatement_ENDIF", true) == 0)
                    {
                        want_new_line = true;
                        if (iStmt > 0)
                        {
                            postdecrement--;
                            postdecrement--;
                        }
                    }
                    else
                        if (String.Compare(Name, "CStatement_THEN", true) == 0)
                            m_count_brackets++;

                        else
                            if (String.Compare(Name, "CStatement_IF", true) == 0)
                            {
                                m_count_brackets--;
                                want_new_line = true;
                                iStmt++;
                                iStmt++;
                            }
                    break;
            }

            if (want_new_line && !suppress_write)
            {
                OnWriteText(System.Environment.NewLine);
                OnWriteText(indention());
            }

            iStmt += postdecrement;
            if (!suppress_write)
                OnWriteElement(" " + MaybeGetLocalizedString(Name), Id, ParentId, Type, valid);
        }
    }
    #endregion

    #region formatting for YES NO Queries
    public class CPrintYesNoFormatter : CPrintFormatter, IPrintFormatter
    {
        void IPrintFormatter.Print(string Name, AtomType Type, Guid Id, Guid ParentId, Boolean valid)
        {
            Boolean want_new_line = false;
            Boolean suppress_write = false;

            int postdecrement = 0;

            switch (Type)
            {
                case AtomType.WORDTEXT:
                case AtomType.PLACEHOLDER:
                case AtomType.INTEGRATION:
                case AtomType.ELSEEXPRESSION:
                case AtomType.STATEMENT:
                    suppress_write = true;
                    break;

                case AtomType.BLOCK:
                    suppress_write = true;
                    want_new_line = true;
                    break;

                case AtomType.RUN:
                    if (String.Compare(Name, "CRun_OpenBracket", true) == 0)
                    {
                        if (m_count_brackets++ <= 0) suppress_write = true;
                    }
                    else
                    {
                        if (--m_count_brackets <= 0) suppress_write = true;
                    }
                    break;

                case AtomType.CONCATENATION:
                    iStmt++;
                    postdecrement--;
                    want_new_line = true;
                    break;
            }

            if (want_new_line )
            {
                OnWriteText(System.Environment.NewLine);
                OnWriteText(indention());
            }

            iStmt += postdecrement;
            if (!suppress_write)
                OnWriteElement(" " + MaybeGetLocalizedString(Name), Id, ParentId, Type, valid);
        }
    }
    #endregion
}

