using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.ExpressionLibrary;
using System.Globalization;

namespace Perfectus.Client.Studio.ExpressionLibrary.Converter
{
    /// <summary>
    /// Helper class to convert from CQuery to 
    /// Perfectus.Common.PackageObjects.Query
    /// </summary>
    public class CConverter
    {
        // Column index in the query table
        const int _colConcatenation = 0;
        const int _colLeftParenthesis = 1;
        const int _colLeftOperand = 2;
        const int _colOperator = 3;
        const int _colRightOperand = 4;
        const int _colRightParenthesis = 5;

        const String _strOpenParentheses = "(";
        const String _strCloseParentheses = ")";

        Package _package;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_package">The package</param>
        public CConverter(Package param_package) { _package = param_package; }

        /// <summary>
        /// Convert a CQuery into a Perfectus.Query
        /// </summary>
        /// <param name="param_query">The CQuery</param>
        /// <returns>The Perfectus Query</returns>
        public Query Convert(CQuery param_query)
        {
            // The current implementation handles only one block
            CStatement statement = param_query._blocks[0].Statement;
            return EvaluateStatement(new Query(_package), statement);
        }


        /// <summary>
        /// Evaluate from the root statement
        /// </summary>
        /// <param name="query">Perfectus query to use</param>
        /// <param name="param_statement">root statement</param>
        /// <returns>resulting query</returns>
        private Query EvaluateStatement(Query query, CExpressionBase param_statement)
        {
            // Used as a REF parameter during recursion (to avoid ugly member variables)
            Boolean wantOpenBracket = false;

            // Perfectus queries are stored in the following table
            DataTable dt = CreateQueryTable();

            // Root statement
            CStatement statement = (CStatement)param_statement;

            // There is no ELSE clause...
            foreach (CRunBase run in statement.if_clause.Run._run_or_condition)
                EvaluateRunOrCondition(run, ref dt, ref wantOpenBracket);

            // Save and evaluate the query
            try
            {
                query.QueryExpression.SaveDataTable(dt, true);
            }
            catch (Exception ex)
            {
                throw new ExpressionLibraryException(
ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("QUERY_Evaluation_Failed"),
ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("QUERY_Evaluation_Failed_Caption"));
            }

            // Each query has zero or one IF branch...
            if (statement.IfStatement != null)
            {
                if (statement.IfStatement is CHtmlStringBinding)
                {
                    CHtmlStringBinding htmlStringBinding = statement.IfStatement as CHtmlStringBinding;
                    if (query.ActionIfTrue == null)
                        query.ActionIfTrue = new HtmlStringBindingAction();
                    HtmlStringBindingAction htmlStringBindingAction = query.ActionIfTrue as HtmlStringBindingAction;
                    htmlStringBindingAction.Payload = htmlStringBinding.Payload;
                }
                else
                if (statement.IfStatement is CNavigationPage)
                {
                    CNavigationPage navigationPage = statement.IfStatement as CNavigationPage;
                    if (query.ActionIfTrue == null)
                        query.ActionIfTrue = new NavigationAction();

                    NavigationAction navigationAction = query.ActionIfTrue as NavigationAction;
                    navigationAction.Page = (InterviewPage)navigationPage.Payload;
                }
                else
                    if (statement.IfStatement is CWordText)
                    {
                        CWordText outcome = statement.IfStatement as CWordText;
                        if (query.ActionIfTrue == null)
                            query.ActionIfTrue = new OutcomeAction();

                        OutcomeAction outcomeAction = query.ActionIfTrue as OutcomeAction;
                        outcomeAction.OfficeOpenXML = (String)outcome.Payload;
                        outcomeAction.DisplayName = outcome.Name;
                    }
                    else
                        if (statement.IfStatement is CIntegration)
                        {
                            CIntegration outcome = statement.IfStatement as CIntegration;
                            if (query.ActionIfTrue == null)
                                query.ActionIfTrue = new OutcomeAction();

                            OutcomeAction outcomeAction = query.ActionIfTrue as OutcomeAction;
                            outcomeAction.DisplayName = outcome.DisplayName;
                            outcomeAction.LibraryKey = outcome.ClauseRef;
                            outcomeAction.LibraryName = outcome.LibraryName;
                            outcomeAction.FetcherId = outcome.FetcherId;
                            outcomeAction.ReportingId = outcome.UniqueLibraryKey;
                        }

                        //else (statement.IfStatement is CNavigation)
                        //    query.ActionIfTrue = new NavigationAction();
                        //else (statement.IfStatement is CNavigation)
                        //    query.ActionIfTrue = new YesNoAction();
                        else
                            if (statement.IfStatement is CPlaceholder)
                            {
                                query.ActionIfTrue = new OutcomeAction(); // do nothing
                            }
                            else
                            {
                                // Recursive call
                                query.ActionIfTrue = (ActionBase)new OutcomeAction();
                                query.ActionIfTrue.SubQuery = EvaluateStatement(new Query(_package), statement.IfStatement as CExpressionBase);
                            }
            }
            /* bad fix for navigation pages 
   
else
query.ActionIfTrue = new OutcomeAction();
             */

            // Each query has zero or one ELSE branch...
            if (statement.ElseStatement != null)
            {
                CExpressionBase elseStmt = (CExpressionBase)(statement.ElseStatement as CElseExpression).Expression;

                if (elseStmt is CHtmlStringBinding)
                {
                    CHtmlStringBinding htmlStringBinding = elseStmt as CHtmlStringBinding;
                    if (query.ActionIfFalse == null)
                        query.ActionIfFalse = new HtmlStringBindingAction();

                    HtmlStringBindingAction htmlStringBindingAction = query.ActionIfFalse as HtmlStringBindingAction;
                    htmlStringBindingAction.Payload= htmlStringBinding.Payload;
                }
                else
                    if (elseStmt is CNavigationPage)
                    {
                        CNavigationPage navigationPage = elseStmt as CNavigationPage;
                        if (query.ActionIfFalse == null)
                            query.ActionIfFalse = new NavigationAction();

                        NavigationAction navigationAction = query.ActionIfFalse as NavigationAction;
                        navigationAction.Page = (InterviewPage)navigationPage.Payload;
                    }
                    else
                    if (elseStmt is CWordText)
                    {
                        CWordText outcome = elseStmt as CWordText;
                        if (query.ActionIfFalse == null)
                            query.ActionIfFalse = new OutcomeAction();

                        OutcomeAction outcomeAction = query.ActionIfFalse as OutcomeAction;
                        outcomeAction.OfficeOpenXML = (String)outcome.Payload;
                        outcomeAction.DisplayName = outcome.Name;
                    }
                    else
                        if (elseStmt is CIntegration)
                        {
                            CIntegration outcome = elseStmt as CIntegration;
                            if (query.ActionIfFalse == null)
                                query.ActionIfFalse = new OutcomeAction();

                            OutcomeAction outcomeAction = query.ActionIfFalse as OutcomeAction;
                            outcomeAction.DisplayName = outcome.DisplayName;
                            outcomeAction.LibraryKey = outcome.ClauseRef;
                            outcomeAction.LibraryName = outcome.LibraryName;
                            outcomeAction.FetcherId = outcome.FetcherId;
                            outcomeAction.ReportingId = outcome.UniqueLibraryKey;
                        }

                     //else (elseStmt is CNavigation)
                        //    query.ActionIfFalse = new NavigationAction();
                        //else (elseStmt is CNavigation)
                        //    query.ActionIfFalse = new YesNoAction();
                        else
                        {
                            CElseExpression elseCase = statement.ElseStatement as CElseExpression;
                            if (elseCase.Expression is CPlaceholder)
                            {
                                query.ActionIfFalse = null; // do nothing
                            }
                            else
                            {
                                // Recursive call
                                query.ActionIfFalse = (ActionBase)new OutcomeAction();
                                query.ActionIfFalse.SubQuery = EvaluateStatement(new Query(_package), elseStmt);
                            }
                        }
            }
            /* bad fix for navigation pages 
            else
            // Have a bug on the server - query.ActionIfFalse must not be null, even if
            // there is no query or outcome
            query.ActionIfFalse = new OutcomeAction();
            */

            return query;
        }

        /// <summary>
        /// Convert a CCondition into an expression row and append to the query table
        /// </summary>
        /// <param name="param_condition">Condition to convert</param>
        /// <param name="param_want_opening_parenthese">Condition shall have opening backet</param>
        /// <param name="param_table">Query table</param>
        private void ConditionIntoTable(CCondition param_condition, Boolean param_want_opening_parenthese, ref DataTable param_table)
        {
            PackageItem leftQuestion = param_condition.lValue;
            PackageItem rightQuestion = param_condition.rValue;
            Operator operand = Operator.Eq;
            if (param_condition.Operator != null)
                operand = param_condition.Operator.Op;

            ExpressionNode expressionNode = new ExpressionNode(null, leftQuestion, operand, rightQuestion);

            // The current library allows for a leading AND in the first condition, hence everything is AND as default
            object initialExpressionOperator = DBNull.Value;
            if (param_condition.concatenation == CConcatenation.concatenationType.OR)
                initialExpressionOperator = Operator.Or;
            else
                initialExpressionOperator = Operator.And;

            object[] rowValues = new object[6];
            // Define the datarow and add to the internal table
            if (initialExpressionOperator == DBNull.Value)
                rowValues[_colConcatenation] = (DBNull)initialExpressionOperator;
            else
                rowValues[_colConcatenation] = new OperatorDisplay((Operator)initialExpressionOperator);

            if (param_want_opening_parenthese)
                rowValues[_colLeftParenthesis] = _strOpenParentheses;
            else
                rowValues[_colLeftParenthesis] = System.String.Empty;

            rowValues[_colLeftOperand] = expressionNode.Operand1;				// Package Item
            rowValues[_colOperator] = new OperatorDisplay(expressionNode.ExpressionOperator);
            rowValues[_colRightOperand] = expressionNode.Operand2;				// Package Item
            param_table.Rows.Add(rowValues);
            // We do not set any closing parenthese here...
        }


        /// <summary>
        /// A statement that always evalutates to TRUE
        /// </summary>
        /// <param name="param_element">dummy parent</param>
        /// <returns>AND <<Yes>>==<<Yes>></Yes></Yes></returns>
        private CCondition alwaysTRUECondition(CRunBase param_element)
        {
            // Note that it must be an AND concatenation
            CCondition condDummy = new CCondition(param_element);
            condDummy.concatenation = CConcatenation.concatenationType.AND;
            condDummy.lValue = new PackageConstant(PerfectusDataType.YesNo, true);
            condDummy.rValue = new PackageConstant(PerfectusDataType.YesNo, true);
            condDummy.Operator.Op = Operator.Eq;
            return condDummy;
        }
            
        /// <summary>
        /// A the_new_run is a sequention of conditions that are surrounded by parenthese
        /// <example>
        /// ( Q1 = 1 and ( Q2 = 2 or Q3 = 3))
        /// 1. Run is ( Q1 = 1 ...)) containing two statements -> one condition and one the_new_run.
        /// 1. Run is ( Q2 = 2 or Q3 = 3) containing two statements -> two conditions
        /// </example>
        /// </summary>
        /// <param name="param_element">Run to convert</param>
        /// <param name="param_table">query table</param>
        /// <param name="wantOpenBracket">Next condition shall have opening parentheses</param>
        private void InsertRun(CRunBase param_element, ref DataTable param_table,ref Boolean wantOpenBracket)
        {
            // If an opening bracket is required and we start another the_new_run, then we have
            // two opening brackets '(('. That is not supported by the Perfectus core and
            // therefore we convert to '( <<Yes>> == <<Yes>> and ('
            if (wantOpenBracket)
                ConditionIntoTable(alwaysTRUECondition(param_element), true, ref param_table);

            // A the_new_run requires opening parentheses
            wantOpenBracket = true;

            
            // Consider  .. q1=1 ) or ( q2 = 2 and q3 = 3)
            // If we start the the_new_run from q2, then we have to preserve the OR with our 'wantOpenBracket'.
            // Otherwise we would use the concatenation bound to q2, which is AND as default.
            bool overrideConcatenation = true;
            
            // Insert the elements recursivly
            foreach (CRunBase element in ((CRun)param_element)._run_or_condition)
            {
                if (overrideConcatenation)
                {
                    element.concatenation = param_element.concatenation;
                    overrideConcatenation = false;
                }
                EvaluateRunOrCondition(element, ref param_table, ref wantOpenBracket);
            }

            // last condition
            DataRow lastRow = param_table.Rows[param_table.Rows.Count - 1];

            // If the last condition already has a closing bracket and we clsoe the the_new_run 
            // Example: ... Q1=1 ) )
            // then we need to insert a dummy ciondition
            // Example: ... Q1=1 ) and <<Yes>> = <<Yes>> )
            if (String.Compare(lastRow[_colRightParenthesis].ToString(), _strCloseParentheses) == 0)
            {
                // Constructor requires CAtom element, which is meaningless in our case
                ConditionIntoTable(alwaysTRUECondition(param_element), false, ref param_table);
                lastRow = param_table.Rows[param_table.Rows.Count - 1];
            }
            lastRow[_colRightParenthesis] = _strCloseParentheses;
        }
    

        /// <summary>
        /// Handle the_new_run or condition
        /// </summary>
        /// <param name="param_element">A the_new_run or condition</param>
        /// <param name="param_table">query table</param>
        /// <param name="wantOpenBracket">want opening brackets</param>
        private void EvaluateRunOrCondition(CRunBase param_element, ref DataTable param_table,
            ref Boolean wantOpenBracket)
        {
            if (param_element is CRun)
                InsertRun(param_element, ref param_table, ref wantOpenBracket);
            else
            if (param_element is CCondition)
            {
                CCondition cond = param_element as CCondition;
                ConditionIntoTable(cond, wantOpenBracket, ref param_table);
                wantOpenBracket = false;
            }
            else throw new Exception("Unexpected case");
        }
        
            
        /// <summary>
        /// REMOVE LATER
        /// </summary>
        /// <returns></returns>
        public static DataTable CreateQueryTable()
        {
            DataTable outTable;
            outTable = new DataTable("Expression");
            outTable.Columns.Add("Operator1", typeof(OperatorDisplay));
            outTable.Columns.Add("LeftParenthesis", typeof(string));
            outTable.Columns.Add("LeftOperand", typeof(PackageItem));
            outTable.Columns.Add("Operator2", typeof(OperatorDisplay));
            outTable.Columns.Add("RightOperand", typeof(PackageItem));
            outTable.Columns.Add("RightParenthesis", typeof(string));

            return outTable;
        }
    }

    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////
    
    /// <summary>
    /// Helper class to convert a Query into a CQuery for the 
    /// new Query editor
    /// </summary>
    public class Creator
    {
        private Package p;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="param_package">Package</param>
        public Creator(Package param_package)
        {
            p = param_package;
        }

        /// <summary>
        /// Test for 'AND <<Yes>> = <<Yes>>'
        /// </summary>
        /// <param name="param_leftValue">left constant</param>
        /// <param name="param_rightValue">right constant</param>
        /// <param name="param_operator">comparision symbol</param>
        /// <param name="param_concatenation">concatenation symbol</param>
        /// <returns></returns>
        private Boolean IsAlwaysTRUE(
            PackageItem param_leftValue, PackageItem param_rightValue,
            OperatorDisplay param_operator, CConcatenation.concatenationType param_concatenation)
        {
            return (param_concatenation == CConcatenation.concatenationType.AND ||
                    param_concatenation == CConcatenation.concatenationType.UKNOWN)
            && param_leftValue is PackageConstant
            && param_rightValue is PackageConstant
            && ((PackageConstant)param_leftValue).DataType == PerfectusDataType.YesNo
            && ((PackageConstant)param_rightValue).DataType == PerfectusDataType.YesNo
            && ((PackageConstant)param_rightValue).ToString().ToUpper(CultureInfo.InvariantCulture) ==
               ((PackageConstant)param_rightValue).ToString().ToUpper(CultureInfo.InvariantCulture);
        }

        // Read the table expression table and translate 
        // into a CRun for one statement
        private CRun BuildStmt(Query param_query, CRun run)
        {
            QueryExpression expression = param_query.QueryExpression;

            // We create a CRun for every bracket term
            Stack<CRunBase> runs = new Stack<CRunBase>();
            runs.Push(run);

            if (null != param_query.QueryExpression.Table)
                foreach (DataRow row in param_query.QueryExpression.Table.Rows)
                {
                    CRun currentRun = runs.Peek() as CRun;

                    OperatorDisplay c0 = (OperatorDisplay)(row[0] == DBNull.Value ? null : row[0]);
                    String c1 = (String)(row[1] == DBNull.Value ? String.Empty : row[1]);
                    AnswerProvider c2 = (AnswerProvider)row[2];
                    OperatorDisplay c3 = (OperatorDisplay)(row[3] == DBNull.Value ? null : row[3]);
                    AnswerProvider c4 = (AnswerProvider)row[4];
                    String c5 = (String)(row[5] == DBNull.Value ? String.Empty : row[5]);

                    CConcatenation.concatenationType con = CConcatenation.concatenationType.UKNOWN;
                    if (c0 != null && c0.Op == Operator.And)
                        con = CConcatenation.concatenationType.AND;
                    else
                        if (c0 != null && c0.Op == Operator.Or)
                            con = CConcatenation.concatenationType.OR;

                    // The following condition test, if we have an opening bracket without a closing
                    // on the same condition. Then we will create a new bracket term.
                    // We support "( q1 = 1", but we ignore "( q1 = 1 )"
                    if (c1.Trim().Length > 0 && c5.Trim().Length == 0)
                        runs.Push(currentRun = currentRun.AddRun(con) as CRun);

                    // For compatibility we might insert conditions like 
                    // "( TRUE == TRUE AND ("
                    // We ignore these conditions and shrink the term to "(("
                    if (!IsAlwaysTRUE(c2, c4, c3, con))
                    {
                        // Otherwise this is a meaningful condition
                        CCondition condition = currentRun.AddCondition(con) as CCondition;
                        condition.lValue = c2;
                        condition.rValue = c4;
                        condition.Operator = c3;
                    }

                    // Test if an inner bracket term ends...
                    if (c1.Trim().Length == 0 && c5.Trim().Length > 0)
                        currentRun = runs.Pop() as CRun;
                }
            return run;
        }

        /// <summary>
        /// Splitt a given Query into the parts
        /// Clause, TRUE case, FALSE case 
        /// and create the corresponding elements
        /// </summary>
        /// <param name="query"></param>
        /// <param name="stmt"></param>
        private void doRun(Query query, CStatement stmt)
        {
            Boolean want_action_not_placeholder = false;
            // The conditions and bracket terms
            CRun run = BuildStmt(query, stmt.if_clause.Run);

            // Pitfall!
            // 1. user might have created outcome by dragging text from the template into the tree
            //    That outcome has WordML, but is invalid (no questions). We need to create empty
            //    questions to be able to open query.
            // 2. New outcomes shall open as WordML action. Again we have no questions, but this
            //    time no WordML as well. Hence we have to create an action name to make the action valid.
            if (run._run_or_condition.Count == 0)
            {
                want_action_not_placeholder = true;
                run.AddCondition();
            }

            // The TRUE case
            if (query.ActionIfTrue != null)
                if (query.ActionIfTrue.SubQuery != null)
                    // Subqueries are handled like 'independent' queries
                    doRun(query.ActionIfTrue.SubQuery, stmt.AddIfStatement());
                else
                {
                    if (query.ActionIfTrue is HtmlStringBindingAction)
                    {
                        HtmlStringBindingAction htmlAction = query.ActionIfTrue as HtmlStringBindingAction;
                        CHtmlStringBinding action = stmt.AddIfHtmlStringBindingAction();
                        action.Payload = htmlAction.Payload;
                    }
                    else
                        if (query.ActionIfTrue is NavigationAction)
                    {
                        NavigationAction naction = query.ActionIfTrue as NavigationAction;
                        CNavigationPage action = stmt.AddIfNavigationAction();
                        action.Payload = naction.Page;
                        if (null != naction.Page)
                            action.Name = naction.Page.Name;
                    }
                    else
                    {
                        // OutcomeAction serve many kings.
                        // If the WordML is valid, then it is an Outcome
                        // If FetcherId is valid, then this is an external object (e.g. DM5)
                        // If neither is valid, then the statement is empty
                        // (note that Perfectus core supports invalid OutcomeAction's, but
                        // they must not be null)
                        OutcomeAction outcome = query.ActionIfTrue as OutcomeAction;

                        if (want_action_not_placeholder)
                        {
                            CWordText action = stmt.AddIfAction();
                            action.Payload = outcome.OfficeOpenXML;
                        }
                        else
                            if ((outcome.DisplayName == null || outcome.DisplayName.Length == 0) && // used by Word + Integration
                            (outcome.FetcherId == null) && // used by Integration
                            (outcome.OfficeOpenXML == null || outcome.OfficeOpenXML.Length == 0)) // used by Word
                            {
                                stmt.AddIfPlaceholderAction();
                            }
                            else
                                if (outcome.FetcherId != null)
                                {
                                    CIntegration integration = stmt.AddIfIntegration();

                                    integration.DisplayName = outcome.DisplayName;
                                    integration.ClauseRef = outcome.LibraryKey;
                                    integration.LibraryName = outcome.LibraryName;
                                    integration.FetcherId = outcome.FetcherId;
                                    integration.UniqueLibraryKey = outcome.ReportingId;
                                }
                                else
                                {
                                    CWordText action = stmt.AddIfAction();
                                    action.Payload = outcome.OfficeOpenXML;
                                    action.Name = String.Format("{0}", outcome.DisplayName);
                                }
                    }
                }

            if (query.ActionIfFalse != null)
                if (query.ActionIfFalse.SubQuery != null)
                    doRun(query.ActionIfFalse.SubQuery, stmt.AddElseStatement());
                else
                {
                    if (query.ActionIfFalse is HtmlStringBindingAction)
                    {
                        HtmlStringBindingAction htmlAction = query.ActionIfFalse as HtmlStringBindingAction;
                        CHtmlStringBinding action = stmt.AddElseHtmlStringBindingAction();
                        action.Payload = htmlAction.Payload;
                    }
                    else
                    if (query.ActionIfFalse is NavigationAction)
                    {
                        NavigationAction naction = query.ActionIfFalse as NavigationAction;
                        CNavigationPage action = stmt.AddElseNavigationAction();

                        // Do not clutter screen with useless ELSE cases
                        if (naction.Page != null)
                        {
                            action.Name = String.Format("{0}", naction.Page.Name);
                            action.Payload = naction.Page;
                        }
                    }
                    else
                    {

                        OutcomeAction outcome = query.ActionIfFalse as OutcomeAction;

                        if ((outcome.DisplayName == null || outcome.DisplayName.Length == 0) &&
                            (outcome.FetcherId == null) && // used by Integration
                            (outcome.OfficeOpenXML == null || outcome.OfficeOpenXML.Length == 0))
                        {
                            // The TRUE case supports Placeholders, but in the FALSE case
                            // we prefer to avoid printing the ELSE at all (because an ELSE
                            // without an outcome, statement or whatever is meaningless and can be ignored)
                            ;
                        }
                        else
                            if (outcome.FetcherId != null)
                            {
                                CIntegration integration = stmt.AddElseIntegration();

                                integration.DisplayName = outcome.DisplayName;
                                integration.ClauseRef = outcome.LibraryKey;
                                integration.LibraryName = outcome.LibraryName;
                                integration.FetcherId = outcome.FetcherId;
                                integration.UniqueLibraryKey = outcome.ReportingId;
                            }
                            else
                            {
                                CWordText action = stmt.AddElseAction();
                                action.Payload = outcome.OfficeOpenXML;
                                action.Name = String.Format("{0}", outcome.DisplayName);
                            }
                    }
                }
        }

        /// <summary>
        /// Create a CQuery for a given Query
        /// </summary>
        /// <param name="p">Query</param>
        /// <returns>CQuery</returns>
        public CQuery Create(Query param_query)
        {
            CQuery answer = new CQuery();
            CBlock block = answer.AddBlock();

            doRun(param_query, block.Statement);
            return answer;
        }

    }
}

