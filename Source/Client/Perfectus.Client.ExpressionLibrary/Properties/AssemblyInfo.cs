﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

// Assembly specific information
[assembly: AssemblyTitle("ExpressionLibrary")]


#if DEBUG
[assembly: InternalsVisibleTo("Perfectus.Client.ExpressionLibrary.NUnit, PublicKey=002400000480000094000000060200000024000052534131000400000100010005f19daa6644173d5f7e845cbdcb00c195155344eb33e843b72e03725e19f00a1c958aaa5c48cbda574971f4f440af0014349a5f8d0f5dcc814753e1ad01a58a361ba58df2ed2d5697b02f8fbf6d18ed27cf17e9e967e5cb493e77d79ee7605bcd37f4bd8409819db3a058091c2eb55211415631aa2ee1f9f31a86c6147f5dda")]
#endif