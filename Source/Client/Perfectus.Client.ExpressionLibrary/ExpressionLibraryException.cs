/* Copyright by Perfectus Technology Ltd. 2007 - All rights reserved */
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Perfectus.Common;

namespace Perfectus.Client.Studio.ExpressionLibrary
{
    /// <summary>
    /// Exception class used by the Expression Library
    /// </summary>
    public class ExpressionLibraryException : System.Exception
    {
        String _hint;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        public
            ExpressionLibraryException(string message)
            : this(message, String.Empty) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="source">A string that identifies the source</param>
        public
            ExpressionLibraryException(string message, string source)
            : this(message, source, String.Empty) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="source">A string that identifies the source</param>
        /// <param name="hint">A hint for the user</param>
        public
            ExpressionLibraryException(string message, string source, string hint)
            : this(message, source, hint, null) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="innerException">Inner Exception</param>
        public
            ExpressionLibraryException(string message, System.Exception innerException)
            : this(message, String.Empty, innerException) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="source">A string that identifies the source</param>
        /// <param name="innerException">Inner Exception</param>
        public
            ExpressionLibraryException(string message, string source, System.Exception innerException)
            : this(message, source, String.Empty, innerException) { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">The error message</param>
        /// <param name="source">A string that identifies the source</param>
        /// <param name="hint">A hint for the user</param>
        /// <param name="innerException">Inner Exception</param>
        public
            ExpressionLibraryException(string message, string source, string hint, System.Exception innerException)
            : base(message, innerException)
        {
            base.Source = Perfectus.Client.Studio.ExpressionLibrary.Constant._name_;
            if (source.Length > 0)
                base.Source += System.Environment.NewLine + source;
            _hint = hint;
        }

        /// <summary>
        /// Composed error message
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            String str = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorOccurred");
            StringBuilder bld = new StringBuilder(str);
            bld.Append(System.Environment.NewLine + Source);
            bld.AppendLine(System.Environment.NewLine + Message);
            if (_hint.Length>0)
                bld.AppendLine(System.Environment.NewLine + _hint);
            return bld.ToString();
        }
    }
}
