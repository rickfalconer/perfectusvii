using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common;

namespace Perfectus.Client.Studio.ExpressionLibrary
{
    /// <summary>
    /// Enumerand for the element types of a complex query
    /// </summary>
    public enum AtomType
    {
        /// <summary>
        /// Base class - does not meant to be a real element
        /// </summary>
        ATOM,
        /// <summary>
        /// The root element that everything else derives from
        /// </summary>
        QUERY,
        /// <summary>
        /// A <c>Query</c> contains 1..n blocks. 
        /// The block itself contains exactly one <c>Statement</c>
        /// </summary>
        BLOCK,
        /// <summary>
        /// A <c>Statement</c> contains one the_new_run- element and
        /// an optionally up to two <c>Runbase</c> elements (IF and ELSE branch)
        /// </summary>
        STATEMENT,
        /// <summary>
        /// Used by a statement. An <c>Expression Base</c> is not a real object,
        /// but is the base for either a <c>Statement</c>, or an <c>Action</c>
        /// </summary>
        EXPRESSIONBASE,
        /// <summary>
        /// Base object of either an <c>If Clause</c>, <c>Else Clause</c> or <c>Action</c>
        /// This object is abstract
        /// </summary>
        CLAUSE,
        /// <summary>
        /// <c>If Clause</c> of a <c>Statement</c>
        /// </summary>
        IFCLAUSE,
        /// <summary>
        /// <c>Else Clause</c> of a <c>Statement</c>
        /// </summary>
        ELSEEXPRESSION,
        /// <summary>
        /// A <c>Run</c> represents a collection of <c>Conditions</c> that
        /// is surrounded by brackets.
        /// The following example contains two condition in one the_new_run
        /// <code>
        /// (Q1 = 100 and Q2 = 'NZ')
        /// </code>
        /// </summary>
        RUN,
        /// <summary>
        /// <c>Run Base</c> is the base object of either a <c>Run</c> , or a <c>Condition</c>
        /// </summary>
        RUNBASE,
        /// <summary>
        /// A <c>Condition</c> is a triple of (left-value,operand,r-value)
        /// </summary>
        CONDITION,
        /// <summary>
        /// A <c>Concatenation</c> links elements of type <c>Run Base</c>
        /// The left-value for the 'or' is a Run, whereas the right-value is a condition
        /// <code>
        /// (Q1 = 100 and Q2 = 'NZ') or Q3 = 200
        /// </code>
        /// </summary>
        CONCATENATION,
        /// <summary>
        /// The little thingy between values
        /// </summary>
        OPERATOR,
        /// <summary>
        /// Base value that hold typically a Perfectus.PackageItem object
        /// </summary>
        VALUE,
        /// <summary>
        /// Identifies the left-value element
        /// </summary>
        LVALUE,
        /// <summary>
        /// Identifies the rights-value element
        /// </summary>
        RVALUE,
        /// <summary>
        /// Intermediate object that is evaluated to NIL, or replaced by a statement (if/else) or action
        /// </summary>
        PLACEHOLDER,
        /// <summary>
        /// Link to an Outcome text, page navigation etc.
        /// </summary>
        WORDTEXT,
        /// <summary>
        /// Integrated object (e.g. DM5)
        /// </summary>
        INTEGRATION,
        /// <summary>
        /// Interview page
        /// </summary>
        NAVIGATIONPAGE,
        /// <summary>
        /// Html editor text from string binding
        /// </summary>
        HTMLSTRINGBINDING
    }

    /// <summary>
    /// Helper class to map classes to AtomType etc.
    /// </summary>
    public static class Info
    {
        /// <summary>
        /// Return the Type of the element
        /// </summary>
        /// <param name="param_typeid"></param>
        /// <returns>identifier</returns>
        public static Type GetElementType(AtomType param_typeid)
        {
            switch (param_typeid)
            {
                case AtomType.ATOM:
                    return typeof(CAtom);
                case AtomType.QUERY:
                    return typeof(CQuery);
                case AtomType.BLOCK:
                    return typeof(CBlock);
                case AtomType.STATEMENT:
                    return typeof(CStatement);
                case AtomType.EXPRESSIONBASE:
                    return typeof(CExpressionBase);
                case AtomType.CLAUSE:
                    return typeof(CClause);
                case AtomType.ELSEEXPRESSION:
                    return typeof(CElseExpression);
                case AtomType.RUN:
                    return typeof(CRun);
                case AtomType.RUNBASE:
                    return typeof(CRunBase);
                case AtomType.CONDITION:
                    return typeof(CCondition);
                case AtomType.CONCATENATION:
                    return typeof(CConcatenation);
                case AtomType.OPERATOR:
                    return typeof(COperand);
                case AtomType.VALUE:
                    return typeof(CValue);
                case AtomType.LVALUE:
                    return typeof(CLValue);
                case AtomType.RVALUE:
                    return typeof(CRValue);
                case AtomType.WORDTEXT:
                    return typeof(CWordText);
                case AtomType.INTEGRATION:
                    return typeof(CIntegration);
                case AtomType.NAVIGATIONPAGE:
                    return typeof(CNavigationPage);
                case AtomType.HTMLSTRINGBINDING:
                    return typeof(CHtmlStringBinding);
                default:
                    throw new ExpressionLibraryException("Missing case for " + param_typeid.ToString(), "Info::GetElementType", "Bug");
            }
        }

        /// <summary>
        /// Return the type by parsing the string
        /// </summary>
        /// <param name="param_string">type as string</param>
        /// <returns>type</returns>
        public static AtomType GetElementType(String param_string)
        {
            try
            {
                return (AtomType)Enum.Parse(typeof(AtomType), param_string);
            }
            catch (Exception ex)
            {
                throw new ExpressionLibraryException("Logical error", "Info::GetElementType", ex);
            }
        }

        /// <summary>
        /// Translate the element type to a localized name
        /// </summary>
        /// <param name="param_typeid"></param>
        /// <returns>Element type</returns>
        public static String GetLocalizedName(AtomType param_typeid)
        {
            switch (param_typeid)
            {
                case AtomType.ATOM:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_ATOM");
                case AtomType.QUERY:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_QUERY");
                case AtomType.BLOCK:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_BLOCK");
                case AtomType.STATEMENT:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_STATEMENT");
                case AtomType.EXPRESSIONBASE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_EXPRESSIONBASE");
                case AtomType.CLAUSE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_CLAUSE");
                case AtomType.IFCLAUSE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_IFCLAUSE");
                case AtomType.ELSEEXPRESSION:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_ELSECLAUSE");
                case AtomType.RUN:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_RUN");
                case AtomType.RUNBASE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_RUNBASE");
                case AtomType.CONDITION:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_CONDITION");
                case AtomType.CONCATENATION:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_CONCATENATION");
                case AtomType.OPERATOR:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_OPERATOR");
                case AtomType.VALUE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_VALUE");
                case AtomType.LVALUE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_LVALUE");
                case AtomType.RVALUE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_RVALUE");
                case AtomType.WORDTEXT:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_ACTION");
                case AtomType.INTEGRATION:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_INTEGRATION");
                case AtomType.PLACEHOLDER:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_PLACEHOLDER");
                case AtomType.NAVIGATIONPAGE:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_NAVIGATIONPAGE");
                case AtomType.HTMLSTRINGBINDING:
                    return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("AtomType_HTMLSTRINGBINDING");
                default:
                    throw new ExpressionLibraryException("Missing case for " + param_typeid.ToString(), "Info::GetElementType", "Bug");

            }
        }
    }

    /// <summary>
    /// Delegate used by the terms of a query to invoke the pretty-printer.
    /// </summary>
    /// <param name="TermText">The (partial-)text that the term defines</param>
    /// <param name="Type"></param>
    /// <param name="Id">The Guid of this term</param>
    /// <param name="ParentId">The Guid of the parent object</param>
    public
    delegate void DisplayYourselfCallback(String TermText, AtomType Type, Guid Id, Guid ParentId, Boolean valid);

    //-------------------------------------------------------
    static public class ExpressionHelper
    {
        private static Dictionary<Guid, CAtom> atomList = new Dictionary<Guid, CAtom>(100);

        static public void Reset() 
        {
            atomList.Clear();
        }
        static internal int Count { get { return atomList.Count; } }
        
        static internal void Register(CAtom param_atom)
        {
            atomList.Add(param_atom.Id, param_atom);
        }
        static internal void UnRegister(CAtom param_atom)
        {
            atomList.Remove(param_atom.Id);
        }
        static public CAtom Find(Guid param_guid)
        {
            if (atomList.ContainsKey(param_guid))
                return atomList[param_guid];
            else
                return null;
        }
    }
    /// <summary>
    /// Global constants and strings like the version number
    /// of this library
    /// </summary>
    static internal class Constant
    {
        internal const String _version_ = "v0.1 alpha";
        internal const String _name_ = "Perfectus Expression Library";
        internal const String _copyright_ = "Perfectus Solutions - All rights reserved 2007";
    }
}
