using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace ExpressionLibrary
{
    public interface IValidation
    {
        Boolean HasChildren();
        IValidation[] Children();
        Boolean IsValid(ref String param_message);
        Boolean IsObjectValid(ref string param_message);
    }

    public enum dataType { UNKNOWN, CHAR, NUMBER, DATE };

    public class CAtom : IValidation
    {
        private Guid _Id;
        public Guid Id
        {
            get { return _Id; }
        }
        Guid _parentId;
        public Guid parentId
        {
            get { return _parentId; }
            set
            {
                _parentId = value;
            }
        }
        public
        CAtom()
        {
            _Id = Guid.NewGuid();
            _parentId = Guid.Empty;
        }
        public
        CAtom(CAtom param_parent)
            : this(param_parent.Id)
        { }
        private
        CAtom(Guid param_parentId)
            : this()
        {
            _parentId = param_parentId;
        }
        public virtual bool validate()
        {
            return false;
        }
        public override String ToString()
        {
            return String.Empty;
        }
        public String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//TYPE   {0}", GetType().ToString()));
            strBld.AppendLine(String.Format("//ID     {0}", Id.ToString()));
            strBld.AppendLine(String.Format("//PARENT {0}", parentId.ToString()));
            return strBld.ToString();
        }

        #region IValidation Members

        public virtual Boolean HasChildren()
        {
            return false;
        }

        public virtual IValidation[] Children()
        {
            return null;
        }

        public virtual bool IsObjectValid(ref string param_message)
        {
            Boolean success = Id != null && Id != Guid.Empty && parentId != null;
            if (!success)
                param_message += System.Environment.NewLine + "Guid's invalid";
            return success;
        }

        public virtual bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (IValidation child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && this.IsObjectValid(ref param_message);
            return success;
        }
        #endregion
    }
    public class CQuery : CAtom
    {
        internal List<CBlock> _blocks;
        public
            CQuery()
        {
            _blocks = new List<CBlock>();
        }
        public CBlock addBlock(String param_name)
        {
            CBlock block = new CBlock(param_name, this);
            _blocks.Add(block);
            return block;
        }
        public CBlock addBlock()
        {
            return addBlock(String.Empty);
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            foreach (CBlock block in _blocks)
            {
                strBld.AppendLine(block.ToString());
            }
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine("//Query");
            strBld.AppendLine(base.DiagnosticString());
            foreach (CBlock block in _blocks)
            {
                strBld.AppendLine(block.DiagnosticString());
            }
            return strBld.ToString();
        }
    }
    public class CBlock : CAtom
    {
        private String _name;
        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private CStatement _statement;
        public CStatement Statement
        { get { return _statement; } }

        public
        CBlock(String param_name, CAtom param_parent)
            : base(param_parent)
        {
            Name = param_name;
            _statement = new CStatement(this);
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("Begin /* {0} */", Name));
            strBld.AppendLine(_statement.ToString());
            strBld.AppendLine(String.Format("End /* {0} */", Name));
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//Block {0}", Name));
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(_statement.DiagnosticString());
            return strBld.ToString();
        }
    }
    public class CStatement : CExpressionBase
    {
        private CClause _if_clause;
        public CClause if_clause
        {
            get { if (_if_clause == null) _if_clause = new CIfClause(this); return _if_clause; }
        }
        private CElseClause _else_clause;
        public CClause else_clause
        {
            get { if (_else_clause == null) _else_clause = new CElseClause(this); return _else_clause; }
        }

        public CStatement(CAtom param_parent)
            : base(param_parent)
        {
            _if_clause = new CIfClause(this);
            _else_clause = new CElseClause(this);
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(_if_clause.ToString());
            strBld.AppendLine(_else_clause.ToString());
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//Statement"));
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(_if_clause.DiagnosticString());
            strBld.AppendLine(_else_clause.DiagnosticString());
            return strBld.ToString();
        }
    }
    public abstract class CClause : CAtom, IValidation
    {
        private CRun _run;
        public CRun Run { get { return _run; } }

        private CExpressionBase _run_or_action = null;
        protected CClause(CAtom param_parent)
            : base(param_parent)
        {
            _run = new CRun(this);
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(_run.ToString());
            if (null != _run_or_action)
                strBld.AppendLine(_run_or_action.ToString());
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//Clause"));
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(_run.DiagnosticString());
            if (null != _run_or_action)
                strBld.AppendLine(_run_or_action.DiagnosticString());
            return strBld.ToString();
        }
        #region IValidation Members

        public override IValidation[] Children()
        {
            return (new IValidation[] { _run });
        }

        public override Boolean HasChildren()
        {
            return true;
        }

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }

        #endregion

    }
    public class CIfClause : CClause, IValidation
    {
        public CIfClause(CAtom param_parent)
            : base(param_parent) { }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(" If ");
            strBld.AppendLine(base.ToString());
            strBld.AppendLine(" Then ");
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//IfClause"));
            strBld.AppendLine(base.DiagnosticString());
            return strBld.ToString();
        }
        #region IValidation Members

        /*
        public override bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (IValidation child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && base.IsValid(ref param_message);
            return success;
        }
         * */

        #endregion

    }
    public class CElseClause : CClause, IValidation
    {
        public CElseClause(CAtom param_parent)
            : base(param_parent) { }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(" Else ");
            strBld.AppendLine(base.ToString());
            strBld.AppendLine(" Then ");
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine(String.Format("//ElseClause"));
            strBld.AppendLine(base.DiagnosticString());
            return strBld.ToString();
        }
    }
    public class CExpressionBase : CAtom
    {
        public
        CExpressionBase(CAtom param_parent)
            : base(param_parent) { }
    }
    public class CAction : CExpressionBase
    {
        public
        CAction(CAtom param_parent)
            : base(param_parent) { }
    }
    public class CConcatenation : CAtom, IValidation
    {
        public
        enum concatenationType { UKNOWN, AND, OR }

        private concatenationType _concatenationType;

        public concatenationType Con { get { return _concatenationType; } set { _concatenationType = value; } }
        public
        CConcatenation(CAtom param_parent)
            : base(param_parent)
        { }

        private String englishString()
        {
            switch (_concatenationType)
            {
                case concatenationType.AND:
                    return "and";
                case concatenationType.OR:
                    return "or";
                case concatenationType.UKNOWN:
                    return ".";
            }
            throw new Exception("unexpected case");
        }


        public override String ToString()
        {
            return " " + englishString();
        }

        #region IValidation Members

        public override bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (IValidation child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && base.IsValid(ref param_message);
            success = success && _concatenationType != CConcatenation.concatenationType.UKNOWN;
            return success;
        }

        #endregion

    }

    public abstract class CRunBase : CAtom, IValidation
    {
        protected CAtom _parent;

        CConcatenation _concatenation;
        public CConcatenation.concatenationType concatenation
        {
            get { return _concatenation.Con; }
            set { _concatenation.Con = value; }
        }

        public
        CRunBase(CAtom param_parent)
            : base(param_parent)
        {
            _parent = param_parent;
            _concatenation = new CConcatenation(param_parent);
            _concatenation.Con = CConcatenation.concatenationType.UKNOWN;
        }

        public void SetParent(CAtom param_parent)
        {
            parentId = param_parent.Id;
            _parent = param_parent;
        }

        public abstract void Remove();

        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            if (_parent is CRun)
            {
                int index = ((CRun)_parent).IndexOf(this);

                if (index > 0)
                    strBld.Append(_concatenation.ToString());

            }
            return strBld.ToString();
        }

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
                success = _concatenation.IsObjectValid(ref param_message);
            return success;
        }

        #endregion
    }

    public class CRun : CRunBase, IValidation
    {
        internal List<CRunBase> _run_or_condition;

        public
        CRun(CAtom param_parent)
            : base(param_parent)
        {
            _run_or_condition = new List<CRunBase>();
        }

        public int IndexOf(CRunBase param_run)
        {
            return this._run_or_condition.IndexOf(param_run);
        }


        internal void Remove(CRunBase param_removal)
        {
            _run_or_condition.Remove(param_removal);
        }
        public override void Remove()
        {
            if (_parent is CRun)
            {
                while (_run_or_condition.Count > 0)
                    _run_or_condition[0].Remove();
                ((CRun)_parent).Remove(this);
            }
        }

        public CRunBase AddRun()
        {
            return AddRun(CConcatenation.concatenationType.AND);
        }
        public CRunBase AddRun(CConcatenation.concatenationType param_concatenation)
        {
            CRunBase run = new CRun(this);
            _run_or_condition.Add(run);
            run.concatenation = param_concatenation;
            return run;
        }

        public virtual CRunBase InsertRun(CRunBase param_run, CConcatenation.concatenationType param_concatenation)
        {
            CRun run = new CRun(this);
            run.concatenation = param_concatenation;

            int index = this._run_or_condition.IndexOf(param_run);
            if (index == -1)
                throw new Exception("not a child");
            run.AddRun(param_run);
            this._run_or_condition[index] = run;

            return run;
        }
        public virtual CRunBase InsertRun(CRunBase param_run)
        {
            return InsertRun(param_run, param_run.concatenation);
        }
        public CRunBase AddRun(CRunBase param_run)
        {
            ((CRunBase)param_run).SetParent(this);
            _run_or_condition.Add(param_run);
            return param_run;
        }
        public virtual CRunBase AddCondition()
        {
            return AddCondition(CConcatenation.concatenationType.AND);
        }
        public virtual CRunBase AddCondition(CConcatenation.concatenationType param_conType)
        {
            CCondition cond = new CCondition(this);
            cond.concatenation = param_conType;
            _run_or_condition.Add(cond);
            return cond;
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(base.ToString());
            strBld.Append("(");
            foreach (CRunBase run in this._run_or_condition)
                strBld.Append(run.ToString());
            strBld.Append(")");
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine("//Run");
            strBld.AppendLine(base.DiagnosticString());
            foreach (CRunBase run in this._run_or_condition)
            {
                strBld.AppendLine(run.DiagnosticString());
            }
            return strBld.ToString();
        }
        #region IValidation Members

        public override IValidation[] Children()
        {
            IValidation[] children = (IValidation[])_run_or_condition.ToArray();
            return (children);
        }

        public override Boolean HasChildren()
        {
            return _run_or_condition.Count > 0;
        }
        /*
        public override bool IsValid(ref string param_message)
        {
            Boolean success = true;
            if (HasChildren())
                foreach (IValidation child in Children())
                    success = success && child.IsValid(ref param_message);
            success = success && base.IsValid(ref param_message);
            return success;
        }
        */
        #endregion
    }

    public class CCondition : CRunBase, IValidation
    {
        private CLValue _lValue;
        private CRValue _rValue;
        private COperand _operand;

        public String lValueName
        {
            get { return _lValue.Name; }
            set { _lValue.Name = value; }
        }
        public String rValueName
        {
            get { return _rValue.Name; }
            set { _rValue.Name = value; }
        }
        public COperand.operandType operand
        {
            get { return _operand.operand; }
            set { _operand.operand = value; }
        }


        public
        CCondition(CAtom param_parent)
            : base(param_parent)
        {
            _parent = param_parent;
            _lValue = new CLValue(this);
            _rValue = new CRValue(this);
            _operand = new COperand(this);
        }

        public virtual CRunBase AddCondition(CConcatenation.concatenationType param_concatenation)
        {
            return ((CRun)_parent).AddCondition(param_concatenation);
        }
        public virtual CRunBase AddRun(CConcatenation.concatenationType param_concatenation)
        {
            return ((CRun)_parent).InsertRun(this, param_concatenation);
        }
        public virtual CRunBase AddRun()
        {
            return ((CRun)_parent).InsertRun(this);
        }

        public override void Remove()
        {
            ((CRun)_parent).Remove(this);
        }

        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(base.ToString());
            strBld.Append(String.Format(" {0} {1} {2} ",
                _lValue.ToString(),
                _operand.ToString(),
                _rValue.ToString()));
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine("//Condition");
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(_lValue.DiagnosticString());
            strBld.AppendLine(_rValue.DiagnosticString());
            strBld.AppendLine(_operand.DiagnosticString());
            return strBld.ToString();
        }

        #region IValidation Members

        public override IValidation[] Children()
        {
            return (new IValidation[] { _lValue, _rValue, _operand });
        }
        public override Boolean HasChildren()
        {
            return true;
        }
        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }
        #endregion
    }
    public abstract class CValue : CAtom, IValidation
    {
        const String _empty_name = "<<Empty>>";
        protected String _name;
        protected dataType _dataType;

        protected String Name
        {
            get { return _name; }
            set { if (value == null || value.Length == 0) _name = _empty_name; else _name = value; }
        }

        private void Init()
        {
            Name = String.Empty;
            _dataType = dataType.UNKNOWN;
        }
        public
        CValue(CAtom param_parent)
            : base(param_parent)
        {
            Init();
        }
        public
        CValue()
        {
            Init();
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", _name));
            return strBld.ToString();
        }

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
            {
                success = Name.Length > 0 && _empty_name != Name && dataType.UNKNOWN != this._dataType;
                if (!success)
                    param_message += System.Environment.NewLine + "Value name not set";
            }
            return success;
        }

        #endregion
    }
    public class CRValue : CValue, IValidation
    {
        Object _value;

        public new String Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        public
        CRValue(CAtom param_parent)
            : base(param_parent)
        {
            _value = null;
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", base.ToString()));
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine("//rValue");
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(String.Format(" {0} := {1} ", _name, _value.ToString(), _dataType.ToString()));
            return strBld.ToString();

        }

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            if (success)
            {
                success = _value != null;
                if (!success)
                    param_message += System.Environment.NewLine + "RValue not set";
            }
            return success;
        }

        #endregion
    }
    public class CLValue : CValue, IValidation
    {
        public new String Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        public
        CLValue(CAtom param_parent)
            : base(param_parent)
        {
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", base.ToString()));
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.AppendLine("//lValue");
            strBld.AppendLine(base.DiagnosticString());
            strBld.AppendLine(String.Format(" {0} := {1} ", _name, _dataType.ToString()));
            return strBld.ToString();
        }

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);
            return success;
        }

        #endregion
    }
    public class COperand : CAtom, IValidation
    {
        private operandType _operandType;

        public operandType operand
        {
            get { return _operandType; }
            set { _operandType = value; }
        }

        public
        enum operandType { UKNOWN, EQUAL, NOTEQUAL, GREATEREQUAL, GREATER, LESSEQUAL, LESS }

        private String arithmeticString()
        {
            switch (_operandType)
            {
                case operandType.UKNOWN:
                    return "?";
                case operandType.EQUAL:
                    return "=";
                case operandType.NOTEQUAL:
                    return "<>";
                case operandType.GREATEREQUAL:
                    return ">=";
                case operandType.GREATER:
                    return ">";
                case operandType.LESS:
                    return "<";
                case operandType.LESSEQUAL:
                    return "<=";
            }
            throw new Exception("unexpected case");
        }
        private String emglishString()
        {
            switch (_operandType)
            {
                case operandType.UKNOWN:
                    return "unnown";
                case operandType.EQUAL:
                    return "equal";
                case operandType.NOTEQUAL:
                    return "not equal";
                case operandType.GREATEREQUAL:
                    return "greater or equal";
                case operandType.GREATER:
                    return "greater";
                case operandType.LESS:
                    return "less";
                case operandType.LESSEQUAL:
                    return "less or equal";
            }
            throw new Exception("unexpected case");
        }

        public
        COperand(CAtom param_parent)
            : base(param_parent)
        {
            _operandType = operandType.UKNOWN;
        }
        public override String ToString()
        {
            StringBuilder strBld = new StringBuilder();
            strBld.Append(String.Format("{0}", arithmeticString()));
            return strBld.ToString();
        }
        public new String DiagnosticString()
        {
            return String.Format(" {0} ", arithmeticString());
        }

        #region IValidation Members

        public override bool IsObjectValid(ref string param_message)
        {
            Boolean success = base.IsObjectValid(ref param_message);

            success = _operandType != operandType.UKNOWN;
            if (!success)
                param_message += System.Environment.NewLine + "Operand is not set";
            return success;
        }

        #endregion
    }



    public class Converter
    {
        Package p;

        public Converter(Package param_p) { p = param_p; }

        public void Convert(CQuery param_query)
        {
            QueryExpression qExp = new QueryExpression();

            DataTable dt = CreateQueryTable();

            CClause clause = param_query._blocks[0].Statement.if_clause;

            foreach (CRunBase run in clause.Run._run_or_condition)
            {
                if (!(run is CCondition))
                    continue;
                CCondition cond = run as CCondition;

                Question q1 = new Question(cond.lValueName);
                Question q2 = new Question(cond.rValueName);
                Operator op = Operator.Eq;
                ExpressionNode expressionNode = new ExpressionNode(null, q1, op, q2);

                object initialExpressionOperator = DBNull.Value;
                if (cond.concatenation == CConcatenation.concatenationType.AND)
                    initialExpressionOperator = Operator.And;
                else
                    if (cond.concatenation == CConcatenation.concatenationType.OR)
                        initialExpressionOperator = Operator.Or;

                //private void CreateDataRow(ExpressionNode expressionNode, object initialExpressionOperator)
                {
                    object[] dr = new object[6];
                    // Define the datarow and add to the internal table
                    if (initialExpressionOperator == DBNull.Value)
                        dr[0] = (DBNull)initialExpressionOperator;
                    else
                        dr[0] = new OperatorDisplay((Operator)initialExpressionOperator); // If more than one expression node - joins the two expressions together

                    dr[1] = "(";
                    dr[2] = expressionNode.Operand1;				// Package Item
                    dr[3] = new OperatorDisplay(expressionNode.ExpressionOperator);
                    dr[4] = expressionNode.Operand2;				// Package Item
                    dr[5] = ")";

                    dt.Rows.Add(dr);
                }
            }

            qExp.SaveDataTable(dt);

            String message;
            if (!qExp.ValidateFromTable(dt, out message))
                throw new Exception(message);

            Outcome outcome = p.Outcomes[0];

            Query q = outcome.Definition;
            q.QueryExpression = qExp;
            q.Evaluate();
        }

        public static DataTable CreateQueryTable()
        {
            DataTable outTable;
            outTable = new DataTable("Expression");
            outTable.Columns.Add("Operator1", typeof(OperatorDisplay));
            outTable.Columns.Add("LeftParenthesis", typeof(string));
            outTable.Columns.Add("LeftOperand", typeof(PackageItem));
            outTable.Columns.Add("Operator2", typeof(OperatorDisplay));
            outTable.Columns.Add("RightOperand", typeof(PackageItem));
            outTable.Columns.Add("RightParenthesis", typeof(string));

            return outTable;
        }
    }
    public class Creator
    {
        private Package p;
        public Creator(Package param_package)
        {
            p = param_package;
        }

        public CQuery Create(PackageItem p)
        {
            CQuery answer = null;
            if (!(p is Outcome))
                return null;

            Outcome outcome = p as Outcome;
            answer = new CQuery();
            CBlock block = answer.addBlock(outcome.Name);

            
            QueryExpression expression = outcome.Definition.QueryExpression;

            CRun primaryRun = block.Statement.if_clause.Run;

            while (expression != null)
            {
                DataTable dt = expression.Table;

                Stack<CRunBase> runs = new Stack<CRunBase>();
                runs.Push(primaryRun);

                foreach (DataRow row in dt.Rows)
                {
                    CRun currentRun = runs.Peek() as CRun;


                    OperatorDisplay c0 = (OperatorDisplay)(row[0] == DBNull.Value ? null : row[0]);
                    String c1 = (String)(row[1] == DBNull.Value ? String.Empty : row[1]);
                    PackageItem c2 = (PackageItem)row[2];
                    OperatorDisplay c3 = (OperatorDisplay)(row[3] == DBNull.Value ? null : row[3]);
                    PackageItem c4 = (PackageItem)row[4];
                    String c5 = (String)(row[5] == DBNull.Value ? String.Empty : row[5]);

                    CConcatenation.concatenationType con = CConcatenation.concatenationType.UKNOWN;
                    if (c0 != null && c0.Op == Operator.And)
                        con = CConcatenation.concatenationType.AND;
                    else
                        if (c0 != null && c0.Op == Operator.Or)
                            con = CConcatenation.concatenationType.OR;

                    if (c1.Length > 0 && c5.Length == 0)
                        runs.Push(currentRun = currentRun.AddRun(con) as CRun);

                    CCondition condition = currentRun.AddCondition(con) as CCondition;
                    condition.lValueName = c2.Name;
                    condition.rValueName = c4.Name;
                    condition.operand = COperand.operandType.EQUAL;

                    if (c1.Length == 0 && c5.Length > 0)
                        currentRun = runs.Pop() as CRun;

                }

                expression = null;

                ActionBase a = outcome.Definition.ActionIfTrue;
                ActionBase b = outcome.Definition.ActionIfFalse;

                if (b.SubQuery != null)
                {
                    primaryRun = block.Statement.else_clause.Run;
                    expression = b.SubQuery.QueryExpression;
                }

            }

            return answer;
        }
    }

}
