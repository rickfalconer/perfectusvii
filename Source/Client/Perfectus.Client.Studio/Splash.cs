using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace Perfectus.Client.Studio
{
	/// <summary>
	/// Summary description for Splash.
	/// </summary>
	public class Splash : Form
	{
		private PictureBox pictureBox2;
		private Label lblCopyright;
		private Label lblVersion;
		private Label lblPatents;
		private IContainer components = null;

		public Splash()
		{
			//
			// Required for Windows Form Designer support
			//

			InitializeComponent();

            //transparent labels
            var pos = this.PointToScreen(lblCopyright.Location);
            pos = pictureBox2.PointToClient(pos);
            lblCopyright.Parent = pictureBox2;
            lblCopyright.Location = pos;
            lblCopyright.BackColor = Color.Transparent;
            pos = this.PointToScreen(lblPatents.Location);
            pos = pictureBox2.PointToClient(pos);
            lblPatents.Parent = pictureBox2;
            lblPatents.Location = pos;
            lblPatents.BackColor = Color.Transparent;
            pos = this.PointToScreen(lblVersion.Location);
            pos = pictureBox2.PointToClient(pos);
            lblVersion.Parent = pictureBox2;
            lblVersion.Location = pos;
            lblVersion.BackColor = Color.Transparent;
            
			lblVersion.Text = Common.About.AssemblyVersion;
			lblPatents.Text = Common.About.Trademark;
			lblCopyright.Text = Common.About.Copyright;

#if      DEBUG
			lblVersion.Text += ".debug";
#endif
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Splash));
            this.lblPatents = new System.Windows.Forms.Label();
            this.lblCopyright = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPatents
            // 
            this.lblPatents.BackColor = System.Drawing.Color.White;
            this.lblPatents.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(81)))), ((int)(((byte)(81)))));
            resources.ApplyResources(this.lblPatents, "lblPatents");
            this.lblPatents.Name = "lblPatents";
            // 
            // lblCopyright
            // 
            this.lblCopyright.BackColor = System.Drawing.Color.White;
            this.lblCopyright.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(81)))), ((int)(((byte)(81)))));
            resources.ApplyResources(this.lblCopyright, "lblCopyright");
            this.lblCopyright.Name = "lblCopyright";
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.White;
            this.lblVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(81)))), ((int)(((byte)(81)))), ((int)(((byte)(81)))));
            resources.ApplyResources(this.lblVersion, "lblVersion");
            this.lblVersion.Name = "lblVersion";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Image = global::Perfectus.Client.Studio.Properties.Resources.Splash;
            this.pictureBox2.InitialImage = global::Perfectus.Client.Studio.Properties.Resources.Splash;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            // 
            // Splash
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblCopyright);
            this.Controls.Add(this.lblPatents);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Splash";
            this.ShowInTaskbar = false;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
	}
}