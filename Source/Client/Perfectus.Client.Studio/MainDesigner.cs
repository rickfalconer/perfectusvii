using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DataDynamics.SharpUI.Panels;
using DataDynamics.SharpUI.Toolbars;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.PluginSystem;
using Perfectus.Client.Studio.UI.Controls;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Client.Studio.UI.Dialogs;
using Perfectus.Client.Studio.UI.PackageExplorer;
using Perfectus.Client.Studio.UI.SharedLibrary;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Client.Studio.UI.Tasks;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Perfectus.OdmaSupport;
using Perfectus.Client.Studio.Versioning;
using System.Xml.Serialization;

namespace Perfectus.Client.Studio
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainDesigner : Form
	{
        MdiChildClass uiToolbarManager_ = new MdiChildClass();
		private StringBuilder odmaTraceBuilder = new StringBuilder();
		private Int32 odmaHandle = int.MinValue;
		private string odmaDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);

        // FB606: To prevent other users from changing or deleting the package file (.ip), the IPManager needs to keep a handle on the
        // file and only close it when no longer needed. Hence we now have a global file stream object which has read/write control
        //  on the file. Other applications can still get read access on the file.
        private FileStream ipFileStream = null;

        //The collection of currently active plugins
        private static Collection<IStudioPlugin> m_plugins = null;
		private static DisplayTypeExtensionInfoCollection m_displayTypeExtensions = null;
		private static FormatStringExtensionInfoCollection m_formatStringExtensions = null;
		private IOpenSaveHandler pluginForSave = null;
		private string pluginTitleText = string.Empty;

		private MRU mruList;
		private UiToolbarManager uiToolbarManager1;
		private Dock dockTop1;
		private Dock dockRight1;
		private Dock dockBottom1;
		private Dock dockLeft1;
		private Pane pane1;
        private ToolWindow twPackageExplorer;
		private ToolWindow twProperties;
		private PropertyGrid propertyGrid1;
		private Command cmNew;
		private Command cmOpen;
		private Command cmSave;
		private Command cmSaveAs;
		private Toolbar tbStandard;
		private ButtonTool btNew;
		private ButtonTool btSave;
		private ButtonTool btSaveAs;
		private PopupMenu popupMenu1;
		private ButtonTool btOpen;
		private Menubar mbMain;
		private PopupTool ptMainFile;
		private PopupMenu popupMenu2;
		private ButtonTool btFileOpen;
		private ButtonTool btFileNew;
		private ButtonTool btFileSave;
		private ButtonTool btFileSaveAs;
		private ButtonTool btExit;
		private Command cmExit;
		private PopupTool ptMainWindow;
		private PopupMenu popupMenu3;
		private MdiListTool mdiListTool1;
		private ExplorerControl explorerControl;
		private Package package;
        private PackageVersionInfo packageVersionInfo;
		private UiTaskBar uiTaskBar1;
		private BulkAdd bulkAdd1;
		private PopupTool ptHelp;
		private Command cmHelpAbout;
		private PopupMenu popupMenu4;
		private ButtonTool btHelpAbout;
		private ImageList imageList1;
		private IContainer components;
		private SaveFileDialog saveFileDialog1;
		private OpenFileDialog openFileDialog1;
		private ImportItems importItems1;
		private Command cmShowLoadQuestionsTaskBar;
		private PopupTool ptTools;
		private PopupMenu popupMenu5;
		private ButtonTool btLoadQuestions;
		private ToolWindow twLoadQuestions;
		private Command cmPublish;
		private ButtonTool btPublish;
		private Command cmCollect;
		private Timer splashTimer;
		private Splash splashForm;
		private string formTitleFormat;
		private Command cmInterviewDiagram;
		private ButtonTool btInterviewDiagram;
		private DataDynamics.SharpUI.Toolbars.ButtonTool btPublish2;
/*
		private DataDynamics.SharpUI.Toolbars.Command cmImportFromXdp;
		private DataDynamics.SharpUI.Toolbars.Command cmExportToXdp;
		private DataDynamics.SharpUI.Toolbars.ButtonTool btImportXdp;
		private DataDynamics.SharpUI.Toolbars.ButtonTool btExportXdp;
		private DataDynamics.SharpUI.Toolbars.ButtonTool btImportXdp2;
		private DataDynamics.SharpUI.Toolbars.ButtonTool btExportXdp2;
		private System.Windows.Forms.OpenFileDialog openFileDialog2;
*/
		private Command cmHelp;
		private ButtonTool btHelpHelp;
		private Command cmOdmaStatus;
		private ButtonTool btOdmaStatus;
		private Hashtable mruMenuItems;
		//private bool reportingEnabled = false;//Convert.ToBoolean(ConfigurationSettings.AppSettings["ReportingEnabled"]);

		// Shared Library variables
		private ToolWindow twSharedLibrary;
		private SharedLibraryControl sharedLibraryControl;
		private ButtonTool btSharedLibraryControl;
		private Command cmShowSharedLibraryControlTaskBar;

        private ButtonTool btClauseReport;
        private TabControlWithDelete tabForms;
        private Pane pane4;
        private Pane pane3;
        private Pane pane2;
        private ButtonTool btFixPackage;
        private Command cmFixPackage;
        private Command cmConfiguration;
        private ButtonTool btCustomize;
        private Command cmShowClauseReport;
		
		internal delegate void PreSaveHandler();
		internal static event PreSaveHandler PreSave;
		internal delegate void ActiveDesignerHandler(DesignerBase item);
		internal static event ActiveDesignerHandler ActiveDesignerChanged;

        private String fileToOpen;

		public MainDesigner(string paramfileToOpen) : this()
		{
            // Need to delay the open package action' as we are on the 
            // startup thread, but might need frm interaction
            this.Load += new System.EventHandler(this.MainDesigner_Load);

            fileToOpen = paramfileToOpen;

        }


        private void MainDesigner_Load (Object sender, EventArgs args)
        {
            System.Threading.Thread.Sleep(1000);
            splashTimer_Tick(null, EventArgs.Empty);

            UnloadPackage( );

            try
			{
				string localPath = null;
				// ODMA docref's all start with '::'. Apparently.
                if (fileToOpen.Trim().Substring(0, 2) == "::")
                {
                    localPath = OdmaFetchFromDm(fileToOpen);
                }
                else
                {
                    // SharePoint: Tye ActiveX control creates a command line like:
                    // http://vpc-michaeldev2/Shared%20Documents/New%20Package.ip SharePoint
                    // http://vpc-michaeldev2/two/Shared%20Documents/New%20Package%20site%20two.ip SharePoint
                    // Note that we migth want to implement other http handlers later
                    String[] parameters = fileToOpen.Split(new char[] { ' ', '\t' });
                    if (parameters.Length > 1 && parameters[1] == "SharePoint")
                    {
                        OpenPackageFromIntegration(parameters[0]);
                        package.SetNoChanges();
                        MaybeInvokePackageFixer(true);
                    }
                    else
                    {
                        // Standard file system
                        localPath = fileToOpen;
                    }
                }
				if (localPath != null)
				{
                    // Open package passing in our file stream and read/write access, and other processes can only have read access..
                    package = Package.ReadFromDisc( localPath, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read );
                    pluginForSave = null;
                    pluginTitleText = String.Empty;
                    mruList.AddFilename(localPath);
                    TestPackageVersion(package.VersionXml, localPath);
                    MaybeInvokePackageFixer(true);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, Common.About.FormsTitle);
				NewPackage();
			}
			ResetUI();
		}
		
		public MainDesigner()
		{
			splashForm = new Splash();
			splashForm.Show();
			splashForm.Refresh();
			//
			// Required for Windows Form Designer support
			//
			try
			{
				InitializeComponent();

                //increase ui sizes
                //Properties.Settings.Default.
                //add to explorer height & width (in pane4)
                explorerControl.Size += new System.Drawing.Size(200, 250);
                twPackageExplorer.DockedSize += new System.Drawing.Size(200, 250);
                twPackageExplorer.MinimumSize += new System.Drawing.Size(200, 250);

                //add to properties height & width (in pane1)
                propertyGrid1.Size += new System.Drawing.Size(200, 150);
                twProperties.DockedSize += new System.Drawing.Size(200, 150);
                twProperties.MinimumSize += new System.Drawing.Size(200, 150);
                pane1.Location += new System.Drawing.Size(0, 250);

                //uiTaskBar1.Size += new System.Drawing.Size(100, 400);
                dockLeft1.Size += new System.Drawing.Size(200, 400);

                //top
                //dockTop1.Size += new System.Drawing.Size(300, 0);
                //sharedLibraryControl.Size += new System.Drawing.Size(0,400);
                //twSharedLibrary.DockedSize += new System.Drawing.Size(0, 400);
                //twSharedLibrary.MinimumSize += new System.Drawing.Size(0, 400);

                twLoadQuestions.DockedSize += new System.Drawing.Size(50, 400);
                twLoadQuestions.MinimumSize += new System.Drawing.Size(50, 400);

                this.Size = this.Size + new System.Drawing.Size(500, 400);
                //int width = Properties.Settings.Default.Studio_Width;
                //int height = Properties.Settings.Default.Studio_Height;
                //if (width > 890) width -= 890;
                //if (height > 609) height -= 609;
                //this.Size = this.Size + new System.Drawing.Size(width, height);
                ReallyCenterToScreen();

				LoadPlugins();

				SharedLibraryProxy sharedLibraryProxy = new SharedLibraryProxy();

                try
                {
                    // Only display the Shared Library menu if it is activated in the client config (does not check activated status on the server)
                    if (!sharedLibraryProxy.IsSharedLibraryClientActivated())
                    {
                        twSharedLibrary.Visible = false;
                        btSharedLibraryControl.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                    MessageBox.Show(message, GetResource("PackageExplorer.ExplorerControl.ExplorerPaneError"), MessageBoxButtons.OK, 
                                    MessageBoxIcon.Exclamation);
                    twSharedLibrary.Visible = false;
                    btSharedLibraryControl.Visible = false;
                }
			}
	
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			formTitleFormat = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.FormTitleFormat");
			explorerControl.PropertyGrid = propertyGrid1;
			mruList = new MRU(Common.About.Company, Common.About.FormsTitle);

			UpdateMruItems();

			NewPackage();

			splashForm.Refresh();

			splashForm.BringToFront();
			splashTimer.Start();

			OdmaRegister();

		}
        protected void ReallyCenterToScreen()
        {
            Screen screen = Screen.FromControl(this);

            System.Drawing.Rectangle workingArea = screen.WorkingArea;
            this.Location = new System.Drawing.Point()
            {
                X = Math.Max(workingArea.X, workingArea.X + (workingArea.Width - this.Width) / 2),
                Y = Math.Max(workingArea.Y, workingArea.Y + (workingArea.Height - this.Height) / 2)
            };
        }

		#region ODMA Support
		
		private bool IsOdmaAvailable()
		{
			return odmaHandle != int.MinValue && odmaHandle != 0;
		}

		private bool HaveOdmaDocument()
		{
			return odmaDocid.Trim().Trim('\0').Length > 0;
		}

		private void OdmaRegister()
		{
			odmaTraceBuilder.Append("OdmaRegister() : Handle = ");
			Int32 handle = 0;
			string errCode = string.Empty;
			try
			{
				OdmaImports.RegisterDMS(Handle.ToInt32(), ref errCode, "IPMANAGER", ref handle);
				if (handle != 0 || errCode.ToLower() != "successful")
				{
					this.odmaHandle = handle;
				}
			}
			catch
			{
			    // Intentional
			} // Assume no ODMA support on this machine

			odmaTraceBuilder.Append(handle);
			odmaTraceBuilder.Append("\n");

		}

		private string OdmaGetOpenFilePath()
		{
			odmaTraceBuilder.Append("OdmaGetOpenFilePath()\n");
			// Our standard file open dialog
			if (!IsOdmaAvailable())
			{
				odmaTraceBuilder.Append(" Odma NotAvailable - using standard file dialog.\n");
				return OdmaGetOpenFileFromStandardDialog();
			}
			else // Else, use the ODMA open dialog and whatnot
			{
				string docId = new string(' ', OdmaImports.ODM_DOCID_MAX);
				int flags = 0;
				odmaTraceBuilder.Append(" Calling ODMSelectDoc.  Returned: ");
				int retval = OdmaImports.ODMSelectDoc(odmaHandle, ref docId, ref flags);
				odmaTraceBuilder.Append(retval);
				odmaTraceBuilder.Append("\n");
				
				if (retval == OdmaImports.ODM_E_CANCEL)
				{
					return null;
				}
				if (retval != 0)
				{
					odmaTraceBuilder.Append(" Select returned non-zero: using standard file dialog.\n");
					return OdmaGetOpenFileFromStandardDialog();
				}
				else
				{

					string localPath = OdmaFetchFromDm(docId);
					if (localPath != null)
					{
						return localPath;
					}
					else
					{
						odmaTraceBuilder.Append(" Open returned non-zero: using standard file dialog.\n");
						return OdmaGetOpenFileFromStandardDialog();
					}
				}
			}
		}

		private string OdmaFetchFromDm(string docref)
		{
			if (IsOdmaAvailable())
			{
				string location = new string(' ', OdmaImports.ODM_FILENAME_MAX);
				int flags = 0;

				odmaTraceBuilder.Append(string.Format(" Callind ODMOpenDoc for docId: [{0}].  Returned: ", docref.Trim().Trim('\0')));
				int retval = OdmaImports.ODMOpenDoc(odmaHandle, flags, ref docref, ref location);
				odmaTraceBuilder.Append(retval);
				odmaTraceBuilder.Append("\n");
	
				if (retval == OdmaImports.ODM_E_CANCEL)
				{
					return null;
				}
	
				if (retval == 0)
				{
					odmaDocid = docref;
					return location.Trim().Trim('\0');
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
	
		}

		private string OdmaGetOpenFileFromStandardDialog()
		{
			DialogResult dr = openFileDialog1.ShowDialog();
			if (dr == DialogResult.OK)
			{
                mruList.AddFilename(openFileDialog1.FileName);
				return openFileDialog1.FileName;
			}
			else
			{
				return null;
			}
		}

		private bool OdmaSave(string path)
		{
			if (IsOdmaAvailable() && HaveOdmaDocument())
			{
				odmaTraceBuilder.Append("OdmaSave()\n");
				string newDocId = new string(' ', OdmaImports.ODM_DOCID_MAX);
				odmaTraceBuilder.Append(string.Format("Calling ODMSaveDoc() for docid: [{0}].  Returned: ", odmaDocid.Trim().Trim('\0')));
				int retVal = OdmaImports.ODMSaveDoc(odmaHandle, ref odmaDocid, ref newDocId);
				odmaTraceBuilder.Append(retVal);
				odmaTraceBuilder.Append("\n");
				if (retVal == 0)
				{
					odmaTraceBuilder.Append(" new odmaDocId: ");
					odmaTraceBuilder.Append(newDocId.Trim().Trim('\0'));
					odmaTraceBuilder.Append("\n");
					odmaDocid = newDocId;
					return true; // successfully saved the document, and got an update docid.
				}
				else
				{
					return false; // non-zero retval for a save means there was a problem.  assume the save failed and our docid is unchanged.
				}
			}
			else
			{
				return true;  // No problem, as we didn't do anything.
			}
		}

		private string OdmaSaveAs(string temporarySaveLocation, string defaultFilename, Guid originalGuid)
		{
			odmaTraceBuilder.Append(string.Format("OdmaSaveAs({0}, {1})\n", temporarySaveLocation,  defaultFilename));
			int retval;
			if (IsOdmaAvailable())
			{
				string format = new string(' ', OdmaImports.ODM_DOCID_MAX);
				string newDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);
				string location = new string(' ', OdmaImports.ODM_FILENAME_MAX);
								
				// If the doc isn't currently in DM, make a new one.
				if ( ! HaveOdmaDocument() )
				{
					odmaTraceBuilder.Append(" ODMNewDoc() returned: ");					
					retval = OdmaImports.ODMNewDoc(odmaHandle, ref odmaDocid, 0, ref format, ref location);
					odmaTraceBuilder.Append(retval);
					odmaTraceBuilder.Append("\n");
					if (retval == OdmaImports.ODM_E_CANCEL)
					{
                        package.UniqueIdentifier = originalGuid;
						return null;
					}
					if (retval != 0)
					{						
						return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
					}
				}

				// Do a 'dm save as' whatever that means.  'SaveAs' the current (or new, if no current) docid, and get a new docid,
				string oldDocid = odmaDocid;
				odmaTraceBuilder.Append(string.Format(" Old docid: [{0}]", oldDocid.Trim().Trim('\0')));
				odmaTraceBuilder.Append(" ODMSaveAs() returned: ");				
				retval = OdmaImports.ODMSaveAs(odmaHandle, ref oldDocid, ref newDocid, ref format, 0, 0);
				odmaTraceBuilder.Append(retval);				
				odmaTraceBuilder.Append("\n");
				
                if (retval == OdmaImports.ODM_E_CANCEL)
				{
                    package.UniqueIdentifier = originalGuid;
					return null;
				}
				if (retval != 0)
				{
                    return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
				}
                else if (Convert.ToInt32(newDocid[0]) == 0) //ODMA spec specifies that if first byte null for this ODMSaveAs param we should call ODMSaveDoc
                {
                    package.UniqueIdentifier = originalGuid;
                    Save();
                    return package.CurrentFilePath; //unchanged because doing a save
                }

				odmaTraceBuilder.Append(string.Format(" New docid: [{0}]", newDocid.Trim().Trim('\0')));
				

				// If we made a new doc, open it and get its location.  Otherwise, location stays.
				if (oldDocid.Trim().Trim('\0') != newDocid.Trim().Trim('\0'))
				{
					odmaTraceBuilder.Append(" ODMOpenDoc() returned: ");
					retval = OdmaImports.ODMOpenDoc(odmaHandle, 0, ref newDocid, ref location);
					odmaTraceBuilder.Append(retval);
					odmaTraceBuilder.Append("\n");
				}
               
				try
				{	
										
					if(location.Trim().Trim('\0').Length > 0)
					{						
						File.Delete(location.Trim().Trim('\0'));
						File.Move(temporarySaveLocation.Trim().Trim('\0'), location.Trim().Trim('\0'));
					}
					else
					{
                        Package.SaveToDisc( ref ipFileStream, package);
                        OdmaSave(temporarySaveLocation.Trim().Trim('\0'));
						return null;
					}

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, Common.About.FormsTitle);
                    return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
				}

				OdmaCloseCurrent();	
				odmaDocid = newDocid;				
                retval = OdmaImports.ODMSaveDoc(odmaHandle,ref odmaDocid,ref newDocid);								
				
				return location.Trim().Trim('\0');
			}
			else
			{
                return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
			}
		}

		private void OdmaClose(string docId, bool setNoneOpen)
		{
			odmaTraceBuilder.Append("OdmaClose()\n");
			if (IsOdmaAvailable() && docId != null && docId.Trim().Length > 0)
			{
				try
				{
					odmaTraceBuilder.Append(" Closing [");
					odmaTraceBuilder.Append(docId.Trim().Trim('\0'));
					
					OdmaImports.ODMCloseDoc(odmaHandle, ref docId, 0, 0, 0, 0);
					odmaTraceBuilder.Append("] success\n");
					if (setNoneOpen)
					{
						odmaDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);
					}
				}
				catch
				{
					odmaTraceBuilder.Append("] failed\n");
				}
			}
		}

		public void OdmaCloseCurrent()
		{
			OdmaClose(odmaDocid, true);
		}
		
		#endregion

		private void UpdateMruItems()
		{
			if (mruMenuItems == null)
			{
				mruMenuItems = new Hashtable(mruList.MruLength);
			}

			foreach (string k in mruMenuItems.Keys)
			{
				object o = mruMenuItems[k];
				if (o is ButtonTool)
				{
					((ButtonTool) o).ParentBand.Tools.Remove((ButtonTool) o);
					//((ButtonTool)o).Dispose();					
				}
			}

			mruMenuItems.Clear();

			bool firstItem = true;
			int shortcut = 1;
			for (int idx = mruList.InnerCollection.Count - 1; idx >= 0; idx --)
			{
				string s = mruList.InnerCollection[idx];
				if (!mruMenuItems.ContainsKey(s.ToUpper()))
				{
					ButtonTool bt = new ButtonTool();
					if (firstItem)
					{
						bt.BeginGroup = true;
						firstItem = false;
					}

					ptMainFile.SubBand.Tools.Add(bt);
					bt.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
					shortcut++;
					bt.Tag = s;
					bt.Click += new EventHandler(mruButtonClick);
					mruMenuItems.Add(s.ToUpper(), bt);
				}
			}

			ptMainFile.ResetTool();
		}


		private void HookupPackage(Package newPackage)
		{
			explorerControl.Package = newPackage;
			bulkAdd1.CurrentPackage = newPackage;
			importItems1.CurrentPackage = newPackage;

			package.PropertyChanged += new PropertyChangedEventHandler(package_PropertyChanged);
			package.ItemDeleted += new EventHandler<PackageItemDeletedEventArgs>(package_ItemDeleted);
			package.ItemCreated += new EventHandler<PackageItemEventArgs>(package_ItemCreated);
            package.EnsureDesignerSaved += new EventHandler<PackageItemEventArgs>(package_EnsureDesignerSaved);

			saveFileDialog1.Reset();
			ResourceManager resources = new ResourceManager(typeof(MainDesigner));
			saveFileDialog1.Filter = resources.GetString("saveFileDialog1.Filter");
			saveFileDialog1.DefaultExt = "ip";

			PackageNameBind();

			cmInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);

			PopulatePackageWithDisplayTypeExtensions();	
			PopulatePackageWithFormatStringExtensions();	

			package.SetNoChanges();
		}


        #region IDisposable Members
        // Track whether Dispose has been called.
        private bool disposed = false;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                try
                {
                    if( disposing )
                    {
                        // Release the managed resources added to this derived class here.
                        if( components != null ) { components.Dispose( ); components = null; }

                        // Dispose package, filestreams, plugins etc....
                        UnloadPackage( );

                        // Shutdown plugins
                        if( m_plugins != null )
                        {
                            foreach( IStudioPlugin p in m_plugins )
                            {
                                try
                                { p.Shutdown( ); }
                                // Intentional 
                                catch { }
                            }
                        }

                    }
                    // Release the native unmanaged resources added to this derived class here.

                    this.disposed = true;
                }
                finally
                {
                    // Call Dispose on the base class.
                    base.Dispose( disposing );
                }
            }
        }
        #endregion


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.components = new System.ComponentModel.Container( );
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( MainDesigner ) );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey1 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey2 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey3 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey4 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey5 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey6 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Toolbars.ShortcutKey shortcutKey7 = new DataDynamics.SharpUI.Toolbars.ShortcutKey( );
            DataDynamics.SharpUI.Panels.UiTaskBarGroup uiTaskBarGroup1 = new DataDynamics.SharpUI.Panels.UiTaskBarGroup( );
            DataDynamics.SharpUI.Panels.UiTaskBarGroup uiTaskBarGroup2 = new DataDynamics.SharpUI.Panels.UiTaskBarGroup( );
            this.importItems1 = new Perfectus.Client.Studio.UI.Tasks.ImportItems( );
            this.bulkAdd1 = new Perfectus.Client.Studio.UI.Tasks.BulkAdd( );
            this.uiToolbarManager1 = new DataDynamics.SharpUI.Toolbars.UiToolbarManager( );
            this.mbMain = new DataDynamics.SharpUI.Toolbars.Menubar( );
            this.ptMainFile = new DataDynamics.SharpUI.Toolbars.PopupTool( );
            this.popupMenu2 = new DataDynamics.SharpUI.Toolbars.PopupMenu( );
            this.btFileNew = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmNew = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btFileOpen = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmOpen = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btFileSave = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmSave = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btFileSaveAs = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmSaveAs = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btPublish = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmPublish = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btExit = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmExit = new DataDynamics.SharpUI.Toolbars.Command( );
            this.ptMainWindow = new DataDynamics.SharpUI.Toolbars.PopupTool( );
            this.popupMenu3 = new DataDynamics.SharpUI.Toolbars.PopupMenu( );
            this.mdiListTool1 = new DataDynamics.SharpUI.Toolbars.MdiListTool( );
            this.ptTools = new DataDynamics.SharpUI.Toolbars.PopupTool( );
            this.popupMenu5 = new DataDynamics.SharpUI.Toolbars.PopupMenu( );
            this.btLoadQuestions = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmShowLoadQuestionsTaskBar = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btSharedLibraryControl = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmShowSharedLibraryControlTaskBar = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btClauseReport = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmShowClauseReport = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btFixPackage = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmFixPackage = new DataDynamics.SharpUI.Toolbars.Command( );
            this.cmConfiguration = new DataDynamics.SharpUI.Toolbars.Command( );
            this.ptHelp = new DataDynamics.SharpUI.Toolbars.PopupTool( );
            this.popupMenu4 = new DataDynamics.SharpUI.Toolbars.PopupMenu( );
            this.btHelpHelp = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmHelp = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btHelpAbout = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmHelpAbout = new DataDynamics.SharpUI.Toolbars.Command( );
            this.btOdmaStatus = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.tbStandard = new DataDynamics.SharpUI.Toolbars.Toolbar( );
            this.btNew = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.btOpen = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.btSave = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.btSaveAs = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.popupMenu1 = new DataDynamics.SharpUI.Toolbars.PopupMenu( );
            this.btPublish2 = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.btInterviewDiagram = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            this.cmInterviewDiagram = new DataDynamics.SharpUI.Toolbars.Command( );
            this.cmOdmaStatus = new DataDynamics.SharpUI.Toolbars.Command( );
            this.twProperties = new DataDynamics.SharpUI.Toolbars.ToolWindow( );
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid( );
            this.twPackageExplorer = new DataDynamics.SharpUI.Toolbars.ToolWindow( );
            this.explorerControl = new Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControl( );
            this.twSharedLibrary = new DataDynamics.SharpUI.Toolbars.ToolWindow( );
            this.sharedLibraryControl = new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryControl( );
            this.twLoadQuestions = new DataDynamics.SharpUI.Toolbars.ToolWindow( );
            this.uiTaskBar1 = new DataDynamics.SharpUI.Panels.UiTaskBar( );
            this.imageList1 = new System.Windows.Forms.ImageList( this.components );
            this.cmCollect = new DataDynamics.SharpUI.Toolbars.Command( );
            this.dockTop1 = new DataDynamics.SharpUI.Toolbars.Dock( );
            this.dockRight1 = new DataDynamics.SharpUI.Toolbars.Dock( );
            this.dockBottom1 = new DataDynamics.SharpUI.Toolbars.Dock( );
            this.dockLeft1 = new DataDynamics.SharpUI.Toolbars.Dock( );
            this.pane1 = new DataDynamics.SharpUI.Toolbars.Pane( );
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog( );
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog( );
            this.splashTimer = new System.Windows.Forms.Timer( this.components );
            this.tabForms = new Perfectus.Client.Studio.UI.PackageExplorer.TabControlWithDelete( );
            this.pane4 = new DataDynamics.SharpUI.Toolbars.Pane( );
            this.pane2 = new DataDynamics.SharpUI.Toolbars.Pane( );
            this.pane3 = new DataDynamics.SharpUI.Toolbars.Pane( );
            this.btCustomize = new DataDynamics.SharpUI.Toolbars.ButtonTool( );
            ( (System.ComponentModel.ISupportInitialize)( this.uiTaskBar1 ) ).BeginInit( );
            this.uiTaskBar1.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // importItems1
            // 
            this.importItems1.BackColor = System.Drawing.Color.White;
            this.importItems1.CurrentPackage = null;
            resources.ApplyResources( this.importItems1, "importItems1" );
            this.importItems1.Name = "importItems1";
            // 
            // bulkAdd1
            // 
            this.bulkAdd1.CurrentPackage = null;
            resources.ApplyResources( this.bulkAdd1, "bulkAdd1" );
            this.bulkAdd1.Name = "bulkAdd1";
            // 
            // uiToolbarManager1
            // 
            this.uiToolbarManager1.ActiveToolWindow = null;
            resources.ApplyResources( this.uiToolbarManager1, "uiToolbarManager1" );
            this.uiToolbarManager1.Bands.AddRange( new DataDynamics.SharpUI.Toolbars.Band[ ] {
            this.mbMain,
            this.tbStandard,
            this.popupMenu1,
            this.popupMenu2,
            this.popupMenu3,
            this.popupMenu4,
            this.popupMenu5} );
            this.uiToolbarManager1.Commands.AddRange( new DataDynamics.SharpUI.Toolbars.Command[ ] {
            this.cmNew,
            this.cmOpen,
            this.cmSave,
            this.cmSaveAs,
            this.cmExit,
            this.cmHelpAbout,
            this.cmShowLoadQuestionsTaskBar,
            this.cmPublish,
            this.cmInterviewDiagram,
            this.cmHelp,
            this.cmOdmaStatus,
            this.cmShowSharedLibraryControlTaskBar,
            this.cmFixPackage,
            this.cmConfiguration} );
            // 
            // 
            // 
            this.uiToolbarManager1.CustomizeImages.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.uiToolbarManager1.CustomizeImages.ImageSize = ( (System.Drawing.Size)( resources.GetObject( "uiToolbarManager1.CustomizeImages.ImageSize" ) ) );
            this.uiToolbarManager1.CustomizeImages.TransparentColor = System.Drawing.Color.Transparent;
            this.uiToolbarManager1.EzMdiOptions.Tabs.IsTabAngled = ( (bool)( resources.GetObject( "resource.IsTabAngled" ) ) );
            this.uiToolbarManager1.EzMdiOptions.Tabs.Position = ( (DataDynamics.SharpUI.Toolbars.TabPositions)( resources.GetObject( "resource.Position" ) ) );
            this.uiToolbarManager1.EzMdiOptions.Tabs.Style = ( (DataDynamics.SharpUI.Toolbars.TabStyles)( resources.GetObject( "resource.Style" ) ) );
            this.uiToolbarManager1.Parent = this;
            this.uiToolbarManager1.ToolWindowOptions.Tabs.Position = ( (DataDynamics.SharpUI.Toolbars.TabPositions)( resources.GetObject( "resource.Position1" ) ) );
            this.uiToolbarManager1.ToolWindowOptions.Tabs.Style = ( (DataDynamics.SharpUI.Toolbars.TabStyles)( resources.GetObject( "resource.Style1" ) ) );
            this.uiToolbarManager1.ToolWindows.AddRange( new DataDynamics.SharpUI.Toolbars.ToolWindow[ ] {
            this.twProperties,
            this.twSharedLibrary,
            this.twLoadQuestions,
            this.twPackageExplorer} );
            this.uiToolbarManager1.EzMdiChildActivate += new System.EventHandler( this.MainDesigner_MdiChildActivate );
            // 
            // mbMain
            // 
            resources.ApplyResources( this.mbMain, "mbMain" );
            this.mbMain.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.ptMainFile,
            this.ptMainWindow,
            this.ptTools,
            this.ptHelp} );
            this.mbMain.Name = "mbMain";
            // 
            // ptMainFile
            // 
            resources.ApplyResources( this.ptMainFile, "ptMainFile" );
            this.ptMainFile.Name = "ptMainFile";
            this.ptMainFile.SubBand = this.popupMenu2;
            // 
            // popupMenu2
            // 
            resources.ApplyResources( this.popupMenu2, "popupMenu2" );
            this.popupMenu2.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.btFileNew,
            this.btFileOpen,
            this.btFileSave,
            this.btFileSaveAs,
            this.btPublish,
            this.btExit} );
            this.popupMenu2.Name = "popupMenu2";
            // 
            // btFileNew
            // 
            resources.ApplyResources( this.btFileNew, "btFileNew" );
            this.btFileNew.Command = this.cmNew;
            // 
            // cmNew
            // 
            resources.ApplyResources( this.cmNew, "cmNew" );
            resources.ApplyResources( shortcutKey1, "shortcutKey1" );
            this.cmNew.Shortcuts.Add( shortcutKey1 );
            this.cmNew.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmNew_Executed );
            // 
            // btFileOpen
            // 
            this.btFileOpen.Command = this.cmOpen;
            resources.ApplyResources( this.btFileOpen, "btFileOpen" );
            // 
            // cmOpen
            // 
            resources.ApplyResources( this.cmOpen, "cmOpen" );
            resources.ApplyResources( shortcutKey2, "shortcutKey2" );
            this.cmOpen.Shortcuts.Add( shortcutKey2 );
            this.cmOpen.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmOpen_Executed );
            // 
            // btFileSave
            // 
            resources.ApplyResources( this.btFileSave, "btFileSave" );
            this.btFileSave.Command = this.cmSave;
            // 
            // cmSave
            // 
            resources.ApplyResources( this.cmSave, "cmSave" );
            resources.ApplyResources( shortcutKey3, "shortcutKey3" );
            this.cmSave.Shortcuts.Add( shortcutKey3 );
            this.cmSave.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmSave_Executed );
            // 
            // btFileSaveAs
            // 
            this.btFileSaveAs.Command = this.cmSaveAs;
            resources.ApplyResources( this.btFileSaveAs, "btFileSaveAs" );
            // 
            // cmSaveAs
            // 
            resources.ApplyResources( this.cmSaveAs, "cmSaveAs" );
            resources.ApplyResources( shortcutKey4, "shortcutKey4" );
            this.cmSaveAs.Shortcuts.Add( shortcutKey4 );
            this.cmSaveAs.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmSaveAs_Executed );
            // 
            // btPublish
            // 
            resources.ApplyResources( this.btPublish, "btPublish" );
            this.btPublish.Command = this.cmPublish;
            // 
            // cmPublish
            // 
            resources.ApplyResources( this.cmPublish, "cmPublish" );
            resources.ApplyResources( shortcutKey5, "shortcutKey5" );
            this.cmPublish.Shortcuts.Add( shortcutKey5 );
            this.cmPublish.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmPublish_Executed );
            // 
            // btExit
            // 
            resources.ApplyResources( this.btExit, "btExit" );
            this.btExit.Command = this.cmExit;
            // 
            // cmExit
            // 
            resources.ApplyResources( this.cmExit, "cmExit" );
            resources.ApplyResources( shortcutKey6, "shortcutKey6" );
            this.cmExit.Shortcuts.Add( shortcutKey6 );
            this.cmExit.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmExit_Executed );
            // 
            // ptMainWindow
            // 
            this.ptMainWindow.Name = "ptMainWindow";
            this.ptMainWindow.SubBand = this.popupMenu3;
            resources.ApplyResources( this.ptMainWindow, "ptMainWindow" );
            // 
            // popupMenu3
            // 
            resources.ApplyResources( this.popupMenu3, "popupMenu3" );
            this.popupMenu3.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.mdiListTool1} );
            this.popupMenu3.Name = "popupMenu3";
            // 
            // mdiListTool1
            // 
            resources.ApplyResources( this.mdiListTool1, "mdiListTool1" );
            // 
            // ptTools
            // 
            this.ptTools.Name = "ptTools";
            this.ptTools.SubBand = this.popupMenu5;
            resources.ApplyResources( this.ptTools, "ptTools" );
            // 
            // popupMenu5
            // 
            resources.ApplyResources( this.popupMenu5, "popupMenu5" );
            this.popupMenu5.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.btLoadQuestions,
            this.btSharedLibraryControl,
            this.btClauseReport,
            this.btFixPackage,
            this.btCustomize} );
            this.popupMenu5.Name = "popupMenu5";
            // 
            // btLoadQuestions
            // 
            this.btLoadQuestions.Command = this.cmShowLoadQuestionsTaskBar;
            resources.ApplyResources( this.btLoadQuestions, "btLoadQuestions" );
            // 
            // cmShowLoadQuestionsTaskBar
            // 
            resources.ApplyResources( this.cmShowLoadQuestionsTaskBar, "cmShowLoadQuestionsTaskBar" );
            this.cmShowLoadQuestionsTaskBar.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmShowLoadQuestionsTaskBar_Executed );
            // 
            // btSharedLibraryControl
            // 
            this.btSharedLibraryControl.Command = this.cmShowSharedLibraryControlTaskBar;
            resources.ApplyResources( this.btSharedLibraryControl, "btSharedLibraryControl" );
            // 
            // cmShowSharedLibraryControlTaskBar
            // 
            resources.ApplyResources( this.cmShowSharedLibraryControlTaskBar, "cmShowSharedLibraryControlTaskBar" );
            this.cmShowSharedLibraryControlTaskBar.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmShowSharedLibraryControlTaskBar_Executed );
            // 
            // btClauseReport
            // 
            this.btClauseReport.Command = this.cmShowClauseReport;
            resources.ApplyResources( this.btClauseReport, "btClauseReport" );
            // 
            // cmShowClauseReport
            // 
            resources.ApplyResources( this.cmShowClauseReport, "cmShowClauseReport" );
            this.cmShowClauseReport.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmShowClauseReport_Executed );
            // 
            // btFixPackage
            // 
            this.btFixPackage.Command = this.cmFixPackage;
            this.btFixPackage.Name = "btFixPackage";
            this.btFixPackage.Visible = false;
            resources.ApplyResources( this.btFixPackage, "btFixPackage" );
            // 
            // cmFixPackage
            // 
            resources.ApplyResources( this.cmFixPackage, "cmFixPackage" );
            this.cmFixPackage.Name = "cmFixPackage";
            this.cmFixPackage.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmFixPackage_Executed );
            // 
            // cmConfiguration
            // 
            resources.ApplyResources( this.cmConfiguration, "cmConfiguration" );
            this.cmConfiguration.Name = "cmConfiguration";
            this.cmConfiguration.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.configuration_button_Click );
            // 
            // ptHelp
            // 
            this.ptHelp.Name = "ptHelp";
            this.ptHelp.SubBand = this.popupMenu4;
            resources.ApplyResources( this.ptHelp, "ptHelp" );
            // 
            // popupMenu4
            // 
            resources.ApplyResources( this.popupMenu4, "popupMenu4" );
            this.popupMenu4.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.btHelpHelp,
            this.btHelpAbout,
            this.btOdmaStatus} );
            this.popupMenu4.Name = "popupMenu4";
            // 
            // btHelpHelp
            // 
            this.btHelpHelp.Command = this.cmHelp;
            resources.ApplyResources( this.btHelpHelp, "btHelpHelp" );
            // 
            // cmHelp
            // 
            resources.ApplyResources( this.cmHelp, "cmHelp" );
            resources.ApplyResources( shortcutKey7, "shortcutKey7" );
            this.cmHelp.Shortcuts.Add( shortcutKey7 );
            this.cmHelp.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmHelp_Executed );
            // 
            // btHelpAbout
            // 
            resources.ApplyResources( this.btHelpAbout, "btHelpAbout" );
            this.btHelpAbout.Command = this.cmHelpAbout;
            // 
            // cmHelpAbout
            // 
            resources.ApplyResources( this.cmHelpAbout, "cmHelpAbout" );
            this.cmHelpAbout.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmHelpAbout_Executed );
            // 
            // btOdmaStatus
            // 
            resources.ApplyResources( this.btOdmaStatus, "btOdmaStatus" );
            this.btOdmaStatus.Click += new System.EventHandler( this.btOdmaStatus_Click );
            // 
            // tbStandard
            // 
            resources.ApplyResources( this.tbStandard, "tbStandard" );
            this.tbStandard.Tools.AddRange( new DataDynamics.SharpUI.Toolbars.Tool[ ] {
            this.btNew,
            this.btOpen,
            this.btSave,
            this.btSaveAs,
            this.btPublish2,
            this.btInterviewDiagram} );
            this.tbStandard.Name = "tbStandard";
            // 
            // btNew
            // 
            this.btNew.Command = this.cmNew;
            resources.ApplyResources( this.btNew, "btNew" );
            this.btNew.Name = "btNew";
            // 
            // btOpen
            // 
            this.btOpen.Command = this.cmOpen;
            resources.ApplyResources( this.btOpen, "btOpen" );
            this.btOpen.Name = "btOpen";
            // 
            // btSave
            // 
            this.btSave.Command = this.cmSave;
            resources.ApplyResources( this.btSave, "btSave" );
            this.btSave.Name = "btSave";
            // 
            // btSaveAs
            // 
            this.btSaveAs.Command = this.cmSaveAs;
            resources.ApplyResources( this.btSaveAs, "btSaveAs" );
            this.btSaveAs.Name = "btSaveAs";
            this.btSaveAs.SubBand = this.popupMenu1;
            // 
            // popupMenu1
            // 
            resources.ApplyResources( this.popupMenu1, "popupMenu1" );
            this.popupMenu1.Name = "popupMenu1";
            // 
            // btPublish2
            // 
            this.btPublish2.Command = this.cmPublish;
            resources.ApplyResources( this.btPublish2, "btPublish2" );
            this.btPublish2.Name = "btPublish2";
            // 
            // btInterviewDiagram
            // 
            resources.ApplyResources( this.btInterviewDiagram, "btInterviewDiagram" );
            this.btInterviewDiagram.Command = this.cmInterviewDiagram;
            this.btInterviewDiagram.Name = "btInterviewDiagram";
            // 
            // cmInterviewDiagram
            // 
            resources.ApplyResources( this.cmInterviewDiagram, "cmInterviewDiagram" );
            this.cmInterviewDiagram.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler( this.cmInterviewDiagram_Executed );
            // 
            // cmOdmaStatus
            // 
            resources.ApplyResources( this.cmOdmaStatus, "cmOdmaStatus" );
            this.cmOdmaStatus.Name = "cmOdmaStatus";
            this.cmOdmaStatus.Text = null;
            // 
            // twProperties
            // 
            resources.ApplyResources( this.twProperties, "twProperties" );
            this.twProperties.HostedControl = this.propertyGrid1;
            this.twProperties.Name = "twProperties";
            this.twProperties.UiToolbarManager = this.uiToolbarManager1;
            this.twProperties.Dock( DataDynamics.SharpUI.Toolbars.DockDirections.Left, new DataDynamics.SharpUI.Toolbars.ToolWindow[ ] {
            twPackageExplorer,
            DataDynamics.SharpUI.Toolbars.ToolWindow.Empty}, new DataDynamics.SharpUI.Toolbars.Edges[ ] {
            DataDynamics.SharpUI.Toolbars.Edges.Bottom,
            DataDynamics.SharpUI.Toolbars.Edges.Left} );
            // 
            // propertyGrid1
            // 
            resources.ApplyResources( this.propertyGrid1, "propertyGrid1" );
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler( this.propertyGrid1_PropertyValueChanged );
            this.propertyGrid1.SelectedGridItemChanged += new SelectedGridItemChangedEventHandler( propertyGrid1_SelectedGridItemChanged );
            // 
            // twPackageExplorer
            // 
            resources.ApplyResources( this.twPackageExplorer, "twPackageExplorer" );
            this.twPackageExplorer.HostedControl = this.explorerControl;
            this.twPackageExplorer.Name = "twPackageExplorer";
            this.twPackageExplorer.UiToolbarManager = this.uiToolbarManager1;
            this.twPackageExplorer.Dock( DataDynamics.SharpUI.Toolbars.DockDirections.Left, new DataDynamics.SharpUI.Toolbars.ToolWindow[ ] {
            twProperties,
            DataDynamics.SharpUI.Toolbars.ToolWindow.Empty}, new DataDynamics.SharpUI.Toolbars.Edges[ ] {
            DataDynamics.SharpUI.Toolbars.Edges.Top,
            DataDynamics.SharpUI.Toolbars.Edges.Left} );
            // 
            // explorerControl
            // 
            resources.ApplyResources( this.explorerControl, "explorerControl" );
            this.explorerControl.Package = null;
            this.explorerControl.Plugins = null;
            this.explorerControl.PropertyGrid = null;
            this.explorerControl.PopupDialogMoved += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>( this.explorerControl_PopupDialogMoved );
            this.explorerControl.OpenDesigner += new Perfectus.Common.PackageObjects.PackageItemOpenDesignerEventHandler( this.packageExplorer_OpenDesigner );
            this.explorerControl.SimpleOutcomeDrop += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>( this.explorerControl_SimpleOutcomeDrop );
            this.explorerControl.OutcomeDrop += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>( this.explorerControl_OutcomeDrop );
            this.explorerControl.Publish += new System.EventHandler( this.explorerControl_Publish );
            // 
            // twSharedLibrary
            // 
            resources.ApplyResources( this.twSharedLibrary, "twSharedLibrary" );
            this.twSharedLibrary.HostedControl = this.sharedLibraryControl;
            this.twSharedLibrary.Name = "twSharedLibrary";
            this.twSharedLibrary.UiToolbarManager = this.uiToolbarManager1;
            this.twSharedLibrary.Dock( DataDynamics.SharpUI.Toolbars.DockDirections.Right, DataDynamics.SharpUI.Toolbars.Edges.Right );
            // 
            // sharedLibraryControl
            // 
            resources.ApplyResources( this.sharedLibraryControl, "sharedLibraryControl" );
            this.sharedLibraryControl.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler( this.sharedLibraryControl_SharedLibraryItemAdded );
            // 
            // twLoadQuestions
            // 
            resources.ApplyResources( this.twLoadQuestions, "twLoadQuestions" );
            this.twLoadQuestions.HostedControl = this.uiTaskBar1;
            this.twLoadQuestions.Name = "twLoadQuestions";
            this.twLoadQuestions.UiToolbarManager = this.uiToolbarManager1;
            this.twLoadQuestions.Dock( DataDynamics.SharpUI.Toolbars.DockDirections.Right, DataDynamics.SharpUI.Toolbars.Edges.Right );
            // 
            // uiTaskBar1
            // 
            resources.ApplyResources( this.uiTaskBar1, "uiTaskBar1" );
            this.uiTaskBar1.Controls.Add( this.importItems1 );
            this.uiTaskBar1.Controls.Add( this.bulkAdd1 );
            uiTaskBarGroup1.HostedControl = this.importItems1;
            resources.ApplyResources( uiTaskBarGroup1, "uiTaskBarGroup1" );
            uiTaskBarGroup2.HostedControl = this.bulkAdd1;
            resources.ApplyResources( uiTaskBarGroup2, "uiTaskBarGroup2" );
            this.uiTaskBar1.Groups.Add( uiTaskBarGroup1 );
            this.uiTaskBar1.Groups.Add( uiTaskBarGroup2 );
            this.uiTaskBar1.ImageList = this.imageList1;
            // 
            // imageList1
            // 
            resources.ApplyResources( this.imageList1, "imageList1" );
            this.imageList1.Images.SetKeyName( 0, "" );
            this.imageList1.Images.SetKeyName( 1, "" );
            // 
            // cmCollect
            // 
            resources.ApplyResources( this.cmCollect, "cmCollect" );
            this.cmCollect.Name = "cmCollect";
            // Do not modify or remove this property.
            this.dockTop1.Name = "dockTop1";
            this.dockTop1.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.dockRight1.Name = "dockRight1";
            this.dockRight1.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.dockBottom1.Name = "dockBottom1";
            this.dockBottom1.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.dockLeft1.Name = "dockLeft1";
            this.dockLeft1.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.pane1.Name = "pane1";
            this.pane1.UiToolbarManager = uiToolbarManager1;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "ip";
            resources.ApplyResources( this.saveFileDialog1, "saveFileDialog1" );
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.ip";
            resources.ApplyResources( this.openFileDialog1, "openFileDialog1" );
            // 
            // splashTimer
            // 
            this.splashTimer.Interval = 3000;
            this.splashTimer.Tick += new System.EventHandler( this.splashTimer_Tick );
            // 
            // tabForms
            // 
            resources.ApplyResources( this.tabForms, "tabForms" );
            this.tabForms.Name = "tabForms";
            this.tabForms.SelectedIndex = 0;
            this.tabForms.Visible = false;
            this.tabForms.tabTryClose += new Perfectus.Client.Studio.UI.PackageExplorer.TabControlWithDelete.TabControlWithDeleteEventHandler( this.tabForms_tabClosing );
            this.tabForms.SelectedIndexChanged += new System.EventHandler( this.tabForms_SelectedIndexChanged );
            // Do not modify or remove this property.
            this.pane4.Name = "pane4";
            this.pane4.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.pane2.Name = "pane2";
            this.pane2.UiToolbarManager = uiToolbarManager1;
            // Do not modify or remove this property.
            this.pane3.Name = "pane3";
            this.pane3.UiToolbarManager = uiToolbarManager1;
            // 
            // btCustomize
            // 
            this.btCustomize.Command = this.cmConfiguration;
            this.btCustomize.Name = "btCustomize";
            this.btCustomize.Visible = false;
            resources.ApplyResources( this.btCustomize, "btCustomize" );
            // 
            // MainDesigner
            // 
            resources.ApplyResources( this, "$this" );
            this.Controls.Add( this.pane3 );
            this.Controls.Add( this.pane2 );
            this.Controls.Add( this.pane4 );
            this.Controls.Add( this.pane1 );
            this.Controls.Add( this.tabForms );
            this.Controls.Add( this.dockRight1 );
            this.Controls.Add( this.dockLeft1 );
            this.Controls.Add( this.dockTop1 );
            this.Controls.Add( this.dockBottom1 );
            this.IsMdiContainer = true;
            this.MdiChildActivate += new System.EventHandler( this.MainDesigner_MdiChildActivate );
            this.Closing += new System.ComponentModel.CancelEventHandler( this.MainDesigner_Closing );
            ( (System.ComponentModel.ISupportInitialize)( this.uiTaskBar1 ) ).EndInit( );
            this.uiTaskBar1.ResumeLayout( false );
            this.ResumeLayout( false );

        }

        #endregion

        private void packageExplorer_OpenDesigner( object sender, PackageItemEventArgs e, PrefferedDesigner prefferedDesigner )
		{
            PackageItem i = e.Item;

			DoOpenDesigner(i, prefferedDesigner);
			return;
		}

		private void DoOpenDesigner(PackageItem i, PrefferedDesigner prefferedDesigner)
		{
			if (i != null)
			{
				try
				{
					Cursor = Cursors.WaitCursor;
					if (i is IDesignable)
					{

                        foreach (TabPage page in tabForms.TabPages)
                        {
                            Form f = (Form)page.Tag;
                            if (f == null)
                                continue;
                            if (f is DesignerBase)
                            {
                                if (((DesignerBase)f).ItemBeingEdited == i)
                                {
                                    // Because the preffered designer variable means we can have multiple designers, we also need to check the designer is the same.
                                    if (((DesignerBase)f).DefaultDesigner == prefferedDesigner)
                                    {
                                        tabForms.SelectTab(page);
                                        page.Select();
                                        f.Activate();
                                        f.Show();
                                        return;
                                    }
                                }
                            }
                        }

                        /*
						// Make sure it's not already open
						foreach (Form f in uiToolbarManager1.EzMdiChildren)
						{
							if (f is DesignerBase)
							{
								if (((DesignerBase) f).ItemBeingEdited == i)
								{
									// Because the preffered designer variable means we can have multiple designers, we also need to check the designer is the same.
									if(((DesignerBase) f).DefaultDesigner == prefferedDesigner)
									{
										f.Activate();
										f.Show();
										return;
									}
								}
							}
						}
                         */

						// Otherwise, we'll establish a new one.

						DesignerBase designerToOpen;

						// Check if a preffered designer is set, this allows the same type of package item to be used by multiple designers and have the designer set at run time.
						if(prefferedDesigner == PrefferedDesigner.ReportViewer)
						{
							designerToOpen = new ReportViewer(i);
						}
						// If a preffered designer hasn't been set then do the next best thing and have a guess based on the items type.
						else if (i is WordTemplateDocument2)
						{
							designerToOpen = new TemplateDesigner();
                            ((TemplateDesigner)designerToOpen).PackageItemSelected += new SelectedPackageLibraryItemHandler(MainDesigner_PackageItemSelected);
						}
						else if (i is InternalPage)
						{
							designerToOpen = new PageDesigner(i, propertyGrid1);
						}
						else if (i is Interview)
						{
							designerToOpen = new InterviewDesigner(i);
							((InterviewDesigner) designerToOpen).OpenDesigner +=new EventHandler<PackageItemEventArgs>(InterviewDesigner_OpenDesigner);
						    //FB9
                            designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_Select);}						
						else
						{	
							return;
							//throw new NotSupportedException();
						}

						designerToOpen.MdiParent = this;

                        // We need to set this here, otherwise the MDIList tool sticks with the old default name.
                        designerToOpen.Text = i.Name;
                        designerToOpen.PropertyChanged += new PropertyChangedEventHandler(templateDesignerForm_PropertyChanged);
                        designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_ItemSelected);

						designerToOpen.Show();

						// Make sure we show the window *before* setting the template - as that triggers the dsoframer
						// to load, and it doesn't work if it's not done after Show().
						if (designerToOpen is TemplateDesigner)
						{
							((TemplateDesigner) designerToOpen).SetTemplate(i as TemplateDocument);
						}
						else if(designerToOpen is ReportViewer)
						{
							((ReportViewer) designerToOpen).Navigate();
						}
                    }
				}
				finally
				{
					Cursor = Cursors.Default;
				}
			}
		}

        /// <summary>
        ///     Handles the PackageItemSelected event as raised from the TemplateDesigner, and ensures the originating
        ///     tempalte designer remains focused. Supports cross threaded UI calls.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        public void MainDesigner_PackageItemSelected(object o, SelectedPackageItemEventArgs e, int foreignPid)
        {
            // Do not accept events from other processes
            if (System.Diagnostics.Process.GetCurrentProcess().Id != foreignPid)
                return;

            // FB1871: Found problems with invoking the code to select the item in explorer control whilst dragging.
            // Possible cause was thread locking when dragging on item and an event occurs to set the focus to another.
            // Changed to perform asyncronous invocation instead, which would allow the drag to complete before we
            // try to set the focus or select a particular item on the explorer list which we are in the process of dragging.
            if( this.InvokeRequired )
            {
                ExplorerSelectItem selectdelegate = new ExplorerSelectItem( this.SelectAndFocusItem );
                this.BeginInvoke( selectdelegate, new object[ ] { e.Name, e.UniqueIdentifier } );
            }
            else
                SelectAndFocusItem( e.Name, e.UniqueIdentifier );
        }

        // Delegate used for cross thread invocation to access the explorer control.
        delegate void ExplorerSelectItem( string name, Guid itemUid );

        /// <summary>
        /// Selects the appropriate package item in the explorer control. Assumes we are in the correct UI thread 
        /// by this stage which would have been invoked via the delegate above...
        /// </summary>
        public void SelectAndFocusItem( string name, Guid itemUid )
        {
            explorerControl.SelectItem( name, itemUid );

            if( this.ActiveMdiChild != null && this.ActiveMdiChild.ActiveControl != null )
                this.ActiveMdiChild.ActiveControl.Focus( );
        }
        
       
		private void cmHelpAbout_Executed(object sender, ToolbarManagerEventArgs e)
		{
			using (About aboutForm = new About(package.VersionXml))
			{
				aboutForm.ShowDialog();
			}
		}

		private void package_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Name":
				case "CurrentFilePath":
					PackageNameBind();
					break;
			}
		}


		[DllImport("shlwapi.dll", CharSet=CharSet.Auto)]
		private static extern int PathCompactPathEx([Out] StringBuilder pszOut, string szPath, int cchMax, int dwFlags);

		private static string trimMruPath(string path)
		{
			StringBuilder sb = new StringBuilder();
			PathCompactPathEx(sb, path, 40, 0);
			return sb.ToString();
		}

		private void PackageNameBind()
		{
			StringBuilder sb = new StringBuilder();
			string fileName;
			if(pluginForSave != null && pluginTitleText != string.Empty)
			{
				Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat,Common.About.FormsTitle,"", pluginTitleText);
			}
			else if (package.CurrentFilePath != null && package.CurrentFilePath.Length > 0)
			{
				PathCompactPathEx(sb, package.CurrentFilePath, 60, 0);
				fileName = sb.ToString();
				Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat, Common.About.FormsTitle, package.Name, fileName);
			}
			else
			{
				fileName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.NotSavedTitleSuffix");
				Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat, Common.About.FormsTitle, package.Name, fileName);			
			}			
		}

		private void templateDesignerForm_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Text":
					mdiListTool1.ResetTool();
					mdiListTool1.IsDirty = true;
					Refresh();
					break;
			}
		}

		private void cmSave_Executed(object sender, ToolbarManagerEventArgs e)
		{
			if(Save())
			{
				DoOfflinePublish();
			}
		}

		private void DoOfflinePublish()
		{
			if (package != null && ConfigurationSettings.AppSettings["OfflinePublishEnabled"].ToLower() == "true")
			{
				try
				{
				OfflinePublishWrapper t = new OfflinePublishWrapper(0, package);
                ServiceProgressDialog dlg = new ServiceProgressDialog(new BackgroundWorkerBase[] { t });
				dlg.Start();
				DialogResult drPub = dlg.ShowDialog();
				if (drPub == DialogResult.Cancel)
				{
					t.Abort(false);			
				}				
			}			
				finally
				{
					package.SetNoChanges();
				}
			}			
		}

		private bool Save()
		{
            if( package == null )
                return false;
            
            if(PreSave != null)
				PreSave();

            // save the current version information inmemory
            PackageVersionInfo packageVersionInfo;
            if (package.VersionXml != null && package.VersionXml.Length > 0)
            {
                packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);
            
                PackageVersionInfo.ModifyPackageInfo(ref packageVersionInfo,
                        System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                        package.Name,
                        package.Description,
                        Common.About.AssemblyVersion);
                package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
            }
            else
            {
                package.VersionXml = PackageVersionInfo.CreatePackageInfo(
                    package.Description,
                    package.Name,
                    Common.About.AssemblyVersion, System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);
            }
            
			// Do an index repair to resolve issue #2260 (and #1802) where old packages with questions imported from even older packages are missing from the internal dictionary of UID -> Question
			package.ReindexQuestions();
			package.TempOfflineId = Guid.NewGuid();

			if (pluginForSave != null)
			{
				return DoPluginSave(pluginForSave, false);
			}
			else
			{
                // Do we have a current file & path?
                if (package.CurrentFilePath != null)
                {
                    try
                    {
                        SaveDesigners( );
                        RefreshActiveDesigner( );
                        
                        // physical save
                        Package.SaveToDisc( ref ipFileStream, package );
                        
                        return OdmaSave( package.CurrentFilePath );
                    }
                    catch( Exception ex )
                    {
                        MessageBox.Show( ex.Message, Common.About.FormsTitle );
                        return false;
                    }
                }
                else
                {
                    RefreshActiveDesigner( );
                    return SaveAs( );
                }
			}
		}

        private string SaveAsStandardDialog( string defaultFilename, string temporarySaveLocation, Guid originalGuid )
        {
            saveFileDialog1.FileName = defaultFilename;
            DialogResult dr;

            try
            {
                dr = saveFileDialog1.ShowDialog( );
            }
            catch // If there is no file at the location of the defaultFileName then an exception happens before the dialog appears.
            {
                saveFileDialog1.FileName = null;
                dr = saveFileDialog1.ShowDialog( );
            }

            if( dr == DialogResult.OK )
            {
                try
                {
                    // Close current file stream. We might be saving to the same file, hence need to do this first.
                    if( ipFileStream != null )
                        ipFileStream.Close( );

                    // Move the temporary file to our new file.
                    ipFileStream = Package.MoveOnDisc( temporarySaveLocation, saveFileDialog1.FileName );

                    mruList.AddFilename( saveFileDialog1.FileName );
                    UpdateMruItems( );
                    return saveFileDialog1.FileName;
                }
                catch( Exception ex )
                {
                    // Immediately restore the file stream on the original file to maintain our write access to the file.
                    try
                    {
                        ipFileStream = Package.OpenOnDisc( package.CurrentFilePath );
                    }
                    catch
                    {
                        // If unable to open the original file again, we simply lose our exclusive access.
                        if( ipFileStream != null )
                        {
                            ipFileStream.Close( );
                            ipFileStream = null;
                        }
                    }
                    MessageBox.Show( ex.Message, Common.About.FormsTitle );
                }
            }

            package.UniqueIdentifier = originalGuid;
            return null;
        }

        private void cmSaveAs_Executed( object sender, ToolbarManagerEventArgs e )
		{
			if (SaveAs())
			{
				pluginForSave = null;	
				DoOfflinePublish();
			}
		}

		public static string LegaliseFilename(string filename)
		{
			foreach(char c in Path.GetInvalidFileNameChars())
			{
				filename = filename.Replace(c, '_');
			}
			filename = filename.Replace('\\', '_');
			filename = filename.Replace('/', '_');
			return filename;
		}

		private bool SaveAs( )
		{
            if( package == null )
                return false;

			// Save to a temporary location.
            FileStream tempFileStream = null;
            string tempLoc = Path.GetTempFileName( );
			string defaultFilename = LegaliseFilename(string.Format("{0}.ip", package.Name));
			package.TempOfflineId = Guid.NewGuid();
            Guid originalGuid = package.UniqueIdentifier;

            try
            {
                try
                {
                    //we do a save to disk here because we dont know which dialog will be displayed to the user (ODMA or File System)
                    //if it is ODMA we lose control and need to have the latest version saved to disk already
                    SaveDesigners( );
                    packageVersionInfo = null;
                    try
                    {
                        packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);
                    }
                    catch { }

                    try
                    {
                        if( packageVersionInfo == null )
                            package.VersionXml =
                            PackageVersionInfo.CreatePackageInfo(
                                package.Description,
                                package.Name,
                                Common.About.AssemblyVersion,
                                System.Security.Principal.WindowsIdentity.GetCurrent( ).Name );
                        else
                        {
                            PackageVersionInfo.CopyPackageInfo(
                                System.Security.Principal.WindowsIdentity.GetCurrent( ).Name,
                                package.Name,
                                package.Description,
                                Common.About.AssemblyVersion,
                                packageVersionInfo );

                            package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                        }
                    }
                    catch { }

                    // Save to temp file....
                    tempFileStream = File.OpenWrite( tempLoc );
                    Package.SaveToDisc( ref tempFileStream, package, tempLoc, true, false );
                }
                catch( Exception ex )
                {
                    MessageBox.Show( ex.Message, Common.About.FormsTitle );
                    return false;
                }
                finally
                {
                    if( tempFileStream != null )
                        tempFileStream.Close( );
                }

                // Ask odma to do its convoluted 'SaveAs' business
                //we pass in the original Guid because if it is a normal windows save dialog and the user cancels, we need to keep reset
                //it to the original Guid
                string finalLocation = OdmaSaveAs( tempLoc, defaultFilename, originalGuid );
                if( finalLocation != null && finalLocation.Length > 0 )
                {
                    pluginTitleText = string.Empty; // otherwise a previous open from plugin will have its title text used.
                    package.CurrentFilePath = finalLocation.Trim( ).Trim( '\0' );
                    //->FB887 MR 13/Jun/07
                    // Hash over package is calculated in "Package.SaveToDisc(...)", but package object will be 
                    // modified again when asking for the final filename. Hence recalc the hash...
                    package.SetNoChanges( );
                    //<-FB887 

                    return true;
                }
                else
                {
                    return false;
                }

            }
            finally
            {
                // Remove working file in temp folder.
                if( !String.IsNullOrEmpty( tempLoc ) )
                    File.Delete( tempLoc );
            }
		}

        protected void package_EnsureDesignerSaved(object sender, PackageItemEventArgs e)
        {
            SaveDesigner(((PackageItem)e.Item).Name);
        }

        private void SaveDesigner(string name)
        {
            if (package != null && uiToolbarManager_.EzMdiChildren.Length > 0)
            {
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
                {
                    // Save the designer by matching the given name
                    if (f is DesignerBase && ((DesignerBase)f).ItemBeingEdited.Name == name)
                    {
                        ((DesignerBase)f).Save();
                    }
                }
            }
        }

		private void SaveDesigners()
		{
            if (package != null && uiToolbarManager_.EzMdiChildren.Length > 0)
			{
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
				{
					if (f is DesignerBase)
					{
						((DesignerBase) f).Save();
					}
				}
			}
		}

		private void cmOpen_Executed(object sender, ToolbarManagerEventArgs e)
		{
			Open();
		}


        private void Open( string filename )
        {
            if( !IsOkayToUnloadPackage( ) )
                return;

            try
            {
                if( explorerControl != null )
                    explorerControl.Suspend( );

                UnloadPackage( );

                String[ ] filenamePart = filename.Split( ' ' );

                // Test for integration 
                // Expect to find a path as the first parameter and then an identifier
                // e.g. SharePoint http://.../file.ip 
                String[ ] parameters = filename.Split( new char[ ] { ' ', '\t' } );
                if( parameters.Length > 1 && parameters[ 0 ] == "SharePoint" )
                {
                    // Remove the 'SharePoint' keyword. The rest should be a valid path into SP
                    OpenPackageFromIntegration( filename.Substring( 11 ).Trim( ) );
                }
                else
                {
                    // Open package passing in our file stream and read/write access, and other processes can only have read access.
                    package = Package.ReadFromDisc( filename, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read );
                    pluginForSave = null;
                    pluginTitleText = String.Empty;
                    TestPackageVersion( package.VersionXml, filename );
                    mruList.AddFilename( filename );
                }

                MaybeInvokePackageFixer( true );

                package.ReindexQuestions( );
            }
            catch( Exception ex )
            {
                string msg = string.Format( ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString( "Perfectus.Client.MainDesigner.PackageOpenException" ), ex.Message );
                msg = msg.Replace( "\\n", "\n" );
                msg = msg.Replace( "\\t", "\t" );
                MessageBox.Show( msg, Common.About.FormsTitle );

                mruList.RemoveFilename( filename );
                UpdateMruItems( );

                // Create new package to display.....
                this.NewPackage( );
            }

            if( explorerControl != null )
                explorerControl.Resume( );

            // Reset UI
            ResetUI( );
            Cursor = Cursors.Default;
        }

        private void OpenPackageFromIntegration( string filename )
        {
            // We only support SharePoint at the moment
            foreach( IStudioPlugin p in m_plugins )
                if( p is IOpenSaveHandler && ( (IOpenSaveHandler)p ).Name == "SharePoint" )
                {
                    IOpenSaveHandler sharePointPlugin = (IOpenSaveHandler)p;

                    // The IOpenSaveHandler interface down't support none-interactive
                    // loading. Therefore reflect over it...
                    MethodInfo invoke = p.GetType( ).GetMethod( "OpenPackageWithURL" );

                    String url = System.Web.HttpUtility.UrlDecode( filename );

                    if( invoke != null )
                    {
                        package = Package.OpenFromStream( (Stream)invoke.Invoke( sharePointPlugin, new object[ ] { url } ) );
                        TestPackageVersion( package.VersionXml, null );

                        pluginForSave = sharePointPlugin;
                        pluginTitleText = sharePointPlugin.IpManagerTitleText;
                        mruList.AddFilename( String.Format( "SharePoint {0}", url ) );
                        break;
                    }
                    else
                    {
                        mruList.RemoveFilename( String.Format( "SharePoint {0}", url ) );
                        throw new Exception( "Incompatible integration module" );
                    }
                }
        }

        private void Open( )
        {
            if( IsOkayToUnloadPackage( ) )
            {
                string previousOdmaDoc = odmaDocid;
                string filename = OdmaGetOpenFilePath( );
                string oldfilename = package != null ? package.CurrentFilePath : String.Empty;

                if( filename != null )
                {
                    // Dispose of current package.
                    UnloadPackage( );

                    try
                    {
                        Cursor = Cursors.WaitCursor;

                        // Open package passing in our file stream and read/write access, and other processes can only have read access..
                        package = Package.ReadFromDisc( filename, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read );

                        TestPackageVersion( package.VersionXml, filename );
                        MaybeInvokePackageFixer( true );

                        package.ReindexQuestions( );
                        pluginForSave = null;
                        pluginTitleText = string.Empty;

                        // Assume a successful open, so close the one we had open earlier
                        if( previousOdmaDoc != null && previousOdmaDoc.Length > 0 )
                        {
                            OdmaClose( previousOdmaDoc, false );
                        }
                    }
                    catch( Exception ex )
                    {
                        string msg = string.Format( ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString( "Perfectus.Client.MainDesigner.PackageOpenException" ), ex.Message );
                        msg = msg.Replace( "\\n", "\n" );
                        msg = msg.Replace( "\\t", "\t" );
                        MessageBox.Show( msg, Common.About.FormsTitle );

                        mruList.RemoveFilename( filename );
                        UpdateMruItems( );

                        // If the ODMA open successfully opened the file, we'll need to close it, and leave the original one open.
                        if( previousOdmaDoc != odmaDocid )
                        {
                            OdmaClose( odmaDocid, false );
                            odmaDocid = previousOdmaDoc;
                        }

                        // Create new package to display.....
                        this.NewPackage( );
                    }

                    // Reset UI. NOTE: The reset function does not clear the UI if the package is null. A new case has raised to address this.
                    ResetUI( );
                    Cursor = Cursors.Default;
                }
            }
        }


        private void TestPackageVersion(String packageVersionXML, String fileName)
        {
            // http://odma.info/downloads/odma20.htm

            packageVersionInfo = null;
            if ( packageVersionXML != null &&
                packageVersionXML.Length > 0 &&
                packageVersionXML.StartsWith ("<?xml" ))
            packageVersionInfo = PackageVersionInfo.Create(packageVersionXML);

        if (packageVersionInfo == null)
        {
            // Only relevant for packages of version 5.6
            packageVersionInfo = PackageVersionInfo.MaybeLoadFromDisk(fileName);
        }

            if (packageVersionInfo == null)
            {
                string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageHasNoVersion"), Common.About.AssemblyVersion);

                if (DialogResult.Yes != MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.UserCancelledLoading"));

                // We might fail in writing to the file, but that doesn't matter as the
                // use then would fail to save the package anyway
                try
                {
                    package.VersionXml = 
                    PackageVersionInfo.CreatePackageInfo(
                        package.Name,
                        "5.0.0.0",
                        System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                }
                catch { }
            }
            else
            {
                PackageVersionInfo.packageVersion result = PackageVersionInfo.TestVersionCompatibility(packageVersionInfo, Common.About.AssemblyVersion);
                if (result == PackageVersionInfo.packageVersion.packageIsOlder)
                {
                    string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfOlderVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion);
                    if (DialogResult.Yes != MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.UserCancelledLoading"));
                    package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                }
                else
                    if (result == PackageVersionInfo.packageVersion.packageIsNewer)
#if !DEBUG
                                    throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfNewerVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion));
#else
                        MessageBox.Show("Debug version bypasses following sanity check: " +
                            string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfNewerVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion));
#endif
            }
        }

		private void cmNew_Executed(object sender, ToolbarManagerEventArgs e)
		{
			NewPackage();
		}

		private void NewPackage()
		{
			if( IsOkayToUnloadPackage( ) )
			{
                UnloadPackage( );

				pluginForSave = null;
				pluginTitleText = string.Empty;
				
				package = ConstructNewPackage();
				OdmaClose(odmaDocid, true);
				ResetUI();
			}
		}

        private Package ConstructNewPackage( )
        {
            string workingFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string basePackageFileName = string.Format(@"{0}{1}default.ip", workingFolder, Path.DirectorySeparatorChar);
            
            if (File.Exists(basePackageFileName))
            {
                try
                {
                    // Open package passing in our file stream and read/write access, and other processes can only have read access..
                    Package p = Package.ReadFromDisc( basePackageFileName, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read );
                    // Give it a new ID
                    p.UniqueIdentifier = Guid.NewGuid();
                    p.CurrentFilePath = null;
                    p.ResetName();
                    
                    return p;
                }
                catch (Exception ex)
                {
                    string msg = String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.LoadDefaultPackageFailed"), basePackageFileName, ex.Message);
                    msg = msg.Replace("\\n", "\n");
                    MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return new Package();
                }
            }
            else
            {
                return new Package();
            }
        }

		private void ResetUI()
		{
			Cursor = Cursors.WaitCursor;
			try
			{
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
				{
					f.Close();
				}
				if (package != null)
				{
					HookupPackage(package);
				}
				propertyGrid1.SelectedObject = null;

				UpdateMruItems();

			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

        private void designerToOpen_Select(object sender, PackageItemEventArgs e)
        {
            explorerControl.Select();
            explorerControl.Focus();
        }

		private void designerToOpen_ItemSelected(object sender, PackageItemEventArgs e)
		{
			explorerControl.SelectItem(e.Item);
        }

		private void package_ItemDeleted(object sender, PackageItemDeletedEventArgs e)
		{
			cmInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
		}

		private void cmShowLoadQuestionsTaskBar_Executed(object sender, ToolbarManagerEventArgs e)
		{
			twLoadQuestions.Visible = true;
		}

		private void cmShowSharedLibraryControlTaskBar_Executed(object sender, ToolbarManagerEventArgs e)
		{
			if(! twSharedLibrary.Visible)
			{
				try
				{
					Cursor = Cursors.WaitCursor;
                    string sharedLibraryPaneLoadedMessage = string.Empty;
					bool activated = sharedLibraryControl.LoadLibrary(out sharedLibraryPaneLoadedMessage);

                    if (activated)
                    {
                        twSharedLibrary.Visible = true;
                    }
                    else
                    {
                        twSharedLibrary.Visible = false;
                    }
		        }
                catch 
                {
                    twSharedLibrary.Visible = false;
                }
				finally
				{
					Cursor = Cursors.Default;
				}
			}
		}

        private void MaybeInvokePackageFixer(bool param_critical_test_only)
        {
            // Find PackageFixer plugins
            foreach (IStudioPlugin plugin in Plugins)
            {
                if (plugin is IPackageFixer)
                {
                    String protocol = System.String.Empty;
                    if (((IPackageFixer)plugin).NeedFixing(package, param_critical_test_only, ref protocol))
                    {
                        StringBuilder message = new StringBuilder(String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixFoundProblem"), plugin.PluginName));
                        message.Append(System.Environment.NewLine + protocol + System.Environment.NewLine );
                        message.Append(System.Environment.NewLine + ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixAskRepair"));
                        DialogResult result = MessageBox.Show(message.ToString(), ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairNeeded"), MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            protocol = System.String.Empty;
                            if (((IPackageFixer)plugin).Fix(ref package, param_critical_test_only, ref protocol))
                            {
                                message = new StringBuilder(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairSucceeded"));
                                message.Append(protocol);
                            }
                            else
                            {
                                message = new StringBuilder(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairFailed"));
                                message.Append(protocol);
                            }
                            MessageBox.Show(message.ToString(), "Result", MessageBoxButtons.OK);
                        }
                    }
                    else
                        if (!param_critical_test_only)
                        MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairNotNeeded"), 
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixPackedResult"),
                            MessageBoxButtons.OK);
                }
            }
        }


        void cmFixPackage_Executed(object sender, ToolbarManagerEventArgs e)
        {
            MaybeInvokePackageFixer(false);

            explorerControl.ResetTreeWithCollapse();
        }

		private void cmShowClauseReport_Executed(object sender, ToolbarManagerEventArgs e)
		{	
			DoShowClauseReport();
		}

		private void DoShowClauseReport()
		{
			ReportViewer designerToOpen = new ReportViewer(ReportViewer.ReportType.AllClauses);	
			designerToOpen.MdiParent = this;
			designerToOpen.Show();			
			designerToOpen.Navigate();					
			designerToOpen.PropertyChanged += new PropertyChangedEventHandler(templateDesignerForm_PropertyChanged);
			designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_ItemSelected);
		}

		private void cmPublish_Executed(object sender, ToolbarManagerEventArgs e)
		{
			DoPublish();
		}

		private void DoPublish()
		{
			SaveDesigners();

            bool hasServer = package.Servers.Count > 0;
            bool hasInterview = package.Interviews.Count > 0;

            if (package != null && hasServer && hasInterview)
			{
                String packageVersionInfoXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                if (packageVersionInfoXml != null && packageVersionInfoXml.Length > 0)
                    package.VersionXml = packageVersionInfoXml;

				PublishDialog dlg = new PublishDialog(package);
                if (packageVersionInfoXml != null && packageVersionInfoXml.Length > 0)
                    dlg.PackageVersionXml = packageVersionInfoXml;

				dlg.ShowDialog();
			}
            else
            {
                string errorMessage = "";
                if (!hasInterview)
                {
                    errorMessage += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PleaseAddAnInterview") + "\n";
                }
                if (!hasServer)
                {
                    errorMessage += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PleaseAddAProcessManager");
                }

                MessageBox.Show(errorMessage, Common.About.FormsTitle);
            }
		}

		private void splashTimer_Tick(object sender, EventArgs e)
		{
            splashTimer.Stop();
            if ( ! splashForm.IsDisposed )
			    splashForm.Dispose();
		}


		private bool IsOkayToUnloadPackage()
		{
            foreach (Form f in uiToolbarManager_.EzMdiChildren)
			{
				f.Close();
				if (f.Visible)
				{
					return false;
				}
			}

			if (package != null)
			{
                bool retval = true;
				if (package.HasChanges())
				{
					DialogResult dr = MessageBox.Show(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.ConfirmSaveChanges"), package.Name), Common.About.FormsTitle, MessageBoxButtons.YesNoCancel);
					switch (dr)
					{
						case DialogResult.Yes:
							if(Save())
							{
								DoOfflinePublish();
							    retval = true;
							}
							else
							{
							    retval = false;
							}
					        break;
						case DialogResult.No:
							retval = true;
					        break;
						default:
						case DialogResult.Cancel:
							retval = false;
					        break;
					}
				}
				else
				{
					retval = true;
				}

                if (retval && pluginForSave != null)
                {
                    pluginForSave.PackageUnloaded();
                }   
			    return retval;
			}
			else
			{
				return true;
			}
		}

        /// <summary>
        /// Disposes of package, filestream and shutdown plugins.
        /// </summary>
        private void UnloadPackage( )
        {
            // Dispose of package.
            if( package != null )
            {
                package.Dispose( );
                package = null;
            }

            // Ensure the package file stream is closed
            if( ipFileStream != null )
            {
                ipFileStream.Flush( );
                ipFileStream.Close( );
                ipFileStream = null;
            }
        }


		private void cmExit_Executed(object sender, ToolbarManagerEventArgs e)
		{
            // Close this form (The recommended function call which will cause the closing event
            // on this form.
            this.Close( );
		}

        private void MainDesigner_Closing( object sender, CancelEventArgs e )
		{
			if( !IsOkayToUnloadPackage( ) )
				e.Cancel = true;
			else
			{
                UnloadPackage( );

                // Copied from the disposing function...
                if( m_plugins != null )
                {
                    foreach( IStudioPlugin p in m_plugins )
                    {
                        try
                        { p.Shutdown( ); }
                        // Intentional 
                        catch { }
                    }
                }

                CleanUpOdma( );
				mruList.SaveList();
                //Properties.Settings.Default.Studio_Width = this.Size.Width;
                //Properties.Settings.Default.Studio_Height = this.Size.Height;
                //Properties.Settings.Default.Save();

			}
		}

        private void CleanUpOdma()
        {
            OdmaCloseCurrent();

            if (IsOdmaAvailable())
            {
                OdmaImports.Unregister_DMS();
            }
        }

		private void explorerControl_Publish(object sender, EventArgs e)
		{
			DoPublish();
		}

		private void mruButtonClick(object sender, EventArgs e)
		{
			if (sender is ButtonTool)
			{
				string filename = ((ButtonTool) sender).Tag.ToString();
				Open(filename);
			}
		}

		private void cmInterviewDiagram_Executed(object sender, ToolbarManagerEventArgs e)
		{
			if (package != null && package.Interviews.Count > 0)
			{
				DoOpenDesigner(package.Interviews[0], PrefferedDesigner.Default);
			}
		}

		private void package_ItemCreated(object sender, PackageItemEventArgs e)
		{
			if (e.Item is Interview)
			{
				cmInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
			}
		}

		private void uiToolbarManager1_EzMdiChildActivate(object sender, EventArgs e)
		{
			RefreshActiveDesigner();
		}




		private void RefreshActiveDesigner()
		{
            if (uiToolbarManager_.ActiveEzMdiChild is DesignerBase)
			{
                ((DesignerBase)(uiToolbarManager_.ActiveEzMdiChild)).UpdateUI();

				if(ActiveDesignerChanged != null)
				{
                    ActiveDesignerChanged((DesignerBase)uiToolbarManager_.ActiveEzMdiChild);
				}
			}
		}

		private void explorerControl_SimpleOutcomeDrop(object sender, PackageItemEventArgs e)
		{
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
			{
                ((TemplateDesigner)(uiToolbarManager_.ActiveEzMdiChild)).WrapSelectionWithSimpleOutcome((SimpleOutcome)e.Item);
			}
		}

		private void explorerControl_OutcomeDrop(object sender, PackageItemEventArgs e)
		{
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
			{
                ((TemplateDesigner)(uiToolbarManager_.ActiveEzMdiChild)).WrapSelectionWithOutcome((Outcome)e.Item);
			}
		}

		public static DisplayTypeExtensionInfoCollection DisplayTypeExtensions
		{
			get { return m_displayTypeExtensions; }
		}

        public static Collection<IStudioPlugin> Plugins
		{
			get { return m_plugins;}
		}

		private void LoadPlugins()
		{
			//Retrieve a plugin collection using our custom Configuration section handler
            m_plugins = (Collection<IStudioPlugin>)ConfigurationSettings.GetConfig("plugins");			
			m_displayTypeExtensions = (DisplayTypeExtensionInfoCollection)ConfigurationSettings.GetConfig("displayTypeExtensions");
			m_formatStringExtensions = (FormatStringExtensionInfoCollection)ConfigurationSettings.GetConfig("formatStringExtensions");

			// Setup click handlers for all OpenSaveHandler type plugins
			EventHandler openHandler = new EventHandler(OpenPlugin_Click);
			EventHandler saveAsHandler = new EventHandler(SaveAsPlugin_Click);
			
			foreach(IStudioPlugin p in m_plugins)
			{
				try
				{
					p.Init(this);
				}
				catch
				{}
				// If the plugin is one that provides file open and save features, add it to the File menu.
				if (p is IOpenSaveHandler)
				{
					IOpenSaveHandler osh = (IOpenSaveHandler)p;
					AddOpenSavePluginMenuItems(openHandler, saveAsHandler, osh);
				}
                if (p is IPackageFixer)
                {
                    IPackageFixer pfx = (IPackageFixer)p;
                    this.btFixPackage.Visible = true; ;
                }
                if (p is IConfiguration)
                {
                    Perfectus.Client.Configuration.Handler.AddPlugin(p as IConfiguration);
                    this.btCustomize.Visible = true;
                }
            }
			explorerControl.Plugins = m_plugins;
		}

        void configuration_button_Click(object sender, EventArgs e)
        {
            Perfectus.Client.Configuration.Handler.ShowOptionForm();
        }

		private void AddOpenSavePluginMenuItems(EventHandler openHandler, EventHandler saveAsHandler, IOpenSaveHandler osh)
		{
			if (osh.SupportsOpen)
			{// Make an 'open from x' menu item
				ButtonTool bt = new ButtonTool();
				bt.Click += openHandler;
				bt.Text = osh.OpenFileMenuName;
				bt.Tag = osh;
				if (osh.FileMenuOpenIcon != null)
				{
					bt.Image = osh.FileMenuOpenIcon;
				}
	
				int openPosition = ptMainFile.SubBand.Tools.IndexOf(btFileOpen);
				ptMainFile.SubBand.Tools.Insert(openPosition + 1, bt);
			}

			if (osh.SupportsSaveAs)
			{// Make a 'save as to x' menu item
				ButtonTool bt = new ButtonTool();
				bt.Click += saveAsHandler;
				bt.Text = osh.SaveAsFileMenuName;
				bt.Tag = osh;
				if (osh.FileMenuSaveAsIcon != null)
				{
					bt.Image = osh.FileMenuSaveAsIcon;
				}
	
				int saveAsPosition = ptMainFile.SubBand.Tools.IndexOf(btFileSaveAs);
				ptMainFile.SubBand.Tools.Insert(saveAsPosition + 1, bt);

			}
		}


		private void OpenPlugin_Click(object sender, EventArgs e)
		{
			Cursor = Cursors.WaitCursor;
			if (IsOkayToUnloadPackage())
			{
                ButtonTool bt = (ButtonTool)sender;
				IOpenSaveHandler osh = (IOpenSaveHandler)bt.Tag;
				try
				{// If a plugin's 'open from' menu item is clicked, ask it to open a stream and give it to us
					// (The plugin itself is in the Tag of the menu item)
					Stream s = osh.OpenPackage();

                    if (s != null)
                    {
                        // Dispose of current package stuff...
                        UnloadPackage();

                        using (s)
                        {
                            package = Package.OpenFromStream( s );

                            TestPackageVersion(package.VersionXml, null);

                            package.SetNoChanges();
                            pluginForSave = osh;
                            pluginTitleText = osh.IpManagerTitleText;

                            // SharePoint only;
                            // We are misusing the window title, which is expected to be 
                            // 'SharePoint http://.../file.ip'
                            // --Other possibility is to reflect over the IOpenSaveHandler object and to
                            // --get the URL from the location property.
                            if (pluginTitleText.StartsWith("SharePoint "))
                                mruList.AddFilename(pluginTitleText);

                            MaybeInvokePackageFixer(true);

                            package.ReindexQuestions();

                            ResetUI();
                        }
                    }
				}
				catch (Exception ex)
				{
                    if ( package!= null)
                        package.SetNoChanges();
                            
					string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PluginError"), osh.Name, ex.Message);
					msg = msg.Replace("\\n", "\n");
					MessageBox.Show(msg, Common.About.FormsTitle);
				}
				finally
				{
					Cursor = Cursors.Default;
				}

			}
		}

		private void SaveAsPlugin_Click(object sender, EventArgs e)
		{
			ButtonTool bt = (ButtonTool)sender;
			IOpenSaveHandler osh = (IOpenSaveHandler)bt.Tag;
			DoPluginSave(osh, true);			
		}
		
		private bool DoPluginSave(IOpenSaveHandler plugin, bool saveAs)
		{
			MemoryStream ms = new MemoryStream();
			try
			{
				if (package != null && plugin != null)
				{
					Package.SaveToStream(package, ms);
					ms.Seek(0, SeekOrigin.Begin);
					if (saveAs)
					{                       
						bool saveAsRetVal = plugin.SavePackageAs(ms,package.Name, LegaliseFilename(package.Name));
						if(saveAsRetVal)
						{
							pluginForSave = plugin;
							pluginTitleText = plugin.IpManagerTitleText;

                            if (pluginTitleText.StartsWith("SharePoint "))
                            {
                                mruList.AddFilename(pluginTitleText);
                                UpdateMruItems();
                            }
                            
							PackageNameBind();
                            //FB887
                            package.SetNoChanges();
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return plugin.SavePackage(ms);
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PluginError"), plugin.Name, ex.Message);
				msg = msg.Replace("\\n", "\n");
				MessageBox.Show(msg, Common.About.FormsTitle);
#if DEBUG
				MessageBox.Show(ex.StackTrace);
#endif
				return false;
			}
			finally
			{
				if (ms != null)
				{
					ms.Close();
				}
			}			
		}
		

		private void cmHelp_Executed(object sender, ToolbarManagerEventArgs e)
		{
			//PF-3181 change help file to help website
            //DoOpenHelp();
            System.Diagnostics.Process.Start(ConfigurationSettings.AppSettings["HelpURL"]);
		}

        //private void DoOpenHelp()
        //{
        //    // Make sure it's not already open
        //    foreach (Form f in uiToolbarManager_.EzMdiChildren)
        //    {
        //        if (f is HelpViewer)
        //        {
        //                f.Activate();
        //                f.Show();
        //                return;
        //        }
        //    }
			
        //    string helpPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + Path.DirectorySeparatorChar + "PerfectusUserGuide.pdf";
        //    HelpViewer viewer = new HelpViewer(helpPath);
        //    viewer.MdiParent = this;			
        //    viewer.Show();
        //    viewer.Refresh();
        //}

		private void PopulatePackageWithDisplayTypeExtensions()
		{
			int maxExtensionCount = 50;
			if (m_displayTypeExtensions != null && package != null && package.ExtensionMapping.Count < maxExtensionCount)
			{
				// If there are no existing mappings, simply copy all our plugins into the package.
				if (package.ExtensionMapping.Count == 0)
				{
					for(int i = 0; i<m_displayTypeExtensions.Count && i < maxExtensionCount; i++)
					{
						int extensionBase = (int)QuestionDisplayType.Extension1;
						package.ExtensionMapping.Add((QuestionDisplayType)(extensionBase + i), m_displayTypeExtensions[i]);
					}
				}
				else
				{
					// If there are alread extensions saved with this package, only add ones that aren't there yet.
					int extensionBase = (int)(QuestionDisplayType.Extension1) + package.ExtensionMapping.Count;
					int extensionCount = 0;
					foreach(DisplayTypeExtensionInfo dtei in m_displayTypeExtensions)
					{
						bool alreadyThere = false;
						foreach(QuestionDisplayType key in package.ExtensionMapping.Keys)
						{
						    DisplayTypeExtensionInfo pdtei = package.ExtensionMapping[key];
							// If it's already been loaded, skip
							if (dtei.ServerSideDisplayPluginId == pdtei.ServerSideDisplayPluginId)
							{
								pdtei.LocalisedDisplayName = dtei.LocalisedDisplayName;
                                // Update the package one with the details from this machine
                                package.ExtensionMapping[key] = dtei; 
								alreadyThere = true;
								break;
							}
						}
						if (alreadyThere)
						{
							continue;
						}
						else
						{
							package.ExtensionMapping.Add((QuestionDisplayType)(extensionBase + extensionCount), dtei);
							extensionCount++;	
						}				
					}
				}
			}
		}

		
		private void PopulatePackageWithFormatStringExtensions()
		{
			int maxExtensionCount = 50;
			if (m_formatStringExtensions != null && package != null && package.FormatStringExtensionMapping.Count < maxExtensionCount)
			{
				// If there are no existing mappings, simply copy all our plugins into the package.
				if (package.FormatStringExtensionMapping.Count == 0)
				{
					for(int i = 0; i<m_formatStringExtensions.Count && i < maxExtensionCount; i++)
					{
						package.FormatStringExtensionMapping.Add(m_formatStringExtensions[i].ServerSideFormatterPluginKey,m_formatStringExtensions[i]);
					}
				}
				else
				{
					// If there are alread extensions saved with this package, only add ones that aren't there yet.
					foreach(FormatStringExtensionInfo fsei in m_formatStringExtensions)
					{
						bool alreadyThere = false;
						foreach(FormatStringExtensionInfo pfsei in package.FormatStringExtensionMapping.Values)
						{
							// If it's already been loaded, skip
							if (pfsei.ServerSideFormatterPluginKey == fsei.ServerSideFormatterPluginKey)
							{
								pfsei.LocalisedDisplayName = fsei.LocalisedDisplayName;
								alreadyThere = true;
								break;
							}
						}
						if (alreadyThere)
						{
							continue;
						}
						else
						{
							package.FormatStringExtensionMapping.Add(fsei.ServerSideFormatterPluginKey,fsei);
						}				
					}
				}
			}
		}

		private void btOdmaStatus_Click(object sender, EventArgs e)
		{
			odmaTraceBuilder.Append(string.Format("  Info window invoked:\n  Current docid = [{0}]\n", odmaDocid.Trim().Trim('\0')));
			
			DebugInfo infoForm = new DebugInfo();
			infoForm.TextInfo = odmaTraceBuilder.ToString().Replace("\n", "\r\n");
			infoForm.ShowDialog();
		}

		private void InterviewDesigner_OpenDesigner(object sender, PackageItemEventArgs e)
		{
			DoOpenDesigner(e.Item, PrefferedDesigner.Default);
		}

		private void sharedLibraryControl_SharedLibraryItemAdded(object o, SharedLibraryItemAddedEventArgs e)
		{
			explorerControl.UpdateSelectedLibraryItemNode(e.AddedLibraryItem);
		}


        private String _currentPropertyName;
        void propertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            _currentPropertyName = e.NewSelection.PropertyDescriptor.Name;

        }


        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            DialogResult result = DialogResult.Yes;

            if (_currentPropertyName.Length > 0)
            {
                // TextQuestionQueryValue
                if (e.OldValue != null && e.OldValue.GetType() == typeof(TextQuestionQueryValue))
                {
                    TextQuestionQueryValue oldValue = e.OldValue as TextQuestionQueryValue;
                    TextQuestionQueryValue newValue = e.ChangedItem.Value as TextQuestionQueryValue;

                    if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query &&
                        newValue.ValueType != TextQuestionQueryValue.TextQuestionQueryValueType.Query)
                    {
                        // case 1: User removes query, but still has a binding. The front-end already asked if that was intended

                        // case 2: Query was replaced with 'normal' text. Let the user confirm

                        // case 3a: Query was removed (e.g. <backspace>)
                        // case 3b: Query was removed by removing query object in editor and first binding was empty

                        if (newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.NoValue || //  cleared out
                            (newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            (newValue.TextValue == null || newValue.TextValue.IndexOf("<text") != 0)))     // replaced with none-binding
                            result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveQuery"),
                                GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    // case 3: Binding object becomes 'normal text'

                    else if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            oldValue.TextValue != null &&
                            newValue.TextValue != null &&
                            oldValue.TextValue.IndexOf("<text") == 0 &&
                            newValue.TextValue.IndexOf("<text") == -1)
                        result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveBinding"),
                                GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    else if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question &&
                             newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                             newValue.TextValue != null &&
                             newValue.TextValue.IndexOf("<text") != 0
                        )
                        result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveQuestion"),
                            GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                    if (result != DialogResult.Yes)
                        try
                        {
                            PackageItem selectedItem = (PackageItem)((PropertyGrid)s).SelectedObject;

                            // Plugins have faked properties...
                            if (selectedItem is PluginBase)
                            {
                                PluginBase myCC = selectedItem as PluginBase;
                                if (myCC != null)
                                {
                                    myCC[_currentPropertyName] = oldValue;
                                }
                            }
                            else
                            {
                                // Other properties items can be invoked...
                                PropertyInfo p = selectedItem.GetType().GetProperty(_currentPropertyName);
                                MethodInfo method = p.GetSetMethod();
                                method.Invoke(selectedItem, new object[] { oldValue });
                                return;
                            }
                        }
                        catch { }
                }
            }
        }
    

        /// <summary>
        /// event to handle movement of the pop-up dialog when making a new outcome. 
        /// This enables the screen refresh when dialog is moved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void explorerControl_PopupDialogMoved(object sender, PackageItemEventArgs e)
        {
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
            {
                uiToolbarManager_.ActiveEzMdiChild.Refresh();
            }
        }

        private void MainDesigner_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild == null)
                this.tabForms.Visible = false;
            else
            {
                if (!(ActiveMdiChild is DesignerBase))
                    return;

                if ( ActiveMdiChild is TemplateDesigner )
                {
                    TemplateDesigner designer = ActiveMdiChild as TemplateDesigner;
                    if (designer.InternalDisposal || designer.Disposing )
                        return;
                }

                this.ActiveMdiChild.WindowState = FormWindowState.Maximized; // Child form always maximized

                // If child form is new and no has tabPage, create new tabPage
                if (this.ActiveMdiChild.Tag == null)
                {
                    // Add a tabPage to tabControl with child form caption
                    TabPage tp = new TabPage(this.ActiveMdiChild.Text);
                    tp.Tag = this.ActiveMdiChild;
                    tp.Parent = this.tabForms;
                    this.tabForms.SelectedTab = tp;

                    uiToolbarManager_.Add(this.ActiveMdiChild);

                    this.ActiveMdiChild.Tag = tp;

                    ((DesignerBase)this.ActiveMdiChild).PropertyChanged += new PropertyChangedEventHandler(ItemBeingEdited_PropertyChanged);
                    this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);
                }

                if (!this.tabForms.Visible) this.tabForms.Visible = true;
            }
            // Set the active child here (maybe we were called by a select-change)
            uiToolbarManager_.SetActiveMDIChild(this.ActiveMdiChild);
            RefreshActiveDesigner();
        }

        private void ItemBeingEdited_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            DesignerBase p = (DesignerBase)sender;
			switch (e.PropertyName)
			{
				case "Text":
                    foreach (TabPage page in tabForms.TabPages)
                        if (page.Tag == p)
                        {
                            page.Text = p.Text;
                            break;
                        }
					break;
			}
		}

        // If child form closed, remove tabPage
        private void ActiveMdiChild_FormClosed(object sender, FormClosedEventArgs e)
        {
            uiToolbarManager_.RemoveForm(sender);

            foreach ( TabPage page in tabForms.TabPages )
                if (page.Tag == sender)
                {
                    this.tabForms.TabPages.Remove(page);
                    break;
                }

            foreach (TabPage page in tabForms.TabPages)
                if (page.Tag is TemplateDesigner)
                {
                    tabForms.SelectedTab = page;
                    //(page.Tag as Form).Select();
                    break;
                }

        }

        private void tabForms_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((this.tabForms.SelectedTab != null) && (this.tabForms.SelectedTab.Tag != null))
                uiToolbarManager_.SelectAndActive(this.tabForms.SelectedTab.Tag as Form);
        }

        void tabForms_tabClosing(object sender, TabControlWithDelete.TabControlWithDeleteEventArgs e)
        {
            Form f = e.tag as Form;
            f.Close();
        }

        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        public class MdiChildClass
        {
            private List<Form> _EzMdiChildren;

            private Form _ActiveEzMdiChild;
            public Form ActiveEzMdiChild
            {
                get { return _ActiveEzMdiChild; }
            }
            public Form[] EzMdiChildren
            {
                get { return _EzMdiChildren.ToArray(); }
            }
            public void Add(Form __ActiveMdiChild)
            {
                _EzMdiChildren.Add(__ActiveMdiChild);
            }
            public void SelectAndActive(Form _form)
            {
                SetActiveMDIChild(_form);
                _form.Select();
            }
            public void SetActiveMDIChild(Form __ActiveMdiChild)
            {
                _ActiveEzMdiChild = __ActiveMdiChild;
            }
            public void RemoveForm(Object sender)
            {
                _EzMdiChildren.Remove(sender as Form);
            }

            public MdiChildClass()
            {
                _EzMdiChildren = new List<Form>();
            }
            //

            //public bool EzMdiContainer { get; set; }
            //public Form ActiveEzMdiChild { get; }
            //public Form[] EzMdiChildren { get; }
            //public bool EzMdiContainer { get; set; }
            //[Description("Occurs when a multiple document interface (MDI) child form is activated or closed within an EzMdi application.")]
            //public event EventHandler EzMdiChildActivate;
            //[Description("Occurs when a multiple document interface (MDI) child form is activated or closed within an EzMdi application.")]
            //public event EventHandler EzMdiChildActivate;
        }
    }
}
