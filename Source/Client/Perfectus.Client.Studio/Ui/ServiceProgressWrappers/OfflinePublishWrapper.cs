using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Principal;
using Perfectus.Client.Studio.WebApi.OfflinePublishing;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Configuration;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	/// <summary>
	/// Summary description for OfflinePublishWrapper.
	/// </summary>
    public class OfflinePublishWrapper : BackgroundWorkerBase
	{
		private Package package;
		private int versionNumber;
		private int revisionNumber;

		public int VersionNumber
		{
			get { return versionNumber; }
		}

		public int RevisionNumber
		{
			get { return revisionNumber; }
		}

		public Package Package
		{
			get { return package; }
		}

		public override bool ProvidesProgress
		{
			get { return false; }
		}

		public override string Name
		{
			get { return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.OfflinePublishWrapper.Name"); }
		}
		
		public override string Message
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.OfflinePublishWrapper.Message");
			}
		}	

		private OfflinePublishWrapper(int index) : base(index){}

		public OfflinePublishWrapper(int index, Package package) : base(index)
		{
			this.package = package;
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            package.PublishedBy = WindowsIdentity.GetCurrent().Name;
            package.PublishedDateTime = DateTime.Now; // Will be overwritten on the server.

            using (MemoryStream ms = new MemoryStream())
            {
                Package.SaveToStream(package, ms, StreamingContextStates.CrossMachine);

                Perfectus.Client.Studio.WebApi.OfflinePublishing.PublishResult pr;

                //DesktopPublishWse publisher = new DesktopPublishWse();
                WebApi.OfflinePublishing.DesktopPublish publisher = new DesktopPublish();

                string url = ConfigurationSettings.AppSettings["reportingServerUrl"];
                if (url[url.Length - 1] != '/')
                {
                    url += "/";
                }

                publisher.Url = url + @"reports/desktopPublish.asmx";
                publisher.Credentials = CredentialCache.DefaultCredentials;

                //PublishResult pr = publisher.DesktopPublish_MTOM_Compressed_Out(ms.ToArray());
                pr = publisher.DesktopPublish_MTOM_Compressed_Out(ms.ToArray());
                this.versionNumber = pr.VersionNumber;
                this.revisionNumber = pr.RevisionNumber;
            }
        }
	}
}