using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;
using System.Xml.Serialization;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Common;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{

	/// <summary>
	/// Summary description for AddServerWrapper.
	/// </summary>
	public class AddServerWrapper : BackgroundWorkerBase
	{
		private Uri address;
		private ServerInfo info = null;
		private PluginDescriptor[] plugins;

		public PluginDescriptor[] Plugins
		{
			get { return plugins; }
		}

		public ServerInfo Info
		{
			get { return info; }
		}

        public override string Name
        {
            get { return address.Host; }
        }

        public override string Message
        {
            get
            {
                return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.Message");
            }
        }

        public override bool ProvidesProgress
        {
            get { return false; }
        }

		private AddServerWrapper(int index) : base(index){}

		public AddServerWrapper(int index, Uri address) : this(index)
		{
			this.address = address;
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
		{
            if (address.IsFile)
            {
                const int timeout = 10000;
                ProcessStartInfo startInfo = new ProcessStartInfo(address.LocalPath);
                startInfo.Arguments = "/serverinfo";
                startInfo.CreateNoWindow = true;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.WorkingDirectory = Path.GetDirectoryName(address.LocalPath);
                startInfo.UseShellExecute = false;
                Process p = Process.Start(startInfo);

                XmlSerializer ser = new XmlSerializer(typeof(ServerInfo));
                object o = ser.Deserialize(p.StandardOutput.BaseStream);
                info = (ServerInfo)o;

                p.WaitForExit(timeout);

                if (!p.HasExited)
                {
                    p.Kill();
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.ServerInfoResponse"));
                }

                startInfo.Arguments = "/plugins";
                startInfo.CreateNoWindow = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.WorkingDirectory = Path.GetDirectoryName(address.LocalPath);
                startInfo.UseShellExecute = false;
                Process p2 = Process.Start(startInfo);

                ser = new XmlSerializer(typeof(PluginDescriptor[]));
                o = ser.Deserialize(p2.StandardOutput.BaseStream);
                plugins = (PluginDescriptor[])o;

                p2.WaitForExit(timeout);
                if (!p2.HasExited)
                {
                    p2.Kill();
                    MessageBox.Show(p2.StandardError.ReadToEnd());
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.ServerPluginResponse"));
                }
            }
            else
            {
                Server svr = new Server();

                svr.Proxy = WebProxy.GetDefaultProxy();
                svr.Credentials = CredentialCache.DefaultCredentials;
                svr.Url = address.ToString() + "/ConfigurationSystem/Server.asmx";

                try
                {
                    info = svr.GetServerInfo();
                }
                catch (Exception ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.GetServerInfoError");
                    // Generate client friendly error message...
                    message = string.Format( message, ExtractClientFriendlyMessage( ex.Message ) );
                    throw new Exception(message);
                }

                Plugins psvc = new Plugins();
                psvc.Proxy = WebProxy.GetDefaultProxy();
                psvc.Credentials = CredentialCache.DefaultCredentials;
                psvc.Url = address.ToString() + "/ConfigurationSystem/Plugins.asmx";

                try
                {
                    plugins = psvc.GetInstalledPlugins();
                }
                catch (Exception ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.GetInstalledPluginsError");
                    // Generate client friendly error message...
                    message = string.Format( message, ExtractClientFriendlyMessage( ex.Message ) );
                    throw new Exception(message);
                }
            }
		}

        /// <summary>
        /// Takes an exception message and returns the html title if the message is meant for a browser.
        /// </summary>
        /// <param name="exmessage">Exception message.</param>
        /// <returns>Client friendly exception message.</returns>
        private String ExtractClientFriendlyMessage( String exmessage )
        {
            String returnmessage = exmessage;
            int titleindex1 = -1;
            if( !String.IsNullOrEmpty( exmessage ) && exmessage.Contains( "text/html" ) && ( titleindex1 = exmessage.IndexOf( "<title>" ) ) > -1 )
            {
                int titleindex2 = exmessage.IndexOf( "</title>", titleindex1 );
                if( titleindex2 > -1 )
                {
                    returnmessage = exmessage.Substring( titleindex1 + 7, titleindex2 - titleindex1 - 7 );
                }
            }

            return returnmessage;
        }
	}
}