using System;
using System.ComponentModel;
using System.Threading;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	public abstract class BackgroundWorkerBase
	{
		BackgroundWorker worker;
		private int index;

		public abstract string Name { get; }
		public abstract string Message { get; }
        public abstract bool ProvidesProgress { get; }

		public int Index
		{
			get { return index; }
		}

        public BackgroundWorkerBase(int index)
		{
			this.index = index;
		}

		public void Abort(bool raiseEvent)
		{
            if (worker != null)
			{
                worker.CancelAsync();
			}
		}

        public abstract void Start(BackgroundWorker worker, DoWorkEventArgs e);
		}
}
