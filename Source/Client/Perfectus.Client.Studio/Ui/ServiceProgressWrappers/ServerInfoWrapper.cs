using System;
using System.ComponentModel;
using System.Net;
using Perfectus.Common;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	/// <summary>
	/// Summary description for ServerInfoWrapper.
	/// </summary>
    public class ServerInfoWrapper : BackgroundWorkerBase
	{
		private Uri address;
		private ServerInfo info = null;

		public ServerInfo Info
		{
			get { return info; }
		}

		private ServerInfoWrapper(int index) : base(index){}

		public ServerInfoWrapper(int index, Uri address) : this(index)
		{
			this.address = address;
		}

		public override string Name
		{
			get { return address.Host; }
		}

		public override string Message
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.Message");
			}
		}

		public override bool ProvidesProgress
		{
			get { return false; }
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            Server svr = new Server();

            svr.Proxy = WebProxy.GetDefaultProxy();
            svr.Credentials = CredentialCache.DefaultCredentials;
            svr.Url = address.ToString() + "/ConfigurationSystem/Server.asmx";
            info = svr.GetServerInfo();
        }
	}
}