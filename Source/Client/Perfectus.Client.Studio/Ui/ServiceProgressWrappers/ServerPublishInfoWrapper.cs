using System;
using System.ComponentModel;
using System.Net;
using Perfectus.Client.Studio.WebApi.ConfigurationSystem.Server;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	/// <summary>
	/// Summary description for ServerPublishInfoWrapper.
	/// </summary>
    public class ServerPublishInfoWrapper : BackgroundWorkerBase
	{
		private Uri address;

		public ServerPublishInfo Info
		{
			get { return info; }
		}

		public override string Name
		{
			get { return address.Host; }
		}

		public override string Message
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerInfoWrapper.Message");
			}
		}

		public override bool ProvidesProgress
		{
			get { return false; }
		}

		private ServerPublishInfo info = null;

		private ServerPublishInfoWrapper(int index) : base(index)
		{
		}

		public ServerPublishInfoWrapper(int index, Uri address) : this(index)
		{
			this.address = address;
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
		{
			Server svr = new Server();
			svr.Proxy = WebProxy.GetDefaultProxy();
			svr.Credentials = CredentialCache.DefaultCredentials;
			svr.Url = address.ToString() + "/ConfigurationSystem/Server.asmx";
			info = svr.GetServerPublishInfo();
		}
	}
}