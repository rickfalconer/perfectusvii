using System;
using System.Threading;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	public class ProgressEventArgs : EventArgs
	{
		private int progress;

		public int Progress
		{
			get { return progress; }
		}

		public ProgressEventArgs(int progress)
		{
			this.progress = progress;
		}
	}

	public class ErrorEventArgs : EventArgs
	{
		private Exception ex;

		public Exception Ex
		{
			get { return ex; }
		}

		public ErrorEventArgs(Exception ex)
		{
			this.ex = ex;
		}

	}

	public abstract class WrapperThreadBase
	{
		private int index;
		public event EventHandler Progress;
		public event EventHandler Done;
		public event EventHandler Error;
		public event EventHandler Started;

		public abstract string Name { get; }

		public abstract string Message { get; }

		public int Index
		{
			get { return index; }
		}

		private Thread thr;

		public abstract bool ProvidesProgress { get; }


		public WrapperThreadBase(int index)
		{
			this.index = index;
		}

		public void Abort(bool raiseEvent)
		{
			if (thr != null)
			{
				thr.Abort();
				if (raiseEvent)
				{
					OnError(new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.WrapperThreadBase.cancelled")));
				}
			}
		}

		public void Start()
		{
			thr = new Thread(new ThreadStart(Starter));
			thr.Start();
		}

		protected void OnDone()
		{
			if (Done != null)
			{
				Done(this, new EventArgs());
			}
		}

		protected void OnProgress(int prog)
		{
			if (Progress != null)
			{
				Progress(this, new ProgressEventArgs(prog));
			}
		}

		protected abstract void Starter();

		protected void OnStart()
		{
			if (Started != null)
			{
				Started(this, new EventArgs());
			}
		}

		protected void OnError(Exception ex)
		{
			if (Error != null)
			{
				Error(this, new ErrorEventArgs(ex));
			}
		}
	}

}
