using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Principal;
using Perfectus.Client.Studio.WebApi.Publishing;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Configuration;
using System.ServiceModel;
using Perfectus.Client.Studio.WebApi.Publishing.PublishStreamService;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
    /// <summary>
    /// Summary description for PublishWrapper.
    /// </summary>
    public class PublishWrapper : BackgroundWorkerBase
    {
        private Package package;
        private int versionNumber;
        private int revisionNumber;

        public int VersionNumber
        {
            get { return versionNumber; }
        }

        public int RevisionNumber
        {
            get { return revisionNumber; }
        }

        public Package Package
        {
            get { return package; }
        }

        public override bool ProvidesProgress
        {
            get { return false; }
        }

        public override string Name
        {
            get { return server.Name; }
        }

        [Browsable(false)]
        public string MyDocumentsURL
        {
            get { return server.MyDocumentsURL; }
        }

        public override string Message
        {
            get
            {
                return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.PublishWrapper.Message");
            }
        }

        private bool newVersion;
        private Server server;

        public Server Server
        {
            get { return server; }
        }

        private PublishWrapper(int index)
            : base(index)
        {
        }

        public PublishWrapper(int index, Server s, Package package, bool newVersion)
            : base(index)
        {
            this.server = s;
            this.package = package;
            this.newVersion = newVersion;
        }

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            package.PublishedBy = WindowsIdentity.GetCurrent().Name;
            package.PublishedDateTime = DateTime.UtcNow; // Will be overwritten on the server.

            using (MemoryStream ms = new MemoryStream())
            {
                Perfectus.Client.Studio.WebApi.Publishing.PublishResult pr;

                Package.SaveToStream(package, ms, StreamingContextStates.CrossMachine);
                ms.Position = 0;

                string url = server.Address.ToString();
                if (url[url.Length - 1] != '/')
                    url += "/";
                try
                {
                    if (ConfigurationSettings.AppSettings["WsePublishEnabled"] != null &&
                        ConfigurationSettings.AppSettings["WsePublishEnabled"].ToLower() == "true")
                    {
                        // Web service publishing
                        //PublishingWse publisher = new PublishingWse();
                        WebApi.Publishing.Publishing publisher = new WebApi.Publishing.Publishing();
                        publisher.Url = string.Format("{0}PackageManager/publishing.asmx", url);
                        publisher.Credentials = CredentialCache.DefaultCredentials;

                        publisher.Timeout = 300000;
                        pr = publisher.Publish_MTOM_Compressed(newVersion, ms.ToArray());
                        this.versionNumber = pr.VersionNumber;
                        this.revisionNumber = pr.RevisionNumber;
                    }
                    else
                    {
                        // WSE publishing
                        BasicHttpBinding binding;
                        try
                        {
                            binding = new BasicHttpBinding("httpPublishStreamBinding");
                        }
                        catch
                        {
                            // Maybe the configuration file has no binding specified...
                            binding = new BasicHttpBinding();
                            binding.TransferMode = TransferMode.Streamed;
                            binding.MessageEncoding = WSMessageEncoding.Mtom;

                            binding.CloseTimeout = TimeSpan.FromMinutes(2);
                            binding.OpenTimeout = TimeSpan.FromMinutes(2);
                            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
                            binding.SendTimeout = TimeSpan.FromMinutes(2);
                        }

                        EndpointAddress address = new EndpointAddress(string.Format("{0}PackageManager/PublishStreaming.svc", url));
                        PublishStreamingClient client = new PublishStreamingClient(binding, address);
                        Perfectus.Client.Studio.WebApi.Publishing.PublishStreamService.PublishResult result;

                        if (newVersion)
                            result = client.Publish_Wse_MTOM_NewVersion(ms);
                        else
                            result = client.Publish_Wse_MTOM(ms);

                        this.versionNumber = result.VersionNumber;
                        this.revisionNumber = result.RevisionNumber;
                    }
                }
                catch (Exception ex)
                {
                    string message = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.PublishWrapper.Publish_MTOM_Compressed");
                    message = string.Format(message, ex.Message);
                    throw new Exception(message, ex);
                }
            }
        }
    }
}