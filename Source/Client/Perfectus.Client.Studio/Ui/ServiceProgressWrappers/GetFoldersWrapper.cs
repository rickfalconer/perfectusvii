using System;
using System.ComponentModel;
using System.Net;
using System.Xml;
using Perfectus.Client.Studio.WebApi.ConfigurationSystem.Folders;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	/// <summary>
	/// Summary description for GetFoldersWrapper.
	/// </summary>
    public class GetFoldersWrapper : BackgroundWorkerBase
	{
		private Server server;
		private XmlDocument folders = null;

		public XmlDocument Folders
		{
			get { return folders; }
		}

		public override string Name
		{
			get { return server.Name; }
		}

		public override string Message
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.GetFoldersWrapper.Message");
			}
		}

		public Server Server
		{
			get { return server; }
		}

		public override bool ProvidesProgress
		{
			get { return false; }
		}

		private GetFoldersWrapper(int index) : base(index){}

		public GetFoldersWrapper(int index, Server s) : base(index)
		{
			this.server = s;
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            Folders f = new Folders();
            f.Url = string.Format("{0}/configurationsystem/folders.asmx", server.Address);
            f.Credentials = CredentialCache.DefaultCredentials;
            string xml = f.GetFolders().Trim();
            folders = new XmlDocument();
            folders.LoadXml(xml);
        }
	}
}