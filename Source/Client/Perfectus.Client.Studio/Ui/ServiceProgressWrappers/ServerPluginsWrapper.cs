using System;
using System.ComponentModel;
using System.Net;
using Perfectus.Common;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
	/// <summary>
	/// Summary description for ServerInfoWrapper.
	/// </summary>
    public class ServerPluginsWrapper : BackgroundWorkerBase
	{
		private Uri address;
		private PluginDescriptor[] plugins;

		public PluginDescriptor[] Plugins
		{
			get { return plugins; }
		}

		private ServerPluginsWrapper(int index) : base(index){}

		public ServerPluginsWrapper(int index, Uri address) : this(index)
		{
			this.address = address;
		}

		public override string Name
		{
			get { return address.Host; }
		}

		public override string Message
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.ServiceProgressWrappers.ServerPluginsWrapper.Message");
			}
		}

		public override bool ProvidesProgress
		{
			get { return false; }
		}

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            Plugins psvc = new Plugins();
            psvc.Proxy = WebProxy.GetDefaultProxy();
            psvc.Credentials = CredentialCache.DefaultCredentials;
            psvc.Url = address.ToString() + "/ConfigurationSystem/Plugins.asmx";
            plugins = psvc.GetInstalledPlugins();
        }
	}
}