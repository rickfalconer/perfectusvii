using System;
using System.Configuration;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Client.Studio.UI.ServiceProgressWrappers
{
    public class SharedLibraryWrapper : BackgroundWorkerBase
    {
        private BackgroundWorker worker;
        private LibraryItem libraryItem;
        private RepositoryItem repositoryItem;        
        private SharedLibraryAction action;
        private string name;
        private string message;

        public LibraryItem LibraryItem
        {
            get { return libraryItem; }
        }

        public RepositoryItem RepositoryItem
        {
            get { return repositoryItem; }
        }

        public SharedLibraryAction Action
        {
            get { return action; }
        }

        public override string Message
        {
            get { return message;  }
        }

        public override string Name
        {
            get { return name;  }
        }

        public override bool ProvidesProgress
        {
            get { return false; }
        }

        private SharedLibraryWrapper(int index) : base(index) { }

        public SharedLibraryWrapper(LibraryItem libraryItem, SharedLibraryAction action, int index, 
                                    string name, string message) : base(index)
        {
            this.name = name;
            this.message = message;
            this.libraryItem = libraryItem;
            this.action = action;

            string uiCultureOverride = ConfigurationManager.AppSettings["UiCultureOverride"];
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(uiCultureOverride);
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
        }

        public SharedLibraryWrapper(RepositoryItem repositoryItem, SharedLibraryAction action, int index, 
                                    string name, string message) : base(index)
        {
            this.name = name;
            this.message = message;
            this.repositoryItem = repositoryItem;
            this.action = action;

            string uiCultureOverride = ConfigurationManager.AppSettings["UiCultureOverride"];
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(uiCultureOverride);
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
        }

        public override void Start(BackgroundWorker worker, DoWorkEventArgs e)
        {
            this.worker = worker;
            SharedLibraryProxy sharedlibraryProxy = new SharedLibraryProxy();
            sharedlibraryProxy.ProgressChanged += new EventHandler<SharedLibraryEventArgs>(sharedlibraryProxy_ProgressChanged);

            switch (action)
            { 
                case SharedLibraryAction.Add:
                    libraryItem = sharedlibraryProxy.AddRepositoryItems(libraryItem);
                    break;
                case SharedLibraryAction.CheckIn:
                    sharedlibraryProxy.CheckIn(libraryItem);
                    break;
                case SharedLibraryAction.GetLatestVersion:
                    if (libraryItem != null)
                    {
                        libraryItem = sharedlibraryProxy.GetLatestVersion(libraryItem);
                    }
                    else
                    {
                        libraryItem = sharedlibraryProxy.GetLatestVersion(repositoryItem);
                    }
                    break;
                case SharedLibraryAction.CheckOut:
                    libraryItem = sharedlibraryProxy.CheckOut(libraryItem);
                    break;
            }
        }

        protected void sharedlibraryProxy_ProgressChanged(object sender, SharedLibraryEventArgs e)
        {
            int dontUpdateProgressPercentage = -1;
            this.message = e.Message;

            worker.ReportProgress(dontUpdateProgressPercentage, message);
        }
    }
}