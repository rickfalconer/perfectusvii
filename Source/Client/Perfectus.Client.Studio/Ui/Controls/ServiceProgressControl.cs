using System;
using System.ComponentModel;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Common;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Client.Studio.UI.Controls
{
	/// <summary>
	/// Summary description for ServiceProgressControl.
	/// </summary>
	public class ServiceProgressControl : UserControl
	{
        private BackgroundWorkerBase worker;
        private BackgroundWorker backgroundWorker;
        private ProgressBar progressBar1;
		private ImageList imageList1;
		private PictureBox pictureBox1;
		private Label lblServerName;
		private Timer timer1;
        private TextBox txtBoxMessage;
		private IContainer components;

		public event EventHandler Complete;

        // These delegates enable asynchronous calls for setting the value properties on given controls.
        delegate void SetTextCallback(string text);
        delegate void SetProgressCallback(int progressValue);
        delegate void SetProgressBarVisibilityCallback(bool visible);

        internal string GetMessageText()
        {
            return txtBoxMessage.Text;
        }

		private void OnComplete()
		{
			if (Complete != null)
			{
				Complete(this, new EventArgs());
			}
		}

        public BackgroundWorkerBase ProgressWorker
		{
			get { return worker; }
		}

        private void UpdateProgressBar(int progress)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressCallback d = new SetProgressCallback(UpdateProgressBar);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                timer1.Enabled = false;

                // ensure the progress value is valid
                if (progress < 0 || progress > this.progressBar1.Maximum)
                {
                    progress = 0;
                }

                this.progressBar1.Value = progress;
            }
        }

        private void UpdateProgressBarVisibility(bool visible)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetProgressBarVisibilityCallback d = new SetProgressBarVisibilityCallback(UpdateProgressBarVisibility);
                this.Invoke(d, new object[] { visible });
            }
            else
            {
                progressBar1.Visible = visible;
            }
        }

        public void RunWorker()
        {
            backgroundWorker.RunWorkerAsync();
        }

        public void CancelProcessing()
        {
            backgroundWorker.CancelAsync();
        }

        private void SetError(string message)
        {
            SetError(message, false);
        }

		private void SetError(string message, bool showScrollBars)
		{
            txtBoxMessage.Text = message;

            if (message.Contains("\n"))
            {
                txtBoxMessage.ScrollBars = ScrollBars.Vertical;
            }
            
            if (imageList1.Images != null && imageList1.Images.Count > 1)
            {
                pictureBox1.Image = imageList1.Images[1];
            }

            if (showScrollBars)
            {
                txtBoxMessage.ScrollBars = ScrollBars.Vertical;
            }

			progressBar1.Visible = false;
			timer1.Enabled = false;
		}

        private void SetComplete(string message)
		{
			SetMessageText(message);

            if (imageList1.Images != null && imageList1.Images.Count > 0)
            {
                pictureBox1.Image = imageList1.Images[0];
            }

			UpdateProgressBar(100);
			OnComplete();
		}

		public ServiceProgressControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
            InitializeBackgroundWorker();

			// Set the 'arrow' picture.
			pictureBox1.Image = imageList1.Images[2];

			// If we aren't getting real progress information, add a bit of randomness to the 
			// 'I can't believe it's not a progress bar' so they don't all march along exactly in step
			// and give the game away

			Random r = new Random(DateTime.Now.Second);
			timer1.Interval = timer1.Interval + (r.Next(0, 70))*20;
		}

        public ServiceProgressControl(BackgroundWorkerBase worker) : this()
		{
			this.worker = worker;
			lblServerName.Text = worker.Name;
			txtBoxMessage.Text = worker.Message;
            Control.CheckForIllegalCrossThreadCalls = false;
		}

        private void InitializeBackgroundWorker()
        {
            // Hook up the workers events
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
        }

        /// <summary>
        ///     Sets the value of the message label asynchronously using the invoke method otherwise
        ///     synchronously using the normal .text = "";
        /// </summary>
        /// <param name="message">The message to be set.</param>
        private void SetMessageText(string message)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.

            // If these threads are different, it returns true.
            if (this.txtBoxMessage.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetMessageText);
                this.Invoke(d, new object[] { message });
            }
            else
            {
                this.txtBoxMessage.Text = message;
            }
        }

        /// <summary>
        ///     Sets the value of the server name label asynchronously using the invoke method otherwise
        ///     synchronously using the normal .text = "";
        /// </summary>
        /// <param name="name">The server name to be set.</param>
        private void SetServerNameText(string name)
        {
            // InvokeRequired required compares the thread ID of the calling thread to the thread ID of the creating thread.

            // If these threads are different, it returns true.
            if (this.lblServerName.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetServerNameText);
                this.Invoke(d, new object[] { name });
            }
            else
            {
                this.lblServerName.Text = name;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.PerformStep();
            if (progressBar1.Value >= 100)
            {
                progressBar1.Value = 0;
            }
        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.ProgressPercentage != -1)
            {
                UpdateProgressBar(e.ProgressPercentage);
            }

            if (e.UserState != null)
            {
                SetMessageText(e.UserState.ToString());
            }
        }

        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message;

            if (e.Error != null)
            {
                if (e.Error is SharedLibraryException)
                {
                    SharedLibraryException exception = (SharedLibraryException)e.Error;

                    if (exception.InnerException != null)
                    {
                        SetError(GetUIErrorMessage(exception.InnerException), true);
                    }
                    else
                    {
                        SetError(GetUIErrorMessage(exception));
                    }
                }
                else
                {
                    if (e.Error.InnerException != null)
                    {
                        SetError(e.Error.InnerException.Message);
                    }
                    else
                    {
                        SetError(e.Error.Message);
                    }
                }
            }
            else if (e.Cancelled)
            {
                message = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").
                            GetString("Perfectus.Client.UI.Controls.ServiceProgressControl.UserCancelled");
                SetComplete(message);
            }
            else
            {
                message = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").
                            GetString("Perfectus.Client.UI.Controls.ServiceProgressControl.Success");
                SetComplete(message);
            }
        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!worker.ProvidesProgress)
            {
                timer1.Start();
            }

            worker.Start(backgroundWorker, e);
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        /// <summary>
        /// Strips a SoapException into a more user friendly error message.
        /// </summary>
        /// <param name="ex">An instance of the SoapExtension.</param>
        /// <returns>The friendly error message.</returns>
        private string GetUIErrorMessage(Exception ex)
        {
            string UIMessage;

            if (ex is SoapException)
            {
                UIMessage = ex.Message.Substring("System.Web.Services.Protocols.SoapException: Server was unable to process request. ---> ".Length);
                UIMessage = UIMessage.Substring(0, UIMessage.IndexOf("\n"));

                // Strip off the exception type from the message string
                int spacer = 2; // 2 chars - : and a space (": ")
                int colonIndex = UIMessage.IndexOf(":") > 0 ? (UIMessage.IndexOf(":") + spacer) : 0;
                UIMessage = UIMessage.Substring(colonIndex);
            }
            else
            {
                UIMessage = ex.Message;
            }

            return UIMessage;
        }

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceProgressControl));
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblServerName = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.txtBoxMessage = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Name = "progressBar1";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // lblServerName
            // 
            resources.ApplyResources(this.lblServerName, "lblServerName");
            this.lblServerName.Name = "lblServerName";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            // 
            // txtBoxMessage
            // 
            resources.ApplyResources(this.txtBoxMessage, "txtBoxMessage");
            this.txtBoxMessage.BackColor = System.Drawing.SystemColors.Control;
            this.txtBoxMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxMessage.Name = "txtBoxMessage";
            this.txtBoxMessage.ReadOnly = true;
            // 
            // ServiceProgressControl
            // 
            this.Controls.Add(this.lblServerName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.txtBoxMessage);
            this.Name = "ServiceProgressControl";
            resources.ApplyResources(this, "$this");
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
	}
}
