using System.Collections.Specialized;
using System.ComponentModel;
using Microsoft.Win32;

namespace Perfectus.Client.Studio.UI.Controls
{
    /// <summary>
    /// Summary description for MRU.
    /// </summary>
    public class MRU : Component
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        private int mruLength = 4;
        private string registryCompanyName = null;
        private string registryApplicationName = null;


        public string RegistryCompanyName
        {
            get { return registryCompanyName; }
        }

        public string RegistryApplicationName
        {
            get { return registryApplicationName; }
        }

        private StringCollection mruStringCollection = new StringCollection();

        public StringCollection InnerCollection
        {
            get { return mruStringCollection; }
        }

        public int MruLength
        {
            get { return mruLength; }
            set { mruLength = value; }
        }

        public MRU(string companyName, string productName)
            : this()
        {
            registryCompanyName = companyName;
            registryApplicationName = productName;
            LoadList();
        }

        public MRU(IContainer container)
        {
            ///
            /// Required for Windows.Forms Class Composition Designer support
            ///
            container.Add(this);
            InitializeComponent();

        }

        private MRU()
        {
            ///
            /// Required for Windows.Forms Class Composition Designer support
            ///
            InitializeComponent();

        }

        private string GetKeyPath()
        {
            string subKey = string.Format(@"Software\{0}\{1}", registryCompanyName, registryApplicationName);
            return subKey;
        }

        private void EnsureKeyExists()
        {
            string subKey = GetKeyPath();
            // If the registry key doesn't exist, make it first.
            RegistryKey mruKey = Registry.CurrentUser.OpenSubKey(subKey, true);
            if (mruKey == null)
            {
                Registry.CurrentUser.CreateSubKey(subKey);
                mruKey = Registry.CurrentUser.OpenSubKey(subKey, true);
            }

            mruKey.Close();
        }

        private void LoadList()
        {
            EnsureKeyExists();
            string subKey = GetKeyPath();
            RegistryKey mruKey = Registry.CurrentUser.OpenSubKey(subKey, true);

            object mruValue = mruKey.GetValue("MRU");
            string[] mruItems;

            if (mruValue == null)
            {
                mruItems = new string[] { };

                mruKey.SetValue("MRU", mruItems);
            }
            else
            {
                mruItems = (string[])mruValue;
            }

            mruStringCollection.Clear();
            int count = 0;
            foreach (string s in mruItems)
            {
                mruStringCollection.Add(s);
                count++;
                if (count >= mruLength)
                {
                    break;
                }
            }
            mruKey.Close();
        }

        public void AddFilename(string filename)
        {
            if (!Contains(filename))
            {
                mruStringCollection.Add(filename);
            }

            if (mruStringCollection.Count > mruLength && mruStringCollection.Count > 0)
            {
                mruStringCollection.RemoveAt(0);
            }

            // Bubble up the recently used name
            int currentPosition = mruStringCollection.IndexOf(filename);
            if (currentPosition != -1 && currentPosition < mruStringCollection.Count - 1)
                for (int pos = currentPosition; pos < mruStringCollection.Count - 1; pos++)
                {
                    string tempStr = mruStringCollection[pos];
                    mruStringCollection[pos] = mruStringCollection[pos + 1];
                    mruStringCollection[pos + 1] = tempStr;
                }
        }

        public void RemoveFilename(string filename)
        {
            if (Contains(filename))
            {
                mruStringCollection.Remove(filename);
            }
        }

        private bool Contains(string key)
        {
            foreach (string s in mruStringCollection)
            {
                if (s.Trim().ToUpper() == key.Trim().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }

        public void SaveList()
        {
            EnsureKeyExists();
            string subKey = GetKeyPath();
            RegistryKey mruKey = Registry.CurrentUser.OpenSubKey(subKey, true);

            string[] mruArray = new string[mruStringCollection.Count];
            mruStringCollection.CopyTo(mruArray, 0);

            mruKey.SetValue("MRU", mruArray);
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion
    }
}