using System;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Controls
{
	/// <summary>
	/// Summary description for MenuItemWithTag.
	/// </summary>
	public class MenuItemWithTag : MenuItem
	{
		public object Tag;
	}
}
