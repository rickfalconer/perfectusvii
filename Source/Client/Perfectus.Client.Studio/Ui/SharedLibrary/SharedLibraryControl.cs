using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Toolbars;
//using DataDynamics.SharpUI.Panels;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace Perfectus.Client.Studio.UI.SharedLibrary
{
	/// <summary>
	/// Summary description for SharedLibraryPanel.
	/// </summary>
	[ToolboxBitmap(typeof(SharedLibraryControl), "SharedLibraryPanel.bmp"),
	Description("A grouped panel of shared library types, displayed using a treeview and listview.")]
	public class SharedLibraryControl : System.Windows.Forms.UserControl
	{
        private System.ComponentModel.IContainer components;
		private System.Windows.Forms.ImageList IconsImageList;
		private SLTypePane SimpleOutcomeSLTypePane;
		private SLTypePane InterviewPageSLTypePane;
		private SLTypePane FunctionsSLTypePane;
		private SLTypePane OutcomesSLTypePane;
		private SLTypePane QuestionsSLTypePane;
		private SLTypePane TemplatesSLTypePane;
        private SharedLibraryProxy mRepository = new SharedLibraryProxy();
		
		// View state holder
		private string _CurrentlySelectedGroup = mcSIMPLE_OUTCOMES;

		// Private variables for panel group text names
		private const string mcFUNCTIONS = "Functions";
        private const string mcINTERVIEW_PAGES = "EasyInterview Pages";
        private const string mcOUTCOMES = "Outcomes";
        private const string mcQUESTIONS = "Questions";    
        private const string mcSIMPLE_OUTCOMES = "Conditional Text";
        private const string mcTEMPLATES = "Templates";

		// Private variables for panel group indexes
		private const int mcSIMPLE_OUTCOMES_INDEX = 0;
		private const int mcINTERVIEW_PAGES_INDEX = 1;
		private const int mcFUNCTIONS_INDEX = 2;
		private const int mcOUTCOMES_INDEX = 3;
        private const int mcQUESTIONS_INDEX = 4;
		private const int mcTEMPLATES_INDEX = 5;

		// Event to notify listeners than an item has been added to the library
		public event SharedLibraryItemAddedHandler SharedLibraryItemAdded;

		public SharedLibraryControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// Set the pane types
			SimpleOutcomeSLTypePane.LibraryItemType = LibraryItemType.SimpleOutcome;
			InterviewPageSLTypePane.LibraryItemType = LibraryItemType.InterviewPage;
			FunctionsSLTypePane.LibraryItemType = LibraryItemType.Function;
			OutcomesSLTypePane.LibraryItemType = LibraryItemType.Outcome;
			QuestionsSLTypePane.LibraryItemType = LibraryItemType.Question;
			TemplatesSLTypePane.LibraryItemType = LibraryItemType.Template;
		}

        public void LoadSharedLibraryPanels()
        {
            // Load the shared library panes
            SimpleOutcomeSLTypePane.LoadTree();
            InterviewPageSLTypePane.LoadTree();
            FunctionsSLTypePane.LoadTree();
            OutcomesSLTypePane.LoadTree();
            QuestionsSLTypePane.LoadTree();
            TemplatesSLTypePane.LoadTree();
        }

        /// <summary>
        /// Loads the shared library panel. We dont do this in the constructor because we only want to 
        /// add the expense of loading the library if it is explicitly needed.
        /// </summary>
        /// <returns>A boolean flag indicating whether the pane was successfully loaded.</returns>
        public bool LoadLibrary(out string sharedLibraryLoadMessage)
        {
            bool activated = mRepository.IsSharedLibraryActivated(out sharedLibraryLoadMessage);

            // Only go further if the shared library is activated
            if (activated)
            {
                try
                {
                    LoadRepositoryPanes();

                    // Successful load
                    return true;
                }
                catch (Exception ex)
                {
                    sharedLibraryLoadMessage = ex.Message;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Loads all the repository panes ensuring they are first cleared of all contents
        /// </summary>
        private void LoadRepositoryPanes()
        {
            // Populate all the types in the string array for updating
            List<LibraryItemType> libraryTypesToUpdate = new List<LibraryItemType>();
            libraryTypesToUpdate.Add(LibraryItemType.Function);
            libraryTypesToUpdate.Add(LibraryItemType.InterviewPage);
            libraryTypesToUpdate.Add(LibraryItemType.Outcome);
            libraryTypesToUpdate.Add(LibraryItemType.Question);
            libraryTypesToUpdate.Add(LibraryItemType.SimpleOutcome);
            libraryTypesToUpdate.Add(LibraryItemType.Template);

            this.RefreshRepostioryView(libraryTypesToUpdate);
        }

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SharedLibraryControl));
            this.SimpleOutcomeSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.InterviewPageSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.FunctionsSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.OutcomesSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.QuestionsSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.TemplatesSLTypePane = new Perfectus.Client.Studio.UI.SharedLibrary.SLTypePane();
            this.IconsImageList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // SimpleOutcomeSLTypePane
            // 
            this.SimpleOutcomeSLTypePane.AllowDrop = true;
            this.SimpleOutcomeSLTypePane.BackColor = System.Drawing.Color.White;
            this.SimpleOutcomeSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.SimpleOutcome;
            resources.ApplyResources(this.SimpleOutcomeSLTypePane, "SimpleOutcomeSLTypePane");
            this.SimpleOutcomeSLTypePane.Name = "SimpleOutcomeSLTypePane";
            this.SimpleOutcomeSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.SimpleOutcomeSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.SimpleOutcomeSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // InterviewPageSLTypePane
            // 
            this.InterviewPageSLTypePane.AllowDrop = true;
            this.InterviewPageSLTypePane.BackColor = System.Drawing.Color.White;
            this.InterviewPageSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.InterviewPage;
            resources.ApplyResources(this.InterviewPageSLTypePane, "InterviewPageSLTypePane");
            this.InterviewPageSLTypePane.Name = "InterviewPageSLTypePane";
            this.InterviewPageSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.InterviewPageSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.InterviewPageSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // FunctionsSLTypePane
            // 
            this.FunctionsSLTypePane.AllowDrop = true;
            this.FunctionsSLTypePane.BackColor = System.Drawing.Color.White;
            this.FunctionsSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.Function;
            resources.ApplyResources(this.FunctionsSLTypePane, "FunctionsSLTypePane");
            this.FunctionsSLTypePane.Name = "FunctionsSLTypePane";
            this.FunctionsSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.FunctionsSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.FunctionsSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // OutcomesSLTypePane
            // 
            this.OutcomesSLTypePane.AllowDrop = true;
            this.OutcomesSLTypePane.BackColor = System.Drawing.Color.White;
            this.OutcomesSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.Outcome;
            resources.ApplyResources(this.OutcomesSLTypePane, "OutcomesSLTypePane");
            this.OutcomesSLTypePane.Name = "OutcomesSLTypePane";
            this.OutcomesSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.OutcomesSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.OutcomesSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // QuestionsSLTypePane
            // 
            this.QuestionsSLTypePane.AllowDrop = true;
            this.QuestionsSLTypePane.BackColor = System.Drawing.Color.White;
            this.QuestionsSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.Question;
            resources.ApplyResources(this.QuestionsSLTypePane, "QuestionsSLTypePane");
            this.QuestionsSLTypePane.Name = "QuestionsSLTypePane";
            this.QuestionsSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.QuestionsSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.QuestionsSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // TemplatesSLTypePane
            // 
            this.TemplatesSLTypePane.AllowDrop = true;
            this.TemplatesSLTypePane.BackColor = System.Drawing.Color.White;
            this.TemplatesSLTypePane.LibraryItemType = Perfectus.Common.PackageObjects.LibraryItemType.Template;
            resources.ApplyResources(this.TemplatesSLTypePane, "TemplatesSLTypePane");
            this.TemplatesSLTypePane.Name = "TemplatesSLTypePane";
            this.TemplatesSLTypePane.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.SLTypePane_SharedLibraryItemAdded);
            this.TemplatesSLTypePane.IncompatibleLibraryItem += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemHandler(this.SLTypePane_IncompatibleLibraryItem);
            this.TemplatesSLTypePane.RefreshLibraryView += new System.EventHandler(this.SLTypePane_RefreshLibraryView);
            // 
            // IconsImageList
            // 
            this.IconsImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconsImageList.ImageStream")));
            this.IconsImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconsImageList.Images.SetKeyName(0, "");
            this.IconsImageList.Images.SetKeyName(1, "");
            this.IconsImageList.Images.SetKeyName(2, "");
            this.IconsImageList.Images.SetKeyName(3, "");
            this.IconsImageList.Images.SetKeyName(4, "");
            this.IconsImageList.Images.SetKeyName(5, "");
            // 
            // SharedLibraryControl
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.SimpleOutcomeSLTypePane);
            this.Controls.Add(this.InterviewPageSLTypePane);
            this.Controls.Add(this.FunctionsSLTypePane);
            this.Controls.Add(this.OutcomesSLTypePane);
            this.Controls.Add(this.QuestionsSLTypePane);
            this.Controls.Add(this.TemplatesSLTypePane);
            this.Name = "SharedLibraryControl";
            resources.ApplyResources(this, "$this");
            this.ResumeLayout(false);

		}

        void SLTypePane_RefreshLibraryView(object sender, EventArgs e)
        {
            LoadRepositoryPanes();
        }

		#endregion

		/// <summary>
		///     Refreshes the treeviews from the repository
		/// </summary>
		public void RefreshRepostioryView(List<LibraryItemType> libraryTypesToUpdate)
		{
            try
            {
                // Refresh the shared library panes if provided in the string array
                foreach (LibraryItemType type in libraryTypesToUpdate)
                {
                    switch (type)
                    {
                        case LibraryItemType.Function:
                            FunctionsSLTypePane.LoadTree();
                            break;
                        case LibraryItemType.InterviewPage:
                            InterviewPageSLTypePane.LoadTree();
                            break;
                        case LibraryItemType.Outcome:
                            OutcomesSLTypePane.LoadTree();
                            break;
                        case LibraryItemType.Question:
                            QuestionsSLTypePane.LoadTree();
                            break;
                        case LibraryItemType.SimpleOutcome:
                            SimpleOutcomeSLTypePane.LoadTree();
                            break;
                        case LibraryItemType.Template:
                            TemplatesSLTypePane.LoadTree();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                MessageBox.Show(message, GetResource("SharedLibrary"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
		}

		/// <summary>
		///		Ensures that all the groups are collapsed except for the one being expanded.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        //private void SharedLibraryUITaskBar_GroupExpandCollapse(object sender, DataDynamics.SharpUI.Panels.UiTaskBarEventArgs e)
        //{
        //    DoExpandCollapse(e.Group.Text);
        //}

        //private void DoExpandCollapse(string groupText)
        //{
        //    // A different group is being expanded, ensure the others are collapsed
        //    if (groupText != _CurrentlySelectedGroup)
        //    {
        //        if (groupText != mcFUNCTIONS)
        //            SharedLibraryUITaskBar.Groups[mcFUNCTIONS_INDEX].Expanded = false;
        //        if (groupText != mcINTERVIEW_PAGES)
        //            SharedLibraryUITaskBar.Groups[mcINTERVIEW_PAGES_INDEX].Expanded = false;
        //        if (groupText != mcOUTCOMES)
        //            SharedLibraryUITaskBar.Groups[mcOUTCOMES_INDEX].Expanded = false;
        //        if (groupText != mcQUESTIONS)
        //            SharedLibraryUITaskBar.Groups[mcQUESTIONS_INDEX].Expanded = false;
        //        if (groupText != mcSIMPLE_OUTCOMES)
        //            SharedLibraryUITaskBar.Groups[mcSIMPLE_OUTCOMES_INDEX].Expanded = false;
        //        if (groupText != mcTEMPLATES)
        //            SharedLibraryUITaskBar.Groups[mcTEMPLATES_INDEX].Expanded = false;

        //        _CurrentlySelectedGroup = groupText;
        //    }
        //}

        //public void SharedLibraryUITaskBar_DragEnter(LibraryItem libraryItem)
        //{
        //    string groupText = string.Empty;

        //    // Collapse all the groups not of the given group text
        //    switch (libraryItem.LibraryType)
        //    {
        //        case LibraryItemType.Function:
        //            DoExpandCollapse(mcFUNCTIONS);
        //            break;
        //        case LibraryItemType.InterviewPage:
        //            DoExpandCollapse(mcINTERVIEW_PAGES);
        //            break;
        //        case LibraryItemType.Outcome:
        //            DoExpandCollapse(mcOUTCOMES);
        //            break;
        //        case LibraryItemType.Question:
        //            DoExpandCollapse(mcQUESTIONS);
        //            break;
        //        case LibraryItemType.SimpleOutcome:
        //            DoExpandCollapse(mcSIMPLE_OUTCOMES);
        //            break;
        //        case LibraryItemType.Template:
        //            DoExpandCollapse(mcTEMPLATES);
        //            break;
        //    }

        //    // Ensure the appropriate group for the libraryItem is expanded
        //    if (libraryItem.LibraryType == LibraryItemType.Function)
        //        SharedLibraryUITaskBar.Groups[mcFUNCTIONS_INDEX].Expanded = true;
        //    else if (libraryItem.LibraryType == LibraryItemType.InterviewPage)
        //        SharedLibraryUITaskBar.Groups[mcINTERVIEW_PAGES_INDEX].Expanded = true;
        //    else if (libraryItem.LibraryType == LibraryItemType.Outcome)
        //        SharedLibraryUITaskBar.Groups[mcOUTCOMES_INDEX].Expanded = true;
        //    else if (libraryItem.LibraryType == LibraryItemType.Question)
        //        SharedLibraryUITaskBar.Groups[mcQUESTIONS_INDEX].Expanded = true;
        //    else if (libraryItem.LibraryType == LibraryItemType.SimpleOutcome)
        //        SharedLibraryUITaskBar.Groups[mcSIMPLE_OUTCOMES_INDEX].Expanded = true;
        //    else if (libraryItem.LibraryType == LibraryItemType.Template)
        //        SharedLibraryUITaskBar.Groups[mcTEMPLATES_INDEX].Expanded = true;
        //}

        /// <summary>
        ///     Handles the IncompatiableLibraryItem event. Ensures the correct pane is open for dragging and dropping
        ///     library items.
        /// </summary>
        /// <param name="o">Object firing the event.</param>
        /// <param name="e">SharedLibraryItemEventArgs, includes a LibraryItem property.</param>
        void SLTypePane_IncompatibleLibraryItem(object o, SharedLibraryItemEventArgs e)
        {
            //SharedLibraryUITaskBar_DragEnter(e.LibraryItem);
        }

		private void SLTypePane_SharedLibraryItemAdded(object o, SharedLibraryItemAddedEventArgs e)
		{
            List<LibraryItemType> libraryTypesToUpdate = new List<LibraryItemType>();

			// Update the panes which have had library items added to them
            PopulatePanesToRefresh(e.AddedLibraryItem, libraryTypesToUpdate);
            RefreshRepostioryView(libraryTypesToUpdate);

			// Reraise the event down the chain
			SharedLibraryItemAdded(o, e);
		}

		private void PopulatePanesToRefresh(LibraryItem libraryItem, List<LibraryItemType> libraryTypesToUpdate)
		{
            if (libraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    PopulatePanesToRefresh(dependentLibraryItem, libraryTypesToUpdate);
                }
            }

            // Add the types to update
            switch (libraryItem.LibraryType)
            { 
                case LibraryItemType.Question:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.Question))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.Question);
                    }
                    break;
                case LibraryItemType.Outcome:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.Outcome))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.Outcome);
                    }
                    break;
                case LibraryItemType.Template:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.Template))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.Template);
                    }
                    break;
                case LibraryItemType.SimpleOutcome:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.SimpleOutcome))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.SimpleOutcome);
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.InterviewPage))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.InterviewPage);
                    }
                    break;
                case LibraryItemType.Function:
                    if (!libraryTypesToUpdate.Contains(LibraryItemType.Function))
                    {
                        libraryTypesToUpdate.Add(LibraryItemType.Function);
                    }
                    break;
            }
		}

        private string GetResource(string key)
        {
            return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(key);
        }
	}
}
