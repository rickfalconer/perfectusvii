using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;

namespace Perfectus.Client.Studio.UI.SharedLibrary
{
    /// <summary>
    /// Summary description for CheckInComments.
    /// </summary>
    public class CheckInComments : Form
    {
        private Button CheckInButton;
        private Button CancelCheckInButton;
        private Label CommentsLabel;
        private PictureBox IconPictureBox;
        private Label ItemNameLabel;
        private Label ModifiedTitleLabel;
        private Label ModifiedDateTimeLabel;
        private TextBox CommentsTextBox;
        private ImageList IconImageList;
        private IContainer components;

        private string _Comments;
        private bool _PerformCheckIn;

        private enum LibraryTypeIconIndex
        {
            SimpleOutcome = 0,
            InterviewPage,
            DateFunction,
            ArithmeticFunction,
            Outcome,
            Question,
            Template
        }

        /// <summary>
        ///  Accessor for the comments of the given library item to be checked in.
        /// </summary>
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        /// <summary>
        ///  Accessor for that indicates whether the given library item should be checked in.
        /// </summary>
        public bool PerformCheckIn
        {
            get { return _PerformCheckIn; }
            set { _PerformCheckIn = value; }
        }

        public CheckInComments()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        public CheckInComments(LibraryItem libraryItem)
        {
            int imageIndex;
            InitializeComponent();

            // Default
            IconPictureBox.Image = IconImageList.Images[0];

            // Set the appropriate icon image depending of the library item type
            switch (libraryItem.LibraryType)
            {
                case LibraryItemType.Function:
                    if (libraryItem.GetType() == typeof (DatePFunction))
                    {
                        imageIndex = Convert.ToInt32(LibraryTypeIconIndex.DateFunction);
                        IconPictureBox.Image = IconImageList.Images[imageIndex];
                    }
                    else
                    {
                        imageIndex = Convert.ToInt32(LibraryTypeIconIndex.ArithmeticFunction);
                        IconPictureBox.Image = IconImageList.Images[imageIndex];
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    imageIndex = Convert.ToInt32(LibraryTypeIconIndex.InterviewPage);
                    IconPictureBox.Image = IconImageList.Images[imageIndex];
                    break;
                case LibraryItemType.Outcome:
                    imageIndex = Convert.ToInt32(LibraryTypeIconIndex.Outcome);
                    IconPictureBox.Image = IconImageList.Images[imageIndex];
                    break;
                case LibraryItemType.Question:
                    imageIndex = Convert.ToInt32(LibraryTypeIconIndex.Question);
                    IconPictureBox.Image = IconImageList.Images[imageIndex];
                    break;
                case LibraryItemType.SimpleOutcome:
                    imageIndex = Convert.ToInt32(LibraryTypeIconIndex.SimpleOutcome);
                    IconPictureBox.Image = IconImageList.Images[imageIndex];
                    break;
                case LibraryItemType.Template:
                    imageIndex = Convert.ToInt32(LibraryTypeIconIndex.Template);
                    IconPictureBox.Image = IconImageList.Images[imageIndex];
                    break;
            }

            ItemNameLabel.Text = libraryItem.Name;
            ModifiedDateTimeLabel.Text = libraryItem.ModifiedDateTime.ToString("g");
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof (CheckInComments));
            this.CheckInButton = new System.Windows.Forms.Button();
            this.CancelCheckInButton = new System.Windows.Forms.Button();
            this.CommentsTextBox = new System.Windows.Forms.TextBox();
            this.CommentsLabel = new System.Windows.Forms.Label();
            this.IconPictureBox = new System.Windows.Forms.PictureBox();
            this.ItemNameLabel = new System.Windows.Forms.Label();
            this.ModifiedTitleLabel = new System.Windows.Forms.Label();
            this.ModifiedDateTimeLabel = new System.Windows.Forms.Label();
            this.IconImageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.IconPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // CheckInButton
            // 
            this.CheckInButton.AccessibleDescription = null;
            this.CheckInButton.AccessibleName = null;
            resources.ApplyResources(this.CheckInButton, "CheckInButton");
            this.CheckInButton.BackgroundImage = null;
            this.CheckInButton.Font = null;
            this.CheckInButton.Name = "CheckInButton";
            this.CheckInButton.Click += new System.EventHandler(this.CheckInButton_Click);
            // 
            // CancelCheckInButton
            // 
            this.CancelCheckInButton.AccessibleDescription = null;
            this.CancelCheckInButton.AccessibleName = null;
            resources.ApplyResources(this.CancelCheckInButton, "CancelCheckInButton");
            this.CancelCheckInButton.BackgroundImage = null;
            this.CancelCheckInButton.Font = null;
            this.CancelCheckInButton.Name = "CancelCheckInButton";
            this.CancelCheckInButton.Click += new System.EventHandler(this.CancelCheckInButton_Click);
            // 
            // CommentsTextBox
            // 
            this.CommentsTextBox.AccessibleDescription = null;
            this.CommentsTextBox.AccessibleName = null;
            resources.ApplyResources(this.CommentsTextBox, "CommentsTextBox");
            this.CommentsTextBox.BackgroundImage = null;
            this.CommentsTextBox.Font = null;
            this.CommentsTextBox.Name = "CommentsTextBox";
            // 
            // CommentsLabel
            // 
            this.CommentsLabel.AccessibleDescription = null;
            this.CommentsLabel.AccessibleName = null;
            resources.ApplyResources(this.CommentsLabel, "CommentsLabel");
            this.CommentsLabel.Font = null;
            this.CommentsLabel.Name = "CommentsLabel";
            // 
            // IconPictureBox
            // 
            this.IconPictureBox.AccessibleDescription = null;
            this.IconPictureBox.AccessibleName = null;
            resources.ApplyResources(this.IconPictureBox, "IconPictureBox");
            this.IconPictureBox.BackgroundImage = null;
            this.IconPictureBox.Font = null;
            this.IconPictureBox.ImageLocation = null;
            this.IconPictureBox.Name = "IconPictureBox";
            this.IconPictureBox.TabStop = false;
            // 
            // ItemNameLabel
            // 
            this.ItemNameLabel.AccessibleDescription = null;
            this.ItemNameLabel.AccessibleName = null;
            resources.ApplyResources(this.ItemNameLabel, "ItemNameLabel");
            this.ItemNameLabel.Font = null;
            this.ItemNameLabel.Name = "ItemNameLabel";
            // 
            // ModifiedTitleLabel
            // 
            this.ModifiedTitleLabel.AccessibleDescription = null;
            this.ModifiedTitleLabel.AccessibleName = null;
            resources.ApplyResources(this.ModifiedTitleLabel, "ModifiedTitleLabel");
            this.ModifiedTitleLabel.Font = null;
            this.ModifiedTitleLabel.Name = "ModifiedTitleLabel";
            // 
            // ModifiedDateTimeLabel
            // 
            this.ModifiedDateTimeLabel.AccessibleDescription = null;
            this.ModifiedDateTimeLabel.AccessibleName = null;
            resources.ApplyResources(this.ModifiedDateTimeLabel, "ModifiedDateTimeLabel");
            this.ModifiedDateTimeLabel.Font = null;
            this.ModifiedDateTimeLabel.Name = "ModifiedDateTimeLabel";
            // 
            // IconImageList
            // 
            this.IconImageList.ImageStream =
                ((System.Windows.Forms.ImageListStreamer) (resources.GetObject("IconImageList.ImageStream")));
            this.IconImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconImageList.Images.SetKeyName(0, "");
            this.IconImageList.Images.SetKeyName(1, "");
            this.IconImageList.Images.SetKeyName(2, "");
            this.IconImageList.Images.SetKeyName(3, "");
            this.IconImageList.Images.SetKeyName(4, "");
            this.IconImageList.Images.SetKeyName(5, "");
            this.IconImageList.Images.SetKeyName(6, "");
            // 
            // CheckInComments
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this.ModifiedDateTimeLabel);
            this.Controls.Add(this.ModifiedTitleLabel);
            this.Controls.Add(this.ItemNameLabel);
            this.Controls.Add(this.IconPictureBox);
            this.Controls.Add(this.CommentsLabel);
            this.Controls.Add(this.CommentsTextBox);
            this.Controls.Add(this.CancelCheckInButton);
            this.Controls.Add(this.CheckInButton);
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckInComments";
            ((System.ComponentModel.ISupportInitialize) (this.IconPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private void CheckInButton_Click(object sender, EventArgs e)
        {
            // Set the comments and close
            Comments = CommentsTextBox.Text;
            PerformCheckIn = true;
            Close();
        }

        private void CancelCheckInButton_Click(object sender, EventArgs e)
        {
            PerformCheckIn = false;
            Comments = "";
            Close();
        }
    }
}