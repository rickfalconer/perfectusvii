using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Perfectus.Common.SharedLibrary.Configuration;

namespace Perfectus.Client.Studio.UI.SharedLibrary
{
    /// <summary>
    /// Summary description for SLTypePane.
    /// </summary>
    public class SLTypePane : UserControl
    {
        private PictureBox pictureBox2;
        private TreeView RepositoryItemTypeTreeView;
        private ListView PropertyListView;
        private ColumnHeader KeyName;
        private ColumnHeader Value;
        private ImageList IconImageList;
        private IContainer components;

        // Property member variables
        private LibraryItemType _LibraryItemType;
        private int _IconIndex = mcARITHMETIC_FUNCTION_IMAGE;

        // Indexes for the PropertyListView column data
        private const int mcVALUE_COLUMN = 1;
        private const int mcOBJECT_NAME = 0;
        private const int mcOBJECT_VERSION = 1;
        private const int mcOBJECT_TYPE = 2;
        private const int mcCREATED_BY = 3;
        private const int mcDATE_CREATED = 4;
        private const int mcMODIFIED_BY = 5;
        private const int mcDATE_MODIFIED = 6;
        private const int mcSTATUS = 7;
        private const int mcLOGIN_NAME = 8;

        // Indexes for the ImageList
        private const int mcARITHMETIC_FUNCTION_IMAGE = 0;
        private const int mcDATE_FUNCTION_IMAGE = 1;        
        private const int mcINTERVIEW_PAGE_IMAGE = 2;
        private const int mcOUTCOME_IMAGE = 3;
        private const int mcQUESTION_IMAGE = 4;
        private const int mcSIMPLE_OUTCOME_IMAGE = 5;
        private const int mcTEMPLATE_IMAGE = 6;
        private const int mcCLOSED_FOLDER_IMAGE = 7;
        private const int mcOPEN_FOLDER_IMAGE = 8;

        // Perfectus Shared Library Folder key values
        internal const string csSIMPLE_OUTCOME_FOLDER = "SimpleOutcomeFolder";
        internal const string csINTERVIEW_PAGE_FOLDER = "InterviewPageFolder";
        internal const string csFUNCTION_FOLDER = "FunctionFolder";
        internal const string csOUTCOME_FOLDER = "OutcomeFolder";
        internal const string csQUESTION_FOLDER = "QuestionFolder";
        internal const string csTEMPLATE_FOLDER = "TemplateFolder";

        // Repository variables
        private SharedLibraryController sharedLibraryController = null;
        //private ContextMenu cmFolder;
        private ContextMenuStrip contextMenuStrip;
        private ToolStripMenuItem RefreshLibraryToolStripMenuItem;
        private ToolStripMenuItem CreateFolderToolStripMenuItem;
        private ToolStripMenuItem CreateRootFolderToolStripMenuItem;
        private MenuItem miCreateFolder;

        // For scrolling .Net controls.
        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        private const int WM_VSCROLL = 277; // Vertical scroll
        private const int SB_PAGEUP = 2; // Scrolls one page up
        private const int SB_PAGEDOWN = 3; // Scrolls one page down

        private string mcREPOSITORY_UNAVAILABLE = GetResource("SLNotAvailable");

        // Event to notify listeners than an item has been added to the library
        public event SharedLibraryItemAddedHandler SharedLibraryItemAdded;
        public event SharedLibraryItemHandler IncompatibleLibraryItem;
        public event EventHandler RefreshLibraryView;

        [CategoryAttribute("Data"), Bindable(true), DescriptionAttribute("The library item type to handle.")]
        public LibraryItemType LibraryItemType
        {
            get { return _LibraryItemType; }
            set { _LibraryItemType = value; }
        }

        /// <summary>
        /// Read only accessor for the icon to be displayed on a library item node
        /// </summary>
        public int IconIndex(string perfectusType)
        {
            switch (_LibraryItemType)
            {
                case LibraryItemType.Function:
                    if (perfectusType == typeof(DatePFunction).ToString())
                    {
                        _IconIndex = mcDATE_FUNCTION_IMAGE;
                    }
                    else
                    {
                        _IconIndex = mcARITHMETIC_FUNCTION_IMAGE;
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    _IconIndex = mcINTERVIEW_PAGE_IMAGE;
                    break;
                case LibraryItemType.Outcome:
                    _IconIndex = mcOUTCOME_IMAGE;
                    break;
                case LibraryItemType.Question:
                    _IconIndex = mcQUESTION_IMAGE;
                    break;
                case LibraryItemType.SimpleOutcome:
                    _IconIndex = mcSIMPLE_OUTCOME_IMAGE;
                    break;
                case LibraryItemType.Template:
                    _IconIndex = mcTEMPLATE_IMAGE;
                    break;
            }

            return _IconIndex;
        }

        public SLTypePane()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            // Set additional properties (ensure the designer doesn't over write their definitions as is the case sometimes when placed
            // in InitializeComponent()
            SetAdditionalControlProperties();

            try
            {
                // Init the proxy object
                sharedLibraryController = new SharedLibraryController();
            }
            catch (Exception ex)
            {
                try
                {
                    bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);

                    string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                    MessageBox.Show(message, GetResource("SharedLibrary"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    // We should only get here if in design view mode in VS.Net, exception handled by the exception policy 
                    // therefore don't handle for design mode.

                }
            }
        }

        /// <summary>
        ///		Ensures the treeview is clear before populating it with shared library objects.
        ///     Only loads the treeview if the Shared Library is activated on te server.
        /// </summary>
        public void LoadTree()
        {
            try
            {
                // Ensure the treeview & details list is clear - epsecially when reloading
                RepositoryItemTypeTreeView.Nodes.Clear();
                UpdateSelectedNodeItemDetails(null);

                // Load the question library items
                RepositoryFolder rootFolder = sharedLibraryController.GetAllItemsByType(LibraryItemType);

                // Add the folders
                AddNodes(rootFolder, null);

                // Select the first item if present
                if (RepositoryItemTypeTreeView.Nodes.Count > 0)
                {
                    if (RepositoryItemTypeTreeView.Nodes != null)
                        RepositoryItemTypeTreeView.SelectedNode = RepositoryItemTypeTreeView.Nodes[0];
                    else
                    {
                        // Ensure the property listing is clear
                        UpdateSelectedNodeItemDetails(null);
                    }
                }
            }
            catch (Exception ex)
            {
                RepositoryItemTypeTreeView.Nodes.Clear();

                // A call has been made to the repository that is not available and this needs to be displayed to the user					
                TreeNode node = new TreeNode(mcREPOSITORY_UNAVAILABLE);
                RepositoryItemTypeTreeView.Nodes.Add(node);

                throw ex;
            }
        }

        #region Private Methods

        private void SetAdditionalControlProperties()
        {
            // Hook up the event handler to update an items property details
            RepositoryItemTypeTreeView.AfterSelect += new TreeViewEventHandler(RepositoryItemTypeTreeView_AfterSelect);
            RepositoryItemTypeTreeView.MouseUp += new MouseEventHandler(RepositoryItemTypeTreeView_MouseUp);
            RepositoryItemTypeTreeView.AfterLabelEdit += new NodeLabelEditEventHandler(RepositoryItemTypeTreeView_AfterLabelEdit);

            // Hook up the drag and drop event handlers
            RepositoryItemTypeTreeView.ItemDrag += new ItemDragEventHandler(RepositoryItemTypeTreeView_ItemDrag);
            RepositoryItemTypeTreeView.DragOver += new DragEventHandler(RepositoryItemTypeTreeView_DragOver);
            RepositoryItemTypeTreeView.DragDrop += new DragEventHandler(RepositoryItemTypeTreeView_DragDrop);
        }

        private string GetRepositoryFolderName(LibraryItemType libraryItemType)
        {
            RepositoryConfigurationView repositoryConfigurationView = new RepositoryConfigurationView();
            ConnectionSetting connectionSetting = repositoryConfigurationView.GetActiveConnectionSettings();

            switch (libraryItemType)
            { 
                case LibraryItemType.SimpleOutcome:
                    return connectionSetting.ConnectionParameters[csSIMPLE_OUTCOME_FOLDER].Value;
                case LibraryItemType.InterviewPage:
                    return connectionSetting.ConnectionParameters[csINTERVIEW_PAGE_FOLDER].Value;
                case LibraryItemType.Function:
                    return connectionSetting.ConnectionParameters[csFUNCTION_FOLDER].Value;
                case LibraryItemType.Outcome:
                    return connectionSetting.ConnectionParameters[csOUTCOME_FOLDER].Value;
                case LibraryItemType.Question:
                    return connectionSetting.ConnectionParameters[csQUESTION_FOLDER].Value;
                case LibraryItemType.Template:
                    return connectionSetting.ConnectionParameters[csTEMPLATE_FOLDER].Value;
                default:
                    throw new Exception(GetResource("InvalidSharedLibraryType"));
            }
        }

        private void AddNodes(RepositoryFolder folder, TreeNode node)
        {
            TreeNode childNode;

            // Add the folders
            foreach (RepositoryFolder subfolder in folder.Subfolders)
            {
                childNode = CreateFolderNode(subfolder);
                childNode.SelectedImageIndex = mcOPEN_FOLDER_IMAGE;
                childNode.ImageIndex = mcCLOSED_FOLDER_IMAGE;

                // Add itself and then handle its children
                if (node == null)
                    RepositoryItemTypeTreeView.Nodes.Add(childNode);
                else
                    node.Nodes.Add(childNode);

                // Recursively handle the children
                AddNodes(subfolder, childNode);
            }

            // Add the items
            foreach (RepositoryItem item in folder.RepositoryItems)
            {
                childNode = CreateItemNode(item);

                // Set the nodes properties
                int index = IconIndex(item.PerfectusType);
                childNode.SelectedImageIndex = index;
                childNode.ImageIndex = index;
                childNode.Tag = item;

                if (node == null)
                {
                    RepositoryItemTypeTreeView.Nodes.Add(childNode);
                }
                else
                {
                    node.Nodes.Add(childNode);
                }
            }
        }

        private static TreeNode CreateItemNode(RepositoryItem item)
        {
            TreeNode node = new TreeNode();

            if (item.Title != null)
            {
                node.Text = (item.Title.Length > 0) ? item.Title : item.Name;
            }
            else 
            {
                node.Text = item.Name;
            }
            
            node.Tag = item;

            return node;
        }

        private static TreeNode CreateFolderNode(RepositoryFolder folder)
        {
            TreeNode node = new TreeNode();

            node.Text = '\u0020' + folder.Name;
            node.Tag = folder;

            return node;
        }

        private void RepositoryItemTypeTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            UpdateSelectedNodeItemDetails(e.Node);
        }

        private void UpdateSelectedNodeItemDetails(TreeNode selectedNode)
        {
            RepositoryItem item = null;

            if (selectedNode != null && selectedNode.Tag is RepositoryItem)
            {
                try
                {
                    item = sharedLibraryController.GetCheckOutStatus((RepositoryItem)selectedNode.Tag);
                }
                catch
                {
                    // Ensure the item is null - ignore any expections
                    item = null;
                }
            }

            if (item != null)
            {
                if (item.Title != null)
                {
                    PropertyListView.Items[mcOBJECT_NAME].SubItems[mcVALUE_COLUMN].Text = (item.Title.Length > 0) ? item.Title : item.Name;
                }
                else
                {
                    PropertyListView.Items[mcOBJECT_NAME].SubItems[mcVALUE_COLUMN].Text = item.Name;
                }
                
                PropertyListView.Items[mcOBJECT_VERSION].SubItems[mcVALUE_COLUMN].Text = item.Version.ToString();
                PropertyListView.Items[mcOBJECT_TYPE].SubItems[mcVALUE_COLUMN].Text = item.LibraryItemType.ToString();
                PropertyListView.Items[mcCREATED_BY].SubItems[mcVALUE_COLUMN].Text = item.CreatedBy;
                PropertyListView.Items[mcDATE_CREATED].SubItems[mcVALUE_COLUMN].Text = item.CreatedDateTime.ToLocalTime().ToString();
                PropertyListView.Items[mcMODIFIED_BY].SubItems[mcVALUE_COLUMN].Text = item.ModifiedBy;
                PropertyListView.Items[mcDATE_MODIFIED].SubItems[mcVALUE_COLUMN].Text = item.ModifiedDateTime.ToLocalTime().ToString();
                PropertyListView.Items[mcSTATUS].SubItems[mcVALUE_COLUMN].Text = item.CheckOutStatus.ToString();
                PropertyListView.Items[mcLOGIN_NAME].SubItems[mcVALUE_COLUMN].Text = item.CheckedOutBy;
            }
            else
            {
                PropertyListView.Items[mcOBJECT_NAME].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcOBJECT_VERSION].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcOBJECT_TYPE].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcCREATED_BY].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcDATE_CREATED].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcMODIFIED_BY].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcDATE_MODIFIED].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcSTATUS].SubItems[mcVALUE_COLUMN].Text = "";
                PropertyListView.Items[mcLOGIN_NAME].SubItems[mcVALUE_COLUMN].Text = "";
            }
        }

        /// <summary>
        ///		Starts the Drag & Drop process off by passing the selected repository item to the DoDragDrop method.
        ///		Only Repository Items are able to be dragged & dropped, therefore folders are ignored.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryItemTypeTreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (((TreeNode) e.Item).Tag is RepositoryItem)
            {
                RepositoryItem item = (RepositoryItem) ((TreeNode) e.Item).Tag;
                RepositoryItemTypeTreeView.DoDragDrop(item, DragDropEffects.Copy);
            }
        }

        /// <summary>
        ///		Checks to see what drag drop event data is present and sets the appropriate DragDrop Effect.
        ///		All Drag & drop effects are set to none, except when a valid drag is being present, which includes
        ///		a repository item or library type item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryItemTypeTreeView_DragOver(object sender, DragEventArgs e)
        {
            object treeNodeObject = null;

            try
            {
                Point pt = RepositoryItemTypeTreeView.PointToClient(new Point(e.X, e.Y));

                // Scroll if need be. there must be a better way to get the location, this seems a bit long winded?	
                if (e.Y < ParentForm.Location.Y + Location.Y + RepositoryItemTypeTreeView.Location.Y + 100)
                {
                    SendMessage(RepositoryItemTypeTreeView.Handle, WM_VSCROLL, (IntPtr) SB_PAGEUP, IntPtr.Zero);
                }
                else if (e.Y > ParentForm.Location.Y + Location.Y + RepositoryItemTypeTreeView.Location.Y +
                         RepositoryItemTypeTreeView.Height + 40)
                {
                    SendMessage(RepositoryItemTypeTreeView.Handle, WM_VSCROLL, (IntPtr) SB_PAGEDOWN, IntPtr.Zero);
                }

                TreeNode selectedTreeNode = RepositoryItemTypeTreeView.GetNodeAt(pt);

                // Set the treenode object, if dropping onto a treenode
                if (selectedTreeNode != null)
                    treeNodeObject = selectedTreeNode.Tag;

                if (e.Data.GetDataPresent("Perfectus.Common.Package.PackageItem"))
                {
                    LibraryItem libraryItem = (LibraryItem) e.Data.GetData("Perfectus.Common.Package.PackageItem");

                    if (LibraryItemType == LibraryItemType.Function && libraryItem.LibraryType == LibraryItemType.Function)
                        e.Effect = DragDropEffects.Copy;
                    else if (LibraryItemType == LibraryItemType.InterviewPage && libraryItem.LibraryType == LibraryItemType.InterviewPage)
                        e.Effect = DragDropEffects.Copy;
                    else if (LibraryItemType == LibraryItemType.Outcome && libraryItem.LibraryType == LibraryItemType.Outcome)
                        e.Effect = DragDropEffects.Copy;
                    else if (LibraryItemType == LibraryItemType.Question && libraryItem.LibraryType == LibraryItemType.Question)
                        e.Effect = DragDropEffects.Copy;
                    else if (LibraryItemType == LibraryItemType.SimpleOutcome && libraryItem.LibraryType == LibraryItemType.SimpleOutcome)
                        e.Effect = DragDropEffects.Copy;
                    else if (LibraryItemType == LibraryItemType.Template && libraryItem.LibraryType == LibraryItemType.Template)
                        e.Effect = DragDropEffects.Copy;
                    else
                    {
                        // Wrong library item for the given window - fire event to open the correct window
                        if (IncompatibleLibraryItem != null)
                        {
                            IncompatibleLibraryItem(this, new SharedLibraryItemEventArgs(libraryItem));
                        }
                    }
                }
                else
                    e.Effect = DragDropEffects.None;
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                if (rethrow)
                {
                    throw;
                }
            }
        }



        private bool RecursivlyTestItemValidity(LibraryItem libraryItem, out String Msg)
        {
            Msg = String.Empty;

            if (libraryItem.DependentLibraryItems != null)
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    if (!dependentLibraryItem.IsValid(ref Msg))
                    {
                        ResetItemAndDependentItem(libraryItem);

                        libraryItem.LibraryUniqueIdentifier = Guid.Empty;
                        libraryItem.Linked = false;
                        libraryItem.RelativePath = String.Empty;

                        return false;
                    }
                    if (!RecursivlyTestItemValidity(dependentLibraryItem, out Msg))
                        return false;
                }
            return true;
        }

                        
        /// <summary>
        ///		Completes the drag and drop process, by recieving the end result of a drag and drop process.
        ///		Only accepts library items, which will be handled.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryItemTypeTreeView_DragDrop(object sender, DragEventArgs e)
        {
            LibraryItem libraryItem = null;

            try
            {
                Point pt = RepositoryItemTypeTreeView.PointToClient(new Point(e.X, e.Y));
                TreeNode selectedTreeNode = RepositoryItemTypeTreeView.GetNodeAt(pt);

                if (e.Data.GetDataPresent("Perfectus.Common.Package.PackageItem"))
                {
                    libraryItem = (LibraryItem) e.Data.GetData("Perfectus.Common.Package.PackageItem");
                    Cursor = Cursors.WaitCursor;

                    if (LibraryItemIsValidToAdd(libraryItem))
                    {
                        SetLibraryItemDependencies(libraryItem);

                        // After setting the dependent item, we need to 
                        // test validity. 
                        String Msg = String.Empty;
                        if (!RecursivlyTestItemValidity(libraryItem, out Msg))
                        {
                            ResetItemAndDependentItem(libraryItem);
                            throw new Exception(Msg);
                        }

                        libraryItem = HandleNewLibraryItem(libraryItem, selectedTreeNode);

                        if (libraryItem != null)
                        {
                            // Raise the event that an item has been added to the library (and ensure it is marked as checked in)
                            SharedLibraryItemAdded(this, new SharedLibraryItemAddedEventArgs(libraryItem));
                        }
                    }
                }
                else
                    e.Effect = DragDropEffects.None;
            }
            catch (SharedLibraryException ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                MessageBox.Show(GetResource("RepositoryItemError") + "\n" + ex.Message + "\n" +
                                GetResource("RepositoryItemError2"), GetResource("SLImportErrorTitle"),
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                try
                {
                    // Remove any newly added items from the library & ensure the UI reflects the original items values
                    DeleteNewlyAddedSharedLibraryItems(ex.LibraryItem);
                    SharedLibraryItemAdded(this, new SharedLibraryItemAddedEventArgs(libraryItem));
                }
                catch (Exception ex2)
                { 
                    // There has been an error deleting the item from the repository
                    //TODO - what do we want to do with exceptions here?
                }
            }
            catch (Exception ex)
            {
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                MessageBox.Show(GetResource("RepositoryItemError") + "\n" + ex.Message + "\n" +
                                GetResource("RepositoryItemError2"), GetResource("SLImportErrorTitle"),
                                MessageBoxButtons.OK, MessageBoxIcon.Error);

                try
                {
                    // Remove any newly added items from the library & ensure the UI reflects the original items values
                    DeleteNewlyAddedSharedLibraryItems(libraryItem);
                }
                catch (Exception ex2)
                {
                    // There has been an error deleting the item from the repository
                    //TODO - what do we want to do with exceptions here?
                }

            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        /// <summary>
        ///     Validates that the library item and dependents are not already added and unlinked.
        /// </summary>
        /// <param name="libraryItem">The item being validated.</param>
        private bool LibraryItemIsValidToAdd(LibraryItem libraryItem)
        {
            String msg = String.Empty;
            if (!libraryItem.IsValid(ref msg))
            {
                msg = GetResource("LibraryItem.NotValid") + System.Environment.NewLine + msg;
                MessageBox.Show(msg, GetResource("LibraryItem.CaptionNotValid"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }

            // Check that this is a new instance
            if (libraryItem.DependentObject == false &&                 // the root element must not already exist in SL
                libraryItem.LibraryUniqueIdentifier != Guid.Empty &&
                libraryItem.Linked)
            {
                string message = GetResource("RepositoryItemError4");
                string caption = GetResource("RepositoryItemError");

                // Item already exists in the SL and is linked, you cannot do this
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            return EnsureLibraryItemIsNotUnlinked(libraryItem);
        }

        private bool EnsureLibraryItemIsNotUnlinked(LibraryItem libraryItem)
        {
            if (libraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    if (!LibraryItemIsValidToAdd(dependentLibraryItem))
                    {
                        return false;
                    }
                }
            }

            if (libraryItem.LibraryUniqueIdentifier != Guid.Empty && !libraryItem.Linked)
            {
                string message = string.Format("{0}\n{1}", GetResource("UnlinkedItems"), GetResource("UnlinkedItems2"));
                string caption = GetResource("SLImportErrorTitle");

                // Don't allow unlinked SL items to be re-added (must be disassociated first)
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            return true;
        }

        private LibraryItem HandleNewLibraryItem(LibraryItem libraryItem, TreeNode selectedTreeNode)
        {
            if (LibraryItemType == LibraryItemType.Function && libraryItem.LibraryType == LibraryItemType.Function)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.Function);
            }
            else if (LibraryItemType == LibraryItemType.InterviewPage && libraryItem.LibraryType == LibraryItemType.InterviewPage)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.InterviewPage);
            }
            else if (LibraryItemType == LibraryItemType.Outcome && libraryItem.LibraryType == LibraryItemType.Outcome)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.Outcome);
            }
            else if (LibraryItemType == LibraryItemType.Question && libraryItem.LibraryType == LibraryItemType.Question)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.Question);
            }
            else if (LibraryItemType == LibraryItemType.SimpleOutcome && libraryItem.LibraryType == LibraryItemType.SimpleOutcome)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.SimpleOutcome);
            }
            else if (LibraryItemType == LibraryItemType.Template && libraryItem.LibraryType == LibraryItemType.Template)
            {
                libraryItem = HandleAddNewSharedLibraryItem(libraryItem, selectedTreeNode, LibraryItemType.Template);
            }

            return libraryItem;
        }

        /// <summary>
        ///     Gets a collection of library items including the given library item and it's dependents. The collection
        ///     items are then cloned to ensure they are not referencing the items which will be updated.
        ///     The cloned library item collection is then returned.
        /// </summary>
        /// <param name="libraryItem">The library item to be handled.</param>
        private void SetLibraryItemDependencies(LibraryItem libraryItem)
        {
            List<LibraryItem> handledLibraryItems = new List<LibraryItem>();
            libraryItem.HandleDependencies(handledLibraryItems);
        }

        private LibraryItem HandleAddNewSharedLibraryItem(LibraryItem libraryItem, TreeNode selectedTreeNode, LibraryItemType libraryItemType)
        {
            List<LibraryItem> handledLibraryItems = new List<LibraryItem>();
            
            // Set the depth level to add the item in the SL to
            if (selectedTreeNode != null && selectedTreeNode.Tag is RepositoryItem)
            {
                // Set the depth level to that of the library item being dropped onto
                string path = RemoveFilenameAndExtension(((RepositoryItem)selectedTreeNode.Tag).RelativePath);
                libraryItem.RepositoryFolder = new RepositoryFolder("", path);
                libraryItem.RelativePath = Serializer.GetRelativePath(libraryItem.RepositoryFolder, libraryItem.Name, libraryItem.LibraryType);

                // Ensure the selected tree node is the parent so that the item is added at the same level as the selected item
                if (selectedTreeNode.Parent != null)
                {
                    selectedTreeNode = selectedTreeNode.Parent;

                    // Set the depth level to that of the folder
                    libraryItem.RelativePath = ((RepositoryFolder)selectedTreeNode.Tag).RelativePath;
                    libraryItem.RepositoryFolder = (RepositoryFolder)selectedTreeNode.Tag;
                }
            }
            else if (selectedTreeNode != null && selectedTreeNode.Tag is RepositoryFolder)
            {
                // Set the depth level to that of the folder
                libraryItem.RelativePath = Serializer.GetRelativePath((RepositoryFolder)selectedTreeNode.Tag, libraryItem.Name, 
                                                                      libraryItem.LibraryType);
                libraryItem.RepositoryFolder = (RepositoryFolder)selectedTreeNode.Tag;
            }

            // Ensure there is no naming clash
            RepositoryItem foundRepositoryItem = sharedLibraryController.DoesRepositoryItemExist(libraryItem);
            if (foundRepositoryItem != null)
            {
                // Item already exists in the SL, do you want to create a new instance?
                string message = string.Format(GetResource("ItemExistsInSL"), foundRepositoryItem.Name, foundRepositoryItem.LibraryItemType);
                DialogResult result = MessageBox.Show(message, GetResource("SharedLibrary"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                libraryItem.DisassociateRootAndDependents(true);
                return null;
            }

            try
            {
                LibraryItem libraryItem2 = sharedLibraryController.AddRepositoryItems(libraryItem);
                if (libraryItem2 == null)
                {
                    ResetItemAndDependentItem(libraryItem);
                    return null;
                }
                else
                    libraryItem = libraryItem2;
            }
            catch (Exception ex)
            {
                ResetItemAndDependentItem(libraryItem);

                string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                MessageBox.Show(message, GetResource("SharedLibrary"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return libraryItem;
        }

        private void ResetItemAndDependentItem(LibraryItem libraryItem)
        {
            if (libraryItem.IsNewLibraryItem)
            {
                libraryItem.LibraryUniqueIdentifier = Guid.Empty;
                libraryItem.Linked = false;
                libraryItem.RelativePath = String.Empty;
            }
            if (null != libraryItem.DependentLibraryItems)
                foreach (LibraryItem item in libraryItem.DependentLibraryItems)
                    ResetItemAndDependentItem(item);

            libraryItem.DependentLibraryItems = null;
        }

        private void RemoveShareLibraryProperties(LibraryItem libraryItem)
        {

        }

        private static string RemoveFilenameAndExtension(string path)
        {
            int filenameIndex = path.LastIndexOf("/");
            return path.Substring(0, filenameIndex);
        }

        /// <summary>
        ///     Deletes the library items contained within the collection, only if the given item is marked as newly added to the library.
        /// </summary>
        /// <remarks>
        ///     This is usually called after an error when new items have been added to the shared library.
        /// </remarks>
        /// <param name="libraryItem">The  LibraryItem that has been updated in the repository.</param>
        private void DeleteNewlyAddedSharedLibraryItems(LibraryItem libraryItem)
        {
            try
            {
                if (libraryItem.DependentLibraryItems != null)
                {
                    foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                    {
                        DeleteNewlyAddedSharedLibraryItems(dependentLibraryItem);
                    }
                }

                if (libraryItem.IsNewLibraryItem)
                {
                    sharedLibraryController.Delete(libraryItem);
                }
            }
            catch (Exception ex)
            {
                // Only log the exception, don't rethrow - as this is called by functions adding new library items and is handled there
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                MessageBox.Show(message, GetResource("SLDeleteionError"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

/*
        private void AddNode(RepositoryItem repositoryItem, TreeNode nodeToAddTo)
        {
            // Add the item
            TreeNode childNode = CreateItemNode(repositoryItem);
            childNode.SelectedImageIndex = IconIndex;
            childNode.ImageIndex = IconIndex;
            childNode.Tag = repositoryItem;

            if (nodeToAddTo == null)
            {
                // Add to the root
                RepositoryItemTypeTreeView.Nodes.Add(childNode);
            }
            else
            {
                // Add to the folder
                nodeToAddTo.Nodes.Add(childNode);
            }
        }
*/

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SLTypePane));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Name",
            ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.Empty, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))));
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "Version",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Type",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Created By",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Date Created",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "Modified By",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "Date Modified",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "Status",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {
            "Checked Out By",
            ""}, -1, System.Drawing.Color.Gray, System.Drawing.Color.Empty, null);
            this.RepositoryItemTypeTreeView = new System.Windows.Forms.TreeView();
            this.IconImageList = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.PropertyListView = new System.Windows.Forms.ListView();
            this.KeyName = new System.Windows.Forms.ColumnHeader();
            this.Value = new System.Windows.Forms.ColumnHeader();
            this.miCreateFolder = new System.Windows.Forms.MenuItem();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RefreshLibraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateRootFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // RepositoryItemTypeTreeView
            // 
            this.RepositoryItemTypeTreeView.AllowDrop = true;
            this.RepositoryItemTypeTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RepositoryItemTypeTreeView.Dock = System.Windows.Forms.DockStyle.Top;
            this.RepositoryItemTypeTreeView.ImageIndex = 0;
            this.RepositoryItemTypeTreeView.ImageList = this.IconImageList;
            this.RepositoryItemTypeTreeView.Location = new System.Drawing.Point(0, 0);
            this.RepositoryItemTypeTreeView.Name = "RepositoryItemTypeTreeView";
            this.RepositoryItemTypeTreeView.SelectedImageIndex = 0;
            this.RepositoryItemTypeTreeView.Size = new System.Drawing.Size(328, 256);
            this.RepositoryItemTypeTreeView.Sorted = true;
            this.RepositoryItemTypeTreeView.TabIndex = 12;
            this.RepositoryItemTypeTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.RepositoryItemTypeTreeView_AfterSelect);
            // 
            // IconImageList
            // 
            this.IconImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("IconImageList.ImageStream")));
            this.IconImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.IconImageList.Images.SetKeyName(0, "");
            this.IconImageList.Images.SetKeyName(1, "");
            this.IconImageList.Images.SetKeyName(2, "");
            this.IconImageList.Images.SetKeyName(3, "");
            this.IconImageList.Images.SetKeyName(4, "");
            this.IconImageList.Images.SetKeyName(5, "");
            this.IconImageList.Images.SetKeyName(6, "");
            this.IconImageList.Images.SetKeyName(7, "");
            this.IconImageList.Images.SetKeyName(8, "");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // PropertyListView
            // 
            this.PropertyListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PropertyListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.KeyName,
            this.Value});
            this.PropertyListView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PropertyListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.PropertyListView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9});
            this.PropertyListView.Location = new System.Drawing.Point(0, 270);
            this.PropertyListView.Name = "PropertyListView";
            this.PropertyListView.Size = new System.Drawing.Size(328, 130);
            this.PropertyListView.TabIndex = 25;
            this.PropertyListView.UseCompatibleStateImageBehavior = false;
            this.PropertyListView.View = System.Windows.Forms.View.Details;
            // 
            // KeyName
            // 
            this.KeyName.Text = "Name";
            this.KeyName.Width = 120;
            // 
            // Value
            // 
            this.Value.Text = "Value";
            this.Value.Width = 200;
            // 
            // miCreateFolder
            // 
            this.miCreateFolder.Index = -1;
            this.miCreateFolder.Text = "";
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RefreshLibraryToolStripMenuItem, this.CreateFolderToolStripMenuItem, this.CreateRootFolderToolStripMenuItem });
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(239, 70);
            // 
            // RefreshLibraryToolStripMenuItem
            // 
            this.RefreshLibraryToolStripMenuItem.Name = "RefreshLibraryToolStripMenuItem";
            this.RefreshLibraryToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.RefreshLibraryToolStripMenuItem.Text = "Refresh Library View"; // TODO - RESOURCE
            this.RefreshLibraryToolStripMenuItem.Click += new System.EventHandler(this.RefreshLibraryToolStripMenuItem_Click);
            // 
            // CreateFoldertoolStripMenuItem
            // 
            this.CreateFolderToolStripMenuItem.Name = "CreateFoldertoolStripMenuItem";
            this.CreateFolderToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.CreateFolderToolStripMenuItem.Text = "Create Folder"; //TODO - RESOURCE
            this.CreateFolderToolStripMenuItem.Click += new System.EventHandler(this.CreateFolderToolStripMenuItem_Click);
            // 
            // CreateRootFolderToolStripMenuItem
            // 
            this.CreateRootFolderToolStripMenuItem.Name = "CreateRootFolderToolStripMenuItem";
            this.CreateRootFolderToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.CreateRootFolderToolStripMenuItem.Text = "Create Root Level Folder"; //TODO - RESOURCE
            this.CreateRootFolderToolStripMenuItem.Click += new System.EventHandler(this.CreateRootFolderToolStripMenuItem_Click);
            // 
            // SLTypePane
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PropertyListView);
            this.Controls.Add(this.RepositoryItemTypeTreeView);
            this.Name = "SLTypePane";
            this.Size = new System.Drawing.Size(328, 400);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        ///		Right Mouse UP - Event Handler
        ///		
        ///		Displays the create folder context menu, if it is deemed appropriate to display it.
        ///		
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RepositoryItemTypeTreeView_MouseUp(object sender, MouseEventArgs e)
        {
            TreeNode n = RepositoryItemTypeTreeView.GetNodeAt(e.X, e.Y);

            if (e.Button == MouseButtons.Right && e.Clicks == 1 && sender == RepositoryItemTypeTreeView)
            {
                if (RepositoryItemTypeTreeView.SelectedNode == null)
                {
                    CreateRootFolderToolStripMenuItem.Visible = false;
                }
                else 
                {
                    CreateRootFolderToolStripMenuItem.Visible = true;
                }

                RepositoryItemTypeTreeView.SelectedNode = n;
                contextMenuStrip.Show(RepositoryItemTypeTreeView, new Point(e.X, e.Y));

                RepositoryItemTypeTreeView.SelectedNode = n;
                RepositoryItemTypeTreeView.Invalidate();
            }
        }

        private void RepositoryItemTypeTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            try
            {
                // Ensure the default folder name or a valid new folder is provided
                if (e.Node.Text.Length == 0 && (e.Label == null || e.Label.Length == 0))
                {
                    // Cancel edit
                    MessageBox.Show(GetResource("ValidFolderName"), GetResource("CreateFolder"), 
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Node.BeginEdit();
                }
                else if (!RepositoryItemTypeTreeView.LabelEdit)
                {
                    //TODO - SL - need to allow folder name edits
                    MessageBox.Show(GetResource("EditExistingFolder"), "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    Cursor = Cursors.WaitCursor;

                    // Set the properties of the new folder
                    RepositoryFolder repositoryFolder = new RepositoryFolder();

                    string relativePath;
                    relativePath = e.Node.FullPath.StartsWith(" ") ? e.Node.FullPath.Substring(1) : e.Node.FullPath;
                    
                    if (e.Label != null && e.Label.Length > 0)
                    {
                        // Use the custom folder name provided by the user
                        repositoryFolder.Name = GetUniqueFolderName(e.Node.Parent, e.Node, e.Label);
                        repositoryFolder.RelativePath = GetRelativeFolderPath(relativePath, repositoryFolder.Name);
                        e.Node.Text = '\u0020' + repositoryFolder.Name;
                    }
                    else
                    {
                        // Use the default folder name
                        repositoryFolder.Name = GetUniqueFolderName(e.Node.Parent, e.Node, e.Node.Text);
                        repositoryFolder.RelativePath = GetRelativeFolderPath(relativePath, repositoryFolder.Name);
                        e.Node.Text = '\u0020' + repositoryFolder.Name;
                    }

                    // Ensure the folder was created in the repository
                    if (!sharedLibraryController.CreateNewFolder(repositoryFolder, LibraryItemType))
                    {
                        // Remove the added treenode folder and display an error message to the user
                        MessageBox.Show(GetResource("CreateFolderError"), GetResource("CreateFolderError2"),
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Node.Remove();
                    }
                    else
                    {
                        // Assumption - you can only rename a folder (directly after creation), and not repository items
                        string rootFolderName = GetRepositoryFolderName(this.LibraryItemType);
                        repositoryFolder.RelativePath = string.Format(@"{0}\{1}", rootFolderName, repositoryFolder.RelativePath);
                        e.Node.Tag = repositoryFolder;
                    }
                }
            }
            finally
            {
                RepositoryItemTypeTreeView.Refresh();
                Cursor = Cursors.Arrow;
                RepositoryItemTypeTreeView.LabelEdit = false;
            }
        }

        private string GetUniqueFolderName(TreeNode parentNode, TreeNode node, string foldername)
        {
            int count = 0;
            TreeNodeCollection nodes;

            if (parentNode != null)
            {
                nodes = parentNode.Nodes;
            }
            else
            {
                nodes = node.TreeView.Nodes;
            }

            return GetUniqueFolderName(nodes, foldername, foldername, count);
        }

        private string GetUniqueFolderName(TreeNodeCollection nodes, string foldername, string proposedName, int count)
        { 
            bool found = false;
            
            if (count > 0)
            {
                proposedName = string.Format("{0}_{1}", foldername, count.ToString());
            }

            foreach (TreeNode childNode in nodes)
            {
                if ((string.Compare(proposedName.Trim(), childNode.Text.Trim(), true) == 0 ||
                    childNode.Text.Trim() == foldername) && !found)
                {
                    found = true;
                }
                else if (string.Compare(proposedName.Trim(), childNode.Text.Trim(), true) == 0 && found)
                { 
                    count++;
                    proposedName = GetUniqueFolderName(nodes, foldername, proposedName, count);
                    break;
                }
            }

            return proposedName;
        }

        /// <summary>
        ///  Replaces the default folder name with the actual name while keeping the provided relative path.
        /// </summary>
        /// <param name="defaultPath">The default path of the folder including a default folder name..</param>
        /// <param name="folderName">The name of the folder.</param>
        /// <returns>The relative path of a new folder including the new folder name.</returns>
        private static string GetRelativeFolderPath(string defaultPath, string folderName)
        {
            int pathUpToFolderNameIndex;

            // Get the folder path without the folder name
            pathUpToFolderNameIndex = defaultPath.LastIndexOf(@"\");

            // Return the relative folder path
            if (pathUpToFolderNameIndex > 0)
            {
                string relativePath = defaultPath.Substring(0, pathUpToFolderNameIndex);
                return relativePath + "\\" + folderName;
            }
            else
            {
                return folderName;
            }
        }

        private void CreateRootFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateFolder(true);
        }

        private void CreateFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CreateFolder(false);
        }

        private void CreateFolder(bool createRootLevel)
        {
            TreeNode folderNode;
            
            folderNode = new TreeNode(GetResource("NewFolder"), mcCLOSED_FOLDER_IMAGE, mcOPEN_FOLDER_IMAGE);

            // Add the new treenode at the root or below
            if (RepositoryItemTypeTreeView.SelectedNode == null || createRootLevel)
                RepositoryItemTypeTreeView.Nodes.Add(folderNode);
            else
            {
                // Below the root or on a repostiory item, create the node at the correct level

                if (RepositoryItemTypeTreeView.SelectedNode.Tag is RepositoryItem)
                {
                    // Repository item, create at the folder level about or at the root level if there is no folder
                    if (RepositoryItemTypeTreeView.SelectedNode.Parent != null)
                    {
                        // Add the folder to the folder of the repository item
                        RepositoryItemTypeTreeView.SelectedNode.Parent.Nodes.Add(folderNode);
                    }
                    else
                    {
                        // Add the folder to the root level
                        RepositoryItemTypeTreeView.Nodes.Add(folderNode);
                    }
                }
                else
                {
                    RepositoryItemTypeTreeView.SelectedNode.Nodes.Add(folderNode);
                }

                RepositoryItemTypeTreeView.SelectedNode.Expand();
            }

            // Expand the now parent folder node and begin the editing process
            RepositoryItemTypeTreeView.LabelEdit = true;
            folderNode.BeginEdit();
        }

        private void RefreshLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RefreshLibraryView != null)
            {
                RefreshLibraryView(sender, e);
            }
        }

        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }
    }

    public delegate void SharedLibraryItemAddedHandler(object o, SharedLibraryItemAddedEventArgs e);

    public class SharedLibraryItemAddedEventArgs : EventArgs
    {
        private LibraryItem _AddedLibraryItem;

        public LibraryItem AddedLibraryItem
        {
            get { return _AddedLibraryItem; }
        }

        public SharedLibraryItemAddedEventArgs(LibraryItem libraryItem)
        {
            _AddedLibraryItem = libraryItem;
        }
    }

    public delegate void SharedLibraryItemHandler(object o, SharedLibraryItemEventArgs e);

    public class SharedLibraryItemEventArgs : EventArgs
    {
        private LibraryItem libraryItem;

        public LibraryItem LibraryItem
        {
            get { return libraryItem; }
        }

        public SharedLibraryItemEventArgs(LibraryItem item)
        {
            libraryItem = item;
        }
    }
}
