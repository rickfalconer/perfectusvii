using System;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Perfectus.Client.Studio.UI.Dialogs;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;

namespace Perfectus.Client.Studio.UI.SharedLibrary
{
    public class SharedLibraryController
    {
        private SharedLibraryProxy sharedLibraryProxy = new SharedLibraryProxy();
        private SharedLibraryWrapper sharedLibraryWrapper;

        #region IRepository Members

        public bool IsSharedLibraryActivated()
        {
            return sharedLibraryProxy.IsSharedLibraryActivated();
        }

        public bool IsSharedLibraryActivated(out string sharedLibraryLoadMessage)
        {
            return sharedLibraryProxy.IsSharedLibraryActivated(out sharedLibraryLoadMessage);
        }

        public bool IsSharedLibraryClientActivated()
        {
            return sharedLibraryProxy.IsSharedLibraryClientActivated();
        }

        public LibraryItem AddRepositoryItems(LibraryItem libraryItem)
        {
            ServiceProgressDialog serviceProgressDialog = GetProgressDialog(libraryItem, SharedLibraryAction.Add, 0);
            serviceProgressDialog.Start();
            DialogResult result = serviceProgressDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                libraryItem = sharedLibraryWrapper.LibraryItem;
            }
            else
            {
                return null;
            }
            
            return libraryItem;
        }

        public RepositoryFolder GetAllItemsByType(LibraryItemType type)
        {
            return sharedLibraryProxy.GetAllItemsByType(type);
        }

        public LibraryItem GetLatestVersion(LibraryItem libraryItem)
        {
            ServiceProgressDialog serviceProgressDialog = GetProgressDialog(libraryItem, SharedLibraryAction.GetLatestVersion, 0);
            serviceProgressDialog.Start();
            DialogResult result = serviceProgressDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                libraryItem = sharedLibraryWrapper.LibraryItem;
            }
            else
            {
                return null;
            }

            return libraryItem;
        }

        public LibraryItem GetLatestVersion(RepositoryItem repositoryItem)
        {
            ServiceProgressDialog serviceProgressDialog = GetProgressDialog(repositoryItem, SharedLibraryAction.GetLatestVersion, 0);
            serviceProgressDialog.Start();
            DialogResult result = serviceProgressDialog.ShowDialog();

            LibraryItem libraryItem;

            if (result == DialogResult.OK)
            {
                libraryItem = sharedLibraryWrapper.LibraryItem;
            }
            else
            {
                return null;
            }

            return libraryItem;
        }

        public void GetCheckOutStatus(LibraryItem libraryItem)
        {
            sharedLibraryProxy.GetCheckOutStatus(libraryItem);
        }

        public RepositoryItem GetCheckOutStatus(RepositoryItem repositoryItem)
        {
            return sharedLibraryProxy.GetCheckOutStatus(repositoryItem);
        }

        public Boolean CheckIn(LibraryItem libraryItem)
        {
            ServiceProgressDialog serviceProgressDialog = GetProgressDialog(libraryItem, SharedLibraryAction.CheckIn, 0);
            serviceProgressDialog.Start();
            return DialogResult.Cancel != serviceProgressDialog.ShowDialog();
        }

        public LibraryItem CheckOut(LibraryItem libraryItem)
        {
            ServiceProgressDialog serviceProgressDialog = GetProgressDialog(libraryItem, SharedLibraryAction.CheckOut, 0);
            serviceProgressDialog.Start();
            DialogResult result = serviceProgressDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                libraryItem = sharedLibraryWrapper.LibraryItem;
            }
            else
            {
                return null;
            }

            return libraryItem;
        }

        public void UndoCheckout(LibraryItem libraryItem)
        {
            sharedLibraryProxy.UndoCheckout(libraryItem);
        }

        public RepositoryItem DoesRepositoryItemExist(LibraryItem libraryItem)
        {
            return sharedLibraryProxy.DoesRepositoryItemExist(libraryItem);
        }

        public bool CreateNewFolder(RepositoryFolder repositoryFolder, LibraryItemType libraryItemType)
        {
            return sharedLibraryProxy.CreateNewFolder(repositoryFolder, libraryItemType);
        }

        public void Rename(LibraryItem libraryItem, string newFileName)
        {
            sharedLibraryProxy.Rename(libraryItem, newFileName);
        }

        public void Delete(LibraryItem libraryItem)
        {
            sharedLibraryProxy.Delete(libraryItem);
            libraryItem.LibraryUniqueIdentifier = Guid.Empty;
            libraryItem.Linked = false;
            libraryItem.RelativePath = String.Empty;
        }

        public void RollBackCheckIn(LibraryItem libraryItem)
        {
            sharedLibraryProxy.RollBackCheckIn(libraryItem);
        }

        public string Search(string searchText)
        {
            return sharedLibraryProxy.Search(searchText);
        }

        #endregion
        #region Private Members

        private ServiceProgressDialog GetProgressDialog(LibraryItem libraryItem, SharedLibraryAction action, Int32 index)
        {
            string name;
            string message;
            SetSharedLibraryInformation(action, out name, out message);
            sharedLibraryWrapper = new SharedLibraryWrapper(libraryItem, action, index, name, message);
            BackgroundWorkerBase[] backgroundWorkers = new BackgroundWorkerBase[] { sharedLibraryWrapper };
            ServiceProgressDialog serviceProgressDialog = new ServiceProgressDialog(backgroundWorkers);

            return serviceProgressDialog;
        }

        private ServiceProgressDialog GetProgressDialog(RepositoryItem repositoryItem, SharedLibraryAction action, Int32 index)
        {
            string name;
            string message;
            SetSharedLibraryInformation(action, out name, out message);
            sharedLibraryWrapper = new SharedLibraryWrapper(repositoryItem, action, index, name, message);
            BackgroundWorkerBase[] backgroundWorkers = new BackgroundWorkerBase[] { sharedLibraryWrapper };
            ServiceProgressDialog serviceProgressDialog = new ServiceProgressDialog(backgroundWorkers);

            return serviceProgressDialog;
        }

        private void SetSharedLibraryInformation(SharedLibraryAction action, out string name, out string message)
        {
            name = GetResource("SharedLibrary");

            // Set the appropriate message
            switch (action)
            {
                case SharedLibraryAction.Add:
                    message = GetResource("SharedLibraryAdd");
                    break;
                case SharedLibraryAction.CheckIn:
                    message = GetResource("SharedLibraryCheckIn");
                    break;
                case SharedLibraryAction.CheckOut:
                    message = GetResource("SharedLibraryCheckOut");
                    break;
                case SharedLibraryAction.GetLatestVersion:
                    message = GetResource("SharedLibraryGetLatestVersion");
                    break;
                default:
                    message = GetResource("SharedLibraryDefault");
                    break;
            }
        }

        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        #endregion
    }
}