using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Property.Class;
//using Microsoft.Office.Interop.Word;

using DeliveryDocument = Perfectus.Common.PackageObjects.Document;
using System.Xml.Serialization;
using System.IO;
using Perfectus.Common;


namespace Perfectus.Client.Studio.UI.Dialogs
{
    public partial class AddDocumentDialog : Form
    {
        // Group names for converter list view
        private ListViewGroup XmlAnswersetGroup = new ListViewGroup("XML Converter", System.Windows.Forms.HorizontalAlignment.Left);
        //private ListViewGroup WordMLGroup = new ListViewGroup("Document Converter", System.Windows.Forms.HorizontalAlignment.Left);
        private ListViewGroup WordML2k7Group = new ListViewGroup("Document Converter Office 2007", System.Windows.Forms.HorizontalAlignment.Left);

        private TemplateDocumentCollection allTemplates;
        public TemplateDocumentCollection AllTemplates
        {
            set
            {
                allTemplates = value;
                ResetTemplateContainer();
            }
        }

        private PluginDescriptorCollection allPlugins;
        public PluginDescriptorCollection AllPlugins
        {
            set
            {
                allPlugins = value;
                ResetConverterContainer();
            }
        }

        private DeliveryDocument document;
        public DeliveryDocument Document
        {
            get { return document; }
            set
            {
                document = value;
                if (document != null && document.Converter != null)
                    Text = GetResource("AddDocumentDialog.EditWindow");
                else
                    Text = GetResource("AddDocumentDialog.CreateWindow");
            }
        }

        // There are two types of 'converter' in the converter list view
        // 1. Traditional converter contain their properties (e.g. Word Doc). Here the item.Tag is the converter
        // 2. Converter (word 2007) that can select the output type (RTF, PDF etc) and therfore onky show a subset of properties
        //    That converter is using EmbeddedConverter in item.Tag
        internal class EmbeddedConverter
        {
            private Converter _converter;
            private PropertyListEntry _entry;
            internal Converter converter { get { return _converter; } }
            internal PropertyListEntry entry { get { return _entry; } }
            internal EmbeddedConverter(Converter paramConverter, PropertyListEntry paramEntry)
            {
                _converter = paramConverter;
                _entry = paramEntry;
            }
        }

        private void ResetConverterContainer()
        {
            lstViewConverter.Items.Clear();
            if (allPlugins != null)
            {
                // Known plugins
                foreach (PluginDescriptor p in allPlugins)
                {
                    if (p.PluginKind != PluginBehaviour.Converter)
                        continue;

                    ListViewItem item = null;
                    Converter c = new Converter(p);

                    // If a plugin has a property of type Perfectus.Common.Property,
                    // then we need to interpret XML rather than the property holding the XML
                    String xml_property_fieldname;
                    if (IsExtendedPropertyPlugin(c, out xml_property_fieldname))
                    {
                        PropertyList selection = PropertyList.CreateFromXML(c[xml_property_fieldname].ToString());

                        // Insert one converter entry into the view for each 'mode' that is supported
                        foreach (PropertyListEntry entry in selection.ListElements)
                        {
                            //pf-3266 doc converters are no longer supported
                            if (entry.InternalName == "wdFormatDocument97")
                                continue;

                            EmbeddedConverter eConverter = new EmbeddedConverter(c, entry);

                            // Group name if not already set
                            if (!lstViewConverter.Groups.Contains(WordML2k7Group))
                                lstViewConverter.Groups.Add(WordML2k7Group);

                            item = new ListViewItem(entry.FriendlyName, 0, WordML2k7Group);
                            item.Tag = eConverter;
                            lstViewConverter.Items.Add(item);
                        }
                        continue;
                    }

                    // Old style converter
                    // XML converter
                    if (p.PreferredDoc == "AnswerSetXml")
                    {
                        if (!lstViewConverter.Groups.Contains(XmlAnswersetGroup))
                            lstViewConverter.Groups.Add(XmlAnswersetGroup);
                        item = new ListViewItem(c.Name, 1, XmlAnswersetGroup);
                    }
                    else
                    // document converter
                    {
                        //pf-3266 doc converters are no longer supported
                        continue;

                        //if (!lstViewConverter.Groups.Contains(WordMLGroup))
                        //    lstViewConverter.Groups.Add(WordMLGroup);
                        //item = new ListViewItem(c.Name, 0, WordMLGroup);
                    }
                    item.Tag = c;
                    lstViewConverter.Items.Add(item);
                }
            }
        }

        private void ResetTemplateContainer()
        {
            lstTemplates.Items.Clear();
            if (allTemplates != null)
                foreach (TemplateDocument d in allTemplates)
                {
                    ListViewItem item = new ListViewItem(d.Name, 2);
                    item.Tag = d;
                    lstTemplates.Items.Add(item);
                }
            // seems to need this
            lstTemplates.View = System.Windows.Forms.View.SmallIcon;
        }

        public AddDocumentDialog()
        {
            InitializeComponent();
        }

        private void lstViewConverter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstViewConverter.SelectedItems.Count == 1)
            {
                Converter c = null;

                // Old style converter
                if (lstViewConverter.SelectedItems[0].Tag is Converter)
                {
                    c = lstViewConverter.SelectedItems[0].Tag as Converter;

                    if (document.Converter == null || document.Converter.Descriptor.UniqueIdentifier != c.Descriptor.UniqueIdentifier)
                        document.SetConverter(c);

                    // Disable the template view if XML converter is used
                    SetTemplateEnabled(c.Descriptor.PreferredDoc != "AnswerSetXml");

                    this.propertyGridConverter.SelectedObject = document.Converter;
                }
                else if (lstViewConverter.SelectedItems[0].Tag is EmbeddedConverter)
                {
                    EmbeddedConverter ec = lstViewConverter.SelectedItems[0].Tag as EmbeddedConverter;
                    PropertyListEntry currentSelection = ec.entry;

                    // Get the entire XML as class 
                    // The property contains all properties and values of each output format (PDF, RTF etc.)
                    PropertyList selection = PropertyList.CreateFromXML(ec.converter["Format"].ToString());

                    // Set the selected output format
                    selection.CurrentElement = currentSelection;

                    // Convert the class back to an XML string...
                    String text = String.Empty;
                    XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
                    using (MemoryStream m = new MemoryStream())
                    {
                        ser.Serialize(m, selection);
                        text = Encoding.ASCII.GetString(m.ToArray(), 0, (int)m.Length);
                    }

                    // and assign it to the converter object in use
                    ec.converter["Format"] = text;

                    document.SetConverter(ec.converter);

                    //this.propertyGridConverter.Visible = ec.entry.properties.Count > 0;
                    this.propertyGridConverter.SelectedObject = ec.entry;
                    SetTemplateEnabled(true);
                }
                else
                {
                    document.SetConverter(null);
                    SetTemplateEnabled(false);
                }
            }
            SetOkButton();
        }

        private void SetTemplateEnabled(bool p)
        {
            lstTemplates.Enabled = p;
            if (!p)
                lstTemplates.SelectedItems.Clear();
        }

        private void lstTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstTemplates.SelectedItems.Count > 0)
                document.SetTemplate(lstTemplates.SelectedItems[0].Tag as TemplateDocument);
            else
                document.SetTemplate(null);

            this.propertyGridDocument.Refresh();
            SetOkButton();
        }

        private void lstTemplates_EnabledChanged(object sender, EventArgs e)
        {
            // want to remove the template from the document object (its an XML converter)
            if (!lstTemplates.Enabled)
                document.SetTemplate(null);
            SetOkButton();
        }

        private void SetOkButton()
        {
            btnOk.Enabled = document.Valid() && lstViewConverter.SelectedItems.Count == 1;
        }

        private bool IsExtendedPropertyPlugin(Converter converter, out String Fieldname)
        {
            Fieldname = String.Empty;
            if (converter.Descriptor.PropertyDescriptors.Length == 1)
            {
                PluginPropertyDescriptor pd = converter.Descriptor.PropertyDescriptors[0];
                if (pd.Converter.Contains("Perfectus.Common.Property"))
                {
                    Fieldname = pd.Name;
                    return true;
                }
            }
            return false;
        }

        private void AddDocumentDialog_Load(object sender, EventArgs e)
        {
            // Test existing documents that are being edited
            string message;
            if ((document.Converter != null ||
                document.Template != null ||
                document.FileName != null) &&
                !document.Valid(out message)
                )
            {
                string caption = GetResource("AddDocumentDialog.InvalidDocumentCaption");
                MessageBox.Show(GetResource("AddDocumentDialog.InvalidDocumentMessage") + message, 
                    GetResource("AddDocumentDialog.InvalidDocumentCaption"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
                document.SetConverter(null);
            }

            // Find and select the template
            if (document.Template != null)
            {
                bool haveFoundTemplate = false;
                foreach (ListViewItem item in lstTemplates.Items)
                {
                    TemplateDocument template = item.Tag as TemplateDocument;
                    if (template.UniqueIdentifier == document.Template.UniqueIdentifier)
                    {
                        haveFoundTemplate = item.Selected = true;
                        break;
                    }
                }

                // If the Template was deleted from the package, then remove the
                // reference in the document as well
                if (!haveFoundTemplate)
                    document.SetTemplate(null);
                else
                {
                    // Scroll the entry into view
                    lstTemplates.View = System.Windows.Forms.View.List;
                    lstTemplates.TopItem = lstTemplates.SelectedItems[0];
                    lstTemplates.View = System.Windows.Forms.View.SmallIcon;
                }
            }

            // Find and select converter
            if (document.Converter != null)
            {
                bool haveFoundConverter = false;
                String filename;
                if (IsExtendedPropertyPlugin(document.Converter, out filename))
                {
                    // Deserialize all possible output formats (and properties) from the plugin
                    String text = document.Converter[filename].ToString();
                    PropertyList data = PropertyList.CreateFromXML(text);
                    PropertyListEntry currentSelection = data.CurrentElement;

                    // Find and select the entry in the list 
                    foreach (ListViewItem item in lstViewConverter.Items)
                    {
                        // Don't consider old style converters
                        if (!(item.Tag is EmbeddedConverter))
                            continue;

                        EmbeddedConverter ec = item.Tag as EmbeddedConverter;

                        // Check the converter (two different converters might have the same friendly name)
                        if (ec.converter.Descriptor.UniqueIdentifier != document.Converter.Descriptor.UniqueIdentifier)
                            continue;

                        // Find the output format
                        if (ec.entry.FriendlyName == currentSelection.FriendlyName)
                        {
                            item.Tag = new EmbeddedConverter(document.Converter, currentSelection);
                            haveFoundConverter = item.Selected = true;
                            break;
                        }
                    }
                }
                else
                    // old style plugin is used
                    foreach (ListViewItem item in lstViewConverter.Items)
                    {
                        Converter converter = item.Tag as Converter;
                        if (converter != null && converter.Descriptor.UniqueIdentifier == document.Converter.Descriptor.UniqueIdentifier)
                        {
                            haveFoundConverter = item.Selected = true;
                            break;
                        }
                    }

                // remove converter if it was not loaded.
                if (!haveFoundConverter)
                {
                    document.SetConverter(null);
                    //pf-3281 old doc converter no longer supported
                    MessageBox.Show("The existing converter is not valid, please select a converter for this document",
                        "Converter invalid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }

                // Scroll the entry into view
                lstViewConverter.View = System.Windows.Forms.View.List;
                if (lstViewConverter.SelectedItems.Count > 0)
                    lstViewConverter.TopItem = lstViewConverter.SelectedItems[0];
                lstViewConverter.View = System.Windows.Forms.View.Details;
            }
            else
            {
                lstViewConverter.View = System.Windows.Forms.View.List;
                if (lstViewConverter.SelectedItems.Count > 0)
                    lstViewConverter.TopItem = lstViewConverter.Items[0];
                lstViewConverter.View = System.Windows.Forms.View.Details;
            }

            propertyGridDocument.SelectedObject = document;
            SetOkButton();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            // Need to serialize any pending changes 
            if (lstViewConverter.SelectedItems[0] == null)
                return;

            if (!(lstViewConverter.SelectedItems[0].Tag is EmbeddedConverter))
                return;

            // Get the current selection and serialize it.
            // then store it as XML with the converter object
            EmbeddedConverter ec = lstViewConverter.SelectedItems[0].Tag as EmbeddedConverter;
            String fieldName;
            IsExtendedPropertyPlugin(ec.converter, out fieldName);
            PropertyListEntry currentSelection = ec.entry;

            PropertyList selection = PropertyList.CreateFromXML(ec.converter[fieldName].ToString());
            selection.CurrentElement = currentSelection;

            String text = String.Empty;
            XmlSerializer ser = new XmlSerializer(typeof(PropertyList));
            using (MemoryStream m = new MemoryStream())
            {
                ser.Serialize(m, selection);
                text = Encoding.ASCII.GetString(m.ToArray(), 0, (int)m.Length);
            }
            ec.converter[fieldName] = text;
        }
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }
    }
}