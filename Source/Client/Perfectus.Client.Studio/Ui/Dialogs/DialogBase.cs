using System.ComponentModel;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	/// <summary>
	/// Summary description for DialogBase.
	/// </summary>
	public class DialogBase : Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public DialogBase()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			//TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// DialogBase
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(488, 286);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "DialogBase";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "DialogBase";

		}

		#endregion
	}
}