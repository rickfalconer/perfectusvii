using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Input;
//using DataDynamics.SharpUI.Panels;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Client.Studio.WebApi.ConfigurationSystem.Server;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Server = Perfectus.Common.PackageObjects.Server;
using Perfectus.Client.Studio.Versioning;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	/// <summary>
	/// Summary description for PublishDialog.
	/// </summary>
	public class PublishDialog : DialogBase
	{
		private Button btnCancel;
        private ImageList imageList1;
		private CheckBox cbxIncrementVersion;
		private IContainer components;
        private Button btnNext;
		private Package package;
		private System.Windows.Forms.Label lblChooseProcessManagers;
        private System.Windows.Forms.Label lblFinishedBlurb;
		private ServiceProgressDialog dlg;
		private System.Windows.Forms.Panel pnlPluginContainer;
		private IPublisher publisherPlugin = null;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;

        private String packageVersionXml;
        public String PackageVersionXml
        {
            set { packageVersionXml = value; }
        }
	
	    public PublishDialog(Package package)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.package = package;

            lblChooseProcessManagers.Visible = true;
            uiListBox1.Visible = true;
            cbxIncrementVersion.Visible = true;
            btnNext.Visible = true;

            lblFinishedBlurb.Visible = false;
            pnlPluginContainer.Visible = false;

			BindList();
		}

		private void BindList()
		{
			foreach (Server s in package.Servers)
			{
                ListViewItem item = new ListViewItem() { Text = s.Name, Tag = s, ImageIndex = 0 };
				uiListBox1.Items.Add(item);
			}
            if (package.Servers.Count == 1)
            {
                uiListBox1.FocusedItem = uiListBox1.Items[0];
            }
            //else
            //{
            //    uiListBox1.SelectedIndex =  -1;
            //}

			// There's only one page before publish now...
			btnNext.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.PublishButtonText");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PublishDialog));
            this.btnNext = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblChooseProcessManagers = new System.Windows.Forms.Label();
            this.cbxIncrementVersion = new System.Windows.Forms.CheckBox();
            this.pnlPluginContainer = new System.Windows.Forms.Panel();
            this.lblFinishedBlurb = new System.Windows.Forms.Label();
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnNext
            // 
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNext.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNext.Location = new System.Drawing.Point(224, 232);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 91;
            this.btnNext.Text = "&Next >";
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(305, 232);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 92;
            this.btnCancel.Text = "&Cancel";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // lblChooseProcessManagers
            // 
            this.lblChooseProcessManagers.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblChooseProcessManagers.Location = new System.Drawing.Point(12, 9);
            this.lblChooseProcessManagers.Name = "lblChooseProcessManagers";
            this.lblChooseProcessManagers.Size = new System.Drawing.Size(368, 16);
            this.lblChooseProcessManagers.TabIndex = 13;
            this.lblChooseProcessManagers.Text = "Choose a ProcessManager";
            // 
            // cbxIncrementVersion
            // 
            this.cbxIncrementVersion.Checked = true;
            this.cbxIncrementVersion.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxIncrementVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cbxIncrementVersion.Location = new System.Drawing.Point(15, 182);
            this.cbxIncrementVersion.Name = "cbxIncrementVersion";
            this.cbxIncrementVersion.Size = new System.Drawing.Size(238, 29);
            this.cbxIncrementVersion.TabIndex = 1;
            this.cbxIncrementVersion.Text = "Mark this Package as a new version.";
            // 
            // pnlPluginContainer
            // 
            this.pnlPluginContainer.AutoScroll = true;
            this.pnlPluginContainer.Location = new System.Drawing.Point(10, 28);
            this.pnlPluginContainer.Name = "pnlPluginContainer";
            this.pnlPluginContainer.Size = new System.Drawing.Size(376, 198);
            this.pnlPluginContainer.TabIndex = 15;
            // 
            // lblFinishedBlurb
            // 
            this.lblFinishedBlurb.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblFinishedBlurb.Location = new System.Drawing.Point(12, 9);
            this.lblFinishedBlurb.Name = "lblFinishedBlurb";
            this.lblFinishedBlurb.Size = new System.Drawing.Size(368, 16);
            this.lblFinishedBlurb.TabIndex = 14;
            this.lblFinishedBlurb.Text = "Your package was successfully published.";
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Location = new System.Drawing.Point(10, 28);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(372, 148);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.uiListBox1.TabIndex = 93;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 400;
            // 
            // PublishDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(394, 267);
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.lblChooseProcessManagers);
            this.Controls.Add(this.pnlPluginContainer);
            this.Controls.Add(this.cbxIncrementVersion);
            this.Controls.Add(this.lblFinishedBlurb);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PublishDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.Text = "Publish to ProcessManagers";
            this.TopMost = true;
            this.ResumeLayout(false);

		}

		#endregion

		private void btnNext_Click(object sender, EventArgs e)
		{
			PickServers();
		}

		private bool PickServers()
		{
			if (uiListBox1.FocusedItem == null)
			{
				MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.NoServerChosenError"), Common.About.FormsTitle);
				return false;
			}
			else
			{
				Server s = uiListBox1.FocusedItem.Tag as Server;
				if (s != null)
				{
					ServerPublishInfoWrapper wrapper = new ServerPublishInfoWrapper(0, new Uri(s.Address));
                    dlg = new ServiceProgressDialog(new BackgroundWorkerBase[] { wrapper });
                    dlg.Start();
                    					
					DialogResult dr = dlg.ShowDialog();
					if (dr == DialogResult.Cancel)
					{
						wrapper.Abort(false);
						return false;
					}
					else if (dr == DialogResult.OK)
					{
						if (wrapper.Info != null)
						{
							if (! LaunchPublishPlugin(wrapper.Info, s))
							{
								return false;
							}
							else
							{
								// Do the publish to the server.
								PublishWrapper t = new PublishWrapper(0, s, package, cbxIncrementVersion.Checked);
                                dlg = new ServiceProgressDialog(new BackgroundWorkerBase[] { t });
								dlg.Start();
								DialogResult drPub = dlg.ShowDialog();
								if (drPub == DialogResult.Cancel)
								{
									t.Abort(false);
									return false;
								}
								else
								{
                                    lblChooseProcessManagers.Visible = false;
                                    uiListBox1.Visible = false;
                                    cbxIncrementVersion.Visible = false;

                                    lblFinishedBlurb.Visible = true;
                                    pnlPluginContainer.Visible = true;
	
									// Publish worked.
									// Now  instantiate the plug-in.
									if (publisherPlugin != null)
									{
										btnNext.Visible = false;
										publisherPlugin.VersionNumber = t.VersionNumber;
										publisherPlugin.RevisionNumber = t.RevisionNumber;
										publisherPlugin.PackageId = package.UniqueIdentifier;
										publisherPlugin.PackageName = package.Name;
										publisherPlugin.MyDocumentsAddress = t.MyDocumentsURL;
                                        publisherPlugin.SuggestedFileName = MainDesigner2.LegaliseFilename(package.Name);
                                        Control plugControl = (Control)publisherPlugin;
										plugControl.Dock = DockStyle.Fill;
										pnlPluginContainer.Controls.Add(plugControl);										
										publisherPlugin.ResetUI();
										btnCancel.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.CloseButtonText");

                                        try
                                        {
                                            PackageVersionInfo pi = PackageVersionInfo.Create(package.VersionXml);

                                            if (pi != null)
                                            {
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedEngine", publisherPlugin.PluginName);
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedDate", DateTime.Now.ToUniversalTime().ToString());
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedServer", s.Address);
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedPackageID", package.UniqueIdentifier.ToString());
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedPackageVersion", t.VersionNumber.ToString());
                                                PackageVersionInfo.SetCustomData(ref pi, "LastPublishedPackageRevision", t.RevisionNumber.ToString());

                                                package.VersionXml = PackageVersionInfo.ConvertToXml(pi);
                                            }
                                        }
                                        catch { };

									}
								}
							}

						}
						else
						{
							return false;
						}
					}
				}
			}
			return false;
		}


		private bool LaunchPublishPlugin(ServerPublishInfo info, Server server)
		{
			bool retVal = false;
			if (info.AssemblyName == null || info.AssemblyName.Trim().Length == 0)
			{
				return false;
			}
			else
			{
				try
				{
					string assemblyName = Path.GetFileNameWithoutExtension(info.AssemblyName);
					string typeName = info.TypeName;

					string combined = string.Format("{0}, {1}", typeName, assemblyName);

                    //Assembly ass = System.Reflection.Assembly.LoadFrom(string.Format(@"{0}\{1}", Application.StartupPath, info.AssemblyName));
                    //Type t = ass.GetType(info.TypeName, true, true);

                    Type t = Type.GetType(combined, true, true);

					object o = Activator.CreateInstance(t);
					if (! (o is IPublisher))
					{
						throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.IPublisherNotImplemented"));
					} else
					if (! (o is Control))
					{
						throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.PluginNotAControl"));
					}
					else
					{
						publisherPlugin = (IPublisher)o;
						publisherPlugin.BaseUiUrl = server.Address;
						publisherPlugin.MyDocumentsAddress = server.MyDocumentsURL;					    

						retVal = true;
					}
				}
				catch (Exception ex)
				{
					string msgstr=ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.PublishDialog.PluginError");
					msgstr = msgstr.Replace("{0}", info.TypeName);
					MessageBox.Show (string.Format("{0} \n{1}", msgstr, ex.Message));
					retVal = false;
				}
				return retVal;
			}			
		}
	}
}