using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	public class AddWebReference : DialogBase
	{
		private Label lblProgress;
		private GroupBox groupBox1;
		private Button btnConnect;
		private Label label1;
		private Button btnOk;
		private Button btnCancel;
		private TextBox txtAddress;
		private ErrorProvider errorProvider1;
		private IContainer components = null;

		private Uri address = null;
		private ImageList ilistTreeView;
		private TreeView treeView1;
		private string layoutXml = null;

		public Uri Address
		{
			get { return address; }
		}

		public string LayoutXml
		{
			get { return layoutXml; }
		}

		public AddWebReference()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddWebReference));
			this.lblProgress = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
			this.btnConnect = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblProgress
			// 
			this.lblProgress.AccessibleDescription = resources.GetString("lblProgress.AccessibleDescription");
			this.lblProgress.AccessibleName = resources.GetString("lblProgress.AccessibleName");
			this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblProgress.Anchor")));
			this.lblProgress.AutoSize = ((bool)(resources.GetObject("lblProgress.AutoSize")));
			this.lblProgress.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblProgress.Dock")));
			this.lblProgress.Enabled = ((bool)(resources.GetObject("lblProgress.Enabled")));
			this.errorProvider1.SetError(this.lblProgress, resources.GetString("lblProgress.Error"));
			this.lblProgress.Font = ((System.Drawing.Font)(resources.GetObject("lblProgress.Font")));
			this.errorProvider1.SetIconAlignment(this.lblProgress, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("lblProgress.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.lblProgress, ((int)(resources.GetObject("lblProgress.IconPadding"))));
			this.lblProgress.Image = ((System.Drawing.Image)(resources.GetObject("lblProgress.Image")));
			this.lblProgress.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblProgress.ImageAlign")));
			this.lblProgress.ImageIndex = ((int)(resources.GetObject("lblProgress.ImageIndex")));
			this.lblProgress.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblProgress.ImeMode")));
			this.lblProgress.Location = ((System.Drawing.Point)(resources.GetObject("lblProgress.Location")));
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblProgress.RightToLeft")));
			this.lblProgress.Size = ((System.Drawing.Size)(resources.GetObject("lblProgress.Size")));
			this.lblProgress.TabIndex = ((int)(resources.GetObject("lblProgress.TabIndex")));
			this.lblProgress.Text = resources.GetString("lblProgress.Text");
			this.lblProgress.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblProgress.TextAlign")));
			this.lblProgress.Visible = ((bool)(resources.GetObject("lblProgress.Visible")));
			// 
			// groupBox1
			// 
			this.groupBox1.AccessibleDescription = resources.GetString("groupBox1.AccessibleDescription");
			this.groupBox1.AccessibleName = resources.GetString("groupBox1.AccessibleName");
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("groupBox1.Anchor")));
			this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
			this.groupBox1.Controls.Add(this.treeView1);
			this.groupBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("groupBox1.Dock")));
			this.groupBox1.Enabled = ((bool)(resources.GetObject("groupBox1.Enabled")));
			this.errorProvider1.SetError(this.groupBox1, resources.GetString("groupBox1.Error"));
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Font = ((System.Drawing.Font)(resources.GetObject("groupBox1.Font")));
			this.errorProvider1.SetIconAlignment(this.groupBox1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("groupBox1.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.groupBox1, ((int)(resources.GetObject("groupBox1.IconPadding"))));
			this.groupBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("groupBox1.ImeMode")));
			this.groupBox1.Location = ((System.Drawing.Point)(resources.GetObject("groupBox1.Location")));
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("groupBox1.RightToLeft")));
			this.groupBox1.Size = ((System.Drawing.Size)(resources.GetObject("groupBox1.Size")));
			this.groupBox1.TabIndex = ((int)(resources.GetObject("groupBox1.TabIndex")));
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = resources.GetString("groupBox1.Text");
			this.groupBox1.Visible = ((bool)(resources.GetObject("groupBox1.Visible")));
			// 
			// treeView1
			// 
			this.treeView1.AccessibleDescription = resources.GetString("treeView1.AccessibleDescription");
			this.treeView1.AccessibleName = resources.GetString("treeView1.AccessibleName");
			this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("treeView1.Anchor")));
			this.treeView1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("treeView1.BackgroundImage")));
			this.treeView1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("treeView1.Dock")));
			this.treeView1.Enabled = ((bool)(resources.GetObject("treeView1.Enabled")));
			this.errorProvider1.SetError(this.treeView1, resources.GetString("treeView1.Error"));
			this.treeView1.Font = ((System.Drawing.Font)(resources.GetObject("treeView1.Font")));
			this.errorProvider1.SetIconAlignment(this.treeView1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("treeView1.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.treeView1, ((int)(resources.GetObject("treeView1.IconPadding"))));
			this.treeView1.ImageIndex = ((int)(resources.GetObject("treeView1.ImageIndex")));
			this.treeView1.ImageList = this.ilistTreeView;
			this.treeView1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("treeView1.ImeMode")));
			this.treeView1.Indent = ((int)(resources.GetObject("treeView1.Indent")));
			this.treeView1.ItemHeight = ((int)(resources.GetObject("treeView1.ItemHeight")));
			this.treeView1.Location = ((System.Drawing.Point)(resources.GetObject("treeView1.Location")));
			this.treeView1.Name = "treeView1";
			this.treeView1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("treeView1.RightToLeft")));
			this.treeView1.SelectedImageIndex = ((int)(resources.GetObject("treeView1.SelectedImageIndex")));
			this.treeView1.Size = ((System.Drawing.Size)(resources.GetObject("treeView1.Size")));
			this.treeView1.TabIndex = ((int)(resources.GetObject("treeView1.TabIndex")));
			this.treeView1.Text = resources.GetString("treeView1.Text");
			this.treeView1.Visible = ((bool)(resources.GetObject("treeView1.Visible")));
			this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			// 
			// ilistTreeView
			// 
			this.ilistTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ilistTreeView.ImageSize = ((System.Drawing.Size)(resources.GetObject("ilistTreeView.ImageSize")));
			this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
			this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// btnConnect
			// 
			this.btnConnect.AccessibleDescription = resources.GetString("btnConnect.AccessibleDescription");
			this.btnConnect.AccessibleName = resources.GetString("btnConnect.AccessibleName");
			this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnConnect.Anchor")));
			this.btnConnect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConnect.BackgroundImage")));
			this.btnConnect.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnConnect.Dock")));
			this.btnConnect.Enabled = ((bool)(resources.GetObject("btnConnect.Enabled")));
			this.errorProvider1.SetError(this.btnConnect, resources.GetString("btnConnect.Error"));
			this.btnConnect.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnConnect.FlatStyle")));
			this.btnConnect.Font = ((System.Drawing.Font)(resources.GetObject("btnConnect.Font")));
			this.errorProvider1.SetIconAlignment(this.btnConnect, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnConnect.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnConnect, ((int)(resources.GetObject("btnConnect.IconPadding"))));
			this.btnConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnConnect.Image")));
			this.btnConnect.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnConnect.ImageAlign")));
			this.btnConnect.ImageIndex = ((int)(resources.GetObject("btnConnect.ImageIndex")));
			this.btnConnect.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnConnect.ImeMode")));
			this.btnConnect.Location = ((System.Drawing.Point)(resources.GetObject("btnConnect.Location")));
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnConnect.RightToLeft")));
			this.btnConnect.Size = ((System.Drawing.Size)(resources.GetObject("btnConnect.Size")));
			this.btnConnect.TabIndex = ((int)(resources.GetObject("btnConnect.TabIndex")));
			this.btnConnect.Text = resources.GetString("btnConnect.Text");
			this.btnConnect.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnConnect.TextAlign")));
			this.btnConnect.Visible = ((bool)(resources.GetObject("btnConnect.Visible")));
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.errorProvider1.SetError(this.label1, resources.GetString("label1.Error"));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.errorProvider1.SetIconAlignment(this.label1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label1.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.label1, ((int)(resources.GetObject("label1.IconPadding"))));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.errorProvider1.SetError(this.btnOk, resources.GetString("btnOk.Error"));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.errorProvider1.SetIconAlignment(this.btnOk, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnOk.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnOk, ((int)(resources.GetObject("btnOk.IconPadding"))));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.CausesValidation = false;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.errorProvider1.SetError(this.btnCancel, resources.GetString("btnCancel.Error"));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.errorProvider1.SetIconAlignment(this.btnCancel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnCancel.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnCancel, ((int)(resources.GetObject("btnCancel.IconPadding"))));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// txtAddress
			// 
			this.txtAddress.AccessibleDescription = resources.GetString("txtAddress.AccessibleDescription");
			this.txtAddress.AccessibleName = resources.GetString("txtAddress.AccessibleName");
			this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAddress.Anchor")));
			this.txtAddress.AutoSize = ((bool)(resources.GetObject("txtAddress.AutoSize")));
			this.txtAddress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAddress.BackgroundImage")));
			this.txtAddress.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAddress.Dock")));
			this.txtAddress.Enabled = ((bool)(resources.GetObject("txtAddress.Enabled")));
			this.errorProvider1.SetError(this.txtAddress, resources.GetString("txtAddress.Error"));
			this.txtAddress.Font = ((System.Drawing.Font)(resources.GetObject("txtAddress.Font")));
			this.errorProvider1.SetIconAlignment(this.txtAddress, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtAddress.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.txtAddress, ((int)(resources.GetObject("txtAddress.IconPadding"))));
			this.txtAddress.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAddress.ImeMode")));
			this.txtAddress.Location = ((System.Drawing.Point)(resources.GetObject("txtAddress.Location")));
			this.txtAddress.MaxLength = ((int)(resources.GetObject("txtAddress.MaxLength")));
			this.txtAddress.Multiline = ((bool)(resources.GetObject("txtAddress.Multiline")));
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.PasswordChar = ((char)(resources.GetObject("txtAddress.PasswordChar")));
			this.txtAddress.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAddress.RightToLeft")));
			this.txtAddress.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAddress.ScrollBars")));
			this.txtAddress.Size = ((System.Drawing.Size)(resources.GetObject("txtAddress.Size")));
			this.txtAddress.TabIndex = ((int)(resources.GetObject("txtAddress.TabIndex")));
			this.txtAddress.Text = resources.GetString("txtAddress.Text");
			this.txtAddress.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAddress.TextAlign")));
			this.txtAddress.Visible = ((bool)(resources.GetObject("txtAddress.Visible")));
			this.txtAddress.WordWrap = ((bool)(resources.GetObject("txtAddress.WordWrap")));
			this.txtAddress.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress_Validating);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
			// 
			// AddWebReference
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.txtAddress);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "AddWebReference";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private void txtAddress_Validating(object sender, CancelEventArgs e)
		{
			try
			{
				new Uri(txtAddress.Text);
				errorProvider1.SetError(txtAddress, string.Empty);
				e.Cancel = false;
			}
			catch (UriFormatException)
			{
				string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddWebReference.InvalidUrlError");
				msg = msg.Replace("\\n", "\n");
				errorProvider1.SetError(txtAddress, msg);
				e.Cancel = true;
			}

		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			btnOk.Enabled = false;
			try
			{
				Cursor = Cursors.WaitCursor;
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddWebReference.ProgressContacting");
				address = new Uri(txtAddress.Text);
				layoutXml = WebReference.GetLayoutXml(address);
				treeView1.Nodes.Clear();
				TreeNode rootNode = new TreeNode();
				rootNode.SelectedImageIndex = (int) TreeIcons.URL;
				rootNode.ImageIndex = (int) TreeIcons.URL;
				rootNode.Text = address.ToString();
				DecorateServiceNode.Decorate(null, rootNode, layoutXml, ServiceNodeFilter.All);
				treeView1.Nodes.Add(rootNode);
				btnOk.Enabled = true;
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddWebReference.ProgressFinished");
			}
				//TODO: Provide more information to the user.  Full on message even? "Click here to see the details..."
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddWebReference.Error");
				

				this.address = null;
				this.layoutXml = null;
				treeView1.Nodes.Clear();
			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

		private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
		{
#if   DEBUG
			if (e.Node.Tag != null)
			{
				lblProgress.Text = e.Node.Tag.ToString();
			}
#endif
		}

		private void btnOk_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}