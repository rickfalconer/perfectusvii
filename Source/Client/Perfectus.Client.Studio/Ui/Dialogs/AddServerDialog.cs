 using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	[Obsolete("Replaced by AddServerDialog2.")]
	public class AddServerDialog : DialogBase
	{
		private ErrorProvider errorProvider1;
		private Button btnOk;
		private Button btnCancel;
		private Label label1;
		private Button btnConnect;
		private GroupBox groupBox1;
		private Label label2;
		private Label label3;
		private Label label4;
		private TextBox txtAddress;
		private TextBox txtName;
		private TextBox txtMessage;
		private TextBox txtVersion;
		private Label lblProgress;
		private IContainer components = null;
		private Uri address;

		private ServerInfo serverInfo;
		private PluginDescriptor[] plugins;

		public Uri Address
		{
			get { return address; }
		}

		public ServerInfo ServerInfo
		{
			get { return serverInfo; }
		}

		public PluginDescriptor[] Plugins
		{
			get { return plugins; }
		}

		public AddServerDialog()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

#if   DEBUG
			txtAddress.Text = "http://localhost/perfectus.server.webapi";

#endif
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddServerDialog));
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.btnConnect = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtVersion = new System.Windows.Forms.TextBox();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.lblProgress = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtAddress
			// 
			this.txtAddress.AccessibleDescription = resources.GetString("txtAddress.AccessibleDescription");
			this.txtAddress.AccessibleName = resources.GetString("txtAddress.AccessibleName");
			this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAddress.Anchor")));
			this.txtAddress.AutoSize = ((bool)(resources.GetObject("txtAddress.AutoSize")));
			this.txtAddress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAddress.BackgroundImage")));
			this.txtAddress.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAddress.Dock")));
			this.txtAddress.Enabled = ((bool)(resources.GetObject("txtAddress.Enabled")));
			this.errorProvider1.SetError(this.txtAddress, resources.GetString("txtAddress.Error"));
			this.txtAddress.Font = ((System.Drawing.Font)(resources.GetObject("txtAddress.Font")));
			this.errorProvider1.SetIconAlignment(this.txtAddress, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtAddress.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.txtAddress, ((int)(resources.GetObject("txtAddress.IconPadding"))));
			this.txtAddress.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAddress.ImeMode")));
			this.txtAddress.Location = ((System.Drawing.Point)(resources.GetObject("txtAddress.Location")));
			this.txtAddress.MaxLength = ((int)(resources.GetObject("txtAddress.MaxLength")));
			this.txtAddress.Multiline = ((bool)(resources.GetObject("txtAddress.Multiline")));
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.PasswordChar = ((char)(resources.GetObject("txtAddress.PasswordChar")));
			this.txtAddress.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAddress.RightToLeft")));
			this.txtAddress.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAddress.ScrollBars")));
			this.txtAddress.Size = ((System.Drawing.Size)(resources.GetObject("txtAddress.Size")));
			this.txtAddress.TabIndex = ((int)(resources.GetObject("txtAddress.TabIndex")));
			this.txtAddress.Text = resources.GetString("txtAddress.Text");
			this.txtAddress.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAddress.TextAlign")));
			this.txtAddress.Visible = ((bool)(resources.GetObject("txtAddress.Visible")));
			this.txtAddress.WordWrap = ((bool)(resources.GetObject("txtAddress.WordWrap")));
			this.txtAddress.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAddress_KeyDown);
			this.txtAddress.Validating += new System.ComponentModel.CancelEventHandler(this.textBox1_Validating);
			this.txtAddress.TextChanged += new System.EventHandler(this.txtAddress_TextChanged);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			this.errorProvider1.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider1.Icon")));
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.errorProvider1.SetError(this.btnOk, resources.GetString("btnOk.Error"));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.errorProvider1.SetIconAlignment(this.btnOk, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnOk.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnOk, ((int)(resources.GetObject("btnOk.IconPadding"))));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.CausesValidation = false;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.errorProvider1.SetError(this.btnCancel, resources.GetString("btnCancel.Error"));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.errorProvider1.SetIconAlignment(this.btnCancel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnCancel.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnCancel, ((int)(resources.GetObject("btnCancel.IconPadding"))));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// label1
			// 
			this.label1.AccessibleDescription = resources.GetString("label1.AccessibleDescription");
			this.label1.AccessibleName = resources.GetString("label1.AccessibleName");
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label1.Anchor")));
			this.label1.AutoSize = ((bool)(resources.GetObject("label1.AutoSize")));
			this.label1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label1.Dock")));
			this.label1.Enabled = ((bool)(resources.GetObject("label1.Enabled")));
			this.errorProvider1.SetError(this.label1, resources.GetString("label1.Error"));
			this.label1.Font = ((System.Drawing.Font)(resources.GetObject("label1.Font")));
			this.errorProvider1.SetIconAlignment(this.label1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label1.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.label1, ((int)(resources.GetObject("label1.IconPadding"))));
			this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
			this.label1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.ImageAlign")));
			this.label1.ImageIndex = ((int)(resources.GetObject("label1.ImageIndex")));
			this.label1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label1.ImeMode")));
			this.label1.Location = ((System.Drawing.Point)(resources.GetObject("label1.Location")));
			this.label1.Name = "label1";
			this.label1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label1.RightToLeft")));
			this.label1.Size = ((System.Drawing.Size)(resources.GetObject("label1.Size")));
			this.label1.TabIndex = ((int)(resources.GetObject("label1.TabIndex")));
			this.label1.Text = resources.GetString("label1.Text");
			this.label1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label1.TextAlign")));
			this.label1.Visible = ((bool)(resources.GetObject("label1.Visible")));
			// 
			// btnConnect
			// 
			this.btnConnect.AccessibleDescription = resources.GetString("btnConnect.AccessibleDescription");
			this.btnConnect.AccessibleName = resources.GetString("btnConnect.AccessibleName");
			this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnConnect.Anchor")));
			this.btnConnect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConnect.BackgroundImage")));
			this.btnConnect.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnConnect.Dock")));
			this.btnConnect.Enabled = ((bool)(resources.GetObject("btnConnect.Enabled")));
			this.errorProvider1.SetError(this.btnConnect, resources.GetString("btnConnect.Error"));
			this.btnConnect.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnConnect.FlatStyle")));
			this.btnConnect.Font = ((System.Drawing.Font)(resources.GetObject("btnConnect.Font")));
			this.errorProvider1.SetIconAlignment(this.btnConnect, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnConnect.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.btnConnect, ((int)(resources.GetObject("btnConnect.IconPadding"))));
			this.btnConnect.Image = ((System.Drawing.Image)(resources.GetObject("btnConnect.Image")));
			this.btnConnect.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnConnect.ImageAlign")));
			this.btnConnect.ImageIndex = ((int)(resources.GetObject("btnConnect.ImageIndex")));
			this.btnConnect.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnConnect.ImeMode")));
			this.btnConnect.Location = ((System.Drawing.Point)(resources.GetObject("btnConnect.Location")));
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnConnect.RightToLeft")));
			this.btnConnect.Size = ((System.Drawing.Size)(resources.GetObject("btnConnect.Size")));
			this.btnConnect.TabIndex = ((int)(resources.GetObject("btnConnect.TabIndex")));
			this.btnConnect.Text = resources.GetString("btnConnect.Text");
			this.btnConnect.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnConnect.TextAlign")));
			this.btnConnect.Visible = ((bool)(resources.GetObject("btnConnect.Visible")));
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.AccessibleDescription = resources.GetString("groupBox1.AccessibleDescription");
			this.groupBox1.AccessibleName = resources.GetString("groupBox1.AccessibleName");
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("groupBox1.Anchor")));
			this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
			this.groupBox1.Controls.Add(this.txtVersion);
			this.groupBox1.Controls.Add(this.txtMessage);
			this.groupBox1.Controls.Add(this.txtName);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("groupBox1.Dock")));
			this.groupBox1.Enabled = ((bool)(resources.GetObject("groupBox1.Enabled")));
			this.errorProvider1.SetError(this.groupBox1, resources.GetString("groupBox1.Error"));
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Font = ((System.Drawing.Font)(resources.GetObject("groupBox1.Font")));
			this.errorProvider1.SetIconAlignment(this.groupBox1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("groupBox1.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.groupBox1, ((int)(resources.GetObject("groupBox1.IconPadding"))));
			this.groupBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("groupBox1.ImeMode")));
			this.groupBox1.Location = ((System.Drawing.Point)(resources.GetObject("groupBox1.Location")));
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("groupBox1.RightToLeft")));
			this.groupBox1.Size = ((System.Drawing.Size)(resources.GetObject("groupBox1.Size")));
			this.groupBox1.TabIndex = ((int)(resources.GetObject("groupBox1.TabIndex")));
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = resources.GetString("groupBox1.Text");
			this.groupBox1.Visible = ((bool)(resources.GetObject("groupBox1.Visible")));
			// 
			// txtVersion
			// 
			this.txtVersion.AccessibleDescription = resources.GetString("txtVersion.AccessibleDescription");
			this.txtVersion.AccessibleName = resources.GetString("txtVersion.AccessibleName");
			this.txtVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtVersion.Anchor")));
			this.txtVersion.AutoSize = ((bool)(resources.GetObject("txtVersion.AutoSize")));
			this.txtVersion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtVersion.BackgroundImage")));
			this.txtVersion.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtVersion.Dock")));
			this.txtVersion.Enabled = ((bool)(resources.GetObject("txtVersion.Enabled")));
			this.errorProvider1.SetError(this.txtVersion, resources.GetString("txtVersion.Error"));
			this.txtVersion.Font = ((System.Drawing.Font)(resources.GetObject("txtVersion.Font")));
			this.errorProvider1.SetIconAlignment(this.txtVersion, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtVersion.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.txtVersion, ((int)(resources.GetObject("txtVersion.IconPadding"))));
			this.txtVersion.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtVersion.ImeMode")));
			this.txtVersion.Location = ((System.Drawing.Point)(resources.GetObject("txtVersion.Location")));
			this.txtVersion.MaxLength = ((int)(resources.GetObject("txtVersion.MaxLength")));
			this.txtVersion.Multiline = ((bool)(resources.GetObject("txtVersion.Multiline")));
			this.txtVersion.Name = "txtVersion";
			this.txtVersion.PasswordChar = ((char)(resources.GetObject("txtVersion.PasswordChar")));
			this.txtVersion.ReadOnly = true;
			this.txtVersion.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtVersion.RightToLeft")));
			this.txtVersion.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtVersion.ScrollBars")));
			this.txtVersion.Size = ((System.Drawing.Size)(resources.GetObject("txtVersion.Size")));
			this.txtVersion.TabIndex = ((int)(resources.GetObject("txtVersion.TabIndex")));
			this.txtVersion.TabStop = false;
			this.txtVersion.Text = resources.GetString("txtVersion.Text");
			this.txtVersion.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtVersion.TextAlign")));
			this.txtVersion.Visible = ((bool)(resources.GetObject("txtVersion.Visible")));
			this.txtVersion.WordWrap = ((bool)(resources.GetObject("txtVersion.WordWrap")));
			// 
			// txtMessage
			// 
			this.txtMessage.AccessibleDescription = resources.GetString("txtMessage.AccessibleDescription");
			this.txtMessage.AccessibleName = resources.GetString("txtMessage.AccessibleName");
			this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtMessage.Anchor")));
			this.txtMessage.AutoSize = ((bool)(resources.GetObject("txtMessage.AutoSize")));
			this.txtMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtMessage.BackgroundImage")));
			this.txtMessage.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtMessage.Dock")));
			this.txtMessage.Enabled = ((bool)(resources.GetObject("txtMessage.Enabled")));
			this.errorProvider1.SetError(this.txtMessage, resources.GetString("txtMessage.Error"));
			this.txtMessage.Font = ((System.Drawing.Font)(resources.GetObject("txtMessage.Font")));
			this.errorProvider1.SetIconAlignment(this.txtMessage, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtMessage.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.txtMessage, ((int)(resources.GetObject("txtMessage.IconPadding"))));
			this.txtMessage.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtMessage.ImeMode")));
			this.txtMessage.Location = ((System.Drawing.Point)(resources.GetObject("txtMessage.Location")));
			this.txtMessage.MaxLength = ((int)(resources.GetObject("txtMessage.MaxLength")));
			this.txtMessage.Multiline = ((bool)(resources.GetObject("txtMessage.Multiline")));
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.PasswordChar = ((char)(resources.GetObject("txtMessage.PasswordChar")));
			this.txtMessage.ReadOnly = true;
			this.txtMessage.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtMessage.RightToLeft")));
			this.txtMessage.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtMessage.ScrollBars")));
			this.txtMessage.Size = ((System.Drawing.Size)(resources.GetObject("txtMessage.Size")));
			this.txtMessage.TabIndex = ((int)(resources.GetObject("txtMessage.TabIndex")));
			this.txtMessage.TabStop = false;
			this.txtMessage.Text = resources.GetString("txtMessage.Text");
			this.txtMessage.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtMessage.TextAlign")));
			this.txtMessage.Visible = ((bool)(resources.GetObject("txtMessage.Visible")));
			this.txtMessage.WordWrap = ((bool)(resources.GetObject("txtMessage.WordWrap")));
			// 
			// txtName
			// 
			this.txtName.AccessibleDescription = resources.GetString("txtName.AccessibleDescription");
			this.txtName.AccessibleName = resources.GetString("txtName.AccessibleName");
			this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtName.Anchor")));
			this.txtName.AutoSize = ((bool)(resources.GetObject("txtName.AutoSize")));
			this.txtName.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtName.BackgroundImage")));
			this.txtName.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtName.Dock")));
			this.txtName.Enabled = ((bool)(resources.GetObject("txtName.Enabled")));
			this.errorProvider1.SetError(this.txtName, resources.GetString("txtName.Error"));
			this.txtName.Font = ((System.Drawing.Font)(resources.GetObject("txtName.Font")));
			this.errorProvider1.SetIconAlignment(this.txtName, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtName.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.txtName, ((int)(resources.GetObject("txtName.IconPadding"))));
			this.txtName.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtName.ImeMode")));
			this.txtName.Location = ((System.Drawing.Point)(resources.GetObject("txtName.Location")));
			this.txtName.MaxLength = ((int)(resources.GetObject("txtName.MaxLength")));
			this.txtName.Multiline = ((bool)(resources.GetObject("txtName.Multiline")));
			this.txtName.Name = "txtName";
			this.txtName.PasswordChar = ((char)(resources.GetObject("txtName.PasswordChar")));
			this.txtName.ReadOnly = true;
			this.txtName.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtName.RightToLeft")));
			this.txtName.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtName.ScrollBars")));
			this.txtName.Size = ((System.Drawing.Size)(resources.GetObject("txtName.Size")));
			this.txtName.TabIndex = ((int)(resources.GetObject("txtName.TabIndex")));
			this.txtName.TabStop = false;
			this.txtName.Text = resources.GetString("txtName.Text");
			this.txtName.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtName.TextAlign")));
			this.txtName.Visible = ((bool)(resources.GetObject("txtName.Visible")));
			this.txtName.WordWrap = ((bool)(resources.GetObject("txtName.WordWrap")));
			// 
			// label4
			// 
			this.label4.AccessibleDescription = resources.GetString("label4.AccessibleDescription");
			this.label4.AccessibleName = resources.GetString("label4.AccessibleName");
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label4.Anchor")));
			this.label4.AutoSize = ((bool)(resources.GetObject("label4.AutoSize")));
			this.label4.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label4.Dock")));
			this.label4.Enabled = ((bool)(resources.GetObject("label4.Enabled")));
			this.errorProvider1.SetError(this.label4, resources.GetString("label4.Error"));
			this.label4.Font = ((System.Drawing.Font)(resources.GetObject("label4.Font")));
			this.errorProvider1.SetIconAlignment(this.label4, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label4.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.label4, ((int)(resources.GetObject("label4.IconPadding"))));
			this.label4.Image = ((System.Drawing.Image)(resources.GetObject("label4.Image")));
			this.label4.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.ImageAlign")));
			this.label4.ImageIndex = ((int)(resources.GetObject("label4.ImageIndex")));
			this.label4.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label4.ImeMode")));
			this.label4.Location = ((System.Drawing.Point)(resources.GetObject("label4.Location")));
			this.label4.Name = "label4";
			this.label4.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label4.RightToLeft")));
			this.label4.Size = ((System.Drawing.Size)(resources.GetObject("label4.Size")));
			this.label4.TabIndex = ((int)(resources.GetObject("label4.TabIndex")));
			this.label4.Text = resources.GetString("label4.Text");
			this.label4.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label4.TextAlign")));
			this.label4.Visible = ((bool)(resources.GetObject("label4.Visible")));
			// 
			// label3
			// 
			this.label3.AccessibleDescription = resources.GetString("label3.AccessibleDescription");
			this.label3.AccessibleName = resources.GetString("label3.AccessibleName");
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label3.Anchor")));
			this.label3.AutoSize = ((bool)(resources.GetObject("label3.AutoSize")));
			this.label3.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label3.Dock")));
			this.label3.Enabled = ((bool)(resources.GetObject("label3.Enabled")));
			this.errorProvider1.SetError(this.label3, resources.GetString("label3.Error"));
			this.label3.Font = ((System.Drawing.Font)(resources.GetObject("label3.Font")));
			this.errorProvider1.SetIconAlignment(this.label3, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label3.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.label3, ((int)(resources.GetObject("label3.IconPadding"))));
			this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
			this.label3.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.ImageAlign")));
			this.label3.ImageIndex = ((int)(resources.GetObject("label3.ImageIndex")));
			this.label3.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label3.ImeMode")));
			this.label3.Location = ((System.Drawing.Point)(resources.GetObject("label3.Location")));
			this.label3.Name = "label3";
			this.label3.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label3.RightToLeft")));
			this.label3.Size = ((System.Drawing.Size)(resources.GetObject("label3.Size")));
			this.label3.TabIndex = ((int)(resources.GetObject("label3.TabIndex")));
			this.label3.Text = resources.GetString("label3.Text");
			this.label3.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label3.TextAlign")));
			this.label3.Visible = ((bool)(resources.GetObject("label3.Visible")));
			// 
			// label2
			// 
			this.label2.AccessibleDescription = resources.GetString("label2.AccessibleDescription");
			this.label2.AccessibleName = resources.GetString("label2.AccessibleName");
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("label2.Anchor")));
			this.label2.AutoSize = ((bool)(resources.GetObject("label2.AutoSize")));
			this.label2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("label2.Dock")));
			this.label2.Enabled = ((bool)(resources.GetObject("label2.Enabled")));
			this.errorProvider1.SetError(this.label2, resources.GetString("label2.Error"));
			this.label2.Font = ((System.Drawing.Font)(resources.GetObject("label2.Font")));
			this.errorProvider1.SetIconAlignment(this.label2, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("label2.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.label2, ((int)(resources.GetObject("label2.IconPadding"))));
			this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
			this.label2.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.ImageAlign")));
			this.label2.ImageIndex = ((int)(resources.GetObject("label2.ImageIndex")));
			this.label2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("label2.ImeMode")));
			this.label2.Location = ((System.Drawing.Point)(resources.GetObject("label2.Location")));
			this.label2.Name = "label2";
			this.label2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("label2.RightToLeft")));
			this.label2.Size = ((System.Drawing.Size)(resources.GetObject("label2.Size")));
			this.label2.TabIndex = ((int)(resources.GetObject("label2.TabIndex")));
			this.label2.Text = resources.GetString("label2.Text");
			this.label2.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("label2.TextAlign")));
			this.label2.Visible = ((bool)(resources.GetObject("label2.Visible")));
			// 
			// lblProgress
			// 
			this.lblProgress.AccessibleDescription = resources.GetString("lblProgress.AccessibleDescription");
			this.lblProgress.AccessibleName = resources.GetString("lblProgress.AccessibleName");
			this.lblProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblProgress.Anchor")));
			this.lblProgress.AutoSize = ((bool)(resources.GetObject("lblProgress.AutoSize")));
			this.lblProgress.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblProgress.Dock")));
			this.lblProgress.Enabled = ((bool)(resources.GetObject("lblProgress.Enabled")));
			this.errorProvider1.SetError(this.lblProgress, resources.GetString("lblProgress.Error"));
			this.lblProgress.Font = ((System.Drawing.Font)(resources.GetObject("lblProgress.Font")));
			this.errorProvider1.SetIconAlignment(this.lblProgress, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("lblProgress.IconAlignment"))));
			this.errorProvider1.SetIconPadding(this.lblProgress, ((int)(resources.GetObject("lblProgress.IconPadding"))));
			this.lblProgress.Image = ((System.Drawing.Image)(resources.GetObject("lblProgress.Image")));
			this.lblProgress.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblProgress.ImageAlign")));
			this.lblProgress.ImageIndex = ((int)(resources.GetObject("lblProgress.ImageIndex")));
			this.lblProgress.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblProgress.ImeMode")));
			this.lblProgress.Location = ((System.Drawing.Point)(resources.GetObject("lblProgress.Location")));
			this.lblProgress.Name = "lblProgress";
			this.lblProgress.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblProgress.RightToLeft")));
			this.lblProgress.Size = ((System.Drawing.Size)(resources.GetObject("lblProgress.Size")));
			this.lblProgress.TabIndex = ((int)(resources.GetObject("lblProgress.TabIndex")));
			this.lblProgress.Text = resources.GetString("lblProgress.Text");
			this.lblProgress.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblProgress.TextAlign")));
			this.lblProgress.Visible = ((bool)(resources.GetObject("lblProgress.Visible")));
			// 
			// AddServerDialog
			// 
			this.AcceptButton = this.btnOk;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.lblProgress);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.txtAddress);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "AddServerDialog";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Load += new System.EventHandler(this.AddServerDialog_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private void textBox1_Validating(object sender, CancelEventArgs e)
		{
			try
			{
				Uri test = new Uri(txtAddress.Text);
				errorProvider1.SetError(txtAddress, string.Empty);
				e.Cancel = false;
			}
			catch (UriFormatException)
			{
				string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServer.InvalidUrlError");
				msg = msg.Replace("\\n", "\n");
				errorProvider1.SetError(txtAddress, msg);
				e.Cancel = true;
			}
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			btnOk.Enabled = false;
			try
			{
				Cursor = Cursors.WaitCursor;
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServer.ContactingServer");
				Server svr = new Server();
				svr.Proxy = WebProxy.GetDefaultProxy();
				svr.Credentials = CredentialCache.DefaultCredentials;
				svr.Url = txtAddress.Text + "/ConfigurationSystem/Server.asmx";

				ServerInfo info = svr.GetServerInfo();
				txtName.Text = info.Name;
				txtMessage.Text = info.Message;
				txtVersion.Text = string.Format("{0}.{1}.{2}.{3}", info.VersionMajor, info.VersionMinor, info.VersionBuild, info.VersionRevision);

				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServer.GettingConfig");
				Plugins psvc = new Plugins();
				psvc.Proxy = WebProxy.GetDefaultProxy();
				psvc.Credentials = CredentialCache.DefaultCredentials;
				psvc.Url = txtAddress.Text + "/ConfigurationSystem/Plugins.asmx";

				PluginDescriptor[] plugs = psvc.GetInstalledPlugins();
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServer.ProgressFinished");
				this.plugins = plugs;
				this.serverInfo = info;
				this.address = new Uri(txtAddress.Text);
				btnOk.Enabled = true;

			}
				//TODO: Provide more information to the user.  Full on message even? "Click here to see the details..."
			catch
			{
				lblProgress.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServer.Error");

				txtName.Text = string.Empty;
				txtMessage.Text = string.Empty;
				txtVersion.Text = string.Empty;
				this.address = null;
			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

		private void AddServerDialog_Load(object sender, EventArgs e)
		{
		}

		private void txtAddress_TextChanged(object sender, EventArgs e)
		{
		}

		private void txtAddress_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				Debug.WriteLine("Return");	
				btnConnect.PerformClick();
			}
		
		}
	}
}