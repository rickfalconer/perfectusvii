using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	public class AddExistingTemplateDialog : DialogBase
	{
		private Button btnOk;
		private Button btnCancel;
		private Label label1;
		private IContainer components = null;
		private ImageList imageList1;
		private TemplateDocumentCollection allTemplates;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		private TemplateDocumentCollection selectedTemplates = new TemplateDocumentCollection();

		public TemplateDocumentCollection AllTemplates
		{
			get { return allTemplates; }
			set
			{
				allTemplates = value;
				ResetList();
			}
		}


		public TemplateDocumentCollection SelectedTemplates
		{
			get { return selectedTemplates; }
		}

		public AddExistingTemplateDialog()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddExistingTemplateDialog));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(176, 224);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(264, 224);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Choose the templates you would like to add:";
            // 
            // uiListBox1
            // 
            this.uiListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiListBox1.CheckBoxes = true;
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Location = new System.Drawing.Point(12, 35);
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(327, 180);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.uiListBox1.TabIndex = 7;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // AddExistingTemplateDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(352, 254);
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddExistingTemplateDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Add Existing Templates";
            this.ResumeLayout(false);

		}

		#endregion

		private void btnOk_Click(object sender, EventArgs e)
		{
			selectedTemplates.Clear();

			foreach (ListViewItem i in uiListBox1.SelectedItems)
			{
				if (i.Tag is TemplateDocument)
				{
					TemplateDocument d = i.Tag as TemplateDocument;
					selectedTemplates.Add(d);
				}

			}
		}

		private void ResetList()
		{
			uiListBox1.Items.Clear();
			if (allTemplates != null)
			{
				foreach (TemplateDocument d in allTemplates)
				{
                    uiListBox1.Items.Add(new ListViewItem { Tag = d, Text = d.Name, ImageKey = "0" });
				}
			}

		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}