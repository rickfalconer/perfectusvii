using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.Controls;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	public class ServiceProgressDialog : DialogBase
	{
		private IContainer components = null;

		public ServiceProgressDialog()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call
		}

		private Button btnClose;

		private ServiceProgressControl[] meters;

		public ServiceProgressControl[] Meters
		{
			get { return meters; }
		}

        public ServiceProgressDialog(BackgroundWorkerBase[] wrappers) : this()
		{
			meters = new ServiceProgressControl[wrappers.Length];
			runningMeterCount = meters.Length;

			int y = 0;
			for (int i = 0; i < wrappers.Length; i++)
			{
				ServiceProgressControl meter = new ServiceProgressControl(wrappers[i]);
                meter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
				meter.Complete += new EventHandler(meter_Complete);
				meters[i] = meter;
				Controls.Add(meter);
				meter.Top = y;
				meter.Left = 0;
				meter.Width = Width;
				y += meter.Height;
			}
			this.Height = y + 67;
		}

        public void Start()
        {
            foreach (Control meter in Controls)
            {
                if (meter is ServiceProgressControl)
                {
                    ((ServiceProgressControl)meter).RunWorker();
                }
            }
        }

		public void SetComplete()
		{
			btnClose.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.ServiceProgressDialog.CloseButtonText");
			this.DialogResult = DialogResult.OK;
			this.Close();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnClose.Location = new System.Drawing.Point(352, 72);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Cancel";
            // 
            // ServiceProgressDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(434, 104);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "ServiceProgressDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Auto;
            this.Text = "Communication Progress";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ServiceProgressDialog_KeyDown);
            this.ResumeLayout(false);

		}

		#endregion

		private int runningMeterCount;

		private void meter_Complete(object sender, EventArgs e)
		{
			runningMeterCount --;
			if (runningMeterCount == 0)
			{
				SetComplete();
			}
		}

		public void HideCancelButton()
		{
			btnClose.Visible = false;
		}

		// So that it's easy to get the whole error message onto clipboard. Because the dialog cuts off most of any errors.
		private void ServiceProgressDialog_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if( e.KeyCode == Keys.C && e.Modifiers == Keys.Control)
			{
				if(meters != null && meters.Length > 0)
				{
					Clipboard.SetDataObject(meters[0].GetMessageText(),true);
				}
			}		
		}
	}
}