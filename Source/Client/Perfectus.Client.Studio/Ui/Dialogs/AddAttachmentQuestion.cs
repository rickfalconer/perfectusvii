using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Dialogs
{
    public partial class AddAttachmentQuestion : Form
    {
        public Question SelectedQuestion
        {
            get
            {
                if (listView1.SelectedItems.Count == 1)
                    return (Question)listView1.SelectedItems[0].Tag;
                return null;
            }
        }

        public void SetQuestions(List<Question> questions)
        {
            listView1.View = View.List;
            foreach (Question q in questions)
            {
                ListViewItem item = new ListViewItem(q.Name);
                item.ImageIndex = 0;
                item.Tag = q;
                listView1.Items.Add(item);
            }
            listView1.TopItem = listView1.Items[0];
            listView1.View = View.List;
        }

        public AddAttachmentQuestion()
        {
            InitializeComponent();
            btnOk.Enabled = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = listView1.SelectedItems.Count == 1;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            btnOk.PerformClick();
        }
    }
}