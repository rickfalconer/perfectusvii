namespace Perfectus.Client.Studio.UI.Dialogs
{
    partial class AddDocumentDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddDocumentDialog));
            this.lstViewConverter = new System.Windows.Forms.ListView();
            this.converterHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageListSmall = new System.Windows.Forms.ImageList(this.components);
            this.propertyGridConverter = new System.Windows.Forms.PropertyGrid();
            this.lstTemplates = new System.Windows.Forms.ListView();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.propertyGridDocument = new System.Windows.Forms.PropertyGrid();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstViewConverter
            // 
            this.lstViewConverter.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lstViewConverter.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.converterHeader});
            this.lstViewConverter.FullRowSelect = true;
            this.lstViewConverter.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstViewConverter.HideSelection = false;
            this.lstViewConverter.Location = new System.Drawing.Point(3, 18);
            this.lstViewConverter.MultiSelect = false;
            this.lstViewConverter.Name = "lstViewConverter";
            this.lstViewConverter.Size = new System.Drawing.Size(347, 237);
            this.lstViewConverter.SmallImageList = this.imageListSmall;
            this.lstViewConverter.TabIndex = 0;
            this.lstViewConverter.UseCompatibleStateImageBehavior = false;
            this.lstViewConverter.View = System.Windows.Forms.View.Details;
            this.lstViewConverter.SelectedIndexChanged += new System.EventHandler(this.lstViewConverter_SelectedIndexChanged);
            // 
            // converterHeader
            // 
            this.converterHeader.Width = 320;
            // 
            // imageListSmall
            // 
            this.imageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmall.ImageStream")));
            this.imageListSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListSmall.Images.SetKeyName(0, "ConverterWordML.png");
            this.imageListSmall.Images.SetKeyName(1, "ConverterXML.png");
            this.imageListSmall.Images.SetKeyName(2, "document.png");
            // 
            // propertyGridConverter
            // 
            this.propertyGridConverter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.propertyGridConverter.HelpVisible = false;
            this.propertyGridConverter.Location = new System.Drawing.Point(4, 278);
            this.propertyGridConverter.Name = "propertyGridConverter";
            this.propertyGridConverter.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.propertyGridConverter.Size = new System.Drawing.Size(346, 182);
            this.propertyGridConverter.TabIndex = 1;
            this.propertyGridConverter.ToolbarVisible = false;
            // 
            // lstTemplates
            // 
            this.lstTemplates.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.lstTemplates.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lstTemplates.FullRowSelect = true;
            this.lstTemplates.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstTemplates.HideSelection = false;
            this.lstTemplates.Location = new System.Drawing.Point(356, 18);
            this.lstTemplates.MultiSelect = false;
            this.lstTemplates.Name = "lstTemplates";
            this.lstTemplates.Size = new System.Drawing.Size(347, 237);
            this.lstTemplates.SmallImageList = this.imageListSmall;
            this.lstTemplates.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstTemplates.TabIndex = 2;
            this.lstTemplates.UseCompatibleStateImageBehavior = false;
            this.lstTemplates.View = System.Windows.Forms.View.Details;
            this.lstTemplates.SelectedIndexChanged += new System.EventHandler(this.lstTemplates_SelectedIndexChanged);
            this.lstTemplates.EnabledChanged += new System.EventHandler(this.lstTemplates_EnabledChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(641, 479);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(560, 479);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.propertyGridDocument);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lstTemplates);
            this.panel1.Controls.Add(this.propertyGridConverter);
            this.panel1.Controls.Add(this.lstViewConverter);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(710, 467);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Converter Properties";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Converter";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(353, 1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Templates";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(353, 262);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Document Properties";
            // 
            // propertyGridDocument
            // 
            this.propertyGridDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGridDocument.HelpVisible = false;
            this.propertyGridDocument.Location = new System.Drawing.Point(356, 278);
            this.propertyGridDocument.Name = "propertyGridDocument";
            this.propertyGridDocument.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGridDocument.Size = new System.Drawing.Size(346, 182);
            this.propertyGridDocument.TabIndex = 3;
            this.propertyGridDocument.ToolbarVisible = false;
            // 
            // AddDocumentDialog
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(722, 508);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddDocumentDialog";
            this.Text = "Create Document";
            this.Load += new System.EventHandler(this.AddDocumentDialog_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstViewConverter;
        private System.Windows.Forms.PropertyGrid propertyGridConverter;
        private System.Windows.Forms.ListView lstTemplates;
        private System.Windows.Forms.ImageList imageListSmall;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ColumnHeader converterHeader;
        private System.Windows.Forms.PropertyGrid propertyGridDocument;
    }
}