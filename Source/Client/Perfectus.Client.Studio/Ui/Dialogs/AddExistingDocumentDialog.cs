using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Dialogs
{
    public partial class AddExistingDocumentDialog : Form
    {
        private ListViewGroup documentGrp;
        private ListViewGroup xmlGrp;
        private ListViewGroup attachmentGrp;

        private List<IDocument> _currentDocuments;
        private List<IDocument> _allDocuments;
        private Package _package;
        private PluginDescriptorCollection _plugins;

        public List<IDocument> SelectedDocuments
        {
            get
            {
                List<IDocument> selectedDocument = new List<IDocument>();
                foreach (ListViewItem item in lstDocuments.SelectedItems)
                        selectedDocument.Add(item.Tag as IDocument);
                return selectedDocument;
            }
        }

        private AddExistingDocumentDialog()
        {
            InitializeComponent();
        }

        public AddExistingDocumentDialog(Package package, List<IDocument> currentDocuments, PluginDescriptorCollection plugins)
            : this()
        {
            _currentDocuments = currentDocuments;
            _package = package;
            _plugins = plugins;

            documentGrp = new ListViewGroup(GetResource("AddExistingDocumentDialog.GroupDocuments"));
            xmlGrp = new ListViewGroup(GetResource("AddExistingDocumentDialog.GroupXML"));
            attachmentGrp = new ListViewGroup(GetResource("AddExistingDocumentDialog.GroupAttachments"));

            _allDocuments = new List<IDocument>(package.DocumentsInUse);

            foreach (IAttachment attachment in package.Attachments)
                _allDocuments.Add(attachment);

            btnOk.Enabled = false;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            AddDocumentDialog addDocumentDialog = new AddDocumentDialog();
            addDocumentDialog.AllTemplates = _package.Templates;
            addDocumentDialog.AllPlugins = _plugins;
            addDocumentDialog.Document = new Document();
            addDocumentDialog.Document.ParentPackage = _package;
            addDocumentDialog.Document.Name = addDocumentDialog.Document.Name;
            if (addDocumentDialog.ShowDialog() == DialogResult.OK)
            {
                _allDocuments.Add(addDocumentDialog.Document);
                ListViewItem item = AddDocumentToView(addDocumentDialog.Document);
                item.Selected = true;
            }

            if (lstDocuments.Items.Count == 0)
                btnCancel.PerformClick();
        }


        private String searchKey( IDocument doc )
        {
            if( doc is Document )
            {
                Document document = doc as Document;

                // FB 2322: Only filter out documents if they are identical (same converter and template) AND have 
                // the same name. Whilst this creates duplicate identical documents on the list, it creates a less confusing 
                // UI for the user.
                return String.Format( "{0}#{1}#{2}",
                    document.Name,
                    document.Converter.Descriptor.UniqueIdentifier,
                    document.HasTemplate ? document.Template.UniqueIdentifier : Guid.Empty );
            }

            if( doc is IAttachment )
            {
                IAttachment attachment = doc as IAttachment;

                // This is the searchkey of the original Attachment from the tree
                if( attachment.OriginalUniqueueIdentifier == Guid.Empty )
                    return String.Format( "{0}#{1}",
                                   attachment.UniqueIdentifier,
                                   Guid.Empty );

                // This is the searchkey of the duplicate attached to a delivery
                return String.Format( "{0}#{1}",
                    attachment.OriginalUniqueueIdentifier,
                    Guid.Empty );
            }
            return String.Empty;
        }

        private void AddExistingDocumentDialog_Load(object sender, EventArgs e)
        {
            // A document can be defined within (let's say) 'Delivery 1'
            // This docu can be added to 'Delivery 2' (as a clone)
            // This clone shall not be added back into 'Delivery 1'.
            // Rule:
            // A document is considerred 'identical', if it has the same converter and 
            // the same template defined.
            // An attachment is 'identical' by ToDo!!!
            List<String> searchKeyList = new List<string>();  
            foreach ( IDocument doc in _currentDocuments )
            {
                String keyString = searchKey (doc);
                if ( !searchKeyList.Contains (keyString ))
                    searchKeyList.Add( keyString  );
            }

            // Lookup those documents that are not already part of the delivery
            foreach (IDocument doc in _allDocuments)
            {
                // Easy case
                if (_currentDocuments.Contains(doc))
                    continue;

                String keyString = searchKey (doc);
                if ( searchKeyList.Contains (keyString))
                    continue;       

                searchKeyList.Add(keyString);
                // Ok, we add it to the list of selectable documents
                ListViewItem item = AddDocumentToView(doc);
            }

           if (lstDocuments.Items.Count == 0)
                btnNew.PerformClick();
            else
            {
                //lstDocuments.View = View.List;
                //lstDocuments.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
                //lstDocuments.View = View.SmallIcon;
                lstDocuments.View = View.Details;
            }
        }

        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }


        private ListViewItem AddDocumentToView(IDocument document)
        {
            ListViewItem item = null;

            String toolTipText = GetToolTipText(document);

            if (document is IAttachment)
            {
                if (!lstDocuments.Groups.Contains(attachmentGrp))
                    lstDocuments.Groups.Add(attachmentGrp);

                int icon = 2;
                if ( document is AttachmentDynamic )
                    icon = 3;
                item = new ListViewItem(document.Name, icon, attachmentGrp);
            }
            else if (((Document)document).HasTemplate)
            {
                if (!lstDocuments.Groups.Contains(documentGrp))
                    lstDocuments.Groups.Add(documentGrp);
                item = new ListViewItem(document.Name, 0, documentGrp);
            }
            else 
            {
                if (!lstDocuments.Groups.Contains(xmlGrp))
                    lstDocuments.Groups.Add(xmlGrp);
                item = new ListViewItem(document.Name, 1, xmlGrp);
            }
            item.Tag = document;
            item.ToolTipText = toolTipText;

            lstDocuments.Items.Add(item);

            return item;
        }

        private string GetToolTipText(IDocument document)
        {
            StringBuilder toolTipTextBuilder = new StringBuilder();
            if (document.ParentDelivery != null &&
                document.ParentDelivery.ParentRule != null)
            {
                toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.DeliveryManager"), document.ParentDelivery.ParentRule.Name));
            }

            if (document.ParentDelivery != null &&
                document.ParentDelivery.ParentRule != null)
            {
                if ( toolTipTextBuilder.Length > 0 )
                    toolTipTextBuilder.Append(System.Environment.NewLine);
                toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.Delivery"), document.ParentDelivery.Name));
            }

            if (document.FileName != null)
            {
                if (toolTipTextBuilder.Length > 0)
                    toolTipTextBuilder.Append(System.Environment.NewLine);

                if (document.FileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
                {
                    if ( document.FileName.TextValue.Contains ("</item>"))
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.FilenameByBinding")));
                    else
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.FilenameByConstant"), document.FileName.TextValue));
            }
                else if (document.FileName.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.FilenameByQuestion"), document.FileName.QuestionValue.Name));
                else
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.FilenameByQuery")));
            }

            if (document is IAttachment &&
                ((PackageItem)document).Notes.Length > 0)
            {
                if (toolTipTextBuilder.Length > 0)
                    toolTipTextBuilder.Append(System.Environment.NewLine);
                toolTipTextBuilder.Append(((PackageItem)document).Notes);
            }
            if (document is Document)
            {
                if (toolTipTextBuilder.Length > 0)
                    toolTipTextBuilder.Append(System.Environment.NewLine);
                Document doc = document as Document;
                if (doc.HasTemplate)
                {
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.Converter"), doc.Converter.Name));
                    toolTipTextBuilder.Append(System.Environment.NewLine);
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.Template"), doc.Template.Name));
                }
                else
                    toolTipTextBuilder.Append(String.Format(GetResource("AddExistingDocumentDialog.XMLConverter"), doc.Converter.Name));
            }
            return toolTipTextBuilder.ToString();
        }

        private void lstDocuments_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = lstDocuments.SelectedItems.Count > 0;
        }

        private void lstDocuments_DoubleClick(object sender, EventArgs e)
        {
            if (lstDocuments.SelectedItems.Count == 1)
                btnOk.PerformClick();
        }
    }
}
