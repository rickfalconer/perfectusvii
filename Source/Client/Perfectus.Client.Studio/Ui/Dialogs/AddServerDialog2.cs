using System;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Server;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	/// <summary>
	/// Summary description for AddServerDialog2.
	/// </summary>
	public class AddServerDialog2 : DialogBase
	{
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblAddServerBlurb;
		private System.Windows.Forms.TextBox txtAddress;
		
		private ServerInfo serverInfo;
		private PluginDescriptor[] plugins;

		public Uri Address
		{
			get { return new Uri(txtAddress.Text); }
		}

		public ServerInfo ServerInfo
		{
			get { return serverInfo; }
		}

		public PluginDescriptor[] Plugins
		{
			get { return plugins; }
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public AddServerDialog2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AddServerDialog2));
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.lblAddServerBlurb = new System.Windows.Forms.Label();
			this.txtAddress = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.CausesValidation = false;
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// lblAddServerBlurb
			// 
			this.lblAddServerBlurb.AccessibleDescription = resources.GetString("lblAddServerBlurb.AccessibleDescription");
			this.lblAddServerBlurb.AccessibleName = resources.GetString("lblAddServerBlurb.AccessibleName");
			this.lblAddServerBlurb.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblAddServerBlurb.Anchor")));
			this.lblAddServerBlurb.AutoSize = ((bool)(resources.GetObject("lblAddServerBlurb.AutoSize")));
			this.lblAddServerBlurb.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblAddServerBlurb.Dock")));
			this.lblAddServerBlurb.Enabled = ((bool)(resources.GetObject("lblAddServerBlurb.Enabled")));
			this.lblAddServerBlurb.Font = ((System.Drawing.Font)(resources.GetObject("lblAddServerBlurb.Font")));
			this.lblAddServerBlurb.Image = ((System.Drawing.Image)(resources.GetObject("lblAddServerBlurb.Image")));
			this.lblAddServerBlurb.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAddServerBlurb.ImageAlign")));
			this.lblAddServerBlurb.ImageIndex = ((int)(resources.GetObject("lblAddServerBlurb.ImageIndex")));
			this.lblAddServerBlurb.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblAddServerBlurb.ImeMode")));
			this.lblAddServerBlurb.Location = ((System.Drawing.Point)(resources.GetObject("lblAddServerBlurb.Location")));
			this.lblAddServerBlurb.Name = "lblAddServerBlurb";
			this.lblAddServerBlurb.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblAddServerBlurb.RightToLeft")));
			this.lblAddServerBlurb.Size = ((System.Drawing.Size)(resources.GetObject("lblAddServerBlurb.Size")));
			this.lblAddServerBlurb.TabIndex = ((int)(resources.GetObject("lblAddServerBlurb.TabIndex")));
			this.lblAddServerBlurb.Text = resources.GetString("lblAddServerBlurb.Text");
			this.lblAddServerBlurb.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblAddServerBlurb.TextAlign")));
			this.lblAddServerBlurb.Visible = ((bool)(resources.GetObject("lblAddServerBlurb.Visible")));
			// 
			// txtAddress
			// 
			this.txtAddress.AccessibleDescription = resources.GetString("txtAddress.AccessibleDescription");
			this.txtAddress.AccessibleName = resources.GetString("txtAddress.AccessibleName");
			this.txtAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtAddress.Anchor")));
			this.txtAddress.AutoSize = ((bool)(resources.GetObject("txtAddress.AutoSize")));
			this.txtAddress.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAddress.BackgroundImage")));
			this.txtAddress.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtAddress.Dock")));
			this.txtAddress.Enabled = ((bool)(resources.GetObject("txtAddress.Enabled")));
			this.txtAddress.Font = ((System.Drawing.Font)(resources.GetObject("txtAddress.Font")));
			this.txtAddress.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtAddress.ImeMode")));
			this.txtAddress.Location = ((System.Drawing.Point)(resources.GetObject("txtAddress.Location")));
			this.txtAddress.MaxLength = ((int)(resources.GetObject("txtAddress.MaxLength")));
			this.txtAddress.Multiline = ((bool)(resources.GetObject("txtAddress.Multiline")));
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.PasswordChar = ((char)(resources.GetObject("txtAddress.PasswordChar")));
			this.txtAddress.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtAddress.RightToLeft")));
			this.txtAddress.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtAddress.ScrollBars")));
			this.txtAddress.Size = ((System.Drawing.Size)(resources.GetObject("txtAddress.Size")));
			this.txtAddress.TabIndex = ((int)(resources.GetObject("txtAddress.TabIndex")));
			this.txtAddress.Text = resources.GetString("txtAddress.Text");
			this.txtAddress.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtAddress.TextAlign")));
			this.txtAddress.Visible = ((bool)(resources.GetObject("txtAddress.Visible")));
			this.txtAddress.WordWrap = ((bool)(resources.GetObject("txtAddress.WordWrap")));
			// 
			// AddServerDialog2
			// 
			this.AcceptButton = this.btnOk;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.lblAddServerBlurb);
			this.Controls.Add(this.txtAddress);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "AddServerDialog2";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.ResumeLayout(false);

		}
		#endregion

		private void btnOk_Click(object sender, System.EventArgs e)
		{			
			Uri address;
			try 
			{
				address = new Uri(txtAddress.Text);
			}
			catch
			{
				MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServerDialog2.InvalidHostURL"), Common.About.FormsTitle);
				return;
			}

			AddServerWrapper svrWrapper = new AddServerWrapper(0, address);
            BackgroundWorkerBase[] wrappers = new BackgroundWorkerBase[] { svrWrapper };
			ServiceProgressDialog dlg = new ServiceProgressDialog(wrappers);
            dlg.Start();
			
			DialogResult dr = dlg.ShowDialog(this);
			if (dr == DialogResult.Cancel)
			{
				svrWrapper.Abort(false);			
			}
			else if (dr == DialogResult.OK)
			{
				this.DialogResult = DialogResult.OK;
				this.serverInfo = svrWrapper.Info;
				this.plugins = svrWrapper.Plugins;

                if (serverInfo != null)
                {
                    ServerInfoDialog infoBox = new ServerInfoDialog();
                    infoBox.ServerMessage = serverInfo.Message;
                    infoBox.ServerVersion = serverInfo.InformationalVersion;
                    infoBox.ServerName = serverInfo.Name;
                    infoBox.ShowDialog();
                }
                else
                {
                    MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Dialogs.AddServerDialog2.InvalidHost"), Common.About.FormsTitle);
                    svrWrapper.Abort(false);
                    this.DialogResult = DialogResult.None;
                }

				//this.Close();
			}
		}
	}
}
