using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;

namespace Perfectus.Client.Studio.UI.Dialogs
{
	public class AddPluginsDialog : DialogBase
	{
		private Label label1;
		private Button btnOk;
        private Button btnCancel;
		private ImageList imageList1;
		private IContainer components = null;

		private PluginDescriptorCollection allPlugins;
		private PluginDescriptor selectedPlugin = null;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;

		private PluginBehaviour behaviourFilter = PluginBehaviour.Unknown;

        public PluginBehaviour BehaviourFilter
        {
            get { return behaviourFilter; }
            set
            {
                behaviourFilter = value;
                if (value == PluginBehaviour.Distributor)
                    this.Text = "Select Distributor";
            }
        }
		

		public PluginDescriptorCollection AllPlugins
		{
			get { return allPlugins; }
			set
			{
				allPlugins = value;
				ResetList();
			}
		}


        public PluginDescriptor SelectedPlugin
        {
            get
            {
                if (uiListBox1.SelectedItems.Count == 1)
                    return (PluginDescriptor)uiListBox1.SelectedItems[0].Tag;
                return null;
            }
        }


		public AddPluginsDialog()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPluginsDialog));
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Choose the tool you would like to add:";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(176, 219);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(264, 219);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "&Cancel";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.FullRowSelect = true;
            this.uiListBox1.Location = new System.Drawing.Point(12, 32);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(326, 178);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.uiListBox1.TabIndex = 11;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 300;
            // 
            // AddPluginsDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(350, 252);
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "AddPluginsDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Auto;
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.Text = "Add Converters or Distributors";
            this.ResumeLayout(false);

		}

		#endregion

		private void btnOk_Click(object sender, EventArgs e)
		{
		}

		private void ResetList()
		{
			uiListBox1.Items.Clear();
			if (allPlugins != null)
			{
				foreach (PluginDescriptor p in allPlugins)
				{
					int imageIndex = 0;
					if (behaviourFilter == PluginBehaviour.Converter && p.PluginKind == PluginBehaviour.Converter)
					{
						imageIndex = 0;
                        uiListBox1.Items.Add(new ListViewItem { ImageIndex = imageIndex, Text = p.FriendlyName, Tag = p });
					}
					else if (behaviourFilter == PluginBehaviour.Distributor && p.PluginKind == PluginBehaviour.Distributor)
					{
						imageIndex = 1;
                        uiListBox1.Items.Add(new ListViewItem { ImageIndex = imageIndex, Text = p.FriendlyName, Tag = p });
					}
				}
			}
		}
	}
}