using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
//using Perfectus.Client.Configuration;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for StringEditor.
	/// </summary>
	public class OutcomeEditor : UITypeEditor
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Launch the LargeTextEditorForm.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="sp"></param>
		/// <param name="value">The current value of the property.</param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            try
			{
				// get the editor service.
				IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService) sp.GetService(typeof (IWindowsFormsEditorService));

				if (edSvc == null)
				{
					// uh oh.
					return value;
				}
                return InvokeEditor((PackageItem)context.Instance, edSvc, value);
            }
            catch ( Exception ){}
            return null;
        }
        /// <summary>
        /// Used as a quick workaround to support the opening of the Query Editor
        /// by double clicking the Outcome node.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="edSvc"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public object InvokeEditor(PackageItem item, IWindowsFormsEditorService edSvc, object value)
		{
			DialogResult result;
            PackageItem packageItem;

			try{
				Query q = value as Query;
                packageItem = ((PackageItem)item);

				// Ensure the property changed event is fired
				if (item is LibraryItem && ((LibraryItem) item).LibraryUniqueIdentifier != Guid.Empty && 
					((LibraryItem) item).Linked == true &&
					((LibraryItem) item).CheckedOutStatus == CheckOutStatus.CheckedIn)
				{
					result = MessageBox.Show(Globals.gcCHECK_OUT_MESSAGE, Globals.gcSHARED_LIBRARY_TITLE, 
						MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				}
				else
				{
					result = DialogResult.Ignore;
				}

                if (result == DialogResult.Yes || result == DialogResult.No || result == DialogResult.Ignore)
                {
                    // Check, unlink (if shared library item), else ignore and continue
                    switch (result)
                    {
                        case DialogResult.Yes:
                            ((LibraryItem)item).DefinitionBeingEdited(Globals.gcCHECKOUT_MARKER);

                            // Ensure the latest version is retrieved & used in this editor
                            Package package = ((LibraryItem)item).ParentPackage;
                            packageItem = (PackageItem)package.GetLibraryItemByLibraryUniqueIdentifier(((LibraryItem)item));

                            break;
                        case DialogResult.No:
                            ((LibraryItem)item).DefinitionBeingEdited(Globals.gcUNLINK_MARKER);
                            break;
                        case DialogResult.Ignore:
                            break;
                    }

                    if (q == null)
                    {
                        q = ((PackageItem)item).ParentPackage.CreateQuery();
                    }
                    else
                    {
                        if (packageItem is Outcome)
                        {
                            q = ((Outcome)packageItem).Definition;
                        }
                        else if (packageItem is SimpleOutcome)
                        {
                            q = ((SimpleOutcome)packageItem).Definition;
                        }
                    }

                    // Set up the query expression
                    IQueryDesigner frm = Launch.CreateQueryDesignerForm(packageItem, packageItem.ParentPackage, q,
                        (packageItem is Outcome ? QueryDesignerMode.Outcome : QueryDesignerMode.YesNo));

                    try
                    {
                        DialogResult dr = edSvc.ShowDialog((Form)frm);

                        if (dr == DialogResult.OK)
                        {
                            return frm.QueryBeingEdited;
                        }
                        else
                        {
                            return value;
                        }
                    }
                    catch (Exception ex)
                    {
                        String message =
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("QueryEditorClosedDueToError");
                        String heading =
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation").GetString("ErrorOccurred");
                        MessageBox.Show(String.Format("{0}{1}{2}",
                            message, Environment.NewLine, ex.ToString()), heading, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return value;
                    }
                }
                else
                {
                    // Do nothing
                    return value;
                }
			}
			finally
			{
				if(item is LibraryItem)
				{
					((LibraryItem) item).ParentPackage.SharedLibraryEventsDisabled = false;
				}
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// Specify that this editor is the popup style ([...] button)
		/// </summary>
		/// <param name="item"></param>
		/// <returns>Modal</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}
	}
}
