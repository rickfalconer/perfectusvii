using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.ArithmeticDesigner
{
	/// <summary>
	/// Summary description for ArithmeticExpressionControl.
	/// </summary>
	public class ArithmeticExpressionControl : UserControl
	{
		private GroupBox groupBox1;
		private Button button1;
		private DataGrid grdExpression;

		private DataTable mathsOperatorTable1;
		private DataTable leftParenthesisTable;
		private DataTable rightParenthesisTable;
		private DataTable leftOperandsTable;

		private DataTable expressionTable;
		private ImageList imageList1;
		private IContainer components;
		private Package package;


		private ArithmeticPFunction itemBeingEdited;

		public ArithmeticPFunction ItemBeingEdited
		{
			get { return itemBeingEdited; }
			set
			{
				itemBeingEdited = value;
				if (itemBeingEdited != null)
				{
					package = itemBeingEdited.ParentPackage;
					DataTable dt = itemBeingEdited.Definition.Table;
					if (dt != null)
					{
						expressionTable = itemBeingEdited.Definition.Table.Clone();
						foreach (DataRow dr in dt.Rows)
						{
							expressionTable.Rows.Add(dr.ItemArray);
						}
					}
					else
					{
						expressionTable = ArithmeticExpression.CreateQueryTable();
					}
					grdExpression.DataSource = expressionTable;
					SetTableStyles();
				}
				else
				{
					package = null;
				}
			}
		}

		public ArithmeticExpressionControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			mathsOperatorTable1 = new DataTable("BooleanOperators");
			mathsOperatorTable1.Columns.Add("Display", typeof (OperatorDisplay));
			mathsOperatorTable1.Columns.Add("Value", typeof (OperatorDisplay));

            // Add a blank row - this is used when there is only one condition used - i.e. if a == b
			DataRow dr = mathsOperatorTable1.NewRow();
            dr["Display"] = DBNull.Value;
            dr["Value"] = DBNull.Value;
            mathsOperatorTable1.Rows.Add(dr);

			foreach (Operator bo in PerfectusTypeSystem.GetArithmeticOperators())
			{
				dr = mathsOperatorTable1.NewRow();
				dr["Display"] = new OperatorDisplay(bo);
				dr["Value"] = new OperatorDisplay(bo);
				mathsOperatorTable1.Rows.Add(dr);
			}

			leftParenthesisTable = new DataTable("LeftParenthesis");
			leftParenthesisTable.Columns.Add("Value");
			leftParenthesisTable.Rows.Add(new object[] {" "});
			leftParenthesisTable.Rows.Add(new object[] {"("});

			rightParenthesisTable = new DataTable("RightParenthesis");
			rightParenthesisTable.Columns.Add("Value");
			rightParenthesisTable.Rows.Add(new object[] {" "});
			rightParenthesisTable.Rows.Add(new object[] {")"});

			expressionTable = ArithmeticExpression.CreateQueryTable();

			leftOperandsTable = new DataTable("LeftOperand");
			leftOperandsTable.Columns.Add("Display", typeof (string));
			leftOperandsTable.Columns.Add("Value", typeof (AnswerProvider));
			leftOperandsTable.Columns.Add("ImageIndex", typeof (Image));			
			leftOperandsTable.DefaultView.Sort = "Value ASC";

			SetTableStyles();

			grdExpression.DataSource = expressionTable;

		}

		private Image GetImage(PackageItem pi)
		{
			if (pi is Question)
			{
				return imageList1.Images[0];
			}
			else if (pi is ArithmeticPFunction || pi is DatePFunction)
			{
				return imageList1.Images[1];
			}
			else
			{
				throw new NotSupportedException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.ArithmeticExpressionControl.LeftOperandColumn"));
			}

		}

		private void SetTableStyles()
		{
			leftOperandsTable.Rows.Clear();
			if (package != null)
			{
				foreach (PackageItem pi in PerfectusTypeSystem.GetNonSelfReferentialItems(itemBeingEdited, PerfectusDataType.Number))
				{
					leftOperandsTable.Rows.Add(new object[] {pi.Name, pi, GetImage(pi)});
				}
			}
			//Remove the existing style, if any.
			if (grdExpression.TableStyles.Count > 0)
				grdExpression.TableStyles.RemoveAt(0);

			// Create a table.
			DataGridTableStyle expressionTableStyle = new DataGridTableStyle(false);
			expressionTableStyle.MappingName = "Expression";
			expressionTableStyle.RowHeadersVisible = true;
			expressionTableStyle.AllowSorting = false;

			// Set up a generic column to play with.
			DataGridTextBoxColumn col;

			// Create a view for the operators

			col = new OperatorComboColumn();
			((OperatorComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			col.MappingName = "Operator1";
			((OperatorComboColumn) col).ColumnComboBox.DataSource = mathsOperatorTable1.DefaultView;
			((OperatorComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((OperatorComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl.OperatorColumnHeader");
			col.NullText = "";


			expressionTableStyle.PreferredRowHeight = ((OperatorComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((OperatorComboColumn) col).ColumnComboBox.Width - 70;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Left Parenthesis" drop-down column.
			col = new ComboColumn();
			((ComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ComboColumn) col).ColumnComboBox.DataSource = leftParenthesisTable.DefaultView;
			((ComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((ComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "LeftParenthesis";
			col.HeaderText = "(";
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((ComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((ComboColumn) col).ColumnComboBox.Width - 73;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Left Operand" drop-down column.
			col = new ItemComboColumn(imageList1);
			((ItemComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ItemComboColumn) col).ColumnComboBox.DataSource = leftOperandsTable.DefaultView;
			((ItemComboColumn) col).ColumnComboBox.DisplayMember = "Display";
			((ItemComboColumn) col).ColumnComboBox.ValueMember = "Value";
			//((ItemComboColumn) col).ColumnComboBox.ImageMember = "ImageIndex";
			col.MappingName = "LeftOperand";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl.LeftOperandColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredColumnWidth = 138;
			expressionTableStyle.GridColumnStyles.Add(col);

			col = new OperatorComboColumn();
			((OperatorComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			col.MappingName = "Operator2";
			((OperatorComboColumn) col).ColumnComboBox.DataSource = mathsOperatorTable1.DefaultView;
			((OperatorComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((OperatorComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl.OperatorColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((OperatorComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((OperatorComboColumn) col).ColumnComboBox.Width - 70;
			expressionTableStyle.GridColumnStyles.Add(col);


			// The "Right Operand" drop-down column.
			col = new ItemConstantColumn(package, PerfectusDataType.Number, "LeftOperand", this.itemBeingEdited);

			col.MappingName = "RightOperand";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl.RightOperandColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredColumnWidth = 150;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Right Parenthesis" drop-down column.
			col = new ComboColumn();
			((ComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ComboColumn) col).ColumnComboBox.DataSource = rightParenthesisTable.DefaultView;
			((ComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((ComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "RightParenthesis";
			col.HeaderText = ")";
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((ComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((ComboColumn) col).ColumnComboBox.Width - 73;
			expressionTableStyle.GridColumnStyles.Add(col);

			// Add our new style to the grids TableStyles collection.
			grdExpression.TableStyles.Add(expressionTableStyle);
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ArithmeticExpressionControl));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.grdExpression = new System.Windows.Forms.DataGrid();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdExpression)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.grdExpression);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(520, 150);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arithmetic Expression";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button1.Location = new System.Drawing.Point(440, 122);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "&Check";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // grdExpression
            // 
            this.grdExpression.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdExpression.CaptionVisible = false;
            this.grdExpression.DataMember = "";
            this.grdExpression.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.grdExpression.Location = new System.Drawing.Point(3, 16);
            this.grdExpression.Name = "grdExpression";
            this.grdExpression.Size = new System.Drawing.Size(514, 102);
            this.grdExpression.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // ArithmeticExpressionControl
            // 
            this.Controls.Add(this.groupBox1);
            this.Name = "ArithmeticExpressionControl";
            this.Size = new System.Drawing.Size(520, 150);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdExpression)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private void button1_Click(object sender, EventArgs e)
		{
			CheckExpressionTable();

		}

		private void CheckExpressionTable()
		{
			if (itemBeingEdited != null && itemBeingEdited.Definition != null)
			{
				string Message;
				bool isOk = itemBeingEdited.Definition.ValidateFromTable(this.expressionTable, out Message);
				if (!isOk)
				{
					string msg = Message.Replace("\\n", "\n");
					msg = msg.Replace("\\t", "\t");
					MessageBox.Show(msg, Common.About.FormsTitle);
				}
				else
				{
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl.ExpressionOkayMessage"), Common.About.FormsTitle);
				}
			}
		}

		public void Commit()
		{
			itemBeingEdited.Definition.SaveDataTable(this.expressionTable);
		}

		public bool ValidateExpression(out string Message)
		{
			if (itemBeingEdited.Definition != null)
			{
				return (this.itemBeingEdited.Definition.ValidateFromTable(this.expressionTable, out Message));
			}
			else
			{
//				Message = "No expression bound to the control.";
				Message = string.Empty;
				return false;
			}
		}
	}
}