
using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.ArithmeticDesigner
{
	/// <summary>
	/// Summary description for ArithmeticDesignerForm.
	/// </summary>
	public class ArithmeticDesignerForm : Form
	{
		private bool okayToClose = true;

		private Button btnOk;
		private Button btnCancel;
		private ArithmeticExpressionControl arithmeticExpressionControl1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private ArithmeticPFunction itemBeingEdited;

		public ArithmeticPFunction ItemBeingEdited
		{
			get { return itemBeingEdited; }
			set { itemBeingEdited = value; }
		}


		public ArithmeticDesignerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ArithmeticDesignerForm));
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.arithmeticExpressionControl1 = new Perfectus.Client.Studio.UI.PackageObjectEditors.ArithmeticDesigner.ArithmeticExpressionControl();
			this.SuspendLayout();
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// arithmeticExpressionControl1
			// 
			this.arithmeticExpressionControl1.AccessibleDescription = resources.GetString("arithmeticExpressionControl1.AccessibleDescription");
			this.arithmeticExpressionControl1.AccessibleName = resources.GetString("arithmeticExpressionControl1.AccessibleName");
			this.arithmeticExpressionControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("arithmeticExpressionControl1.Anchor")));
			this.arithmeticExpressionControl1.AutoScroll = ((bool)(resources.GetObject("arithmeticExpressionControl1.AutoScroll")));
			this.arithmeticExpressionControl1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("arithmeticExpressionControl1.AutoScrollMargin")));
			this.arithmeticExpressionControl1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("arithmeticExpressionControl1.AutoScrollMinSize")));
			this.arithmeticExpressionControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("arithmeticExpressionControl1.BackgroundImage")));
			this.arithmeticExpressionControl1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("arithmeticExpressionControl1.Dock")));
			this.arithmeticExpressionControl1.Enabled = ((bool)(resources.GetObject("arithmeticExpressionControl1.Enabled")));
			this.arithmeticExpressionControl1.Font = ((System.Drawing.Font)(resources.GetObject("arithmeticExpressionControl1.Font")));
			this.arithmeticExpressionControl1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("arithmeticExpressionControl1.ImeMode")));
			this.arithmeticExpressionControl1.ItemBeingEdited = null;
			this.arithmeticExpressionControl1.Location = ((System.Drawing.Point)(resources.GetObject("arithmeticExpressionControl1.Location")));
			this.arithmeticExpressionControl1.Name = "arithmeticExpressionControl1";
			this.arithmeticExpressionControl1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("arithmeticExpressionControl1.RightToLeft")));
			this.arithmeticExpressionControl1.Size = ((System.Drawing.Size)(resources.GetObject("arithmeticExpressionControl1.Size")));
			this.arithmeticExpressionControl1.TabIndex = ((int)(resources.GetObject("arithmeticExpressionControl1.TabIndex")));
			this.arithmeticExpressionControl1.Visible = ((bool)(resources.GetObject("arithmeticExpressionControl1.Visible")));
			// 
			// ArithmeticDesignerForm
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.arithmeticExpressionControl1);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "ArithmeticDesignerForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ArithmeticDesignerForm_Closing);
			this.Load += new System.EventHandler(this.ArithmeticDesignerForm_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private void ArithmeticDesignerForm_Load(object sender, EventArgs e)
		{
			arithmeticExpressionControl1.ItemBeingEdited = itemBeingEdited;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			SaveSelf();
		}


		private void SaveSelf()
		{
			string message = null;
			bool expressionOk = arithmeticExpressionControl1.ValidateExpression(out message);
			if (!expressionOk)
			{
                message = message.Replace("\\n", "\n")
                                 .Replace("\\t", "\t");
                MessageBox.Show(message, Common.About.FormsTitle);
				okayToClose = false;
			}
			else
			{
				arithmeticExpressionControl1.Commit();


				okayToClose = true;
			}
		}

		private void ArithmeticDesignerForm_Closing(object sender, CancelEventArgs e)
		{
			if (!okayToClose)
			{
				e.Cancel = true;
			}
			else
			{
				e.Cancel = false;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			okayToClose = true;
		}
	}
}

