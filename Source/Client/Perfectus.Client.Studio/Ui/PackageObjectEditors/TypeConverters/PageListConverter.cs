using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
	public class PageListConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			InterviewPage v = value as InterviewPage;
			if (v == null)
			{
				return null;
			}
			else
			{
				return v.Name;
			}
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (string))
			{
				return false;
			}
			else
			{
				return base.CanConvertFrom(context, sourceType);
			}
		}

	}
}