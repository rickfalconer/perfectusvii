using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
	public class StringBindingConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			string v = value as string;
			if (v == null)
			{
				return null;
			}
			else
			{		
				if(v.IndexOf("<text") == 0)
				{
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.StringBindingConverter.XmlValue"));
				}
				else
				{
					return v;
				}
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value != null)
			{
				return value.ToString();				
			}
			else
			{
				return null;
			}
		}
	}
}