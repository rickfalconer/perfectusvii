using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	public class OutcomeConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			if (value != null)
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.OutcomeConverter.DefinitionValue");
			}
			else
			{
				return null;
			}
		}
	}
}

