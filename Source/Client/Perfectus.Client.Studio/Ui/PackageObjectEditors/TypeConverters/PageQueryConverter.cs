using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
    /// <summary>
    /// Summary description for TextQuestionConverter.
    /// </summary>
    public class PageQueryConverter : StringConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            PageQueryValue v = value as PageQueryValue;
            if (v == null)
            {
                return null;
            }
            else
            {
                switch (v.ValueType)
                {
                    case PageQueryValue.PageQueryValueType.NoValue:
                        return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.PageQueryNullDefault");
                    case PageQueryValue.PageQueryValueType.Page:
                        if (v.PageValue != null)
                        {
                            return string.Format(CultureInfo.InvariantCulture, "�{0}�", v.PageValue.Name);
                        }
                        else
                        {
                            return null;
                        }
                    case PageQueryValue.PageQueryValueType.Query:
                        return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.QueryValue");
                    default:
                        return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.PageQueryNullDefault");
                }
            }
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return false;
            }
            else
            {
                return base.CanConvertFrom(context, sourceType);
            }
        }

    }
}