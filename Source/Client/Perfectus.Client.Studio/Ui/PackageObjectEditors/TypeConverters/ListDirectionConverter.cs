using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class ListDirectionConverter : StringConverter
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			//return new StandardValuesCollection(Enum.GetNames(typeof (Question.ListDirection)));
			string[] localisedItems = new string[Question.ListDirection.GetNames(typeof(Question.ListDirection)).Length];
			int i = 0;
			foreach(Question.ListDirection ld in (Question.ListDirection[])Question.ListDirection.GetValues(typeof(Question.ListDirection)))
			{
				localisedItems[i] = Question.LocaliseQuestionDirection(ld);
				i++;
			}
			return new StandardValuesCollection(localisedItems);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (Question.ListDirection))
			{
				return true;
			}
			else
			{
				return base.CanConvertFrom(context, sourceType);
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				return Question.DelocaliseDirection((string)value);
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is Question.ListDirection)
			{
				return Question.LocaliseQuestionDirection((Question.ListDirection)value);
			}
			else
			{
				return base.ConvertTo (context, culture, value, destinationType);
			}
		}


	}
}