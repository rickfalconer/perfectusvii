using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
	public class ItemsConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			ItemsItemCollection v = value as ItemsItemCollection;
			if (v == null)
			{
				return null;
			}
			else
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.ItemsConverter.CollectionValue");
			}
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType != typeof (ItemsItemCollection))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
	}
}