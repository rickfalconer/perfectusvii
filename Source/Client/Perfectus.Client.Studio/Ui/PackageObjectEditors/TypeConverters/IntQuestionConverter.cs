using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for IntQuestionConverter.
	/// </summary>
	public class IntQuestionConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			IntQuestionValue v = value as IntQuestionValue;
			if (v == null)
			{
				return null;
			}
			else
			{
				switch (v.ValueType)
				{
					case IntQuestionValue.IntQuestionValueType.NoValue:
						return null;
					case IntQuestionValue.IntQuestionValueType.Question:
						return string.Format(CultureInfo.InvariantCulture, "�{0}�", v.QuestionValue.Name);
					case IntQuestionValue.IntQuestionValueType.Int:
						return v.IntValue.ToString();
					default:
						return null;
				}
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				IntQuestionValue output = new IntQuestionValue();
				if (value.ToString() == string.Empty)
				{
					// return a 'null' value object.
					return output;
				}
				int v = int.Parse(value.ToString());
				output.IntValue = v;
				return output;
			}
			else
			{
				return null;
			}
		}
	}
}