using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for YesNoQueryConverter.
	/// </summary>
	public class YesNoQueryConverter : StringConverter
	{		
		private string localYes = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter.YesValue");
        private string localNo = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter.NoValue");

		
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			YesNoQueryValue v = value as YesNoQueryValue;
			if (v == null)
			{
                return null;
			}
			else
			{
				switch (v.ValueType)
				{
					case YesNoQueryValue.YesNoQueryValueType.NoValue:
                        return null;
					case YesNoQueryValue.YesNoQueryValueType.Query:
						return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter.QueryValue");
					case YesNoQueryValue.YesNoQueryValueType.YesNo:
						return v.YesNoValue ? localYes : localNo;
					default:
                        return null;
				}
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				string v = value.ToString();
				YesNoQueryValue output = new YesNoQueryValue();
				string compare = v.ToUpper(CultureInfo.InvariantCulture);
				if (compare == "YES" 
					|| compare == "TRUE" 
					|| compare == localYes.ToUpper(CultureInfo.InvariantCulture) )
				{
					output.YesNoValue = true;
					return output;				
				}				
				
				if (compare == "NO"
					|| compare == "FALSE"
					|| compare == localNo.ToUpper(CultureInfo.InvariantCulture))
				{
					output.YesNoValue = false;
					return output;					
				}

				return null;
			}
			else
			{
				return null;
			}
		}

        /// <summary>
        /// Preventing the user from changing the YesNo values from the ones which are allowable.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return false;
            }
            else
            {
                return base.CanConvertFrom(context, sourceType);
            }
        }
	}
}