using System;
using System.ComponentModel;
using System.Globalization;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class FormatStringConverter : StringConverter
	{

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			string v = null;
			if (value is string)
			{
                v = value as string;					
			}

			if (v == null || v.Length == 0)
			{
				return null;
			}
			else
			{
				return "Format String...";
				//return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.ItemsConverter.CollectionValue");
			}
		}
	}
}
