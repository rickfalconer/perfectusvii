using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WordDocumentPropertyConverter.
	/// </summary>
	public class WordDocumentPropertyCollectionConverter : StringConverter
	{		
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			WordDocumentPropertyCollection v = value as WordDocumentPropertyCollection;
			
			if (v == null)
			{
				return null;
			}
			else
			{
				if(v.Count == 0)
				{
					return null;
				}
				else
				{
					context.OnComponentChanged();
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.WordDocumentPropertyCollectionConverter.CollectionValue"));
				}
			}
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType != typeof (WordDocumentPropertyCollection))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

	}
}