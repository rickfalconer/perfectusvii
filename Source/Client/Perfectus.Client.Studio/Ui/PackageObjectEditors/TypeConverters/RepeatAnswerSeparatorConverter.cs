using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class RepeatAnswerSeparatorConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			if (value == null)
			{
				return null;
			}
			else
			{
				string v = value as string;
				if(v.ToUpper().IndexOf("#TAB#") >= 0)
				{
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.Tab"));
				}
				else if(v.ToUpper().IndexOf("#PARA#") >= 0)
				{
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.Para"));
				}
				else if(v.ToUpper().IndexOf("#CR#") >= 0)
				{
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.CR"));
				}

				return v;
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			// selected string to object

			string v = value as string;

			if(v.ToUpper().IndexOf(string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.Tab")).ToUpper()) >= 0)
			{
				return "#TAB#";
			}
			else if(v.ToUpper().IndexOf(string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.Para")).ToUpper()) >= 0)
			{
				return "#PARA#";
			}
			else if(v.ToUpper().IndexOf(string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RepeatAnswerSeparatorConverter.CR")).ToUpper()) >= 0)
			{
				return "#CR#";
			}			

			return base.ConvertFrom(context, culture, value);

		}	

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return false;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			string[] names = new string[] {"#TAB#", "#PARA#", "#CR#", ", ", ". ", " - ", " and ", " & ", " or "};
			return new StandardValuesCollection(names);
		}
	}
}