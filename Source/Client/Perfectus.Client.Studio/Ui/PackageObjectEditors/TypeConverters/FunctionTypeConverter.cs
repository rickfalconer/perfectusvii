using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for FunctionTypeConverter.
	/// </summary>
	public class FunctionTypeConverter : StringConverter
	{
	    public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.FunctionTypeConverter.DefinitionValue");
		}
	}
}
