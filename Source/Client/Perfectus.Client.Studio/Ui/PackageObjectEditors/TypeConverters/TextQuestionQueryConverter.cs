using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
    /// <summary>
    /// Summary description for TextQuestionConverter.
    /// </summary>
    public class TextQuestionQueryConverter : StringConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            TextQuestionQueryValue v = value as TextQuestionQueryValue;
            if (v == null)
            {
                return null;
            }
            else
            {
                switch (v.ValueType)
                {
                    case TextQuestionQueryValue.TextQuestionQueryValueType.NoValue:
                        return null;
                    case TextQuestionQueryValue.TextQuestionQueryValueType.Question:
                        return string.Format(CultureInfo.InvariantCulture, "�{0}�", v.QuestionValue.Name);
                    case TextQuestionQueryValue.TextQuestionQueryValueType.Query:
                        return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.TextQuestionQueryConverter.Query");
                    case TextQuestionQueryValue.TextQuestionQueryValueType.Text:
                        if (v.TextValue != null)
                        {
                            if (v.TextValue.IndexOf("<text") == 0)
                            {
                                return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.StringBindingConverter.XmlValue"));
                            }
                            else
                            {
                                return v.TextValue;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    default:
                        return null;
                }
            }
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is string)
            {
                TextQuestionQueryValue output = new TextQuestionQueryValue();
                if (value.ToString() == string.Empty)
                {
                    // return a 'null' value object.
                    return output;
                }
                string v = value.ToString();
                output.TextValue = v;
                return output;
            }
            else
            {
                return null;
            }
        }
    }
}