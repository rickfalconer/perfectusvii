using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
	public class BindingInfoConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			DataBindingInfo v = value as DataBindingInfo;
			if (v == null)
			{
				return null;
			}
			else
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.BindingInfoConverter.BoundValue");
			}
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (string) || sourceType == typeof (DataBindingInfo))
			{
				return true;
			}
			else
			{
				return base.CanConvertFrom(context, sourceType);
			}
		}


		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string && value.ToString().Length == 0)
			{
				return null;
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

	}
}