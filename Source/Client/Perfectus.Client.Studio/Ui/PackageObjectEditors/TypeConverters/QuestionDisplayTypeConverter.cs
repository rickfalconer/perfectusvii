using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class QuestionDisplayTypeConverter : StringConverter
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			if (context.Instance is Question)
			{
				QuestionDisplayType[] options = PerfectusTypeSystem.GetAvailableDisplayTypes(context.Instance as Question);
				string[] retVal = new string[options.Length];
				for (int i = 0; i < options.Length; i++)
				{
					if (options[i] != QuestionDisplayType.HtmlArea)
					{
						retVal[i] = PerfectusTypeSystem.LocaliseDisplayType(options[i], (Question)context.Instance);//Enum.GetName(typeof (QuestionDisplayType), options[i]);
					}
				}
				return new StandardValuesCollection(retVal);
			}
			else
			{
				return null;
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				//return Enum.Parse(typeof (QuestionDisplayType), value as string, true);
				return PerfectusTypeSystem.DelocaliseDisplayTypeName((string)value, context.Instance as Question);
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is QuestionDisplayType && context.Instance is Question)
			{
				return PerfectusTypeSystem.LocaliseDisplayType((QuestionDisplayType)value, (Question)context.Instance);
			}
			else
			{
				return base.ConvertTo (context, culture, value, destinationType);
			}
		}

	}
}