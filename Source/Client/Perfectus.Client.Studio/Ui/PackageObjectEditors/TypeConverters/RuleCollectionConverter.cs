using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for RuleCollectionConverter.
	/// </summary>
	public class RuleCollectionConverter : StringConverter
	{		
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			Rule2Collection v = value as Rule2Collection;
			
			if (v == null)
			{
				return null;
			}
			else
			{
				if(v.Count == 0)
				{
					return null;
				}
				else
				{
					return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.RuleCollectionConverter.CollectionValue"));
				}
			}
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType != typeof (Rule2Collection))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

	}
}