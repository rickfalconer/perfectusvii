using System.ComponentModel;
using System.Configuration;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class CssNameConverter : StringConverter
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return false;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			//string[] webFonts = new string[] {"Arial", "Times New Roman", "Verdana"};
			string configKey = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.Config.CssNames");
			string configValue = ConfigurationSettings.AppSettings[configKey];
				
			//string configValue = ConfigurationSettings.AppSettings["CssNames"];
			if (configValue != null)
			{
				string[] names = configValue.Split('\t');
				return new StandardValuesCollection(names);
			}
			else
			{
				return null;
			}

		}
	}
}