using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for TextQuestionConverter.
	/// </summary>
	public class RegexUrlConverter : StringConverter
	{
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
		{
			RegexUrlValue v = value as RegexUrlValue;
			if (v == null)
			{
				return null;
			}
			else
			{
				switch (v.ValueType)
				{
					case RegexUrlValue.RegexUrlValueType.NoValue:
						return null;
					case RegexUrlValue.RegexUrlValueType.Regex:
						return string.Format(CultureInfo.InvariantCulture, "�{0}�", v.RegexValue.ToString());
					case RegexUrlValue.RegexUrlValueType.Url:
						return string.Format(CultureInfo.InvariantCulture, "�{0}�", v.UrlValue.ToString());
					default:
						return null;
				}
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
            // FB2302: The user types in a regular expression without using the editor. In this case we can assume it is 
            // a regular expression and NOT a url as the editor no longer allows the user to enter urls. The facility to enter 
            // urls was removed with the comment "Navitas issue tracker 3305".
            
            // This will convert to a string.
            string stringvalue = (string)base.ConvertFrom( context, culture, value );

            // Trim chars which are inserted by us.
            char[ ] trimchars = new char[ ] { '�', '�' };
            stringvalue = stringvalue.Trim( trimchars );

            // Now we need to convert it to a RegexUrlValue object 
            RegexUrlValue regvalue = new RegexUrlValue( );
            
            // Perform validation
            string errormsg = String.Empty;
            if( !regvalue.IsValidRegex( stringvalue, ref errormsg ) )
                throw new FormatException( errormsg );
            
            // As per the comment above, we can assume its a regex.
            regvalue.ValueType = RegexUrlValue.RegexUrlValueType.Regex;
            regvalue.RegexValue = stringvalue;
            return regvalue;
		}


        public override bool CanConvertFrom( ITypeDescriptorContext context, Type sourceType )
        {
            if( sourceType == typeof( string ) )
            {
                return true;
            }
            else
            {
                return base.CanConvertFrom( context, sourceType );
            }
        }

	}
}