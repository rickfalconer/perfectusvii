using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.Ui.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for ListPreviewStyleConverter.
	/// </summary>
	public class ListPreviewStyleConverter : StringConverter
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			//return new StandardValuesCollection(Enum.GetNames(typeof (Question.ListDirection)));
			string[] localisedItems = new string[InternalPage.ListPreviewStyle.GetNames(typeof(InternalPage.ListPreviewStyle)).Length];
			int i = 0;
			foreach(InternalPage.ListPreviewStyle lps in (InternalPage.ListPreviewStyle[])InternalPage.ListPreviewStyle.GetValues(typeof(InternalPage.ListPreviewStyle)))
			{
				localisedItems[i] = InternalPage.LocalisePreviewStyle(lps);
				i++;
			}
			return new StandardValuesCollection(localisedItems);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (InternalPage.ListPreviewStyle))
			{
				return true;
			}
			else
			{
				return base.CanConvertFrom(context, sourceType);
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
                return InternalPage.DelocalisePreviewStyle((string)value);
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is InternalPage.ListPreviewStyle)
			{
                return InternalPage.LocalisePreviewStyle((InternalPage.ListPreviewStyle)value);
			}
			else
			{
				return base.ConvertTo (context, culture, value, destinationType);
			}
		}
	}
}
