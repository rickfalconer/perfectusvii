using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
    public class ArithmeticPFunctionQueryRoundingConverter : StringConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            if (value == null)
            {
                return string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.ArithmeticPFunctionQueryRoundingConverter.Null"));
            }
            else
            {
                return value.ToString();
            }
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            // selected string to object
            string v = value as string;

            if (v.IndexOf(string.Format(CultureInfo.InvariantCulture, "{0}", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.ArithmeticPFunctionQueryRoundingConverter.Null"))) >= 0)
            {
                return null;
            }

            return base.ConvertFrom(context, culture, value);
        }

        // ------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///		Indicate that this converter provideds a drop-down of choices.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Returns true</returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public override bool GetStandardValuesSupported(
            ITypeDescriptorContext context)
        {
            return true;
        }

        // ------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Returns true</returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public override bool GetStandardValuesExclusive(
            ITypeDescriptorContext context)
        {
            // returning false here means the property will
            // have a drop down and a value that can be manually
            // entered.      
            return false;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            object[] values = new object[] { null, 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            return new StandardValuesCollection(values);
        }
    }
}
