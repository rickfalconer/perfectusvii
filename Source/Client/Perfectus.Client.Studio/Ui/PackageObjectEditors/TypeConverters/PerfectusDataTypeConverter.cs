using System;
using System.ComponentModel;
using System.Globalization;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters
{
	/// <summary>
	/// Summary description for WebFontListConverter.
	/// </summary>
	public class PerfectusDataTypeConverter : StringConverter
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that this converter provideds a drop-down of choices.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesSupported(
			ITypeDescriptorContext context)
		{
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Indicate that the property can't be typed into, only values from the dropdown are allowed.
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Returns true</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override bool GetStandardValuesExclusive(
			ITypeDescriptorContext context)
		{
			// returning false here means the property will
			// have a drop down and a value that can be manually
			// entered.      
			return true;
		}

		public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			return new StandardValuesCollection(PerfectusTypeSystem.GetEnabledTypeNamesLocalised());
		}

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof (PerfectusDataType))
			{
				return true;
			}
			else
			{
                if (context.Instance is Question)
                {
                    Question q = context.Instance as Question;
                    if (q.DataType != PerfectusDataType.Attachment)
                        // lookup Attachment list
                        foreach (AttachmentBase attachment in q.ParentPackage.Attachments)
                        {
                            if (attachment is AttachmentDynamic &&
                            ((AttachmentDynamic)attachment).GetPayload() == q.UniqueIdentifier)
                            {
                                q.DataType = PerfectusDataType.Attachment;

                                string localAttachmentsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.AttachmentsNodeName");
                                string message = String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Attachment.CannotChangeQuestionType"), localAttachmentsNodeName);
                                string caption = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Attachment.CannotChangeQuestionTypeCaption");
                                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                return false;
                            }

                        }
                }
                return base.CanConvertFrom(context, sourceType);
			}
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				return PerfectusTypeSystem.DelocaliseTypeName((string)value);
			}
			else
			{
				return base.ConvertFrom(context, culture, value);
			}
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is PerfectusDataType)
			{
                String cur = ((PerfectusDataType)value).ToString();
				return PerfectusTypeSystem.LocaliseType((PerfectusDataType)value);
			}
			else
			{
				return base.ConvertTo (context, culture, value, destinationType);
			}
		}


	}
}