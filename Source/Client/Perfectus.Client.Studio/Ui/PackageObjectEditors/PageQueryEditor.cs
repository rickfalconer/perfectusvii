using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for YesNoQueryEditor.
	/// </summary>
	public class PageQueryEditor : UITypeEditor
	{
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
		{
			PageQueryEditorControl ui = null;

			// get the editor service.
			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService) sp.GetService(typeof (IWindowsFormsEditorService));

			if (edSvc == null)
			{
				// uh oh.
				return value;
			}

			// create our UI
			if (ui == null)
			{
				ui = new PageQueryEditorControl(edSvc);
				object p = context.Instance as InterviewPage;
				if (p != null)
				{
					ui.Pages = ((InterviewPage) p).ParentInterview.Pages;

					// Don't let the next page be myself.
					InterviewPageCollection excludePages = new InterviewPageCollection();
					excludePages.Add(p as InterviewPage);
					ui.PagesToExclude = excludePages;

					ui.PageBeingEdited = (InterviewPage) p;
				}
				else
				{
					ui.Pages = null;
				}
				ui.Value = value as PageQueryValue;
			}

			edSvc.DropDownControl(ui);

			return ui.Value;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Specify that this editor is the drop down style (instead of the [...] popup style)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>DropDown</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}
	}
}