using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for TextQuestionEditor.
	/// </summary>
	public class TextQuestionQueryEditor : UITypeEditor
	{
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            TextQuestionEditorControl ui = null;
            DialogResult result;

            // get the editor service.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)sp.GetService(typeof(IWindowsFormsEditorService));

            if (edSvc == null)
            {
                // uh oh.
                return value;
            }

            // Ensure the property changed event is fired
            if (context.Instance is LibraryItem && ((LibraryItem)context.Instance).LibraryUniqueIdentifier != Guid.Empty &&
                ((LibraryItem)context.Instance).Linked == true &&
                ((LibraryItem)context.Instance).CheckedOutStatus == CheckOutStatus.CheckedIn)
            {
                result = MessageBox.Show(Globals.gcCHECK_OUT_MESSAGE, Globals.gcSHARED_LIBRARY_TITLE,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            else
            {
                result = DialogResult.Ignore;
            }

            if (result == DialogResult.Yes || result == DialogResult.No || result == DialogResult.Ignore)
            {
                // Check, unlink (if shared library item), else ignore and continue
                switch (result)
                {
                    case DialogResult.Yes:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcCHECKOUT_MARKER);
                        break;
                    case DialogResult.No:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcUNLINK_MARKER);
                        break;
                    case DialogResult.Ignore:
                        break;
                }

                // create our UI

                TextQuestionQueryValue oldValue = null;
                if ( value != null )
                    oldValue = (value as TextQuestionQueryValue).Clone() as TextQuestionQueryValue;

                if (ui == null)
                {
                    ui = new TextQuestionEditorControl(edSvc);

                    //TODO: Handle pageItems as well as packageItems. See IntQuestionEditor code.
                    object c = context.Instance as PackageItem;
                    if (c != null)
                    {
                        ui.Questions = ((PackageItem)c).ParentPackage.Questions;
                        ui.Package = ((PackageItem)c).ParentPackage;
                        ui.ItemBeingEdited = ((PackageItem)c);
                    }
                    else
                    {
                        ui.Questions = null;
                    }

                    // if we are editing the default answer property, we need to tell the form a circular refernce check must be performed.
                    if (context.PropertyDescriptor.Name.ToUpper().IndexOf("DEFAULT") > -1)
                    {
                        ui.CheckCircularRef = true;
                    }

                    ui.Value = value as TextQuestionQueryValue;
                }

                edSvc.DropDownControl(ui);

                if (oldValue != null &&
                    oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query &&
                    ui.Value.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
                    if (DialogResult.No == MessageBox.Show(
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.ShallRemoveQuery"),
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.UserConfirmation"),
    MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        return oldValue;

                return ui.Value;
            }
            else
            {
                // Do nothing
                return value;
            }
        }

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Specify that this editor is the drop down style (instead of the [...] popup style)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>DropDown</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}

		public override bool GetPaintValueSupported(ITypeDescriptorContext context)
		{
			return false;
		}

//		public override void PaintValue(PaintValueEventArgs e)
//		{
//			
//			ImageList imageList1 = new ImageList();
//			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(TextQuestionEditor));
//			imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
//			
//			TextQuestionValue v = e.Value as TextQuestionValue;
//			if (v == null || v.ValueType == TextQuestionValue.TextQuestionValueType.NoValue)
//			{
//				e.Graphics.FillRectangle(new SolidBrush(Color.White), e.Bounds);
//			}
//			else
//			{
//				switch (v.ValueType)
//				{
//					case TextQuestionValue.TextQuestionValueType.Question:
//						e.Graphics.DrawImage(imageList1.Images[0], e.Bounds);
//						break;
//					case TextQuestionValue.TextQuestionValueType.Text:
//						e.Graphics.DrawImage(imageList1.Images[1], e.Bounds);
//						break;
//				}
//			}
//			resources.ReleaseAllResources();
//		}
	}
}
