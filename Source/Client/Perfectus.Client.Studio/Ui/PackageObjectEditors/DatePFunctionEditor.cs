using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for StringEditor.
	/// </summary>
	public class DatePFunctionEditor : UITypeEditor
	{
		private DateFunctionEditorForm ui;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Launch the LargeTextEditorForm.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sp"></param>
		/// <param name="value">The current value of the property.</param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            DialogResult result;

            // get the editor service.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)sp.GetService(typeof(IWindowsFormsEditorService));

            if (edSvc == null)
            {
                // uh oh.
                return value;
            }

            // Ensure the property changed event is fired
            if (context.Instance is LibraryItem && ((LibraryItem)context.Instance).LibraryUniqueIdentifier != Guid.Empty &&
                ((LibraryItem)context.Instance).Linked == true &&
                ((LibraryItem)context.Instance).CheckedOutStatus == CheckOutStatus.CheckedIn)
            {
                result = MessageBox.Show(Globals.gcCHECK_OUT_MESSAGE, Globals.gcSHARED_LIBRARY_TITLE,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            else
            {
                result = DialogResult.Ignore;
            }

            if (result == DialogResult.Yes || result == DialogResult.No || result == DialogResult.Ignore)
            {
                // Check, unlink (if shared library item), else ignore and continue
                switch (result)
                {
                    case DialogResult.Yes:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcCHECKOUT_MARKER);
                        break;
                    case DialogResult.No:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcUNLINK_MARKER);
                        break;
                    case DialogResult.Ignore:
                        break;
                }

                Package p = null;
                if (context.Instance is PackageItem)
                {
                    p = ((PackageItem)context.Instance).ParentPackage;
                }

                ui = new DateFunctionEditorForm();
                ui.Package = p;
                if (value == null)
                {
                    ui.Definition = null;
                }
                else
                {
                    ui.Definition = value as DateExpression;
                }

                ui.FunctionBeingEdited = ((DatePFunction)context.Instance);
                DialogResult dr = edSvc.ShowDialog(ui);
                DateExpression resultDefintion = ui.Definition;

                ui.Dispose();
                if (dr == DialogResult.OK)
                    return resultDefintion;
                else
                    return value;
            }
            else
            {
                // Do nothing
                return value;
            }
        }

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// Specify that this editor is the popup style ([...] button)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Modal</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}
	}
}
