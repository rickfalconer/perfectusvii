using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
using Perfectus.Common;

namespace Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for PropertiesSummaryControl.
	/// </summary>
	public class PropertiesSummaryControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label lblTitle;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private WordDocumentPropertyCollection documentProperties = new WordDocumentPropertyCollection();
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox txtTitle;
		private System.Windows.Forms.ComboBox cbxTitle;
		private System.Windows.Forms.ComboBox cbxSubject;
		private System.Windows.Forms.TextBox txtSubject;
		private System.Windows.Forms.ComboBox cbxAuthor;
		private System.Windows.Forms.TextBox txtAuthor;
		private System.Windows.Forms.Panel pnlTitle;
		private System.Windows.Forms.RadioButton rbtTitleTextValue;
		private System.Windows.Forms.RadioButton rbtTitleQuestionValue;
		private System.Windows.Forms.RadioButton rbtSubjectTextValue;
		private System.Windows.Forms.RadioButton rbtSubjectQuestionValue;
		private System.Windows.Forms.RadioButton rbtAuthorTextValue;
		private System.Windows.Forms.RadioButton rbtAuthorQuestionValue;
		private System.Windows.Forms.Panel pnlSubject;
		private System.Windows.Forms.Label lblSubject;
		private System.Windows.Forms.Panel pnlAuthor;
		private System.Windows.Forms.Label lblAuthor;
		private System.Windows.Forms.Panel pnlManager;
		private System.Windows.Forms.RadioButton rbtManagerTextValue;
		private System.Windows.Forms.Label lblManager;
		private System.Windows.Forms.RadioButton rbtManagerQuestionValue;
		private System.Windows.Forms.ComboBox cbxManager;
		private System.Windows.Forms.TextBox txtManager;
		private System.Windows.Forms.Panel pnlCompany;
		private System.Windows.Forms.RadioButton rbtCompanyTextValue;
		private System.Windows.Forms.Label lblCompany;
		private System.Windows.Forms.RadioButton rbtCompanyQuestionValue;
		private System.Windows.Forms.ComboBox cbxCompany;
		private System.Windows.Forms.TextBox txtCompany;
		private System.Windows.Forms.Panel pnlHyperlinkBase;
		private System.Windows.Forms.RadioButton rbtHyperlinkBaseTextValue;
		private System.Windows.Forms.RadioButton rbtHyperlinkBaseQuestionValue;
		private System.Windows.Forms.ComboBox cbxHyperlinkBase;
		private System.Windows.Forms.TextBox txtHyperlinkBase;
		private System.Windows.Forms.Panel pnlKeywords;
		private System.Windows.Forms.RadioButton rbtKeywordsTextValue;
		private System.Windows.Forms.Label lblKeywords;
		private System.Windows.Forms.RadioButton rbtKeywordsQuestionValue;
		private System.Windows.Forms.ComboBox cbxKeywords;
		private System.Windows.Forms.TextBox txtKeywords;
		private System.Windows.Forms.Panel pnlCategory;
		private System.Windows.Forms.RadioButton rbtCategoryTextValue;
		private System.Windows.Forms.Label lblCategory;
		private System.Windows.Forms.RadioButton rbtCategoryQuestionValue;
		private System.Windows.Forms.ComboBox cbxCategory;
		private System.Windows.Forms.TextBox txtCategory;
		private System.Windows.Forms.Label lblHyperlinkBase;
		private System.Windows.Forms.Panel pnlDescription;
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.RadioButton rbtDescriptionTextValue;
		private System.Windows.Forms.RadioButton rbtDescriptionQuestionValue;
		private System.Windows.Forms.ComboBox cbxDescription;
		private System.Windows.Forms.Label lblDescription;
		private Package package = null;

		public WordDocumentPropertyCollection DocumentProperties
		{
			get 
			{ 				
				return documentProperties;
			}
			set 
			{ 
				if(package == null)
				{
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.PackageNotSet"));
				}

				documentProperties = value;
				PopulateFields();
			}
		}

		public Package PackageBeingEdited
		{
            get { return package; }
			set {package = value;}
		}

		public PropertiesSummaryControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void PopulateFields()
		{
			foreach(Control c in this.Controls) // loop through controls, if we find a Panel perform operations on its Children.
			{
				if(c is Panel)
				{
					foreach(Control c2 in c.Controls) // populate the combobox in the panel
					{
						if(c2 is ComboBox)
						{										
							PopulateCombo((ComboBox) c2);
							break;
						}
					}

					foreach(WordDocumentProperty dp in documentProperties) // Loop though each document property, and see if it matches the current panel we are on.
					{
						if(dp.PropertyType != WordDocumentProperty.WordDocumentPropertyType.Standard)
						{
							continue;
						}

						if(dp.DocumentPropertyName.ToLower().Trim() == c.Name.Remove(0,3).ToLower().Trim()) // remove "pnl" before doing the check.
						{
							foreach(Control c3 in c.Controls) // loop through each child control of the Panel and find the textbox and combobox to populate.
							{
								if(c3 is TextBox)
								{
									if(dp.DocumentPropertyValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text && dp.DocumentPropertyValue.TextValue != null)
									{											
										((TextBox) c3).Text = dp.DocumentPropertyValue.TextValue;
										break;									
									}
								}
								else if(c3 is ComboBox)
								{
                                    if (dp.DocumentPropertyValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question && dp.DocumentPropertyValue.QuestionValue != null)
									{	
										foreach(Control c4 in c.Controls) // ensure the correct radio button is checked.
										{
											if(c4 is RadioButton && c4.Name.ToLower().IndexOf("questionvalue") > -1)													
											{
												((RadioButton) c4).Checked = true;
												break;
											}
											
											// select correct combobox item.
											((ComboBox)c3).SelectedItem = dp.DocumentPropertyValue.QuestionValue;
										}
									}
								}
							}

							break;
						}
					}
				}				
			}
		}

		private void PopulateCombo(ComboBox cb)
		{
			foreach(Question q in package.Questions)
			{					
				cb.Items.Add(q);
			}                    					
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertiesSummaryControl));
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.cbxTitle = new System.Windows.Forms.ComboBox();
            this.rbtTitleTextValue = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtTitleQuestionValue = new System.Windows.Forms.RadioButton();
            this.pnlTitle = new System.Windows.Forms.Panel();
            this.pnlSubject = new System.Windows.Forms.Panel();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.rbtSubjectTextValue = new System.Windows.Forms.RadioButton();
            this.lblSubject = new System.Windows.Forms.Label();
            this.rbtSubjectQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxSubject = new System.Windows.Forms.ComboBox();
            this.pnlAuthor = new System.Windows.Forms.Panel();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.rbtAuthorTextValue = new System.Windows.Forms.RadioButton();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.rbtAuthorQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxAuthor = new System.Windows.Forms.ComboBox();
            this.pnlManager = new System.Windows.Forms.Panel();
            this.txtManager = new System.Windows.Forms.TextBox();
            this.rbtManagerTextValue = new System.Windows.Forms.RadioButton();
            this.lblManager = new System.Windows.Forms.Label();
            this.rbtManagerQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxManager = new System.Windows.Forms.ComboBox();
            this.pnlCompany = new System.Windows.Forms.Panel();
            this.txtCompany = new System.Windows.Forms.TextBox();
            this.rbtCompanyTextValue = new System.Windows.Forms.RadioButton();
            this.lblCompany = new System.Windows.Forms.Label();
            this.rbtCompanyQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxCompany = new System.Windows.Forms.ComboBox();
            this.pnlHyperlinkBase = new System.Windows.Forms.Panel();
            this.txtHyperlinkBase = new System.Windows.Forms.TextBox();
            this.rbtHyperlinkBaseTextValue = new System.Windows.Forms.RadioButton();
            this.rbtHyperlinkBaseQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxHyperlinkBase = new System.Windows.Forms.ComboBox();
            this.lblHyperlinkBase = new System.Windows.Forms.Label();
            this.pnlDescription = new System.Windows.Forms.Panel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.rbtDescriptionTextValue = new System.Windows.Forms.RadioButton();
            this.lblDescription = new System.Windows.Forms.Label();
            this.rbtDescriptionQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxDescription = new System.Windows.Forms.ComboBox();
            this.pnlKeywords = new System.Windows.Forms.Panel();
            this.txtKeywords = new System.Windows.Forms.TextBox();
            this.rbtKeywordsTextValue = new System.Windows.Forms.RadioButton();
            this.lblKeywords = new System.Windows.Forms.Label();
            this.rbtKeywordsQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxKeywords = new System.Windows.Forms.ComboBox();
            this.pnlCategory = new System.Windows.Forms.Panel();
            this.txtCategory = new System.Windows.Forms.TextBox();
            this.rbtCategoryTextValue = new System.Windows.Forms.RadioButton();
            this.lblCategory = new System.Windows.Forms.Label();
            this.rbtCategoryQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxCategory = new System.Windows.Forms.ComboBox();
            this.pnlTitle.SuspendLayout();
            this.pnlSubject.SuspendLayout();
            this.pnlAuthor.SuspendLayout();
            this.pnlManager.SuspendLayout();
            this.pnlCompany.SuspendLayout();
            this.pnlHyperlinkBase.SuspendLayout();
            this.pnlDescription.SuspendLayout();
            this.pnlKeywords.SuspendLayout();
            this.pnlCategory.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            resources.ApplyResources(this.lblTitle, "lblTitle");
            this.lblTitle.Name = "lblTitle";
            // 
            // txtTitle
            // 
            resources.ApplyResources(this.txtTitle, "txtTitle");
            this.txtTitle.Name = "txtTitle";
            // 
            // cbxTitle
            // 
            resources.ApplyResources(this.cbxTitle, "cbxTitle");
            this.cbxTitle.Name = "cbxTitle";
            // 
            // rbtTitleTextValue
            // 
            resources.ApplyResources(this.rbtTitleTextValue, "rbtTitleTextValue");
            this.rbtTitleTextValue.Checked = true;
            this.rbtTitleTextValue.Name = "rbtTitleTextValue";
            this.rbtTitleTextValue.TabStop = true;
            this.rbtTitleTextValue.CheckedChanged += new System.EventHandler(this.rbtTitleText_CheckedChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // rbtTitleQuestionValue
            // 
            resources.ApplyResources(this.rbtTitleQuestionValue, "rbtTitleQuestionValue");
            this.rbtTitleQuestionValue.Name = "rbtTitleQuestionValue";
            // 
            // pnlTitle
            // 
            resources.ApplyResources(this.pnlTitle, "pnlTitle");
            this.pnlTitle.Controls.Add(this.txtTitle);
            this.pnlTitle.Controls.Add(this.rbtTitleTextValue);
            this.pnlTitle.Controls.Add(this.lblTitle);
            this.pnlTitle.Controls.Add(this.rbtTitleQuestionValue);
            this.pnlTitle.Controls.Add(this.cbxTitle);
            this.pnlTitle.Name = "pnlTitle";
            // 
            // pnlSubject
            // 
            resources.ApplyResources(this.pnlSubject, "pnlSubject");
            this.pnlSubject.Controls.Add(this.txtSubject);
            this.pnlSubject.Controls.Add(this.rbtSubjectTextValue);
            this.pnlSubject.Controls.Add(this.lblSubject);
            this.pnlSubject.Controls.Add(this.rbtSubjectQuestionValue);
            this.pnlSubject.Controls.Add(this.cbxSubject);
            this.pnlSubject.Name = "pnlSubject";
            // 
            // txtSubject
            // 
            resources.ApplyResources(this.txtSubject, "txtSubject");
            this.txtSubject.Name = "txtSubject";
            // 
            // rbtSubjectTextValue
            // 
            resources.ApplyResources(this.rbtSubjectTextValue, "rbtSubjectTextValue");
            this.rbtSubjectTextValue.Checked = true;
            this.rbtSubjectTextValue.Name = "rbtSubjectTextValue";
            this.rbtSubjectTextValue.TabStop = true;
            this.rbtSubjectTextValue.CheckedChanged += new System.EventHandler(this.rbtSubjectText_CheckedChanged);
            // 
            // lblSubject
            // 
            resources.ApplyResources(this.lblSubject, "lblSubject");
            this.lblSubject.Name = "lblSubject";
            // 
            // rbtSubjectQuestionValue
            // 
            resources.ApplyResources(this.rbtSubjectQuestionValue, "rbtSubjectQuestionValue");
            this.rbtSubjectQuestionValue.Name = "rbtSubjectQuestionValue";
            // 
            // cbxSubject
            // 
            resources.ApplyResources(this.cbxSubject, "cbxSubject");
            this.cbxSubject.Name = "cbxSubject";
            // 
            // pnlAuthor
            // 
            resources.ApplyResources(this.pnlAuthor, "pnlAuthor");
            this.pnlAuthor.Controls.Add(this.txtAuthor);
            this.pnlAuthor.Controls.Add(this.rbtAuthorTextValue);
            this.pnlAuthor.Controls.Add(this.lblAuthor);
            this.pnlAuthor.Controls.Add(this.rbtAuthorQuestionValue);
            this.pnlAuthor.Controls.Add(this.cbxAuthor);
            this.pnlAuthor.Name = "pnlAuthor";
            // 
            // txtAuthor
            // 
            resources.ApplyResources(this.txtAuthor, "txtAuthor");
            this.txtAuthor.Name = "txtAuthor";
            // 
            // rbtAuthorTextValue
            // 
            resources.ApplyResources(this.rbtAuthorTextValue, "rbtAuthorTextValue");
            this.rbtAuthorTextValue.Checked = true;
            this.rbtAuthorTextValue.Name = "rbtAuthorTextValue";
            this.rbtAuthorTextValue.TabStop = true;
            this.rbtAuthorTextValue.CheckedChanged += new System.EventHandler(this.rbtAuthorTextValue_CheckedChanged);
            // 
            // lblAuthor
            // 
            resources.ApplyResources(this.lblAuthor, "lblAuthor");
            this.lblAuthor.Name = "lblAuthor";
            // 
            // rbtAuthorQuestionValue
            // 
            resources.ApplyResources(this.rbtAuthorQuestionValue, "rbtAuthorQuestionValue");
            this.rbtAuthorQuestionValue.Name = "rbtAuthorQuestionValue";
            // 
            // cbxAuthor
            // 
            resources.ApplyResources(this.cbxAuthor, "cbxAuthor");
            this.cbxAuthor.Name = "cbxAuthor";
            // 
            // pnlManager
            // 
            resources.ApplyResources(this.pnlManager, "pnlManager");
            this.pnlManager.Controls.Add(this.txtManager);
            this.pnlManager.Controls.Add(this.rbtManagerTextValue);
            this.pnlManager.Controls.Add(this.lblManager);
            this.pnlManager.Controls.Add(this.rbtManagerQuestionValue);
            this.pnlManager.Controls.Add(this.cbxManager);
            this.pnlManager.Name = "pnlManager";
            // 
            // txtManager
            // 
            resources.ApplyResources(this.txtManager, "txtManager");
            this.txtManager.Name = "txtManager";
            // 
            // rbtManagerTextValue
            // 
            resources.ApplyResources(this.rbtManagerTextValue, "rbtManagerTextValue");
            this.rbtManagerTextValue.Checked = true;
            this.rbtManagerTextValue.Name = "rbtManagerTextValue";
            this.rbtManagerTextValue.TabStop = true;
            this.rbtManagerTextValue.CheckedChanged += new System.EventHandler(this.rbtManagerTextValue_CheckedChanged);
            // 
            // lblManager
            // 
            resources.ApplyResources(this.lblManager, "lblManager");
            this.lblManager.Name = "lblManager";
            // 
            // rbtManagerQuestionValue
            // 
            resources.ApplyResources(this.rbtManagerQuestionValue, "rbtManagerQuestionValue");
            this.rbtManagerQuestionValue.Name = "rbtManagerQuestionValue";
            // 
            // cbxManager
            // 
            resources.ApplyResources(this.cbxManager, "cbxManager");
            this.cbxManager.Name = "cbxManager";
            // 
            // pnlCompany
            // 
            resources.ApplyResources(this.pnlCompany, "pnlCompany");
            this.pnlCompany.Controls.Add(this.txtCompany);
            this.pnlCompany.Controls.Add(this.rbtCompanyTextValue);
            this.pnlCompany.Controls.Add(this.lblCompany);
            this.pnlCompany.Controls.Add(this.rbtCompanyQuestionValue);
            this.pnlCompany.Controls.Add(this.cbxCompany);
            this.pnlCompany.Name = "pnlCompany";
            // 
            // txtCompany
            // 
            resources.ApplyResources(this.txtCompany, "txtCompany");
            this.txtCompany.Name = "txtCompany";
            // 
            // rbtCompanyTextValue
            // 
            resources.ApplyResources(this.rbtCompanyTextValue, "rbtCompanyTextValue");
            this.rbtCompanyTextValue.Checked = true;
            this.rbtCompanyTextValue.Name = "rbtCompanyTextValue";
            this.rbtCompanyTextValue.TabStop = true;
            this.rbtCompanyTextValue.CheckedChanged += new System.EventHandler(this.rbtCompanyTextValue_CheckedChanged);
            // 
            // lblCompany
            // 
            resources.ApplyResources(this.lblCompany, "lblCompany");
            this.lblCompany.Name = "lblCompany";
            // 
            // rbtCompanyQuestionValue
            // 
            resources.ApplyResources(this.rbtCompanyQuestionValue, "rbtCompanyQuestionValue");
            this.rbtCompanyQuestionValue.Name = "rbtCompanyQuestionValue";
            // 
            // cbxCompany
            // 
            resources.ApplyResources(this.cbxCompany, "cbxCompany");
            this.cbxCompany.Name = "cbxCompany";
            // 
            // pnlHyperlinkBase
            // 
            resources.ApplyResources(this.pnlHyperlinkBase, "pnlHyperlinkBase");
            this.pnlHyperlinkBase.Controls.Add(this.txtHyperlinkBase);
            this.pnlHyperlinkBase.Controls.Add(this.rbtHyperlinkBaseTextValue);
            this.pnlHyperlinkBase.Controls.Add(this.rbtHyperlinkBaseQuestionValue);
            this.pnlHyperlinkBase.Controls.Add(this.cbxHyperlinkBase);
            this.pnlHyperlinkBase.Controls.Add(this.lblHyperlinkBase);
            this.pnlHyperlinkBase.Name = "pnlHyperlinkBase";
            // 
            // txtHyperlinkBase
            // 
            resources.ApplyResources(this.txtHyperlinkBase, "txtHyperlinkBase");
            this.txtHyperlinkBase.Name = "txtHyperlinkBase";
            // 
            // rbtHyperlinkBaseTextValue
            // 
            resources.ApplyResources(this.rbtHyperlinkBaseTextValue, "rbtHyperlinkBaseTextValue");
            this.rbtHyperlinkBaseTextValue.Checked = true;
            this.rbtHyperlinkBaseTextValue.Name = "rbtHyperlinkBaseTextValue";
            this.rbtHyperlinkBaseTextValue.TabStop = true;
            this.rbtHyperlinkBaseTextValue.CheckedChanged += new System.EventHandler(this.rbtHyperlinkBaseTextValue_CheckedChanged);
            // 
            // rbtHyperlinkBaseQuestionValue
            // 
            resources.ApplyResources(this.rbtHyperlinkBaseQuestionValue, "rbtHyperlinkBaseQuestionValue");
            this.rbtHyperlinkBaseQuestionValue.Name = "rbtHyperlinkBaseQuestionValue";
            // 
            // cbxHyperlinkBase
            // 
            resources.ApplyResources(this.cbxHyperlinkBase, "cbxHyperlinkBase");
            this.cbxHyperlinkBase.Name = "cbxHyperlinkBase";
            // 
            // lblHyperlinkBase
            // 
            resources.ApplyResources(this.lblHyperlinkBase, "lblHyperlinkBase");
            this.lblHyperlinkBase.Name = "lblHyperlinkBase";
            // 
            // pnlDescription
            // 
            resources.ApplyResources(this.pnlDescription, "pnlDescription");
            this.pnlDescription.Controls.Add(this.txtDescription);
            this.pnlDescription.Controls.Add(this.rbtDescriptionTextValue);
            this.pnlDescription.Controls.Add(this.lblDescription);
            this.pnlDescription.Controls.Add(this.rbtDescriptionQuestionValue);
            this.pnlDescription.Controls.Add(this.cbxDescription);
            this.pnlDescription.Name = "pnlDescription";
            // 
            // txtDescription
            // 
            resources.ApplyResources(this.txtDescription, "txtDescription");
            this.txtDescription.Name = "txtDescription";
            // 
            // rbtDescriptionTextValue
            // 
            resources.ApplyResources(this.rbtDescriptionTextValue, "rbtDescriptionTextValue");
            this.rbtDescriptionTextValue.Checked = true;
            this.rbtDescriptionTextValue.Name = "rbtDescriptionTextValue";
            this.rbtDescriptionTextValue.TabStop = true;
            this.rbtDescriptionTextValue.CheckedChanged += new System.EventHandler(this.rbtDescriptionTextValue_CheckedChanged);
            // 
            // lblDescription
            // 
            resources.ApplyResources(this.lblDescription, "lblDescription");
            this.lblDescription.Name = "lblDescription";
            // 
            // rbtDescriptionQuestionValue
            // 
            resources.ApplyResources(this.rbtDescriptionQuestionValue, "rbtDescriptionQuestionValue");
            this.rbtDescriptionQuestionValue.Name = "rbtDescriptionQuestionValue";
            // 
            // cbxDescription
            // 
            resources.ApplyResources(this.cbxDescription, "cbxDescription");
            this.cbxDescription.Name = "cbxDescription";
            // 
            // pnlKeywords
            // 
            resources.ApplyResources(this.pnlKeywords, "pnlKeywords");
            this.pnlKeywords.Controls.Add(this.txtKeywords);
            this.pnlKeywords.Controls.Add(this.rbtKeywordsTextValue);
            this.pnlKeywords.Controls.Add(this.lblKeywords);
            this.pnlKeywords.Controls.Add(this.rbtKeywordsQuestionValue);
            this.pnlKeywords.Controls.Add(this.cbxKeywords);
            this.pnlKeywords.Name = "pnlKeywords";
            // 
            // txtKeywords
            // 
            resources.ApplyResources(this.txtKeywords, "txtKeywords");
            this.txtKeywords.Name = "txtKeywords";
            // 
            // rbtKeywordsTextValue
            // 
            resources.ApplyResources(this.rbtKeywordsTextValue, "rbtKeywordsTextValue");
            this.rbtKeywordsTextValue.Checked = true;
            this.rbtKeywordsTextValue.Name = "rbtKeywordsTextValue";
            this.rbtKeywordsTextValue.TabStop = true;
            this.rbtKeywordsTextValue.CheckedChanged += new System.EventHandler(this.rbtKeywordsTextValue_CheckedChanged);
            // 
            // lblKeywords
            // 
            resources.ApplyResources(this.lblKeywords, "lblKeywords");
            this.lblKeywords.Name = "lblKeywords";
            // 
            // rbtKeywordsQuestionValue
            // 
            resources.ApplyResources(this.rbtKeywordsQuestionValue, "rbtKeywordsQuestionValue");
            this.rbtKeywordsQuestionValue.Name = "rbtKeywordsQuestionValue";
            // 
            // cbxKeywords
            // 
            resources.ApplyResources(this.cbxKeywords, "cbxKeywords");
            this.cbxKeywords.Name = "cbxKeywords";
            // 
            // pnlCategory
            // 
            resources.ApplyResources(this.pnlCategory, "pnlCategory");
            this.pnlCategory.Controls.Add(this.txtCategory);
            this.pnlCategory.Controls.Add(this.rbtCategoryTextValue);
            this.pnlCategory.Controls.Add(this.lblCategory);
            this.pnlCategory.Controls.Add(this.rbtCategoryQuestionValue);
            this.pnlCategory.Controls.Add(this.cbxCategory);
            this.pnlCategory.Name = "pnlCategory";
            // 
            // txtCategory
            // 
            resources.ApplyResources(this.txtCategory, "txtCategory");
            this.txtCategory.Name = "txtCategory";
            // 
            // rbtCategoryTextValue
            // 
            resources.ApplyResources(this.rbtCategoryTextValue, "rbtCategoryTextValue");
            this.rbtCategoryTextValue.Checked = true;
            this.rbtCategoryTextValue.Name = "rbtCategoryTextValue";
            this.rbtCategoryTextValue.TabStop = true;
            this.rbtCategoryTextValue.CheckedChanged += new System.EventHandler(this.rbtCategoryTextValue_CheckedChanged);
            // 
            // lblCategory
            // 
            resources.ApplyResources(this.lblCategory, "lblCategory");
            this.lblCategory.Name = "lblCategory";
            // 
            // rbtCategoryQuestionValue
            // 
            resources.ApplyResources(this.rbtCategoryQuestionValue, "rbtCategoryQuestionValue");
            this.rbtCategoryQuestionValue.Name = "rbtCategoryQuestionValue";
            // 
            // cbxCategory
            // 
            resources.ApplyResources(this.cbxCategory, "cbxCategory");
            this.cbxCategory.Name = "cbxCategory";
            // 
            // PropertiesSummaryControl
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.pnlHyperlinkBase);
            this.Controls.Add(this.pnlDescription);
            this.Controls.Add(this.pnlKeywords);
            this.Controls.Add(this.pnlCategory);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlCompany);
            this.Controls.Add(this.pnlManager);
            this.Controls.Add(this.pnlAuthor);
            this.Controls.Add(this.pnlSubject);
            this.Controls.Add(this.pnlTitle);
            this.Controls.Add(this.label1);
            this.Name = "PropertiesSummaryControl";
            this.pnlTitle.ResumeLayout(false);
            this.pnlTitle.PerformLayout();
            this.pnlSubject.ResumeLayout(false);
            this.pnlSubject.PerformLayout();
            this.pnlAuthor.ResumeLayout(false);
            this.pnlAuthor.PerformLayout();
            this.pnlManager.ResumeLayout(false);
            this.pnlManager.PerformLayout();
            this.pnlCompany.ResumeLayout(false);
            this.pnlCompany.PerformLayout();
            this.pnlHyperlinkBase.ResumeLayout(false);
            this.pnlHyperlinkBase.PerformLayout();
            this.pnlDescription.ResumeLayout(false);
            this.pnlDescription.PerformLayout();
            this.pnlKeywords.ResumeLayout(false);
            this.pnlKeywords.PerformLayout();
            this.pnlCategory.ResumeLayout(false);
            this.pnlCategory.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		internal void UpdateStandardDocumentProperties()
		{
			for (int i = documentProperties.Count - 1; i >= 0; i--)
			{
				if(documentProperties[i].PropertyType == WordDocumentProperty.WordDocumentPropertyType.Standard)
				{
					documentProperties.RemoveAt(i);
				}
			}

			foreach(Control c in this.Controls) // loop through controls, if we find a Panel work through its children.
			{
				if(c is Panel)
				{					
					foreach(Control c2 in c.Controls) // loop through each child control of the Panel and find the textbox and combobox to populate.
					{
						if(c2 is RadioButton && c2.Name.ToLower().IndexOf("textvalue") > -1 && ((RadioButton)c2).Checked == true)													
						{
							foreach(Control c3 in c.Controls)
							{
								if(c3 is TextBox)
								{
									WordDocumentProperty dp = new WordDocumentProperty();
									TextQuestionQueryValue tqv = new TextQuestionQueryValue();
									tqv.TextValue = c3.Text;
                                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
									dp.DocumentPropertyValue = tqv;
									dp.DocumentPropertyName = c3.Name.Replace("txt","");
									dp.PropertyType =WordDocumentProperty.WordDocumentPropertyType.Standard;

									documentProperties.Add(dp);
								}
							}
						}
						else if(c2 is RadioButton && ((RadioButton) c2).Checked== true) // it's a question value.
						{
							foreach(Control c3 in c.Controls)
							{
								if(c3 is ComboBox)
								{
									WordDocumentProperty dp = new WordDocumentProperty();
									TextQuestionQueryValue tqv = new TextQuestionQueryValue();
									tqv.QuestionValue = (Question) ((ComboBox) c3).SelectedItem;
                                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Question;
									dp.DocumentPropertyValue = tqv;
									dp.DocumentPropertyName = c3.Name.Replace("cbx","");
									dp.PropertyType =WordDocumentProperty.WordDocumentPropertyType.Standard;

									documentProperties.Add(dp);
								}
							}
						}
					}
				}
			}

		}


		private void rbtTitleText_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtTitleTextValue.Checked == true)
			{
				txtTitle.Enabled = true;
				cbxTitle.Enabled = false;
			}
			else
			{
				txtTitle.Enabled = false;
				cbxTitle.Enabled = true;
			}
		}

		private void rbtSubjectText_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtSubjectTextValue.Checked == true)
			{
				txtSubject.Enabled = true;
				cbxSubject.Enabled = false;
			}
			else
			{
				txtSubject.Enabled = false;
				cbxSubject.Enabled = true;
			}		
		}

		private void rbtAuthorTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtAuthorTextValue.Checked == true)
			{
				txtAuthor.Enabled = true;
				cbxAuthor.Enabled = false;
			}
			else
			{
				txtAuthor.Enabled = false;
				cbxAuthor.Enabled = true;
			}	
		}

		private void rbtManagerTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtManagerTextValue.Checked == true)
			{
				txtManager.Enabled = true;
				cbxManager.Enabled = false;
			}
			else
			{
				txtManager.Enabled = false;
				cbxManager.Enabled = true;
			}			
		}

		private void rbtCompanyTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtCompanyTextValue.Checked == true)
			{
				txtCompany.Enabled = true;
				cbxCompany.Enabled = false;
			}
			else
			{
				txtCompany.Enabled = false;
				cbxCompany.Enabled = true;
			}			
		}

		private void rbtCategoryTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtCategoryTextValue.Checked == true)
			{
				txtCategory.Enabled = true;
				cbxCategory.Enabled = false;
			}
			else
			{
				txtCategory.Enabled = false;
				cbxCategory.Enabled = true;
			}			
		}

		private void rbtKeywordsTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtKeywordsTextValue.Checked == true)
			{
				txtKeywords.Enabled = true;
				cbxKeywords.Enabled = false;
			}
			else
			{
				txtKeywords.Enabled = false;
				cbxKeywords.Enabled = true;
			}			
		}

		private void rbtDescriptionTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtDescriptionTextValue.Checked == true)
			{
				txtDescription.Enabled = true;
				cbxDescription.Enabled = false;
			}
			else
			{
				txtDescription.Enabled = false;
				cbxDescription.Enabled = true;
			}			
		}

		private void rbtHyperlinkBaseTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtHyperlinkBaseTextValue.Checked == true)
			{
				txtHyperlinkBase.Enabled = true;
				cbxHyperlinkBase.Enabled = false;
			}
			else
			{
				txtHyperlinkBase.Enabled = false;
				cbxHyperlinkBase.Enabled = true;
			}			
		}
	}
}
