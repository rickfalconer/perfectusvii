using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for PageQueryEditorControl.
	/// </summary>
	public class PageQueryEditorControl : UserControl
	{
		private Button btnNone;
		private Button btnNewQuery;
        private Button btnEditQuery;
		private IContainer components;
		private IWindowsFormsEditorService edSvc;


		private PageQueryValue currentValue;

		private InterviewPageCollection pages = new InterviewPageCollection();
		private InterviewPageCollection pagesToExclude = new InterviewPageCollection();
		private System.Windows.Forms.ImageList imageList1;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		private InterviewPage pageBeingEdited;

		public InterviewPage PageBeingEdited
		{
			get { return pageBeingEdited; }
			set { pageBeingEdited = value; }
		}

		public PageQueryValue Value
		{
			get { return currentValue; }
			set
			{
				currentValue = value;
				SetButtons();
			}
		}

		public InterviewPageCollection Pages
		{
			get { return pages; }
			set
			{
				pages = value;
				BindList();
			}
		}

		public InterviewPageCollection PagesToExclude
		{
			get { return pagesToExclude; }
			set
			{
				pagesToExclude = value;
				BindList();
			}
		}

		private void SetButtons()
		{
			btnEditQuery.Enabled = (currentValue != null && currentValue.QueryValue != null && currentValue.ValueType == PageQueryValue.PageQueryValueType.Query);
            btnNewQuery.Enabled = (currentValue == null || currentValue.ValueType == PageQueryValue.PageQueryValueType.NoValue || currentValue.ValueType == PageQueryValue.PageQueryValueType.Page);
		}

		private void BindList()
		{
			uiListBox1.Items.Clear();
			if (pages != null)
			{
				//TODO:  Pretty sure this will never be used on a question, but if it is, need to make sure no circular references happen
				foreach (InterviewPage p in pages)
				{
					if (! (pagesToExclude.Contains(p)))
					{
						int imageIndex = 0;
						if(p is ExternalPage)
						{
							imageIndex = 1;
						}

						//int itemAdded = uiListBox1.Items.Add(p, p.Name, imageIndex);
                        ListViewItem item = uiListBox1.Items.Add(new ListViewItem() { Text = p.Name, Tag = p, ImageIndex = imageIndex });
						if (currentValue != null && currentValue.PageValue != null && currentValue.PageValue == p)
						{
							//uiListBox1.SelectedItem = uiListBox1.Items[itemAdded];
                            uiListBox1.FocusedItem = item;
						}
					}
				}
                uiListBox1.Sorting = SortOrder.Ascending;
                uiListBox1.Sort();
			}
		}

		public PageQueryEditorControl(IWindowsFormsEditorService edSvc)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.edSvc = edSvc;
			SetButtons();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageQueryEditorControl));
            this.btnNone = new System.Windows.Forms.Button();
            this.btnNewQuery = new System.Windows.Forms.Button();
            this.btnEditQuery = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnNone
            // 
            this.btnNone.BackColor = System.Drawing.Color.LightGray;
            this.btnNone.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNone.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNone.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNone.Location = new System.Drawing.Point(0, 40);
            this.btnNone.Name = "btnNone";
            this.btnNone.Size = new System.Drawing.Size(150, 20);
            this.btnNone.TabIndex = 2;
            this.btnNone.Text = "None";
            this.btnNone.UseVisualStyleBackColor = false;
            this.btnNone.Click += new System.EventHandler(this.btnNone_Click);
            // 
            // btnNewQuery
            // 
            this.btnNewQuery.BackColor = System.Drawing.Color.LightGray;
            this.btnNewQuery.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnNewQuery.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnNewQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNewQuery.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNewQuery.Location = new System.Drawing.Point(0, 20);
            this.btnNewQuery.Name = "btnNewQuery";
            this.btnNewQuery.Size = new System.Drawing.Size(150, 20);
            this.btnNewQuery.TabIndex = 1;
            this.btnNewQuery.Text = "New Query...";
            this.btnNewQuery.UseVisualStyleBackColor = false;
            this.btnNewQuery.Click += new System.EventHandler(this.btnNewQuery_Click);
            // 
            // btnEditQuery
            // 
            this.btnEditQuery.BackColor = System.Drawing.Color.LightGray;
            this.btnEditQuery.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditQuery.Enabled = false;
            this.btnEditQuery.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEditQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditQuery.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEditQuery.Location = new System.Drawing.Point(0, 0);
            this.btnEditQuery.Name = "btnEditQuery";
            this.btnEditQuery.Size = new System.Drawing.Size(150, 20);
            this.btnEditQuery.TabIndex = 0;
            this.btnEditQuery.Text = "Edit Query...";
            this.btnEditQuery.UseVisualStyleBackColor = false;
            this.btnEditQuery.Click += new System.EventHandler(this.btnEditQuery_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 60);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(150, 90);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.TabIndex = 3;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 200;
            // 
            // PageQueryEditorControl
            // 
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.btnNone);
            this.Controls.Add(this.btnNewQuery);
            this.Controls.Add(this.btnEditQuery);
            this.Name = "PageQueryEditorControl";
            this.ResumeLayout(false);

		}

		#endregion

		private void btnEditQuery_Click(object sender, EventArgs e)
		{
            if (currentValue.QueryValue != null)
            {
                currentValue.QueryValue.QueryExpression.ParentPackage = pageBeingEdited.ParentPackage;

                edSvc.CloseDropDown();

                IQueryDesigner frm = Launch.CreateQueryDesignerForm(pageBeingEdited, pageBeingEdited.ParentPackage, currentValue.QueryValue, QueryDesignerMode.Navigation);

                DialogResult dr = ((Form)frm).ShowDialog();
                if (dr == DialogResult.OK)
                {
                    currentValue.QueryValue = frm.QueryBeingEdited;
                }
            }
			//TODO: Launch query editor for existing Query - careful not to adjust currentValue if query editor is cancelled
		}

		private void btnNewQuery_Click(object sender, EventArgs e)
		{
			//currentValue.ValueType = PageQueryValue.PageQueryValueType.Query;
			//TODO: Launch query editor for new Query - careful not to adjust currentValue if query editor is cancelled
			Query q = pageBeingEdited.ParentPackage.CreateQuery();
            q.ActionIfTrue = new NavigationAction();
            q.ActionIfFalse = new NavigationAction();

            edSvc.CloseDropDown();

            IQueryDesigner frm = Launch.CreateQueryDesignerForm(pageBeingEdited, pageBeingEdited.ParentPackage, q, QueryDesignerMode.Navigation);

            DialogResult dr = ((Form)frm).ShowDialog();
			if (dr == DialogResult.OK)
			{
                currentValue.QueryValue = frm.QueryBeingEdited;
			}
		}

		private void btnNone_Click(object sender, EventArgs e)
		{
            if (currentValue != null && currentValue.QueryValue != null)
            {
                DialogResult dr = MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.YesNoQueryEditorControl.OverwriteQueryMessage"), Common.About.Title, MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    currentValue.SetNoValue();
                    edSvc.CloseDropDown();
                }
                else
                {
                    edSvc.CloseDropDown();
                }
            }
            else
            {
                currentValue.SetNoValue();
                edSvc.CloseDropDown();
            }
		}

        private void uiListBox1_MouseUp(object sender, MouseEventArgs e)
        {
            // This code has been refractored to prevent duplicate onChange events when the query is removed, and the
            // potential of the dropdown not been closed.
            DialogResult dr = DialogResult.Yes;

            if (currentValue != null && currentValue.QueryValue != null)
            {
                dr = MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").
                    GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.YesNoQueryEditorControl.OverwriteQueryMessage"),
                    Common.About.Title, MessageBoxButtons.YesNo);
            }

            if (dr == DialogResult.Yes)
            {
                if (uiListBox1.Items.Count > 0 && uiListBox1.FocusedItem != null)
                    currentValue.PageValue = uiListBox1.FocusedItem.Tag as InterviewPage;
                else if (currentValue != null && currentValue.QueryValue != null)
                    currentValue.SetNoValue();
            }

            edSvc.CloseDropDown();
        }
	}
}