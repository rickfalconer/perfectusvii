using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Data;
using Perfectus.Client.Studio.ExpressionLibrary.Converter;


namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for TextQuestionEditorControl.
	/// </summary>
	public class TextQuestionEditorControl : UserControl
	{
        protected Button btnEditText;
        private IContainer components;

        protected TextQuestionQueryValue currentValue = new TextQuestionQueryValue();
        protected IWindowsFormsEditorService edSvc;
		protected ImageList imageList1;
		protected Package package;	
		protected QuestionCollection questions = null;
		protected bool checkCircularRef = false;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		protected PackageItem itemBeingEdited = null;

		public TextQuestionQueryValue Value
		{
			get { return currentValue; }
			set
			{
				currentValue = value;
				if (currentValue == null)
				{
					currentValue = new TextQuestionQueryValue();
				}
			}
		}
		public Package Package
		{
			get { return package; }
			set 
			{
				package = value;
			}
		}	

		public QuestionCollection Questions
		{
			get { return questions; }
			set
			{
				questions = value;
				BindList();
			}
		}

		public bool CheckCircularRef
		{
			get
			{
				return checkCircularRef;
			}
			set
			{
				checkCircularRef = value;
			}
		}

		public PackageItem ItemBeingEdited
		{
			get
			{
				return itemBeingEdited;
			}
			set
			{
				itemBeingEdited = value;
			}
		}

		public TextQuestionEditorControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		public TextQuestionEditorControl(IWindowsFormsEditorService edSvc)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.edSvc = edSvc;
			BindList();
		}

		protected virtual void BindList()
		{
			uiListBox1.Items.Clear();
			if (questions != null)
			{
				//TODO:  Pretty sure this will never be used on a question, but if it is, need to make sure no circular references happen
				foreach (Question q in questions)
				{
					//uiListBox1.Items.Add(q, q.Name, 0);
                    uiListBox1.Items.Add(new ListViewItem() { Text = q.Name, Tag = q, ImageIndex = 0 });
				}
				uiListBox1.Sort();
			}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextQuestionEditorControl));
            this.btnEditText = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnEditText
            // 
            this.btnEditText.BackColor = System.Drawing.Color.LightGray;
            this.btnEditText.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditText.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnEditText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEditText.ImageIndex = 2;
            this.btnEditText.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnEditText.Location = new System.Drawing.Point(0, 0);
            this.btnEditText.Name = "btnEditText";
            this.btnEditText.Size = new System.Drawing.Size(150, 20);
            this.btnEditText.TabIndex = 0;
            this.btnEditText.Text = "Edit Text";
            this.btnEditText.UseVisualStyleBackColor = false;
            this.btnEditText.Click += new System.EventHandler(this.btnEditText_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 20);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(150, 130);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.uiListBox1.TabIndex = 1;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 200;
            // 
            // TextQuestionEditorControl
            // 
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.btnEditText);
            this.Name = "TextQuestionEditorControl";
            this.ResumeLayout(false);

		}

		#endregion

		protected void btnEditText_Click(object sender, EventArgs e)
		{

            Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery.StringBindingQueryForm frm = new Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery.StringBindingQueryForm();

            frm.Package = this.Package;
            frm.ItemBeingEdited = itemBeingEdited;

            //if we have a text question value already, set it
            if (currentValue != null)
            {
                frm.Val = currentValue;
            }

            DialogResult dr = frm.ShowDialog();

            if (dr == DialogResult.OK)
            {
                currentValue = frm.Val;
                currentValue.ParentPackage = package;
            }
            edSvc.CloseDropDown();
            frm.Dispose();
		}

        protected void uiListBox1_MouseUp(object sender, MouseEventArgs e)
        {
            QuestionSelected();
        }

		protected void QuestionSelected()
		{
			if (uiListBox1.Items.Count > 0 && uiListBox1.FocusedItem != null)
			{
				if(checkCircularRef)
				{
					// the isCircularRef method thinks its getting xml as made by the editor, so we need to make up the xml here.
                    string itemXml = String.Format("<text>{0}</text>", StringBinding.GetItemNodeXml((PackageItem)uiListBox1.FocusedItem.Tag, ""));
					bool isRef = StringBindingMod.IsCircularRefExist(itemXml,itemBeingEdited);
				
					if(isRef)
					{
						MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs.StringBindingEditorForm.CircularReferenceError"),Common.About.FormsTitle);					
						return;
					}
				}

                currentValue.QuestionValue = uiListBox1.FocusedItem.Tag as Question;
				edSvc.CloseDropDown();


			}
		}
	}
}