using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;


namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
    /// <summary>
    /// Summary description for TextQuestionEditorControl.
    /// </summary>
    public class DateQuestionEditorControl : UserControl
    {
        protected Button btnEditText;
        private IContainer components;

        protected TextQuestionQueryValue currentValue = new TextQuestionQueryValue();
        protected IWindowsFormsEditorService edSvc;
        protected ImageList imageList1;
        protected Package package;
        protected bool checkCircularRef = false;
        protected PackageItem itemBeingEdited = null;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;

        public TextQuestionQueryValue Value
        {
            get { return currentValue; }
            set
            {
                currentValue = value;
                if (currentValue == null)
                {
                    currentValue = new TextQuestionQueryValue();
                }
            }
        }
        public Package Package
        {
            get { return package; }
            set
            {
                package = value;
                BindList();
            }
        }

        QuestionCollection questions;
        public QuestionCollection Questions
        {
            get { return questions; }
            set
            {
                questions = value;
                BindList();
            }
        }


        public bool CheckCircularRef
        {
            get
            {
                return checkCircularRef;
            }
            set
            {
                checkCircularRef = value;
            }
        }

        public PackageItem ItemBeingEdited
        {
            get
            {
                return itemBeingEdited;
            }
            set
            {
                itemBeingEdited = value;
            }
        }

        public DateQuestionEditorControl()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
        }

        public DateQuestionEditorControl(IWindowsFormsEditorService edSvc)
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            this.edSvc = edSvc;
            BindList();
        }

        protected virtual void BindList()
        {
            uiListBox1.Items.Clear();

            if (Package == null)
                return;

            //TODO:  Pretty sure this will never be used on a question, but if it is, need to make sure no circular references happen
            foreach (Question q in Package.Questions)
            {
                if (q.DataType == PerfectusDataType.Date)
                    uiListBox1.Items.Add(new ListViewItem() { Text = q.Name, Tag = q, ImageIndex = 0 });
            }
            /*
            foreach (PFunction p in Package.Functions)
            {
                if (p.DataType == PerfectusDataType.Date)
                    uiListBox1.Items.Add(new ListViewItem() { Text = p.Name, Tag = p, ImageIndex = 0 });
            }
             */
            uiListBox1.Sorting =  SortOrder.Ascending;
            uiListBox1.Sort();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DateQuestionEditorControl));
            this.btnEditText = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // btnEditText
            // 
            this.btnEditText.BackColor = System.Drawing.Color.LightGray;
            resources.ApplyResources(this.btnEditText, "btnEditText");
            this.btnEditText.Name = "btnEditText";
            this.btnEditText.UseVisualStyleBackColor = false;
            this.btnEditText.Click += new System.EventHandler(this.btnEditText_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            resources.ApplyResources(this.uiListBox1, "uiListBox1");
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseUp);
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // DateQuestionEditorControl
            // 
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.btnEditText);
            this.Name = "DateQuestionEditorControl";
            this.ResumeLayout(false);

        }

        #endregion

        protected void btnEditText_Click(object sender, EventArgs e)
        {
            DateTime dateTime;
            if (currentValue.TextValue != null &&
                currentValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
                dateTime = DateTime.Parse(currentValue.TextValue);
            else
                dateTime = DateTime.Now;

            Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorConstantDatepicker
            frm = new Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorConstantDatepicker(dateTime);

            frm.ShowDialog();
            currentValue.TextValue = frm.dateTime.ToLongDateString();
            currentValue.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
            edSvc.CloseDropDown();
            frm.Dispose();
        }

        protected void uiListBox1_MouseUp(object sender, MouseEventArgs e)
        {
            QuestionSelected();
        }

        protected void QuestionSelected()
        {
            if (uiListBox1.FocusedItem != null)
            {
                if (checkCircularRef)
                {
                    // the isCircularRef method thinks its getting xml as made by the editor, so we need to make up the xml here.
                    string itemXml = String.Format("<text>{0}</text>", StringBinding.GetItemNodeXml((PackageItem)uiListBox1.FocusedItem.Tag, ""));
                    bool isRef = StringBindingMod.IsCircularRefExist(itemXml, itemBeingEdited);

                    if (isRef)
                    {
                        MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs.StringBindingEditorForm.CircularReferenceError"), Common.About.FormsTitle);
                        return;
                    }
                }

                currentValue.QuestionValue = uiListBox1.FocusedItem.Tag as Question;
                edSvc.CloseDropDown();
            }
        }
    }
}