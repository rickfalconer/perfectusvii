using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Input;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
    
    /// <summary>
	/// Summary description for ItemConstantPicker.
	/// </summary>
	public class ItemConstantPicker : UserControl
	{
		private bool wantCombo = false;
        private NoKeyUpUiCombo uiComboBox1;
		private ImageList imageList1;
		private IContainer components;
		private ComboboxItem nowListItem;
        private ComboboxItem constantListItem;
        private ComboboxItem yesListItem;
        private ComboboxItem noListItem;
        private ComboboxItem notAnsweredListItem;
        private ComboboxItem nullListItem;

        private Package package;
		private ErrorProvider errorProvider1;

		private PerfectusDataType dataType;
		private NoKeyUpTextBox txtFocusFix;
        private NumericUpDown uiNumericEditBox1;
        private DateTimePicker uiCalendarCombo1;
        private Button btnShowCalendar;

		public PerfectusDataType DataType
		{
			get { return dataType; }
			set
			{
				dataType = value;
				//ResetUI();
			}
		}

		public Package Package
		{
			get { return package; }
			set
			{
				package = value;
				//ResetUI();
			}
		}


		private PackageItem itemBeingEdited;

		public PackageItem ItemBeingEdited
		{
			get { return itemBeingEdited; }
			set
			{
				itemBeingEdited = value;
				//ResetUI();
			}
		}

		public ItemConstantPicker(Package package) : this()
		{
			this.package = package;
		}

		public ItemConstantPicker()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			string localNow = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.Now");
			string localYes = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.Yes");
			string localNo = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.No");
			string localNotAnswered = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.NotAnswered");
			string localNull = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.Null");

			nowListItem = new ComboboxItem(){ Text = localNow,Value = new PackageConstant(PerfectusDataType.Date, "DateTime.Now") };
            yesListItem = new ComboboxItem() { Text = localYes, Value = new PackageConstant(PerfectusDataType.YesNo, true) };
			noListItem = new ComboboxItem(){ Text = localNo, Value = new PackageConstant(PerfectusDataType.YesNo, false) };
            notAnsweredListItem = new ComboboxItem() { Text = localNotAnswered, Value = new PackageConstant(dataType, "Not Answered") };
			nullListItem = new ComboboxItem(){ Text = localNull, Value = new PackageConstant(dataType, "Null") };	
			constantListItem = new ComboboxItem();			

			// HACK:  Needs this time-less format to correctly start off on 'today'.
			uiCalendarCombo1.Value = DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy"));
			uiComboBox1.Focus();
		}

		public void ResetUI()
		{
			uiComboBox1.Items.Clear();

			if (package != null)
			{
				switch (dataType)
				{
					case PerfectusDataType.Date:
						ShowNotAnsweredListItem();		
						ShowNullListItem();	
						ShowNowListItem();	
						HideYesNoListItems();
						uiComboBox1.DropDownStyle = ComboBoxStyle.DropDown;
						uiComboBox1.Sorted = true;
						wantCombo = false;
						btnShowCalendar.Show();
						btnShowCalendar.Refresh();
						btnShowCalendar.BringToFront();
						uiNumericEditBox1.Hide();
						uiCalendarCombo1.Show();
						break;
					case PerfectusDataType.Text:
						ShowNotAnsweredListItem();
						ShowNullListItem();		
						HideNowListItem();
						HideYesNoListItems();
						btnShowCalendar.Hide();
						uiNumericEditBox1.Hide();
						uiComboBox1.DropDownStyle = ComboBoxStyle.DropDown;
                        uiComboBox1.Sorted = true;
						uiCalendarCombo1.Hide();
						wantCombo = true;
						break;
					case PerfectusDataType.Number:
						ShowNotAnsweredListItem();
						ShowNullListItem();		
						HideNowListItem();
						HideYesNoListItems();
						btnShowCalendar.Hide();
						uiCalendarCombo1.Hide();
						uiComboBox1.DropDownStyle = ComboBoxStyle.DropDown;
                        uiComboBox1.Sorted = true;
						wantCombo = true;
						//						uiNumericEditBox1.Show();
						break;
					case PerfectusDataType.YesNo:
						ShowNotAnsweredListItem();
						ShowNullListItem();		
						HideNowListItem();
						ShowYesNoListItems();
						uiComboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                        uiComboBox1.Sorted = true;
						wantCombo = false;
						btnShowCalendar.Hide();
						uiCalendarCombo1.Hide();
						uiNumericEditBox1.Hide();
						break;
					default:
						//throw new NotSupportedException("This control doesn't support that datatype");
						HideNotAnsweredListItem();
						HideNullListItem();
						HideNowListItem();
						HideYesNoListItems();
						btnShowCalendar.Hide();
						uiComboBox1.Hide();
						wantCombo = false;
						uiCalendarCombo1.Hide();
						txtFocusFix.Enabled = false;
						break;
				}

				PackageItem[] itemsToOffer;
				if (itemBeingEdited != null && itemBeingEdited is PFunction)
				{
					itemsToOffer = PerfectusTypeSystem.GetNonSelfReferentialItems((PFunction) ItemBeingEdited, dataType);
					HideNullListItem();
					HideNotAnsweredListItem();
				}
				else
				{
					itemsToOffer = PerfectusTypeSystem.GetCompatibleItems(dataType, package);
				}

				foreach (PackageItem pi in itemsToOffer)
				{
					//TODO: Check for circular references by walking dependencies, starting at itemToExclude
					if (pi != itemBeingEdited)
					{
						ComboboxItem newItem = new ComboboxItem() { Text = pi.Name, Value = pi };
						if (pi is Question)
						{
							//newItem.ImageIndex = 1;
						}
						else if (pi is DatePFunction)
						{
							//newItem.ImageIndex = 2;
						}
						else if (pi is ArithmeticPFunction)
						{
							//newItem.ImageIndex = 3;
						}

						uiComboBox1.Items.Add(newItem);
					}
				}

				uiComboBox1.Focus();
				SetValue();
			}
		}

        private void SetValue()
        {
            if (initialValue != null)
            {
                if (initialValue is PackageConstant)
                {
                    if (((PackageConstant)initialValue).Value is string)
                    {
                        string initVal = ((PackageConstant)initialValue).Value.ToString();

                        if (initVal == "Not Answered")
                        {
                            ShowNotAnsweredListItem();
                            uiComboBox1.SelectedItem = notAnsweredListItem;
                            return;
                        }
                        else if (initVal == "Null")
                        {
                            ShowNullListItem();
                            uiComboBox1.SelectedItem = nullListItem;
                            return;
                        }
                        else if (initVal == "DateTime.Now")
                        {
                            ShowNowListItem();
                            uiComboBox1.SelectedItem = nowListItem;
                            return;
                        }
                    }
                }

                switch (dataType)
                {
                    case PerfectusDataType.Date:
                        if (initialValue.DataType == PerfectusDataType.Date)
                        {
                            if (initialValue is PackageConstant)
                            {
                                SetDateConstant((DateTime)(((PackageConstant)initialValue).Value));
                                return;
                            }
                            else
                            {
                                foreach (ComboboxItem li in uiComboBox1.Items)
                                {
                                    if (li.Value == initialValue)
                                    {
                                        uiComboBox1.SelectedItem = li;
                                        return;
                                    }
                                }
                            }
                        }
                        break;
                    case PerfectusDataType.YesNo:
                        if (initialValue.DataType == PerfectusDataType.YesNo)
                        {
                            if (initialValue is PackageConstant)
                            {
                                bool b = (bool)((PackageConstant)initialValue).Value;
                                if (b)
                                {
                                    uiComboBox1.SelectedItem = yesListItem;
                                }
                                else
                                {
                                    uiComboBox1.SelectedItem = noListItem;
                                }
                                return;
                            }
                            else
                            {
                                foreach (ComboboxItem li in uiComboBox1.Items)
                                {
                                    if (((AnswerProvider)li.Value) == initialValue)
                                    {
                                        uiComboBox1.SelectedItem = li;
                                        return;
                                    }
                                }
                            }
                        }
                        break;
                    case PerfectusDataType.Text:
                        if (initialValue.DataType == PerfectusDataType.Text)
                        {
                            if (initialValue is PackageConstant)
                            {
                                string s = (string)((PackageConstant)initialValue).Value;
                                uiComboBox1.Text = s;

                            }
                            else
                            {
                                foreach (ComboboxItem li in uiComboBox1.Items)
                                {
                                    if (((AnswerProvider)li.Value) == initialValue)
                                    {
                                        uiComboBox1.SelectedItem = li;
                                        return;
                                    }
                                }
                            }
                        }
                        break;
                    case PerfectusDataType.Number:
                        if (initialValue.DataType == PerfectusDataType.Number)
                        {
                            if (initialValue is PackageConstant)
                            {
                                double d = Convert.ToDouble(((PackageConstant)initialValue).Value);
                                uiComboBox1.Text = d.ToString();
                            }
                            else
                            {
                                foreach (ComboboxItem li in uiComboBox1.Items)
                                {
                                    if (((AnswerProvider)li.Value) == initialValue)
                                    {
                                        uiComboBox1.SelectedItem = li;
                                        return;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                uiComboBox1.Text = string.Empty;
            }
        }

		private void ShowNowListItem()
		{
			if (! uiComboBox1.Items.Contains(nowListItem))
			{
				uiComboBox1.Items.Add(nowListItem);
			}
		}

		private void HideNowListItem()
		{
			if (uiComboBox1.Items.Contains(nowListItem))
			{
				uiComboBox1.Items.Remove(nowListItem);
			}
		}

		private void ShowNotAnsweredListItem()
		{
			if (! uiComboBox1.Items.Contains(notAnsweredListItem))
			{
				uiComboBox1.Items.Add(notAnsweredListItem);
			}
		}

		private void HideNotAnsweredListItem()
		{
			if (uiComboBox1.Items.Contains(notAnsweredListItem))
			{
				uiComboBox1.Items.Remove(notAnsweredListItem);
			}
		}

		private void ShowNullListItem()
		{
			if (! uiComboBox1.Items.Contains(nullListItem))
			{
				uiComboBox1.Items.Add(nullListItem);
			}
		}

		private void HideNullListItem()
		{
			if (uiComboBox1.Items.Contains(nullListItem))
			{
				uiComboBox1.Items.Remove(nullListItem);
			}
		}


		private void ShowYesNoListItems()
		{
			if (! uiComboBox1.Items.Contains(yesListItem))
			{
				uiComboBox1.Items.Add(yesListItem);
			}
			if (! uiComboBox1.Items.Contains(noListItem))
			{
				uiComboBox1.Items.Add(noListItem);
			}
		}

		private void HideYesNoListItems()
		{
			if (uiComboBox1.Items.Contains(yesListItem))
			{
				uiComboBox1.Items.Remove(yesListItem);
			}
			if (uiComboBox1.Items.Contains(noListItem))
			{
				uiComboBox1.Items.Remove(noListItem);
			}
		}

		private AnswerProvider initialValue;

		public AnswerProvider Value
		{
			get { return GetValue(); }
			set
			{
				initialValue = value;
				//ResetUI();
			}
		}

		private AnswerProvider GetValue()
		{
			switch (dataType)
			{
				case PerfectusDataType.Date:
					{
						if (uiComboBox1.SelectedItem != null)
						{
                            ComboboxItem item = (ComboboxItem)uiComboBox1.SelectedItem;
							return (AnswerProvider) (item.Value);
						}
						else
						{
							return null;
						}
					}
				case PerfectusDataType.YesNo:
                    if (uiComboBox1.SelectedItem != null)
					{
                        ComboboxItem item = (ComboboxItem)uiComboBox1.SelectedItem;
						return (AnswerProvider) item.Value;
					}
					else
					{
						return null;
					}
				case PerfectusDataType.Text:
                    if (uiComboBox1.SelectedItem != null)
					{
                        ComboboxItem item = (ComboboxItem)uiComboBox1.SelectedItem;
                        return (AnswerProvider)item.Value;
					}
					else
					{
						return new PackageConstant(PerfectusDataType.Text, uiComboBox1.Text);
					}
				case PerfectusDataType.Number:
                    if (uiComboBox1.SelectedItem != null)
					{
                        ComboboxItem item = (ComboboxItem)uiComboBox1.SelectedItem;
                        return (AnswerProvider)item.Value;
                    }
					else
					{
						try
						{
							return (new PackageConstant(PerfectusDataType.Number, double.Parse(uiComboBox1.Text, NumberStyles.Number, CultureInfo.CurrentUICulture.NumberFormat)));
						}
						catch
						{
							uiComboBox1.Text = string.Empty;
							return null;
						}
					}

				default:
					return null;
			}

		}

		public string GetValueString()
		{
			AnswerProvider v = GetValue();
			if (v is PackageConstant)
			{
				return ((PackageConstant) v).ToString();
			}
			else
			{
				return v.Name;
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemConstantPicker));
            this.uiComboBox1 = new Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.NoKeyUpUiCombo();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtFocusFix = new Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.NoKeyUpTextBox();
            this.uiNumericEditBox1 = new System.Windows.Forms.NumericUpDown();
            this.uiCalendarCombo1 = new System.Windows.Forms.DateTimePicker();
            this.btnShowCalendar = new System.Windows.Forms.Button();
            //((System.ComponentModel.ISupportInitialize)(this.uiComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNumericEditBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiComboBox1
            // 
            this.uiComboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uiComboBox1.Location = new System.Drawing.Point(0, 0);
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.Size = new System.Drawing.Size(216, 20);
            this.uiComboBox1.TabIndex = 0;
            this.uiComboBox1.TabStop = false;
            this.uiComboBox1.SelectedIndexChanged += new System.EventHandler(this.uiComboBox1_SelectedIndexChanged);
            this.uiComboBox1.Enter += new System.EventHandler(this.uiComboBox1_Enter);
            this.uiComboBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uiComboBox1_KeyUp);
            this.uiComboBox1.Leave += new System.EventHandler(this.uiComboBox1_Leave);
            this.uiComboBox1.Validating += new System.ComponentModel.CancelEventHandler(this.uiComboBox1_Validating);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // txtFocusFix
            // 
            this.txtFocusFix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFocusFix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFocusFix.Location = new System.Drawing.Point(2, 5);
            this.txtFocusFix.Name = "txtFocusFix";
            this.txtFocusFix.Size = new System.Drawing.Size(172, 13);
            this.txtFocusFix.TabIndex = 4;
            this.txtFocusFix.TextChanged += new System.EventHandler(this.txtFocusFix_TextChanged);
            // 
            // uiNumericEditBox1
            // 
            this.uiNumericEditBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiNumericEditBox1.Location = new System.Drawing.Point(179, 0);
            this.uiNumericEditBox1.Margin = new System.Windows.Forms.Padding(0);
            this.uiNumericEditBox1.Name = "uiNumericEditBox1";
            this.uiNumericEditBox1.Size = new System.Drawing.Size(19, 20);
            this.uiNumericEditBox1.TabIndex = 5;
            this.uiNumericEditBox1.TabStop = false;
            this.uiNumericEditBox1.ValueChanged += new System.EventHandler(this.uiNumericEditBox1_ValueChanged);
            // 
            // uiCalendarCombo1
            // 
            this.uiCalendarCombo1.Location = new System.Drawing.Point(0, 0);
            this.uiCalendarCombo1.Name = "uiCalendarCombo1";
            this.uiCalendarCombo1.Size = new System.Drawing.Size(216, 20);
            this.uiCalendarCombo1.TabIndex = 6;
            this.uiCalendarCombo1.ValueChanged += new System.EventHandler(this.uiCalendarCombo1_ValueChanged);
            // 
            // btnShowCalendar
            // 
            this.btnShowCalendar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowCalendar.Image = ((System.Drawing.Image)(resources.GetObject("btnShowCalendar.Image")));
            this.btnShowCalendar.Location = new System.Drawing.Point(174, -3);
            this.btnShowCalendar.Margin = new System.Windows.Forms.Padding(0);
            this.btnShowCalendar.Name = "btnShowCalendar";
            this.btnShowCalendar.Size = new System.Drawing.Size(24, 23);
            this.btnShowCalendar.TabIndex = 7;
            this.btnShowCalendar.UseVisualStyleBackColor = true;
            this.btnShowCalendar.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // ItemConstantPicker
            // 
            this.Controls.Add(this.btnShowCalendar);
            this.Controls.Add(this.uiNumericEditBox1);
            this.Controls.Add(this.txtFocusFix);
            this.Controls.Add(this.uiComboBox1);
            this.Controls.Add(this.uiCalendarCombo1);
            this.Name = "ItemConstantPicker";
            this.Size = new System.Drawing.Size(216, 21);
            //((System.ComponentModel.ISupportInitialize)(this.uiComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiNumericEditBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

        private void uiButton1_Click(object sender, EventArgs e)
        {
            //TODO see if this is needed
            //this.uiCalendarCombo1.Open_dtp();
        }

        private void uiCalendarCombo1_ValueChanged(object sender, EventArgs e)
        {
            DateTime v = uiCalendarCombo1.Value;
            SetDateConstant(v);
        }

		private void SetDateConstant(DateTime v)
		{
			constantListItem.Text = v.ToShortDateString();
			constantListItem.Value = new PackageConstant(PerfectusDataType.Date, v);
			if (! uiComboBox1.Items.Contains(constantListItem))
			{
				uiComboBox1.Items.Add(constantListItem);
			}
            uiComboBox1.SelectedItem = constantListItem;
			uiComboBox1.Text = constantListItem.Text;
		}

        private void uiComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (uiComboBox1.SelectedItem != constantListItem && uiComboBox1.Items.Contains(constantListItem))
            {
                uiComboBox1.Items.Remove(constantListItem);
            }

            if (txtFocusFix.Visible)
            {
                txtFocusFix.Text = uiComboBox1.Text;
            }
        }

        private void uiNumericEditBox1_ValueChanged(object sender, EventArgs e)
        {
            uiComboBox1.Text = uiNumericEditBox1.Value.ToString();
            uiComboBox1.Refresh();
        }

        private void uiComboBox1_Validating(object sender, CancelEventArgs e)
        {
            errorProvider1.SetError(uiCalendarCombo1, string.Empty);
            if (dataType == PerfectusDataType.Number)
            {
                if (uiComboBox1.SelectedItem != null)
                {
                    ComboboxItem item = (ComboboxItem)uiComboBox1.SelectedItem;
                    if (!(item.Value is PackageItem))
                    {
                        double d;
                        if (!double.TryParse(uiComboBox1.Text, NumberStyles.Number, CultureInfo.CurrentUICulture.NumberFormat, out d))
                        {
                            errorProvider1.SetError(uiComboBox1, ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Controls.ItemConstantPicker.InvalidNumberError"));
                            e.Cancel = true;
                        }
                        else
                        {
                            errorProvider1.SetError(uiCalendarCombo1, string.Empty);
                            e.Cancel = false;
                        }
                    }
                }
            }
            else
            {
                errorProvider1.SetError(uiCalendarCombo1, string.Empty);
                e.Cancel = false;
            }

        }

        //private void ItemConstantPicker_Enter(object sender, EventArgs e)
        //{
        //}

        private void uiComboBox1_Leave(object sender, EventArgs e)
        {
            Debug.WriteLine("uiComboBox1_Leave");
        }



        private void uiComboBox1_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            Debug.WriteLine("uiComboBox1.KeyUp");
        }

        //private void ItemConstantPicker_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
        //{
        //    Debug.WriteLine("ItemConstantPicker_KeyUp");
        //}

		public void FixDropdown()
		{
			if (wantCombo)
			{
				//uiComboBox1.DropDownStyle = ComboBoxStyle.DropDown;
				//uiComboBox1.Refresh();

				//HACK!
				txtFocusFix.Text = uiComboBox1.Text;
				txtFocusFix.Visible = true;
				txtFocusFix.Focus();
				txtFocusFix.SelectAll();
				
			}
			else
			{
				txtFocusFix.Visible = false;
			}
		}

        private void uiComboBox1_Enter(object sender, System.EventArgs e)
        {
            Debug.WriteLine("uiComboBox1_Enter");
        }

		private void txtFocusFix_TextChanged(object sender, System.EventArgs e)
		{
					
			uiComboBox1.Text = txtFocusFix.Text;
			//uiComboBox1.SelectedIndex = -1;
        }

	}
}