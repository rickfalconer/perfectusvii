using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
    /// <summary>
    /// Summary description for YesNoQueryEditorControl.
    /// </summary>
    public class YesNoQueryEditorControl : UserControl
    {
        private Button btnFalse;
        private Button btnTrue;
        private Button btnNewQuery;
        private Button btnEditQuery;
        private PackageItem itemBeingEdited;

        public Package PackageBeingEdited
        {
            get { return packageBeingEdited; }
            set { packageBeingEdited = value; }
        }

        private Package packageBeingEdited;

        public PackageItem ItemBeingEdited
        {
            get { return itemBeingEdited; }
            set { itemBeingEdited = value; }
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        private YesNoQueryValue currentValue = new YesNoQueryValue();

        private IWindowsFormsEditorService edSvc;

        public YesNoQueryEditorControl(IWindowsFormsEditorService edSvc)
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
            this.edSvc = edSvc;
        }

        public YesNoQueryValue Value
        {
            get { return currentValue; }
            set
            {
                currentValue = value;
                if (currentValue == null)
                {
                    currentValue = new YesNoQueryValue();
                }
                SetButtons();
            }
        }

        private void SetButtons()
        {
            btnEditQuery.Enabled = (currentValue != null && currentValue.QueryValue != null && currentValue.ValueType == YesNoQueryValue.YesNoQueryValueType.Query);
            btnNewQuery.Enabled = currentValue == null || currentValue.ValueType == YesNoQueryValue.YesNoQueryValueType.NoValue || currentValue.ValueType == YesNoQueryValue.YesNoQueryValueType.YesNo;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(YesNoQueryEditorControl));
            this.btnFalse = new System.Windows.Forms.Button();
            this.btnTrue = new System.Windows.Forms.Button();
            this.btnNewQuery = new System.Windows.Forms.Button();
            this.btnEditQuery = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFalse
            // 
            this.btnFalse.AccessibleDescription = resources.GetString("btnFalse.AccessibleDescription");
            this.btnFalse.AccessibleName = resources.GetString("btnFalse.AccessibleName");
            this.btnFalse.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnFalse.Anchor")));
            this.btnFalse.BackColor = System.Drawing.Color.LightGray;
            this.btnFalse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFalse.BackgroundImage")));
            this.btnFalse.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnFalse.Dock")));
            this.btnFalse.Enabled = ((bool)(resources.GetObject("btnFalse.Enabled")));
            this.btnFalse.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnFalse.FlatStyle")));
            this.btnFalse.Font = ((System.Drawing.Font)(resources.GetObject("btnFalse.Font")));
            this.btnFalse.Image = ((System.Drawing.Image)(resources.GetObject("btnFalse.Image")));
            this.btnFalse.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnFalse.ImageAlign")));
            this.btnFalse.ImageIndex = ((int)(resources.GetObject("btnFalse.ImageIndex")));
            this.btnFalse.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnFalse.ImeMode")));
            this.btnFalse.Location = ((System.Drawing.Point)(resources.GetObject("btnFalse.Location")));
            this.btnFalse.Name = "btnFalse";
            this.btnFalse.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnFalse.RightToLeft")));
            this.btnFalse.Size = ((System.Drawing.Size)(resources.GetObject("btnFalse.Size")));
            this.btnFalse.TabIndex = ((int)(resources.GetObject("btnFalse.TabIndex")));
            this.btnFalse.Text = resources.GetString("btnFalse.Text");
            this.btnFalse.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnFalse.TextAlign")));
            this.btnFalse.Visible = ((bool)(resources.GetObject("btnFalse.Visible")));
            this.btnFalse.Click += new System.EventHandler(this.btnFalse_Click);
            // 
            // btnTrue
            // 
            this.btnTrue.AccessibleDescription = resources.GetString("btnTrue.AccessibleDescription");
            this.btnTrue.AccessibleName = resources.GetString("btnTrue.AccessibleName");
            this.btnTrue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnTrue.Anchor")));
            this.btnTrue.BackColor = System.Drawing.Color.LightGray;
            this.btnTrue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTrue.BackgroundImage")));
            this.btnTrue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnTrue.Dock")));
            this.btnTrue.Enabled = ((bool)(resources.GetObject("btnTrue.Enabled")));
            this.btnTrue.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnTrue.FlatStyle")));
            this.btnTrue.Font = ((System.Drawing.Font)(resources.GetObject("btnTrue.Font")));
            this.btnTrue.Image = ((System.Drawing.Image)(resources.GetObject("btnTrue.Image")));
            this.btnTrue.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnTrue.ImageAlign")));
            this.btnTrue.ImageIndex = ((int)(resources.GetObject("btnTrue.ImageIndex")));
            this.btnTrue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnTrue.ImeMode")));
            this.btnTrue.Location = ((System.Drawing.Point)(resources.GetObject("btnTrue.Location")));
            this.btnTrue.Name = "btnTrue";
            this.btnTrue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnTrue.RightToLeft")));
            this.btnTrue.Size = ((System.Drawing.Size)(resources.GetObject("btnTrue.Size")));
            this.btnTrue.TabIndex = ((int)(resources.GetObject("btnTrue.TabIndex")));
            this.btnTrue.Text = resources.GetString("btnTrue.Text");
            this.btnTrue.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnTrue.TextAlign")));
            this.btnTrue.Visible = ((bool)(resources.GetObject("btnTrue.Visible")));
            this.btnTrue.Click += new System.EventHandler(this.btnTrue_Click);
            // 
            // btnNewQuery
            // 
            this.btnNewQuery.AccessibleDescription = resources.GetString("btnNewQuery.AccessibleDescription");
            this.btnNewQuery.AccessibleName = resources.GetString("btnNewQuery.AccessibleName");
            this.btnNewQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnNewQuery.Anchor")));
            this.btnNewQuery.BackColor = System.Drawing.Color.LightGray;
            this.btnNewQuery.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewQuery.BackgroundImage")));
            this.btnNewQuery.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnNewQuery.Dock")));
            this.btnNewQuery.Enabled = ((bool)(resources.GetObject("btnNewQuery.Enabled")));
            this.btnNewQuery.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnNewQuery.FlatStyle")));
            this.btnNewQuery.Font = ((System.Drawing.Font)(resources.GetObject("btnNewQuery.Font")));
            this.btnNewQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnNewQuery.Image")));
            this.btnNewQuery.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNewQuery.ImageAlign")));
            this.btnNewQuery.ImageIndex = ((int)(resources.GetObject("btnNewQuery.ImageIndex")));
            this.btnNewQuery.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnNewQuery.ImeMode")));
            this.btnNewQuery.Location = ((System.Drawing.Point)(resources.GetObject("btnNewQuery.Location")));
            this.btnNewQuery.Name = "btnNewQuery";
            this.btnNewQuery.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnNewQuery.RightToLeft")));
            this.btnNewQuery.Size = ((System.Drawing.Size)(resources.GetObject("btnNewQuery.Size")));
            this.btnNewQuery.TabIndex = ((int)(resources.GetObject("btnNewQuery.TabIndex")));
            this.btnNewQuery.Text = resources.GetString("btnNewQuery.Text");
            this.btnNewQuery.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnNewQuery.TextAlign")));
            this.btnNewQuery.Visible = ((bool)(resources.GetObject("btnNewQuery.Visible")));
            this.btnNewQuery.Click += new System.EventHandler(this.btnNewQuery_Click);
            // 
            // btnEditQuery
            // 
            this.btnEditQuery.AccessibleDescription = resources.GetString("btnEditQuery.AccessibleDescription");
            this.btnEditQuery.AccessibleName = resources.GetString("btnEditQuery.AccessibleName");
            this.btnEditQuery.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnEditQuery.Anchor")));
            this.btnEditQuery.BackColor = System.Drawing.Color.LightGray;
            this.btnEditQuery.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEditQuery.BackgroundImage")));
            this.btnEditQuery.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnEditQuery.Dock")));
            this.btnEditQuery.Enabled = ((bool)(resources.GetObject("btnEditQuery.Enabled")));
            this.btnEditQuery.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnEditQuery.FlatStyle")));
            this.btnEditQuery.Font = ((System.Drawing.Font)(resources.GetObject("btnEditQuery.Font")));
            this.btnEditQuery.Image = ((System.Drawing.Image)(resources.GetObject("btnEditQuery.Image")));
            this.btnEditQuery.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditQuery.ImageAlign")));
            this.btnEditQuery.ImageIndex = ((int)(resources.GetObject("btnEditQuery.ImageIndex")));
            this.btnEditQuery.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnEditQuery.ImeMode")));
            this.btnEditQuery.Location = ((System.Drawing.Point)(resources.GetObject("btnEditQuery.Location")));
            this.btnEditQuery.Name = "btnEditQuery";
            this.btnEditQuery.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnEditQuery.RightToLeft")));
            this.btnEditQuery.Size = ((System.Drawing.Size)(resources.GetObject("btnEditQuery.Size")));
            this.btnEditQuery.TabIndex = ((int)(resources.GetObject("btnEditQuery.TabIndex")));
            this.btnEditQuery.Text = resources.GetString("btnEditQuery.Text");
            this.btnEditQuery.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditQuery.TextAlign")));
            this.btnEditQuery.Visible = ((bool)(resources.GetObject("btnEditQuery.Visible")));
            this.btnEditQuery.Click += new System.EventHandler(this.btnEditQuery_Click);
            // 
            // YesNoQueryEditorControl
            // 
            this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
            this.AccessibleName = resources.GetString("$this.AccessibleName");
            this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
            this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
            this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.btnFalse);
            this.Controls.Add(this.btnTrue);
            this.Controls.Add(this.btnNewQuery);
            this.Controls.Add(this.btnEditQuery);
            this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
            this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
            this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
            this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
            this.Name = "YesNoQueryEditorControl";
            this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
            this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
            this.ResumeLayout(false);

        }

        #endregion

        private void btnTrue_Click(object sender, EventArgs e)
        {
            HandleYesNoClick(true);
        }

        private void btnFalse_Click(object sender, EventArgs e)
        {
            HandleYesNoClick(false);
        }

        private void HandleYesNoClick(bool yesNo)
        {
            if (currentValue != null && currentValue.QueryValue != null && currentValue.ValueType == YesNoQueryValue.YesNoQueryValueType.Query)
            {
                DialogResult dr = MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.YesNoQueryEditorControl.OverwriteQueryMessage"), Common.About.Title, MessageBoxButtons.YesNo);

                if (dr == DialogResult.Yes)
                {
                    currentValue.YesNoValue = yesNo;
                }

                edSvc.CloseDropDown();
            }
            else
            {
                currentValue.YesNoValue = yesNo;
                edSvc.CloseDropDown();
            }
        }

        private void btnEditQuery_Click(object sender, EventArgs e)
        {
            Query origQuery = currentValue.QueryValue;
            YesNoQueryValue.YesNoQueryValueType origType = currentValue.ValueType;

            Package p = null;
            if (itemBeingEdited == null && packageBeingEdited != null)
                p = packageBeingEdited;
            else
                if (itemBeingEdited != null)
                {
                    p = itemBeingEdited.ParentPackage;
                }

            currentValue.QueryValue.QueryExpression.ParentPackage = p;

            edSvc.CloseDropDown();

            IQueryDesigner frm = Launch.CreateQueryDesignerForm(itemBeingEdited, p, origQuery, QueryDesignerMode.YesNo);
            DialogResult dr = edSvc.ShowDialog((Form)frm);

            if (dr == DialogResult.OK)
            {
                currentValue.QueryValue = frm.QueryBeingEdited;
            }
            else
            {
                currentValue.QueryValue = origQuery;
                currentValue.ValueType = origType;
            }
        }

        private void btnNewQuery_Click(object sender, EventArgs e)
        {
            bool origValue = currentValue.YesNoValue;
            YesNoQueryValue.YesNoQueryValueType origType = currentValue.ValueType;

            currentValue.ValueType = YesNoQueryValue.YesNoQueryValueType.Query;
            Package p;
            if (itemBeingEdited != null)
            {
                p = itemBeingEdited.ParentPackage;
            }
            else
            {
                p = packageBeingEdited;
            }
            Query q = p.CreateQuery();
            currentValue.QueryValue = q;

            edSvc.CloseDropDown();

            IQueryDesigner frm = Launch.CreateQueryDesignerForm(itemBeingEdited, p, q, QueryDesignerMode.YesNo);
            DialogResult dr = edSvc.ShowDialog((Form)frm);

            if (dr == DialogResult.OK)
            {
                currentValue.QueryValue = frm.QueryBeingEdited;
            }
            else
            {
                currentValue.QueryValue = null;
                currentValue.ValueType = origType;
                currentValue.YesNoValue = origValue;
            }
        }
    }
}