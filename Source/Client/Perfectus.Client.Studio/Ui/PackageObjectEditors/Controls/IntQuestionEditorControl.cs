using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;


namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for IntQuestionEditorControl.
	/// </summary>
	public class IntQuestionEditorControl : UserControl
	{
		private System.ComponentModel.IContainer components;

		protected IntQuestionValue currentValue = new IntQuestionValue();
		protected IWindowsFormsEditorService edSvc;
		protected ImageList imageList1;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;

		public IntQuestionValue Value
		{
			get { return currentValue; }
			set
			{
				currentValue = value;
				if (currentValue == null)
				{
					currentValue = new IntQuestionValue();
				}
			}
		}


		protected QuestionCollection questions = null;

		public QuestionCollection Questions
		{
			get { return questions; }
			set
			{
				questions = value;
				BindList();
			}
		}


		public IntQuestionEditorControl(IWindowsFormsEditorService edSvc)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			this.edSvc = edSvc;
			BindList();
		}

		protected virtual void BindList()
		{
			uiListBox1.Items.Clear();
			if (questions != null)
			{
				//TODO:  Pretty sure this will never be used on a question, but if it is, need to make sure no circular references happen
				foreach (Question q in questions)
				{
					if(q.DataType == PerfectusDataType.Number)
					{
                        uiListBox1.Items.Add(new ListViewItem() { Text = q.Name, Tag = q, ImageIndex = 0 });
					}
				}
                uiListBox1.Sorting = SortOrder.Ascending;
                uiListBox1.Sort();
			}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntQuestionEditorControl));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 0);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(150, 150);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.TabIndex = 0;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 400;
            // 
            // IntQuestionEditorControl
            // 
            this.Controls.Add(this.uiListBox1);
            this.Name = "IntQuestionEditorControl";
            this.ResumeLayout(false);

		}

		#endregion

        protected void uiListBox1_MouseUp(object sender, MouseEventArgs e)
        {
            QuestionSelected();
        }

		protected void QuestionSelected()
		{
			if (uiListBox1.FocusedItem != null)
			{
                currentValue.QuestionValue = uiListBox1.FocusedItem.Tag as Question;
				edSvc.CloseDropDown();
			}
		}
	}
}