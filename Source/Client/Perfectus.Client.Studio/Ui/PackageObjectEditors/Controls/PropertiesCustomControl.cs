using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for PropertiesCustomControl.
	/// </summary>
	public class PropertiesCustomControl : System.Windows.Forms.UserControl
	{
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblQuestionValue;
		private System.Windows.Forms.RadioButton rbtTextValue;
		private System.Windows.Forms.RadioButton rbtQuestionValue;
		private System.Windows.Forms.ComboBox cbxQuestionValue;
		private System.Windows.Forms.TextBox txtTextValue;
		private System.Windows.Forms.Label lblTextValue;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Button butDelete;
		private System.Windows.Forms.Button butModify;
		private System.Windows.Forms.ComboBox cbxType;
		private System.Windows.Forms.Label lblName;
		private System.Windows.Forms.Label lblType;
		private Package package = null;
		WordDocumentPropertyCollection documentProperties = new WordDocumentPropertyCollection();
		private System.Windows.Forms.ListView listView1;
		private System.Windows.Forms.ColumnHeader colName;
		private System.Windows.Forms.ColumnHeader colValue;
		private System.Windows.Forms.ColumnHeader colType;
		private System.Windows.Forms.ColumnHeader colValueType;
		private System.Windows.Forms.Button butAdd;
		private System.Windows.Forms.Panel pnlValueHolder;

		public WordDocumentPropertyCollection DocumentProperties
		{
			get 
			{ 				
				return documentProperties;
			}
			set 
			{ 
				if(package == null)
				{
					throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.PackageNotSet"));
				}

				documentProperties = value;
				Init();
			}
		}

		public Package PackageBeingEdited
		{
            get { return package; }
            set {package = value;}
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public PropertiesCustomControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PropertiesCustomControl));
            this.lblQuestionValue = new System.Windows.Forms.Label();
            this.pnlValueHolder = new System.Windows.Forms.Panel();
            this.rbtTextValue = new System.Windows.Forms.RadioButton();
            this.rbtQuestionValue = new System.Windows.Forms.RadioButton();
            this.cbxQuestionValue = new System.Windows.Forms.ComboBox();
            this.txtTextValue = new System.Windows.Forms.TextBox();
            this.lblTextValue = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.butDelete = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValueType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.butModify = new System.Windows.Forms.Button();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.butAdd = new System.Windows.Forms.Button();
            this.pnlValueHolder.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblQuestionValue
            // 
            resources.ApplyResources(this.lblQuestionValue, "lblQuestionValue");
            this.lblQuestionValue.Name = "lblQuestionValue";
            // 
            // pnlValueHolder
            // 
            resources.ApplyResources(this.pnlValueHolder, "pnlValueHolder");
            this.pnlValueHolder.Controls.Add(this.rbtTextValue);
            this.pnlValueHolder.Controls.Add(this.rbtQuestionValue);
            this.pnlValueHolder.Controls.Add(this.cbxQuestionValue);
            this.pnlValueHolder.Controls.Add(this.txtTextValue);
            this.pnlValueHolder.Name = "pnlValueHolder";
            // 
            // rbtTextValue
            // 
            resources.ApplyResources(this.rbtTextValue, "rbtTextValue");
            this.rbtTextValue.Checked = true;
            this.rbtTextValue.Name = "rbtTextValue";
            this.rbtTextValue.TabStop = true;
            this.rbtTextValue.CheckedChanged += new System.EventHandler(this.rbtTextValue_CheckedChanged);
            // 
            // rbtQuestionValue
            // 
            resources.ApplyResources(this.rbtQuestionValue, "rbtQuestionValue");
            this.rbtQuestionValue.Name = "rbtQuestionValue";
            // 
            // cbxQuestionValue
            // 
            resources.ApplyResources(this.cbxQuestionValue, "cbxQuestionValue");
            this.cbxQuestionValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxQuestionValue.Name = "cbxQuestionValue";
            // 
            // txtTextValue
            // 
            resources.ApplyResources(this.txtTextValue, "txtTextValue");
            this.txtTextValue.Name = "txtTextValue";
            this.txtTextValue.TextChanged += new System.EventHandler(this.txtTextValue_TextChanged);
            // 
            // lblTextValue
            // 
            resources.ApplyResources(this.lblTextValue, "lblTextValue");
            this.lblTextValue.Name = "lblTextValue";
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // butDelete
            // 
            resources.ApplyResources(this.butDelete, "butDelete");
            this.butDelete.Name = "butDelete";
            this.butDelete.Click += new System.EventHandler(this.butDelete_Click);
            // 
            // listView1
            // 
            resources.ApplyResources(this.listView1, "listView1");
            this.listView1.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colValue,
            this.colType,
            this.colValueType});
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // colName
            // 
            resources.ApplyResources(this.colName, "colName");
            // 
            // colValue
            // 
            resources.ApplyResources(this.colValue, "colValue");
            // 
            // colType
            // 
            resources.ApplyResources(this.colType, "colType");
            // 
            // colValueType
            // 
            resources.ApplyResources(this.colValueType, "colValueType");
            // 
            // butModify
            // 
            resources.ApplyResources(this.butModify, "butModify");
            this.butModify.Name = "butModify";
            this.butModify.Click += new System.EventHandler(this.butModify_Click);
            // 
            // cbxType
            // 
            resources.ApplyResources(this.cbxType, "cbxType");
            this.cbxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxType.Name = "cbxType";
            this.cbxType.SelectedIndexChanged += new System.EventHandler(this.cbxType_SelectedIndexChanged);
            // 
            // lblName
            // 
            resources.ApplyResources(this.lblName, "lblName");
            this.lblName.Name = "lblName";
            // 
            // lblType
            // 
            resources.ApplyResources(this.lblType, "lblType");
            this.lblType.Name = "lblType";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // butAdd
            // 
            resources.ApplyResources(this.butAdd, "butAdd");
            this.butAdd.Name = "butAdd";
            this.butAdd.Click += new System.EventHandler(this.butAdd_Click);
            // 
            // PropertiesCustomControl
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.butDelete);
            this.Controls.Add(this.lblQuestionValue);
            this.Controls.Add(this.pnlValueHolder);
            this.Controls.Add(this.lblTextValue);
            this.Controls.Add(this.butAdd);
            this.Controls.Add(this.butModify);
            this.Controls.Add(this.cbxType);
            this.Controls.Add(this.lblType);
            this.Name = "PropertiesCustomControl";
            this.pnlValueHolder.ResumeLayout(false);
            this.pnlValueHolder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

		private void Init()
		{
			PopulateTypeList();
			PopulateQuestionCombo();
			PopulatePropertiesList();
			CalculateButtonState();
		}

		private void PopulateTypeList()
		{
			this.cbxType.DataSource = System.Enum.GetValues(typeof(WordDocumentProperty.WordDocumentCustomPropertyType));
		}

        private void PopulateQuestionCombo()
        {
            this.cbxQuestionValue.Items.Clear();
            WordDocumentProperty.WordDocumentCustomPropertyType selectedType = (WordDocumentProperty.WordDocumentCustomPropertyType)Enum.Parse(typeof(WordDocumentProperty.WordDocumentCustomPropertyType), this.cbxType.SelectedValue.ToString(), true);

            // disable the question selection for data type DATE
            rbtQuestionValue.Checked = false;
            rbtTextValue.Checked = true;
            rbtQuestionValue.Enabled = selectedType != WordDocumentProperty.WordDocumentCustomPropertyType.Date;

            foreach (Question q in package.Questions)
            {
                switch (q.DataType)
                {
                    case PerfectusDataType.Text:
                        if (selectedType == WordDocumentProperty.WordDocumentCustomPropertyType.Text)
                        {
                            this.cbxQuestionValue.Items.Add(q);
                        }
                        break;
                    case PerfectusDataType.YesNo:
                        if (selectedType == WordDocumentProperty.WordDocumentCustomPropertyType.YesNo)
                        {
                            this.cbxQuestionValue.Items.Add(q);
                        }
                        break;
                    case PerfectusDataType.Date:
                        if (selectedType == WordDocumentProperty.WordDocumentCustomPropertyType.Date)
                        {
                            this.cbxQuestionValue.Items.Add(q);
                        }
                        break;
                    case PerfectusDataType.Number:
                        if (selectedType == WordDocumentProperty.WordDocumentCustomPropertyType.Number)
                        {
                            this.cbxQuestionValue.Items.Add(q);
                        }
                        break;
                }
            }
        }

		private void PopulatePropertiesList()
		{
			foreach(WordDocumentProperty dp in documentProperties)
			{
				if(dp.PropertyType == WordDocumentProperty.WordDocumentPropertyType.Custom)
				{
					string pName = GetFriendlyFromWordMlSafeString(dp.DocumentPropertyName);
					string pType = dp.CustomDocumentPropertyType.ToString();
					string pValue = string.Empty;
					string pValueType = string.Empty;

                    if (dp.DocumentPropertyValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
					{
						pValue = GetFriendlyValue(dp.DocumentPropertyValue.TextValue, dp.CustomDocumentPropertyType);
						pValueType = "text";
					}
                    else if (dp.DocumentPropertyValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question)
					{
						pValue = string.Format("<<{0}>>",dp.DocumentPropertyValue.QuestionValue.Name);
						pValueType = "question";
					}

					string[] myItems = new string[]{pName, pValue, pType, pValueType};
					
					ListViewItem lvitem = new ListViewItem(myItems,0);
					listView1.Items.Insert(listView1.Items.Count, lvitem);

				}
			}
		}

		private string GetFriendlyValue(string valueToConvert, WordDocumentProperty.WordDocumentCustomPropertyType propertyType)
		{
			//try 
			//{
			switch(propertyType)			
			{
				case WordDocumentProperty.WordDocumentCustomPropertyType.Date :

					//dates will be stored like the following: 2006-04-09T21:09:00Z
					//int year = int.Parse(valueToConvert.Substring(0,4));
					//int month = int.Parse(valueToConvert.Substring(5,2));
					//int day = int.Parse(valueToConvert.Substring(8,2));					
					DateTime datetime = DateTime.Parse(valueToConvert);
					return datetime.ToShortDateString();
				case WordDocumentProperty.WordDocumentCustomPropertyType.Number :
					return valueToConvert;
				case WordDocumentProperty.WordDocumentCustomPropertyType.YesNo :
					if(valueToConvert == "1")
					{
						return "Yes";
					}
					else if(valueToConvert == "0")
					{
						return "No";
					}
					return valueToConvert;
				default :
					return valueToConvert;				
			}		
			//}
			//catch
			//{
			//	return valueToConvert;
			//}
		}

		private string FriendlyValueToWordType(string valueToConvert, WordDocumentProperty.WordDocumentCustomPropertyType propertyType)
		{
			try 
			{
				switch(propertyType)			
				{
					case WordDocumentProperty.WordDocumentCustomPropertyType.Date:	
				
						int day = int.Parse(valueToConvert.Substring(0,2));
						int month = int.Parse(valueToConvert.Substring(3,2));
						int year = int.Parse(valueToConvert.Substring(6,4));					
						
                        DateTime dateTime = new DateTime(year,month,day);
                        return String.Format("{0:u}", dateTime);

					case WordDocumentProperty.WordDocumentCustomPropertyType.Number :
						return valueToConvert;
					case WordDocumentProperty.WordDocumentCustomPropertyType.YesNo :
						if(valueToConvert == "Yes")
						{
							return "1";
						}
						else
						{
							return "0";
						}
					default :
						return valueToConvert;				
				}		
			}
			catch
			{
				return valueToConvert;
			}
		}

		private void rbtTextValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if(rbtTextValue.Checked == true)
			{
				this.txtTextValue.Enabled = true;
				this.cbxQuestionValue.Enabled = false;
			}
			else
			{
				this.txtTextValue.Enabled = false;
				this.cbxQuestionValue.Enabled = true;
			}

			CalculateButtonState();
		}

		private void butAdd_Click(object sender, System.EventArgs e)
		{
			if(ValidateItem())
			{
				AddNewItem();
			}
		}

		private void AddNewItem()
		{	
			ListViewItem lvitem = GetListViewItem();
			listView1.Items.Insert(listView1.Items.Count, lvitem);

			ClearFields();
		}

		private void ClearFields()
		{
			this.txtTextValue.Text = "";
			this.txtName.Text = "";
			this.cbxType.SelectedIndex = 0;
			butAdd.Visible = true;
			butAdd.Enabled = false;
			butDelete.Enabled = false;
			this.txtName.Focus();
		}

		private void listView1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if(listView1.SelectedItems.Count > 0)
			{
				this.txtTextValue.Text = "";

				this.butAdd.Visible = false;
				butDelete.Enabled = true;
			
				string sName = listView1.SelectedItems[0].SubItems[0].Text;
				string sValue = listView1.SelectedItems[0].SubItems[1].Text;
				string sType = listView1.SelectedItems[0].SubItems[2].Text;
				string sValueType = listView1.SelectedItems[0].SubItems[3].Text;

				this.txtName.Text = sName;
				this.butAdd.Visible = false;

				//select type
				this.cbxType.SelectedItem = (WordDocumentProperty.WordDocumentCustomPropertyType) Enum.Parse(typeof(WordDocumentProperty.WordDocumentCustomPropertyType), sType, true);
				
				if(sValueType == "question")
				{
					sValue = sValue.Replace("<<","");
					sValue = sValue.Replace(">>","");

					foreach(Question q in package.Questions)
					{		
						if(q.Name.ToLower() == sValue.ToLower())
						{
							this.cbxQuestionValue.SelectedItem = q;
							this.rbtQuestionValue.Checked = true;
							break;
						}
					}
				}
				else
				{
					this.txtTextValue.Text = sValue;
					this.rbtTextValue.Checked = true;
				}
			}
			else
			{
				this.butAdd.Visible = true;
				this.butDelete.Enabled = false;
			}
		}

		private void txtName_TextChanged(object sender, System.EventArgs e)
		{	
			// if we modify the name, then its a new one, not a modification.
			if(butModify.Visible)
			{
				butAdd.Visible = true;
				butDelete.Enabled = false;
			}

			CalculateButtonState();
		}

		private void butModify_Click(object sender, System.EventArgs e)
		{
			if(ValidateItem())
			{
				ModifyItem();
			}
		}

		private void ModifyItem()
		{
			ListViewItem lvitem = GetListViewItem();
			listView1.SelectedItems[0].SubItems[0].Text = lvitem.SubItems[0].Text;
			listView1.SelectedItems[0].SubItems[1].Text = lvitem.SubItems[1].Text;
			listView1.SelectedItems[0].SubItems[2].Text = lvitem.SubItems[2].Text;
			listView1.SelectedItems[0].SubItems[3].Text = lvitem.SubItems[3].Text;

			ClearFields();
		}

		private ListViewItem GetListViewItem()
		{
			string sValue = string.Empty;
			string sValueType = string.Empty;

			if(this.rbtTextValue.Checked)
			{
				sValue = this.txtTextValue.Text;
				sValueType = "text";
			}
			else
			{
				Question q = (Question) this.cbxQuestionValue.SelectedItem;
				sValue = string.Format("<<{0}>>",q.Name);
				sValueType = "question";
			}

			string[] myItems = new string[]{this.txtName.Text,sValue,this.cbxType.SelectedValue.ToString(),sValueType};
			ListViewItem lvitem = new ListViewItem(myItems,0);

			return lvitem;
		}

		private void txtTextValue_TextChanged(object sender, System.EventArgs e)
		{
			CalculateButtonState();
		}

		private void CalculateButtonState()
		{		
			if(this.txtName.Text.Length == 0)
			{
				DisableButtons();
				return;
			}
			if(this.rbtTextValue.Checked && this.txtTextValue.Text.Length == 0)
			{
				DisableButtons();
				return;
			}

			EnableButtons();
		}

		private void DisableButtons()
		{
			butAdd.Enabled = false;
			butModify.Enabled = false;
			butDelete.Enabled = false;
		}

		private void EnableButtons()
		{
			butAdd.Enabled = true;
			butModify.Enabled = true;
			
			if(butAdd.Visible)
			{
				butDelete.Enabled = false;
			}
			else
			{
				butDelete.Enabled = true;
			}
		}

		internal void UpdateCustomDocumentProperties()
		{
			for (int i = documentProperties.Count - 1; i >= 0; i--)
			{
				if(documentProperties[i].PropertyType == WordDocumentProperty.WordDocumentPropertyType.Custom)
				{
					documentProperties.RemoveAt(i);
				}
			}

			foreach(ListViewItem lvitem in this.listView1.Items)
			{
				WordDocumentProperty dp = new WordDocumentProperty();
				TextQuestionQueryValue tqv = new TextQuestionQueryValue();
							
				dp.DocumentPropertyName= GetWordMlSafeString(lvitem.SubItems[0].Text);
				dp.PropertyType = WordDocumentProperty.WordDocumentPropertyType.Custom;
				dp.CustomDocumentPropertyType = (WordDocumentProperty.WordDocumentCustomPropertyType) Enum.Parse(typeof(WordDocumentProperty.WordDocumentCustomPropertyType), lvitem.SubItems[2].Text, true);

				string tqvValueType = lvitem.SubItems[3].Text;
				if(tqvValueType == "text")
				{
					tqv.TextValue = FriendlyValueToWordType(lvitem.SubItems[1].Text,dp.CustomDocumentPropertyType);
                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
				}
				else
				{
					string qValue = lvitem.SubItems[1].Text;
					qValue = qValue.Replace("<<","");
					qValue = qValue.Replace(">>","");

					foreach(Question q in package.Questions)
					{		
						if(q.Name.ToLower() == qValue.ToLower())
						{
							tqv.QuestionValue = q;
							break;
						}
					}

                    tqv.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Question;
				}
			
				dp.DocumentPropertyValue = tqv;
								
				documentProperties.Add(dp);
			}
		}

		private string GetWordMlSafeString(string stringToConvert)
		{
			stringToConvert = stringToConvert.Replace(" ","_x0020_");
			stringToConvert = stringToConvert.Replace("&","_x0026_");
			stringToConvert = stringToConvert.Replace("<","_x003c_");
			stringToConvert = stringToConvert.Replace(">","_x003E_");
			stringToConvert = stringToConvert.Replace("\"","_x0022_");
			stringToConvert = stringToConvert.Replace("'","_x0027_");

			return stringToConvert;
		}

		private string GetFriendlyFromWordMlSafeString(string stringToConvert)
		{
			stringToConvert = stringToConvert.Replace("_x0020_"," ");
			stringToConvert = stringToConvert.Replace("_x0026_","&");
			stringToConvert = stringToConvert.Replace("_x003c_","<");
			stringToConvert = stringToConvert.Replace("_x003E_",">");
			stringToConvert = stringToConvert.Replace("_x0022_","\"");
			stringToConvert = stringToConvert.Replace("_x0027_","'");

			return stringToConvert;
		}

		private void butDelete_Click(object sender, System.EventArgs e)
		{
			this.listView1.SelectedItems[0].Remove();
			ClearFields();
		}

		private bool ValidateItem()
		{
			if(this.rbtQuestionValue.Checked == true)
			{
				if(this.cbxQuestionValue.SelectedItem == null)
				{
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.SelectQuestion"),"Error");
					return false;
				}
			}		

			if(this.cbxType.SelectedItem.ToString().ToLower() == WordDocumentProperty.WordDocumentCustomPropertyType.Number.ToString().ToLower() && this.rbtTextValue.Checked)
			{	
				try 
				{
					float f = float.Parse(this.txtTextValue.Text.Trim());
					txtTextValue.Text = f.ToString();
					return true;
				}
				catch
				{
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.ValidNumber"),"Error");
					return false;
				}				
			}

			if(this.cbxType.SelectedItem.ToString().ToLower() == WordDocumentProperty.WordDocumentCustomPropertyType.YesNo.ToString().ToLower() && this.rbtTextValue.Checked)
			{
				string s = this.txtTextValue.Text.Trim().ToLower();
				if (s == "yes" || s == "no")
				{
					return true;
				}
			
				MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.EnterYesOrNo"),"Error");
				return false;			
			}

			if(this.cbxType.SelectedItem.ToString().ToLower() == WordDocumentProperty.WordDocumentCustomPropertyType.Date.ToString().ToLower() && this.rbtTextValue.Checked)
			{
				// check for dd/mm/yyyy
				try
				{
					string s = this.txtTextValue.Text.Trim();
					int day = int.Parse(s.Substring(0,2));
					int month = int.Parse(s.Substring(3,2));
					int year = int.Parse(s.Substring(6,4));

					DateTime dateTime = new DateTime(year,month,day); // will throw exception if funny values
					return true;
				}
				catch
				{
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.EnterValidDate"), "Error");
					return false;
				}
						
			}

			return true;
		}

		private void cbxType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.PopulateQuestionCombo();
		}
	}
}
