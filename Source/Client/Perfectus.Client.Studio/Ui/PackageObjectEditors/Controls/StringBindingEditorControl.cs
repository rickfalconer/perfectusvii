using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using mshtml;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.Designers;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for StringBindingEditorControl.
	/// </summary>
	public class StringBindingEditorControl : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private WebBrowser editor1;		
		private string pendingDocBody;
		private Package packageBeingEdited;
	    private bool havePainted = false;
	    private bool haveSetEditMode = false;

		public StringBindingEditorControl()
		{
			InitializeComponent();
			// This call is required by the Windows.Forms Form Designer.
		}

		public Package Package
		{
			get {return packageBeingEdited;}
			set { packageBeingEdited = value;}
		}

		public string HtmlBody
		{
			get 
			{
				return editor1.Document.Body.InnerHtml;		
			}
		}
		
		public string XmlBody
		{
			get 
			{
				return StringBindingMod.GetXmlFromDoc((HTMLDocument)(editor1.Document.DomDocument),true);
			}
			set 
			{
				if(value != null)
				{
					if(editor1.ReadyState == WebBrowserReadyState.Complete)
					{
						editor1.Document.Body.InnerHtml = StringBindingMod.GetDocHtmlFromXml(value, packageBeingEdited);							
					}
					else
					{
						pendingDocBody = StringBindingMod.GetDocHtmlFromXml(value,packageBeingEdited);
                        // it's possible the ready state changed during execution of last method, in which case we need to set html now!
                        if (editor1.ReadyState == WebBrowserReadyState.Complete)
						{
							editor1.Document.Body.InnerHtml = pendingDocBody;
						}
					}
				}
				else
				{
                    if (editor1.ReadyState == WebBrowserReadyState.Complete)
                    {
						editor1.Document.Body.InnerHtml = "";
					}
					else
					{
						pendingDocBody = "";
					}
				}
			}         
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.editor1 = new WebBrowser();
			this.SuspendLayout();
			// 
			// editor1
			// 
			//JM - replaced with system.forms.webbrowser - mostly
//		    this.editor1.DefaultComposeSettings.BackColor = System.Drawing.Color.White;
//			this.editor1.DefaultComposeSettings.DefaultFont = new System.Drawing.Font("Arial", 10F);
//			this.editor1.DefaultComposeSettings.Enabled = false;
//			this.editor1.DefaultComposeSettings.ForeColor = System.Drawing.Color.Black;
			this.editor1.Dock = System.Windows.Forms.DockStyle.Fill;
//			this.editor1.DocumentEncoding = onlyconnect.EncodingType.WindowsCurrent;
//			this.editor1.IsDesignMode = true;
			this.editor1.Location = new System.Drawing.Point(0, 0);
//			this.editor1.Name = "editor1";
//			this.editor1.OpenLinksInNewWindow = true;
//			this.editor1.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Left;
//			this.editor1.SelectionBackColor = System.Drawing.Color.Empty;
//			this.editor1.SelectionBullets = false;
//			this.editor1.SelectionFont = null;
//			this.editor1.SelectionForeColor = System.Drawing.Color.Empty;
//			this.editor1.SelectionNumbering = false;
			this.editor1.Size = new System.Drawing.Size(528, 368);
			this.editor1.TabIndex = 7;
			//this.editor1.Text = "Text";
			//this.editor1.DragOver += new System.Windows.Forms.DragEventHandler(this.editor1_DragOver);

            this.editor1.DocumentText = string.Empty;
            editor1.Document.ExecCommand("EditMode", true, null);
            haveSetEditMode = true;
            // 
			// StringBindingEditorControl
			// 
			this.Controls.Add(this.editor1);
			this.Name = "StringBindingEditorControl";
			this.Size = new System.Drawing.Size(528, 368);
			this.ResumeLayout(false);
            this.Paint += new PaintEventHandler(StringBindingEditorControl_Paint);

		}

        void StringBindingEditorControl_Paint(object sender, PaintEventArgs e)
        {
            if (havePainted)
            {
                return;
            }
            if (!haveSetEditMode)
            {
                return;
            }
            IHTMLDocument2 doc2 = (IHTMLDocument2)editor1.Document.DomDocument;

            while (editor1.ReadyState != WebBrowserReadyState.Complete)
            {
                Thread.Sleep(200);
            }

            if (pendingDocBody != null)
            {
                //editor1.Document.Body.InnerHtml = StringBindingMod.GetDocHtmlFromXml(pendingBodyValue, PackageBeingEdited);
                editor1.Document.Body.InnerHtml = pendingDocBody;

            }

            if (doc2.styleSheets.length == 0)
            {
                string cssAddress =
                    string.Format(@"file://{0}\stringEditor.css",
                                  Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                doc2.createStyleSheet(cssAddress, 0);
            }
            havePainted = true;

        }
		#endregion		
	}
}
