using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// This control only displays internal pages i.e. no external pages
	/// </summary>
	public class PageListEditorControl : UserControl
    {
		private ImageList imageList1;
		private IContainer components;
		private IWindowsFormsEditorService edSvc;


		private InterviewPage currentValue = null;

		public InterviewPageCollection Pages
		{
			get { return pages; }
			set
			{
				pages = value;
				BindList();
			}
		}

		private InterviewPageCollection pages = new InterviewPageCollection();
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		private InterviewPageCollection pagesToExclude = new InterviewPageCollection();

		public InterviewPage Value
		{
			get { return currentValue; }
			set { currentValue = value; }
		}

		public InterviewPageCollection PagesToExclude
		{
			get { return pagesToExclude; }
			set
			{
				pagesToExclude = value;
				BindList();
			}
		}

		public PageListEditorControl(IWindowsFormsEditorService edSvc)
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.edSvc = edSvc;
		}

		private void BindList()
		{
			uiListBox1.Items.Clear();
			if (pages != null)
			{
				//TODO:  Pretty sure this will never be used on a question, but if it is, need to make sure no circular references happen
				foreach (InterviewPage p in pages)
				{
					if (! (pagesToExclude.Contains(p)))
					{
						int imageIndex = 0;
						if(p is ExternalPage)
						{
							imageIndex = 1;
						}

                        int itemAdded = int.MinValue;
                        ListViewItem item = null;

                        if(!(p is ExternalPage))//this test stops external pages from being set as the start page. case
                        {
						     //itemAdded = uiListBox1.Items.Add(p, p.Name, imageIndex);
                            item = new ListViewItem() { Text = p.Name, ImageIndex = imageIndex, Tag = p };
                            //itemAdded = uiListBox1.Items.Add(item);
                            uiListBox1.Items.Add(item);
                        }

						if (currentValue != null && currentValue == p)
						{
							//uiListBox1.SelectedItem = uiListBox1.Items[itemAdded];
                            uiListBox1.FocusedItem = item;
						}
					}
				}
                uiListBox1.Sorting = SortOrder.Ascending;
                uiListBox1.Sort();
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageListEditorControl));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 0);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(150, 150);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.TabIndex = 0;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseUp);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 200;
            // 
            // PageListEditorControl
            // 
            this.Controls.Add(this.uiListBox1);
            this.Name = "PageListEditorControl";
            this.ResumeLayout(false);

		}

		#endregion

        private void uiListBox1_MouseUp(object sender, MouseEventArgs e)
        {
            QuestionSelected();
        }

		private void QuestionSelected()
		{
			if (uiListBox1.Items.Count > 0 && uiListBox1.FocusedItem != null)
			{
				//currentValue = uiListBox1.SelectedItem.ItemData as InterviewPage;
                currentValue = uiListBox1.FocusedItem.Tag as InterviewPage;
				edSvc.CloseDropDown();
			}
		}

	}
}