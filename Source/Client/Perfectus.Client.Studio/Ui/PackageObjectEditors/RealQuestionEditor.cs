﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.Designers.PageItems;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
    /// <summary>
    /// Summary description for realQuestionEditor.
    /// pf-3035 Real Field
    /// </summary>
    public class RealQuestionEditor : UITypeEditor
    {

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            RealQuestionEditorControl ui = null;

            // get the editor service.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)sp.GetService(typeof(IWindowsFormsEditorService));

            if (edSvc == null)
            {
                // uh oh.
                return value;
            }

            // create our UI
            if (ui == null)
            {
                ui = new RealQuestionEditorControl(edSvc);
                object c = null;

                if (context.Instance is PackageItem)
                {
                    c = context.Instance as PackageItem;
                }
                else if (context.Instance is Item)
                {
                    c = context.Instance as Item;
                }

                if (c != null && c is PackageItem)
                {
                    ui.Questions = ((PackageItem)c).ParentPackage.Questions;
                }
                else if (c != null && c is Item)
                {
                    ui.Questions = ((Item)c).PackageBeingEdited.Questions;
                }
                else
                {
                    ui.Questions = null;
                }

                ui.Value = value as RealQuestionValue;
            }

            edSvc.DropDownControl(ui);

            return ui.Value;
        }

        // ------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        ///		Specify that this editor is the drop down style (instead of the [...] popup style)
        /// </summary>
        /// <param name="context"></param>
        /// <returns>DropDown</returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return false;
        }
    }
}