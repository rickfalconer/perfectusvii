using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	// SEE: http://www.syncfusion.com/FAQ/WinForms/FAQ_c44c.asp#q480q
	public class ComboColumn : DataGridTextBoxColumn
	{
		public NoKeyUpCombo ColumnComboBox;
		private CurrencyManager _source;
		private int _rowNum;
		private bool _isEditing;
		internal static int _rowCount;
		private int _selectedIndex;
		private bool _hasSentKeyPress = false;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Constructor that creates and sets up the combo box control we will be placing in the active cell of the column.
		/// </summary>
		// ------------------------------------------------------------------------------------------------------------------------------
		public ComboColumn() : base()
		{
			_source = null;
			_isEditing = false;
			_rowCount = -1;

			ColumnComboBox = new NoKeyUpCombo();
			ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

			ColumnComboBox.Leave += new EventHandler(LeaveComboBox);
			ColumnComboBox.Enter += new EventHandler(ComboEnter);

			ColumnComboBox.SelectionChangeCommitted += new EventHandler(ComboStartEditing);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets the _isEditing field to true when the combo box receives focus (usually when the cell is entered)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboEnter(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Called by the combo box's SelectionChangeCommitted event.  Turns on _isEditing, and calls the base
		///		ColumnStartedEditing handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboStartEditing(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;

			base.ColumnStartedEditing((Control) sender);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Makes sure the combo box is hidden when the user scrolls the grid (otherwise it floats over the grid and looks odd)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void HandleScroll(object sender, EventArgs e)
		{
			if (ColumnComboBox.Visible)
				ColumnComboBox.Hide();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		When the combo box loses focus, turn off _isEditing and write the selected value into the datarow.
		///		Then hide the combo box, remove the Scroll handler from the grid, and return the column to its 'not doing anything' state.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void LeaveComboBox(object sender, EventArgs e)
		{
			try
			{
				if (_isEditing)
				{
					SetColumnValueAtRow(_source, _rowNum, ColumnComboBox.Text);
					_isEditing = false;
					Invalidate();
				}
				ColumnComboBox.Hide();
				Invalidate();
				this.DataGridTableStyle.DataGrid.Scroll -= new EventHandler(HandleScroll);
				_hasSentKeyPress = false;
			}
			catch
			{
			} // Odd 'null' problem.

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets up the combo box with the correct selectedValue (according to the actual data), and displays it
		///		within the rectangle that would have been filled by the cell's Text Box control.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="bounds"></param>
		/// <param name="readOnly"></param>
		/// <param name="instantText"></param>
		/// <param name="cellIsVisible"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!_hasSentKeyPress)

			{
				// Send a key-down Windows Message to this cell's text box, to make the grid act as if the user was
				// typing normally.  This is to make sure that clicking in the 'new row' row still makes the grid
				// add a new 'new row' row underneath it.
				Sneaky(this.TextBox);

				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);

				_rowNum = rowNum;
				_source = source;
				_rowCount = _source.Count;

				// Position the combo box.
				ColumnComboBox.Parent = this.TextBox.Parent;
				ColumnComboBox.Location = this.TextBox.Location;
				ColumnComboBox.Size = new Size(this.TextBox.Size.Width, ColumnComboBox.Size.Height);

				// Set its selected index.
				ColumnComboBox.SelectedIndex = -1;
				ColumnComboBox.SelectedIndex = ColumnComboBox.FindStringExact(this.TextBox.Text);
				ColumnComboBox.Text = this.TextBox.Text;

				_selectedIndex = ColumnComboBox.SelectedIndex;

				if (_selectedIndex < 0 && ColumnComboBox.Items.Count > 0)
				{
					ColumnComboBox.SelectedIndex = 0;
					_selectedIndex = 0;
				}

				// Hide the text box, display the combo.
				this.TextBox.Visible = false;
				ColumnComboBox.Visible = true;

				// Handle scroll events on the grid, to hide the combo.
				this.DataGridTableStyle.DataGrid.Scroll += new EventHandler(HandleScroll);

				ColumnComboBox.BringToFront();
				ColumnComboBox.Focus();
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the value to the dataset.
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="rowNum"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			if (_isEditing)
			{
				_isEditing = false;
				_hasSentKeyPress = false;

				SetColumnValueAtRow(dataSource, rowNum, ColumnComboBox.Text);
			}
			return true;
		}

		protected override void ConcedeFocus()
		{
			_isEditing = false;
			_hasSentKeyPress = false;

			base.ConcedeFocus();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Instead of getting the value from the text box, get it from the selected item of the visible combo box.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <returns>Returns the selected value from the combo box.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override object GetColumnValueAtRow(CurrencyManager source, int rowNum)
		{
			object s = base.GetColumnValueAtRow(source, rowNum);
			DataView dv = (DataView) this.ColumnComboBox.DataSource;
			int rowCount = dv.Count;
			int i = 0;

			//if things are slow, you could order your dataview
			//& use binary search instead of this linear one
			while (i < rowCount)
			{
				if (s.Equals(dv[i][this.ColumnComboBox.ValueMember]))
					break;
				++i;
			}

			if (i < rowCount)
			{
				object o = dv[i][this.ColumnComboBox.DisplayMember];
				return o.ToString();
			}

			return DBNull.Value;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the combo box's selected value into the data set.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="value"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void SetColumnValueAtRow(CurrencyManager source, int rowNum, object value)
		{
			try
			{
				if (rowNum == 0 && MappingName == "BooleanOperator")
				{
					base.SetColumnValueAtRow(source, rowNum, DBNull.Value);
				}

				object s = value;

				DataView dv = (DataView) this.ColumnComboBox.DataSource;
				int rowCount = dv.Count;
				int i = 0;

				//if things are slow, you could order your dataview
				//& use binary search instead of this linear one
				while (i < rowCount)
				{
					if (s.Equals(dv[i][this.ColumnComboBox.DisplayMember]))
						break;
					++i;
				}
				if (i < rowCount)
					s = dv[i][this.ColumnComboBox.ValueMember];
				else
					s = DBNull.Value;

				base.SetColumnValueAtRow(source, rowNum, s);
			}
			catch // Empty catch, in case of the rowNum issue with datagrids 'over-editing'.
			{
			}

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Send a key-up message to a control (hWin).
		/// </summary>
		/// <param name="c"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void Sneaky(Control c)
		{
			_hasSentKeyPress = true;
			IntPtr h = c.Handle;
			int wParam = 0x97;
			uint lParam = 0x1E0001;
			WinMessage.SendMessage(h.ToInt32(), 0x102, wParam, lParam);
			_isEditing = true;
		}
	}
}