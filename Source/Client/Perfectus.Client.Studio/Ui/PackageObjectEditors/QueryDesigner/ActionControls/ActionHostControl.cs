using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls
{
	/// <summary>
	/// Summary description for ActionHostControl.
	/// </summary>
	public class ActionHostControl : UserControl
	{
        public event CancelEventHandler SubqueryEdit;
		private Button btnSubquery;
		private QueryDesignerMode mode;
		private PackageItem itemBeingEdited;

		public PackageItem ItemBeingEdited
		{
			get { return itemBeingEdited; }
			set { itemBeingEdited = value; }
		}

		public QueryDesignerMode Mode
		{
			get { return mode; }
			set { mode = value; }
		}

		private Package package;
        private TabControl uiTabView1;
        private TabPage tabAction;
        private TabPage tabSubquery;

		public Package Package
		{
			get { return package; }
			set { package = value; }
		}

		private ActionControlBase hostedControl;

		public ActionControlBase HostedControl
		{
			get { return hostedControl; }
			set
			{
				hostedControl = value;
				if (hostedControl != null)
				{
					tabAction.Controls.Clear();
					tabAction.Controls.Add(hostedControl);
                    //hostedControl.Dock = DockStyle.Fill;

                    // fix docking width
                    tabAction.Width = this.Width - 5;

                    hostedControl.Width = tabAction.Width - 10;
					hostedControl.Height = tabAction.Height - 9;
					hostedControl.Top = 2;
					hostedControl.Left = 5;
				}
			}
		}

		public void Commit()
		{
			if (uiTabView1.SelectedTab != tabSubquery)
			{
				Action.SubQuery = null;
				hostedControl.Commit();
			}
			else
			{
				hostedControl.Clear();
			}
		}

		public ActionBase Action
		{
			get { return hostedControl.ActionBeingEdited; }
			set
			{
				hostedControl.ActionBeingEdited = value;
				if (value.SubQuery != null)
				{
					uiTabView1.SelectedTab = tabSubquery;
				}
				else
				{
					uiTabView1.SelectedTab = tabAction;
				}
			}
		}

		public bool IsSubqueryTabVisible
		{
			get { return uiTabView1.SelectedTab == tabSubquery; }
		}


		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public ActionHostControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionHostControl));
            this.btnSubquery = new System.Windows.Forms.Button();
            this.uiTabView1 = new System.Windows.Forms.TabControl();
            this.tabAction = new System.Windows.Forms.TabPage();
            this.tabSubquery = new System.Windows.Forms.TabPage();
            this.uiTabView1.SuspendLayout();
            this.tabSubquery.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSubquery
            // 
            resources.ApplyResources(this.btnSubquery, "btnSubquery");
            this.btnSubquery.Name = "btnSubquery";
            this.btnSubquery.Click += new System.EventHandler(this.btnSubquery_Click);
            // 
            // uiTabView1
            // 
            this.uiTabView1.Controls.Add(this.tabAction);
            this.uiTabView1.Controls.Add(this.tabSubquery);
            resources.ApplyResources(this.uiTabView1, "uiTabView1");
            this.uiTabView1.Name = "uiTabView1";
            this.uiTabView1.SelectedIndex = 0;
            // 
            // tabAction
            // 
            resources.ApplyResources(this.tabAction, "tabAction");
            this.tabAction.Name = "tabAction";
            this.tabAction.UseVisualStyleBackColor = true;
            // 
            // tabSubquery
            // 
            this.tabSubquery.Controls.Add(this.btnSubquery);
            resources.ApplyResources(this.tabSubquery, "tabSubquery");
            this.tabSubquery.Name = "tabSubquery";
            this.tabSubquery.UseVisualStyleBackColor = true;
            // 
            // ActionHostControl
            // 
            this.Controls.Add(this.uiTabView1);
            this.Name = "ActionHostControl";
            resources.ApplyResources(this, "$this");
            this.uiTabView1.ResumeLayout(false);
            this.tabSubquery.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private bool OnSubqueryEdit()
		{
			if (this.SubqueryEdit != null)
			{
				CancelEventArgs cea = new CancelEventArgs();
				SubqueryEdit(this, cea);
				return cea.Cancel;
			}
			else
			{
				return true;
			}
		}

		private void btnSubquery_Click(object sender, EventArgs e)
		{
			bool cancelEdit = OnSubqueryEdit();
			if (!cancelEdit)
			{
				if (Action != null)
				{
					bool wasNull = false;

					if (Action.SubQuery == null)
					{
						wasNull = true;
						Action.SubQuery = new Query(package);
					}

					Form thisForm = FindForm();

					QueryDesignerForm frm = new QueryDesignerForm();

					frm.StartPosition = FormStartPosition.Manual;
					frm.Left = thisForm.Left + 15;
					frm.Top = thisForm.Top + 15;

					frm.ItemBeingEdited = itemBeingEdited;
					frm.QueryBeingEdited = Action.SubQuery;
					frm.Mode = mode;

					DialogResult dr = frm.ShowDialog();
					if (dr == DialogResult.Cancel && wasNull)
					{
						Action.SubQuery = null;
					}
				}
			}
		}
	}
}