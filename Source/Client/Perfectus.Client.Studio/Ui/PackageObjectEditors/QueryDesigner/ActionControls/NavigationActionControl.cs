using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls
{
	public class NavigationActionControl : ActionControlBase
	{
        private IContainer components = null;
		private Button button1;
		private ImageList imageList1;
		private RadioButton rdoEndInterview;
		private RadioButton rdoPage;
        private ComboBox cbxPage;
		private InterviewPage pageBeingEdited;

		public InterviewPage PageBeingEdited
		{
			get { return pageBeingEdited; }
			set
			{
				pageBeingEdited = value;
				ResetUI();
			}
		}

		protected override void ResetUI()
		{
            groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;

			cbxPage.Items.Clear();
			Interview iv = pageBeingEdited.ParentInterview;
			//UiListItem itemToSelect = null;
            ComboboxItem itemToSelect = null;

			foreach (InterviewPage pg in iv.Pages)
			{
				if (pg != pageBeingEdited)
				{
					//UiListItem li = new UiListItem(pg.Name, pg, 0);
                    ComboboxItem li = new ComboboxItem() { Text = pg.Name, Value = pg };

					cbxPage.Items.Add(li);
					if (ActionBeingEdited != null && ActionBeingEdited is NavigationAction && 
                        pg == ((NavigationAction) ActionBeingEdited).Page)
					{
						itemToSelect = li;
					}

				}
			}
			if (itemToSelect != null)
			{
				cbxPage.SelectedItem = itemToSelect;
				rdoPage.Checked = true;
				cbxPage.Enabled = true;
			}
			else
			{
				cbxPage.SelectedItem = null;
				rdoEndInterview.Checked = true;
				cbxPage.Enabled = false;
			}

		}

        public override void Commit()
        {
            if (ActionBeingEdited is NavigationAction)
                if (rdoEndInterview.Checked || cbxPage.SelectedItem == null)
                {
                    ((NavigationAction)ActionBeingEdited).Page = null;
                }
                else
                {
                    //((NavigationAction)ActionBeingEdited).Page = (InterviewPage)(cbxPage.SelectedItem.ItemData);
                    ComboboxItem selectedItem = (ComboboxItem)cbxPage.SelectedItem;
                    ((NavigationAction)ActionBeingEdited).Page = (InterviewPage)selectedItem.Value;
                }
        }

		public override void Clear()
		{
            if ( ActionBeingEdited is NavigationAction)
			((NavigationAction) ActionBeingEdited).Page = null;
		}


		public NavigationActionControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NavigationActionControl));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rdoEndInterview = new System.Windows.Forms.RadioButton();
            this.rdoPage = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.cbxPage = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxPage);
            this.groupBox1.Controls.Add(this.rdoPage);
            this.groupBox1.Controls.Add(this.rdoEndInterview);
            this.groupBox1.Controls.Add(this.button1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // rdoEndInterview
            // 
            resources.ApplyResources(this.rdoEndInterview, "rdoEndInterview");
            this.rdoEndInterview.Checked = true;
            this.rdoEndInterview.Name = "rdoEndInterview";
            this.rdoEndInterview.TabStop = true;
            this.rdoEndInterview.CheckedChanged += new System.EventHandler(this.rdoEndInterview_CheckedChanged);
            // 
            // rdoPage
            // 
            resources.ApplyResources(this.rdoPage, "rdoPage");
            this.rdoPage.Name = "rdoPage";
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            // 
            // cbxPage
            // 
            resources.ApplyResources(this.cbxPage, "cbxPage");
            this.cbxPage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPage.FormattingEnabled = true;
            this.cbxPage.Name = "cbxPage";
            // 
            // NavigationActionControl
            // 
            this.Name = "NavigationActionControl";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private void rdoEndInterview_CheckedChanged(object sender, EventArgs e)
		{
			cbxPage.Enabled = rdoPage.Checked;
		}
	}
}