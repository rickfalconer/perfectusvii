using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls
{
	/// <summary>
	/// Summary description for ActionControlBase.
	/// </summary>
	public class ActionControlBase : UserControl
	{
		protected GroupBox groupBox1;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private ActionBase actionBeingEdited;

		public ActionBase ActionBeingEdited
		{
			get { return actionBeingEdited; }
			set
			{
				actionBeingEdited = value;
				ResetUI();
			}
		}


		public virtual void Commit()
		{
		}

		protected virtual void ResetUI()
		{
		}

		public virtual void Clear()
		{
		}

		public ActionControlBase()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call
            

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(272, 224);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "ActionDescription";
			// 
			// ActionControlBase
			// 
			this.Controls.Add(this.groupBox1);
			this.Name = "ActionControlBase";
			this.Size = new System.Drawing.Size(272, 224);
			this.ResumeLayout(false);

		}

		#endregion
	}
}