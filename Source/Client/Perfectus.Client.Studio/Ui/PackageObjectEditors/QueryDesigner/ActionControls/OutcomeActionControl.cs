using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.PluginSystem;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls
{
	public class OutcomeActionControl : ActionControlBase
	{
		private CheckBox checkBox1;
		private Button btnEditText;
		private IContainer components = null;
		private ImageList imageList1;
		//private string pendingWordML = null;
        private string pendingOfficeOpenXML = null;
		private Label lblCurrentValue;
		private string pendingClauseRef = null;
		private string pendingFetcherId = null;
		private string pendingDisplayName = null;
		private string pendingLibraryName = null;
		private string pendingUniqueLibraryKey = null;

		private Outcome outcomeBeingEdited;

		public Outcome OutcomeBeingEdited
		{
			get { return outcomeBeingEdited; }
			set
			{
				outcomeBeingEdited = value;
				ResetUI();
			}
		}

		protected override void ResetUI()
		{
			base.ResetUI ();
			OutcomeAction oa = (OutcomeAction)ActionBeingEdited;
			if (oa != null)
			{
                if (oa.OfficeOpenXML != null)
				{
					lblCurrentValue.Text = "OOXML";
				} 
				else if (oa.LibraryKey != null)
				{
					lblCurrentValue.Text =  oa.DisplayName;
				}
				else
				{
					lblCurrentValue.Text = string.Empty;
				}
			}
		}


		public OutcomeActionControl()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();


		}

		private void AddClauseLibrary(IClauseLibrary plugin, int count)
		{
			btnEditText.Top = 13;
			Button btn = new Button();
			btn.Width = btnEditText.Width;
			btn.Height = btnEditText.Height;
			btn.Text = plugin.ButtonText;
			//btn.Anchor = btnEditText.Anchor;
			btn.Click += new EventHandler(pluginButton_Click);
			groupBox1.Controls.Add(btn);
			btn.Left = btnEditText.Left;
			btn.Top = btnEditText.Top + ((btn.Height + 5) * count);
			btn.FlatStyle = FlatStyle.System;
			btn.Tag = plugin;
			btn.Show();
		}

		public override void Clear()
		{
            ((OutcomeAction)ActionBeingEdited).OfficeOpenXML = null;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(OutcomeActionControl));
			this.btnEditText = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.lblCurrentValue = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.lblCurrentValue);
			this.groupBox1.Controls.Add(this.checkBox1);
			this.groupBox1.Controls.Add(this.btnEditText);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Text = resources.GetString("groupBox1.Text");
			// 
			// btnEditText
			// 
			this.btnEditText.AccessibleDescription = resources.GetString("btnEditText.AccessibleDescription");
			this.btnEditText.AccessibleName = resources.GetString("btnEditText.AccessibleName");
			this.btnEditText.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnEditText.Anchor")));
			this.btnEditText.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEditText.BackgroundImage")));
			this.btnEditText.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnEditText.Dock")));
			this.btnEditText.Enabled = ((bool)(resources.GetObject("btnEditText.Enabled")));
			this.btnEditText.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnEditText.FlatStyle")));
			this.btnEditText.Font = ((System.Drawing.Font)(resources.GetObject("btnEditText.Font")));
			this.btnEditText.Image = ((System.Drawing.Image)(resources.GetObject("btnEditText.Image")));
			this.btnEditText.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditText.ImageAlign")));
			this.btnEditText.ImageIndex = ((int)(resources.GetObject("btnEditText.ImageIndex")));
			this.btnEditText.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnEditText.ImeMode")));
			this.btnEditText.Location = ((System.Drawing.Point)(resources.GetObject("btnEditText.Location")));
			this.btnEditText.Name = "btnEditText";
			this.btnEditText.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnEditText.RightToLeft")));
			this.btnEditText.Size = ((System.Drawing.Size)(resources.GetObject("btnEditText.Size")));
			this.btnEditText.TabIndex = ((int)(resources.GetObject("btnEditText.TabIndex")));
			this.btnEditText.Text = resources.GetString("btnEditText.Text");
			this.btnEditText.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnEditText.TextAlign")));
			this.btnEditText.Visible = ((bool)(resources.GetObject("btnEditText.Visible")));
			this.btnEditText.Click += new System.EventHandler(this.btnEditText_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.AccessibleDescription = resources.GetString("checkBox1.AccessibleDescription");
			this.checkBox1.AccessibleName = resources.GetString("checkBox1.AccessibleName");
			this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("checkBox1.Anchor")));
			this.checkBox1.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("checkBox1.Appearance")));
			this.checkBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("checkBox1.BackgroundImage")));
			this.checkBox1.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.CheckAlign")));
			this.checkBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("checkBox1.Dock")));
			this.checkBox1.Enabled = ((bool)(resources.GetObject("checkBox1.Enabled")));
			this.checkBox1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("checkBox1.FlatStyle")));
			this.checkBox1.Font = ((System.Drawing.Font)(resources.GetObject("checkBox1.Font")));
			this.checkBox1.Image = ((System.Drawing.Image)(resources.GetObject("checkBox1.Image")));
			this.checkBox1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.ImageAlign")));
			this.checkBox1.ImageIndex = ((int)(resources.GetObject("checkBox1.ImageIndex")));
			this.checkBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("checkBox1.ImeMode")));
			this.checkBox1.Location = ((System.Drawing.Point)(resources.GetObject("checkBox1.Location")));
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("checkBox1.RightToLeft")));
			this.checkBox1.Size = ((System.Drawing.Size)(resources.GetObject("checkBox1.Size")));
			this.checkBox1.TabIndex = ((int)(resources.GetObject("checkBox1.TabIndex")));
			this.checkBox1.Text = resources.GetString("checkBox1.Text");
			this.checkBox1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.TextAlign")));
			this.checkBox1.Visible = ((bool)(resources.GetObject("checkBox1.Visible")));
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageList1.ImageSize")));
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// lblCurrentValue
			// 
			this.lblCurrentValue.AccessibleDescription = resources.GetString("lblCurrentValue.AccessibleDescription");
			this.lblCurrentValue.AccessibleName = resources.GetString("lblCurrentValue.AccessibleName");
			this.lblCurrentValue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblCurrentValue.Anchor")));
			this.lblCurrentValue.AutoSize = ((bool)(resources.GetObject("lblCurrentValue.AutoSize")));
			this.lblCurrentValue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblCurrentValue.Dock")));
			this.lblCurrentValue.Enabled = ((bool)(resources.GetObject("lblCurrentValue.Enabled")));
			this.lblCurrentValue.Font = ((System.Drawing.Font)(resources.GetObject("lblCurrentValue.Font")));
			this.lblCurrentValue.Image = ((System.Drawing.Image)(resources.GetObject("lblCurrentValue.Image")));
			this.lblCurrentValue.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCurrentValue.ImageAlign")));
			this.lblCurrentValue.ImageIndex = ((int)(resources.GetObject("lblCurrentValue.ImageIndex")));
			this.lblCurrentValue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblCurrentValue.ImeMode")));
			this.lblCurrentValue.Location = ((System.Drawing.Point)(resources.GetObject("lblCurrentValue.Location")));
			this.lblCurrentValue.Name = "lblCurrentValue";
			this.lblCurrentValue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblCurrentValue.RightToLeft")));
			this.lblCurrentValue.Size = ((System.Drawing.Size)(resources.GetObject("lblCurrentValue.Size")));
			this.lblCurrentValue.TabIndex = ((int)(resources.GetObject("lblCurrentValue.TabIndex")));
			this.lblCurrentValue.Text = resources.GetString("lblCurrentValue.Text");
			this.lblCurrentValue.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblCurrentValue.TextAlign")));
			this.lblCurrentValue.Visible = ((bool)(resources.GetObject("lblCurrentValue.Visible")));
			// 
			// OutcomeActionControl
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "OutcomeActionControl";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.Load += new System.EventHandler(this.OutcomeActionControl_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private void btnEditText_Click(object sender, EventArgs e)
		{
			OutcomeWordDialog dlg = new OutcomeWordDialog(OutcomeBeingEdited.ParentPackage,OutcomeBeingEdited);
            if (pendingOfficeOpenXML != null)
			{
                dlg.OfficeOpenXML = pendingOfficeOpenXML;
			}
			else
			{
                if (((OutcomeAction)ActionBeingEdited).OfficeOpenXML != null)
                {
                    dlg.OfficeOpenXML = ((OutcomeAction)ActionBeingEdited).OfficeOpenXML;
                }
                else
                    dlg.OfficeOpenXML = String.Empty;
			}
			DialogResult dr = dlg.ShowDialog();
			if (dr == DialogResult.OK)
			{
                pendingOfficeOpenXML = dlg.OfficeOpenXML;
				lblCurrentValue.Text = "OOXML";
			}
			FindForm().Activate();
		}

		public override void Commit()
		{
            if (pendingOfficeOpenXML != null)
			{
                ((OutcomeAction)ActionBeingEdited).OfficeOpenXML = pendingOfficeOpenXML;
			}
			if (pendingClauseRef != null)
			{
				((OutcomeAction) ActionBeingEdited).LibraryKey = pendingClauseRef;
				((OutcomeAction) ActionBeingEdited).DisplayName = pendingDisplayName;
				((OutcomeAction) ActionBeingEdited).LibraryName = pendingLibraryName;
				((OutcomeAction) ActionBeingEdited).FetcherId = pendingFetcherId;
				((OutcomeAction) ActionBeingEdited).ReportingId = pendingUniqueLibraryKey;
			}
		}

		private void pluginButton_Click(object sender, EventArgs e)
		{
			IClauseLibrary plugin;
			plugin = ((Button)sender).Tag as IClauseLibrary;
			if (plugin != null)
			{
				string clauseRef = null;
				string displayName = null;
				string libraryName = null;
				string uniqueLibraryKey = null;
			    bool result = false;
				try
				{
                    result = plugin.PickClause(out clauseRef, out displayName, out libraryName, out uniqueLibraryKey);
                    //clauseRef = plugin.PickClause(out displayName, out libraryName, out uniqueLibraryKey);
				}
				catch (Exception ex)
				{
					string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PluginError"), plugin.PluginName, ex.Message);
					msg = msg.Replace("\\n", "\n");
					MessageBox.Show(msg, Common.About.FormsTitle);

				}
				if (clauseRef != null && result)
				{
					pendingFetcherId = plugin.ServerSideFetcherId;
					pendingClauseRef = clauseRef;
					pendingDisplayName = displayName;
					pendingLibraryName = libraryName;
					pendingUniqueLibraryKey = uniqueLibraryKey;
					lblCurrentValue.Text = displayName;
				}
			}
			FindForm().Activate();
		}

		private void OutcomeActionControl_Load(object sender, EventArgs e)
		{
		   // Add a button for any clauselibrary plugins
            Collection<IStudioPlugin> plugins = MainDesigner2.Plugins;
			if (plugins != null)
			{
				int i = 0;
				foreach(IStudioPlugin isp in plugins)
				{

					if (isp is IClauseLibrary)
					{
						i++;
						AddClauseLibrary((IClauseLibrary)isp, i);
					}
				}
			}
		}
	}
}
