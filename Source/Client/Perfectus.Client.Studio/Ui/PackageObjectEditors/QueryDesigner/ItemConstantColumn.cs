using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	/// <summary>
	///		A derived DataGridTextBoxColumn that displays an ItemConstantPicer control istead of a textbox or a combobox.
	///		Similar to ComboColumn.
	/// </summary>
	// Step 1. Derive a custom column style from DataGridTextBoxColumn
	//	a) add a ComboBox member
	//  b) track when the combobox has focus in Enter and Leave events
	//  c) override Edit to allow the ComboBox to replace the TextBox
	//  d) override Commit to save the changed data
	public class ItemConstantColumn : DataGridTextBoxColumn
	{
		public ItemConstantPicker picker;
		private CurrencyManager _source;
		private int _rowNum;
		private bool _isEditing;
		internal static int _RowCount;
		private bool _hasSentKeyPress = false;
		private string typeDeterminingItemColumnName = null;


		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Constructor.  Sets up private fields and creates the picker control.
		/// </summary>
		// ------------------------------------------------------------------------------------------------------------------------------
		public ItemConstantColumn(Package package, PerfectusDataType pdt, string typeDeterminingItemColumnName, PackageItem itemBeingEdited) : base()
		{
			_source = null;
			_isEditing = false;
			_hasSentKeyPress = false;
			_RowCount = -1;


			this.typeDeterminingItemColumnName = typeDeterminingItemColumnName;

			picker = new ItemConstantPicker(package);
			picker.DataType = pdt;
			picker.ItemBeingEdited = itemBeingEdited;
			picker.Leave += new EventHandler(LeaveComboBox);
			picker.Enter += new EventHandler(ComboMadeCurrent);

		}


		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Hide the picker if the grid is scrolled while its visible.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void HandleScroll(object sender, EventArgs e)
		{
			if (picker.Visible)
				picker.Hide();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Turns _isEditing on when the picker gets focus.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboMadeCurrent(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the value in the picker into the datasource when focus leaves the picker.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void LeaveComboBox(object sender, EventArgs e)
		{
			if (_isEditing)
			{
				AnswerProvider pickerValue = picker.Value;
				if (pickerValue != null)
					SetColumnValueAtRow(_source, _rowNum, picker.Value);
				else
					SetColumnValueAtRow(_source, _rowNum, DBNull.Value);
				_isEditing = false;
				Invalidate();
			}
			picker.Hide();
			this.DataGridTableStyle.DataGrid.Scroll -= new EventHandler(HandleScroll);
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sends a keypress to the active cell and displays the picker control when a cell is edited.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="bounds"></param>
		/// <param name="readOnly"></param>
		/// <param name="instantText"></param>
		/// <param name="cellIsVisible"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!_hasSentKeyPress)
			{
				Sneaky(this.TextBox);

				_hasSentKeyPress = false;

				//on very first pass, set this static member to number of items in tables
				if (_RowCount == -1)
					_RowCount = source.Count;

				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);

				_rowNum = rowNum;
				_source = source;

				picker.Parent = this.TextBox.Parent;
				picker.Location = this.TextBox.Location;
				picker.Size = new Size(this.TextBox.Size.Width, picker.Size.Height);

				this.TextBox.Visible = false;

				IList dl = source.List;
				DataView dv = dl as DataView;			

				AnswerProvider curValue = dv[rowNum][this.MappingName] as AnswerProvider;
				picker.Value = curValue;

				if (typeDeterminingItemColumnName != null && !Convert.IsDBNull(((DataRowView) (source.Current))[typeDeterminingItemColumnName]) && (((DataRowView) source.Current)[typeDeterminingItemColumnName] != null))
				{
					object otherColValue = ((DataRowView) source.Current)[typeDeterminingItemColumnName];

					AnswerProvider iap = (AnswerProvider) otherColValue;

					picker.DataType = iap.DataType;
				}

				this.DataGridTableStyle.DataGrid.Scroll += new EventHandler(HandleScroll);

				picker.Visible = true;
				picker.BringToFront();
//				picker.Focus();

				
				picker.ResetUI();
				picker.FixDropdown();
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the value in the picker to the datasource when the edit ends.
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="rowNum"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			if (_isEditing)
			{
				if (picker.Value != null)
					SetColumnValueAtRow(dataSource, rowNum, picker.Value);
				else
					SetColumnValueAtRow(dataSource, rowNum, DBNull.Value);
				_hasSentKeyPress = false;
			}
			return true;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Concede focus.
		/// </summary>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void ConcedeFocus()
		{
			base.ConcedeFocus();
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Reads the value from the data source and returns the name of the packageItem it is pointing to (or the localised 'Yes' or 'No').
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override object GetColumnValueAtRow(CurrencyManager source, int rowNum)
		{
			object s = base.GetColumnValueAtRow(source, rowNum);
			if (Convert.IsDBNull(s))
				return DBNull.Value;
			else
			{
				if (s is AnswerProvider)
				{
					PackageItem v = (PackageItem) s;
					return v;

				}
			}
			return DBNull.Value;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sends a key down and key up message to a control (hWin).
		/// </summary>
		/// <param name="c">The control to send the keypress to.</param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void Sneaky(Control c)
		{
			_hasSentKeyPress = true;
			IntPtr h = c.Handle;
			c.Focus();
			int wParam = 0x97;
			uint lParam = 0x1E0001;
			WinMessage.SendMessage(h.ToInt32(), 0x102, wParam, lParam);

			wParam = 0x65;
			lParam = 0xC01E0001;
			WinMessage.SendMessage(h.ToInt32(), 0x101, wParam, lParam);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the chosen value into the datasource.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="value"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void SetColumnValueAtRow(CurrencyManager source, int rowNum, object value)
		{
			try
			{
				if (rowNum > source.List.Count)
					rowNum = source.List.Count;

				int pos = source.Position;
				source.Position = rowNum;

				base.SetColumnValueAtRow(source, rowNum, value);
				source.Position = pos;
			}
			catch //(Exception ex)
			{
				//MessageBox.Show("ItemConstant: " + ex.Message + ";Type = " + ex.GetType());
			}
		}
	}
}