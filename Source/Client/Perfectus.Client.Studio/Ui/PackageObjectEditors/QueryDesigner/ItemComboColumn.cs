using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Input;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	// SEE: http://www.syncfusion.com/FAQ/WinForms/FAQ_c44c.asp#q480q
	public class ItemComboColumn : DataGridTextBoxColumn
	{
		private ImageList iList;
        //public ComboBox ColumnComboBox;
        public NoKeyUpUiCombo ColumnComboBox;
        private CurrencyManager _source;
		private int _rowNum;
		private bool _isEditing;
		internal static int _rowCount;
		private int _selectedIndex;
		private bool _hasSentKeyPress = false;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Constructor that creates and sets up the combo box control we will be placing in the active cell of the column.
		/// </summary>
		// ------------------------------------------------------------------------------------------------------------------------------
		public ItemComboColumn(ImageList iList) : base()
		{
			this.iList = iList;
			_source = null;
			_isEditing = false;
			_rowCount = -1;

			ColumnComboBox = new NoKeyUpUiCombo();
			ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			//ColumnComboBox.ImageList = iList;

			ColumnComboBox.MaxDropDownItems = 12;


			ColumnComboBox.Leave += new EventHandler(LeaveComboBox);
			ColumnComboBox.Enter += new EventHandler(ComboEnter);

			ColumnComboBox.SelectedIndexChanged += new EventHandler(ComboStartEditing);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets the _isEditing field to true when the combo box receives focus (usually when the cell is entered)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboEnter(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Called by the combo box's SelectionChangeCommitted event.  Turns on _isEditing, and calls the base
		///		ColumnStartedEditing handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboStartEditing(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;

			base.ColumnStartedEditing((Control) sender);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Makes sure the combo box is hidden when the user scrolls the grid (otherwise it floats over the grid and looks odd)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void HandleScroll(object sender, EventArgs e)
		{
			if (ColumnComboBox.Visible)
				ColumnComboBox.Hide();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		When the combo box loses focus, turn off _isEditing and write the selected value into the datarow.
		///		Then hide the combo box, remove the Scroll handler from the grid, and return the column to its 'not doing anything' state.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void LeaveComboBox(object sender, EventArgs e)
		{
			if (_isEditing)
			{
				if (ColumnComboBox.SelectedItem != null)
				{
					try
					{
                        //ComboboxItem item = (ComboboxItem)ColumnComboBox.SelectedItem;
                        DataRowView item = ColumnComboBox.SelectedItem as DataRowView;
                        //SetColumnValueAtRow(_source, _rowNum, item.Value);
                        SetColumnValueAtRow(_source, _rowNum, item.Row[1]);
                    }
                    catch
					{
					}
				}
				_isEditing = false;
				Invalidate();
			}
			if (ColumnComboBox != null)
			{
				ColumnComboBox.Hide();
			}
			Invalidate();
			this.DataGridTableStyle.DataGrid.Scroll -= new EventHandler(HandleScroll);
			_hasSentKeyPress = false;

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets up the combo box with the correct selectedValue (according to the actual data), and displays it
		///		within the rectangle that would have been filled by the cell's Text Box control.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="bounds"></param>
		/// <param name="readOnly"></param>
		/// <param name="instantText"></param>
		/// <param name="cellIsVisible"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!_hasSentKeyPress)

			{
				// Send a key-down Windows Message to this cell's text box, to make the grid act as if the user was
				// typing normally.  This is to make sure that clicking in the 'new row' row still makes the grid
				// add a new 'new row' row underneath it.
				Sneaky(this.TextBox);

				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);

				_rowNum = rowNum;
				_source = source;
				_rowCount = _source.Count;

				// Position the combo box.
				ColumnComboBox.Parent = this.TextBox.Parent;
				ColumnComboBox.Location = this.TextBox.Location;
				ColumnComboBox.Size = new Size(this.TextBox.Size.Width, ColumnComboBox.Size.Height);

				// Set its selected index.
				ColumnComboBox.SelectedIndex = -1;

				IList dl = source.List;
				DataView dv = dl as DataView;
				if (dv != null)
				{
                    //foreach (ComboboxItem li in ColumnComboBox.Items)
                    foreach (System.Data.DataRowView li in ColumnComboBox.Items)
                    {
                        //if (li.value == dv[rowNum][this.MappingName])
                        if (li.Row[1] == dv[rowNum][this.MappingName])
                        {
                            ColumnComboBox.SelectedItem = li;
							break;
						}
					}
				}
				//ColumnComboBox.SelectedIndex = ColumnComboBox.FindStringExact(this.TextBox.Text);
				//ColumnComboBox.Text =  this.TextBox.Text;

				_selectedIndex = ColumnComboBox.SelectedIndex;

				if (_selectedIndex < 0 && ColumnComboBox.Items.Count > 0)
				{
					ColumnComboBox.SelectedIndex = 0;
					_selectedIndex = 0;
				}

				try
				{
					// Hide the text box, display the combo.
					this.TextBox.Visible = false;
					ColumnComboBox.Visible = true;

					// Handle scroll events on the grid, to hide the combo.
					this.DataGridTableStyle.DataGrid.Scroll += new EventHandler(HandleScroll);

					ColumnComboBox.BringToFront();
					ColumnComboBox.Focus();
				}
				catch
				{
				}
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the value to the dataset.
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="rowNum"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			if (_isEditing)
			{
				_isEditing = false;
				_hasSentKeyPress = false;

				if (ColumnComboBox.SelectedItem != null)
				{
                    //ComboboxItem item = (ComboboxItem)ColumnComboBox.SelectedItem;
                    DataRowView item = ColumnComboBox.SelectedItem as DataRowView;
                    //SetColumnValueAtRow(dataSource, rowNum, item.Value);
                    SetColumnValueAtRow(_source, _rowNum, item.Row[1]);
                }
            }
			return true;
		}

		protected override void ConcedeFocus()
		{
			_isEditing = false;
			_hasSentKeyPress = false;

			base.ConcedeFocus();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Instead of getting the value from the text box, get it from the selected item of the visible combo box.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <returns>Returns the selected value from the combo box.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override object GetColumnValueAtRow(CurrencyManager source, int rowNum)
		{
//			//return "OI!";
//			object s = base.GetColumnValueAtRow(source, rowNum);
//			DataView dv = (DataView) this.ColumnComboBox.DataSource;
//			int rowCount = dv.Count;
//			int i = 0;
//
//			//if things are slow, you could order your dataview
//			//& use binary search instead of this linear one
//			while (i < rowCount)
//			{
//				if (s.Equals(dv[i][this.ColumnComboBox.ValueMember]))
//					break;
//				++i;
//			}
//
//			if (i < rowCount)
//			{
//				object o = dv[i][this.ColumnComboBox.ValueMember];
//				return ((PackageItem) o);
//			}
//
//			return DBNull.Value;

			object s = base.GetColumnValueAtRow(source, rowNum);
			if (Convert.IsDBNull(s))
				return DBNull.Value;
			else
			{
				if (s is AnswerProvider)
				{
					PackageItem v = (PackageItem) s;
					return v;

				}
			}
			return DBNull.Value;
		}


		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Send a key-up message to a control (hWin).
		/// </summary>
		/// <param name="c"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void Sneaky(Control c)
		{
			_hasSentKeyPress = true;
			IntPtr h = c.Handle;
			int wParam = 0x97;
			uint lParam = 0x1E0001;
			WinMessage.SendMessage(h.ToInt32(), 0x102, wParam, lParam);
			_isEditing = true;
		}
	}
}