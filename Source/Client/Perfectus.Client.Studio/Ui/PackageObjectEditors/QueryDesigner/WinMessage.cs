using System.Runtime.InteropServices;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	/// <summary>
	///		Wraps calls to the WinAPI SendMessageA method.
	/// </summary>
	internal class WinMessage
	{
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sends a Windows message (messageId, wParam and lParam) to the specified window handle.
		/// </summary>
		/// <returns>Returns the message's result code.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		[DllImport("User32.dll", EntryPoint="SendMessageA")]
		internal static extern int SendMessage(
			int hWnd, // handle to destination window
			int Msg, // message
			int wParam, // first message parameter
			uint lParam // second message parameter
			);
	}
}