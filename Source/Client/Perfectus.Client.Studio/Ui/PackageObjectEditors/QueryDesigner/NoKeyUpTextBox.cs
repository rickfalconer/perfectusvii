using System;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	/// <summary>
	/// Summary description for NoKeyUpTextbox.
	/// </summary>
	public class NoKeyUpTextBox : TextBox
	{
		private const int WM_KEYUP = 0x101;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Intercept all messages received by this control, and pass all of them to the base handler except KeyUp,
		///		which is ignored.
		/// </summary>
		/// <param name="m">The incoming windows message.</param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_KEYUP)
			{
				//ignore keyup to avoid problem with tabbing & dropdownlist;
				return;
			}
			base.WndProc(ref m);
		}
	}
}
