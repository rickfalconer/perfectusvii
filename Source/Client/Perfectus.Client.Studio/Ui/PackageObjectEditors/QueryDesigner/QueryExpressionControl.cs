using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Syncfusion.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	/// <summary>
	/// Summary description for Expression.
	/// </summary>
	public class QueryExpressionControl : UserControl
	{
		private GroupBox groupBox1;
		private DataGrid grdExpression;
		private IContainer components;

		private DataTable booleanOperators;
		private DataTable leftParenthesisTable;
		private DataTable rightParenthesisTable;
		private DataTable leftOperandsTable;

		private DataTable expressionTable;
		private ImageList imageList1;
		private Button btnCheck;

		private QueryExpression queryExpressionBeingEdited;
		private Button btnLoadExpression;
		private TextBox txtExpression;
		private PopupControlContainer popupControlContainer1;
		private DataGrid grdExpressionLibrary;
		private DataGridTableStyle dataGridTableStyle1;
		private DataGridTextBoxColumn dataGridTextBoxColumn1;
		private DataGridTextBoxColumn dataGridTextBoxColumn2;
		private DataGridTextBoxColumn dataGridTextBoxColumn3;

		private Package package;

		public QueryExpression QueryExpressionBeingEdited
		{
			get { return queryExpressionBeingEdited; }
			set
			{
				queryExpressionBeingEdited = value;
				if (queryExpressionBeingEdited != null)
				{
					package = queryExpressionBeingEdited.ParentPackage;
					DataTable dt = queryExpressionBeingEdited.Table;
					if (dt != null)
					{
						expressionTable = queryExpressionBeingEdited.Table.Clone();
						foreach (DataRow dr in dt.Rows)
						{
							expressionTable.Rows.Add(dr.ItemArray);
						}
					}
					else
					{
						expressionTable = QueryExpression.CreateQueryTable();
					}
					grdExpression.DataSource = expressionTable;
					SetTableStyles();
					txtExpression.Text = queryExpressionBeingEdited.ToPreviewString();
				}
				else
				{
					package = null;
				}

				if (package != null)
				{
					ExpressionInfoCollection coll = package.GetAllExpressions();

					grdExpressionLibrary.DataSource = coll;
				}
			}
		}


		/// <summary>
		/// Constructor
		/// </summary>
		public QueryExpressionControl()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			booleanOperators = new DataTable("BooleanOperators");
			booleanOperators.Columns.Add("Display", typeof (OperatorDisplay));
			booleanOperators.Columns.Add("Value", typeof (OperatorDisplay));

			booleanOperators.Rows.Add(new object[] {null, DBNull.Value});
			foreach (Operator bo in PerfectusTypeSystem.GetBooleanOperators())
			{
				DataRow dr = booleanOperators.NewRow();
				dr["Display"] = new OperatorDisplay(bo);
				dr["Value"] = new OperatorDisplay(bo);
				booleanOperators.Rows.Add(dr);
			}

			leftParenthesisTable = new DataTable("LeftParenthesis");
			leftParenthesisTable.Columns.Add("Value");
			leftParenthesisTable.Columns.Add("Display");
			leftParenthesisTable.Rows.Add(new object[] {" "});
			leftParenthesisTable.Rows.Add(new object[] {"(", "("});

			rightParenthesisTable = new DataTable("RightParenthesis");
			rightParenthesisTable.Columns.Add("Value");
			rightParenthesisTable.Columns.Add("Display");
			rightParenthesisTable.Rows.Add(new object[] {" "});
			rightParenthesisTable.Rows.Add(new object[] {")", ")"});

			expressionTable = QueryExpression.CreateQueryTable();

			leftOperandsTable = new DataTable("LeftOperand");
			leftOperandsTable.Columns.Add("Display", typeof (string));
			leftOperandsTable.Columns.Add("Value", typeof (AnswerProvider));
			leftOperandsTable.Columns.Add("ImageIndex", typeof (Image));							
			leftOperandsTable.DefaultView.Sort = "Value ASC";

			//SetTableStyles();
			grdExpression.DataSource = expressionTable;

		}


		private Image GetImage(PackageItem pi)
		{
			if (pi is Question)
			{
				return imageList1.Images[0];
			}
			else if (pi is DatePFunction)
			{
				return imageList1.Images[1];
			}
			else if (pi is ArithmeticPFunction)
			{
				return imageList1.Images[2];
			}
			else
			{
				throw new NotSupportedException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.LeftOperand"));
			}

		}


		private void SetTableStyles()
		{
			leftOperandsTable.Rows.Clear();
			if (package != null)
			{
				foreach (PackageItem pi in PerfectusTypeSystem.GetCompatibleItems(PerfectusDataType.Date, package))
				{
					leftOperandsTable.Rows.Add(new object[] {pi.Name, pi, GetImage(pi)});
				}
				foreach (PackageItem pi in PerfectusTypeSystem.GetCompatibleItems(PerfectusDataType.Text, package))
				{
					leftOperandsTable.Rows.Add(new object[] {pi.Name, pi, GetImage(pi)});
				}
				foreach (PackageItem pi in PerfectusTypeSystem.GetCompatibleItems(PerfectusDataType.Number, package))
				{
					leftOperandsTable.Rows.Add(new object[] {pi.Name, pi, GetImage(pi)});
				}
				foreach (PackageItem pi in PerfectusTypeSystem.GetCompatibleItems(PerfectusDataType.YesNo, package))
				{
					leftOperandsTable.Rows.Add(new object[] {pi.Name, pi, GetImage(pi)});
				}
			}
			//Remove the existing style, if any.
			if (grdExpression.TableStyles.Count > 0)
				grdExpression.TableStyles.RemoveAt(0);

			// Create a table.
			DataGridTableStyle expressionTableStyle = new DataGridTableStyle(false);
			expressionTableStyle.MappingName = "Expression";
			expressionTableStyle.RowHeadersVisible = true;
			expressionTableStyle.AllowSorting = false;

			// Set up a generic column to play with.
			DataGridTextBoxColumn col;

			// The "Boolean Operator" drop-down column.
			// Create a view for the operators

			col = new OperatorComboColumn();
			((OperatorComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((OperatorComboColumn) col).ColumnComboBox.DataSource = booleanOperators.DefaultView;
			((OperatorComboColumn) col).ColumnComboBox.DisplayMember = "Display";
			((OperatorComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "Operator1";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.LogicColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((OperatorComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((OperatorComboColumn) col).ColumnComboBox.Width - 70;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Left Parenthesis" drop-down column.
			col = new ComboColumn();
			((ComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ComboColumn) col).ColumnComboBox.DataSource = leftParenthesisTable.DefaultView;
			((ComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((ComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "LeftParenthesis";
			col.HeaderText = "(";
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((ComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((ComboColumn) col).ColumnComboBox.Width - 73;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Left Operand" drop-down column.
			col = new ItemComboColumn(imageList1);
			((ItemComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ItemComboColumn) col).ColumnComboBox.DataSource = leftOperandsTable.DefaultView;
			((ItemComboColumn) col).ColumnComboBox.DisplayMember = "Display";
			((ItemComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "LeftOperand";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.LeftOperandColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredColumnWidth = 138;
			expressionTableStyle.GridColumnStyles.Add(col);

			col = new OperatorComboColumn("LeftOperand");
			((OperatorComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

			col.MappingName = "Operator2";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.OperatorColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((OperatorComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((OperatorComboColumn) col).ColumnComboBox.Width - 70;
			expressionTableStyle.GridColumnStyles.Add(col);


			// The "Right Operand" drop-down column.
			col = new ItemConstantColumn(package,PerfectusDataType.Text, "LeftOperand", null);			
			col.MappingName = "RightOperand";
			col.HeaderText = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.RightOperandColumnHeader");
			col.NullText = "";
			expressionTableStyle.PreferredColumnWidth = 150;
			expressionTableStyle.GridColumnStyles.Add(col);

			// The "Right Parenthesis" drop-down column.
			col = new ComboColumn();
			((ComboColumn) col).ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
			((ComboColumn) col).ColumnComboBox.DataSource = rightParenthesisTable.DefaultView;
			((ComboColumn) col).ColumnComboBox.DisplayMember = "Value";
			((ComboColumn) col).ColumnComboBox.ValueMember = "Value";
			col.MappingName = "RightParenthesis";
			col.HeaderText = ")";
			col.NullText = "";
			expressionTableStyle.PreferredRowHeight = ((ComboColumn) col).ColumnComboBox.Height + 2;
			expressionTableStyle.PreferredColumnWidth = ((ComboColumn) col).ColumnComboBox.Width - 73;
			expressionTableStyle.GridColumnStyles.Add(col);

			// Add our new style to the grids TableStyles collection.
			grdExpression.TableStyles.Add(expressionTableStyle);
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>		
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(QueryExpressionControl));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.popupControlContainer1 = new Syncfusion.Windows.Forms.PopupControlContainer(this.components);
			this.grdExpressionLibrary = new System.Windows.Forms.DataGrid();
			this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
			this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.txtExpression = new System.Windows.Forms.TextBox();
			this.btnLoadExpression = new System.Windows.Forms.Button();
			this.btnCheck = new System.Windows.Forms.Button();
			this.grdExpression = new System.Windows.Forms.DataGrid();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.groupBox1.SuspendLayout();
			this.popupControlContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grdExpressionLibrary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.grdExpression)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.AccessibleDescription = resources.GetString("groupBox1.AccessibleDescription");
			this.groupBox1.AccessibleName = resources.GetString("groupBox1.AccessibleName");
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("groupBox1.Anchor")));
			this.groupBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("groupBox1.BackgroundImage")));
			this.groupBox1.Controls.Add(this.popupControlContainer1);
			this.groupBox1.Controls.Add(this.txtExpression);
			this.groupBox1.Controls.Add(this.btnLoadExpression);
			this.groupBox1.Controls.Add(this.btnCheck);
			this.groupBox1.Controls.Add(this.grdExpression);
			this.groupBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("groupBox1.Dock")));
			this.groupBox1.Enabled = ((bool)(resources.GetObject("groupBox1.Enabled")));
			this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.groupBox1.Font = ((System.Drawing.Font)(resources.GetObject("groupBox1.Font")));
			this.groupBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("groupBox1.ImeMode")));
			this.groupBox1.Location = ((System.Drawing.Point)(resources.GetObject("groupBox1.Location")));
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("groupBox1.RightToLeft")));
			this.groupBox1.Size = ((System.Drawing.Size)(resources.GetObject("groupBox1.Size")));
			this.groupBox1.TabIndex = ((int)(resources.GetObject("groupBox1.TabIndex")));
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = resources.GetString("groupBox1.Text");
			this.groupBox1.Visible = ((bool)(resources.GetObject("groupBox1.Visible")));
			// 
			// popupControlContainer1
			// 
			this.popupControlContainer1.AccessibleDescription = resources.GetString("popupControlContainer1.AccessibleDescription");
			this.popupControlContainer1.AccessibleName = resources.GetString("popupControlContainer1.AccessibleName");
			this.popupControlContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("popupControlContainer1.Anchor")));
			this.popupControlContainer1.AutoScroll = ((bool)(resources.GetObject("popupControlContainer1.AutoScroll")));
			this.popupControlContainer1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("popupControlContainer1.AutoScrollMargin")));
			this.popupControlContainer1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("popupControlContainer1.AutoScrollMinSize")));
			this.popupControlContainer1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("popupControlContainer1.BackgroundImage")));
			this.popupControlContainer1.Controls.Add(this.grdExpressionLibrary);
			this.popupControlContainer1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("popupControlContainer1.Dock")));
			this.popupControlContainer1.Enabled = ((bool)(resources.GetObject("popupControlContainer1.Enabled")));
			this.popupControlContainer1.Font = ((System.Drawing.Font)(resources.GetObject("popupControlContainer1.Font")));
			this.popupControlContainer1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("popupControlContainer1.ImeMode")));
			this.popupControlContainer1.Location = ((System.Drawing.Point)(resources.GetObject("popupControlContainer1.Location")));
			this.popupControlContainer1.Name = "popupControlContainer1";
			this.popupControlContainer1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("popupControlContainer1.RightToLeft")));
			this.popupControlContainer1.Size = ((System.Drawing.Size)(resources.GetObject("popupControlContainer1.Size")));
			this.popupControlContainer1.TabIndex = ((int)(resources.GetObject("popupControlContainer1.TabIndex")));
			this.popupControlContainer1.Text = resources.GetString("popupControlContainer1.Text");
			this.popupControlContainer1.Visible = ((bool)(resources.GetObject("popupControlContainer1.Visible")));
			// 
			// grdExpressionLibrary
			// 
			this.grdExpressionLibrary.AccessibleDescription = resources.GetString("grdExpressionLibrary.AccessibleDescription");
			this.grdExpressionLibrary.AccessibleName = resources.GetString("grdExpressionLibrary.AccessibleName");
			this.grdExpressionLibrary.AllowNavigation = false;
			this.grdExpressionLibrary.AlternatingBackColor = System.Drawing.Color.WhiteSmoke;
			this.grdExpressionLibrary.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grdExpressionLibrary.Anchor")));
			this.grdExpressionLibrary.BackColor = System.Drawing.Color.White;
			this.grdExpressionLibrary.BackgroundColor = System.Drawing.SystemColors.Window;
			this.grdExpressionLibrary.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grdExpressionLibrary.BackgroundImage")));
			this.grdExpressionLibrary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.grdExpressionLibrary.CaptionBackColor = System.Drawing.SystemColors.InactiveCaption;
			this.grdExpressionLibrary.CaptionFont = ((System.Drawing.Font)(resources.GetObject("grdExpressionLibrary.CaptionFont")));
			this.grdExpressionLibrary.CaptionForeColor = System.Drawing.SystemColors.InactiveCaptionText;
			this.grdExpressionLibrary.CaptionText = resources.GetString("grdExpressionLibrary.CaptionText");
			this.grdExpressionLibrary.DataMember = "";
			this.grdExpressionLibrary.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grdExpressionLibrary.Dock")));
			this.grdExpressionLibrary.Enabled = ((bool)(resources.GetObject("grdExpressionLibrary.Enabled")));
			this.grdExpressionLibrary.FlatMode = true;
			this.grdExpressionLibrary.Font = ((System.Drawing.Font)(resources.GetObject("grdExpressionLibrary.Font")));
			this.grdExpressionLibrary.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.grdExpressionLibrary.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grdExpressionLibrary.ImeMode")));
			this.grdExpressionLibrary.Location = ((System.Drawing.Point)(resources.GetObject("grdExpressionLibrary.Location")));
			this.grdExpressionLibrary.Name = "grdExpressionLibrary";
			this.grdExpressionLibrary.ParentRowsVisible = false;
			this.grdExpressionLibrary.ReadOnly = true;
			this.grdExpressionLibrary.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grdExpressionLibrary.RightToLeft")));
			this.grdExpressionLibrary.RowHeadersVisible = false;
			this.grdExpressionLibrary.Size = ((System.Drawing.Size)(resources.GetObject("grdExpressionLibrary.Size")));
			this.grdExpressionLibrary.TabIndex = ((int)(resources.GetObject("grdExpressionLibrary.TabIndex")));
			this.grdExpressionLibrary.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																											 this.dataGridTableStyle1});
			this.grdExpressionLibrary.Visible = ((bool)(resources.GetObject("grdExpressionLibrary.Visible")));
			this.grdExpressionLibrary.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdExpressionLibrary_MouseUp);
			this.grdExpressionLibrary.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grdExpressionLibrary_MouseMove);
			// 
			// dataGridTableStyle1
			// 
			this.dataGridTableStyle1.AlternatingBackColor = System.Drawing.Color.WhiteSmoke;
			this.dataGridTableStyle1.BackColor = System.Drawing.Color.White;
			this.dataGridTableStyle1.DataGrid = this.grdExpressionLibrary;
			this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																												  this.dataGridTextBoxColumn1,
																												  this.dataGridTextBoxColumn2,
																												  this.dataGridTextBoxColumn3});
			this.dataGridTableStyle1.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None;
			this.dataGridTableStyle1.HeaderFont = ((System.Drawing.Font)(resources.GetObject("dataGridTableStyle1.HeaderFont")));
			this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridTableStyle1.MappingName = "ExpressionInfoCollection";
			this.dataGridTableStyle1.PreferredColumnWidth = ((int)(resources.GetObject("dataGridTableStyle1.PreferredColumnWidth")));
			this.dataGridTableStyle1.PreferredRowHeight = ((int)(resources.GetObject("dataGridTableStyle1.PreferredRowHeight")));
			this.dataGridTableStyle1.ReadOnly = true;
			this.dataGridTableStyle1.RowHeadersVisible = false;
			this.dataGridTableStyle1.RowHeaderWidth = ((int)(resources.GetObject("dataGridTableStyle1.RowHeaderWidth")));
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.Alignment = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("dataGridTextBoxColumn1.Alignment")));
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.HeaderText = resources.GetString("dataGridTextBoxColumn1.HeaderText");
			this.dataGridTextBoxColumn1.MappingName = resources.GetString("dataGridTextBoxColumn1.MappingName");
			this.dataGridTextBoxColumn1.NullText = resources.GetString("dataGridTextBoxColumn1.NullText");
			this.dataGridTextBoxColumn1.Width = ((int)(resources.GetObject("dataGridTextBoxColumn1.Width")));
			// 
			// dataGridTextBoxColumn2
			// 
			this.dataGridTextBoxColumn2.Alignment = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("dataGridTextBoxColumn2.Alignment")));
			this.dataGridTextBoxColumn2.Format = "";
			this.dataGridTextBoxColumn2.FormatInfo = null;
			this.dataGridTextBoxColumn2.HeaderText = resources.GetString("dataGridTextBoxColumn2.HeaderText");
			this.dataGridTextBoxColumn2.MappingName = resources.GetString("dataGridTextBoxColumn2.MappingName");
			this.dataGridTextBoxColumn2.NullText = resources.GetString("dataGridTextBoxColumn2.NullText");
			this.dataGridTextBoxColumn2.Width = ((int)(resources.GetObject("dataGridTextBoxColumn2.Width")));
			// 
			// dataGridTextBoxColumn3
			// 
			this.dataGridTextBoxColumn3.Alignment = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("dataGridTextBoxColumn3.Alignment")));
			this.dataGridTextBoxColumn3.Format = "";
			this.dataGridTextBoxColumn3.FormatInfo = null;
			this.dataGridTextBoxColumn3.HeaderText = resources.GetString("dataGridTextBoxColumn3.HeaderText");
			this.dataGridTextBoxColumn3.MappingName = resources.GetString("dataGridTextBoxColumn3.MappingName");
			this.dataGridTextBoxColumn3.NullText = resources.GetString("dataGridTextBoxColumn3.NullText");
			this.dataGridTextBoxColumn3.Width = ((int)(resources.GetObject("dataGridTextBoxColumn3.Width")));
			// 
			// txtExpression
			// 
			this.txtExpression.AccessibleDescription = resources.GetString("txtExpression.AccessibleDescription");
			this.txtExpression.AccessibleName = resources.GetString("txtExpression.AccessibleName");
			this.txtExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("txtExpression.Anchor")));
			this.txtExpression.AutoSize = ((bool)(resources.GetObject("txtExpression.AutoSize")));
			this.txtExpression.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtExpression.BackgroundImage")));
			this.txtExpression.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtExpression.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("txtExpression.Dock")));
			this.txtExpression.Enabled = ((bool)(resources.GetObject("txtExpression.Enabled")));
			this.txtExpression.Font = ((System.Drawing.Font)(resources.GetObject("txtExpression.Font")));
			this.txtExpression.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("txtExpression.ImeMode")));
			this.txtExpression.Location = ((System.Drawing.Point)(resources.GetObject("txtExpression.Location")));
			this.txtExpression.MaxLength = ((int)(resources.GetObject("txtExpression.MaxLength")));
			this.txtExpression.Multiline = ((bool)(resources.GetObject("txtExpression.Multiline")));
			this.txtExpression.Name = "txtExpression";
			this.txtExpression.PasswordChar = ((char)(resources.GetObject("txtExpression.PasswordChar")));
			this.txtExpression.ReadOnly = true;
			this.txtExpression.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("txtExpression.RightToLeft")));
			this.txtExpression.ScrollBars = ((System.Windows.Forms.ScrollBars)(resources.GetObject("txtExpression.ScrollBars")));
			this.txtExpression.Size = ((System.Drawing.Size)(resources.GetObject("txtExpression.Size")));
			this.txtExpression.TabIndex = ((int)(resources.GetObject("txtExpression.TabIndex")));
			this.txtExpression.Text = resources.GetString("txtExpression.Text");
			this.txtExpression.TextAlign = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("txtExpression.TextAlign")));
			this.txtExpression.Visible = ((bool)(resources.GetObject("txtExpression.Visible")));
			this.txtExpression.WordWrap = ((bool)(resources.GetObject("txtExpression.WordWrap")));
			// 
			// btnLoadExpression
			// 
			this.btnLoadExpression.AccessibleDescription = resources.GetString("btnLoadExpression.AccessibleDescription");
			this.btnLoadExpression.AccessibleName = resources.GetString("btnLoadExpression.AccessibleName");
			this.btnLoadExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnLoadExpression.Anchor")));
			this.btnLoadExpression.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLoadExpression.BackgroundImage")));
			this.btnLoadExpression.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnLoadExpression.Dock")));
			this.btnLoadExpression.Enabled = ((bool)(resources.GetObject("btnLoadExpression.Enabled")));
			this.btnLoadExpression.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnLoadExpression.FlatStyle")));
			this.btnLoadExpression.Font = ((System.Drawing.Font)(resources.GetObject("btnLoadExpression.Font")));
			this.btnLoadExpression.Image = ((System.Drawing.Image)(resources.GetObject("btnLoadExpression.Image")));
			this.btnLoadExpression.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnLoadExpression.ImageAlign")));
			this.btnLoadExpression.ImageIndex = ((int)(resources.GetObject("btnLoadExpression.ImageIndex")));
			this.btnLoadExpression.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnLoadExpression.ImeMode")));
			this.btnLoadExpression.Location = ((System.Drawing.Point)(resources.GetObject("btnLoadExpression.Location")));
			this.btnLoadExpression.Name = "btnLoadExpression";
			this.btnLoadExpression.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnLoadExpression.RightToLeft")));
			this.btnLoadExpression.Size = ((System.Drawing.Size)(resources.GetObject("btnLoadExpression.Size")));
			this.btnLoadExpression.TabIndex = ((int)(resources.GetObject("btnLoadExpression.TabIndex")));
			this.btnLoadExpression.Text = resources.GetString("btnLoadExpression.Text");
			this.btnLoadExpression.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnLoadExpression.TextAlign")));
			this.btnLoadExpression.Visible = ((bool)(resources.GetObject("btnLoadExpression.Visible")));
			this.btnLoadExpression.Click += new System.EventHandler(this.btnLoadExpression_Click);
			// 
			// btnCheck
			// 
			this.btnCheck.AccessibleDescription = resources.GetString("btnCheck.AccessibleDescription");
			this.btnCheck.AccessibleName = resources.GetString("btnCheck.AccessibleName");
			this.btnCheck.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCheck.Anchor")));
			this.btnCheck.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCheck.BackgroundImage")));
			this.btnCheck.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCheck.Dock")));
			this.btnCheck.Enabled = ((bool)(resources.GetObject("btnCheck.Enabled")));
			this.btnCheck.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCheck.FlatStyle")));
			this.btnCheck.Font = ((System.Drawing.Font)(resources.GetObject("btnCheck.Font")));
			this.btnCheck.Image = ((System.Drawing.Image)(resources.GetObject("btnCheck.Image")));
			this.btnCheck.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCheck.ImageAlign")));
			this.btnCheck.ImageIndex = ((int)(resources.GetObject("btnCheck.ImageIndex")));
			this.btnCheck.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCheck.ImeMode")));
			this.btnCheck.Location = ((System.Drawing.Point)(resources.GetObject("btnCheck.Location")));
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCheck.RightToLeft")));
			this.btnCheck.Size = ((System.Drawing.Size)(resources.GetObject("btnCheck.Size")));
			this.btnCheck.TabIndex = ((int)(resources.GetObject("btnCheck.TabIndex")));
			this.btnCheck.Text = resources.GetString("btnCheck.Text");
			this.btnCheck.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCheck.TextAlign")));
			this.btnCheck.Visible = ((bool)(resources.GetObject("btnCheck.Visible")));
			this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
			// 
			// grdExpression
			// 
			this.grdExpression.AccessibleDescription = resources.GetString("grdExpression.AccessibleDescription");
			this.grdExpression.AccessibleName = resources.GetString("grdExpression.AccessibleName");
			this.grdExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grdExpression.Anchor")));
			this.grdExpression.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grdExpression.BackgroundImage")));
			this.grdExpression.CaptionFont = ((System.Drawing.Font)(resources.GetObject("grdExpression.CaptionFont")));
			this.grdExpression.CaptionText = resources.GetString("grdExpression.CaptionText");
			this.grdExpression.CaptionVisible = false;
			this.grdExpression.DataMember = "";
			this.grdExpression.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grdExpression.Dock")));
			this.grdExpression.Enabled = ((bool)(resources.GetObject("grdExpression.Enabled")));
			this.grdExpression.Font = ((System.Drawing.Font)(resources.GetObject("grdExpression.Font")));
			this.grdExpression.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.grdExpression.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grdExpression.ImeMode")));
			this.grdExpression.Location = ((System.Drawing.Point)(resources.GetObject("grdExpression.Location")));
			this.grdExpression.Name = "grdExpression";
			this.grdExpression.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grdExpression.RightToLeft")));
			this.grdExpression.Size = ((System.Drawing.Size)(resources.GetObject("grdExpression.Size")));
			this.grdExpression.TabIndex = ((int)(resources.GetObject("grdExpression.TabIndex")));
			this.grdExpression.Visible = ((bool)(resources.GetObject("grdExpression.Visible")));
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = ((System.Drawing.Size)(resources.GetObject("imageList1.ImageSize")));
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// QueryExpressionControl
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.Controls.Add(this.groupBox1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.Name = "QueryExpressionControl";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.Size = ((System.Drawing.Size)(resources.GetObject("$this.Size")));
			this.groupBox1.ResumeLayout(false);
			this.popupControlContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.grdExpressionLibrary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.grdExpression)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private void btnCheck_Click(object sender, EventArgs e)
		{
			CheckExpressionTable();
		}


		public bool ValidateExpression(out string Message)
		{
			return (queryExpressionBeingEdited.ValidateFromTable(this.expressionTable, out Message));
		}


		public void Commit()
		{
			queryExpressionBeingEdited.SaveDataTable(this.expressionTable);
		}


		private void CheckExpressionTable()
		{
			if (queryExpressionBeingEdited != null)
			{
				string Message;
				bool isOk = queryExpressionBeingEdited.ValidateFromTable(this.expressionTable, out Message);
				if (!isOk)
				{
					string msg = Message.Replace("\\n", "\n");
					msg = msg.Replace("\\t", "\t");
					MessageBox.Show(msg, Common.About.FormsTitle);
				}
				else
				{
					string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl.ExpressionOkayMessage");
					msg = msg.Replace("\\n", "\n");
					msg = msg.Replace("\\t", "\t");
					MessageBox.Show(msg, Common.About.FormsTitle);
				}
			}
		}

		private void btnLoadExpression_Click(object sender, EventArgs e)
		{
			popupControlContainer1.Width = Width - 40;
			popupControlContainer1.ShowPopup(PointToScreen(new Point(btnLoadExpression.Left + 5, btnLoadExpression.Bottom)));
			popupControlContainer1.Focus();
		}

		private void grdExpressionLibrary_MouseMove(object sender, MouseEventArgs e)
		{
			DataGrid.HitTestInfo hti = grdExpressionLibrary.HitTest(e.X, e.Y);
			if (hti.Type == DataGrid.HitTestType.Cell)
			{
				int rowCount = BindingContext[grdExpressionLibrary.DataSource].Count;
				for (int i = 0; i < rowCount; i++)
				{
					grdExpressionLibrary.UnSelect(i);
				}
				grdExpressionLibrary.Select(hti.Row);
			}
		}

		private void ApplyExpressionInfo(ExpressionInfo info)
		{
			this.QueryExpressionBeingEdited = (QueryExpression) (info.Expression.Clone());
		}

		private void grdExpressionLibrary_MouseUp(object sender, MouseEventArgs e)
		{
			DataGrid.HitTestInfo hti = grdExpressionLibrary.HitTest(e.X, e.Y);
			if (hti.Type == DataGrid.HitTestType.Cell)
			{
				if (hti.Row == grdExpressionLibrary.CurrentRowIndex)
				{
					ExpressionInfo info = ((ExpressionInfoCollection) (grdExpressionLibrary.DataSource))[hti.Row];
					if (info != null)
					{
						ApplyExpressionInfo(info);
						popupControlContainer1.HidePopup();
					}
				}
			}


		}

	}
}