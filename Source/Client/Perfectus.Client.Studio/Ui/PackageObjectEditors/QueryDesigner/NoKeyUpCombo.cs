using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	// SEE: http://www.syncfusion.com/FAQ/WinForms/FAQ_c44c.asp#q480q for this and ComboColumn2
	/// <summary>
	///		A derived ComboBox control that ignores KeyUp evets, for use in the ComboBox and ItemPicker DataGridColumns.
	/// </summary>
	public class NoKeyUpCombo : ComboBox
	{
		private const int WM_KEYUP = 0x101;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Intercept all messages received by this control, and pass all of them to the base handler except KeyUp,
		///		which is ignored.
		/// </summary>
		/// <param name="m">The incoming windows message.</param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_KEYUP)
			{
				//ignore keyup to avoid problem with tabbing & dropdownlist;
				return;
			}
			base.WndProc(ref m);
		}
	}
}