using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
    
	/// <summary>
	/// Summary description for QueryDesigner.
	/// </summary>
    public class QueryDesignerForm : Form, IQueryDesigner
	{
		private bool okayToClose = true;

		private QueryExpressionControl expression1;
		private Button btnOk;
		private Button btnCancel;
		private GroupBox gbxActionIfFalse;
		private GroupBox gbxActionIfTrue;
		private CheckBox checkBox1;
		private ActionHostControl actionHostFalse;
		private ActionHostControl actionHostTrue;
		private Query queryBeingEdited;
		private PackageItem itemBeingEdited;
        private Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.QueryDesignerMode mode;

		public Query QueryBeingEdited
		{
			get { return queryBeingEdited; }
			set
			{
				queryBeingEdited = value;
				if (queryBeingEdited != null)
				{
					expression1.QueryExpressionBeingEdited = queryBeingEdited.QueryExpression;
				}
				else
				{
					expression1.QueryExpressionBeingEdited = null;
				}
			}
		}

		public PackageItem ItemBeingEdited
		{
			get { return itemBeingEdited; }
			set { itemBeingEdited = value; }
		}

        public QueryDesignerMode Mode   
		{
			get { return mode; }
			set
			{
				mode = value;
				ResetUI();
			}
		}

		private void ResetUI()
		{
			switch (mode)
			{
				case QueryDesignerMode.Navigation:
					NavigationActionControl navTrue = new NavigationActionControl();
					NavigationActionControl navFalse = new NavigationActionControl();

					navTrue.PageBeingEdited = (InterviewPage) itemBeingEdited;
					navFalse.PageBeingEdited = (InterviewPage) itemBeingEdited;

					if (queryBeingEdited.ActionIfTrue == null)
					{
						queryBeingEdited.ActionIfTrue = new NavigationAction();
					}

					if (queryBeingEdited.ActionIfFalse == null)
					{
						queryBeingEdited.ActionIfFalse = new NavigationAction();
					}

					actionHostTrue.HostedControl = navTrue;
					actionHostFalse.HostedControl = navFalse;

					actionHostTrue.Action = queryBeingEdited.ActionIfTrue;
					actionHostFalse.Action = queryBeingEdited.ActionIfFalse;

					break;

				case QueryDesignerMode.Outcome:
					OutcomeActionControl outcomeTrue = new OutcomeActionControl();
					OutcomeActionControl outcomeFalse = new OutcomeActionControl();

					outcomeTrue.OutcomeBeingEdited = (Outcome) itemBeingEdited;
					outcomeFalse.OutcomeBeingEdited = (Outcome) itemBeingEdited;

					if (queryBeingEdited.ActionIfTrue == null)
					{
						queryBeingEdited.ActionIfTrue = new OutcomeAction();
					}
					if (queryBeingEdited.ActionIfFalse == null)
					{
						queryBeingEdited.ActionIfFalse = new OutcomeAction();
					}

					actionHostTrue.HostedControl = outcomeTrue;
					actionHostFalse.HostedControl = outcomeFalse;

					actionHostTrue.Action = queryBeingEdited.ActionIfTrue;
					actionHostFalse.Action = queryBeingEdited.ActionIfFalse;

					break;

				case QueryDesignerMode.YesNo:
					Height = expression1.Bottom + 65;
					gbxActionIfTrue.Hide();
					gbxActionIfFalse.Hide();
					actionHostTrue.Hide();
					actionHostFalse.Hide();
					return;


			}

			actionHostTrue.Package = itemBeingEdited.ParentPackage;
			actionHostFalse.Package = itemBeingEdited.ParentPackage;

			actionHostTrue.Mode = mode;
			actionHostFalse.Mode = mode;

			actionHostTrue.ItemBeingEdited = itemBeingEdited;
			actionHostFalse.ItemBeingEdited = itemBeingEdited;
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public QueryDesignerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			actionHostTrue.SubqueryEdit += new CancelEventHandler(actionHostTrue_SubqueryEdit);
			actionHostFalse.SubqueryEdit += new CancelEventHandler(actionHostTrue_SubqueryEdit);

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(QueryDesignerForm));
			this.expression1 = new Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.QueryExpressionControl();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.gbxActionIfFalse = new System.Windows.Forms.GroupBox();
			this.actionHostFalse = new Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls.ActionHostControl();
			this.gbxActionIfTrue = new System.Windows.Forms.GroupBox();
			this.actionHostTrue = new Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls.ActionHostControl();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.gbxActionIfFalse.SuspendLayout();
			this.gbxActionIfTrue.SuspendLayout();
			this.SuspendLayout();
			// 
			// expression1
			// 
			this.expression1.AccessibleDescription = resources.GetString("expression1.AccessibleDescription");
			this.expression1.AccessibleName = resources.GetString("expression1.AccessibleName");
			this.expression1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("expression1.Anchor")));
			this.expression1.AutoScroll = ((bool)(resources.GetObject("expression1.AutoScroll")));
			this.expression1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("expression1.AutoScrollMargin")));
			this.expression1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("expression1.AutoScrollMinSize")));
			this.expression1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("expression1.BackgroundImage")));
			this.expression1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("expression1.Dock")));
			this.expression1.Enabled = ((bool)(resources.GetObject("expression1.Enabled")));
			this.expression1.Font = ((System.Drawing.Font)(resources.GetObject("expression1.Font")));
			this.expression1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("expression1.ImeMode")));
			this.expression1.Location = ((System.Drawing.Point)(resources.GetObject("expression1.Location")));
			this.expression1.Name = "expression1";
			this.expression1.QueryExpressionBeingEdited = null;
			this.expression1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("expression1.RightToLeft")));
			this.expression1.Size = ((System.Drawing.Size)(resources.GetObject("expression1.Size")));
			this.expression1.TabIndex = ((int)(resources.GetObject("expression1.TabIndex")));
			this.expression1.Visible = ((bool)(resources.GetObject("expression1.Visible")));
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// gbxActionIfFalse
			// 
			this.gbxActionIfFalse.AccessibleDescription = resources.GetString("gbxActionIfFalse.AccessibleDescription");
			this.gbxActionIfFalse.AccessibleName = resources.GetString("gbxActionIfFalse.AccessibleName");
			this.gbxActionIfFalse.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("gbxActionIfFalse.Anchor")));
			this.gbxActionIfFalse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gbxActionIfFalse.BackgroundImage")));
			this.gbxActionIfFalse.Controls.Add(this.actionHostFalse);
			this.gbxActionIfFalse.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("gbxActionIfFalse.Dock")));
			this.gbxActionIfFalse.Enabled = ((bool)(resources.GetObject("gbxActionIfFalse.Enabled")));
			this.gbxActionIfFalse.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.gbxActionIfFalse.Font = ((System.Drawing.Font)(resources.GetObject("gbxActionIfFalse.Font")));
			this.gbxActionIfFalse.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("gbxActionIfFalse.ImeMode")));
			this.gbxActionIfFalse.Location = ((System.Drawing.Point)(resources.GetObject("gbxActionIfFalse.Location")));
			this.gbxActionIfFalse.Name = "gbxActionIfFalse";
			this.gbxActionIfFalse.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("gbxActionIfFalse.RightToLeft")));
			this.gbxActionIfFalse.Size = ((System.Drawing.Size)(resources.GetObject("gbxActionIfFalse.Size")));
			this.gbxActionIfFalse.TabIndex = ((int)(resources.GetObject("gbxActionIfFalse.TabIndex")));
			this.gbxActionIfFalse.TabStop = false;
			this.gbxActionIfFalse.Text = resources.GetString("gbxActionIfFalse.Text");
			this.gbxActionIfFalse.Visible = ((bool)(resources.GetObject("gbxActionIfFalse.Visible")));
			// 
			// actionHostFalse
			// 
			this.actionHostFalse.AccessibleDescription = resources.GetString("actionHostFalse.AccessibleDescription");
			this.actionHostFalse.AccessibleName = resources.GetString("actionHostFalse.AccessibleName");
			this.actionHostFalse.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("actionHostFalse.Anchor")));
			this.actionHostFalse.AutoScroll = ((bool)(resources.GetObject("actionHostFalse.AutoScroll")));
			this.actionHostFalse.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("actionHostFalse.AutoScrollMargin")));
			this.actionHostFalse.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("actionHostFalse.AutoScrollMinSize")));
			this.actionHostFalse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("actionHostFalse.BackgroundImage")));
			this.actionHostFalse.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("actionHostFalse.Dock")));
			this.actionHostFalse.Enabled = ((bool)(resources.GetObject("actionHostFalse.Enabled")));
			this.actionHostFalse.Font = ((System.Drawing.Font)(resources.GetObject("actionHostFalse.Font")));
			this.actionHostFalse.HostedControl = null;
			this.actionHostFalse.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("actionHostFalse.ImeMode")));
			this.actionHostFalse.ItemBeingEdited = null;
			this.actionHostFalse.Location = ((System.Drawing.Point)(resources.GetObject("actionHostFalse.Location")));
			this.actionHostFalse.Mode = Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.QueryDesignerMode.Navigation;
			this.actionHostFalse.Name = "actionHostFalse";
			this.actionHostFalse.Package = null;
			this.actionHostFalse.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("actionHostFalse.RightToLeft")));
			this.actionHostFalse.Size = ((System.Drawing.Size)(resources.GetObject("actionHostFalse.Size")));
			this.actionHostFalse.TabIndex = ((int)(resources.GetObject("actionHostFalse.TabIndex")));
			this.actionHostFalse.Visible = ((bool)(resources.GetObject("actionHostFalse.Visible")));
			// 
			// gbxActionIfTrue
			// 
			this.gbxActionIfTrue.AccessibleDescription = resources.GetString("gbxActionIfTrue.AccessibleDescription");
			this.gbxActionIfTrue.AccessibleName = resources.GetString("gbxActionIfTrue.AccessibleName");
			this.gbxActionIfTrue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("gbxActionIfTrue.Anchor")));
			this.gbxActionIfTrue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gbxActionIfTrue.BackgroundImage")));
			this.gbxActionIfTrue.Controls.Add(this.actionHostTrue);
			this.gbxActionIfTrue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("gbxActionIfTrue.Dock")));
			this.gbxActionIfTrue.Enabled = ((bool)(resources.GetObject("gbxActionIfTrue.Enabled")));
			this.gbxActionIfTrue.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.gbxActionIfTrue.Font = ((System.Drawing.Font)(resources.GetObject("gbxActionIfTrue.Font")));
			this.gbxActionIfTrue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("gbxActionIfTrue.ImeMode")));
			this.gbxActionIfTrue.Location = ((System.Drawing.Point)(resources.GetObject("gbxActionIfTrue.Location")));
			this.gbxActionIfTrue.Name = "gbxActionIfTrue";
			this.gbxActionIfTrue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("gbxActionIfTrue.RightToLeft")));
			this.gbxActionIfTrue.Size = ((System.Drawing.Size)(resources.GetObject("gbxActionIfTrue.Size")));
			this.gbxActionIfTrue.TabIndex = ((int)(resources.GetObject("gbxActionIfTrue.TabIndex")));
			this.gbxActionIfTrue.TabStop = false;
			this.gbxActionIfTrue.Text = resources.GetString("gbxActionIfTrue.Text");
			this.gbxActionIfTrue.Visible = ((bool)(resources.GetObject("gbxActionIfTrue.Visible")));
			// 
			// actionHostTrue
			// 
			this.actionHostTrue.AccessibleDescription = resources.GetString("actionHostTrue.AccessibleDescription");
			this.actionHostTrue.AccessibleName = resources.GetString("actionHostTrue.AccessibleName");
			this.actionHostTrue.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("actionHostTrue.Anchor")));
			this.actionHostTrue.AutoScroll = ((bool)(resources.GetObject("actionHostTrue.AutoScroll")));
			this.actionHostTrue.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("actionHostTrue.AutoScrollMargin")));
			this.actionHostTrue.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("actionHostTrue.AutoScrollMinSize")));
			this.actionHostTrue.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("actionHostTrue.BackgroundImage")));
			this.actionHostTrue.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("actionHostTrue.Dock")));
			this.actionHostTrue.Enabled = ((bool)(resources.GetObject("actionHostTrue.Enabled")));
			this.actionHostTrue.Font = ((System.Drawing.Font)(resources.GetObject("actionHostTrue.Font")));
			this.actionHostTrue.HostedControl = null;
			this.actionHostTrue.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("actionHostTrue.ImeMode")));
			this.actionHostTrue.ItemBeingEdited = null;
			this.actionHostTrue.Location = ((System.Drawing.Point)(resources.GetObject("actionHostTrue.Location")));
			this.actionHostTrue.Mode = Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.QueryDesignerMode.Navigation;
			this.actionHostTrue.Name = "actionHostTrue";
			this.actionHostTrue.Package = null;
			this.actionHostTrue.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("actionHostTrue.RightToLeft")));
			this.actionHostTrue.Size = ((System.Drawing.Size)(resources.GetObject("actionHostTrue.Size")));
			this.actionHostTrue.TabIndex = ((int)(resources.GetObject("actionHostTrue.TabIndex")));
			this.actionHostTrue.Visible = ((bool)(resources.GetObject("actionHostTrue.Visible")));
			// 
			// checkBox1
			// 
			this.checkBox1.AccessibleDescription = resources.GetString("checkBox1.AccessibleDescription");
			this.checkBox1.AccessibleName = resources.GetString("checkBox1.AccessibleName");
			this.checkBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("checkBox1.Anchor")));
			this.checkBox1.Appearance = ((System.Windows.Forms.Appearance)(resources.GetObject("checkBox1.Appearance")));
			this.checkBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("checkBox1.BackgroundImage")));
			this.checkBox1.CheckAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.CheckAlign")));
			this.checkBox1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("checkBox1.Dock")));
			this.checkBox1.Enabled = ((bool)(resources.GetObject("checkBox1.Enabled")));
			this.checkBox1.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("checkBox1.FlatStyle")));
			this.checkBox1.Font = ((System.Drawing.Font)(resources.GetObject("checkBox1.Font")));
			this.checkBox1.Image = ((System.Drawing.Image)(resources.GetObject("checkBox1.Image")));
			this.checkBox1.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.ImageAlign")));
			this.checkBox1.ImageIndex = ((int)(resources.GetObject("checkBox1.ImageIndex")));
			this.checkBox1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("checkBox1.ImeMode")));
			this.checkBox1.Location = ((System.Drawing.Point)(resources.GetObject("checkBox1.Location")));
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("checkBox1.RightToLeft")));
			this.checkBox1.Size = ((System.Drawing.Size)(resources.GetObject("checkBox1.Size")));
			this.checkBox1.TabIndex = ((int)(resources.GetObject("checkBox1.TabIndex")));
			this.checkBox1.Text = resources.GetString("checkBox1.Text");
			this.checkBox1.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("checkBox1.TextAlign")));
			this.checkBox1.Visible = ((bool)(resources.GetObject("checkBox1.Visible")));
			// 
			// QueryDesignerForm
			// 
			this.AcceptButton = this.btnOk;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.gbxActionIfTrue);
			this.Controls.Add(this.gbxActionIfFalse);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.expression1);
			this.Controls.Add(this.checkBox1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "QueryDesignerForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.Closing += new System.ComponentModel.CancelEventHandler(this.QueryDesignerForm_Closing);
			this.Load += new System.EventHandler(this.QueryDesignerForm_Load);
            this.FormClosed += new FormClosedEventHandler(QueryDesignerForm_FormClosed);
			this.gbxActionIfFalse.ResumeLayout(false);
			this.gbxActionIfTrue.ResumeLayout(false);
			this.ResumeLayout(false);

		}
#endregion

		private void btnOk_Click(object sender, EventArgs e)
		{
			SaveSelf();
		}

		private void SaveSelf()
		{
			string message = null;
			bool expressionOk = expression1.ValidateExpression(out message);
			if (!expressionOk)
			{
				string msg = message;
				msg = msg.Replace("\\n", "\n");
				msg = msg.Replace("\\t", "\t");
				MessageBox.Show(msg, Common.About.FormsTitle);
				okayToClose = false;
			}
			else
			{
				if ((actionHostTrue.IsSubqueryTabVisible && actionHostTrue.Action.SubQuery == null) ||
					(actionHostFalse.IsSubqueryTabVisible && actionHostFalse.Action.SubQuery == null)
					)
				{
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.QueryDesigner.QueryDesignerForm.NoSubqueryMessage"), Common.About.FormsTitle);
					okayToClose = false;
				}
				else
				{
					if (mode != QueryDesignerMode.YesNo)
					{
						actionHostFalse.Commit();
						actionHostTrue.Commit();
					}
					expression1.Commit();

					// This may have changed if an existing query expression was reused (cloned)					
					queryBeingEdited.QueryExpression = expression1.QueryExpressionBeingEdited;

					okayToClose = true;
				}
			}
		}

		private void QueryDesignerForm_Closing(object sender, CancelEventArgs e)
		{
			if (!okayToClose)
			{
				e.Cancel = true;
			}
			else
			{
				e.Cancel = false;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			okayToClose = true;
		}

		private void actionHostTrue_SubqueryEdit(object sender, CancelEventArgs e)
		{
			string message = null;
			bool expressionOk = expression1.ValidateExpression(out message);
			if (!expressionOk)
			{
				string msg = message.Replace("\\n", "\n");
				msg = msg.Replace("\\t", "\t");
				MessageBox.Show(msg, Common.About.FormsTitle);
				e.Cancel = true;
			}
			else
			{
				e.Cancel = false;
			}
		}

		private void QueryDesignerForm_Load(object sender, System.EventArgs e)
		{
			this.BringToFront();
			this.Focus();
		}

        void QueryDesignerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // FB1081
            // This is an ugly work-around. Actually, minimizing and maximizing 
            // solved the issue...
            Form mainForm = this.Owner;

            if (mode == QueryDesignerMode.Outcome && null != mainForm)
            {
                FormWindowState currState = mainForm.WindowState;
                mainForm.WindowState = FormWindowState.Minimized;
                mainForm.WindowState = currState;
            }
        }
    }
}

