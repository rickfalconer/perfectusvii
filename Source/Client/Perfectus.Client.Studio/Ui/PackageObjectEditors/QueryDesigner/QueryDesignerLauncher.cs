using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Configuration;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{

    public static class Launch
    {

        public static IQueryDesigner CreateQueryDesignerForm(
            PackageItem param_item,
            Package param_package,
            Query param_query,
            QueryDesignerMode param_mode)
        {
            IQueryDesigner retForm = null;

            Boolean useQE2 = false;
            try
            {
                ModuleMain main = new ModuleMain();
                CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");
                if (config.Editor == CodeEditControlSection.enumEditor.ASK)
                {
                    Perfectus.Client.Configuration.QueryEditConfiguration.QueryEditorAskVersion form = new Perfectus.Client.Configuration.QueryEditConfiguration.QueryEditorAskVersion();

                    useQE2 = form.ShowDialog(DesktopWindow.Instance) == DialogResult.Yes;
                    if (form.RememberSelection)
                    {
                        config.Editor = useQE2 ?
                            CodeEditControlSection.enumEditor.NEW :
                            CodeEditControlSection.enumEditor.OLD;
                        main.SaveConfig(config);
                    }
                }
                else
                    useQE2 = config.Editor == CodeEditControlSection.enumEditor.NEW;
            }
            catch (Exception)
            {
                // Failed to scan the boolean app-setting
            }

            if (useQE2)
            {
                retForm = new Query2DesignerForm();
                // Upload the integration DLL's
                // The form will test if they implement the necessary inteface IClauseLibrary
                foreach (SDK.IStudioPlugin plugin in MainDesigner2.Plugins)
                    ((Query2DesignerForm)retForm).AddPlugin(plugin);

            }
            else
                retForm = new QueryDesignerForm();

            if (null != param_item)
                retForm.ItemBeingEdited = param_item;
            retForm.QueryBeingEdited = param_query;
            retForm.Mode = (QueryDesignerMode)param_mode;

            param_query.ParentPackage = param_query.QueryExpression.ParentPackage = param_package;
            return retForm;
        }
    }
}
