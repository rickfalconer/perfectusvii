using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
	// SEE: http://www.syncfusion.com/FAQ/WinForms/FAQ_c44c.asp#q480q
	public class OperatorComboColumn : DataGridTextBoxColumn
	{
		public NoKeyUpCombo ColumnComboBox;
		private CurrencyManager _source;
		private int _rowNum;
		private bool _isEditing;
		internal static int _rowCount;
		private int _selectedIndex;
		private bool _hasSentKeyPress = false;
		private DataTable operatorsTable;

		private bool dynamicOperatorList = false;

		private string typeDeterminingItemColumnName;
		private PerfectusDataType lastDeterminingType;
		private bool _isBusy = false;


		public OperatorComboColumn() : base()
		{
			_source = null;
			_isEditing = false;
			_rowCount = -1;

			ColumnComboBox = new NoKeyUpCombo();
			ColumnComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

			ColumnComboBox.Leave += new EventHandler(LeaveComboBox);
			ColumnComboBox.Enter += new EventHandler(ComboEnter);

			ColumnComboBox.SelectionChangeCommitted += new EventHandler(ComboStartEditing);


			//ColumnComboBox.Font = new Font("Arial", 11, GraphicsUnit.Point);

			dynamicOperatorList = false;

		}

		private DataTable CreateOperatorsTable()
		{
			DataTable ot = new DataTable("Operators");
			ot.Columns.Add("Display", typeof (string));
			ot.Columns.Add("Value", typeof (OperatorDisplay));

			return ot;
		}

		public OperatorComboColumn(string typeDeterminingItemColumnName) : this()
		{
			this.typeDeterminingItemColumnName = typeDeterminingItemColumnName;

			ColumnComboBox.DisplayMember = "Display";
			ColumnComboBox.ValueMember = "Value";
			operatorsTable = CreateOperatorsTable();
			ColumnComboBox.DataSource = operatorsTable.DefaultView;
			dynamicOperatorList = true;
			SetComboItems(PerfectusDataType.Number);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets the _isEditing field to true when the combo box receives focus (usually when the cell is entered)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboEnter(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Called by the combo box's SelectionChangeCommitted event.  Turns on _isEditing, and calls the base
		///		ColumnStartedEditing handler.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void ComboStartEditing(object sender, EventArgs e)
		{
			_isEditing = true;
			_hasSentKeyPress = false;

			base.ColumnStartedEditing((Control) sender);
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Makes sure the combo box is hidden when the user scrolls the grid (otherwise it floats over the grid and looks odd)
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void HandleScroll(object sender, EventArgs e)
		{
			if (ColumnComboBox.Visible)
				ColumnComboBox.Hide();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		When the combo box loses focus, turn off _isEditing and write the selected value into the datarow.
		///		Then hide the combo box, remove the Scroll handler from the grid, and return the column to its 'not doing anything' state.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void LeaveComboBox(object sender, EventArgs e)
		{
			if (_isEditing)
			{
				object si = ColumnComboBox.SelectedItem;
				if (si == null)
				{
					SetColumnValueAtRow(_source, _rowNum, DBNull.Value);
				}
				else
				{
					SetColumnValueAtRow(_source, _rowNum, si);
				}
				_isEditing = false;
				Invalidate();
			}
			ColumnComboBox.Hide();
			Invalidate();
			this.DataGridTableStyle.DataGrid.Scroll -= new EventHandler(HandleScroll);
			_hasSentKeyPress = false;

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Sets up the combo box with the correct selectedValue (according to the actual data), and displays it
		///		within the rectangle that would have been filled by the cell's Text Box control.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="bounds"></param>
		/// <param name="readOnly"></param>
		/// <param name="instantText"></param>
		/// <param name="cellIsVisible"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void Edit(CurrencyManager source, int rowNum, Rectangle bounds, bool readOnly, string instantText, bool cellIsVisible)
		{
			if (!_hasSentKeyPress && _isBusy == false)
			{
				// Send a key-down Windows Message to this cell's text box, to make the grid act as if the user was
				// typing normally.  This is to make sure that clicking in the 'new row' row still makes the grid
				// add a new 'new row' row underneath it.
				
				_isBusy = true; // This flag fixes an occasional bug when the very left operand on a new row is clicked, without this flag a stackOverflow exception occurs.
				Sneaky(this.TextBox);
				_isBusy = false;

				base.Edit(source, rowNum, bounds, readOnly, instantText, cellIsVisible);

				_rowNum = rowNum;
				_source = source;
				_rowCount = _source.Count;

				// Position the combo box.
				ColumnComboBox.Parent = this.TextBox.Parent;
				ColumnComboBox.Location = this.TextBox.Location;
				ColumnComboBox.Size = new Size(this.TextBox.Size.Width, ColumnComboBox.Size.Height);

				// Set its selected index.
				ColumnComboBox.SelectedIndex = -1;
				ColumnComboBox.SelectedIndex = ColumnComboBox.FindStringExact(this.TextBox.Text);
				ColumnComboBox.Text = this.TextBox.Text;

				_selectedIndex = ColumnComboBox.SelectedIndex;

				if (_selectedIndex < 0 && ColumnComboBox.Items.Count > 0)
				{
					ColumnComboBox.SelectedIndex = 0;
					_selectedIndex = 0;
				}

				// Hide the text box, display the combo.
				this.TextBox.Visible = false;
				SetItems(source);
				ColumnComboBox.Visible = true;

				// Handle scroll events on the grid, to hide the combo.
				this.DataGridTableStyle.DataGrid.Scroll += new EventHandler(HandleScroll);

				ColumnComboBox.BringToFront();
				ColumnComboBox.Focus();
			}
		}

		private void SetItems(CurrencyManager source)
		{
			if (dynamicOperatorList)
			{
				if (typeDeterminingItemColumnName != null && !Convert.IsDBNull(((DataRowView) (source.Current))[typeDeterminingItemColumnName]) && (((DataRowView) source.Current)[typeDeterminingItemColumnName] != null))
				{
					object otherColValue = ((DataRowView) source.Current)[typeDeterminingItemColumnName];

					AnswerProvider iap = (AnswerProvider) otherColValue;

					PerfectusDataType determiningType = iap.DataType;

					if (determiningType != lastDeterminingType)
					{
						SetComboItems(determiningType);

						lastDeterminingType = determiningType;

					}
				}
			}
		}

		private void SetComboItems(PerfectusDataType determiningType)
		{
			if (dynamicOperatorList)
			{
				operatorsTable = null;
				operatorsTable = CreateOperatorsTable();
				ColumnComboBox.DataSource = operatorsTable.DefaultView;


				foreach (Operator op in PerfectusTypeSystem.GetComparisonOperators(determiningType))
				{
					OperatorDisplay opdisp = new OperatorDisplay(op);

					operatorsTable.Rows.Add(new object[] {opdisp.Display, opdisp});
				}
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the value to the dataset.
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="rowNum"></param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override bool Commit(CurrencyManager dataSource, int rowNum)
		{
			if (_isEditing)
			{
				_isEditing = false;
				_hasSentKeyPress = false;

				object si = ColumnComboBox.SelectedItem;
				if (si == null)
				{
					SetColumnValueAtRow(dataSource, rowNum, DBNull.Value);
				}
				else
				{
					SetColumnValueAtRow(dataSource, rowNum, si);
				}
			}
			return true;
		}

		protected override void ConcedeFocus()
		{
			_isEditing = false;
			_hasSentKeyPress = false;

			base.ConcedeFocus();
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Instead of getting the value from the text box, get it from the selected item of the visible combo box.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <returns>Returns the selected value from the combo box.</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
//		protected override object GetColumnValueAtRow(CurrencyManager source, int rowNum)
//		{
//			object s =  base.GetColumnValueAtRow(source, rowNum);
//			DataView dv = (DataView)this.ColumnComboBox.DataSource;
//			int rowCount = dv.Count;
//			int i = 0;
//
//			//if things are slow, you could order your dataview
//			//& use binary search instead of this linear one
//			while (i < rowCount)
//			{
//				if( s.Equals( dv[i][this.ColumnComboBox.ValueMember]))
//					break;
//				++i;
//			}
//			
//			if(i < rowCount)
//			{
//				object o = dv[i][this.ColumnComboBox.ValueMember];
//				return (OperatorDisplay)o;
//			}
//			
//			return DBNull.Value;
//		}
		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Writes the combo box's selected value into the data set.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="rowNum"></param>
		/// <param name="value"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		protected override void SetColumnValueAtRow(CurrencyManager source, int rowNum, object value)
		{
			try
			{
				object s = value;

				if (s is DataRowView)
				{
					DataRowView drv = (DataRowView) s;
					base.SetColumnValueAtRow(source, rowNum, drv[ColumnComboBox.ValueMember]);

				}
				else
				{
					base.SetColumnValueAtRow(source, rowNum, DBNull.Value);

				}
			}
			catch // Empty catch, in case of the rowNum issue with datagrids 'over-editing'.
			{
			}

		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Send a key-up message to a control (hWin).
		/// </summary>
		/// <param name="c"></param>
		// ------------------------------------------------------------------------------------------------------------------------------
		private void Sneaky(Control c)
		{
			_hasSentKeyPress = true;
			IntPtr h = c.Handle;
			int wParam = 0x97;
			uint lParam = 0x1E0001;
			WinMessage.SendMessage(h.ToInt32(), 0x102, wParam, lParam);
			_isEditing = true;
			
		}
	}
}
