using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for DataBindingsEditorForm.
	/// </summary>
	public class DataBindingsEditorForm : Form
	{
		private ImageList ilistTreeView;
		private Button btnOk;
        private Button btnCancel;
		private TreeView tvwItems;
		private Label label2;
		private Label label3;
		private Label label4;
		private IContainer components;
		private TreeView tvwParams;
		private Package package;
		private Button btnAdd;
        private Button btnRemove;
		private DataBindingInfo info = new DataBindingInfo();
		private TagTreeNodeDictionary dictAnswer = new TagTreeNodeDictionary();
        private TagTreeNodeDictionary dictItems = new TagTreeNodeDictionary();
		private Label label5;
		private RadioButton rdoWebService;
		private RadioButton rdoRepeatedQuestion;
        private TagTreeNodeDictionary dictParams = new TagTreeNodeDictionary();
        private Label label1;
        private TreeView tvwAnswer;
        private TabControl uiTabView1;
        private TabPage uiTabPage1;
        private TabPage uiTabPage2;
        private TabPage uiTabPage3;
        private ComboBox cbxDisplayValue;
        private ComboBox cbxQuestionBind;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		private Question questionBeingEdited;

		public DataBindingInfo Info
		{
			get { return info; }
			set
			{
				if (value == null)
				{
					info = new DataBindingInfo();
				}
				else
				{
					info = value;
					ResetUI();
				}
			}
		}

		public DataBindingsEditorForm(Question q)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.questionBeingEdited = q;
			this.package = q.ParentPackage;

			ResetUI();
		}

		private void ResetUI()
		{
			dictAnswer = new TagTreeNodeDictionary();
			dictItems = new TagTreeNodeDictionary();
			dictParams = new TagTreeNodeDictionary();

			tvwAnswer.Nodes.Clear();
			tvwItems.Nodes.Clear();
			tvwParams.Nodes.Clear();
			cbxDisplayValue.Items.Clear();
			uiListBox1.Items.Clear();

			foreach (DataSource ds in package.DataSources)
			{
				if (ds is WebReference)
				{
					WebReference wr = ds as WebReference;
					TreeNode answerRoot = new TreeNode();
					TreeNode itemsRoot = new TreeNode();
					TreeNode paramsRoot = new TreeNode();

					answerRoot.Text = itemsRoot.Text = paramsRoot.Text = ds.Name;
					answerRoot.Tag = itemsRoot.Tag = paramsRoot.Tag = wr;
					answerRoot.ImageIndex = itemsRoot.ImageIndex = paramsRoot.ImageIndex = (int) TreeIcons.URL;
					answerRoot.SelectedImageIndex = itemsRoot.SelectedImageIndex = paramsRoot.SelectedImageIndex = (int) TreeIcons.URL;

					dictAnswer = DecorateServiceNode.Decorate(wr, answerRoot, wr.LayoutXml, ServiceNodeFilter.ResultsOnly, dictAnswer);
					dictItems = DecorateServiceNode.Decorate(wr, itemsRoot, wr.LayoutXml, ServiceNodeFilter.ResultsOnly, dictItems);
					dictParams = DecorateServiceNode.Decorate(wr, paramsRoot, wr.LayoutXml, ServiceNodeFilter.ParametersOnly, dictParams);

					tvwAnswer.Nodes.Add(answerRoot);
					tvwItems.Nodes.Add(itemsRoot);
					tvwParams.Nodes.Add(paramsRoot);					
				}
			}

			// add parameters to param tab.
			if (info.ParameterBindings != null)
			{
				foreach (string s in info.ParameterBindings)
				{
                    if (dictParams[s] != null)
					{
                        TreeNode n = dictParams[s];
						//UiListItem item = new UiListItem();
                        ListViewItem item = new ListViewItem();
						item.Text = n.Text;
						item.Tag = s;
                        item.ImageIndex = (int)TreeIcons.Param;
						uiListBox1.Items.Add(item);
					}
                    else
			{
                        foreach (string key in dictParams.Keys)
				{
                            string oldKey = key.Substring(key.IndexOf("|") + 1);

                            foreach (string paramKey in info.ParameterBindings)
					{
                                if (oldKey == paramKey)
                                {
                                    TreeNode n = dictParams[key];
                                    //UiListItem item = new UiListItem();
                                    ListViewItem item = new ListViewItem();
                                    item.Text = n.Text;
                                    item.Tag = s;
                                    item.ImageIndex = (int)TreeIcons.Param;
                                    uiListBox1.Items.Add(item);
                                }
                            }
                        }
					}
				}                    				
			}	

			// Populate the list of Questions-that-are-in-repeaters

			cbxQuestionBind.Items.Clear();
			Hashtable ht = new Hashtable();
			foreach (Interview iv in package.Interviews)
			{
				foreach (InterviewPage pg in iv.Pages)
				{
					foreach (IPageItem ipi in pg.Items)
					{
						if (ipi is RepeaterZone)
						{
							foreach (IPageItem ipi2 in ((RepeaterZone) ipi).Items)
							{
								if (ipi2 is Question && ipi2 != questionBeingEdited)
								{
									Question q = (Question) ipi2;
									if (!ht.ContainsKey(q.UniqueIdentifier))
									{
										ht.Add(q.UniqueIdentifier, q);
									}
								}
							}
						}
					}
				}
			}

			foreach (object k in ht.Keys)
			{
				Question q = ht[k] as Question;
				//cbxQuestionBind.Items.Add(q, q.Name, 10);
                cbxQuestionBind.Items.Add( new ComboboxItem() { Text = q.Name, Value = q });
			}
		
			// answer tab select current value nodes
			if (info.AnswerBinding != null)
			{

                if (dictAnswer[info.AnswerBinding] != null)
				{
                    tvwAnswer.SelectedNode = dictAnswer[info.AnswerBinding];
                }
                else
					{
                    foreach (string key in dictAnswer.Keys)
						{
                        string oldKey = key.Substring(key.IndexOf("|") + 1);

                        if (oldKey == info.AnswerBinding)
							{
                            tvwAnswer.SelectedNode = dictAnswer[key];
								break;
							}
						}
					}
			}
			// items tab select current value nodes
			if (info.ItemsValueBinding != null)
			{
				if (info.ItemsValueBinding[0] == 'Q')
				{
					try
					{
						string guidPart = info.ItemsValueBinding.Substring(1);
						Guid g = new Guid(guidPart);
                        //foreach (UiListItem li in cbxQuestionBind.Items)
						foreach (ComboboxItem li in cbxQuestionBind.Items)
						{
							Question q = (Question) li.Value;
							if (q.UniqueIdentifier == g)
							{
								cbxQuestionBind.SelectedItem = li;
								rdoRepeatedQuestion.Checked = true;
								break;
							}
						}
					}
					catch
					{
					}
				}
				else
				{
                    if (dictItems[info.ItemsValueBinding] != null)
                    {
					tvwItems.SelectedNode = dictItems[info.ItemsValueBinding];
				}
                    else
                    {
                        foreach (string key in dictItems.Keys)
                        {
                            string oldKey = key.Substring(key.IndexOf("|") + 1);

                            if (oldKey == info.ItemsValueBinding)
                            {
                                tvwItems.SelectedNode = dictItems[key];
                                break;
                            }
                        }
                    }
				}
			}

			if (tvwAnswer.SelectedNode != null)
			{
				tvwAnswer.SelectedNode.EnsureVisible();
			}
			if (tvwItems.SelectedNode != null)
			{
				tvwItems.SelectedNode.EnsureVisible();
			}
			if (info.ItemsDisplayBinding != null)
			{
				cbxDisplayValue.SelectedIndex = cbxDisplayValue.FindString(info.ItemsDisplayBinding);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataBindingsEditorForm));
            this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tvwParams = new System.Windows.Forms.TreeView();
            this.label5 = new System.Windows.Forms.Label();
            this.rdoWebService = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tvwItems = new System.Windows.Forms.TreeView();
            this.rdoRepeatedQuestion = new System.Windows.Forms.RadioButton();
            this.uiTabView1 = new System.Windows.Forms.TabControl();
            this.uiTabPage1 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.tvwAnswer = new System.Windows.Forms.TreeView();
            this.uiTabPage2 = new System.Windows.Forms.TabPage();
            this.cbxQuestionBind = new System.Windows.Forms.ComboBox();
            this.cbxDisplayValue = new System.Windows.Forms.ComboBox();
            this.uiTabPage3 = new System.Windows.Forms.TabPage();
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.uiTabView1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ilistTreeView
            // 
            this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
            this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ilistTreeView.Images.SetKeyName(0, "");
            this.ilistTreeView.Images.SetKeyName(1, "");
            this.ilistTreeView.Images.SetKeyName(2, "");
            this.ilistTreeView.Images.SetKeyName(3, "");
            this.ilistTreeView.Images.SetKeyName(4, "");
            this.ilistTreeView.Images.SetKeyName(5, "");
            this.ilistTreeView.Images.SetKeyName(6, "");
            this.ilistTreeView.Images.SetKeyName(7, "");
            this.ilistTreeView.Images.SetKeyName(8, "");
            this.ilistTreeView.Images.SetKeyName(9, "");
            this.ilistTreeView.Images.SetKeyName(10, "");
            this.ilistTreeView.Images.SetKeyName(11, "");
            this.ilistTreeView.Images.SetKeyName(12, "");
            this.ilistTreeView.Images.SetKeyName(13, "");
            this.ilistTreeView.Images.SetKeyName(14, "");
            this.ilistTreeView.Images.SetKeyName(15, "");
            this.ilistTreeView.Images.SetKeyName(16, "");
            this.ilistTreeView.Images.SetKeyName(17, "");
            this.ilistTreeView.Images.SetKeyName(18, "");
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(578, 517);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 90;
            this.btnOk.Text = "&OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(658, 517);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 91;
            this.btnCancel.Text = "&Cancel";
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemove.Enabled = false;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnRemove.Location = new System.Drawing.Point(416, 134);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(64, 23);
            this.btnRemove.TabIndex = 8;
            this.btnRemove.Text = "< Remove";
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Enabled = false;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnAdd.Location = new System.Drawing.Point(416, 87);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(64, 23);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.Text = "Add >";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(31, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(494, 32);
            this.label4.TabIndex = 37;
            this.label4.Text = "Choose the Web Service parameters that will be provided by this Question:";
            // 
            // tvwParams
            // 
            this.tvwParams.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvwParams.HideSelection = false;
            this.tvwParams.ImageIndex = 0;
            this.tvwParams.ImageList = this.ilistTreeView;
            this.tvwParams.Indent = 19;
            this.tvwParams.ItemHeight = 16;
            this.tvwParams.Location = new System.Drawing.Point(31, 56);
            this.tvwParams.Name = "tvwParams";
            this.tvwParams.SelectedImageIndex = 0;
            this.tvwParams.Size = new System.Drawing.Size(379, 394);
            this.tvwParams.TabIndex = 6;
            this.tvwParams.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwParams_AfterSelect);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(152, 397);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(503, 29);
            this.label5.TabIndex = 41;
            this.label5.Text = "Or, bind to a Question that has been placed in a Repeater on an EasyInterview Pag" +
                "e";
            // 
            // rdoWebService
            // 
            this.rdoWebService.Checked = true;
            this.rdoWebService.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdoWebService.Location = new System.Drawing.Point(16, 56);
            this.rdoWebService.Name = "rdoWebService";
            this.rdoWebService.Size = new System.Drawing.Size(104, 24);
            this.rdoWebService.TabIndex = 1;
            this.rdoWebService.TabStop = true;
            this.rdoWebService.Text = "Web Service";
            this.rdoWebService.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(152, 349);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(471, 16);
            this.label3.TabIndex = 37;
            this.label3.Text = "Choose the corresponding field for the \'Items\' display name:";
            // 
            // label2
            // 
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(152, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(328, 32);
            this.label2.TabIndex = 36;
            this.label2.Text = "Choose the Web Service result field that will provide the possible Values for thi" +
                "s Question\'s \'Items\' property:";
            // 
            // tvwItems
            // 
            this.tvwItems.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvwItems.HideSelection = false;
            this.tvwItems.ImageIndex = 0;
            this.tvwItems.ImageList = this.ilistTreeView;
            this.tvwItems.Indent = 19;
            this.tvwItems.ItemHeight = 16;
            this.tvwItems.Location = new System.Drawing.Point(144, 56);
            this.tvwItems.Name = "tvwItems";
            this.tvwItems.SelectedImageIndex = 0;
            this.tvwItems.Size = new System.Drawing.Size(551, 285);
            this.tvwItems.TabIndex = 2;
            this.tvwItems.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwItems_AfterSelect);
            // 
            // rdoRepeatedQuestion
            // 
            this.rdoRepeatedQuestion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rdoRepeatedQuestion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdoRepeatedQuestion.Location = new System.Drawing.Point(16, 429);
            this.rdoRepeatedQuestion.Name = "rdoRepeatedQuestion";
            this.rdoRepeatedQuestion.Size = new System.Drawing.Size(131, 24);
            this.rdoRepeatedQuestion.TabIndex = 4;
            this.rdoRepeatedQuestion.Text = "A Repeated Question";
            // 
            // uiTabView1
            // 
            this.uiTabView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTabView1.Controls.Add(this.uiTabPage1);
            this.uiTabView1.Controls.Add(this.uiTabPage2);
            this.uiTabView1.Controls.Add(this.uiTabPage3);
            this.uiTabView1.Location = new System.Drawing.Point(0, 0);
            this.uiTabView1.Name = "uiTabView1";
            this.uiTabView1.SelectedIndex = 0;
            this.uiTabView1.Size = new System.Drawing.Size(746, 501);
            this.uiTabView1.TabIndex = 0;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.tvwAnswer);
            this.uiTabPage1.Location = new System.Drawing.Point(4, 22);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.uiTabPage1.Size = new System.Drawing.Size(738, 475);
            this.uiTabPage1.TabIndex = 0;
            this.uiTabPage1.Text = "Answer";
            this.uiTabPage1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(31, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(310, 32);
            this.label1.TabIndex = 35;
            this.label1.Text = "Choose the Web Service result field that will provide the Answer to this Question" +
                ":";
            // 
            // tvwAnswer
            // 
            this.tvwAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvwAnswer.HideSelection = false;
            this.tvwAnswer.ImageIndex = 0;
            this.tvwAnswer.ImageList = this.ilistTreeView;
            this.tvwAnswer.Indent = 19;
            this.tvwAnswer.ItemHeight = 16;
            this.tvwAnswer.Location = new System.Drawing.Point(31, 56);
            this.tvwAnswer.Name = "tvwAnswer";
            this.tvwAnswer.SelectedImageIndex = 0;
            this.tvwAnswer.Size = new System.Drawing.Size(664, 392);
            this.tvwAnswer.TabIndex = 0;
            this.tvwAnswer.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwAnswer_AfterSelect);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.cbxQuestionBind);
            this.uiTabPage2.Controls.Add(this.cbxDisplayValue);
            this.uiTabPage2.Controls.Add(this.label5);
            this.uiTabPage2.Controls.Add(this.rdoWebService);
            this.uiTabPage2.Controls.Add(this.label3);
            this.uiTabPage2.Controls.Add(this.label2);
            this.uiTabPage2.Controls.Add(this.tvwItems);
            this.uiTabPage2.Controls.Add(this.rdoRepeatedQuestion);
            this.uiTabPage2.Location = new System.Drawing.Point(4, 22);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.uiTabPage2.Size = new System.Drawing.Size(738, 475);
            this.uiTabPage2.TabIndex = 1;
            this.uiTabPage2.Text = "Items";
            this.uiTabPage2.UseVisualStyleBackColor = true;
            // 
            // cbxQuestionBind
            // 
            this.cbxQuestionBind.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxQuestionBind.FormattingEnabled = true;
            this.cbxQuestionBind.Location = new System.Drawing.Point(144, 430);
            this.cbxQuestionBind.Name = "cbxQuestionBind";
            this.cbxQuestionBind.Size = new System.Drawing.Size(551, 21);
            this.cbxQuestionBind.TabIndex = 5;
            // 
            // cbxDisplayValue
            // 
            this.cbxDisplayValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxDisplayValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDisplayValue.FormattingEnabled = true;
            this.cbxDisplayValue.Location = new System.Drawing.Point(144, 368);
            this.cbxDisplayValue.Name = "cbxDisplayValue";
            this.cbxDisplayValue.Size = new System.Drawing.Size(551, 21);
            this.cbxDisplayValue.TabIndex = 3;
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiListBox1);
            this.uiTabPage3.Controls.Add(this.btnRemove);
            this.uiTabPage3.Controls.Add(this.btnAdd);
            this.uiTabPage3.Controls.Add(this.label4);
            this.uiTabPage3.Controls.Add(this.tvwParams);
            this.uiTabPage3.Location = new System.Drawing.Point(4, 22);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(738, 475);
            this.uiTabPage3.TabIndex = 2;
            this.uiTabPage3.Text = "Parameters";
            this.uiTabPage3.UseVisualStyleBackColor = true;
            // 
            // uiListBox1
            // 
            this.uiListBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Location = new System.Drawing.Point(487, 56);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(223, 394);
            this.uiListBox1.SmallImageList = this.ilistTreeView;
            this.uiListBox1.TabIndex = 9;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.List;
            this.uiListBox1.SelectedIndexChanged += new System.EventHandler(this.uiListBox1_SelectedIndexChanged);
            this.uiListBox1.Click += new System.EventHandler(this.uiListBox1_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 255;
            // 
            // DataBindingsEditorForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(746, 557);
            this.Controls.Add(this.uiTabView1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(408, 408);
            this.Name = "DataBindingsEditorForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Data Bindings";
            this.Load += new System.EventHandler(this.DataBindingsEditorForm_Load);
            this.uiTabView1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private void DataBindingsEditorForm_Load(object sender, EventArgs e)
		{
		}

		private void tvwAnswer_AfterSelect(object sender, TreeViewEventArgs e)
		{
			TreeNode n = tvwAnswer.SelectedNode;
			if (n != null)
			{
				string t = n.Tag.ToString();
				// If it's not a'return' value at the end of the hierarchy, undo the selection and return.
				if (! (t.IndexOf("/returns") >= 0 && n.Nodes.Count == 0))
				{
					tvwAnswer.SelectedNode = null;
					return;
				}
			}
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			TreeNode n = (TreeNode) tvwParams.SelectedNode;
			// Return if it's not a param node.
			if (n == null || !(n.Tag.ToString().IndexOf("/parameter") > 0 && n.Nodes.Count == 0))
			{
				tvwParams.SelectedNode = null;
				return;
			}
			else
			{
				string t = n.Tag.ToString();
				// Make sure it's not already chosen:
                //foreach (UiListItem item in uiListBox1.Items)
                foreach (ListViewItem item in uiListBox1.Items)
                {
					if (item.Tag.ToString() == t)
					{
						return;
					}
				}

				// Add it:
				//UiListItem newItem = new UiListItem();
                ListViewItem newItem = new ListViewItem();
				newItem.Text = n.Text;
				newItem.Tag = t;
				newItem.ImageIndex = (int) TreeIcons.Param;
				uiListBox1.Items.Add(newItem);
			}
		}

		private void tvwItems_AfterSelect(object sender, TreeViewEventArgs e)
		{
			// Make sure we're a retval or return field

			// Find all the 'sibling' fields (if any) of the selected node.

			// Don't reset the 'Display' drop-down if we haven't moved off the current retval.

			string oldTag = null;

			//UiListItem oldItem = cbxDisplayValue.SelectedItem;
            ComboboxItem oldItem = cbxDisplayValue.SelectedItem as ComboboxItem;
			if (oldItem != null)
			{
				oldTag = oldItem.Value.ToString();
			}


			TreeNode n = tvwItems.SelectedNode;
			cbxDisplayValue.Items.Clear();
			if (n != null)
			{
				string t = n.Tag.ToString();
				// If it's not a'return' value at the end of the hierarchy, undo the selection and return.
				if (! (t.IndexOf("/returns") >= 0 && n.Nodes.Count == 0))
				{
					tvwItems.SelectedNode = null;
					return;
				}
				TreeNode p = n.Parent;
				if (p != null)
				{
					foreach (TreeNode sibling in p.Nodes)
					{
						//UiListItem item = new UiListItem();
                        ComboboxItem item = new ComboboxItem();
						item.Text = sibling.Text;
						item.Value = sibling.Tag.ToString();
						//item.ImageIndex = n.ImageIndex;
						cbxDisplayValue.Items.Add(item);
					}
					// Make sure something is selected.
					if (cbxDisplayValue.Items.Count > 0)
					{
						cbxDisplayValue.SelectedIndex = 0;
					}

					// Try and improve on our selection.
					if (oldTag != null)
					{
						//foreach (UiListItem checkingItem in cbxDisplayValue.Items)
                        foreach (ComboboxItem checkingItem in cbxDisplayValue.Items)
                        {
							if (checkingItem.Value.ToString() == oldTag)
							{
								cbxDisplayValue.SelectedItem = checkingItem;
								break;
							}
						}
					}
				}
			}
		}

		private void tvwParams_AfterSelect(object sender, TreeViewEventArgs e)
		{
			TreeNode n = tvwParams.SelectedNode;
			if (n != null)
			{
				string t = n.Tag.ToString();
				// If it's not a'return' value at the end of the hierarchy, undo the selection and return.
				if (! (t.IndexOf("/parameter") >= 0 && n.Nodes.Count == 0))
				{
					tvwParams.SelectedNode = null;
					btnAdd.Enabled = false;
					return;
				}
				else
				{
					btnAdd.Enabled = true;
				}
			}

		}

        private void uiListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (uiListBox1.SelectedIndex >= 0)
            if (uiListBox1.SelectedItems.Count > 0)
            {
                btnRemove.Enabled = true;
                //UiListItem item = uiListBox1.SelectedItem;
                ListViewItem item = uiListBox1.FocusedItem;
                string t = item.Tag.ToString();
                if (dictParams != null)
                {
                    TreeNode n = dictParams[t];
                    if (n != null)
                    {
                        tvwParams.SelectedNode = n;
                        n.EnsureVisible();
                    }
                }
            }
            else
            {
                btnRemove.Enabled = false;
            }
        }

		private void btnRemove_Click(object sender, EventArgs e)
		{
			//UiListItem item = uiListBox1.SelectedItem;
            ListViewItem item = uiListBox1.FocusedItem;
			if (item != null)
			{
				uiListBox1.Items.Remove(item);
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			TreeNode answerNode = tvwAnswer.SelectedNode;
			TreeNode itemsNode = tvwItems.SelectedNode;

			string answerBind = null;
			string itemsBindValue = null;
			string itemsBindDisplay = null;
			string[] paramsBind = null;

			if (answerNode != null )
			{
				answerBind = answerNode.Tag.ToString();
			}

			if (itemsNode != null && rdoWebService.Checked)
			{
				itemsBindValue = itemsNode.Tag.ToString();
				itemsBindDisplay = cbxDisplayValue.Text;
			}

			if (rdoRepeatedQuestion.Checked && cbxQuestionBind.SelectedItem != null)
			{
                //Question q = (Question)(cbxQuestionBind.SelectedItem.ItemData);
                ComboboxItem item = cbxQuestionBind.SelectedItem as ComboboxItem;
                Question q = (Question)(item.Value);
                itemsBindValue = string.Format("Q{0}", q.UniqueIdentifier);
			}
			
			ArrayList a = new ArrayList();
            //foreach (UiListItem i in uiListBox1.Items)
            foreach (ListViewItem i in uiListBox1.Items)
			{
				a.Add(i.Tag.ToString());
			}

			paramsBind = (string[]) a.ToArray(typeof (string));

			info.AnswerBinding = answerBind;			
			info.ItemsDisplayBinding = itemsBindDisplay;
			info.ItemsValueBinding = itemsBindValue;
			info.ParameterBindings = paramsBind;

		}	

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			if (rdoWebService.Checked)
			{
				rdoRepeatedQuestion.Checked = false;
				cbxQuestionBind.Enabled = false;
				cbxDisplayValue.Enabled = true;
				tvwItems.Enabled = true;
			}
			else
			{
				rdoRepeatedQuestion.Checked = true;
				cbxQuestionBind.Enabled = true;
				cbxDisplayValue.Enabled = false;
				tvwItems.Enabled = false;
			}
		}

		private void rdoAnswerWebService_CheckedChanged(object sender, System.EventArgs e)
		{
				rdoRepeatedQuestion.Checked = true;
				tvwAnswer.Enabled = false;
			}
		}
}
