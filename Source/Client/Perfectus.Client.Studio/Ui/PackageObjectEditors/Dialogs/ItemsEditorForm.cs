using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for ItemsEditorForm.
	/// </summary>
	public class ItemsEditorForm : Form
	{
		private Label lblText;
		private Button btnDelete;
		private DataGrid grdItems;
		private Button btnOk;
		private Button btnCancel;
		private DataTable dt;

		private ItemsItemCollection items = new ItemsItemCollection();
		private DataGridTableStyle dataGridTableStyle1;
		private DataGridTextBoxColumn dataGridTextBoxColumn1;
		private DataGridTextBoxColumn dataGridTextBoxColumn2;

		public ItemsItemCollection Items
		{
			get { return items; }
			set
			{
				items = value;
				ResetUI();
			}
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public ItemsEditorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();


			dt = new DataTable("items");
			dt.Columns.Add("display", typeof (string));
			dt.Columns.Add("value", typeof (string));
		}

		private void ResetUI()
		{
			dt.Clear();
			if (items != null)
			{
				foreach (ItemsItem i in items)
				{
					DataRow dr = dt.NewRow();
					dr["display"] = i.Display;
					dr["value"] = i.Value;
					dt.Rows.Add(dr);
				}
			}

			grdItems.DataSource = dt;
		}

		private ItemsItemCollection ItemsFromTable()
		{
			ItemsItemCollection i = new ItemsItemCollection();
			if (dt != null)
			{
				foreach (DataRow dr in dt.Rows)
				{
					ItemsItem newItem = new ItemsItem();
					newItem.Display = dr["display"].ToString();
					if (Convert.IsDBNull(dr["value"]) || dr["value"] == null || dr["value"].ToString().Trim().Length == 0)
					{
						newItem.Value = newItem.Display;
					}
					else
					{
						newItem.Value = dr["value"].ToString();
					}
					i.Add(newItem);
				}
			}
			return i;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ItemsEditorForm));
			this.lblText = new System.Windows.Forms.Label();
			this.btnDelete = new System.Windows.Forms.Button();
			this.grdItems = new System.Windows.Forms.DataGrid();
			this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
			this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.grdItems)).BeginInit();
			this.SuspendLayout();
			// 
			// lblText
			// 
			this.lblText.AccessibleDescription = resources.GetString("lblText.AccessibleDescription");
			this.lblText.AccessibleName = resources.GetString("lblText.AccessibleName");
			this.lblText.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("lblText.Anchor")));
			this.lblText.AutoSize = ((bool)(resources.GetObject("lblText.AutoSize")));
			this.lblText.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("lblText.Dock")));
			this.lblText.Enabled = ((bool)(resources.GetObject("lblText.Enabled")));
			this.lblText.Font = ((System.Drawing.Font)(resources.GetObject("lblText.Font")));
			this.lblText.Image = ((System.Drawing.Image)(resources.GetObject("lblText.Image")));
			this.lblText.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblText.ImageAlign")));
			this.lblText.ImageIndex = ((int)(resources.GetObject("lblText.ImageIndex")));
			this.lblText.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("lblText.ImeMode")));
			this.lblText.Location = ((System.Drawing.Point)(resources.GetObject("lblText.Location")));
			this.lblText.Name = "lblText";
			this.lblText.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("lblText.RightToLeft")));
			this.lblText.Size = ((System.Drawing.Size)(resources.GetObject("lblText.Size")));
			this.lblText.TabIndex = ((int)(resources.GetObject("lblText.TabIndex")));
			this.lblText.Text = resources.GetString("lblText.Text");
			this.lblText.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("lblText.TextAlign")));
			this.lblText.Visible = ((bool)(resources.GetObject("lblText.Visible")));
			this.lblText.Click += new System.EventHandler(this.lblText_Click);
			// 
			// btnDelete
			// 
			this.btnDelete.AccessibleDescription = resources.GetString("btnDelete.AccessibleDescription");
			this.btnDelete.AccessibleName = resources.GetString("btnDelete.AccessibleName");
			this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnDelete.Anchor")));
			this.btnDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDelete.BackgroundImage")));
			this.btnDelete.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnDelete.Dock")));
			this.btnDelete.Enabled = ((bool)(resources.GetObject("btnDelete.Enabled")));
			this.btnDelete.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnDelete.FlatStyle")));
			this.btnDelete.Font = ((System.Drawing.Font)(resources.GetObject("btnDelete.Font")));
			this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
			this.btnDelete.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.ImageAlign")));
			this.btnDelete.ImageIndex = ((int)(resources.GetObject("btnDelete.ImageIndex")));
			this.btnDelete.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnDelete.ImeMode")));
			this.btnDelete.Location = ((System.Drawing.Point)(resources.GetObject("btnDelete.Location")));
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnDelete.RightToLeft")));
			this.btnDelete.Size = ((System.Drawing.Size)(resources.GetObject("btnDelete.Size")));
			this.btnDelete.TabIndex = ((int)(resources.GetObject("btnDelete.TabIndex")));
			this.btnDelete.Text = resources.GetString("btnDelete.Text");
			this.btnDelete.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnDelete.TextAlign")));
			this.btnDelete.Visible = ((bool)(resources.GetObject("btnDelete.Visible")));
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// grdItems
			// 
			this.grdItems.AccessibleDescription = resources.GetString("grdItems.AccessibleDescription");
			this.grdItems.AccessibleName = resources.GetString("grdItems.AccessibleName");
			this.grdItems.AccessibleRole = System.Windows.Forms.AccessibleRole.Table;
			this.grdItems.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("grdItems.Anchor")));
			this.grdItems.BackgroundColor = System.Drawing.SystemColors.ControlLight;
			this.grdItems.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grdItems.BackgroundImage")));
			this.grdItems.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.grdItems.CaptionFont = ((System.Drawing.Font)(resources.GetObject("grdItems.CaptionFont")));
			this.grdItems.CaptionText = resources.GetString("grdItems.CaptionText");
			this.grdItems.CaptionVisible = false;
			this.grdItems.DataMember = "";
			this.grdItems.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("grdItems.Dock")));
			this.grdItems.Enabled = ((bool)(resources.GetObject("grdItems.Enabled")));
			this.grdItems.Font = ((System.Drawing.Font)(resources.GetObject("grdItems.Font")));
			this.grdItems.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.grdItems.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("grdItems.ImeMode")));
			this.grdItems.Location = ((System.Drawing.Point)(resources.GetObject("grdItems.Location")));
			this.grdItems.Name = "grdItems";
			this.grdItems.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("grdItems.RightToLeft")));
			this.grdItems.Size = ((System.Drawing.Size)(resources.GetObject("grdItems.Size")));
			this.grdItems.TabIndex = ((int)(resources.GetObject("grdItems.TabIndex")));
			this.grdItems.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
																								 this.dataGridTableStyle1});
			this.grdItems.Visible = ((bool)(resources.GetObject("grdItems.Visible")));
			// 
			// dataGridTableStyle1
			// 
			this.dataGridTableStyle1.DataGrid = this.grdItems;
			this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
																												  this.dataGridTextBoxColumn1,
																												  this.dataGridTextBoxColumn2});
			this.dataGridTableStyle1.HeaderFont = ((System.Drawing.Font)(resources.GetObject("dataGridTableStyle1.HeaderFont")));
			this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGridTableStyle1.MappingName = "items";
			this.dataGridTableStyle1.PreferredColumnWidth = ((int)(resources.GetObject("dataGridTableStyle1.PreferredColumnWidth")));
			this.dataGridTableStyle1.PreferredRowHeight = ((int)(resources.GetObject("dataGridTableStyle1.PreferredRowHeight")));
			this.dataGridTableStyle1.RowHeadersVisible = false;
			this.dataGridTableStyle1.RowHeaderWidth = ((int)(resources.GetObject("dataGridTableStyle1.RowHeaderWidth")));
			// 
			// dataGridTextBoxColumn1
			// 
			this.dataGridTextBoxColumn1.Alignment = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("dataGridTextBoxColumn1.Alignment")));
			this.dataGridTextBoxColumn1.Format = "";
			this.dataGridTextBoxColumn1.FormatInfo = null;
			this.dataGridTextBoxColumn1.HeaderText = resources.GetString("dataGridTextBoxColumn1.HeaderText");
			this.dataGridTextBoxColumn1.MappingName = resources.GetString("dataGridTextBoxColumn1.MappingName");
			this.dataGridTextBoxColumn1.NullText = resources.GetString("dataGridTextBoxColumn1.NullText");
			this.dataGridTextBoxColumn1.Width = ((int)(resources.GetObject("dataGridTextBoxColumn1.Width")));
			// 
			// dataGridTextBoxColumn2
			// 
			this.dataGridTextBoxColumn2.Alignment = ((System.Windows.Forms.HorizontalAlignment)(resources.GetObject("dataGridTextBoxColumn2.Alignment")));
			this.dataGridTextBoxColumn2.Format = "";
			this.dataGridTextBoxColumn2.FormatInfo = null;
			this.dataGridTextBoxColumn2.HeaderText = resources.GetString("dataGridTextBoxColumn2.HeaderText");
			this.dataGridTextBoxColumn2.MappingName = resources.GetString("dataGridTextBoxColumn2.MappingName");
			this.dataGridTextBoxColumn2.NullText = resources.GetString("dataGridTextBoxColumn2.NullText");
			this.dataGridTextBoxColumn2.Width = ((int)(resources.GetObject("dataGridTextBoxColumn2.Width")));
			// 
			// btnOk
			// 
			this.btnOk.AccessibleDescription = resources.GetString("btnOk.AccessibleDescription");
			this.btnOk.AccessibleName = resources.GetString("btnOk.AccessibleName");
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnOk.Anchor")));
			this.btnOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOk.BackgroundImage")));
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnOk.Dock")));
			this.btnOk.Enabled = ((bool)(resources.GetObject("btnOk.Enabled")));
			this.btnOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnOk.FlatStyle")));
			this.btnOk.Font = ((System.Drawing.Font)(resources.GetObject("btnOk.Font")));
			this.btnOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOk.Image")));
			this.btnOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.ImageAlign")));
			this.btnOk.ImageIndex = ((int)(resources.GetObject("btnOk.ImageIndex")));
			this.btnOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnOk.ImeMode")));
			this.btnOk.Location = ((System.Drawing.Point)(resources.GetObject("btnOk.Location")));
			this.btnOk.Name = "btnOk";
			this.btnOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnOk.RightToLeft")));
			this.btnOk.Size = ((System.Drawing.Size)(resources.GetObject("btnOk.Size")));
			this.btnOk.TabIndex = ((int)(resources.GetObject("btnOk.TabIndex")));
			this.btnOk.Text = resources.GetString("btnOk.Text");
			this.btnOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnOk.TextAlign")));
			this.btnOk.Visible = ((bool)(resources.GetObject("btnOk.Visible")));
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.AccessibleDescription = resources.GetString("btnCancel.AccessibleDescription");
			this.btnCancel.AccessibleName = resources.GetString("btnCancel.AccessibleName");
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btnCancel.Anchor")));
			this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btnCancel.Dock")));
			this.btnCancel.Enabled = ((bool)(resources.GetObject("btnCancel.Enabled")));
			this.btnCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btnCancel.FlatStyle")));
			this.btnCancel.Font = ((System.Drawing.Font)(resources.GetObject("btnCancel.Font")));
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.ImageAlign")));
			this.btnCancel.ImageIndex = ((int)(resources.GetObject("btnCancel.ImageIndex")));
			this.btnCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btnCancel.ImeMode")));
			this.btnCancel.Location = ((System.Drawing.Point)(resources.GetObject("btnCancel.Location")));
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btnCancel.RightToLeft")));
			this.btnCancel.Size = ((System.Drawing.Size)(resources.GetObject("btnCancel.Size")));
			this.btnCancel.TabIndex = ((int)(resources.GetObject("btnCancel.TabIndex")));
			this.btnCancel.Text = resources.GetString("btnCancel.Text");
			this.btnCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btnCancel.TextAlign")));
			this.btnCancel.Visible = ((bool)(resources.GetObject("btnCancel.Visible")));
			// 
			// ItemsEditorForm
			// 
			this.AcceptButton = this.btnOk;
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.CancelButton = this.btnCancel;
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.lblText);
			this.Controls.Add(this.btnDelete);
			this.Controls.Add(this.grdItems);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "ItemsEditorForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			((System.ComponentModel.ISupportInitialize)(this.grdItems)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private void btnOk_Click(object sender, EventArgs e)
		{
			items = ItemsFromTable();
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (dt != null && dt.Rows.Count > 0)
			{
				dt.Rows[this.grdItems.CurrentRowIndex].Delete();
			}
		}

		private void lblText_Click(object sender, System.EventArgs e)
		{
		
		}
	}
}