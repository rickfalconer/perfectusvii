using System;
using System.ComponentModel;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Input;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for DateFunctionEditorForm.
	/// </summary>
	public class DateFunctionEditorForm : Form
	{
		private Label label2;
		private Label label3;
		private Button btnOk;
        private Button btnCancel;
		private Label label4;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private ComboboxItem dayAddListItem;
		private ComboboxItem dayDiffListItem;
		private ComboboxItem monthAddListItem;
		private ComboboxItem monthDiffListItem;
		private ComboboxItem yearAddListItem;
        private ComboboxItem dateNowListItem;
		private ComboboxItem yearDiffListItem;
		private DateExpression definition = new DateExpression();
		private DatePFunction functionBeingEdited;
        private ComboBox uiComboBox2;
        private ItemConstantPicker icpParameter1;
        private ItemConstantPicker icpParameter2;

		public DatePFunction FunctionBeingEdited
		{
			get { return functionBeingEdited; }
			set { functionBeingEdited = value; }
		}

		private Package package;

		public Package Package
		{
			get { return package; }
			set
			{
				package = value;
				icpParameter1.Package = package;
				icpParameter2.Package = package;
			}
		}

		public DateExpression Definition
		{
			get { return definition; }
			set { definition = value; }
		}

		private void ResetUI()
		{
			icpParameter1.ItemBeingEdited = functionBeingEdited;
			icpParameter2.ItemBeingEdited = functionBeingEdited;

			if (definition != null)
			{
				foreach (ComboboxItem li in uiComboBox2.Items)
				{
					if ((DateFunctionMethod) li.Value == definition.Method)
					{
						uiComboBox2.SelectedItem = li;
						break;
					}
				}


				icpParameter1.Value = definition.Parameter1;
				icpParameter2.Value = definition.Parameter2;

				icpParameter1.ResetUI();
				icpParameter2.ResetUI();
				icpParameter1.FixDropdown();
				icpParameter2.FixDropdown();
			}

		}

		public DateFunctionEditorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			dayAddListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.DATEADD"), Value = DateFunctionMethod.DayAdd };
			dayDiffListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.DAYDIFF"), Value = DateFunctionMethod.DayDiff };
			monthAddListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.MONTHADD"), Value = DateFunctionMethod.MonthAdd };
			monthDiffListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.MONTHDIFF"), Value = DateFunctionMethod.MonthDiff };
			yearAddListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.YEARADD"), Value = DateFunctionMethod.YearAdd };
			yearDiffListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.YEARDIFF"), Value = DateFunctionMethod.YearDiff };
			dateNowListItem = new ComboboxItem(){ Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.NOW"), Value = DateFunctionMethod.Now };
			
			uiComboBox2.Items.AddRange(new ComboboxItem[] {dayAddListItem, dayDiffListItem, monthAddListItem, monthDiffListItem, yearAddListItem, yearDiffListItem, dateNowListItem});

			icpParameter1.Package = package;
			icpParameter1.DataType = PerfectusDataType.Date;
			icpParameter1.ResetUI();
			icpParameter1.FixDropdown();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DateFunctionEditorForm));
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.uiComboBox2 = new System.Windows.Forms.ComboBox();
            this.icpParameter1 = new Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.ItemConstantPicker();
            this.icpParameter2 = new Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.ItemConstantPicker();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(16, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Parameter 1:";
            // 
            // label3
            // 
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Parameter 2:";
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(222, 104);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 6;
            this.btnOk.Text = "&OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(302, 104);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "&Cancel";
            // 
            // label4
            // 
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(16, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 23);
            this.label4.TabIndex = 0;
            this.label4.Text = "Function:";
            // 
            // uiComboBox2
            // 
            this.uiComboBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiComboBox2.FormattingEnabled = true;
            this.uiComboBox2.Location = new System.Drawing.Point(96, 15);
            this.uiComboBox2.Name = "uiComboBox2";
            this.uiComboBox2.Size = new System.Drawing.Size(281, 21);
            this.uiComboBox2.TabIndex = 1;
            this.uiComboBox2.SelectedIndexChanged += new System.EventHandler(this.uiComboBox2_SelectedIndexChanged);
            // 
            // icpParameter1
            // 
            this.icpParameter1.DataType = Perfectus.Common.PerfectusDataType.Number;
            this.icpParameter1.ItemBeingEdited = null;
            this.icpParameter1.Location = new System.Drawing.Point(96, 42);
            this.icpParameter1.Name = "icpParameter1";
            this.icpParameter1.Package = null;
            this.icpParameter1.Size = new System.Drawing.Size(216, 21);
            this.icpParameter1.TabIndex = 3;
            this.icpParameter1.Value = ((Perfectus.Common.PackageObjects.AnswerProvider)(resources.GetObject("icpParameter1.Value")));
            // 
            // icpParameter2
            // 
            this.icpParameter2.DataType = Perfectus.Common.PerfectusDataType.Number;
            this.icpParameter2.ItemBeingEdited = null;
            this.icpParameter2.Location = new System.Drawing.Point(96, 69);
            this.icpParameter2.Name = "icpParameter2";
            this.icpParameter2.Package = null;
            this.icpParameter2.Size = new System.Drawing.Size(216, 21);
            this.icpParameter2.TabIndex = 5;
            this.icpParameter2.Value = ((Perfectus.Common.PackageObjects.AnswerProvider)(resources.GetObject("icpParameter2.Value")));
            // 
            // DateFunctionEditorForm
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(386, 136);
            this.Controls.Add(this.icpParameter2);
            this.Controls.Add(this.icpParameter1);
            this.Controls.Add(this.uiComboBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DateFunctionEditorForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Date Function Editor";
            this.Load += new System.EventHandler(this.DateFunctionEditorForm_Load);
            this.ResumeLayout(false);

		}

		#endregion

        private void uiComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem selectedFunctionItem = uiComboBox2.SelectedItem as ComboboxItem;
            if (selectedFunctionItem.Value == dayAddListItem ||
                selectedFunctionItem.Value == monthAddListItem ||
                selectedFunctionItem.Value == yearAddListItem)
            {
                icpParameter2.DataType = PerfectusDataType.Number;
            }
            else
            {
                icpParameter2.DataType = PerfectusDataType.Date;
            }

            if (selectedFunctionItem == dateNowListItem)
            {
                this.icpParameter1.Enabled = false;
                this.icpParameter2.Enabled = false;
            }
            else
            {
                this.icpParameter1.Enabled = true;
                this.icpParameter2.Enabled = true;
            }

            icpParameter1.ResetUI();
            icpParameter2.ResetUI();
            icpParameter1.FixDropdown();
            icpParameter2.FixDropdown();

        }

		private void btnOk_Click(object sender, EventArgs e)
		{
			if (uiComboBox2.SelectedItem != null)
			{
				if (definition == null)
				{
					definition = new DateExpression();
				}
                ComboboxItem item = uiComboBox2.SelectedItem as ComboboxItem;
				definition.Method = (DateFunctionMethod) (item.Value);
				definition.Parameter1 = icpParameter1.Value;
				definition.Parameter2 = icpParameter2.Value;
			}
		}

		private void DateFunctionEditorForm_Load(object sender, EventArgs e)
		{
			ResetUI();
		}

	}
}