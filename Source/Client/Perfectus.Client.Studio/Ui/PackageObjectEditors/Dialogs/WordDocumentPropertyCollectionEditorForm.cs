using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for WordDocumentPropertyCollectionEditorForm.
	/// </summary>
	public class WordDocumentPropertyCollectionEditorForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage CustomTab;
		private Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls.PropertiesCustomControl propertiesCustomControl1;
		private System.Windows.Forms.Button butOk;
		private System.Windows.Forms.Button butCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TabPage SummaryTab;
		private Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls.PropertiesSummaryControl propertiesSummaryControl2;
		private WordDocumentPropertyCollection documentProperties = new WordDocumentPropertyCollection();
		private Package package = null;

		public Package PackageBeingEdited
		{
            get { return package; }
            set { package = value;}
		}

		public WordDocumentPropertyCollection DocumentProperties
		{
			get { return documentProperties;}
			set 
			{	
				if(package == null)
				{
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.PackageNotSet"));
				}

				documentProperties = value;
                //propertiesSummaryControl2.PackageBeingEdited = package;
                //propertiesSummaryControl2.DocumentProperties = documentProperties;	
				
                //propertiesCustomControl1.PackageBeingEdited = package;
                //propertiesCustomControl1.DocumentProperties = documentProperties;	
			}
		}

        public WordDocumentPropertyCollectionEditorForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //		
        }

        public WordDocumentPropertyCollectionEditorForm(Package p, WordDocumentPropertyCollection docprp)
        {
            PackageBeingEdited = p;
            DocumentProperties = docprp;
            InitializeComponent();
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WordDocumentPropertyCollectionEditorForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SummaryTab = new System.Windows.Forms.TabPage();
            this.propertiesSummaryControl2 = new Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls.PropertiesSummaryControl();
            this.propertiesSummaryControl2.PackageBeingEdited = package;
            this.CustomTab = new System.Windows.Forms.TabPage();
            this.propertiesCustomControl1 = new Perfectus.Client.Studio.Ui.PackageObjectEditors.Controls.PropertiesCustomControl();
            this.propertiesCustomControl1.PackageBeingEdited = package;
            this.butOk = new System.Windows.Forms.Button();
            this.butCancel = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.SummaryTab.SuspendLayout();
            this.CustomTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.SummaryTab);
            this.tabControl1.Controls.Add(this.CustomTab);
            resources.ApplyResources(this.tabControl1, "tabControl1");
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // SummaryTab
            // 
            this.SummaryTab.Controls.Add(this.propertiesSummaryControl2);
            resources.ApplyResources(this.SummaryTab, "SummaryTab");
            this.SummaryTab.Name = "SummaryTab";
            // 
            // propertiesSummaryControl2
            // 
            resources.ApplyResources(this.propertiesSummaryControl2, "propertiesSummaryControl2");
            //this.propertiesSummaryControl2.DocumentProperties = ((Perfectus.Common.PackageObjects.WordDocumentPropertyCollection)(resources.GetObject("propertiesSummaryControl2.DocumentProperties")));
            this.propertiesSummaryControl2.DocumentProperties = DocumentProperties;
            this.propertiesSummaryControl2.Name = "propertiesSummaryControl2";
            // 
            // CustomTab
            // 
            this.CustomTab.Controls.Add(this.propertiesCustomControl1);
            resources.ApplyResources(this.CustomTab, "CustomTab");
            this.CustomTab.Name = "CustomTab";
            // 
            // propertiesCustomControl1
            // 
            resources.ApplyResources(this.propertiesCustomControl1, "propertiesCustomControl1");
            //this.propertiesCustomControl1.DocumentProperties = ((Perfectus.Common.PackageObjects.WordDocumentPropertyCollection)(resources.GetObject("propertiesCustomControl1.DocumentProperties")));
            this.propertiesCustomControl1.DocumentProperties = DocumentProperties;
            this.propertiesCustomControl1.Name = "propertiesCustomControl1";
            // 
            // butOk
            // 
            resources.ApplyResources(this.butOk, "butOk");
            this.butOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.butOk.Name = "butOk";
            this.butOk.Click += new System.EventHandler(this.butOk_Click);
            // 
            // butCancel
            // 
            resources.ApplyResources(this.butCancel, "butCancel");
            this.butCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.butCancel.Name = "butCancel";
            this.butCancel.Click += new System.EventHandler(this.butCancel_Click);
            // 
            // WordDocumentPropertyCollectionEditorForm
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.butCancel);
            this.Controls.Add(this.butOk);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WordDocumentPropertyCollectionEditorForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.tabControl1.ResumeLayout(false);
            this.SummaryTab.ResumeLayout(false);
            this.CustomTab.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void butCancel_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void tabControl1_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		
		}

		private void butOk_Click(object sender, System.EventArgs e)
		{
			propertiesSummaryControl2.UpdateStandardDocumentProperties();
			propertiesCustomControl1.UpdateCustomDocumentProperties();

			// Because both controls Reference the same collection we can get at the document properties via either control.
			documentProperties = propertiesSummaryControl2.DocumentProperties;
			this.Close();
		}

	}
}
