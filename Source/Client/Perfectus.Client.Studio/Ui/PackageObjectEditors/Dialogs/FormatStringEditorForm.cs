using System;
using System.ComponentModel;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Button=System.Windows.Forms.Button;
using Label=System.Windows.Forms.Label;
using TextBox=System.Windows.Forms.TextBox;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
    /// <summary>
    /// Summary description for FormatStringEditorForm.
    /// </summary>
    public class FormatStringEditorForm : Form
    {
        private const string FORMATSTRINGPROPERTYNAME = "FormatString";
        
        private Button btnAdvanced;
        private Button btnOk;
        private Button btnCancel;
        private TextBox txtFormatParam;
        private ComboBox cbxFormatString;
        private Label label1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        private string formatStringVal;
        private PackageItem itemBeingEdited;
        private PropertyDescriptor propertyBeingEdited;
        private Package package;
        private Label label2;
        private string formatStringParams;

        public string FormatStringVal
        {
            get { return formatStringVal; }
        }

        public PackageItem ItemBeingEdited
        {
            set
            {
                itemBeingEdited = value;
                Init();
            }
        }

        public PropertyDescriptor PropertyBeingEdited
        {
            set { propertyBeingEdited = value; }
        }

        public FormatStringEditorForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            Height = 135;
        }

        private void Init()
        {
            if (itemBeingEdited != null && itemBeingEdited is ITemplateItem)
            {
                package = itemBeingEdited.ParentPackage;

                // No, this is not pretty, but it's how the editor got written in the first place, so best not to rearchitect for the sake of it.
                if (propertyBeingEdited.Name == FORMATSTRINGPROPERTYNAME)
                {
                    formatStringVal = ((ITemplateItem)itemBeingEdited).FormatString;
                    formatStringParams = ((ITemplateItem)itemBeingEdited).FormatStringParams;
                }
                else
                {
                    if (itemBeingEdited is Question)
                    {
                        formatStringVal = ((Question)itemBeingEdited).InterviewFormatString;
                        formatStringParams = ((Question)itemBeingEdited).InterviewFormatStringParams;
                    }
                    else if (itemBeingEdited is PFunction)
                    {
                        formatStringVal = ((PFunction)itemBeingEdited).InterviewFormatString;
                        formatStringParams = ((PFunction)itemBeingEdited).InterviewFormatStringParams;
                    }
                }
                txtFormatParam.Text = formatStringParams;
            }
            else
            {
                throw new Exception(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                        "Perfectus.Client.Studio.Config.ITemplateItem"));
            }

            // add formats defined in config.
            AddStandardValues();

            // add format plugin extensions
            AddFormatPluginValues();
        }

        private void AddFormatPluginValues()
        {
            AnswerProvider ap = (AnswerProvider) (itemBeingEdited);

            foreach (FormatStringExtensionInfo ext in package.FormatStringExtensionMapping.Values)
            {
                switch (ap.DataType)
                {
                    case PerfectusDataType.Number:
                        if (ext.ForDataType != "NumberFormats")
                        {
                            continue;
                        }
                        break;
                    case PerfectusDataType.Date:
                        if (ext.ForDataType != "DateFormats")
                        {
                            continue;
                        }
                        break;
                    case PerfectusDataType.Text:
                        if (ext.ForDataType != "TextFormats")
                        {
                            continue;
                        }
                        break;
                    default:
                        continue;
                }

                ListItem item = new ListItem();
                item.Text = ext.LocalisedDisplayName;
                item.Value = String.Format("{0}{1}", "#formatplugin#", ext.ServerSideFormatterPluginKey);

                cbxFormatString.Items.Add(item);

                if (item.Value == formatStringVal)
                {
                    cbxFormatString.SelectedItem = item;
                }
            }
        }

        private string GetConfigKey()
        {
            if (itemBeingEdited != null)
            {
                AnswerProvider ap = (AnswerProvider) (itemBeingEdited);
                switch (ap.DataType)
                {
                    case PerfectusDataType.Number:
                        return
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                                "Perfectus.Client.Studio.Config.NumberFormats");
                    case PerfectusDataType.Date:
                        return
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                                "Perfectus.Client.Studio.Config.DateFormats");
                    case PerfectusDataType.Text:
                        return
                            ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                                "Perfectus.Client.Studio.Config.TextFormats");
                    default:
                        return null;
                }
            }
            else
            {
                return null;
            }
        }

        private void AddStandardValues()
        {
            string key = GetConfigKey();

            if (key == null)
            {
                return;
            }

            try
            {
                string formatStrings = ConfigurationSettings.AppSettings[key];
                if (formatStrings != null)
                {
                    string[] splitSamples = formatStrings.Split('\t');
                    for (int i = 0; i < splitSamples.Length; i += 2)
                    {
                        string name = splitSamples[i].Trim();
                        string expr = splitSamples[i + 1].Trim();
                        ListItem item = new ListItem(name, expr);

                        cbxFormatString.Items.Add(item);

                        if (expr == formatStringVal)
                        {
                            cbxFormatString.SelectedItem = item;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg =
                    string.Format(
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                            "Perfectus.Client.UI.PackageObjectEditors.TypeConverters.FormatStringConverter.ConfigError"),
                        ex.Message);
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(msg, Common.About.FormsTitle);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof (FormatStringEditorForm));
            this.btnAdvanced = new System.Windows.Forms.Button();
            this.txtFormatParam = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbxFormatString = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAdvanced
            // 
            this.btnAdvanced.AccessibleDescription = null;
            this.btnAdvanced.AccessibleName = null;
            resources.ApplyResources(this.btnAdvanced, "btnAdvanced");
            this.btnAdvanced.BackgroundImage = null;
            this.btnAdvanced.CausesValidation = false;
            this.btnAdvanced.Font = null;
            this.btnAdvanced.Name = "btnAdvanced";
            this.btnAdvanced.Click += new System.EventHandler(this.btnAdvanced_Click);
            // 
            // txtFormatParam
            // 
            this.txtFormatParam.AccessibleDescription = null;
            this.txtFormatParam.AccessibleName = null;
            resources.ApplyResources(this.txtFormatParam, "txtFormatParam");
            this.txtFormatParam.BackgroundImage = null;
            this.txtFormatParam.Font = null;
            this.txtFormatParam.Name = "txtFormatParam";
            // 
            // btnOk
            // 
            this.btnOk.AccessibleDescription = null;
            this.btnOk.AccessibleName = null;
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.BackgroundImage = null;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Font = null;
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AccessibleDescription = null;
            this.btnCancel.AccessibleName = null;
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.BackgroundImage = null;
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = null;
            this.btnCancel.Name = "btnCancel";
            // 
            // cbxFormatString
            // 
            this.cbxFormatString.AccessibleDescription = null;
            this.cbxFormatString.AccessibleName = null;
            resources.ApplyResources(this.cbxFormatString, "cbxFormatString");
            this.cbxFormatString.BackgroundImage = null;
            this.cbxFormatString.Font = null;
            this.cbxFormatString.Name = "cbxFormatString";
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            // 
            // FormatStringEditorForm
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this.txtFormatParam);
            this.Controls.Add(this.cbxFormatString);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdvanced);
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormatStringEditorForm";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            DoOkay();
        }

        private void DoOkay()
        {
            if (cbxFormatString.SelectedItem != null)
            {
                ListItem selectedItem = (ListItem) cbxFormatString.SelectedItem;
                formatStringVal = selectedItem.Value;
            }
            else
            {
                formatStringVal = string.Empty;
            }

            formatStringParams = txtFormatParam.Text;
            if (propertyBeingEdited.Name == FORMATSTRINGPROPERTYNAME)
            {
                ((ITemplateItem) itemBeingEdited).FormatString = formatStringVal;
                ((ITemplateItem) itemBeingEdited).FormatStringParams = formatStringParams;
            }
            else
            {
                if (itemBeingEdited is Question)
                {
                    ((Question)itemBeingEdited).InterviewFormatString = formatStringVal;
                    ((Question)itemBeingEdited).InterviewFormatStringParams = formatStringParams;
                }
                else if (itemBeingEdited is PFunction)
                {
                    ((PFunction)itemBeingEdited).InterviewFormatString = formatStringVal;
                    ((PFunction)itemBeingEdited).InterviewFormatStringParams = formatStringParams;
                }               
            }
        }

        private void btnAdvanced_Click(object sender, EventArgs e)
        {
            ShowAdvanced();
        }

        private void ShowAdvanced()
        {
            btnAdvanced.Enabled = false;
            Height = txtFormatParam.Bottom + btnOk.Height + 55;
        }
    }
}