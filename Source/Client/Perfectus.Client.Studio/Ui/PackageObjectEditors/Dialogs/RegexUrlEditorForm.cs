using System;
using System.ComponentModel;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for ValidationEditorForm.
	/// </summary>
	public class RegexUrlEditorForm : Form
	{
		private IWindowsFormsEditorService edSvc;
		private Button btnOk;
		private Button btnCancel;

        private IContainer components;

		private Label lblChooseExp;
		private Button btnAdvanced;
		private TextBox txtUrl;
		private TextBox txtRegexp;
		private RadioButton rdoRegex;
		private RadioButton rdoUrl;
		private ErrorProvider errorProvider1;
        private ComboBox uiComboBox1;


		private RegexUrlValue currentValue = new RegexUrlValue();

		public RegexUrlValue CurrentValue
		{
			get { return currentValue; }
			set
			{
				currentValue = value;
				if (currentValue == null)
				{
					currentValue = new RegexUrlValue();
				}
				ResetUI();
			}
		}

		public RegexUrlEditorForm(IWindowsFormsEditorService edSvc)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			Height = 134;

			this.edSvc = edSvc;

			uiComboBox1.Items.Clear();
			try
			{
				string configKey = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.Config.ValidationExpressions");
				string validationSamples = ConfigurationSettings.AppSettings[configKey];
				if (validationSamples != null)
				{
					string[] splitSamples = validationSamples.Split('\t');
					for (int i = 0; i < splitSamples.Length; i += 2)
					{
						string name = splitSamples[i].Trim();
						string expr = splitSamples[i + 1].Trim();
                        ComboboxItem item = new ComboboxItem() { Text = name, Value = expr };
						uiComboBox1.Items.Add(item);
					}
				}
			}
			catch (Exception ex)
			{
				string msgstr2=ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.RegexUrlEditorForm.LoadConfigError");
				MessageBox.Show(String.Format("{0} ({1})", msgstr2,ex.Message), Common.About.FormsTitle);
			}

			// Navitas issue tracker 3305.
			rdoUrl.Visible = false;
			txtUrl.Visible = false;

		}

		private void ResetUI()
		{
			// If our value is a regex, and it's standard, make it the selected item in the combo
			if (currentValue.ValueType == RegexUrlValue.RegexUrlValueType.Regex && currentValue.RegexValue != null)
			{
				bool foundItem = false;
				foreach (ComboboxItem i in uiComboBox1.Items)
				{
					if (i.Value.ToString() == currentValue.RegexValue)
					{
						uiComboBox1.SelectedItem = i;
						foundItem = true;
						break;
					}
				}

				if (!foundItem)
				{
					rdoRegex.Checked = true;
					rdoUrl.Checked = false;
					ShowAdvanced();
				}

				txtRegexp.Text = currentValue.RegexValue;
			}

			if (currentValue.ValueType == RegexUrlValue.RegexUrlValueType.Url)
			{
				rdoUrl.Checked = true;
				rdoRegex.Checked = false;
				txtUrl.Text = currentValue.UrlValue.ToString();
				ShowAdvanced();
			}

		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegexUrlEditorForm));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblChooseExp = new System.Windows.Forms.Label();
            this.btnAdvanced = new System.Windows.Forms.Button();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtRegexp = new System.Windows.Forms.TextBox();
            this.rdoRegex = new System.Windows.Forms.RadioButton();
            this.rdoUrl = new System.Windows.Forms.RadioButton();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.uiComboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.errorProvider1.SetError(this.btnOk, resources.GetString("btnOk.Error"));
            this.errorProvider1.SetIconAlignment(this.btnOk, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnOk.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.btnOk, ((int)(resources.GetObject("btnOk.IconPadding"))));
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.CausesValidation = false;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.errorProvider1.SetError(this.btnCancel, resources.GetString("btnCancel.Error"));
            this.errorProvider1.SetIconAlignment(this.btnCancel, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnCancel.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.btnCancel, ((int)(resources.GetObject("btnCancel.IconPadding"))));
            this.btnCancel.Name = "btnCancel";
            // 
            // lblChooseExp
            // 
            resources.ApplyResources(this.lblChooseExp, "lblChooseExp");
            this.errorProvider1.SetError(this.lblChooseExp, resources.GetString("lblChooseExp.Error"));
            this.errorProvider1.SetIconAlignment(this.lblChooseExp, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("lblChooseExp.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.lblChooseExp, ((int)(resources.GetObject("lblChooseExp.IconPadding"))));
            this.lblChooseExp.Name = "lblChooseExp";
            // 
            // btnAdvanced
            // 
            resources.ApplyResources(this.btnAdvanced, "btnAdvanced");
            this.btnAdvanced.CausesValidation = false;
            this.errorProvider1.SetError(this.btnAdvanced, resources.GetString("btnAdvanced.Error"));
            this.errorProvider1.SetIconAlignment(this.btnAdvanced, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("btnAdvanced.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.btnAdvanced, ((int)(resources.GetObject("btnAdvanced.IconPadding"))));
            this.btnAdvanced.Name = "btnAdvanced";
            this.btnAdvanced.Click += new System.EventHandler(this.btnAdvanced_Click);
            // 
            // txtUrl
            // 
            resources.ApplyResources(this.txtUrl, "txtUrl");
            this.errorProvider1.SetError(this.txtUrl, resources.GetString("txtUrl.Error"));
            this.errorProvider1.SetIconAlignment(this.txtUrl, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtUrl.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.txtUrl, ((int)(resources.GetObject("txtUrl.IconPadding"))));
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.TextChanged += new System.EventHandler(this.txtUrl_TextChanged);
            this.txtUrl.Validating += new System.ComponentModel.CancelEventHandler(this.txtUrl_Validating);
            // 
            // txtRegexp
            // 
            resources.ApplyResources(this.txtRegexp, "txtRegexp");
            this.errorProvider1.SetError(this.txtRegexp, resources.GetString("txtRegexp.Error"));
            this.errorProvider1.SetIconAlignment(this.txtRegexp, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("txtRegexp.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.txtRegexp, ((int)(resources.GetObject("txtRegexp.IconPadding"))));
            this.txtRegexp.Name = "txtRegexp";
            this.txtRegexp.TextChanged += new System.EventHandler(this.txtRegexp_TextChanged);
            this.txtRegexp.Validating += new System.ComponentModel.CancelEventHandler(this.txtRegexp_Validating);
            // 
            // rdoRegex
            // 
            resources.ApplyResources(this.rdoRegex, "rdoRegex");
            this.rdoRegex.CausesValidation = false;
            this.errorProvider1.SetError(this.rdoRegex, resources.GetString("rdoRegex.Error"));
            this.errorProvider1.SetIconAlignment(this.rdoRegex, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("rdoRegex.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.rdoRegex, ((int)(resources.GetObject("rdoRegex.IconPadding"))));
            this.rdoRegex.Name = "rdoRegex";
            // 
            // rdoUrl
            // 
            resources.ApplyResources(this.rdoUrl, "rdoUrl");
            this.rdoUrl.CausesValidation = false;
            this.errorProvider1.SetError(this.rdoUrl, resources.GetString("rdoUrl.Error"));
            this.errorProvider1.SetIconAlignment(this.rdoUrl, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("rdoUrl.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.rdoUrl, ((int)(resources.GetObject("rdoUrl.IconPadding"))));
            this.rdoUrl.Name = "rdoUrl";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            resources.ApplyResources(this.errorProvider1, "errorProvider1");
            // 
            // uiComboBox1
            // 
            resources.ApplyResources(this.uiComboBox1, "uiComboBox1");
            this.errorProvider1.SetError(this.uiComboBox1, resources.GetString("uiComboBox1.Error"));
            this.uiComboBox1.FormattingEnabled = true;
            this.errorProvider1.SetIconAlignment(this.uiComboBox1, ((System.Windows.Forms.ErrorIconAlignment)(resources.GetObject("uiComboBox1.IconAlignment"))));
            this.errorProvider1.SetIconPadding(this.uiComboBox1, ((int)(resources.GetObject("uiComboBox1.IconPadding"))));
            this.uiComboBox1.Name = "uiComboBox1";
            this.uiComboBox1.SelectedIndexChanged += new System.EventHandler(this.uiComboBox1_SelectedIndexChanged);
            // 
            // RegexUrlEditorForm
            // 
            this.AcceptButton = this.btnOk;
            resources.ApplyResources(this, "$this");
            this.CancelButton = this.btnCancel;
            this.Controls.Add(this.uiComboBox1);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.txtRegexp);
            this.Controls.Add(this.rdoRegex);
            this.Controls.Add(this.rdoUrl);
            this.Controls.Add(this.btnAdvanced);
            this.Controls.Add(this.lblChooseExp);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RegexUrlEditorForm";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.ValidationEditorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private void ValidationEditorForm_Load(object sender, EventArgs e)
		{
		}

		private void btnAdvanced_Click(object sender, EventArgs e)
		{
			ShowAdvanced();

		}

		private void ShowAdvanced()
		{
			btnAdvanced.Enabled = false;
			Height = txtRegexp.Bottom + btnOk.Height + 50;
		}

        private void uiComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboboxItem item = uiComboBox1.SelectedItem as ComboboxItem;
            txtRegexp.Text = item.Value.ToString();
            rdoRegex.Checked = true;
            rdoUrl.Checked = false;
        }

		private void txtRegexp_Validating(object sender, CancelEventArgs e)
		{
            string errormsg = String.Empty;

            if( rdoRegex.Checked && !CurrentValue.IsValidRegex( txtRegexp.Text, ref errormsg ) )
            {
                errorProvider1.SetError( txtRegexp, errormsg );
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
                errorProvider1.SetError( txtRegexp, string.Empty );
            }
		}

		private void txtUrl_Validating(object sender, CancelEventArgs e)
		{
			if (rdoUrl.Checked)
			{
				try
				{
					Uri test = new Uri(txtUrl.Text);
					errorProvider1.SetError(txtUrl, string.Empty);
					e.Cancel = false;
				}
				catch (UriFormatException)
				{
					errorProvider1.SetError(txtUrl, ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageObjectEditors.Dialogs.RegexUrlEditorForm.InvalidServiceUrlError"));
					e.Cancel = true;
				}
			}
			else
			{
				errorProvider1.SetError(txtUrl, string.Empty);
				e.Cancel = false;
			}
		}

		private void txtUrl_TextChanged(object sender, EventArgs e)
		{
			rdoUrl.Checked = true;
			rdoRegex.Checked = false;
		}

		private void txtRegexp_TextChanged(object sender, EventArgs e)
		{
			rdoUrl.Checked = false;
			rdoRegex.Checked = true;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			if (rdoUrl.Checked)
			{
				Uri test = new Uri(txtUrl.Text);
				currentValue.UrlValue = test;
			}
			else if (rdoRegex.Checked)
			{
				currentValue.RegexValue = txtRegexp.Text;
			}
			else
			{
				currentValue.ValueType = RegexUrlValue.RegexUrlValueType.NoValue;
			}
		}
	}
}