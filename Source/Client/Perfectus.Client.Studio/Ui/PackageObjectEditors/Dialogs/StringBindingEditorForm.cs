using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Client.Studio.UI.PackageExplorer;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs
{
	/// <summary>
	/// Summary description for StringBindingEditorForm.
	/// </summary>
	public class StringBindingEditorForm : Form
	{
		protected Button btOk;
		protected Button btCancel;
		protected Panel panel1;
		protected Panel panel2;
		protected Splitter splitter1;
		protected Panel panelExplorer;
		protected Panel panelMain;		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private Package package;
		private PackageItem itemBeingEdited;
		bool checkCircularRef = false;			
		private ExplorerControlMini explorerControlMini;
		private StringBindingEditorControl stringBindingEditorControl1;
		private string val;

		public string Val
		{
			get 
			{ 				
				return val; 
			}
			set 
			{ 
				stringBindingEditorControl1.XmlBody = value; 
			}
		}

		public bool CheckCircularRef
		{
			get
			{
				return checkCircularRef;
			}
			set
			{
				checkCircularRef = value;
			}
		}

		public PackageItem ItemBeingEdited
		{
			get
			{
				return itemBeingEdited;
			}
			set
			{
				itemBeingEdited = value;
			}
		}

		public StringBindingEditorForm(Package packageBeingEdited)
		{
			package = packageBeingEdited;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			explorerControlMini.Package = package;			
			stringBindingEditorControl1.Package = package;
		}		

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(StringBindingEditorForm));
			this.btOk = new System.Windows.Forms.Button();
			this.btCancel = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panelMain = new System.Windows.Forms.Panel();
			this.stringBindingEditorControl1 = new Perfectus.Client.Studio.UI.PackageObjectEditors.Controls.StringBindingEditorControl();
			this.panelExplorer = new System.Windows.Forms.Panel();
			this.explorerControlMini = new Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlMini();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panelMain.SuspendLayout();
			this.panelExplorer.SuspendLayout();
			this.SuspendLayout();
			// 
			// btOk
			// 
			this.btOk.AccessibleDescription = resources.GetString("btOk.AccessibleDescription");
			this.btOk.AccessibleName = resources.GetString("btOk.AccessibleName");
			this.btOk.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btOk.Anchor")));
			this.btOk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btOk.BackgroundImage")));
			this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btOk.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btOk.Dock")));
			this.btOk.Enabled = ((bool)(resources.GetObject("btOk.Enabled")));
			this.btOk.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btOk.FlatStyle")));
			this.btOk.Font = ((System.Drawing.Font)(resources.GetObject("btOk.Font")));
			this.btOk.Image = ((System.Drawing.Image)(resources.GetObject("btOk.Image")));
			this.btOk.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btOk.ImageAlign")));
			this.btOk.ImageIndex = ((int)(resources.GetObject("btOk.ImageIndex")));
			this.btOk.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btOk.ImeMode")));
			this.btOk.Location = ((System.Drawing.Point)(resources.GetObject("btOk.Location")));
			this.btOk.Name = "btOk";
			this.btOk.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btOk.RightToLeft")));
			this.btOk.Size = ((System.Drawing.Size)(resources.GetObject("btOk.Size")));
			this.btOk.TabIndex = ((int)(resources.GetObject("btOk.TabIndex")));
			this.btOk.Text = resources.GetString("btOk.Text");
			this.btOk.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btOk.TextAlign")));
			this.btOk.Visible = ((bool)(resources.GetObject("btOk.Visible")));
			this.btOk.Click += new System.EventHandler(this.btOk_Click);
			// 
			// btCancel
			// 
			this.btCancel.AccessibleDescription = resources.GetString("btCancel.AccessibleDescription");
			this.btCancel.AccessibleName = resources.GetString("btCancel.AccessibleName");
			this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("btCancel.Anchor")));
			this.btCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btCancel.BackgroundImage")));
			this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btCancel.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("btCancel.Dock")));
			this.btCancel.Enabled = ((bool)(resources.GetObject("btCancel.Enabled")));
			this.btCancel.FlatStyle = ((System.Windows.Forms.FlatStyle)(resources.GetObject("btCancel.FlatStyle")));
			this.btCancel.Font = ((System.Drawing.Font)(resources.GetObject("btCancel.Font")));
			this.btCancel.Image = ((System.Drawing.Image)(resources.GetObject("btCancel.Image")));
			this.btCancel.ImageAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btCancel.ImageAlign")));
			this.btCancel.ImageIndex = ((int)(resources.GetObject("btCancel.ImageIndex")));
			this.btCancel.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("btCancel.ImeMode")));
			this.btCancel.Location = ((System.Drawing.Point)(resources.GetObject("btCancel.Location")));
			this.btCancel.Name = "btCancel";
			this.btCancel.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("btCancel.RightToLeft")));
			this.btCancel.Size = ((System.Drawing.Size)(resources.GetObject("btCancel.Size")));
			this.btCancel.TabIndex = ((int)(resources.GetObject("btCancel.TabIndex")));
			this.btCancel.Text = resources.GetString("btCancel.Text");
			this.btCancel.TextAlign = ((System.Drawing.ContentAlignment)(resources.GetObject("btCancel.TextAlign")));
			this.btCancel.Visible = ((bool)(resources.GetObject("btCancel.Visible")));
			// 
			// panel1
			// 
			this.panel1.AccessibleDescription = resources.GetString("panel1.AccessibleDescription");
			this.panel1.AccessibleName = resources.GetString("panel1.AccessibleName");
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel1.Anchor")));
			this.panel1.AutoScroll = ((bool)(resources.GetObject("panel1.AutoScroll")));
			this.panel1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMargin")));
			this.panel1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel1.AutoScrollMinSize")));
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Controls.Add(this.btOk);
			this.panel1.Controls.Add(this.btCancel);
			this.panel1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel1.Dock")));
			this.panel1.Enabled = ((bool)(resources.GetObject("panel1.Enabled")));
			this.panel1.Font = ((System.Drawing.Font)(resources.GetObject("panel1.Font")));
			this.panel1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel1.ImeMode")));
			this.panel1.Location = ((System.Drawing.Point)(resources.GetObject("panel1.Location")));
			this.panel1.Name = "panel1";
			this.panel1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel1.RightToLeft")));
			this.panel1.Size = ((System.Drawing.Size)(resources.GetObject("panel1.Size")));
			this.panel1.TabIndex = ((int)(resources.GetObject("panel1.TabIndex")));
			this.panel1.Text = resources.GetString("panel1.Text");
			this.panel1.Visible = ((bool)(resources.GetObject("panel1.Visible")));
			// 
			// panel2
			// 
			this.panel2.AccessibleDescription = resources.GetString("panel2.AccessibleDescription");
			this.panel2.AccessibleName = resources.GetString("panel2.AccessibleName");
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panel2.Anchor")));
			this.panel2.AutoScroll = ((bool)(resources.GetObject("panel2.AutoScroll")));
			this.panel2.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMargin")));
			this.panel2.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panel2.AutoScrollMinSize")));
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.Controls.Add(this.splitter1);
			this.panel2.Controls.Add(this.panelMain);
			this.panel2.Controls.Add(this.panelExplorer);
			this.panel2.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panel2.Dock")));
			this.panel2.Enabled = ((bool)(resources.GetObject("panel2.Enabled")));
			this.panel2.Font = ((System.Drawing.Font)(resources.GetObject("panel2.Font")));
			this.panel2.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panel2.ImeMode")));
			this.panel2.Location = ((System.Drawing.Point)(resources.GetObject("panel2.Location")));
			this.panel2.Name = "panel2";
			this.panel2.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panel2.RightToLeft")));
			this.panel2.Size = ((System.Drawing.Size)(resources.GetObject("panel2.Size")));
			this.panel2.TabIndex = ((int)(resources.GetObject("panel2.TabIndex")));
			this.panel2.Text = resources.GetString("panel2.Text");
			this.panel2.Visible = ((bool)(resources.GetObject("panel2.Visible")));
			// 
			// splitter1
			// 
			this.splitter1.AccessibleDescription = resources.GetString("splitter1.AccessibleDescription");
			this.splitter1.AccessibleName = resources.GetString("splitter1.AccessibleName");
			this.splitter1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("splitter1.Anchor")));
			this.splitter1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("splitter1.BackgroundImage")));
			this.splitter1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("splitter1.Dock")));
			this.splitter1.Enabled = ((bool)(resources.GetObject("splitter1.Enabled")));
			this.splitter1.Font = ((System.Drawing.Font)(resources.GetObject("splitter1.Font")));
			this.splitter1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("splitter1.ImeMode")));
			this.splitter1.Location = ((System.Drawing.Point)(resources.GetObject("splitter1.Location")));
			this.splitter1.MinExtra = ((int)(resources.GetObject("splitter1.MinExtra")));
			this.splitter1.MinSize = ((int)(resources.GetObject("splitter1.MinSize")));
			this.splitter1.Name = "splitter1";
			this.splitter1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("splitter1.RightToLeft")));
			this.splitter1.Size = ((System.Drawing.Size)(resources.GetObject("splitter1.Size")));
			this.splitter1.TabIndex = ((int)(resources.GetObject("splitter1.TabIndex")));
			this.splitter1.TabStop = false;
			this.splitter1.Visible = ((bool)(resources.GetObject("splitter1.Visible")));
			// 
			// panelMain
			// 
			this.panelMain.AccessibleDescription = resources.GetString("panelMain.AccessibleDescription");
			this.panelMain.AccessibleName = resources.GetString("panelMain.AccessibleName");
			this.panelMain.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelMain.Anchor")));
			this.panelMain.AutoScroll = ((bool)(resources.GetObject("panelMain.AutoScroll")));
			this.panelMain.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelMain.AutoScrollMargin")));
			this.panelMain.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelMain.AutoScrollMinSize")));
			this.panelMain.BackColor = System.Drawing.SystemColors.ControlDark;
			this.panelMain.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelMain.BackgroundImage")));
			this.panelMain.Controls.Add(this.stringBindingEditorControl1);
			this.panelMain.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelMain.Dock")));
			this.panelMain.Enabled = ((bool)(resources.GetObject("panelMain.Enabled")));
			this.panelMain.Font = ((System.Drawing.Font)(resources.GetObject("panelMain.Font")));
			this.panelMain.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelMain.ImeMode")));
			this.panelMain.Location = ((System.Drawing.Point)(resources.GetObject("panelMain.Location")));
			this.panelMain.Name = "panelMain";
			this.panelMain.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelMain.RightToLeft")));
			this.panelMain.Size = ((System.Drawing.Size)(resources.GetObject("panelMain.Size")));
			this.panelMain.TabIndex = ((int)(resources.GetObject("panelMain.TabIndex")));
			this.panelMain.Text = resources.GetString("panelMain.Text");
			this.panelMain.Visible = ((bool)(resources.GetObject("panelMain.Visible")));
			// 
			// stringBindingEditorControl1
			// 
			this.stringBindingEditorControl1.AccessibleDescription = resources.GetString("stringBindingEditorControl1.AccessibleDescription");
			this.stringBindingEditorControl1.AccessibleName = resources.GetString("stringBindingEditorControl1.AccessibleName");
			this.stringBindingEditorControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("stringBindingEditorControl1.Anchor")));
			this.stringBindingEditorControl1.AutoScroll = ((bool)(resources.GetObject("stringBindingEditorControl1.AutoScroll")));
			this.stringBindingEditorControl1.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("stringBindingEditorControl1.AutoScrollMargin")));
			this.stringBindingEditorControl1.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("stringBindingEditorControl1.AutoScrollMinSize")));
			this.stringBindingEditorControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("stringBindingEditorControl1.BackgroundImage")));
			this.stringBindingEditorControl1.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("stringBindingEditorControl1.Dock")));
			this.stringBindingEditorControl1.Enabled = ((bool)(resources.GetObject("stringBindingEditorControl1.Enabled")));
			this.stringBindingEditorControl1.Font = ((System.Drawing.Font)(resources.GetObject("stringBindingEditorControl1.Font")));
			this.stringBindingEditorControl1.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("stringBindingEditorControl1.ImeMode")));
			this.stringBindingEditorControl1.Location = ((System.Drawing.Point)(resources.GetObject("stringBindingEditorControl1.Location")));
			this.stringBindingEditorControl1.Name = "stringBindingEditorControl1";
			this.stringBindingEditorControl1.Package = null;
			this.stringBindingEditorControl1.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("stringBindingEditorControl1.RightToLeft")));
			this.stringBindingEditorControl1.Size = ((System.Drawing.Size)(resources.GetObject("stringBindingEditorControl1.Size")));
			this.stringBindingEditorControl1.TabIndex = ((int)(resources.GetObject("stringBindingEditorControl1.TabIndex")));
			this.stringBindingEditorControl1.Visible = ((bool)(resources.GetObject("stringBindingEditorControl1.Visible")));
			this.stringBindingEditorControl1.XmlBody = null;
			// 
			// panelExplorer
			// 
			this.panelExplorer.AccessibleDescription = resources.GetString("panelExplorer.AccessibleDescription");
			this.panelExplorer.AccessibleName = resources.GetString("panelExplorer.AccessibleName");
			this.panelExplorer.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("panelExplorer.Anchor")));
			this.panelExplorer.AutoScroll = ((bool)(resources.GetObject("panelExplorer.AutoScroll")));
			this.panelExplorer.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("panelExplorer.AutoScrollMargin")));
			this.panelExplorer.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("panelExplorer.AutoScrollMinSize")));
			this.panelExplorer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelExplorer.BackgroundImage")));
			this.panelExplorer.Controls.Add(this.explorerControlMini);
			this.panelExplorer.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("panelExplorer.Dock")));
			this.panelExplorer.Enabled = ((bool)(resources.GetObject("panelExplorer.Enabled")));
			this.panelExplorer.Font = ((System.Drawing.Font)(resources.GetObject("panelExplorer.Font")));
			this.panelExplorer.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("panelExplorer.ImeMode")));
			this.panelExplorer.Location = ((System.Drawing.Point)(resources.GetObject("panelExplorer.Location")));
			this.panelExplorer.Name = "panelExplorer";
			this.panelExplorer.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("panelExplorer.RightToLeft")));
			this.panelExplorer.Size = ((System.Drawing.Size)(resources.GetObject("panelExplorer.Size")));
			this.panelExplorer.TabIndex = ((int)(resources.GetObject("panelExplorer.TabIndex")));
			this.panelExplorer.Text = resources.GetString("panelExplorer.Text");
			this.panelExplorer.Visible = ((bool)(resources.GetObject("panelExplorer.Visible")));
			// 
			// explorerControlMini
			// 
			this.explorerControlMini.AccessibleDescription = resources.GetString("explorerControlMini.AccessibleDescription");
			this.explorerControlMini.AccessibleName = resources.GetString("explorerControlMini.AccessibleName");
			this.explorerControlMini.Anchor = ((System.Windows.Forms.AnchorStyles)(resources.GetObject("explorerControlMini.Anchor")));
			this.explorerControlMini.AutoScroll = ((bool)(resources.GetObject("explorerControlMini.AutoScroll")));
			this.explorerControlMini.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("explorerControlMini.AutoScrollMargin")));
			this.explorerControlMini.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("explorerControlMini.AutoScrollMinSize")));
			this.explorerControlMini.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("explorerControlMini.BackgroundImage")));
			this.explorerControlMini.Dock = ((System.Windows.Forms.DockStyle)(resources.GetObject("explorerControlMini.Dock")));
			this.explorerControlMini.Enabled = ((bool)(resources.GetObject("explorerControlMini.Enabled")));
			this.explorerControlMini.Font = ((System.Drawing.Font)(resources.GetObject("explorerControlMini.Font")));
			this.explorerControlMini.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("explorerControlMini.ImeMode")));
			this.explorerControlMini.Location = ((System.Drawing.Point)(resources.GetObject("explorerControlMini.Location")));
			this.explorerControlMini.Name = "explorerControlMini";
			this.explorerControlMini.Package = null;
			this.explorerControlMini.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("explorerControlMini.RightToLeft")));
			this.explorerControlMini.Size = ((System.Drawing.Size)(resources.GetObject("explorerControlMini.Size")));
			this.explorerControlMini.TabIndex = ((int)(resources.GetObject("explorerControlMini.TabIndex")));
			this.explorerControlMini.Visible = ((bool)(resources.GetObject("explorerControlMini.Visible")));
			// 
			// StringBindingEditorForm
			// 
			this.AccessibleDescription = resources.GetString("$this.AccessibleDescription");
			this.AccessibleName = resources.GetString("$this.AccessibleName");
			this.AutoScaleBaseSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScaleBaseSize")));
			this.AutoScroll = ((bool)(resources.GetObject("$this.AutoScroll")));
			this.AutoScrollMargin = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMargin")));
			this.AutoScrollMinSize = ((System.Drawing.Size)(resources.GetObject("$this.AutoScrollMinSize")));
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = ((System.Drawing.Size)(resources.GetObject("$this.ClientSize")));
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Enabled = ((bool)(resources.GetObject("$this.Enabled")));
			this.Font = ((System.Drawing.Font)(resources.GetObject("$this.Font")));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.ImeMode = ((System.Windows.Forms.ImeMode)(resources.GetObject("$this.ImeMode")));
			this.Location = ((System.Drawing.Point)(resources.GetObject("$this.Location")));
			this.MaximizeBox = false;
			this.MaximumSize = ((System.Drawing.Size)(resources.GetObject("$this.MaximumSize")));
			this.MinimizeBox = false;
			this.MinimumSize = ((System.Drawing.Size)(resources.GetObject("$this.MinimumSize")));
			this.Name = "StringBindingEditorForm";
			this.RightToLeft = ((System.Windows.Forms.RightToLeft)(resources.GetObject("$this.RightToLeft")));
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = ((System.Windows.Forms.FormStartPosition)(resources.GetObject("$this.StartPosition")));
			this.Text = resources.GetString("$this.Text");
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			this.panelExplorer.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

			
		private void btOk_Click(object sender, EventArgs e)
		{			
			if(stringBindingEditorControl1 != null)
			{
				val = stringBindingEditorControl1.XmlBody;

				if(checkCircularRef && itemBeingEdited != null && StringBindingMod.IsCircularRefExist(val,itemBeingEdited))
				{
					// restore before showing error.
					stringBindingEditorControl1.XmlBody = val;
					MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs.StringBindingEditorForm.CircularReferenceError"),Common.About.FormsTitle);					
					this.DialogResult = DialogResult.None;
				}
			}
			else
			{
				val = null;
			}
			
		}
		
	}
}
