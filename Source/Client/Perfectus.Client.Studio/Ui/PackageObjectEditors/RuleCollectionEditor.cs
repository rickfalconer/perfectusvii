using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for StringEditor.
	/// </summary>
	public class RuleCollectionEditor : UITypeEditor
	{
		private RuleCollectionEditorForm ui;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Launch the LargeTextEditorForm.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sp"></param>
		/// <param name="value">The current value of the property.</param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
		{
			// get the editor service.
			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService) sp.GetService(typeof (IWindowsFormsEditorService));

			if (edSvc == null)
			{
				// uh oh.
				return value;
			}

			//TODO: Handle pageItems as well as packageItems. See IntQuestionEditor code.
			Package package = null;

			object c = context.Instance as PackageItem;
			if (c != null)
			{					
				package = ((PackageItem) c).ParentPackage;
			}

			ui = new RuleCollectionEditorForm();
			ui.Package = package;			

			if (value == null)
			{
				ui.Rules = new Rule2Collection();
			}
			else
			{
				ui.Rules = (Rule2Collection) value;
			}

			ui.StartPosition = FormStartPosition.CenterParent;
			
			DialogResult dr = edSvc.ShowDialog(ui);
			Rule2Collection result = ui.Rules;

			ui.Dispose();
			if (dr == DialogResult.OK)
				return result;
			else
				return value;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// Specify that this editor is the popup style ([...] button)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Modal</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}
	}
}