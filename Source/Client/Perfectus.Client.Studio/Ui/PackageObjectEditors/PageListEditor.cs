using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for YesNoQueryEditor.
	/// </summary>
	public class PageListEditor : UITypeEditor
	{
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
		{
			PageListEditorControl ui = null;

			// get the editor service.
			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService) sp.GetService(typeof (IWindowsFormsEditorService));

			if (edSvc == null)
			{
				// uh oh.
				return value;
			}

			// create our UI
			if (ui == null)
			{
				ui = new PageListEditorControl(edSvc);
				object c = context.Instance;
				Interview interview = null;
				
				if( c != null)
				{
					if(c is Interview)
					{
						interview = c as Interview;
					}
					else if(c is Converter)
					{
						if(((Converter)c).ParentPackage.Interviews.Count > 0)
						{
							//TODO: We are making the assumption there is only ever 1 Interview, which is true currently, but may change.
							interview = ((Converter)c).ParentPackage.Interviews[0];
						}
					}
				}

				if (interview != null)
				{
					ui.Pages = interview.Pages;
				}
				else
				{
					ui.Pages = null;
				}

				ui.Value = value as InterviewPage;
			}

			edSvc.DropDownControl(ui);

			return ui.Value;
		}

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Specify that this editor is the drop down style (instead of the [...] popup style)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>DropDown</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}

		public override bool GetPaintValueSupported(ITypeDescriptorContext context)
		{
			return false;
		}
	}
}