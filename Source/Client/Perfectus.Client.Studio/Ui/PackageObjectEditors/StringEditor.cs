using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors
{
	/// <summary>
	/// Summary description for StringEditor.
	/// </summary>
	public class StringEditor : UITypeEditor
	{
		private StringEditorForm ui;

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		///		Launch the LargeTextEditorForm.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sp"></param>
		/// <param name="value">The current value of the property.</param>
		/// <returns></returns>
		// ------------------------------------------------------------------------------------------------------------------------------
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
        {
            // get the editor service.
            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)sp.GetService(typeof(IWindowsFormsEditorService));
            DialogResult result;

            if (edSvc == null)
            {
                // uh oh.
                return value;
            }

            // Ensure the property changed event is fired
            if (context.Instance is LibraryItem && ((LibraryItem)context.Instance).LibraryUniqueIdentifier != Guid.Empty &&
                ((LibraryItem)context.Instance).Linked == true &&
                ((LibraryItem)context.Instance).CheckedOutStatus == CheckOutStatus.CheckedIn)
            {
                result = MessageBox.Show(Globals.gcCHECK_OUT_MESSAGE, Globals.gcSHARED_LIBRARY_TITLE,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            }
            else
            {
                result = DialogResult.Ignore;
            }

            if (result == DialogResult.Yes || result == DialogResult.No || result == DialogResult.Ignore)
            {
                // Check, unlink (if shared library item), else ignore and continue
                switch (result)
                {
                    case DialogResult.Yes:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcCHECKOUT_MARKER);
                        break;
                    case DialogResult.No:
                        ((LibraryItem)context.Instance).DefinitionBeingEdited(Globals.gcUNLINK_MARKER);
                        break;
                    case DialogResult.Ignore:
                        break;
                }

                ui = new StringEditorForm();
                if (value == null)
                {
                    ui.Val = null;
                }
                else
                {
                    ui.Val = value.ToString();
                }
                DialogResult dr = edSvc.ShowDialog(ui);
                string resultVal = ui.Val;

                ui.Dispose();
                if (dr == DialogResult.OK)
                    return resultVal;
                else
                    return value;
            }
            else
            {
                // Do nothing
                return value;
            }
        }

		// ------------------------------------------------------------------------------------------------------------------------------
		/// <summary>
		/// Specify that this editor is the popup style ([...] button)
		/// </summary>
		/// <param name="context"></param>
		/// <returns>Modal</returns>
		// ------------------------------------------------------------------------------------------------------------------------------
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}
	}
}
