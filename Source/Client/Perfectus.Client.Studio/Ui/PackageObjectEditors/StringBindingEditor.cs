//using System;
//using System.ComponentModel;
//using System.Drawing.Design;
//using System.Windows.Forms;
//using System.Windows.Forms.Design;
//using Perfectus.Client.Studio.UI.PackageObjectEditors.Dialogs;
//using Perfectus.Common.PackageObjects;
//
//namespace Perfectus.Client.Studio.UI.PackageObjectEditors
//{
//	/// <summary>
//	/// Summary description for StringEditor.
//	/// </summary>
//	public class StringBindingEditor : UITypeEditor
//	{		
//		// ------------------------------------------------------------------------------------------------------------------------------
//		/// <summary>
//		///		Launch the StringBindingEditor Form.
//		/// </summary>
//		/// <param name="context"></param>
//		/// <param name="sp"></param>
//		/// <param name="value">The current value of the property.</param>
//		/// <returns></returns>
//		// ------------------------------------------------------------------------------------------------------------------------------
//		public override object EditValue(ITypeDescriptorContext context, IServiceProvider sp, object value)
//		{
//			// get the editor service.
//			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService) sp.GetService(typeof (IWindowsFormsEditorService));
//
//			if (edSvc == null)
//			{
//				// uh oh.
//				return value;
//			}
//			
//			//TODO. Handle pageItems as well as packageItems. See IntQuestionEditor code.
//			Package package = null;
//
//			object c = context.Instance as PackageItem;
//			if (c != null)
//			{					
//				package = ((PackageItem) c).ParentPackage;
//			}
//
//			using(StringBindingEditorForm ui = new StringBindingEditorForm(package))
//			{
//			
//				if (value == null)
//				{
//					ui.Val = null;
//				}
//				else
//				{
//					ui.Val = value.ToString();
//				}
//			
//				ui.StartPosition = FormStartPosition.CenterScreen;
//				ui.ItemBeingEdited = (PackageItem) c;
//
//				// if we are editing the default answer property, we need to tell the form a circular refernce check must be performed.
//				if(context.PropertyDescriptor.Name.ToUpper().IndexOf("DEFAULT") >= 0)
//				{
//					ui.CheckCircularRef = true;
//				}
//            
//				// we are not using edSv.ShowDialog() because the html editor has a bug whereby it is getting confused and wont close the dialog correctly, something going wrong in Dispose() probably.
//				//DialogResult dr = edSv.ShowDialog();
//				DialogResult dr = ui.ShowDialog();				
//			
//				string result = ui.Val;
//				if (dr == DialogResult.OK)
//				{
//					return result;				
//				}
//				else
//				{
//					return value;
//				}
//			}
//		}
//		
//		// ------------------------------------------------------------------------------------------------------------------------------
//		/// <summary>
//		/// Specify that this editor is the popup style ([...] button)
//		/// </summary>
//		/// <param name="context"></param>
//		/// <returns>Modal</returns>
//		// ------------------------------------------------------------------------------------------------------------------------------
//		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
//		{
//			return UITypeEditorEditStyle.Modal;
//		}
//	}
//}