using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Tasks
{
	/// <summary>
	/// Summary description for BulkAdd.
	/// </summary>
	public class BulkAdd : UserControl
	{
		private DataGrid dataGrid1;
		private Button btnAdd;
        private Button btnClear;
		private Package package = null;
		private DataTable itemsToAdd;
		private DataGridTableStyle dataGridTableStyle1;
		private DataGridTextBoxColumn dataGridTextBoxColumn1;

		public Package CurrentPackage
		{
			get { return package; }
			set
			{
				package = value;
				btnAdd.Enabled = (package != null);
			}
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public BulkAdd()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			components = new Container();

			itemsToAdd = new DataTable("ItemsToAdd");
			itemsToAdd.Locale = CultureInfo.CurrentUICulture;
			itemsToAdd.Columns.Add(new DataColumn("ItemName", typeof (string)));
			dataGrid1.DataSource = itemsToAdd;

			components.Add(dataGrid1);
			components.Add(itemsToAdd);

			SetColumnSize();
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BulkAdd));
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGrid1
            // 
            this.dataGrid1.AllowNavigation = false;
            this.dataGrid1.AllowSorting = false;
            resources.ApplyResources(this.dataGrid1, "dataGrid1");
            this.dataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataGrid1.ColumnHeadersVisible = false;
            this.dataGrid1.DataMember = "";
            this.dataGrid1.FlatMode = true;
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.ParentRowsVisible = false;
            this.dataGrid1.RowHeadersVisible = false;
            this.dataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.dataGridTableStyle1});
            this.dataGrid1.Navigate += new System.Windows.Forms.NavigateEventHandler(this.dataGrid1_Navigate);
            this.dataGrid1.Resize += new System.EventHandler(this.dataGrid1_Resize);
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.AllowSorting = false;
            this.dataGridTableStyle1.ColumnHeadersVisible = false;
            this.dataGridTableStyle1.DataGrid = this.dataGrid1;
            this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1});
            this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridTableStyle1.MappingName = "ItemsToAdd";
            resources.ApplyResources(this.dataGridTableStyle1, "dataGridTableStyle1");
            this.dataGridTableStyle1.RowHeadersVisible = false;
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            resources.ApplyResources(this.dataGridTextBoxColumn1, "dataGridTextBoxColumn1");
            // 
            // btnAdd
            // 
            resources.ApplyResources(this.btnAdd, "btnAdd");
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // BulkAdd
            // 
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dataGrid1);
            resources.ApplyResources(this, "$this");
            this.Name = "BulkAdd";
            this.Resize += new System.EventHandler(this.BulkAdd_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private void btnClear_Click(object sender, EventArgs e)
		{
			itemsToAdd.Clear();
			itemsToAdd.AcceptChanges();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			ArrayList rowsToDelete = new ArrayList(itemsToAdd.Rows.Count);
			Cursor = Cursors.WaitCursor;
			try
			{
				for (int i = 0; i < itemsToAdd.Rows.Count; i++)
				{
					DataRow dr = itemsToAdd.Rows[i];
					if (Convert.IsDBNull(dr["ItemName"]))
					{
						rowsToDelete.Add(dr);
						continue;
					}
					else
					{
						string itemName = dr["ItemName"].ToString();
						Question q = package.CreateQuestion();
						q.Name = itemName;
						q.Prompt.TextValue = itemName;
						rowsToDelete.Add(dr);
					}
				}
			}
			finally
			{
				for (int i = rowsToDelete.Count - 1; i >= 0; i --)
				{
					((DataRow) rowsToDelete[i]).Delete();
				}
				itemsToAdd.AcceptChanges();
				Cursor = Cursors.Default;
			}
		}

		private void dataGrid1_Navigate(object sender, NavigateEventArgs ne)
		{
		}

		private void dataGrid1_Resize(object sender, EventArgs e)
		{
			SetColumnSize();
		}

		private void SetColumnSize()
		{
			dataGrid1.SuspendLayout();
			dataGrid1.TableStyles[0].GridColumnStyles[0].Width = dataGrid1.Width - 34;
			dataGrid1.ResumeLayout();
			dataGrid1.Invalidate(true);
		}

		//TODO: Remove empty handlers like this one...
		private void BulkAdd_Resize(object sender, EventArgs e)
		{
		}
	}
}