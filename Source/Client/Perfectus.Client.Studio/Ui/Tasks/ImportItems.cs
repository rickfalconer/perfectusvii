using System;
using System.ComponentModel;
using System.Windows.Forms;
//using DataDynamics.SharpUI.Input;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Tasks
{
	/// <summary>
	/// Summary description for Import.
	/// </summary>
	public class ImportItems : UserControl
    {
        private Package package;
		private LinkLabel lblPackageImport;
        private OpenFileDialog openFileDialog2;
		private IContainer components;

		public Package CurrentPackage
		{
			get { return package; }
			set { package = value; }
		}

		public ImportItems()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//TODO: Add any initialization after the InitializeComponent call

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportItems));
            this.lblPackageImport = new System.Windows.Forms.LinkLabel();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // lblPackageImport
            // 
            resources.ApplyResources(this.lblPackageImport, "lblPackageImport");
            this.lblPackageImport.BackColor = System.Drawing.Color.LightGray;
            this.lblPackageImport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblPackageImport.Name = "lblPackageImport";
            this.lblPackageImport.TabStop = true;
            this.lblPackageImport.UseCompatibleTextRendering = true;
            this.lblPackageImport.Click += new System.EventHandler(this.lblPackageImport_Click);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.DefaultExt = "*.ip";
            resources.ApplyResources(this.openFileDialog2, "openFileDialog2");
            // 
            // ImportItems
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblPackageImport);
            resources.ApplyResources(this, "$this");
            this.Name = "ImportItems";
            this.ResumeLayout(false);

		}

		#endregion

		private void uiGradientBand1_Click(object sender, EventArgs e)
		{
		}

		private void lblPackageImport_Click(object sender, EventArgs e)
		{
			try
			{
				DialogResult dr = openFileDialog2.ShowDialog();
				if (dr == DialogResult.OK && package != null)
				{
					Cursor = Cursors.WaitCursor;
                    package.ImportQuestionsFromPackage( openFileDialog2.FileName );
                }
			}
			catch (Exception ex)
			{
                MessageBox.Show(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").
                    GetString("Perfectus.Client.Studio.UI.Tasks.ImportItems.ReadingPackageError"), ex.Message), Common.About.FormsTitle);
			}
			finally
			{
				Cursor = Cursors.Default;
			}

		}
	}
}
