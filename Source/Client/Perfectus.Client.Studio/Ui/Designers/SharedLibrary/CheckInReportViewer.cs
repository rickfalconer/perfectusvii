using System.IO;
using System.Windows.Forms;
using System.Configuration;
using Perfectus.Common.SharedLibrary;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers.SharedLibrary
{
	/// <summary>
	/// Summary description for CheckInReportViewer.
	/// </summary>
	public class CheckInReportViewer : DesignerBase
	{
		private LibraryItem libraryItem = null;
		private WebBrowser axWebBrowser1;
		private string reportPath = null;
		private System.Windows.Forms.Label DescriptionLabel;
		private System.Windows.Forms.Label TypeLabel;
		private System.Windows.Forms.Label NameLabel;
		private System.Windows.Forms.Label PackageAffectedLabel;
		private System.Windows.Forms.Button ContinueButton;
		private System.Windows.Forms.Button ExitButton;
		private System.Windows.Forms.Label CheckInNameValue;
		private System.Windows.Forms.Label CheckInTypeValue;
		private System.Windows.Forms.GroupBox groupBox1;

		public override PrefferedDesigner DefaultDesigner
		{
			get { return PrefferedDesigner.ReportViewer;}
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public CheckInReportViewer(LibraryItem item) : base(item)
		{		
			InitializeComponent();

			// Set the report url - including the query string parameters for the specific library item
			libraryItem = item;
			this.Text = libraryItem.Name;

			string url = ConfigurationSettings.AppSettings["reportingServerUrl"];

			if (url[url.Length - 1] != '/')
			{
				url += "/";
			}

			string filePath = string.Format(@"reports/report.aspx?report=library&itemId={0}&itemType={1}&itemName={2}", libraryItem.LibraryUniqueIdentifier, libraryItem.LibraryType, libraryItem.Name);
			this.reportPath = url + filePath;

			// Set the display of the form
			CheckInNameValue.Text = libraryItem.Name;

			switch (libraryItem.LibraryType)
			{
				case LibraryItemType.Function:
					if (libraryItem.GetType() == typeof(ArithmeticPFunction))
						CheckInTypeValue.Text = "Arithmetic Function";
					else
						CheckInTypeValue.Text = "Date Function";
					break;
				case LibraryItemType.InterviewPage:
					CheckInTypeValue.Text = "Interview Page";
					break;
				case LibraryItemType.Outcome:
					CheckInTypeValue.Text = "Outcome";
					break;
				case LibraryItemType.Question:
					CheckInTypeValue.Text = "Question";
					break;
				case LibraryItemType.SimpleOutcome:
					CheckInTypeValue.Text = "Conditional Text";
					break;
				case LibraryItemType.Template:
					CheckInTypeValue.Text = "Template";
					break;
			}

		}

		public void Navigate()
		{			
			if (reportPath != null)
			{				
				axWebBrowser1.Navigate(reportPath);
			}
		}
	
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public enum ReportType
		{
			AllClauses,
			LibraryItem
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckInReportViewer));
            this.axWebBrowser1 = new WebBrowser();
            this.DescriptionLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.CheckInNameValue = new System.Windows.Forms.Label();
            this.CheckInTypeValue = new System.Windows.Forms.Label();
            this.PackageAffectedLabel = new System.Windows.Forms.Label();
            this.ContinueButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // axWebBrowser1
            // 
            this.axWebBrowser1.AccessibleDescription = null;
            this.axWebBrowser1.AccessibleName = null;
            resources.ApplyResources(this.axWebBrowser1, "axWebBrowser1");
            this.axWebBrowser1.Font = null;
            // 
            // DescriptionLabel
            // 
            this.DescriptionLabel.AccessibleDescription = null;
            this.DescriptionLabel.AccessibleName = null;
            resources.ApplyResources(this.DescriptionLabel, "DescriptionLabel");
            this.DescriptionLabel.Font = null;
            this.DescriptionLabel.Name = "DescriptionLabel";
            // 
            // NameLabel
            // 
            this.NameLabel.AccessibleDescription = null;
            this.NameLabel.AccessibleName = null;
            resources.ApplyResources(this.NameLabel, "NameLabel");
            this.NameLabel.Name = "NameLabel";
            // 
            // TypeLabel
            // 
            this.TypeLabel.AccessibleDescription = null;
            this.TypeLabel.AccessibleName = null;
            resources.ApplyResources(this.TypeLabel, "TypeLabel");
            this.TypeLabel.Name = "TypeLabel";
            // 
            // CheckInNameValue
            // 
            this.CheckInNameValue.AccessibleDescription = null;
            this.CheckInNameValue.AccessibleName = null;
            resources.ApplyResources(this.CheckInNameValue, "CheckInNameValue");
            this.CheckInNameValue.Font = null;
            this.CheckInNameValue.Name = "CheckInNameValue";
            // 
            // CheckInTypeValue
            // 
            this.CheckInTypeValue.AccessibleDescription = null;
            this.CheckInTypeValue.AccessibleName = null;
            resources.ApplyResources(this.CheckInTypeValue, "CheckInTypeValue");
            this.CheckInTypeValue.Font = null;
            this.CheckInTypeValue.Name = "CheckInTypeValue";
            // 
            // PackageAffectedLabel
            // 
            this.PackageAffectedLabel.AccessibleDescription = null;
            this.PackageAffectedLabel.AccessibleName = null;
            resources.ApplyResources(this.PackageAffectedLabel, "PackageAffectedLabel");
            this.PackageAffectedLabel.Name = "PackageAffectedLabel";
            // 
            // ContinueButton
            // 
            this.ContinueButton.AccessibleDescription = null;
            this.ContinueButton.AccessibleName = null;
            resources.ApplyResources(this.ContinueButton, "ContinueButton");
            this.ContinueButton.BackgroundImage = null;
            this.ContinueButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ContinueButton.Font = null;
            this.ContinueButton.Name = "ContinueButton";
            // 
            // ExitButton
            // 
            this.ExitButton.AccessibleDescription = null;
            this.ExitButton.AccessibleName = null;
            resources.ApplyResources(this.ExitButton, "ExitButton");
            this.ExitButton.BackgroundImage = null;
            this.ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitButton.Font = null;
            this.ExitButton.Name = "ExitButton";
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleDescription = null;
            this.groupBox1.AccessibleName = null;
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.BackgroundImage = null;
            this.groupBox1.Controls.Add(this.DescriptionLabel);
            this.groupBox1.Font = null;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // CheckInReportViewer
            // 
            this.AcceptButton = this.ExitButton;
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.CancelButton = this.ExitButton;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.ContinueButton);
            this.Controls.Add(this.PackageAffectedLabel);
            this.Controls.Add(this.CheckInTypeValue);
            this.Controls.Add(this.CheckInNameValue);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.axWebBrowser1);
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckInReportViewer";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion
	}
}
