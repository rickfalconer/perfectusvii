using System;
using mshtml;
using Perfectus.Common.PackageObjects;
using System.Text.RegularExpressions;
using System.Xml;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Contains static methods that are useful for any editors that need perfectus string binding.
	/// </summary>
	public abstract class StringBindingMod
	{
		#region String binding parsing, xml to html, html to xml, etc.

		/// <summary>
		/// Converts editor HTML to XML that a perfectus server can understand.
		/// </summary>
		/// <param name="doc">The document to convert</param>
		/// <param name="removeOtherHtml">Indicates if HTML, for example Bold tags should be stripped.</param>
		/// <returns>A string containing the converted XML</returns>
		public static string GetXmlFromDoc(HTMLDocument doc, bool removeOtherHtml)
		{
			// This process involves:
			// get html -> make a string containing markers -> save back to body.innerHtml -> convert our new InnerText (which has our markers, but mshtml fluff removed) to XML.
			string retVal = null;
			AddMarker(doc);
			if(removeOtherHtml)
			{
                //FB669 - Hitting 'enter' creates <P></P> whereas 'shift-enter' results in '\r\n'
                // Want to convert paragraph into break to preserve the visual appearance 
                String tmpInnerHTML = doc.body.innerHTML;
                if (null != tmpInnerHTML) // FB956
                {
                    tmpInnerHTML = tmpInnerHTML.Replace("<P>", "")
                                 .Replace("</P>", "<BR>")
                        // convert \r\n to <BR>, otherwise reassignment to 'innerHTML' would loose the break
                                 .Replace(System.Environment.NewLine, "<BR>");
                    doc.body.innerHTML = tmpInnerHTML;
                }
				retVal = GetXmlFromInnerText(doc.body.innerText);
			}
			else
			{
				retVal = GetXmlFromInnerText(doc.body.innerHTML); // we dont always want to strip html (eg, the page item htmlItem).
			}

			return retVal;
		}

		private static string AddMarker(mshtml.HTMLDocument doc)
		{
			IHTMLElementCollection spanTags;
			spanTags = doc.getElementsByTagName("SPAN");

			foreach(IHTMLElement el in spanTags)
			{
				if(el.getAttribute("type",0).ToString().ToLower() == "stringitem")
				{		
					// "{=}" is used as a mark that InnerTextToXml can pick up.
					string markerText = "{MARKER+ITEMNAME{=}ID{=}";
					string uId = el.getAttribute("Id",0).ToString();
					markerText += uId;					
					string defaultText = GetDefaultDisplay(el);
				    object fieldId = el.getAttribute("fieldId", 0);                    
					markerText += "{=}" + defaultText;
				    markerText += "{=}" + (fieldId != null ? fieldId.ToString() : string.Empty);
					markerText += "{=}END}";					
					el.insertAdjacentText("beforeBegin",markerText);
					el.outerHTML = "";
				}
			}			
			
			return doc.body.innerHTML;
		}
		
		private static string GetXmlFromInnerText(string innerText)
		{
			string retVal = innerText;

			// make an array of our marker strings.
			Regex splitReg = new Regex("{MARKER");						
			string[] markers;
			
			if(retVal != null && retVal.IndexOf("{MARKER") >= 0)
			{
				markers = splitReg.Split(retVal);	
			}
			else
			{
				return retVal;
			}

			// loop through array of marker strings, split into an array items based on "{=}" 
			foreach(string s in markers)
			{
				if(s.IndexOf("END}") > 0)
				{
					// make an array of items we want to retrive.
					Regex splitRegItem = new Regex("{=}");						
					string[] markerItem = splitRegItem.Split(s);

					string nodeXml = GetNodeXml(markerItem[2],markerItem[3], markerItem[4]);					
					
					// replace the first instance of "{Marker", to first instance of "end}" with our new node text. It's safe to do this because of the order of items in markers.
					int startPo = retVal.IndexOf("{MARKER");
					int finishPo = retVal.IndexOf("END}");

					retVal = retVal.Remove(startPo,finishPo - startPo + 4); // + 4 is to remove "END}"
					retVal = retVal.Insert(startPo,nodeXml);                    
				}
			}

			// add outside xml element
			retVal = string.Format("<text>{0}</text>",retVal);
			return retVal;
		}

		private static string GetDefaultDisplay(IHTMLElement spanEl)
		{        
			string retVal = "";

			foreach(IHTMLElement el in (IHTMLElementCollection) spanEl.children)
			{
				if(el.getAttribute("type",0).ToString().ToLower() == "defaultdisplay")
				{					
					retVal = el.innerText; 
					break;
				}
			}		

			return retVal;

		}

		private static string GetNodeXml(string uId, string defaultText, string fieldId)
		{
            if (fieldId != null && fieldId.Length > 0)
            {
                return string.Format("<item uId=\"{0}\" fieldId=\"{2}\">{1}</item>", uId, defaultText, fieldId);
            }
            else
            {
                return string.Format("<item uId=\"{0}\">{1}</item>",uId,defaultText);
            }
		}

		/// <summary>
		/// Takes XML that was previously produced by a string binding editor, and converts it back to HTML.
		/// </summary>
		/// <param name="xmlBody">A string containing the XML to convert</param>
		/// <param name="package">The package being edited, this is used to help recreate the tags.</param>
		/// <returns></returns>
		public static string GetDocHtmlFromXml(string xmlBody, Package package)
		{
			return GetDocHtmlFromXml(xmlBody,package,false, false);
		}

        // Fix for FB1135
        public static string GetDocHtmlFromXmlForHTMLItem(string xmlBody, Package package)
        {
            return GetDocHtmlFromXml(xmlBody, package, false, true);
        }
        // Old signature (retain to avoid changing other sourec code)
        public static string GetDocHtmlFromXml(string xmlBody, Package package, bool isQuestionItem)
        {
            return GetDocHtmlFromXml(xmlBody, package, isQuestionItem, false);
        }

		/// <summary>
		/// Takes XML that was previously produced by a string binding editor, and converts it back to HTML.
		/// </summary>
		/// <param name="xmlBody">A string containing the XML to convert</param>
		/// <param name="package">The package being edited, this is used to help recreate the tags.</param>
		/// <param name="isQuestionItem">A bool representing if the HTML we are making is to be displayed on a QuestionItem. We dont want xml to be displayed to the user.</param>
		/// <returns></returns>
		public static string GetDocHtmlFromXml(string xmlBody, Package package, bool isQuestionItem, bool isHtmlItem)
		{
            //FB669 The editor return <BR/> and <para/> as "\r\n". 
            // Need to convert this back into breaks, as editor would strip out "\r\n" foreever.
            if ((!isQuestionItem) && (!isHtmlItem))
                xmlBody = xmlBody.Replace(System.Environment.NewLine, "<BR>");

			if(xmlBody.IndexOf("<text>") == -1)
			{
                return xmlBody; 
			}

			// replace any <item> tags, with new html built from scratch.
			Regex itemReg = new Regex("<item.*</item>");
					
			Match itemMatch = itemReg.Match(xmlBody,0,xmlBody.IndexOf("</item>")+8); // we only want to get one.

			while(itemMatch.Success)
			{
				// make our html from the node
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(itemMatch.ToString());

				string defaultText = doc.SelectSingleNode("/item").InnerText;
				string strUId = doc.SelectSingleNode("/item/@uId").Value;
			    XmlNode fieldIdNode = doc.SelectSingleNode("/item/@fieldId");
			    string fieldId = null;
                if (fieldIdNode != null)
                {
                    fieldId = fieldIdNode.InnerText;
                }
                
                Guid uId;
                string htmlTags = "";

                if (strUId != string.Empty)
                {
                    uId = new Guid(strUId);
                }
                else
                {
                    uId = Guid.Empty;
                }

                if (!isQuestionItem)
				{
					htmlTags = StringBinding.MakeHTMLTags(package, uId, defaultText, fieldId);				
				}
				else
				{
					htmlTags = StringBinding.MakeSimpleHTMLTags(package,uId,defaultText);
				}

				//replace xml node with our new html.
				xmlBody = xmlBody.Remove(itemMatch.Index,itemMatch.Length);
				xmlBody = xmlBody.Insert(itemMatch.Index,htmlTags);

				itemMatch = itemReg.Match(xmlBody,0,xmlBody.IndexOf("</item>")+8);                
			}

            xmlBody = xmlBody.Replace("<text>", "");
            xmlBody = xmlBody.Replace("</text>", "");

			return xmlBody;
		}

		/// <summary>
		/// Checks if a ciruclar reference exists.
		/// </summary>
		/// <returns>true if there is a circular ref, false if there is not</returns>
		public static bool IsCircularRefExist(string xmlBody, PackageItem itemBeingEdited)
		{
			//TODO: call recursively on any item xml. Currently we only go one deep.

			if(xmlBody == null || xmlBody.IndexOf("<text>") == -1)
			{	
				return false;
			}

			Package package = itemBeingEdited.ParentPackage;			
			Regex itemReg = new Regex("<item.*</item>");					
			
			Match itemMatch = itemReg.Match(xmlBody,0,xmlBody.IndexOf("</item>")+8); // we only want to get one at a time.

			while(itemMatch.Success)
			{			
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(itemMatch.ToString());

				string strUId = doc.SelectSingleNode("/item/@uId").Value;
				Guid uId = new Guid(strUId);

				IStringBindingItem itemToCheck = package.StringBindingItemByGuid[uId];

				if(itemToCheck is Question)
				{
					// same question as one been edited should not be allowed.
					if(((PackageItem)itemToCheck).UniqueIdentifier == itemBeingEdited.UniqueIdentifier)
					{
						return true;
					}

					Question q = (Question) itemToCheck;
					if(q.DefaultAnswer != null)
					{
						string defaultAnswer = q.DefaultAnswer.EvaluateAndKeepXml();
						
						if(defaultAnswer != null)
						{
							if(defaultAnswer.IndexOf(itemBeingEdited.UniqueIdentifier.ToString()) >= 0)
							{
								return true;
							}
						}
					}
				}
				xmlBody = xmlBody.Remove(itemMatch.Index,itemMatch.Length);	
				itemMatch = itemReg.Match(xmlBody,0,xmlBody.IndexOf("</item>")+8);     
			}
     
			return false;
		}

		#endregion
	}
}
