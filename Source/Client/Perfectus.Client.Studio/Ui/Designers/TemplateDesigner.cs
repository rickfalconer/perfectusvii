
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;
using System.Windows.Forms;
using System.Xml;
//using AxDSOFramer;
//using DSOFramer;
using AxEDWordLib;
using EDWordLib;
using Microsoft.Office.Interop.Word;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using System.Collections.Generic;
//using Application = Microsoft.Office.Interop.Word.Application;
//using WordDocument = Microsoft.Office.Interop.Word.Document;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Summary description for TemplateDesigner.
	/// </summary>
	public class TemplateDesigner : DesignerBase
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
        /// 
        private const String _PID_varName = "PID";

		private Container components = null;
        private DialogResult dr = new DialogResult();
        //private AxFramerControl axFramerControl;
        //private List<string> workingFileNames = new List<string>( );
        //private Application app = null;
        //private Microsoft.Office.Interop.Word.Document doc = null;
        private AxEDWord axFramerControl;

        private List<string> workingFileNames = new List<string>();
        //private Application app = null;
        private Microsoft.Office.Interop.Word.Application app = null;
        private Microsoft.Office.Interop.Word.Document doc = null;
        private bool isBeingDeleted = false;
        
        private bool m_disposing = false;
        public bool InternalDisposal { get { return m_disposing; } }
        public event SelectedPackageLibraryItemHandler PackageItemSelected;

		protected override void ResetUI()
		{
			Cursor = Cursors.WaitCursor;

			try
			{
				WordTemplateDocument2 _wordTemplateDocument = ItemBeingEdited as WordTemplateDocument2;

				if (_wordTemplateDocument != null)
				{
					string templateFilename = _wordTemplateDocument.FilePath;
					object oMissing = Missing.Value;
                    //object oFileFormat = WdSaveFormat.wdFormatFlatXML;
                    object oFileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatFlatXML;

                    workingFileNames.Insert( 0, Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) ) + ".xml" );
                    object oWorkingFilename = workingFileNames[ 0 ];

                    // Make sure the user can't open other documents, create blank ones, or save this one somewhere funny.

                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileOpen, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileSaveAs, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileSave, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileNew, false);

                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableClose, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableNew, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableOfficeButton, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableOpen, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSave, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSaveAs, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSaveAsMenu, true);

                    axFramerControl.DisableSaveHotKey(true);

                    // don't do this - edraw decomes unstable
                    //axFramerControl.DisableStandardCommand(EDWordLib.CommandType.cmdTypeClose, true);
                    //axFramerControl.DisableStandardCommand(EDWordLib.CommandType.cmdTypeSave, true);

                    axFramerControl.Refresh();


                    if (_wordTemplateDocument.FileHasContent)
					{
						object oFalse = false;

						// Before opening the file, update its tags with the Tag Preview XML from the Package.
						// UpdatePerfectusTags(templateFilename);

                        //axFramerControl.Open( templateFilename, oFalse, "Word.Document", oMissing, oMissing );
                        axFramerControl.Open(templateFilename);
                    }
                    else
					{
						//axFramerControl.CreateNew("Word.Document");
                        axFramerControl.NewDoc();
						Save();
					}

                    // If that worked and we have an active document...
                    //if( axFramerControl.ActiveDocument != null )
                    if (axFramerControl.ActiveDocument() != null)
                    {
                        //doc = axFramerControl.ActiveDocument as WordDocument;
                        doc = (Microsoft.Office.Interop.Word.Document)axFramerControl.ActiveDocument();

                        if ( doc != null )
                        {
                            //app = doc.Application;

                            //CreateVarPID( );

                            // Turn on the tag view
                            //if (!doc.FormsDesign)
                            //{
                            //    doc.ToggleFormsDesign();
                            //}

                            // Put Word into Print View and Zoom to Fit.
                            //app.ActiveWindow.View.Type = WdViewType.wdPrintView;
                            //app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitBestFit;

                            //RDF 8 May 2013 Word 2013 does not have these props
                            //try
                            //{
                            //    doc.XMLSchemaReferences.HideValidationErrors = false;
                            //    doc.XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = true;
                            //    doc.XMLSchemaReferences.AutomaticValidation = false;
                            //}
                            //catch( COMException cex )
                            //{
                            //    switch( cex.ErrorCode )
                            //    {
                            //        case -2146823108: // Word cannot this document template.
                            //            MessageBox.Show( ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString( "Perfectus.Client.UI.Designers.TemplateDesigner.MissingWordTemplateWarning" ), Common.About.FormsTitle );
                            //            break;
                            //        default:
                            //            throw;
                            //    }
                            //}

                            // Create some inline XML to put the document into 'XML Mode'.  Otherwise a Schema is required.
                            //string tempXml = ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString( "Perfectus.Client.UI.Designers.TemplateDesigner.TemporaryTagXML" );
                            //doc.Content.InsertXML( tempXml, ref oMissing );
                            //object oOnce = 1;
                            //doc.Undo( ref oOnce );

                            // Save the document to the correct file/format so next time 'Save()' will be enough to do it for us.
                            //doc.SaveAs( ref oWorkingFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );
                            axFramerControl.SaveAs(workingFileNames[0], oFileFormat);

                            // Add event to word document whenever the user selects a content control. Will update the UI.
                            doc.ContentControlOnEnter += new DocumentEvents2_ContentControlOnEnterEventHandler(doc_ContentControlOnEnter);

                            //Add event so that we can fiddle the ids after control has been created.
                            //doc.ContentControlAfterAdd += new DocumentEvents2_ContentControlAfterAddEventHandler(doc_ContentControlAfterAdd);
                           
                        }
                    }
                    
                    axFramerControl.Visible = true;
				}
			}
			catch (COMException cex)
			{
				string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.WordNoResponseError");
				msg = msg.Replace("\\n", "\n");
				MessageBox.Show(string.Format(msg, cex.Message), Common.About.FormsTitle);
                this.Close();
			}

			catch (Exception ex)
			{
				string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.TemplateNoOpenError");
				msg = msg.Replace("\\n", "\n");
				MessageBox.Show(string.Format(msg, ex.Message), Common.About.FormsTitle);
				//MessageBox.Show(string.Format("The Template could not be opened for editing. ({0})", ex.Message), Common.About.FormsTitle);
                this.Close();
			}
			finally
			{
				Cursor = Cursors.Default;
			}
		}

        void doc_ContentControlOnEnter(ContentControl ContentControl)
        {
            // Read the variable with the PID
            int pid = -1;

            try
            {
                //WordDocument d = ContentControl.Application.ActiveDocument;
                //Object varName = _PID_varName;
                //Variable abc = d.Variables.get_Item(ref varName);
                //pid = Int32.Parse(abc.Value);
            }
            catch (Exception)
            { }

            // Do not accept events from other processes
            //if (System.Diagnostics.Process.GetCurrentProcess().Id != pid)
            //    return;

            try
            {
                // Raise the PackageItemSelected event
                if (ContentControl.Title != null)
                {
                    //SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs(ContentControl.Tag, new System.Guid(ContentControl.Title));
                    //word 2016 switch these TODO detect switch case
                    SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs(ContentControl.Title, new System.Guid(ContentControl.Tag));

                    if (PackageItemSelected != null)
                    {
                        PackageItemSelected(this, args, pid);
                    }
                }
            }
            catch (Exception ex)
            {
                string messge = ex.Message;
            }
        }

        //private void DeleteVarPID( )
        //{
        //    // Put Word into Print View and Zoom to Fit.
        //    if (doc != null)
        //    {
        //        Object varName = _PID_varName;
        //        try
        //        {
        //            doc.Variables.get_Item(ref varName).Delete();
        //        }
        //        catch (Exception) { }
        //    }
        //}

        //private void CreateVarPID()
        //{
        //    DeleteVarPID();
        //    Object pid = System.Diagnostics.Process.GetCurrentProcess().Id.ToString();
        //    doc.Variables.Add(_PID_varName, ref pid);
        //}


        /// <summary>
        ///     Handles the application objects XmlSelectionChange event, locates the new selected XmlNode's Uid attribute 
        ///     and raises the PackageItemSelected event.
        /// </summary>
        /// <param name="Sel">The selected text as it currently stands.</param>
        /// <param name="OldXMLNode">The old XmlNode that was selected</param>
        /// <param name="NewXMLNode">The new XmlNode that has been selected.</param>
        /// <param name="Reason">The reason for the event being fired.</param>
        void app_XMLSelectionChange(Selection Sel, XMLNode OldXMLNode, XMLNode NewXMLNode, ref int Reason)
        {
            string name = string.Empty;
            Guid uniqueIdentifier = Guid.Empty;
            string packageItemUid = null;
            int reasonMove = Convert.ToInt32( WdXMLSelectionChangeReason.wdXMLSelectionChangeReasonMove );

            // Read the variable with the PID
            int pid = -1;
            try
            {
                //WordDocument d = Sel.Application.ActiveDocument;
                Microsoft.Office.Interop.Word.Document d = Sel.Application.ActiveDocument;

                Object varName = _PID_varName;
                Variable abc = d.Variables.get_Item( ref varName );
                pid = Int32.Parse( abc.Value );
            }
            catch( Exception )
            { }

            // Do not accept events from other processes
            if( System.Diagnostics.Process.GetCurrentProcess( ).Id != pid )
                return;

            try
            {
                // Only handle it the new node is valid and the selected text chnaged (not inserted or deleted)
                if( NewXMLNode != null && Reason == reasonMove )
                {
                    // Get the Uid attribute from the selected XmlNode and exit as soon as it is found
                    foreach( Microsoft.Office.Interop.Word.XMLNode attribute in NewXMLNode.Attributes )
                    {
                        if( attribute.NodeValue != null )
                        {
                            packageItemUid = attribute.NodeValue;
                            name = NewXMLNode.BaseName;

                            if( Globals.IsGuid( packageItemUid, out uniqueIdentifier ) )
                            {
                                break;
                            }
                        }
                    }

                    // Raise the PackageItemSelected event
                    if( uniqueIdentifier != null )
                    {
                        SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs( name, uniqueIdentifier );

                        if( PackageItemSelected != null )
                        {
                            PackageItemSelected( this, args, pid );
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                string messge = ex.Message;
            }
        }


/*		private void ProcessTags (Document doc)
		{
			// Run through all the tags to update their name and contents.

			axFramerControl.Hide();

			Microsoft.Office.Interop.Word.XMLNodes nColl = doc.SelectNodes("*[@uid]", string.Empty, true);

			if (nColl != null)
			{
				foreach(Microsoft.Office.Interop.Word.XMLNode n in nColl)
				{
					string uid = n.Attributes[1].Text;  // 1 based arrays...

					Guid g = new Guid(uid);

					if (itemBeingEdited.ParentPackage.TemplateItemByGuid.ContainsKey(g))
					{
						ITemplateItem item = itemBeingEdited.ParentPackage.TemplateItemByGuid[g];
						Range r = n.Range;
				
						try
						{
							n.Delete();
							object oMissing = Missing.Value;
							r.InsertXML(WordTemplateDocument2.GenerateTag((PackageItem)item, false), ref oMissing);		
						}
						catch (Exception ex)
						{
							r.Text = string.Format("Error in ProcessTags: {0}", ex.Message);
						}
					}
				}
			}

			axFramerControl.Show();
		}*/


		public TemplateDesigner() : base(null)
		{
			//
			// Required for Windows Form Designer support
			//
			components = new Container();
			InitializeComponent();
		}

		public void SetTemplate(TemplateDocument t)
		{
			itemBeingEdited = t;
			if (itemBeingEdited != null)
			{
				itemBeingEdited.PropertyChanged -= new PropertyChangedEventHandler(itemBeingEdited_PropertyChanged);
			}
			t.ParentPackage.ItemDeleting += new EventHandler<PackageItemCancelEventArgs>(ParentPackage_ItemDeleting);
			itemBeingEdited.PropertyChanged += new PropertyChangedEventHandler(itemBeingEdited_PropertyChanged);
			ResetUI();
		}

        public void SetFocus()
        {
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
            m_disposing = disposing;

            // if (disposing=true), we can dispose of managed resources.
            if (disposing)
			{
				if (axFramerControl != null)
				{
                    //axFramerControl.Close();
                    axFramerControl.CloseDoc();
                    axFramerControl.Dispose();
					axFramerControl = null;
				}
				if (components != null)
				{
					components.Dispose();
                    components = null;
				}

				GC.Collect();
			}

            // Dispose of unmanaged resources.
            // Remove working files in temp folder.
            foreach( string wrkfile in workingFileNames )
            {
                try
                {
                    File.Delete( wrkfile );
                }
                catch
                {
                    // Ignore. Certain files locked by ? will persist FB1576
                }
            }
            workingFileNames.Clear( );
            
            base.Dispose(disposing); 
		}

		public override void UpdateUI()
		{
            if (null != axFramerControl && !m_disposing)
            {
                axFramerControl.Visible = true;
                axFramerControl.Enabled = true;
                //if (axFramerControl.ActiveDocument != null)
                //{
                //    ((WordDocument)(axFramerControl.ActiveDocument)).Activate();
                //}
                //axFramerControl.Activate();

                axFramerControl.BringToFront();

            }

		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplateDesigner));
            this.axFramerControl = new AxEDWordLib.AxEDWord();
            ((System.ComponentModel.ISupportInitialize)(this.axFramerControl)).BeginInit();
            this.SuspendLayout();
            // 
            // axFramerControl
            // 
            this.axFramerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axFramerControl.Enabled = true;
            this.axFramerControl.Location = new System.Drawing.Point(0, 0);
            this.axFramerControl.Name = "axFramerControl";
            this.axFramerControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axFramerControl.OcxState")));
            this.axFramerControl.Size = new System.Drawing.Size(344, 334);
            this.axFramerControl.TabIndex = 0;
            this.axFramerControl.TabStop = false;
            this.axFramerControl.BeforeDocumentOpened += new System.EventHandler(this.axFramerControl_BeforeDocumentOpened);
            // 
            // TemplateDesigner
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(344, 334);
            this.Controls.Add(this.axFramerControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TemplateDesigner";
            this.Text = "Template Designer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TemplateDesigner_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.axFramerControl)).EndInit();
            this.ResumeLayout(false);

		}

        #endregion

		public override bool Save()
		{
			bool success = false;
            //if (axFramerControl.ActiveDocument != null)
            if (axFramerControl.ActiveDocument() != null)
            {
				WordTemplateDocument2 _wordTemplateDocument = ItemBeingEdited as WordTemplateDocument2;
				if (_wordTemplateDocument != null)
				{
					string templateFilename = _wordTemplateDocument.FilePath;
					object oMissing = Missing.Value;
                    //object oFileFormat = WdSaveFormat.wdFormatFlatXML;
                    object oFileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatFlatXML;
                    //object oFileFormat = EDWordLib.WdSaveFormat.wdFormatXMLDocument;

                    object oWorkingFilename = workingFileNames[ 0 ];

					object oFilename = templateFilename;
					try
					{
                        //WordDocument doc = axFramerControl.ActiveDocument as WordDocument;
                        //Microsoft.Office.Interop.Word.Document doc = axFramerControl.ActiveDocument() as Microsoft.Office.Interop.Word.Document;

                        //XMLNode n = doc.SelectSingleNode("//w:docPr", "xmlns:w='http://schemas.microsoft.com/office/word/2003/wordml'", false);

                        //RDF 8 May 2013 Word 2013 does not have these props
                        //try
                        //{
                        //    doc.XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = true;
                        //    doc.XMLShowAdvancedErrors = true;
                        //}
                        //catch (COMException cex)
                        //{
                        //    switch (cex.ErrorCode)
                        //    {
                        //        case -2146823108: // Word cannot this document template.
                        //            // MessageBox.Show("This document references a Word Template that cannot be located.");
                        //            // Let 'missing Word Template exceptions' slide...
                        //            break;
                        //        default:
                        //            throw;
                        //    }
                        //}

                        // Save the template document to the temp file the axFramerControl opened originally. 
                        try
                        {
                            //doc.SaveAs( ref oFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );
                            axFramerControl.SaveAs(templateFilename, oFileFormat);
                        }
                        catch ( COMException ex )
                        {
                            switch( ex.ErrorCode )
                            {
                                // This occurs if the document is already opened and/or locked by another process. See FB1576.
                                // In this case we simply save the word document under a different name. The reasons for this error are unknown and 
                                // a probable cause is a bug in the axFramerControl. Its caused by multiple active DSOFramer objects and the inclusion of the 
                                // ApplicationEvents4_XMLSelectionChangeEventHandler event handler, which results on a lock on this file when 
                                // another application includes the event within its own document?? 
                                case WordTemplateDocument2.FILE_IN_USE_SAVE_ERROR:
                                    // Record file name for later deletion
                                    workingFileNames.Add( _wordTemplateDocument.FilePath );
                                    // Create new file....
                                    _wordTemplateDocument.GenerateNewFilePath( );
                                    oFilename = _wordTemplateDocument.FilePath;
                                    // Save under the new name. If this fails we pass the exception on to the UI.
                                    //doc.SaveAs( ref oFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );
                                    axFramerControl.SaveAs(_wordTemplateDocument.FilePath, oFileFormat);
                                    break;
                                default:
                                    throw;
                            }
                        }

                        // Save as our temporary one again to the template object can be serialised without locking issues with the active document
                        try
                        {
                            //doc.SaveAs( ref oWorkingFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing );
                            axFramerControl.SaveAs(workingFileNames[0], oFileFormat);
                        }
                        catch
                        {
                            // We don't want the application to fail if this fails. Not sure why..
                        }

                        _wordTemplateDocument.ForceDocumentPropertiesUpdate( );
						success = true;
					}
					catch (COMException ex )
					{
						string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.WordGeneralOpenError");
						msg = msg.Replace("\\n", "\n");
						MessageBox.Show(msg, Common.About.FormsTitle);
						
						success = false;
					}
				}
				else
				{
					success = true;
				}
			}

			if (success)
			{
				// Touch the item, so that the package is marked as changed.
				itemBeingEdited.Touch = Guid.NewGuid();
			}
			return success;
		}


		protected override bool OkayToClose()
		{
            //WordDocument doc;
            Microsoft.Office.Interop.Word.Document doc;

            if (isBeingDeleted)
			{
				return true;
			}

			try
			{
                //doc = axFramerControl.ActiveDocument as WordDocument;
                doc = (Microsoft.Office.Interop.Word.Document)axFramerControl.ActiveDocument();
                //if ((itemBeingEdited != null && axFramerControl.ActiveDocument != null && doc != null && !doc.Saved))
                if ((itemBeingEdited != null && axFramerControl.ActiveDocument() != null && axFramerControl.IsDirty()))
                {
                        DialogResult dr = MessageBox.Show(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.CommitChangesPrompt"), itemBeingEdited.Name), Common.About.Title, MessageBoxButtons.YesNoCancel);
					switch (dr)
					{
						case DialogResult.Yes:
                            {
                                //this.DeleteVarPID();
                                Save();
                                axFramerControl.CloseDoc();
                                return true;
                            }
						case DialogResult.No:
                            {
                                //this.DeleteVarPID();
                                axFramerControl.CloseDoc();
                                return true;
                            }
						case DialogResult.Cancel:
						default:
							return false;
					}
				}
				else
				{
					return true;
				}
			}
			catch
			{
				return true;
			}

		}


		private void ParentPackage_ItemDeleting(object sender, PackageItemCancelEventArgs e)
		{
			if (e.Item == ItemBeingEdited)
			{
				isBeingDeleted = true;
				Close();
				Dispose();
			}
		}

/*
		private void UpdatePerfectusTags(string templateFilename)
		{
			ResourceManager rm = new ResourceManager(typeof(TemplateDesigner));
			string xslt = rm.GetString("xslt");
			
			XslTransform transform = new XslTransform();
			XmlDocument xsltDoc = new XmlDocument();
			xsltDoc.LoadXml(xslt);
			
			transform.Load(xsltDoc.CreateNavigator());
			XsltArgumentList args = new XsltArgumentList();
			args.AddParam("templatePath", string.Empty, templateFilename);


			MemoryStream outputStream = new MemoryStream();
			transform.Transform(itemBeingEdited.ParentPackage.ToPreviewXml(), args, outputStream, new MyResolver());

			using (FileStream fs = new FileStream(templateFilename, FileMode.Open, FileAccess.Write))
			{
				fs.Write(outputStream.ToArray(), 0, (int)outputStream.Length);
			}
		}
*/

		public void WrapSelectionWithSimpleOutcome(SimpleOutcome so)
		{
			if (so == null)
			{
				throw new ArgumentNullException("so", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.NullSimpleOutcome"));
			}
			else
			{
				string tagName = so.GetTagName();
				WrapSelectionWithTag(tagName, so.UniqueIdentifier);
			}
		}


		private void WrapSelectionWithTag(string tagName, Guid uid)
		{
			if (app != null)
			{
				object oMissing = Missing.Value;
				Range r = app.Selection.Range;
				
				object oRange = r;

                Int32 start = r.Start;
                Int32 end = r.End;
				// Introduce the tag to our on-the-fly schema
				if (doc != null)
				{
					XMLNodes nodes = doc.SelectNodes(string.Format("//{0}", tagName), string.Empty, false);
					if (nodes == null || nodes.Count == 0)
					{
						bool doSelectHack = false;
						try
						{
							r.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
							doSelectHack = true;
						}
						catch
						{
							try
							{
								object oOne = 1;
								r = doc.Range(ref oOne, ref oOne);
								r.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
								doSelectHack = true;
							}						
							catch 
							{
								doc.Content.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
							}

						}
						object oOnce = 1;
						doc.Undo(ref oOnce);

						if (doSelectHack)
						{
                            r.Start = start;
                            r.End = end;
							r.Select();
						}
					}

					try
					{
                        Microsoft.Office.Interop.Word.ContentControl rtc = doc.ContentControls.Add(WdContentControlType.wdContentControlRichText, oRange);
                        // pf-3037/pf-3156 guid is displayed in word 2013 content controls, detect word version and switch values for alias, tag
                        decimal wordVersion = decimal.Parse(app.Version);
                        if (wordVersion < 15)
                        {
                            rtc.Tag = tagName;
                            rtc.Title = uid.ToString();
                        }
                        else
                        {
                            rtc.Tag = uid.ToString();
                            rtc.Title = tagName;
                        }
                    }
					catch(Exception ex)
					{
                        MessageBox.Show(ex.ToString());
					}
				}
			}
		}

		public void WrapSelectionWithOutcome(Outcome o)
		{
			if (o == null)
			{
				throw new ArgumentNullException("o", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.NullOutcome"));
			}
			else
			{
				if (doc != null && app != null && app.Selection != null)
				{
					string tagName = o.GetTagName();

                    string currentXml = app.Selection.WordOpenXML;

					// Older packages may have not have a definition defined in construction
					if (o.Definition == null)
					{
						o.Definition = o.ParentPackage.CreateQuery();
					}

					if (o.Definition.ActionIfTrue == null)
					{
						o.Definition.ActionIfTrue = new OutcomeAction();
					}

					OutcomeAction actionIfTrue = ((OutcomeAction) (o.Definition.ActionIfTrue));


					if (actionIfTrue.OfficeOpenXML == null && actionIfTrue.SubQuery == null)
					{
                        actionIfTrue.OfficeOpenXML = currentXml;
						actionIfTrue.SubQuery = null;
					}
					else
					{
						string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.OverwriteOutcomeActionPrompt"), o.Name);
						msg = msg.Replace("\\n", "\n");
						msg = msg.Replace("\\t", "\t");
                        this.dr = MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (dr == DialogResult.Yes)
						{
                            actionIfTrue.OfficeOpenXML = currentXml;
							actionIfTrue.SubQuery = null;
						}
						else
						{
							return;
						}
					}

					FindForm().Focus();
					axFramerControl.Focus();

					WrapSelectionWithTag(tagName, o.UniqueIdentifier);
				}
			}
		}

        private void TemplateDesigner_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (app != null)
            {
                doc.ContentControlOnEnter -= new DocumentEvents2_ContentControlOnEnterEventHandler(doc_ContentControlOnEnter);
                //doc.ContentControlAfterAdd -= new DocumentEvents2_ContentControlAfterAddEventHandler(doc_ContentControlAfterAdd);
            }
        }

        private void axFramerControl_BeforeDocumentOpened(object sender, EventArgs e)
        {
            axFramerControl.ShowRibbonTitlebar(false);

            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableClose, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableNew, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableOfficeButton, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableOpen, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableSave, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableSaveAs, true);
            //axFramerControl.DisableFileCommand(WdUIType.wdUIDisableSaveAsMenu, true);

            //axFramerControl.DisableSaveHotKey(true);

            // don't do this - edraw decomes unstable
            //axFramerControl.DisableStandardCommand(CommandType.cmdTypeClose, true);
            //axFramerControl.DisableStandardCommand(CommandType.cmdTypeSave, true);
        }
    }

    public class MyResolver : XmlUrlResolver
	{
		public MyResolver() : base() { }

		public override Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			if (baseUri == null & relativeUri == string.Empty)
			{
				return new Uri(HttpContext.Current.Server.MapPath("."));
			}
			else
			{
				return base.ResolveUri(baseUri, relativeUri);
			}
		}
	}

    public delegate void SelectedPackageLibraryItemHandler(object o, SelectedPackageItemEventArgs e, int id);

    public class SelectedPackageItemEventArgs : EventArgs
    {
        private string name;
        private Guid uniqueIdentifier;

        public string Name
        {
            get { return name; }
        }

        public Guid UniqueIdentifier
        {
            get { return uniqueIdentifier; }
        }

        public SelectedPackageItemEventArgs(string name, Guid guid)
        {
            this.name = name;
            this.uniqueIdentifier = guid;
        }
    }
}
