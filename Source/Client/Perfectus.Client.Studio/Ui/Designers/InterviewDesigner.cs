using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using Perfectus.Client.Studio.UI.Designers.Diagrams;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Syncfusion.Windows.Forms.Diagram;
using Syncfusion.Windows.Forms.Diagram.Controls;
using Tool=Syncfusion.Windows.Forms.Diagram.Tool;
//TODO: Dispose controls correctly

namespace Perfectus.Client.Studio.UI.Designers
{
    /// <summary>
    /// Summary description for InterviewDesigner.
    /// </summary>
    public class InterviewDesigner : DesignerBase
    {
        private IContainer components;

        private bool suspended = false;
        private Diagram diagram1;
        
        private PageSymbolDictionary pageSymbolMapping = new PageSymbolDictionary( );
        private PageSymbolDictionary pageSymbolCache = new PageSymbolDictionary( );
        private DestinationSymbol destinationSymbol = null;
        private LayoutContext layoutContext = null;

        private ImageList ilstSymbols;
        private PrintPreviewDialog printPreviewDialog1;
        private PrintDialog printDialog1;
        private ToolStrip toolStrip1;
        private ToolStripButton btPrintSettings;
        private ToolStripButton btPreviewPrint;
        private ToolStripButton btPrint;
        private ToolStripButton btPan;
        private ToolStripButton btSelect;
        private ToolStripButton btReset;
        private ToolStripComboBox ctZoom;
        private PageSetupDialog pageSetupDialog1;
        public event EventHandler<PackageItemEventArgs> OpenDesigner;

        protected override bool OkayToClose()
        {
            return true;
        }

        public override bool HasChanges
        {
            get { return true; }
            set { base.HasChanges = value; }
        }

        private void LoadDiagram( )
        {
            Interview interview = (Interview)itemBeingEdited;
            if( interview.Diagram != null )
            {
                MemoryStream str = interview.Diagram;
                str.Seek( 0, SeekOrigin.Begin );
                diagram1.LoadSoap( str );
            }

            ResetUI( false );
        }

        private void SaveDiagram( )
        {
            Interview interview = (Interview)itemBeingEdited;
            MemoryStream str = new MemoryStream( );
            diagram1.SaveSoap( str );
            str.Seek( 0, SeekOrigin.Begin );
            interview.Diagram = str;
        }

        private void ClearSymbols( )
        {
            if( pageSymbolMapping != null )
            {
                foreach( PageSymbol ps in pageSymbolMapping.Values )
                {
                    InterviewPage p = ps.Page;
                    if( p != null )
                        p.PropertyChanged -= new PropertyChangedEventHandler( diagramPage_PropertyChanged );

                    // Remove any dependency on position and relation to other symbols.
                    ps.ClearLinksAndPosition( );

                    // Save the symbol into the temp cache. This is to prevent the recreation of the symbols which are not releasing the 
                    // unmanaged resources properly within Syncfusion.
                    if( !pageSymbolCache.Contains( p.UniqueIdentifier ) )
                        pageSymbolCache.Add( p.UniqueIdentifier, ps );
                }
                pageSymbolMapping.Clear( );
            }

            // Remove all objects from the model.
            if( diagram1.Model != null )
                diagram1.Model.RemoveAllChildren( );
        }

        protected override void ResetUI( bool refresh )
        {
            System.Diagnostics.Debug.Assert( diagram1 != null, "Missing Syncfusion Diagram class object" );
            System.Diagnostics.Debug.Assert( ItemBeingEdited != null, "There is no item being edited (ItemBeingEdited)" );

            // Without a diagram to draw on, there is not much we can do..
            if( diagram1 == null )
                return;

            // We must be editing an interview in this class...
            if( !( ItemBeingEdited is Interview ) )
                return;

            // This will be the interview we are editing.
            Interview interview = ItemBeingEdited as Interview;
            // Symbol that represents the starting page...
            PageSymbol rootPageSymbol = null;

            suspended = true;
            SuspendLayout( );

            // Reset UI elements such as tab label and toolbar.
            base.ResetUI( refresh );

            // Create a list of current positions so that we can restore them after the layout algorithm has been applied.
            Dictionary<Guid, SymbolLocation> symbolPositions = new Dictionary<Guid, SymbolLocation>( );
            if( !refresh )
            {
                foreach( INode node in diagram1.Model.Nodes )
                {
                    if( node is PageSymbol )
                        // Note that if this diagram had just been de-serialised, the page symbols would not have any corresponding interviewPages.
                        // hence we must use the interviewId variable as it is the only local property that is serialised.
                        symbolPositions.Add( ( (PageSymbol)node ).InterviewId, new SymbolLocation( ( (PageSymbol)node ).X, ( (PageSymbol)node ).Y ) );

                    else if( node is DestinationSymbol )
                        // Load the destination symbol with an empty guid value.
                        symbolPositions.Add( Guid.Empty, new SymbolLocation( ( (DestinationSymbol)node ).X, ( (DestinationSymbol)node ).Y ) );
                }
            }

            // Dispose of all the current symbols 
            ClearSymbols( );

            // Construct and configure the layout manager....
            if (diagram1.LayoutManager == null)
                diagram1.LayoutManager = new InterviewManager2(diagram1.Model);

            diagram1.LayoutManager.AutoLayout = false;
            diagram1.Model.LineStyle.LineJoin = LineJoin.Round;
            diagram1.Model.BoundaryConstraintsEnabled = false;

            // Construct the destination symbol...
            if( destinationSymbol == null )
            {
                destinationSymbol = new DestinationSymbol( );
                destinationSymbol.Value = interview;
                destinationSymbol.ResetUI( );
            }
            diagram1.Model.AppendChild( destinationSymbol );

            // Build all the pages starting from the StartPage and navigating down to the last page.
            if( interview.StartPage != null )
                rootPageSymbol = DrawPage( interview.StartPage, null, destinationSymbol, 1 );
            else
            {
                MessageBox.Show(
                    ResourceLoader.GetResourceManager( "Perfectus.Client.Studio.Localisation" ).GetString(
                        "Perfectus.Client.UI.Designers.InterviewDesigner.NoStartPageError" ),
                    Common.About.FormsTitle );
            }

            // If there are pages missing, these would not have been connected to the start page, and hence we draw them ( and their connected pages) in 'lost mode' // DRB!!!
            foreach( InterviewPage p in interview.Pages )
            {
                if( !pageSymbolMapping.ContainsKey( p.UniqueIdentifier ) )
                    DrawPage( p, null, destinationSymbol, -1 );
            }

            // Construct the layout context object....
            if( layoutContext == null )
                layoutContext = new LayoutContext( symbolPositions, rootPageSymbol, Graphics.FromHwnd( diagram1.Handle ) );
            else
            {
                layoutContext.RootNode = rootPageSymbol;
                layoutContext.SymbolPositions = symbolPositions;
            }

             // Automatically layout the symbols.
             diagram1.LayoutManager.UpdateLayout( layoutContext );

            // Clear our temporary cache
            pageSymbolCache.Clear( );
            // Finally clear any current selections.
            diagram1.View.SelectionList.Clear( );
            
            diagram1.Refresh( );
            ResumeLayout( );
            suspended = false;
        }

        protected PageSymbol DrawPage( InterviewPage p, PageSymbol parentSymbol, DestinationSymbol destSymbol, int rank )
        {
            PageSymbol pageSymbol = null;
            bool alreadyOnDiagram = false;
            bool mustIgnoreHistory = false;
            
            // Use our dictionary as a shortcut to determine whether this page is on the diagram yet.  If not, set it up.
            if( !pageSymbolMapping.ContainsKey( p.UniqueIdentifier ) )
            {
                // See if we have a cache of the symbol....
                if( pageSymbolCache.Contains( p.UniqueIdentifier ) )
                {
                    pageSymbol = pageSymbolCache[ p.UniqueIdentifier ];

                    // If symbol was not ranked, and now is, we want to recalculate its position
                    //if( pageSymbol.Rank != rank && ( pageSymbol.Rank == -1 ) )
                    //if( pageSymbol.Rank != rank && ( pageSymbol.Rank == -1 || rank == -1 ) )
                        //if( pageSymbol.Rank != rank && pageSymbol.Rank == -1 && pageSymbol.X == (float)PageSymbol.OrphanX )
                            //mustIgnoreHistory = true;

                    // If next page changed from a non-query to a query and vice versa, dispose of the symbol and create a new one.
                    if( ( p.NextPage.ValueType != PageQueryValue.PageQueryValueType.Query && pageSymbol.HasQueryBranch ) ||
                          ( p.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query && !pageSymbol.HasQueryBranch ) )
                    {
                        pageSymbol.Dispose( );
                        pageSymbol = null;
                    }

                    // If rank has changed, redraw the symbol....
                    if( pageSymbol != null && pageSymbol.Rank != rank )
                        // Reassign new images etc....
                        pageSymbol.DrawSymbol( rank );
                }

                // Must we still add one?
                if( pageSymbol == null )
                {
                    pageSymbol = new PageSymbol( );
                    pageSymbol.Page = p;
                    pageSymbol.CenterPort.AttachAtPerimeter = true;

                    // Draw the symbol now..
                    pageSymbol.ResetUI( rank );
                }

                // Set flag on history
                pageSymbol.IgnoreHistory = mustIgnoreHistory;

                // Add to the Syncfusion diagram
                diagram1.Model.AppendChild( pageSymbol );

                // Add to our own internal list.
                pageSymbolMapping.Add( p.UniqueIdentifier, pageSymbol );

                // If we're not on the diagram yet, start watching the page's propertyChanged event
                p.PropertyChanged += new PropertyChangedEventHandler( diagramPage_PropertyChanged );

                // If we're a 'lost' page, don't draw its links, just add it to the diagram for display along an edge.  
                if( rank < 0 )
                    return pageSymbol;
            }
            else
            {
                alreadyOnDiagram = true;
                pageSymbol = pageSymbolMapping[ p.UniqueIdentifier ];
            }

            // Link this page to its previous one.
            if( parentSymbol != null )
            {
                // Add a reference counter to the parent's ChildrenCount
                Link l = new Link( Link.Shapes.Line );

                l.LineStyle.LineWidth = 1;
                l.LineStyle.LineJoin = LineJoin.Round;
                l.LineStyle.LineColor = Color.Gray;
                l.EndPoints.LastEndPointDecorator = new EndPointDecorator( EndPointVisuals.ClosedArrow );
                pageSymbol.Connect( pageSymbol.Ports[ 1 ], l.HeadPort );
                parentSymbol.Connect( parentSymbol.Ports[ 0 ], l.TailPort );
                diagram1.Model.AppendChild( l );

                // If we're already on the diagram, all we need to do is draw a line to its new 'parent' and stop.
                if( alreadyOnDiagram )
                    return pageSymbol;
            }

            // If it only has one next page, draw that page.
            switch( p.NextPage.ValueType )
            {
                case PageQueryValue.PageQueryValueType.Page:
                    {
                        InterviewPage page = p.NextPage.PageValue;
                        if( page != null )
                            DrawPage( page, pageSymbol, destSymbol, ++rank );
                    }
                    break;

                case PageQueryValue.PageQueryValueType.Query:
                    {
                        foreach( ActionBase a in p.NextPage.QueryValue.AllPossibleActions )
                        {
                            NavigationAction na = a as NavigationAction;
                            if( na != null )
                            {
                                if( na.Page != null )
                                    DrawPage( na.Page, pageSymbol, destSymbol, rank + 1 );
                                else
                                    AttachToDestinationSymbol( destSymbol, pageSymbol );
                            }
                        }
                    }
                    break;

                case PageQueryValue.PageQueryValueType.NoValue:
                    AttachToDestinationSymbol( destSymbol, pageSymbol );
                    break;
            }
            
            return pageSymbol;
        }

        private void AttachToDestinationSymbol(DestinationSymbol destSymbol, PageSymbol s)
        {
            Link l2 = new Link( Link.Shapes.Line );
            l2.LineStyle.LineWidth = 1;
            l2.LineStyle.LineJoin = LineJoin.Round;
            l2.LineStyle.LineColor = Color.Gray;
            l2.EndPoints.LastEndPointDecorator = new EndPointDecorator( EndPointVisuals.ClosedArrow );

            destSymbol.CenterPort.AttachAtPerimeter = true;
            destSymbol.Connect( destSymbol.CenterPort, l2.HeadPort );
            s.Connect( s.Ports[ 0 ], l2.TailPort );
            diagram1.Model.AppendChild( l2 );
        }

        private void SetTools()
        {
            // Get the collection of registered Tools, and disable those that are not required 
            Tool[] tools = diagram1.Controller.GetAllTools();
            foreach (Tool tool in tools)
            {
                // Retain the Enabled state for the Zoom and Pan tools 
                if ((tool.Name == "ZoomTool") || (tool.Name == "PanTool") || (tool.Name == "SelectTool") ||
                    (tool.Name == "MoveTool"))
                    continue;
                tool.Enabled = false;
            }
            diagram1.AllowDrop = false;
            diagram1.Enabled = true;
            //cmSelect.Execute();
        }

        public InterviewDesigner(PackageItem item) : base(item)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            // Load components specifically for debug purposes.
            //AddDebugComponents();

            ctZoom.SelectedIndex = 2;
            MainDesigner2.PreSave += new MainDesigner2.PreSaveHandler(MainDesigner_PreSave);
            item.ParentPackage.ItemCreated += new EventHandler<PackageItemEventArgs>(ParentPackage_ItemCreated);
            item.ParentPackage.ItemDeleting += new EventHandler<PackageItemCancelEventArgs>(ParentPackage_ItemDeleting);
            item.ParentPackage.ItemDeleted += new EventHandler<PackageItemDeletedEventArgs>(ParentPackage_ItemDeleted);
            LoadDiagram();
            SetTools();
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            // We clear all symbols and resulting links to event handlers.....
            ClearSymbols( );

            if (disposing)
            {
                // Clear up managed resources.
                ItemBeingEdited.ParentPackage.ItemCreated -= new EventHandler<PackageItemEventArgs>(ParentPackage_ItemCreated);

                // Dispose of the diagram
                if( diagram1 != null ) { diagram1.Dispose( ); diagram1 = null; }
                // Dispose of the layout context
                if( layoutContext != null ) { layoutContext.Dispose( ); layoutContext = null; }
                if( components != null ) { components.Dispose( ); components = null; }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InterviewDesigner));
            this.diagram1 = new Syncfusion.Windows.Forms.Diagram.Controls.Diagram();
            this.ilstSymbols = new System.Windows.Forms.ImageList(this.components);
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btPrintSettings = new System.Windows.Forms.ToolStripButton();
            this.btPreviewPrint = new System.Windows.Forms.ToolStripButton();
            this.btPrint = new System.Windows.Forms.ToolStripButton();
            this.btPan = new System.Windows.Forms.ToolStripButton();
            this.btSelect = new System.Windows.Forms.ToolStripButton();
            this.btReset = new System.Windows.Forms.ToolStripButton();
            this.ctZoom = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // diagram1
            // 
            this.diagram1.AllowDrop = true;
            this.diagram1.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.diagram1.Controller.MaxHistory = 256;
            resources.ApplyResources(this.diagram1, "diagram1");
            this.diagram1.HScroll = true;
            this.diagram1.LayoutManager = null;
            // 
            // 
            // 
            this.diagram1.Model.BackgroundStyle.Color = System.Drawing.Color.White;
            this.diagram1.Model.BackgroundStyle.GradientAngle = 90F;
            this.diagram1.Model.BackgroundStyle.GradientBounds = ((System.Drawing.RectangleF)(resources.GetObject("diagram1.Model.BackgroundStyle.GradientBounds")));
            this.diagram1.Model.BackgroundStyle.GradientStartColor = System.Drawing.Color.White;
            this.diagram1.Model.BackgroundStyle.Properties = this.diagram1.Model;
            this.diagram1.Model.BackgroundStyle.Texture = null;
            this.diagram1.Model.BackgroundStyle.TextureWrapMode = System.Drawing.Drawing2D.WrapMode.Tile;
            this.diagram1.Model.BackgroundStyle.Type = Syncfusion.Windows.Forms.Diagram.BackgroundStyle.BackgroundType.Solid;
            this.diagram1.Model.BoundaryConstraintsEnabled = false;
            this.diagram1.Model.FillStyle.AlphaFactor = 255;
            this.diagram1.Model.FillStyle.Color = System.Drawing.Color.LightGray;
            this.diagram1.Model.FillStyle.GradientAngle = 90F;
            this.diagram1.Model.FillStyle.GradientBounds = ((System.Drawing.RectangleF)(resources.GetObject("diagram1.Model.FillStyle.GradientBounds")));
            this.diagram1.Model.FillStyle.GradientStartColor = System.Drawing.Color.LightGray;
            this.diagram1.Model.FillStyle.Properties = this.diagram1.Model;
            this.diagram1.Model.FillStyle.Texture = null;
            this.diagram1.Model.FillStyle.TextureWrapMode = System.Drawing.Drawing2D.WrapMode.Tile;
            this.diagram1.Model.FillStyle.Type = Syncfusion.Windows.Forms.Diagram.FillStyle.FillType.Solid;
            this.diagram1.Model.Height = 5200F;
            this.diagram1.Model.LineStyle.DashCap = System.Drawing.Drawing2D.DashCap.Flat;
            this.diagram1.Model.LineStyle.DashOffset = 0F;
            this.diagram1.Model.LineStyle.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.diagram1.Model.LineStyle.EndCap = System.Drawing.Drawing2D.LineCap.Flat;
            this.diagram1.Model.LineStyle.LineColor = System.Drawing.Color.Black;
            this.diagram1.Model.LineStyle.LineJoin = System.Drawing.Drawing2D.LineJoin.Bevel;
            this.diagram1.Model.LineStyle.LineWidth = 2F;
            this.diagram1.Model.LineStyle.MiterLimit = 10F;
            this.diagram1.Model.LineStyle.Properties = this.diagram1.Model;
            this.diagram1.Model.MeasurementScale = 1F;
            this.diagram1.Model.MeasurementUnits = System.Drawing.GraphicsUnit.Pixel;
            this.diagram1.Model.Name = "Model";
            this.diagram1.Model.RenderingStyle.Properties = this.diagram1.Model;
            this.diagram1.Model.RenderingStyle.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            this.diagram1.Model.ShadowStyle.AlphaFactor = 128;
            this.diagram1.Model.ShadowStyle.Color = System.Drawing.Color.LightGray;
            this.diagram1.Model.ShadowStyle.OffsetX = 6F;
            this.diagram1.Model.ShadowStyle.OffsetY = 6F;
            this.diagram1.Model.ShadowStyle.Properties = this.diagram1.Model;
            this.diagram1.Model.ShadowStyle.Visible = false;
            this.diagram1.Model.Width = 1024F;
            this.diagram1.Name = "diagram1";
            this.diagram1.NudgeIncrement = 1F;
            this.diagram1.ScrollGranularity = 0.5F;
            this.diagram1.SmartSizeBox = false;
            // 
            // 
            // 
            this.diagram1.View.BackgroundColor = System.Drawing.Color.White;
            this.diagram1.View.Grid.Color = System.Drawing.Color.Black;
            this.diagram1.View.Grid.DashOffset = 4F;
            this.diagram1.View.Grid.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            this.diagram1.View.Grid.GridStyle = Syncfusion.Windows.Forms.Diagram.GridStyle.Point;
            this.diagram1.View.Grid.HorizontalSpacing = 10F;
            this.diagram1.View.Grid.SnapToGrid = true;
            this.diagram1.View.Grid.VerticalSpacing = 10F;
            this.diagram1.View.Grid.Visible = false;
            this.diagram1.View.HandleAnchorColor = System.Drawing.Color.LightGray;
            this.diagram1.View.HandleColor = System.Drawing.Color.White;
            this.diagram1.View.HandleDisabledColor = System.Drawing.Color.Black;
            this.diagram1.View.HandleSize = 6;
            this.diagram1.View.HorizontalRuler.Visible = false;
            this.diagram1.View.MouseTrackingEnabled = true;
            this.diagram1.View.PasteOffsetX = 10;
            this.diagram1.View.PasteOffsetY = 10;
            this.diagram1.View.RulerFillColor = System.Drawing.Color.LightGray;
            this.diagram1.View.RulerLineColor = System.Drawing.Color.Black;
            this.diagram1.View.RulerSize = 16;
            this.diagram1.View.RulerUnits = Syncfusion.Windows.Forms.Diagram.MeasurementsUnit.Inches;
            this.diagram1.View.ShowPageBounds = false;
            this.diagram1.View.VerticalRuler.Visible = false;
            this.diagram1.VScroll = true;
            this.diagram1.SelectionChanged += new Syncfusion.Windows.Forms.Diagram.NodeCollection.EventHandler(this.diagram1_SelectionChanged);
            this.diagram1.NodeDoubleClick += new Syncfusion.Windows.Forms.Diagram.NodeMouseEventHandler(this.diagram1_NodeDoubleClick);
            // 
            // ilstSymbols
            // 
            this.ilstSymbols.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilstSymbols.ImageStream")));
            this.ilstSymbols.TransparentColor = System.Drawing.Color.Transparent;
            this.ilstSymbols.Images.SetKeyName(0, "");
            this.ilstSymbols.Images.SetKeyName(1, "");
            this.ilstSymbols.Images.SetKeyName(2, "");
            this.ilstSymbols.Images.SetKeyName(3, "");
            // 
            // printPreviewDialog1
            // 
            resources.ApplyResources(this.printPreviewDialog1, "printPreviewDialog1");
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            // 
            // toolStrip1
            // 
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btPrintSettings,
            this.btPreviewPrint,
            this.btPrint,
            this.btPan,
            this.btSelect,
            this.btReset,
            this.ctZoom});
            this.toolStrip1.Name = "toolStrip1";
            // 
            // btPrintSettings
            // 
            this.btPrintSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btPrintSettings, "btPrintSettings");
            this.btPrintSettings.Name = "btPrintSettings";
            this.btPrintSettings.Click += new System.EventHandler(this.btPrintSettings_Click);
            // 
            // btPreviewPrint
            // 
            this.btPreviewPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btPreviewPrint, "btPreviewPrint");
            this.btPreviewPrint.Name = "btPreviewPrint";
            this.btPreviewPrint.Click += new System.EventHandler(this.btPreviewPrint_Click);
            // 
            // btPrint
            // 
            this.btPrint.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btPrint, "btPrint");
            this.btPrint.Name = "btPrint";
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btPan
            // 
            this.btPan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btPan, "btPan");
            this.btPan.Name = "btPan";
            this.btPan.Click += new System.EventHandler(this.btPan_Click);
            // 
            // btSelect
            // 
            this.btSelect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btSelect, "btSelect");
            this.btSelect.Name = "btSelect";
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // btReset
            // 
            this.btReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btReset, "btReset");
            this.btReset.Name = "btReset";
            this.btReset.Click += new System.EventHandler(this.btReset_Click);
            // 
            // ctZoom
            // 
            this.ctZoom.Items.AddRange(new object[] {
            resources.GetString("ctZoom.Items"),
            resources.GetString("ctZoom.Items1"),
            resources.GetString("ctZoom.Items2"),
            resources.GetString("ctZoom.Items3"),
            resources.GetString("ctZoom.Items4"),
            resources.GetString("ctZoom.Items5"),
            resources.GetString("ctZoom.Items6")});
            this.ctZoom.Name = "ctZoom";
            resources.ApplyResources(this.ctZoom, "ctZoom");
            this.ctZoom.SelectedIndexChanged += new System.EventHandler(this.ctZoom_SelectedIndexChanged);
            // 
            // InterviewDesigner
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.diagram1);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.Name = "InterviewDesigner";
            this.Closed += new System.EventHandler(this.InterviewDesigner_Closed);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion


        private void diagram1_SelectionChanged(object sender, NodeCollection.EventArgs evtArgs)
        {
            if (btSelect.Checked)
            {
                diagram1.Controller.GetTool("MoveTool").Enabled = true;
            }
            else
            {
                diagram1.Controller.GetTool("MoveTool").Enabled = false;
            }

            if (!suspended)
            {
                if (evtArgs.Node == null || evtArgs.Nodes.Length != 1)
                {
                    OnItemSelected(null);
                }
                else
                {
                    if (evtArgs.Node is PageSymbol)
                    {
                        diagram1.Controller.GetTool("MoveTool").Enabled = true;
                        OnItemSelected(((PageSymbol)evtArgs.Node).Page);
                    }
                    else if (evtArgs.Node is DestinationSymbol)
                    {
                        diagram1.Controller.GetTool("MoveTool").Enabled = true;
                    }
                    //else if(evtArgs.Node is Link)
                    //{	
                    //diagram1.Controller.GetTool("MoveTool").Enabled = false;	
                    //}
                    else
                    {
                        diagram1.Controller.SelectionList.Clear();
                        diagram1.Controller.View.Update(true);
                        diagram1.Controller.GetTool("MoveTool").Enabled = false;
                    }
                }
            }
        }

        private void diagram1_NodeDoubleClick(object sender, NodeMouseEventArgs evtArgs)
        {
            if (evtArgs.Node is PageSymbol)
            {
                PageSymbol ps = (PageSymbol)evtArgs.Node;


                PackageItem item = ps.Page;

                if (OpenDesigner != null)
                {
                    OpenDesigner(this, new PackageItemEventArgs(item));
                }
            }
            else
            {
                evtArgs.ContinueOperation = false;
            }
        }

        private void diagramPage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "NextPage")
            {
                ResetUI(false);
            }
            else if (e.PropertyName == "Name")
            {
                PackageItem item = sender as PackageItem;
                if (item != null)
                {
                    PageSymbol sym = pageSymbolMapping[item.UniqueIdentifier];
                    if (sym != null)
                    {
                        sym.DrawLabel();
                        diagram1.Refresh();
                        diagram1.View.SelectionList.Clear();
                    }
                }
            }
        }

        private void ParentPackage_ItemCreated(object sender, PackageItemEventArgs e)
        {
            if (e.Item is InterviewPage)
            {
                ResetUI( false );
            }
        }

        private void ParentPackage_ItemDeleting(object sender, PackageItemCancelEventArgs e)
        {
            if (e.Item == ItemBeingEdited)
            {
                Close();
                Dispose();
            }
        }

        private void ParentPackage_ItemDeleted(object sender, PackageItemDeletedEventArgs e)
        {
            if (pageSymbolMapping.ContainsKey(e.Id))
            {
                try
                {
                    // First simply remove the page from the diagram model
                    diagram1.Model.RemoveChild( diagram1.Model.GetChildIndex( pageSymbolMapping[ e.Id ] ) );
                    // Force the diagram to refresh the interface
                    diagram1.Refresh();
                    // Remove the page symbol from our internal list....
                    pageSymbolMapping.Remove(e.Id);
                }
                catch
                {
                    // If unable to remove the individual item, reset the whole UI as before.
                    ResetUI( false );
                }
            }
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            PrintDocument printDoc = diagram1.CreatePrintDocument();
            printDialog1.Document = printDoc;
            printDialog1.AllowSomePages = true;
            if (diagram1.Model.PageSettings != null)
            {
                printDialog1.Document.DefaultPageSettings = diagram1.Model.PageSettings;
                if (diagram1.Model.PageSettings.PrinterSettings != null)
                {
                    printDialog1.Document.PrinterSettings = diagram1.Model.PageSettings.PrinterSettings;
                }
            }

            printDoc.DocumentName = itemBeingEdited.Name;

            if (printDialog1.ShowDialog() == DialogResult.OK)
            {
                printDoc.Print();
            }
        }

        private void btPreviewPrint_Click(object sender, EventArgs e)
        {
            PrintDocument printDoc = diagram1.CreatePrintDocument();
            printDoc.DocumentName = itemBeingEdited.Name;
            printPreviewDialog1.Document = printDoc;

            if (diagram1.Model.PageSettings != null)
            {
                printPreviewDialog1.Document.DefaultPageSettings = diagram1.Model.PageSettings;
                if (diagram1.Model.PageSettings.PrinterSettings != null)
                {
                    printPreviewDialog1.Document.PrinterSettings = diagram1.Model.PageSettings.PrinterSettings;
                }
            }
            printPreviewDialog1.AutoScale = true;
            printPreviewDialog1.Icon = FindForm().Icon;

            try
            {
                printPreviewDialog1.ShowDialog();
            }
            catch (Win32Exception)
            {
                // Cancelling some printer dialogs (e.g. Print to file) can cause a mighty useful 'user clicked cancel' exception.	
            }
        }

        private void btPrintSettings_Click(object sender, EventArgs e)
        {
            pageSetupDialog1.PageSettings = diagram1.Model.PageSettings;
            pageSetupDialog1.PrinterSettings = diagram1.Model.PageSettings.PrinterSettings;
            pageSetupDialog1.AllowOrientation = true;
            pageSetupDialog1.AllowMargins = true;
            pageSetupDialog1.AllowPrinter = true;
            if (pageSetupDialog1.ShowDialog() == DialogResult.OK)
            {
                diagram1.Model.PageSettings = pageSetupDialog1.PageSettings;
                diagram1.Model.PageSettings.PrinterSettings = pageSetupDialog1.PrinterSettings;
            }
        }

        private void btSelect_Click(object sender, EventArgs e)
        {
            Tool tool = diagram1.Controller.GetTool("SelectTool");

            if (btSelect.Checked)
            {
                diagram1.Enabled = false;
                diagram1.Controller.DeactivateTool(tool);
                Tool movetool = diagram1.Controller.GetTool("MoveTool");
                diagram1.Controller.DeactivateTool(movetool);
                btSelect.Checked = false;
            }
            else
            {
                diagram1.Enabled = true;
                diagram1.Controller.ActivateTool(tool);
                btSelect.Checked = true;
                btPan.Checked = false;
            }
        }

        private void btPan_Click(object sender, EventArgs e)
        {
            Tool tool = diagram1.Controller.GetTool("PanTool");
            if (tool.Active)
            {
                diagram1.Enabled = false;
                diagram1.Controller.DeactivateTool(tool);
                btPan.Checked = false;
            }
            else
            {
                diagram1.Enabled = true;
                diagram1.Controller.ActivateTool(tool);
                btPan.Checked = true;

                // turn off select if its on.
                Tool tool2 = diagram1.Controller.GetTool("SelectTool");
                if (tool2.Active)
                {
                    diagram1.Controller.DeactivateTool(tool2);
                }
                btSelect.Checked = false;

                // unselect any selcted nodes.
                diagram1.Controller.SelectionList.Clear();
                diagram1.Controller.View.Update(true);
            }
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show(
                    string.Format(
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                            "Perfectus.Client.UI.Designers.InterviewDesigner.ConfirmReset")), Common.About.FormsTitle,
                    MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                // Redraw the UI from scratch...
                ResetUI(true);
            }
        }

        private void InterviewDesigner_Closed(object sender, EventArgs e)
        {
            SaveDiagram();
            MainDesigner2.PreSave -= new MainDesigner2.PreSaveHandler(MainDesigner_PreSave);
            ((Interview)itemBeingEdited).ParentPackage.ItemCreated -= new EventHandler<PackageItemEventArgs>( ParentPackage_ItemCreated );
            ((Interview)itemBeingEdited).ParentPackage.ItemDeleting -= new EventHandler<PackageItemCancelEventArgs>( ParentPackage_ItemDeleting );
            ((Interview)itemBeingEdited).ParentPackage.ItemDeleted -= new EventHandler<PackageItemDeletedEventArgs>( ParentPackage_ItemDeleted );
        }

        private void MainDesigner_PreSave()
        {
            SaveDiagram();
        }

        private void ctZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strMagValue = ctZoom.Text;
            int idxPctSign = strMagValue.IndexOf('%');
            if (idxPctSign >= 0)
            {
                strMagValue = strMagValue.Remove(idxPctSign, 1);
            }
            int magVal = Convert.ToInt32(strMagValue);
            diagram1.View.Magnification = new Size(magVal, magVal);
            diagram1.Refresh();
        }





        //[Conditional( "DEBUG" )]
        //private void AddDebugComponents()
        //{
        //    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InterviewDesigner));

        //    this.btDebug = new DataDynamics.SharpUI.Toolbars.ButtonTool();
        //    this.cmDebug = new DataDynamics.SharpUI.Toolbars.Command();

        //    this.uiToolbarManager1.Commands.Add(this.cmDebug);
        //    this.tbDiagram.Tools.Add(this.btDebug);

        //    // 
        //    // btDebug
        //    // 
        //    this.btDebug.Command = this.cmDebug;
        //    this.btDebug.Image = (Image)resources.GetObject( "btReset.Image" );
        //    this.btDebug.Name = "btDebug";
        //    this.btDebug.Text = "Debugging Button";
        //    // 
        //    // cmDebug
        //    // 
        //    this.cmDebug.Name = "cmDebug";
        //    this.cmDebug.Executed += new DataDynamics.SharpUI.Toolbars.ToolbarManagerEventHandler(this.cmDebug_Executed);
        //}

    //    private void cmDebug_Executed(object sender, ToolbarManagerEventArgs e)
    //    {
    //        if (MessageBox.Show("Will remove all possible pages to test for system crash. Continue?", "Debug!!", MessageBoxButtons.OKCancel) == DialogResult.OK)
    //        {
    //            Interview interview = ItemBeingEdited as Interview;
    //            Package package = interview.ParentPackage;
    //            String msg = String.Empty;

    //            bool haveDeleted = true;
    //            int count = 0;

    //            while( count < 10 && haveDeleted  )
    //            {
    //                haveDeleted = false;

    //                try
    //                {
    //                    foreach (InterviewPage page in interview.Pages)
    //                    {
    //                        try
    //                        {
    //                            package.DeleteItem(page, false);
    //                            haveDeleted = true;
    //                            count++;
    //                            break;
    //                        }
    //                        catch
    //                        { }
    //                    }
    //                }
    //                catch
    //                {
    //                    haveDeleted = false;
    //                    MessageBox.Show("Exception Caught!!", "Debug!!");
    //                }
    //            }                
    //        }
    //    }
    }

    // X/Y cooridnates of symbol on Syncfusion diagram in floats.
    public struct SymbolLocation
    {
        public float X;
        public float Y;

        public SymbolLocation( float x, float y )
        { X = x; Y = y; }
    };
    
    public class LayoutContext : IDisposable
    {
        private PageSymbol rootNode;
        private Graphics g;
        private Dictionary<Guid, SymbolLocation> symbolPositions = new Dictionary<Guid, SymbolLocation>( );

        public Graphics G
        {
            get { return g; }
        }

        public Dictionary<Guid, SymbolLocation> SymbolPositions
        {
            get { return symbolPositions; }
            set { symbolPositions = value; }
        }

        public PageSymbol RootNode
        {
            get { return rootNode; }
            set { rootNode = value; }
        }

        public LayoutContext( Dictionary<Guid, SymbolLocation> symbolPositions, PageSymbol rootNode, Graphics g)
        {
            this.symbolPositions = symbolPositions;
            this.rootNode = rootNode;
            this.g = g;
        }
 
        #region IDisposable interface
        // Track whether Dispose has been called.
        private bool disposed = false;

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose( )
        {
            Dispose( true );
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if( !this.disposed )
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if( disposing )
                {
                    // Dispose managed resources.
                    this.rootNode.Dispose( );
                    this.g.Dispose( );
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.

                // Note disposing has been done.
                disposed = true;
            }
        }
        
        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~LayoutContext( )
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion

    }
}