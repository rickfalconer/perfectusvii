using System.IO;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Summary description for PdfViewer.
	/// </summary>
	public class HelpViewer : DesignerBase
	{
		private string pdfPath = null;
		private WebBrowser axWebBrowser1;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public HelpViewer(PackageItem item) : base(item)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		public HelpViewer(string pdfPath) : this((PackageItem)null)
		{
			this.pdfPath = pdfPath;
			string title = Path.GetFileName(pdfPath);
			this.Text = title;
		}

		public override void Refresh()
		{
			base.Refresh ();
			if (pdfPath != null)
			{
				object oUrl = pdfPath;
				axWebBrowser1.Navigate(pdfPath);
				//axWebBrowser1.Navigate2(ref oUrl);
			}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(HelpViewer));
			this.axWebBrowser1 = new WebBrowser();		
			this.SuspendLayout();
			// 
			// axWebBrowser1
			// 
			this.axWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.axWebBrowser1.Location = new System.Drawing.Point(0, 0);
			this.axWebBrowser1.Size = new System.Drawing.Size(292, 266);
			this.axWebBrowser1.TabIndex = 0;
			// 
			// HelpViewer
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.axWebBrowser1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "HelpViewer";
			this.Text = "Help";
			
			this.ResumeLayout(false);

		}
#endregion
	}
}
