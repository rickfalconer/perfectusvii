using System.ComponentModel;
using System.Windows.Forms;
using System;

namespace Perfectus.Client.Studio.UI.Designers.Diagrams
{
	/// <summary>
	/// Summary description for DiagramSymbols.
	/// </summary>
	[Serializable()]
	public class DiagramSymbols : Component
	{
		internal PictureBox pbxDecision;
		internal PictureBox pbxPage;
		internal PictureBox pbxOrphan;
		internal PictureBox pbxDestination;
		internal PictureBox pbxStartPage;
		internal PictureBox pbxExternalPage;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public DiagramSymbols(IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			//TODO: Add any constructor code after InitializeComponent call
			//
		}

		public DiagramSymbols()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			//TODO: Add any constructor code after InitializeComponent call
			//
		}

		private static DiagramSymbols instance = null;

		public static DiagramSymbols GetInstance()
		{
			if (instance == null)
			{
				instance = new DiagramSymbols();
			}

			return instance;

		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(DiagramSymbols));
			this.pbxDecision = new System.Windows.Forms.PictureBox();
			this.pbxPage = new System.Windows.Forms.PictureBox();
			this.pbxOrphan = new System.Windows.Forms.PictureBox();
			this.pbxDestination = new System.Windows.Forms.PictureBox();
			this.pbxStartPage = new System.Windows.Forms.PictureBox();
			this.pbxExternalPage = new System.Windows.Forms.PictureBox();
			// 
			// pbxDecision
			// 
			this.pbxDecision.Image = ((System.Drawing.Image)(resources.GetObject("pbxDecision.Image")));
			this.pbxDecision.Location = new System.Drawing.Point(498, 17);
			this.pbxDecision.Name = "pbxDecision";
			this.pbxDecision.TabIndex = 0;
			this.pbxDecision.TabStop = false;
			// 
			// pbxPage
			// 
			this.pbxPage.Image = ((System.Drawing.Image)(resources.GetObject("pbxPage.Image")));
			this.pbxPage.Location = new System.Drawing.Point(139, 17);
			this.pbxPage.Name = "pbxPage";
			this.pbxPage.TabIndex = 0;
			this.pbxPage.TabStop = false;
			// 
			// pbxOrphan
			// 
			this.pbxOrphan.Image = ((System.Drawing.Image)(resources.GetObject("pbxOrphan.Image")));
			this.pbxOrphan.Location = new System.Drawing.Point(241, 17);
			this.pbxOrphan.Name = "pbxOrphan";
			this.pbxOrphan.TabIndex = 0;
			this.pbxOrphan.TabStop = false;
			// 
			// pbxDestination
			// 
			this.pbxDestination.Image = ((System.Drawing.Image)(resources.GetObject("pbxDestination.Image")));
			this.pbxDestination.Location = new System.Drawing.Point(358, 17);
			this.pbxDestination.Name = "pbxDestination";
			this.pbxDestination.TabIndex = 0;
			this.pbxDestination.TabStop = false;
			// 
			// pbxStartPage
			// 
			this.pbxStartPage.Image = ((System.Drawing.Image)(resources.GetObject("pbxStartPage.Image")));
			this.pbxStartPage.Location = new System.Drawing.Point(620, 17);
			this.pbxStartPage.Name = "pbxStartPage";
			this.pbxStartPage.TabIndex = 0;
			this.pbxStartPage.TabStop = false;
			// 
			// pbxExternalPage
			// 
			this.pbxExternalPage.Image = ((System.Drawing.Image)(resources.GetObject("pbxExternalPage.Image")));
			this.pbxExternalPage.Location = new System.Drawing.Point(750, 17);
			this.pbxExternalPage.Name = "pbxExternalPage";
			this.pbxExternalPage.TabIndex = 0;
			this.pbxExternalPage.TabStop = false;

		}

		#endregion

	}
}