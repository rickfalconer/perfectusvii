using System;
using System.Collections;
using System.Drawing;
using Syncfusion.Windows.Forms.Diagram;

namespace Perfectus.Client.Studio.UI.Designers.Diagrams
{
	/// <summary>
	/// Summary description for InterviewManager2.
	/// </summary>
	public class InterviewManager2 : LayoutManager
	{
		private float verticalSpacing = 80;
		private float minHorizontalSpacing = 140;
		private float leftMargin = 180;
		private Graphics g;

		public InterviewManager2(Model m) : base(m)
		{
		}

		public InterviewManager2()
		{
			//
			//TODO: Add constructor logic here
			//
		}

		public override bool UpdateLayout(object contextInfo)
		{
			Model.Height = 0f;
			Model.Width = 700f;
			// Pick apart the context passed to us.
            PageSymbol rootPage = ((LayoutContext)contextInfo).RootNode;
			Hashtable symbolRankMapping = new Hashtable();
            g = ((LayoutContext)contextInfo).G;


			// Compress the ranks.  Make sure there're no gaps.
			int maxRank = 0;
			foreach(INode curNode in this.Model.Nodes)
			{
				if (curNode is PageSymbol)
				{
					PageSymbol curSym = (PageSymbol)curNode;
					if (symbolRankMapping.ContainsKey(curSym.Rank))
					{
						((PageSymbolDictionary)(symbolRankMapping[curSym.Rank])).Add(curSym.InterviewId, curSym);
					}
					else
					{
						symbolRankMapping.Add(curSym.Rank, new PageSymbolDictionary());
						((PageSymbolDictionary)(symbolRankMapping[curSym.Rank])).Add(curSym.InterviewId, curSym);
					}
					if (curSym.Rank > maxRank)
					{
						maxRank = curSym.Rank;
					}
				}
			}

			int newKey = 0;
			for(int i = 1; i<= maxRank; i++)
			{
				if (symbolRankMapping.ContainsKey(i))
				{
					continue;
				}
				else
				{
					// We've found a gap, so renumber all succeeding ranks down one.
					newKey = i;
					for(int j = i; j <= maxRank; j++)
					{
						if (symbolRankMapping.ContainsKey(j))
						{
							symbolRankMapping.Add(newKey, symbolRankMapping[j]);
							symbolRankMapping.Remove(j);
							foreach(PageSymbol pSym in ((PageSymbolDictionary)(symbolRankMapping[newKey])).Values)
							{
								pSym.Rank = newKey;
							}
							newKey++;
						}
					}					
					maxRank = newKey - 1;
				}
			}

			if (this.Model != null)
			{
				if (rootPage != null)
				{
					// Recursively position each page, starting from the root.

					// Position the start page in the middle of the diagram.
					PositionPage( contextInfo, rootPage, this.Model.Bounds.Left + leftMargin, this.Model.Bounds.Right, null);
				}
			}


			// Having positioned all the pages, put the destination symbol below the lowest one that references it.
			foreach (INode curNode in this.Model.Nodes)
			{
				DestinationSymbol destSym = curNode as DestinationSymbol;
				if (destSym != null)
				{
                    if( !destSym.IgnoreHistory &&  ( (LayoutContext)contextInfo ).SymbolPositions.ContainsKey( Guid.Empty ) )
                    {
                        destSym.X = ( (LayoutContext)contextInfo ).SymbolPositions[ Guid.Empty ].X;
                        destSym.Y = ( (LayoutContext)contextInfo ).SymbolPositions[ Guid.Empty ].Y;
                    }
                    else
                    {
                        float range = this.Model.Width;
                        float curLeft = this.Model.Bounds.Left;
                        float curRight = curLeft + range;
                        float rangeCenter = ( ( curLeft + curRight ) / 2.0f );

                        destSym.X = rangeCenter - ( destSym.Width / 2.0f );
                        destSym.Y = ( maxRank + 1 ) * ( verticalSpacing + 100 );
                    }
				}
			}


			// Find all the lost pages (rank < 0) and put them down the edge.
			int lostPageCount = 0;
			foreach (INode curNode in this.Model.Nodes)
			{
				PageSymbol pageSym = curNode as PageSymbol;
				if (pageSym != null && pageSym.Rank < 0 )
				{
                    // First check if we already have a fixed position. Note to stop new pages or unpositioned pages ending on top of lost pages along
                    // the edge, we also need to reposition lost pages
                    if( !pageSym.IgnoreHistory && ( (LayoutContext)contextInfo ).SymbolPositions.ContainsKey( pageSym.InterviewId ) )
                    {
                        if( ( (LayoutContext)contextInfo ).SymbolPositions[ pageSym.InterviewId ].X != (float)PageSymbol.OrphanX )
                        {
                            pageSym.X = ( (LayoutContext)contextInfo ).SymbolPositions[ pageSym.InterviewId ].X;
                            pageSym.Y = ( (LayoutContext)contextInfo ).SymbolPositions[ pageSym.InterviewId ].Y;
                            pageSym.Positioned = true;
                        }
                    }

                    if( !pageSym.Positioned )
                    {
                        pageSym.X = (float)PageSymbol.OrphanX;
                        pageSym.Y = ( ( verticalSpacing + 20 ) * lostPageCount ) + 50;
					    lostPageCount ++;
                    }
				}
			}

			OnLayoutUpdated(new EventArgs());


			return true;
		}


		private void PositionPage(object contextInfo, PageSymbol pageSym, float rangeLeft, float rangeRight, PageSymbol parentSym)
		{
            float symWidth = pageSym.Width;
            float rangeCenter = ( rangeLeft + rangeRight ) / 2.0f;

            // First check if we already have a fixed position.
            if( !pageSym.IgnoreHistory &&  ( (LayoutContext)contextInfo ).SymbolPositions.ContainsKey( pageSym.InterviewId ) )
            {
                pageSym.X = ( (LayoutContext)contextInfo ).SymbolPositions[ pageSym.InterviewId ].X;
                pageSym.Y = ( (LayoutContext)contextInfo ).SymbolPositions[ pageSym.InterviewId ].Y;
            }
            else
            {
                // Set the vertical position of the page: virticalspacing below its parent.
                if( !pageSym.Positioned )
                {
                    if( parentSym != null )
                        pageSym.Y = pageSym.Rank * ( verticalSpacing + 100 ); // parentSym.Y + parentSym.Height + verticalSpacing;
                    else
                        pageSym.Y = verticalSpacing + 100;
                }

                // Set the horizontal position of the page: right in the middle of the range.			
                pageSym.X = rangeCenter - ( symWidth / 2.0f );
            }


            // Readjust model size.
            if ((pageSym.Y + pageSym.Height) > Model.Height)
				Model.Height = pageSym.Y + pageSym.Height;
			if ((pageSym.X + symWidth) > Model.Width)
				Model.Width = pageSym.X + symWidth;

			pageSym.Positioned = true;

			// Get the children (with a lower rank - i.e. below us in the diagram) of this symbol
			ICollection childrenPages = pageSym.ChildrenPages;

			int numChildren = childrenPages.Count;

			// Recursively layout the children pages.
			if (numChildren > 0)
			{
				// Calculate how much space to give each child.  Divide our range up equally between the kids
				float childRange = (rangeRight - rangeLeft)/((float) numChildren);
				if (childRange < minHorizontalSpacing)
				{
					childRange = minHorizontalSpacing;
				}


				float childLeft = rangeLeft;
				float childRight = rangeLeft + childRange;

				foreach (IGraphNode n in childrenPages)
				{
					PageSymbol p = n as PageSymbol;
					if (p != null && !(p.Positioned))
					{
                        this.PositionPage( contextInfo, p, childLeft, childRight, pageSym );

						childLeft = childRight;
						childRight = childRight + childRange;
					}
				}
			}
		}
	}
}