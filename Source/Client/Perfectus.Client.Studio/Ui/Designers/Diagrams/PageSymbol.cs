using System.Collections;
using System.Drawing;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Syncfusion.Windows.Forms.Diagram;
using Label = Syncfusion.Windows.Forms.Diagram.Label;
using System;
using System.Runtime.Serialization;

namespace Perfectus.Client.Studio.UI.Designers.Diagrams
{
	/// <summary>
	/// Summary description for PageSymbol.
	/// </summary>
	[Serializable()]
	public class PageSymbol : Symbol, ISerializable
	{
		private Label lblName = null;
		private BitmapNode img = null;
		private BitmapNode imgDecision = null;
		private InterviewPage page = null;
		private int rank = 0;
        private bool ignoreHistory = false;
        private bool positioned = false;
        private Guid interviewId;
        static public uint OrphanX = 70;

        // Indicates the symbol has already been positioned.
		public bool Positioned
		{
			get { return positioned; }
			set { positioned = value; }
		}

        // If set to true, the layout manager will recalculate a new position 
        // irrespective of previous position history.
        public bool IgnoreHistory
        {
            get { return ignoreHistory; }
            set { ignoreHistory = value; }
        }

        public ICollection ChildrenPages
		{
			get
			{
				ArrayList children = new ArrayList();

				IGraphNode thisNode = this as IGraphNode;

				if (thisNode != null)
				{
					ICollection edgesLeaving = thisNode.EdgesLeaving;
					if (edgesLeaving != null)
					{
						foreach (IGraphEdge curEdge in edgesLeaving)
						{
							IGraphNode curPage = curEdge.ToNode; // as PageSymbol;
							if (curPage != null && curPage is PageSymbol)
							{
								//int childRank = ((PageSymbol) curPage).Rank;
								//if (childRank >= this.rank)// || (childRank < this.Rank && ! ((PageSymbol)curPage).Positioned))// = this.Rank + 1)
								//if (((PageSymbol)curPage).Rank != this.Rank)// && !((PageSymbol)curPage).Positioned)
								//{
									children.Add(curPage);
								//}
							}
						}
					}
				}

				return children;
			}
		}

		public int Rank
		{
			get { return rank; }
			set { rank = value; }
		}

        public bool HasQueryBranch
        {
            get { return imgDecision != null; }
        }

		private DiagramSymbols images;

		public PageSymbol()
		{
			images = DiagramSymbols.GetInstance();
		}

        public InterviewPage Page
		{
			get { return page; }
			set 
            {
                page = value;

                // Every InterviewPage has a unique id, which we record in a separate variable 
                // for serialisation.
                this.InterviewId = (page == null) ? Guid.Empty :  page.UniqueIdentifier;
            }
		}

		public Guid InterviewId
		{
			get {return interviewId;}
			set {interviewId = value;}
		}

		internal void ResetUI( int therank )
		{
            // Clear any existing links and reset position flag.
            ClearLinksAndPosition( );

            // Draw all objects based on its ranking...
            this.DrawSymbol( therank );

            // Clear out any existing ports
            this.Ports.Clear( );
            
            // Assign new connection ports only if we have a page.
            if( page != null )
			{
				CirclePort bp = new CirclePort();
				bp.Name = "BottomPort";
				bp.Visible = false;
                Ports.Add( bp );
                bp.X = img.X + ( img.Width / 2 );
                bp.Y = img.Y + img.Height;

				CirclePort pp = new CirclePort();
				pp.Visible = false;
				pp.Name = "PagePort";
				Ports.Add( pp );
                pp.X = img.X + ( img.Width / 2 );
                pp.Y = img.Y + ( img.Height / 2 );
            }

            // Maybe draw the query symbol
            DrawQuery( );
                
            // Draw label under symbol
            DrawLabel( );
		}

        // Draw symbol graphics based on rank
        internal void DrawSymbol( int therank )
        {
            this.Rank = therank;

            // Clearout any children attached to this, but only the objects we intend adding within this class.
            for( int i = this.children.Count - 1; i >= 0; i-- )
            {
                INode node = this.children[ i ];

                if( node is BitmapNode || node is Line ) 
                    this.children.RemoveAt( i );
            }

            // Dispose of current image which we are going to re-insert
            if( img != null ) { img.Dispose( ); img = null; }
            if( imgDecision != null ) { imgDecision.Dispose( ); imgDecision = null; }

            if( page != null )
            {
                if( rank >= 0 )
                {
                    if( page.ParentInterview.StartPage == page )
                    {
                        img = new BitmapNode( new Bitmap( images.pbxStartPage.Image ) );
                    }
                    else if( page is ExternalPage )
                    {
                        img = new BitmapNode( new Bitmap( images.pbxExternalPage.Image ) );
                    }
                    else
                    {
                        img = new BitmapNode( new Bitmap( images.pbxPage.Image ) );
                    }
                }
                else
                {
                    img = new BitmapNode( new Bitmap( images.pbxOrphan.Image ) );
                }
                AppendChild( img );
            }
        }

        // Draw query graphics 
        internal void DrawQuery(  )
        {
            // Important to do this step otherwise adding additional images to the symbol will cause it to have excessive sizes.
            this.X = 0.0F;
            this.Y = 0.0F;

            if( page != null && rank >= 0 && page.NextPage.ValueType == PageQueryValue.PageQueryValueType.Query )
            {
                if( imgDecision == null )
                {
                    imgDecision = new BitmapNode( new Bitmap( images.pbxDecision.Image ) );
                    imgDecision.X = ( img.X + ( img.Width / 2 ) ) - ( imgDecision.Width / 2 );
                    imgDecision.Y = ( img.Y + img.Height ) + 8;
                    AppendChild( imgDecision );

                    PointF start = new PointF( img.X + ( img.Width / 2 ), img.Y + img.Height );
                    PointF end = new PointF( imgDecision.X + ( imgDecision.Width / 2 ), imgDecision.Y );
                    Line l = new Line( start, end );
                    AppendChild( l );
                }
            }
            else if( imgDecision != null )
            {
                // Clearout the existing image
                foreach( INode node in this.children )
                {
                    if( node is BitmapNode && node == imgDecision )
                    {
                        this.children.Remove( node );
                        break;
                    }
                }
                imgDecision.Dispose( ); 
                imgDecision = null; 
            }

            // Position the bottom port.
            CirclePort bp = null;
            foreach( Port port in this.Ports )
            {
                if( port is CirclePort && port.Name == "BottomPort" )
                { bp = (CirclePort)port; break; }
            }

            if( bp != null )
            {
                if( this.HasQueryBranch )
                {
                    bp.X = imgDecision.X + ( imgDecision.Width / 2 );
                    bp.Y = imgDecision.Y + imgDecision.Height;
                }
                else
                {
                    bp.X = img.X + ( img.Width / 2 );
                    bp.Y = img.Y + img.Height;
                }
            }
        }

        // Draw label under symbol
        internal void DrawLabel( )
        {
            // Maybe add new label...
            if( this.lblName == null )
                lblName = AddLabel( "", BoxPosition.BottomCenter );

            // Maybe add the page name.
            if( page != null )
            {
                lblName.Text = page.Name;
                lblName.ReadOnly = true;
                lblName.FontStyle.Family = "Arial";
                lblName.FontStyle.Bold = true;
                lblName.FontStyle.PointSize = 11f;
                lblName.OffsetY = 20;
                lblName.Width = img.Width * 3.5f;
                lblName.HorizontalAlignment = StringAlignment.Center;
                lblName.VerticalAlignment = StringAlignment.Center;
            }
        }

        // Removes all connectivity and positioning required for redrawing...
        public void ClearLinksAndPosition( )
        {
            positioned = false;

            // Remove all connections.
            foreach( Port port in Ports )
                this.DisconnectAll( port );
        }

		public override bool AcceptConnection(Port sourcePort, Port targetPort)
		{
			return true;
		}

		#region ISerializable Members

		public PageSymbol(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			try
			{
				interviewId = (Guid) info.GetValue("psInterviewId", typeof (Guid));
			}
			catch
			{
			}
		}

		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("psInterviewId", page.UniqueIdentifier);			
		}

		#endregion

        #region IDisposable Members
        // Track whether Dispose has been called.
        private bool disposed = false;
        protected override void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                try
                {
                    if( disposing )
                    {
                        // Release the managed resources you added in
                        // this derived class here.
                        if( img != null )
                        { img.Dispose( ); img = null; }
                        if( imgDecision != null )
                        { imgDecision.Dispose( ); imgDecision = null; }
                        if( lblName != null )
                        { lblName.Dispose( ); lblName = null; }
                    }
                    // Release the native unmanaged resources you added
                    // in this derived class here.

                    this.disposed = true;
                }
                finally
                {
                    // Call Dispose on your base class.
                    base.Dispose( disposing );
                }
            }
        }
        #endregion
    }
}