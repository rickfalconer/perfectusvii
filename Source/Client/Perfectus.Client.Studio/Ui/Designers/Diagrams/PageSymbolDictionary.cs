using System;
using System.Collections;

namespace Perfectus.Client.Studio.UI.Designers.Diagrams
{
    /// <summary>
    /// A dictionary with keys of type Guid and values of type PageSymbol
    /// </summary>
    [Serializable()]
    public class PageSymbolDictionary : DictionaryBase
    {

        /// <summary>
        /// Gets or sets the PageSymbol associated with the given Guid
        /// </summary>
        /// <param name="key">
        /// The Guid whose value to get or set.
        /// </param>
        public virtual PageSymbol this[Guid key]
        {
            get { return (PageSymbol) Dictionary[key]; }
            set { Dictionary[key] = value; }
        }

        /// <summary>
        /// Adds an element with the specified key and value to this PageSymbolDictionary.
        /// </summary>
        /// <param name="key">
        /// The Guid key of the element to add.
        /// </param>
        /// <param name="value">
        /// The PageSymbol value of the element to add.
        /// </param>
        public virtual void Add(Guid key, PageSymbol value)
        {
            Dictionary.Add(key, value);
        }

        /// <summary>
        /// Determines whether this PageSymbolDictionary contains a specific key.
        /// </summary>
        /// <param name="key">
        /// The Guid key to locate in this PageSymbolDictionary.
        /// </param>
        /// <returns>
        /// true if this PageSymbolDictionary contains an element with the specified key;
        /// otherwise, false.
        /// </returns>
        public virtual bool Contains(Guid key)
        {
            return Dictionary.Contains(key);
        }

        /// <summary>
        /// Determines whether this PageSymbolDictionary contains a specific key.
        /// </summary>
        /// <param name="key">
        /// The Guid key to locate in this PageSymbolDictionary.
        /// </param>
        /// <returns>
        /// true if this PageSymbolDictionary contains an element with the specified key;
        /// otherwise, false.
        /// </returns>
        public virtual bool ContainsKey(Guid key)
        {
            return Dictionary.Contains(key);
        }

        /// <summary>
        /// Determines whether this PageSymbolDictionary contains a specific value.
        /// </summary>
        /// <param name="value">
        /// The PageSymbol value to locate in this PageSymbolDictionary.
        /// </param>
        /// <returns>
        /// true if this PageSymbolDictionary contains an element with the specified value;
        /// otherwise, false.
        /// </returns>
        public virtual bool ContainsValue(PageSymbol value)
        {
            foreach (PageSymbol item in Dictionary.Values)
            {
                if (item == value)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Removes the element with the specified key from this PageSymbolDictionary.
        /// </summary>
        /// <param name="key">
        /// The Guid key of the element to remove.
        /// </param>
        public virtual void Remove(Guid key)
        {
            Dictionary.Remove(key);
        }

        /// <summary>
        /// Gets a collection containing the keys in this PageSymbolDictionary.
        /// </summary>
        public virtual ICollection Keys
        {
            get { return Dictionary.Keys; }
        }

        /// <summary>
        /// Gets a collection containing the values in this PageSymbolDictionary.
        /// </summary>
        public virtual ICollection Values
        {
            get { return Dictionary.Values; }
        }
    }
}