using System.Drawing;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Syncfusion.Windows.Forms.Diagram;
using System;
using System.Runtime.Serialization;

namespace Perfectus.Client.Studio.UI.Designers.Diagrams
{
	/// <summary>
	/// Summary description for DestinationSymbol.
	/// </summary>
	[Serializable()]
	public class DestinationSymbol : Symbol, ISerializable
	{
		private Label lblName = null;
		private BitmapNode img = null;
		private Interview interview;
        private bool ignoreHistory = false;
		private DiagramSymbols images;

		public DestinationSymbol()
		{
			CenterPort.AttachAtPerimeter = true;
			images = DiagramSymbols.GetInstance();
		}

		public Interview Value
		{
			get { return interview; }
			set { interview = value; }
		}

        // If set to true, the layout manager will recalculate a new position 
        // irrespective of previous position history.
        public bool IgnoreHistory
        {
            get { return ignoreHistory; }
            set { ignoreHistory = value; }
        }

        public override bool AcceptConnection( Port sourcePort, Port targetPort )
		{
			return true;
		}

		internal void ResetUI()
		{
            // Clearout any children attached to this
            this.RemoveAllChildren( );

            // Dispose of all resources which we are going to re-insert
            if( img != null ) { img.Dispose( ); img = null; }
            if( lblName != null ) { lblName.Dispose( ); lblName = null; }
            
            img = new BitmapNode( new Bitmap( images.pbxDestination.Image ) );
			AppendChild(img);

			CenterPort.AttachAtPerimeter = true;

			lblName = AddLabel("", BoxPosition.BottomCenter);

			if (interview != null)
			{
				if (interview.DestinationUrl != null)
				{
					lblName.Text = interview.DestinationUrl;
				}
				else
				{
					lblName.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.Diagrams.DestinationSymbol.EndOfInterviewLabel");
				}
			}

			lblName.ReadOnly = true;
			lblName.FontStyle.Family = "Arial";
			lblName.FontStyle.Bold = true;
			lblName.FontStyle.PointSize = 11f;
			lblName.OffsetY = 20;
			lblName.Width = img.Width*3.5f;
			lblName.HorizontalAlignment = StringAlignment.Center;
			lblName.VerticalAlignment = StringAlignment.Center;
		}

		#region ISerializable Members

		public DestinationSymbol(SerializationInfo info, StreamingContext context) : base(info, context)
		{

		}

		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		#endregion

        #region IDisposable Members
        // Track whether Dispose has been called.
        private bool disposed = false;
        protected override void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                try
                {
                    if( disposing )
                    {
                        // Release the managed resources you added in
                        // this derived class here.
                        if( img != null )
                        { img.Dispose( ); img = null; }
                        if( lblName != null )
                        { lblName.Dispose( ); lblName = null; }
                    }
                    // Release the native unmanaged resources you added
                    // in this derived class here.

                    this.disposed = true;
                }
                finally
                {
                    // Call Dispose on your base class.
                    base.Dispose( disposing );
                }
            }
        }
        #endregion
    }
}