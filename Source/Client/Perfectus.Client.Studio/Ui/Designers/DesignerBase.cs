using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Summary description for DesignerBase.
	/// </summary>
    #if DESIGN
	public class DesignerBase : Form
    #else
    public abstract class DesignerBase : Form
    #endif
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		protected PackageItem itemBeingEdited = null;


        public virtual bool HasChanges
        {
            get
            {
                 return false;
            }
            set 
            {
                //Do nothing
            }
        }

		public virtual PrefferedDesigner DefaultDesigner
		{
			get { return PrefferedDesigner.Default;}
		}

		public virtual void UpdateUI()
		{
		}

        #if DESIGN
        public DesignerBase()
        { 
            InitializeComponent();
        }
        #endif
        
		public DesignerBase(PackageItem item)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			itemBeingEdited = item;
			if (itemBeingEdited != null)
			{
				itemBeingEdited.PropertyChanged += new PropertyChangedEventHandler(itemBeingEdited_PropertyChanged);
			}
		}

		public PackageItem ItemBeingEdited
		{
			get { return itemBeingEdited; }
		}

        protected virtual void ResetUI( )
        {
            this.ResetUI( false );
        }
        
        protected virtual void ResetUI( bool refresh )
		{
			DocumentNameBind();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (itemBeingEdited != null)
			{
				itemBeingEdited.PropertyChanged -= new PropertyChangedEventHandler(itemBeingEdited_PropertyChanged);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		public virtual bool Save()
		{
			return true;
		}

		[field : NonSerialized]
		public event PropertyChangedEventHandler PropertyChanged;

		[field : NonSerialized]
		public event EventHandler<PackageItemEventArgs> ItemSelected;

		protected void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void OnItemSelected(PackageItem item)
		{
			if (ItemSelected != null)
			{
				ItemSelected(this, new PackageItemEventArgs(item));
			}
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignerBase));
            this.SuspendLayout();
            // 
            // DesignerBase
            // 
            resources.ApplyResources(this, "$this");
            this.Name = "DesignerBase";
            this.ResumeLayout(false);

		}

		#endregion

		protected virtual void itemBeingEdited_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Name":
					DocumentNameBind();
					break;
			}
		}

		protected virtual bool OkayToClose()
		{
			return true;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (!OkayToClose())
			{
				e.Cancel = true;
			}

			base.OnClosing(e);
		}


		protected void DocumentNameBind()
		{
			if (itemBeingEdited != null)
			{
				Text = itemBeingEdited.Name;
				OnPropertyChanged("Text");
			}
			else
			{
				Text = String.Empty;
			}
		}

	}
}