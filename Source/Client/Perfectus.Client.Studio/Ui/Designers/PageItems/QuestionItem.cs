using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.Designers;
using System.Resources;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	public class QuestionItem : Item
	{
		private IContainer components = null;
		private Question question = null;
		private int oldHeight = 0;
        private ResourceManager rm;


		public Question Question
		{
			get { return question; }
		}

		protected override string name
		{
			get
			{
				if (question != null)
				{
					return question.Name;
				}
				else
				{
					return string.Empty;
				}
			}
		}

		public QuestionItem()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();
			this.imageIndex = 0;
            rm = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation");
		}

		private void SetHeight()
		{
			if (!IsInHorizontal())
			{
				switch (question.DisplayType)
				{
					case QuestionDisplayType.CheckBoxList:
					case QuestionDisplayType.HtmlArea:
					case QuestionDisplayType.ListBox:
					case QuestionDisplayType.MultiSelectListBox:
					case QuestionDisplayType.RadioList:
					case QuestionDisplayType.TextArea:
						Height = 154;
						break;
					default:
						Height = 104;
						break;
				}

				if (Height != oldHeight && oldHeight != 0)
				{
					ParentItemList.ResetUI();
				}
				oldHeight = Height;
			}
		}

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			base.OnDragEnter(drgevent);
			drgevent.Effect = DragDropEffects.None;
		}


		public QuestionItem(Question question) : this()
		{
			this.question = question;
			this.PackageBeingEdited = question.ParentPackage;

			SetHeight();
			question.PropertyChanged += new PropertyChangedEventHandler(question_PropertyChanged);
			this.SetStyle(ControlStyles.Selectable, true);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (question != null)
				{
					question.PropertyChanged -= new PropertyChangedEventHandler(question_PropertyChanged);
				}
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		protected bool IsInHorizontal()
		{
			return (ParentItemList is HorizontalGroup || ParentItemList is RepeaterItem);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			RectangleF myRect = new RectangleF(0, 0, Width, Height);

			bool isHoriz = IsInHorizontal();
			
			//if we have a query with true and false action, we need to show "Query..." for the prompt,
            //if we have a query with one level only and only a true action, we need to show the resultant string 
            //(with string bound elements)
            string prompt = string.Empty;
            string hint = string.Empty;
            string example = string.Empty;
            string defaultAnswer = string.Empty;

            if (question.Prompt.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query)
            {
                prompt = rm.GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.QueryValue");
            }
            else
            {
                prompt = question.Prompt.EvaluateAndKeepXml();
            }

            if (question.Hint.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query)
            {
                hint = rm.GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.QueryValue");
            }
            else
            {
                hint = question.Hint.EvaluateAndKeepXml();
            }

            if (question.Example.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query)
            {
                example = rm.GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.QueryValue");
            }
            else
            {
                example = question.Example.EvaluateAndKeepXml();
            }

            if (question.DefaultAnswer.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query)
            {
                defaultAnswer = string.Format(" {0}", rm.GetString("Perfectus.Client.UI.PackageObjectEditors.TypeConverters.PageQueryConverter.QueryValue"));
            }
            else
            {
                defaultAnswer = string.Format(" {0}", question.DefaultAnswer);
            }

			if(prompt != null)
			{
				prompt = StringBindingMod.GetDocHtmlFromXml(prompt,question.ParentPackage, true);
			}
			if(hint != null)
			{
				hint = StringBindingMod.GetDocHtmlFromXml(hint,question.ParentPackage, true);
			}
			if(example != null)
			{
				example = StringBindingMod.GetDocHtmlFromXml(example,question.ParentPackage, true);
			}
			if(defaultAnswer != null)
			{
				defaultAnswer = StringBindingMod.GetDocHtmlFromXml(defaultAnswer,question.ParentPackage, true);
			}

			RectangleF promptRect;
			RectangleF hintRect;
			RectangleF exampleRect;
			RectangleF controlRect;

			StringFormat sf = new StringFormat(StringFormat.GenericDefault);
			sf.Trimming = StringTrimming.EllipsisCharacter;

			Brush promptBrush = new SolidBrush(Color.FromArgb(0x11, 0x44, 0x69));
			Brush exampleBrush = new SolidBrush(Color.FromArgb(0x99, 0x99, 0x99));
			Brush hintBrush = new SolidBrush(Color.FromArgb(0x66, 0x66, 0x66));

			Font promptFont = new Font("Arial", 12, FontStyle.Bold, GraphicsUnit.Pixel);
			Font exampleFont = new Font("Arial", 10, GraphicsUnit.Pixel);
			Font hintFont = new Font("Arial", 10, GraphicsUnit.Pixel);
			Font answerFont = new Font("Arial", 11, GraphicsUnit.Pixel);

			float leftMargin = 12.0f;
			float topMargin = 34.0f;
			float fullWidth = Width - ((leftMargin*2) + 9.0f);
			float fullHeight = Height - (topMargin + 18.0f);

			float vPad = 2.0f;
			float hPad = 3.0f;

            if (!isHoriz)
            {
                promptRect = new RectangleF(leftMargin, topMargin, fullWidth * 0.45f, fullHeight);
                switch (question.DisplayType)
                {
                    case QuestionDisplayType.CheckBoxList:
                    case QuestionDisplayType.HtmlArea:
                    case QuestionDisplayType.ListBox:
                    case QuestionDisplayType.MultiSelectListBox:
                    case QuestionDisplayType.RadioList:
                    case QuestionDisplayType.TextArea:
                        controlRect = new RectangleF(promptRect.Right + hPad, topMargin, fullWidth - (promptRect.Width + hPad), fullHeight - (exampleFont.Size + hintFont.Size + 10.0f));
                        break;

                    case QuestionDisplayType.Calendar:
                    case QuestionDisplayType.CheckBox:
                    //					case QuestionDisplayType.InterwovenObjectPicker:
                    case QuestionDisplayType.Password:
                    case QuestionDisplayType.TextBox:
                    case QuestionDisplayType.DropDownList:
                    default:
                        controlRect = new RectangleF(promptRect.Right + hPad, topMargin, fullWidth - (promptRect.Width + hPad), promptFont.Size + 4.0f);
                        break;

                }

                hintRect = new RectangleF(controlRect.Left, controlRect.Bottom + vPad, controlRect.Width, hintFont.Size + 2.0f);
                exampleRect = new RectangleF(controlRect.Left, hintRect.Bottom + vPad, controlRect.Width, exampleFont.Size + 2.0f);
            }
            else
            {
                promptRect = new RectangleF(leftMargin, topMargin, fullWidth, promptFont.Size + 4.0f);
                switch (question.DisplayType)
                {
                    case QuestionDisplayType.CheckBoxList:
                    case QuestionDisplayType.HtmlArea:
                    case QuestionDisplayType.ListBox:
                    case QuestionDisplayType.MultiSelectListBox:
                    case QuestionDisplayType.RadioList:
                    case QuestionDisplayType.TextArea:
                        controlRect = new RectangleF(leftMargin, promptRect.Bottom + vPad, fullWidth, fullHeight - (exampleFont.Size + hintFont.Size + 24.0f));
                        break;

                    case QuestionDisplayType.Calendar:
                    case QuestionDisplayType.CheckBox:
                    //					case QuestionDisplayType.InterwovenObjectPicker:
                    case QuestionDisplayType.Password:
                    case QuestionDisplayType.TextBox:
                    case QuestionDisplayType.DropDownList:
                    default:
                        controlRect = new RectangleF(leftMargin, promptRect.Bottom + vPad, fullWidth, promptFont.Size + 4.0f);
                        break;
                }
                hintRect = new RectangleF(leftMargin, controlRect.Bottom + vPad,  fullWidth, hintFont.Size + 4.0f);
                exampleRect = new RectangleF(leftMargin, hintRect.Bottom, fullWidth, exampleFont.Size + 3.0f);
            }

			switch (question.DisplayType)
			{
				case QuestionDisplayType.DropDownList:
				case QuestionDisplayType.TextBox:
				case QuestionDisplayType.TextArea:
				case QuestionDisplayType.Password:
				case QuestionDisplayType.HtmlArea:
//				case QuestionDisplayType.InterwovenObjectPicker:
					RectangleF tbRect = new RectangleF(controlRect.Left, controlRect.Top, controlRect.Width, answerFont.Size + 4.0f);
					//tbRect.Y = controlRect.Y + ((controlRect.Height - tbRect.Height) / 2f);
					e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(0xd7, 0xe2, 0xe6)), tbRect);
					ControlPaint.DrawBorder(e.Graphics, Rectangle.Truncate(tbRect), Color.FromArgb(0x7f, 0x9d, 0xb9), ButtonBorderStyle.Solid);

					if (question.DisplayType == QuestionDisplayType.DropDownList)
					{
						ControlPaint.DrawComboButton(e.Graphics, (int) tbRect.Right - 17, (int) tbRect.Top + 1, 15, (int) tbRect.Height - 2, ButtonState.Normal);
					}

					if (question.DisplayType == QuestionDisplayType.Password)
					{
						defaultAnswer = new string('\u25cf', 6); // six black dots
					}

					e.Graphics.DrawString(defaultAnswer, answerFont, Brushes.Black, tbRect, sf);
					break;
				case QuestionDisplayType.Calendar:
					RectangleF calTbRect = new RectangleF(controlRect.Left, controlRect.Top, 70.0f, answerFont.Size + 4.0f);
					calTbRect.Y = controlRect.Y + ((controlRect.Height - calTbRect.Height)/2f);
					e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(0xd7, 0xe2, 0xe6)), calTbRect);
					ControlPaint.DrawBorder(e.Graphics, Rectangle.Truncate(calTbRect), Color.FromArgb(0x7f, 0x9d, 0xb9), ButtonBorderStyle.Solid);
					e.Graphics.DrawString(defaultAnswer, answerFont, Brushes.Black, calTbRect, sf);
					RectangleF calBtnRect = new RectangleF(calTbRect.Right + hPad, calTbRect.Top, 25.0f, calTbRect.Height);
//					ControlPaint.DrawButton(e.Graphics, Rectangle.Truncate(calBtnRect), ButtonState.Normal);
					e.Graphics.DrawImageUnscaled(Images.GetInstance().pbxCalendar.Image, Rectangle.Truncate(calBtnRect));
					break;
				case QuestionDisplayType.CheckBox:
					ControlPaint.DrawCheckBox(e.Graphics, (int) controlRect.X, (int) controlRect.Y + 2, 15, 15, ButtonState.Normal);
					break;
				case QuestionDisplayType.ListBox:
				case QuestionDisplayType.MultiSelectListBox:
					RectangleF bRect = new RectangleF(controlRect.Left, controlRect.Top, controlRect.Width, controlRect.Height);
					e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(0xd7, 0xe2, 0xe6)), bRect);
					ControlPaint.DrawBorder(e.Graphics, Rectangle.Truncate(bRect), Color.FromArgb(0x7f, 0x9d, 0xb9), ButtonBorderStyle.Solid);
					float itemTop = bRect.Top + 2.0f;
					if (question.Items != null && question.Items.Count > 0)
					{
						foreach (ItemsItem ii in question.Items)
						{
							RectangleF itemRect = new RectangleF(bRect.Left + 2.0f, itemTop, bRect.Width - 4.0f, answerFont.Size + 2.0f);
							e.Graphics.DrawString(ii.Display, answerFont, Brushes.Black, itemRect, sf);
							itemTop += (answerFont.Size + 2.0f);
							if (itemTop + (answerFont.Size + 2.0f) > bRect.Bottom)
							{
								break;
							}
						}
					}

					break;
				case QuestionDisplayType.CheckBoxList:
				case QuestionDisplayType.RadioList:
					float cItemTop = controlRect.Top + 2.0f;
					float cItemLeft = controlRect.X;
					if (question.Items != null && question.Items.Count > 0)
					{
						foreach (ItemsItem ii in question.Items)
						{
							if (question.DisplayType == QuestionDisplayType.CheckBoxList)
							{
								ControlPaint.DrawCheckBox(e.Graphics, (int) cItemLeft, (int) cItemTop + 2, 15, 15, ButtonState.Normal);
							}
							else if (question.DisplayType == QuestionDisplayType.RadioList)
							{
								ControlPaint.DrawRadioButton(e.Graphics, (int) cItemLeft, (int) cItemTop + 2, 13, 13, ButtonState.Normal);
							}
							RectangleF itemRect = new RectangleF(cItemLeft + 20.0f, cItemTop, controlRect.Width - 4.0f, answerFont.Size + 2.0f);
							e.Graphics.FillRectangle(Brushes.Transparent, itemRect);
							e.Graphics.DrawString(ii.Display, answerFont, Brushes.Black, itemRect, sf);

							if (question.Direction == Question.ListDirection.Vertical)
							{
								cItemTop += (answerFont.Size + 2.0f);
								if (cItemTop + (answerFont.Size + 2.0f) > controlRect.Bottom)
								{
									break;
								}
							}
							else
							{
								cItemLeft += e.Graphics.MeasureString(ii.Display, answerFont, (int) (itemRect.X), sf).Width + 30.0f;

							}

						}
					}
					break;
			}


			e.Graphics.DrawString(prompt, promptFont, promptBrush, promptRect, sf);
            e.Graphics.DrawString(hint, hintFont, hintBrush, hintRect, sf);
			e.Graphics.DrawString(example, exampleFont, exampleBrush, exampleRect, sf);
			


		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Name = "pictureBox1";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Name = "pictureBox2";
			// 
			// pictureBox3
			// 
			this.pictureBox3.Name = "pictureBox3";
			// 
			// pictureBox5
			// 
			this.pictureBox5.Name = "pictureBox5";
			// 
			// pictureBox6
			// 
			this.pictureBox6.Name = "pictureBox6";
			// 
			// pictureBox7
			// 
			this.pictureBox7.Name = "pictureBox7";
			// 
			// pictureBox8
			// 
			this.pictureBox8.Name = "pictureBox8";
			// 
			// pictureBox20
			// 
			this.pictureBox20.Name = "pictureBox20";
			// 
			// pictureBox15
			// 
			this.pictureBox15.Name = "pictureBox15";
			this.ResumeLayout(false);

		}

		#endregion

		private void question_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			switch (e.PropertyName)
			{
				case "Name":
				case "Prompt":
				case "DefaultAnswer":
				case "Hint":
				case "Example":
				case "Items":
				case "DisplayType":
				case "Direction":
					SetHeight();
					Invalidate();
					break;
			}
		}
	}
}
