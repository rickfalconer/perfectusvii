using System;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	/// <summary>
	/// Summary description for SelectablePanel.
	/// </summary>
	public class SelectablePanel : Panel
	{
		private HorizontalGroup parentGroup;

		public HorizontalGroup ParentGroup
		{
			get { return parentGroup; }
			set { parentGroup = value; }
		}

		private InvisibleButton btn;

		public SelectablePanel()
		{
			btn = new InvisibleButton();
			btn.Width = 1;
			btn.Height = 1;
			//btn.SendToBack();
			Controls.Add(btn);


			SetStyle(ControlStyles.Selectable, true);

			btn.GotFocus += new EventHandler(btn_GotFocus);
			btn.Click += new EventHandler(btn_Click);
		}

		private void btn_GotFocus(object sender, EventArgs e)
		{
			if (parentGroup != null)
			{
				parentGroup.LookFocused();
			}
		}

		private void btn_Click(object sender, EventArgs e)
		{
			btn_GotFocus(sender, e);
		}
	}
}