using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	/// <summary>
	/// Summary description for HorizontalGroup.
	/// </summary>
	public class HorizontalGroup : Item
	{
		private SelectablePanel pnlChildren;

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public HorizontalGroup()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			pnlChildren.ParentGroup = this;

			imageIndex = 2;
		}


		protected override string name
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.HorizontalGroupName");
			}
		}

		public override void ResetUI()
		{
			ResetUI(pnlChildren);
		}

		protected override void Position(Item item, Item prevItem)
		{
			int newLeftMargin = pnlChildren.Left;
			int newTopMargin = 0;


			item.Width = ((pnlChildren.Width - newLeftMargin)/this.Length);
			item.Height = pnlChildren.Height - 5;

			item.Top = newTopMargin;
			if (prevItem == null)
			{
				item.Left = newLeftMargin;
			}
			else
			{
				item.Left = prevItem.Right;
			}
		}

		protected override void OnResize(EventArgs e)
		{
			base.OnResize(e);
			if (pnlChildren != null && Head != null)
			{
				//panel1.SuspendLayout();
				Item i = Head;
				if (Head != null)
				{
					do
					{
						Position(i, i.Prev);
						i = i.Next;
					} while (i != null);
				}
				//panel1.ResumeLayout();
				pnlChildren.Invalidate(true);
			}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		public void LookFocused()
		{
			OnEnter(new EventArgs());
		}


		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);

			object o = (pnlChildren.Controls[0]);
			if (o is Button)
			{
				Button b = (Button) o;
				this.FindForm().Select();
				b.PerformClick();
			}

			//pnlChildren.Select();
			//LookFocused();

			//((Button)(pnlChildren.Controls[pnlChildren.Controls.Count-1])).PerformClick();
		}

		protected override void OnEnter(EventArgs e)
		{
			base.OnEnter(e);
			foreach (Control c in pnlChildren.Controls)
			{
				if (c is Item)
				{
					((Item) c).LookBlurred();
				}
			}
		}


		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pnlChildren = new SelectablePanel();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.Location = new System.Drawing.Point(14, 169);
			this.pictureBox2.Name = "pictureBox2";
			// 
			// pictureBox3
			// 
			this.pictureBox3.Location = new System.Drawing.Point(314, 169);
			this.pictureBox3.Name = "pictureBox3";
			// 
			// pictureBox6
			// 
			this.pictureBox6.Name = "pictureBox6";
			// 
			// pictureBox8
			// 
			this.pictureBox8.Name = "pictureBox8";
			// 
			// panel1
			// 
			this.pnlChildren.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
				| System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.pnlChildren.BackColor = System.Drawing.Color.White;
			this.pnlChildren.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlChildren.Location = new System.Drawing.Point(8, 29);
			this.pnlChildren.Name = "pnlChildren";
			this.pnlChildren.Size = new System.Drawing.Size(307, 141);
			this.pnlChildren.TabIndex = 14;
			// 
			// HorizontalGroup
			// 
			this.Controls.Add(this.pnlChildren);
			//this.Name = "HorizontalGroup";
			//this.Size = new System.Drawing.Size(328, 184);
			this.Controls.SetChildIndex(this.pictureBox6, 0);
			this.Controls.SetChildIndex(this.pictureBox8, 0);
			this.Controls.SetChildIndex(this.pictureBox2, 0);
			this.Controls.SetChildIndex(this.pictureBox3, 0);
			this.Controls.SetChildIndex(this.pnlChildren, 0);
			this.ResumeLayout(false);

		}

		#endregion
	}
}