#if ADOBE
using System;
using System.ComponentModel;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	public class TextItem : Item
	{
		private TextBox textbox;
		private bool isResizing = false;
		private IContainer components = null;
		private byte[] initialHash;

		public new string Text
		{
			get { return textbox.Text; }
			set { textbox.Text = value; }
		}

		public TextItem()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			imageIndex = 1;

			SetNoChanges();


		}

		protected override string name
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.TextItemName");
			}
		}


		public TextItem(string textBody) : this()
		{
			textbox.Text = textBody;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textbox = new TextBox();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Name = "pictureBox1";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Cursor = System.Windows.Forms.Cursors.SizeNS;
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(301, 7);
			this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
			this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
			this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
			// 
			// pictureBox3
			// 
			this.pictureBox3.Name = "pictureBox3";
			// 
			// pictureBox5
			// 
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(6, 123);
			// 
			// pictureBox6
			// 
			this.pictureBox6.Name = "pictureBox6";
			// 
			// pictureBox8
			// 
			this.pictureBox8.Name = "pictureBox8";
			// 
			// editor1
			// 
			this.textbox.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
				| System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.textbox.Location = new System.Drawing.Point(9, 29);
			this.textbox.Name = "textbox";
			this.textbox.Multiline = true;
			this.textbox.Size = new System.Drawing.Size(305, 108);
			this.textbox.TabIndex = 7;
			// 
			// HtmlItem
			// 
			this.Controls.Add(this.textbox);
			this.Name = "TextItem";
			this.Controls.SetChildIndex(this.pictureBox5, 0);
			this.Controls.SetChildIndex(this.pictureBox1, 0);
			this.Controls.SetChildIndex(this.pictureBox6, 0);
			this.Controls.SetChildIndex(this.pictureBox3, 0);
			this.Controls.SetChildIndex(this.pictureBox2, 0);
			this.Controls.SetChildIndex(this.pictureBox8, 0);
			this.Controls.SetChildIndex(this.textbox, 0);
			this.ResumeLayout(false);

		}

		#endregion

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			base.OnDragEnter(drgevent);
			drgevent.Effect = DragDropEffects.None;
		}

		private byte[] GetHash()
		{
			string s = textbox.Text;
			if (s == null)
			{
				return new byte[] {};
			}
			else
			{
				MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
				return provider.ComputeHash(Encoding.UTF8.GetBytes(s));
			}
		}

		public void SetNoChanges()
		{
			initialHash = GetHash();
		}

		public bool HasChanges()
		{
			byte[] currentHash = GetHash();

			return ! CompareHashes(currentHash, initialHash);
		}

		//TODO: Refactor this duplicate of package.CompareHashes into a shared location
		private bool CompareHashes(byte[] h1, byte[] h2)
		{
			int i = 0;
			if (h1.Length != h2.Length)
			{
				return false;
			}

			while ((i < h1.Length) && h1[i] == h2[i])
			{
				i++;
			}
			if (i == h1.Length)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
		{
			isResizing = true;
			//SuspendLayout();
			HideShadows();
			textbox.Visible = false;

		}

		private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				ParentItemList.ResetUI();
				ParentItemList.ScrollControlIntoView(this);
				ShowShadows();
			}
			isResizing = false;
			textbox.Visible = true;

			//ResumeLayout();
		}

		private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				Point screenMousePoint = pictureBox2.PointToScreen(new Point(e.X, e.Y));
				Point screenTopPoint = ParentItemList.PointToScreen(new Point(Left, Top));

				Form f = FindForm();

				Point topLeft = f.PointToClient(screenTopPoint);
				Point bottomRight = f.PointToClient(screenMousePoint);

				if (screenMousePoint.Y > screenTopPoint.Y + 50)
				{
					this.Height = screenMousePoint.Y - screenTopPoint.Y;
					Invalidate();
				}


			}
		}


		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			if (this.FindForm() is PageDesigner)
			{
				((PageDesigner) (this.FindForm())).ActiveItem = this;
			}

		}

	}
}
#endif