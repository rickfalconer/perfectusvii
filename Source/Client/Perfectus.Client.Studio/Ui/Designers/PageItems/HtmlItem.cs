using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using mshtml;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	public class HtmlItem : Item
	{
        private WebBrowser editor1;
		private bool isResizing = false;
		private IContainer components = null;
		private byte[] initialHash;
	    private bool haveSetEditMode = false;
	    private bool havePainted = false;
		private string pendingBodyValue;

		public string XmlBody
		{
			get 
			{
                if (havePainted) // If the control hasn't been eyeballed, it will not have a document. So return the unmodified original value instead.
                {
                    string originalHtml = editor1.Document.Body.InnerHtml;
                    string retVal = StringBindingMod.GetXmlFromDoc((HTMLDocument) (editor1.Document.DomDocument), false);
                    // do restore.
                    editor1.Document.Body.InnerHtml = originalHtml;
                    return retVal;
                }
                else
                {
                    return pendingBodyValue;
                }
			}
		}

		public HtmlItem()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			imageIndex = 1;
            
		}

		protected override string name
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.HtmlTextName");
			}
		}

        //FB958
        public HtmlItem(string htmlBody)
            : this(htmlBody, null) { }
		public HtmlItem(string htmlBody, Perfectus.Common.PackageObjects.Package parentPackage ) : this()
		{
            this.PackageBeingEdited = parentPackage;
			pendingBodyValue = htmlBody;
		    initialHash = GetHash();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (editor1 != null)
				{
					editor1.Dispose();
				}
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        private void InitializeComponent()
		{
		    this.editor1 = new WebBrowser();

		    this.SuspendLayout();
		    // 
		    // pictureBox1
		    // 
		    this.pictureBox1.Name = "pictureBox1";
		    // 
		    // pictureBox2
		    // 
		    this.pictureBox2.Cursor = System.Windows.Forms.Cursors.SizeNS;
		    this.pictureBox2.Name = "pictureBox2";
		    this.pictureBox2.Size = new System.Drawing.Size(301, 7);
		    this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
		    this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
		    this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
		    // 
		    // pictureBox3
		    // 
		    this.pictureBox3.Name = "pictureBox3";
		    // 
		    // pictureBox5
		    // 
		    this.pictureBox5.Name = "pictureBox5";
		    this.pictureBox5.Size = new System.Drawing.Size(6, 123);
		    // 
		    // pictureBox6
		    // 
		    this.pictureBox6.Name = "pictureBox6";
		    // 
		    // pictureBox8
		    // 
		    this.pictureBox8.Name = "pictureBox8";
		    // 
		    // editor1
		    // 
		    this.editor1.Anchor =
		        ((System.Windows.Forms.AnchorStyles)
		         ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
		            | System.Windows.Forms.AnchorStyles.Left)
		           | System.Windows.Forms.AnchorStyles.Right)));
		    this.editor1.Location = new System.Drawing.Point(9, 29);
		    this.editor1.Name = "editor1";
		    this.editor1.Size = new System.Drawing.Size(305, 108);
		    this.editor1.TabIndex = 7;
		    this.editor1.DocumentText = String.Empty;
		    editor1.Document.ExecCommand("EditMode", true, null);
		    haveSetEditMode = true;
		    this.Controls.Add(this.editor1);
		    // 
		    // HtmlItem
		    // 
		    this.Paint += new PaintEventHandler(HtmlItem_Paint);
		    this.Controls.SetChildIndex(this.pictureBox5, 0);
		    this.Controls.SetChildIndex(this.pictureBox1, 0);
		    this.Controls.SetChildIndex(this.pictureBox6, 0);
		    this.Controls.SetChildIndex(this.pictureBox3, 0);
		    this.Controls.SetChildIndex(this.pictureBox2, 0);
		    this.Controls.SetChildIndex(this.pictureBox8, 0);
		    this.Controls.SetChildIndex(this.editor1, 0);
		    this.ResumeLayout(false);
		}

	    #endregion

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			base.OnDragEnter(drgevent);
			drgevent.Effect = DragDropEffects.None;
		}


		public void SetBold()
		{
			editor1.Document.ExecCommand("bold", false, null);
		}

		public void SetItalic()
		{
			editor1.Document.ExecCommand("italic", false, null);
		}

		public void SetUnderline()
		{
			editor1.Document.ExecCommand("underline", false, null);
		}

		public void SetStyle(string className)
		{
		    if (editor1.Document != null)
		    {
                IHTMLDocument2 doc2 = (IHTMLDocument2) editor1.Document.DomDocument;
                if (doc2.selection.type == "Text")
                {
                    IHTMLTxtRange t = (IHTMLTxtRange)doc2.selection.createRange();
                    t.pasteHTML(string.Format("&nbsp;<span class='{0}'>{1}</span>&nbsp;", className, t.text));
                }
		        
		    }
		}

        void HtmlItem_Paint(object sender, PaintEventArgs e)
        {
            if (havePainted)
            {
                return;
            }
            if (!haveSetEditMode)
            {
                return;
            }
            IHTMLDocument2 doc2 = (IHTMLDocument2)editor1.Document.DomDocument;

            int retryCount = 0;

            while (editor1.ReadyState != WebBrowserReadyState.Complete)
            {
                Thread.Sleep(100);
                ++retryCount;

                // If we're in an infinite loop, give up - the next paint may be better for the activex
                if (retryCount > 2)
                {
                    havePainted = false;
                    return;
                }
            }

            if (pendingBodyValue != null)
            {
                editor1.Document.Body.InnerHtml = StringBindingMod.GetDocHtmlFromXmlForHTMLItem(pendingBodyValue, PackageBeingEdited);
            }
            initialHash = GetHash();


            if (doc2.styleSheets.length == 0)
            {
                string cssAddress = string.Format(@"file://{0}\app.css", Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                doc2.createStyleSheet(cssAddress, 0);
            }
            havePainted = true;
        }

	    
		private byte[] GetHash()
		{
		    string s = null;
            if (havePainted)
            {
                s = editor1.Document.Body.InnerHtml;
            }
            else if (pendingBodyValue != null)
            {
                s = StringBindingMod.GetDocHtmlFromXmlForHTMLItem(pendingBodyValue, PackageBeingEdited);
            }
		    if (s == null)
			{
				return new byte[] {};
			}
			else
			{
				MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
				return provider.ComputeHash(Encoding.UTF8.GetBytes(s));
			}
		}

		public void SetNoChanges()
		{
			initialHash = GetHash();
		}

		public bool HasChanges()
		{
			byte[] currentHash = GetHash();

			return ! CompareHashes(currentHash, initialHash);
		}

		//TODO: Refactor this duplicate of package.CompareHashes into a shared location
		private static bool CompareHashes(byte[] h1, byte[] h2)
		{
			int i = 0;
			if (h1.Length != h2.Length)
			{
				return false;
			}

			while ((i < h1.Length) && h1[i] == h2[i])
			{
				i++;
			}
			if (i == h1.Length)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
		{
			isResizing = true;
			editor1.Visible = false;

		}

		private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				ParentItemList.ResetUI();
				ParentItemList.ScrollControlIntoView(this);
			}
			isResizing = false;
			editor1.Visible = true;
		}

		private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				Point screenMousePoint = pictureBox2.PointToScreen(new Point(e.X, e.Y));
				Point screenTopPoint = ParentItemList.PointToScreen(new Point(Left, Top));

				if (screenMousePoint.Y > screenTopPoint.Y + 50)
				{
					Height = screenMousePoint.Y - screenTopPoint.Y;
					Invalidate();
				}


			}
		}		

		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			if (FindForm() is PageDesigner)
			{
				((PageDesigner) (FindForm())).ActiveItem = this;
			}
		}
	}
}
