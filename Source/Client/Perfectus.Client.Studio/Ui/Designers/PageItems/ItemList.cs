using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
    
	/// <summary>
	/// Summary description for Page.
	/// </summary>
	public class ItemList : UserControl
	{
        private Item adjacentItem = null;
        private Item head = null;
		private Item tail = null;
		private int length = 0;

		public int Length
		{
			get { return length; }
		}

		public Item Head
		{
			get { return head; }
			set { head = value; }
		}

		public Item Tail
		{
			get { return tail; }
			set { tail = value; }
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private PageDesigner topPage;

		public ItemList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			this.AutoScroll = true;

		}

		public void RemoveHead()
		{
			if (head == null && tail == null)
			{
				return;
			}

			Item oldHead = head;
			oldHead.ParentItemList = null;
			head = head.Next;
			if (oldHead == tail)
			{
				tail = null;
			}
			oldHead.Next = null;
			if (head != null)
			{
				head.Prev = null;
			}
			length --;

			RemoveFromPageDictionary(oldHead);
		}

		public void RemoveTail()
        {
			if (head == null && tail == null)
			{
				return;
			}
			Item oldTail = tail;
			oldTail.ParentItemList = null;
			tail = tail.Prev;
			if (oldTail == head)
			{
				head = null;
			}
			oldTail.Prev = null;
			if (tail != null)
			{
                adjacentItem = tail;
				tail.Next = null;
			}
			length --;

			RemoveFromPageDictionary(oldTail);
		}


		public void Prepend(Item i)
		{
			if (OkayToAdd(i))
			{
				if (head == null)
				{
					head = i;
					tail = i;
					i.Prev = null;
					i.Next = null;
				}
				else
				{
					head.Prev = i;
					i.Prev = null;
					i.Next = head;
					head = i;
				}
				length ++;
				i.ParentItemList = this;
				AddItemToPageDictionary(i);
			}
		}

		public void Append(Item i)
		{
			if (OkayToAdd(i))
			{
				if (tail == null)
				{
					head = i;
					tail = i;
					i.Prev = null;
					i.Next = null;
				}
				else
				{
					tail.Next = i;
					i.Prev = tail;
					i.Next = null;
					tail = i;
				}
				length ++;
				i.ParentItemList = this;
				AddItemToPageDictionary(i);
			}
		}

		public void InsertBefore(Item newItem, Item beforeItem)
		{
			if (beforeItem == head)
			{
				Prepend(newItem);
			}
			else
			{
				if (OkayToAdd(newItem))
				{
					newItem.Prev = beforeItem.Prev;
					newItem.Next = beforeItem;
					newItem.Prev.Next = newItem;
					beforeItem.Prev = newItem;
					newItem.ParentItemList = this;
					length ++;
					AddItemToPageDictionary(newItem);
				}
			}
		}

		public void InsertAfter(Item newItem, Item afterItem)
		{
			if (afterItem == tail)
			{
				Append(newItem);
			}
			else
			{
				if (OkayToAdd(newItem))
				{
					newItem.Next = afterItem.Next;
					newItem.Prev = afterItem;
					afterItem.Next = newItem;
					newItem.Next.Prev = newItem;
					newItem.ParentItemList = this;
					length ++;
					AddItemToPageDictionary(newItem);
				}
			}
		}

		public void Remove(Item i)
		{
			if (i == head || i == tail)
			{
				if (i == head)
				{
					RemoveHead();
				}
				if (i == tail)
				{
					RemoveTail();
				}
			}
			else
			{
                adjacentItem = i.Next;
				i.Prev.Next = i.Next;
				i.Next.Prev = i.Prev;
				length --;
				i.ParentItemList = null;
				RemoveFromPageDictionary(i);
			}
		}

		public virtual void ResetUI()
		{
			ResetUI(this);
		}
		protected virtual void ResetUI(Control c)
		{
			c.SuspendLayout();
//			c.Controls.Clear();
			Hashtable controlsToRemove = new Hashtable();
			foreach (Control cc in c.Controls)
			{
				if (c is Item)
				{
					controlsToRemove.Add(cc, null);
				}
			}

			foreach (object o in controlsToRemove.Keys)
			{
				c.Controls.Remove((Control) o);
			}

			Item i = head;
			if (head != null)
			{
				do
				{
					c.Controls.Add(i);
					Position(i, i.Prev);
					i = i.Next;
				} while (i != null);            
            
            }

            c.ResumeLayout();
            if (adjacentItem != null)
            {
                adjacentItem.Focus();
            }
		}

		protected const int leftMargin = 20;
		protected const int topMargin = 20;
		protected const int vPadding = 10;
		protected const int hPadding = 10;

		protected virtual void Position(Item item, Item prevItem)
		{
			item.Left = leftMargin;
			if (prevItem == null)
			{
				item.Top = topMargin;
			}
			else
			{
				item.Top = prevItem.Bottom + vPadding;
			}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Page
			// 
			this.AllowDrop = true;
			this.BackColor = System.Drawing.Color.White;
			this.Name = "Page";
			this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Page_DragEnter);
			this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Page_DragDrop);

		}

		#endregion

		private void Page_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.All;

		}

		private void Page_DragDrop(object sender, DragEventArgs e)
		{
			IDataObject o = e.Data;
			bool isNewItem = false;
			bool isFolder = false;
			Folder folder = null;
			ArrayList items = new ArrayList();

			Item i = Item.ItemFromDataObject(o, out isNewItem,out isFolder,out folder);

			if(isFolder && folder != null)
			{
				if(folder is QuestionFolder)
				{
					foreach(Folder f in folder.GetAllFolders()) // Get all folders gets current folder and sub folders into one collection
					{
						foreach(Question q in folder.ParentPackage.Questions)
						{
							if(q.ParentFolder == f)
							{
								i = new QuestionItem(q);
								items.Add(i);
							}
						}						
					}
				}
			}
			else
			{
				items.Add(i);
			}

			for(int j=0;j<items.Count;j++)
			{
				i = (Item) items[j];

				if ((i != this) && (i != null) && (i.ParentItemList != this) && !(this is HorizontalGroup && i is HorizontalGroup))
				{
					ItemList oldParent = i.ParentItemList;
					i.RemoveFromParentList();
					if (oldParent != null)
					{
						oldParent.ResetUI();
					}
					Append(i);
					if (i.ParentItemList != null)
					{
						i.ParentItemList.ResetUI();
					}
					//ResetUI();
				}
			}

			ScrollControlIntoView(i);
			i.Select();

		}

		public bool OkayToAdd(Item i)
		{
			if (topPage == null)
			{
				FindPage();
			}

			if (topPage != null)
			{
				if (i is QuestionItem)
				{
					PageDesigner pageForm = this.FindForm() as PageDesigner;
					Question q = ((QuestionItem) i).Question;

					if (pageForm.QuestionsOnPage.Contains(q.UniqueIdentifier))
					{
						string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.DuplicateQuestionError");
						msg = msg.Replace("\\n", "\n");
						MessageBox.Show(msg, Common.About.FormsTitle);
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}

		private void AddItemToPageDictionary(Item i)
		{
			if (topPage == null)
			{
				FindPage();
			}
			if (topPage != null)
			{
				topPage.HasChanges = true;
				if (i is QuestionItem)
				{
					Question q = ((QuestionItem) i).Question;
					if (!topPage.QuestionsOnPage.ContainsKey(q.UniqueIdentifier))
					{
						topPage.QuestionsOnPage.Add(q.UniqueIdentifier, q);
						topPage.DoRefreshQuestionList();
					}
				}
			}
		}

		public void RemoveFromPageDictionary(Item i)
		{
			if (topPage == null)
			{
				FindPage();
			}
			if (topPage != null)
			{
				topPage.HasChanges = true;
				if (i is QuestionItem)
				{
					Question q = ((QuestionItem) i).Question;
					if (topPage.QuestionsOnPage.ContainsKey(q.UniqueIdentifier))
					{
						topPage.QuestionsOnPage.Remove(q.UniqueIdentifier);
						topPage.DoRefreshQuestionList();
					}
				}
			}
		}

		private void FindPage()
		{
			Form f = FindForm();
			if (f != null && f is PageDesigner)
			{
				topPage = (PageDesigner) f;
			}
		}

	}
}