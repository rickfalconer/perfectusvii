using System.Drawing;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	/// <summary>
	/// Summary description for Page.
	/// </summary>
	public class Page : ItemList
	{
		public Page() : base()
		{
			this.AutoScroll = true;
			vTop = new Label();
			vTop.Height = 1;
			vTop.Width = 1;
			vTop.Top = 0;
			vTop.Left = 0;
			vTop.BackColor = Color.Pink;
			Controls.Add(vTop);
			InitializeComponent();

		}

		private Control activeControl;
		private Control lastActiveControl;
		private Label vTop;

		new public Control ActiveControl
		{
			get { return activeControl; }
			set
			{
				if (activeControl != null)
				{
					lastActiveControl = activeControl;
				}
				activeControl = value;
				base.ActiveControl = value;
			}
		}

		public Control LastActiveControl
		{
			get { return lastActiveControl; }
		}


		protected override void Position(Item item, Item prevItem)
		{
			item.Left = leftMargin;
			if (prevItem == null)
			{
				ScrollControlIntoView(vTop);
				
				item.Top = topMargin;
			}
			else
			{
				item.Top = prevItem.Bottom;
			}

			item.Width = this.Width - (leftMargin*2);
			item.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
		}

		private void InitializeComponent()
		{
			// 
			// Page
			// 
			this.Cursor = Cursors.Default;
			this.Name = "Page";

		}

		public override void ResetUI()
		{
			base.ResetUI();
			Refresh();
		}

	}
}