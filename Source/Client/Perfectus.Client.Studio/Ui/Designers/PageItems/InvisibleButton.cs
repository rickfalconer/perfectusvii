using System.Drawing;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	/// <summary>
	/// Summary description for InvisibleButton.
	/// </summary>
	public class InvisibleButton : Button
	{
		public InvisibleButton() : base()
		{
		}

		protected override void OnPaint(PaintEventArgs e)
		{
//			base.OnPaint (e);
			e.Graphics.Clear(Color.White);
		}


	}
}