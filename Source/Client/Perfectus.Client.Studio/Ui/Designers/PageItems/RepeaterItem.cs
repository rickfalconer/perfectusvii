using Perfectus.Common;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using System.Collections;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	public class RepeaterItem : HorizontalGroup
	{
		private IContainer components = null;
		private IntQuestionValue maxRows = new IntQuestionValue();

		public RepeaterItem()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			imageIndex = 3;

		}

		[Category("PageItem")]
		[Browsable(true)]
		[Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.IntQuestionEditor, Studio", typeof (UITypeEditor))]
		[TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.IntQuestionConverter, Studio")]	
		public IntQuestionValue MaxRows
		{
			get { return maxRows;  }
			set { maxRows = value; }
		}

		protected override string name
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.RepeaterItemName");
			}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}

		#endregion
	}
}