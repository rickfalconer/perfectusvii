#if ADOBE
using System;
using System.ComponentModel;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	public class ImageItem : Item
	{
		private bool hasChanges = false;
		private bool isResizing = false;
		private IContainer components = null;
		private PictureBox pbxImage;

		[Browsable(true)]
		[Category("PageItem")]
		public Image Image
		{
			get { return pbxImage.Image; }
			set 
			{ 
				pbxImage.Image = value; 
				hasChanges = true;
				if (value != null)
				{
					this.Height = pbxImage.Image.Height + 50;
					ParentItemList.ResetUI();
				}
			}
		}

		public ImageItem()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			imageIndex = 4;

			SetNoChanges();


		}

		protected override string name
		{
			get
			{
				return ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.ImageName");			}
		}


		public ImageItem(Image imageBody) : this()
		{
			pbxImage.Image = imageBody;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pbxImage = new PictureBox();
			
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox1.Name = "pictureBox1";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Cursor = System.Windows.Forms.Cursors.SizeNS;
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(301, 7);
			this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
			this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
			this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
			// 
			// pictureBox3
			// 
			this.pictureBox3.Name = "pictureBox3";
			// 
			// pictureBox5
			// 
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new System.Drawing.Size(6, 123);
			// 
			// pictureBox6
			// 
			this.pictureBox6.Name = "pictureBox6";
			// 
			// pictureBox8
			// 
			this.pictureBox8.Name = "pictureBox8";
			// 
			// editor1
			// 
			this.pbxImage.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
				| System.Windows.Forms.AnchorStyles.Left)
				| System.Windows.Forms.AnchorStyles.Right)));
			this.pbxImage.Location = new System.Drawing.Point(9, 29);
			this.pbxImage.Size = new System.Drawing.Size(305, 108);
			this.pbxImage.SizeMode = PictureBoxSizeMode.CenterImage;
			// 
			// HtmlItem
			// 
			this.Controls.Add(this.pbxImage);
			this.Name = "TextItem";
			this.Controls.SetChildIndex(this.pictureBox5, 0);
			this.Controls.SetChildIndex(this.pictureBox1, 0);
			this.Controls.SetChildIndex(this.pictureBox6, 0);
			this.Controls.SetChildIndex(this.pictureBox3, 0);
			this.Controls.SetChildIndex(this.pictureBox2, 0);
			this.Controls.SetChildIndex(this.pictureBox8, 0);
			this.Controls.SetChildIndex(this.pbxImage, 0);
			this.ResumeLayout(false);

		}

		#endregion

		protected override void OnDragEnter(DragEventArgs drgevent)
		{
			base.OnDragEnter(drgevent);
			drgevent.Effect = DragDropEffects.None;
		}


		public void SetNoChanges()
		{
			hasChanges = false;
		}

		public bool HasChanges()
		{
		

			return hasChanges;
		}


		private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
		{
			isResizing = true;
			//SuspendLayout();
			HideShadows();
//			textbox.Visible = false;

		}

		private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				ParentItemList.ResetUI();
				ParentItemList.ScrollControlIntoView(this);
				ShowShadows();
			}
			isResizing = false;
//			textbox.Visible = true;

			//ResumeLayout();
		}

		private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
		{
			if (isResizing)
			{
				Point screenMousePoint = pictureBox2.PointToScreen(new Point(e.X, e.Y));
				Point screenTopPoint = ParentItemList.PointToScreen(new Point(Left, Top));

				Form f = FindForm();

				Point topLeft = f.PointToClient(screenTopPoint);
				Point bottomRight = f.PointToClient(screenMousePoint);

				if (screenMousePoint.Y > screenTopPoint.Y + 50)
				{
					this.Height = screenMousePoint.Y - screenTopPoint.Y;
					Invalidate();
				}


			}
		}


		protected override void OnParentChanged(EventArgs e)
		{
			base.OnParentChanged(e);
			if (this.FindForm() is PageDesigner)
			{
				((PageDesigner) (this.FindForm())).ActiveItem = this;
			}

		}

	}
}
#endif