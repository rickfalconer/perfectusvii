using System.ComponentModel;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
	/// <summary>
	/// Summary description for Images.
	/// </summary>
	public class Images : Component
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		internal PictureBox pbxCalendar;
		private static Images instance = null;

		public static Images GetInstance()
		{
			if (instance == null)
			{
				instance = new Images();
			}

			return instance;
		}

		public Images(IContainer container)
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			container.Add(this);
			InitializeComponent();

			//
			//TODO: Add any constructor code after InitializeComponent call
			//
		}

		public Images()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			//
			//TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof (Images));
			this.pbxCalendar = new System.Windows.Forms.PictureBox();
			// 
			// pbxCalendar
			// 
			this.pbxCalendar.Image = ((System.Drawing.Image) (resources.GetObject("pbxCalendar.Image")));
			this.pbxCalendar.Location = new System.Drawing.Point(17, 17);
			this.pbxCalendar.Name = "pbxCalendar";
			this.pbxCalendar.TabIndex = 0;
			this.pbxCalendar.TabStop = false;

		}

		#endregion
	}
}