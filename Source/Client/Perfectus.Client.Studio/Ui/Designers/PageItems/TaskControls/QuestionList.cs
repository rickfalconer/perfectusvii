using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls
{
	/// <summary>
	/// Summary description for QuestionList.
	/// </summary>
	public class QuestionList : UserControl
	{
		private Package package;
		private UiListItemDictionary itemMap;


		private PictureBox pbxQuestion;
		private IContainer components = null;

		private Interview interviewBeingEdited;
		private PictureBox pbxQuestionRed;
		private PictureBox pbxQuestionGrey;
		private QuestionDictionary questionsOnPage;
        private ImageList imageList1;
        private ListView uiListBox1;
        private ColumnHeader columnHeader1;
		private InterviewPage page;

		public InterviewPage Page
		{
			get { return page; }
			set { page = value; }
		}

		public QuestionDictionary QuestionsOnPage
		{
			get { return questionsOnPage; }
			set { questionsOnPage = value; }
		}


		public Interview InterviewBeingEdited
		{
			get { return interviewBeingEdited; }
			set { interviewBeingEdited = value; }
		}

		public void RefreshIcons()
		{
			foreach (ListViewItem uili in uiListBox1.Items)
			{
				object o = uili.Tag;
				if (o != null && o is Question)
				{
					Question q = (Question) o;
					SetListItemImage(q, uili);
				}
			}

			uiListBox1.Refresh();
		}

		public QuestionList()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			itemMap = new UiListItemDictionary();

			uiListBox1.MouseDown += new MouseEventHandler(uiListBox1_MouseDown);
            
            imageList1.Images.Add("0", pbxQuestion.Image);
            imageList1.Images.Add("1", pbxQuestionRed.Image);
            imageList1.Images.Add("2", pbxQuestionGrey.Image);
            uiListBox1.SmallImageList = imageList1;
		}

		public QuestionList(QuestionDictionary questionsOnPage, Interview interviewBeingEdited) : this()
		{
			this.questionsOnPage = questionsOnPage;
			this.interviewBeingEdited = interviewBeingEdited;
		}


		public Package Package
		{
			get { return package; }
			set
			{
				if (package != null)
				{
					package.ItemCreated -= new EventHandler<PackageItemEventArgs>(p_ItemCreated);
					package.ItemDeleted -= new EventHandler<PackageItemDeletedEventArgs>(p_ItemDeleted);
				}
				package = value;
				if (package != null)
				{
					package.ItemCreated += new EventHandler<PackageItemEventArgs>(p_ItemCreated);
					package.ItemDeleted += new EventHandler<PackageItemDeletedEventArgs>(p_ItemDeleted);
					ResetUI();
				}
			}
		}

		private void ResetUI()
		{
			uiListBox1.SuspendLayout();
			foreach (Question q in package.Questions)
			{
				AddQuestionToList(q);
			}
            uiListBox1.Sorting = SortOrder.Ascending;
            uiListBox1.Sort();
			uiListBox1.ResumeLayout();
		}

		private void AddQuestionToList(Question q)
		{
            string _disableQuickQuestion = System.Configuration.ConfigurationSettings.AppSettings["disableQuickQuestion"];

            try
            {
                if ((_disableQuickQuestion != null) && (Boolean.Parse(_disableQuickQuestion) == true))
                    return;
            }
            catch (Exception) { }

			ListViewItem listItem = new ListViewItem(q.Name, 0);
            listItem.Tag = q;
			SetListItemImage(q, listItem);
			uiListBox1.Items.Add(listItem);
			q.PropertyChanged += new PropertyChangedEventHandler(q_PropertyChanged);
			itemMap.Add(q.UniqueIdentifier, listItem);
		}

		private void SetListItemImage(Question q, ListViewItem listItem)
		{
			if (questionsOnPage != null && questionsOnPage.Contains(q.UniqueIdentifier))
			{
                listItem.ImageKey = "2";
			}
			else
			{
				bool isInInterview = false;
				if (interviewBeingEdited != null && interviewBeingEdited.Pages != null)
				{
					foreach (InterviewPage pg in interviewBeingEdited.Pages)
					{
						if (pg != page && pg is InternalPage && ((InternalPage)pg).ContainsReferenceTo(q))
						{
                            listItem.ImageKey = "0";
							isInInterview = true;
							break;
						}
					}
					if (!isInInterview)
					{
                        listItem.ImageKey = "1";
					}
				}
				else
				{
                    listItem.ImageKey = "0";
				}
			}
		}


		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionList));
            this.pbxQuestion = new System.Windows.Forms.PictureBox();
            this.pbxQuestionRed = new System.Windows.Forms.PictureBox();
            this.pbxQuestionGrey = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestionRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestionGrey)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxQuestion
            // 
            this.pbxQuestion.BackColor = System.Drawing.Color.White;
            this.pbxQuestion.Image = ((System.Drawing.Image)(resources.GetObject("pbxQuestion.Image")));
            this.pbxQuestion.Location = new System.Drawing.Point(57, 94);
            this.pbxQuestion.Name = "pbxQuestion";
            this.pbxQuestion.Size = new System.Drawing.Size(16, 16);
            this.pbxQuestion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxQuestion.TabIndex = 1;
            this.pbxQuestion.TabStop = false;
            this.pbxQuestion.Visible = false;
            // 
            // pbxQuestionRed
            // 
            this.pbxQuestionRed.BackColor = System.Drawing.Color.White;
            this.pbxQuestionRed.Image = ((System.Drawing.Image)(resources.GetObject("pbxQuestionRed.Image")));
            this.pbxQuestionRed.Location = new System.Drawing.Point(48, 32);
            this.pbxQuestionRed.Name = "pbxQuestionRed";
            this.pbxQuestionRed.Size = new System.Drawing.Size(16, 16);
            this.pbxQuestionRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxQuestionRed.TabIndex = 2;
            this.pbxQuestionRed.TabStop = false;
            this.pbxQuestionRed.Visible = false;
            // 
            // pbxQuestionGrey
            // 
            this.pbxQuestionGrey.BackColor = System.Drawing.Color.White;
            this.pbxQuestionGrey.Image = ((System.Drawing.Image)(resources.GetObject("pbxQuestionGrey.Image")));
            this.pbxQuestionGrey.Location = new System.Drawing.Point(100, 69);
            this.pbxQuestionGrey.Name = "pbxQuestionGrey";
            this.pbxQuestionGrey.Size = new System.Drawing.Size(16, 16);
            this.pbxQuestionGrey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbxQuestionGrey.TabIndex = 3;
            this.pbxQuestionGrey.TabStop = false;
            this.pbxQuestionGrey.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 0);
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(150, 150);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.TabIndex = 4;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 440;
            // 
            // QuestionList
            // 
            this.Controls.Add(this.uiListBox1);
            this.Controls.Add(this.pbxQuestionGrey);
            this.Controls.Add(this.pbxQuestionRed);
            this.Controls.Add(this.pbxQuestion);
            this.Name = "QuestionList";
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestionRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxQuestionGrey)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private void p_ItemCreated(object sender, PackageItemEventArgs e)
		{
			if (e.Item is Question && !(itemMap.ContainsKey(e.Item.UniqueIdentifier)))
			{
				AddQuestionToList(e.Item as Question);
                uiListBox1.Sorting = SortOrder.Ascending;
                uiListBox1.Sort();
				uiListBox1.Invalidate();
			}
		}

		private void p_ItemDeleted(object sender, PackageItemDeletedEventArgs e)
		{
			if (itemMap.ContainsKey(e.Id))
			{
				ListViewItem i = itemMap[e.Id];
				uiListBox1.Items.Remove(i);
				itemMap.Remove(e.Id);
				uiListBox1.Refresh();
			}
		}

		private void q_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			Question q = sender as Question;
			if (q != null)
			{
				switch (e.PropertyName)
				{
					case "Name":
						if(itemMap.ContainsKey(q.UniqueIdentifier))
						{
							itemMap[q.UniqueIdentifier].Text = q.Name;
                            uiListBox1.Sorting = SortOrder.Ascending;
                            uiListBox1.Sort();
                            uiListBox1.Refresh();
						}
						break;
				}
			}
		}

        private void uiListBox1_MouseDown(object sender, MouseEventArgs e)
        {
            DataObject obj = new DataObject();

            ListViewItem i = uiListBox1.FocusedItem;

            if (i != null)
            {
                obj.SetData("Perfectus.Common.Package.PackageItem", i.Tag as Question);
                StringBinding.AddHTMLTagToDataObject(obj, i.Tag as Question, null);
                DoDragDrop(obj, DragDropEffects.All);
            }
        }
	}
}