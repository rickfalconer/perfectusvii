using System;
using System.Collections;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type UiListItem
	/// </summary>
	public class UiListItemDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the UiListItemDictionary class
		/// </summary>
		public UiListItemDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the UiListItem associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual ListViewItem this[Guid key]
		{
            get { return (ListViewItem)this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this UiListItemDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The UiListItem value of the element to add.
		/// </param>
		public virtual void Add(Guid key, ListViewItem value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this UiListItemDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this UiListItemDictionary.
		/// </param>
		/// <returns>
		/// true if this UiListItemDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this UiListItemDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this UiListItemDictionary.
		/// </param>
		/// <returns>
		/// true if this UiListItemDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this UiListItemDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The UiListItem value to locate in this UiListItemDictionary.
		/// </param>
		/// <returns>
		/// true if this UiListItemDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(ListViewItem value)
		{
			foreach (ListViewItem item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this UiListItemDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this UiListItemDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this UiListItemDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}