using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls
{
	/// <summary>
	/// Summary description for LayoutItems.
	/// </summary>
	public class LayoutItems : UserControl
    {
		private ImageList imageList1;
        private IContainer components;
        private ListView uiListBox1;
        private ColumnHeader colText;

		public Package PackageBeingEdited
		{
			get { return packageBeingEdited; }
			set
			{
				packageBeingEdited = value;
				ResetUI();
			}
		}

		private Package packageBeingEdited;

		public LayoutItems()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			//ResetUI();

		}

		private void ResetUI()
		{
			uiListBox1.Items.Clear();
            
            ListViewItem htmlListItem = new ListViewItem(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.TaskControls.LayoutItems.HTMLTextAreaName"), 0);
            htmlListItem.Tag = typeof(HtmlItem);

            ListViewItem repeaterListItem = new ListViewItem(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.TaskControls.LayoutItems.RepeaterName"), 2 );
            repeaterListItem.Tag = typeof (RepeaterItem);
            
            ListViewItem tableListItem = new ListViewItem(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.PageItems.TaskControls.LayoutItems.HorizontalLayoutName"), 1);
            tableListItem.Tag = typeof(HorizontalGroup);

            uiListBox1.Items.Add(htmlListItem);
			uiListBox1.Items.Add(repeaterListItem);
			uiListBox1.Items.Add(tableListItem);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayoutItems));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiListBox1 = new System.Windows.Forms.ListView();
            this.colText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            // 
            // uiListBox1
            // 
            this.uiListBox1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colText});
            this.uiListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiListBox1.Location = new System.Drawing.Point(0, 0);
            this.uiListBox1.MultiSelect = false;
            this.uiListBox1.Name = "uiListBox1";
            this.uiListBox1.Size = new System.Drawing.Size(240, 208);
            this.uiListBox1.SmallImageList = this.imageList1;
            this.uiListBox1.TabIndex = 0;
            this.uiListBox1.UseCompatibleStateImageBehavior = false;
            this.uiListBox1.View = System.Windows.Forms.View.SmallIcon;
            this.uiListBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.uiListBox1_MouseDown);
            // 
            // colText
            // 
            this.colText.Text = "Text";
            this.colText.Width = 440;
            // 
            // LayoutItems
            // 
            this.Controls.Add(this.uiListBox1);
            this.Name = "LayoutItems";
            this.Size = new System.Drawing.Size(240, 208);
            this.ResumeLayout(false);

		}

		#endregion

        private void uiListBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (uiListBox1.FocusedItem != null)
            {
                DataObject obj = new DataObject();

                ListViewItem i = uiListBox1.FocusedItem;
                if (i != null)
                {
                    Type t = i.Tag as Type;
                    if (t != null)
                    {
                        Item itemToDrop = (Item)Activator.CreateInstance(t);
                        itemToDrop.PackageBeingEdited = this.packageBeingEdited;
                        obj.SetData("Perfectus.Client.Studio.UI.Designers.PageItems.Item", itemToDrop);
                        DoDragDrop(obj, DragDropEffects.All);
                    }
                }
            }
        }
	}
}