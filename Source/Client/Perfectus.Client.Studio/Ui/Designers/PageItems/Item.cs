using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers.PageItems
{
    /// <summary>
    /// Summary description for UserControl1.
    /// </summary>
    public class Item : ItemList, ICustomTypeDescriptor
    {
        private bool deleting = false;
        private bool isSelected = false;
        private Item prev = null;
        private Item next = null;
        protected Panel panel1;
        protected Panel panel2;
        protected Panel panel3;
        protected Panel panel4;
        private ItemList parentItemList;
        private Panel pnlRightEdge;
        private Panel pnlLeftEdge;
        private Panel pnlBottomEdge;
        private Panel pnlTopEdge;
        protected internal PictureBox pictureBox1;
        protected PictureBox pictureBox2;
        protected internal PictureBox pictureBox3;
        private PictureBox pictureBox4;
        protected internal PictureBox pictureBox5;
        protected internal PictureBox pictureBox6;
        protected PictureBox pictureBox7;
        protected internal PictureBox pictureBox8;
        protected Color gradientStart = Color.FromArgb(200, 218, 239);
        protected Color gradientEnd = Color.FromArgb(230, 240, 250);

//		protected Color gradientEnd = Color.FromArgb(218, 239, 255);
//		protected Color gradientStart = Color.White;
        private PageDesigner frm;
        private Guid uniqueIdentifier = Guid.NewGuid();
        private YesNoQueryValue visible = new YesNoQueryValue();

        public Package PackageBeingEdited
        {
            get { return packageBeingEdited; }
            set { packageBeingEdited = value; }
        }

        private Package packageBeingEdited;
        private PictureBox pictureBox16;
        private PictureBox pictureBox17;
        private PictureBox pictureBox19;
        protected internal PictureBox pictureBox20;
        private PictureBox pictureBox14;
        protected internal PictureBox pictureBox15;
        private PictureBox pictureBox10;
        private PictureBox pictureBox9;
        private PictureBox pictureBox11;

        [Category("PageItem")]
        [Browsable(true)]
        [Editor("Perfectus.Client.Studio.UI.PackageObjectEditors.YesNoQueryEditor, Studio", typeof (UITypeEditor))]
        [TypeConverter("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.YesNoQueryConverter, Studio")]
        public new YesNoQueryValue Visible
        {
            get { return visible; }
            set
            {
                visible = value;
            }
        }

        public Guid UniqueIdentifier
        {
            get { return uniqueIdentifier; }
            set { uniqueIdentifier = value; }
        }


        protected virtual string name
        {
            get { return "Item - base"; }
        }

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public Item()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            visible.YesNoValue = true;

            frm = FindForm() as PageDesigner;
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.Selectable, true);
            SetStyle(ControlStyles.FixedHeight, true);
            SetStyle(ControlStyles.FixedWidth, true);

            pnlLeftEdge.BackColor = Color.White;
            pnlTopEdge.BackColor = Color.White;
            pnlBottomEdge.BackColor = Color.White;
            pnlRightEdge.BackColor = Color.White;

            SetPulled();
        }

        protected int imageIndex = -1;

        public ItemList ParentItemList
        {
            get { return parentItemList; }
            set { parentItemList = value; }
        }

        public Item Prev
        {
            get { return prev; }
            set { prev = value; }
        }


        public Item Next
        {
            get { return next; }
            set { next = value; }
        }


        protected bool IsInHorizontalZone()
        {
            return parentItemList is HorizontalGroup;
        }

        protected bool cancelResizeEvent = false;

        public override void ResetUI()
        {
            cancelResizeEvent = true;
            base.ResetUI();
            Width = parentItemList.Width - (leftMargin*2);
            cancelResizeEvent = false;
        }

        protected override void OnEnter(EventArgs e)
        {
            if (!deleting)
            {
                Debug.WriteLine(string.Format("OnEnter: {0}", GetType().Name));
                base.OnEnter(e);
                isSelected = true;
                SetPushed();
                Invalidate(false);

                if (FindForm() is PageDesigner)
                {
                    ((PageDesigner) (FindForm())).ActiveItem = this;
                }
            }
        }

        protected override void OnLeave(EventArgs e)
        {
            Debug.WriteLine(string.Format("OnLeave: {0}", GetType().Name));

            base.OnLeave(e);
            SetPulled();
            isSelected = false;
            Invalidate(false);
        }

        public void LookBlurred()
        {
            SetPulled();
            isSelected = false;
            Invalidate(false);
        }

        public void RemoveFromParentList()
        {
            if (parentItemList != null)
            {
                parentItemList.Remove(this);
            }

            parentItemList = null;
        }

        public void InsertBefore(Item i)
        {
            if (i != this)
            {
                RemoveFromParentList();

                if (i == i.ParentItemList.Head)
                {
                    i.ParentItemList.Prepend(this);
                }
                else
                {
                    i.parentItemList.InsertBefore(this, i);
                }
            }
        }

        public void InsertAfter(Item i)
        {
            if (i != this)
            {
                RemoveFromParentList();
                if (i == i.ParentItemList.Tail)
                {
                    i.ParentItemList.Append(this);
                }
                else
                {
                    i.ParentItemList.InsertAfter(this, i);
                }
            }
        }

        private void EdgePanel_DragDrop(object sender, DragEventArgs e)
        {
            ((Panel) sender).BackColor = Color.White;

            IDataObject o = e.Data;
            bool isNewItem = false;
            bool isFolder = false;
            Folder folder = null;
            ArrayList items = new ArrayList();

            Item itemToDrop = ItemFromDataObject(o, out isNewItem, out isFolder, out folder);

            if (isFolder && folder != null)
            {
                if (folder is QuestionFolder)
                {
                    foreach (Folder f in folder.GetAllFolders())
                        // GetAllFolders gets current folder and sub folders into one collection
                    {
                        foreach (Question q in folder.ParentPackage.Questions)
                        {
                            if (q.ParentFolder == f)
                            {
                                itemToDrop = new QuestionItem(q);
                                items.Add(itemToDrop);
                            }
                        }
                    }
                }
            }
            else if (itemToDrop != null)
            {
                items.Add(itemToDrop);
            }


            for (int j = 0; j < items.Count; j++)
            {
                itemToDrop = (Item) items[j];

                ItemList oldList = itemToDrop.ParentItemList;
                // Don't allow horizontal layout or repeaters to be placed beside us
                if ((sender == pnlLeftEdge || sender == pnlRightEdge) &&
                    (itemToDrop is HorizontalGroup || this is HorizontalGroup)
                    ||
                    ((sender == pnlTopEdge || sender == pnlBottomEdge) &&
                     (itemToDrop is HorizontalGroup || this is HorizontalGroup) &&
                     (parentItemList is HorizontalGroup || parentItemList is RepeaterItem)))
                {
                    return;
                }


                if (parentItemList != null && (!isNewItem || parentItemList.OkayToAdd(itemToDrop)))
                {
                    // If the item being dropped is a QuestionItem, and we are also a QuestionItem - and not already in a horizontal block - , make a  horizontal layout block, put ourself in it, then put the drop-item next to us.
                    if ((sender == pnlLeftEdge || sender == pnlRightEdge) && !(parentItemList is HorizontalGroup))
                    {
                        // Make the layout item.
                        HorizontalGroup grp = new HorizontalGroup();
                        grp.PackageBeingEdited = packageBeingEdited;

                        // Place it before the drop target (us).
                        grp.InsertBefore(this);

                        // Put ourself in the group
                        ItemList myOldList = parentItemList;
                        RemoveFromParentList();
                        if (myOldList != null)
                        {
                            myOldList.ResetUI();
                        }
                        grp.Prepend(this);
                    }

                    // Insert the item into the list, before or after us - depending on which panel was the target.
                    if (sender == pnlRightEdge || sender == pnlBottomEdge)
                    {
                        itemToDrop.InsertAfter(this);
                    }
                    else if (sender == pnlLeftEdge || sender == pnlTopEdge)
                    {
                        itemToDrop.InsertBefore(this);
                    }
                    if (oldList != null)
                    {
                        oldList.ResetUI();
                    }

                    if (itemToDrop.ParentItemList != oldList)
                    {
                        itemToDrop.ParentItemList.ResetUI();
                    }
                }

                itemToDrop.ParentItemList.ScrollControlIntoView(itemToDrop);
                itemToDrop.Select();
            }
        }

        private void EdgePanel_DragEnter(object sender, DragEventArgs e)
        {
            IDataObject o = e.Data;
            ((Panel) sender).BackColor = SystemColors.HotTrack;
            if (o.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                if (o.GetDataPresent("Perfectus.Client.Studio.UI.Designers.PageItems.Item"))
                {
                    e.Effect = DragDropEffects.Move;
                }
            }
        }


        private void EdgePanel_DragLeave(object sender, EventArgs e)
        {
            ((Panel) sender).BackColor = Color.White;
        }


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (parentItemList != null)
                {
                    parentItemList.RemoveFromPageDictionary(this);
                }
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof (Item));
            this.pnlRightEdge = new System.Windows.Forms.Panel();
            this.pnlLeftEdge = new System.Windows.Forms.Panel();
            this.pnlBottomEdge = new System.Windows.Forms.Panel();
            this.pnlTopEdge = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pnlRightEdge
            // 
            this.pnlRightEdge.AllowDrop = true;
            this.pnlRightEdge.BackColor = System.Drawing.Color.Transparent;
            this.pnlRightEdge.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRightEdge.Location = new System.Drawing.Point(320, 0);
            this.pnlRightEdge.Name = "pnlRightEdge";
            this.pnlRightEdge.Size = new System.Drawing.Size(8, 152);
            this.pnlRightEdge.TabIndex = 1;
            this.pnlRightEdge.DragEnter += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragEnter);
            this.pnlRightEdge.DragLeave += new System.EventHandler(this.EdgePanel_DragLeave);
            this.pnlRightEdge.DragDrop += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragDrop);
            // 
            // pnlLeftEdge
            // 
            this.pnlLeftEdge.AllowDrop = true;
            this.pnlLeftEdge.BackColor = System.Drawing.Color.Transparent;
            this.pnlLeftEdge.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeftEdge.Location = new System.Drawing.Point(0, 0);
            this.pnlLeftEdge.Name = "pnlLeftEdge";
            this.pnlLeftEdge.Size = new System.Drawing.Size(8, 152);
            this.pnlLeftEdge.TabIndex = 2;
            this.pnlLeftEdge.DragEnter += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragEnter);
            this.pnlLeftEdge.DragLeave += new System.EventHandler(this.EdgePanel_DragLeave);
            this.pnlLeftEdge.DragDrop += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragDrop);
            // 
            // pnlBottomEdge
            // 
            this.pnlBottomEdge.AllowDrop = true;
            this.pnlBottomEdge.BackColor = System.Drawing.Color.Transparent;
            this.pnlBottomEdge.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBottomEdge.Location = new System.Drawing.Point(8, 144);
            this.pnlBottomEdge.Name = "pnlBottomEdge";
            this.pnlBottomEdge.Size = new System.Drawing.Size(312, 8);
            this.pnlBottomEdge.TabIndex = 3;
            this.pnlBottomEdge.DragEnter += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragEnter);
            this.pnlBottomEdge.DragLeave += new System.EventHandler(this.EdgePanel_DragLeave);
            this.pnlBottomEdge.DragDrop += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragDrop);
            // 
            // pnlTopEdge
            // 
            this.pnlTopEdge.AllowDrop = true;
            this.pnlTopEdge.BackColor = System.Drawing.Color.Transparent;
            this.pnlTopEdge.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopEdge.Location = new System.Drawing.Point(8, 0);
            this.pnlTopEdge.Name = "pnlTopEdge";
            this.pnlTopEdge.Size = new System.Drawing.Size(312, 8);
            this.pnlTopEdge.TabIndex = 4;
            this.pnlTopEdge.DragEnter += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragEnter);
            this.pnlTopEdge.DragLeave += new System.EventHandler(this.EdgePanel_DragLeave);
            this.pnlTopEdge.DragDrop += new System.Windows.Forms.DragEventHandler(this.EdgePanel_DragDrop);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(8, 137);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(6, 7);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(14, 137);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(306, 7);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(314, 137);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(6, 7);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(314, 8);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(6, 6);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 8;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(314, 13);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(6, 323);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(8, 8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(18, 21);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox6.TabIndex = 11;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox7.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(24, 8);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(280, 21);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 12;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Visible = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox8.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(293, 8);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(21, 21);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox8.TabIndex = 13;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.pictureBox8_Click);
            this.pictureBox8.MouseEnter += new System.EventHandler(this.pictureBox8_MouseEnter);
            this.pictureBox8.MouseLeave += new System.EventHandler(this.pictureBox8_MouseLeave);
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(247, 78);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(21, 21);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox16.TabIndex = 34;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Visible = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox17.Image")));
            this.pictureBox17.Location = new System.Drawing.Point(271, 78);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(21, 21);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox17.TabIndex = 33;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Visible = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox19.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(15, 102);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(297, 21);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 32;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Visible = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(15, 78);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(18, 21);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox20.TabIndex = 31;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                 (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                   | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox14.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(15, 54);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(297, 21);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 30;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Visible = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(15, 30);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(18, 21);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox15.TabIndex = 29;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Visible = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.White;
            this.pictureBox10.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(247, 30);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(21, 21);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 28;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(271, 30);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(21, 21);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox9.TabIndex = 26;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Visible = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image) (resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(271, 30);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(21, 21);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox11.TabIndex = 27;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Visible = false;
            // 
            // Item
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlTopEdge);
            this.Controls.Add(this.pnlBottomEdge);
            this.Controls.Add(this.pnlLeftEdge);
            this.Controls.Add(this.pnlRightEdge);
            this.Controls.Add(this.pictureBox5);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "Item";
            this.Size = new System.Drawing.Size(328, 152);
            //this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Item_MouseDown);
            this.ResumeLayout(false);
        }

        #endregion

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            DataObject obj = new DataObject();

            obj.SetData("Perfectus.Client.Studio.UI.Designers.PageItems.Item", this);
            DoDragDrop(obj, DragDropEffects.Move);
            OnEnter(new EventArgs());
        }

        public static Item ItemFromDataObject(IDataObject dob, out bool isNewItem, out bool isFolder, out Folder folder)
        {
            Item itemToDrop = null;
            isNewItem = false;
            isFolder = false;
            folder = null;

            // Examine the data.  If the drop is a Question instance, generate a QuestionItem.
            // Otherwise, use the Item from the data itself.
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is Question)
                {
                    // Generate an item based on the question, attaching it to our own list.
                    itemToDrop = new QuestionItem(p as Question);
                    isNewItem = true;
                }
                if (p != null && p is QuestionFolder)
                {
                    folder = p as QuestionFolder;
                    isFolder = true;
                    itemToDrop = null;
                }
            }
            else
            {
                if (dob.GetDataPresent("Perfectus.Client.Studio.UI.Designers.PageItems.Item"))
                {
                    itemToDrop = dob.GetData("Perfectus.Client.Studio.UI.Designers.PageItems.Item") as Item;
                }
            }


            return itemToDrop;
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            Graphics g = pevent.Graphics;
            //Rectangle clipRect = pevent.ClipRectangle;
            Rectangle clipRect = new Rectangle(0, 0, Width, Height);

            g.FillRectangle(new LinearGradientBrush(clipRect, gradientStart, gradientEnd, 45, false), clipRect);


//			if (isSelected)
//			{
//				g.FillRectangle(Brushes.White, pictureBox1.Left,  pictureBox1.Top, pictureBox1.Right, pictureBox1.Bottom);
//				g.FillRectangle(Brushes.White, pictureBox2.Left,  pictureBox2.Top, pictureBox2.Right, pictureBox2.Bottom);
//				g.FillRectangle(Brushes.White, pictureBox3.Left,  pictureBox3.Top, pictureBox3.Right, pictureBox3.Bottom);
//				g.FillRectangle(Brushes.White, pictureBox4.Left,  pictureBox4.Top, pictureBox4.Right, pictureBox4.Bottom);
//				g.FillRectangle(Brushes.White, pictureBox5.Left,  pictureBox5.Top, pictureBox5.Right, pictureBox5.Bottom);
//			}

            // Draw the titlebar
            g.DrawImageUnscaled(pictureBox6.Image, pictureBox6.Bounds);
            g.DrawImage(pictureBox7.Image, pictureBox7.Bounds);
            //g.DrawImageUnscaled(pictureBox8.Image, pictureBox8.Bounds);
        }

        private void SetPushed()
        {
            //HideShadows();
            pictureBox6.Image = pictureBox15.Image;
            pictureBox7.Image = pictureBox14.Image;
            pictureBox8.Image = pictureBox10.Image;
        }

        private void SetPulled()
        {
            //ShowShadows();
            pictureBox6.Image = pictureBox20.Image;
            pictureBox7.Image = pictureBox19.Image;
            pictureBox8.Image = pictureBox16.Image;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            if (frm == null)
            {
                frm = FindForm() as PageDesigner;
            }

            // Draw the title bar's icon and text
            if (imageIndex >= 0 && frm != null)
            {
                e.Graphics.CompositingMode = CompositingMode.SourceOver;
                e.Graphics.DrawImage(frm.ilstItems.Images[imageIndex], 15, 11);
            }

            Font f = new Font("Microsoft Sans Serif", 8.5f, FontStyle.Bold, GraphicsUnit.Point);
            g.DrawString(name, f, Brushes.White, 35, 11);

            f.Dispose();
        }


        private void pictureBox8_MouseEnter(object sender, EventArgs e)
        {
            if (isSelected)
            {
                pictureBox8.Image = pictureBox9.Image;
            }
            else
            {
                pictureBox8.Image = pictureBox17.Image;
            }
        }

        private void pictureBox8_MouseLeave(object sender, EventArgs e)
        {
            if (isSelected)
            {
                pictureBox8.Image = pictureBox10.Image;
            }
            else
            {
                pictureBox8.Image = pictureBox16.Image;
            }
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            deleting = true;
            if (FindForm() is PageDesigner)
            {
                ((PageDesigner) (FindForm())).ActiveItem = null;
            }

            ItemList oldList = parentItemList;
            if (oldList != null)
            {
                RemoveFromParentList();
                Dispose();
                oldList.ResetUI();
            }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            Debug.WriteLine(string.Format("OnGotFocus: {0}", GetType().Name));
            base.OnGotFocus(e);
            OnEnter(e);
        }


        protected override void OnLostFocus(EventArgs e)
        {
            Debug.WriteLine(string.Format("OnLostFocus: {0}", GetType().Name));
            base.OnLostFocus(e);
            OnLeave(e);
        }

        #region ICustomTypeDescriptor Members

        public TypeConverter GetConverter()
        {
            return (TypeDescriptor.GetConverter(this, true));
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return (TypeDescriptor.GetEvents(this, attributes, true));
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return (TypeDescriptor.GetEvents(this, true));
        }

        public string GetComponentName()
        {
            return (TypeDescriptor.GetComponentName(this, true));
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public AttributeCollection GetAttributes()
        {
            return (TypeDescriptor.GetAttributes(this, true));
        }

        public PropertyDescriptorCollection GetProperties(Attribute[]
                                                              attributes)

        {
            // DONE:  Add Filterable.GetProperties implementation

            PropertyDescriptorCollection
                pdc = TypeDescriptor.GetProperties(this, attributes, true);

            PropertyDescriptorCollection pds = new
                PropertyDescriptorCollection(new PropertyDescriptor[0]);

            foreach (PropertyDescriptor pd in pdc)

            {
                Attribute a = pd.Attributes[typeof (CategoryAttribute)];

                if (a != null && ((CategoryAttribute) a).Category == "PageItem")
                {
                    pds.Add(pd);
                }
            }
            return pds;
        }


        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return GetProperties(new Attribute[] {});
        }

        public object GetEditor(Type editorBaseType)
        {
            return (TypeDescriptor.GetEditor(this, editorBaseType, true));
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return (TypeDescriptor.GetDefaultProperty(this, true));
        }

        public EventDescriptor GetDefaultEvent()
        {
            return (TypeDescriptor.GetDefaultEvent(this, true));
        }

        public string GetClassName()
        {
            return (TypeDescriptor.GetClassName(this, true));
        }

        #endregion
    }
}