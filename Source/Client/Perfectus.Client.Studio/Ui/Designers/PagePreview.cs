using System.Windows.Forms;
using System.ComponentModel;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Summary description for PagePreview.
	/// </summary>
	public class PagePreview : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private string serialisedPage = null;
		private string address = null;
        private WebBrowser webBrowser;
		private bool haveSubmitted = false;

		public PagePreview()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
            webBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser_DocumentCompleted);

            string url = "about:blank";
			base.Cursor = Cursors.WaitCursor;
			try
			{
                webBrowser.Navigate(url);
			}
			finally
			{
				base.Cursor = Cursors.Default;
			}
		}

		public string SerialisedPage
		{
			get { return serialisedPage; }
			set { serialisedPage = value; }
		}

		public string Address
		{
			get { return address; }
			set { address = string.Format("{0}/InterviewPreview.aspx", value); }
		}

		public void ResetUI()
		{
			FindForm().Cursor = Cursors.WaitCursor;
			haveSubmitted = false;
            webBrowser.Visible = true;
            webBrowser.Refresh();
            webBrowser.Navigate(address);
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PagePreview));
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(528, 384);
            this.webBrowser.TabIndex = 0;
            // 
            // PagePreview
            // 
            this.Controls.Add(this.webBrowser);
            this.Name = "PagePreview";
            this.Size = new System.Drawing.Size(528, 384);
            this.ResumeLayout(false);
		}

		#endregion

        /// <summary>
        ///     Handles the web browers document completed event.
        ///     The web browser first browses to the page and then submits. Therefore this 
        ///     handler is hit twice, hence the only submit once if condition.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                webBrowser.Invalidate();

                // Only submit once
                if (!haveSubmitted && address != null) //&& serialisedPage != null)
                {
                    haveSubmitted = true;

                    HtmlDocument htmlDocument = webBrowser.Document;
                    HtmlElement form = htmlDocument.Forms["PerfectusPreviewForm"];
                    HtmlElement field = htmlDocument.All["PAGEDATA"];

                    if (form != null)
                    {
                        form.SetAttribute("target", "_self");
                        field.SetAttribute("value", serialisedPage);
                        form.InvokeMember("submit");
                    }
                }
            }
            finally
            {
                Form f = FindForm();
                if (f != null)
                {
                    f.Cursor = Cursors.Default;
                }
                else
                {
                    Cursor = Cursors.Default;
                }
            }
        }
	}
}
