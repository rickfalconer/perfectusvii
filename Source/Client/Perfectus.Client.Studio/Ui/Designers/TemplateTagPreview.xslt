<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
<xsl:param name="templatePath" />
	<!-- get a reference to the document being transformed -->
	<xsl:variable name="xmlData" select="." />
	<xsl:variable name="wmlTemplate" select="document($templatePath)" />
	<!-- root template -->
	<xsl:template match="/">
		<!-- WordML identifier -->
		<xsl:text disable-output-escaping="yes">&#60;&#63;mso-application progid="Word.Document"&#63;&#62;</xsl:text>
		<!-- start copying the template -->
		<xsl:apply-templates select="$wmlTemplate/*" />
	</xsl:template>
	<!-- generic copy template to bring in all other tags in the Word document -->
	<xsl:template match="* | @* | node()">
		<xsl:copy>
			<xsl:apply-templates select="* | @* | node()" />
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="*[@uid]">
	
		<xsl:copy-of select="$xmlData//*[@uid='{@uid}']"></xsl:copy-of>
	
	</xsl:template>

</xsl:stylesheet>