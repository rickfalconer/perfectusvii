using System.IO;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using System.Configuration;
using Perfectus.Common;

namespace Perfectus.Client.Studio.UI.Designers
{
	/// <summary>
	/// Summary description for PdfViewer.
	/// </summary>
	public class ReportViewer : DesignerBase
	{
		private LibraryItem libItem = null;
		private WebBrowser axWebBrowser1;
		private string reportPath = null;

		public override PrefferedDesigner DefaultDesigner
		{
			get { return PrefferedDesigner.ReportViewer;}
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;


		public ReportViewer(PackageItem item) : base(item)
		{		
			InitializeComponent();

			libItem = (LibraryItem) item;
			this.Text = libItem.Name;

			string url = ConfigurationSettings.AppSettings["ReportingServerUrl"];

			if (url[url.Length - 1] != '/')
			{
				url += "/";
			}

			string filePath = string.Format(@"reports/report.aspx?report=library&itemId={0}&itemType={1}&itemName={2}", libItem.LibraryUniqueIdentifier, libItem.LibraryType, libItem.Name);
			this.reportPath = url + filePath;
		}

		public ReportViewer(ReportType reportType) : base(null)
		{			
			InitializeComponent();

			if(reportType == ReportType.AllClauses)
			{
				//TODO: Localize me
				this.Text = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.ReportViewer.Clauses");

				string url = ConfigurationSettings.AppSettings["reportingServerUrl"];

				if (url[url.Length - 1] != '/')
				{
					url += "/";
				}
				this.reportPath = url + @"reports/report.aspx?report=clauses";			
			}
		}

		public void Navigate()
		{			
			if (reportPath != null)
			{				
				axWebBrowser1.Navigate(reportPath);
				//axWebBrowser1.Navigate2(ref oUrl);
			}
		}

//		public ReportViewer(string pdfPath) : this((PackageItem)null)
//		{
//			this.pdfPath = pdfPath;
//			string title = Path.GetFileName(pdfPath);
//			this.Text = title;
//		}

	
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		public enum ReportType
		{
			AllClauses,
			LibraryItem
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportViewer));
            this.axWebBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // axWebBrowser1
            // 
            this.axWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axWebBrowser1.Location = new System.Drawing.Point(0, 0);
            this.axWebBrowser1.Name = "axWebBrowser1";
            this.axWebBrowser1.Size = new System.Drawing.Size(350, 306);
            this.axWebBrowser1.TabIndex = 0;
            // 
            // ReportViewer
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(350, 306);
            this.Controls.Add(this.axWebBrowser1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportViewer";
            this.Text = "Report";
            this.ResumeLayout(false);

		}
		#endregion
	}
}
