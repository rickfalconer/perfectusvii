using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Perfectus.Client.Studio.UI.Designers.PageItems;
using Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.Designers
{
    /// <summary>
    /// Summary description for PageDesigner.
    /// </summary>
    public class PageDesigner : DesignerBase
    {
        public ImageList imageList1;
        public ImageList ilstItems;
        private IContainer components;
        private bool hasChanges = false;
        private bool isBeingDeleted = false;
        private bool callTabChangeEvents = true;
        private ToolStripLabel btRefresh;
        private ToolStripComboBox cbServer;
        private ToolStripComboBox cbStyle;
        private ToolStrip toolStripHtml;
        private ToolStripLabel lblStyle;
        private ToolStripSeparator sepPreview;
        private ToolStripLabel lblServer;
        private SplitContainer splitContainer1;
        private Panel panelTasks;
        private Label lblTasks;
        private QuestionList questionList1;
        private LayoutItems layoutItems2;
        private TextBox textBlurb;
        private Label label2;
        private Label label1;
        private Page page;
        private PagePreview pagePreview1;
        private ToolStripButton btDesign;
        private ToolStripButton btPreview;
        private ToolStripButton btBold;
        private ToolStripButton btItalic;
        private ToolStripButton btUnderline;

        public PropertyGrid SharedPropertyGrid
        {
            get { return sharedPropertyGrid; }
        }

        private PropertyGrid sharedPropertyGrid;

        public override bool HasChanges
        {
            get { return hasChanges; }
            set { hasChanges = value; }
        }

        private bool HasItemChanges()
        {
            return FindItemChanges(page);
        }

        private static bool FindItemChanges(ItemList listToSave)
        {
            Item i = listToSave.Head;
            while (i != null)
            {
                if (i is HtmlItem)
                {
                    if (((HtmlItem) i).HasChanges())
                    {
                        return true;
                    }
                }

                else if (i is HorizontalGroup || i is RepeaterItem)
                {
                    if (FindItemChanges(i))
                    {
                        return true;
                    }
                }


                i = i.Next;
            }
            return false;
        }

        private static void SetItemsUnchanged(ItemList listToSet)
        {
            Item i = listToSet.Head;
            while (i != null)
            {
                if (i is HtmlItem)
                {
                    (((HtmlItem) i)).SetNoChanges();
                }

                else if (i is HorizontalGroup)
                {
                    SetItemsUnchanged(i);
                }

                i = i.Next;
            }
        }

        private QuestionDictionary questionsOnPage = new QuestionDictionary();

        public QuestionDictionary QuestionsOnPage
        {
            get { return questionsOnPage; }
        }


        public PageDesigner(PackageItem item, PropertyGrid propertyGrid) : base(item)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            
            //increase default ui sizes
            //questionList1.Size += new System.Drawing.Size(100, 415);
            //twItems.DockedSize += new System.Drawing.Size(100, 415);

            layoutItems2.PackageBeingEdited = ItemBeingEdited.ParentPackage;

            sharedPropertyGrid = propertyGrid;
            item.ParentPackage.ItemDeleting += new EventHandler<PackageItemCancelEventArgs>(ParentPackage_ItemDeleting);
            string configKey =
                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                    "Perfectus.Client.Studio.Config.CssNames");
            string configValue = ConfigurationSettings.AppSettings[configKey];

            if (configValue != null)
            {
                string[] names = configValue.Split('\t');
                foreach (string n in names)
                {
                    cbStyle.Items.Add(n);
                }
            }

            ResetUI();
            questionList1.QuestionsOnPage = questionsOnPage;
            questionList1.Page = (InterviewPage) ItemBeingEdited;
            questionList1.InterviewBeingEdited = ((InterviewPage) item).ParentInterview;
            questionList1.Package = item.ParentPackage;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private Item activeItem = null;

        public Item ActiveItem
        {
            get { return activeItem; }
            set
            {
                if (value == null)
                {
                    //TODO whats this for - which control to use ??
                    if (ActiveControl == splitContainer1 ) //dockTop1)
                    {
                        EnableHtmlToolbar();
                        if (sharedPropertyGrid != null)
                        {
                            sharedPropertyGrid.Enabled = true;
                            sharedPropertyGrid.SelectedObject = activeItem;
                        }
                    }
                    else
                    {
                        activeItem = value;
                        DisableHtmlToolbar();
                        if (sharedPropertyGrid != null)
                        {
                            sharedPropertyGrid.SelectedObject = null;
                            sharedPropertyGrid.Refresh();
                        }
                    }
                }

                else
                {
                    if (value is HtmlItem)
                    {
                        activeItem = value;
                        EnableHtmlToolbar();
                        if (sharedPropertyGrid != null)
                        {
                            sharedPropertyGrid.Enabled = true;
                            sharedPropertyGrid.SelectedObject = activeItem;
                        }
                    }

                    else if (value is QuestionItem)
                    {
                        activeItem = value;
                        DisableHtmlToolbar();
                        SharedPropertyGrid.SelectedObject = ((QuestionItem) activeItem).Question;
                        OnItemSelected(((QuestionItem) value).Question);

                        if (((QuestionItem)value).Question.LibraryUniqueIdentifier != Guid.Empty &&
                            ((QuestionItem)value).Question.Linked &&
                            ((QuestionItem)value).Question.CheckedOutStatus == CheckOutStatus.CheckedIn)
                        {
                            sharedPropertyGrid.Enabled = false;
                        }
                        else
                        {
                            sharedPropertyGrid.Enabled = true;
                        }
                    }
                    else if (value is RepeaterItem || value is HorizontalGroup)
                    {
                        activeItem = value;
                        DisableHtmlToolbar();
                        sharedPropertyGrid.Enabled = true;
                        SharedPropertyGrid.SelectedObject = value;
                    }
                }
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageDesigner));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.ilstItems = new System.Windows.Forms.ImageList(this.components);
            this.cbServer = new System.Windows.Forms.ToolStripComboBox();
            this.btRefresh = new System.Windows.Forms.ToolStripLabel();
            this.page = new Perfectus.Client.Studio.UI.Designers.PageItems.Page();
            this.pagePreview1 = new Perfectus.Client.Studio.UI.Designers.PagePreview();
            this.cbStyle = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripHtml = new System.Windows.Forms.ToolStrip();
            this.btDesign = new System.Windows.Forms.ToolStripButton();
            this.btBold = new System.Windows.Forms.ToolStripButton();
            this.btItalic = new System.Windows.Forms.ToolStripButton();
            this.btUnderline = new System.Windows.Forms.ToolStripButton();
            this.lblStyle = new System.Windows.Forms.ToolStripLabel();
            this.sepPreview = new System.Windows.Forms.ToolStripSeparator();
            this.btPreview = new System.Windows.Forms.ToolStripButton();
            this.lblServer = new System.Windows.Forms.ToolStripLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panelTasks = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.questionList1 = new Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls.QuestionList();
            this.layoutItems2 = new Perfectus.Client.Studio.UI.Designers.PageItems.TaskControls.LayoutItems();
            this.textBlurb = new System.Windows.Forms.TextBox();
            this.lblTasks = new System.Windows.Forms.Label();
            this.toolStripHtml.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panelTasks.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // ilstItems
            // 
            this.ilstItems.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilstItems.ImageStream")));
            this.ilstItems.TransparentColor = System.Drawing.Color.Transparent;
            this.ilstItems.Images.SetKeyName(0, "");
            this.ilstItems.Images.SetKeyName(1, "");
            this.ilstItems.Images.SetKeyName(2, "");
            this.ilstItems.Images.SetKeyName(3, "");
            this.ilstItems.Images.SetKeyName(4, "");
            // 
            // cbServer
            // 
            this.cbServer.Name = "cbServer";
            resources.ApplyResources(this.cbServer, "cbServer");
            // 
            // btRefresh
            // 
            this.btRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btRefresh.Name = "btRefresh";
            resources.ApplyResources(this.btRefresh, "btRefresh");
            this.btRefresh.Click += new System.EventHandler(this.btRefresh_Click);
            // 
            // page
            // 
            this.page.AllowDrop = true;
            resources.ApplyResources(this.page, "page");
            this.page.BackColor = System.Drawing.Color.White;
            this.page.Cursor = System.Windows.Forms.Cursors.Default;
            this.page.Head = null;
            this.page.Name = "page";
            this.page.Tail = null;
            // 
            // pagePreview1
            // 
            this.pagePreview1.Address = resources.GetString("pagePreview1.Address");
            this.pagePreview1.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this.pagePreview1, "pagePreview1");
            this.pagePreview1.Name = "pagePreview1";
            this.pagePreview1.SerialisedPage = null;
            // 
            // cbStyle
            // 
            this.cbStyle.Name = "cbStyle";
            resources.ApplyResources(this.cbStyle, "cbStyle");
            this.cbStyle.SelectedIndexChanged += new System.EventHandler(this.cbStyle_SelectedIndexChanged);
            // 
            // toolStripHtml
            // 
            this.toolStripHtml.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btDesign,
            this.btBold,
            this.btItalic,
            this.btUnderline,
            this.lblStyle,
            this.cbStyle,
            this.sepPreview,
            this.btPreview,
            this.lblServer,
            this.cbServer,
            this.btRefresh});
            resources.ApplyResources(this.toolStripHtml, "toolStripHtml");
            this.toolStripHtml.Name = "toolStripHtml";
            // 
            // btDesign
            // 
            this.btDesign.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btDesign, "btDesign");
            this.btDesign.Name = "btDesign";
            this.btDesign.Click += new System.EventHandler(this.btDesign_Click);
            // 
            // btBold
            // 
            this.btBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btBold, "btBold");
            this.btBold.Name = "btBold";
            this.btBold.Click += new System.EventHandler(this.btBold_Click);
            // 
            // btItalic
            // 
            this.btItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btItalic, "btItalic");
            this.btItalic.Name = "btItalic";
            this.btItalic.Click += new System.EventHandler(this.btItalic_Click);
            // 
            // btUnderline
            // 
            this.btUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btUnderline, "btUnderline");
            this.btUnderline.Name = "btUnderline";
            this.btUnderline.Click += new System.EventHandler(this.btUnderline_Click);
            // 
            // lblStyle
            // 
            this.lblStyle.Name = "lblStyle";
            resources.ApplyResources(this.lblStyle, "lblStyle");
            // 
            // sepPreview
            // 
            this.sepPreview.Name = "sepPreview";
            resources.ApplyResources(this.sepPreview, "sepPreview");
            // 
            // btPreview
            // 
            this.btPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            resources.ApplyResources(this.btPreview, "btPreview");
            this.btPreview.Name = "btPreview";
            this.btPreview.Click += new System.EventHandler(this.btPreview_Click);
            // 
            // lblServer
            // 
            this.lblServer.Name = "lblServer";
            resources.ApplyResources(this.lblServer, "lblServer");
            // 
            // splitContainer1
            // 
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.pagePreview1);
            this.splitContainer1.Panel1.Controls.Add(this.page);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panelTasks);
            // 
            // panelTasks
            // 
            resources.ApplyResources(this.panelTasks, "panelTasks");
            this.panelTasks.BackColor = System.Drawing.SystemColors.Control;
            this.panelTasks.Controls.Add(this.label2);
            this.panelTasks.Controls.Add(this.label1);
            this.panelTasks.Controls.Add(this.questionList1);
            this.panelTasks.Controls.Add(this.layoutItems2);
            this.panelTasks.Controls.Add(this.textBlurb);
            this.panelTasks.Controls.Add(this.lblTasks);
            this.panelTasks.Name = "panelTasks";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // questionList1
            // 
            resources.ApplyResources(this.questionList1, "questionList1");
            this.questionList1.InterviewBeingEdited = null;
            this.questionList1.Name = "questionList1";
            this.questionList1.Package = null;
            this.questionList1.Page = null;
            this.questionList1.QuestionsOnPage = null;
            // 
            // layoutItems2
            // 
            resources.ApplyResources(this.layoutItems2, "layoutItems2");
            this.layoutItems2.Name = "layoutItems2";
            this.layoutItems2.PackageBeingEdited = null;
            // 
            // textBlurb
            // 
            resources.ApplyResources(this.textBlurb, "textBlurb");
            this.textBlurb.Name = "textBlurb";
            // 
            // lblTasks
            // 
            resources.ApplyResources(this.lblTasks, "lblTasks");
            this.lblTasks.Name = "lblTasks";
            // 
            // PageDesigner
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStripHtml);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "PageDesigner";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.PageDesigner_Closing);
            this.toolStripHtml.ResumeLayout(false);
            this.toolStripHtml.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panelTasks.ResumeLayout(false);
            this.panelTasks.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public override bool Save()
        {
            // Walk the page controls linked list of items, putting them into our 'Page' objects Items collection.

            SaveToCollection(((InterviewPage) ItemBeingEdited).Items, page);
            hasChanges = false;
            SetItemsUnchanged(page);
            return true;
        }

        protected override void ResetUI()
        {
            // Walk our 'Items' collection, appending them to our Page control's linked list.
            InterviewPage p = ItemBeingEdited as InterviewPage;
            if (p != null)
            {
                LoadFromCollection(p.Items, page);
            }
            HookupServerCombos();

            btDesign_Click(this, new EventArgs());
        }

        private void HookupServerCombos()
        {
            if (itemBeingEdited != null)
            {
                Package p = itemBeingEdited.ParentPackage;
                if (p != null)
                {
                    foreach(Server s in p.Servers)
                    {
                        ComboboxItem item = new ComboboxItem();
                        item.Text = s.Name;
                        item.Value = s.Address;
                        cbServer.Items.Add(item);
                    }
                    // use the value as follows:
                    // MessageBox.Show((comboBox1.SelectedItem as ComboboxItem).Value.ToString());
                }
            }
        }

        private void LoadFromCollection(PageItemCollection col, ItemList listToLoad)
        {
            foreach (IPageItem i in col)
            {
                if (i is Question)
                {
                    QuestionItem qi = new QuestionItem(i as Question);
                    listToLoad.Append(qi);
                    if (!questionsOnPage.Contains(qi.Question.UniqueIdentifier))
                    {
                        questionsOnPage.Add(qi.Question.UniqueIdentifier, qi.Question);
                    }
                }

                else if (i is HtmlBlock)
                {
                    HtmlItem hi = new HtmlItem(((HtmlBlock)i).Html, itemBeingEdited.ParentPackage);
                    hi.Height = ((HtmlBlock) i).DesignerHeight;
                    hi.UniqueIdentifier = ((HtmlBlock) i).UniqueIdentifier;
                    hi.Visible = ((HtmlBlock) i).Visible;

                    listToLoad.Append(hi);
                }

                else if (i is HorizontalLayoutZone)
                {
                    HorizontalGroup hgi = new HorizontalGroup();
                    hgi.UniqueIdentifier = ((HorizontalLayoutZone) i).UniqueIdentifier;
                    hgi.Visible = ((HorizontalLayoutZone) i).Visible;
                    hgi.PackageBeingEdited = itemBeingEdited.ParentPackage;
                    listToLoad.Append(hgi);
                    LoadFromCollection(((HorizontalLayoutZone) i).Items, hgi);
                }
                else if (i is RepeaterZone)
                {
                    RepeaterItem rpt = new RepeaterItem();
                    rpt.UniqueIdentifier = ((RepeaterZone) i).UniqueIdentifier;
                    rpt.Visible = ((RepeaterZone) i).Visible;
                    rpt.MaxRows = ((RepeaterZone) i).MaxRows;
                    rpt.PackageBeingEdited = itemBeingEdited.ParentPackage;
                    listToLoad.Append(rpt);
                    LoadFromCollection(((RepeaterZone) i).Items, rpt);
                }
            }
            listToLoad.ResetUI();

            hasChanges = false;
        }


        private static void SaveToCollection(PageItemCollection col, ItemList listToSave)
        {
            col.Clear();
            Item i = listToSave.Head;
            while (i != null)
            {
                if (i is QuestionItem)
                {
                    col.Add(((QuestionItem) i).Question);
                }
                else if (i is HtmlItem)
                {
                    string b = ((HtmlItem) i).XmlBody;
                    HtmlBlock block = new HtmlBlock(b, i.Height, i.UniqueIdentifier, i.Visible);
                    col.Add(block);
                }

                else if (i is HorizontalGroup)
                {
                    if (i is RepeaterItem)
                    {
                        RepeaterZone rzone = new RepeaterZone(i.UniqueIdentifier, i.Visible, ((RepeaterItem) i).MaxRows);
                        col.Add(rzone);
                        SaveToCollection(rzone.Items, i);
                    }
                    else
                    {
                        HorizontalLayoutZone zone = new HorizontalLayoutZone(i.UniqueIdentifier, i.Visible);
                        col.Add(zone);
                        SaveToCollection(zone.Items, i);
                    }
                }

                i = i.Next;
            }
        }


        private void ParentPackage_ItemDeleting(object sender, PackageItemCancelEventArgs e)
        {
            if (e.Item is Question)
            {
                DeleteQuestionFromList(e.Item as Question, page);
            }
            else if (e.Item == ItemBeingEdited)
            {
                isBeingDeleted = true;
                Close();
                Dispose();
            }
        }

        private static void DeleteQuestionFromList(Question q, ItemList l)
        {
            Item i = l.Head;
            while (i != null)
            {
                if (i is QuestionItem)
                {
                    if (((QuestionItem) i).Question == q)
                    {
                        i.RemoveFromParentList();
                        i.Dispose();
                    }
                }
                else if (i is HorizontalGroup)
                {
                    DeleteQuestionFromList(q, i);
                }
                i = i.Next;
            }
            l.ResetUI();
        }

        protected override bool OkayToClose()
        {
            if ((! hasChanges && ! HasItemChanges()) || isBeingDeleted)
            {
                return true;
            }
            else
            {
                if (itemBeingEdited != null)
                {
                    DialogResult dr =
                        MessageBox.Show(
                            string.Format(
                                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                                    "Perfectus.Client.UI.Designers.PageDesigner.ConfirmCommit"), itemBeingEdited.Name),
                            Common.About.FormsTitle, MessageBoxButtons.YesNoCancel);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            return Save();
                        case DialogResult.No:
                            SetItemsUnchanged(page);
                            hasChanges = false;
                            return true;
                        case DialogResult.Cancel:
                        default:
                            return false;
                    }
                }
            }
            return true;
        }


        internal void DoRefreshQuestionList()
        {
            if (questionList1 != null)
            {
                questionList1.RefreshIcons();
            }
        }

        public override void UpdateUI()
        {
            DoRefreshQuestionList();
        }

        private void AutoPreview()
        {
            if (cbServer.Items.Count == 0)
            {
                MessageBox.Show(
                    ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                        "Perfectus.Client.UI.Designers.PageDesigner.ServerBeforePreviewError"), Common.About.FormsTitle);
                return;
            }
            if (cbServer.Items.Count == 1)
            {
                cbServer.SelectedItem = cbServer.Items[0];
                DoRefreshPreview();
                return;
            }
            if (cbServer.Items.Count > 0 && cbServer.SelectedItem != null)
            {
                DoRefreshPreview();
            }
        }

        private void DisableHtmlToolbar()
        {
            btBold.Enabled = false;
            btItalic.Enabled = false;
            btUnderline.Enabled = false;
            lblStyle.Enabled = false;
            cbStyle.Enabled = false;
        }

        private void EnableHtmlToolbar()
        {
            btBold.Enabled = true;
            btItalic.Enabled = true;
            btUnderline.Enabled = true;
            lblStyle.Enabled = true;
            cbStyle.Enabled = true;
        }

        private void DisablePreviewToolbar()
        {
            lblServer.Enabled = false;
            cbServer.Enabled = false;
            btRefresh.Enabled = false;
        }

        private void EnablePreviewToolbar()
        {
            lblServer.Enabled = true;
            cbServer.Enabled = true;
            btRefresh.Enabled = true;
        }


        private void DoRefreshPreview()
        {
            ComboboxItem selectedItem = cbServer.SelectedItem as ComboboxItem;

            if (selectedItem.Value.ToString() != null)
                {
                pagePreview1.Address = selectedItem.Value.ToString();

                if (itemBeingEdited != null && itemBeingEdited is InterviewPage)
                {
                    InterviewPage pageBeingEdited = (InterviewPage) itemBeingEdited;

                    SaveToCollection(pageBeingEdited.Items, page);

                    Package packageBeingEdited = pageBeingEdited.ParentPackage;

                    Package clonePackage = (Package) (packageBeingEdited.Clone());

                    clonePackage.ClearForPreview(pageBeingEdited.UniqueIdentifier);

                    InterviewPage pageToSend = clonePackage.InterviewPageByGuid[pageBeingEdited.UniqueIdentifier];

                    string page64;

                    using (MemoryStream ms = new MemoryStream())
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Context = new StreamingContext(StreamingContextStates.Persistence);
                        formatter.Serialize(ms, pageToSend);
                        ms.Flush();
                        ms.Seek(0, SeekOrigin.Begin);
                        byte[] bytes = ms.ToArray();
                        page64 = Convert.ToBase64String(bytes);
                    }

                    pagePreview1.SerialisedPage = page64;
                    hasChanges = false;
                    pagePreview1.ResetUI();
                }
            }
            else
            {
                string serverName = selectedItem.Text;
                string msg =
                    string.Format(
                        ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString(
                            "Perfectus.Client.UI.Designers.PageDesigner.NoPreviewAddressError"), serverName);
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(msg, Common.About.FormsTitle);
            }
        }

        private void PageDesigner_Closing(object sender, CancelEventArgs e)
        {
            Show();

            callTabChangeEvents = false;

            // TODO is this to force designer changes to save?
            // switch to preview and bac to design
            //tabControl1.SelectedIndex = 1;
            //tabControl1.SelectedIndex = 0;

            callTabChangeEvents = true;

            if (sharedPropertyGrid != null)
            {
                sharedPropertyGrid.SelectedObject = null;
            }
        }

        private void cbStyle_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (activeItem != null && activeItem is HtmlItem)
            {
                ((HtmlItem)activeItem).SetStyle(cbStyle.Text);
            }
        }

        private void btBold_Click(object sender, EventArgs e)
        {
            if (activeItem != null && activeItem is HtmlItem)
            {
                ((HtmlItem)activeItem).SetBold();
            }
        }

        private void btItalic_Click(object sender, EventArgs e)
        {
            if (activeItem != null && activeItem is HtmlItem)
            {
                ((HtmlItem)activeItem).SetItalic();
            }
        }

        private void btUnderline_Click(object sender, EventArgs e)
        {
            if (activeItem != null && activeItem is HtmlItem)
            {
                ((HtmlItem)activeItem).SetUnderline();
            }
        }

        private void btRefresh_Click(object sender, EventArgs e)
        {
            DoRefreshPreview();
        }

        private void btDesign_Click(object sender, EventArgs e)
        {
            DisableHtmlToolbar();
            DisablePreviewToolbar();
            splitContainer1.Panel2Collapsed = false;
            page.Visible = true;
            pagePreview1.Visible = false;
            btDesign.CheckState = CheckState.Checked;
            btPreview.CheckState = CheckState.Unchecked;
        }

        private void btPreview_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = true;
            EnablePreviewToolbar();
            DisableHtmlToolbar();
            page.Visible = false;
            pagePreview1.Visible = true;
            btDesign.CheckState = CheckState.Unchecked;
            btPreview.CheckState = CheckState.Checked;
            AutoPreview();
        }
    }
}
