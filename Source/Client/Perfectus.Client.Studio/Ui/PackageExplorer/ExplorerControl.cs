using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Client.Studio.UI.Designers.SharedLibrary;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Client.Studio.UI.SharedLibrary;
using Perfectus.Client.Studio.UI.Dialogs;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.Server.WebAPI.ConfigurationSystem.Plugins;
using Perfectus.Common.SharedLibrary;
using Application=System.Windows.Forms.Application;
using WordDocument = Microsoft.Office.Interop.Word.Document;
using Font = System.Drawing.Font;
using Page = Perfectus.Common.PackageObjects.InterviewPage;
using DeliveryDoc = Perfectus.Common.PackageObjects.Document;
using System.Drawing.Design;
using System.Text;
using Perfectus.Client.Studio.UI.Dialogs;
using System.Data;

namespace Perfectus.Client.Studio.UI.PackageExplorer
{
    /// <summary>
    /// Summary description for PackageExplorer.
    /// </summary>
    public class ExplorerControl : UserControl
    {
        #region Privates

        private TreeNode savedNode = null;
        private ArrayList importTemplateMenuItems;
        private ArrayList exportTemplateMenuItems;
        private Collection<IStudioPlugin> plugins;
        private ImageList ilistTreeView;
        private IContainer components;
        private Package package;
        private PropertyGrid propertyGrid = null;
        private TreeView tvwExplorer;
        private TreeNode rootNode;
        private TreeNode serversNode;
        private TreeNode questionsNode;
        private TreeNode interviewNode;
        private TreeNode outcomesNode;
        private TreeNode functionsNode;
        private TreeNode webReferencesNode;
        private TreeNode simpleOutcomesNode;
        private TreeNode templatesNode;
        private TreeNode attachmentsNode;
        private GuidToPfTreeNodeCollectionDictionary guidNodeMap;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private ContextMenu cmExplorer;
        private MenuItem mnuNewInterview;
        private MenuItem mnuNewPage;
        private MenuItem mnuDelete;
        private MenuItem mnuRemoveFromRule;
        private MenuItem mnuNewArithmeticFunction;
        private MenuItem mnuNewDateFunction;
        private MenuItem mnuNewOutcome;
        private MenuItem mnuNewServer;
        private MenuItem mnuNewRule;
        private MenuItem mnuRefresh;
        private MenuItem mnuRefreshDataSource;
        private MenuItem mnuAddDocumentToDelivery;
        private MenuItem mnuRemoveAllDocumentsFromDelivery;
        private MenuItem mnuEditDocumentToDelivery;
        private MenuItem mnuAddDeliveryToRule;
        private MenuItem mnuAddDistributorToDelivery;
        private MenuItem mnuExportTemplate;
        private MenuItem mnuNewQuestion;
        private MenuItem mnuNewTemplate;
        private MenuItem mnuNewAttachment;
        private MenuItem mnuImportTemplate;
        private MenuItem mnuImportAttachment;
        private MenuItem mnuLinkQuestionAttachment;
        private MenuItem mnuNewWebReference;
        private MenuItem mnuRename;
        private MenuItem mnuPublish;
        private MenuItem mnuOpen;
        private MenuItem mnuSeparator1;
        private MenuItem mnuSeparator2;
        private MenuItem mnuSeparator3;
        private MenuItem mnuSeparator4;
        private MenuItem mnuCreateCopy;
        private MenuItem mnuNewSimpleOutcome;
        private MenuItem mnuNewFolder;
        private bool suspended = false;
        private MenuItem mnuNewExternalPage;
        private DesignerBase activeDesigner;
        private bool sharedLibraryEventsDisabled = false;
        private bool reportingEnabled = Convert.ToBoolean(ConfigurationSettings.AppSettings["ReportingEnabled"]);
        private MenuItem mnuDependencies;

        // Rule2

        // Repository variables
        private SharedLibraryController sharedLibraryController;
        private SharedLibraryProxy sharedLibraryProxy;
        private MenuItem mnuUpdateAllLinkedSharedLibraryItems;
        private MenuItem mnuCheckOut;
        private MenuItem mnuCheckIn;
        private MenuItem mnuUndoCheckOut;
        private MenuItem mnuGetLatestVersion;
        private MenuItem mnuLinkSharedItem;
        private MenuItem mnuUnlinkSharedItem;
        private MenuItem mnuShareLibrarySeparator;
        private MenuItem mnuDisassociate;
        private MenuItem mnuViewReport;

        // For scrolling .Net controls.
        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        private const int WM_VSCROLL = 277; // Vertical scroll	
        private const int SB_PAGEUP = 2; // Scrolls one page up	
        private const int SB_PAGEDOWN = 3; // Scrolls one page down
        private const int packageRootNode = 0;

        #endregion

        #region Properties, Enums & Events

        private enum TreeNodeIndex
        {
            SimpleOutcome = 0,
            EasyInterview,
            Function,
            Outcome,
            ProcessManager,
            Question,
            Template,
            WebReference
        }

        [field : NonSerialized]
        [Browsable(true)]
        public event PackageItemOpenDesignerEventHandler OpenDesigner;

        [field : NonSerialized]
        [Browsable(true)]
        public event EventHandler Publish;

        [field : NonSerialized]
        [Browsable(true)]
        public event EventHandler<PackageItemEventArgs> SimpleOutcomeDrop;

        [field : NonSerialized]
        [Browsable(true)]
        public event EventHandler<PackageItemEventArgs> OutcomeDrop;

        [field : NonSerialized]
        [Browsable(false)]
        public event EventHandler<PackageItemEventArgs> PopupDialogMoved;

        [Browsable(false)]
        public Collection<IStudioPlugin> Plugins
        {
            get { return plugins; }
            set
            {
                plugins = value;
                LoadPlugins();
            }
        }

        public PropertyGrid PropertyGrid
        {
            get { return propertyGrid; }
            set { propertyGrid = value; }
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        public Package Package
        {
            get { return package; }
            set
            {
                tvwExplorer.SuspendLayout();
                suspended = true;
                package = value;
                if (package != null)
                {
                    package.PropertyChanged += new PropertyChangedEventHandler(package_PropertyChanged);
                    package.ItemCreated += new EventHandler<PackageItemEventArgs>(package_ItemCreated);
                    package.RuleDeliveryAdded += new EventHandler<RuleDeliveryEventArgs>(package_RuleDeliveryAdded);
                    package.DeliveryDistributorAdded += new EventHandler<DeliveryDistributorEventArgs>(package_DeliveryDistributorAdded);
                    package.DeliveryDocumentAdded += new EventHandler<DeliveryDocumentEventArgs>(package_DeliveryDocumentAdded);
                    package.ItemDeleted += new EventHandler<PackageItemDeletedEventArgs>(package_ItemDeleted);
                    package.BulkLoadStarted += new EventHandler(package_BulkLoadStarted);
                    package.BulkLoadDone += new EventHandler(package_BulkLoadDone);
                    package.ItemUIDChanged += new EventHandler<LibraryItemEventArgs>(package_ItemUIDChanged);
                    package.LibraryItemUpdated += new EventHandler<LibraryItemEventArgs>(package_LibraryItemUpdated);
                    package.BindingsCannotBeReconnected += new EventHandler<PackageItemEventArgs>(package_BindingsCannotBeReconnected);
                }
                ResetTreeWithCollapse();
                suspended = false;
                tvwExplorer.ResumeLayout();
            }
        }

        //IPMAnager UI refresh - single click to expand, not edit.
        private bool bAllowEdit;

        #endregion

        #region Constructor - InitializeComponent

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        public ExplorerControl()
        {
            try
            {
                //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl");
                //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr");
                // This call is required by the Windows.Forms Form Designer.
                InitializeComponent();

                guidNodeMap = new GuidToPfTreeNodeCollectionDictionary();


                //IPManager UI 
                //MainDesigner.ActiveDesignerChanged += new MainDesigner.ActiveDesignerHandler(MainDesignerX_ActiveDesignerChanged);
                MainDesigner2.ActiveDesignerChanged += new MainDesigner2.ActiveDesignerHandler(MainDesignerX_ActiveDesignerChanged);

                // Init the repository object
                sharedLibraryProxy = new SharedLibraryProxy();
                sharedLibraryController = new SharedLibraryController();
            }
            catch (Exception ex)
            {
                try
                {
                    bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);

                    if (rethrow)
                    {
                        throw ex;
                    }
                }
                catch
                {
                    string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                    MessageBox.Show(message, GetResource("PackageExplorer.ExplorerControl.ExplorerPaneError"), MessageBoxButtons.OK, 
                                    MessageBoxIcon.Exclamation);
                }
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExplorerControl));
            this.tvwExplorer = new System.Windows.Forms.TreeView();
            this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
            this.cmExplorer = new System.Windows.Forms.ContextMenu();
            this.mnuPublish = new System.Windows.Forms.MenuItem();
            this.mnuSeparator1 = new System.Windows.Forms.MenuItem();
            this.mnuOpen = new System.Windows.Forms.MenuItem();
            this.mnuCreateCopy = new System.Windows.Forms.MenuItem();
            this.mnuRemoveFromRule = new System.Windows.Forms.MenuItem();
            this.mnuImportTemplate = new System.Windows.Forms.MenuItem();
            this.mnuImportAttachment = new System.Windows.Forms.MenuItem();
            this.mnuLinkQuestionAttachment = new System.Windows.Forms.MenuItem();
            this.mnuExportTemplate = new System.Windows.Forms.MenuItem();
            this.mnuRefresh = new System.Windows.Forms.MenuItem();
            this.mnuRefreshDataSource = new System.Windows.Forms.MenuItem();
            this.mnuShareLibrarySeparator = new System.Windows.Forms.MenuItem();
            this.mnuUpdateAllLinkedSharedLibraryItems = new System.Windows.Forms.MenuItem();
            this.mnuCheckIn = new System.Windows.Forms.MenuItem();
            this.mnuCheckOut = new System.Windows.Forms.MenuItem();
            this.mnuUndoCheckOut = new System.Windows.Forms.MenuItem();
            this.mnuGetLatestVersion = new System.Windows.Forms.MenuItem();
            this.mnuLinkSharedItem = new System.Windows.Forms.MenuItem();
            this.mnuUnlinkSharedItem = new System.Windows.Forms.MenuItem();
            this.mnuDisassociate = new System.Windows.Forms.MenuItem();
            this.mnuViewReport = new System.Windows.Forms.MenuItem();
            this.mnuSeparator2 = new System.Windows.Forms.MenuItem();
            this.mnuNewInterview = new System.Windows.Forms.MenuItem();
            this.mnuNewPage = new System.Windows.Forms.MenuItem();
            this.mnuNewExternalPage = new System.Windows.Forms.MenuItem();
            this.mnuNewArithmeticFunction = new System.Windows.Forms.MenuItem();
            this.mnuNewDateFunction = new System.Windows.Forms.MenuItem();
            this.mnuNewOutcome = new System.Windows.Forms.MenuItem();
            this.mnuNewSimpleOutcome = new System.Windows.Forms.MenuItem();
            this.mnuNewServer = new System.Windows.Forms.MenuItem();
            this.mnuNewRule = new System.Windows.Forms.MenuItem();
            this.mnuNewQuestion = new System.Windows.Forms.MenuItem();
            this.mnuNewTemplate = new System.Windows.Forms.MenuItem();
            this.mnuNewAttachment = new System.Windows.Forms.MenuItem();
            this.mnuNewWebReference = new System.Windows.Forms.MenuItem();
            this.mnuNewFolder = new System.Windows.Forms.MenuItem();
            this.mnuSeparator3 = new System.Windows.Forms.MenuItem();
            this.mnuAddDocumentToDelivery = new System.Windows.Forms.MenuItem();
            this.mnuRemoveAllDocumentsFromDelivery = new System.Windows.Forms.MenuItem();
            this.mnuEditDocumentToDelivery = new System.Windows.Forms.MenuItem();
            this.mnuAddDeliveryToRule = new System.Windows.Forms.MenuItem();
            this.mnuAddDistributorToDelivery = new System.Windows.Forms.MenuItem();
            this.mnuSeparator4 = new System.Windows.Forms.MenuItem();
            this.mnuRename = new System.Windows.Forms.MenuItem();
            this.mnuDependencies = new System.Windows.Forms.MenuItem();
            this.mnuDelete = new System.Windows.Forms.MenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // tvwExplorer
            // 
            this.tvwExplorer.AllowDrop = true;
            this.tvwExplorer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.tvwExplorer, "tvwExplorer");
            this.tvwExplorer.HideSelection = false;
            this.tvwExplorer.ImageList = this.ilistTreeView;
            this.tvwExplorer.ItemHeight = 18;
            this.tvwExplorer.LabelEdit = true;
            this.tvwExplorer.Name = "tvwExplorer";
            this.tvwExplorer.ShowLines = false;
            this.tvwExplorer.ShowPlusMinus = false;
            this.tvwExplorer.ShowRootLines = false;
            this.tvwExplorer.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvwExplorer_BeforeLabelEdit);
            this.tvwExplorer.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvwExplorer_AfterLabelEdit);
            this.tvwExplorer.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwExplorer_ItemDrag);
            this.tvwExplorer.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwExplorer_AfterSelect);
            this.tvwExplorer.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvwExplorer_DragDrop);
            this.tvwExplorer.DragOver += new System.Windows.Forms.DragEventHandler(this.tvwExplorer_DragOver);
            this.tvwExplorer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tvwExplorer_KeyUp);
            this.tvwExplorer.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tvwExplorer_MouseUp);
            // 
            // ilistTreeView
            // 
            this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
            this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ilistTreeView.Images.SetKeyName(0, "");
            this.ilistTreeView.Images.SetKeyName(1, "");
            this.ilistTreeView.Images.SetKeyName(2, "");
            this.ilistTreeView.Images.SetKeyName(3, "");
            this.ilistTreeView.Images.SetKeyName(4, "");
            this.ilistTreeView.Images.SetKeyName(5, "");
            this.ilistTreeView.Images.SetKeyName(6, "");
            this.ilistTreeView.Images.SetKeyName(7, "Folder-new.png");
            this.ilistTreeView.Images.SetKeyName(8, "");
            this.ilistTreeView.Images.SetKeyName(9, "");
            this.ilistTreeView.Images.SetKeyName(10, "Question-new.png");
            this.ilistTreeView.Images.SetKeyName(11, "");
            this.ilistTreeView.Images.SetKeyName(12, "Template-new.png");
            this.ilistTreeView.Images.SetKeyName(13, "");
            this.ilistTreeView.Images.SetKeyName(14, "");
            this.ilistTreeView.Images.SetKeyName(15, "");
            this.ilistTreeView.Images.SetKeyName(16, "");
            this.ilistTreeView.Images.SetKeyName(17, "");
            this.ilistTreeView.Images.SetKeyName(18, "");
            this.ilistTreeView.Images.SetKeyName(19, "");
            this.ilistTreeView.Images.SetKeyName(20, "");
            this.ilistTreeView.Images.SetKeyName(21, "Conditional_Text-new.png");
            this.ilistTreeView.Images.SetKeyName(22, "EasyInterview-new.png");
            this.ilistTreeView.Images.SetKeyName(23, "Functions-new.png");
            this.ilistTreeView.Images.SetKeyName(24, "Outcomes-new.png");
            this.ilistTreeView.Images.SetKeyName(25, "Process_Manager-new.png");
            this.ilistTreeView.Images.SetKeyName(26, "Questions-new.png");
            this.ilistTreeView.Images.SetKeyName(27, "Templates-new.png");
            this.ilistTreeView.Images.SetKeyName(28, "Web_References-new.png");
            this.ilistTreeView.Images.SetKeyName(29, "");
            this.ilistTreeView.Images.SetKeyName(30, "");
            this.ilistTreeView.Images.SetKeyName(31, "");
            this.ilistTreeView.Images.SetKeyName(32, "");
            this.ilistTreeView.Images.SetKeyName(33, "");
            this.ilistTreeView.Images.SetKeyName(34, "");
            this.ilistTreeView.Images.SetKeyName(35, "");
            this.ilistTreeView.Images.SetKeyName(36, "");
            this.ilistTreeView.Images.SetKeyName(37, "");
            this.ilistTreeView.Images.SetKeyName(38, "");
            this.ilistTreeView.Images.SetKeyName(39, "");
            this.ilistTreeView.Images.SetKeyName(40, "");
            this.ilistTreeView.Images.SetKeyName(41, "");
            this.ilistTreeView.Images.SetKeyName(42, "");
            this.ilistTreeView.Images.SetKeyName(43, "");
            this.ilistTreeView.Images.SetKeyName(44, "");
            this.ilistTreeView.Images.SetKeyName(45, "");
            this.ilistTreeView.Images.SetKeyName(46, "");
            this.ilistTreeView.Images.SetKeyName(47, "");
            this.ilistTreeView.Images.SetKeyName(48, "");
            this.ilistTreeView.Images.SetKeyName(49, "");
            this.ilistTreeView.Images.SetKeyName(50, "");
            this.ilistTreeView.Images.SetKeyName(51, "");
            this.ilistTreeView.Images.SetKeyName(52, "");
            this.ilistTreeView.Images.SetKeyName(53, "");
            this.ilistTreeView.Images.SetKeyName(54, "");
            this.ilistTreeView.Images.SetKeyName(55, "");
            this.ilistTreeView.Images.SetKeyName(56, "");
            this.ilistTreeView.Images.SetKeyName(57, "");
            this.ilistTreeView.Images.SetKeyName(58, "");
            this.ilistTreeView.Images.SetKeyName(59, "unlinked.bmp");
            this.ilistTreeView.Images.SetKeyName(60, "start-checked-in.bmp");
            this.ilistTreeView.Images.SetKeyName(61, "start-checked-out.bmp");
            this.ilistTreeView.Images.SetKeyName(62, "start-checked-out-other.bmp");
            this.ilistTreeView.Images.SetKeyName(63, "document_notebook.png");
            this.ilistTreeView.Images.SetKeyName(64, "document_add.png");
            this.ilistTreeView.Images.SetKeyName(65, "Attachments-new.png");
            this.ilistTreeView.Images.SetKeyName(66, "document_question.png");
            this.ilistTreeView.Images.SetKeyName(67, "folder_document.png");
            this.ilistTreeView.Images.SetKeyName(68, "scroll_refresh.png");
            this.ilistTreeView.Images.SetKeyName(69, "blank-new.png");
            this.ilistTreeView.Images.SetKeyName(70, "");
            this.ilistTreeView.Images.SetKeyName(71, "");
            this.ilistTreeView.Images.SetKeyName(72, "");
            this.ilistTreeView.Images.SetKeyName(73, "Attachments-new.png");
            this.ilistTreeView.Images.SetKeyName(74, "");
            this.ilistTreeView.Images.SetKeyName(75, "");
            this.ilistTreeView.Images.SetKeyName(76, "");
            this.ilistTreeView.Images.SetKeyName(77, "");
            this.ilistTreeView.Images.SetKeyName(78, "");
            this.ilistTreeView.Images.SetKeyName(79, "");
            this.ilistTreeView.Images.SetKeyName(80, "folder_add.png");
            this.ilistTreeView.Images.SetKeyName(81, "");
            // 
            // cmExplorer
            // 
            this.cmExplorer.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuPublish,
            this.mnuSeparator1,
            this.mnuOpen,
            this.mnuCreateCopy,
            this.mnuRemoveFromRule,
            this.mnuImportTemplate,
            this.mnuImportAttachment,
            this.mnuLinkQuestionAttachment,
            this.mnuExportTemplate,
            this.mnuRefresh,
            this.mnuRefreshDataSource,
            this.mnuShareLibrarySeparator,
            this.mnuUpdateAllLinkedSharedLibraryItems,
            this.mnuCheckIn,
            this.mnuCheckOut,
            this.mnuUndoCheckOut,
            this.mnuGetLatestVersion,
            this.mnuLinkSharedItem,
            this.mnuUnlinkSharedItem,
            this.mnuDisassociate,
            this.mnuViewReport,
            this.mnuSeparator2,
            this.mnuNewInterview,
            this.mnuNewPage,
            this.mnuNewExternalPage,
            this.mnuNewArithmeticFunction,
            this.mnuNewDateFunction,
            this.mnuNewOutcome,
            this.mnuNewSimpleOutcome,
            this.mnuNewServer,
            this.mnuNewRule,
            this.mnuNewQuestion,
            this.mnuNewTemplate,
            this.mnuNewAttachment,
            this.mnuNewWebReference,
            this.mnuNewFolder,
            this.mnuSeparator3,
            this.mnuAddDocumentToDelivery,
            this.mnuRemoveAllDocumentsFromDelivery,
            this.mnuEditDocumentToDelivery,
            this.mnuAddDeliveryToRule,
            this.mnuAddDistributorToDelivery,
            this.mnuSeparator4,
            this.mnuRename,
            this.mnuDependencies,
            this.mnuDelete});
            // 
            // mnuPublish
            // 
            this.mnuPublish.Index = 0;
            resources.ApplyResources(this.mnuPublish, "mnuPublish");
            this.mnuPublish.Click += new System.EventHandler(this.mnuPublish_Click);
            // 
            // mnuSeparator1
            // 
            this.mnuSeparator1.Index = 1;
            resources.ApplyResources(this.mnuSeparator1, "mnuSeparator1");
            // 
            // mnuOpen
            // 
            this.mnuOpen.Index = 2;
            resources.ApplyResources(this.mnuOpen, "mnuOpen");
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // mnuCreateCopy
            // 
            this.mnuCreateCopy.Index = 3;
            resources.ApplyResources(this.mnuCreateCopy, "mnuCreateCopy");
            this.mnuCreateCopy.Click += new System.EventHandler(this.mnuCreateCopy_Click);
            // 
            // mnuRemoveFromRule
            // 
            this.mnuRemoveFromRule.Index = 4;
            resources.ApplyResources(this.mnuRemoveFromRule, "mnuRemoveFromRule");
            this.mnuRemoveFromRule.Click += new System.EventHandler(this.mnuRemoveFromRule_Click);
            // 
            // mnuImportTemplate
            // 
            this.mnuImportTemplate.Index = 5;
            resources.ApplyResources(this.mnuImportTemplate, "mnuImportTemplate");
            this.mnuImportTemplate.Click += new System.EventHandler(this.mnuImportTemplate_Click);
            // 
            // mnuImportAttachment
            // 
            this.mnuImportAttachment.Index = 6;
            resources.ApplyResources(this.mnuImportAttachment, "mnuImportAttachment");
            this.mnuImportAttachment.Click += new System.EventHandler(this.mnuImportAttachment_Click);
            // 
            // mnuLinkQuestionAttachment
            // 
            this.mnuLinkQuestionAttachment.Index = 7;
            resources.ApplyResources(this.mnuLinkQuestionAttachment, "mnuLinkQuestionAttachment");
            this.mnuLinkQuestionAttachment.Click += new System.EventHandler(this.mnuLinkQuestionAttachment_Click);
            // 
            // mnuExportTemplate
            // 
            this.mnuExportTemplate.Index = 8;
            resources.ApplyResources(this.mnuExportTemplate, "mnuExportTemplate");
            this.mnuExportTemplate.Click += new System.EventHandler(this.mnuExportTemplate_Click);
            // 
            // mnuRefresh
            // 
            this.mnuRefresh.Index = 9;
            resources.ApplyResources(this.mnuRefresh, "mnuRefresh");
            this.mnuRefresh.Click += new System.EventHandler(this.mnuRefresh_Click);
            // 
            // mnuRefreshDataSource
            // 
            this.mnuRefreshDataSource.Index = 10;
            resources.ApplyResources(this.mnuRefreshDataSource, "mnuRefreshDataSource");
            this.mnuRefreshDataSource.Click += new System.EventHandler(this.mnuRefreshDataSource_Click);
            // 
            // mnuShareLibrarySeparator
            // 
            this.mnuShareLibrarySeparator.Index = 11;
            resources.ApplyResources(this.mnuShareLibrarySeparator, "mnuShareLibrarySeparator");
            // 
            // mnuUpdateAllLinkedSharedLibraryItems
            // 
            this.mnuUpdateAllLinkedSharedLibraryItems.Index = 12;
            resources.ApplyResources(this.mnuUpdateAllLinkedSharedLibraryItems, "mnuUpdateAllLinkedSharedLibraryItems");
            this.mnuUpdateAllLinkedSharedLibraryItems.Click += new System.EventHandler(this.mnuUpdateAllLinkedSharedLibraryItems_Click);
            // 
            // mnuCheckIn
            // 
            this.mnuCheckIn.Index = 13;
            resources.ApplyResources(this.mnuCheckIn, "mnuCheckIn");
            this.mnuCheckIn.Click += new System.EventHandler(this.mnuCheckIn_Click);
            // 
            // mnuCheckOut
            // 
            this.mnuCheckOut.Index = 14;
            resources.ApplyResources(this.mnuCheckOut, "mnuCheckOut");
            this.mnuCheckOut.Click += new System.EventHandler(this.mnuCheckOut_Click);
            // 
            // mnuUndoCheckOut
            // 
            this.mnuUndoCheckOut.Index = 15;
            resources.ApplyResources(this.mnuUndoCheckOut, "mnuUndoCheckOut");
            this.mnuUndoCheckOut.Click += new System.EventHandler(this.mnuUndoCheckOut_Click);
            // 
            // mnuGetLatestVersion
            // 
            this.mnuGetLatestVersion.Index = 16;
            resources.ApplyResources(this.mnuGetLatestVersion, "mnuGetLatestVersion");
            this.mnuGetLatestVersion.Click += new System.EventHandler(this.mnuGetLatestVersion_Click);
            // 
            // mnuLinkSharedItem
            // 
            this.mnuLinkSharedItem.Index = 17;
            resources.ApplyResources(this.mnuLinkSharedItem, "mnuLinkSharedItem");
            this.mnuLinkSharedItem.Click += new System.EventHandler(this.mnuLinkSharedItem_Click);
            // 
            // mnuUnlinkSharedItem
            // 
            this.mnuUnlinkSharedItem.Index = 18;
            resources.ApplyResources(this.mnuUnlinkSharedItem, "mnuUnlinkSharedItem");
            this.mnuUnlinkSharedItem.Click += new System.EventHandler(this.mnuUnlinkSharedItem_Click);
            // 
            // mnuDisassociate
            // 
            this.mnuDisassociate.Index = 19;
            resources.ApplyResources(this.mnuDisassociate, "mnuDisassociate");
            this.mnuDisassociate.Click += new System.EventHandler(this.mnuDisassociateSharedLibraryItem_Click);
            // 
            // mnuViewReport
            // 
            this.mnuViewReport.Index = 20;
            resources.ApplyResources(this.mnuViewReport, "mnuViewReport");
            this.mnuViewReport.Click += new System.EventHandler(this.mnuViewReport_Click);
            // 
            // mnuSeparator2
            // 
            this.mnuSeparator2.Index = 21;
            resources.ApplyResources(this.mnuSeparator2, "mnuSeparator2");
            // 
            // mnuNewInterview
            // 
            this.mnuNewInterview.Index = 22;
            resources.ApplyResources(this.mnuNewInterview, "mnuNewInterview");
            this.mnuNewInterview.Click += new System.EventHandler(this.mnuNewInterview_Click);
            // 
            // mnuNewPage
            // 
            this.mnuNewPage.Index = 23;
            resources.ApplyResources(this.mnuNewPage, "mnuNewPage");
            this.mnuNewPage.Click += new System.EventHandler(this.mnuNewPage_Click);
            // 
            // mnuNewExternalPage
            // 
            this.mnuNewExternalPage.Index = 24;
            resources.ApplyResources(this.mnuNewExternalPage, "mnuNewExternalPage");
            this.mnuNewExternalPage.Click += new System.EventHandler(this.mnuNewExternalPage_Click);
            // 
            // mnuNewArithmeticFunction
            // 
            this.mnuNewArithmeticFunction.Index = 25;
            resources.ApplyResources(this.mnuNewArithmeticFunction, "mnuNewArithmeticFunction");
            this.mnuNewArithmeticFunction.Click += new System.EventHandler(this.mnuNewArithmeticFunction_Click);
            // 
            // mnuNewDateFunction
            // 
            this.mnuNewDateFunction.Index = 26;
            resources.ApplyResources(this.mnuNewDateFunction, "mnuNewDateFunction");
            this.mnuNewDateFunction.Click += new System.EventHandler(this.mnuNewDateFunction_Click);
            // 
            // mnuNewOutcome
            // 
            this.mnuNewOutcome.Index = 27;
            resources.ApplyResources(this.mnuNewOutcome, "mnuNewOutcome");
            this.mnuNewOutcome.Click += new System.EventHandler(this.mnuNewOutcome_Click);
            // 
            // mnuNewSimpleOutcome
            // 
            this.mnuNewSimpleOutcome.Index = 28;
            resources.ApplyResources(this.mnuNewSimpleOutcome, "mnuNewSimpleOutcome");
            this.mnuNewSimpleOutcome.Click += new System.EventHandler(this.mnuNewSimpleOutcome_Click);
            // 
            // mnuNewServer
            // 
            this.mnuNewServer.Index = 29;
            resources.ApplyResources(this.mnuNewServer, "mnuNewServer");
            this.mnuNewServer.Click += new System.EventHandler(this.mnuNewServer_Click);
            // 
            // mnuNewRule
            // 
            this.mnuNewRule.Index = 30;
            resources.ApplyResources(this.mnuNewRule, "mnuNewRule");
            this.mnuNewRule.Click += new System.EventHandler(this.mnuNewRule_Click);
            // 
            // mnuNewQuestion
            // 
            this.mnuNewQuestion.Index = 31;
            resources.ApplyResources(this.mnuNewQuestion, "mnuNewQuestion");
            this.mnuNewQuestion.Click += new System.EventHandler(this.mnuNewQuestion_Click);
            // 
            // mnuNewTemplate
            // 
            this.mnuNewTemplate.Index = 32;
            resources.ApplyResources(this.mnuNewTemplate, "mnuNewTemplate");
            this.mnuNewTemplate.Click += new System.EventHandler(this.mnuNewTemplate_Click);
            // 
            // mnuNewAttachment
            // 
            this.mnuNewAttachment.Index = 33;
            resources.ApplyResources(this.mnuNewAttachment, "mnuNewAttachment");
            this.mnuNewAttachment.Click += new System.EventHandler(this.mnuNewAttachment_Click);
            // 
            // mnuNewWebReference
            // 
            this.mnuNewWebReference.Index = 34;
            resources.ApplyResources(this.mnuNewWebReference, "mnuNewWebReference");
            this.mnuNewWebReference.Click += new System.EventHandler(this.mnuNewWebReference_Click);
            // 
            // mnuNewFolder
            // 
            this.mnuNewFolder.Index = 35;
            resources.ApplyResources(this.mnuNewFolder, "mnuNewFolder");
            this.mnuNewFolder.Click += new System.EventHandler(this.mnuNewFolder_Click);
            // 
            // mnuSeparator3
            // 
            this.mnuSeparator3.Index = 36;
            resources.ApplyResources(this.mnuSeparator3, "mnuSeparator3");
            // 
            // mnuAddDocumentToDelivery
            // 
            this.mnuAddDocumentToDelivery.Index = 37;
            resources.ApplyResources(this.mnuAddDocumentToDelivery, "mnuAddDocumentToDelivery");
            this.mnuAddDocumentToDelivery.Click += new System.EventHandler(this.mnuAddDocumentToDelivery_Click);
            // 
            // mnuRemoveAllDocumentsFromDelivery
            // 
            this.mnuRemoveAllDocumentsFromDelivery.Index = 38;
            resources.ApplyResources(this.mnuRemoveAllDocumentsFromDelivery, "mnuRemoveAllDocumentsFromDelivery");
            this.mnuRemoveAllDocumentsFromDelivery.Click += new System.EventHandler(this.mnuRemoveAllDocumentsFromDelivery_Click);
            // 
            // mnuEditDocumentToDelivery
            // 
            this.mnuEditDocumentToDelivery.Index = 39;
            resources.ApplyResources(this.mnuEditDocumentToDelivery, "mnuEditDocumentToDelivery");
            this.mnuEditDocumentToDelivery.Click += new System.EventHandler(this.mnuEditDocumentToDelivery_Click);
            // 
            // mnuAddDeliveryToRule
            // 
            this.mnuAddDeliveryToRule.Index = 40;
            resources.ApplyResources(this.mnuAddDeliveryToRule, "mnuAddDeliveryToRule");
            this.mnuAddDeliveryToRule.Click += new System.EventHandler(this.mnuAddDeliveryToRule_Click);
            // 
            // mnuAddDistributorToDelivery
            // 
            this.mnuAddDistributorToDelivery.Index = 41;
            resources.ApplyResources(this.mnuAddDistributorToDelivery, "mnuAddDistributorToDelivery");
            this.mnuAddDistributorToDelivery.Click += new System.EventHandler(this.mnuAddDistributorToDelivery_Click);
            // 
            // mnuSeparator4
            // 
            this.mnuSeparator4.Index = 42;
            resources.ApplyResources(this.mnuSeparator4, "mnuSeparator4");
            // 
            // mnuRename
            // 
            this.mnuRename.Index = 43;
            resources.ApplyResources(this.mnuRename, "mnuRename");
            this.mnuRename.Click += new System.EventHandler(this.mnuRename_Click);
            // 
            // mnuDependencies
            // 
            this.mnuDependencies.Index = 44;
            resources.ApplyResources(this.mnuDependencies, "mnuDependencies");
            this.mnuDependencies.Click += new System.EventHandler(this.mnuDependencies_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 45;
            resources.ApplyResources(this.mnuDelete, "mnuDelete");
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.docx";
            resources.ApplyResources(this.openFileDialog1, "openFileDialog1");
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.docx";
            resources.ApplyResources(this.saveFileDialog1, "saveFileDialog1");
            // 
            // ExplorerControl
            // 
            this.Controls.Add(this.tvwExplorer);
            resources.ApplyResources(this, "$this");
            this.Name = "ExplorerControl";
            this.ResumeLayout(false);

        }

        #endregion

        #endregion

        #region Treeview - General Methods - Reset, Select, Dispose, Enable, Disable

        /// <summary>
        ///		Called from the form that this control is on. When an item is added to the shared library, an event is raised
        ///		to the main form which calls this method. The item that has been added to the library is updated to reflect
        ///		that it is now a member of the shared library.
        /// </summary>
        /// <param name="libraryItems">The item that has been added to the shared library.</param>
        public void UpdateSelectedLibraryItemNode(LibraryItem libraryItem)
        {
            // Update without prompts
            sharedLibraryEventsDisabled = true;

            if (libraryItem.DependentLibraryItems != null)
            {
                // Update all the library items to ensure they indicate being from the shared library
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    UpdateSelectedLibraryItemNode(dependentLibraryItem);
                }
            }

            if (libraryItem.RelativePath == null || libraryItem.RelativePath.Length == 0)
            {
                LibraryItem packageItem = (LibraryItem)tvwExplorer.SelectedNode.Tag;
                packageItem.RelativePath = libraryItem.RelativePath;
            }

            libraryItem.ParentPackage = package;
            libraryItem.IsNewLibraryItem = false;
            SetLibraryItemTreeNodeIcon(libraryItem);
            SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());

            sharedLibraryEventsDisabled = false;
        }

        private string GetCurrentWindowsUsername()
        {
            WindowsIdentity windowsIdentity = WindowsIdentity.GetCurrent();

            if (windowsIdentity != null)
            {
                return windowsIdentity.Name;
            }
            
            return string.Empty;
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void ResetTree()
        {
            try
            {
                tvwExplorer.SuspendLayout();
                suspended = true;

                DisableControl();
                tvwExplorer.Hide();

                if (propertyGrid != null)
                {
                    propertyGrid.Hide();
                }

                ResetTree(rootNode);
            }
            finally
            {
                tvwExplorer.ResumeLayout();
                suspended = false;
                EnableControl();

                if (propertyGrid != null)
                {
                    propertyGrid.Show();
                    propertyGrid.Refresh();
                }

                tvwExplorer.Show();
            }
        }

        private void ResetTreeMaintainSelection(PackageItem item)
        {
            ResetTreeWithCollapse();
            SelectItem(item);
        }

        public void ResetTreeWithCollapse()
        {
            ResetTree();
            tvwExplorer.Refresh();
            tvwExplorer.CollapseAll();
            tvwExplorer.Nodes[0].Expand();
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------

        public TreeNode SelectItem(string name, Guid itemUid)
        {
            PackageItem item = (PackageItem)package.GetLibraryItemByUniqueIdentifier(itemUid);

            if (item != null)
            {
                return SelectItem(item);
            }
            else
            {
                string title = GetResource("PackageExplorer.ExplorerControl.DeletedPackageItemTitle");
                string message = GetResource("PackageExplorer.ExplorerControl.DeletedPackageItemMessage");
                message = string.Format(message, name);
 
                // The selected item in the designer does not exist in the package - display warning to the user
                MessageBox.Show(DesktopWindow.Instance, message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return null;
            }
        }

        delegate TreeNode SetSelectedNodeCallback(PackageItem item);

        public TreeNode SelectItem(PackageItem item)
        {
            TreeNode nodeToSelect = null;

            if (item == null)
            {
                tvwExplorer.SelectedNode = null;
                if (propertyGrid != null)
                {
                    SetPropertyGridSelectedObject(null);
                }
            }
            else
            {
                PfTreeNodeCollection c = guidNodeMap[item.UniqueIdentifier];
                if (c != null && c.Count > 0)
                {
                    nodeToSelect = c[0];

                    if (!tvwExplorer.InvokeRequired)
                    {
                        tvwExplorer.SelectedNode = nodeToSelect;
                        nodeToSelect.EnsureVisible();
                    }
                    else 
                    { 
                        SetSelectedNodeCallback d = new SetSelectedNodeCallback(SelectItem);
                        tvwExplorer.Invoke(d, item);
                    }                    
                }
            }
            
            return nodeToSelect;
        }

        /*
        private void SelectTreeNodeAsync(TreeNode node)
        {
            if (tvwExplorer..InvokeRequired)
            {
                SetProgressCallback d = new SetProgressCallback(UpdateProgressBar);
                this.Invoke(d, new object[] { progress });
            }
            else
            {
                timer1.Enabled = false;

                // ensure the progress value is valid
                if (progress < 0 || progress > this.progressBar1.Maximum)
                {
                    progress = 0;
                }

                this.progressBar1.Value = progress;
            }
        }
         * */

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void ResetTree(TreeNode n)
        {
            if (n == rootNode)
            {
                tvwExplorer.Nodes.Clear();
                guidNodeMap.Clear();

                // Set up the root nodes			
                string localQuestionsNodeName = GetResource("PackageExplorer.ExplorerControl.QuestionsNodeName");
                string localOutcomesNodeName = GetResource("PackageExplorer.ExplorerControl.OutcomesNodeName");
                string localFunctionsNodeName = GetResource("PackageExplorer.ExplorerControl.FunctionsNodeName");
                string localTemplatesNodeName = GetResource("PackageExplorer.ExplorerControl.TemplatesNodeName");
                string localAttachmentsNodeName = GetResource("PackageExplorer.ExplorerControl.AttachmentsNodeName");
                string localWebReferencesNodeName = GetResource("PackageExplorer.ExplorerControl.WebReferencesNodeName");
                string localInterviewNodeName = GetResource("PackageExplorer.ExplorerControl.InterviewNodeName");
                string localServersNodeName = GetResource("PackageExplorer.ExplorerControl.ServersNodeName");
                string localSimpleOutcomesNodeName = GetResource("PackageExplorer.ExplorerControl.SimpleOutcomesNodeName");

                //UI Refresh - no icon for root
                //rootNode = new TreeNode("No package loaded", (int)TreeIcons.FolderPackage, (int)TreeIcons.FolderPackage);
                rootNode = new TreeNode("New Package", (int)TreeIcons.BlankIcon, (int)TreeIcons.BlankIcon);
                rootNode.ForeColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumBlue;
                questionsNode = new TreeNode(localQuestionsNodeName, (int)TreeIcons.FolderQuestions, (int)TreeIcons.FolderQuestions);
                outcomesNode = new TreeNode(localOutcomesNodeName, (int) TreeIcons.FolderOutcomes, (int) TreeIcons.FolderOutcomes);
                functionsNode = new TreeNode(localFunctionsNodeName, (int) TreeIcons.FolderFunctions, (int) TreeIcons.FolderFunctions);
                templatesNode = new TreeNode(localTemplatesNodeName, (int) TreeIcons.FolderTemplates, (int) TreeIcons.FolderTemplates);
                attachmentsNode = new TreeNode(localAttachmentsNodeName, (int)TreeIcons.FolderAttachment, (int)TreeIcons.FolderAttachment);
                webReferencesNode = new TreeNode(localWebReferencesNodeName, (int) TreeIcons.FolderWebReferences, (int) TreeIcons.FolderWebReferences);
                
                string interviewNodeName;
                interviewNodeName = localInterviewNodeName;
                interviewNode = new TreeNode(interviewNodeName, (int) TreeIcons.FolderEasyInterview, (int) TreeIcons.FolderEasyInterview);
                serversNode = new TreeNode(localServersNodeName, (int) TreeIcons.FolderProcessManagers, (int) TreeIcons.FolderProcessManagers);
                simpleOutcomesNode = new TreeNode(localSimpleOutcomesNodeName, (int) TreeIcons.FolderSimpleOutcome,                                  (int) TreeIcons.FolderSimpleOutcome);
                rootNode.Nodes.AddRange(new TreeNode[] { templatesNode, attachmentsNode, simpleOutcomesNode, outcomesNode,
                                                         questionsNode, webReferencesNode, functionsNode, interviewNode,
                                                         serversNode});
                tvwExplorer.Nodes.Add(rootNode);
                tvwExplorer.TreeViewNodeSorter = new NodeSorter();

                if (package == null)
                {
                    DisableControl();
                }
                else
                {
                    PackageNameBind();
                    rootNode.Tag = package;
                    interviewNode.Tag = package.Interviews;

                    foreach (Interview ivw in package.Interviews)
                    {
                        AddNode(ivw);
                        foreach (Page p in ivw.Pages)
                        {
                            AddNode(p);
                        }
                    }
                    
                    templatesNode.Tag = package.Templates;
                    foreach (TemplateDocument tpt in package.Templates)
                    {
                        AddNode(tpt);
                    }

                    attachmentsNode.Tag = package.Attachments;

                    foreach (AttachmentFolder af in package.AttachmentFolders)
                    {
                        AddNode(af);
                        FolderCollection subFolders = af.GetChildFolders();
                        foreach (AttachmentFolder subOf in subFolders)
                        {
                            AddNode(subOf);
                        }
                    }
                    
                    foreach (AttachmentBase attachment in package.Attachments)
                    {
                        AddNode(attachment);
                    }

                    outcomesNode.Tag = package.Outcomes;

                    foreach (OutcomeFolder of in package.OutcomeFolders)
                    {
                        AddNode(of);
                        FolderCollection subFolders = of.GetChildFolders();
                        foreach (OutcomeFolder subOf in subFolders)
                        {
                            AddNode(subOf);
                        }
                    }

                    foreach (Outcome o in package.Outcomes)
                    {
                        AddNode(o);
                    }

                    simpleOutcomesNode.Tag = package.SimpleOutcomes;
                    foreach (SimpleOutcomeFolder sof in package.SimpleOutcomeFolders)
                    {
                        AddNode(sof);
                        FolderCollection subFolders = sof.GetChildFolders();
                        foreach (SimpleOutcomeFolder subSof in subFolders)
                        {
                            AddNode(subSof);
                        }
                    }
                    foreach (SimpleOutcome so in package.SimpleOutcomes)
                    {
                        AddNode(so);
                    }

                    // Add the functions
                    functionsNode.Tag = package.Functions;
                    foreach (PFunctionFolder pFunctionFolder in package.FunctionFolders)
                    {
                        AddNode(pFunctionFolder);

                        FolderCollection pFunctionSubFolders = pFunctionFolder.GetChildFolders();
                        foreach (PFunctionFolder pFunctionSubFolder in pFunctionSubFolders)
                        {
                            AddNode(pFunctionSubFolder);
                        }
                    }
                    foreach (PFunction f in package.Functions)
                    {
                        AddNode(f);
                    }

                    serversNode.Tag = package.Servers;
                    foreach (Server s in package.Servers)
                    {
                        AddNode(s);
                        
                        foreach (Rule2 r in s.Rule2)
                        {
                            AddNode(r);

                            foreach (Delivery delivery in r.Deliveries)
                            {
                                AddRuleDeliveryNode(r,delivery);

                                AddRuleDistributorNode(delivery, delivery.Distributor);
        
                                foreach (IDocument doc in delivery.Documents)
                                    AddRuleDocumentNode(delivery, doc);
                            }
                        }
                    }

                    webReferencesNode.Tag = package.DataSources;
                    foreach (DataSource d in package.DataSources)
                    {
                        AddNode(d);
                    }
                    questionsNode.Tag = package.Questions;

                    foreach (QuestionFolder qf in package.QuestionsFolders)
                    {
                        AddNode(qf);
                        FolderCollection subFolders = qf.GetChildFolders();
                        foreach (QuestionFolder subQf in subFolders)
                        {
                            AddNode(subQf);
                        }
                    }

                    foreach (Question q in package.Questions)
                    {
                        AddNode(q);
                    }

                    //tvwExplorer.Sort();
                }
            }
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void DisableControl()
        {
            tvwExplorer.Enabled = false;
        }

        private void EnableControl()
        {
            tvwExplorer.Enabled = true;
        }

        // ---------------------------------------------------------------------------------------
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void DisplayErrorToUI(Exception ex)
        {
            string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
            MessageBox.Show(message, GetResource("PackageExplorer.ExplorerControl.ExplorerPaneError"), MessageBoxButtons.OK, 
                            MessageBoxIcon.Exclamation);
        }

        #endregion

        #region Treeview - Set Context Menus

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void tvwExplorer_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (propertyGrid != null)
            {
                if (e.Node.Tag != null)
                {
                    if (e.Node.Tag is LibraryItem)
                    {
                        SetPropertyGridSelectedObject((LibraryItem)e.Node.Tag, GetCurrentWindowsUsername(), e.Node);
                    }
                    else
                    {
                        SetPropertyGridSelectedObject(e.Node.Tag);
                    }
                }
                else
                {
                    SetPropertyGridSelectedObject(null);
                }

                propertyGrid.Refresh();
            }

            SetCommands();
        }

        private void SetSharedLibraryMenu(TreeNode selectedNode)
        {
            LibraryItem libraryItem = (LibraryItem) selectedNode.Tag;

            try
            {
                // Only show the shared library menu if the shared library is activated and the item has been linked to the library
                if (sharedLibraryController.IsSharedLibraryActivated() && libraryItem.LibraryUniqueIdentifier != Guid.Empty)
                {
                    // Ensure the shared library separator is shown and also for library items always show menu items
                    MenuItemOn(mnuShareLibrarySeparator);
                    MenuItemOn(mnuDisassociate);

                    if (reportingEnabled)
                    {
                        MenuItemOn(mnuViewReport);
                    }

                    if (libraryItem.Linked)
                    {
                        // Handle a linked shared library item
                        if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedOut &&
                            libraryItem.CheckedOutByLogInName != null &&
                            libraryItem.CheckedOutByLogInName.ToLower() == WindowsIdentity.GetCurrent().Name.ToLower())
                        {
                            // Checked out item
                            MenuItemOn(mnuCheckIn);
                            MenuItemOn(mnuGetLatestVersion);
                            MenuItemOn(mnuUndoCheckOut);
                        }
                        else
                        {
                            // Checked in item
                            MenuItemOn(mnuCheckOut);
                            MenuItemOn(mnuGetLatestVersion);
                            MenuItemOn(mnuUnlinkSharedItem);
                        }
                    }
                    else
                    {
                        // Unlinked item - give menu option to relink
                        MenuItemOn(mnuLinkSharedItem);
                    }
                }
            }
            catch (Exception ex)
            {
                string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                MessageBox.Show(message, GetResource("PackageExplorer.ExplorerControl.ExplorerPaneError"), MessageBoxButtons.OK, 
                                MessageBoxIcon.Exclamation);
            }
        }

        private void MenuItemOn(MenuItem i)
        {
            i.Enabled = true;
            i.Visible = true;
        }

        private void MenuItemOff(MenuItem i)
        {
            i.Enabled = false;
            i.Visible = false;
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------

        private void HideContextMenuItems()
        {
            foreach (MenuItem mi in cmExplorer.MenuItems)
            {
                MenuItemOff(mi);
            }
        }

        private void SetPackageMenu(TreeNode n)
        {
            if (n != null && n.Tag != null)
            {
                if (n.Tag is Package)
                {
                    MenuItemOn(mnuNewQuestion);
                    MenuItemOn(mnuSeparator2);
                    MenuItemOn(mnuSeparator1);
                    MenuItemOn(mnuPublish);
                    MenuItemOn(mnuNewServer);
                    MenuItemOn(mnuNewWebReference);
                    MenuItemOn(mnuImportTemplate);
                    MenuItemOn(mnuImportAttachment);
                    MenuItemOn(mnuLinkQuestionAttachment);
                    MenuItemOn(mnuNewArithmeticFunction);
                    MenuItemOn(mnuNewDateFunction);
                    MenuItemOn(mnuNewOutcome);
                    MenuItemOn(mnuNewTemplate);
                    MenuItemOn(mnuNewAttachment);
                    MenuItemOn(mnuNewSimpleOutcome);
                    MenuItemOn(mnuSeparator4);
                    MenuItemOn(mnuRename);

                    if (sharedLibraryController.IsSharedLibraryClientActivated())
                    {
                        MenuItemOn(mnuUpdateAllLinkedSharedLibraryItems);
                    }

                    EnableImportPluginMenuItems();
                    // No point in right-clicking on a package to export a template
                    // as the template needs to be selected in the tree to succeed
                    //EnableExportPluginMenuItems();
                    //cmAddSimpleOutcome.Enabled = true;

                    if (package.Interviews.Count == 0)
                    {
                        MenuItemOn(mnuNewInterview);
                    }
                    if (package.Interviews.Count == 1)
                    {
                        MenuItemOn(mnuNewPage);
                        MenuItemOn(mnuNewExternalPage);
                    }
                    if (package.Interviews.Count > 0)
                    {
                        MenuItemOff(mnuNewInterview);
                    }
                }
            }
        }

        private void SetInterviewCollectionMenu()
        {
            if (package.Interviews.Count == 0)
            {
                MenuItemOn(mnuNewInterview);
            }
            if (package.Interviews.Count == 1)
            {
                MenuItemOn(mnuNewPage);
                MenuItemOn(mnuNewExternalPage);
            }
            if (package.Interviews.Count > 0)
            {
                MenuItemOff(mnuNewInterview);
            }
        }

        private void SetInterviewMenu()
        {
            MenuItemOn(mnuNewPage);
            MenuItemOn(mnuNewExternalPage);
            MenuItemOn(mnuOpen);
            MenuItemOn(mnuSeparator2);
            MenuItemOn(mnuSeparator4);
            MenuItemOn(mnuDelete);
            MenuItemOn(mnuRename);
        }

        private void SetCommands()
        {
            if (! suspended)
            {
                // Hide everything
                HideContextMenuItems();

                if (package.Interviews.Count > 0)
                {
                    //cmAddWebInterview.Enabled = false;
                }
                else
                {
                    //cmAddWebInterview.Enabled = true;
                }

                if (package.Interviews.Count == 1)
                {
                    //cmAddPage.Enabled = true;
                }

                TreeNode n = tvwExplorer.SelectedNode;

                if (n != null && n.Tag != null)
                {
                    if (n.Tag is Package)
                    {
                        SetPackageMenu(n);
                    }
                    else if (n.Tag is InterviewCollection)
                    {
                        SetInterviewCollectionMenu();
                    }
                    else if (n.Tag is Interview)
                    {
                        SetInterviewMenu();
                    }
                    else if (n.Tag is Page)
                    {
                        MenuItemOn(mnuOpen);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);                  

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }

                    else if (n.Tag is PFunctionCollection)
                    {
                        MenuItemOn(mnuNewDateFunction);
                        MenuItemOn(mnuNewArithmeticFunction);
                        MenuItemOn(mnuNewFolder);
                    }
                    else if (n.Tag is PFunction)
                    {
                        MenuItemOn(mnuCreateCopy);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                        MenuItemOn(mnuDependencies);

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }
                    else if (n.Tag is OutcomeCollection)
                    {
                        MenuItemOn(mnuNewOutcome);
                        MenuItemOn(mnuNewFolder);
                    }
                    else if (n.Tag is SimpleOutcomeCollection)
                    {
                        MenuItemOn(mnuNewSimpleOutcome);
                        MenuItemOn(mnuNewFolder);
                    }
                    else if (n.Tag is Outcome)
                    {
                        MenuItemOn(mnuCreateCopy);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);                        

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }
                    else if (n.Tag is SimpleOutcome)
                    {
                        MenuItemOn(mnuCreateCopy);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }
                    else if (n.Tag is ServerCollection)
                    {
                        MenuItemOn(mnuNewServer);
                    }
                    else if (n.Tag is Server)
                    {                        
                        MenuItemOn(mnuSeparator2);
                        MenuItemOn(mnuNewRule);
                        MenuItemOn(mnuRefresh);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is Rule2)
                    {
                        MenuItemOn(mnuAddDeliveryToRule);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is Delivery)
                    {
                        MenuItemOn(this.mnuAddDistributorToDelivery);
                        MenuItemOn(this.mnuAddDocumentToDelivery);
                        MenuItemOn(this.mnuRemoveAllDocumentsFromDelivery);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is Distributor)
                    {
                        MenuItemOn(mnuRemoveFromRule);
                    }
                    else if (n.Tag is DeliveryDoc)
                    {
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                        MenuItemOn(mnuEditDocumentToDelivery);
                    }
                    else if( n.Tag is DocumentFolderTreeNode )
                    {
                        MenuItemOn(mnuAddDocumentToDelivery);
                        MenuItemOn(mnuRemoveAllDocumentsFromDelivery);

                    }
                    else if( n.Tag is AttachmentCollection )
                    {
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuImportAttachment);
                        MenuItemOn(mnuLinkQuestionAttachment);
                    }
                    else if (n.Tag is AttachmentBase )
                    {
                        MenuItemOn( mnuRename );
                        MenuItemOn(mnuDelete);

                        // FB 2175: Add Dependencies menu item to attachments, only if a source attachment.
                        if( ((AttachmentBase)n.Tag).AttachmentSource )
                        //if( n.Parent.Tag != null && (n.Parent.Tag is AttachmentCollection || n.Parent.Tag is AttachmentFolder) )
                            MenuItemOn( mnuDependencies );
                    }
                    else if (n.Tag is TemplateDocumentCollection)
                    {
                        MenuItemOn(mnuSeparator2);
                        MenuItemOn(mnuNewTemplate);
                        MenuItemOn(mnuImportTemplate);
                        EnableImportPluginMenuItems();
                    }
                    else if (n.Tag is TemplateDocument && n.Parent.Tag != null && n.Parent.Tag is TemplateDocumentCollection)
                    {
                        MenuItemOn(mnuExportTemplate);
                        EnableExportPluginMenuItems();
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn( mnuDependencies );
                        MenuItemOn( mnuRename );
                        MenuItemOn(mnuDelete);

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }
                    else if (n.Tag is QuestionCollection)
                    {
                        MenuItemOn(mnuNewQuestion);
                        MenuItemOn(mnuNewFolder);
                    }
                    else if (n.Tag is Question)
                    {
                        MenuItemOn(mnuCreateCopy);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                        MenuItemOn(mnuDependencies);

                        // Handle Shared Library
                        SetSharedLibraryMenu(n);
                    }
                    else if (n.Tag is DataSourceCollection)
                    {
                        MenuItemOn(mnuNewWebReference);
                    }
                    else if (n.Tag is DataSource)
                    {
                        MenuItemOn(mnuRefreshDataSource);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is QuestionFolder)
                    {
                        MenuItemOn(mnuNewQuestion);
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is AttachmentFolder)
                    {
                        MenuItemOn(mnuImportAttachment);
                        MenuItemOn(mnuLinkQuestionAttachment);
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is OutcomeFolder)
                    {
                        MenuItemOn(mnuNewOutcome);
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is SimpleOutcomeFolder)
                    {
                        MenuItemOn(mnuNewSimpleOutcome);
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                    else if (n.Tag is PFunctionFolder)
                    {
                        MenuItemOn(mnuNewArithmeticFunction);
                        MenuItemOn(mnuNewDateFunction);
                        MenuItemOn(mnuNewFolder);
                        MenuItemOn(mnuSeparator4);
                        MenuItemOn(mnuRename);
                        MenuItemOn(mnuDelete);
                    }
                }
            }
        }

        private void EnableImportPluginMenuItems()
        {
            if (importTemplateMenuItems != null)
            {
                foreach (object o in importTemplateMenuItems)
                {
                    if (o is MenuItem)
                    {
                        MenuItemOn((MenuItem) o);
                    }
                }
            }
        }

        private void EnableExportPluginMenuItems()
        {
            if (exportTemplateMenuItems != null)
            {
                foreach (object o in exportTemplateMenuItems)
                {
                    if (o is MenuItem)
                    {
                        MenuItemOn((MenuItem) o);
                    }
                }
            }
        }

        #endregion

        #region Treeview - Main Mehtods - Create, Delete, Modify nodes etc

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void DoNewInterview()
        {
            package.CreateInterview(true);
        }

        private void UpdateNode(PackageItem itemToUpdate)
        {
            if (itemToUpdate is Question)
            {
                Package.UpdatePackageItem(itemToUpdate);
            }
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void AddNode(PackageItem itemToAdd)
        {
            AddNode(itemToAdd, false);
        }

        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        private void AddNode(PackageItem itemToAdd, bool isEditableAfterInsert)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                TreeNode newNode = new TreeNode();

                string nodeName;
                int nodeImageIndex;
                object nodeTag;
                TreeNode parentNode;

                // Set the nodes icon
                int nodeIcon = DiscoverLibraryItemTreeNodeIconToSet(itemToAdd);

                //colour blue
                newNode.ForeColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumBlue;

                //debug package contents
                //Console.WriteLine(itemToAdd.Name);
                //System.Diagnostics.Debug.Print(itemToAdd.Name);

                if (itemToAdd is Interview)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int) TreeIcons.Interview;
                    nodeTag = itemToAdd;
                    parentNode = interviewNode;
                }
                else if (itemToAdd is Question)
                {
                    //Question
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;

                    // if we are at root add to questions node, else to folder
                    if ((QuestionFolder) itemToAdd.ParentFolder == null)
                    {
                        parentNode = questionsNode;
                    }
                    else
                    {
                        // Find the parent node (another question folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((Question) itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderQuestion"));
                        }
                    }
                }
                else if (itemToAdd is TemplateDocument)
                {
                    // Template
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;
                    parentNode = templatesNode;
                }
                else if (itemToAdd is AttachmentBase)
                {
                    if ( itemToAdd is AttachmentDynamic )
                        nodeImageIndex = (int)TreeIcons.AttachmentInterviewIcon;
                    else
                    nodeImageIndex = (int)TreeIcons.AttachmentIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;

                    // if we are at root add to Outcomes node, else to folder
                    if ((AttachmentFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = attachmentsNode;
                    }
                    else
                    {
                        // Find the parent node (another outcome folder)
                        PfTreeNodeCollection tnc = guidNodeMap[itemToAdd.ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderOutcome"));
                        }
                    }
                }
                else if (itemToAdd is Outcome)
                {
                    // Outcome
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;

                    // if we are at root add to Outcomes node, else to folder
                    if ((OutcomeFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = outcomesNode;
                    }
                    else
                    {
                        // Find the parent node (another outcome folder)
                        PfTreeNodeCollection tnc = guidNodeMap[itemToAdd.ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderOutcome"));
                        }
                    }
                }
                else if (itemToAdd is SimpleOutcome)
                {
                    // Simple Outcome
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;

                    // if we are at root add to simpleOutcomes node, else to folder
                    if (itemToAdd.ParentFolder == null)
                    {
                        parentNode = simpleOutcomesNode;
                    }
                    else
                    {
                        // Find the parent node (another simpleOutcome folder)
                        PfTreeNodeCollection tnc = guidNodeMap[itemToAdd.ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderSimpleOutcome"));
                        }
                    }
                }
                else if (itemToAdd is DatePFunction)
                {
                    // Date Function
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;
                    if ((PFunctionFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = functionsNode;
                    }
                    else
                    {
                        // Find the parent node (another function folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((DatePFunction)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFunction"));
                        }
                    }
                }
                else if (itemToAdd is ArithmeticPFunction)
                {
                    // Arithmetic Function
                    nodeImageIndex = nodeIcon;
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;
                    if ((PFunctionFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = functionsNode;
                    }
                    else
                    {
                        // Find the parent node (another function folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((ArithmeticPFunction)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFunction"));
                        }
                    }
                }
                else if (itemToAdd is Server)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.Server;
                    nodeTag = itemToAdd;
                    parentNode = serversNode;
                }
                else if (itemToAdd is Rule2)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.Rule;
                    nodeTag = itemToAdd;

                    // Find the node for the parent Server

                    PfTreeNodeCollection tnc = guidNodeMap[((Rule2)itemToAdd).ParentServer.UniqueIdentifier];
                    if (tnc != null && tnc.Count > 0)
                    {
                        parentNode = tnc[0];
                    }
                    else
                    {
                        throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderServer"));
                    }
                }
                else if (itemToAdd is Delivery)
                {
                    nodeName = itemToAdd.Name;
                    newNode.ImageIndex = (int)TreeIcons.DeliveryIcon;
                    nodeImageIndex = (int)TreeIcons.DeliveryIcon;
                    nodeTag = itemToAdd;
                    PfTreeNodeCollection tnc = guidNodeMap[((Delivery)itemToAdd).ParentRule.UniqueIdentifier];
                    if (tnc != null && tnc.Count > 0)
                    {
                        parentNode = tnc[0];
                    }
                    else
                    {
                        throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFunction"));
                    }
                }
                else if (itemToAdd is IDocument)
                {
                    nodeName = itemToAdd.Name;
                    nodeTag = itemToAdd;

                    if (itemToAdd is IAttachment)
                    {
                        if (itemToAdd is AttachmentDynamic)
                        {
                            newNode.ImageIndex = (int)TreeIcons.AttachmentInterviewIcon;
                            nodeImageIndex = (int)TreeIcons.AttachmentInterviewIcon;
                        }
                        else
                        {
                            newNode.ImageIndex = (int)TreeIcons.AttachmentIcon;
                            nodeImageIndex = (int)TreeIcons.AttachmentIcon;
                        }
                    }
                    else
                    {
                        newNode.ImageIndex = (int)TreeIcons.DocumentIcon;
                        nodeImageIndex = (int)TreeIcons.DocumentIcon;
                    }
                    PfTreeNodeCollection tnc = guidNodeMap[((IDocument)itemToAdd).ParentDelivery.UniqueIdentifier];
                    if (tnc != null && tnc.Count > 0)
                    {
                        parentNode = tnc[0];
                        foreach (TreeNode n in parentNode.Nodes)
                        {
                            if( n.Tag is DocumentFolderTreeNode )
                            {
                                parentNode = n;
                                break;
                            }
                        }
                    }
                    else
                    {
                        throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFunction"));
                    }
                }
                else if (itemToAdd is WebReference)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.URL;
                    nodeTag = itemToAdd;
                    parentNode = webReferencesNode;
                    DecorateServiceNode.Decorate((WebReference)itemToAdd, newNode, ((WebReference)itemToAdd).LayoutXml,
                                                 ServiceNodeFilter.All);
                }
                else if (itemToAdd is Page)
                {
                    nodeName = itemToAdd.Name;
                    Page pg = (Page)itemToAdd;
                    if (pg.ParentInterview.StartPage == pg)
                    {
                        nodeImageIndex = (int)TreeIcons.StartPage;
                        newNode.NodeFont =
                            new Font(tvwExplorer.Font.FontFamily.Name, tvwExplorer.Font.Size, FontStyle.Bold);
                    }
                    else
                    {
                        if (itemToAdd is InternalPage)
                        {
                            nodeImageIndex = nodeIcon;
                        }
                        else
                        {
                            nodeImageIndex = (int)TreeIcons.ExternalPage;
                        }
                    }
                    nodeTag = itemToAdd;

                    // Find the node for the parent interview
                    PfTreeNodeCollection tnc = guidNodeMap[((Page)itemToAdd).ParentInterview.UniqueIdentifier];
                    if (tnc != null && tnc.Count > 0)
                    {
                        parentNode = tnc[0];
                    }
                    else
                    {
                        throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderInterview"));
                    }
                }
                else if (itemToAdd is QuestionFolder)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                    nodeTag = itemToAdd;

                    // if we are at root, add to questions node
                    if ((QuestionFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = questionsNode;
                    }
                    else
                    {
                        // Find the parent node (another question folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((QuestionFolder)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderQuestionFolder"));
                        }
                    }
                }
                else if (itemToAdd is AttachmentFolder)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                    nodeTag = itemToAdd;

                    // if we are at root, add to outcome node
                    if (itemToAdd.ParentFolder == null)
                    {
                        parentNode = attachmentsNode;
                    }
                    else
                    {
                        // Find the parent node (another outcome folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((AttachmentFolder)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFolder"));
                        }
                    }
                }
                else if (itemToAdd is OutcomeFolder)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                    nodeTag = itemToAdd;

                    // if we are at root, add to outcome node
                    if (itemToAdd.ParentFolder == null)
                    {
                        parentNode = outcomesNode;
                    }
                    else
                    {
                        // Find the parent node (another outcome folder)
                        PfTreeNodeCollection tnc = guidNodeMap[((OutcomeFolder)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFolder"));
                        }
                    }
                }
                else if (itemToAdd is SimpleOutcomeFolder)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                    nodeTag = itemToAdd;

                    // if we are at root, add to simple outcomes node
                    if ((SimpleOutcomeFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = simpleOutcomesNode;
                    }
                    else
                    {
                        // Find the parent node (another simpleoutcome folder)
                        PfTreeNodeCollection tnc =
                            guidNodeMap[((SimpleOutcomeFolder)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFolder"));
                        }
                    }
                }
                else if (itemToAdd is PFunctionFolder)
                {
                    nodeName = itemToAdd.Name;
                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                    nodeTag = itemToAdd;

                    // if we are at root, add to simple outcomes node
                    if ((PFunctionFolder)itemToAdd.ParentFolder == null)
                    {
                        parentNode = functionsNode;
                    }
                    else
                    {
                        // Find the parent node (another simpleoutcome folder)
                        PfTreeNodeCollection tnc =
                            guidNodeMap[((PFunctionFolder)itemToAdd).ParentFolder.UniqueIdentifier];
                        if (tnc != null && tnc.Count > 0)
                        {
                            parentNode = tnc[0];
                        }
                        else
                        {
                            throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControls.NoParentFolderFolder"));
                        }
                    }
                }

                else
                {
                    throw new NotImplementedException();
                }

                // Bit of a hack, so that folds sit at top even when sorted alphabetically, need to come up with something nicer than this.
                if (itemToAdd is QuestionFolder || itemToAdd is OutcomeFolder || itemToAdd is SimpleOutcomeFolder ||
                    itemToAdd is PFunctionFolder || itemToAdd is AttachmentFolder)
                {
                    string firstChar = nodeName.Substring(0, 1);
                    if (firstChar.Equals(" "))
                    {
                        newNode.Text = nodeName;
                    }
                    else
                    {
                        newNode.Text = '\u0020' + nodeName;
                    }
                }
                else
                {
                    newNode.Text = nodeName;
                }

                newNode.Tag = nodeTag;
                newNode.SelectedImageIndex = nodeImageIndex;
                newNode.ImageIndex = nodeImageIndex;

                parentNode.Nodes.Add(newNode);

                // See if there's already one, and if so, add to its collection in the nodemap
                PfTreeNodeCollection c = guidNodeMap[itemToAdd.UniqueIdentifier];
                if (c == null)
                {
                    c = new PfTreeNodeCollection();
                    guidNodeMap[itemToAdd.UniqueIdentifier] = c;
                }
                else
                {
                    if (c.Count > 1 && !(itemToAdd.AllowDuplicatesInPackage))
                    {
                        throw new Exception(GetResource("PackageExplorer.ExplorerControls.ItemAlreadyExists"));
                    }
                }

                c.Add(newNode);

                itemToAdd.PropertyChanged += new PropertyChangedEventHandler(itemToAdd_PropertyChanged);

                if (!suspended)
                {
                    newNode.EnsureVisible();
                    if (isEditableAfterInsert)
                    {
                        savedNode = newNode;
                        newNode.BeginEdit();
                    }
                }
            }
            finally
            {
                Cursor = Cursors.Default;

                if (!suspended)
                {
                    tvwExplorer.Sorted = false;
                    tvwExplorer.Sorted = true;
                    tvwExplorer.Refresh();
                    tvwExplorer.SelectedNode = savedNode;
                    if (null != savedNode)
                        savedNode.BeginEdit();
                }
            }
        }

        private int DiscoverLibraryItemTreeNodeIconToSet(PackageItem libraryItem)
        {
            CheckOutStatus libraryItemStatus;

            // Only continue if the node is a library item
            if (!(libraryItem is LibraryItem))
                return -1;

            // Library Item - Get its shared library status
            libraryItemStatus = GetLibraryItemSharedLibraryStatus((LibraryItem) libraryItem);

            // Assign the appropriate treenode icon according to its library status
            if (libraryItem is PFunction)
            {
                if (libraryItem is DatePFunction)
                {
                    switch (libraryItemStatus)
                    {
                        case CheckOutStatus.CheckedIn:
                            return (int) TreeIcons.DateFunctionCheckedIn;
                        case CheckOutStatus.CheckedOutOtherUser:
                            return (int) TreeIcons.DateFunctionCheckedOutOtherUser;
                        case CheckOutStatus.CheckedOut:
                            return (int) TreeIcons.DateFunctionCheckedOut;
                        case CheckOutStatus.Unlinked:
                            return (int) TreeIcons.DateFunctionUnlinked;
                        default:
                            return (int) TreeIcons.DateFunction;
                    }
                }
                else
                {
                    switch (libraryItemStatus)
                    {
                        case CheckOutStatus.CheckedIn:
                            return (int) TreeIcons.ArithmeticFunctionCheckedIn;
                        case CheckOutStatus.CheckedOutOtherUser:
                            return (int) TreeIcons.ArithmeticFunctionCheckedOutOtherUser;
                        case CheckOutStatus.CheckedOut:
                            return (int) TreeIcons.ArithmeticFunctionCheckedOut;
                        case CheckOutStatus.Unlinked:
                            return (int) TreeIcons.ArithmeticFunctionUnlinked;
                        default:
                            return (int) TreeIcons.ArithmeticFunction;
                    }
                }
            }
            else if (libraryItem is InterviewPage)
            {
                switch (libraryItemStatus)
                {
                    case CheckOutStatus.CheckedIn:
                        return (int) TreeIcons.InternalPageCheckedIn;
                    case CheckOutStatus.CheckedOutOtherUser:
                        return (int) TreeIcons.InternalPageCheckedOutOtherUser;
                    case CheckOutStatus.CheckedOut:
                        return (int) TreeIcons.InternalPageCheckedOut;
                    case CheckOutStatus.Unlinked:
                        return (int) TreeIcons.InternalPageUnlinked;
                    default:
                        return (int) TreeIcons.Page;
                }
            }
            else if (libraryItem is Outcome)
            {
                switch (libraryItemStatus)
                {
                    case CheckOutStatus.CheckedIn:
                        return (int) TreeIcons.OutcomeCheckedIn;
                    case CheckOutStatus.CheckedOutOtherUser:
                        return (int) TreeIcons.OutcomeCheckedOutOtherUser;
                    case CheckOutStatus.CheckedOut:
                        return (int) TreeIcons.OutcomeCheckedOut;
                    case CheckOutStatus.Unlinked:
                        return (int) TreeIcons.OutcomeUnlinked;
                    default:
                        return (int) TreeIcons.Outcome;
                }
            }
            else if (libraryItem is Question)
            {
                switch (libraryItemStatus)
                {
                    case CheckOutStatus.CheckedIn:
                        return (int) TreeIcons.QuestionCheckedIn;
                    case CheckOutStatus.CheckedOutOtherUser:
                        return (int) TreeIcons.QuestionCheckedOutOtherUser;
                    case CheckOutStatus.CheckedOut:
                        return (int) TreeIcons.QuestionCheckedOut;
                    case CheckOutStatus.Unlinked:
                        return (int) TreeIcons.QuestionUnlinked;
                    default:
                        return (int) TreeIcons.Question;
                }
            }
            else if (libraryItem is SimpleOutcome)
            {
                switch (libraryItemStatus)
                {
                    case CheckOutStatus.CheckedIn:
                        return (int) TreeIcons.SimpleOutcomeCheckedIn;
                    case CheckOutStatus.CheckedOutOtherUser:
                        return (int) TreeIcons.SimpleOutcomeCheckedOutOtherUser;
                    case CheckOutStatus.CheckedOut:
                        return (int) TreeIcons.SimpleOutcomeCheckedOut;
                    case CheckOutStatus.Unlinked:
                        return (int) TreeIcons.SimpleOutcomeUnlinked;
                    default:
                        return (int) TreeIcons.SimpleOutcome;
                }
            }
            else if (libraryItem is TemplateDocument)
            {
                switch (libraryItemStatus)
                {
                    case CheckOutStatus.CheckedIn:
                        return (int) TreeIcons.TemplateCheckedIn;
                    case CheckOutStatus.CheckedOutOtherUser:
                        return (int) TreeIcons.TemplateCheckedOutOtherUser;
                    case CheckOutStatus.CheckedOut:
                        return (int) TreeIcons.TemplateCheckedOut;
                    case CheckOutStatus.Unlinked:
                        return (int) TreeIcons.TemplateUnlinked;
                    default:
                        return (int) TreeIcons.Template;
                }
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        ///		Assessed the library item and returns its given shared library status.
        ///		Statuses can be none (not from the shared library), unlinked, checked in, 
        ///		checked out to another user, or checked out (to the current user).
        /// </summary>
        /// <param name="libraryItem">The item being assessed.</param>
        /// <returns>The status of the library item as a CheckOutstatus.</returns>
        private CheckOutStatus GetLibraryItemSharedLibraryStatus(LibraryItem libraryItem)
        {
            // Is the item from the shared library ?
            if (libraryItem.LibraryUniqueIdentifier == new Guid())
            {
                // Not from the shared library
                return CheckOutStatus.None;
            }
            else
            {
                // Library item is from the Shared Library

                // Is the item linked to the shared library ?
                if (libraryItem.Linked)
                {
                    // Yes - get the latest check out status
                    sharedLibraryController.GetCheckOutStatus(libraryItem);

                    // Return it's current status
                    if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedIn)
                    {
                        return CheckOutStatus.CheckedIn;
                    }
                    else if (libraryItem.CheckedOutByLogInName != null &&
                             libraryItem.CheckedOutByLogInName.ToLower() != WindowsIdentity.GetCurrent().Name.ToLower())
                    {
                        return CheckOutStatus.CheckedOutOtherUser;
                    }
                    else
                    {
                        return CheckOutStatus.CheckedOut;
                    }
                }
                else
                {
                    // No - return the fact that it is unlinked
                    return CheckOutStatus.Unlinked;
                }
            }
        }

        //private void tvwExplorer_Click(object sender, EventArgs e)
        //{
        //    //DoDesignSelectedNode();
        //}

        //TODO: double-clicking whitespace can cause the selection not to change
        // ---------------------------------------------------------------------------------------
        // ---------------------------------------------------------------------------------------
        //private void tvwExplorer_DoubleClick(object sender, EventArgs e)
        //{
        //    //IPManger UI Refresh - see MouseUp - single click selects and expands
        //    //DoDesignSelectedNode();
        //}

        private void DoDesignSelectedNode()
        {
            if (tvwExplorer.SelectedNode != null)
            {
                object tag = tvwExplorer.SelectedNode.Tag;
                if (tag is IDesignable && tag is PackageItem)
                {
                    // Don't open the designer if the item is checked into the shared library
                    if (tag is LibraryItem)
                    {
                        if (((LibraryItem)tag).Linked &&
                            ((LibraryItem)tag).LibraryUniqueIdentifier != Guid.Empty &&
                            ((LibraryItem)tag).CheckedOutStatus == CheckOutStatus.CheckedIn)
                        {
                            return;
                        }
                    }

                    // FB835 ->
                    // Possibility to open Query Editor via dbl-click
                    if (tag is Outcome)
                    {
                        OnEditOutcome(tag as Outcome);
                    } 
                    else
                        if (tag is SimpleOutcome)
                        {
                            OnEditSimpleOutcome(tag as SimpleOutcome);
                        }
                        else

                    // <- FB835
                    
                    OnOpenDesigner(tag as PackageItem, PrefferedDesigner.Default);
                }
                //IPManager UI refresh - no need for this as single click already selected it
                //tvwExplorer.SelectedNode = savedNode;
            }
        }


        private void OnOpenDesigner(PackageItem item, PrefferedDesigner prefferedDesigner)
        {
            if (OpenDesigner != null)
            {
                OpenDesigner(this, new PackageItemEventArgs(item), prefferedDesigner);
            }
        }


        class WindowsFormsEditorService : System.Windows.Forms.Design.IWindowsFormsEditorService
        {
            Form _host;
            public Form HostingForm { get { return _host; } }
            internal protected WindowsFormsEditorService(Form host)
            {
                _host = host;
            }

            #region IWindowsFormsEditorService Members

            public void CloseDropDown()
            {
                throw new Exception("The method or operation is not implemented.");
            }

            public void DropDownControl(Control control)
            {
                throw new Exception("The method or operation is not implemented.");
            }

            public DialogResult ShowDialog(Form dialog)
            {
                if (dialog.Owner == null)
                    dialog.Owner = HostingForm;

                return dialog.ShowDialog();
            }

            #endregion
        }

        // TODO: The following code is a quick and dirty solution to support opening of the Query Editor
        //       by double clicking the Outcome node.
        //
        // 1. The code should be generic for modal ediors ( as OnOpenDesigner () demonstrates)
        // 2. The property grid should be refactored to support the editing of the 
        //    'Definition' property more natural.
        private void OnEditOutcome(Outcome item)
        {
            Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor ed = new Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor();
            item.Definition = (Query) ed.InvokeEditor ( item, new WindowsFormsEditorService(this.FindForm()), item.Definition);
        }

        private void OnEditSimpleOutcome(SimpleOutcome item)
        {
            Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor ed = new Perfectus.Client.Studio.UI.PackageObjectEditors.OutcomeEditor();
            item.Definition = (Query)ed.InvokeEditor(item, new WindowsFormsEditorService(this.FindForm()), item.Definition);
        }


        private TemplateDocument DoNewTemplate()
        {
            return DoNewTemplate(true);
        }

        private TemplateDocument DoNewTemplate(bool openDesigner)
        {
            WordTemplateDocument2 template = package.CreateWordTemplate(true);
            if (template != null && openDesigner)
            {
                OpenDesigner(this, new PackageItemEventArgs(template), PrefferedDesigner.Default);
            }

            return template;
        }

        
        private Question DoNewQuestion()
        {
            // check if we have a parent folder. then call CreateQuestionFolder initializer as appropriate.			
            TreeNode n = tvwExplorer.SelectedNode;

            if (n != null)
            {
                if (n.Tag is QuestionFolder)
                {
                    Folder f = null;
                    f = (Folder) n.Tag;
                    return Package.CreateQuestion(f);
                }
                else
                {
                    return package.CreateQuestion();
                }
            }
            else
                return null;
        }

        
        private void DoAddFolder()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n == questionsNode)
                {
                    DoNewQuestionFolder();
                }
                else if (n == attachmentsNode)
                {
                    DoNewAttachmentFolder();
                }
                else if (n == outcomesNode)
                {
                    DoNewOutcomeFolder();
                }
                else if (n == simpleOutcomesNode)
                {
                    DoNewSimpleOutcomeFolder();
                }
                else if (n == functionsNode)
                {
                    DoNewPFunctionFolder();
                }
                else if (n.Tag is PFunctionFolder)
                {
                    DoNewPFunctionFolder((PFunctionFolder) n.Tag);
                }
                else if (n.Tag is QuestionFolder)
                {
                    DoNewQuestionFolder((QuestionFolder) n.Tag);
                }
                else if (n.Tag is AttachmentFolder)
                {
                    DoNewAttachmentFolder((AttachmentFolder)n.Tag);
                }
                else if (n.Tag is OutcomeFolder)
                {
                    DoNewOutcomeFolder((OutcomeFolder) n.Tag);
                }
                else if (n.Tag is SimpleOutcomeFolder)
                {
                    DoNewSimpleOutcomeFolder((SimpleOutcomeFolder) n.Tag);
                }
            }
        }

        private void DoNewQuestionFolder()
        {
            package.CreateQuestionFolder();
        }

        private static void DoNewQuestionFolder(QuestionFolder parentFolder)
        {
            parentFolder.CreateChild();
        }

        private void DoNewAttachmentFolder()
        {
            package.CreateAttachmentFolder();
        }

        private void DoNewOutcomeFolder()
        {
            package.CreateOutcomeFolder();
        }

        private static void DoNewAttachmentFolder(AttachmentFolder parentFolder)
        {
            parentFolder.CreateChild();
        }

        private static void DoNewOutcomeFolder(OutcomeFolder parentFolder)
        {
            parentFolder.CreateChild();
        }

        private void DoNewSimpleOutcomeFolder()
        {
            package.CreateSimpleOutcomeFolder();
        }

        private static void DoNewPFunctionFolder(PFunctionFolder parentFolder)
        {
            parentFolder.CreateChild();
        }

        private void DoNewPFunctionFolder()
        {
            package.CreatePFunctionFolder();
        }


        private void DoNewSimpleOutcomeFolder(SimpleOutcomeFolder parentFolder)
        {
            parentFolder.CreateChild();
        }

        
        private Outcome DoNewOutcome()
        {
            // check if we have a parent folder. then call CreateOutcome as appropriate.	
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is OutcomeFolder)
                {
                    Folder f = null;
                    f = (Folder) n.Tag;
                    return package.CreateOutcome(f);
                }
                else
                {
                    return package.CreateOutcome();
                }
            }
            else
                return null;
        }
        
        private AttachmentBase DoNewAttachment()
        {
            // check if we have a parent folder. then call CreateOutcome as appropriate.	
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is AttachmentFolder)
                {
                    Folder f = null;
                    f = (Folder)n.Tag;
                    return package.CreateStaticAttachment(f);
                }
                else
                {
                    return package.CreateStaticAttachment();
                }
            }
            else
                return null;
        }
        
        private DatePFunction DoNewDateFunction()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is PFunctionFolder)
                {
                    Folder f = null;
                    f = (Folder) n.Tag;
                    return package.CreateDateFunction(f);
                }
                else
                {
                    return package.CreateDateFunction();
                }
            }
            else
                return null;
        }

        
        private ArithmeticPFunction DoNewArithmeticFunction()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is PFunctionFolder)
                {
                    Folder f = null;
                    f = (Folder) n.Tag;
                    return package.CreateArithmeticFunction(f);
                }
                else
                {
                    return package.CreateArithmeticFunction();
                }
            }
            else
                return null;
        }

        
        private InterviewPage DoNewPage()
        {
            // Make sure there's an interview selected

            if (package.Interviews.Count == 1)
            {
                return package.CreatePage(package.Interviews[0]);
            }
            else
            {
                TreeNode n = tvwExplorer.SelectedNode;
                if (n != null)
                {
                    Interview tag = n.Tag as Interview;
                    if (tag != null)
                    {
                        return package.CreatePage(tag);
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }

        
        private SimpleOutcome DoNewSimpleOutcome()
        {
            // check if we have a parent folder. then call CreateSimpleOutcome as appropriate.	
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is SimpleOutcomeFolder)
                {
                    Folder f = (Folder) n.Tag;
                    return package.CreateSimpleOutcome(f);
                }
                else
                {
                    return package.CreateSimpleOutcome();
                }
            }
            else
                return null;
        }

        private void mnuNewSimpleOutcome_Click(object sender, EventArgs e)
        {
            DoNewSimpleOutcome();
        }

        
        private void DoNewExternalPage()
        {
            // Make sure there's an interview selected

            if (package.Interviews.Count == 1)
            {
                package.CreateExternalPage(package.Interviews[0]);
            }
            else
            {
                TreeNode n = tvwExplorer.SelectedNode;
                if (n != null)
                {
                    Interview tag = n.Tag as Interview;
                    if (tag != null)
                    {
                        package.CreateExternalPage(tag);
                    }
                }
            }
        }

        
        private void DoNewWebReference()
        {
            AddWebReference dlg = new AddWebReference();
            DialogResult dr = dlg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                package.CreateWebReference(dlg.Address, dlg.LayoutXml);
            }
        }

        private void tvwExplorer_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            object tag = e.Node.Tag;
            if (tag != null && (tag is PackageItem || tag is Package)
                && ! (tag is Distributor) && bAllowEdit) // Don't allow changing of the distributor name, prevent edits based on flag bAllowEdit
            {
                if (tag is LibraryItem && ((LibraryItem)tag).CheckedOutStatus == CheckOutStatus.CheckedIn &&
                    ((LibraryItem)tag).Linked == true)
                {
                    // Don't allow changes to a checked in SL Item
                    e.CancelEdit = true;
                }
                else
                {
                    e.CancelEdit = false;
                }
            }
            else
            {
                e.CancelEdit = true;
            }
            //reset flag to prevent edits by single click
            bAllowEdit = false;
        }

        private void tvwExplorer_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (e.Node != null)
            {
                object tag = e.Node.Tag;
                if (e.Node.Tag is PackageItem)
                {
                    if (e.Label == null || e.Label.Length == 0 || ((PackageItem) e.Node.Tag).Equals(e.Label))
                    {
                        e.CancelEdit = true;
                    }
                    else if (tag != null)
                    {
                        ((PackageItem) tag).Name = e.Label;

                        if (tag is LibraryItem)
                        {
                            SetPropertyGridSelectedObject((LibraryItem)tag, GetCurrentWindowsUsername());
                        }
                        else
                        {
                            SetPropertyGridSelectedObject(tag);
                        }
                        
                        e.CancelEdit = true; // Cancel, so that the text is actually set from the underlying PackageItem
                        e.Node.Text = ((PackageItem) tag).Name; // Name may have changed to ensure uniqueness
                        propertyGrid.Refresh();
                    }
                }
                else if (e.Node.Tag is Package)
                {
                    if (e.Label == null || e.Label.Length == 0 || ((Package) e.Node.Tag).Equals(e.Label))
                    {
                        e.CancelEdit = true;
                    }
                    else if (tag != null)
                    {
                        ((Package) tag).Name = e.Label;

                        if (tag is LibraryItem)
                        {
                            SetPropertyGridSelectedObject((LibraryItem)tag, GetCurrentWindowsUsername());
                        }
                        else
                        {
                            SetPropertyGridSelectedObject(tag);
                        }
                        propertyGrid.Refresh();
                    }
                }
            }
        }


        private void package_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Name":
                    PackageNameBind();
                    break;
            }
        }

        private void PackageNameBind()
        {
            rootNode.Text = package.Name;
        }

        private void itemToAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Name":
                    if (sender != null && sender is PackageItem)
                    {
                        PackageItem item = sender as PackageItem;
                        Guid g = item.UniqueIdentifier;

                        PfTreeNodeCollection tnc = guidNodeMap[g];
                        if (tnc != null)
                        {
                            foreach (TreeNode n in tnc)
                            {
                                n.Text = item.Name;
                            }
                        }
                    }

                    if (sender is Server)
                    {
                        Server srv = sender as Server;
                        srv.serverInfo.Name = srv.Name;
                    }
                    break;
            }

            if (sender is Interview)
            {
                if (e.PropertyName == "StartPage")
                {
                    RefreshInterviewChildren(((Interview) sender));
                }
            }
        }

        private void RefreshInterviewChildren(Interview interview)
        {
            // Find the node
            PfTreeNodeCollection tnc = guidNodeMap[interview.UniqueIdentifier];
            if (tnc != null && tnc.Count > 0)
            {
                TreeNode interviewNode = tnc[0];

                foreach (TreeNode n in interviewNode.Nodes)
                {
                    Page pg = n.Tag as Page;
                    if (pg != null && pg == interview.StartPage)
                    {
                        n.ImageIndex = (int) TreeIcons.StartPage;
                        n.SelectedImageIndex = (int) TreeIcons.StartPage;
                        n.NodeFont = new Font(tvwExplorer.Font.FontFamily.Name, tvwExplorer.Font.Size, FontStyle.Bold);
                        n.Text = n.Text;
                    }
                    else
                    {
                        if (pg is ExternalPage)
                        {
                            n.ImageIndex = (int) TreeIcons.ExternalPage;
                            n.SelectedImageIndex = (int) TreeIcons.ExternalPage;
                        }
                        else
                        {
                            n.ImageIndex = (int) TreeIcons.Page;
                            n.SelectedImageIndex = (int) TreeIcons.Page;
                        }
                        n.NodeFont = null;
                    }
                }
            }
        }

        private void tvwExplorer_MouseUp(object sender, MouseEventArgs e)
        {
            TreeNode n = tvwExplorer.GetNodeAt(e.X, e.Y);

            if (n != null)
            {
                if (e.Button == MouseButtons.Left && e.Clicks == 1 && sender == tvwExplorer)
                {
                    //IPManger UI Refresh - use mouseup to expand collapse by inverting the node
                    if (n.IsExpanded)
                    {
                        n.Collapse();
                    }
                    else
                    {
                        n.Expand();
                    }

                    tvwExplorer.SelectedNode = n;
                    if (n.Tag != null)
                    {
                        if (n.Tag is LibraryItem)
                        {
                            SetPropertyGridSelectedObject((LibraryItem)n.Tag, GetCurrentWindowsUsername(), n);
                        }
                        else
                        {
                            SetPropertyGridSelectedObject(n.Tag);
                        }
                    }
                    //IPManger UI Refresh - use mouseup to select node in designer
                    DoDesignSelectedNode();
                }
                else if (e.Button == MouseButtons.Right && e.Clicks == 1 && sender == tvwExplorer)
                {
                    tvwExplorer.SelectedNode = n;
                    SetCommands();

                    cmExplorer.Show(tvwExplorer, new System.Drawing.Point(e.X, e.Y));

                    tvwExplorer.SelectedNode = n;
                    tvwExplorer.Invalidate();
                }
            }
        }


        private void package_ItemCreated(object sender, PackageItemEventArgs e)
        {
            if (!guidNodeMap.ContainsKey(e.Item.UniqueIdentifier))
            {
                AddNode(e.Item, true);
            }
        }

        private void package_ItemUIDChanged(object sender, LibraryItemEventArgs e)
        {
            if (guidNodeMap.ContainsKey(e.CurrentLibraryItem.OriginalUniqueIdentifier))
            {
                PfTreeNodeCollection pfTreeNodeCollection = guidNodeMap[e.CurrentLibraryItem.OriginalUniqueIdentifier];
                guidNodeMap.Remove(e.CurrentLibraryItem.OriginalUniqueIdentifier);
                guidNodeMap.Add(e.CurrentLibraryItem.UniqueIdentifier, pfTreeNodeCollection);
            }

            //TODO - update any wordML if required!
        }

        void package_LibraryItemUpdated(object sender, LibraryItemEventArgs e)
        {
            //TODO - Update Tag and icon
        }

        void package_BindingsCannotBeReconnected(object sender, PackageItemEventArgs e)
        {
            string message = GetResource("PackageExplorer.ExplorerControl.BindingsCannotBeReconnected");
            message = string.Format(message, e.Item.Name);
            string title = GetResource("SharedLibrary");

            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        
        private void DoNewServer()
        {
            AddServerDialog2 dlg = new AddServerDialog2();
            DialogResult dr = dlg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                    if (dlg.ServerInfo == null)
                    {
                        MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ServerInfoNotFoundMsg"));
                        return;
                    }

                    if (dlg.Plugins == null)
                    {
                        MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.NoPluginsFoundMsg"));
                        return;
                    }

                    package.CreateServer(dlg.ServerInfo, dlg.Plugins, dlg.Address);
                }
            }

        private void package_RuleDeliveryAdded(object sender, RuleDeliveryEventArgs e)
        {
            AddRuleDeliveryNode(e.Rule2, e.Delivery);
        }

        private void AddRuleDeliveryNode(Rule2 r, Delivery d)
        {
            TreeNode ruleNode;

            PfTreeNodeCollection tnc = guidNodeMap[r.UniqueIdentifier];
            if (tnc == null)
            {
                throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControl.NoRuleNode"));
            }
            else if (tnc.Count > 1)
            {
                throw new Exception(GetResource("PackageExplorer.ExplorerControl.DuplicateRuleNode"));
            }
            else
            {
                ruleNode = tnc[0];
            }

            if (ruleNode != null)
            {

                AddNode(d, false);
        
                TreeNode ruleDocumentsNode =
                    new TreeNode("Documents & Files", (int)TreeIcons.DocumentFolderIcon, (int)TreeIcons.DocumentFolderIcon);
                ruleDocumentsNode.Tag = new DocumentFolderTreeNode( );

                TreeNode deliveryNode = guidNodeMap[d.UniqueIdentifier][0];
                deliveryNode.Nodes.AddRange(new TreeNode[] { ruleDocumentsNode });
            }
        }

        private void AddRuleDistributorNode(Delivery d, Distributor dist)
        {
            // Can happen if a delivery is wip and loaded from disk
            if (dist == null)
                return;

            TreeNode deliveryNode;

            PfTreeNodeCollection tnc = guidNodeMap[d.UniqueIdentifier];
            if (tnc == null)
            {
                throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControl.NoRuleNode"));
            }
            else if (tnc.Count > 1)
            {
                throw new Exception(GetResource("PackageExplorer.ExplorerControl.DuplicateRuleNode"));
            }
            else
            {
                deliveryNode = tnc[0];
            }

            TreeNode newNode = new TreeNode();
            newNode.Text = dist.Name;
            newNode.Tag = dist;
            newNode.ImageIndex = (int)TreeIcons.Distributor;
            newNode.SelectedImageIndex = (int)TreeIcons.Distributor;
            deliveryNode.Nodes.Add(newNode);

            guidNodeMap.Add(dist.UniqueIdentifier, new PfTreeNodeCollection(new TreeNode[] { newNode }));
/* Do we need it in the map? */// guidNodeMap[d.UniqueIdentifier].Add(newNode);
        }        

        
        private void AddRuleDocumentNode(Delivery d, IDocument doc)
        {
            TreeNode deliveryNode;


            PfTreeNodeCollection tnc = guidNodeMap[d.UniqueIdentifier];
            if (tnc == null)
            {
                throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControl.NoRuleNode"));
            }
            else if (tnc.Count > 1)
            {
                throw new Exception(GetResource("PackageExplorer.ExplorerControl.DuplicateRuleNode"));
            }
            else
            {
                deliveryNode = tnc[0];
            }

            if (deliveryNode != null)
            {
                TreeNode parentNode = null;
                // Find which folder node to put it in
                foreach (TreeNode n in deliveryNode.Nodes)
                {
                    if( n.Tag is DocumentFolderTreeNode )
                    {
                        parentNode = n;
                        break;
                    }
                }

                if (parentNode != null)
                {
                    doc.ParentDelivery = deliveryNode.Tag as Delivery;

                    // That woudl be the normal call,
                    //AddNode(doc as PackageItem, false);

                    // but we might have a reference to an attachment and therefore
                    // AddNode() doen't know where to put the node. Therefore code duplication...
                    TreeNode newNode;

                    if (doc is IAttachment)
                    {
                        if (doc is AttachmentDynamic)
                            newNode = new TreeNode(doc.Name, (int)TreeIcons.AttachmentInterviewIcon, (int)TreeIcons.AttachmentInterviewIcon);
                        else
                            newNode = new TreeNode(doc.Name, (int)TreeIcons.AttachmentIcon, (int)TreeIcons.AttachmentIcon);
                    }
                    else
                        newNode = new TreeNode(doc.Name, (int)TreeIcons.DocumentIcon, (int)TreeIcons.DocumentIcon);

                    newNode.Tag = doc;

                    parentNode.Nodes.Add(newNode);
                    PfTreeNodeCollection c = guidNodeMap[doc.UniqueIdentifier];
                    if (c == null)
                    {
                        c = new PfTreeNodeCollection();
                        guidNodeMap[doc.UniqueIdentifier] = c;
                    }
                    c.Add(newNode);

                    ((PackageItem)doc).PropertyChanged += new PropertyChangedEventHandler(itemToAdd_PropertyChanged);

                    newNode.EnsureVisible();
                }
            }
        }

        private void DoNewRule()
        {
            // Make sure there's an interview selected
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                Server tag = n.Tag as Server;
                if (tag != null)
                {
                    package.CreateRule(tag);
                }
                AddDeliveryExecuted();
            }
        }


        void package_DeliveryDocumentAdded(object sender, DeliveryDocumentEventArgs e)
        {
            AddRuleDocumentNode(e.Delivery, e.Document);
        }

        private void DoAddDocumentToDelivery()
        {
            Delivery d = null;

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is Delivery)
                {
                    d = n.Tag as Delivery;
                }
                else
                {
                    if( n.Tag is DocumentFolderTreeNode )
                    {
                        d = n.Parent.Tag as Delivery;
                    }
                }
            }

            if (d == null)
            {
                throw new NotSupportedException(GetResource("PackageExplorer.ExplorerControl.NodeMustBeRule"));
            }


            List<IDocument> currentDocuments = new List<IDocument>(d.Documents);
            PluginDescriptorCollection plugins = new PluginDescriptorCollection(d.ParentRule.ParentServer.AvailablePlugins);

            AddExistingDocumentDialog dlg = new AddExistingDocumentDialog(package, currentDocuments, plugins);
            DialogResult dr = dlg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                List<IDocument> tempList = dlg.SelectedDocuments;

                // Maybe add documents from delivery
                foreach (IDocument document in tempList)
                    if (!d.Documents.Contains(document))
                    {
                        IDocument copy = document.Clone() as IDocument;
                        copy.Name = copy.Name;
                        package.AddDocumentToDelivery(d, copy);
                       // SelectItem(copy.Name, copy.UniqueIdentifier);
                        SelectItem(copy as PackageItem);
                    }
            }
        }

        private void DoRemoveAllDocumentsFromDelivery()
        {
            Delivery d = null;

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is Delivery)
                {
                    d = n.Tag as Delivery;
                }
                else
                {
                    if( n.Tag is DocumentFolderTreeNode )
                    {
                        d = n.Parent.Tag as Delivery;
                    }
                }
            }

            if (d == null)
            {
                throw new NotSupportedException(GetResource("PackageExplorer.ExplorerControl.NodeMustBeRule"));
            }

            if (DialogResult.Yes == MessageBox.Show(
                GetResource("PackageExplorer.ExplorerControl.ConfirmDeleteAllDocuments"), 
                GetResource("PackageExplorer.ExplorerControl.DeletePackageItemTitle"), 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            while (d.Documents.Count > 0)
                package.DeleteItem((PackageItem)d.Documents[d.Documents.Count-1], false);
        }

        private void DoEditDocumentToDelivery()
        {
            DeliveryDoc doc = null;
            Delivery d = null;

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                if (n.Tag is DeliveryDoc)
                {
                    doc = n.Tag as DeliveryDoc;
                    d = n.Parent.Parent.Tag as Delivery;
                }
            }

            if (d == null || doc == null)
            {
                throw new NotSupportedException(GetResource("PackageExplorer.ExplorerControl.NodeMustBeRule"));
            }
            AddDocumentDialog dlg = new AddDocumentDialog();
            
            dlg.AllTemplates = package.Templates;
            dlg.AllPlugins = new PluginDescriptorCollection(d.ParentRule.ParentServer.AvailablePlugins);

            DeliveryDoc saveDoc = doc.Clone() as DeliveryDoc;
            dlg.Document = doc;
            DialogResult dr = dlg.ShowDialog();
            if (dr != DialogResult.OK)
            {
                //n.Tag = saveDoc;
                package.DeleteItem(doc, false);
                package.AddDocumentToDelivery(d, saveDoc);
                SelectItem(saveDoc);
            }
            else SelectItem(doc);
        }


        private void DoAddDeliveryToRule()
        {
            AddDeliveryExecuted();
        }

        private void AddDeliveryExecuted()
        {
            // Identify the rule we're adding to
            Rule2 r = null;

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && n.Tag is Rule2)
            {
                r = n.Tag as Rule2;
            }

            if (r == null)
            {
                throw new NotSupportedException(GetResource("PackageExplorer.ExplorerControl.NodeMustBeRule2"));
            }

            Delivery delivery = new Delivery(r, "New Delivery");
            package.AddDeliveryToRule(r, delivery);
        }

        
        private void DoAddDistributorToDelivery()
        {
            // Identify the rule we're adding to
            Delivery d = null;

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && n.Tag is Delivery)
            {
                d = n.Tag as Delivery;
            }

            if (d == null)
            {
                throw new NotSupportedException(GetResource("PackageExplorer.ExplorerControl.NodeMustBeRule2"));
            }


            AddPluginsDialog dlg = new AddPluginsDialog();
            dlg.BehaviourFilter = PluginBehaviour.Distributor;
            dlg.AllPlugins = new PluginDescriptorCollection(d.ParentRule.ParentServer.AvailablePlugins);

            DialogResult dr = dlg.ShowDialog();

            if (dr == DialogResult.OK)
            {
                PluginDescriptor selectedPlugin = dlg.SelectedPlugin;
                if (selectedPlugin != null)
                {
                    Distributor dist = new Distributor(selectedPlugin);
                    if (d.Distributor != null)
                    {
                        SelectItem(d.Distributor );
                        DoDelete();
                    }
                    if ( null == SelectItem(d.Distributor ))
                    package.AddDistributorToDelivery(d, dist);
                    SelectItem(d.Distributor);
                }
            }
        }

        private void package_DeliveryDistributorAdded(object sender, DeliveryDistributorEventArgs e)
        {
            AddRuleDistributorNode(e.Delivery,e.Distributor);
        }
        
        private void DoDelete()
        {
            // Work out what node was selected
            TreeNode n = tvwExplorer.SelectedNode;

            if (n != null)
            {
                object tag = n.Tag;
                if (tag is PackageItem)
                {
                    /*
                       if (tag is Distributor)
                    { 
                        // Avoid the confirmation message
                        package.DeleteItem((PackageItem)tag, false);
                    }
                    else 
                     */ if (tag is QuestionFolder || tag is OutcomeFolder || tag is SimpleOutcomeFolder ||
                             tag is PFunctionFolder || tag is AttachmentFolder)
                    {
                        DeleteFolder(n);
                    }
                    else
                    {
                        string msg;
                        if (!package.IsOkayToDelete((PackageItem)tag, out msg))
                        {
                            MessageBox.Show(msg, Common.About.FormsTitle);
                        }
                        else
                        {
                            string name = ((PackageItem)tag).Name;
                            string message = GetResource("PackageExplorer.ExplorerControl.DeletePackageItemMessage");
                            message = string.Format(message, name);
                            string title = GetResource("PackageExplorer.ExplorerControl.DeletePackageItemTitle");

                            DialogResult result = MessageBox.Show(DesktopWindow.Instance, message, title,
                                                                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (result == DialogResult.Yes)
                            {
                                package.DeleteItem((PackageItem)tag, false);
                            }
                        }
                    }
                }
            }
        }

        private void CheckDependencies()
        {
            StringBuilder dependenciesMessageBuilder = new StringBuilder();

            // Work out what node was selected
            TreeNode n = tvwExplorer.SelectedNode;

            if (n != null)
            {
                object tag = n.Tag;
                if (tag is PackageItem)
                {
                    PackageItem item = (PackageItem)tag;

                    if (!package.FindIfDependentsExist(item, ref dependenciesMessageBuilder))
                    {
                        string messageHeader = string.Format("\"{0}\" {1}\n{2}", item.Name, GetResource("PackageExplorer.ExplorerControl.Dependencies"), dependenciesMessageBuilder.ToString());
                        MessageBox.Show(messageHeader, Common.About.FormsTitle);
                    }
                    else
                    {
                        string messageHeader = string.Format("\"{0}\" {1}\n", item.Name, GetResource("PackageExplorer.ExplorerControl.NoDependencies"));
                        MessageBox.Show(messageHeader, Common.About.FormsTitle);
                    }
                }
            }
        }

        private void DeleteFolder(TreeNode n)
        {
            bool doDelete = true;
            Folder f = (Folder) n.Tag;

            // if we get an answer we should never ask again, if user never gets asked then safe to delete.
            if (f.ChildFolders.Count > 0)
            {
                doDelete = confirmFolderDelete();
            }
            else if (n.Tag is QuestionFolder)
            {
                foreach (Question q in f.ParentPackage.Questions)
                {
                    if (q.ParentFolder == f)
                    {
                        doDelete = confirmFolderDelete();
                        break;
                    }
                }
            }
            else if (n.Tag is AttachmentFolder)
            {
                foreach (AttachmentBase a in f.ParentPackage.Attachments)
                {
                    if (a.ParentFolder == f)
                    {
                        doDelete = confirmFolderDelete();
                        break;
                    }
                }
            }
            else if (n.Tag is OutcomeFolder)
            {
                foreach (Outcome o in f.ParentPackage.Outcomes)
                {
                    if (o.ParentFolder == f)
                    {
                        doDelete = confirmFolderDelete();
                        break;
                    }
                }
            }
            else if (n.Tag is SimpleOutcomeFolder)
            {
                foreach (SimpleOutcome so in f.ParentPackage.SimpleOutcomes)
                {
                    if (so.ParentFolder == f)
                    {
                        doDelete = confirmFolderDelete();
                        break;
                    }
                }
            }
            else if (n.Tag is PFunctionFolder)
            {
                foreach (PFunction pf in f.ParentPackage.Functions)
                {
                    if (pf.ParentFolder == f)
                    {
                        doDelete = confirmFolderDelete();
                        break;
                    }
                }
            }

            if (doDelete)
            {
                try
                {
                    package.DeleteItem((PackageItem) n.Tag, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Common.About.FormsTitle);
                }
            }
        }

        private bool confirmFolderDelete()
        {
            if (
                MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.FolderDeleteConfirm"),
                                GetResource("PackageExplorer.ExplorerControl.DeletePackageItemTitle")
                                /*Common.About.FormsTitle*/, MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveFromDelivery(TreeNode n)
        {
            if( !(n.Parent.Tag is DocumentFolderTreeNode) )
                throw new Exception( "Invalid parent tree node in function 'RemoveFromDelivery'. Expected parent node with tag of type 'DocumentFolderTreeNode'. " );

            Package.RemoveDocumentFromDelivery((IDocument)n.Tag, (Delivery)n.Parent.Parent.Tag);

            PfTreeNodeCollection tnc = guidNodeMap[((PackageItem) (n.Tag)).UniqueIdentifier];
            if (tnc != null && tnc.Contains(n))
            {
                tnc.Remove(n);
            }
            n.Remove();
        }

        private void package_ItemDeleted(object sender, PackageItemDeletedEventArgs e)
        {
            PfTreeNodeCollection tnc = guidNodeMap[e.Id];
            if (tnc != null)
            {
                for (int i = tnc.Count - 1; i >= 0; i--)
                {
                    TreeNode nodeToRemove = tnc[i];
                    nodeToRemove.Remove();
                    tnc.Remove(nodeToRemove);
                    nodeToRemove = null;
                }
                if (tnc.Count == 0)
                {
                    guidNodeMap.Remove(e.Id);
                    tnc = null;
                }
            }
        }

        private void package_BulkLoadStarted(object sender, EventArgs e)
        {
            tvwExplorer.SuspendLayout();
            suspended = true;
        }

        private void package_BulkLoadDone(object sender, EventArgs e)
        {
            tvwExplorer.ResumeLayout();
            suspended = false;
            tvwExplorer.Refresh();
        }

        
        private void DoExport()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                object tag = n.Tag;
                if (tag is WordTemplateDocument2)
                {
                    ExportTemplate(tag as WordTemplateDocument2);
                }
            }
        }

        private void ExportTemplate(WordTemplateDocument2 t)
        {
            try
            {
                saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                //pf-3065 docx
                //saveFileDialog1.FileName = t.Name + ".xml";
                saveFileDialog1.FileName = t.Name + ".docx";
                DialogResult dr = saveFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    t.Export(saveFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(GetResource("PackageExplorer.ExplorerControl.TemplateExport") + " ({0})", ex.Message),
                                Common.About.FormsTitle);
            }
        }


                private void DoImport()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && n == templatesNode)
            {
                openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                DialogResult dr = openFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    ImportTemplate(openFileDialog1.FileName);
                }
            }
        }


        private void ImportTemplate(string filename)
        {
            suspended = true;
            //dn4
            //ApplicationClass app = null;
            Microsoft.Office.Interop.Word.Application app = null;
            
            
            object oTrue = true;
            object oFalse = false;
            object oMissing = Missing.Value;
            WordTemplateDocument2 t = null;
            WordDocument doc = null;
            string workingFilename = null;
            object oWorkingFilename;

            try
            {
                Cursor = Cursors.WaitCursor;
                t = package.CreateWordTemplate();
                string templateName = new FileInfo(filename).Name;
                //pf-3134 use the default name, not the tmp file name
                //t.Name = templateName;

                workingFilename = Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) ) + ".xml";
                oWorkingFilename = workingFilename;

                // We have to automate Word here, sadly.  We need to make sure the document gets saved as WordML, and there's no other way...
                //dn4
                //app = new ApplicationClass();
                app = new Microsoft.Office.Interop.Word.Application();

                object oFileName = filename;
                doc =
                    app.Documents.Open(ref oFileName, ref oMissing, ref oTrue, ref oMissing, ref oMissing, ref oMissing,
                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oFalse,
                                       ref oMissing, ref oMissing, ref oMissing, ref oMissing);


                oFileName = t.FilePath;
                object oFileFormat = WdSaveFormat.wdFormatFlatXML;

                // Saved to the template objects pickup point.
                doc.SaveAs(ref oFileName, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                           ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                // Save as our temporary one again to avoid locking issues
                doc.SaveAs(ref oWorkingFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                           ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                           ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                t.ForceDocumentPropertiesUpdate();
            }
            catch (Exception ex)
            {
                if ( t !=null)
                    try
                    {
                        package.DeleteItem(t , true);
                    }
                    catch { }

                MessageBox.Show(string.Format(GetResource("PackageExplorer.ExplorerControl.TemplateImport") + " ({0})", ex.Message),
                                Common.About.FormsTitle);
            }
            finally
            {
                suspended = false;
                try
                {
                    if (doc != null)
                    {
                        doc.Close(ref oFalse, ref oMissing, ref oMissing);
                    }
                    if (app != null)
                    {
                        app.Quit(ref oFalse, ref oMissing, ref oMissing);
                    }
                }
                catch
                {
                }

                // Remove the temp file.
                if( !String.IsNullOrEmpty( workingFilename ) )
                    File.Delete( workingFilename );
                
                Cursor = Cursors.Default;
            }
        }

        // Attachment import ->

        private void DoImportAttachment()
        {
            // check if we have a parent folder. then call CreateOutcome as appropriate.	
            TreeNode n = tvwExplorer.SelectedNode;
            Folder f = null;
                    
            if (n != null)
            {
                if (n.Tag is AttachmentFolder)
                {
                    f = (Folder) n.Tag;
                }
        
                openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                DialogResult dr = openFileDialog1.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    ImportAttachment(openFileDialog1.FileName, f);
                }
            }
        }


        private void DoLinkQuestion()
        {
            List<Question> qList = new List<Question>();
            foreach (Question q in package.Questions)
            {
                bool found = false;
                if (q.DataType == PerfectusDataType.Attachment)
                {
                    foreach (AttachmentBase attachment in package.Attachments)
                        if (attachment is AttachmentDynamic &&
                          ((AttachmentDynamic)attachment).GetPayload() == q.UniqueIdentifier)
                        {
                            found = true;
                            break;
                        }
                    if (!found)
                        qList.Add(q);
                }
            }

            if (qList.Count == 0)
            {
                MessageBox.Show(GetResource("Attachment.NoAttachmentQuestion"), GetResource("Attachment.NoAttachmentQuestionCaprion"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                AddAttachmentQuestion questionSelectionForm = new AddAttachmentQuestion();
                questionSelectionForm.SetQuestions(qList);
                if (questionSelectionForm.ShowDialog() == DialogResult.OK)
                {
                    ImportQuestionAttachment(questionSelectionForm.SelectedQuestion);
                }
            }
        }


        private void ImportAttachment(string filename, Folder f)
        {
            AttachmentStatic attachment;
            
            try
            {
                Cursor = Cursors.WaitCursor;
                suspended = true;

                attachment = package.CreateStaticAttachment(f) as AttachmentStatic;

                string attachmentName = new FileInfo(filename).Name;
                attachment.Name = attachmentName;

                attachment.SetPayload ( File.ReadAllBytes(filename));
                attachment.FileName = new TextQuestionQueryValue();
                attachment.FileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                attachment.FileName.TextValue = attachmentName;
                attachment.Notes = filename;

                SelectItem(attachment as PackageItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(GetResource("PackageExplorer.ExplorerControl.AttachmentImport") + " ({0})", ex.Message),
                                Common.About.FormsTitle);
            }
            finally
            {
                suspended = false;
                Cursor = Cursors.Default;
            }
        }


        private void ImportQuestionAttachment(Question question)
        {
            if (question.DataType != PerfectusDataType.Attachment)
                return;

            // check if we have a parent folder. then call CreateOutcome as appropriate.	
            TreeNode n = tvwExplorer.SelectedNode;
            Folder f = null;
            AttachmentDynamic attachment;

            if (n != null)
            {
                if (n.Tag is AttachmentFolder)
                {
                    f = (Folder)n.Tag;
                }
                try
                {
                    Cursor = Cursors.WaitCursor;
                    suspended = true;

                    attachment = package.CreateDynamicAttachment(f) as AttachmentDynamic;

                    string attachmentName = question.Name;
                    attachment.Name = attachmentName;
                    attachment.SetPayload(question.UniqueIdentifier);
                    attachment.FileName = new TextQuestionQueryValue();
                    attachment.FileName.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                    attachment.FileName.TextValue = attachmentName;
                    attachment.Notes = String.Format("Question: {0}", question.Name);
                    SelectItem(attachment as PackageItem);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format(GetResource("PackageExplorer.ExplorerControl.AttachmentImport") + " ({0})", ex.Message),
                                    Common.About.FormsTitle);
                }
                finally
                {
                    suspended = false;
                    Cursor = Cursors.Default;
                }
            }
        }
        // Attachment Import <-

        private void DoRename()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null)
            {
                bAllowEdit = true;
                n.BeginEdit();
            }
        }

        private PackageItem DoDuplicate()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && package != null && n.Tag is PackageItem)
            {
                return package.DuplicateItem((PackageItem) (n.Tag));
            }
            else
            {
                return null;
            }
        }


        private void DoRefreshServer()
        {
            Dictionary<String, DataTable> oldData = new Dictionary<String, DataTable>();

            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && n.Tag is Server)
            {
                Server svr = (Server)(n.Tag);

                // grab the livelink distributor property values before refreshing the server, in case we need to upgrade LL to OTCS
                // this data comes from the package .ip file
                //string Livelink = "Livelink";
                //int iLLDistCount = 0;
                //foreach (var plugin in svr.AvailablePlugins)
                //    if (plugin.TypeName.Contains(Livelink))
                //        iLLDistCount++;

                //if (iLLDistCount > 0)
                //{
                //    foreach (Rule2 rule in svr.Rule2)
                //        foreach (Delivery delivery in rule.Deliveries)
                //        {
                //            if (delivery.Distributor.Descriptor.FriendlyName.Contains(Livelink))
                //            {
                //                oldData.Add(delivery.UniqueIdentifier + "-" + delivery.Distributor.Descriptor.UniqueIdentifier, delivery.Distributor.GetPropertyValuesForPackageMigration());
                //            }
                //        }
                //}

                //refresh the server - this gets a list of plugins from the perfectus web server
                Uri address = new Uri(svr.Address);

                AddServerWrapper svrWrapper = new AddServerWrapper(0, address);
                BackgroundWorkerBase[] wrappers = new BackgroundWorkerBase[] { svrWrapper };
                ServiceProgressDialog dlg = new ServiceProgressDialog(wrappers);
                dlg.Start();
                DialogResult dr = dlg.ShowDialog(this);
                if (dr == DialogResult.Cancel)
                {
                    svrWrapper.Abort(false);
                }
                else if (dr == DialogResult.OK)
                {
                    svr.Name = svrWrapper.Info.Name;
                    svrWrapper.Info.Name = svr.Name;
                    svrWrapper.Info.UniqueIdentifier = svr.UniqueIdentifier;

                    svr.Refresh(svrWrapper.Info, svrWrapper.Plugins, oldData);

                    //Dec 2016 - redraw the entire tree to pick up any new names, re-sort nodes
                    ResetTreeMaintainSelection(svr);
                }
            }
        }

        private void DoRefreshDataSource()
        {
            TreeNode n = tvwExplorer.SelectedNode;
            if (n != null && n.Tag is WebReference)
            {
                Cursor = Cursors.WaitCursor;

                WebReference ds = n.Tag as WebReference;
                try
                {
                    this.Package.RefreshWebReference(ds.WebServiceAddress, ds.UniqueIdentifier);
                }
                catch (Exception ex)
                {
                    string msg = string.Format(GetResource("PackageExplorer.ExplorerControl.WebReferencesRefreshFailed"), ex.Message);
                    msg = msg.Replace("\\n", "\n");
                    MessageBox.Show(msg, Common.About.FormsTitle);
                }

                Cursor = Cursors.Default;
            }
        }


        private void mnuOpen_Click(object sender, EventArgs e)
        {
            DoDesignSelectedNode();
        }

        private void mnuDelete_Click(object sender, EventArgs e)
        {
            DoDelete();
        }

        private void mnuDependencies_Click(object sender, EventArgs e)
        {
            CheckDependencies();
        }

        private void mnuRename_Click(object sender, EventArgs e)
        {
            DoRename();
        }

        private void mnuRemoveFromRule_Click(object sender, EventArgs e)
        {
            DoDelete();
        }

        private void mnuImportTemplate_Click(object sender, EventArgs e)
        {
            DoImport();
        }

        private void mnuImportAttachment_Click(object sender, EventArgs e)
        {
            DoImportAttachment();
        }

        private void mnuLinkQuestionAttachment_Click(object sender, EventArgs e)
        {
            DoLinkQuestion();
        }

        private void mnuExportTemplate_Click(object sender, EventArgs e)
        {
            DoExport();
        }

        private void mnuRefresh_Click(object sender, EventArgs e)
        {
            DoRefreshServer();
        }

        private void mnuRefreshDataSource_Click(object sender, EventArgs e)
        {
            DoRefreshDataSource();
        }

        private void mnuNewInterview_Click(object sender, EventArgs e)
        {
            DoNewInterview();
        }

        private void mnuNewPage_Click(object sender, EventArgs e)
        {
            DoNewPage();
        }

        private void mnuNewArithmeticFunction_Click(object sender, EventArgs e)
        {
            DoNewArithmeticFunction();
        }

        private void mnuNewDateFunction_Click(object sender, EventArgs e)
        {
            DoNewDateFunction();
        }

        private void mnuNewOutcome_Click(object sender, EventArgs e)
        {
            DoNewOutcome();
        }

        private void mnuNewServer_Click(object sender, EventArgs e)
        {
            DoNewServer();
        }

        private void mnuNewRule_Click(object sender, EventArgs e)
        {
            DoNewRule();
        }

        private void mnuNewQuestion_Click(object sender, EventArgs e)
        {
            DoNewQuestion();
        }

        private void mnuNewFolder_Click(object sender, EventArgs e)
        {
            DoAddFolder();
        }

        private void mnuNewTemplate_Click(object sender, EventArgs e)
        {
            DoNewTemplate();
        }

        private void mnuNewAttachment_Click(object sender, EventArgs e)
        {
            DoNewAttachment();
        }

        private void mnuNewWebReference_Click(object sender, EventArgs e)
        {
            DoNewWebReference();
        }

        private void mnuAddDocumentToDelivery_Click(object sender, EventArgs e)
        {
            DoAddDocumentToDelivery();
        }

        private void mnuRemoveAllDocumentsFromDelivery_Click(object sender, EventArgs e)
        {
            DoRemoveAllDocumentsFromDelivery();
        }

        private void mnuEditDocumentToDelivery_Click(object sender, EventArgs e)
        {
            DoEditDocumentToDelivery();
        }


        private void mnuAddDeliveryToRule_Click(object sender, EventArgs e)
        {
            DoAddDeliveryToRule();
        }

        private void mnuAddDistributorToDelivery_Click(object sender, EventArgs e)
        {
            DoAddDistributorToDelivery();
        }

        private void mnuPublish_Click(object sender, EventArgs e)
        {
            DoPublish();
        }

        private void mnuNewExternalPage_Click(object sender, EventArgs e)
        {
            DoNewExternalPage();
        }

        private void DoPublish()
        {
            OnPublish();
        }

        private void OnPublish()
        {
            if (Publish != null)
            {
                Publish(this, new EventArgs());
            }
        }

        /// <summary>
        ///     Duplicate the given package item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuCreateCopy_Click(object sender, EventArgs e)
        {
            sharedLibraryEventsDisabled = true;
            DoDuplicate();
            sharedLibraryEventsDisabled = false;
        }

        public void Suspend()
        {
            suspended = true;
            tvwExplorer.SuspendLayout();
            tvwExplorer.AfterSelect -= new TreeViewEventHandler(tvwExplorer_AfterSelect);
        }

        public void Resume()
        {
            suspended = false;
            tvwExplorer.ResumeLayout();
            tvwExplorer.AfterSelect += new TreeViewEventHandler(tvwExplorer_AfterSelect);
        }

        #endregion

        #region Treeview - Drag & Drop

        #region Item Drag

        private void tvwExplorer_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (e.Item != null && e.Item is TreeNode)
            {
                object t = ((TreeNode) e.Item).Tag;
                if (t != null && t is PackageItem)
                {
                    tvwExplorer.SelectedNode = (TreeNode) e.Item;
                    DataObject obj = new DataObject();

                    obj.SetData("Perfectus.Common.Package.PackageItem", ((TreeNode) e.Item).Tag);

                    if (t is ITemplateItem)
                    {
                        ((ITemplateItem) t).AddWordMLTagToObject(obj);
                    }
                    if (t is IStringBindingItem && activeDesigner is TemplateDesigner != true)
                    {
                        ((IStringBindingItem) t).AddHTMLTagToObject(obj, null);
                    }

                    DoDragDrop(obj, DragDropEffects.All);
                }
            }
        }

        #endregion

        #region Drag Over

        private void tvwExplorer_DragOver(object sender, DragEventArgs e)
        {
            System.Drawing.Point pt = tvwExplorer.PointToClient(new System.Drawing.Point(e.X, e.Y));

            // Scroll if need be. there must be a better way to get the location, this seems a bit long winded?	
            if (e.Y < ParentForm.Location.Y + Location.Y + tvwExplorer.Location.Y + 100)
            {
                SendMessage(tvwExplorer.Handle, WM_VSCROLL, (IntPtr) SB_PAGEUP, IntPtr.Zero);
            }
            else if (e.Y > ParentForm.Location.Y + Location.Y + tvwExplorer.Location.Y + tvwExplorer.Height + 40)
            {
                SendMessage(tvwExplorer.Handle, WM_VSCROLL, (IntPtr) SB_PAGEDOWN, IntPtr.Zero);
            }

            TreeNode n = tvwExplorer.GetNodeAt(pt);
            if (n != null)
            {
                object t = n.Tag;

                if ((t is SimpleOutcomeCollection || t is OutcomeCollection || t is OutcomeFolder || t is SimpleOutcomeFolder) &&
                    DragDataIsWordML(e.Data))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is SimpleOutcome || t is Outcome) && DragDataIsWordML(e.Data))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is QuestionFolder || n == questionsNode) && (DragIsAQuestion(e.Data) || DragIsAQuestionFolder(e.Data)))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is AttachmentFolder || n == attachmentsNode) && (DragIsAnAttachment(e.Data) || DragIsAnAttachmentFolder(e.Data)))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is OutcomeFolder || n == outcomesNode) && (DragIsAOutcome(e.Data) || DragIsAOutcomeFolder(e.Data)))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is SimpleOutcomeFolder || n == simpleOutcomesNode) && (DragIsASimpleOutcome(e.Data) ||
                         DragIsASimpleOutcomeFolder(e.Data)))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((t is PFunctionFolder || n == functionsNode) && (DragIsADateFunction(e.Data) ||
                         DragIsAArithmeticFunction(e.Data) || DragIsAFunctionFolder(e.Data)))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else if (DragIsASharedLibraryItem(n, e))
                {
                    e.Effect = SharedLibraryItemDragOver(n, e);
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
        }

        private bool DragIsAQuestion(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is Question)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool DragIsAnAttachment(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is AttachmentBase)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        
        private bool DragIsAOutcome(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is Outcome)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool DragIsASimpleOutcome(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is SimpleOutcome)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool DragIsADateFunction(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is DatePFunction)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool DragIsAArithmeticFunction(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is ArithmeticPFunction)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        ///		Returns a boolean whether the given data is a package folder or not.
        /// </summary>
        /// <param name="dob">The data to be tested.</param>
        /// <returns>A flag indicating whether the object was a package folder or not.</returns>
        private bool DragIsAFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is Folder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///		Returns a boolean whether the given data is a question folder or not.
        /// </summary>
        /// <param name="dob">The data to be tested.</param>
        /// <returns>A flag indicating whether the object was a question folder or not.</returns>
        private bool DragIsAQuestionFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is QuestionFolder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        private bool DragIsAnAttachmentFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is AttachmentFolder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        ///		Returns a boolean whether the given data is a outcome folder or not.
        /// </summary>
        /// <param name="dob">The data to be tested.</param>
        /// <returns>A flag indicating whether the object was a outcome folder or not.</returns>
        private bool DragIsAOutcomeFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is OutcomeFolder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///		Returns a boolean where the given data is a simple outcome folder or not.
        /// </summary>
        /// <param name="dob">The data to be tested.</param>
        /// <returns>A flag indicating whether the object was a simple outcome folder or not.</returns>
        private bool DragIsASimpleOutcomeFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is SimpleOutcomeFolder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///		Returns a boolean where the given data is a function folder or not.
        /// </summary>
        /// <param name="dob">The data to be tested.</param>
        /// <returns>A flag indicating whether the object was a function folder or not.</returns>
        private bool DragIsAFunctionFolder(IDataObject dob)
        {
            if (dob.GetDataPresent("Perfectus.Common.Package.PackageItem"))
            {
                PackageItem p = dob.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                if (p != null && p is PFunctionFolder)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///		Handles a Shared Library node (from the shared library pane) being dropped onto an explorer node item and
        ///		returns the appropriate drag drop effects enum.
        /// </summary>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        /// <returns>The appropriate drag drop effects enum for the given action.</returns>
        private DragDropEffects SharedLibraryItemDragOver(TreeNode n, DragEventArgs e)
        {
            RepositoryItem repositoryItem = e.Data.GetData("Perfectus.Common.SharedLibrary.RepositoryItem") as RepositoryItem;

            // Only handle drags onto supported Shared Library items
            if (n == functionsNode && repositoryItem.LibraryItemType == LibraryItemType.Function)
                return DragDropEffects.Copy; // function
            else if (n == interviewNode && repositoryItem.LibraryItemType == LibraryItemType.InterviewPage)
                return DragDropEffects.Copy;
            else if (n.Tag is Interview && repositoryItem.LibraryItemType == LibraryItemType.InterviewPage)
                return DragDropEffects.Copy;
            else if (n == outcomesNode && repositoryItem.LibraryItemType == LibraryItemType.Outcome)
                return DragDropEffects.Copy; // outcome
            else if (n == questionsNode && repositoryItem.LibraryItemType == LibraryItemType.Question)
                return DragDropEffects.Copy; // question
            else if (n == simpleOutcomesNode && repositoryItem.LibraryItemType == LibraryItemType.SimpleOutcome)
                return DragDropEffects.Copy; // simple outcome
            else if (n == templatesNode && repositoryItem.LibraryItemType == LibraryItemType.Template)
                return DragDropEffects.Copy; // template
            else if (n.Tag is QuestionFolder && repositoryItem.LibraryItemType == LibraryItemType.Question)
                return DragDropEffects.Copy; // question folder
            else if (n.Tag is OutcomeFolder && repositoryItem.LibraryItemType == LibraryItemType.Outcome)
                return DragDropEffects.Copy; // outcome folder
            else if (n.Tag is SimpleOutcomeFolder && repositoryItem.LibraryItemType == LibraryItemType.SimpleOutcome)
                return DragDropEffects.Copy; // simple outcome folder
            else
                return DragDropEffects.None;
        }

        #endregion

        #region Drag Drop

        /// <summary>
        ///		Event Handler for the completion of drag and drop events on the explorer treeview control.
        ///		Handled drag events include: WordML data, item dragged onto a folder, folder dragged onto another folder,
        ///		aa folder dragged onto a root item and Shared Library items being dragged into the explorer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvwExplorer_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                System.Drawing.Point pt = tvwExplorer.PointToClient(new System.Drawing.Point(e.X, e.Y));

                // Node being dropped onto at the given point
                TreeNode n = tvwExplorer.GetNodeAt(pt);

                Application.DoEvents();

                if (n != null)
                {
                    // Object being dropped onto
                    object t = n.Tag;

                    // WordML Drag
                    if (DragDataIsWordML(e.Data))
                        HandleDragWordMLData(t, n, e);

                    // Item draged onto folder Drag
                    if (DragIsItemOntoFolder(t, n, e))
                        HandleDragItemOntoFolder(t, n, e);

                    // Folder Drag
                    if (DragIsAFolder(e.Data))
                        HandleDragFolderOntoFolder(t, n, e);

                    // Folder dragged onto root item
                    if (DragIsFolderOntoRootItem(n, e))
                        HandleDragFolderOntoRootItem(t, n, e);

                    // Shared Library item drag
                    if (DragIsASharedLibraryItem(n, e)) {

                        tvwExplorer.SuspendLayout();
                        HandleDragFromSharedLibrary(t, n, e);

                        if (tvwExplorer.SelectedNode != null && tvwExplorer.SelectedNode.Tag is LibraryItem) {
                            SetPropertyGridSelectedObject((LibraryItem) tvwExplorer.SelectedNode.Tag, GetCurrentWindowsUsername());
                            tvwExplorer.SelectedNode.EndEdit(false);
                        }

                        tvwExplorer.ResumeLayout(true);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL3);
                DisplayErrorToUI(ex);
            }
        }

        /// <summary>
        ///		Evaluates whether a drag drop action onto a treenode contains WordML data.
        /// </summary>
        /// <param name="obj">The data of the object being dropped.</param>
        /// <returns>boolean indicating whether the drag drop contains WordML Data.</returns>
        private bool DragDataIsWordML(IDataObject obj)
        {
            if (obj.GetDataPresent(DataFormats.Rtf, false) && obj.GetDataPresent(DataFormats.Html, false) &&
                obj.GetDataPresent("Woozle"))
                return true;
            else
                return false;
        }

        /// <summary>
        ///		Evaluates whether a folder drag is onto a root Package Item treenode.
        ///		Only package items of Questions, Outcomes, or SimpleOutcomes can have there folders
        ///		dragged onto there root items. (These types only support folders).
        /// </summary>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        /// <returns>boolean indicating whether the drag is onto a root node or not.</returns>
        private bool DragIsFolderOntoRootNode(TreeNode n, DragEventArgs e)
        {
            if (n == questionsNode || n == outcomesNode || n == simpleOutcomesNode)
            {
                if (DragIsAQuestionFolder(e.Data) || DragIsAOutcomeFolder(e.Data) || DragIsASimpleOutcomeFolder(e.Data))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///		Evaluates whether an item drag is onto a folder.
        /// </summary>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        /// <returns>boolean indicating whether the drag is onto a root node or not.</returns>
        private bool DragIsItemOntoFolder(object t, TreeNode n, DragEventArgs e)
        {
            // Drag is onto a folder
            if (t is QuestionFolder || t is OutcomeFolder || t is SimpleOutcomeFolder || t is PFunctionFolder || t is AttachmentFolder)
            {
                if (DragIsAQuestion(e.Data) || DragIsAOutcome(e.Data) || DragIsASimpleOutcome(e.Data) ||
                    DragIsAArithmeticFunction(e.Data) || DragIsADateFunction(e.Data) ||
                    DragIsAnAttachment(e.Data))
                    return true;
            }

            // Drag is onto an item
            if (n == questionsNode || n == outcomesNode || n == simpleOutcomesNode || n == functionsNode || n == attachmentsNode)
            {
                if (DragIsAQuestion(e.Data) || DragIsAOutcome(e.Data) || DragIsASimpleOutcome(e.Data) ||
                    DragIsAArithmeticFunction(e.Data) || DragIsADateFunction(e.Data) ||
                    DragIsAnAttachment(e.Data))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///		Handles a folder tree node being dragged from the explorer tree onto a root node item.
        /// </summary>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private bool DragIsFolderOntoRootItem(TreeNode n, DragEventArgs e)
        {
            if (n == questionsNode || n == outcomesNode || n == simpleOutcomesNode || n == functionsNode || n == attachmentsNode)
            {
                if (DragIsAQuestionFolder(e.Data) || DragIsAOutcomeFolder(e.Data) || DragIsASimpleOutcomeFolder(e.Data) ||
                    DragIsAFunctionFolder(e.Data) || DragIsAnAttachmentFolder(e.Data))
                    return true;
            }

            return false;
        }

        /// <summary>
        ///		Handles a Shared Library node (from the shared library pane) being dropped onto an explorer node item.
        /// </summary>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private bool DragIsASharedLibraryItem(TreeNode n, DragEventArgs e)
        {
            // Only handle drags onto supported Shared Library items
            if (n == questionsNode || n == outcomesNode || n == simpleOutcomesNode || n == templatesNode || n == functionsNode ||
                n == interviewNode || n.Tag is Interview || n.Tag is SimpleOutcomeFolder || n.Tag is OutcomeFolder || 
                n.Tag is QuestionFolder) {

                if (e.Data.GetDataPresent("Perfectus.Common.SharedLibrary.RepositoryItem"))
                {
                    RepositoryItem repositoryItem = e.Data.GetData("Perfectus.Common.SharedLibrary.RepositoryItem") as RepositoryItem;

                    // Only allow drags it there is a valid repository item being dropped
                    if (repositoryItem != null)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            else
                return false;
        }

        /// <summary>
        ///		Handles a tree node being dragged from the explorer tree which contains WordML data.
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private void HandleDragWordMLData(object t, TreeNode n, DragEventArgs e)
        {
            if (t is SimpleOutcomeCollection || t is SimpleOutcomeFolder)
            {
                SimpleOutcome so = package.CreateSimpleOutcome();
                ObjectNameEditorDialog dlg = new ObjectNameEditorDialog();
                dlg.ItemName = so.Name;
                DialogResult dr = dlg.ShowDialog();

                if (dr == DialogResult.Cancel)
                    package.DeleteItem(so, true);
                else
                {
                    if (dr == DialogResult.OK)
                    {
                        if (dlg.ItemName != so.Name)
                            so.Name = dlg.ItemName;

                        OnSimpleOutcomeDrop(so);
                        
                        if (t is SimpleOutcomeFolder)
                        {
                            TreeNode newNode = GetNodeByDropTarget(so, simpleOutcomesNode);
                            so.ParentFolder = (SimpleOutcomeFolder)t;
                            MoveNode(newNode, n);
                        }
                    }
                }
            }
            else if (t is SimpleOutcome)
            {
                OnSimpleOutcomeDrop((SimpleOutcome) t);
            }
            else if (t is OutcomeCollection || t is OutcomeFolder)
            {
                Outcome o = package.CreateOutcome();
                ObjectNameEditorDialog dlg = new ObjectNameEditorDialog();
                dlg.ItemName = o.Name;
                dlg.DialogMoved += new EventHandler<PackageItemEventArgs>(ObjectNameEditorDialog_DialogMoved);
                DialogResult dr = dlg.ShowDialog();
                if (dr == DialogResult.Cancel)
                {
                    package.DeleteItem(o, true);
                }
                else
                {
                    if (dr == DialogResult.OK)
                    {
                        if (dlg.ItemName != o.Name)
                        {
                            o.Name = dlg.ItemName;
                        }

                        OnOutcomeDrop(o);

                        if (t is OutcomeFolder)
                        {
                            o.ParentFolder = (Folder) t;
                            TreeNode newNode = GetNodeByDropTarget(o, outcomesNode);
                            MoveNode(newNode, n);
                        }
                    }
                }
            }
            else if (t is Outcome)
            {
                OnOutcomeDrop((Outcome) t);
            }
        }

        /// <summary>
        /// event fired, is captured by the main designer. This is fired when new outcme dialog pop-up is moved. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectNameEditorDialog_DialogMoved(object sender, PackageItemEventArgs e)
        {
            if (PopupDialogMoved != null)
            {
                PopupDialogMoved(sender, e);
            }
        }

        /// <summary>
        ///		Handles drag and drops from the shared library onto explorer items.
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private void HandleDragFromSharedLibrary(object t, TreeNode n, DragEventArgs e)
        {
            LibraryItem libraryItem = null;
            RepositoryItem repositoryItem = null;
            repositoryItem = e.Data.GetData("Perfectus.Common.SharedLibrary.RepositoryItem") as RepositoryItem;

            try
            {
                Cursor = Cursors.WaitCursor;

                // Ensure we have valid data
                if (repositoryItem == null)
                {
                    throw new NullReferenceException(GetResource("PackageExplorer.ExplorerControl.NullSharedLibraryItem"));
                }

                // Disable update events on the new item
                sharedLibraryEventsDisabled = true;
                tvwExplorer.SuspendLayout();
                libraryItem = HandleSharedLibraryDrag(t, n, e);

                if (libraryItem != null)
                {
                    UpdateSelectedLibraryItemNode(libraryItem);
                }
            }
            finally
            {
                sharedLibraryEventsDisabled = false;
                tvwExplorer.ResumeLayout();

                // Ensure the default cursor is set back
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        ///		Handles drag and drop from the shared library onto explorer items.
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private LibraryItem HandleSharedLibraryDrag(object t, TreeNode n, DragEventArgs e)
        {
            LibraryItem libraryItem = null;
            bool replaceExisting = false;

            try
            {
                RepositoryItem repositoryItem = (RepositoryItem) e.Data.GetData(typeof (RepositoryItem));

                // Get the object from the shared library
                libraryItem = sharedLibraryController.GetLatestVersion(repositoryItem);
                if (null == libraryItem.ParentPackage)
                    libraryItem.ParentPackage = this.Package;

                libraryItem.DependentObject = false; // This is the root item and is therefore not dependent

                // Is the function new or existing?
                switch (GetMatchedLibraryItemAction(libraryItem))
                { 
                    case MatchedAction.Replace:
                        replaceExisting = true;
                        break;
                    case MatchedAction.CreateNew:
                        replaceExisting = false;
                        break;
                    default:
                        return null;
                }

                // Add the item to the package (new | replace existing)
                package.AddLibraryItem(libraryItem, sharedLibraryProxy, true, false, replaceExisting);
                
                // Change the nodes image to show a linked function (either checked in or out)
                if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedIn)
                {
                    SetTreeNodeImage(tvwExplorer.SelectedNode, CheckOutStatus.CheckedIn);
                }
                else if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedOut && libraryItem.CheckedOutByLogInName != null &&
                         libraryItem.CheckedOutByLogInName.ToLower() == WindowsIdentity.GetCurrent().Name.ToLower())
                {
                    SetTreeNodeImage(tvwExplorer.SelectedNode, CheckOutStatus.CheckedOut);
                }
                else
                {
                    SetTreeNodeImage(tvwExplorer.SelectedNode, CheckOutStatus.CheckedOutOtherUser);
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }

            return libraryItem;
        }

        private void SetLibraryItemTreeNodeIcon(LibraryItem sharedLibraryItem)
        {
            int iconIndex;
            TreeNode libraryItemTreeNode = null;

            // Update the dependent items if provided
            if (sharedLibraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in sharedLibraryItem.DependentLibraryItems)
                {
                    SetLibraryItemTreeNodeIcon(dependentLibraryItem);
                }
            }

            // Find the given treenode
            libraryItemTreeNode = SelectItem((PackageItem)sharedLibraryItem);

            // Update the library item
            if (libraryItemTreeNode != null)
            {
                // Commented Out due to FB-1426
                //if (sharedLibraryItem.DependentObject)
                //{
                    //Package.HandleLibraryItemUpdate((LibraryItem)libraryItemTreeNode.Tag, sharedLibraryItem);
                //}

                tvwExplorer.SelectedNode = libraryItemTreeNode;

                // Update the node
                if (libraryItemTreeNode != null && libraryItemTreeNode.Tag is LibraryItem)
                {
                    // Set the appropriate (library status) icon index to display for the selected item
                    iconIndex = DiscoverLibraryItemTreeNodeIconToSet((LibraryItem)libraryItemTreeNode.Tag);

                    // Set the icon to the set icon index
                    libraryItemTreeNode.ImageIndex = iconIndex;
                    libraryItemTreeNode.SelectedImageIndex = iconIndex;
                }
            }

            sharedLibraryItem.IsNewLibraryItem = false;
        }

        /// <summary>
        ///		Handles a tree node being dragged from onto the explorer tree which contains WordML data.
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private void HandleDragItemOntoFolder(object t, TreeNode n, DragEventArgs e)
        {
            if (t is QuestionFolder && DragIsAQuestion(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                QuestionFolder f = (QuestionFolder) t;
                ((Question) p).ParentFolder = f;
                TreeNode newNode = GetNodeByDropTarget(p, questionsNode);
                MoveNode(newNode, n);
            }
            if (t is AttachmentFolder && DragIsAnAttachment(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                AttachmentFolder f = (AttachmentFolder)t;
                ((AttachmentBase)p).ParentFolder = f;
                TreeNode newNode = GetNodeByDropTarget(p, attachmentsNode);
                MoveNode(newNode, n);
            }
            else if (t is OutcomeFolder && DragIsAOutcome(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                OutcomeFolder f = (OutcomeFolder) t;
                ((Outcome) p).ParentFolder = f;
                TreeNode newNode = GetNodeByDropTarget(p, outcomesNode);
                MoveNode(newNode, n);
            }
            else if (t is SimpleOutcomeFolder && DragIsASimpleOutcome(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                SimpleOutcomeFolder f = (SimpleOutcomeFolder) t;
                ((SimpleOutcome) p).ParentFolder = f;
                TreeNode newNode = GetNodeByDropTarget(p, simpleOutcomesNode);
                MoveNode(newNode, n);
            } // Function Folder
            else if (t is PFunctionFolder && (DragIsAArithmeticFunction(e.Data) || DragIsADateFunction(e.Data)))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                PFunctionFolder f = (PFunctionFolder) t;
                if (p is ArithmeticPFunction)
                {
                    ((ArithmeticPFunction) p).ParentFolder = f;
                }
                else if (p is DatePFunction)
                {
                    ((DatePFunction) p).ParentFolder = f;
                }
                TreeNode newNode = GetNodeByDropTarget(p, functionsNode);
                MoveNode(newNode, n);
            }
            else if (n == questionsNode && DragIsAQuestion(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((Question) p).ParentFolder = null;
                TreeNode newNode = GetNodeByDropTarget(p, questionsNode);
                MoveNode(newNode, n);
            }
            else if (n == attachmentsNode && DragIsAnAttachment(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((AttachmentBase)p).ParentFolder = null;
                TreeNode newNode = GetNodeByDropTarget(p, attachmentsNode);
                MoveNode(newNode, n);
            }
            else if (n == outcomesNode && DragIsAOutcome(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((Outcome) p).ParentFolder = null;
                TreeNode newNode = GetNodeByDropTarget(p, outcomesNode);
                MoveNode(newNode, n);
            }
            else if (n == simpleOutcomesNode && DragIsASimpleOutcome(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((SimpleOutcome) p).ParentFolder = null;
                TreeNode newNode = GetNodeByDropTarget(p, simpleOutcomesNode);
                MoveNode(newNode, n);
            } // Function
            else if (n == functionsNode && (DragIsAArithmeticFunction(e.Data) || DragIsADateFunction(e.Data)))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;

                if (p is ArithmeticPFunction)
                {
                    ((ArithmeticPFunction) p).ParentFolder = null;
                }
                else if (p is DatePFunction)
                {
                    ((DatePFunction) p).ParentFolder = null;
                }

                TreeNode newNode = GetNodeByDropTarget(p, functionsNode);
                MoveNode(newNode, n);
            }
        }

        /// <summary>
        ///		Handles a tree node being dragged from the explorer tree and dropped onto it's root item node
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private void HandleDragFolderOntoRootItem(object t, TreeNode n, DragEventArgs e)
        {
            // Questions
            if (n == questionsNode && DragIsAQuestionFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((QuestionFolder) p).ChangeToRootFolder();
                TreeNode newNode = GetNodeByDropTarget(p, questionsNode);
                MoveNode(newNode, n);
            }
            // Attachments
            if (n == attachmentsNode && DragIsAnAttachmentFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((AttachmentFolder)p).ChangeToRootFolder();
                TreeNode newNode = GetNodeByDropTarget(p, attachmentsNode);
                MoveNode(newNode, n);
            }
            // Outcomes
            else if (n == outcomesNode && DragIsAOutcomeFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((OutcomeFolder) p).ChangeToRootFolder();
                TreeNode newNode = GetNodeByDropTarget(p, outcomesNode);
                MoveNode(newNode, n);
            } // Simple Outcomes
            else if (n == simpleOutcomesNode && DragIsASimpleOutcomeFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((SimpleOutcomeFolder) p).ChangeToRootFolder();
                TreeNode newNode = GetNodeByDropTarget(p, simpleOutcomesNode);
                MoveNode(newNode, n);
            } // Functions
            else if (n == functionsNode && DragIsAFunctionFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                ((PFunctionFolder) p).ChangeToRootFolder();
                TreeNode newNode = GetNodeByDropTarget(p, functionsNode);
                MoveNode(newNode, n);
            }
        }

        /// <summary>
        ///		Handles drag and drops of folders onto folders.
        /// </summary>
        /// <param name="t">The object being dropped onto.</param>
        /// <param name="n">The node being droppped onto.</param>
        /// <param name="e">The data being dropped.</param>
        private void HandleDragFolderOntoFolder(object t, TreeNode n, DragEventArgs e)
        {
            // Questions
            if (t is QuestionFolder && DragIsAQuestionFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                QuestionFolder f = (QuestionFolder) t;
                if (((QuestionFolder) p).MoveToFolder(f))
                {
                    TreeNode newNode = GetNodeByDropTarget(p, questionsNode);
                    MoveNode(newNode, n);
                }
            }
            // Attachments
            if (t is AttachmentFolder && DragIsAnAttachmentFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                AttachmentFolder f = (AttachmentFolder )t;
                if (((AttachmentFolder)p).MoveToFolder(f))
                {
                    TreeNode newNode = GetNodeByDropTarget(p, attachmentsNode);
                    MoveNode(newNode, n);
                }
            } // Outcomes
            else if (t is OutcomeFolder && DragIsAOutcomeFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                OutcomeFolder f = (OutcomeFolder) t;
                if (((OutcomeFolder) p).MoveToFolder(f))
                {
                    TreeNode newNode = GetNodeByDropTarget(p, outcomesNode);
                    MoveNode(newNode, n);
                }
            } // Simple Outcomes
            else if (t is SimpleOutcomeFolder && DragIsASimpleOutcomeFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                SimpleOutcomeFolder f = (SimpleOutcomeFolder) t;
                if (((SimpleOutcomeFolder) p).MoveToFolder(f))
                {
                    TreeNode newNode = GetNodeByDropTarget(p, simpleOutcomesNode);
                    MoveNode(newNode, n);
                }
            } // Functions
            else if (t is PFunctionFolder && DragIsAFunctionFolder(e.Data))
            {
                PackageItem p = e.Data.GetData("Perfectus.Common.Package.PackageItem") as PackageItem;
                PFunctionFolder f = (PFunctionFolder) t;
                if (((PFunctionFolder) p).MoveToFolder(f))
                {
                    TreeNode newNode = GetNodeByDropTarget(p, functionsNode);
                    MoveNode(newNode, n);
                }
            }
        }

        private TreeNode GetNodeByDropTarget(PackageItem p, TreeNode startNode)
        {
            PfTreeNodeCollection tnc = new PfTreeNodeCollection();

            // walk treeNodes, adding all the time to our treeNode collection
            RecursiveGetAllNodes(startNode, tnc);

            foreach (TreeNode n in tnc)
            {
                if (n.Tag == p)
                {
                    return n;
                }
            }
            return null;
        }

        private void RecursiveGetAllNodes(TreeNode startNode, PfTreeNodeCollection nodesCollectionToBuild)
        {
            nodesCollectionToBuild.Add(startNode);

            foreach (TreeNode n in startNode.Nodes)
            {
                nodesCollectionToBuild.Add(n);
                if (n.Nodes.Count > 0)
                {
                    foreach (TreeNode subn in n.Nodes)
                    {
                        RecursiveGetAllNodes(subn, nodesCollectionToBuild);
                    }
                }
            }
        }

        private void MoveNode(TreeNode newNode, TreeNode targetNode)
        {
            if (newNode != null && targetNode != null)
            {
                if (propertyGrid != null)
                {
                    propertyGrid.Hide();
                }

                newNode.Remove();
                targetNode.Nodes.Add(newNode);
                tvwExplorer.SelectedNode = newNode;
                newNode.EnsureVisible();

                if (propertyGrid != null)
                {
                    propertyGrid.Show();
                    propertyGrid.Refresh();
                }
            }
        }

        private void OnSimpleOutcomeDrop(SimpleOutcome so)
        {
            if (SimpleOutcomeDrop != null)
            {
                SimpleOutcomeDrop(this, new PackageItemEventArgs(so));
            }
        }

        private void OnOutcomeDrop(Outcome o)
        {
            if (OutcomeDrop != null)
            {
                OutcomeDrop(this, new PackageItemEventArgs(o));
            }
        }

        #endregion

        #endregion

        #region Treeview - Plugins

        private void tvwExplorer_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    DoDelete();
                    break;

                case Keys.F2:
                    DoRename();
                    break;
            }
        }

        private void MainDesignerX_ActiveDesignerChanged(DesignerBase item)
        {
            activeDesigner = item;
        }

        private void LoadPlugins()
        {
            importTemplateMenuItems = new ArrayList();
            exportTemplateMenuItems = new ArrayList();
            if (plugins != null)
            {
                foreach (IStudioPlugin p in plugins)
                {
                    EventHandler importHandler = new EventHandler(ImportPlugin_Click);
                    EventHandler exportHandler = new EventHandler(ExportPlugin_Click);

                    if (p is IOpenSaveHandler)
                    {
                        IOpenSaveHandler osh = (IOpenSaveHandler) p;
                        if (osh.SupportsImportTemplate)
                        {
                            AddImportPluginMenuItems(importHandler, osh);
                        }
                        if (osh.SupportsExportTemplate)
                        {
                            AddExportPluginMenuItems(exportHandler, osh);
                        }
                    }
                }
            }
        }

        private void AddImportPluginMenuItems(EventHandler importHandler, IOpenSaveHandler osh)
        {
            if (osh.SupportsImportTemplate)
            {
                MenuItem mi = new MenuItem();
                importTemplateMenuItems.Add(mi);
                mi.Click += importHandler;
                mi.Tag = osh;
                mi.Text = osh.ImportTemplateMenuName;

                int standardItemIndex = cmExplorer.MenuItems.IndexOf(mnuImportTemplate);
                cmExplorer.MenuItems.Add(standardItemIndex + 1, mi);
            }
        }

        private void AddExportPluginMenuItems(EventHandler exportHandler, IOpenSaveHandler osh)
        {
            if (osh.SupportsImportTemplate)
            {
                MenuItem mi = new MenuItem();
                exportTemplateMenuItems.Add(mi);
                mi.Click += exportHandler;
                mi.Tag = osh;
                mi.Text = osh.ExportTemplateMenuName;

                int standardItemIndex = cmExplorer.MenuItems.IndexOf(mnuExportTemplate);
                cmExplorer.MenuItems.Add(standardItemIndex + 1, mi);
            }
        }

        private void ImportPlugin_Click(object sender, EventArgs e)
        {
            //if (sender is ButtonTool)
            //{
            //    ButtonTool bt = (ButtonTool) sender;
            //    if (bt.Tag != null && bt.Tag is IOpenSaveHandler)
            //    {
            //        IOpenSaveHandler osh = (IOpenSaveHandler) bt.Tag;
            //        DoPluginImport(osh);
            //    }
            //}
            //else if (sender is MenuItem)
            if (sender is MenuItem)
            {
                MenuItem mi = (MenuItem) sender;
                if (mi.Tag != null && mi.Tag is IOpenSaveHandler)
                {
                    IOpenSaveHandler osh = (IOpenSaveHandler) mi.Tag;
                    DoPluginImport(osh);
                }
            }
        }

        private void ExportPlugin_Click(object sender, EventArgs e)
        {
            if (sender is MenuItem)
            {
                MenuItem mi = (MenuItem)sender;
                if (mi.Tag != null && mi.Tag is IOpenSaveHandler)
                {
                    IOpenSaveHandler osh = (IOpenSaveHandler)mi.Tag;

                    TreeNode n = tvwExplorer.SelectedNode;
                    if (n != null)
                    {
                        object tag = n.Tag;
                        if (tag is WordTemplateDocument2)
                        {
                            DoPluginExport(osh, tag as WordTemplateDocument2);
                        }
                    }
                }
            }
        }

        private void DoPluginImport(IOpenSaveHandler plugin)
        {
            string filename = null;
            bool isOkay = false;
            try
            {
                isOkay = plugin.ImportTemplate(out filename);
            }
            catch (Exception ex)
            {
                string msg = string.Format(GetResource("Perfectus.Client.MainDesigner.PluginError"), plugin.Name, ex.Message);
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(msg, Common.About.FormsTitle);
            }

            if (isOkay && filename != null)
            {
                try
                {
                    try
                    {
                        ImportTemplate(filename);
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format(GetResource("PackageExplorer.PluginImportError"), plugin.Name, ex.Message);
                        msg = msg.Replace("\\n", "\n");
                        MessageBox.Show(msg, Common.About.FormsTitle);
                    }
                }
                finally
                {
                    try
                    {
                        plugin.ImportTemplateCleanUp(filename);
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format(GetResource("PackageExplorer.PluginImportCleanupError"), plugin.Name, ex.Message);
                        msg = msg.Replace("\\n", "\n");
                        MessageBox.Show(msg, Common.About.FormsTitle);
                    }
                }
            }
        }

        private static void DoPluginExport(IOpenSaveHandler plugin, WordTemplateDocument2 templateDoc)
        {
            bool isOkay = false;

            try
            {
                isOkay = plugin.SupportsExportTemplate;
            }
            catch (Exception ex)
            {
                string msg = string.Format(GetResource("Perfectus.Client.MainDesigner.PluginError"), plugin.Name, ex.Message);
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(msg, Common.About.FormsTitle);
            }

            if (isOkay)
            {
                try
                {
                    //pf-3065 save docx file
                    //string newFilepath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()) + ".xml";
                    string newFilepath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()) + ".docx";

                    templateDoc.Export(newFilepath);
                    plugin.ExportTemplate(newFilepath, templateDoc.Name,
                                          MainDesigner2.LegaliseFilename(templateDoc.Name));

                    try
                    {
                        File.Delete(newFilepath);
                    }
                    catch (Exception Exception)
                    { }
                }
                catch (Exception ex)
                {
                    string msg = string.Format(GetResource("Perfectus.Client.MainDesigner.PluginError"), plugin.Name, ex.Message);
                    msg = msg.Replace("\\n", "\n");
                    MessageBox.Show(msg, Common.About.FormsTitle);
                }
            }
        }

        #endregion

        #region Shared Library

        private void mnuUpdateAllLinkedSharedLibraryItems_Click(object sender, EventArgs e)
        {
            sharedLibraryEventsDisabled = true;
            string unconnectedMessage = package.UpdateAllLinkedSharedLibraryItems(sharedLibraryProxy);
            sharedLibraryEventsDisabled = false;

            if (package != null)
            {
                if (unconnectedMessage.Length > 0)
                {
                    MessageBox.Show(GetResource("PackageExplorer.ExplorerControls.UnableToReconnect") + "\n\n" + unconnectedMessage,
                                    GetResource("PackageExplorer.ExplorerControls.UnableToReconnect2"), MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
            
            ResetTreeWithCollapse();
        }

        private void mnuCheckIn_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode treenode = tvwExplorer.SelectedNode;

                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        LibraryItem libraryItem = (LibraryItem)treenode.Tag;

                        LibraryItem existingItem = package.GetLibraryItemByName(libraryItem);
                        if (ItemContainsUnlinkedDependentItems(existingItem))
                        {
                            return;
                        }

                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                        CheckOutStatus checkoutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);
                        
                        switch (checkoutStatus)
                        {
                            case CheckOutStatus.CheckedOut:
                                Boolean itemIsCheckedIn = PerformLibraryItemCheckIn(libraryItem, treenode);

                                // Only update the check in status it the action was performed sucessfully
                                if (itemIsCheckedIn)
                                {
                                    checkoutStatus = CheckOutStatus.CheckedIn;
                                }
                                
                                break;
                            case CheckOutStatus.Unlinked:
                                // Unlink and change the icon
                                PerformLibraryItemUnlink(libraryItem);
                                checkoutStatus = CheckOutStatus.Unlinked;
                                break;
                            default:
                                // Item is already checked into the library - ask if they want the latest version
                                DialogResult result = MessageBox.Show(GetResource("AlreadyCheckedIn"), GetResource("SharedLibrary"), 
                                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (result == DialogResult.Yes)
                                {
                                    PerformLibraryItemGetLatestVersion(libraryItem);
                                    checkoutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);
                                }
                                else
                                {
                                    checkoutStatus = CheckOutStatus.CheckedIn;
                                }
                                break;
                        }

                        SetTreeNodeImage(treenode, checkoutStatus);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                        sharedLibraryEventsDisabled = false; // Re-enable
                    }
                }
            }

            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode treenode = tvwExplorer.SelectedNode;

                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        LibraryItem libraryItem = (LibraryItem)treenode.Tag;
                        LibraryItem existingItem = package.GetLibraryItemByName(libraryItem);
                        if (ItemContainsUnlinkedDependentItems(existingItem))
                        {
                            return;
                        }

                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                        CheckOutStatus checkOutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);
                        
                        switch (checkOutStatus)
                        {
                            case CheckOutStatus.CheckedIn:
                                if (PerformLibraryItemGetLatestVersion(libraryItem)) // Ensure dependencies
                                checkOutStatus = PerformLibraryItemCheckOut(libraryItem);
                                break;
                            case CheckOutStatus.Unlinked:
                                // Unlink and change the icon
                                PerformLibraryItemUnlink(libraryItem);
                                checkOutStatus = CheckOutStatus.Unlinked;
                                break;
                            case CheckOutStatus.CheckedOutOtherUser:
                            case CheckOutStatus.CheckedOut:

                                checkOutStatus = CheckOutStatus.CheckedOutOtherUser;
                                libraryItem.CheckedOutStatus = CheckOutStatus.CheckedIn; // Set checked in status - as the item is created out else where

                                // Item is already checked out of the library - inform the user
                                DialogResult result = MessageBox.Show(GetResource("AlreadyCheckedOut"), GetResource("SharedLibrary"),
                                                                      MessageBoxButtons.OK, MessageBoxIcon.Information);

                                break;
                        }                        
                        
                        SetTreeNodeImage(treenode, checkOutStatus);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                        sharedLibraryEventsDisabled = false; // Re-enable
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuUndoCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode treenode = tvwExplorer.SelectedNode;

                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        LibraryItem libraryItem = (LibraryItem)treenode.Tag;
                        LibraryItem existingItem = package.GetLibraryItemByName(libraryItem);
                        if (ItemContainsUnlinkedDependentItems(existingItem))
                        {
                            return;
                        }
                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                        CheckOutStatus checkOutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);

                        switch (checkOutStatus)
                        { 
                            case CheckOutStatus.CheckedOut:
                                PerformLibraryItemUndoCheckOut(libraryItem, ref checkOutStatus);
                                break;
                            default:
                                // Item is already checked into the library - ask if they want the latest version
                                DialogResult result = MessageBox.Show(GetResource("AlreadyCheckedIn"), GetResource("SharedLibrary"),
                                                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                                if (result == DialogResult.Yes)
                                {
                                    PerformLibraryItemGetLatestVersion(libraryItem);
                                    checkOutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);
                                }
                                break;
                        }
                        
                        sharedLibraryEventsDisabled = false; // Re-enable

                        // Change the icon
                        SetTreeNodeImage(treenode, checkOutStatus);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuLinkSharedItem_Click(object sender, EventArgs e)
        {
            LibraryItem libraryItem;
            TreeNode treenode = tvwExplorer.SelectedNode;

            try
            {
                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        // Get the item, link it and update the treeview image
                        libraryItem = (LibraryItem) treenode.Tag;
                        LibraryItem existingItem = package.GetLibraryItemByName(libraryItem);
                        if (ItemContainsUnlinkedDependentItems(existingItem))
                        {
                            return;
                        }

                        DialogResult result = MessageBox.Show(GetResource("ReLinkWarnChanges"), GetResource("SharedLibrary"),
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result != DialogResult.Yes)
                            return;
                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)
                        PerformLibraryItemLink(libraryItem);
                        PerformLibraryItemGetLatestVersion(libraryItem);
                        CheckOutStatus checkOutStatus= GetLibraryItemSharedLibraryStatus(libraryItem);

                        SetTreeNodeImage(treenode, checkOutStatus);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                        sharedLibraryEventsDisabled = false; // Re-enable

                        if (libraryItem != null)
                        {
                            UpdateSelectedLibraryItemNode(libraryItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuUnlinkSharedItem_Click(object sender, EventArgs e)
        {
            LibraryItem libraryItem;
            TreeNode treenode = tvwExplorer.SelectedNode;

            try
            {
                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                        // Get the item, unlink and update the treeview image
                        libraryItem = (LibraryItem) treenode.Tag;
                        PerformLibraryItemUnlink(libraryItem);
                        SetTreeNodeImage(treenode, CheckOutStatus.Unlinked);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                        sharedLibraryEventsDisabled = false; // Re-enable
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuDisassociateSharedLibraryItem_Click(object sender, EventArgs e)
        {
            LibraryItem libraryItem;
            TreeNode treenode = tvwExplorer.SelectedNode;

            try
            {
                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        string message = GetResource("SharedLibrary.Explorer.DisassoicationConfirmation");
                        string caption = GetResource("SharedLibrary.Explorer.DisassoicationConfirmationCaption");
                        
                        DialogResult result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (result == DialogResult.Yes)
                        {

                            sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                            // Get the item, unlink and update the treeview image
                            libraryItem = (LibraryItem)treenode.Tag;
                            libraryItem.DisassociateFromSharedLibrary();
                            SetTreeNodeImage(treenode, CheckOutStatus.Disassociated);
                            SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                            sharedLibraryEventsDisabled = false; // Re-enable
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuGetLatestVersion_Click(object sender, EventArgs e)
        {
            LibraryItem libraryItem;
            TreeNode treenode = tvwExplorer.SelectedNode;

            try
            {
                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        sharedLibraryEventsDisabled = true; // Temp disable on add (don't fire check out events etc)

                        libraryItem = (LibraryItem) treenode.Tag;
                        LibraryItem existingItem = package.GetLibraryItemByName(libraryItem);
                        if (ItemContainsUnlinkedDependentItems(existingItem))
                        {
                            return;
                        }

                        PerformLibraryItemGetLatestVersion(libraryItem);
                        CheckOutStatus checkOutStatus = GetLibraryItemSharedLibraryStatus(libraryItem);

                        SetTreeNodeImage(treenode, checkOutStatus);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                        sharedLibraryEventsDisabled = false; // Re-enable

                        if (libraryItem != null)
                        {
                            UpdateSelectedLibraryItemNode(libraryItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private void mnuViewReport_Click(object sender, EventArgs e)
        {
            LibraryItem libraryItem;
            TreeNode treenode = tvwExplorer.SelectedNode;

            try
            {
                if (treenode != null)
                {
                    // Handle if the node is a library item (should always be as they shouldn't see the menu item otherwise)
                    if (treenode.Tag is LibraryItem)
                    {
                        libraryItem = (LibraryItem) treenode.Tag;

                        // Fire event that will tell main designer to open up the report
                        OnOpenDesigner(libraryItem, PrefferedDesigner.ReportViewer);
                        SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
            }
        }

        private Boolean PerformLibraryItemCheckIn(LibraryItem libraryItem, TreeNode treenode)
        {
            Boolean success = true;
                
            CheckInComments CheckInCommentsForm;
            CheckInReportViewer CheckInReportViewerForm;
            List<LibraryItem> handledLibraryItems = new List<LibraryItem>();

            // Exit if this item is not currently linked to the shared library
            if (libraryItem.Linked == false)
                return false;

            // Exit if this item is already as checked in
            if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedIn)
                return false;

            try
            {
                // Show the pre CheckIn report form
                using (CheckInReportViewerForm = new CheckInReportViewer(libraryItem))
                {
                    if (reportingEnabled)
                    {
                        // Navigate to the report for the specific library item
                        CheckInReportViewerForm.Navigate();

                        // Show the pre check in pre check in report to the user
                        CheckInReportViewerForm.ShowDialog();
                    }
                    else
                    {
                        CheckInReportViewerForm.DialogResult = DialogResult.Ignore;
                    }

                    libraryItem.ModifiedDateTime = DateTime.Now;

                    // Continue to show the check in form if the user has clicked on continue
                    if (CheckInReportViewerForm.DialogResult == DialogResult.OK ||
                        CheckInReportViewerForm.DialogResult == DialogResult.Ignore)
                    {
                        // Show the CheckInComment form to get the check in comments
                        using (CheckInCommentsForm = new CheckInComments(libraryItem))
                        {
                            // Get the comments
                            CheckInCommentsForm.ShowDialog();

                            if (CheckInCommentsForm.PerformCheckIn)
                            {
                                libraryItem.CheckInComment = CheckInCommentsForm.Comments;

                                // Set the library item's dependent items and check in (also adding any new dependent items)
                                libraryItem.DependentObject = false; // Root item is not dependent
                                libraryItem.HandleDependencies(handledLibraryItems);

                                success = sharedLibraryController.CheckIn(libraryItem);
                                if (success)
                                {
                                    UpdateSelectedLibraryItemNode(libraryItem);

                                    // Mark the item as checked in within the explorer
                                    libraryItem.CheckedOutStatus = CheckOutStatus.CheckedIn;
                                    libraryItem.CheckedOutBy = string.Empty;
                                    libraryItem.CheckedOutByLogInName = string.Empty;

                                    ResetTreeMaintainSelection(libraryItem);
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                bool rethrow = ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                DisplayErrorToUI(ex);
                success = false;
            }

            finally {
                if (!success)
                {
                    // Remove any newly added items from the library & ensure the UI reflects the original items values
                    RollBackCheckIn(libraryItem);
                    SynchroniseCheckOutStatus(libraryItem);
                    DeleteNewlyAddedSharedLibraryItems(libraryItem);
                    UpdateSelectedLibraryItemNode(libraryItem);
                    ResetTreeMaintainSelection(libraryItem);
                }
            }

            // Item was checked in
            return true;
        }

        private void RollBackCheckIn(LibraryItem libraryItem)
        {
            List<LibraryItem> updatedLibraryItems = new List<LibraryItem>();
            sharedLibraryController.RollBackCheckIn(libraryItem);
        }

        private void SynchroniseCheckOutStatus(LibraryItem libraryItem)
        {
            string user = string.Empty;

            if (libraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    SynchroniseCheckOutStatus(dependentLibraryItem);
                }
            }

            sharedLibraryController.GetCheckOutStatus(libraryItem);

            // Ensure the item is in sync with the library
            if (libraryItem.CheckedOutBy == string.Empty)
            {
                // Mark the item as checked in within the explorer
                libraryItem.CheckedOutStatus = CheckOutStatus.CheckedIn;
                libraryItem.CheckedOutBy = string.Empty;
                libraryItem.CheckedOutByLogInName = string.Empty;
            }
        }

        /// <summary>
        ///     Gets a collection of library items including the given library item and it's dependents. The collection
        ///     items are then cloned to ensure they are not referencing the items which will be updated.
        ///     The cloned library item collection is then returned.
        /// </summary>
        /// <param name="libraryItem">The library item to be handled.</param>
        /// <param name="handledLibraryItems">
        ///     Library Item list of items that are already handled. Ensures cyclic processing occurring
        /// </param>
        /// <returns>A List<LibraryItem> containing clones of the given LibrayrItem and it's dependents.</returns>
        private List<LibraryItem> PrepareDependents(LibraryItem libraryItem, List<LibraryItem> handledLibraryItems)
        {
            List<LibraryItem> libraryItems = new List<LibraryItem>();
            libraryItem.HandleDependencies(handledLibraryItems);

            if (libraryItem.DependentLibraryItems != null)
            {
                foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                {
                    PrepareDependents(dependentLibraryItem, handledLibraryItems);
                }
            }

            libraryItems.Add((LibraryItem)libraryItem.Clone());

            return libraryItems;
        }

        /// <summary>
        ///     Deletes the library items contained within the collection, only if the given item is marked as 
        ///     newly added to the library.
        /// </summary>
        /// <remarks>
        ///     This is usually called after an error when new items have been added to the shared library.
        /// </remarks>
        /// <param name="libraryItem">
        ///     The LibraryItem which has been updated in the repository.
        /// </param>
        private void DeleteNewlyAddedSharedLibraryItems(LibraryItem libraryItem)
        {
            try
            {
                if (libraryItem.DependentLibraryItems != null)
                {
                    foreach (LibraryItem dependentLibraryItem in libraryItem.DependentLibraryItems)
                    {
                        DeleteNewlyAddedSharedLibraryItems(dependentLibraryItem);
                    }
                }

                if (libraryItem.IsNewLibraryItem)
                {
                    sharedLibraryController.Delete(libraryItem);
                    libraryItem.LibraryUniqueIdentifier = Guid.Empty;
                    libraryItem.Linked = false;
                    libraryItem.RelativePath = String.Empty;
                }
            }
            catch (Exception ex)
            {
                // Only log the exception, don't rethrow - as this is called by functions adding new library items and is handled there
                ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                MessageBox.Show(message, GetResource("SharedLibrary.SLTypePane.SLDeletionError"), MessageBoxButtons.OK, 
                                MessageBoxIcon.Error);
            }
        }

        private CheckOutStatus PerformLibraryItemCheckOut(LibraryItem libraryItem)
        {
            DialogResult result;
            LibraryItem libraryItemCheckedOut;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Exit if this item is not currently linked to the shared library
                if (libraryItem.Linked == false)
                {
                    return CheckOutStatus.None;
                }

                // get the latest checked out status
                sharedLibraryController.GetCheckOutStatus(libraryItem);
                
                // Exit if this item is already as checked out (to this current user)
                if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedOut &&
                    libraryItem.CheckedOutByLogInName == WindowsIdentity.GetCurrent().Name)
                {
                    return CheckOutStatus.None;
                }
                                
                if (libraryItem.CheckedOutBy == null)
                {
                    // Check out the item and get the latest version
                    libraryItemCheckedOut = sharedLibraryController.CheckOut(libraryItem);

                    // Drill down the dependents ensuring they are all present and updated
                    package.DrillDownDependentItems(libraryItem, sharedLibraryProxy, libraryItem.DependentObject);

                    // Update the item to the latest version
                    Package.HandleLibraryItemUpdate(libraryItem, libraryItemCheckedOut);

                    // Update the icons image
                    tvwExplorer.SelectedNode.SelectedImageIndex = DiscoverLibraryItemTreeNodeIconToSet(libraryItem);
                    tvwExplorer.SelectedNode.ImageIndex = DiscoverLibraryItemTreeNodeIconToSet(libraryItem);

                    return CheckOutStatus.CheckedOut; // Completed with linking in place
                }
                else
                {
                    // Inform the user that the item is already checked out by another user

                    if (libraryItem.CheckedOutByLogInName != null &&
                        libraryItem.CheckedOutByLogInName.ToLower() != WindowsIdentity.GetCurrent().Name.ToLower())
                    {
                        string message = string.Format(GetResource("SharedLibrary.SLTypePane.SLCheckOutError"), libraryItem.CheckedOutBy);
                        string title = GetResource("SharedLibrary");
                        result = MessageBox.Show(message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        // Ask them if they wish to create a new stand alone copy & unlink
                        if (result != DialogResult.Yes)
                        {
                            return CheckOutStatus.None; // Completed with linking in place
                        }
                        else
                        {
                            return CheckOutStatus.Unlinked; // Completed and need to unlink
                        }
                    }
                    else
                    {
                        return CheckOutStatus.None;
                    }
                }
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            SetPropertyGridSelectedObject(libraryItem, GetCurrentWindowsUsername());
        }

        private void PerformLibraryItemUndoCheckOut(LibraryItem libraryItem, ref CheckOutStatus checkOutStatus)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Exit if this item is not currently linked to the shared library
                if (libraryItem.Linked == false)
                    return;

                // Exit if this item is already as checked in
                if (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedIn)
                    return;

                DialogResult result = MessageBox.Show(GetResource("CheckedInWarnChanges"), GetResource("SharedLibrary"),
                                      MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if ( result != DialogResult.Yes )
                    return;

                // Undo the checkout & update the item back to the latest version in the repository
                sharedLibraryController.UndoCheckout(libraryItem);
                PerformLibraryItemGetLatestVersion(libraryItem);
                checkOutStatus = CheckOutStatus.CheckedIn;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private Boolean PerformLibraryItemGetLatestVersion(LibraryItem libraryItem)
        {
            LibraryItem latestVersionLibraryItem;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Exit if this item is not currently linked to the shared library
                if (libraryItem.Linked == false)
                    return false;

                // Update the current library item to reflect the latest version
                latestVersionLibraryItem = sharedLibraryController.GetLatestVersion(libraryItem);

                LibraryItem packageItem = Package.GetLibraryItemByName(latestVersionLibraryItem);
                if (packageItem != null &&
                    latestVersionLibraryItem.LibraryUniqueIdentifier != packageItem.LibraryUniqueIdentifier)
                {
                    string errorMessage = GetResource("PackageExplorer.SharedLibrary.NoneSLItemAlreadyExist");
                    MessageBox.Show(String.Format(errorMessage, libraryItem.Name, packageItem.Name), "Shared Library", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    return false;
                }


                // Drill down the dependents ensuring they are all present and updated
                package.DrillDownDependentItems(latestVersionLibraryItem, sharedLibraryProxy, latestVersionLibraryItem.DependentObject);

                // Update the item to the latest version
                Package.HandleLibraryItemUpdate(libraryItem, latestVersionLibraryItem);

                return true;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void PerformLibraryItemLink(LibraryItem libraryItem)
        {
            // Exit if this item is currently linked to the shared library
            if (libraryItem.Linked)
                return;

            LibraryItem packageItem = package.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
            packageItem.Linked = false;
            libraryItem.Linked = true;
        }

        private void PerformLibraryItemUnlink(LibraryItem libraryItem)
        {
            // Exit if this item is not currently linked to the shared library
            if (!libraryItem.Linked)
                return;

            LibraryItem packageItem = package.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
            packageItem.Linked = false;
            libraryItem.Linked = false;
        }

        private void SetTreeNodeImage(TreeNode treenode, CheckOutStatus action)
        {
            string checkedOutLoginName = string.Empty;

            switch (action)
            {
                case CheckOutStatus.CheckedIn:
                    SetTreeNodeToCheckedIn(treenode);
                    break;
                case CheckOutStatus.CheckedOut:
                    SetTreeNodeToCheckedOut(treenode);
                    break;
                case CheckOutStatus.CheckedOutOtherUser:
                    SetTreeNodeToCheckedOutOtherUser(treenode);
                    break;
                case CheckOutStatus.Unlinked:
                    SetTreeNodeToUnlinked(treenode);
                    break;
                case CheckOutStatus.Disassociated:
                    SetTreeNodeToNormal(treenode);
                    break;
                case CheckOutStatus.Relinked: // Relink

                    LibraryItem libraryItem = (LibraryItem)treenode.Tag;

                    // Discover the check out status and call the appropriate setting function
                    sharedLibraryController.GetCheckOutStatus(libraryItem);

                    if (libraryItem.CheckedOutBy == null)
                    {
                        SetTreeNodeToCheckedIn(treenode);
                    }
                    else if (libraryItem.CheckedOutByLogInName.ToLower() == WindowsIdentity.GetCurrent().Name.ToLower())
                    {
                        SetTreeNodeToCheckedOut(treenode);
                        break;
                    }
                    else
                    {
                        SetTreeNodeToCheckedOutOtherUser(treenode);
                        break;
                    }
                    break;
            }
        }

        private void SetTreeNodeToCheckedIn(TreeNode treenode)
        {
            if (treenode != null && treenode.Tag is LibraryItem)
            {
                switch (((LibraryItem) treenode.Tag).LibraryType)
                {
                    case LibraryItemType.Function:
                        if (treenode.Tag is DatePFunction)
                        {
                            treenode.ImageIndex = (int) TreeIcons.DateFunctionCheckedIn;
                            treenode.SelectedImageIndex = (int) TreeIcons.DateFunctionCheckedIn;
                        }
                        else
                        {
                            treenode.ImageIndex = (int) TreeIcons.ArithmeticFunctionCheckedIn;
                            treenode.SelectedImageIndex = (int) TreeIcons.ArithmeticFunctionCheckedIn;
                        }
                        break;
                    case LibraryItemType.InterviewPage:
                        if (package.Interviews[0].StartPage == treenode.Tag)
                        {
                            treenode.ImageIndex = (int)TreeIcons.StartPageCheckedIn;
                            treenode.SelectedImageIndex = (int)TreeIcons.StartPageCheckedIn;
                        }
                        else
                        {
                            treenode.ImageIndex = (int)TreeIcons.InternalPageCheckedIn;
                            treenode.SelectedImageIndex = (int)TreeIcons.InternalPageCheckedIn;
                        }
                        break;
                    case LibraryItemType.Outcome:
                        treenode.ImageIndex = (int) TreeIcons.OutcomeCheckedIn;
                        treenode.SelectedImageIndex = (int) TreeIcons.OutcomeCheckedIn;
                        break;
                    case LibraryItemType.Question:
                        treenode.ImageIndex = (int) TreeIcons.QuestionCheckedIn;
                        treenode.SelectedImageIndex = (int) TreeIcons.QuestionCheckedIn;
                        break;
                    case LibraryItemType.SimpleOutcome:
                        treenode.ImageIndex = (int) TreeIcons.SimpleOutcomeCheckedIn;
                        treenode.SelectedImageIndex = (int) TreeIcons.SimpleOutcomeCheckedIn;
                        break;
                    case LibraryItemType.Template:
                        treenode.ImageIndex = (int) TreeIcons.TemplateCheckedIn;
                        treenode.SelectedImageIndex = (int) TreeIcons.TemplateCheckedIn;
                        break;
                }
            }
        }

        private void SetTreeNodeToCheckedOut(TreeNode treenode)
        {
            if (treenode.Tag is LibraryItem)
            {
                switch (((LibraryItem)treenode.Tag).LibraryType)
            {
                case LibraryItemType.Function:
                    if (treenode.Tag is DatePFunction)
                    {
                            treenode.ImageIndex = (int)TreeIcons.DateFunctionCheckedOut;
                            treenode.SelectedImageIndex = (int)TreeIcons.DateFunctionCheckedOut;
                    }
                    else
                    {
                            treenode.ImageIndex = (int)TreeIcons.ArithmeticFunctionCheckedOut;
                            treenode.SelectedImageIndex = (int)TreeIcons.ArithmeticFunctionCheckedOut;
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    if (package.Interviews[0].StartPage == treenode.Tag)
                    {
                        treenode.ImageIndex = (int)TreeIcons.StartPageCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.StartPageCheckedOut;
                    }
                    else
                    {
                        treenode.ImageIndex = (int)TreeIcons.InternalPageCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.InternalPageCheckedOut;
                    }
                    break;
                case LibraryItemType.Outcome:
                        treenode.ImageIndex = (int)TreeIcons.OutcomeCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.OutcomeCheckedOut;
                    break;
                case LibraryItemType.Question:
                        treenode.ImageIndex = (int)TreeIcons.QuestionCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.QuestionCheckedOut;
                    break;
                case LibraryItemType.SimpleOutcome:
                        treenode.ImageIndex = (int)TreeIcons.SimpleOutcomeCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.SimpleOutcomeCheckedOut;
                    break;
                case LibraryItemType.Template:
                        treenode.ImageIndex = (int)TreeIcons.TemplateCheckedOut;
                        treenode.SelectedImageIndex = (int)TreeIcons.TemplateCheckedOut;
                    break;
            }
        }
        }

        private void SetTreeNodeToCheckedOutOtherUser(TreeNode treenode)
        {
            switch (((LibraryItem) treenode.Tag).LibraryType)
            {
                case LibraryItemType.Function:
                    if (treenode.Tag is DatePFunction)
                    {
                        treenode.ImageIndex = (int) TreeIcons.DateFunctionCheckedOutOtherUser;
                        treenode.SelectedImageIndex = (int) TreeIcons.DateFunctionCheckedOutOtherUser;
                    }
                    else
                    {
                        treenode.ImageIndex = (int) TreeIcons.ArithmeticFunctionCheckedOutOtherUser;
                        treenode.SelectedImageIndex = (int) TreeIcons.ArithmeticFunctionCheckedOutOtherUser;
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    if (package.Interviews[0].StartPage == treenode.Tag)
                    {
                        treenode.ImageIndex = (int)TreeIcons.StartPageCheckedOutOtherUser;
                        treenode.SelectedImageIndex = (int)TreeIcons.StartPageCheckedOutOtherUser;
                    }
                    else
                    {
                        treenode.ImageIndex = (int)TreeIcons.InternalPageCheckedOutOtherUser;
                        treenode.SelectedImageIndex = (int)TreeIcons.InternalPageCheckedOutOtherUser;
                    }
                    break;
                case LibraryItemType.Outcome:
                    treenode.ImageIndex = (int) TreeIcons.OutcomeCheckedOutOtherUser;
                    treenode.SelectedImageIndex = (int) TreeIcons.OutcomeCheckedOutOtherUser;
                    break;
                case LibraryItemType.Question:
                    treenode.ImageIndex = (int) TreeIcons.QuestionCheckedOutOtherUser;
                    treenode.SelectedImageIndex = (int) TreeIcons.QuestionCheckedOutOtherUser;
                    break;
                case LibraryItemType.SimpleOutcome:
                    treenode.ImageIndex = (int) TreeIcons.SimpleOutcomeCheckedOutOtherUser;
                    treenode.SelectedImageIndex = (int) TreeIcons.SimpleOutcomeCheckedOutOtherUser;
                    break;
                case LibraryItemType.Template:
                    treenode.ImageIndex = (int) TreeIcons.TemplateCheckedOutOtherUser;
                    treenode.SelectedImageIndex = (int) TreeIcons.TemplateCheckedOutOtherUser;
                    break;
            }
        }

        private void SetTreeNodeToUnlinked(TreeNode treenode)
        {
            switch (((LibraryItem) treenode.Tag).LibraryType)
            {
                case LibraryItemType.Function:
                    if (treenode.Tag is DatePFunction)
                    {
                        treenode.ImageIndex = (int) TreeIcons.DateFunctionUnlinked;
                        treenode.SelectedImageIndex = (int) TreeIcons.DateFunctionUnlinked;
                    }
                    else
                    {
                        treenode.ImageIndex = (int) TreeIcons.ArithmeticFunctionUnlinked;
                        treenode.SelectedImageIndex = (int) TreeIcons.ArithmeticFunctionUnlinked;
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    if (package.Interviews[0].StartPage == treenode.Tag)
                    {
                        treenode.ImageIndex = (int)TreeIcons.StartPageUnlinked;
                        treenode.SelectedImageIndex = (int)TreeIcons.StartPageUnlinked;
                    }
                    else
                    {
                        treenode.ImageIndex = (int)TreeIcons.InternalPageUnlinked;
                        treenode.SelectedImageIndex = (int)TreeIcons.InternalPageUnlinked;
                    }
                    break;
                case LibraryItemType.Outcome:
                    treenode.ImageIndex = (int) TreeIcons.OutcomeUnlinked;
                    treenode.SelectedImageIndex = (int) TreeIcons.OutcomeUnlinked;
                    break;
                case LibraryItemType.Question:
                    treenode.ImageIndex = (int) TreeIcons.QuestionUnlinked;
                    treenode.SelectedImageIndex = (int) TreeIcons.QuestionUnlinked;
                    break;
                case LibraryItemType.SimpleOutcome:
                    treenode.ImageIndex = (int) TreeIcons.SimpleOutcomeUnlinked;
                    treenode.SelectedImageIndex = (int) TreeIcons.SimpleOutcomeUnlinked;
                    break;
                case LibraryItemType.Template:
                    treenode.ImageIndex = (int) TreeIcons.TemplateUnlinked;
                    treenode.SelectedImageIndex = (int) TreeIcons.TemplateUnlinked;
                    break;
            }
        }

        private void SetTreeNodeToNormal(TreeNode treenode)
        {
            switch (((LibraryItem) treenode.Tag).LibraryType)
            {
                case LibraryItemType.Function:
                    if (treenode.Tag is DatePFunction)
                    {
                        treenode.ImageIndex = (int) TreeIcons.DateFunction;
                        treenode.SelectedImageIndex = (int) TreeIcons.DateFunction;
                    }
                    else
                    {
                        treenode.ImageIndex = (int) TreeIcons.ArithmeticFunction;
                        treenode.SelectedImageIndex = (int) TreeIcons.ArithmeticFunction;
                    }
                    break;
                case LibraryItemType.InterviewPage:
                    if (package.Interviews[0].StartPage == treenode.Tag)
                    {
                        treenode.ImageIndex = (int)TreeIcons.StartPage;
                        treenode.SelectedImageIndex = (int)TreeIcons.StartPage;
                    }
                    else
                    {
                        treenode.ImageIndex = (int)TreeIcons.Page;
                        treenode.SelectedImageIndex = (int)TreeIcons.Page;
                    }
                    break;
                case LibraryItemType.Outcome:
                    treenode.ImageIndex = (int) TreeIcons.Outcome;
                    treenode.SelectedImageIndex = (int) TreeIcons.Outcome;
                    break;
                case LibraryItemType.Question:
                    treenode.ImageIndex = (int) TreeIcons.Question;
                    treenode.SelectedImageIndex = (int) TreeIcons.Question;
                    break;
                case LibraryItemType.SimpleOutcome:
                    treenode.ImageIndex = (int) TreeIcons.SimpleOutcome;
                    treenode.SelectedImageIndex = (int) TreeIcons.SimpleOutcome;
                    break;
                case LibraryItemType.Template:
                    treenode.ImageIndex = (int) TreeIcons.Template;
                    treenode.SelectedImageIndex = (int) TreeIcons.Template;
                    break;
            }
        }

        /// <summary>
        ///		Asks the user if they want to create a new instance of the dragged shared library item 
        ///		(having being found to be a duplicate), or not.
        /// </summary>
        /// <param name="libraryItem">The item being added.</param>
        /// <param name="existingLibraryItem">A reference to the item in the package if it already exists.</param>
        /// <returns>The action to take should the item be matched to an existing item in the package.</returns>
        private MatchedAction HandleSLItemAlreadyInPackage(LibraryItem libraryItem, LibraryItem existingLibraryItem)
        {
            DialogResult result;
            
            // Are there any unlinked dependencys - Don't allow to continue
            if (ItemContainsUnlinkedDependentItems(existingLibraryItem))
            {
                result = DialogResult.Cancel;
            }
            else
            {
                string title = GetResource("SharedLibrary");
                string message = GetResource("SLItemAlreadyExists");
                message = message.Replace("\\n", "\n");
                result = MessageBox.Show(message, title, MessageBoxButtons.OKCancel);
            }

            switch (result)
            { 
                case DialogResult.OK:
                    return MatchedAction.Replace;
                case DialogResult.Cancel:
                    return MatchedAction.Cancel;
                default:
                    throw new InvalidOperationException(GetResource("InvalidPackageMatchAction"));
            }
        }

        private bool ItemContainsUnlinkedDependentItems(LibraryItem libraryItem)
        {
            List<LibraryItem> unlinkedDependencies = null;
            if (libraryItem != null)
            {
                unlinkedDependencies = libraryItem.GetUnlinkedDependencies();
            }

            if (unlinkedDependencies != null && unlinkedDependencies.Count > 0)
            {
                string unlinkedMessage = "";
                foreach (LibraryItem unlinkedlibraryItem in unlinkedDependencies)
                {
                    unlinkedMessage += unlinkedlibraryItem.Name + "\n";
                }

                MessageBox.Show(String.Format(GetResource("CannotUpdateExistingUnlinkedDependent"), "\n" + unlinkedMessage),
                                    GetResource("UnlinkedDependentItemsTitle"), MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);

                return true;
            }

            return false;
        }

        /// <summary>
        ///		Gets the action to perform on new package items from the shared library.
        /// </summary>
        /// <param name="libraryItem">The library item that will be tested to see if it already exists in the package.</param>
        /// <returns>A matched action indicating the action to take when adding any new items from the shared library.</returns>
        private MatchedAction GetMatchedLibraryItemAction(LibraryItem libraryItem)
        {
            LibraryItem existingItem = package.GetLibraryItemByLibraryUniqueIdentifier(libraryItem);
            if (existingItem == null)
                existingItem = package.GetLibraryItemByName(libraryItem);
            else
            {
                LibraryItem existingItemByName = package.GetLibraryItemByName(libraryItem);
                if (existingItemByName != null && existingItem != existingItemByName)
                {
                    string errorMessage = GetResource("PackageExplorer.SharedLibrary.NoneSLItemAlreadyExist");
                    MessageBox.Show(String.Format(errorMessage, existingItem.Name,libraryItem.Name ), "Shared Library", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    return MatchedAction.Cancel;
                }
            }

            if (existingItem != null)
                return HandleSLItemAlreadyInPackage(libraryItem, existingItem);
            else
                return MatchedAction.CreateNew;
        }

        private void SetPropertyGridSelectedObject(LibraryItem libraryItem, string Username)
        {
            SetPropertyGridSelectedObject(libraryItem, Username, null);
        }

        private void SetPropertyGridSelectedObject(LibraryItem libraryItem, string Username, TreeNode node)
        {
            propertyGrid.SelectedObject = libraryItem;

            if (propertyGrid.SelectedObject is LibraryItem)
            {
                if (libraryItem.Linked)
                {
                    bool IsCheckedIn = (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedIn);
                    bool IsCheckedOutToOtherUser = (libraryItem.CheckedOutStatus == CheckOutStatus.CheckedOut && libraryItem.CheckedOutByLogInName != null &&
                                                    libraryItem.CheckedOutByLogInName.ToLower() != Username.ToLower());

                    if ((IsCheckedIn) || (IsCheckedOutToOtherUser))
                    {
                        propertyGrid.Enabled = false;
                    }
                    else
                    {
                        propertyGrid.Enabled = true;
                    }
                }
                else
                {
                    propertyGrid.Enabled = true;
                }
            }
            else
            {
                propertyGrid.Enabled = true;
            }
        }

        private void SetPropertyGridSelectedObject(object tag)
        {
            PropertyGrid.SelectedObject = tag;
            propertyGrid.Enabled = true;
        }

        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        #endregion


    }

    /// <summary>
    /// Simple class to represent a document tree node. It has a ToString( ) function to support
    /// existing code that specifically tests for the string value. (although this has been changed)
    /// </summary>
    public class DocumentFolderTreeNode
    {
        public override string ToString( )
        {
            return "DOCUMENTSFOLDER";
        }
    }


    /// <summary>
    /// Custom sort comparer for the treeview.
    /// To use:
    /// tvwExplorer.TreeViewNodeSorter = new NodeSorter();
    /// </summary>
    public class NodeSorter : IComparer
    {
        // compare between two tree nodes
        public int Compare(object thisObj, object otherObj)
        {
            TreeNode thisNode = thisObj as TreeNode;
            TreeNode otherNode = otherObj as TreeNode;

            // don't sort the top level
            if (thisNode.Level == 1) return 1;

            //alphabetically sorting
            return thisNode.Text.CompareTo(otherNode.Text);
        }
    }
        

}
