using System.Collections;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.UI.PackageExplorer
{
	/// <summary>
	/// A collection of elements of type TreeNode
	/// </summary>
	public class PfTreeNodeCollection : CollectionBase
	{
		/// <summary>
		/// Initializes a new empty instance of the PfTreeNodeCollection class.
		/// </summary>
		public PfTreeNodeCollection()
		{
			// empty
		}

		/// <summary>
		/// Initializes a new instance of the PfTreeNodeCollection class, containing elements
		/// copied from an array.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the new PfTreeNodeCollection.
		/// </param>
		public PfTreeNodeCollection(TreeNode[] items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Initializes a new instance of the PfTreeNodeCollection class, containing elements
		/// copied from another instance of PfTreeNodeCollection
		/// </summary>
		/// <param name="items">
		/// The PfTreeNodeCollection whose elements are to be added to the new PfTreeNodeCollection.
		/// </param>
		public PfTreeNodeCollection(PfTreeNodeCollection items)
		{
			this.AddRange(items);
		}

		/// <summary>
		/// Adds the elements of an array to the end of this PfTreeNodeCollection.
		/// </summary>
		/// <param name="items">
		/// The array whose elements are to be added to the end of this PfTreeNodeCollection.
		/// </param>
		public virtual void AddRange(TreeNode[] items)
		{
			foreach (TreeNode item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds the elements of another PfTreeNodeCollection to the end of this PfTreeNodeCollection.
		/// </summary>
		/// <param name="items">
		/// The PfTreeNodeCollection whose elements are to be added to the end of this PfTreeNodeCollection.
		/// </param>
		public virtual void AddRange(PfTreeNodeCollection items)
		{
			foreach (TreeNode item in items)
			{
				this.List.Add(item);
			}
		}

		/// <summary>
		/// Adds an instance of type TreeNode to the end of this PfTreeNodeCollection.
		/// </summary>
		/// <param name="value">
		/// The TreeNode to be added to the end of this PfTreeNodeCollection.
		/// </param>
		public virtual void Add(TreeNode value)
		{
			this.List.Add(value);
		}

		/// <summary>
		/// Determines whether a specfic TreeNode value is in this PfTreeNodeCollection.
		/// </summary>
		/// <param name="value">
		/// The TreeNode value to locate in this PfTreeNodeCollection.
		/// </param>
		/// <returns>
		/// true if value is found in this PfTreeNodeCollection;
		/// false otherwise.
		/// </returns>
		public virtual bool Contains(TreeNode value)
		{
			return this.List.Contains(value);
		}

		/// <summary>
		/// Return the zero-based index of the first occurrence of a specific value
		/// in this PfTreeNodeCollection
		/// </summary>
		/// <param name="value">
		/// The TreeNode value to locate in the PfTreeNodeCollection.
		/// </param>
		/// <returns>
		/// The zero-based index of the first occurrence of the _ELEMENT value if found;
		/// -1 otherwise.
		/// </returns>
		public virtual int IndexOf(TreeNode value)
		{
			return this.List.IndexOf(value);
		}

		/// <summary>
		/// Inserts an element into the PfTreeNodeCollection at the specified index
		/// </summary>
		/// <param name="index">
		/// The index at which the TreeNode is to be inserted.
		/// </param>
		/// <param name="value">
		/// The TreeNode to insert.
		/// </param>
		public virtual void Insert(int index, TreeNode value)
		{
			this.List.Insert(index, value);
		}

		/// <summary>
		/// Gets or sets the TreeNode at the given index in this PfTreeNodeCollection.
		/// </summary>
		public virtual TreeNode this[int index]
		{
			get { return (TreeNode) this.List[index]; }
			set { this.List[index] = value; }
		}

		/// <summary>
		/// Removes the first occurrence of a specific TreeNode from this PfTreeNodeCollection.
		/// </summary>
		/// <param name="value">
		/// The TreeNode value to remove from this PfTreeNodeCollection.
		/// </param>
		public virtual void Remove(TreeNode value)
		{
			this.List.Remove(value);
		}

		/// <summary>
		/// Type-specific enumeration class, used by PfTreeNodeCollection.GetEnumerator.
		/// </summary>
		public class Enumerator : IEnumerator
		{
			private IEnumerator wrapped;

			public Enumerator(PfTreeNodeCollection collection)
			{
				this.wrapped = ((CollectionBase) collection).GetEnumerator();
			}

			public TreeNode Current
			{
				get { return (TreeNode) (this.wrapped.Current); }
			}

			object IEnumerator.Current
			{
				get { return (TreeNode) (this.wrapped.Current); }
			}

			public bool MoveNext()
			{
				return this.wrapped.MoveNext();
			}

			public void Reset()
			{
				this.wrapped.Reset();
			}
		}

		/// <summary>
		/// Returns an enumerator that can iterate through the elements of this PfTreeNodeCollection.
		/// </summary>
		/// <returns>
		/// An object that implements System.Collections.IEnumerator.
		/// </returns>        
		new public virtual Enumerator GetEnumerator()
		{
			return new Enumerator(this);
		}
	}
}