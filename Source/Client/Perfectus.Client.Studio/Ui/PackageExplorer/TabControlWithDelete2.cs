﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace Perfectus.Client.Studio.Ui.PackageExplorer
{
    public partial class TabControlWithDelete2 : UserControl
    {
        public class TabControlWithDelete2EventArgs : EventArgs
        {
            private Object _tag;
            public Object  tag {
                get { return _tag; }
                set { _tag = value; }
            }
            public
            TabControlWithDelete2EventArgs(Object param_tag ) {
                _tag = param_tag;
            }
        }

        public delegate void TabControlWithDelete2EventHandler(object sender, TabControlWithDelete2EventArgs e);
        public event TabControlWithDelete2EventHandler tabTryClose;

        public delegate void TabControlWithDelete2SelectedItemEventHandler(object sender, TabControlWithDelete2EventArgs e);
        public event TabControlWithDelete2SelectedItemEventHandler selectedIndexChanged;

        public TabControl TabControl2;
        public TabControl.TabPageCollection TabPages;
        //public TabPage SelectedTab;
        public bool Visible;
        private ToolStripButton prevTSB;
        private int LeftToolButtonIndex = 0;
        private int DeletedToolButtonIndex = 0;

        public TabControlWithDelete2()
        {
            InitializeComponent();

            toolStrip1.BackColor = Client.Studio.MainDesigner2.PerfectusColour.MediumGray;
            toolStrip2.BackColor = Client.Studio.MainDesigner2.PerfectusColour.MediumGray;
            this.BackColor = Client.Studio.MainDesigner2.PerfectusColour.MediumGray;
            //force the toolstrips to be slightly wider to cover the curved right edge.
            toolStrip2.Width += 4;
            //these look best with autosize true, otherwise the ugly stacker box takes over.
            //make the control really wide to avoid the stacker
            toolStrip1.Width += 2000;
            toolStrip1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 3);
            TabControl2 = new TabControl();
            TabControl2.Visible = false;
            TabPages = TabControl2.TabPages;
        }

        public void SelectTab(TabPage t)
        {
            // find the tab for the page
            foreach (ToolStripButton tsb in toolStrip1.Items)
            {
                System.Diagnostics.Debug.Print(tsb.Text + " " + tsb.Tag.ToString() + " " + t.Name);

                if (tsb.Tag == t)
                {
                    UpdatePreviousTab(tsb);
                    
                    //highlight tab, and show it
                    //tsb.Font = new System.Drawing.Font(this.Font, FontStyle.Bold);
                    //tsb.BackColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumGray;
                    //tsb.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);

                    //bring the tsb into focus
                    int i = 150;
                    while (!tsb.Visible && i-- > 0)
                    {
                        if (LeftToolButtonIndex > 0 && toolStrip1.Items.Count > 0)
                            toolStrip1.Items[--LeftToolButtonIndex].Visible = true;
                    }
                    
                    tsb.ForeColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumBlue;
                    System.Diagnostics.Debug.Print(">>" + tsb.Text + " " + tsb.Tag.ToString() + " " + t.Name + " " + t.ToString());
                    TabControl2.SelectedTab = t;
                    return;
                }
            }
        }

        // Called from MainDesigner2 when an MID form is activated
        // The form is on the TabPage.Tag, find TP and select it to force the tab header to highlight.
        public void SelectTab(Form designer)
        {
            foreach (TabPage tp in TabPages)
            {
                if (tp.Tag == designer)
                    SelectTab(tp);
            }
        }

        private void UpdatePreviousTab(ToolStripButton tsb)
        {
            if (prevTSB != null)
            {
                //if (prevTSB != tsb) prevTSB.Font = new System.Drawing.Font(this.Font, FontStyle.Regular);
                //if (prevTSB != tsb) prevTSB.BackColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.LightGray;
                //prevTSB.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
                prevTSB.ForeColor = System.Drawing.Color.Black;
            }
            prevTSB = tsb;
        }

        public void AddTab(TabPage t)
        {
            // add tool and page
            ToolStripButton tsb = new ToolStripButton()
            {
                BackColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.LightGray,
                ForeColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumBlue,
                //Font = new System.Drawing.Font(this.Font, FontStyle.Bold),
                //Margin = new System.Windows.Forms.Padding(0),
                DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text,
                Name = "tsb" + t.Name,
                AutoSize = true,
                Text = t.Text,
                Tag = t
            };
            toolStrip1.Items.Add(tsb);
            tsb.Click += new EventHandler(toolStripButton_Click);

            UpdatePreviousTab(tsb);

            t.Parent = TabControl2;
            TabControl2.SelectedTab = t;
        }

        private void toolStripButton_Click(object sender, EventArgs e)
        {
            ToolStripButton tsb = sender as ToolStripButton;

            //find page using tool tag
            foreach (TabPage t in TabPages)
            {
                if (tsb.Tag == t)
                {
                    UpdatePreviousTab(tsb);

                    //highlight tab, and show it
                    //tsb.Font = new System.Drawing.Font(this.Font, FontStyle.Bold);
                    //tsb.BackColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumGray; 
                    //tsb.Margin = new System.Windows.Forms.Padding(0);
                    tsb.ForeColor = Perfectus.Client.Studio.MainDesigner2.PerfectusColour.MediumBlue;
                    TabControl2.SelectedTab = t;
                    break;
                }
            }
            //this doesn't matter, its not used
            TabControlWithDelete2EventArgs eventInfo = new TabControlWithDelete2EventArgs(tsb.Tag); 

            // tell maindesigner we need to change form (it uses selectedtab, not the event parm.)
            if (this.selectedIndexChanged != null)
                selectedIndexChanged(this, eventInfo);
        }

        private void toolStripButtonLeft_Click(object sender, EventArgs e)
        {
            if (LeftToolButtonIndex > 0 && toolStrip1.Items.Count > 0)
                toolStrip1.Items[--LeftToolButtonIndex].Visible = true;
        }

        private void toolStripButtonRight_Click(object sender, EventArgs e)
        {
            if (LeftToolButtonIndex < toolStrip1.Items.Count && toolStrip1.Right > toolStrip2.Left)
                toolStrip1.Items[LeftToolButtonIndex++].Visible = false;
        }

        private void toolStripButtonDelete_Click(object sender, EventArgs e)
        {
            if (TabControl2.SelectedTab != null)
            {
                DeletedToolButtonIndex = toolStrip1.Items.IndexOf(prevTSB);

                if (prevTSB != null)
                {
                    // The affected tool tab
                    toolStrip1.Items.Remove(prevTSB);
                    prevTSB = null;
                }

                // The affected page...
                TabControlWithDelete2EventArgs eventInfo = new TabControlWithDelete2EventArgs(TabControl2.SelectedTab.Tag /* Form */);
                
                if (this.tabTryClose != null)
                    tabTryClose(this, eventInfo);
            }

        }

    }
}
