using System;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.UI.PackageExplorer
{
	/// <summary>
	/// Summary description for ExplorerControlMini.
	/// </summary>
	public class ExplorerControlMini : UserControl
	{
		private System.ComponentModel.IContainer components;
		private TreeView tvwExplorer;
		private Package package;
		private bool suspended = false;
		private TreeNode rootNode;		
		private TreeNode questionsNode;			
		private TreeNode functionsNode;				
		private ImageList ilistTreeView;
		private GuidToPfTreeNodeCollectionDictionary guidNodeMap;

		public Package Package
		{
			get {return package;}
			set 
			{ 
				package = value;
				
				if(value != null)
				{
					guidNodeMap = new GuidToPfTreeNodeCollectionDictionary();
					ResetTree();
				}
			}

		}

		public ExplorerControlMini()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
		}
		
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void ResetTree()
		{
			try
			{
				tvwExplorer.SuspendLayout();
				suspended = true;

				DisableControl();
				tvwExplorer.Hide();	
				ResetTree(rootNode);
			}
			finally
			{
				tvwExplorer.ResumeLayout();
				suspended = false;
				EnableControl();
				tvwExplorer.Show();				
			}

			tvwExplorer.Refresh();
			tvwExplorer.CollapseAll();
			tvwExplorer.Nodes[0].Expand();
		}

		private void ResetTree(TreeNode n)
		{
			if (n == rootNode)
			{				
				tvwExplorer.Nodes.Clear();
				guidNodeMap.Clear();

				// Set up the root nodes			
				string localQuestionsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.QuestionsNodeName");
				string localFunctionsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.FunctionsNodeName");
				
				
				rootNode = new TreeNode(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageExplorer.ExplorerControl.NoPackageLoaded"), (int) TreeIcons.FolderPackage, (int) TreeIcons.FolderPackage);
				questionsNode = new TreeNode(localQuestionsNodeName, (int) TreeIcons.FolderQuestions, (int) TreeIcons.FolderQuestions);
				functionsNode = new TreeNode(localFunctionsNodeName, (int) TreeIcons.FolderFunctions, (int) TreeIcons.FolderFunctions);
				rootNode.Nodes.AddRange(new TreeNode[] {questionsNode, functionsNode});

				tvwExplorer.Nodes.Add(rootNode);

				PackageNameBind();
				rootNode.Tag = package;
			

				functionsNode.Tag = package.Functions;
				foreach (PFunction f in package.Functions)
				{
					//temp dont allow anything but datefunctions.
					//if(f is DatePFunction && ((DatePFunction)f).Definition != null && ((DatePFunction)f).Definition.Method == DateFunctionMethod.Now)
					//{
						AddNode(f);
					//}
				}				

				questionsNode.Tag = package.Questions;

				foreach(QuestionFolder qf in package.QuestionsFolders)
				{
					AddNode(qf);						
					FolderCollection subFolders = qf.GetChildFolders();
					foreach(QuestionFolder subQf in subFolders)
					{
						AddNode(subQf);
					}
				}
			
				foreach (Question q in package.Questions)
				{	
					if(q.DataType == PerfectusDataType.Text || q.DataType == PerfectusDataType.Number || q.DataType == PerfectusDataType.Date)
					{
						AddNode(q);
					}									
				}									
			}			
		}

	
		private void AddNode(PackageItem itemToAdd)
		{
			Cursor = Cursors.WaitCursor;
			try
			{
				TreeNode newNode = new TreeNode();

				string nodeName;
				int nodeImageIndex;
				object nodeTag;
				TreeNode parentNode;
				
				if (itemToAdd is Question)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.Question;
					nodeTag = itemToAdd;

					// if we are at root add to questions node, else to folder
					if(itemToAdd.ParentFolder == null)
					{
						parentNode = questionsNode;
					}
					else
					{					
						// Find the parent node (another question folder)
						PfTreeNodeCollection tnc = guidNodeMap[itemToAdd.ParentFolder.UniqueIdentifier];						
						if (tnc != null && tnc.Count > 0)
						{
							parentNode = tnc[0];
						}
						else
						{
                            throw new NullReferenceException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControls.NoParentFolderQuestion"));
						}
					}
				}			
				else if (itemToAdd is DatePFunction)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.DateFunction;
					nodeTag = itemToAdd;
					parentNode = functionsNode;
				}
				else if (itemToAdd is ArithmeticPFunction)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.ArithmeticFunction;
					nodeTag = itemToAdd;
					parentNode = functionsNode;
				}
				else if (itemToAdd is QuestionFolder)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.FolderClosed;
					nodeTag = itemToAdd;

					// if we are at root, add to questions node
					if(itemToAdd.ParentFolder == null)
					{
						parentNode = questionsNode;
					}
					else
					{					
						// Find the parent node (another question folder)
						PfTreeNodeCollection tnc = guidNodeMap[((QuestionFolder) itemToAdd).ParentFolder.UniqueIdentifier];						
						if (tnc != null && tnc.Count > 0)
						{
							parentNode = tnc[0];
						}
						else
						{
                            throw new NullReferenceException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControls.NoParentFolderQuestionFolder"));
						}
					}
				}
				else
				{
					throw new NotImplementedException();
				}

				// Bit of a hack, so that folds sit at top even when sorted alphabetically, need to come up with something nicer than this.
				if(itemToAdd is QuestionFolder || itemToAdd is OutcomeFolder || itemToAdd is SimpleOutcomeFolder )
				{
					newNode.Text = '\u0020' + nodeName;
				}
				else
				{
					newNode.Text = nodeName;
				}

                // Add nodelets for each field if the question uses a picker with fields
                if (itemToAdd is Question)
                {
                    Question q = (Question) itemToAdd;
                    if (q.DisplayType >= QuestionDisplayType.Extension1 && package.ExtensionMapping[q.DisplayType].FieldNames.Length > 0)
                    {
                        foreach (string s in package.ExtensionMapping[q.DisplayType].FieldNames)
                        {
                            TreeNode fieldNode = new TreeNode(s);
                            fieldNode.ImageIndex = (int)TreeIcons.StructField;
                            fieldNode.SelectedImageIndex = fieldNode.ImageIndex;
                            fieldNode.Tag = s;
                            newNode.Nodes.Add(fieldNode);
                        }
                    }
                }

				newNode.Tag = nodeTag;
				newNode.SelectedImageIndex = nodeImageIndex;
				newNode.ImageIndex = nodeImageIndex;
								
				parentNode.Nodes.Add(newNode);

				// See if there's already one, and if so, add to its collection in the nodemap
				PfTreeNodeCollection c = guidNodeMap[itemToAdd.UniqueIdentifier];
				if (c == null)
				{
					c = new PfTreeNodeCollection();
					guidNodeMap[itemToAdd.UniqueIdentifier] = c;
				}
				else
				{
					if (c.Count > 1 && !(itemToAdd.AllowDuplicatesInPackage))
					{
						throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlsMini.DuplicateItem"));
					}
				}

				c.Add(newNode);

				tvwExplorer.SelectedNode = newNode;
			}
			finally
			{
				Cursor = Cursors.Default;

				if (!suspended)
				{
					tvwExplorer.Sorted = false;
					tvwExplorer.Sorted = true;						
					tvwExplorer.Refresh();
				}
			}
		}

		private void DisableControl()
		{
			tvwExplorer.Enabled = false;
		}

		private void EnableControl()
		{
			tvwExplorer.Enabled = true;
		}

		private void PackageNameBind()
		{
			rootNode.Text = package.Name;
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ExplorerControlMini));
			this.tvwExplorer = new System.Windows.Forms.TreeView();
			this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
			this.SuspendLayout();
			// 
			// tvwExplorer
			// 
			this.tvwExplorer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvwExplorer.ImageList = this.ilistTreeView;
			this.tvwExplorer.Location = new System.Drawing.Point(0, 0);
			this.tvwExplorer.Name = "tvwExplorer";
			this.tvwExplorer.Size = new System.Drawing.Size(240, 344);
			this.tvwExplorer.Sorted = true;
			this.tvwExplorer.TabIndex = 0;
			this.tvwExplorer.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwExplorer_ItemDrag);
			// 
			// ilistTreeView
			// 
			this.ilistTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ilistTreeView.ImageSize = new System.Drawing.Size(16, 16);
			this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
			this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// ExplorerControlMini
			// 
			this.Controls.Add(this.tvwExplorer);
			this.Name = "ExplorerControlMini";
			this.Size = new System.Drawing.Size(240, 344);
			this.ResumeLayout(false);

		}
		#endregion

		private void tvwExplorer_ItemDrag(object sender, ItemDragEventArgs e)
		{
			if (e.Item != null && e.Item is TreeNode)
			{
				object t = ((TreeNode) e.Item).Tag;
				if (t != null)
				{
                    if (t is IStringBindingItem)
                    {
                        tvwExplorer.SelectedNode = (TreeNode) e.Item;
                        DataObject obj = new DataObject();

                        obj.SetData("Perfectus.Common.Package.PackageItem", ((TreeNode) e.Item).Tag);
                        ((IStringBindingItem) t).AddHTMLTagToObject(obj, null);
                        //((ITemplateItem) t).AddWordMLTagToObject(obj);

                        DoDragDrop(obj, DragDropEffects.All);
                    }
                    else if (t is string)
                    {
                        string fieldName = t.ToString();
                        Question q = ((TreeNode) e.Item).Parent.Tag as Question;
                        if (q != null)
                        {
                            tvwExplorer.SelectedNode = (TreeNode)e.Item;
                            DataObject obj = new DataObject();

                            obj.SetData("Perfectus.Common.Package.PackageItem", q);
                            StringBinding.AddHTMLTagToDataObject(obj, q, fieldName);
                            DoDragDrop(obj, DragDropEffects.All);
                        }
                    }
				}
			}
		}
	}
}
