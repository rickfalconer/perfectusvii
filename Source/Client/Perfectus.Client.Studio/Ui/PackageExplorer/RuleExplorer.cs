using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.Controls;


namespace Perfectus.Client.Studio.UI.PackageExplorer
{
	/// <summary>
	/// Summary description for RuleExplorer.
	/// </summary>
	public class RuleExplorer : System.Windows.Forms.UserControl
	{
		private System.ComponentModel.IContainer components;		
		
		private Package package;
		private bool suspended = false;
		private System.Windows.Forms.TreeView tvwExplorer;
		private System.Windows.Forms.ImageList ilistTreeView;
		private GuidToPfTreeNodeCollectionDictionary guidNodeMap;		
		private Rule2Collection rules;
				
		public Rule2Collection Rules
		{
			get 
			{
				UpdateRuleCollection();				
				return rules;
			}	
			set 
			{
				rules = value;	
				ResetTree();
			}
		}

		public Package Package
		{
			get 
			{
				return package;
			}	
			set 
			{
				package = value;
				ResetTree();
			}
		}

		private void UpdateRuleCollection()
		{
           rules.Clear();

			foreach(TreeNode serverNode in tvwExplorer.Nodes)
			{
				foreach(TreeNode ruleNode in serverNode.Nodes)
				{
					if(ruleNode.Tag is Perfectus.Common.PackageObjects.Rule2 && ruleNode.Checked == true)
					{
						rules.Add((Perfectus.Common.PackageObjects.Rule2) ruleNode.Tag);
					}
				}
			}
		}

		private void ResetTree()
		{
			if(package != null && rules != null)
			{			
				try
				{
					tvwExplorer.SuspendLayout();
					this.suspended = true;

					DisableControl();
					tvwExplorer.Hide();	

					tvwExplorer.Nodes.Clear();
					guidNodeMap.Clear();
				
					foreach (Server s in package.Servers)
					{
						AddNode(s);
						foreach (Perfectus.Common.PackageObjects.Rule2 r in s.Rule2)
						{
							AddNode(r);							
						}
					}					
				}
				finally
				{
					tvwExplorer.ResumeLayout();
					this.suspended = false;
					EnableControl();
					tvwExplorer.Show();				
				}

				tvwExplorer.Refresh();
				tvwExplorer.ExpandAll();
			}
			else
			{
				DisableControl();
			}
		}

		// ---------------------------------------------------------------------------------------
		// ---------------------------------------------------------------------------------------
		private void AddNode(PackageItem itemToAdd)
		{
			AddNode(itemToAdd, false);
		}

		// ---------------------------------------------------------------------------------------
		// ---------------------------------------------------------------------------------------
		private void AddNode(PackageItem itemToAdd, bool isEditableAfterInsert)
		{
			Cursor = Cursors.WaitCursor;
			try
			{
				TreeNode newNode = new TreeNode();

				string nodeName;
				int nodeImageIndex;
				object nodeTag;
				TreeNode parentNode = null;
				
				if (itemToAdd is Server)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.Server;
					nodeTag = itemToAdd;					
				}
				else if (itemToAdd is Perfectus.Common.PackageObjects.Rule2)
				{
					nodeName = itemToAdd.Name;
					nodeImageIndex = (int) TreeIcons.Rule;
					nodeTag = itemToAdd;

					if(rules.Contains((Perfectus.Common.PackageObjects.Rule2) itemToAdd))
					{
                        newNode.Checked = true;
					}

					// Find the node for the parent Server
					PfTreeNodeCollection tnc = guidNodeMap[((Perfectus.Common.PackageObjects.Rule2) itemToAdd).ParentServer.UniqueIdentifier];
					if (tnc != null && tnc.Count > 0)
					{
						parentNode = tnc[0];
					}
					else
					{
                        throw new NullReferenceException(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControls.NoParentFolderServer"));
					}					
				}
				else
				{
					throw new NotImplementedException();
				}

				newNode.Tag = nodeTag;
				newNode.SelectedImageIndex = nodeImageIndex;
				newNode.ImageIndex = nodeImageIndex;
				newNode.Text = nodeName;

				if(itemToAdd is Server)
				{
					tvwExplorer.Nodes.Add(newNode);
				}
				else if(itemToAdd is Perfectus.Common.PackageObjects.Rule2)
				{
					parentNode.Nodes.Add(newNode);
				}
				
				

				// See if there's already one, and if so, add to its collection in the nodemap
				PfTreeNodeCollection c = guidNodeMap[itemToAdd.UniqueIdentifier];
				if (c == null)
				{
					c = new PfTreeNodeCollection();
					guidNodeMap[itemToAdd.UniqueIdentifier] = c;
				}
				else
				{
					if (c.Count > 1 && !(itemToAdd.AllowDuplicatesInPackage))
					{
                        throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlsMini.DuplicateItem"));
					}
				}

				c.Add(newNode);				
			}
			finally
			{
				Cursor = Cursors.Default;

				if (!suspended)
				{
					tvwExplorer.Sorted = false;
					tvwExplorer.Sorted = true;						
					tvwExplorer.Refresh();
				}
			}
		}

		private void DisableControl()
		{
			tvwExplorer.Enabled = false;
		}

		private void EnableControl()
		{
			tvwExplorer.Enabled = true;
		}

		public RuleExplorer()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();	
			guidNodeMap = new GuidToPfTreeNodeCollectionDictionary();
		}		

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RuleExplorer));
			this.tvwExplorer = new System.Windows.Forms.TreeView();
			this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
			this.SuspendLayout();
			// 
			// tvwExplorer
			// 
			this.tvwExplorer.CheckBoxes = true;
			this.tvwExplorer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvwExplorer.ImageList = this.ilistTreeView;
			this.tvwExplorer.Location = new System.Drawing.Point(0, 0);
			this.tvwExplorer.Name = "tvwExplorer";
			this.tvwExplorer.Size = new System.Drawing.Size(400, 432);
			this.tvwExplorer.TabIndex = 0;
			this.tvwExplorer.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvwExplorer_AfterCheck);
			// 
			// ilistTreeView
			// 
			this.ilistTreeView.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
			this.ilistTreeView.ImageSize = new System.Drawing.Size(16, 16);
			this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
			this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// RuleExplorer
			// 
			this.Controls.Add(this.tvwExplorer);
			this.Name = "RuleExplorer";
			this.Size = new System.Drawing.Size(400, 432);
			this.ResumeLayout(false);

		}
		#endregion

		private void tvwExplorer_AfterCheck(object sender, System.Windows.Forms.TreeViewEventArgs e)
		{
			if(e.Node.Tag is Server && e.Action != TreeViewAction.Unknown) // Unknown is the action when unchecked by code.
			{
				if(e.Node.Checked)
				{
					SelectAllChildren(e.Node);
				}
				else
				{
					UnselectAllChildren(e.Node);
				}
			}
			else if(e.Node.Tag is Perfectus.Common.PackageObjects.Rule)
			{			
				if(e.Node.Checked == false && e.Node.Parent.Checked)
				{
					e.Node.Parent.Checked = false;
				}
			}
		}

		private void SelectAllChildren(TreeNode node)
		{
			foreach(TreeNode childNode in node.Nodes)
			{
				childNode.Checked = true;
			}
		}

		private void UnselectAllChildren(TreeNode node)
		{
			foreach(TreeNode childNode in node.Nodes)
			{
				childNode.Checked = false;
			}
		}
	}
}
