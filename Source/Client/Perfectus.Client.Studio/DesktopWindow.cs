using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Perfectus.Client.Studio
{
    public class DesktopWindow : IWin32Window
    {
        // *** Instance to return
        private static DesktopWindow m_DesktopWindow = new DesktopWindow();

        // *** Don't allow instantiation
        private DesktopWindow() { }

        public static IWin32Window Instance
        {
            get { return m_DesktopWindow; }
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// This method returns the actual window handle
        /// This is one convoluted way to do a simple thing
        /// </summary>
        IntPtr IWin32Window.Handle
        {
            get
            {
                return GetForegroundWindow();
            }
        }
    }
}