using System;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Perfectus.Common;

namespace Perfectus.Client.Studio
{
	/// <summary>
	/// Summary description for EntryPoint.
	/// </summary>
	public sealed class EntryPoint
	{
		private EntryPoint()
		{
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			string uiCultureOverride = System.Configuration.ConfigurationSettings.AppSettings["UiCultureOverride"];		
			bool cultureOverridden = false;
			if (uiCultureOverride != null)
			{
				try
				{
					Thread.CurrentThread.CurrentUICulture = new CultureInfo(uiCultureOverride);
					Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
					cultureOverridden = true;

				}
				catch{}
			}

			if (!cultureOverridden)
			{
				Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
			}

//Switched to the manifest way of enabling themes, as this is *really* bung.  SEHExceptions all over.
//			if (OSFeature.Feature.IsPresent((OSFeature.Themes)))
//			{
//				Application.EnableVisualStyles();
//				// This is required to prevent exceptions on form close caused by the 'enablevisualstyles' call above.
//				// See: http://blogs.msdn.com/rprabhu/archive/2003/09/28/56540.aspx
//				Application.DoEvents();
//			}
//
			//TODO: How to handle unhandled exceptions that bubble this far?

            MainDesigner2 frm2 = null;

            try
            {
                string parameterLine = string.Empty;

                if (args.Length > 0)
                {
                    parameterLine = args[0];
                    for (int i = 1; i < args.Length; i++)
                        parameterLine = String.Format("{0} {1}", parameterLine, args[i]);
                }

                try
                {
                    if (args.Length > 0)
                    {
                        frm2 = new MainDesigner2(parameterLine);
                    }
                    else
                    {
                        frm2 = new MainDesigner2();
                    }

                    Application.Run(frm2);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, Common.About.FormsTitle);

                    // Log the exception
                    ExceptionPolicy.HandleException(ex, Globals.gcEXCEPTION_LEVEL2);
                    throw (ex);
                }
            }
            finally
            {
                if (frm2 != null)
                {
                    frm2.OdmaCloseCurrent();
                }
            }
		}
	}
}