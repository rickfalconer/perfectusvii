using System;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Windows.Forms;
using Perfectus.Client.Studio.Versioning;

namespace Perfectus.Client.Studio
{
    /// <summary>
    /// Summary description for About.
    /// </summary>
    public class About : Form
    {
        private GroupBox groupBox1;
        private DataGrid dataGrid1;
        private Button btnClose;
        private DataGridTableStyle dataGridTableStyle1;
        private DataGridTextBoxColumn dataGridTextBoxColumn1;
        private DataGridTextBoxColumn dataGridTextBoxColumn2;
        private DataGridTextBoxColumn dataGridTextBoxColumn3;
        private DataGridTextBoxColumn dataGridTextBoxColumn4;
        private PictureBox pictureBox1;

        private String _filepath;
        private DataTable dm;
        private DataTable dt;

        private DataGridTableStyle dataGridTableStyle2;
        private DataGridTextBoxColumn Key;
        private DataGridTextBoxColumn Value;
        private RadioButton Package;
        private RadioButton Modules;
        private Label lblExeVersion;
        private Label labelCopyright;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        //TODO: Help|About splash image
        public About()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            Text = string.Format(CultureInfo.CurrentUICulture, Text, Common.About.FormsTitle);
            //RDF remove sUI
            //uiLabelCopyright.Text = Common.About.Copyright;
            labelCopyright.Text = Common.About.Copyright;

            dt = Common.About.LoadedAssemblyTable;
            dataGrid1.DataSource = dt;

            this.lblExeVersion.Text = "IPManager: " + Common.About.AssemblyVersion;
        }

        public About(String packageVersionXML)
            : this()
        {
            if (packageVersionXML != null && packageVersionXML.Length > 0 )
            {
                dm = PackageVersionInfo.LoadPackageInformationTable(packageVersionXML);
                this.Package.Checked = true;
            }
            else
            {
                this.Package.Checked = false;
                this.Package.Enabled = false;
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblExeVersion = new System.Windows.Forms.Label();
            this.Package = new System.Windows.Forms.RadioButton();
            this.Modules = new System.Windows.Forms.RadioButton();
            this.dataGrid1 = new System.Windows.Forms.DataGrid();
            this.dataGridTableStyle1 = new System.Windows.Forms.DataGridTableStyle();
            this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTextBoxColumn4 = new System.Windows.Forms.DataGridTextBoxColumn();
            this.dataGridTableStyle2 = new System.Windows.Forms.DataGridTableStyle();
            this.Key = new System.Windows.Forms.DataGridTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridTextBoxColumn();
            this.btnClose = new System.Windows.Forms.Button();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Controls.Add(this.lblExeVersion);
            this.groupBox1.Controls.Add(this.Package);
            this.groupBox1.Controls.Add(this.Modules);
            this.groupBox1.Controls.Add(this.dataGrid1);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // lblExeVersion
            // 
            resources.ApplyResources(this.lblExeVersion, "lblExeVersion");
            this.lblExeVersion.Name = "lblExeVersion";
            // 
            // Package
            // 
            resources.ApplyResources(this.Package, "Package");
            this.Package.Name = "Package";
            this.Package.UseVisualStyleBackColor = true;
            this.Package.CheckedChanged += new System.EventHandler(this.button1_Click);
            // 
            // Modules
            // 
            resources.ApplyResources(this.Modules, "Modules");
            this.Modules.Checked = true;
            this.Modules.Name = "Modules";
            this.Modules.TabStop = true;
            this.Modules.UseVisualStyleBackColor = true;
            this.Modules.CheckedChanged += new System.EventHandler(this.button1_Click);
            // 
            // dataGrid1
            // 
            this.dataGrid1.AllowNavigation = false;
            resources.ApplyResources(this.dataGrid1, "dataGrid1");
            this.dataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataGrid1.CaptionVisible = false;
            this.dataGrid1.DataMember = "";
            this.dataGrid1.FlatMode = true;
            this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid1.Name = "dataGrid1";
            this.dataGrid1.ParentRowsVisible = false;
            this.dataGrid1.ReadOnly = true;
            this.dataGrid1.RowHeadersVisible = false;
            this.dataGrid1.TableStyles.AddRange(new System.Windows.Forms.DataGridTableStyle[] {
            this.dataGridTableStyle1,
            this.dataGridTableStyle2});
            // 
            // dataGridTableStyle1
            // 
            this.dataGridTableStyle1.DataGrid = this.dataGrid1;
            this.dataGridTableStyle1.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.dataGridTextBoxColumn1,
            this.dataGridTextBoxColumn2,
            this.dataGridTextBoxColumn3,
            this.dataGridTextBoxColumn4});
            this.dataGridTableStyle1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridTableStyle1.MappingName = "Assemblies";
            this.dataGridTableStyle1.ReadOnly = true;
            this.dataGridTableStyle1.RowHeadersVisible = false;
            // 
            // dataGridTextBoxColumn1
            // 
            this.dataGridTextBoxColumn1.Format = "";
            this.dataGridTextBoxColumn1.FormatInfo = null;
            resources.ApplyResources(this.dataGridTextBoxColumn1, "dataGridTextBoxColumn1");
            // 
            // dataGridTextBoxColumn2
            // 
            this.dataGridTextBoxColumn2.Format = "";
            this.dataGridTextBoxColumn2.FormatInfo = null;
            resources.ApplyResources(this.dataGridTextBoxColumn2, "dataGridTextBoxColumn2");
            // 
            // dataGridTextBoxColumn3
            // 
            this.dataGridTextBoxColumn3.Format = "dd-MMM-yyyy HH:mm:ss";
            this.dataGridTextBoxColumn3.FormatInfo = null;
            resources.ApplyResources(this.dataGridTextBoxColumn3, "dataGridTextBoxColumn3");
            // 
            // dataGridTextBoxColumn4
            // 
            this.dataGridTextBoxColumn4.Format = "";
            this.dataGridTextBoxColumn4.FormatInfo = null;
            resources.ApplyResources(this.dataGridTextBoxColumn4, "dataGridTextBoxColumn4");
            // 
            // dataGridTableStyle2
            // 
            this.dataGridTableStyle2.DataGrid = this.dataGrid1;
            this.dataGridTableStyle2.GridColumnStyles.AddRange(new System.Windows.Forms.DataGridColumnStyle[] {
            this.Key,
            this.Value});
            this.dataGridTableStyle2.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGridTableStyle2.MappingName = "PackageInformation";
            this.dataGridTableStyle2.ReadOnly = true;
            this.dataGridTableStyle2.RowHeadersVisible = false;
            // 
            // Key
            // 
            this.Key.Format = "";
            this.Key.FormatInfo = null;
            resources.ApplyResources(this.Key, "Key");
            // 
            // Value
            // 
            this.Value.Format = "";
            this.Value.FormatInfo = null;
            resources.ApplyResources(this.Value, "Value");
            // 
            // btnClose
            // 
            resources.ApplyResources(this.btnClose, "btnClose");
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Name = "btnClose";
            // 
            // labelCopyright
            // 
            resources.ApplyResources(this.labelCopyright, "labelCopyright");
            this.labelCopyright.Name = "labelCopyright";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = global::Perfectus.Client.Studio.Properties.Resources.Splash;
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // About
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "About";
            this.ShowInTaskbar = false;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void button1_Click(object sender, System.EventArgs e)
        {
            dataGrid1.DataSource = Modules.Checked ? dt : dm;

            if (Package.Checked)
            {
                Key.Width = System.Convert.ToInt32(dataGrid1.Width * .3);
                Value.Width = System.Convert.ToInt32(dataGrid1.Width * .7);
            }
        }
    }
}