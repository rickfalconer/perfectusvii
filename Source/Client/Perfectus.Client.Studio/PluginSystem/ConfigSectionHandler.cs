using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Xml;
using Perfectus.Client.SDK;

namespace Perfectus.Client.Studio.PluginSystem
{
	/// <summary>
	/// Summary description for ConfigSectionHandler.
	/// </summary>
	public class ConfigSectionHandler : IConfigurationSectionHandler
	{
		public ConfigSectionHandler()
		{
			//
			//TODO: Add constructor logic here
			//
		}

		public object Create(object parent, object configContext, XmlNode section)
		{
            Collection<IStudioPlugin> plugins = new Collection<IStudioPlugin>();
			foreach(XmlNode node in section.SelectNodes("plugin"))
			{
				try
				{
					//Use the Activator class's 'CreateInstance' method
					//to try and create an instance of the plugin by
					//passing in the type name specified in the attribute value
					object plugObject = Activator.CreateInstance(Type.GetType(node.Attributes["type"].Value));

					//Cast this to an IStudioPlugin interface and add to the collection
					if (plugObject is IStudioPlugin)
					{
						IStudioPlugin plugin = (IStudioPlugin)plugObject;
						plugins.Add(plugin);
					}
				}
				catch
				{
					//Catch any exceptions
					//but continue iterating for more plugins
				}
			}

			return plugins;
		}
	}
}
