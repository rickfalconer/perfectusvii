using System.Configuration;
using System.Xml;
using Perfectus.Common;

namespace Perfectus.Client.Studio.PluginSystem
{
	/// <summary>
	/// Summary description for ConfigSectionHandler.
	/// </summary>
	public class DisplayTypesSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{

			DisplayTypeExtensionInfoCollection extensions = new DisplayTypeExtensionInfoCollection();

			foreach(XmlNode node in section.SelectNodes("displayType"))
			{
				string forDataType = node.Attributes.GetNamedItem("forDataType").Value;
				string serverSideDisplayPluginId = node.Attributes.GetNamedItem("serverSideDisplayPluginId").Value;
				string serverSideFetcherPluginId = null;
				if (node.Attributes.GetNamedItem("serverSideFetcherPluginId") != null)
				{
					serverSideFetcherPluginId = node.Attributes.GetNamedItem("serverSideFetcherPluginId").Value;
				}
				string specificCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
				string languageCulture = null;
				string defaultName = null;
				string specificName = null;
				string languageName = null;

				string[] cultureParts = specificCulture.Split('-');
				if (cultureParts.Length == 2)
				{
					languageCulture = cultureParts[0];
				}

			    string[] fields = null;
			    XmlNodeList fieldNodes = node.SelectNodes("field");
                if (fieldNodes != null && fieldNodes.Count > 0)
                {
                    fields = new string[fieldNodes.Count];
                    for (int i = 0; i < fieldNodes.Count; ++i)
                    {
                        fields[i] = fieldNodes[i].InnerText;
                    }
                }

			    string name = null;
				foreach(XmlNode nameNode in node.SelectNodes("name"))
				{
					XmlNode cultureAttribute = nameNode.Attributes.GetNamedItem("uiCulture");
					string cultureAttributeValue = null;
					if (cultureAttribute != null)
					{
						cultureAttributeValue = cultureAttribute.Value;
					}
					if (cultureAttributeValue == null)
					{
						defaultName = nameNode.InnerText;
					}
					else if (languageCulture != null && cultureAttributeValue == languageCulture)
					{
						languageName = nameNode.InnerText;
					}
					else if (specificCulture != null && cultureAttributeValue == specificCulture)
					{
						specificName = nameNode.InnerText;
					}

					name = defaultName;
					if (specificName != null)
					{
						name = specificName;
					}
					else if (languageName != null)
					{
						name = languageName;
					}
				}
                DisplayTypeExtensionInfo dtei = new DisplayTypeExtensionInfo(serverSideDisplayPluginId, serverSideFetcherPluginId, forDataType, name);
			    if (fields != null)
			    {
			        dtei.FieldNames = fields;
			    }			
                extensions.Add(dtei);
			}

			return extensions;
		}
	}
}
