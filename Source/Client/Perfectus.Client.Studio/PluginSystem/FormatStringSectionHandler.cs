using System;
using System.Configuration;
using System.Xml;
using Perfectus.Common;

namespace Perfectus.Client.Studio.PluginSystem
{
	/// <summary>
	/// Summary description for FormatStringSectionHandler.
	/// </summary>
	public class FormatStringSectionHandler : IConfigurationSectionHandler
	{
		public FormatStringSectionHandler()
		{
		}

		public object Create(object parent, object configContext, XmlNode section)
		{
			FormatStringExtensionInfoCollection extensions = new FormatStringExtensionInfoCollection();

			foreach(XmlNode node in section.SelectNodes("formatString"))
			{
				string forDataType = node.Attributes.GetNamedItem("forDataType").Value;
				string serverSideFormatterPluginKey = node.Attributes.GetNamedItem("serverSidePluginKey").Value;
				
				string specificCulture = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
				string languageCulture = null;
				string defaultName = null;
				string specificName = null;
				string languageName = null;

				string[] cultureParts = specificCulture.Split('-');
				if (cultureParts.Length == 2)
				{
					languageCulture = cultureParts[0];
				}

				string name = null;
				foreach(XmlNode nameNode in node.SelectNodes("name"))
				{
					XmlNode cultureAttribute = nameNode.Attributes.GetNamedItem("uiCulture");
					string cultureAttributeValue = null;
					if (cultureAttribute != null)
					{
						cultureAttributeValue = cultureAttribute.Value;
					}
					if (cultureAttributeValue == null)
					{
						defaultName = nameNode.InnerText;
					}
					else if (languageCulture != null && cultureAttributeValue == languageCulture)
					{
						languageName = nameNode.InnerText;
					}
					else if (specificCulture != null && cultureAttributeValue == specificCulture)
					{
						specificName = nameNode.InnerText;
					}

					name = defaultName;
					if (specificName != null)
					{
						name = specificName;
					}
					else if (languageName != null)
					{
						name = languageName;
					}
				}
				extensions.Add(new FormatStringExtensionInfo(serverSideFormatterPluginKey, forDataType, name));
			}

			return extensions;
		}

	}
}
