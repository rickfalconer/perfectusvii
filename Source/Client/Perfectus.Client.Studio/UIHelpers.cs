﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Perfectus.Client.Studio
{
    // Add Text and Value to combo box item
    // usage: x = (comboBox1.SelectedItem as ComboboxItem).Value.ToString();
    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }


    public static class Extensions
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int PostMessage(IntPtr hwnd, Int32 wMsg, Int32 wParam, Int32 lParam);

        // send keys to open a DateTimePicker
        // usage: myDTPicker.Open();
        public static void Open_dtp(this DateTimePicker obj)
        {
            const int WM_LBUTTONDOWN = 0x0201;
            int width = obj.Width - 10;
            int height = obj.Height / 2;
            int lParam = width + height * 0x00010000; // VooDoo to shift height
            PostMessage(obj.Handle, WM_LBUTTONDOWN, 1, lParam);
        }

    }

}
