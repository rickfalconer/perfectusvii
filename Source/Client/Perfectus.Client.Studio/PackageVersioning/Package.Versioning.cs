// FB1538 
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Data;

namespace Perfectus.Client.Studio.Versioning
{
    public class PackageVersionInfo : IXmlSerializable
    {
        public enum packageVersion { packageIsOlder, packageIsSame, packageIsNewer }

        
        const int _blocklen = 1024;

        private const string _xml_start = "<?xml ";
        private const String _xml_package_name = "packagename";
        private const String _xml_version_str = "version";
        private const String _xml_created_by = "createdby";
        private const String _xml_created_date = "createddate";
        private const String _xml_modified_by = "modifiedby";
        private const String _xml_modified_date = "modifieddate";
        private const String _xml_description = "description";
        private const String _xml_custom = "custom";
        private const String _xml_root_close = "</PackageVersionInfo>";

        private String _PackageName;
        private String _Description;
        private String _VersionString;
        private String _CreatedBy;
        private DateTime _CreatedDate;
        private String _ModifiedBy;
        private DateTime _ModifiedDate;
        internal IDictionary<String, String> CustomData;

        #region public properties
        
        public String PackageName
        {
            get { return _PackageName; }
            set
            {
                if (value.Length == 0) throw new Exception("Package name cannot be empty");
                if (_PackageName != value)
                {
                    if (null != _PackageName)
                        SetValue("PreviousName", _PackageName);
                    _PackageName = value.Trim();
                }
            }
        }
        public String VersionString
        {
            get { return _VersionString; }
            set
            {
                if (value.Length == 0) throw new Exception("Version cannot be empty");
                if (_VersionString != value)
                {

                    Regex vReg = new Regex(@"(\d){1,2}.(\d){1,2}.(\d){1,2}.(\d){1,4}");
                    if (!vReg.IsMatch(value))
                        throw new Exception("Version must be of the form x.x.x.xxxx");
                    if (null != _VersionString)
                    {
                        SetValue("PreviousVersion", _VersionString);
                        if (GetCustomData("CreationVersion").Length == 0)
                            SetValue("CreationVersion", _VersionString);
                    }
                    SetValue("PackageCompatibility", Perfectus.Common.About.PackageCompatibility);
                    _VersionString = value.Trim();
                }
            }
        }
        public String CreatedBy
        {
            get { return _CreatedBy; }
            set
            {
                if (value.Length == 0) throw new Exception("CreatedBy cannot be empty");
                _CreatedBy = value.Trim();
            }
        }
        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
        }
        public String ModifiedBy
        {
            get { return _ModifiedBy; }
            set
            {
                if (value.Length == 0) throw new Exception("ModifiedBy cannot be empty");
                _ModifiedBy = value.Trim();
            }
        }
        public DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
        }
        public String Description
        {
            get { return _Description; }
            set { _Description = value.Trim(); }
        }
#endregion
        
        private void SetValue(String _name, String _value)
        {
            switch (_name)
            {
                case _xml_package_name:
                    _PackageName = _value;
                    break;
                case _xml_version_str:
                    _VersionString = _value;
                    break;
                case _xml_created_by:
                    _CreatedBy = _value;
                    break;
                case _xml_created_date:
                    _CreatedDate = DateTime.Parse(_value);
                    break;
                case _xml_modified_by:
                    _ModifiedBy = _value;
                    break;
                case _xml_modified_date:
                    _ModifiedDate = DateTime.Parse(_value);
                    break;
                case _xml_description:
                    _Description = _value;
                    break;
                default:
                    if (CustomData == null)
                        CustomData = new Dictionary<String, String>();
                    _name = _name.Trim();
                    _value = _value.Trim();
                    if (CustomData.ContainsKey(_name))
                        CustomData[_name] = _value;
                    else
                        CustomData.Add(_name, _value);
                    break;
            }
        }
        private static PackageVersionInfo Create(String param_xml)
        {
            XmlSerializer ser = new XmlSerializer(typeof(PackageVersionInfo));
            MemoryStream m = new MemoryStream(Encoding.ASCII.GetBytes(param_xml));
            return ser.Deserialize(m) as PackageVersionInfo;
        }

        private void SetCustomData(String key, String value)
        {
            SetValue (key, value);
        }
        private String GetCustomData(String key)
        {
            if ( CustomData != null )
                if ( CustomData.ContainsKey(key))
                    return CustomData[key];
            return String.Empty;
        }


        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    if (!reader.IsEmptyElement)
                    {
                        String _name = reader.Name;
                        reader.Read(); // Read the start tag.
                        if (reader.IsStartElement())  // Handle nested elements.
                            _name = reader.Name;
                        String _value = reader.ReadString().Trim();
                        SetValue(_name, _value);
                    }
                }
            }
        }
        public void WriteXml(XmlWriter writer)
        {
            //writer.WriteStartElement(_xml_root);
            if (PackageName != null && PackageName.Length > 0)
                writer.WriteElementString(_xml_package_name, PackageName);
            if (VersionString != null && VersionString.Length > 0)
                writer.WriteElementString(_xml_version_str, VersionString);
            if (CreatedBy != null && CreatedBy.Length > 0)
                writer.WriteElementString(_xml_created_by, CreatedBy);
            if (CreatedDate != null && CreatedDate != DateTime.MinValue)
                writer.WriteElementString(_xml_created_date, CreatedDate.ToString("s"));
            if (ModifiedBy != null && ModifiedBy.Length > 0)
                writer.WriteElementString(_xml_modified_by, ModifiedBy);
            if (ModifiedDate != null && ModifiedDate != DateTime.MinValue)
                writer.WriteElementString(_xml_modified_date, ModifiedDate.ToString("s"));
            if (Description != null && Description.Length > 0)
                writer.WriteElementString(_xml_description, Description);
            if (CustomData != null && CustomData.Count > 0)
            {
                writer.WriteStartElement(_xml_custom);
                foreach ( String key in CustomData.Keys)
                    writer.WriteElementString(key, CustomData[key]);
                writer.WriteEndElement();
            }
        }

        #endregion


        #region static public methods

        public static DataTable LoadPackageInformationTable(String param_packagefilename)
        {
            PackageVersionInfo pi = GetPackageInformation(param_packagefilename);

            DataTable dt = new DataTable("PackageInformation");

            dt.Columns.Add("Key", typeof(string));
            dt.Columns.Add("Value", typeof(string));

            DataRow row = dt.NewRow();
            row["Key"] = "Package name";
            row["Value"] = pi.PackageName;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Key"] = "Description";
            row["Value"] = pi.Description;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Key"] = "Version";
            row["Value"] = pi.VersionString;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Key"] = "Created by";
            row["Value"] = pi.CreatedBy;
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Key"] = "Creation date";
            row["Value"] = pi.CreatedDate.ToString();
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["Key"] = "Modified by";
            row["Value"] = pi.ModifiedBy;
            dt.Rows.Add(row);

            if (pi.ModifiedDate != DateTime.MinValue)
            {
                row = dt.NewRow();
                row["Key"] = "Modified date";
                row["Value"] = pi.ModifiedDate.ToString();
                dt.Rows.Add(row);
            }

            if (pi.CustomData != null)
                foreach (String key in pi.CustomData.Keys)
                {
                    row = dt.NewRow();
                    row["Key"] = key;
                    row["Value"] = pi.CustomData[key];
                    dt.Rows.Add(row);
                }
            return dt;
        }


        public static packageVersion TestVersionCompatibility(PackageVersionInfo pInfo, string exeVersion)
        {
            String _packageCompatibility = pInfo.GetCustomData ( "PackageCompatibility" );

            if (null != _packageCompatibility)
            {
                try
                {
                    int PackageVersionNumber = Int32.Parse(_packageCompatibility);
                    int PackageSupportedVersion = Int32.Parse(Perfectus.Common.About.PackageCompatibility);

                    if (PackageVersionNumber > PackageSupportedVersion)
                        return packageVersion.packageIsNewer;
                    else
                        if (PackageVersionNumber < PackageSupportedVersion)
                            return packageVersion.packageIsOlder;
                        else
                            return packageVersion.packageIsSame;
                }
                catch {}
            }

            String[] pV = pInfo.VersionString.Split(new char[] { '.' });
            String[] eV = exeVersion.Split(new char[] { '.' });

            if (Int32.Parse(pV[0]) > Int32.Parse(eV[0]))
                return packageVersion.packageIsNewer;
            if (Int32.Parse(pV[0]) < Int32.Parse(eV[0]))
                return packageVersion.packageIsOlder;

            if (Int32.Parse(pV[1]) > Int32.Parse(eV[1]))
                return packageVersion.packageIsNewer;
            if (Int32.Parse(pV[1]) < Int32.Parse(eV[1]))
                return packageVersion.packageIsOlder;

            if (Int32.Parse(pV[2]) > Int32.Parse(eV[2]))
                return packageVersion.packageIsNewer;
            if (Int32.Parse(pV[2]) < Int32.Parse(eV[2]))
                return packageVersion.packageIsOlder;

            return packageVersion.packageIsSame;
        }

        private static void WritePackageData(String param_packagefilename, PackageVersionInfo pInfo)
        {
            if (!File.Exists(param_packagefilename))
                throw new Exception(String.Format("File not found {0}", param_packagefilename));

            if (pInfo.VersionString == null || pInfo.VersionString.Length == 0 )
                throw new Exception(String.Format("Version information incorrect"));

            if (pInfo.PackageName== null || pInfo.PackageName.Length == 0 )
                throw new Exception(String.Format("Package name incorrect"));

            XmlSerializer ser = new XmlSerializer(typeof(PackageVersionInfo));
            MemoryStream m = new MemoryStream();
            ser.Serialize(m, pInfo);

            using (FileStream fs = File.Open(param_packagefilename, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                long position = GetPosOfXML(fs);
                if ( position <= 0)
                    position = fs.Length;
                fs.Seek(position, SeekOrigin.Begin);
                fs.Write(m.ToArray(), 0, (int)m.Length);
                fs.SetLength(fs.Seek(0, SeekOrigin.Current));
            }
        }


        public static void CopyPackageInfo(String param_packagefilename,
            String ModifiedBy,
            String NewPackageName,
            String NewPackageDescription,
            String NewVersion,
            PackageVersionInfo pInfo)
        {
            if (pInfo != null)
                WritePackageData(param_packagefilename, pInfo);
            ModifyPackageInfo(param_packagefilename, ModifiedBy, NewPackageName, NewPackageDescription, NewVersion);
        }

        public static void CreatePackageInfo(String param_packagefilename, String Description,
            String packagename, String Version, String createdBy)
        {
            PackageVersionInfo pInfo = GetPackageInformation(param_packagefilename);
            if ( null != pInfo )
                throw new Exception(String.Format("Package info already exist"));

            pInfo = new PackageVersionInfo();
            pInfo.PackageName = packagename;
            pInfo.VersionString = Version;

            pInfo.CreatedBy = createdBy;
            pInfo._CreatedDate = DateTime.Now;
            if (Description.Length > 0)
                pInfo.Description = Description;

            WritePackageData(param_packagefilename, pInfo);
        }

        public static void CreatePackageInfo(String param_packagefilename,
            String param_packagename, String Version, String createdBy)
        {
            CreatePackageInfo(param_packagefilename, String.Empty, param_packagename, Version, createdBy);
        }


        public static void ModifyPackageInfo(String param_packagefilename,
            String ModifiedBy,
            String NewPackageName,
            String NewPackageDescription,
            String NewVersion)
        {
            PackageVersionInfo pInfo = GetPackageInformation(param_packagefilename);
            if (pInfo == null)
                throw new Exception ("Need to create info first");
            
            if (ModifiedBy.Length == 0)
                throw new Exception(String.Format("Need modification user name"));
            pInfo.ModifiedBy = ModifiedBy;
            pInfo._ModifiedDate = DateTime.Now;


            if (NewPackageDescription != null && NewPackageDescription.Length > 0)
                pInfo.Description = NewPackageDescription;
            if (NewPackageName.Length > 0)
                pInfo.PackageName = NewPackageName;
            if (NewVersion.Length > 0)
                pInfo.VersionString = NewVersion;

            WritePackageData(param_packagefilename, pInfo);
        }

        public static void SetCustomData(String param_packagefilename,
            String key,
            String value)
        {
            PackageVersionInfo pInfo = GetPackageInformation(param_packagefilename);
            if (pInfo == null)
                throw new Exception("Need to create info first");

            pInfo.SetCustomData(key, value);
            WritePackageData(param_packagefilename, pInfo);
        }
        
        public static String GetCustomData(String param_packagefilename,
            String key)
        {
            PackageVersionInfo pInfo = GetPackageInformation(param_packagefilename);
            if (pInfo == null)
                throw new Exception ("No data");
            
            return pInfo.GetCustomData(key);
        }
        public static PackageVersionInfo GetPackageInformation(String param_packagefilename)
        {
            if (!File.Exists(param_packagefilename))
                return null;

            try
            {
                using (
                FileStream fs = File.Open(param_packagefilename, FileMode.Open, FileAccess.Read,FileShare.ReadWrite))
                    return GetPackageInformation(fs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

#endregion
        #region private methods
        private static long GetPosOfXML(FileStream param_packagestream)
        {
            long position = param_packagestream.Seek(0, SeekOrigin.End);

            // Package damaged
            if (position < PackageVersionInfo._xml_root_close.Length)
                return -1;

            // Expected position of end token
            position = param_packagestream.Seek(-PackageVersionInfo._xml_root_close.Length, SeekOrigin.End);

            Byte[] bStream = new byte[PackageVersionInfo._xml_root_close.Length];
            param_packagestream.Read(bStream, 0, PackageVersionInfo._xml_root_close.Length);

            String answerXML = Encoding.ASCII.GetString(bStream);
            int retOff = _blocklen;

            // Test for data block available
            if (answerXML.CompareTo(PackageVersionInfo._xml_root_close) != 0)
                return -1;

            while (true)
            {
                long blockToRead = Math.Min(param_packagestream.Length, retOff);
                bStream = new byte[blockToRead];

                position = param_packagestream.Seek(-blockToRead, SeekOrigin.End);
                param_packagestream.Read(bStream, 0, (int)blockToRead);
                answerXML = Encoding.ASCII.GetString(bStream);

                // Lookup the starting node
                int pos = answerXML.LastIndexOf(PackageVersionInfo._xml_start);
                if (pos == -1)
                {
                    // not found
                    if (blockToRead == param_packagestream.Length)
                        return -1;
                    else
                    {
                        // search a wider area
                        retOff += _blocklen;
                        continue;
                    }
                }
                else
                    return position + pos;

            }
        }

        private static PackageVersionInfo GetPackageInformation(FileStream param_packagestream)
        {
            long pos = GetPosOfXML(param_packagestream);
            if (-1 == pos)
                return null;
            
            Byte[] bStream = new byte[param_packagestream.Length - pos];
            pos = param_packagestream.Seek(pos, SeekOrigin.Begin);
            param_packagestream.Read(bStream, 0, (int)param_packagestream.Length - (int)pos);
            String answerXML = Encoding.ASCII.GetString(bStream);
            return PackageVersionInfo.Create(answerXML);
        }
        #endregion
    }
}
