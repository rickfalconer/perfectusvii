﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.PluginSystem;
using Perfectus.Client.Studio.UI.Controls;
using Perfectus.Client.Studio.UI.Designers;
using Perfectus.Client.Studio.UI.Dialogs;
using Perfectus.Client.Studio.UI.PackageExplorer;
using Perfectus.Client.Studio.UI.SharedLibrary;
using Perfectus.Client.Studio.UI.ServiceProgressWrappers;
using Perfectus.Client.Studio.UI.Tasks;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Common.SharedLibrary;
using Perfectus.OdmaSupport;
using Perfectus.Client.Studio.Versioning;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;



namespace Perfectus.Client.Studio
{
    public partial class MainDesigner2 : Form
    {
        MdiChildClass uiToolbarManager_ = new MdiChildClass();
        private StringBuilder odmaTraceBuilder = new StringBuilder();
        private Int32 odmaHandle = int.MinValue;
        private string odmaDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);

        // FB606: To prevent other users from changing or deleting the package file (.ip), the IPManager needs to keep a handle on the
        // file and only close it when no longer needed. Hence we now have a global file stream object which has read/write control
        //  on the file. Other applications can still get read access on the file.
        private FileStream ipFileStream = null;

        //The collection of currently active plugins
        private static Collection<IStudioPlugin> m_plugins = null;
        private static DisplayTypeExtensionInfoCollection m_displayTypeExtensions = null;
        private static FormatStringExtensionInfoCollection m_formatStringExtensions = null;
        private IOpenSaveHandler pluginForSave = null;
        private string pluginTitleText = string.Empty;
        private bool packageUpgraded = false;

        private MRU mruList;
        private Package package;
        private PackageVersionInfo packageVersionInfo;
        private ImageList imageList1;

        private Splash splashForm;
        private string formTitleFormat;
        private Hashtable mruMenuItems;
        //private bool reportingEnabled = false;//Convert.ToBoolean(ConfigurationSettings.AppSettings["ReportingEnabled"]);

        internal delegate void PreSaveHandler();
        internal static event PreSaveHandler PreSave;
        internal delegate void ActiveDesignerHandler(DesignerBase item);
        internal static event ActiveDesignerHandler ActiveDesignerChanged;

        private String fileToOpen;

        public MainDesigner2(string paramfileToOpen) : this()
        {
            // Need to delay the open package action' as we are on the 
            // startup thread, but might need frm interaction
            this.Load += new System.EventHandler(this.MainDesigner2_Load);

            fileToOpen = paramfileToOpen;

        }

        private void MainDesigner2_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(175);
            splashTimer_Tick(null, EventArgs.Empty);

            UnloadPackage();

            try
            {
                //change properties grid split
                int width = Convert.ToInt32(panel2.Width * .55);
                SetLabelColumnWidth(propertyGrid1, width);

                string localPath = null;
                // ODMA docref's all start with '::'. Apparently.
                if (fileToOpen.Trim().Substring(0, 2) == "::")
                {
                    localPath = OdmaFetchFromDm(fileToOpen);
                }
                else
                {
                    // SharePoint: Tye ActiveX control creates a command line like:
                    // http://vpc-michaeldev2/Shared%20Documents/New%20Package.ip SharePoint
                    // http://vpc-michaeldev2/two/Shared%20Documents/New%20Package%20site%20two.ip SharePoint
                    // Note that we migth want to implement other http handlers later
                    String[] parameters = fileToOpen.Split(new char[] { ' ', '\t' });
                    if (parameters.Length > 1 && parameters[1] == "SharePoint")
                    {
                        OpenPackageFromIntegration(parameters[0]);
                        package.SetNoChanges();
                        MaybeInvokePackageFixer(true);
                        MaybeFixWord2013ContentControls();
                    }
                    else
                    {
                        // Standard file system
                        localPath = fileToOpen;
                    }
                }
                if (localPath != null)
                {
                    // Open package passing in our file stream and read/write access, and other processes can only have read access..
                    package = Package.ReadFromDisc(localPath, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read);
                    pluginForSave = null;
                    pluginTitleText = String.Empty;
                    mruList.AddFilename(localPath);
                    TestPackageVersion(package.VersionXml, localPath);
                    MaybeInvokePackageFixer(true);
                    MaybeFixWord2013ContentControls();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Common.About.FormsTitle);
                NewPackage();
            }
            ResetUI();
        }

        public MainDesigner2()
        {
            splashForm = new Splash();
            splashForm.Show();
            splashForm.Refresh();
            //
            // Required for Windows Form Designer support
            //
            try
            {
                InitializeComponent();
                this.Closing += new System.ComponentModel.CancelEventHandler(this.MainDesigner2_Closing);
                this.explorerControl.PopupDialogMoved += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>(this.explorerControl_PopupDialogMoved);

                //hide tool windows
                panel3.Visible = false;
                sharedLibraryControl.Visible = false;
                bulkAdd1.Visible = false;
                importItems1.Visible = false;

                //set styles
                this.Height = 818;
                labelExplorerHeader.BackColor = PerfectusColour.DarkBlue;
                labelPropertiesHeader.BackColor = PerfectusColour.MediumBlue;

                propertyGrid1.CommandsBackColor = PerfectusColour.MediumGreen;
                propertyGrid1.LineColor = PerfectusColour.LightGray;
                propertyGrid1.BackColor = PerfectusColour.LightGray;
                //propertyGrid1.ViewForeColor = PerfectusColour.MediumGreen;
                propertyGrid1.ViewBackColor = PerfectusColour.LightGray;
                propertyGrid1.CategoryForeColor = PerfectusColour.DarkGrey;
                propertyGrid1.HelpBackColor = PerfectusColour.LightGray;
                //propertyGrid1.item

                ReallyCenterToScreen();

                LoadPlugins();

                SharedLibraryProxy sharedLibraryProxy = new SharedLibraryProxy();

                try
                {
                    // Only display the Shared Library menu if it is activated in the client config (does not check activated status on the server)
                    if (!sharedLibraryProxy.IsSharedLibraryClientActivated())
                    {
                        sharedLibraryControl.Visible = false;
                        //TODO
                        //btSharedLibraryControl.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    string message = (ex is SharedLibraryException) ? ((SharedLibraryException)ex).Message : ex.Message;
                    MessageBox.Show(message, GetResource("PackageExplorer.ExplorerControl.ExplorerPaneError"), MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation);
                    sharedLibraryControl.Visible = false;
                    //TODO
                    //btSharedLibraryControl.Visible = false;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            formTitleFormat = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.FormTitleFormat");
            explorerControl.PropertyGrid = propertyGrid1;
            mruList = new MRU(Common.About.Company, Common.About.FormsTitle);

            UpdateMruItems();

            NewPackage();

            splashForm.Refresh();

            splashForm.BringToFront();
            splashTimer.Start();

            OdmaRegister();

        }

        protected void ReallyCenterToScreen()
        {
            Screen screen = Screen.FromControl(this);

            System.Drawing.Rectangle workingArea = screen.WorkingArea;
            this.Location = new System.Drawing.Point()
            {
                X = Math.Max(workingArea.X, workingArea.X + (workingArea.Width - this.Width) / 2),
                Y = Math.Max(workingArea.Y, workingArea.Y + (workingArea.Height - this.Height) / 2)
            };
        }

        public static void SetLabelColumnWidth(PropertyGrid grid, int width)
        {
            if (grid == null)
                return;

            FieldInfo fi = grid.GetType().GetField("gridView", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fi == null)
                return;

            Control view = fi.GetValue(grid) as Control;
            if (view == null)
                return;

            MethodInfo mi = view.GetType().GetMethod("MoveSplitterTo", BindingFlags.Instance | BindingFlags.NonPublic);
            if (mi == null)
                return;
            mi.Invoke(view, new object[] { width });
        }


        #region ODMA Support

        private bool IsOdmaAvailable()
        {
            return odmaHandle != int.MinValue && odmaHandle != 0;
        }

        private bool HaveOdmaDocument()
        {
            return odmaDocid.Trim().Trim('\0').Length > 0;
        }

        private void OdmaRegister()
        {
            odmaTraceBuilder.Append("OdmaRegister() : Handle = ");
            Int32 handle = 0;
            string errCode = string.Empty;
            try
            {
                OdmaImports.RegisterDMS(Handle.ToInt32(), ref errCode, "IPMANAGER", ref handle);
                if (handle != 0 || errCode.ToLower() != "successful")
                {
                    this.odmaHandle = handle;
                }
            }
            catch
            {
                // Intentional
            } // Assume no ODMA support on this machine

            odmaTraceBuilder.Append(handle);
            odmaTraceBuilder.Append("\n");

        }

        private string OdmaGetOpenFilePath()
        {
            odmaTraceBuilder.Append("OdmaGetOpenFilePath()\n");
            // Our standard file open dialog
            if (!IsOdmaAvailable())
            {
                odmaTraceBuilder.Append(" Odma NotAvailable - using standard file dialog.\n");
                return OdmaGetOpenFileFromStandardDialog();
            }
            else // Else, use the ODMA open dialog and whatnot
            {
                string docId = new string(' ', OdmaImports.ODM_DOCID_MAX);
                int flags = 0;
                odmaTraceBuilder.Append(" Calling ODMSelectDoc.  Returned: ");
                int retval = OdmaImports.ODMSelectDoc(odmaHandle, ref docId, ref flags);
                odmaTraceBuilder.Append(retval);
                odmaTraceBuilder.Append("\n");

                if (retval == OdmaImports.ODM_E_CANCEL)
                {
                    return null;
                }
                if (retval != 0)
                {
                    odmaTraceBuilder.Append(" Select returned non-zero: using standard file dialog.\n");
                    return OdmaGetOpenFileFromStandardDialog();
                }
                else
                {

                    string localPath = OdmaFetchFromDm(docId);
                    if (localPath != null)
                    {
                        return localPath;
                    }
                    else
                    {
                        odmaTraceBuilder.Append(" Open returned non-zero: using standard file dialog.\n");
                        return OdmaGetOpenFileFromStandardDialog();
                    }
                }
            }
        }

        private string OdmaFetchFromDm(string docref)
        {
            if (IsOdmaAvailable())
            {
                string location = new string(' ', OdmaImports.ODM_FILENAME_MAX);
                int flags = 0;

                odmaTraceBuilder.Append(string.Format(" Callind ODMOpenDoc for docId: [{0}].  Returned: ", docref.Trim().Trim('\0')));
                int retval = OdmaImports.ODMOpenDoc(odmaHandle, flags, ref docref, ref location);
                odmaTraceBuilder.Append(retval);
                odmaTraceBuilder.Append("\n");

                if (retval == OdmaImports.ODM_E_CANCEL)
                {
                    return null;
                }

                if (retval == 0)
                {
                    odmaDocid = docref;
                    return location.Trim().Trim('\0');
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        private string OdmaGetOpenFileFromStandardDialog()
        {
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                mruList.AddFilename(openFileDialog1.FileName);
                return openFileDialog1.FileName;
            }
            else
            {
                return null;
            }
        }

        private bool OdmaSave(string path)
        {
            if (IsOdmaAvailable() && HaveOdmaDocument())
            {
                odmaTraceBuilder.Append("OdmaSave()\n");
                string newDocId = new string(' ', OdmaImports.ODM_DOCID_MAX);
                odmaTraceBuilder.Append(string.Format("Calling ODMSaveDoc() for docid: [{0}].  Returned: ", odmaDocid.Trim().Trim('\0')));
                int retVal = OdmaImports.ODMSaveDoc(odmaHandle, ref odmaDocid, ref newDocId);
                odmaTraceBuilder.Append(retVal);
                odmaTraceBuilder.Append("\n");
                if (retVal == 0)
                {
                    odmaTraceBuilder.Append(" new odmaDocId: ");
                    odmaTraceBuilder.Append(newDocId.Trim().Trim('\0'));
                    odmaTraceBuilder.Append("\n");
                    odmaDocid = newDocId;
                    return true; // successfully saved the document, and got an update docid.
                }
                else
                {
                    return false; // non-zero retval for a save means there was a problem.  assume the save failed and our docid is unchanged.
                }
            }
            else
            {
                return true;  // No problem, as we didn't do anything.
            }
        }

        private string OdmaSaveAs(string temporarySaveLocation, string defaultFilename, Guid originalGuid)
        {
            odmaTraceBuilder.Append(string.Format("OdmaSaveAs({0}, {1})\n", temporarySaveLocation, defaultFilename));
            int retval;
            if (IsOdmaAvailable())
            {
                string format = new string(' ', OdmaImports.ODM_DOCID_MAX);
                string newDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);
                string location = new string(' ', OdmaImports.ODM_FILENAME_MAX);

                // If the doc isn't currently in DM, make a new one.
                if (!HaveOdmaDocument())
                {
                    odmaTraceBuilder.Append(" ODMNewDoc() returned: ");
                    retval = OdmaImports.ODMNewDoc(odmaHandle, ref odmaDocid, 0, ref format, ref location);
                    odmaTraceBuilder.Append(retval);
                    odmaTraceBuilder.Append("\n");
                    if (retval == OdmaImports.ODM_E_CANCEL)
                    {
                        package.UniqueIdentifier = originalGuid;
                        return null;
                    }
                    if (retval != 0)
                    {
                        return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
                    }
                }

                // Do a 'dm save as' whatever that means.  'SaveAs' the current (or new, if no current) docid, and get a new docid,
                string oldDocid = odmaDocid;
                odmaTraceBuilder.Append(string.Format(" Old docid: [{0}]", oldDocid.Trim().Trim('\0')));
                odmaTraceBuilder.Append(" ODMSaveAs() returned: ");
                retval = OdmaImports.ODMSaveAs(odmaHandle, ref oldDocid, ref newDocid, ref format, 0, 0);
                odmaTraceBuilder.Append(retval);
                odmaTraceBuilder.Append("\n");

                if (retval == OdmaImports.ODM_E_CANCEL)
                {
                    package.UniqueIdentifier = originalGuid;
                    return null;
                }
                if (retval != 0)
                {
                    return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
                }
                else if (Convert.ToInt32(newDocid[0]) == 0) //ODMA spec specifies that if first byte null for this ODMSaveAs param we should call ODMSaveDoc
                {
                    package.UniqueIdentifier = originalGuid;
                    Save();
                    return package.CurrentFilePath; //unchanged because doing a save
                }

                odmaTraceBuilder.Append(string.Format(" New docid: [{0}]", newDocid.Trim().Trim('\0')));


                // If we made a new doc, open it and get its location.  Otherwise, location stays.
                if (oldDocid.Trim().Trim('\0') != newDocid.Trim().Trim('\0'))
                {
                    odmaTraceBuilder.Append(" ODMOpenDoc() returned: ");
                    retval = OdmaImports.ODMOpenDoc(odmaHandle, 0, ref newDocid, ref location);
                    odmaTraceBuilder.Append(retval);
                    odmaTraceBuilder.Append("\n");
                }

                try
                {

                    if (location.Trim().Trim('\0').Length > 0)
                    {
                        File.Delete(location.Trim().Trim('\0'));
                        File.Move(temporarySaveLocation.Trim().Trim('\0'), location.Trim().Trim('\0'));
                    }
                    else
                    {
                        Package.SaveToDisc(ref ipFileStream, package);
                        OdmaSave(temporarySaveLocation.Trim().Trim('\0'));
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Common.About.FormsTitle);
                    return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
                }

                OdmaCloseCurrent();
                odmaDocid = newDocid;
                retval = OdmaImports.ODMSaveDoc(odmaHandle, ref odmaDocid, ref newDocid);

                return location.Trim().Trim('\0');
            }
            else
            {
                return SaveAsStandardDialog(defaultFilename, temporarySaveLocation, originalGuid);
            }
        }

        private void OdmaClose(string docId, bool setNoneOpen)
        {
            odmaTraceBuilder.Append("OdmaClose()\n");
            if (IsOdmaAvailable() && docId != null && docId.Trim().Length > 0)
            {
                try
                {
                    odmaTraceBuilder.Append(" Closing [");
                    odmaTraceBuilder.Append(docId.Trim().Trim('\0'));

                    OdmaImports.ODMCloseDoc(odmaHandle, ref docId, 0, 0, 0, 0);
                    odmaTraceBuilder.Append("] success\n");
                    if (setNoneOpen)
                    {
                        odmaDocid = new string(' ', OdmaImports.ODM_DOCID_MAX);
                    }
                }
                catch
                {
                    odmaTraceBuilder.Append("] failed\n");
                }
            }
        }

        public void OdmaCloseCurrent()
        {
            OdmaClose(odmaDocid, true);
        }

        #endregion

        private void MaybeFixWord2013ContentControls()
        {
            //if (packageUpgraded)
            //    return;

            // word 2013 displays the control controls differently to word 2010
            // switch the title and tag properties. (document assembly will switch them back)

            //Microsoft.Office.Interop.Word.Application app;
            //app = new Microsoft.Office.Interop.Word.Application();
            //decimal version = 12;

            //try
            //{
            //    version = decimal.Parse(app.Version);
            //    if (version > 14) // word 2013 or higher
            //    {
            //        foreach (PackageItem item in package.Templates)
            //        {
            //            WordTemplateDocument2 wt = (WordTemplateDocument2)item;
            //            wt.SwitchSdtAliasTagWord2013();
            //        }
            //        //outcomes
            //        foreach (PackageItem item in package.Outcomes)
            //        {
            //            Outcome oc = (Outcome)item;
            //            if (oc.Definition == null)
            //                continue;
            //            ActionBase[] actions = oc.Definition.AllPossibleActions;
            //            foreach (ActionBase action in actions)
            //            {
            //                if (action != null && ((OutcomeAction)action).OfficeOpenXML != null)
            //                {
            //                    bool bSwapped = false;
            //                    string thisXml = ((OutcomeAction)action).OfficeOpenXML;
            //                    XmlDocument xmlDoc = new XmlDocument();

            //                    // this will fail if its already been processed - invalid xml not sure exactly why, maybe prior save encoded it badly
            //                    try
            //                    {
            //                        xmlDoc.LoadXml(thisXml);

            //                        XmlNamespaceManager nsm = new XmlNamespaceManager(xmlDoc.NameTable);
            //                        nsm.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");

            //                        string matchSdtpr = "//w:sdt/w:sdtPr";
            //                        XmlNodeList mySdtprs = xmlDoc.SelectNodes(matchSdtpr, nsm);
            //                        foreach (XmlNode mySdt in mySdtprs)
            //                        {
            //                            if (mySdt != null)
            //                            {
            //                                string title = mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value;
            //                                string tag = mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value;
            //                                if (IsGuid(title))
            //                                {
            //                                    bSwapped = true;
            //                                    mySdt.SelectSingleNode("w:alias", nsm).Attributes["w:val"].Value = tag;
            //                                    mySdt.SelectSingleNode("w:tag", nsm).Attributes["w:val"].Value = title;
            //                                }
            //                            }
            //                        }
            //                    }
            //                    catch (Exception ebadload)
            //                    {
            //                        //ignore
            //                    }
            //                    if (bSwapped)
            //                    {
            //                        using (MemoryStream msOutput = new MemoryStream())
            //                        {
            //                            xmlDoc.Save(msOutput);
            //                            ((OutcomeAction)action).OfficeOpenXML = Encoding.UTF8.GetString(msOutput.ToArray());
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            //finally
            //{
            //    if (app != null)
            //    {
            //        System.Object oMissing = System.Reflection.Missing.Value;
            //        app.Quit(ref oMissing, ref oMissing, ref oMissing);
            //        app = null;
            //    }
            //}

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
        }

        private static void PreserveXmlWhitespace(XmlDocument oOutput)
        {
            string word2006Namespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

            //set preserve whitespace
            XPathExpression preserveWhiteSpaceXpe;
            XPathNavigator spaceXpn = oOutput.CreateNavigator();
            XmlNamespaceManager nsm = new XmlNamespaceManager(new NameTable());
            nsm.AddNamespace("w", word2006Namespace);

            preserveWhiteSpaceXpe = spaceXpn.Compile("//w:t");
            preserveWhiteSpaceXpe.SetContext(nsm);

            XPathNodeIterator preserveWhiteSpaceIterator = spaceXpn.Select(preserveWhiteSpaceXpe);
            while (preserveWhiteSpaceIterator.MoveNext())
            {
                XmlNode preserveWhiteSpaceNode = ((IHasXmlNode)preserveWhiteSpaceIterator.Current).GetNode();

                if (!string.IsNullOrEmpty(preserveWhiteSpaceNode.InnerText))
                {
                    XmlAttribute preserveSpaceAttribute = oOutput.CreateAttribute("xml:space");
                    preserveSpaceAttribute.Value = "preserve";
                    preserveWhiteSpaceNode.Attributes.Append(preserveSpaceAttribute);
                }
            }
        }


        private static Regex isGuid = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);

        private static bool IsGuid(string candidate)
        {
            if (candidate != null)
            {
                if (isGuid.IsMatch(candidate))
                {
                    return true;
                }
            }

            return false;
        }


        private void MaybeInvokePackageFixer(bool param_critical_test_only)
        {
            packageUpgraded = false;

            //pf3196 fix parentpackage refs
            MaybeFixDistributorParentPackage();

            // Find PackageFixer plugins
            foreach (IStudioPlugin plugin in Plugins)
            {
                if (plugin is IPackageFixer)
                {
                    String protocol = System.String.Empty;
                    if (((IPackageFixer)plugin).NeedFixing(package, param_critical_test_only, ref protocol))
                    {
                        StringBuilder message = new StringBuilder(String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixFoundProblem"), plugin.PluginName));
                        message.Append(System.Environment.NewLine + protocol + System.Environment.NewLine);
                        message.Append(System.Environment.NewLine + ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixAskRepair"));
                        DialogResult result = MessageBox.Show(message.ToString(), ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairNeeded"), MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                        {
                            packageUpgraded = true;
                            protocol = System.String.Empty;
                            if (((IPackageFixer)plugin).Fix(ref package, param_critical_test_only, ref protocol))
                            {
                                message = new StringBuilder(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairSucceeded"));
                                message.Append(protocol);
                            }
                            else
                            {
                                message = new StringBuilder(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairFailed"));
                                message.Append(protocol);
                            }
                            MessageBox.Show(message.ToString(), "Result", MessageBoxButtons.OK);
                        }
                    }
                    else
                        if (!param_critical_test_only)
                            MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixRepairNotNeeded"),
                                ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageFixPackedResult"),
                                MessageBoxButtons.OK);
                }
            }
        }


        private void UpdateMruItems()
        {
            if (mruMenuItems == null)
            {
                mruMenuItems = new Hashtable(mruList.MruLength);
            }

            foreach (string k in mruMenuItems.Keys)
            {
                object o = mruMenuItems[k];
                //TODO
                //if (o is ButtonTool)
                //{
                //    ((ButtonTool)o).ParentBand.Tools.Remove((ButtonTool)o);
                //    //((ButtonTool)o).Dispose();					
                //}
            }

            mruMenuItems.Clear();

            bool firstItem = true;
            int shortcut = 1;
            for (int idx = mruList.InnerCollection.Count - 1; idx >= 0; idx--)
            {
                string s = mruList.InnerCollection[idx];
                if (!mruMenuItems.ContainsKey(s.ToUpper()))
                {
                    //TODO
                    //ButtonTool bt = new ButtonTool();
                    //if (firstItem)
                    //{
                    //    bt.BeginGroup = true;
                    //    firstItem = false;
                    //}

                    //ptMainFile.SubBand.Tools.Add(bt);
                    //bt.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
                    //shortcut++;
                    //bt.Tag = s;
                    //bt.Click += new EventHandler(mruButtonClick);
                    //mruMenuItems.Add(s.ToUpper(), bt);

                    if (shortcut == 1)
                    {
                        toolStripMenuItemMRU1.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
                        toolStripMenuItemMRU1.Tag = s;
                    }
                    if (shortcut == 2)
                    {
                        toolStripMenuItemMRU2.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
                        toolStripMenuItemMRU2.Tag = s;
                    }
                    if (shortcut == 3)
                    {
                        toolStripMenuItemMRU3.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
                        toolStripMenuItemMRU3.Tag = s;
                    }
                    if (shortcut == 4)
                    {
                        toolStripMenuItemMRU4.Text = string.Format("&{0} {1}", shortcut, trimMruPath(s));
                        toolStripMenuItemMRU4.Tag = s;
                    }
                    shortcut++;
                    
                }
            }

            //TODO
            //ptMainFile.ResetTool();
        }

        private void Open(string filename)
        {
            if (!IsOkayToUnloadPackage())
                return;

            try
            {
                if (explorerControl != null)
                    explorerControl.Suspend();

                UnloadPackage();

                String[] filenamePart = filename.Split(' ');

                // Test for integration 
                // Expect to find a path as the first parameter and then an identifier
                // e.g. SharePoint http://.../file.ip 
                String[] parameters = filename.Split(new char[] { ' ', '\t' });
                if (parameters.Length > 1 && parameters[0] == "SharePoint")
                {
                    // Remove the 'SharePoint' keyword. The rest should be a valid path into SP
                    OpenPackageFromIntegration(filename.Substring(11).Trim());
                }
                else
                {
                    // Open package passing in our file stream and read/write access, and other processes can only have read access.
                    package = Package.ReadFromDisc(filename, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read);
                    pluginForSave = null;
                    pluginTitleText = String.Empty;
                    TestPackageVersion(package.VersionXml, filename);
                    mruList.AddFilename(filename);
                }

                MaybeInvokePackageFixer(true);
                MaybeFixWord2013ContentControls();

                package.ReindexQuestions();
            }
            catch (Exception ex)
            {
                string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageOpenException"), ex.Message);
                msg = msg.Replace("\\n", "\n");
                msg = msg.Replace("\\t", "\t");
                MessageBox.Show(msg, Common.About.FormsTitle);

                mruList.RemoveFilename(filename);
                UpdateMruItems();

                // Create new package to display.....
                this.NewPackage();
            }

            if (explorerControl != null)
                explorerControl.Resume();

            // Reset UI
            ResetUI();
            Cursor = Cursors.Default;
        }

        private void OpenPackageFromIntegration(string filename)
        {
            // We only support SharePoint at the moment
            foreach (IStudioPlugin p in m_plugins)
                if (p is IOpenSaveHandler && ((IOpenSaveHandler)p).Name == "SharePoint")
                {
                    IOpenSaveHandler sharePointPlugin = (IOpenSaveHandler)p;

                    // The IOpenSaveHandler interface down't support none-interactive
                    // loading. Therefore reflect over it...
                    MethodInfo invoke = p.GetType().GetMethod("OpenPackageWithURL");

                    String url = System.Web.HttpUtility.UrlDecode(filename);

                    if (invoke != null)
                    {
                        package = Package.OpenFromStream((Stream)invoke.Invoke(sharePointPlugin, new object[] { url }));
                        TestPackageVersion(package.VersionXml, null);

                        pluginForSave = sharePointPlugin;
                        pluginTitleText = sharePointPlugin.IpManagerTitleText;
                        mruList.AddFilename(String.Format("SharePoint {0}", url));
                        break;
                    }
                    else
                    {
                        mruList.RemoveFilename(String.Format("SharePoint {0}", url));
                        throw new Exception("Incompatible integration module");
                    }
                }
        }

        private void Open()
        {
            if (IsOkayToUnloadPackage())
            {
                string previousOdmaDoc = odmaDocid;
                string filename = OdmaGetOpenFilePath();
                string oldfilename = package != null ? package.CurrentFilePath : String.Empty;

                if (filename != null)
                {
                    // Dispose of current package.
                    UnloadPackage();

                    try
                    {
                        Cursor = Cursors.WaitCursor;

                        // Open package passing in our file stream and read/write access, and other processes can only have read access..
                        package = Package.ReadFromDisc(filename, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read);

                        TestPackageVersion(package.VersionXml, filename);
                        MaybeInvokePackageFixer(true);
                        MaybeFixWord2013ContentControls();

                        package.ReindexQuestions();
                        pluginForSave = null;
                        pluginTitleText = string.Empty;

                        // Assume a successful open, so close the one we had open earlier
                        if (previousOdmaDoc != null && previousOdmaDoc.Length > 0)
                        {
                            OdmaClose(previousOdmaDoc, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageOpenException"), ex.Message);
                        msg = msg.Replace("\\n", "\n");
                        msg = msg.Replace("\\t", "\t");
                        MessageBox.Show(msg, Common.About.FormsTitle);

                        mruList.RemoveFilename(filename);
                        UpdateMruItems();

                        // If the ODMA open successfully opened the file, we'll need to close it, and leave the original one open.
                        if (previousOdmaDoc != odmaDocid)
                        {
                            OdmaClose(odmaDocid, false);
                            odmaDocid = previousOdmaDoc;
                        }

                        // Create new package to display.....
                        this.NewPackage();
                    }

                    // Reset UI. NOTE: The reset function does not clear the UI if the package is null. A new case has raised to address this.
                    ResetUI();
                    Cursor = Cursors.Default;
                }
            }
        }

        private void MaybeFixDistributorParentPackage()
        {
            try
            {
                int iFixed = 0;

                foreach (Server s in package.Servers)
                {
                    foreach (Rule2 r in s.Rule2)
                    {
                        foreach (Delivery dv in r.Deliveries)
                        {
                            if (dv.Distributor.Descriptor.FriendlyName.ToLower() == "Livelink Document Distributor".ToLower())
                            {
                                if (dv.Distributor.ParentPackage == null)
                                {
                                    dv.Distributor.ParentPackage = r.ParentPackage;
                                    iFixed++;
                                }
                            }
                            if (dv.Distributor.Descriptor.FriendlyName.ToLower() == "Livelink Container Metadata Distributor".ToLower())
                            {
                                if (dv.Distributor.ParentPackage == null)
                                {
                                    dv.Distributor.ParentPackage = r.ParentPackage;
                                    iFixed++;
                                }
                            }
                        }
                    }
                }
                if (iFixed > 0)
                {
                    MessageBox.Show(string.Format("{0} LiveLink distributors were missing parentPackage and have been fixed.", iFixed), Common.About.FormsTitle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to perform Distributor check/fix. " + ex.Message, Common.About.FormsTitle);
            }
        }

        private void TestPackageVersion(String packageVersionXML, String fileName)
        {
            // http://odma.info/downloads/odma20.htm

            packageVersionInfo = null;
            if (packageVersionXML != null &&
                packageVersionXML.Length > 0 &&
                packageVersionXML.StartsWith("<?xml"))
                packageVersionInfo = PackageVersionInfo.Create(packageVersionXML);

            if (packageVersionInfo == null)
            {
                // Only relevant for packages of version 5.6
                packageVersionInfo = PackageVersionInfo.MaybeLoadFromDisk(fileName);
            }

            if (packageVersionInfo == null)
            {
                string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageHasNoVersion"), Common.About.AssemblyVersion);

                if (DialogResult.Yes != MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.UserCancelledLoading"));

                // We might fail in writing to the file, but that doesn't matter as the
                // use then would fail to save the package anyway
                try
                {
                    package.VersionXml =
                    PackageVersionInfo.CreatePackageInfo(
                        package.Name,
                        "5.0.0.0",
                        System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                }
                catch { }
            }
            else
            {
                PackageVersionInfo.packageVersion result = PackageVersionInfo.TestVersionCompatibility(packageVersionInfo, Common.About.AssemblyVersion);
                if (result == PackageVersionInfo.packageVersion.packageIsOlder)
                {
                    string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfOlderVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion);
                    if (DialogResult.Yes != MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                        throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.UserCancelledLoading"));
                    package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                }
                else
                    if (result == PackageVersionInfo.packageVersion.packageIsNewer)
#if !DEBUG
                                    throw new Exception(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfNewerVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion));
#else
                        MessageBox.Show("Debug version bypasses following sanity check: " +
                            string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PackageIsOfNewerVersion"), packageVersionInfo.VersionString, Common.About.AssemblyVersion));
#endif
            }
        }

        private void NewPackage()
        {
            if (IsOkayToUnloadPackage())
            {
                UnloadPackage();

                pluginForSave = null;
                pluginTitleText = string.Empty;

                package = ConstructNewPackage();
                OdmaClose(odmaDocid, true);
                ResetUI();
            }
        }

        private Package ConstructNewPackage()
        {
            string workingFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string basePackageFileName = string.Format(@"{0}{1}default.ip", workingFolder, Path.DirectorySeparatorChar);

            if (File.Exists(basePackageFileName))
            {
                try
                {
                    // Open package passing in our file stream and read/write access, and other processes can only have read access..
                    Package p = Package.ReadFromDisc(basePackageFileName, ref ipFileStream, FileAccess.ReadWrite, FileShare.Read);
                    // Give it a new ID
                    p.UniqueIdentifier = Guid.NewGuid();
                    p.CurrentFilePath = null;
                    p.ResetName();

                    return p;
                }
                catch (Exception ex)
                {
                    string msg = String.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.LoadDefaultPackageFailed"), basePackageFileName, ex.Message);
                    msg = msg.Replace("\\n", "\n");
                    MessageBox.Show(msg, Common.About.FormsTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return new Package();
                }
            }
            else
            {
                return new Package();
            }
        }

        private void ResetUI()
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
                {
                    f.Close();
                }
                if (package != null)
                {
                    HookupPackage(package);
                }
                propertyGrid1.SelectedObject = null;

                UpdateMruItems();

            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        private void HookupPackage(Package newPackage)
        {
            explorerControl.Package = newPackage;
            bulkAdd1.CurrentPackage = newPackage;
            importItems1.CurrentPackage = newPackage;

            package.PropertyChanged += new PropertyChangedEventHandler(package_PropertyChanged);
            package.ItemDeleted += new EventHandler<PackageItemDeletedEventArgs>(package_ItemDeleted);
            package.ItemCreated += new EventHandler<PackageItemEventArgs>(package_ItemCreated);
            package.EnsureDesignerSaved += new EventHandler<PackageItemEventArgs>(package_EnsureDesignerSaved);

            saveFileDialog1.Reset();
            ResourceManager resources = new ResourceManager(typeof(MainDesigner2));
            saveFileDialog1.Filter = resources.GetString("saveFileDialog1.Filter");
            saveFileDialog1.DefaultExt = "ip";

            PackageNameBind();

            //TODO
            //cmInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
            btInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
            //menu item.enabled ...

            PopulatePackageWithDisplayTypeExtensions();
            PopulatePackageWithFormatStringExtensions();

            package.SetNoChanges();
        }


        private bool IsOkayToUnloadPackage()
        {
            foreach (Form f in uiToolbarManager_.EzMdiChildren)
            {
                f.Close();
                if (f.Visible)
                {
                    return false;
                }
            }

            if (package != null)
            {
                bool retval = true;
                if (package.HasChanges())
                {
                    DialogResult dr = MessageBox.Show(string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.ConfirmSaveChanges"), package.Name), Common.About.FormsTitle, MessageBoxButtons.YesNoCancel);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            if (Save())
                            {
                                DoOfflinePublish();
                                retval = true;
                            }
                            else
                            {
                                retval = false;
                            }
                            break;
                        case DialogResult.No:
                            retval = true;
                            break;
                        default:
                        case DialogResult.Cancel:
                            retval = false;
                            break;
                    }
                }
                else
                {
                    retval = true;
                }

                if (retval && pluginForSave != null)
                {
                    pluginForSave.PackageUnloaded();
                }
                return retval;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Disposes of package, filestream and shutdown plugins.
        /// </summary>
        private void UnloadPackage()
        {
            // Dispose of package.
            if (package != null)
            {
                package.Dispose();
                package = null;
            }

            // Ensure the package file stream is closed
            if (ipFileStream != null)
            {
                ipFileStream.Flush();
                ipFileStream.Close();
                ipFileStream = null;
            }
        }

        private void PopulatePackageWithDisplayTypeExtensions()
        {
            int maxExtensionCount = 50;
            if (m_displayTypeExtensions != null && package != null && package.ExtensionMapping.Count < maxExtensionCount)
            {
                // If there are no existing mappings, simply copy all our plugins into the package.
                if (package.ExtensionMapping.Count == 0)
                {
                    for (int i = 0; i < m_displayTypeExtensions.Count && i < maxExtensionCount; i++)
                    {
                        int extensionBase = (int)QuestionDisplayType.Extension1;
                        package.ExtensionMapping.Add((QuestionDisplayType)(extensionBase + i), m_displayTypeExtensions[i]);
                    }
                }
                else
                {
                    // If there are alread extensions saved with this package, only add ones that aren't there yet.
                    int extensionBase = (int)(QuestionDisplayType.Extension1) + package.ExtensionMapping.Count;
                    int extensionCount = 0;
                    foreach (DisplayTypeExtensionInfo dtei in m_displayTypeExtensions)
                    {
                        bool alreadyThere = false;
                        foreach (QuestionDisplayType key in package.ExtensionMapping.Keys)
                        {
                            DisplayTypeExtensionInfo pdtei = package.ExtensionMapping[key];
                            // If it's already been loaded, skip
                            if (dtei.ServerSideDisplayPluginId == pdtei.ServerSideDisplayPluginId)
                            {
                                pdtei.LocalisedDisplayName = dtei.LocalisedDisplayName;
                                // Update the package one with the details from this machine
                                package.ExtensionMapping[key] = dtei;
                                alreadyThere = true;
                                break;
                            }
                        }
                        if (alreadyThere)
                        {
                            continue;
                        }
                        else
                        {
                            package.ExtensionMapping.Add((QuestionDisplayType)(extensionBase + extensionCount), dtei);
                            extensionCount++;
                        }
                    }
                }
            }
        }


        private void PopulatePackageWithFormatStringExtensions()
        {
            int maxExtensionCount = 50;
            if (m_formatStringExtensions != null && package != null && package.FormatStringExtensionMapping.Count < maxExtensionCount)
            {
                // If there are no existing mappings, simply copy all our plugins into the package.
                if (package.FormatStringExtensionMapping.Count == 0)
                {
                    for (int i = 0; i < m_formatStringExtensions.Count && i < maxExtensionCount; i++)
                    {
                        package.FormatStringExtensionMapping.Add(m_formatStringExtensions[i].ServerSideFormatterPluginKey, m_formatStringExtensions[i]);
                    }
                }
                else
                {
                    // If there are alread extensions saved with this package, only add ones that aren't there yet.
                    foreach (FormatStringExtensionInfo fsei in m_formatStringExtensions)
                    {
                        bool alreadyThere = false;
                        foreach (FormatStringExtensionInfo pfsei in package.FormatStringExtensionMapping.Values)
                        {
                            // If it's already been loaded, skip
                            if (pfsei.ServerSideFormatterPluginKey == fsei.ServerSideFormatterPluginKey)
                            {
                                pfsei.LocalisedDisplayName = fsei.LocalisedDisplayName;
                                alreadyThere = true;
                                break;
                            }
                        }
                        if (alreadyThere)
                        {
                            continue;
                        }
                        else
                        {
                            package.FormatStringExtensionMapping.Add(fsei.ServerSideFormatterPluginKey, fsei);
                        }
                    }
                }
            }
        }



        private void splashTimer_Tick(object sender, EventArgs e)
        {
            splashTimer.Stop();
            if (!splashForm.IsDisposed)
                splashForm.Dispose();
        }

        public static DisplayTypeExtensionInfoCollection DisplayTypeExtensions
        {
            get { return m_displayTypeExtensions; }
        }

        public static Collection<IStudioPlugin> Plugins
        {
            get { return m_plugins; }
        }

        private void LoadPlugins()
        {
            //Retrieve a plugin collection using our custom Configuration section handler
            m_plugins = (Collection<IStudioPlugin>)ConfigurationSettings.GetConfig("plugins");
            m_displayTypeExtensions = (DisplayTypeExtensionInfoCollection)ConfigurationSettings.GetConfig("displayTypeExtensions");
            m_formatStringExtensions = (FormatStringExtensionInfoCollection)ConfigurationSettings.GetConfig("formatStringExtensions");

            // Setup click handlers for all OpenSaveHandler type plugins
            EventHandler openHandler = new EventHandler(OpenPlugin_Click);
            EventHandler saveAsHandler = new EventHandler(SaveAsPlugin_Click);

            foreach (IStudioPlugin p in m_plugins)
            {
                try
                {
                    p.Init(this);
                }
                catch
                { }
                // If the plugin is one that provides file open and save features, add it to the File menu.
                if (p is IOpenSaveHandler)
                {
                    IOpenSaveHandler osh = (IOpenSaveHandler)p;
                    AddOpenSavePluginMenuItems(openHandler, saveAsHandler, osh);
                }
                if (p is IPackageFixer)
                {
                    IPackageFixer pfx = (IPackageFixer)p;
                    //TODO
                    //this.btFixPackage.Visible = true; ;
                }
                if (p is IConfiguration)
                {
                    Perfectus.Client.Configuration.Handler.AddPlugin(p as IConfiguration);
                    //TODO
                    //this.btCustomize.Visible = true;
                }
            }
            explorerControl.Plugins = m_plugins;
        }


        private void AddOpenSavePluginMenuItems(EventHandler openHandler, EventHandler saveAsHandler, IOpenSaveHandler osh)
        {
            if (osh.SupportsOpen)
            {// Make an 'open from x' menu item
                //TODO
                //ButtonTool bt = new ButtonTool();
                //bt.Click += openHandler;
                //bt.Text = osh.OpenFileMenuName;
                //bt.Tag = osh;
                //if (osh.FileMenuOpenIcon != null)
                //{
                //    bt.Image = osh.FileMenuOpenIcon;
                //}

                //int openPosition = ptMainFile.SubBand.Tools.IndexOf(btFileOpen);
                //ptMainFile.SubBand.Tools.Insert(openPosition + 1, bt);
            }

            if (osh.SupportsSaveAs)
            {// Make a 'save as to x' menu item
                //TODO
                //ButtonTool bt = new ButtonTool();
                //bt.Click += saveAsHandler;
                //bt.Text = osh.SaveAsFileMenuName;
                //bt.Tag = osh;
                //if (osh.FileMenuSaveAsIcon != null)
                //{
                //    bt.Image = osh.FileMenuSaveAsIcon;
                //}

                //int saveAsPosition = ptMainFile.SubBand.Tools.IndexOf(btFileSaveAs);
                //ptMainFile.SubBand.Tools.Insert(saveAsPosition + 1, bt);

            }
        }


        private void OpenPlugin_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (IsOkayToUnloadPackage())
            {
                //TODO
                //ButtonTool bt = (ButtonTool)sender;
                //IOpenSaveHandler osh = (IOpenSaveHandler)bt.Tag;
                //try
                //{// If a plugin's 'open from' menu item is clicked, ask it to open a stream and give it to us
                //    // (The plugin itself is in the Tag of the menu item)
                //    Stream s = osh.OpenPackage();

                //    if (s != null)
                //    {
                //        // Dispose of current package stuff...
                //        UnloadPackage();

                //        using (s)
                //        {
                //            package = Package.OpenFromStream(s);

                //            TestPackageVersion(package.VersionXml, null);

                //            package.SetNoChanges();
                //            pluginForSave = osh;
                //            pluginTitleText = osh.IpManagerTitleText;

                //            // SharePoint only;
                //            // We are misusing the window title, which is expected to be 
                //            // 'SharePoint http://.../file.ip'
                //            // --Other possibility is to reflect over the IOpenSaveHandler object and to
                //            // --get the URL from the location property.
                //            if (pluginTitleText.StartsWith("SharePoint "))
                //                mruList.AddFilename(pluginTitleText);

                //            MaybeInvokePackageFixer(true);

                //            package.ReindexQuestions();

                //            ResetUI();
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{
                //    if (package != null)
                //        package.SetNoChanges();

                //    string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PluginError"), osh.Name, ex.Message);
                //    msg = msg.Replace("\\n", "\n");
                //    MessageBox.Show(msg, Common.About.FormsTitle);
                //}
                //finally
                //{
                //    Cursor = Cursors.Default;
                //}

            }
        }

        private void SaveAsPlugin_Click(object sender, EventArgs e)
        {
            //TODO
            //ButtonTool bt = (ButtonTool)sender;
            //IOpenSaveHandler osh = (IOpenSaveHandler)bt.Tag;
            //DoPluginSave(osh, true);
        }

        private bool DoPluginSave(IOpenSaveHandler plugin, bool saveAs)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                if (package != null && plugin != null)
                {
                    Package.SaveToStream(package, ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    if (saveAs)
                    {
                        bool saveAsRetVal = plugin.SavePackageAs(ms, package.Name, LegaliseFilename(package.Name));
                        if (saveAsRetVal)
                        {
                            pluginForSave = plugin;
                            pluginTitleText = plugin.IpManagerTitleText;

                            if (pluginTitleText.StartsWith("SharePoint "))
                            {
                                mruList.AddFilename(pluginTitleText);
                                UpdateMruItems();
                            }

                            PackageNameBind();
                            //FB887
                            package.SetNoChanges();
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return plugin.SavePackage(ms);
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PluginError"), plugin.Name, ex.Message);
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(msg, Common.About.FormsTitle);
#if DEBUG
                MessageBox.Show(ex.StackTrace);
#endif
                return false;
            }
            finally
            {
                if (ms != null)
                {
                    ms.Close();
                }
            }
        }
		
        /// <summary>
        ///     Gets a string resource from the default resource file for the given key.
        /// </summary>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string key)
        {
            return GetResource("Perfectus.Client.Studio.Localisation", key);
        }

        /// <summary>
        ///     Gets a string resource from the given resource file using the key.
        /// </summary>
        /// <param name="resourceFilename">The localisation file to locate the resource.</param>
        /// <param name="key">The resource key to get.</param>
        /// <returns>A string from the localisation file.</returns>
        private static string GetResource(string resourceFilename, string key)
        {
            return ResourceLoader.GetResourceManager(resourceFilename).GetString(key);
        }

        private void DoOfflinePublish()
        {
            if (package != null && ConfigurationSettings.AppSettings["OfflinePublishEnabled"].ToLower() == "true")
            {
                try
                {
                    OfflinePublishWrapper t = new OfflinePublishWrapper(0, package);
                    ServiceProgressDialog dlg = new ServiceProgressDialog(new BackgroundWorkerBase[] { t });
                    dlg.Start();
                    DialogResult drPub = dlg.ShowDialog();
                    if (drPub == DialogResult.Cancel)
                    {
                        t.Abort(false);
                    }
                }
                finally
                {
                    package.SetNoChanges();
                }
            }
        }

        private bool Save()
        {
            if (package == null)
                return false;

            if (PreSave != null)
                PreSave();

            // save the current version information inmemory
            PackageVersionInfo packageVersionInfo;
            if (package.VersionXml != null && package.VersionXml.Length > 0)
            {
                packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);

                PackageVersionInfo.ModifyPackageInfo(ref packageVersionInfo,
                        System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                        package.Name,
                        package.Description,
                        Common.About.AssemblyVersion);
                package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
            }
            else
            {
                package.VersionXml = PackageVersionInfo.CreatePackageInfo(
                    package.Description,
                    package.Name,
                    Common.About.AssemblyVersion, System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);
            }

            // Do an index repair to resolve issue #2260 (and #1802) where old packages with questions imported from even older packages are missing from the internal dictionary of UID -> Question
            package.ReindexQuestions();
            package.TempOfflineId = Guid.NewGuid();

            if (pluginForSave != null)
            {
                return DoPluginSave(pluginForSave, false);
            }
            else
            {
                // Do we have a current file & path?
                if (package.CurrentFilePath != null)
                {
                    try
                    {
                        SaveDesigners();
                        RefreshActiveDesigner();

                        // physical save
                        Package.SaveToDisc(ref ipFileStream, package);

                        return OdmaSave(package.CurrentFilePath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, Common.About.FormsTitle);
                        return false;
                    }
                }
                else
                {
                    RefreshActiveDesigner();
                    return SaveAs();
                }
            }
        }

        private string SaveAsStandardDialog(string defaultFilename, string temporarySaveLocation, Guid originalGuid)
        {
            saveFileDialog1.FileName = defaultFilename;
            DialogResult dr;

            try
            {
                dr = saveFileDialog1.ShowDialog();
            }
            catch // If there is no file at the location of the defaultFileName then an exception happens before the dialog appears.
            {
                saveFileDialog1.FileName = null;
                dr = saveFileDialog1.ShowDialog();
            }

            if (dr == DialogResult.OK)
            {
                try
                {
                    // Close current file stream. We might be saving to the same file, hence need to do this first.
                    if (ipFileStream != null)
                        ipFileStream.Close();

                    // Move the temporary file to our new file.
                    ipFileStream = Package.MoveOnDisc(temporarySaveLocation, saveFileDialog1.FileName);

                    mruList.AddFilename(saveFileDialog1.FileName);
                    UpdateMruItems();
                    return saveFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    // Immediately restore the file stream on the original file to maintain our write access to the file.
                    try
                    {
                        ipFileStream = Package.OpenOnDisc(package.CurrentFilePath);
                    }
                    catch
                    {
                        // If unable to open the original file again, we simply lose our exclusive access.
                        if (ipFileStream != null)
                        {
                            ipFileStream.Close();
                            ipFileStream = null;
                        }
                    }
                    MessageBox.Show(ex.Message, Common.About.FormsTitle);
                }
            }

            package.UniqueIdentifier = originalGuid;
            return null;
        }

        public static string LegaliseFilename(string filename)
        {
            foreach (char c in Path.GetInvalidFileNameChars())
            {
                filename = filename.Replace(c, '_');
            }
            filename = filename.Replace('\\', '_');
            filename = filename.Replace('/', '_');
            return filename;
        }

        private bool SaveAs()
        {
            if (package == null)
                return false;

            // Save to a temporary location.
            FileStream tempFileStream = null;
            string tempLoc = Path.GetTempFileName();
            string defaultFilename = LegaliseFilename(string.Format("{0}.ip", package.Name));
            package.TempOfflineId = Guid.NewGuid();
            Guid originalGuid = package.UniqueIdentifier;

            try
            {
                try
                {
                    //we do a save to disk here because we dont know which dialog will be displayed to the user (ODMA or File System)
                    //if it is ODMA we lose control and need to have the latest version saved to disk already
                    SaveDesigners();
                    packageVersionInfo = null;
                    try
                    {
                        packageVersionInfo = PackageVersionInfo.Create(package.VersionXml);
                    }
                    catch { }

                    try
                    {
                        if (packageVersionInfo == null)
                            package.VersionXml =
                            PackageVersionInfo.CreatePackageInfo(
                                package.Description,
                                package.Name,
                                Common.About.AssemblyVersion,
                                System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                        else
                        {
                            PackageVersionInfo.CopyPackageInfo(
                                System.Security.Principal.WindowsIdentity.GetCurrent().Name,
                                package.Name,
                                package.Description,
                                Common.About.AssemblyVersion,
                                packageVersionInfo);

                            package.VersionXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                        }
                    }
                    catch { }

                    // Save to temp file....
                    tempFileStream = File.OpenWrite(tempLoc);
                    Package.SaveToDisc(ref tempFileStream, package, tempLoc, true, false);

                    //
                    //save an xml copy
                    //using (var writer = new System.IO.StreamWriter(@"C:\Temp\ip.xml"))
                    //{
                    //    var serializer = new XmlSerializer(package.GetType());
                    //    serializer.Serialize(writer, package);
                    //    writer.Flush();
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Common.About.FormsTitle);
                    return false;
                }
                finally
                {
                    if (tempFileStream != null)
                        tempFileStream.Close();
                }

                // Ask odma to do its convoluted 'SaveAs' business
                //we pass in the original Guid because if it is a normal windows save dialog and the user cancels, we need to keep reset
                //it to the original Guid
                string finalLocation = OdmaSaveAs(tempLoc, defaultFilename, originalGuid);
                if (finalLocation != null && finalLocation.Length > 0)
                {
                    pluginTitleText = string.Empty; // otherwise a previous open from plugin will have its title text used.
                    package.CurrentFilePath = finalLocation.Trim().Trim('\0');
                    //->FB887 MR 13/Jun/07
                    // Hash over package is calculated in "Package.SaveToDisc(...)", but package object will be 
                    // modified again when asking for the final filename. Hence recalc the hash...
                    package.SetNoChanges();
                    //<-FB887 

                    return true;
                }
                else
                {
                    return false;
                }

            }
            finally
            {
                // Remove working file in temp folder.
                if (!String.IsNullOrEmpty(tempLoc))
                    File.Delete(tempLoc);
            }
        }

        private void RefreshActiveDesigner()
        {
            if (uiToolbarManager_.ActiveEzMdiChild is DesignerBase)
            {
                ((DesignerBase)(uiToolbarManager_.ActiveEzMdiChild)).UpdateUI();

                if (ActiveDesignerChanged != null)
                {
                    ActiveDesignerChanged((DesignerBase)uiToolbarManager_.ActiveEzMdiChild);
                }
            }
        }

        protected void package_EnsureDesignerSaved(object sender, PackageItemEventArgs e)
        {
            SaveDesigner(((PackageItem)e.Item).Name);
        }

        private void SaveDesigner(string name)
        {
            if (package != null && uiToolbarManager_.EzMdiChildren.Length > 0)
            {
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
                {
                    // Save the designer by matching the given name
                    if (f is DesignerBase && ((DesignerBase)f).ItemBeingEdited.Name == name)
                    {
                        ((DesignerBase)f).Save();
                    }
                }
            }
            if (package != null && this.MdiChildren.Length > 0)
            {
                foreach (Form f in this.MdiChildren)
                {
                    // Save the designer by matching the given name
                    if (f is DesignerBase && ((DesignerBase)f).ItemBeingEdited.Name == name)
                    {
                        ((DesignerBase)f).Save();
                    }
                }
            }
        }

        private void SaveDesigners()
        {
            if (package != null && uiToolbarManager_.EzMdiChildren.Length > 0)
            {
                foreach (Form f in uiToolbarManager_.EzMdiChildren)
                {
                    if (f is DesignerBase)
                    {
                        ((DesignerBase)f).Save();
                    }
                }
            }
            if (package != null && this.MdiChildren.Length > 0)
            {
                foreach (Form f in this.MdiChildren)
                {
                    if (f is DesignerBase)
                    {
                        ((DesignerBase)f).Save();
                    }
                }
            }
        }

        private void package_ItemCreated(object sender, PackageItemEventArgs e)
        {
            if (e.Item is Interview)
            {
                btInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
            }
        }

        private void package_ItemDeleted(object sender, PackageItemDeletedEventArgs e)
        {
            btInterviewDiagram.Enabled = (package != null && package.Interviews.Count > 0);
        }
        
        private void package_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Name":
                case "CurrentFilePath":
                    PackageNameBind();
                    break;
            }
        }

        private void PackageNameBind()
        {
            StringBuilder sb = new StringBuilder(61);
            string fileName;
            if (pluginForSave != null && pluginTitleText != string.Empty)
            {
                Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat, Common.About.FormsTitle, "", pluginTitleText);
            }
            else if (package.CurrentFilePath != null && package.CurrentFilePath.Length > 0)
            {
                PathCompactPathEx(sb, package.CurrentFilePath, 60, 0);
                fileName = sb.ToString();
                Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat, Common.About.FormsTitle, package.Name, fileName);
            }
            else
            {
                fileName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.NotSavedTitleSuffix");
                Text = string.Format(CultureInfo.CurrentUICulture, formTitleFormat, Common.About.FormsTitle, package.Name, fileName);
            }
        }

        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        private static extern int PathCompactPathEx([Out] StringBuilder pszOut, string szPath, int cchMax, int dwFlags);

        private static string trimMruPath(string path)
        {
            StringBuilder sb = new StringBuilder(41);
            PathCompactPathEx(sb, path, 40, 0);
            return sb.ToString();
        }

        private String _currentPropertyName;
        private void propertyGrid1_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            DialogResult result = DialogResult.Yes;

            if (_currentPropertyName.Length > 0)
            {
                // TextQuestionQueryValue
                if (e.OldValue != null && e.OldValue.GetType() == typeof(TextQuestionQueryValue))
                {
                    TextQuestionQueryValue oldValue = e.OldValue as TextQuestionQueryValue;
                    TextQuestionQueryValue newValue = e.ChangedItem.Value as TextQuestionQueryValue;

                    if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query &&
                        newValue.ValueType != TextQuestionQueryValue.TextQuestionQueryValueType.Query)
                    {
                        // case 1: User removes query, but still has a binding. The front-end already asked if that was intended

                        // case 2: Query was replaced with 'normal' text. Let the user confirm

                        // case 3a: Query was removed (e.g. <backspace>)
                        // case 3b: Query was removed by removing query object in editor and first binding was empty

                        if (newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.NoValue || //  cleared out
                            (newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            (newValue.TextValue == null || newValue.TextValue.IndexOf("<text") != 0)))     // replaced with none-binding
                            result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveQuery"),
                                GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    }
                    // case 3: Binding object becomes 'normal text'

                    else if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                            oldValue.TextValue != null &&
                            newValue.TextValue != null &&
                            oldValue.TextValue.IndexOf("<text") == 0 &&
                            newValue.TextValue.IndexOf("<text") == -1)
                        result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveBinding"),
                                GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    else if (oldValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Question &&
                             newValue.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text &&
                             newValue.TextValue != null &&
                             newValue.TextValue.IndexOf("<text") != 0
                        )
                        result = MessageBox.Show(GetResource("PackageExplorer.ExplorerControl.ShallRemoveQuestion"),
                            GetResource("PackageExplorer.ExplorerControl.UserConfirmation"),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                    if (result != DialogResult.Yes)
                        try
                        {
                            PackageItem selectedItem = (PackageItem)((PropertyGrid)s).SelectedObject;

                            // Plugins have faked properties...
                            if (selectedItem is PluginBase)
                            {
                                PluginBase myCC = selectedItem as PluginBase;
                                if (myCC != null)
                                {
                                    myCC[_currentPropertyName] = oldValue;
                                }
                            }
                            else
                            {
                                // Other properties items can be invoked...
                                PropertyInfo p = selectedItem.GetType().GetProperty(_currentPropertyName);
                                MethodInfo method = p.GetSetMethod();
                                method.Invoke(selectedItem, new object[] { oldValue });
                                return;
                            }
                        }
                        catch { }
                }
            }

        }

        private void propertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
            _currentPropertyName = e.NewSelection.PropertyDescriptor.Name;

        }

        private void btNew_Click(object sender, EventArgs e)
        {
            NewPackage();
        }

        private void btOpen_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                DoOfflinePublish();
            }
        }

        private void btPublish2_Click(object sender, EventArgs e)
        {
            DoPublish();
        }

        private void btInterviewDiagram_Click(object sender, EventArgs e)
        {
            if (package != null && package.Interviews.Count > 0)
            {
                DoOpenDesigner(package.Interviews[0], PrefferedDesigner.Default);
            }
        }

        private void DoPublish()
        {
            SaveDesigners();

            bool hasServer = package.Servers.Count > 0;
            bool hasInterview = package.Interviews.Count > 0;

            if (package != null && hasServer && hasInterview)
            {
                String packageVersionInfoXml = PackageVersionInfo.ConvertToXml(packageVersionInfo);
                if (packageVersionInfoXml != null && packageVersionInfoXml.Length > 0)
                    package.VersionXml = packageVersionInfoXml;

                PublishDialog dlg = new PublishDialog(package);
                if (packageVersionInfoXml != null && packageVersionInfoXml.Length > 0)
                    dlg.PackageVersionXml = packageVersionInfoXml;

                dlg.ShowDialog();
            }
            else
            {
                string errorMessage = "";
                if (!hasInterview)
                {
                    errorMessage += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PleaseAddAnInterview") + "\n";
                }
                if (!hasServer)
                {
                    errorMessage += ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.MainDesigner.PleaseAddAProcessManager");
                }

                MessageBox.Show(errorMessage, Common.About.FormsTitle);
            }
        }


        private void packageExplorer_OpenDesigner(object sender, PackageItemEventArgs e, PrefferedDesigner prefferedDesigner)
        {
            PackageItem i = e.Item;

            DoOpenDesigner(i, prefferedDesigner);
            return;
        }

        private void DoOpenDesigner(PackageItem i, PrefferedDesigner prefferedDesigner)
        {
            if (i != null)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    if (i is IDesignable)
                    {

                        //foreach (TabPage page in tabForms.TabPages)
                        foreach (TabPage page in tabForms2.TabPages)
                            {
                            Form f = (Form)page.Tag;
                            if (f == null)
                                continue;
                            if (f is DesignerBase)
                            {
                                if (((DesignerBase)f).ItemBeingEdited == i)
                                {
                                    // Because the preffered designer variable means we can have multiple designers, we also need to check the designer is the same.
                                    if (((DesignerBase)f).DefaultDesigner == prefferedDesigner)
                                    {
                                        //tabForms.SelectTab(page);
                                        tabForms2.SelectTab(page);
                                        //page.Select();
                                        f.Activate();
                                        f.Show();
                                        return;
                                    }
                                }
                            }
                        }

                        /*
						// Make sure it's not already open
						foreach (Form f in uiToolbarManager1.EzMdiChildren)
						{
							if (f is DesignerBase)
							{
								if (((DesignerBase) f).ItemBeingEdited == i)
								{
									// Because the preffered designer variable means we can have multiple designers, we also need to check the designer is the same.
									if(((DesignerBase) f).DefaultDesigner == prefferedDesigner)
									{
										f.Activate();
										f.Show();
										return;
									}
								}
							}
						}
                         */

                        // Otherwise, we'll establish a new one.

                        DesignerBase designerToOpen;

                        // Check if a preffered designer is set, this allows the same type of package item to be used by multiple designers and have the designer set at run time.
                        if (prefferedDesigner == PrefferedDesigner.ReportViewer)
                        {
                            designerToOpen = new ReportViewer(i);
                        }
                        // If a preffered designer hasn't been set then do the next best thing and have a guess based on the items type.
                        else if (i is WordTemplateDocument2)
                        {
                            designerToOpen = new TemplateDesigner();
                            ((TemplateDesigner)designerToOpen).PackageItemSelected += new SelectedPackageLibraryItemHandler(MainDesigner2_PackageItemSelected);
                        }
                        else if (i is InternalPage)
                        {
                            designerToOpen = new PageDesigner(i, propertyGrid1);
                        }
                        else if (i is Interview)
                        {
                            designerToOpen = new InterviewDesigner(i);
                            ((InterviewDesigner)designerToOpen).OpenDesigner += new EventHandler<PackageItemEventArgs>(InterviewDesigner_OpenDesigner);
                            //FB9
                            designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_Select);
                        }
                        else
                        {
                            return;
                            //throw new NotSupportedException();
                        }

                        designerToOpen.MdiParent = this;

                        // We need to set this here, otherwise the MDIList tool sticks with the old default name.
                        designerToOpen.Text = i.Name;
                        designerToOpen.PropertyChanged += new PropertyChangedEventHandler(templateDesignerForm_PropertyChanged);
                        designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_ItemSelected);

                        designerToOpen.Show();

                        // Make sure we show the window *before* setting the template - as that triggers the dsoframer
                        // to load, and it doesn't work if it's not done after Show().
                        if (designerToOpen is TemplateDesigner)
                        {
                            ((TemplateDesigner)designerToOpen).SetTemplate(i as TemplateDocument);
                        }
                        else if (designerToOpen is ReportViewer)
                        {
                            ((ReportViewer)designerToOpen).Navigate();
                        }
                    }
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }


        /// <summary>
        ///     Handles the PackageItemSelected event as raised from the TemplateDesigner, and ensures the originating
        ///     tempalte designer remains focused. Supports cross threaded UI calls.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        public void MainDesigner2_PackageItemSelected(object o, SelectedPackageItemEventArgs e, int foreignPid)
        {
            // Do not accept events from other processes
            //if (System.Diagnostics.Process.GetCurrentProcess().Id != foreignPid)
            //    return;
            //WORD 2016 ignore for now
            if (foreignPid != -1)
                if (System.Diagnostics.Process.GetCurrentProcess().Id != foreignPid)
                    return;

            // FB1871: Found problems with invoking the code to select the item in explorer control whilst dragging.
            // Possible cause was thread locking when dragging on item and an event occurs to set the focus to another.
            // Changed to perform asyncronous invocation instead, which would allow the drag to complete before we
            // try to set the focus or select a particular item on the explorer list which we are in the process of dragging.
            if (this.InvokeRequired)
            {
                ExplorerSelectItem selectdelegate = new ExplorerSelectItem(this.SelectAndFocusItem);
                this.BeginInvoke(selectdelegate, new object[] { e.Name, e.UniqueIdentifier });
            }
            else
                SelectAndFocusItem(e.Name, e.UniqueIdentifier);
        }

        // Delegate used for cross thread invocation to access the explorer control.
        delegate void ExplorerSelectItem(string name, Guid itemUid);

        /// <summary>
        /// Selects the appropriate package item in the explorer control. Assumes we are in the correct UI thread 
        /// by this stage which would have been invoked via the delegate above...
        /// </summary>
        public void SelectAndFocusItem(string name, Guid itemUid)
        {
            explorerControl.SelectItem(name, itemUid);

            if (this.ActiveMdiChild != null && this.ActiveMdiChild.ActiveControl != null)
                this.ActiveMdiChild.ActiveControl.Focus();
        }

        private void InterviewDesigner_OpenDesigner(object sender, PackageItemEventArgs e)
        {
            DoOpenDesigner(e.Item, PrefferedDesigner.Default);
        }

        private void designerToOpen_Select(object sender, PackageItemEventArgs e)
        {
            explorerControl.Select();
            explorerControl.Focus();
        }

        private void designerToOpen_ItemSelected(object sender, PackageItemEventArgs e)
        {
            explorerControl.SelectItem(e.Item);
        }

        private void templateDesignerForm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Text":
                    //TODO
                    //mdiListTool1.ResetTool();
                    //mdiListTool1.IsDirty = true;
                    Refresh();
                    break;
            }
        }

        private void MainDesigner2_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild == null)
            {
                //this.tabForms.Visible = false;
                this.tabForms2.Visible = false;
            }
            else
            {
                if (!(ActiveMdiChild is DesignerBase))
                    return;

                if (ActiveMdiChild is TemplateDesigner)
                {
                    TemplateDesigner designer = ActiveMdiChild as TemplateDesigner;
                    if (designer.InternalDisposal || designer.Disposing)
                        return;
                }

                this.ActiveMdiChild.ControlBox = false;
                this.ActiveMdiChild.MinimizeBox = false;
                this.ActiveMdiChild.MaximizeBox = false;
                this.ActiveMdiChild.ShowIcon = false;
                //this.ActiveMdiChild.ShowInTaskbar = false; //causes exception
                this.ActiveMdiChild.WindowState = FormWindowState.Maximized; // Child form always maximized

                // If child form is new and no has tabPage, create new tabPage
                if (this.ActiveMdiChild.Tag == null)
                {
                    // Add a tabPage to tabControl with child form caption
                    TabPage tp = new TabPage(this.ActiveMdiChild.Text);
                    tp.Tag = this.ActiveMdiChild;
                    //tp.Parent = this.tabForms;
                    //cannot be added to non tab cntrol 
                    //tp.Parent = this.tabForms2.TabControl2;
                    //this.tabForms.SelectedTab = tp;
                    //this.tabForms2.SelectedTab = tp;
                    tabForms2.AddTab(tp);

                    uiToolbarManager_.Add(this.ActiveMdiChild);

                    this.ActiveMdiChild.Tag = tp;

                    ((DesignerBase)this.ActiveMdiChild).PropertyChanged += new PropertyChangedEventHandler(ItemBeingEdited_PropertyChanged);
                    this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);
                }

                //if (!this.tabForms.Visible) this.tabForms.Visible = true;
                if (!this.tabForms2.Visible) this.tabForms2.Visible = true;
            }
            // Set the active child here (maybe we were called by a select-change)
            uiToolbarManager_.SetActiveMDIChild(this.ActiveMdiChild);

            // ensure the tab is same as the form. this form may have been activated after a delete, or click, for example.
            // activepdichild is recorded as the tabpage tag, so pass the activemdichecld and let tabcontrol find it
            if (this.ActiveMdiChild != null)
                tabForms2.SelectTab(this.ActiveMdiChild);

            RefreshActiveDesigner();
        }

        private void ItemBeingEdited_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            DesignerBase p = (DesignerBase)sender;
            switch (e.PropertyName)
            {
                case "Text":
                    //foreach (TabPage page in tabForms.TabPages)
                    foreach (TabPage page in tabForms2.TabPages)
                            if (page.Tag == p)
                        {
                            page.Text = p.Text;
                            break;
                        }
                    break;
            }
        }
        // If child form closed, remove tabPage
        private void ActiveMdiChild_FormClosed(object sender, FormClosedEventArgs e)
        {
            uiToolbarManager_.RemoveForm(sender);

            //foreach (TabPage page in tabForms.TabPages)
            foreach (TabPage page in tabForms2.TabPages)
                    if (page.Tag == sender)
                {
                    //this.tabForms.TabPages.Remove(page);
                    //try this to fix word on server 2012? rdf jan 2016
                    ((sender as Form).Tag as TabPage).Dispose();
                    break;
                }

            //foreach (TabPage page in tabForms.TabPages)
            foreach (TabPage page in tabForms2.TabPages)
                    if (page.Tag is TemplateDesigner)
                {
                    //tabForms.SelectedTab = page;
                    tabForms2.SelectTab(page);
                    //(page.Tag as Form).Select();
                    break;
                }
        }

        private void tabForms2_selectedIndexChanged(object sender, Ui.PackageExplorer.TabControlWithDelete2.TabControlWithDelete2EventArgs e)
        {
            if ((this.tabForms2.TabControl2.SelectedTab != null) && (this.tabForms2.TabControl2.SelectedTab.Tag != null))
                uiToolbarManager_.SelectAndActive(this.tabForms2.TabControl2.SelectedTab.Tag as Form);
        }

        private void tabForms2_tabTryClose(object sender, Ui.PackageExplorer.TabControlWithDelete2.TabControlWithDelete2EventArgs e)
        {
            Form f = e.tag as Form;
            f.Close();
        }

        private void explorerControl_OutcomeDrop(object sender, PackageItemEventArgs e)
        {
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
            {
                ((TemplateDesigner)(uiToolbarManager_.ActiveEzMdiChild)).WrapSelectionWithOutcome((Outcome)e.Item);
            }
        }

        private void explorerControl_Publish(object sender, EventArgs e)
        {
            DoPublish();
        }

        private void explorerControl_SimpleOutcomeDrop(object sender, PackageItemEventArgs e)
        {
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
            {
                ((TemplateDesigner)(uiToolbarManager_.ActiveEzMdiChild)).WrapSelectionWithSimpleOutcome((SimpleOutcome)e.Item);
            }
        }

        /// <summary>
        /// event to handle movement of the pop-up dialog when making a new outcome. 
        /// This enables the screen refresh when dialog is moved.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void explorerControl_PopupDialogMoved(object sender, PackageItemEventArgs e)
        {
            if (uiToolbarManager_.ActiveEzMdiChild is TemplateDesigner)
            {
                uiToolbarManager_.ActiveEzMdiChild.Refresh();
            }
        }

        private void newPackageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewPackage();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Save())
            {
                DoOfflinePublish();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SaveAs())
            {
                pluginForSave = null;
                DoOfflinePublish();
            }
        }

        private void publishToProcessManagersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoPublish();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Close this form (The recommended function call which will cause the closing event
            // on this form.
            this.Close();
        }

        private void MainDesigner2_Closing(object sender, CancelEventArgs e)
        {
            if (!IsOkayToUnloadPackage())
                e.Cancel = true;
            else
            {
                UnloadPackage();

                // Copied from the disposing function...
                if (m_plugins != null)
                {
                    foreach (IStudioPlugin p in m_plugins)
                    {
                        try
                        { p.Shutdown(); }
                        // Intentional 
                        catch { }
                    }
                }

                CleanUpOdma();
                mruList.SaveList();
                //Properties.Settings.Default.Studio_Width = this.Size.Width;
                //Properties.Settings.Default.Studio_Height = this.Size.Height;
                //Properties.Settings.Default.Save();

            }
        }

        private void CleanUpOdma()
        {
            OdmaCloseCurrent();

            if (IsOdmaAvailable())
            {
                OdmaImports.Unregister_DMS();
            }
        }

        private void loadQuestionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel3.Visible = true;
            bulkAdd1.Visible = true;
            importItems1.Visible = true;
            sharedLibraryControl.Visible = false;
        }

        private void sharedLibraryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!sharedLibraryControl.Visible)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;
                    string sharedLibraryPaneLoadedMessage = string.Empty;
                    bool activated = sharedLibraryControl.LoadLibrary(out sharedLibraryPaneLoadedMessage);

                    if (activated)
                    {
                        panel3.Visible = true;
                        sharedLibraryControl.Visible = true;
                        bulkAdd1.Visible = false;
                        importItems1.Visible = false;
                    }
                    else
                    {
                        sharedLibraryControl.Visible = false;
                        panel3.Visible = false;
                    }
                }
                catch
                {
                    sharedLibraryControl.Visible = false;
                    panel3.Visible = false;
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
        }

        private void clausesReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoShowClauseReport();
        }

        private void DoShowClauseReport()
        {
            ReportViewer designerToOpen = new ReportViewer(ReportViewer.ReportType.AllClauses);
            designerToOpen.MdiParent = this;
            designerToOpen.Show();
            designerToOpen.Navigate();
            designerToOpen.PropertyChanged += new PropertyChangedEventHandler(templateDesignerForm_PropertyChanged);
            designerToOpen.ItemSelected += new EventHandler<PackageItemEventArgs>(designerToOpen_ItemSelected);
        }

        private void testPackageIntegrityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MaybeInvokePackageFixer(false);
            MaybeFixWord2013ContentControls();

            explorerControl.ResetTreeWithCollapse();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Perfectus.Client.Configuration.Handler.ShowOptionForm();
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //PF-3181 change help file to help website
            //DoOpenHelp();
            System.Diagnostics.Process.Start(ConfigurationSettings.AppSettings["HelpURL"]);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (About aboutForm = new About(package.VersionXml))
            {
                aboutForm.ShowDialog();
            }
        }

        private void oDMAStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        // supports 4 mru items only
        private void toolStripMenuItemMRU1_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripDropDownItem)
            {
                string filename = ((ToolStripDropDownItem)sender).Tag.ToString();
                Open(filename);
            }
        }

        private void toolStripMenuItemMRU2_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripDropDownItem)
            {
                string filename = ((ToolStripDropDownItem)sender).Tag.ToString();
                Open(filename);
            }
        }

        private void toolStripMenuItemMRU3_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripDropDownItem)
            {
                string filename = ((ToolStripDropDownItem)sender).Tag.ToString();
                Open(filename);
            }
        }

        private void toolStripMenuItemMRU4_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripDropDownItem)
            {
                string filename = ((ToolStripDropDownItem)sender).Tag.ToString();
                Open(filename);
            }
        }

        private void sharedLibraryControl_SharedLibraryItemAdded(object o, SharedLibraryItemAddedEventArgs e)
        {
            explorerControl.UpdateSelectedLibraryItemNode(e.AddedLibraryItem);
        }

        public static class PerfectusColour
        {
            public static Color DarkBlue = Color.FromArgb(0, 82, 130);
            public static Color MediumBlue = Color.FromArgb(0, 133, 207);
            public static Color HighlightBlue = Color.FromArgb(108, 204, 255);
            public static Color LightBlue = Color.FromArgb(188, 239, 255);
            public static Color DarkGreen = Color.FromArgb(0, 130, 113);
            public static Color MediumGreen = Color.FromArgb(1, 201, 177);
            public static Color DarkGrey = Color.FromArgb(43, 43, 46);
            public static Color MediumGray = Color.FromArgb(85, 85, 90);
            public static Color LightGray = Color.FromArgb(248, 248, 248);
        }

        public class MdiChildClass
        {
            private List<Form> _EzMdiChildren;

            private Form _ActiveEzMdiChild;
            public Form ActiveEzMdiChild
            {
                get { return _ActiveEzMdiChild; }
            }
            public Form[] EzMdiChildren
            {
                get { return _EzMdiChildren.ToArray(); }
            }
            public void Add(Form __ActiveMdiChild)
            {
                _EzMdiChildren.Add(__ActiveMdiChild);
            }
            public void SelectAndActive(Form _form)
            {
                SetActiveMDIChild(_form);
                _form.Select();
            }
            public void SetActiveMDIChild(Form __ActiveMdiChild)
            {
                _ActiveEzMdiChild = __ActiveMdiChild;
            }
            public void RemoveForm(Object sender)
            {
                _EzMdiChildren.Remove(sender as Form);
            }

            public MdiChildClass()
            {
                _EzMdiChildren = new List<Form>();
            }
            //

            //public bool EzMdiContainer { get; set; }
            //public Form ActiveEzMdiChild { get; }
            //public Form[] EzMdiChildren { get; }
            //public bool EzMdiContainer { get; set; }
            //[Description("Occurs when a multiple document interface (MDI) child form is activated or closed within an EzMdi application.")]
            //public event EventHandler EzMdiChildActivate;
            //[Description("Occurs when a multiple document interface (MDI) child form is activated or closed within an EzMdi application.")]
            //public event EventHandler EzMdiChildActivate;
        }

        private void llPanel3Close_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
        }

        private void splitter1_SplitterMoving(object sender, SplitterEventArgs e)
        {
            labelExplorerHeader.Width = panel1.Width;
        }

        private void splitter2_SplitterMoving(object sender, SplitterEventArgs e)
        {
            labelPropertiesHeader.Width = panel2.Width;
        }

        private void fileToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            fileToolStripMenuItem.ForeColor = Color.Black;
        }

        private void fileToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            fileToolStripMenuItem.ForeColor = Color.White;
        }

        private void toolsToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            toolsToolStripMenuItem.ForeColor = Color.Black;
            
            //find the MDI window items and set the styles, just set all the items.
            ToolStripSeparator sep = null;

            foreach (object obj in toolsToolStripMenuItem.DropDownItems)
            {
                if (obj.GetType() == typeof(ToolStripMenuItem))
                {
                    ToolStripMenuItem tsi = (ToolStripMenuItem)obj;
                    tsi.BackColor = Color.Black;
                    tsi.ForeColor = Color.White;
                }
                if (obj.GetType() == typeof(ToolStripSeparator))
                {
                    sep = (ToolStripSeparator)obj;
                }
            }
            if (sep != null)
                toolsToolStripMenuItem.DropDownItems.Remove(sep);
        }

        private void toolsToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            toolsToolStripMenuItem.ForeColor = Color.White;
        }

        private void helpToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {
            helpToolStripMenuItem.ForeColor = Color.Black;
        }

        private void helpToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {
            helpToolStripMenuItem.ForeColor = Color.White;
        }

        // TODO highlight the treeview backcolor green, fore white
        // ref: http://stackoverflow.com/questions/21198668/treenode-selected-backcolor-while-treeview-not-focused
        // can't be done for propertyGrid, so don't bother.
        //
        //public TreeNode previousSelectedNode = null;
        //private void treeView1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    treeView1.SelectedNode.BackColor = SystemColors.Highlight;
        //    treeView1.SelectedNode.ForeColor = Color.White;
        //    previousSelectedNode = treeView1.SelectedNode;
        //}

        //private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        //{
        //    if (previousSelectedNode != null)
        //    {
        //        previousSelectedNode.BackColor = treeView1.BackColor;
        //        previousSelectedNode.ForeColor = treeView1.ForeColor;
        //    }
        //}
    }
}
