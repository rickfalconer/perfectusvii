using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Assembly specific information
[assembly : AssemblyTitle("Perfectus IPManager")]

// Assembly Misc settings
[assembly: ComVisible( false )]
[assembly: CLSCompliant( true )]
[assembly: AssemblyCulture( "" )]
[assembly: Guid( "3d5900ae-111a-45be-96b3-d9e4606ca793" )]

// Other general Assembly information will be handled by GlobalAssemblyInfo.cs