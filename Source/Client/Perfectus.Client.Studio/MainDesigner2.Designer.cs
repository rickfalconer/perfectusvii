﻿namespace Perfectus.Client.Studio
{
    partial class MainDesigner2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainDesigner2));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btNew = new System.Windows.Forms.ToolStripButton();
            this.btOpen = new System.Windows.Forms.ToolStripButton();
            this.btSave = new System.Windows.Forms.ToolStripButton();
            this.btInterviewDiagram = new System.Windows.Forms.ToolStripButton();
            this.btPublish2 = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newPackageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.publishToProcessManagersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMRU1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMRU2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMRU3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMRU4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadQuestionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sharedLibraryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clausesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testPackageIntegrityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oDMAStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelExplorerHeader = new System.Windows.Forms.Label();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.labelPropertiesHeader = new System.Windows.Forms.Label();
            this.splashTimer = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.explorerControl = new Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControl();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel3 = new System.Windows.Forms.Panel();
            this.llPanel3Close = new System.Windows.Forms.LinkLabel();
            this.importItems1 = new Perfectus.Client.Studio.UI.Tasks.ImportItems();
            this.bulkAdd1 = new Perfectus.Client.Studio.UI.Tasks.BulkAdd();
            this.sharedLibraryControl = new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryControl();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabForms2 = new Perfectus.Client.Studio.Ui.PackageExplorer.TabControlWithDelete2();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.toolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolStrip1.CanOverflow = false;
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(47, 45);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNew,
            this.btOpen,
            this.btSave,
            this.btInterviewDiagram,
            this.btPublish2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 39);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1249, 52);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "Perfectus";
            // 
            // btNew
            // 
            this.btNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNew.Image = ((System.Drawing.Image)(resources.GetObject("btNew.Image")));
            this.btNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNew.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(51, 49);
            this.btNew.Text = "New Package";
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // btOpen
            // 
            this.btOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btOpen.Image = ((System.Drawing.Image)(resources.GetObject("btOpen.Image")));
            this.btOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btOpen.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.btOpen.Name = "btOpen";
            this.btOpen.Size = new System.Drawing.Size(51, 49);
            this.btOpen.Text = "Open";
            this.btOpen.Click += new System.EventHandler(this.btOpen_Click);
            // 
            // btSave
            // 
            this.btSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btSave.Image = ((System.Drawing.Image)(resources.GetObject("btSave.Image")));
            this.btSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSave.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(51, 49);
            this.btSave.Text = "Save";
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btInterviewDiagram
            // 
            this.btInterviewDiagram.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btInterviewDiagram.Image = ((System.Drawing.Image)(resources.GetObject("btInterviewDiagram.Image")));
            this.btInterviewDiagram.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btInterviewDiagram.Name = "btInterviewDiagram";
            this.btInterviewDiagram.Size = new System.Drawing.Size(51, 49);
            this.btInterviewDiagram.Text = "Show flow diagram";
            this.btInterviewDiagram.Click += new System.EventHandler(this.btInterviewDiagram_Click);
            // 
            // btPublish2
            // 
            this.btPublish2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btPublish2.Image = ((System.Drawing.Image)(resources.GetObject("btPublish2.Image")));
            this.btPublish2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btPublish2.Name = "btPublish2";
            this.btPublish2.Size = new System.Drawing.Size(51, 49);
            this.btPublish2.Text = "Pu&blish to ProcessManagers...";
            this.btPublish2.Click += new System.EventHandler(this.btPublish2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.MdiWindowListItem = this.toolsToolStripMenuItem;
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1249, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newPackageToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.publishToProcessManagersToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.toolStripMenuItemMRU1,
            this.toolStripMenuItemMRU2,
            this.toolStripMenuItemMRU3,
            this.toolStripMenuItemMRU4});
            this.fileToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            this.fileToolStripMenuItem.DropDownClosed += new System.EventHandler(this.fileToolStripMenuItem_DropDownClosed);
            this.fileToolStripMenuItem.DropDownOpened += new System.EventHandler(this.fileToolStripMenuItem_DropDownOpened);
            // 
            // newPackageToolStripMenuItem
            // 
            this.newPackageToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.newPackageToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.newPackageToolStripMenuItem.Name = "newPackageToolStripMenuItem";
            this.newPackageToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.newPackageToolStripMenuItem.Text = "New Package";
            this.newPackageToolStripMenuItem.Click += new System.EventHandler(this.newPackageToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.openToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.saveToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.saveAsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // publishToProcessManagersToolStripMenuItem
            // 
            this.publishToProcessManagersToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.publishToProcessManagersToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.publishToProcessManagersToolStripMenuItem.Name = "publishToProcessManagersToolStripMenuItem";
            this.publishToProcessManagersToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.publishToProcessManagersToolStripMenuItem.Text = "Pu&blish to ProcessManagers";
            this.publishToProcessManagersToolStripMenuItem.Click += new System.EventHandler(this.publishToProcessManagersToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(218, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripMenuItemMRU1
            // 
            this.toolStripMenuItemMRU1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.toolStripMenuItemMRU1.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemMRU1.Name = "toolStripMenuItemMRU1";
            this.toolStripMenuItemMRU1.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemMRU1.Text = "1";
            this.toolStripMenuItemMRU1.Click += new System.EventHandler(this.toolStripMenuItemMRU1_Click);
            // 
            // toolStripMenuItemMRU2
            // 
            this.toolStripMenuItemMRU2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.toolStripMenuItemMRU2.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemMRU2.Name = "toolStripMenuItemMRU2";
            this.toolStripMenuItemMRU2.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemMRU2.Text = "2";
            this.toolStripMenuItemMRU2.Click += new System.EventHandler(this.toolStripMenuItemMRU2_Click);
            // 
            // toolStripMenuItemMRU3
            // 
            this.toolStripMenuItemMRU3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.toolStripMenuItemMRU3.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemMRU3.Name = "toolStripMenuItemMRU3";
            this.toolStripMenuItemMRU3.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemMRU3.Text = "3";
            this.toolStripMenuItemMRU3.Click += new System.EventHandler(this.toolStripMenuItemMRU3_Click);
            // 
            // toolStripMenuItemMRU4
            // 
            this.toolStripMenuItemMRU4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.toolStripMenuItemMRU4.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemMRU4.Name = "toolStripMenuItemMRU4";
            this.toolStripMenuItemMRU4.Size = new System.Drawing.Size(218, 22);
            this.toolStripMenuItemMRU4.Text = "4";
            this.toolStripMenuItemMRU4.Click += new System.EventHandler(this.toolStripMenuItemMRU4_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadQuestionsToolStripMenuItem,
            this.sharedLibraryToolStripMenuItem,
            this.clausesReportToolStripMenuItem,
            this.testPackageIntegrityToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            this.toolsToolStripMenuItem.DropDownClosed += new System.EventHandler(this.toolsToolStripMenuItem_DropDownClosed);
            this.toolsToolStripMenuItem.DropDownOpened += new System.EventHandler(this.toolsToolStripMenuItem_DropDownOpened);
            // 
            // loadQuestionsToolStripMenuItem
            // 
            this.loadQuestionsToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.loadQuestionsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.loadQuestionsToolStripMenuItem.Name = "loadQuestionsToolStripMenuItem";
            this.loadQuestionsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.loadQuestionsToolStripMenuItem.Text = "Load Questions";
            this.loadQuestionsToolStripMenuItem.Click += new System.EventHandler(this.loadQuestionsToolStripMenuItem_Click);
            // 
            // sharedLibraryToolStripMenuItem
            // 
            this.sharedLibraryToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.sharedLibraryToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.sharedLibraryToolStripMenuItem.Name = "sharedLibraryToolStripMenuItem";
            this.sharedLibraryToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.sharedLibraryToolStripMenuItem.Text = "Shared Library";
            this.sharedLibraryToolStripMenuItem.Click += new System.EventHandler(this.sharedLibraryToolStripMenuItem_Click);
            // 
            // clausesReportToolStripMenuItem
            // 
            this.clausesReportToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.clausesReportToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.clausesReportToolStripMenuItem.Name = "clausesReportToolStripMenuItem";
            this.clausesReportToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.clausesReportToolStripMenuItem.Text = "Clauses Report";
            this.clausesReportToolStripMenuItem.Click += new System.EventHandler(this.clausesReportToolStripMenuItem_Click);
            // 
            // testPackageIntegrityToolStripMenuItem
            // 
            this.testPackageIntegrityToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.testPackageIntegrityToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.testPackageIntegrityToolStripMenuItem.Name = "testPackageIntegrityToolStripMenuItem";
            this.testPackageIntegrityToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.testPackageIntegrityToolStripMenuItem.Text = "Test Package Integrity";
            this.testPackageIntegrityToolStripMenuItem.Click += new System.EventHandler(this.testPackageIntegrityToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.optionsToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.optionsToolStripMenuItem.Text = "Options...";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.aboutToolStripMenuItem,
            this.oDMAStatusToolStripMenuItem});
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.DropDownClosed += new System.EventHandler(this.helpToolStripMenuItem_DropDownClosed);
            this.helpToolStripMenuItem.DropDownOpened += new System.EventHandler(this.helpToolStripMenuItem_DropDownOpened);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.helpToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(143, 22);
            this.helpToolStripMenuItem1.Text = "&Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // oDMAStatusToolStripMenuItem
            // 
            this.oDMAStatusToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.oDMAStatusToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.oDMAStatusToolStripMenuItem.Name = "oDMAStatusToolStripMenuItem";
            this.oDMAStatusToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.oDMAStatusToolStripMenuItem.Text = "ODMA Status";
            this.oDMAStatusToolStripMenuItem.Visible = false;
            this.oDMAStatusToolStripMenuItem.Click += new System.EventHandler(this.oDMAStatusToolStripMenuItem_Click);
            // 
            // labelExplorerHeader
            // 
            this.labelExplorerHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelExplorerHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(130)))));
            this.labelExplorerHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelExplorerHeader.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.labelExplorerHeader.ForeColor = System.Drawing.Color.White;
            this.labelExplorerHeader.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelExplorerHeader.Location = new System.Drawing.Point(0, 0);
            this.labelExplorerHeader.Margin = new System.Windows.Forms.Padding(0);
            this.labelExplorerHeader.Name = "labelExplorerHeader";
            this.labelExplorerHeader.Padding = new System.Windows.Forms.Padding(3, 4, 0, 6);
            this.labelExplorerHeader.Size = new System.Drawing.Size(624, 35);
            this.labelExplorerHeader.TabIndex = 0;
            this.labelExplorerHeader.Text = "PACKAGE                                                                          " +
    "                                            ";
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid1.BackColor = System.Drawing.Color.LightGray;
            this.propertyGrid1.CategoryForeColor = System.Drawing.Color.DarkGray;
            this.propertyGrid1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.propertyGrid1.HelpBackColor = System.Drawing.Color.LightGray;
            this.propertyGrid1.LineColor = System.Drawing.Color.DarkGray;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 34);
            this.propertyGrid1.Margin = new System.Windows.Forms.Padding(0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(315, 309);
            this.propertyGrid1.TabIndex = 3;
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.ViewBackColor = System.Drawing.Color.LightGray;
            this.propertyGrid1.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid1_PropertyValueChanged);
            this.propertyGrid1.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propertyGrid1_SelectedGridItemChanged);
            // 
            // labelPropertiesHeader
            // 
            this.labelPropertiesHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPropertiesHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(133)))), ((int)(((byte)(207)))));
            this.labelPropertiesHeader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelPropertiesHeader.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.labelPropertiesHeader.ForeColor = System.Drawing.Color.White;
            this.labelPropertiesHeader.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelPropertiesHeader.Location = new System.Drawing.Point(0, 0);
            this.labelPropertiesHeader.Margin = new System.Windows.Forms.Padding(0);
            this.labelPropertiesHeader.Name = "labelPropertiesHeader";
            this.labelPropertiesHeader.Padding = new System.Windows.Forms.Padding(3, 4, 0, 6);
            this.labelPropertiesHeader.Size = new System.Drawing.Size(624, 34);
            this.labelPropertiesHeader.TabIndex = 1;
            this.labelPropertiesHeader.Text = "PROPERTIES                                                                       " +
    "                                                   ";
            // 
            // splashTimer
            // 
            this.splashTimer.Interval = 1000;
            this.splashTimer.Tick += new System.EventHandler(this.splashTimer_Tick);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "ip";
            this.saveFileDialog1.Filter = "Package files (*.ip)|*.ip;|All files (*.*)|*.*";
            this.saveFileDialog1.Title = "Save Package";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.ip";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Package files|*.ip;|All files|*.*";
            this.openFileDialog1.Title = "Open Package";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.explorerControl);
            this.panel1.Controls.Add(this.labelExplorerHeader);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 91);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(315, 343);
            this.panel1.TabIndex = 4;
            // 
            // explorerControl
            // 
            this.explorerControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.explorerControl.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.explorerControl.Location = new System.Drawing.Point(0, 35);
            this.explorerControl.Margin = new System.Windows.Forms.Padding(0);
            this.explorerControl.Name = "explorerControl";
            this.explorerControl.Package = null;
            this.explorerControl.Plugins = null;
            this.explorerControl.PropertyGrid = null;
            this.explorerControl.Size = new System.Drawing.Size(315, 308);
            this.explorerControl.TabIndex = 2;
            this.explorerControl.OpenDesigner += new Perfectus.Common.PackageObjects.PackageItemOpenDesignerEventHandler(this.packageExplorer_OpenDesigner);
            this.explorerControl.Publish += new System.EventHandler(this.explorerControl_Publish);
            this.explorerControl.SimpleOutcomeDrop += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>(this.explorerControl_SimpleOutcomeDrop);
            this.explorerControl.OutcomeDrop += new System.EventHandler<Perfectus.Common.PackageObjects.PackageItemEventArgs>(this.explorerControl_OutcomeDrop);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.White;
            this.splitter1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.splitter1.Location = new System.Drawing.Point(315, 91);
            this.splitter1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 343);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelPropertiesHeader);
            this.panel2.Controls.Add(this.propertyGrid1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(318, 91);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(315, 343);
            this.panel2.TabIndex = 6;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.LightGray;
            this.splitter2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.splitter2.Location = new System.Drawing.Point(633, 91);
            this.splitter2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 343);
            this.splitter2.TabIndex = 7;
            this.splitter2.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.llPanel3Close);
            this.panel3.Controls.Add(this.importItems1);
            this.panel3.Controls.Add(this.bulkAdd1);
            this.panel3.Controls.Add(this.sharedLibraryControl);
            this.panel3.Location = new System.Drawing.Point(1057, 126);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(192, 308);
            this.panel3.TabIndex = 9;
            // 
            // llPanel3Close
            // 
            this.llPanel3Close.AutoSize = true;
            this.llPanel3Close.BackColor = System.Drawing.Color.LightGray;
            this.llPanel3Close.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.llPanel3Close.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.llPanel3Close.Location = new System.Drawing.Point(147, 7);
            this.llPanel3Close.Name = "llPanel3Close";
            this.llPanel3Close.Size = new System.Drawing.Size(35, 13);
            this.llPanel3Close.TabIndex = 3;
            this.llPanel3Close.TabStop = true;
            this.llPanel3Close.Tag = "";
            this.llPanel3Close.Text = "Close";
            this.llPanel3Close.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.llPanel3Close.Click += new System.EventHandler(this.llPanel3Close_Click);
            // 
            // importItems1
            // 
            this.importItems1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.importItems1.BackColor = System.Drawing.Color.LightGray;
            this.importItems1.CurrentPackage = null;
            this.importItems1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.importItems1.Location = new System.Drawing.Point(0, 0);
            this.importItems1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.importItems1.Name = "importItems1";
            this.importItems1.Size = new System.Drawing.Size(192, 72);
            this.importItems1.TabIndex = 1;
            // 
            // bulkAdd1
            // 
            this.bulkAdd1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bulkAdd1.BackColor = System.Drawing.Color.LightGray;
            this.bulkAdd1.CurrentPackage = null;
            this.bulkAdd1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.bulkAdd1.Location = new System.Drawing.Point(0, 72);
            this.bulkAdd1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bulkAdd1.Name = "bulkAdd1";
            this.bulkAdd1.Size = new System.Drawing.Size(192, 236);
            this.bulkAdd1.TabIndex = 0;
            // 
            // sharedLibraryControl
            // 
            this.sharedLibraryControl.BackColor = System.Drawing.Color.White;
            this.sharedLibraryControl.Location = new System.Drawing.Point(0, 0);
            this.sharedLibraryControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sharedLibraryControl.Name = "sharedLibraryControl";
            this.sharedLibraryControl.Size = new System.Drawing.Size(328, 584);
            this.sharedLibraryControl.TabIndex = 2;
            this.sharedLibraryControl.SharedLibraryItemAdded += new Perfectus.Client.Studio.UI.SharedLibrary.SharedLibraryItemAddedHandler(this.sharedLibraryControl_SharedLibraryItemAdded);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 24);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1249, 15);
            this.panel4.TabIndex = 13;
            // 
            // tabForms2
            // 
            this.tabForms2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(85)))), ((int)(((byte)(90)))));
            this.tabForms2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabForms2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabForms2.ForeColor = System.Drawing.Color.White;
            this.tabForms2.Location = new System.Drawing.Point(636, 91);
            this.tabForms2.Margin = new System.Windows.Forms.Padding(0);
            this.tabForms2.Name = "tabForms2";
            this.tabForms2.Size = new System.Drawing.Size(613, 35);
            this.tabForms2.TabIndex = 11;
            this.tabForms2.tabTryClose += new Perfectus.Client.Studio.Ui.PackageExplorer.TabControlWithDelete2.TabControlWithDelete2EventHandler(this.tabForms2_tabTryClose);
            this.tabForms2.selectedIndexChanged += new Perfectus.Client.Studio.Ui.PackageExplorer.TabControlWithDelete2.TabControlWithDelete2SelectedItemEventHandler(this.tabForms2_selectedIndexChanged);
            // 
            // MainDesigner2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(46)))));
            this.ClientSize = new System.Drawing.Size(1249, 434);
            this.Controls.Add(this.tabForms2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainDesigner2";
            this.Text = "Perfectus IP Manager";
            this.MdiChildActivate += new System.EventHandler(this.MainDesigner2_MdiChildActivate);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btNew;
        private System.Windows.Forms.ToolStripButton btOpen;
        private System.Windows.Forms.ToolStripButton btSave;
        private System.Windows.Forms.ToolStripButton btInterviewDiagram;
        private System.Windows.Forms.ToolStripButton btPublish2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label labelExplorerHeader;
        private System.Windows.Forms.Label labelPropertiesHeader;
        private UI.PackageExplorer.ExplorerControl explorerControl;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.Timer splashTimer;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem newPackageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem publishToProcessManagersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadQuestionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sharedLibraryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clausesReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testPackageIntegrityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oDMAStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMRU1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMRU2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMRU3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMRU4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel3;
        private UI.Tasks.ImportItems importItems1;
        private UI.Tasks.BulkAdd bulkAdd1;
        private UI.SharedLibrary.SharedLibraryControl sharedLibraryControl;
        private System.Windows.Forms.LinkLabel llPanel3Close;
        private Ui.PackageExplorer.TabControlWithDelete2 tabForms2;
        private System.Windows.Forms.Panel panel4;
    }
}