using System;
using System.ComponentModel;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.ExpressionLibrary;
using Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter;
using Perfectus.Client.Studio.QueryEditControl;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls;
using Perfectus.Client.SDK;
using Perfectus.Client.Studio.ExpressionLibrary.Converter;
using System.Drawing;
using System.Collections.Generic;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
    /// <summary>
	/// Summary description for QueryDesigner.
	/// </summary>
    public class Query2DesignerForm : Form, IQueryDesigner
	{
        private bool okayToClose = true;
		private Button btnOk;
        private Button btnCancel;
		private Query queryBeingEdited;
        private PackageItem itemBeingEdited;
        private QueryEditControl.QueryEditUserControl codeCtrl;
		private QueryDesignerMode mode;
        private Boolean needUglyFixForFrozenMenus;

        private IPrintFormatter m_prettyPrinter;

		public Query QueryBeingEdited
		{
            get { return queryBeingEdited; }
			set {
				queryBeingEdited = value;

                if (queryBeingEdited != null)
                {
                    // Convert from Query to CQuery
                    Creator cvrt = new Creator(value.ParentPackage);
                    ExpressionHelper.Reset();
                    CQuery query = cvrt.Create(value);
                    codeCtrl.SetQuery(value.ParentPackage, query);
                }
                else
                {
                    throw new Exception("When does that happen");
                }
			}
		}

        public PackageItem ItemBeingEdited
        {
            get { return itemBeingEdited; }
            set
            {
                itemBeingEdited = value;
                if ( itemBeingEdited is InterviewPage )
                    Interview = ((InterviewPage)itemBeingEdited).ParentInterview;
                this.Text += value.Name;
            }
        }

        public Package ParentPackage
        {
            get
            {
                if (ItemBeingEdited == null) return QueryBeingEdited.ParentPackage;
                else return ItemBeingEdited.ParentPackage;
            }
        }


        Interview interview;
        private Interview Interview
        {
            get { return interview; }
            set { interview= value; }
        }

        public QueryDesignerMode Mode
        {
            get { return mode; }
            set
            {
                mode = value;

                if (mode == QueryDesignerMode.Outcome)
                    m_prettyPrinter =
                        new Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter.
                    CPrintOutcomeFormatter();
                else
                    if (mode == QueryDesignerMode.YesNo)
                        m_prettyPrinter =
                            new Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter.
                    CPrintYesNoFormatter();
                    else
                        m_prettyPrinter =
                        new Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter.
                    CPrintNavigationFormatter();

                codeCtrl.SetQueryFormatterAndMode(m_prettyPrinter, value);
            }
        }

        public void AddPlugin(IStudioPlugin param_plugin)
        {
            this.codeCtrl.AddPlugin(param_plugin);
        }

        /// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public Query2DesignerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

            needUglyFixForFrozenMenus = false;

            // WordText, Integration and other elements must be handled ouside the 
            // query editor. They have delegates to hook into...
            this.codeCtrl.actionClick += new QueryEditUserControl.ActionClick(codeCtrl_actionClick);
            
            // e.g. a DM5 integration
            this.codeCtrl.integrationClick += new QueryEditUserControl.ActionClick(codeCtrl_integrationClick);

            this.codeCtrl.navigationPageClick += new QueryEditUserControl.ActionClick(codeCtrl_navigationPageClick);
		}
        void codeCtrl_navigationPageClick(CExpressionBase sender)
        {
            if (!(sender is CNavigationPage))
                return;
            CNavigationPage action = sender as CNavigationPage;

            InterviewPageSelection frm = new InterviewPageSelection();

            List<InterviewPage> pages = new List<InterviewPage>(Interview.Pages.Count);

            foreach (InterviewPage page in Interview.Pages)
                pages.Add( page );
            pages.Sort(new ComparePageNames());

            foreach (InterviewPage page in pages)
            {
                if (page == ItemBeingEdited)
                    continue;
                int idx = 0;
                if (page == Interview.StartPage)
                    idx = 1;
                else
                if (page is ExternalPage)
                    idx = 2;
                
                frm.AddPage(page.Name, page.Notes, page, idx);
            }
            if (DialogResult.OK == frm.ShowDialog())
            {
                action.Payload = frm.SelectedPage();
                if (action.Payload != null)
                    action.Name = ((InterviewPage)action.Payload).Name;
            }

        }

        /// <summary>
        /// Open the Word outcome editor
        /// </summary>
        /// <param name="sender"></param>
        void codeCtrl_actionClick(CExpressionBase sender)
        {
            if (!(sender is CWordText))
                return;
            CWordText action = sender as CWordText;

            String pendingOfficeOpenXML = System.String.Empty;

            if (action.Payload != null && (action.Payload as String).Length > 0)
                pendingOfficeOpenXML = action.Payload.ToString();

            OutcomeWordDialog dlg = new OutcomeWordDialog(queryBeingEdited.ParentPackage, ItemBeingEdited, action.Name);
            if (pendingOfficeOpenXML != null)
                dlg.OfficeOpenXML = pendingOfficeOpenXML;

            DialogResult dr = dlg.ShowDialog();
            if (dr == DialogResult.OK)
            {
                action.Payload = dlg.OfficeOpenXML;
                action.Name = dlg.OutcomeName;
            }
            needUglyFixForFrozenMenus = true;
            FindForm().Activate();
        }

        /// <summary>
        /// Invoke the integration callback
        /// </summary>
        /// <param name="sender"></param>
        void codeCtrl_integrationClick(CExpressionBase sender)
        {
            if (!(sender is CIntegration))
                return;

            CIntegration integration = sender as CIntegration;

            IClauseLibrary integrationObject = (IClauseLibrary)integration.ClauseLibrary;

            System.Reflection.MethodInfo method = integrationObject.GetType().GetMethod("SetTarget");
            if (method != null && integration.UniqueLibraryKey != null && integration.UniqueLibraryKey.Length > 0)
            {
                String startUrl = integration.UniqueLibraryKey;
                if (startUrl.EndsWith(integration.DisplayName))
                    startUrl = startUrl.Substring(0, startUrl.Length - integration.DisplayName.Length - 1);
                method.Invoke(integrationObject, new object[] { startUrl });
            }
            string clauseId = String.Empty;
            string displayName = String.Empty;
            string libraryName = String.Empty;
            string libraryUniqueIdentifier = String.Empty;

            if (integrationObject.PickClause(out clauseId, out displayName, out libraryName, out libraryUniqueIdentifier))
            {
                integration.FetcherId = integrationObject.ServerSideFetcherId;
                integration.ClauseRef = clauseId;
                integration.DisplayName = displayName;
                integration.LibraryName = libraryName;
                integration.UniqueLibraryKey = libraryUniqueIdentifier;
            }
        }
        
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Query2DesignerForm));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.codeCtrl = new Perfectus.Client.Studio.QueryEditControl.QueryEditUserControl();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // codeCtrl
            // 
            resources.ApplyResources(this.codeCtrl, "codeCtrl");
            this.codeCtrl.MinimumSize = new System.Drawing.Size(318, 174);
            this.codeCtrl.Name = "codeCtrl";
            // 
            // Query2DesignerForm
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.codeCtrl);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Query2DesignerForm";
            this.ShowInTaskbar = false;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EQueryDesignerForm_FormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.EQueryDesignerForm_Closing);
            this.Load += new System.EventHandler(this.EQueryDesignerForm_Load);
            this.ResumeLayout(false);

		}
        #endregion

		private void btnOk_Click(object sender, EventArgs e)
		{
			SaveSelf();
		}

        private void SaveSelf()
        {
            try
            {
                CQuery query = codeCtrl.GetQuery();
                CConverter converter = new CConverter(ParentPackage);

                // This will invoke the internal evaluation
                queryBeingEdited = converter.Convert(query);
                okayToClose = true;
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                msg = msg.Replace("\\n", "\n");
                msg = msg.Replace("\\t", "\t");
                MessageBox.Show(msg, Common.About.FormsTitle);
                okayToClose = false;
            }
        }

		private void EQueryDesignerForm_Closing(object sender, CancelEventArgs e)
		{
			if (!okayToClose )
			{
				e.Cancel = true;
			}
			else
			{
				e.Cancel = false;
                codeCtrl.set_lastCodeWindowSize(Size,Location);
			}
		}

        private System.ComponentModel.ComponentResourceManager resources()
        {
            return new System.ComponentModel.ComponentResourceManager(typeof(Query2DesignerForm));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Query2DesignerForm));

            okayToClose = true;

            if (codeCtrl.haveUnicommitedChanged)
            {
                DialogResult result = MessageBox.Show(
                resources.GetString("ConfirmCancel"),
                resources.GetString("ConfirmCancelCaption"),
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                    btnOk.PerformClick();
                else
                    if (result == DialogResult.Cancel)
                        okayToClose = false;
            }
        }
        
		private void EQueryDesignerForm_Load(object sender, System.EventArgs e)
		{
            Point pt = new Point();
            this.Size = this.codeCtrl.get_lastCodeWindowSize(ref pt);
            if (pt.X >= 0)
                this.Location = pt;

			this.BringToFront();
			this.Focus();
		}

        void EQueryDesignerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // FB1081
            // This is an ugly work-around. Actually, minimizing and maximizing 
            // solved the issue...
            if (needUglyFixForFrozenMenus)
            {
                Form mainForm = this.Owner;

                if (mode == QueryDesignerMode.Outcome && null != mainForm)
                {
                    FormWindowState currState = mainForm.WindowState;
                    mainForm.WindowState = FormWindowState.Minimized;
                    mainForm.WindowState = currState;
                }
            }
        }
	}

    /// <summary>
    /// Case insensitive comparer for CItem instances
    /// </summary>
    public class ComparePageNames : IComparer<InterviewPage>
    {
        public int Compare(InterviewPage x, InterviewPage y)
        {
            return String.Compare(x.Name, y.Name, true);
        }
    }

    public enum QueryDesignerMode
    {
        Navigation,
        Outcome,
        YesNo,
        HTMLBinding
    } ;

    public interface IQueryDesigner
    {
        Perfectus.Common.PackageObjects.Query QueryBeingEdited { get;set; }
        Perfectus.Common.PackageObjects.PackageItem ItemBeingEdited { get;set; }
        QueryDesignerMode Mode { get; set;}
    }
}

