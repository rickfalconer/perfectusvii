using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using ExpressionLibrary;
using System.Drawing;
using PrettyPrinter;


namespace CodeEditControl
{
    public partial class CodeCtrl : RichTextBox
    {
        #region Private variables

        private CQuery _query;
        private IPrettyPrint _prettyPrinter;

        internal class CTextAttr
        {
            internal Color color = Color.Black;
            internal String name;
            internal
            CTextAttr(Color param_color) { color = param_color; }
            internal
            CTextAttr():this ( Color.Black ) { }
        }

        internal CTextAttr colCodeFore= new CTextAttr(Color.Black);
        internal CTextAttr colCodeBack = new CTextAttr(Color.White);
        internal CTextAttr colNumber = new CTextAttr(Color.IndianRed);

        CTextAttr colText = new CTextAttr(Color.DarkBlue);
        CTextAttr colDate = new CTextAttr(Color.DarkGoldenrod);
        CTextAttr colBool = new CTextAttr(Color.Lavender);
        CTextAttr colNone = new CTextAttr(Color.Gray);

        internal Dictionary<int, CTextAttr> attributes;


        
        System.Windows.Forms.Timer _textMarkingTimer = new Timer();
        System.Windows.Forms.Timer _mouseMoveTimer = new Timer();

        #endregion

        #region Constructor
        public
        CodeCtrl()
        {
            ReadOnly = true;
            
            this.AutoWordSelection = false;

            attributes = new Dictionary<int, CTextAttr>();
            _textMarkingTimer.Tick += new EventHandler(CodeEditBox_SelectionChanged_Tick);
            _textMarkingTimer.Interval = System.Windows.Forms.SystemInformation.MouseHoverTime;

            _mouseMoveTimer.Tick += new EventHandler(CodeEditBox_MouseHover_Tick);
            _mouseMoveTimer.Interval = System.Windows.Forms.SystemInformation.MouseHoverTime;

            this.MouseEnter += new System.EventHandler(CodeCtrl_MouseEnter);
            this.SelectionChanged += new System.EventHandler(CodeCtrl_SelectionChanged);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(CodeCtrl_MouseMove);
            this.MouseLeave += new System.EventHandler(CodeCtrl_MouseLeave);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(CodeCtrl_MouseDown);
        }

        #endregion

        #region Initialize query
        internal void SetQuery(IPrettyPrint param_prettyPrinter)
        {
            _prettyPrinter = param_prettyPrinter;

            _prettyPrinter.writeText += new WriteText(WriteText);
            _prettyPrinter.writeElement += new WriteElement(WriteElement);

            // Brand new query
            _query = CQuery.AsNewQuery();

            ReDraw();
        }

        internal void SetQuery(IPrettyPrint param_prettyPrinter, CQuery param_query)
        {
            if (param_query == null)
                throw new ExpressionLibraryException("Query must not be null", "CodeCtrl::SetQuery", "Software defect");

            if (param_prettyPrinter == null)
                throw new ExpressionLibraryException("Pretty-printer must not be null", "CodeCtrl::SetQuery", "Software defect");

            _prettyPrinter = param_prettyPrinter;
            _prettyPrinter.writeText += new WriteText(WriteText);
            _prettyPrinter.writeElement += new WriteElement(WriteElement);

            _query = param_query;
            _prettyPrinter = param_prettyPrinter;

            ReDraw();
        }

        public void ReDraw (){
            SuspendEvents();
            float curentZoom = ZoomFactor;
            Clear();
            _query.DisplayYourself(_prettyPrinter.Print);
            ResumeEvents();
            String msg = "";
            OnQueryRedraw(_query.IsValid(ref msg), msg);
            ZoomFactor = curentZoom;
        }

        internal CQuery GetQuery()
        {
            return _query;
        }

        #endregion
    }
}