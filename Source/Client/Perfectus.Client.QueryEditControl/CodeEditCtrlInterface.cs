using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ExpressionLibrary;

namespace CodeEditControl
{
    public partial class CodeCtrl 
    {
        #region public methods
        internal void WriteText(String param_text)
        {
            if (param_text.Length > 0)
            {
                SelectionColor = Color.Black; ;
                SelectedText = param_text;
            }
        }

        internal void WriteElement(string param_text, Guid param_Id, Guid param_parentId, AtomType param_type, Boolean param_valid)
        {
            SuspendEvents();

            ObjectInfoStruc param_info= new ObjectInfoStruc(param_text, param_Id, param_parentId, param_type, param_valid);

            Color color = Color.Empty;
            if (param_info.Type == AtomType.LVALUE ||
                param_info.Type == AtomType.RVALUE)
            {
                CValue value = (CValue)ExpressionHelper.Find(param_Id);
                if (value.AnswerProviderItem != null)
                    switch ( value.AnswerProviderItem.DataType )
                    {
                        case Perfectus.Common.PerfectusDataType.Number:
                            color = colNumber.color;
                            break;
                        case Perfectus.Common.PerfectusDataType.Text:
                            color = colText.color;
                            break;
                        case Perfectus.Common.PerfectusDataType.Date:
                            color = colDate.color;
                            break;
                        case Perfectus.Common.PerfectusDataType.YesNo:
                            color = colBool.color;
                            break                            ;
                    }
                else
                    color = colNone.color;
            }
            else
            {
                if (attributes.ContainsKey(param_info.Type.GetHashCode()))
                    color = attributes[param_info.Type.GetHashCode()].color;
            }

            AddHiddenText(PrefixText);

            if (!param_info.Valid)
            {
                SelectionUnderlineStyle = UnderlineStyle.Wave;
                SelectionUnderlineColor = UnderlineColor.Red;
            }
            SelectionColor = color;
            SelectedText = param_info.VisibleText();
            AddHiddenText(param_info.HiddenText());

            ResumeEvents();
        }


        private void AddHiddenText(String hiddenText)
        {
            String rtf = SelectedRtf;

            int i = rtf.LastIndexOf('}');
            rtf = rtf.Substring(0, i);
            rtf = rtf + @"{\rtf1\ansi{\v " + hiddenText + "}}";
            SelectedRtf = rtf;
        }


        internal CTextAttr GetOrCreateAttribute(AtomType param_type)
        {
            if (!attributes.ContainsKey(param_type.GetHashCode()))
                attributes.Add(param_type.GetHashCode(), new CTextAttr());
            return attributes[param_type.GetHashCode()];
        }
        internal void Register(AtomType param_type, Color param_color)
        {
            GetOrCreateAttribute(param_type).color = param_color;
            if (Text.Length > 0)
                ReDraw();
        }
        internal void UnRegister(AtomType param_type)
        {
            if (attributes.ContainsKey(param_type.GetHashCode()))
                attributes.Remove(param_type.GetHashCode());
        }

        /// <summary>
        /// Get the element that is highest in the tree
        /// Example: 
        /// * if a questtion and an operator is marked, then 
        /// the returned element is the condition
        /// * if a question is marked, then the question only is returned
        /// </summary>
        /// <returns></returns>
        internal Guid GetCurrentElementOrParentElement()
        {
            // 1. If there is only one element, then return it
            ObjectsInfoStruc objects = GetObjectsInRange(SelectionStart, SelectionStart + SelectionLength);
            int numOfObject = objects.GetObjects().Length;

            if (numOfObject == 0)
                return Guid.Empty;
            else
            if (numOfObject == 1)
            {
                Guid id = objects.GetObjects()[0].Id;
                // if the current element is only 'partly' marked, then return it 
                // otherwise return its parent.
                if (IsObjectCompletlyMarked(id))
                    return objects.GetObjects()[0].ParentId;
                else
                    return id;
            }

            Boolean success = true;
            Guid parentId = objects.GetObjects()[0].ParentId;

            // 2. If all objects have the same parent, then return the parent
            for (int i = 1; i < numOfObject && success; i++)
                success = parentId == objects.GetObjects()[i].ParentId;
            if (success)
            {
                // Example: Complete condition is marked, then return the parent run
                if (IsObjectCompletlyMarked(parentId))
                {
                    CAtom atom = (CAtom)ExpressionHelper.Find(parentId);
                    return atom.ParentId;
                }
                else
                    // Example: only one questiona and the operator is marked (but not the 
                    //          second question.
                    return parentId;
            }


            success = true;
            // 3. The user marked 'arbitary' code
            //    We want to reset the marking to something useful
            //    Therefore locate the first 'parent' that is not selected and use it.
            //    Forget any other objects that are not contained in this branch
            
            // Maybe the outermost element is marked. Then we return this element
            parentId = objects.GetObjects()[0].Id;

            for (int i = 0; i < numOfObject && success; i++)
            {
                // Try to find the parent of the element in the list of marked items
                bool found = false;
                for (int j = 0; j < numOfObject && success; j++)
                    if (  objects.GetObjects()[i].ParentId == 
                        objects.GetObjects()[j].Id )
                    {
                        found = true;
                        break;
                    }

                // We want this parent to be marked
                if ( ! found )
                {
                    // We might be on a CRun and the parent is a Clause. Depending on the pretty-printer, 
                    // this object might not be drawable. Hence check and maybe choose the next parent
                    CAtom a1 = ExpressionHelper.Find(parentId);
                    do
                    {
                        parentId = a1.ParentId;
                        a1 = ExpressionHelper.Find(parentId);

                        // Hmm..... we are at root level
                        if (a1 == null)
                            break;

                    }
                    while (Find(new ObjectInfoStruc(a1).HiddenText()) == -1);
                        break;
                }
            }
            return parentId;
        }

        internal Guid[] ExtendSelectionRight()
        {
            return ExtendSelection(false);
        }
        internal Guid[] ExtendSelectionLeft()
        {
            return ExtendSelection(true);
        }
        private Guid[] ExtendSelection(Boolean left)
        {
            ObjectsInfoStruc objects = GetObjectsInRange(SelectionStart, SelectionStart + SelectionLength);
            int numOfObject = objects.GetObjects().Length;
            if (numOfObject == 0)
                return new Guid[] { Guid.Empty };

            CAtom atom = ExpressionHelper.Find(objects.GetObjects()[0].Id);

            if (numOfObject == 1)
            {
                if (atom.type == AtomType.LVALUE ||
                 atom.type == AtomType.RVALUE ||
                  atom.type == AtomType.VALUE ||
                   atom.type == AtomType.OPERAND)

                    return new Guid[] { GetCurrentElementOrParentElement() };
            }

            // We assume that the objects are sorted from left to right
            // (otherwise this would be a big mistake)
            atom = ExpressionHelper.Find(objects.GetObjects()[left ? 0 : numOfObject-1].ParentId);

            // Find the left concatenation symbol using the left-most marked element
            CCondition condition = null;

            switch (atom.type)
            {
                case AtomType.LVALUE:
                case AtomType.RVALUE:
                case AtomType.VALUE:
                case AtomType.OPERAND:
                    // Parent of the above must be a condition
                    condition = (CCondition) ExpressionHelper.Find(atom.ParentId);
                    break;

                case AtomType.CONDITION:
                    condition = (CCondition)atom;
                    break;

                case AtomType.CONCATENATION:
                case AtomType.RUN:
                    // not supported
                    break;
            }

            List<Guid> retGuids = new List<Guid>();
            
            if (condition != null)
            {
                // Get the run...
                CRun parent = (CRun)ExpressionHelper.Find(condition.ParentId);
                // and let the run decide which expression is on the left of the 'and/or'
                CRunBase base1Run = left ? parent.LeftOf(condition) : parent.RightOf(condition); 
                if (base1Run != null)
                    retGuids.Add(base1Run.Id);
            }
            foreach (ObjectInfoStruc element in objects.GetObjects())
                retGuids.Add(element.Id);

            return retGuids.ToArray(); ;
        }




        private void GetElementPositionInText(Guid param_guid, ref int param_start, ref int param_end)
        {
            param_start = System.Int32.MaxValue;
            param_end = 0;
            String id_as_string = param_guid.ToString();

            // Find ( .. ) destroys the current selection, so we have to save it
            int saveSelectionStart = SelectionStart;
            int saveSelectionLength = SelectionLength;

            int start = 0, 
                end = 0, 
                visibleEnd = 0;
            
            while ((end = Find(id_as_string, end, RichTextBoxFinds.None)) != -1)
            {
                if (FindStringByPosition(end,
                    ref start,
                    ref end,
                    ref visibleEnd))
                {
                    param_start = Math.Min(param_start, start);
                    param_end = Math.Max(param_end, visibleEnd);
                }
                else end++;
            }
            SelectionStart= saveSelectionStart;
            SelectionLength = saveSelectionLength;
        }

        private Boolean IsObjectCompletlyMarked(Guid param_id)
        {
            Boolean result = false;
            int FirstElementCharacter = 0;
            int LastElementCharacter = 0;

            GetElementPositionInText(param_id, ref FirstElementCharacter, ref LastElementCharacter);

            result = FirstElementCharacter != -1 &&
                SelectionStart <= FirstElementCharacter &&
                    LastElementCharacter <= SelectionStart + SelectionLength;
            return result;
        }


        /// <summary>
        /// Get the element under the cursor
        /// </summary>
        /// <returns></returns>
        internal ObjectInfoStruc GetCurrentElement()
        {
            ObjectInfoStruc info = GetClickedObject(SelectionStart);
            return info;
        }
        public void MarkObject(Guid[] param_ids)
        {
            SuspendEvents();

            int oevrall_start = System.Int32.MaxValue;
            int oevrall_end = 0;

            foreach (Guid id in param_ids)
            {
                String id_as_string = id.ToString();
                int start = 0, end = 0, visibleEnde = 0;
                while ((end = Find(id_as_string, end, RichTextBoxFinds.None)) != -1)
                {
                    if (FindStringByPosition(end,
                        ref start,
                        ref end,
                        ref visibleEnde))
                    {
                        oevrall_start = Math.Min(oevrall_start, start);
                        oevrall_end = Math.Max(oevrall_end, visibleEnde);
                    }
                    else end++;
                }
            }
            if (oevrall_end > oevrall_start)
            {
                SelectionStart = oevrall_start;
                SelectionLength = oevrall_end - oevrall_start;
            }

            ResumeEvents();
        }
        public void MarkObject(Guid param_id)
        {
            SuspendEvents();
            int index = Find(param_id.ToString());

            int start = 0, end = 0, visibleEnde=0;


            if (FindStringByPosition(index,
                ref start,
                ref end,
                    ref visibleEnde))
            {
                SelectionStart = start;
                SelectionLength = visibleEnde - start;
            }
            ResumeEvents();
        }
        public void MarkWithParent(Guid param_guid)
        {
            SuspendEvents();
            String id_as_string = param_guid.ToString();

            int oevrall_start = 1000000;
            int oevrall_end = 0;

            int start = 0, end = 0, visibleEnde = 0;

            while ((end = Find(id_as_string, end, RichTextBoxFinds.None)) != -1)
            {
                if (FindStringByPosition(end,
                    ref start,
                    ref end,
                    ref visibleEnde))
                {
                    oevrall_start = Math.Min(oevrall_start, start);
                    oevrall_end = Math.Max(oevrall_end, visibleEnde);
                }
                else end++;
            }
            if (oevrall_end > oevrall_start)
            {
                SelectionStart = oevrall_start;
                SelectionLength = oevrall_end - oevrall_start;
            }
            ResumeEvents();
        }

        public void UnMark()
        {
            SuspendEvents();
            SelectionLength = 0;
            ResumeEvents();
        }

        #endregion
    }
}
    