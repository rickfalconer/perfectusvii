using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ExpressionLibrary;

namespace CodeEditControl
{
    public partial class CodeCtrl 
    {
        #region Events

        private void CodeCtrl_MouseDown(object sender, MouseEventArgs e)
        {
            // Test if events are supressed. If true there is no point in creating event data
            if (_suspend)
                return;

            if (e.Clicks == 1 && 
                e.Button == MouseButtons.Left && 
                SelectionLength > 0)
                UserInputOperation(enumAction.LClick, e);

            if (e.Clicks == 2 && // double-click marks the word underneath
                e.Button == MouseButtons.Left)
                UserInputOperation(enumAction.LDblClick, e);

            if (e.Clicks == 1 && 
                e.Button == MouseButtons.Right)
                UserInputOperation(enumAction.RClick, e);
        }

        private void UserInputOperation(enumAction param_action, EventArgs e)
        {
            // sanity check
            if (Text.Length == 0)
                return;

            // Position in text stream
            int positionClicked = GetCharIndexFromPosition(PointToClient(Cursor.Position));

            // Test if some text is marked and the click happened ouside
            if (SelectionLength > 0)
            {
                if ((positionClicked < SelectionStart ||
                positionClicked > SelectionStart + SelectionLength)
                    && param_action != enumAction.Hover)
                {

                    OnInvalidHit(String.Format("Right-clicked text is outside of the marked area. Don't know what to do!"));
                    return;
                }

                ObjectsInfoStruc objects = GetObjectsInRange(SelectionStart, SelectionStart + SelectionLength);
                objects.Action = param_action;

                int numOfObject = objects.GetObjects().Length;
                if (numOfObject == 1)
                {
                    ObjectInfoStruc obj = objects.GetObjects()[0];
                    obj.Action = param_action;
                    OnObjectHit(obj);
                }
                else
                    if (numOfObject > 1)
                        OnObjectsHit(objects);
            }
            else
            {
                // We are looking for one object only
                ObjectInfoStruc info = GetClickedObject(positionClicked);
                if (info != null)
                {
                    info.Action = param_action;
                    if (param_action == enumAction.Hover)
                        OnObjectHover(info);
                    else
                        OnObjectHit(info);
                }
            }
        }


        private ObjectsInfoStruc GetObjectsInRange(int param_start_index, int param_end_index)
        {
            if (Text.Length == 0)
                return null;

            ObjectsInfoStruc objects = new ObjectsInfoStruc();

            if (param_end_index == param_start_index)
            {
                ObjectInfoStruc info = GetClickedObject(param_start_index);
                if (info != null)
                    objects.Add(info);
            }
            else // multiple objects possible
            while (param_start_index <= param_end_index)
            {
                int FirstCharPos = 0, LastCharPos = 0;
                ObjectInfoStruc info = GetClickedObject(param_start_index, ref FirstCharPos, ref LastCharPos);

                if (info != null)
                {
                    objects.Add(info);
                    // jump over the next delimiter
                    // 2 delimiters of 2 = 4 
                    // plus FirstCharPos==LastCharPos -> Length = 1
                    param_start_index = LastCharPos + 5; 
                }
                else
                    param_start_index += 20; // Any string we are looking for has at least xx characters
            }
            return objects;
        }

        private Boolean FindStringByPosition(int param_positionClicked, ref int stringStartPosition, ref int stringEndPosition)
        {
            int stringVisibleEndPosition = -1;
            return FindStringByPosition(param_positionClicked, ref stringStartPosition, ref stringEndPosition, ref stringVisibleEndPosition);
        }
        private Boolean FindStringByPosition ( int param_positionClicked, ref int stringStartPosition, ref int stringEndPosition, ref int stringVisibleEndPosition ) 
        {
           String leftStr = Text.Substring(0, param_positionClicked);
            int openLeftMarker = leftStr.LastIndexOf("<%");
            int ctrlLeftMarker = leftStr.LastIndexOf("%>");

            // Outside of element?
            if (openLeftMarker == -1 || openLeftMarker < ctrlLeftMarker)
                return false;

            String rightStr = Text.Substring(param_positionClicked, Text.Length - param_positionClicked);
            int closeRightMarker = rightStr.IndexOf("%>");
            int ctrlRightMarker = rightStr.IndexOf("<%");

            // That is actually an error!
            if (ctrlRightMarker < closeRightMarker)
                return false;

            stringStartPosition = openLeftMarker + 2;
            stringEndPosition = param_positionClicked + closeRightMarker - 1;

            if (stringVisibleEndPosition != -1)
            {
                String clickedText = Text.Substring(stringStartPosition, stringEndPosition - stringStartPosition);
                stringVisibleEndPosition = clickedText.IndexOf("#") + stringStartPosition;
            }
            
            return true;
        }

        private ObjectInfoStruc GetClickedObject(int param_positionClicked)
        {
            int start=0, end=0;
            return GetClickedObject(param_positionClicked, ref start, ref end);
        }

        private ObjectInfoStruc GetClickedObject(int param_positionClicked, ref int stringStartPosition, ref int stringEndPosition)
        {
            if (FindStringByPosition(param_positionClicked, ref stringStartPosition, ref stringEndPosition))
            {
                // Find the whole word (including guid data)
                String clickedText = Text.Substring(stringStartPosition, stringEndPosition - stringStartPosition);
                try
                {
                    // Expect to find a string of the form
                    // NAME#ID_AS_GUID#PARENTID_AS_GUID#TYPE
                    String[] tokenizedText = clickedText.Split(new char[] { '#' });
                    Guid id = new Guid(tokenizedText[1]);
                    Guid pid = new Guid(tokenizedText[2]);
                    AtomType type = Info.GetElementType(tokenizedText[3]);
                    ObjectInfoStruc info = new ObjectInfoStruc(tokenizedText[0], id, pid, type);
                    return info;
                }
                // not an error. User clicked just 'somewhere' 
                catch (System.Exception)
                {
                }
                return null;
            }
            return null;
        }


        private void CodeCtrl_MouseMove(object sender, MouseEventArgs e)
        {
            _mouseMoveTimer.Stop();
            _mouseMoveTimer.Start();
        }
        private void CodeEditBox_MouseHover_Tick(object sender, EventArgs e)
        {
            _mouseMoveTimer.Stop();
            // Test if events are supressed. If true there is no point in creating event data
            if (_suspend)
                return;
            UserInputOperation(enumAction.Hover, e);
        }

        #region Selection / Hover
        private void CodeCtrl_MouseLeave(object sender, EventArgs e)
        {
            _mouseMoveTimer.Stop();
            _textMarkingTimer.Stop();
        }
        private void CodeCtrl_MouseEnter(object sender, EventArgs e)
        {
            _mouseMoveTimer.Start();
            _textMarkingTimer.Start();
        }

        #region Selection / Hover Selection Changed
        private void CodeCtrl_SelectionChanged(object sender, EventArgs e)
        {
            _textMarkingTimer.Stop();
            _textMarkingTimer.Start();
        }
        
        void CodeEditBox_SelectionChanged_Tick(object sender, EventArgs e)
        {
            _textMarkingTimer.Stop();
            // Test if events are supressed. If true there is no point in creating event data
            if (_suspend)
                return;

            ObjectsInfoStruc objects = GetObjectsInRange(SelectionStart, SelectionStart + SelectionLength);
            if (null == objects || objects.GetObjects().Length == 0)
                return;

            objects.Action = enumAction.SelChg;

            int numOfObject = objects.GetObjects().Length;
            if (numOfObject == 1)
            {
                ObjectInfoStruc obj = objects.GetObjects()[0];
                obj.Action = enumAction.SelChg;
                OnObjectHit(obj);
            }
            else
                if (numOfObject > 1)
                    OnObjectsHit(objects);

        }

        #endregion
        #endregion
        #endregion
    }
}
    