using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public class SelectorGlobal
    {
        public delegate void SelectionValidCbk(Boolean param_valid, LibraryItem param_item);
        public delegate void ItemSelectedCbk();
        public delegate void WrapSelectionWithSimpleOutcome(SimpleOutcome so);
        
        public enum VisiblePackageItemType { QUESTION, FUNCTION, OUTCOME, CONDITIONALTEXT, CONSTANT }
        public enum DragNDropOperation { NONE, SOURCE_WORDML, SOURCE_HTML, TARGET_WORDML, TARGET_HTML }
    }

    internal interface IItemViewType
    {
        event SelectorGlobal.ItemSelectedCbk dblClick;
        event SelectorGlobal.SelectionValidCbk selectionValid;

        void Reset(LibraryItem param_select_item);
        void maybeSelectItem(LibraryItem param_select_item);
        
        void AddDatatype(PerfectusDataType param_showType);
        void AddDragNDropOperation(SelectorGlobal.DragNDropOperation dropTargetType);
        void AddPackageItemType(SelectorGlobal.VisiblePackageItemType param_showItem);

        Package Package { set;}
    }

    public delegate void SelectedPackageLibraryItemHandler(object o, SelectedPackageItemEventArgs e, int id);
    public class SelectedPackageItemEventArgs : EventArgs
    {
        private string name;
        private Guid uniqueIdentifier;

        public string Name
        {
            get { return name; }
        }

        public Guid UniqueIdentifier
        {
            get { return uniqueIdentifier; }
        }

        public SelectedPackageItemEventArgs(string name, Guid guid)
        {
            this.name = name;
            this.uniqueIdentifier = guid;
        }
    }
}
