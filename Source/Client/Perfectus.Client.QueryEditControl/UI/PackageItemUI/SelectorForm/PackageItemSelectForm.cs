using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class PackageItemSelectForm : Form
    {
        public LibraryItem AnswerProviderItem
        {
            get { return selectorCard1.AnswerProviderItem; }
            set
            {
                selectorCard1.AnswerProviderItem = value;
                if (selectorCard1.AnswerProviderItem is AnswerProvider)
                {
                    AnswerProvider item = value as AnswerProvider;
                    if (item is PackageConstant)
                        Text += "constant value";
                    else
                        Text += String.Format("[{0}] {1}", Perfectus.Common.PerfectusTypeSystem.LocaliseType(item.DataType), item.Name);
                }
            }
        }

        public PackageItemSelectForm()
        {
            InitializeComponent();

            this.AcceptButton = btnAccept;
            this.CancelButton = btnCancel;

            DesktopLocation = new Point(Cursor.Position.X, Cursor.Position.Y + 8);

        }

        Package _package = null;
        public Package Package
        {
            set
            {
                _package = value;
                this.selectorCard1.Package = _package;
            }
        }

        internal void showItem(SelectorGlobal.VisiblePackageItemType param_showType)
        {
            this.selectorCard1.AddPackageItemType(param_showType);
        }
        internal void showType(PerfectusDataType param_showType)
        {
            this.selectorCard1.AddDatatype(param_showType);
        }
        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
        void selectorCard1_dblClick()
        {
            this.btnAccept.PerformClick();
        }
        void selectorCard1_selectionValid(bool param_valid, Perfectus.Common.PackageObjects.LibraryItem param_item)
        {
            this.btnAccept.Enabled = param_valid;
        }

        private void PackageItemSelectForm_Load(object sender, EventArgs e)
        {
            // Ugly fix for DSOFramer FB1517
            // The COM doesn't respect modal windows and "tries" to take ownership
            // of the popup window. As a result the 'real' owner slips behind the main window.
            // The following tries to 'reestablish' the z-order.
            Form owner = Application.OpenForms[0];
            //Form owner = this;
            while (owner.Owner != null)
                owner = owner.Owner;

            if (null != owner)
            {
                owner.Select();
                owner.Focus();
            }
        }
    }
}