namespace Perfectus.Client.Studio.QueryEditControl
{
    partial class PackageItemSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code


        //this.selectorCard1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
        //                | System.Windows.Forms.AnchorStyles.Left)
        //                | System.Windows.Forms.AnchorStyles.Right)));
        //    this.selectorCard1.AnswerProviderItem = null;
        //    this.selectorCard1.Location = new System.Drawing.Point(-1, -1);
        //    this.selectorCard1.Name = "selectorCard1";
        //    this.selectorCard1.Size = new System.Drawing.Size(190, 244);
        //    this.selectorCard1.TabIndex = 0;
        //    this.selectorCard1.dblClick +=new Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorList.ItemSelectedCbk(selectorCard1_dblClick);
        //    this.selectorCard1.selectionValid += new Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorList.SelectionValidCbk(selectorCard1_selectionValid);
            
        //this.selectorCard1 = new SelectorCard(_package);
            

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( PackageItemSelectForm ) );
            this.btnAccept = new System.Windows.Forms.Button( );
            this.btnCancel = new System.Windows.Forms.Button( );
            this.selectorCard1 = new Perfectus.Client.Studio.QueryEditControl.CPIWithConstant( );
            this.SuspendLayout( );
            // 
            // btnAccept
            // 
            resources.ApplyResources( this.btnAccept, "btnAccept" );
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler( this.btnAccept_Click );
            // 
            // btnCancel
            // 
            resources.ApplyResources( this.btnCancel, "btnCancel" );
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler( this.btnCancel_Click );
            // 
            // selectorCard1
            // 
            resources.ApplyResources( this.selectorCard1, "selectorCard1" );
            this.selectorCard1.AnswerProviderItem = null;
            this.selectorCard1.Name = "selectorCard1";
            this.selectorCard1.dblClick += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.ItemSelectedCbk( this.selectorCard1_dblClick );
            this.selectorCard1.selectionValid += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.SelectionValidCbk( this.selectorCard1_selectionValid );
            // 
            // PackageItemSelectForm
            // 
            resources.ApplyResources( this, "$this" );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ControlBox = false;
            this.Controls.Add( this.btnCancel );
            this.Controls.Add( this.btnAccept );
            this.Controls.Add( this.selectorCard1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "PackageItemSelectForm";
            this.TopMost = true;
            this.Load += new System.EventHandler( this.PackageItemSelectForm_Load );
            this.ResumeLayout( false );

        }

        #endregion

        private CPIWithConstant selectorCard1;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
    }
}