using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;
using System.Resources;
using System.Windows.Forms;
using System.ComponentModel;
using Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class CPIWithConstant : UserControl, IItemViewType
    {
        internal struct LocalizedConstantName
        {
            internal Object storageValue;
            internal String foreignName;
            internal PerfectusDataType dataType;
            internal LocalizedConstantName(Object param_storageValue, String param_foreignName, PerfectusDataType param_datatype)
            {
                storageValue = param_storageValue;
                foreignName = param_foreignName;
                dataType = param_datatype;
            }
            public override string ToString()
            {
                return foreignName;
            }
        } 
        
        private void SetupConstantCard()
        {
            // Constants
            // Load localized strings for <<Yes>>, <<No>> etc.
            ResourceManager resources = ResourceLoader.GetResourceManager("Perfectus.Common.Localisation");
            lstBoxConstantValues.Items.Add(new LocalizedConstantName("Null", resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Null"), PerfectusDataType.Number));
            lstBoxConstantValues.Items.Add(new LocalizedConstantName("Not Answered", resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.NotAnswered"), PerfectusDataType.Number));
            if (_dataTypes.Contains(PerfectusDataType.YesNo))
            {
                lstBoxConstantValues.Items.Add(new LocalizedConstantName(false, resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.No"), PerfectusDataType.YesNo));
                lstBoxConstantValues.Items.Add(new LocalizedConstantName(true, resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Yes"), PerfectusDataType.YesNo));
            }
            if (_dataTypes.Contains(PerfectusDataType.Date)) // ��
                lstBoxConstantValues.Items.Add(new LocalizedConstantName("DateTime.Now", resources.GetString("Perfectus.Common.PackageObjects.PackageConstant.Now"), PerfectusDataType.Date));
            btnCalendar.Enabled = _dataTypes.Contains(PerfectusDataType.Date);
        }

        public void Reset(LibraryItem param_select_item)
        {
            if (param_select_item != null &&
                param_select_item is PackageConstant)
            {
                txtConstantInput.Text = param_select_item.ToString();
                foreach (LocalizedConstantName item in lstBoxConstantValues.Items)
                {
                    if (item.foreignName == param_select_item.ToString())
                    {
                        lstBoxConstantValues.SelectedItem = item;
                        return;
                    }
                }
            }
            else
            {
                list_selectionValid(false, null); // disable the Accept button
                txtConstantInput.Text = String.Empty;
            }
        }

        private void lstBoxConstantValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (null == lstBoxConstantValues.SelectedItem)
                return;
            LocalizedConstantName item = (LocalizedConstantName )lstBoxConstantValues.SelectedItem;
            txtConstantInput.Text = item.foreignName;
            list_selectionValid(true, new PackageConstant(item.dataType, item.storageValue));
        }
        private void lstBoxConstantValues_DoubleClick(object sender, EventArgs e)
        {
            if (null == lstBoxConstantValues.SelectedItem)
                return;
            LocalizedConstantName item = (LocalizedConstantName)lstBoxConstantValues.SelectedItem;
            txtConstantInput.Text = item.foreignName;
            list_selectionValid(true, new PackageConstant(item.dataType, item.storageValue));
            OnDblClick();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            _answerProvider = null;
            m_did_validate = false;
            list_enableDefaultOKButton(txtConstantInput.Text.Length > 0);
        }

        Boolean m_did_validate = false;
        private void txtConstantInput_Validating(object sender, CancelEventArgs e)
        {
            PerfectusDataType testedType = PerfectusDataType.Text;
            try
            {
                if (m_did_validate)
                    return;

                if (_answerProvider != null)
                    return;

                if (txtConstantInput.Text.Length == 0)
                {
                    list_selectionValid(false, null);
                    return;
                }
                // Text accepts everything
                if (_dataTypes.Contains(PerfectusDataType.Text))
                {
                    list_selectionValid(true, new PackageConstant(PerfectusDataType.Text, txtConstantInput.Text));
                    return;
                }

                // Number and data should be mutual exclusive
                // (if both types are valid, then text is valid also)
                if (_dataTypes.Contains(PerfectusDataType.Number))
                {
                    testedType = PerfectusDataType.Number;
                    Double d = Double.Parse(txtConstantInput.Text);

                    txtConstantInput.Text = d.ToString();
                    list_selectionValid(true, new PackageConstant(PerfectusDataType.Number, d));

                    return;
                }

                if (_dataTypes.Contains(PerfectusDataType.Date))
                {
                    testedType = PerfectusDataType.Date;
                    DateTime date = DateTime.Parse(txtConstantInput.Text);
                    list_selectionValid(true, new PackageConstant(PerfectusDataType.Date, date));
                    return;
                }

                if (_dataTypes.Contains(PerfectusDataType.YesNo))
                {
                    testedType = PerfectusDataType.YesNo;
                    Boolean b = Boolean.Parse(txtConstantInput.Text);
                    list_selectionValid(true, new PackageConstant(PerfectusDataType.YesNo, b));
                    return;
                }


            }
            catch (Exception)
            {
                m_did_validate = true;

                e.Cancel = true;
                list_selectionValid(false, null);
                // txtConstantInput.Text = "";
                txtConstantInput.SelectAll();

                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPIWithConstant));
                String message = String.Format(resources.GetString("InputTypeValidationFailed"), testedType.ToString());
                MessageBox.Show(message, resources.GetString("InputTypeValidationFailedCapture"), MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }


        private void btnCalendar_Click(object sender, EventArgs e)
        {
            QuestionSelectorConstantDatepicker datePickerForm = new QuestionSelectorConstantDatepicker(DateTime.Now);
            datePickerForm.ShowDialog();
            txtConstantInput.Text = datePickerForm.dateTime.ToShortDateString();
            list_selectionValid(true, new PackageConstant(PerfectusDataType.Date, datePickerForm.dateTime));
        }

        void IItemViewType.maybeSelectItem(LibraryItem param_select_item)
        {
        }

    }
}
