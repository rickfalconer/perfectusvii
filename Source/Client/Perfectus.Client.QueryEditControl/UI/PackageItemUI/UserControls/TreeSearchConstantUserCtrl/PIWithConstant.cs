using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class CPIWithConstant : UserControl, IItemViewType
    {

        #region IItemViewType Members

        public event SelectorGlobal.ItemSelectedCbk dblClick;
        private void OnDblClick()
        {
            if (dblClick != null)
                dblClick();
        }

        public event SelectorGlobal.SelectionValidCbk selectionValid;
        private void OnSelectionValid(Boolean param_valid, LibraryItem param_item)
        {
            if (selectionValid != null)
                selectionValid(param_valid, param_item);
        }        Package _package;
        public Package Package
        {
            set
            {
                _package = value;
                this.cpiTreeView1.Package = _package;
            }
        }

        private List<SelectorGlobal.VisiblePackageItemType> _typesToShow;
        public void AddPackageItemType(SelectorGlobal.VisiblePackageItemType param_showItem)
        {
            _typesToShow.Add(param_showItem);
            this.cpiTreeView1.AddPackageItemType (param_showItem );
        }

        private List<SelectorGlobal.DragNDropOperation> _dropTargetType;
        public void AddDragNDropOperation(SelectorGlobal.DragNDropOperation dropTargetType)
        {
            _dropTargetType.Add (dropTargetType);
            this.cpiTreeView1.AddDragNDropOperation (dropTargetType );
        }

        private List<PerfectusDataType> _dataTypes;
        public void AddDatatype(PerfectusDataType param_showType)
        {
            _dataTypes.Add(param_showType);
            this.cpiTreeView1.AddDatatype(param_showType);
        }

     
    #endregion

        private LibraryItem _answerProvider;
        public LibraryItem AnswerProviderItem
        {
            get { return _answerProvider; }
            set { _answerProvider = value;
            cpiTreeView1.AnswerProviderItem = value; }
        }

        public CPIWithConstant()
        {
            InitializeComponent();

            _typesToShow = new List<SelectorGlobal.VisiblePackageItemType>();
            _dataTypes = new List<PerfectusDataType>();
            _dropTargetType = new List<SelectorGlobal.DragNDropOperation>();
        }

        private void CPIWithConstant_Load(object sender, EventArgs e)
        {
            // Constant tab
            if (!_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.CONSTANT))
            {
                this.tabConstant.Enabled = false;
                //this.tabPackageItems.TabPages.Remove(this.tabConstant);
                return;
            }
            SetupConstantCard();

            // Tell the control to select an item
            if (AnswerProviderItem == null ||
                AnswerProviderItem is PackageConstant)
            {
                this.tabPackageItems.SelectedTab = this.tabConstant;
                txtConstantInput.Select();
                txtConstantInput.Focus();
                txtConstantInput.SelectAll();
            }
        }

        void list_selectionValid(bool param_valid, LibraryItem param_item)
        {
            param_valid = param_valid && param_item != null;
            if (param_valid)
                _answerProvider = param_item;
            OnSelectionValid(param_valid, param_item);
        }

        void list_enableDefaultOKButton(Boolean param_enable)
        {
            OnSelectionValid(param_enable, null);
        }

        void list_dblClick()
        {
            OnDblClick();
        }

        private void tabPackageItems_Selected(object sender, TabControlEventArgs e)
        {
            // Be aware that 'AnswerProviderItem' can be set by other controls to something
            // arbitrary
            if (e.Action == TabControlAction.Selected)
            {
                if (
                e.TabPage == this.tabConstant)
                    Reset(AnswerProviderItem);
                else
                    if (e.TabPage == this.tabPackageItem)
                        this.cpiTreeView1.Reset(null);
            }
        }
    }
}
