using System.Windows.Forms;
namespace Perfectus.Client.Studio.QueryEditControl
{
    partial class CPIWithConstant
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        //    this.cpiTreeView1.dblClick += new SelectorGlobal.ItemSelectedCbk(this.list_dblClick);
        //    this.cpiTreeView1.selectionValid += new SelectorGlobal.SelectionValidCbk(this.list_selectionValid);

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPIWithConstant));
            this.tabPackageItem = new System.Windows.Forms.TabPage();
            this.cpiTreeView1 = new Perfectus.Client.Studio.QueryEditControl.CPITreeSearch();
            this.tabConstant = new System.Windows.Forms.TabPage();
            this.btnCalendar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConstantInput = new System.Windows.Forms.TextBox();
            this.lstBoxConstantValues = new System.Windows.Forms.ListBox();
            this.tabPackageItems = new System.Windows.Forms.TabControl();
            this.tabPackageItem.SuspendLayout();
            this.tabConstant.SuspendLayout();
            this.tabPackageItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPackageItem
            // 
            this.tabPackageItem.Controls.Add(this.cpiTreeView1);
            resources.ApplyResources(this.tabPackageItem, "tabPackageItem");
            this.tabPackageItem.Name = "tabPackageItem";
            this.tabPackageItem.UseVisualStyleBackColor = true;
            // 
            // cpiTreeView1
            // 
            resources.ApplyResources(this.cpiTreeView1, "cpiTreeView1");
            this.cpiTreeView1.Name = "cpiTreeView1";
            this.cpiTreeView1.dblClick += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.ItemSelectedCbk(this.list_dblClick);
            this.cpiTreeView1.selectionValid += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.SelectionValidCbk(this.list_selectionValid);
            // 
            // tabConstant
            // 
            this.tabConstant.Controls.Add(this.btnCalendar);
            this.tabConstant.Controls.Add(this.label3);
            this.tabConstant.Controls.Add(this.label2);
            this.tabConstant.Controls.Add(this.label1);
            this.tabConstant.Controls.Add(this.txtConstantInput);
            this.tabConstant.Controls.Add(this.lstBoxConstantValues);
            resources.ApplyResources(this.tabConstant, "tabConstant");
            this.tabConstant.Name = "tabConstant";
            this.tabConstant.UseVisualStyleBackColor = true;
            // 
            // btnCalendar
            // 
            resources.ApplyResources(this.btnCalendar, "btnCalendar");
            this.btnCalendar.FlatAppearance.BorderSize = 0;
            this.btnCalendar.Name = "btnCalendar";
            this.btnCalendar.UseVisualStyleBackColor = true;
            this.btnCalendar.Click += new System.EventHandler(this.btnCalendar_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // txtConstantInput
            // 
            resources.ApplyResources(this.txtConstantInput, "txtConstantInput");
            this.txtConstantInput.Name = "txtConstantInput";
            this.txtConstantInput.Validating += new System.ComponentModel.CancelEventHandler(this.txtConstantInput_Validating);
            this.txtConstantInput.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lstBoxConstantValues
            // 
            resources.ApplyResources(this.lstBoxConstantValues, "lstBoxConstantValues");
            this.lstBoxConstantValues.FormattingEnabled = true;
            this.lstBoxConstantValues.Name = "lstBoxConstantValues";
            this.lstBoxConstantValues.DoubleClick += new System.EventHandler(this.lstBoxConstantValues_DoubleClick);
            this.lstBoxConstantValues.SelectedIndexChanged += new System.EventHandler(this.lstBoxConstantValues_SelectedIndexChanged);
            // 
            // tabPackageItems
            // 
            this.tabPackageItems.Controls.Add(this.tabPackageItem);
            this.tabPackageItems.Controls.Add(this.tabConstant);
            resources.ApplyResources(this.tabPackageItems, "tabPackageItems");
            this.tabPackageItems.MinimumSize = new System.Drawing.Size(214, 259);
            this.tabPackageItems.Name = "tabPackageItems";
            this.tabPackageItems.SelectedIndex = 0;
            this.tabPackageItems.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabPackageItems_Selected);
            // 
            // CPIWithConstant
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabPackageItems);
            this.Name = "CPIWithConstant";
            this.Load += new System.EventHandler(this.CPIWithConstant_Load);
            this.tabPackageItem.ResumeLayout(false);
            this.tabConstant.ResumeLayout(false);
            this.tabConstant.PerformLayout();
            this.tabPackageItems.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabPage tabPackageItem;
        private CPITreeSearch cpiTreeView1;
        private TabPage tabConstant;
        private Label label2;
        private Label label1;
        private Button btnCalendar;
        private TextBox txtConstantInput;
        private ListBox lstBoxConstantValues;
        private TabControl tabPackageItems;
        private Label label3;

    }
}
