using System;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.QueryEditControl;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Drawing;

namespace Perfectus.Client.Studio.UI.PackageExplorer
{

    /// <summary>
    /// Summary description for ExplorerControlMini.
    /// </summary>
    public class ExplorerControlMini2 : UserControl, IItemViewType
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

        private const int WM_VSCROLL = 277; // Vertical scroll	
        private const int SB_PAGEUP = 2; // Scrolls one page up	
        private const int SB_PAGEDOWN = 3; // Scrolls one page down
        
        
        private System.ComponentModel.IContainer components;
        private TreeView tvwExplorer;
        private Package _package;
        private bool suspended = false;
        private TreeNode rootNode;
        private TreeNode questionsNode;
        private TreeNode functionsNode;
        private TreeNode outcomeNode;
        private TreeNode conditionalTextNode;
        private ImageList ilistTreeView;
        private GuidToPfTreeNodeCollectionDictionary guidNodeMap;

        public ExplorerControlMini2()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            this.tvwExplorer.AllowDrop = false;

            _typesToShow = new List<SelectorGlobal.VisiblePackageItemType>();
            _dataTypes = new List<PerfectusDataType>();
            _dropTargetType = new List<SelectorGlobal.DragNDropOperation>();
        }

        #region IItemViewType Members


        public event SelectorGlobal.SelectionValidCbk selectionValid;
        private void OnSelectionValid(Boolean param_valid, LibraryItem param_item)
        {
            if (selectionValid != null)
                selectionValid(param_valid, param_item);
        }

        public event SelectorGlobal.ItemSelectedCbk dblClick;
        private void OnDblClick()
        {
            if (dblClick != null)
                dblClick();
        }

        public Package Package
        {
            set
            {
                _package = value;
                if (value != null)
                {
                    guidNodeMap = new GuidToPfTreeNodeCollectionDictionary();
                    Reset(null);
                }
            }
        }
        private List<SelectorGlobal.VisiblePackageItemType> _typesToShow;
        public void AddPackageItemType(SelectorGlobal.VisiblePackageItemType param_showItem)
        {
            _typesToShow.Add(param_showItem);
            Reset(null);
        }

        private List<SelectorGlobal.DragNDropOperation> _dropTargetType;
        public void AddDragNDropOperation(SelectorGlobal.DragNDropOperation dropTargetType)
        {
            _dropTargetType.Add(dropTargetType);
            if (dropTargetType == SelectorGlobal.DragNDropOperation.TARGET_HTML ||
                dropTargetType == SelectorGlobal.DragNDropOperation.TARGET_WORDML)
            {
                this.tvwExplorer.AllowDrop = true;
            }
        }

        private List<PerfectusDataType> _dataTypes;
        public void AddDatatype(PerfectusDataType param_showType)
        {
            _dataTypes.Add(param_showType);
            Reset(null);
        }

        void IItemViewType.maybeSelectItem(LibraryItem param_select_item)
        {
            if (param_select_item != null)
                FindAndSelectNode(param_select_item);
        }

        #endregion


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public void Reset(LibraryItem param_select_item)
        {
            if (_package == null
                || !Created)
                return;

            try
            {
                tvwExplorer.SuspendLayout();
                suspended = true;

                DisableControl();
                tvwExplorer.Hide();
                ResetNode(rootNode);
            }
            finally
            {
                tvwExplorer.ResumeLayout();
                suspended = false;
                EnableControl();
                tvwExplorer.Show();
            }

            tvwExplorer.Refresh();
            tvwExplorer.CollapseAll();

            if (questionsNode != null)
                questionsNode.Expand();
            if (functionsNode != null)
                functionsNode.Expand();
            if (param_select_item != null)
                FindAndSelectNode(param_select_item);
            else
                tvwExplorer.Nodes[0].Expand();
        }


        private TreeNode FindNode(TreeNode startNode, Object itemToMatch)
        {
            foreach (TreeNode node in startNode.Nodes)
            {
                if (itemToMatch == node.Tag)
                    return node;
                TreeNode found = FindNode(node, itemToMatch);
                if (found != null)
                    return found;
            }
            return null;
        }

        private void FindAndSelectNode(LibraryItem param_select_item)
        {
            if (param_select_item == null)
                return;
            TreeNode node = FindNode(rootNode, param_select_item);
            if (null != node)
            {
                node.EnsureVisible();
                tvwExplorer.SelectedNode = node;
            }
        }


        private void AddFolder(System.Collections.CollectionBase param_folderCollection)
        {
            foreach (Folder folder in param_folderCollection)
            {
                AddNode(folder);
                FolderCollection subFolders = folder.GetChildFolders();
                foreach (Folder subQf in subFolders)
                {
                    AddNode(subQf);
                }
            }
        }


        private void ResetNode(TreeNode n)
        {
            if (n == rootNode)
            {
                tvwExplorer.Nodes.Clear();
                guidNodeMap.Clear();

                // Set up the root nodes			
                string localQuestionsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.QuestionsNodeName");
                string localFunctionsNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.FunctionsNodeName");
                string localOutcomeNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.OutcomesNodeName");
                string localConditionalTextNodeName = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("PackageExplorer.ExplorerControl.SimpleOutcomesNodeName");

                rootNode = new TreeNode(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.PackageExplorer.ExplorerControl.NoPackageLoaded"), (int)TreeIcons.FolderPackage, (int)TreeIcons.FolderPackage);

                if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.QUESTION)
                    && _package.Questions.Count > 0)
                {
                    questionsNode = new TreeNode(localQuestionsNodeName, (int)TreeIcons.FolderQuestions, (int)TreeIcons.FolderQuestions);
                    questionsNode.Tag = _package.Questions;
                    rootNode.Nodes.Add(questionsNode);

                    // Insert folders
                    AddFolder(_package.QuestionsFolders);

                    if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.QUESTION))
                        foreach (Question question in _package.Questions)
                            if (_dataTypes.Contains(question.DataType))
                                AddNode(question);
                }

                if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.FUNCTION)
                    && _package.Functions.Count > 0)
                {
                    functionsNode = new TreeNode(localFunctionsNodeName, (int)TreeIcons.FolderFunctions, (int)TreeIcons.FolderFunctions);
                    functionsNode.Tag = _package.Functions;
                    rootNode.Nodes.Add(functionsNode);

                    // Insert folders
                    AddFolder(_package.FunctionFolders);

                    foreach (PFunction function in _package.Functions)
                        if (_dataTypes.Contains(function.DataType))
                            AddNode(function);
                }

                // Show empty simple Folder, if dragging is possible
                if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.CONDITIONALTEXT)
                    && _package.SimpleOutcomes.Count > 0 ||
                    _dropTargetType.Contains (SelectorGlobal.DragNDropOperation.TARGET_WORDML) )
                {
                    conditionalTextNode = new TreeNode(localConditionalTextNodeName, (int)TreeIcons.FolderSimpleOutcome, (int)TreeIcons.SimpleOutcome);
                    conditionalTextNode.Tag = _package.SimpleOutcomes;
                    rootNode.Nodes.Add(conditionalTextNode);

                    // Insert folders
                    AddFolder(_package.SimpleOutcomeFolders);

                    foreach (SimpleOutcome simple in _package.SimpleOutcomes)
                        AddNode(simple);
                }

                if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.OUTCOME)
                    && _package.Outcomes.Count > 0)
                {
                    outcomeNode = new TreeNode(localOutcomeNodeName, (int)TreeIcons.FolderOutcomes, (int)TreeIcons.Outcome);
                    outcomeNode.Tag = _package.Outcomes;
                    rootNode.Nodes.Add(outcomeNode);

                    // Insert folders
                    AddFolder(_package.OutcomeFolders);

                    foreach (Outcome outcome in _package.Outcomes)
                        AddNode(outcome);
                }

                tvwExplorer.Nodes.Add(rootNode);

                PackageNameBind();
                rootNode.Tag = _package;
            }
        }


        private void AddNode(PackageItem itemToAdd)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                TreeNode newNode = new TreeNode();

                string nodeName = itemToAdd.Name;
                object nodeTag = itemToAdd;

                int nodeImageIndex;
                TreeNode parentNode;

                if (itemToAdd is Question)
                {
                    nodeImageIndex = (int)TreeIcons.Question;
                    parentNode = questionsNode;
                }
                else
                    if (itemToAdd is DatePFunction)
                    {
                        nodeImageIndex = (int)TreeIcons.DateFunction;
                        parentNode = functionsNode;
                    }
                    else
                        if (itemToAdd is ArithmeticPFunction)
                        {
                            nodeImageIndex = (int)TreeIcons.ArithmeticFunction;
                            parentNode = functionsNode;
                        }
                        else
                                if (itemToAdd is Outcome)
                                {
                                    nodeImageIndex = (int)TreeIcons.Outcome;
                                    parentNode = outcomeNode;
                                }
                                else
                                    if (itemToAdd is SimpleOutcome)
                                    {
                                        nodeImageIndex = (int)TreeIcons.SimpleOutcome;
                                        parentNode = conditionalTextNode;
                                    }
                                    else
                                        if (itemToAdd is QuestionFolder)
                                        {
                                            nodeImageIndex = (int)TreeIcons.FolderClosed;
                                            parentNode = questionsNode;
                                        }
                                        else
                                            if (itemToAdd is PFunctionFolder)
                                            {
                                                nodeImageIndex = (int)TreeIcons.FolderClosed;
                                                parentNode = functionsNode;
                                            }
                                            else
                                                if (itemToAdd is OutcomeFolder)
                                                {
                                                    nodeImageIndex = (int)TreeIcons.FolderClosed;
                                                    parentNode = outcomeNode;
                                                }
                                                else
                                                    if (itemToAdd is SimpleOutcomeFolder)
                                                    {
                                                        nodeImageIndex = (int)TreeIcons.FolderClosed;
                                                        parentNode = conditionalTextNode;
                                                    }

                                            else
                                                throw new NotImplementedException();


                if (itemToAdd.ParentFolder != null)
                {
                    // Find the parent node (another question folder)
                    PfTreeNodeCollection tnc = guidNodeMap[itemToAdd.ParentFolder.UniqueIdentifier];
                    if (tnc != null && tnc.Count > 0)
                        parentNode = tnc[0];
                }
                //////////////////////////


                // Bit of a hack, so that folds sit at top even when sorted alphabetically, need to come up with something nicer than this.
                if (itemToAdd is QuestionFolder || itemToAdd is OutcomeFolder || itemToAdd is SimpleOutcomeFolder)
                {
                    newNode.Text = '\u0020' + nodeName;
                }
                else
                {
                    newNode.Text = nodeName;
                }

                // Add nodelets for each field if the question uses a picker with fields
                if (itemToAdd is Question)
                {
                    Question q = (Question)itemToAdd;
                    if (q.DisplayType >= QuestionDisplayType.Extension1 && _package.ExtensionMapping[q.DisplayType].FieldNames.Length > 0)
                    {
                        foreach (string s in _package.ExtensionMapping[q.DisplayType].FieldNames)
                        {
                            TreeNode fieldNode = new TreeNode(s);
                            fieldNode.ImageIndex = (int)TreeIcons.StructField;
                            fieldNode.SelectedImageIndex = fieldNode.ImageIndex;
                            fieldNode.Tag = s;
                            newNode.Nodes.Add(fieldNode);
                        }
                    }
                }

                newNode.Tag = nodeTag;
                newNode.SelectedImageIndex = nodeImageIndex;
                newNode.ImageIndex = nodeImageIndex;

                parentNode.Nodes.Add(newNode);

                // See if there's already one, and if so, add to its collection in the nodemap
                PfTreeNodeCollection c = guidNodeMap[itemToAdd.UniqueIdentifier];
                if (c == null)
                {
                    c = new PfTreeNodeCollection();
                    guidNodeMap[itemToAdd.UniqueIdentifier] = c;
                }
                else
                {
                    if (c.Count > 1 && !(itemToAdd.AllowDuplicatesInPackage))
                    {
                        throw new Exception(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlsMini.DuplicateItem"));
                    }
                }

                c.Add(newNode);

                tvwExplorer.SelectedNode = newNode;
            }
            finally
            {
                Cursor = Cursors.Default;

                if (!suspended)
                {
                    tvwExplorer.Sorted = false;
                    tvwExplorer.Sorted = true;
                    tvwExplorer.Refresh();
                }
            }
        }

        private void DisableControl()
        {
            tvwExplorer.Enabled = false;
        }

        private void EnableControl()
        {
            tvwExplorer.Enabled = true;
        }

        private void PackageNameBind()
        {
            rootNode.Text = _package.Name;
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExplorerControlMini2));
            this.tvwExplorer = new System.Windows.Forms.TreeView();
            this.ilistTreeView = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // tvwExplorer
            // 
            resources.ApplyResources(this.tvwExplorer, "tvwExplorer");
            this.tvwExplorer.HideSelection = false;
            this.tvwExplorer.ImageList = this.ilistTreeView;
            this.tvwExplorer.Name = "tvwExplorer";
            this.tvwExplorer.Sorted = true;
            this.tvwExplorer.DragDrop += new System.Windows.Forms.DragEventHandler(this.tvwExplorer_DragDrop);
            this.tvwExplorer.DragOver += new System.Windows.Forms.DragEventHandler(this.tvwExplorer_DragOver);
            this.tvwExplorer.DoubleClick += new System.EventHandler(this.tvwExplorer_DoubleClick);
            this.tvwExplorer.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwExplorer_AfterSelect);
            this.tvwExplorer.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvwExplorer_ItemDrag);
            // 
            // ilistTreeView
            // 
            this.ilistTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilistTreeView.ImageStream")));
            this.ilistTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ilistTreeView.Images.SetKeyName(0, "");
            this.ilistTreeView.Images.SetKeyName(1, "");
            this.ilistTreeView.Images.SetKeyName(2, "");
            this.ilistTreeView.Images.SetKeyName(3, "");
            this.ilistTreeView.Images.SetKeyName(4, "");
            this.ilistTreeView.Images.SetKeyName(5, "");
            this.ilistTreeView.Images.SetKeyName(6, "");
            this.ilistTreeView.Images.SetKeyName(7, "");
            this.ilistTreeView.Images.SetKeyName(8, "");
            this.ilistTreeView.Images.SetKeyName(9, "");
            this.ilistTreeView.Images.SetKeyName(10, "");
            this.ilistTreeView.Images.SetKeyName(11, "");
            this.ilistTreeView.Images.SetKeyName(12, "");
            this.ilistTreeView.Images.SetKeyName(13, "");
            this.ilistTreeView.Images.SetKeyName(14, "");
            this.ilistTreeView.Images.SetKeyName(15, "");
            this.ilistTreeView.Images.SetKeyName(16, "");
            this.ilistTreeView.Images.SetKeyName(17, "");
            this.ilistTreeView.Images.SetKeyName(18, "");
            this.ilistTreeView.Images.SetKeyName(19, "");
            this.ilistTreeView.Images.SetKeyName(20, "");
            this.ilistTreeView.Images.SetKeyName(21, "");
            this.ilistTreeView.Images.SetKeyName(22, "");
            this.ilistTreeView.Images.SetKeyName(23, "");
            this.ilistTreeView.Images.SetKeyName(24, "");
            this.ilistTreeView.Images.SetKeyName(25, "");
            this.ilistTreeView.Images.SetKeyName(26, "");
            this.ilistTreeView.Images.SetKeyName(27, "");
            this.ilistTreeView.Images.SetKeyName(28, "");
            this.ilistTreeView.Images.SetKeyName(29, "");
            // 
            // ExplorerControlMini2
            // 
            this.Controls.Add(this.tvwExplorer);
            resources.ApplyResources(this, "$this");
            this.Name = "ExplorerControlMini2";
            this.ResumeLayout(false);

        }
        #endregion

        private void tvwExplorer_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if ((_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_WORDML)
                || _dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_HTML))
                && (e.Item != null && e.Item is TreeNode))
            {
                object t = ((TreeNode)e.Item).Tag;
                if (t != null)
                {
                    tvwExplorer.SelectedNode = (TreeNode)e.Item;
                    DataObject obj = new DataObject();

                    if (_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_HTML))
                    {
                        if (t is IStringBindingItem)
                        {
                            ((IStringBindingItem)t).AddHTMLTagToObject(obj, null);
                        }
                        else 
                            if ( t is string)
                        {
                            string fieldName = t.ToString();
                            Question q = ((TreeNode)e.Item).Parent.Tag as Question;
                            if (q != null)
                            {
                                tvwExplorer.SelectedNode = (TreeNode)e.Item;
                                obj = new DataObject();

                                obj.SetData("Perfectus.Common.Package.PackageItem", q);
                                StringBinding.AddHTMLTagToDataObject(obj, q, fieldName);
                            }
                        }
                    }
                    else
                        if (_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_WORDML) &&
                            (t is Question ||
                            t is PFunction ||
                            t is Outcome))
                        {
                            PackageItem p = t as PackageItem;
                            obj.SetData("Perfectus.Common.Package.PackageItem", ((TreeNode)e.Item).Tag);
                            WordTemplateDocument2.AddWordMLTagToDataObject(obj, p, p.Name);
                        }
                        else
                            return;
                    DoDragDrop(obj, DragDropEffects.All);
                }
            }
        }
       
        private void tvwExplorer_DoubleClick(object sender, EventArgs e)
        {
            if (tvwExplorer.SelectedNode != null)
                OnDblClick();
        }

        private void tvwExplorer_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (!suspended)
            if (e.Node != null)
                OnSelectionValid(true, e.Node.Tag as LibraryItem);
        }


        internal event SelectorGlobal.WrapSelectionWithSimpleOutcome wrapSelectionWithSimpleOutcome;
        private void OnWrapSelectionWithSimpleOutcome(SimpleOutcome so)
        {
            if (wrapSelectionWithSimpleOutcome != null)
            {
                wrapSelectionWithSimpleOutcome(so);
            }
        }
        
        private void MoveNode(TreeNode newNode, TreeNode targetNode)
        {
            if (newNode != null && targetNode != null)
            {
                newNode.Remove();
                targetNode.Nodes.Add(newNode);
                tvwExplorer.SelectedNode = newNode;
                newNode.EnsureVisible();
            }
        }

        private void RecursiveGetAllNodes(TreeNode startNode, PfTreeNodeCollection nodesCollectionToBuild)
        {
            nodesCollectionToBuild.Add(startNode);

            foreach (TreeNode n in startNode.Nodes)
            {
                nodesCollectionToBuild.Add(n);
                if (n.Nodes.Count > 0)
                {
                    foreach (TreeNode subn in n.Nodes)
                    {
                        RecursiveGetAllNodes(subn, nodesCollectionToBuild);
                    }
                }
            }
        }


        private TreeNode GetNodeByDropTarget(PackageItem p, TreeNode startNode)
        {
            PfTreeNodeCollection tnc = new PfTreeNodeCollection();

            // walk treeNodes, adding all the time to our treeNode collection
            RecursiveGetAllNodes(startNode, tnc);

            foreach (TreeNode n in tnc)
            {
                if (n.Tag == p)
                {
                    return n;
                }
            }
            return null;
        }

        private void HandleDragWordMLData(object t, TreeNode n, DragEventArgs e)
        {
            if (t is SimpleOutcomeCollection || t is SimpleOutcomeFolder)
            {
                SimpleOutcome so = null;
                
                if (t is SimpleOutcomeFolder)
                    so = _package.CreateSimpleOutcome(t as SimpleOutcomeFolder);
                else
                    so = _package.CreateSimpleOutcome();

                ObjectNameEditorDialog2 dlg = new ObjectNameEditorDialog2();
                dlg.ItemName = so.Name;
                DialogResult dr = dlg.ShowDialog();

                if (dr == DialogResult.OK)
                {
                    if (dlg.ItemName != so.Name)
                        so.Name = dlg.ItemName;

                    OnWrapSelectionWithSimpleOutcome(so);

                    AddNode(so);
                }
                else
                    _package.DeleteItem(so, true);


            }
            else if (t is SimpleOutcome)
            {
                OnWrapSelectionWithSimpleOutcome((SimpleOutcome)t);
            }
        }

        private void tvwExplorer_DragDrop(object sender, DragEventArgs e)
        {
            Point pt = tvwExplorer.PointToClient(new Point(e.X, e.Y));

            // Node being dropped onto at the given point
            TreeNode n = tvwExplorer.GetNodeAt(pt);

            Application.DoEvents();

            if (DragDataIsWordML(e.Data))
                HandleDragWordMLData(n.Tag, n, e);
        }

        private Boolean ElementIsConditionalText(TreeNode param_node)
        {
            while (param_node != conditionalTextNode &&
                param_node.Parent != null)
                param_node = param_node.Parent;
            return param_node == conditionalTextNode;
        }

        private void tvwExplorer_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.None;
            Point pt = tvwExplorer.PointToClient(new Point(e.X, e.Y));

            // Scroll if need be. there must be a better way to get the location, this seems a bit long winded?	
            if (e.Y < ParentForm.Location.Y + Location.Y + tvwExplorer.Location.Y + 100)
                            SendMessage(tvwExplorer.Handle, WM_VSCROLL, (IntPtr)SB_PAGEUP, IntPtr.Zero);
                        else if (e.Y > ParentForm.Location.Y + Location.Y + tvwExplorer.Location.Y + tvwExplorer.Height + 40)
                            SendMessage(tvwExplorer.Handle, WM_VSCROLL, (IntPtr)SB_PAGEDOWN, IntPtr.Zero);
            
            // The current version is limited to accept WordML onto conditional Text (or Folder)
            // 1. Is it WordML?
            if ( ! DragDataIsWordML(e.Data) )
                return;

            // 2. Target is conditional outcome?
            TreeNode n = tvwExplorer.GetNodeAt(pt);
            if (ElementIsConditionalText ( n ))
            {
                n.Expand();
                e.Effect= DragDropEffects.Copy;
            }
        }

        /// <summary>
        ///		Evaluates whether a drag drop action onto a treenode contains WordML data.
        /// </summary>
        /// <param name="obj">The data of the object being dropped.</param>
        /// <returns>boolean indicating whether the drag drop contains WordML Data.</returns>
        /// 
        /// ToDo: Duplicated from ExplorerControl.cs - refactor later
        private bool DragDataIsWordML(IDataObject obj)
        {
            if (obj.GetDataPresent(DataFormats.Rtf, false) && obj.GetDataPresent(DataFormats.Html, false) &&
                obj.GetDataPresent("Woozle"))
                return true;
            else
                return false;
        }

    }
}
