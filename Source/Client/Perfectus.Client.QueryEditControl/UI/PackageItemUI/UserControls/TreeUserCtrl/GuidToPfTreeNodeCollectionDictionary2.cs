using System;
using System.Collections;

namespace Perfectus.Client.Studio.UI.PackageExplorer
{
	/// <summary>
	/// A dictionary with keys of type Guid and values of type PfTreeNodeCollection
	/// </summary>
	public class GuidToPfTreeNodeCollectionDictionary : DictionaryBase
	{
		/// <summary>
		/// Initializes a new empty instance of the GuidToPfTreeNodeCollectionDictionary class
		/// </summary>
		public GuidToPfTreeNodeCollectionDictionary()
		{
			// empty
		}

		/// <summary>
		/// Gets or sets the PfTreeNodeCollection associated with the given Guid
		/// </summary>
		/// <param name="key">
		/// The Guid whose value to get or set.
		/// </param>
		public virtual PfTreeNodeCollection this[Guid key]
		{
			get { return (PfTreeNodeCollection) this.Dictionary[key]; }
			set { this.Dictionary[key] = value; }
		}

		/// <summary>
		/// Adds an element with the specified key and value to this GuidToPfTreeNodeCollectionDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to add.
		/// </param>
		/// <param name="value">
		/// The PfTreeNodeCollection value of the element to add.
		/// </param>
		public virtual void Add(Guid key, PfTreeNodeCollection value)
		{
			this.Dictionary.Add(key, value);
		}

		/// <summary>
		/// Determines whether this GuidToPfTreeNodeCollectionDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this GuidToPfTreeNodeCollectionDictionary.
		/// </param>
		/// <returns>
		/// true if this GuidToPfTreeNodeCollectionDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool Contains(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this GuidToPfTreeNodeCollectionDictionary contains a specific key.
		/// </summary>
		/// <param name="key">
		/// The Guid key to locate in this GuidToPfTreeNodeCollectionDictionary.
		/// </param>
		/// <returns>
		/// true if this GuidToPfTreeNodeCollectionDictionary contains an element with the specified key;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsKey(Guid key)
		{
			return this.Dictionary.Contains(key);
		}

		/// <summary>
		/// Determines whether this GuidToPfTreeNodeCollectionDictionary contains a specific value.
		/// </summary>
		/// <param name="value">
		/// The PfTreeNodeCollection value to locate in this GuidToPfTreeNodeCollectionDictionary.
		/// </param>
		/// <returns>
		/// true if this GuidToPfTreeNodeCollectionDictionary contains an element with the specified value;
		/// otherwise, false.
		/// </returns>
		public virtual bool ContainsValue(PfTreeNodeCollection value)
		{
			foreach (PfTreeNodeCollection item in this.Dictionary.Values)
			{
				if (item == value)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the element with the specified key from this GuidToPfTreeNodeCollectionDictionary.
		/// </summary>
		/// <param name="key">
		/// The Guid key of the element to remove.
		/// </param>
		public virtual void Remove(Guid key)
		{
			this.Dictionary.Remove(key);
		}

		/// <summary>
		/// Gets a collection containing the keys in this GuidToPfTreeNodeCollectionDictionary.
		/// </summary>
		public virtual ICollection Keys
		{
			get { return this.Dictionary.Keys; }
		}

		/// <summary>
		/// Gets a collection containing the values in this GuidToPfTreeNodeCollectionDictionary.
		/// </summary>
		public virtual ICollection Values
		{
			get { return this.Dictionary.Values; }
		}
	}
}