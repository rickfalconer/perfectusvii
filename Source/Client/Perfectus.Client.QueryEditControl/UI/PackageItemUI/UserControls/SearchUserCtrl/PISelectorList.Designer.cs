using System.Windows.Forms;
namespace Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm
{
    partial class QuestionSelectorList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuestionSelectorList));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listView = new System.Windows.Forms.ListView();
            this.columnHeaderName = new System.Windows.Forms.ColumnHeader(((int)(resources.GetObject("listView.Columns"))));
            this.columnHeaderFolder = new System.Windows.Forms.ColumnHeader(((int)(resources.GetObject("listView.Columns1"))));
            this.imageQuestiontypeList = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.Name = "textBox1";
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // listView
            // 
            resources.ApplyResources(this.listView, "listView");
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderName,
            this.columnHeaderFolder});
            this.listView.FullRowSelect = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.HideSelection = false;
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowGroups = false;
            this.listView.SmallImageList = this.imageQuestiontypeList;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            this.listView.DoubleClick += new System.EventHandler(this.listView_DoubleClick);
            this.listView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listView_ItemSelectionChanged);
            this.listView.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.listView_ItemDrag);
            // 
            // columnHeaderName
            // 
            resources.ApplyResources(this.columnHeaderName, "columnHeaderName");
            // 
            // columnHeaderFolder
            // 
            resources.ApplyResources(this.columnHeaderFolder, "columnHeaderFolder");
            // 
            // imageQuestiontypeList
            // 
            this.imageQuestiontypeList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageQuestiontypeList.ImageStream")));
            this.imageQuestiontypeList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageQuestiontypeList.Images.SetKeyName(0, "");
            this.imageQuestiontypeList.Images.SetKeyName(1, "");
            this.imageQuestiontypeList.Images.SetKeyName(2, "");
            this.imageQuestiontypeList.Images.SetKeyName(3, "");
            this.imageQuestiontypeList.Images.SetKeyName(4, "");
            this.imageQuestiontypeList.Images.SetKeyName(5, "");
            this.imageQuestiontypeList.Images.SetKeyName(6, "");
            this.imageQuestiontypeList.Images.SetKeyName(7, "");
            this.imageQuestiontypeList.Images.SetKeyName(8, "");
            this.imageQuestiontypeList.Images.SetKeyName(9, "document_gear.png");
            this.imageQuestiontypeList.Images.SetKeyName(10, "document_into.png");
            // 
            // QuestionSelectorList
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.listView);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "QuestionSelectorList";
            this.Load += new System.EventHandler(this.QuestionSelectorList_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox textBox1;
        private ListView listView;
        private ColumnHeader columnHeaderName;
        private ColumnHeader columnHeaderFolder;
        private ImageList imageQuestiontypeList;
    }
}
