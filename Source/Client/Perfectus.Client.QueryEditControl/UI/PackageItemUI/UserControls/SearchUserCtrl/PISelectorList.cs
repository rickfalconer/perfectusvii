using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm
{
    /// <summary>
    /// QuestionSelectorList
    /// User control with a list of questions and a textbox for user input
    /// to select a question.
    /// The control tests input 'while typing' for valid combinations and 
    /// adjusts the list
    /// </summary>
    internal partial class QuestionSelectorList : UserControl, IItemViewType
    {
        const int ImageIndexNone = 0;
        const int ImageIndexQText = 1;
        const int ImageIndexQYesNo = 2;
        const int ImageIndexQNumber = 3;
        const int ImageIndexQDate = 4;
        const int ImageIndexPNumber = 5;
        const int ImageIndexPDate = 6;
        const int ImageIndexHQuestion = 7;
        const int ImageIndexHFolder = 8;
        const int ImageIndexOutcome = 9;
        const int ImageIndexConditionalText = 10;
        

        CItems<CItem> _items;

        // used to remember a configuration selection
        int _lastLength;
        int _lastLower;
        int _lastUpper;

        /// <summary>
        /// CItem
        /// Helper class (wrapper) around a question
        /// </summary>
        internal class CItem
        {
            private LibraryItem _item;
            public LibraryItem Item
            {
                get { return _item; }
            }

            private String _name;
            public String Name
            {
                get { return _name; }
            }
            private String _folder;
            public String Folder
            {
                get { return _folder; }
            }
            private int _imageIndex;
            public int ImageIndex
            {
                get { return _imageIndex; }
            }


            internal CItem(LibraryItem param_item)
                : this(param_item.Name, String.Empty)
            {
                Folder f = param_item.ParentFolder;

                while (f != null)
                {
                    if (_folder.Length > 0)
                        _folder = String.Format("{0}/{1}", f.Name, _folder);
                    else
                        _folder = f.Name;
                    f = f.ParentFolder;
                }
                _item = param_item;


                // Set image index
                if (param_item is Question)
                    switch ((param_item as Question).DataType)
                    {
                        case Perfectus.Common.PerfectusDataType.Text:
                            _imageIndex = ImageIndexQText;
                            break;
                        case Perfectus.Common.PerfectusDataType.Number:
                            _imageIndex = ImageIndexQNumber;
                            break;
                        case Perfectus.Common.PerfectusDataType.YesNo:
                            _imageIndex = ImageIndexQYesNo;
                            break;
                        case Perfectus.Common.PerfectusDataType.Date:
                            _imageIndex = ImageIndexQDate;
                            break;
                    }
                else
                    if (param_item is DatePFunction)
                        _imageIndex = ImageIndexPDate;
                    else
                        if (param_item is ArithmeticPFunction)
                            _imageIndex = ImageIndexPNumber;
                        else
                            if (param_item is Outcome)
                                _imageIndex = ImageIndexOutcome;
                            else
                                if (param_item is SimpleOutcome)
                                    _imageIndex = ImageIndexConditionalText;
                                else
                                    _imageIndex = ImageIndexNone;

            }

            //Used for searching and sorting
            internal CItem(String param_name)
                : this(param_name, String.Empty) { }
            private CItem(String param_name, String param_folder)
            {
                _name = param_name;
                _folder = param_folder;
            }
            public override string ToString()
            {
                if (_folder != null && _folder.Length > 0)
                    return String.Format("{0} \t[{1}]", Name, Folder);
                else
                    return Name;
            }
        }
        /// <summary>
        /// Case insensitive comparer for CItem instances
        /// </summary>
        public class CompareItemNames : IComparer<CItem>
        {
            public int Compare(CItem x, CItem y)
            {
                return String.Compare(x.Name, y.Name, true);
            }
        }

        /// <summary>
        /// CItems
        /// (Shadow-) list of available questions
        /// All configuration and sort operations will be performed against this list.
        /// The list view will then be loaded with a subset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        internal class CItems<T> : List<T>
        {
            internal CItems() { }

            internal
            String[] GetNames()
            {
                return ConvertAll<String>(new Converter<T, String>(Convertx)).ToArray();
            }
            internal String Convertx(T obj)
            {
                return obj.ToString();
            }
        }

        // Constructor
        internal QuestionSelectorList()
        {
            InitializeComponent();
            _items = new CItems<CItem>();
            _lastLength = 0;
            _lastLower = 0;
            _lastUpper = 0;

            _typesToShow = new List<SelectorGlobal.VisiblePackageItemType>();
            _dataTypes = new List<PerfectusDataType>();
            _dropTargetType = new List<SelectorGlobal.DragNDropOperation>();
        }


        internal int Count { get { return _items.Count; } }
        // Caller uses this method to fill the list
        internal void Add(LibraryItem param_question)
        {
            _items.Add(new CItem(param_question));
        }


        LibraryItem defaultItem = null;
        public void Reset(LibraryItem param_select_item)
        {
            defaultItem = param_select_item;

            if ( _package == null
                || !Created)
                return;

            // Empty data
            this.textBox1.Clear();
            this._items.Clear();


            // Questions
            // Load question listbox
            if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.QUESTION))
                foreach (Question question in _package.Questions)
                    if (_dataTypes.Contains(question.DataType))
                        Add(question);

            // Functions
            if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.FUNCTION))
                foreach (PFunction function in _package.Functions)
                    if (_dataTypes.Contains(function.DataType))
                        Add(function);
            
            // Outcomes
            if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.OUTCOME))
                foreach (Outcome outcome in _package.Outcomes)
                    Add(outcome);

            // conditional Text
            if (_typesToShow.Contains(SelectorGlobal.VisiblePackageItemType.CONDITIONALTEXT))
                foreach (SimpleOutcome simple in _package.SimpleOutcomes)
                    Add(simple);

            Publish(-1, -1);
            if (param_select_item != null)
            {
                ListViewItem item = listView.FindItemWithText(param_select_item.Name);
                item.Selected = true;
                listView.TopItem = item;
                listView.Select();
            }
            else
                listView.SelectedItems.Clear();
        }
        private void QuestionSelectorList_Load(object sender, EventArgs e)
        {
            Reset(defaultItem);
        }
        // Copy the valid range of questions from the shadow list
        // into the list view
        private void Publish(int param_lower, int param_upper)
        {
            listView.Items.Clear();

            int len = _items.Count;
            int srcIdx = 0;

            // param_lower is -1 for complete redraw 
            if (param_lower >= 0 && param_lower < len)
                srcIdx = param_lower;

            // Test if this is a valid subset
            if (param_upper >= 0 && param_upper < len && param_upper >= param_lower)
                len = param_upper - param_lower + 1;

            // Sort() is necessary for subsequent calls to BinarySearch()
            _items.Sort(new CompareItemNames());

            for (int idx = srcIdx; idx < srcIdx + len; idx++)
            {
                ListViewItem item = new ListViewItem(new String[] { _items[idx].Name, _items[idx].Folder });
                item.ImageIndex = _items[idx].ImageIndex;
                listView.Items.Add(item);
            }
            listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize );
        }


        private Boolean GetValidRange(String param_cmp_str, ref int param_match, ref int param_lower, ref int param_upper)
        {
            // Simple case - no configuration required
            if (param_cmp_str.Length == 0)
            {
                param_lower = 0;
                param_upper = Count - 1;
                return true;
            }

            IComparer<CItem> comparer = new CompareItemNames();

            // An index greater or equal is an exact match. Consider
            // questions 'AB' and 'ABC'. If we find 'AB' the we still need to
            // show 'ABC'
            int index = param_match = _items.BinarySearch(new CItem(param_cmp_str), comparer);

            // We have no exact match, but maybe a selection 'in-range'
            if (index < 0)
                index = ~index;

            if (index >= 0 && index < _items.Count)
            {
                String entry = _items[index].Name;

                if (entry.StartsWith(param_cmp_str, true, null))
                {
                    // We have 'ABC', 'ABD' and 'ABE'
                    // Search string is 'AB'.
                    // Hence we look for 'AC' to get all items that are 
                    // greater 'AB' and smaller 'AC'
                    param_lower = index;
                    Byte b = (Byte)param_cmp_str[param_cmp_str.Length - 1];
                    b++;
                    param_cmp_str = param_cmp_str.Substring(0, param_cmp_str.Length - 1) + (Char)b;

                    index = _items.BinarySearch(new CItem(param_cmp_str), comparer);
                    if (index < 0)
                    {
                        index = ~index;
                        // No match at all
                        if (index == -1)
                            param_upper = -1;
                        // Last valid element if before the found location
                        param_upper = Math.Max(param_lower, index - 1);
                    }
                    else
                        // Have an exact match
                        param_upper = index - 1;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Input key pressed
        /// Test if a valid selection can exist, if the 
        /// character appends the configuration text
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">key info</param>
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // We do not handle any control code here
            if (Char.IsDigit(e.KeyChar) ||
                Char.IsLetter(e.KeyChar) ||
                Char.IsPunctuation(e.KeyChar) ||
                Char.IsSeparator(e.KeyChar))
            {
                // if "this.textBox1.Text + e.KeyChar" return an empty result, then 
                // we discard the pressed key
                int dummy0 = 0, dummy1 = 0, dummy2 = 0;
                e.Handled = !GetValidRange(this.textBox1.Text + e.KeyChar, ref dummy0, ref dummy1, ref dummy2);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            String searchString = ((TextBox)sender).Text;

            // Expand the result set if the user pressed Del or Backspace
            Boolean backMode = searchString.Length < _lastLength;

            int lower = 0, upper = 0, match = 0;

            if (backMode)
            {
                String newString = searchString;
                while (
                    GetValidRange(newString, ref match, ref lower, ref upper) &&
                    lower >= _lastLower &&
                    upper <= _lastUpper &&
                    newString.Length > 0)
                    newString = newString.Substring(0, newString.Length - 1);

                _lastLower = lower;
                _lastUpper = upper;
                _lastLength = newString.Length;

                // We WANT recursively  call ourself. But that only happen, if
                // the strings are not identical. In that case we call ourself explicitly 
                if (String.Compare(this.textBox1.Text, newString) == 0)
                    textBox1_TextChanged(textBox1, EventArgs.Empty);
                else
                    this.textBox1.Text = newString;

            }
            else
            {
                if (!GetValidRange(searchString, ref match, ref lower, ref upper))
                    this.textBox1.Text = this.textBox1.Text.Substring(0, textBox1.Text.Length - 1);


                // maybe extend the text
                // Example: Users types 'Qu' and the next match would be 'Question1', 'Question11'.
                //          Then we want to extend the text to 'Question'
                // Extend the word until we narrow the result
                int tmp_match = match, tmp_lower = lower, tmp_upper = upper;

                if (_items[lower].Name.Length > searchString.Length)
                {
                    searchString = _items[lower].Name.Substring(0, searchString.Length + 1);

                    if (GetValidRange(searchString, ref tmp_match, ref tmp_lower, ref tmp_upper) &&
                        tmp_lower == lower && tmp_upper == upper)
                    {
                        this.textBox1.Text = searchString;
                        return;
                    }
                }

                if (lower == upper &&
                    lower >= 0 &&
                    String.Compare(_items[lower].Name, this.textBox1.Text, true) != 0)
                {
                    this.textBox1.Text = _items[lower].Name;
                    this.textBox1.SelectionStart = this.textBox1.Text.Length;
                }
                _lastLower = lower;
                _lastUpper = upper;
                _lastLength = this.textBox1.Text.Length;
                Publish(lower, upper);

                Boolean valid = lower == upper && lower >= 0 || match > 0;

                // We found an entry, 
                // select the entry and scoll into view position
                ListViewItem item = listView.FindItemWithText(_items[lower].Name);
                item.Selected = true;

                //OnSelectionValid(valid, valid ? _items[lower].Question : null);
                this.textBox1.SelectionStart = this.textBox1.Text.Length;
            }
        }

        private void listView_DoubleClick(object sender, EventArgs e)
        {
            if (listView.SelectedItems.Count != 1)
                return;
            OnDblClick();
        }

        private void listView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                if ((listView.SelectedItems.Count != 1) || (!e.Item.Selected))
                    return;
                int index = _items.BinarySearch(new CItem(e.Item.Text), new CompareItemNames());
                if (index >= 0)
                    OnSelectionValid(true, _items[index].Item);
                else
                    OnSelectionValid(false, null);
            }
        }

        private void listView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            // Configured as a drop source?
            if (!_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_WORDML) &&
                !_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_HTML))
                return;

            DataObject obj = new DataObject();

            int index = _items.BinarySearch(new CItem(((ListViewItem)e.Item).Text), new CompareItemNames());

            if (index >= 0)
            {
                LibraryItem li = _items[index].Item as LibraryItem;

                if (null != li)
                {
                    if (_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_HTML) &&
                        li is IStringBindingItem)
                    {
                        ((IStringBindingItem)li).AddHTMLTagToObject(obj, null);
                    }
                    else
                        if (_dropTargetType.Contains(SelectorGlobal.DragNDropOperation.SOURCE_WORDML) &&
                            (li is Question ||
                            li is PFunction ||
                            li is Outcome))
                        {
                            obj.SetData("Perfectus.Common.Package.PackageItem", li);

                            WordTemplateDocument2.AddWordMLTagToDataObject(obj, li, li.Name);
                        }
                        else
                            return;
                    DoDragDrop(obj, DragDropEffects.All);
                }

            }
        }

        #region IItemViewType Members

        /// <summary>
        /// Delegate SelectionValidCbk
        /// Fires if the user selected a question.
        /// For example: The subscriber could enable the 'Accept' button.
        /// </summary>
        /// <param name="param_valid">Is exactly one question selected</param>
        /// <param name="param_question">The question or null</param>
        public event SelectorGlobal.SelectionValidCbk selectionValid;
        private void OnSelectionValid(Boolean param_valid, LibraryItem param_item)
        {
            if (selectionValid != null)
                selectionValid(param_valid, param_item);
        }

        public event SelectorGlobal.ItemSelectedCbk dblClick;
        private void OnDblClick()
        {
            if (dblClick != null)
                dblClick();
        }

        Package _package;
        public Package Package
        {
            set
            {
                _package = value;
                Reset(null);
            }
        }

        private List<SelectorGlobal.VisiblePackageItemType> _typesToShow;
        public void AddPackageItemType(SelectorGlobal.VisiblePackageItemType param_showItem)
        {
            _typesToShow.Add(param_showItem);
        }

        private List<SelectorGlobal.DragNDropOperation> _dropTargetType;
        public void AddDragNDropOperation(SelectorGlobal.DragNDropOperation dropTargetType)
        {
            _dropTargetType.Add(dropTargetType);
            Reset(null);
        }

        private List<PerfectusDataType> _dataTypes;
        public void AddDatatype(PerfectusDataType param_showType)
        {
            _dataTypes.Add(param_showType);
            Reset(null);
        }

        void IItemViewType.maybeSelectItem(LibraryItem param_select_item)
        {

            ListViewItem item = listView.FindItemWithText(param_select_item.Name);
            item.Selected = true;
            listView.TopItem = item;
//            listView.Select();

        }

        #endregion
    }
}
