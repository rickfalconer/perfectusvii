using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm
{
    public partial class QuestionSelectorConstantDatepicker : Form
    {
        private DateTime _dateTime;

        public DateTime dateTime
        {
            get { return _dateTime; }
        }
	
        public QuestionSelectorConstantDatepicker(DateTime param_datetime)
        {
            InitializeComponent();
            _dateTime = param_datetime;
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            _dateTime = e.Start;
            Close();
        }
    }
}