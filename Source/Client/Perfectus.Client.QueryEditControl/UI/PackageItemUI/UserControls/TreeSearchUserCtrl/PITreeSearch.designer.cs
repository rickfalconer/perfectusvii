using System.Windows.Forms;
namespace Perfectus.Client.Studio.QueryEditControl
{
    partial class CPITreeSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        //this.questionSelectorList1 = new Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorList();
            
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CPITreeSearch));
            this.questionSelectorList1 = new Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorList();
            this.tabTreeView = new System.Windows.Forms.TabPage();
            this.treePackageItem = new Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlMini2();
            this.tabSearchView = new System.Windows.Forms.TabPage();
            this.tabPackageItems = new System.Windows.Forms.TabControl();
            this.tablPageImageList = new System.Windows.Forms.ImageList(this.components);
            this.tabTreeView.SuspendLayout();
            this.tabSearchView.SuspendLayout();
            this.tabPackageItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // questionSelectorList1
            // 
            resources.ApplyResources(this.questionSelectorList1, "questionSelectorList1");
            this.questionSelectorList1.Name = "questionSelectorList1";
            this.questionSelectorList1.dblClick += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.ItemSelectedCbk(this.list_dblClick);
            this.questionSelectorList1.selectionValid += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.SelectionValidCbk(this.list_selectionValid);
            // 
            // tabTreeView
            // 
            this.tabTreeView.Controls.Add(this.treePackageItem);
            resources.ApplyResources(this.tabTreeView, "tabTreeView");
            this.tabTreeView.Name = "tabTreeView";
            this.tabTreeView.UseVisualStyleBackColor = true;
            // 
            // treePackageItem
            // 
            resources.ApplyResources(this.treePackageItem, "treePackageItem");
            this.treePackageItem.Name = "treePackageItem";
            this.treePackageItem.dblClick += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.ItemSelectedCbk(this.list_dblClick);
            this.treePackageItem.selectionValid += new Perfectus.Client.Studio.QueryEditControl.SelectorGlobal.SelectionValidCbk(this.list_selectionValid);
            // 
            // tabSearchView
            // 
            this.tabSearchView.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tabSearchView.Controls.Add(this.questionSelectorList1);
            resources.ApplyResources(this.tabSearchView, "tabSearchView");
            this.tabSearchView.Name = "tabSearchView";
            this.tabSearchView.UseVisualStyleBackColor = true;
            // 
            // tabPackageItems
            // 
            resources.ApplyResources(this.tabPackageItems, "tabPackageItems");
            this.tabPackageItems.Controls.Add(this.tabTreeView);
            this.tabPackageItems.Controls.Add(this.tabSearchView);
            this.tabPackageItems.ImageList = this.tablPageImageList;
            this.tabPackageItems.Name = "tabPackageItems";
            this.tabPackageItems.SelectedIndex = 0;
            this.tabPackageItems.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabPackageItems.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabPackageItems_Selected);
            // 
            // tablPageImageList
            // 
            this.tablPageImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("tablPageImageList.ImageStream")));
            this.tablPageImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.tablPageImageList.Images.SetKeyName(0, "List_BulletsHS.png");
            this.tablPageImageList.Images.SetKeyName(1, "OrgChartHS.png");
            // 
            // CPITreeSearch
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabPackageItems);
            this.Name = "CPITreeSearch";
            this.Load += new System.EventHandler(this.questionSelectorList_Load);
            this.tabTreeView.ResumeLayout(false);
            this.tabSearchView.ResumeLayout(false);
            this.tabPackageItems.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TabPage tabTreeView;
        private Perfectus.Client.Studio.UI.PackageExplorer.ExplorerControlMini2 treePackageItem;
        private TabPage tabSearchView;
        private TabControl tabPackageItems;
        private ImageList tablPageImageList;
        private Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm.QuestionSelectorList questionSelectorList1;

    }
}
