using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.QueryEditControl.QuestionSelectorForm;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class CPITreeSearch : UserControl, IItemViewType
    {
        
        #region IItemViewType Members

        public event SelectorGlobal.ItemSelectedCbk dblClick;
        private void OnDblClick()
        {
            if (dblClick != null)
                dblClick();
        }

        public event SelectorGlobal.SelectionValidCbk selectionValid;
        private void OnSelectionValid(Boolean param_valid, LibraryItem param_item)
        {
            if (selectionValid != null)
                selectionValid(param_valid, param_item);
        }

        public Package Package
        {
            set
            {
                foreach (TabPage page in tabPackageItems.TabPages)
                    foreach (Control ctrl in page.Controls)
                        if (ctrl is IItemViewType)
                        {
                            ((IItemViewType)ctrl).Package = value;

                            break;
                        }
            }
        }


        public void AddPackageItemType(SelectorGlobal.VisiblePackageItemType param_showItem)
        {
            foreach ( TabPage page in tabPackageItems.TabPages )
                foreach ( Control ctrl in page.Controls )
                    if (ctrl is IItemViewType)
                    {
                        ((IItemViewType)ctrl).AddPackageItemType(param_showItem);
                        break;
                    }
            // // or
            //this.treePackageItem.AddPackageItemType(param_showItem);
            //this.questionSelectorList1.AddPackageItemType (param_showItem);
        }

        public void AddDragNDropOperation(SelectorGlobal.DragNDropOperation dropTargetType)
        {
            foreach ( TabPage page in tabPackageItems.TabPages )
                foreach ( Control ctrl in page.Controls )
                    if (ctrl is IItemViewType)
                    {
                        ((IItemViewType)ctrl).AddDragNDropOperation(dropTargetType);
                        break;
                    }
            // // or
            //this.treePackageItem.AddDragNDropOperation(dropTargetType);
            //this.questionSelectorList1.AddDragNDropOperation(dropTargetType);
        }        

        public void AddDatatype(PerfectusDataType param_showType)
        {
            foreach (TabPage page in tabPackageItems.TabPages)
                foreach (Control ctrl in page.Controls)
                    if (ctrl is IItemViewType)
                    {
                        ((IItemViewType)ctrl).AddDatatype(param_showType);
                        break;
                    }
            // // or
            //this.treePackageItem.AddDatatype(param_showType);
            //this.questionSelectorList1.AddDatatype(param_showType);
        }

        public void Reset(LibraryItem param_select_item)
        {
            questionSelectorList_Load(null, EventArgs.Empty);
        }        

    #endregion

        // The item shall be read-only
        // It's a copy for internal usage (select default item) of the hosting controls item
        // ToDo: implement callback for clarity
        private LibraryItem _answerProvider;
        public LibraryItem AnswerProviderItem
        {
            set {
                if (_answerProvider == null && (!(value is PackageConstant)))
                _answerProvider = value; }
        }

        public CPITreeSearch()
        {
            InitializeComponent();
        }

        private void questionSelectorList_Load(object sender, EventArgs e)
        {
            treePackageItem.wrapSelectionWithSimpleOutcome += new SelectorGlobal.WrapSelectionWithSimpleOutcome(OnWrapSelectionWithSimpleOutcome);

            tabPackageItems.SelectedTab = this.tabTreeView;
            tabPackageItems_Selected(tabPackageItems, new TabControlEventArgs (tabTreeView,0,TabControlAction.Selected));
        }

        void list_selectionValid(bool param_valid, LibraryItem param_item)
        {
            OnSelectionValid(param_valid, param_item);
        }

        void list_dblClick()
        {
            OnDblClick();
        }

        /// <summary>
        /// Changing from tree-view to list-view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tabPackageItems_Selected(object sender, TabControlEventArgs e)
        {
            list_selectionValid(false, null); // disable the Accept button

            if (e.Action == TabControlAction.Selected)
            {
                foreach (Control ctrl in e.TabPage.Controls)
                    if (ctrl is IItemViewType)
                    {
                        ((IItemViewType)ctrl).Reset(_answerProvider);
                        ctrl.Select();
                    }
            }
        }

        
        delegate void maybeSelectItemCallback(LibraryItem item);

        public void maybeSelectItem(LibraryItem param_select_item)
        {
            if (this.InvokeRequired)
            {
                maybeSelectItemCallback d = new maybeSelectItemCallback(maybeSelectItem);
                Invoke(d, param_select_item);
            }

            TabPage page = tabPackageItems.SelectedTab;
            foreach (Control ctrl in page.Controls)
                if (ctrl is IItemViewType)
                    ((IItemViewType)ctrl).maybeSelectItem(param_select_item);
        }

       
        // Propagate the event from the tree ctrl to the host
        internal event SelectorGlobal.WrapSelectionWithSimpleOutcome wrapSelectionWithSimpleOutcome;
        private void OnWrapSelectionWithSimpleOutcome(SimpleOutcome so)
        {
            if (wrapSelectionWithSimpleOutcome != null)
                wrapSelectionWithSimpleOutcome(so);
        }

    }
}
