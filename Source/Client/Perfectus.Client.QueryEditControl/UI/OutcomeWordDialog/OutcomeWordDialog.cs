using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
//using AxDSOFramer;
//using DSOFramer;
using AxEDWordLib;
using EDWordLib;
using Microsoft.Office.Interop.Word;
using Perfectus.Common;
using Application = Microsoft.Office.Interop.Word.Application;
using WordDocument = Microsoft.Office.Interop.Word.Document;
using Perfectus.Common.PackageObjects;
using System.Runtime.InteropServices;
using Perfectus.Client.Studio.QueryEditControl;
using System.Collections.Generic;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner.ActionControls
{
	/// <summary>
	/// Summary description for OutcomeWordDialog.
	/// </summary>
    public class OutcomeWordDialog : Form
    {
        private const String _PID_varName = "PID";
        public event SelectedPackageLibraryItemHandler PackageItemSelected;

        private Button btnOk;
        private Button btnCancel;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;
        //private AxFramerControl axFramerControl;
        private AxEDWord axFramerControl;
        private SplitContainer splitContainer1;
        private TextBox textBoxOutcomeName;
        private Label label1;

        private CPITreeSearch cpiTreeView1;
        private Package _package;
        private Outcome _outcome;
        private Int32 originalHash;
        private Panel panel1;

        private string tempFilePath;
        internal String OutcomeName { get { return this.textBoxOutcomeName.Text; } }

        public string OfficeOpenXML
        {
            get { return GetOfficeOpenXMLFromDisc(); }
            set
            {
                WordTemplateDocument2 t = new WordTemplateDocument2();
                t.OfficeOpenXMLToString = value;

                tempFilePath = t.FilePath;
                SendWordMLtoDisc( tempFilePath, value );
                SetTemplate(t);
            }
        }

        private void SendWordMLtoDisc( string path, string wordML )
        {
            if (wordML != null)
            {
                using (FileStream fs = File.OpenWrite( path ))
                {
                    byte[] bytes = Encoding.UTF8.GetBytes(wordML);
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
            }
        }


        private string GetOfficeOpenXMLFromDisc()
        {
            string officeOpenXML;

            using (FileStream sf = File.Open(tempFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                StreamReader sr = new StreamReader(sf);
                officeOpenXML = sr.ReadToEnd();
                sr.Close();
            }
            return officeOpenXML;
        }

        public OutcomeWordDialog(Package param_package, PackageItem param_outcome)
            : this (param_package, param_outcome, null)
        {
            this.textBoxOutcomeName.Enabled = false;
        }

        public OutcomeWordDialog(Package param_package, PackageItem param_outcome, String param_name)
        {
            if (!(param_outcome is Outcome))
                throw new Exception("Item must be outcome");

            _outcome = param_outcome as Outcome;

            //
            // Required for Windows Form Designer support
            //
            this._package = param_package;
            InitializeComponent();

            cpiTreeView1.Package = _package;

            cpiTreeView1.wrapSelectionWithSimpleOutcome += new SelectorGlobal.WrapSelectionWithSimpleOutcome(cpiTreeView1_wrapSelectionWithSimpleOutcome);

            this.cpiTreeView1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.QUESTION);
            this.cpiTreeView1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.OUTCOME);
            this.cpiTreeView1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.FUNCTION);
            this.cpiTreeView1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.CONDITIONALTEXT);

            this.cpiTreeView1.AddDatatype(PerfectusDataType.Number);
            this.cpiTreeView1.AddDatatype(PerfectusDataType.Text);
            this.cpiTreeView1.AddDatatype(PerfectusDataType.Date);
            this.cpiTreeView1.AddDatatype(PerfectusDataType.YesNo);

            this.cpiTreeView1.AddDragNDropOperation(SelectorGlobal.DragNDropOperation.SOURCE_WORDML);
            this.cpiTreeView1.AddDragNDropOperation(SelectorGlobal.DragNDropOperation.TARGET_WORDML);

            this.Text += param_outcome.Name;
            this.textBoxOutcomeName.Text = param_name;

            this.PackageItemSelected += new SelectedPackageLibraryItemHandler(OutcomeWordDialog_PackageItemSelected);
        }

        void OutcomeWordDialog_PackageItemSelected(object o, SelectedPackageItemEventArgs e, int foreignPid)
        {
            // Do not accept events from other processes
            if (System.Diagnostics.Process.GetCurrentProcess().Id != foreignPid)
                return;

            LibraryItem item = this._package.GetLibraryItemByUniqueIdentifier(e.UniqueIdentifier);
            if (item != null)
                cpiTreeView1.maybeSelectItem(item);
            else
            {
                string title = global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.DeletedPackageItemTitle;
                string message = global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.DeletedPackageItemMessage;
                message = string.Format(message, e.Name);

                // The selected item in the designer does not exist in the package - display warning to the user
                MessageBox.Show(DesktopWindow.Instance, message, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            // if (disposing=true), we can dispose of managed resources.
            if( disposing )
            {
                if (axFramerControl != null)
                {
                    //axFramerControl.Close();
                    axFramerControl.CloseDoc();
                    object oFalse = false;
                    object oMissing = Missing.Value;
                    axFramerControl.Dispose();
                    axFramerControl = null;
                }
                if (components != null)
                {
                    components.Dispose();
                    components = null;
                }
            }

            // Dispose of unmanaged resources.
            // Remove working files in temp folder.
            if( !String.IsNullOrEmpty( workingFilename ) )
            {
                File.Delete( workingFilename );
                workingFilename = null;
            }
            if( !String.IsNullOrEmpty( tempFilePath ) )
            {
                File.Delete( tempFilePath );
                tempFilePath = null;
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OutcomeWordDialog));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.textBoxOutcomeName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cpiTreeView1 = new Perfectus.Client.Studio.QueryEditControl.CPITreeSearch();
            this.axFramerControl = new AxEDWordLib.AxEDWord();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axFramerControl)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOk.Location = new System.Drawing.Point(501, 487);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "OK";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(582, 487);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            // 
            // textBoxOutcomeName
            // 
            this.textBoxOutcomeName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutcomeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBoxOutcomeName.Location = new System.Drawing.Point(227, 489);
            this.textBoxOutcomeName.MaxLength = 255;
            this.textBoxOutcomeName.Name = "textBoxOutcomeName";
            this.textBoxOutcomeName.Size = new System.Drawing.Size(258, 22);
            this.textBoxOutcomeName.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(104, 492);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Outcome text name:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Controls.Add(this.textBoxOutcomeName);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.btnOk);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(669, 519);
            this.panel1.TabIndex = 11;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cpiTreeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.axFramerControl);
            this.splitContainer1.Size = new System.Drawing.Size(669, 483);
            this.splitContainer1.SplitterDistance = 223;
            this.splitContainer1.TabIndex = 7;
            // 
            // cpiTreeView1
            // 
            this.cpiTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cpiTreeView1.Location = new System.Drawing.Point(0, 0);
            this.cpiTreeView1.Margin = new System.Windows.Forms.Padding(0);
            this.cpiTreeView1.Name = "cpiTreeView1";
            this.cpiTreeView1.Size = new System.Drawing.Size(223, 483);
            this.cpiTreeView1.TabIndex = 0;
            // 
            // axFramerControl
            // 
            this.axFramerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axFramerControl.Enabled = true;
            this.axFramerControl.Location = new System.Drawing.Point(0, 0);
            this.axFramerControl.Name = "axFramerControl";
            this.axFramerControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axFramerControl.OcxState")));
            this.axFramerControl.RightToLeft = false;
            this.axFramerControl.Size = new System.Drawing.Size(442, 483);
            this.axFramerControl.TabIndex = 0;
            // 
            // OutcomeWordDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(669, 519);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(400, 250);
            this.Name = "OutcomeWordDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Outcome: ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OutcomeWordDialog_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OutcomeWordDialog_FormClosed);
            this.Load += new System.EventHandler(this.OutcomeWordDialog_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axFramerControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private void SaveWordDoc()
        {
            if (tempFilePath == null)
            {
                tempFilePath = Path.GetTempFileName();
            }
            object oTempFilePath = tempFilePath;

            //object oFileFormat = WdSaveFormat.wdFormatFlatXML;
            object oFileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatFlatXML;

            object oMissing = Missing.Value;

            //WordDocument doc = axFramerControl.ActiveDocument as WordDocument;
            //Microsoft.Office.Interop.Word.Document doc = axFramerControl.ActiveDocument() as Microsoft.Office.Interop.Word.Document;

            if (axFramerControl.ActiveDocument() != null)
            {
                try
                {
                    //doc.SaveAs(ref oTempFilePath, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                    axFramerControl.SaveAs(tempFilePath, oFileFormat);
                }

                catch (COMException cex)
                {
                    int i = cex.ErrorCode;

                    switch (cex.ErrorCode)
                    {
                        case -2146823135: // Word cannot save to this filename (probably pending IO)
                            MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation")
                                .GetString("Perfectus.Client.UI.Designers.TemplateDesigner.CannotSavePendingIO"), Common.About.FormsTitle);
                            break;
                        default:
                            throw;
                    }


                }
            }
        }

        private void OutcomeWordDialog_Load(object sender, EventArgs e)
        {
            ResetUI();
            originalHash = GetOfficeOpenXMLFromDisc().GetHashCode();
        }

        /// <summary>
        /// //////////////////////////////////////////////
        /// </summary>


        String workingFilename;
        Object oWorkingFilename;
        private Application app = null;
        private WordDocument doc = null;


        protected void ResetUI()
        {
            Cursor = Cursors.WaitCursor;

            try
            {
                WordTemplateDocument2 _wordTemplateDocument = ItemBeingEdited as WordTemplateDocument2;

                if (_wordTemplateDocument != null)
                {
                    string templateFilename = _wordTemplateDocument.FilePath;
                    object oMissing = Missing.Value;
                    //object oFileFormat = WdSaveFormat.wdFormatFlatXML;
                    object oFileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatFlatXML;

                    workingFilename = Path.Combine( Path.GetTempPath( ), Path.GetRandomFileName( ) ) + ".xml";
                    oWorkingFilename = workingFilename;

                    // Make sure the user can't open other documents, create blank ones, or save this one somewhere funny.

                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileOpen, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileSaveAs, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileSave, false);
                    //axFramerControl.set_EnableFileCommand(dsoFileCommandType.dsoFileNew, false);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableNew, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableOpen, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSave, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSaveAs, true);
                    axFramerControl.DisableFileCommand(EDWordLib.WdUIType.wdUIDisableSaveAsMenu, true);

                    axFramerControl.Refresh();

                    if (_wordTemplateDocument.FileHasContent)
                    {
                        object oFalse = false;

                        // Create copy of the word document to open as a working copy within the framer control. 
                        File.Copy( templateFilename, workingFilename, true );

                        // FB2362: We must load the working copy instead, because if we have another application that hosts another framer control, 
                        // the other application will place a temporary lock on the file. Hence we use the working copy as we don't care if 
                        // another application puts a lock on the file. Its the temporary file 'templateFilename' that we actual save to when closing.
                        // This is a work around an obvious bug in the framer control. 
                        //axFramerControl.Open( workingFilename, oFalse, "Word.Document", oMissing, oMissing );
                        axFramerControl.Open(workingFilename);
                    }
                    else
                    {
                        //axFramerControl.CreateNew("Word.Document");
                        axFramerControl.NewDoc();
                        Save();
                    }

                    // If that worked and we have an active document...
                    //if (axFramerControl.ActiveDocument != null)
                    if (axFramerControl.ActiveDocument() != null)
                    {
                        //doc = axFramerControl.ActiveDocument as WordDocument;
                        doc = (Microsoft.Office.Interop.Word.Document)axFramerControl.ActiveDocument();
                        

                        if ( doc != null )
                        {
                            //app = doc.Application;
                            app = (Application)axFramerControl.GetApplication();

                            //CreateVarPID();

                            // Turn on the tag view
                            //app.ActiveWindow.View.ShowXMLMarkup = 1;
                            //if (!doc.FormsDesign)
                            //{
                            //    doc.ToggleFormsDesign();
                            //}

                            //Add event to word that will call us back so that we have ability to set properties on it
                            //doc.ContentControlAfterAdd += new DocumentEvents2_ContentControlAfterAddEventHandler(doc_ContentControlAfterAdd);


                            // Put Word into Normal View and Zoom to Fit.
                            //app.ActiveWindow.View.Type = WdViewType.wdNormalView;
                            //app.ActiveWindow.View.Zoom.PageFit = WdPageFit.wdPageFitBestFit;

                            // Add event to word document whenever the user selects a content control. Will update the UI.
                            doc.ContentControlOnEnter += new DocumentEvents2_ContentControlOnEnterEventHandler(doc_ContentControlOnEnter);

                            //Add event so that we can fiddle the ids after control has been created.
                            //doc.ContentControlAfterAdd += new DocumentEvents2_ContentControlAfterAddEventHandler(doc_ContentControlAfterAdd);
                        
                            //RDF 8 May 2013 Word 2013 does not have these props
                            //try
                            //{
                            //    doc.XMLSchemaReferences.HideValidationErrors = false;
                            //    doc.XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = true;
                            //    doc.XMLSchemaReferences.AutomaticValidation = false;
                            //}
                            //catch (COMException cex)
                            //{
                            //    switch (cex.ErrorCode)
                            //    {
                            //        case -2146823108: // Word cannot this document template.
                            //            MessageBox.Show(ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.MissingWordTemplateWarning"), Common.About.FormsTitle);
                            //            break;
                            //        default:
                            //            throw;
                            //    }
                            //}

                        // Create some inline XML to put the document into 'XML Mode'.  Otherwise a Schema is required.
                        //string tempXml = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.TemporaryTagXML");
                        //doc.Content.InsertXML(tempXml, ref oMissing);
                        //object oOnce = 1;
                        //doc.Undo(ref oOnce);
                        }
                    }

                    axFramerControl.Visible = true;
                }
            }
            catch (COMException cex)
            {
                string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.WordNoResponseError");
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(string.Format(msg, cex.Message), Common.About.FormsTitle);
                this.Close();
            }

            catch (Exception ex)
            {
                string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.TemplateNoOpenError");
                msg = msg.Replace("\\n", "\n");
                MessageBox.Show(string.Format(msg, ex.Message), Common.About.FormsTitle);
                //MessageBox.Show(string.Format("The Template could not be opened for editing. ({0})", ex.Message), Common.About.FormsTitle);
                this.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        void doc_ContentControlOnEnter(ContentControl ContentControl)
        {
            // Read the variable with the PID
            int pid = -1;

            try
            {
                //WordDocument d = ContentControl.Application.ActiveDocument;
                //Object varName = _PID_varName;
                //Variable abc = d.Variables.get_Item(ref varName);
                //pid = Int32.Parse(abc.Value);
            }
            catch (Exception)
            { }

            // Do not accept events from other processes
            //if (System.Diagnostics.Process.GetCurrentProcess().Id != pid)
            //    return;

            try
            {
                // Raise the PackageItemSelected event
                if (ContentControl.Title != null)
                {
                    //SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs(ContentControl.Tag, new System.Guid(ContentControl.Title));
                    //word 2016 switch these TODO detect switch case
                    SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs(ContentControl.Title, new System.Guid(ContentControl.Tag));

                    if (PackageItemSelected != null)
                    {
                        PackageItemSelected(this, args, pid);
                    }
                }
            }
            catch (Exception ex)
            {
                string messge = ex.Message;
            }
        }

 

        //private void DeleteVarPID()
        //{
        //    // Put Word into Print View and Zoom to Fit.
        //    if (doc != null)
        //    {
        //        Object varName = _PID_varName;
        //        try
        //        {
        //            doc.Variables.get_Item(ref varName).Delete();
        //        }
        //        catch (Exception) { }
        //    }
        //}

        //private void CreateVarPID()
        //{
        //    DeleteVarPID();
        //    Object pid = System.Diagnostics.Process.GetCurrentProcess().Id.ToString();
        //    doc.Variables.Add(_PID_varName, ref pid);
        //}



        protected PackageItem itemBeingEdited = null;

        public PackageItem ItemBeingEdited
        {
            get { return itemBeingEdited; }
        }

        public void SetTemplate(TemplateDocument t)
        {
            itemBeingEdited = t;
        }

        public bool Save()
        {
            bool success = false;
            //if (axFramerControl.ActiveDocument != null)
            if (axFramerControl.ActiveDocument() != null)
            {
                    WordTemplateDocument2 _wordTemplateDocument = ItemBeingEdited as WordTemplateDocument2;
                if (_wordTemplateDocument != null)
                {
                    string templateFilename = _wordTemplateDocument.FilePath;
                    object oMissing = Missing.Value;
                    //object oFileFormat = WdSaveFormat.wdFormatFlatXML;
                    object oFileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatFlatXML;

                    object oFilename = templateFilename;
                    try
                    {
                        //WordDocument doc = axFramerControl.ActiveDocument as WordDocument;
                        //Microsoft.Office.Interop.Word.Document doc = axFramerControl.ActiveDocument() as Microsoft.Office.Interop.Word.Document;

                        //RDF 8 May 2013 Word 2013 does not have these props
                        //try
                        //{
                        //    doc.XMLSchemaReferences.AllowSaveAsXMLWithoutValidation = true;
                        //    doc.XMLShowAdvancedErrors = true;
                        //}
                        //catch (COMException cex)
                        //{
                        //    switch (cex.ErrorCode)
                        //    {
                        //        case -2146823108: // Word cannot this document template.
                        //            //									MessageBox.Show("This document references a Word Template that cannot be located.");
                        //            // Let 'missing Word Template exceptions' slide...
                        //            break;
                        //        default:
                        //            throw;
                        //    }

                        //}
                        // Saved to the template objects pickup point.
                        //doc.SaveAs(ref oFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                        axFramerControl.SaveAs(templateFilename, oFileFormat);

                        // Save as our temporary one again to the template object can be serialised without locking issues with the active document
                        //doc.SaveAs(ref oWorkingFilename, ref oFileFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                        axFramerControl.SaveAs(workingFilename, oFileFormat);

                        _wordTemplateDocument.ForceDocumentPropertiesUpdate();
                        success = true;
                    }
                    catch (COMException)
                    {
                        string msg = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.WordGeneralOpenError");
                        msg = msg.Replace("\\n", "\n");
                        MessageBox.Show(msg, Common.About.FormsTitle);

                        success = false;
                    }
                }
                else
                {
                    success = true;
                }
            }

            if (success)
            {
                // Touch the item, so that the package is marked as changed.
                itemBeingEdited.Touch = Guid.NewGuid();
            }
            return success;
        }

        #region client callback

        // Marking and dragging text into the 'conditional text' 
        // 1. User marks text 
        // 2. User drags text into tree
        // 3. Arbitrary libtrary creates SimpleOutcome
        // 4. Markes text will be wrapped with XML 
        // The following callback fullfills step 4.
        void cpiTreeView1_wrapSelectionWithSimpleOutcome(SimpleOutcome so)
        {
            if (so == null)
            {
                throw new ArgumentNullException("so", ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation").GetString("Perfectus.Client.UI.Designers.TemplateDesigner.NullSimpleOutcome"));
            }
            else
            {
                string tagName = so.GetTagName();
                WrapSelectionWithTag(tagName, so.UniqueIdentifier);
            }
        }

        private void WrapSelectionWithTag(string tagName, Guid uid)
        {
            if (app != null)
            {
                object oMissing = Missing.Value;
                Range r = app.Selection.Range;

                object oRange = r;

                Int32 start = r.Start;
                Int32 end = r.End;

                // Introduce the tag to our on-the-fly schema
                if (doc != null)
                {
                    XMLNodes nodes = doc.SelectNodes(string.Format("//{0}", tagName), string.Empty, false);
                    if (nodes == null || nodes.Count == 0)
                    {
                        object oZero = 0;
                        bool doSelectHack = false;
                        try
                        {
                            r.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
                            doSelectHack = true;
                        }
                        catch
                        {
                            try
                            {
                                object oOne = 1;
                                r = doc.Range(ref oOne, ref oOne);
                                r.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
                                doSelectHack = true;
                            }
                            catch
                            {
                                doc.Content.InsertXML(string.Format("<{0} uid=\"123\" />", tagName), ref oMissing);
                            }

                        }
                        object oOnce = 1;
                        doc.Undo(ref oOnce);

                        if (doSelectHack)
                        {
                            r.Start = start;
                            r.End = end;
                            r.Select();
                        }
                    }
                    try
                    {
                        //XMLNode n = doc.XMLNodes.Add(tagName, string.Empty, ref oRange);
                        //XMLNode attr = n.Attributes.Add("uid", string.Empty, ref oMissing);
                        //attr.NodeValue = uid.ToString();

                        Microsoft.Office.Interop.Word.ContentControl rtc = doc.ContentControls.Add(WdContentControlType.wdContentControlRichText, oRange);

                        // pf-3037/pf-3156 guid is displayed in word 2013 content controls, detect word version and switch values for alias, tag
                        decimal wordVersion = decimal.Parse(app.Version);
                        if (wordVersion < 15)
                        {
                            rtc.Tag = tagName;
                            rtc.Title = uid.ToString();
                        }
                        else
                        {
                            rtc.Tag = uid.ToString();
                            rtc.Title = tagName;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }
        }
        #endregion

        /// <summary>
        ///     Handles the application objects XmlSelectionChange event, locates the new selected XmlNode's Uid attribute 
        ///     and raises the PackageItemSelected event.
        /// </summary>
        /// <param name="Sel">The selected text as it currently stands.</param>
        /// <param name="OldXMLNode">The old XmlNode that was selected</param>
        /// <param name="NewXMLNode">The new XmlNode that has been selected.</param>
        /// <param name="Reason">The reason for the event being fired.</param>
        void app_XMLSelectionChange(Selection Sel, XMLNode OldXMLNode, XMLNode NewXMLNode, ref int Reason)
        {
            string name = string.Empty;
            Guid uniqueIdentifier = Guid.Empty;
            string packageItemUid = null;
            int reasonMove = Convert.ToInt32(WdXMLSelectionChangeReason.wdXMLSelectionChangeReasonMove);

            // Read the variable with the PID
            int pid = -1;
            try
            {
                //WordDocument d = Sel.Application.ActiveDocument;
                Microsoft.Office.Interop.Word.Document d = Sel.Application.ActiveDocument;
                Object varName = _PID_varName;
                Variable abc = d.Variables.get_Item(ref varName);
                pid = Int32.Parse(abc.Value);
            }
            catch (Exception)
            { }

            // Do not accept events from other processes
            if( System.Diagnostics.Process.GetCurrentProcess( ).Id != pid )
                return;

            try
            {
                // Only handle it the new node is valid and the selected text chnaged (not inserted or deleted)
                if (NewXMLNode != null && Reason == reasonMove)
                {
                    // Get the Uid attribute from the selected XmlNode and exit as soon as it is found
                    foreach (Microsoft.Office.Interop.Word.XMLNode attribute in NewXMLNode.Attributes)
                    {
                        if (attribute.NodeValue != null)
                        {
                            packageItemUid = attribute.NodeValue;
                            name = NewXMLNode.BaseName;

                            if (Globals.IsGuid(packageItemUid, out uniqueIdentifier))
                            {
                                break;
                            }
                        }
                    }

                    // Raise the PackageItemSelected event
                    if (uniqueIdentifier != null)
                    {
                        SelectedPackageItemEventArgs args = new SelectedPackageItemEventArgs(name, uniqueIdentifier);

                        if (PackageItemSelected != null)
                        {
                            PackageItemSelected(this, args, pid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string messge = ex.Message;
            }
        }


        private Boolean IsItemReferrenced(Guid param_guid, List<LibraryItem> param_referencedItems, ref String message)
        {
            return IsItemReferrenced(param_guid, param_referencedItems, ref message, 0);
        }
        
        private Boolean IsItemReferrenced(Guid param_guid, List<LibraryItem> param_referencedItems, ref String message, int recusionCounter)
        {
            if (++recusionCounter > 100)
            {
                // Had a case that q1 was depending on q2 and the other way around.
                // Checking the dependencies resulted in endless recursion.
                return true;
            }

            foreach (LibraryItem lib in param_referencedItems)
                if (param_guid == lib.UniqueIdentifier)
                {
                    // Set outcome name - this is necessary if O1 contains O1.
                    // We would not reach the following foreach statement
                    message = lib.Name;
                    return true;
                }

            foreach (LibraryItem lib in param_referencedItems)
            {
                List<LibraryItem> result = lib.GetDependentLibraryItems();
                if (IsItemReferrenced(param_guid, result, ref message, recusionCounter))
                {
                    // Set the name of the oucome ower - that is much more helpful
                    message = lib.Name;
                    return true;
                }
            }
            return false;
        }


        private void OutcomeWordDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            // error on opening the template, hence close asap with no action
            if (doc == null)
                return;

            //DeleteVarPID();

            SaveWordDoc();
            axFramerControl.CloseDoc();
            String WordML = GetOfficeOpenXMLFromDisc();
            Int32 newHash = WordML.GetHashCode();

            if (DialogResult != btnOk.DialogResult && newHash != originalHash)
            {
                DialogResult = MessageBox.Show( String.Format( global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.CancelSaveMsg, _outcome.Name ),
                    global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.ConfirmMsg,
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);

                e.Cancel = DialogResult.Cancel == DialogResult;
                if (DialogResult == DialogResult.Yes)
                    DialogResult = btnOk.DialogResult;
            }

            if (e.Cancel == false && DialogResult == btnOk.DialogResult)
            {
                String message = System.String.Empty;
                // Get the list of referrenced items from the current (unsaved!) WordML
                Dictionary<string, Guid> referencedItems = Perfectus.Common.PackageObjects.WordML.WordMLUtilities.GetDistinctPackageItemListFromWordML(WordML);

                List<LibraryItem> result = new List<LibraryItem>(referencedItems.Count);

                int missingItems = 0;
                foreach (String name in referencedItems.Keys)
                {
                    Guid guid = referencedItems[name];
                    LibraryItem libItem = _package.GetLibraryItemByUniqueIdentifier(guid);

                    if (null == libItem)
                    {
                        if ((missingItems++) > 0)
                            message += ", ";
                        message += name;
                    }
                    else
                        result.Add(libItem);
                }

                if (missingItems > 0)
                {
                    MessageBox.Show(String.Format(global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.MissingItemMsg, message),
                    global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.MissingItemTitleMsg,
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (IsItemReferrenced(_outcome.UniqueIdentifier, result, ref message))
                {
                    MessageBox.Show(String.Format(global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.RecursiveMsg, message),
                                                global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.RecursiveTitleMsg,

                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    e.Cancel = true;

                }
            }
            //if (e.Cancel == true)
            //    CreateVarPID();
        }

        private void OutcomeWordDialog_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (app != null)
            {
                doc.ContentControlOnEnter -= new DocumentEvents2_ContentControlOnEnterEventHandler(doc_ContentControlOnEnter);
                //doc.ContentControlAfterAdd -= new DocumentEvents2_ContentControlAfterAddEventHandler(doc_ContentControlAfterAdd);
            }
        }
    }


    // !!! DUPLICATED CODE !!!
    // see: Perfectus.Client.Studio.DesktopWindow
    public class DesktopWindow : IWin32Window
    {
        // *** Instance to return
        private static DesktopWindow m_DesktopWindow = new DesktopWindow();

        // *** Don't allow instantiation
        private DesktopWindow() { }

        public static IWin32Window Instance
        {
            get { return m_DesktopWindow; }
        }

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// This method returns the actual window handle
        /// This is one convoluted way to do a simple thing
        /// </summary>
        IntPtr IWin32Window.Handle
        {
            get
            {
                return GetForegroundWindow();
            }
        }
    }
}