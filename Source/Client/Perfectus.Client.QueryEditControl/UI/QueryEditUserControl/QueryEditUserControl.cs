using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter;
using Perfectus.Client.Studio.ExpressionLibrary;
using Perfectus.Common.PackageObjects;
using System.Configuration;
using Perfectus.Common;
using System.Collections.ObjectModel;
using Perfectus.Client.SDK;
using System.Resources;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class QueryEditUserControl : UserControl
    {
        private Package _package;
        private IPrintFormatter _iPrintFormatter;
        private QueryDesignerMode _mode;
        public Boolean haveUnicommitedChanged { get { return m_UndoRedoList.Count > 0; } }

        private List<IStudioPlugin> _integrationPlugins;
        /// <summary>
        /// Called by the hosting form
        /// Will register every plugin of type IStudioPlugin
        /// and create a context menu entry.
        /// </summary>
        /// <param name="param_plugin"></param>
        public void AddPlugin(IStudioPlugin param_plugin)
        {
            if (param_plugin is IClauseLibrary)
            {
                IClauseLibrary isp = param_plugin as IClauseLibrary;
                _integrationPlugins.Add(isp);

                // Make this plugin available in the menu structure
                ToolStripMenuItem menu = new ToolStripMenuItem(isp.ButtonText, null, convertToIntegrationToolStripMenuItem_Click, isp.PluginName);
                menu.Tag = isp;
                this.contextMenuPlaceholder.Items.Add(menu);
            }
        }
        private IClauseLibrary FindMatchingIntegrationLibrary ( String param_fetcherID )
        {
            foreach (IClauseLibrary isp in _integrationPlugins)
                if (param_fetcherID == isp.ServerSideFetcherId)
                    return isp;
            return null;
        }

        public QueryEditUserControl()
        {
            InitializeComponent();

            _integrationPlugins = new List<IStudioPlugin>();

            SetButtonDisabled();

            SetupConfig();
        }


        private void SetButtonDisabled()
        {
            toolStripBtnSelect.Enabled = false;
            toolStripBtnRemove.Enabled = false;
            toolStripBtnMarkLeft.Enabled = false;
            toolStripBtnMarkRight.Enabled = false;
            toolStripBtnAddParentheses.Enabled = false;
            toolStripBtnRemoveParentheses.Enabled = false;
        }

        #region Zoom button event
        private void toolStripZoom_SelectedIndexChanged(object sender, EventArgs e)
        {
            String value = this.toolStripZoom.Text;
            int i = value.LastIndexOf("%");
            try
            {
                int factor = Int32.Parse(value.Substring(0, i));
                this.codeWnd.ZoomFactor = (float)factor / 100;
            }
            catch (Exception) { }
        }
        #endregion

        #region wrapper for RichEdit
        public void SetQuery(Package param_package, CQuery param_query)
        {
            _package = param_package;
            SetQuery(param_query);
        }
        public void SetQueryFormatterAndMode(IPrintFormatter param_prettyPrinter, QueryDesignerMode param_mode)
        {
            _mode = param_mode;
            _iPrintFormatter = param_prettyPrinter;
            this.codeWnd.SetQuery(param_prettyPrinter);

            // HTML binding needs to know when the selection changes for single left clicks
            // to mark the 'payload'. Whereas all other modes would use the double-click.
            if ( _mode == QueryDesignerMode.HTMLBinding )
                this.selectionChanged += new SelectionChanged(QueryEditUserControl_selectionChanged);
        }
        private void SetQuery(CQuery param_query)
        {
            this.codeWnd.SetQuery(param_query);
        }
        public CQuery GetQuery()
        {
            return this.codeWnd.GetQuery();
        }
        public void ReDraw()
        {
            this.codeWnd.ReDraw();
        }
        #endregion

        private void CodeEditUserControl_Load(object sender, EventArgs e)
        {
            //codeWnd.invalidHit += new QueryCtrl.InvalidHit(codeWnd_invalidHit);
            codeWnd.objectsHit += new QueryCtrl.ObjectsHit(codeWnd_objectsHit);
            codeWnd.objectHit += new QueryCtrl.ObjectHit(codeWnd_objectHit);
            
            codeWnd.queryRedraw+=new QueryCtrl.QueryRedraw(codeWnd_queryRedraw);

            // The operator symbols are localized; Setup the menu correctly
            ResourceManager rm = Perfectus.Common.ResourceLoader.GetResourceManager("Perfectus.Client.Studio.ExpressionLibrary.ExpressionLibraryLocalisation");
            this.l_op_menu.Text = String.Format ("{0} ({1})", rm.GetString("COperator_Lt"),rm.GetString("COperator_Lt_Description"));
            this.l_op_menu.Tag = Operator.Lt;
            this.le_op_menu.Text = String.Format ("{0} ({1})", rm.GetString("COperator_Le"),rm.GetString("COperator_Le_Description"));
            this.le_op_menu.Tag = Operator.Le;
            this.g_op_menu.Text = String.Format("{0} ({1})",rm.GetString("COperator_Gt"),rm.GetString("COperator_Gt_Description"));
            this.g_op_menu.Tag = Operator.Gt;
            this.ge_op_menu.Text = String.Format("{0} ({1})",rm.GetString("COperator_Ge"),rm.GetString("COperator_Ge_Description"));
            this.ge_op_menu.Tag = Operator.Ge;
            this.eq_op_menu.Text = String.Format("{0} ({1})",rm.GetString("COperator_Eq"),rm.GetString("COperator_Eq_Description"));
            this.eq_op_menu.Tag = Operator.Eq;
            this.neq_op_menu.Text = String.Format("{0} ({1})",rm.GetString("COperator_Ne"),rm.GetString("COperator_Ne_Description"));
            this.neq_op_menu.Tag = Operator.Ne;

            this.codeWnd.ReDraw(); // That shall invoke ...queryRedraw
        }

        void codeWnd_queryRedraw(bool param_success, string param_message)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryEditUserControl));
            if (param_success)
            {
                this.statusStripLabel.Text = String.Empty;
                this.toolStripStatusIcon.Image = global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.check;
            }
            else
            {
                this.statusStripLabel.Text = param_message;
                this.toolStripStatusIcon.Image = global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.delete;
            }
        }
        

        #region Event handler

        void codeWnd_objectHit(QueryCtrl.ObjectInfoStruc param_structure)
        {
            if (param_structure.Action == QueryCtrl.enumAction.SelChg)
                HandleSelectionChanged(param_structure);
            else
                if ((param_structure.Action == QueryCtrl.enumAction.LDblClick))
                    HandleLeftDblClick(param_structure);
                else
                    if (param_structure.Action == QueryCtrl.enumAction.RClick)
                        HandleRightClick(param_structure);
        }
        void codeWnd_objectsHit(QueryCtrl.ObjectsInfoStruc param_structures)
        {
            if (param_structures.Action == QueryCtrl.enumAction.SelChg)
                HandleSelectionChangedMultipleObjects(param_structures);
            else
                if (param_structures.Action == QueryCtrl.enumAction.RClick)
                    HandleRightClickMultipleObjects(param_structures);


        }
        void codeWnd_invalidHit(QueryCtrl.InvalidHitMessage param_message)
        {
            statusStripLabel.Text = param_message.Message;
        }
        #endregion

        #region Event multiple objects implementation

        private CRunBase[] IsOptionAvailable(QueryCtrl.ObjectsInfoStruc param_structures, ref Boolean param_canAddParentheses, ref Boolean param_canRemoveParentheses)
        {
            param_canAddParentheses = false;
            param_canRemoveParentheses = false;

            try
            {
                Dictionary<Guid, CAtom> elements = new Dictionary<Guid, CAtom>(20);

                // Build a list of all marked elements
                foreach (QueryCtrl.ObjectInfoStruc element in param_structures.GetObjects())
                {
                    switch (element.Type)
                    {
                        case AtomType.OPERATOR:
                        case AtomType.RVALUE:
                        case AtomType.LVALUE:
                        case AtomType.VALUE:
                            if (!elements.ContainsKey(element.ParentId))
                            {
                                CAtom atom = ExpressionHelper.Find(element.ParentId);
                                elements.Add(atom.Id, atom);
                            }
                            break;

                        case AtomType.CONCATENATION:
                            if (!elements.ContainsKey(element.ParentId))
                            {
                                // if the "AND" is marked, then we would like to include the 
                                // condition left of it (note that the 'AND', 'OR' belongs to
                                // the right-side condition).
                                CAtom atom = ExpressionHelper.Find(element.ParentId);
                                if (!(atom is CRun))
                                    return null;
                                CRun parent = (CRun)atom;
                                CRunBase base1Run = parent.LeftOf(ExpressionHelper.Find(element.Id) as CConcatenation);
                                if (base1Run != null && !elements.ContainsKey(base1Run.Id))
                                    elements.Add(base1Run.Id, base1Run);
                            }
                            break;
                        default:
                            // Currently only condtions are supported 
                            // Hence cancel out if we find anything else
                            return null;
                    }
                }
                if (elements.Count <= 0)
                    return null;

                Guid[] allGuids = new Guid[elements.Count];
                elements.Keys.CopyTo(allGuids, 0);

                CRunBase[] allElements = new CRunBase[elements.Count];
                elements.Values.CopyTo(allElements, 0);

                CRunBase xyz = (CRunBase)ExpressionHelper.Find(allGuids[0]);
                CRun mainRun = (CRun)ExpressionHelper.Find(xyz.ParentId);

                param_canAddParentheses = mainRun.CanConvertToRun(allElements);
                param_canRemoveParentheses = mainRun.CanRemoveRun();

                return allElements;
            }
            catch
            {
                return null;
            }
        }
        private void HandleSelectionChangedMultipleObjects(QueryCtrl.ObjectsInfoStruc param_structures)
        {
            this.toolStripBtnSelect.Enabled = false;
            this.toolStripBtnRemove.Enabled = false;

            Boolean canAddParentheses = false;
            Boolean canRemoveParentheses = false;
            IsOptionAvailable(param_structures, ref canAddParentheses, ref canRemoveParentheses);

            this.toolStripBtnAddParentheses.Enabled = canAddParentheses;
            this.toolStripBtnRemoveParentheses.Enabled = canRemoveParentheses;
        }

        private void HandleRightClickMultipleObjects(QueryCtrl.ObjectsInfoStruc param_structures)
        {
            Boolean CanConvertToRun = false;
            Boolean CanRemoveRun = false;

            CRunBase[] allElements = IsOptionAvailable(param_structures, ref CanConvertToRun, ref CanRemoveRun);

            if (CanConvertToRun || CanRemoveRun)
            {
            //    codeWnd.MarkObject(allGuids);

                System.Windows.Forms.ContextMenuStrip contextValue = new ContextMenuStrip();

                contextValue.SuspendLayout();
                contextValue.Size = new System.Drawing.Size(180, 99);
                
                // Option Embrace selection
                ToolStripMenuItem menuItem = new ToolStripMenuItem("Embrace selection");
                menuItem.Click += new EventHandler(multipleObjectsEmbraceSelecetion_Click);
                menuItem.Enabled = CanConvertToRun;
                menuItem.Tag = allElements;
                contextValue.Items.Add(menuItem);

                // Option Remove surrounding brackets
                menuItem = new ToolStripMenuItem("Remove sourrounding brackets");
                menuItem.Click += new EventHandler(multipleObjectsRemoveSelecetion_Click);
                menuItem.Enabled = CanRemoveRun;
                menuItem.Tag = allElements;
                contextValue.Items.Add(menuItem);

                contextValue.ResumeLayout(false);
                contextValue.PerformLayout();

                Point pt = Cursor.Position;
                pt.Y += 8;
                contextValue.Show(pt);
            }
        }

        #endregion
        
        #region Event implementation

        public delegate void SelectionChanged(CAtom sender);
        public event SelectionChanged selectionChanged;
        private void OnSelectionChanged(Guid param_guid)
        {
            if (selectionChanged != null)
            {
//                CExpressionBase parameter = ExpressionHelper.Find(param_guid) as CExpressionBase;

                CAtom parameter = ExpressionHelper.Find(param_guid);

                if ( parameter != null )
                    selectionChanged(parameter);
            }
        }



        private void HandleRightClick(QueryCtrl.ObjectInfoStruc param_structure)
        {
            ContextMenuStrip menu = null;

            switch (param_structure.Type)
            {
                case AtomType.LVALUE:
                case AtomType.RVALUE:
                    menu = contextMenuValue;
                    break;

                case AtomType.OPERATOR:
                    {
                        CCondition condition = ExpressionHelper.Find(param_structure.ParentId) as CCondition;
                        List<Perfectus.Common.PerfectusDataType> _dataTypes =
                            new List<PerfectusDataType>(condition.GetDataTypes());
                        // some operators are only valid for numeric types
                        this.l_op_menu.Visible =
                        this.le_op_menu.Visible =
                        this.g_op_menu.Visible =
                        this.ge_op_menu.Visible = _dataTypes.Contains(PerfectusDataType.Number) ||
                        _dataTypes.Contains(PerfectusDataType.Date);

                        menu = contextMenuOperator;
                    }
                    break;

                case AtomType.CONCATENATION:
                    menu = contextMenuConcatenation;
                    break;

                case AtomType.PLACEHOLDER:
                    menu = contextMenuPlaceholder;
                    break;

                case AtomType.STATEMENT:
                    menu = contextMenuStatement;
                    break;

                case AtomType.ELSEEXPRESSION:
                    menu = contextMenuElse;
                    break;

                case AtomType.WORDTEXT:
                    menu = contextMenuWordOutcome;
                    break;

                case AtomType.INTEGRATION:
                    menu = contextMenuIntegration;
                    break;

                case AtomType.NAVIGATIONPAGE:
                    menu = contextMenuNavigation;
                    break;

                case AtomType.HTMLSTRINGBINDING:
                    menu = contextMenuHtmlStringBinding;
                    break;

                default:
                    return;
            }

            if (menu.Items.Count == 0)
                return;

            // Fix FB1517
            menu.Opened += new EventHandler(menu_Opened);

            menu.Items[0].Tag = param_structure;

            Point pt = Cursor.Position;
            pt.Y += 8;
            menu.Show(pt);
            
        }

        // Fix FB1517
        // Dso COM objects may take ownership of the opening menu and the query editor 
        // moves behind the main window.
        // Try to recreate the z-order here...
        void menu_Opened(object sender, EventArgs e)
        {
            if ( ! ( sender is ContextMenuStrip ))
                return;
            codeWnd.Focus();
            ((ContextMenuStrip)sender).Focus();
        }

        private void HandleSelectionChanged(QueryCtrl.ObjectInfoStruc param_structure)
        {
            String infoStrType = String.Empty;
            String infoStrDataType = String.Empty;
            String infoStrShared = String.Empty;
            String infoStrName = String.Empty;
            String infoStrNote = String.Empty;
            switch (param_structure.Type)
            {
                case AtomType.LVALUE:
                case AtomType.RVALUE:
                case AtomType.VALUE:
                    {
                        // setup the status bar

                        infoStrName = param_structure.VisibleText();
                        CValue val = ExpressionHelper.Find(param_structure.Id) as CValue;
                        if (val.AnswerProviderItem != null)
                        {
                            infoStrDataType = Perfectus.Common.PerfectusTypeSystem.LocaliseType(val.AnswerProviderItem.DataType);

                        // Questions
                            if (val.AnswerProviderItem is Question)
                            {
                                infoStrType = "{0} Question{1}: ";
                                Question q = val.AnswerProviderItem as Question;
                                if (q.DataType == PerfectusDataType.Text)
                                    infoStrType = String.Format(infoStrType, infoStrDataType, " (" + Perfectus.Common.PerfectusTypeSystem.LocaliseDisplayType(q.DisplayType, q) + ")");
                                else
                                    infoStrType = String.Format(infoStrType, infoStrDataType, String.Empty);

                                if (q.LibraryUniqueIdentifier != Guid.Empty)
                                    infoStrShared = ", Shared Item";

                                if (null != q.Notes && q.Notes.Length > 0)
                                    infoStrNote = ", " + q.Notes;
                            }
                        // Functions
                            else if (val.AnswerProviderItem is ArithmeticPFunction)
                                infoStrType = String.Format("{0}: ",
ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.AritnmeticPFunction"));
                            else if (val.AnswerProviderItem is DatePFunction)
                            {
                                infoStrType = String.Format("{0}: ",
ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.DatePFunction"));
                                infoStrNote = String.Format(", Return Type = {0}", infoStrDataType);
                            }
                       // Constant
                            else if (val.AnswerProviderItem is PackageConstant)
                                infoStrType = String.Format("{0} {1}: ", infoStrDataType,
ResourceLoader.GetResourceManager("Perfectus.Common.Localisation").GetString("Perfectus.Common.PackageObjects.PackageConstant"));
                            
                        }
                        else
                        {
                        // Empty items
                            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryEditUserControl));
                            infoStrType = resources.GetString("MSG_InvalidItem");
                            infoStrName = String.Empty;
                        }
                        toolStripBtnRemove.Enabled = true;
                        toolStripBtnSelect.Enabled = true;
                        toolStripBtnAddParentheses.Enabled = false;
                        toolStripBtnRemoveParentheses.Enabled = false;
                        toolStripBtnMarkRight.Enabled = true;
                        toolStripBtnMarkLeft.Enabled = true;
                    }
                    break;
                case AtomType.CONDITION:
                    toolStripBtnRemove.Enabled = true;
                    toolStripBtnSelect.Enabled = false;
                    toolStripBtnAddParentheses.Enabled = false;
                    toolStripBtnRemoveParentheses.Enabled = false;
                    toolStripBtnMarkRight.Enabled = true;
                    toolStripBtnMarkLeft.Enabled = true;
                    break;
                case AtomType.OPERATOR:
                    toolStripBtnRemove.Enabled = false;
                    toolStripBtnSelect.Enabled = false;
                    toolStripBtnAddParentheses.Enabled = false;
                    toolStripBtnRemoveParentheses.Enabled = false;
                    toolStripBtnMarkLeft.Enabled = true;
                    toolStripBtnMarkRight.Enabled = true;
                    break;
                case AtomType.STATEMENT:
                case AtomType.ELSEEXPRESSION:
                    toolStripBtnRemove.Enabled = true;
                    toolStripBtnSelect.Enabled = false;
                    toolStripBtnAddParentheses.Enabled = false;
                    toolStripBtnRemoveParentheses.Enabled = false;
                    toolStripBtnMarkLeft.Enabled = false;
                    toolStripBtnMarkRight.Enabled = false;
                    break;
                case AtomType.PLACEHOLDER:
                    toolStripBtnRemove.Enabled = false;
                    toolStripBtnSelect.Enabled = true;
                    toolStripBtnAddParentheses.Enabled = false;
                    toolStripBtnRemoveParentheses.Enabled = false;
                    toolStripBtnMarkLeft.Enabled = false;
                    toolStripBtnMarkRight.Enabled = false;
                    System.ComponentModel.ComponentResourceManager resources2 = new System.ComponentModel.ComponentResourceManager(typeof(QueryEditUserControl));
                    infoStrType = resources2.GetString("Msg_Placeholder");
                    break;

                case AtomType.WORDTEXT:
                case AtomType.INTEGRATION:
                case AtomType.NAVIGATIONPAGE:
                case AtomType.HTMLSTRINGBINDING:
                    toolStripBtnRemove.Enabled = true;
                    toolStripBtnSelect.Enabled = true;
                    toolStripBtnAddParentheses.Enabled = false;
                    toolStripBtnRemoveParentheses.Enabled = false;
                    toolStripBtnMarkLeft.Enabled = false;
                    toolStripBtnMarkRight.Enabled = false;

                    if (param_structure.Type == AtomType.WORDTEXT)
                    {
                        CWordText word = ExpressionHelper.Find(param_structure.Id) as CWordText;

                        if (null != word && word.Name != null && word.Name.Length > 0)
                            infoStrNote = String.Format(", {0}", word.Name);
                    }
                    break;
                default:
                    SetButtonDisabled();
                    break;
            }
            if (infoStrType.Length == 0)
                infoStrType = Info.GetLocalizedName(param_structure.Type);

            statusStripLabel.Text = String.Format("{0}{1}{2}{3}",
                infoStrType,
                infoStrName,
                infoStrShared,
                infoStrNote);

            OnSelectionChanged(param_structure.Id);
        }

        /// <summary>
        /// Left double-click and marked left-click
        /// </summary>
        /// <param name="param_structure"></param>
        private void HandleLeftDblClick(QueryCtrl.ObjectInfoStruc param_structure)
        {
            // These elemenst do not act on double-click
            // Therefore avoid the annouying text selection
            if (param_structure.Type == AtomType.STATEMENT ||
                param_structure.Type == AtomType.ELSEEXPRESSION)
                return;

            codeWnd.MarkObject(param_structure.Id);

            switch (param_structure.Type)
            {
                case AtomType.RVALUE:
                case AtomType.LVALUE:
                case AtomType.VALUE:
                    selectQuestionMenu_Click(this, param_structure);
                    break;
                case AtomType.CONCATENATION:
                    selectConcatenationMenu_Click(this, param_structure);
                    break;
                case AtomType.WORDTEXT:
                    selectActionMenu_Click(this, param_structure);
                    break;
                case AtomType.INTEGRATION:
                    selectIntegrationMenu_Click(this, param_structure);
                    break;
                case AtomType.NAVIGATIONPAGE:
                    selectCNavigationPageMenu_Click(this, param_structure);
                    break;
                case AtomType.HTMLSTRINGBINDING:
                    selectCHtmlStringBindingMenu_Click(this, param_structure);
                    break;
                case AtomType.OPERATOR:
                    this.toggleOperator(this, param_structure);
                    break;
                case AtomType.PLACEHOLDER:
                    contextMenuPlaceholder.Items[0].Tag = param_structure;
                    Point pt = Cursor.Position;
                    pt.Y += 8;
                    contextMenuPlaceholder.Show(pt);
                    break;
            }
        }
        
        #endregion

        #region Buttons in user control

        #region Undo

        
        Stack<CQuery> m_UndoRedoList = new Stack<CQuery>();

        private void toolStripBtnUndo_Click(object sender, EventArgs e)
        {
            if (m_UndoRedoList.Count > 0)
            {
                CQuery q = GetAndRemoveTopUndoElement();

                // The cache contains all elements from all Queries.
                // We have to rollback to the last snapshot. The cache has
                // no knowledge about this snapshot, there we clear it and 
                // clone the query, which fills the cache again with exactly 
                // the elements we need.
                ExpressionHelper.Reset();
                
                this.SetQuery(q.Clone());
                codeWnd.ReDraw();
            }
            this.toolStripBtnUndo.Enabled = m_UndoRedoList.Count>0;
        }
        private CQuery GetAndRemoveTopUndoElement()
        {
            return m_UndoRedoList.Pop();
        }
        private void SaveUndo()
        {
            CQuery c = GetQuery().Clone();
            m_UndoRedoList.Push(c);
            this.toolStripBtnUndo.Enabled = true;
        }

        #endregion

        private void toolStripBtnSelect_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("not implemented yet. Please right-click the item in the text and choose from the context menu");
            Guid id = codeWnd.GetCurrentElementOrParentElement();
            CAtom atom = ExpressionHelper.Find(id);
            if (atom != null)
            {
                QueryCtrl.ObjectInfoStruc info = new QueryCtrl.ObjectInfoStruc(atom);

                switch (info.Type )
                {
                    case AtomType.OPERATOR:
                        MarkText(info);
                        selectOperandMenu_Click ( this, info );
                        break;
                    default:
                        HandleLeftDblClick(info);
                        break;
                }
            }
        }

        private void toolStripBtnRemove_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("not implemented yet. Please right-click the item in the text and choose from the context menu");
            Guid id = codeWnd.GetCurrentElementOrParentElement();
            CAtom atom = ExpressionHelper.Find(id);
            if (atom != null)
            {
                QueryCtrl.ObjectInfoStruc info = new QueryCtrl.ObjectInfoStruc(atom);
                MarkText(info);

                switch (atom.type)
                {
                    case AtomType.LVALUE:
                    case AtomType.RVALUE:
                    case AtomType.VALUE:
                        resetQuestionMenu_Click(null, info);
                        break;
                    case AtomType.CONDITION:
                        removeConditionMenu_Click(null, info);
                        break;
                    case AtomType.STATEMENT:
                        removeStatementMenu_Click(null, info);
                        break;
                    case AtomType.ELSEEXPRESSION:
                        removeELSEToolStripMenuItem_Click(null, info);
                        break;
                    case AtomType.WORDTEXT:
                        removeActionMenu_Click(null, info);
                        break;
                    case AtomType.INTEGRATION:
                        removeIntegrationMenu_Click(null, info);
                        break;
                    case AtomType.NAVIGATIONPAGE:
                        removeCNavigationPageMenu_Click(null, info);
                        break;
                }
            }
        }

        private void toolStripBtnAddParentheses_Click(object sender, EventArgs e)
        {
            Boolean CanConvertToRun = false;
            Boolean CanRemoveRun = false;

            QueryCtrl.ObjectsInfoStruc elements = codeWnd.GetSelectedElements();
            CRunBase[] allElements = IsOptionAvailable(elements, ref CanConvertToRun, ref CanRemoveRun);

            // ToDo: Early implementation for for MenuItems only. Hence try to be a menu-click
            ToolStripItem dummy = new ToolStripLabel();
            dummy.Tag = allElements;
            multipleObjectsEmbraceSelecetion_Click(dummy, EventArgs.Empty);
        }

        private void toolStripBtnRemoveParentheses_Click(object sender, EventArgs e)
        {
            Boolean CanConvertToRun = false;
            Boolean CanRemoveRun = false;

            QueryCtrl.ObjectsInfoStruc elements = codeWnd.GetSelectedElements();
            CRunBase[] allElements = IsOptionAvailable(elements, ref CanConvertToRun, ref CanRemoveRun);

            // ToDo: Early implementation for for MenuItems only. Hence try to be a menu-click
            ToolStripItem dummy = new ToolStripLabel();
            dummy.Tag = allElements;
            multipleObjectsRemoveSelecetion_Click(dummy, EventArgs.Empty);
        }
        private void toolStripBtnMarkLeft_Click(object sender, EventArgs e)
        {
            Guid [] guids = codeWnd.ExtendSelectionLeft();
            codeWnd.MarkObject(guids);
        }
        private void toolStripBtnMarkRight_Click(object sender, EventArgs e)
        {
            Guid[] guids = codeWnd.ExtendSelectionRight();
            codeWnd.MarkObject(guids);
        }

        #endregion

        #region Menu Selection / Hover

        private void MarkText(QueryCtrl.ObjectInfoStruc info)
        {
            switch (info.Type)
            {
                case AtomType.STATEMENT:
                    codeWnd.MarkWithParent(info.Id);
                    break;
                case AtomType.CONDITION:
                    codeWnd.MarkWithParent(info.Id);
                    break;
                case AtomType.WORDTEXT:
                    codeWnd.MarkObject(info.Id); break;
                case AtomType.HTMLSTRINGBINDING:
                    codeWnd.MarkObject(info.Id); break;
                case AtomType.ELSEEXPRESSION:
                    codeWnd.MarkWithParent(info.Id);
                    break;
                default:
                    codeWnd.MarkObject(info.Id);
                    break;
            }
        }
        private void MenuItem_MouseHover(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData (sender, e);

            if (info == null)
                return;

            if (sender == addANDRightConditionMenu ||
                sender == addORRightConditionMenu||
                sender == removeConditionMenu ||
                sender == andToolStripMenuItem ||
                sender == orToolStripMenuItem ||
                sender == removeToolStripMenuItem ||
                sender == loadConditionMenu)
            {
                CAtom atom = ExpressionHelper.Find(info.ParentId);
                info = new QueryCtrl.ObjectInfoStruc(atom);
            }
            MarkText(info);
        }
        #endregion

        #region Helper
        private QueryCtrl.ObjectInfoStruc GetData(object param_sender, EventArgs param_data, AtomType param_type)
        {
            QueryCtrl.ObjectInfoStruc info = GetData (param_sender, param_data);

            if ( info != null && param_type != info.Type)
                throw new ExpressionLibraryException("Data type incorrect", "GetData","Bug");
            return info;
        }
        private QueryCtrl.ObjectInfoStruc GetData(object param_sender, EventArgs param_data)
        {
            if (param_data is QueryCtrl.ObjectInfoStruc)
                return param_data as QueryCtrl.ObjectInfoStruc;
            
            Object data = null;
            QueryCtrl.ObjectInfoStruc info = null;

            // Menu has data in the first item tag
            if (param_sender != null)
                if (param_sender is ToolStripItem)
                    data = (param_sender as ToolStripItem).GetCurrentParent().Items[0].Tag;
                else
                    if (param_sender is Button)
                        data = (param_sender as Button).Tag;

            if (data is QueryCtrl.ObjectInfoStruc)
                info = data as QueryCtrl.ObjectInfoStruc;

            if (info == null)
                throw new ExpressionLibraryException("Data null", "GetData", "Bug");
            return info;
        }
        #endregion

        #region Event R-VAlue

        private void selectQuestionMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender,e);
            CValue value = (CValue) ExpressionHelper.Find(info.Id);
            CCondition condition = (CCondition)ExpressionHelper.Find(info.ParentId);

            if (value == null || condition == null)
                return;

            if (_package != null)
            {
                PackageItemSelectForm f = new PackageItemSelectForm();
                Point pt = new Point();
                f.Size = get_lastQuestionWindowSize(ref pt);
                
                f.showItem(SelectorGlobal.VisiblePackageItemType.QUESTION);
                //f.AddPackageItemType(SelectorCard.VisiblePackageItemType.OUTCOME);
                f.showItem(SelectorGlobal.VisiblePackageItemType.FUNCTION);
                if ( value is CRValue )
                    f.showItem(SelectorGlobal.VisiblePackageItemType.CONSTANT);
                
                //f.AddPackageItemType(SelectorCard.VisiblePackageItemType.CONDITIONALTEXT);
                foreach (PerfectusDataType dataType in condition.GetDataTypes())
                    f.showType(dataType);

                f.Package = _package;
                f.AnswerProviderItem = value.AnswerProviderItem;

                f.ShowDialog();

                if (f.DialogResult == DialogResult.OK)
                {
                    set_lastQuestionWindowSize(f.Size, f.Location);

                    SaveUndo();

                    value.AnswerProviderItem = f.AnswerProviderItem as AnswerProvider;
                    // Note that PackageConstant does not set the type.
                    // Placeholders like <<Not Answered>> are of type NUMBER, even if used with text-questions
                    if (!(value.AnswerProviderItem == null || value.AnswerProviderItem is PackageConstant))
                    {
                        condition.MaybeEnforceType(value.AnswerProviderItem.DataType);
                    }
                    codeWnd.ReDraw();
                }
                else
                    codeWnd.UnMark();
            }
            else
            MessageBox.Show(String.Format ("There is no package loaded - {0} {1}", Info.GetLocalizedName(info.Type),info.Name));
        }
        private void resetQuestionMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender,e);
            CValue value = (CValue)ExpressionHelper.Find(info.Id);
            value.AnswerProviderItem = null;
            codeWnd.ReDraw();
        }
        #endregion

        #region event Condition
        private void AddConditionRight_MenuClick(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender,e);

            CConcatenation.concatenationType concatenation =
                  (sender == addORRightConditionMenu ||
                   sender == orToolStripMenuItem ) ?
                  CConcatenation.concatenationType.OR :
                  CConcatenation.concatenationType.AND;

            CAtom atom = ExpressionHelper.Find(info.ParentId);
            ((CRunBase)atom).AddCondition(concatenation);

            codeWnd.ReDraw();
        }
        private void removeConditionMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender,e);

                try
                {
                    CAtom atom = (CAtom)ExpressionHelper.Find(info.ParentId);
                    atom.Remove();
                    codeWnd.ReDraw();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Command failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
        }

        // This is a quick solution to enable the insert of existing queries
        // The solution shall not require any changes to Perfectus.Common / Studio
        // Hence it's quite awkward
        private void loadConditionMenu_Click(object sender, EventArgs e)
        {
            // Load available queries & display selection form
            ExpressionInfoCollection allExpressions = _package.GetAllExpressions();
            LoadExistingCondition querySelectionForm = new LoadExistingCondition(allExpressions);
            if (querySelectionForm.ShowDialog() == DialogResult.OK)
                try
                {
                    // Part 1 - Create a CQuery fragment from the selected expression
                    // .GetAllExpressions() is an old method that doesn't understand the 
                    // new CQuery structure. The converter shall turn an old-style query
                    // into the new structure
                    Perfectus.Client.Studio.ExpressionLibrary.Converter.Creator cvrt =
                        new Perfectus.Client.Studio.ExpressionLibrary.Converter.Creator(_package);
                    Query queryFromExression = new Query();
                    queryFromExression.QueryExpression = querySelectionForm.QueryExpr;
                    CQuery convertedQuery = cvrt.Create(queryFromExression);

                    // The converter is designed to only handle complete queries. Need
                    // to get the 'QueryExpression' part out of it.
                    // Assume a selection of "(�d1� <> "02/07/2008")"
                    // The resulting query looks like this:
                    //   If �d1� <> "02/07/2008 then
                    //      payload
                    //   -- else
                    // where �d1� <> "02/07/2008 is now of type CRunBase.
                    // CRunBase is anything between a simple condition (a=b), or a complex structure
                    // like (a=b AND ( c=d OR e=f))
                    
                    CBlock block = (CBlock)convertedQuery.Children()[0];
                    CStatement statement = (CStatement)block.Children()[0];
                    CRun runToInsert = statement.if_clause.Run;

                    // Part 2 - Find the existing condition that shall be replaced
                    QueryCtrl.ObjectInfoStruc info = GetData(sender, e);
                    CCondition conditionToReplace = (CCondition)ExpressionHelper.Find(info.ParentId);
                    CRun parentRun = (CRun)ExpressionHelper.Find(conditionToReplace.ParentId);

                    // We want to replace an existing CRun with the newly created CRun
                    // Three possibilities 
                    // A. If A=B Then -> If �d1� <> "02/07/2008 Then
                    //    The condition is the only child of a run, which is owned by the CClause 
                    //    (note: the uppermost run of a statement is special - e.g. you cannot remove it)
                    // B. If A=B and C=D then -> If �d1� <> "02/07/2008 and C=D then
                    //    The run of the condition cannot be replaced (as it contains two condition)
                    // C. If (A=B) and C=D then -> If �d1� <> "02/07/2008 and C=D then
                    //    The run of A=B can be replaced
                    //    (note: here we have two runs - just we don't print the parentheses of the
                    //     outermost one. The outer run contains a child-run and C=D. The child-run
                    //     contains A=B).

                    // Case A and C are ok. Case B needs to be converted into case C.
                    SaveUndo();
                    // Detect case B.
                    if (parentRun.Children().Length > 1)
                    {
                        // Emulate the 'Add parentheses' function
                        // That would create case C.
                        ToolStripItem dummy = new ToolStripLabel();
                        dummy.Tag = new CRunBase[] { conditionToReplace };
                        multipleObjectsEmbraceSelecetion_internal(dummy, EventArgs.Empty);
                        parentRun = (CRun)ExpressionHelper.Find(conditionToReplace.ParentId);
                    }

                    // Substitute the run
                    parentRun.SubstituteRun(runToInsert);
                    codeWnd.ReDraw();
                }
                catch
                {
                    System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryEditUserControl));
                    MessageBox.Show(
                        resources.GetString("libraryInsertFailed"),
                        resources.GetString("libraryInsertFailedCaption"), 
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
        }


        #endregion

        #region Events Statement
        private void addELSEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.STATEMENT);

            CStatement atom = (CStatement)ExpressionHelper.Find(info.Id);
            if (atom.ElseStatement == null)
            {
                if (_mode == QueryDesignerMode.Navigation)
                    atom.AddElseNavigationAction();
                else
                    if (_mode == QueryDesignerMode.HTMLBinding)
                        atom.AddElseHtmlStringBindingAction();
                    else
                {
                    CWordText outcome = atom.AddElseAction();
                    outcome.Name = "new outcome";
                    //atom.AddElsePlaceholderAction();
                }
                codeWnd.ReDraw();
            }
        }

        private void coverWithIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.STATEMENT);

            ISupportOuterIF atom = (ISupportOuterIF)ExpressionHelper.Find(info.Id);
            atom.InsertOuterIF();
            codeWnd.ReDraw();
        }

        private void removeStatementMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.STATEMENT);

            try
            {
                SaveUndo();

                // The Remove() will automatically insert a placeholder, but that is unsuitable for Navigation Page Queries
                // Therefore add the correct type after removal if mode = Navigation
                CAtom atom = (CAtom)ExpressionHelper.Find(info.Id);

                // For navigation pages only...
                bool else_case = false;
                CAtom parent = ExpressionHelper.Find(atom.ParentId);
                if (parent is CElseExpression)
                {
                    else_case = true;
                    parent = ExpressionHelper.Find(parent.ParentId);
                }

                atom.Remove();

                if (_mode == QueryDesignerMode.Navigation)
                {
                    if (else_case)
                        (parent as CStatement).AddElseNavigationAction();
                    else
                        (parent as CStatement).AddIfNavigationAction();
                }
                else
                    if (_mode == QueryDesignerMode.HTMLBinding)
                    {
                        if (else_case)
                            (parent as CStatement).AddElseHtmlStringBindingAction();
                        else
                            (parent as CStatement).AddIfHtmlStringBindingAction();
                    }
                codeWnd.ReDraw();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Command failed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        #endregion

        #region Events Else branch

        private void removeELSEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.ELSEEXPRESSION);
            CElseExpression else_expr = (CElseExpression)ExpressionHelper.Find(info.Id);

                SaveUndo();
            
                else_expr.Remove();
                codeWnd.ReDraw();
        }
        #endregion
 
        #region Events placeholder

        private void convertToStatementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.PLACEHOLDER);

            CExpressionBase parent = (CExpressionBase)ExpressionHelper.Find(info.ParentId);
            CExpressionBase atom = (CExpressionBase)ExpressionHelper.Find(info.Id);

            CStatement statement = parent.AddStatement(atom);

            if (statement != null)
                statement.if_clause.Run.AddCondition();

            codeWnd.ReDraw();
        }


        private void convertToIntegrationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Unknwon caller?
            if (!(sender is ToolStripItem))
                return;
            
            // Sanity test
            IClauseLibrary integrationObject = ((ToolStripItem)sender).Tag as IClauseLibrary;
            if ( integrationObject == null )
                return;

            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.PLACEHOLDER);

            CExpressionBase parent = (CExpressionBase)ExpressionHelper.Find(info.ParentId);
            CExpressionBase atom = (CExpressionBase)ExpressionHelper.Find(info.Id);
            CIntegration integration = parent.AddIntegration(atom);
            integration.ClauseLibrary = integrationObject;

            this.selectIntegrationMenu_Click(sender, new QueryCtrl.ObjectInfoStruc(integration));
        }


        private void convertToActionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.PLACEHOLDER);

            CExpressionBase parent = (CExpressionBase)ExpressionHelper.Find(info.ParentId);
            CExpressionBase atom = (CExpressionBase)ExpressionHelper.Find(info.Id);
            CWordText wordObject = parent.AddAction(atom);
            selectActionMenu_Click(null, new QueryCtrl.ObjectInfoStruc(wordObject));
        }

        private void addOuterIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e);

            CExpressionBase atom = (CExpressionBase)ExpressionHelper.Find(info.Id);
            atom.InsertOuterIF();
            codeWnd.ReDraw();
        }

        #endregion

        #region Event Operand

        private void toggleOperator(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.OPERATOR);
            COperand operand = (COperand)ExpressionHelper.Find(info.Id);
            CCondition condition = (CCondition)ExpressionHelper.Find(info.ParentId);
            Perfectus.Common.PerfectusDataType[] DataTypes = condition.GetDataTypes();

            // Test if we can use Lt, Gt, Le and Ge
            Boolean support_all_operators = false;
            foreach (Perfectus.Common.PerfectusDataType type in DataTypes)
            {
                if (type == Perfectus.Common.PerfectusDataType.Date ||
                      type == Perfectus.Common.PerfectusDataType.Number)
                {
                    support_all_operators = true;
                    break;
                }
            }

            if (operand.OperatorDisplay.Op == Perfectus.Common.Operator.Eq)
                operand.OperatorDisplay.Op = Perfectus.Common.Operator.Ne;
            else
                if (operand.OperatorDisplay.Op == Perfectus.Common.Operator.Ge ||
                    (operand.OperatorDisplay.Op == Perfectus.Common.Operator.Ne && !support_all_operators))
                    operand.OperatorDisplay.Op = Perfectus.Common.Operator.Eq;
                else
                {
                    int idx = System.Convert.ToInt32(operand.OperatorDisplay.Op);
                    operand.OperatorDisplay.Op = (Perfectus.Common.Operator)Enum.GetValues(typeof(Perfectus.Common.Operator)).GetValue(++idx);
                }
            codeWnd.ReDraw();
        }
        private void selectOperandMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.OPERATOR);
            COperand operand = (COperand)ExpressionHelper.Find(info.Id);

            if (((ToolStripMenuItem)sender).Tag is Operator)
            {
                OperatorDisplay od = new OperatorDisplay((Operator)((ToolStripMenuItem)sender).Tag);
                operand.OperatorDisplay = od;
            }
            else
                operand.Reset();
            
            codeWnd.ReDraw();
        }
        #endregion

        #region Event Concatenation
        private void selectConcatenationMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.CONCATENATION);

            CConcatenation concatenation = (CConcatenation)ExpressionHelper.Find(info.Id);
            concatenation.Toggle();
            codeWnd.ReDraw();
        }
        #endregion

        #region Event Action

        public delegate void CanDeleteAction(CExpressionBase sender, CancelEventArgs eventArgs);
        public delegate void ActionDeleted(CExpressionBase sender);
        public delegate void ActionClick(CExpressionBase sender);

        public event CanDeleteAction canDeleteAction;
        public event ActionDeleted actionDeleted;
        public event ActionClick actionClick;
        private Boolean OnCanDeleteAction(CWordText param_action)
        {
            CancelEventArgs c = new CancelEventArgs(false);
            if (canDeleteAction != null)
                canDeleteAction(param_action,c );
            return ! c.Cancel;
        }
        private void OnActionDeleted(CWordText param_action)
        {
            if (actionDeleted != null)
                actionDeleted(param_action);
        }
        private void OnActionClick(CWordText param_action)
        {
            if (actionClick != null)
                actionClick(param_action);
            codeWnd.ReDraw();
        }

        private void removeActionMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.WORDTEXT);
            CAtom atom = ExpressionHelper.Find(info.Id);

            if (atom is CWordText)
            {
                CWordText action = atom as CWordText;
                if (OnCanDeleteAction(action))
                {
                    SaveUndo();
            
                    atom.Remove();
                    codeWnd.ReDraw();
                    OnActionDeleted(action);
                }
                else
                    codeWnd.UnMark();
            }
        }
        private void selectActionMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.WORDTEXT);
            CAtom atom = ExpressionHelper.Find(info.Id);
            if (atom is CWordText)
            {
                CWordText action = atom as CWordText;
                OnActionClick(action);
            }
        }

        #endregion

        #region Event NavigationPage
        public event CanDeleteAction canDeleteNavigationPage;
        public event ActionDeleted navigationPageDeleted;
        public event ActionClick navigationPageClick;

        private Boolean OnCanDeleteNavigationPage(CNavigationPage param_action)
        {
            CancelEventArgs c = new CancelEventArgs(false);
            if (canDeleteNavigationPage != null)
                canDeleteNavigationPage(param_action, c);
            return !c.Cancel;
        }
        private void OnNavigationPageDeleted(CNavigationPage param_action)
        {
            if (navigationPageDeleted != null)
                navigationPageDeleted(param_action);
        }
        private void OnNavigationPageClick(CNavigationPage param_action)
        {
            if (navigationPageClick != null)
                navigationPageClick(param_action);
            codeWnd.ReDraw();
        }

        private void removeCNavigationPageMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.NAVIGATIONPAGE);
            CAtom atom = ExpressionHelper.Find(info.Id);

            if (atom is CNavigationPage)
            {
                CNavigationPage action = atom as CNavigationPage;
                if (OnCanDeleteNavigationPage(action))
                {
                    SaveUndo();
                    action.Payload = null;
                    action.Name = string.Empty;
                    codeWnd.ReDraw();
                    OnNavigationPageDeleted(action);
                }
                else
                    codeWnd.UnMark();
            }
        }
        private void selectCNavigationPageMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.NAVIGATIONPAGE);
            CAtom atom = ExpressionHelper.Find(info.Id);
            if (atom is CNavigationPage)
            {
                CNavigationPage action = atom as CNavigationPage;
                OnNavigationPageClick(action);
            }
        }
        #endregion

        #region Event HtmlStringBinding
        public event ActionClick HtmlStringBindingClick;

        private void OnHtmlStringBindingClick(CHtmlStringBinding param_action)
        {
            if (HtmlStringBindingClick != null)
                HtmlStringBindingClick(param_action);
            codeWnd.ReDraw();
        }

        private void selectCHtmlStringBindingMenu_Click(object sender, EventArgs e)
        {
            SaveUndo();

            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.HTMLSTRINGBINDING);
            CAtom atom = ExpressionHelper.Find(info.Id);
            if (atom is CHtmlStringBinding)
            {
                CHtmlStringBinding action = atom as CHtmlStringBinding;
                OnHtmlStringBindingClick(action);
            }
        }
        void QueryEditUserControl_selectionChanged(CAtom sender)
        {
            if (sender is CHtmlStringBinding)
                MarkText(new QueryCtrl.ObjectInfoStruc(sender));
        }
        #endregion
        
        #region Event Integration
        public event CanDeleteAction canDeleteIntegration;
        public event ActionDeleted integrationDeleted;
        public event ActionClick integrationClick;
        private Boolean OnCanDeleteIntegration(CIntegration param_action)
        {
            CancelEventArgs c = new CancelEventArgs(false);
            if (canDeleteIntegration != null)
                canDeleteIntegration(param_action, c);
            return ! c.Cancel;
        }
        private void OnIntegrationDeleted(CIntegration param_action)
        {
            if (integrationDeleted != null)
                integrationDeleted(param_action);
        }
        private void OnIntegrationClick(CIntegration param_action)
        {
            if (integrationClick != null)
               integrationClick(param_action);
            codeWnd.ReDraw();
        }

        private void removeIntegrationMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.INTEGRATION);
            CAtom atom = ExpressionHelper.Find(info.Id);

            if (atom is CIntegration)
            {
                CIntegration integration = atom as CIntegration;
                if (OnCanDeleteIntegration(integration))
                {
                    SaveUndo();
            
                    atom.Remove();
                    codeWnd.ReDraw();
                    OnIntegrationDeleted(integration);
                }
                else
                    codeWnd.UnMark();
            }
        }
        private void selectIntegrationMenu_Click(object sender, EventArgs e)
        {
            QueryCtrl.ObjectInfoStruc info = GetData(sender, e, AtomType.INTEGRATION);
            CIntegration integration = ExpressionHelper.Find(info.Id) as CIntegration;
            if (integration != null)
            {
                // If the object was loaded from disk, then we have to reassign it to a library;
                if ( integration.ClauseLibrary == null )
                {
                    integration.ClauseLibrary = FindMatchingIntegrationLibrary ( integration.FetcherId );
                    if ( integration.ClauseLibrary == null )
                        throw new Exception ("The integration library could not be found");
                }

                // We don't knwo if subscriber modify our object...
                SaveUndo();
                int hash = integration.GetHashCode();

                IClauseLibrary integrationObject = (IClauseLibrary)integration.ClauseLibrary;
                OnIntegrationClick(integration);

                if (hash != integration.GetHashCode())
                    codeWnd.ReDraw();
                else
                    // Remove us from the UnDo stack
                    GetAndRemoveTopUndoElement();
            }
        }

        #endregion

        #region Implementation multiple elements marked 
        void multipleObjectsEmbraceSelecetion_Click(object sender, EventArgs e)
        {
            multipleObjectsEmbraceSelecetion_internal(sender, e);
            codeWnd.ReDraw();
        }
        void multipleObjectsEmbraceSelecetion_internal(object sender, EventArgs e)
        {
            if (!(sender is ToolStripItem))
                return;
            CRunBase[] elements = (CRunBase[])(sender as ToolStripItem).Tag;
            if (null == elements || null == elements[0])
                return;
            SaveUndo(); 
            CRun mainRun = (CRun)ExpressionHelper.Find(elements[0].ParentId);
            mainRun.InsertRun(elements);
        }
        void multipleObjectsRemoveSelecetion_Click(object sender, EventArgs e)
        {
            if (!(sender is ToolStripItem))
                return;
            SaveUndo();
            CRunBase[] elements = (CRunBase[])(sender as ToolStripItem).Tag;
            CRun mainRun = (CRun)ExpressionHelper.Find(elements[0].ParentId);
            mainRun.ElevateRun();
            codeWnd.ReDraw();
        }
        #endregion
    }
}
