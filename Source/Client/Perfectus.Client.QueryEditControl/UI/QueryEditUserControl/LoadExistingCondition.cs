using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using System.Collections;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class LoadExistingCondition : Form
    {
        ExpressionInfoCollection _allExpressions;

        private QueryExpression queryExpr;
        public QueryExpression QueryExpr
        {
            get { return queryExpr; }
        }
	

        public LoadExistingCondition(ExpressionInfoCollection allExpressions)
        {
            InitializeComponent();

            _allExpressions = allExpressions;
            queryExpr = null;
            btnOk.Enabled = false;
        }

        private void LoadExistingCondition_Load(object sender, EventArgs e)
        {
            if (_allExpressions.Count == 0)
            {
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadExistingCondition));
                MessageBox.Show(
                    resources.GetString("NoItems"),
                    resources.GetString("NoItemsCaption"),
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
                /*
                   </data>
	<data name="NoItems" xml:space="preserve">
    <value>There are currently no expressions available to choose from.</value>
  </data>
	<data name="NoItemsCaption" xml:space="preserve">
    <value>Expression library</value>
  </data>
* 
                 */

                Close();
                return;
            }
            ListViewItem item;
            ListViewGroup group = null;
            int imageIndex = -1;
            _columnSorter = new ExpressionViewColumnSorter();
            expressionView.ListViewItemSorter = _columnSorter;

            foreach (ExpressionInfo expressionInfo in _allExpressions)
            {
                if (expressionInfo.ParentItem is Question)
                {
                    imageIndex = 0;
                    group = expressionView.Groups["Questions"];
                }
                else if (expressionInfo.ParentItem is Outcome)
                {
                    imageIndex = 2;
                    group = expressionView.Groups["Outcomes"];
                }
                else if (expressionInfo.ParentItem is SimpleOutcome)
                {
                    imageIndex = 3;
                    group = expressionView.Groups["SimpleOutcomes"];
                }

                item = new ListViewItem(expressionInfo.ParentItem.Name, imageIndex);
                item.Group = group;
                item.Tag = expressionInfo;
                item.SubItems.Add (new ListViewItem.ListViewSubItem (item, expressionInfo.ParentPropertyName ));
                item.SubItems.Add (new ListViewItem.ListViewSubItem(item, expressionInfo.PreviewString));
                this.expressionView.Items.Add(item); ;
            }
            expressionView.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            expressionView.Columns[1].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            expressionView.Columns[2].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            this.Location = new Point (Cursor.Position.X, Math.Max (Cursor.Position.Y - this.Height, 1));
        }


        private ExpressionViewColumnSorter _columnSorter;
        public class ExpressionViewColumnSorter : IComparer
        {
            private int ColumnToSort;
            private SortOrder OrderOfSort;
            private CaseInsensitiveComparer ObjectCompare;

            public int SortColumn
            {
                get { return ColumnToSort; }
                set { ColumnToSort = value; }
            }

            /// <summary>
            /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
            /// </summary>
            public SortOrder Order
            {
                get { return OrderOfSort; }
                set { OrderOfSort = value; }
            }

            public ExpressionViewColumnSorter()
            {
                ColumnToSort = 0;
                OrderOfSort = SortOrder.None;
                ObjectCompare = new CaseInsensitiveComparer();
            }

            public int Compare(object x, object y)
            {
                int compareResult;
                ListViewItem listviewX, listviewY;

                listviewX = (ListViewItem)x;
                listviewY = (ListViewItem)y;

                compareResult = ObjectCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);

                if (OrderOfSort == SortOrder.Ascending)
                {
                    return compareResult;
                }
                else if (OrderOfSort == SortOrder.Descending)
                {
                    return (-compareResult);
                }
                else
                {
                    return 0;
                }
            }
        }


        private void ListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            Cursor currentCursor = this.Cursor;
            
            try
            {
                Cursor = Cursors.WaitCursor;

                if (e.Column == _columnSorter.SortColumn)
                {
                    if (_columnSorter.Order == SortOrder.Ascending)
                        _columnSorter.Order = SortOrder.Descending;
                    else
                        _columnSorter.Order = SortOrder.Ascending;


                }
                else
                {
                    _columnSorter.Order = SortOrder.Ascending;
                    _columnSorter.SortColumn = e.Column;
                }

                expressionView.Sort();
            }
            finally
            {
                Cursor = currentCursor;
            }
        }


        private void expressionView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (expressionView.SelectedItems.Count != 1)
            {
                btnOk.Enabled = false;
                queryExpr = null;
                return;
            }
            btnOk.Enabled = true;
            ExpressionInfo exp = (ExpressionInfo)expressionView.SelectedItems[0].Tag;
            queryExpr = exp.Expression;
        }

        private void expressionView_DoubleClick(object sender, EventArgs e)
        {
            if (queryExpr != null)
                btnOk.PerformClick();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}