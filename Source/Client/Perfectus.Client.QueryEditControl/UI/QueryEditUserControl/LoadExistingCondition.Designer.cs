namespace Perfectus.Client.Studio.QueryEditControl
{
    partial class LoadExistingCondition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("Questions", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Outcomes", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewGroup listViewGroup3 = new System.Windows.Forms.ListViewGroup("Simple Outcomes", System.Windows.Forms.HorizontalAlignment.Left);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadExistingCondition));
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.expressionView = new System.Windows.Forms.ListView();
            this.colItemType = new System.Windows.Forms.ColumnHeader();
            this.colProperty = new System.Windows.Forms.ColumnHeader();
            this.colExpression = new System.Windows.Forms.ColumnHeader();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(447, 241);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(528, 242);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // expressionView
            // 
            this.expressionView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.expressionView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colItemType,
            this.colProperty,
            this.colExpression});
            this.expressionView.FullRowSelect = true;
            listViewGroup1.Header = "Questions";
            listViewGroup1.Name = "Questions";
            listViewGroup2.Header = "Outcomes";
            listViewGroup2.Name = "Outcomes";
            listViewGroup3.Header = "Simple Outcomes";
            listViewGroup3.Name = "SimpleOutcomes";
            this.expressionView.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1,
            listViewGroup2,
            listViewGroup3});
            this.expressionView.Location = new System.Drawing.Point(2, 1);
            this.expressionView.MultiSelect = false;
            this.expressionView.Name = "expressionView";
            this.expressionView.ShowGroups = false;
            this.expressionView.Size = new System.Drawing.Size(611, 235);
            this.expressionView.SmallImageList = this.imageList1;
            this.expressionView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.expressionView.TabIndex = 2;
            this.expressionView.UseCompatibleStateImageBehavior = false;
            this.expressionView.View = System.Windows.Forms.View.Details;
            this.expressionView.SelectedIndexChanged += new System.EventHandler(this.expressionView_SelectedIndexChanged);
            this.expressionView.DoubleClick += new System.EventHandler(this.expressionView_DoubleClick);
            this.expressionView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ListView_ColumnClick);
            // 
            // colItemType
            // 
            this.colItemType.Text = "Name";
            // 
            // colProperty
            // 
            this.colProperty.Text = "Property";
            // 
            // colExpression
            // 
            this.colExpression.Text = "Expression";
            this.colExpression.Width = 180;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Question");
            this.imageList1.Images.SetKeyName(1, "Function");
            this.imageList1.Images.SetKeyName(2, "Outcome");
            this.imageList1.Images.SetKeyName(3, "SimpleOutcome");
            // 
            // LoadExistingCondition
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(615, 266);
            this.Controls.Add(this.expressionView);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(372, 293);
            this.Name = "LoadExistingCondition";
            this.Text = "Expressions";
            this.Load += new System.EventHandler(this.LoadExistingCondition_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListView expressionView;
        private System.Windows.Forms.ColumnHeader colItemType;
        private System.Windows.Forms.ColumnHeader colProperty;
        private System.Windows.Forms.ColumnHeader colExpression;
        private System.Windows.Forms.ImageList imageList1;
    }
}