using System;
using System.Configuration;
using System.Drawing;
using Perfectus.Client.Configuration;
using Perfectus.Client.Configuration.QueryEditConfiguration;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class QueryEditUserControl
    {
        private void SetupConfig()
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");

            QESetupUserControl form = new QESetupUserControl("QueryEditorConfigurationSection");
            form.LoadConfigValues();
            RegisterColors( );
            form.Dispose();

            toolStripZoom.Text = String.Format("{0}%", config.Zoom);
            toolStripZoom_SelectedIndexChanged(null, EventArgs.Empty);
        }

        private Size get_lastQuestionWindowSize(ref Point param_location)
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");

            Size size = new Size(0,0);
            param_location = new Point(-1, -1);
            if (config != null)
            {
                param_location = config.SelectionFormLocation;
                return config.SelectionFormSize;
            }
            return size;
        }
        private void set_lastQuestionWindowSize(Size param_size, Point param_location)
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");
        
            if ( config != null )
            {
                config.SelectionFormSize = param_size;
                config.SelectionFormLocation= param_location;
                main.SaveConfig(config);
            }
        }
        public Size get_lastCodeWindowSize(ref Point param_location)
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");

            Size size = new Size(0, 0);
            param_location = new Point(-1, -1);
            if (config != null)
            {
                param_location = config.CodeFormLocation;
                return config.CodeFormSize;
            }
            return size ;
        }
        public void set_lastCodeWindowSize(Size param_size, Point param_location)
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection
            _config = main.GetConfig("QueryEditorConfigurationSection") as CodeEditControlSection;

            if (_config != null)
            {
                _config.CodeFormSize = param_size;
                _config.CodeFormLocation = param_location;
                main.SaveConfig(_config);
            }
        }

        private void RegisterColors()
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");

            foreach ( ColorElement element in  config.libraryList )
                codeWnd.Register(element.Name, element.Color);
            
            // The background color of the code window almost never changes.
            ColorElement background = config.libraryList ["Background"];
            if ( background != null )
                codeWnd.BackColor = background.Color;
        }
    }
}