using System;
using System.Windows.Forms;
namespace Perfectus.Client.Studio.QueryEditControl
{
    partial class QueryEditUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QueryEditUserControl));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolSelectStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripZoom = new System.Windows.Forms.ToolStripComboBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.contextMenuPlaceholder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toActionPlaceholderMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.createStatementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuValue = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addANDRightConditionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.addORRightConditionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.resetQuestionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.removeConditionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuOperator = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.eq_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.neq_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.ge_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.g_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.le_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.l_op_menu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.andToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuConcatenation = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toggleConcatenationMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStatement = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createIFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createELSEToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.removeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuElse = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuWordOutcome = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createIFToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.removeToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuIntegration = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createIFToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.removeToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuNavigation = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createIFToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.removeToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuHtmlStringBinding = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ifSubqueryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadConditionMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripBtnSelect = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnRemove = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnUndo = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnMarkLeft = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnMarkRight = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnAddParentheses = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnRemoveParentheses = new System.Windows.Forms.ToolStripButton();
            this.toolStripStatusIcon = new System.Windows.Forms.ToolStripStatusLabel();
            this.codeWnd = new Perfectus.Client.Studio.QueryEditControl.QueryCtrl();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolSelectStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.contextMenuPlaceholder.SuspendLayout();
            this.contextMenuValue.SuspendLayout();
            this.contextMenuOperator.SuspendLayout();
            this.contextMenuConcatenation.SuspendLayout();
            this.contextMenuStatement.SuspendLayout();
            this.contextMenuElse.SuspendLayout();
            this.contextMenuWordOutcome.SuspendLayout();
            this.contextMenuIntegration.SuspendLayout();
            this.contextMenuNavigation.SuspendLayout();
            this.contextMenuHtmlStringBinding.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            this.toolStripContainer1.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.codeWnd);
            resources.ApplyResources(this.toolStripContainer1.ContentPanel, "toolStripContainer1.ContentPanel");
            resources.ApplyResources(this.toolStripContainer1, "toolStripContainer1");
            this.toolStripContainer1.Name = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolSelectStrip);
            // 
            // toolSelectStrip
            // 
            resources.ApplyResources(this.toolSelectStrip, "toolSelectStrip");
            this.toolSelectStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnSelect,
            this.toolStripBtnRemove,
            this.toolStripSeparator1,
            this.toolStripBtnUndo,
            this.toolStripSeparator3,
            this.toolStripBtnMarkLeft,
            this.toolStripBtnMarkRight,
            this.toolStripBtnAddParentheses,
            this.toolStripBtnRemoveParentheses,
            this.toolStripSeparator4,
            this.toolStripZoom});
            this.toolSelectStrip.Name = "toolSelectStrip";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // toolStripZoom
            // 
            this.toolStripZoom.DropDownWidth = 60;
            this.toolStripZoom.Items.AddRange(new object[] {
            resources.GetString("toolStripZoom.Items"),
            resources.GetString("toolStripZoom.Items1"),
            resources.GetString("toolStripZoom.Items2"),
            resources.GetString("toolStripZoom.Items3"),
            resources.GetString("toolStripZoom.Items4"),
            resources.GetString("toolStripZoom.Items5"),
            resources.GetString("toolStripZoom.Items6"),
            resources.GetString("toolStripZoom.Items7"),
            resources.GetString("toolStripZoom.Items8")});
            resources.ApplyResources(this.toolStripZoom, "toolStripZoom");
            this.toolStripZoom.Name = "toolStripZoom";
            this.toolStripZoom.SelectedIndexChanged += new System.EventHandler(this.toolStripZoom_SelectedIndexChanged);
            // 
            // statusStrip
            // 
            this.statusStrip.GripMargin = new System.Windows.Forms.Padding(0);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripLabel,
            this.toolStripStatusIcon});
            resources.ApplyResources(this.statusStrip, "statusStrip");
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.statusStrip.SizingGrip = false;
            // 
            // statusStripLabel
            // 
            this.statusStripLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.statusStripLabel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.statusStripLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.statusStripLabel.Name = "statusStripLabel";
            resources.ApplyResources(this.statusStripLabel, "statusStripLabel");
            this.statusStripLabel.Spring = true;
            // 
            // contextMenuPlaceholder
            // 
            this.contextMenuPlaceholder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toActionPlaceholderMenu,
            this.createStatementToolStripMenuItem});
            this.contextMenuPlaceholder.Name = "contextMenuPlaceholder";
            resources.ApplyResources(this.contextMenuPlaceholder, "contextMenuPlaceholder");
            // 
            // toActionPlaceholderMenu
            // 
            this.toActionPlaceholderMenu.Name = "toActionPlaceholderMenu";
            resources.ApplyResources(this.toActionPlaceholderMenu, "toActionPlaceholderMenu");
            this.toActionPlaceholderMenu.Click += new System.EventHandler(this.convertToActionToolStripMenuItem_Click);
            // 
            // createStatementToolStripMenuItem
            // 
            this.createStatementToolStripMenuItem.Name = "createStatementToolStripMenuItem";
            resources.ApplyResources(this.createStatementToolStripMenuItem, "createStatementToolStripMenuItem");
            this.createStatementToolStripMenuItem.Click += new System.EventHandler(this.convertToStatementToolStripMenuItem_Click);
            // 
            // contextMenuValue
            // 
            this.contextMenuValue.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addANDRightConditionMenu,
            this.addORRightConditionMenu,
            this.toolStripMenuItem1,
            this.resetQuestionMenu,
            this.toolStripMenuItem7,
            this.loadConditionMenu,
            this.removeConditionMenu});
            this.contextMenuValue.Name = "contextMenuValue";
            resources.ApplyResources(this.contextMenuValue, "contextMenuValue");
            // 
            // addANDRightConditionMenu
            // 
            this.addANDRightConditionMenu.Name = "addANDRightConditionMenu";
            resources.ApplyResources(this.addANDRightConditionMenu, "addANDRightConditionMenu");
            this.addANDRightConditionMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.addANDRightConditionMenu.Click += new System.EventHandler(this.AddConditionRight_MenuClick);
            // 
            // addORRightConditionMenu
            // 
            this.addORRightConditionMenu.Name = "addORRightConditionMenu";
            resources.ApplyResources(this.addORRightConditionMenu, "addORRightConditionMenu");
            this.addORRightConditionMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.addORRightConditionMenu.Click += new System.EventHandler(this.AddConditionRight_MenuClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // resetQuestionMenu
            // 
            this.resetQuestionMenu.Name = "resetQuestionMenu";
            resources.ApplyResources(this.resetQuestionMenu, "resetQuestionMenu");
            this.resetQuestionMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.resetQuestionMenu.Click += new System.EventHandler(this.resetQuestionMenu_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            resources.ApplyResources(this.toolStripMenuItem7, "toolStripMenuItem7");
            // 
            // removeConditionMenu
            // 
            this.removeConditionMenu.Name = "removeConditionMenu";
            resources.ApplyResources(this.removeConditionMenu, "removeConditionMenu");
            this.removeConditionMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeConditionMenu.Click += new System.EventHandler(this.removeConditionMenu_Click);
            // 
            // contextMenuOperator
            // 
            this.contextMenuOperator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eq_op_menu,
            this.neq_op_menu,
            this.ge_op_menu,
            this.g_op_menu,
            this.le_op_menu,
            this.l_op_menu,
            this.toolStripMenuItem2,
            this.andToolStripMenuItem,
            this.orToolStripMenuItem,
            this.removeToolStripMenuItem});
            this.contextMenuOperator.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuOperator, "contextMenuOperator");
            // 
            // eq_op_menu
            // 
            this.eq_op_menu.Name = "eq_op_menu";
            resources.ApplyResources(this.eq_op_menu, "eq_op_menu");
            this.eq_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.eq_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // neq_op_menu
            // 
            this.neq_op_menu.Name = "neq_op_menu";
            resources.ApplyResources(this.neq_op_menu, "neq_op_menu");
            this.neq_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.neq_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // ge_op_menu
            // 
            this.ge_op_menu.Name = "ge_op_menu";
            resources.ApplyResources(this.ge_op_menu, "ge_op_menu");
            this.ge_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.ge_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // g_op_menu
            // 
            this.g_op_menu.Name = "g_op_menu";
            resources.ApplyResources(this.g_op_menu, "g_op_menu");
            this.g_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.g_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // le_op_menu
            // 
            this.le_op_menu.Name = "le_op_menu";
            resources.ApplyResources(this.le_op_menu, "le_op_menu");
            this.le_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.le_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // l_op_menu
            // 
            this.l_op_menu.Name = "l_op_menu";
            resources.ApplyResources(this.l_op_menu, "l_op_menu");
            this.l_op_menu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.l_op_menu.Click += new System.EventHandler(this.selectOperandMenu_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // andToolStripMenuItem
            // 
            this.andToolStripMenuItem.Name = "andToolStripMenuItem";
            resources.ApplyResources(this.andToolStripMenuItem, "andToolStripMenuItem");
            this.andToolStripMenuItem.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.andToolStripMenuItem.Click += new System.EventHandler(this.AddConditionRight_MenuClick);
            // 
            // orToolStripMenuItem
            // 
            this.orToolStripMenuItem.Name = "orToolStripMenuItem";
            resources.ApplyResources(this.orToolStripMenuItem, "orToolStripMenuItem");
            this.orToolStripMenuItem.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.orToolStripMenuItem.Click += new System.EventHandler(this.AddConditionRight_MenuClick);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            resources.ApplyResources(this.removeToolStripMenuItem, "removeToolStripMenuItem");
            this.removeToolStripMenuItem.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.removeConditionMenu_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
            // 
            // contextMenuConcatenation
            // 
            this.contextMenuConcatenation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toggleConcatenationMenu});
            this.contextMenuConcatenation.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuConcatenation, "contextMenuConcatenation");
            // 
            // toggleConcatenationMenu
            // 
            this.toggleConcatenationMenu.Name = "toggleConcatenationMenu";
            resources.ApplyResources(this.toggleConcatenationMenu, "toggleConcatenationMenu");
            this.toggleConcatenationMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.toggleConcatenationMenu.Click += new System.EventHandler(this.selectConcatenationMenu_Click);
            // 
            // contextMenuStatement
            // 
            this.contextMenuStatement.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createIFToolStripMenuItem,
            this.createELSEToolStripMenuItem1,
            this.toolStripMenuItem3,
            this.removeToolStripMenuItem1});
            this.contextMenuStatement.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStatement, "contextMenuStatement");
            // 
            // createIFToolStripMenuItem
            // 
            this.createIFToolStripMenuItem.Name = "createIFToolStripMenuItem";
            resources.ApplyResources(this.createIFToolStripMenuItem, "createIFToolStripMenuItem");
            this.createIFToolStripMenuItem.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.createIFToolStripMenuItem.Click += new System.EventHandler(this.coverWithIFToolStripMenuItem_Click);
            // 
            // createELSEToolStripMenuItem1
            // 
            this.createELSEToolStripMenuItem1.Name = "createELSEToolStripMenuItem1";
            resources.ApplyResources(this.createELSEToolStripMenuItem1, "createELSEToolStripMenuItem1");
            this.createELSEToolStripMenuItem1.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.createELSEToolStripMenuItem1.Click += new System.EventHandler(this.addELSEToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
            // 
            // removeToolStripMenuItem1
            // 
            this.removeToolStripMenuItem1.Name = "removeToolStripMenuItem1";
            resources.ApplyResources(this.removeToolStripMenuItem1, "removeToolStripMenuItem1");
            this.removeToolStripMenuItem1.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem1.Click += new System.EventHandler(this.removeStatementMenu_Click);
            // 
            // contextMenuElse
            // 
            this.contextMenuElse.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem2});
            this.contextMenuElse.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuElse, "contextMenuElse");
            // 
            // removeToolStripMenuItem2
            // 
            this.removeToolStripMenuItem2.Name = "removeToolStripMenuItem2";
            resources.ApplyResources(this.removeToolStripMenuItem2, "removeToolStripMenuItem2");
            this.removeToolStripMenuItem2.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem2.Click += new System.EventHandler(this.removeELSEToolStripMenuItem_Click);
            // 
            // contextMenuWordOutcome
            // 
            this.contextMenuWordOutcome.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createIFToolStripMenuItem1,
            this.toolStripMenuItem4,
            this.removeToolStripMenuItem3});
            this.contextMenuWordOutcome.Name = "contextMenuWordOutcome";
            resources.ApplyResources(this.contextMenuWordOutcome, "contextMenuWordOutcome");
            // 
            // createIFToolStripMenuItem1
            // 
            this.createIFToolStripMenuItem1.Name = "createIFToolStripMenuItem1";
            resources.ApplyResources(this.createIFToolStripMenuItem1, "createIFToolStripMenuItem1");
            this.createIFToolStripMenuItem1.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.createIFToolStripMenuItem1.Click += new System.EventHandler(this.addOuterIFToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
            // 
            // removeToolStripMenuItem3
            // 
            this.removeToolStripMenuItem3.Name = "removeToolStripMenuItem3";
            resources.ApplyResources(this.removeToolStripMenuItem3, "removeToolStripMenuItem3");
            this.removeToolStripMenuItem3.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem3.Click += new System.EventHandler(this.removeActionMenu_Click);
            // 
            // contextMenuIntegration
            // 
            this.contextMenuIntegration.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createIFToolStripMenuItem2,
            this.toolStripMenuItem5,
            this.removeToolStripMenuItem4});
            this.contextMenuIntegration.Name = "contextMenuNavigation";
            resources.ApplyResources(this.contextMenuIntegration, "contextMenuIntegration");
            // 
            // createIFToolStripMenuItem2
            // 
            this.createIFToolStripMenuItem2.Name = "createIFToolStripMenuItem2";
            resources.ApplyResources(this.createIFToolStripMenuItem2, "createIFToolStripMenuItem2");
            this.createIFToolStripMenuItem2.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.createIFToolStripMenuItem2.Click += new System.EventHandler(this.addOuterIFToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
            // 
            // removeToolStripMenuItem4
            // 
            this.removeToolStripMenuItem4.Name = "removeToolStripMenuItem4";
            resources.ApplyResources(this.removeToolStripMenuItem4, "removeToolStripMenuItem4");
            this.removeToolStripMenuItem4.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem4.Click += new System.EventHandler(this.removeIntegrationMenu_Click);
            // 
            // contextMenuNavigation
            // 
            this.contextMenuNavigation.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createIFToolStripMenuItem3,
            this.toolStripMenuItem6,
            this.removeToolStripMenuItem5});
            this.contextMenuNavigation.Name = "contextMenuNavigation";
            resources.ApplyResources(this.contextMenuNavigation, "contextMenuNavigation");
            // 
            // createIFToolStripMenuItem3
            // 
            this.createIFToolStripMenuItem3.Name = "createIFToolStripMenuItem3";
            resources.ApplyResources(this.createIFToolStripMenuItem3, "createIFToolStripMenuItem3");
            this.createIFToolStripMenuItem3.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.createIFToolStripMenuItem3.Click += new System.EventHandler(this.addOuterIFToolStripMenuItem_Click);
            // 
            // removeToolStripMenuItem5
            // 
            this.removeToolStripMenuItem5.Name = "removeToolStripMenuItem5";
            resources.ApplyResources(this.removeToolStripMenuItem5, "removeToolStripMenuItem5");
            this.removeToolStripMenuItem5.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.removeToolStripMenuItem5.Click += new System.EventHandler(this.removeCNavigationPageMenu_Click);
            // 
            // contextMenuHtmlStringBinding
            // 
            this.contextMenuHtmlStringBinding.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ifSubqueryToolStripMenuItem});
            this.contextMenuHtmlStringBinding.Name = "contextMenuHtmlStringBinding";
            resources.ApplyResources(this.contextMenuHtmlStringBinding, "contextMenuHtmlStringBinding");
            // 
            // ifSubqueryToolStripMenuItem
            // 
            this.ifSubqueryToolStripMenuItem.Name = "ifSubqueryToolStripMenuItem";
            resources.ApplyResources(this.ifSubqueryToolStripMenuItem, "ifSubqueryToolStripMenuItem");
            this.ifSubqueryToolStripMenuItem.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.ifSubqueryToolStripMenuItem.Click += new System.EventHandler(this.addOuterIFToolStripMenuItem_Click);
            // 
            // loadConditionMenu
            // 
            this.loadConditionMenu.Name = "loadConditionMenu";
            resources.ApplyResources(this.loadConditionMenu, "loadConditionMenu");
            this.loadConditionMenu.MouseHover += new System.EventHandler(this.MenuItem_MouseHover);
            this.loadConditionMenu.Click += new System.EventHandler(this.loadConditionMenu_Click);
            // 
            // toolStripBtnSelect
            // 
            resources.ApplyResources(this.toolStripBtnSelect, "toolStripBtnSelect");
            this.toolStripBtnSelect.Name = "toolStripBtnSelect";
            this.toolStripBtnSelect.Click += new System.EventHandler(this.toolStripBtnSelect_Click);
            // 
            // toolStripBtnRemove
            // 
            resources.ApplyResources(this.toolStripBtnRemove, "toolStripBtnRemove");
            this.toolStripBtnRemove.Name = "toolStripBtnRemove";
            this.toolStripBtnRemove.Click += new System.EventHandler(this.toolStripBtnRemove_Click);
            // 
            // toolStripBtnUndo
            // 
            resources.ApplyResources(this.toolStripBtnUndo, "toolStripBtnUndo");
            this.toolStripBtnUndo.Name = "toolStripBtnUndo";
            this.toolStripBtnUndo.Click += new System.EventHandler(this.toolStripBtnUndo_Click);
            // 
            // toolStripBtnMarkLeft
            // 
            resources.ApplyResources(this.toolStripBtnMarkLeft, "toolStripBtnMarkLeft");
            this.toolStripBtnMarkLeft.Name = "toolStripBtnMarkLeft";
            this.toolStripBtnMarkLeft.Click += new System.EventHandler(this.toolStripBtnMarkLeft_Click);
            // 
            // toolStripBtnMarkRight
            // 
            resources.ApplyResources(this.toolStripBtnMarkRight, "toolStripBtnMarkRight");
            this.toolStripBtnMarkRight.Name = "toolStripBtnMarkRight";
            this.toolStripBtnMarkRight.Click += new System.EventHandler(this.toolStripBtnMarkRight_Click);
            // 
            // toolStripBtnAddParentheses
            // 
            resources.ApplyResources(this.toolStripBtnAddParentheses, "toolStripBtnAddParentheses");
            this.toolStripBtnAddParentheses.Name = "toolStripBtnAddParentheses";
            this.toolStripBtnAddParentheses.Click += new System.EventHandler(this.toolStripBtnAddParentheses_Click);
            // 
            // toolStripBtnRemoveParentheses
            // 
            resources.ApplyResources(this.toolStripBtnRemoveParentheses, "toolStripBtnRemoveParentheses");
            this.toolStripBtnRemoveParentheses.Name = "toolStripBtnRemoveParentheses";
            this.toolStripBtnRemoveParentheses.Click += new System.EventHandler(this.toolStripBtnRemoveParentheses_Click);
            // 
            // toolStripStatusIcon
            // 
            this.toolStripStatusIcon.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusIcon.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripStatusIcon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripStatusIcon.Image = global::Perfectus.Client.Studio.QueryEditControl.Properties.Resources.delete;
            this.toolStripStatusIcon.Name = "toolStripStatusIcon";
            resources.ApplyResources(this.toolStripStatusIcon, "toolStripStatusIcon");
            // 
            // codeWnd
            // 
            this.codeWnd.BackColor = System.Drawing.SystemColors.Info;
            this.codeWnd.DetectUrls = false;
            resources.ApplyResources(this.codeWnd, "codeWnd");
            this.codeWnd.HideSelection = false;
            this.codeWnd.Name = "codeWnd";
            this.codeWnd.ReadOnly = true;
            this.codeWnd.SelectionUnderlineColor = Perfectus.Client.Studio.QueryEditControl.QueryCtrl.UnderlineColor.Black;
            this.codeWnd.SelectionUnderlineStyle = Perfectus.Client.Studio.QueryEditControl.QueryCtrl.UnderlineStyle.Normal;
            // 
            // QueryEditUserControl
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.statusStrip);
            this.MinimumSize = new System.Drawing.Size(318, 174);
            this.Name = "QueryEditUserControl";
            this.Load += new System.EventHandler(this.CodeEditUserControl_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolSelectStrip.ResumeLayout(false);
            this.toolSelectStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.contextMenuPlaceholder.ResumeLayout(false);
            this.contextMenuValue.ResumeLayout(false);
            this.contextMenuOperator.ResumeLayout(false);
            this.contextMenuConcatenation.ResumeLayout(false);
            this.contextMenuStatement.ResumeLayout(false);
            this.contextMenuElse.ResumeLayout(false);
            this.contextMenuWordOutcome.ResumeLayout(false);
            this.contextMenuIntegration.ResumeLayout(false);
            this.contextMenuNavigation.ResumeLayout(false);
            this.contextMenuHtmlStringBinding.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private StatusStrip statusStrip;
        private ToolStripStatusLabel statusStripLabel;
        private ToolStripStatusLabel toolStripStatusIcon;
        private ToolStripContainer toolStripContainer1;
        private QueryCtrl codeWnd;
        private ToolStrip toolSelectStrip;
        private ToolStripButton toolStripBtnSelect;
        private ToolStripButton toolStripBtnRemove;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton toolStripBtnMarkLeft;
        private ToolStripButton toolStripBtnMarkRight;
        private ToolStripButton toolStripBtnAddParentheses;
        private ToolStripButton toolStripBtnRemoveParentheses;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripButton toolStripBtnUndo;
        private ToolStripComboBox toolStripZoom;
        private ToolStripSeparator toolStripSeparator4;
        private ContextMenuStrip contextMenuPlaceholder;
        private ToolStripMenuItem toActionPlaceholderMenu;
        private ToolStripMenuItem createStatementToolStripMenuItem;
        private ContextMenuStrip contextMenuValue;
        private ContextMenuStrip contextMenuOperator;
        private ContextMenuStrip contextMenuConcatenation;
        private ContextMenuStrip contextMenuStatement;
        private ContextMenuStrip contextMenuElse;
        private ToolStripMenuItem resetQuestionMenu;
        private ToolStripSeparator toolStripMenuItem1;
        private ToolStripMenuItem addANDRightConditionMenu;
        private ToolStripMenuItem addORRightConditionMenu;
        private ToolStripMenuItem removeConditionMenu;
        private ToolStripSeparator toolStripMenuItem2;
        private ToolStripMenuItem andToolStripMenuItem;
        private ToolStripMenuItem orToolStripMenuItem;
        private ToolStripMenuItem removeToolStripMenuItem;
        private ToolStripMenuItem toggleConcatenationMenu;
        private ToolStripMenuItem createIFToolStripMenuItem;
        private ToolStripMenuItem createELSEToolStripMenuItem1;
        private ToolStripSeparator toolStripMenuItem3;
        private ToolStripMenuItem removeToolStripMenuItem1;
        private ToolStripMenuItem removeToolStripMenuItem2;
        private ContextMenuStrip contextMenuWordOutcome;
        private ContextMenuStrip contextMenuIntegration;
private ContextMenuStrip contextMenuNavigation;
        private ToolStripMenuItem createIFToolStripMenuItem1;
        private ToolStripSeparator toolStripMenuItem4;
        private ToolStripMenuItem removeToolStripMenuItem3;
        private ToolStripMenuItem createIFToolStripMenuItem2;
        private ToolStripMenuItem createIFToolStripMenuItem3;
        private ToolStripSeparator toolStripMenuItem5;
        private ToolStripMenuItem removeToolStripMenuItem4;
        private ToolStripMenuItem removeToolStripMenuItem5;
        private ToolStripSeparator toolStripMenuItem6;
        private ToolStripSeparator toolStripMenuItem7;
        private ToolStripMenuItem eq_op_menu;
        private ToolStripMenuItem neq_op_menu;
        private ToolStripMenuItem ge_op_menu;
        private ToolStripMenuItem g_op_menu;
        private ToolStripMenuItem le_op_menu;
        private ToolStripMenuItem l_op_menu;
        private ContextMenuStrip contextMenuHtmlStringBinding;
        private ToolStripMenuItem ifSubqueryToolStripMenuItem;
        private ToolStripMenuItem loadConditionMenu;
    }
}
