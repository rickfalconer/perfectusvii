using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using mshtml;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.PackageObjectEditors;
using System.Diagnostics;
//using Perfectus.Client.Studio.UI.Designers;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.Controls
{
	/// <summary>
	/// Summary description for StringBindingEditorControl.
	/// </summary>
	public class StringBindingEditorControl2 : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = new System.ComponentModel.Container( );
		private WebBrowser editor1;		
		private string pendingDocBody;
		private Package packageBeingEdited;
        private Panel panel1;
        //private bool documentCompleteEventFired = false;
        //private bool documentEditMode = false;
        private bool havePainted = false;
        private bool haveSetEditMode = false;
        //private System.Windows.Forms.Timer timer = null;

		public StringBindingEditorControl2()
		{
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent( );

            // Add the webbrowser to our components list so that it gets disposed.
            components.Add( this.editor1 );

            //this.Enter += new System.EventHandler( StringBindingEditorControl2_Enter );
        }

        //void StringBindingEditorControl2_Enter(object sender, System.EventArgs e)
        //{
        //    // FB2405: Now that the ExecCommand has been called, we need to reset the contents of the document so that the WebBrowser will
        //    // have a document loaded and the Document Completed event will fire.
        //    editor1.Refresh( WebBrowserRefreshOption.Completely );
        //}

		public Package Package
		{
			get {return packageBeingEdited;}
			set { packageBeingEdited = value;}
		}

		public string HtmlBody
		{
			get 
			{
				return editor1.Document.Body.InnerHtml;		
			}
		}
		
		public string XmlBody
		{
			get 
			{
				return StringBindingMod.GetXmlFromDoc((HTMLDocument)(editor1.Document.DomDocument),true);
			}
			set 
			{
				if(value != null)
				{
					if(editor1.ReadyState == WebBrowserReadyState.Complete)
					{
                        System.String tempStr = StringBindingMod.GetDocHtmlFromXml(value, packageBeingEdited);
                        if (null != tempStr)
                        {
                            int normalizedLength = tempStr.Length - tempStr.TrimStart(null).Length;
                            if (normalizedLength > 0)
                            {
                                tempStr = tempStr.TrimStart(null);
                                for (int i = 0; i < normalizedLength; i++)
                                    tempStr = "&nbsp;" + tempStr;
                            }
                        }
                        editor1.Document.Body.InnerHtml = tempStr;
					}
					else
					{
						pendingDocBody = StringBindingMod.GetDocHtmlFromXml(value,packageBeingEdited);
                        // it's possible the ready state changed during execution of last method, in which case we need to set html now!
                        if (editor1.ReadyState == WebBrowserReadyState.Complete)
						{
							editor1.Document.Body.InnerHtml = pendingDocBody;
						}
					}
				}
				else
				{
                    if (editor1.ReadyState == WebBrowserReadyState.Complete)
                    {
						editor1.Document.Body.InnerHtml = "";
					}
					else
					{
						pendingDocBody = "";
					}
				}
			}         
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.editor1 = new System.Windows.Forms.WebBrowser( );
            this.panel1 = new System.Windows.Forms.Panel( );
            this.panel1.SuspendLayout( );
            this.SuspendLayout( );
            // 
            // editor1
            // 
            this.editor1.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                        | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.editor1.Location = new System.Drawing.Point( 1, 1 );
            this.editor1.Name = "editor1";
            this.editor1.Size = new System.Drawing.Size( 535, 371 );
            this.editor1.TabIndex = 7;
            this.editor1.DocumentText = string.Empty;
            editor1.Document.ExecCommand("EditMode", true, null);
            haveSetEditMode = true;            
            //this.editor1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler( this.editor1_DocumentCompleted );
            // 
            // panel1
            // 
            this.panel1.Controls.Add( this.editor1 );
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.panel1.Location = new System.Drawing.Point( 0, 0 );
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size( 537, 373 );
            this.panel1.TabIndex = 8;
            // 
            // StringBindingEditorControl2
            // 
            this.Controls.Add( this.panel1 );
            this.Name = "StringBindingEditorControl2";
            this.Size = new System.Drawing.Size( 537, 373 );
            //this.Load += new System.EventHandler( this.StringBindingEditorControl2_Load );
            //this.Paint += new System.Windows.Forms.PaintEventHandler(this.StringBindingEditorControl2_Paint);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.StringBindingEditorControl_Paint);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout( false );

		}
		#endregion		
	
        internal void SetFrame(bool Enabled)
        {
            panel1.BackColor = Enabled ? Control.DefaultForeColor : Control.DefaultBackColor;
        }

        void StringBindingEditorControl_Paint(object sender, PaintEventArgs e)
        {
            if (havePainted)
            {
                return;
            }
            if (!haveSetEditMode)
            {
                return;
            }
            IHTMLDocument2 doc2 = (IHTMLDocument2)editor1.Document.DomDocument;

            //pf-368 don't allow an infinite loop waiting for browser control
            for (int i = 0; i < 25; i++) //5 secs 
            {
                Thread.Sleep(200);
                Application.DoEvents();
                Debug.WriteLine(string.Format("StringBindingEditorControl_Paint: {0}", editor1.ReadyState));
                if (editor1.ReadyState == WebBrowserReadyState.Complete)
                    i = 50; //exit for asap
            }

            if (pendingDocBody != null)
            {
                //editor1.Document.Body.InnerHtml = StringBindingMod.GetDocHtmlFromXml(pendingBodyValue, PackageBeingEdited);
                editor1.Document.Body.InnerHtml = pendingDocBody;

            }

            if (doc2.styleSheets.length == 0)
            {
                string cssAddress =
                    string.Format(@"file://{0}\stringEditor.css",
                                  Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                doc2.createStyleSheet(cssAddress, 0);
            }
            havePainted = true;

        }
        
        //private void editor1_DocumentCompleted( object sender, WebBrowserDocumentCompletedEventArgs e )
        //{
        //    if( documentCompleteEventFired == false )
        //    {
        //        // FB2405 : Only once the document is completed, do we load the style sheet and update the inner html 
        //        // if we already have content.
        //        IHTMLDocument2 doc2 = (IHTMLDocument2)editor1.Document.DomDocument;
        //        if( doc2.styleSheets.length == 0 )
        //        {
        //            string cssAddress = string.Format( @"file://{0}\stringEditor.css", Path.GetDirectoryName( Assembly.GetExecutingAssembly( ).Location ) );
        //            doc2.createStyleSheet( cssAddress, 0 );

        //        }

        //        // Load any existing content.
        //        if( pendingDocBody != null )
        //            editor1.Document.Body.InnerHtml = pendingDocBody;
        //    }
        //    documentCompleteEventFired = true;
        //}

        //private void StringBindingEditorControl2_Load( object sender, System.EventArgs e )
        //{
        //    // FB2405: The doco says one must not access the Document object until the document completed event has fired. However if we wait 
        //    // for the event, and then call the ExecCommand to place the control in edit mode, the contents disappear and the style sheet is gone.
        //    // Hence we must call the ExecCommand before the document is loaded?? However at this stage there wouldn't be a Document object,
        //    // hence the call to set the DocumentText which automatically creates a Document object.
        //    this.editor1.DocumentText = string.Empty;
        //    this.editor1.Document.ExecCommand( "EditMode", true, null );
           
        //    Application.DoEvents( );
        //}

        //private void timer_Tick( object sender, System.EventArgs e )
        //{
        //    if( documentCompleteEventFired )
        //    {
        //        this.timer.Enabled = false;
        //        return;
        //    }

        //    // FB2405: Try kicking the activex control to fire the document complete event.
        //    this.editor1.Refresh( WebBrowserRefreshOption.Completely );
        //}

        //private void StringBindingEditorControl2_Paint( object sender, PaintEventArgs e )
        //{
        //    // FB2405: On some systems the DocumentComplete event just doesn't fire. If we get here and it hasn't yet fired
        //    // its a good chance its not going to. We need to kick the activex control to try again. Sometime the state is complete
        //    // but the event still hosn't fired. The cause appears to be due to setting the EditMode too early.
        //    if( ( documentCompleteEventFired == false ) && ( this.timer == null ) )
        //    {
        //        this.timer = new System.Windows.Forms.Timer( components );
        //        this.timer.Interval = 200;
        //        this.timer.Enabled = true;
        //        this.timer.Tick += new System.EventHandler( this.timer_Tick );
        //    }
        //}
    }
}
