using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner;
using Perfectus.Client.Studio.ExpressionLibrary.Converter;
using Perfectus.Client.Studio.ExpressionLibrary;
using Perfectus.Common;
using System.Resources;

namespace Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery
{
    public enum StringBindingQueryFormMode
    {
        QueryMode = 0,
        StringBindingMode = 1
    }

    public partial class StringBindingQueryForm : Form, IQueryDesigner
    {
        private Package _package;
        private TextQuestionQueryValue val;
        private ResourceManager rm;
        private StringBindingQueryFormMode formMode = StringBindingQueryFormMode.StringBindingMode;
        private CHtmlStringBinding currentStringBindingText;

        private Boolean _haveChanges;

        public TextQuestionQueryValue Val
        {
            get { return val; }
            set
            {
                val = value;

                if (val.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Query)
                {
                    formMode = StringBindingQueryFormMode.QueryMode;
                    ShowQuerySection(value);
                }
                else if (val.ValueType == TextQuestionQueryValue.TextQuestionQueryValueType.Text)
                {
                    formMode = StringBindingQueryFormMode.StringBindingMode;
                    stringBindingUCtrl.SetData(val.TextValue);
                    splitContainer1.SplitterDistance = 0;
                }
            }
        }

        public Package Package
        {
            get { return _package; }
            set
            {
                _package = value;
                this.stringBindingUCtrl.Package = _package;
            }
        }

        public StringBindingQueryForm()
        {
            InitializeComponent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StringBindingQueryForm));
            rm = ResourceLoader.GetResourceManager("Perfectus.Client.Studio.Localisation");
            this.queryEditUserControl1.SetQueryFormatterAndMode(
                 new Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter.CPrintHtmlStringBindingFormatter(),
                 QueryDesignerMode.HTMLBinding);

            this.queryEditUserControl1.HtmlStringBindingClick += new QueryEditUserControl.ActionClick(queryEditUserControl1_HtmlStringBindingClick);

            this.Load += new EventHandler(StringBindingQueryForm_Load);

            this.queryEditUserControl1.selectionChanged += new QueryEditUserControl.SelectionChanged(queryEditUserControl1_selectionChanged);
            splitContainer1.SplitterDistance = 0; //default to query section hidden
            splitContainer1.IsSplitterFixed = true;
            _haveChanges = false;
        }

        void StringBindingQueryForm_Load(object sender, EventArgs e)
        {
            stringBindingUCtrl.EnforceFocusToWebBrowser();
        }

        CHtmlStringBinding htmlObject;

        private void SetData(CHtmlStringBinding bindingObj)
        {
            if (bindingObj != null)
            {
                String text = stringBindingUCtrl.GetData();
                if (htmlObject.Payload != text)
                {
                    _haveChanges = true;
                    htmlObject.Payload = text;
                    this.queryEditUserControl1.ReDraw();
                }
            }
        }

        void queryEditUserControl1_selectionChanged(CAtom sender)
        {
            if (sender == htmlObject)
                return;

            if (sender is CHtmlStringBinding)
            {
                SetData(htmlObject);

                htmlObject = sender as CHtmlStringBinding;
                stringBindingUCtrl.SetData(htmlObject.Payload);
                currentStringBindingText = htmlObject;

                this.stringBindingUCtrl.Enabled = true;
            }
            else
            {
                SetData(htmlObject);
                stringBindingUCtrl.SetData(String.Empty);
                htmlObject = null;
                this.stringBindingUCtrl.Enabled = false;
            }
        }

        void queryEditUserControl1_HtmlStringBindingClick(CExpressionBase sender)
        {
            this.stringBindingUCtrl.Enabled = true;

            if (sender is CHtmlStringBinding)
            {
                SetData(htmlObject);

                htmlObject = sender as CHtmlStringBinding;
                stringBindingUCtrl.SetData(htmlObject.Payload);
                currentStringBindingText = htmlObject;
                stringBindingUCtrl.EnforceFocusToWebBrowser();
            }
            else
            {
                SetData(htmlObject);
                stringBindingUCtrl.SetData(String.Empty);
                this.stringBindingUCtrl.Enabled = false;
                htmlObject = null;
            }
        }

        #region IQueryDesigner Members

        public QueryDesignerMode Mode
        {
            get { return QueryDesignerMode.HTMLBinding; }
            set { throw new Exception("You cannot set the Query Mode for this window."); }
        }

        private Query queryBeingEdited;
        public Query QueryBeingEdited
        {
            get { return queryBeingEdited; }
            set
            {
                queryBeingEdited = value;
            }
        }

        private PackageItem itemBeingEdited;
        public PackageItem ItemBeingEdited
        {
            get { return itemBeingEdited; }
            set
            {
                itemBeingEdited = value;
                this.Text += value.Name;
            }
        }
        #endregion

        #region circular
        private bool EnsureNoCircularRefs(Query query)
        {
            //here we check that the package item being edited is not included in the query, or string bound text anywhere.
            List<PackageItem> dependentPackageItems = new List<PackageItem>();

            // Action if true dependencies
            SetHtmlActionDependencies(dependentPackageItems, query.ActionIfTrue);

            // Action if false dependencies
            SetHtmlActionDependencies(dependentPackageItems, query.ActionIfFalse);

            // Query expression dependencies
            SetDependentObjects(dependentPackageItems, query.QueryExpression.RootExpressionNode);

            return dependentPackageItems.Contains(itemBeingEdited);
        }

        private void SetHtmlActionDependencies(List<PackageItem> dependentPackageItems, ActionBase htmlAction)
        {
            if (htmlAction != null)
            {
                if (htmlAction.SubQuery != null)
                {
                    // Sub query action if true
                    SetHtmlActionDependencies(dependentPackageItems, htmlAction.SubQuery.ActionIfTrue);

                    // Sub query action if false
                    SetHtmlActionDependencies(dependentPackageItems, htmlAction.SubQuery.ActionIfFalse);

                    // Sub query query expression
                    SetDependentObjects(dependentPackageItems, htmlAction.SubQuery.QueryExpression.RootExpressionNode);
                }
                else if (((HtmlStringBindingAction)htmlAction).Payload != null)
                {
                    PackageItem[] items = StringBinding.GetPackageItemsFromXml(((HtmlStringBindingAction)htmlAction).Payload, _package.StringBindingItemByGuid);

                    if (items != null)
                    {
                        foreach (PackageItem pi in items)
                        {
                            if (!dependentPackageItems.Contains(pi))
                            {
                                dependentPackageItems.Add(pi);
                            }
                        }
                    }
                }
            }
        }

        private void SetDependentObjects(List<PackageItem> dependentPackageItems, PackageItem packageItem)
        {
            if (packageItem is ExpressionNode)
            {
                SetDependentObjects(dependentPackageItems, ((ExpressionNode)packageItem).Operand1);
                SetDependentObjects(dependentPackageItems, ((ExpressionNode)packageItem).Operand2);
            }
            else
            {
                if (packageItem != null && !(packageItem is PackageConstant))
                {
                    // Add the dependent object if it is not a package constant and if it is not already in the dependencies collection
                    if (!dependentPackageItems.Contains(packageItem))
                    {
                        dependentPackageItems.Add(packageItem);
                    }
                }
            }
        }
        #endregion

        private void btnOk_Click(object sender, EventArgs e)
        {
            string stringBindingText = stringBindingUCtrl.GetData();

            //check our query is valid, if mode is "add query"
            if (formMode == StringBindingQueryFormMode.QueryMode)
            {
                try
                {
                    //set the last string binding value to our last selected html payload
                    if (currentStringBindingText != null)
                    {
                        currentStringBindingText.Payload = stringBindingUCtrl.GetData();
                    }

                    val = new TextQuestionQueryValue();
                    val.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Query;
                    CQuery cQuery = queryEditUserControl1.GetQuery();
                    CConverter converter = new CConverter(_package);
                    Query query = converter.Convert(cQuery);

                    // restore before showing dialog.
                    stringBindingUCtrl.SetData(stringBindingText);

                    //we need to check that "itemBeingEdited" is not part of the query
                    if (EnsureNoCircularRefs(query))
                    {
                        MessageBox.Show(string.Format(rm.GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.StringBindingQueryForm.CircularTextQuestionQueryItem"), itemBeingEdited.Name));
                        return;
                    }

                    val.QueryValue = query;
                }
                catch (Exception ex)
                {
                    // restore before showing dialog.
                    stringBindingUCtrl.SetData(stringBindingText);

                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                val = new TextQuestionQueryValue();
                val.ValueType = TextQuestionQueryValue.TextQuestionQueryValueType.Text;
                val.TextValue = stringBindingUCtrl.GetData();

                PackageItem[] items = StringBinding.GetPackageItemsFromXml(val.TextValue, _package.StringBindingItemByGuid);

                if (items != null)
                {
                    foreach (PackageItem pi in items)
                    {
                        if (pi == itemBeingEdited)
                        {
                            // restore before showing dialog.
                            stringBindingUCtrl.SetData(stringBindingText);

                            MessageBox.Show(string.Format(rm.GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.StringBindingQueryForm.CircularRefStringBinding"), itemBeingEdited.Name));
                            return;
                        }
                    }
                }
            }

            this.DialogResult = DialogResult.OK;
        }

        private void btnAddRemQuery_Click(object sender, EventArgs e)
        {
            string stringBindingText = stringBindingUCtrl.GetData();

            //when showing, if there is any text available on editor, place it in first line of true in the query
            //when hiding, if we have a query, show warning before removing it and setting back to normal
            if (formMode == StringBindingQueryFormMode.StringBindingMode)//show the query control
            {
                ShowQuerySection();
                formMode = StringBindingQueryFormMode.QueryMode;

                // restore before showing dialog.
                stringBindingUCtrl.SetData(stringBindingText);
            }
            else//show warning and destroy the query..
            {
                // restore before showing dialog.
                stringBindingUCtrl.SetData(stringBindingText);
                {
                    if (val.QueryValue != null && val.QueryValue.ActionIfTrue is HtmlStringBindingAction)
                    {
                        HtmlStringBindingAction action = val.QueryValue.ActionIfTrue as HtmlStringBindingAction;
                        stringBindingUCtrl.SetData(action.Payload);
                    }
                    btnAddRemQuery.Text = rm.GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.StringBindingQueryForm.AddQuery");
                    formMode = StringBindingQueryFormMode.StringBindingMode;
                    stringBindingUCtrl.Enabled = true;
                    splitContainer1.SplitterDistance = 0;
                    queryEditUserControl1.Visible = false;
                    splitContainer1.IsSplitterFixed = true;
                    currentStringBindingText = null;
                }/*
                else
                {
                    //do nothing
                    return;
                }*/
            }
        }

        private void ShowQuerySection()
        {
            ShowQuerySection(null);
        }

        private void ShowQuerySection(TextQuestionQueryValue tqv)
        {
            if (tqv == null)
            {
                tqv = new TextQuestionQueryValue();
                //place our text in the first line of query
                tqv.QueryValue = Package.CreateQuery();
                tqv.QueryValue.ActionIfTrue = new HtmlStringBindingAction();
                ((HtmlStringBindingAction)tqv.QueryValue.ActionIfTrue).Payload = stringBindingUCtrl.GetData();
                tqv.QueryValue.ActionIfFalse = new HtmlStringBindingAction();
            }

            Creator cvrt = new Creator(_package);
            ExpressionHelper.Reset();
            CQuery cQuery = cvrt.Create(tqv.QueryValue);

            queryEditUserControl1.SetQuery(_package, cQuery);
            stringBindingUCtrl.Enabled = false;

            btnAddRemQuery.Text = rm.GetString("Perfectus.Client.Studio.UI.PackageObjectEditors.TypeConverters.StringBindingQueryForm.RemoveQuery");
            splitContainer1.SplitterDistance = splitContainer1.Height / 2;
            queryEditUserControl1.Visible = true;
            splitContainer1.IsSplitterFixed = false;
        }

        private void StringBindingQueryForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetData(htmlObject);
            if ((queryEditUserControl1.haveUnicommitedChanged ||
                _haveChanges) &&
                this.DialogResult != DialogResult.OK)
            {
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StringBindingQueryForm));

                DialogResult result = MessageBox.Show(
                resources.GetString("ConfirmCancel"),
                resources.GetString("ConfirmCancelCaption"),
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                    btnOk.PerformClick();
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }
    }
}