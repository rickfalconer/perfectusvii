namespace Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery
{
    partial class StringBindingQueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.queryEditUserControl1 = new Perfectus.Client.Studio.QueryEditControl.QueryEditUserControl();
            this.btnOk = new System.Windows.Forms.Button();
            this.stringBindingUCtrl = new Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery.StringBindingUCtrl();
            this.btnAddRemQuery = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // queryEditUserControl1
            // 
            this.queryEditUserControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.queryEditUserControl1.Location = new System.Drawing.Point(0, 0);
            this.queryEditUserControl1.MinimumSize = new System.Drawing.Size(318, 174);
            this.queryEditUserControl1.Name = "queryEditUserControl1";
            this.queryEditUserControl1.Size = new System.Drawing.Size(547, 185);
            this.queryEditUserControl1.TabIndex = 0;
            this.queryEditUserControl1.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.Location = new System.Drawing.Point(395, 397);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // stringBindingUCtrl
            // 
            this.stringBindingUCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stringBindingUCtrl.Location = new System.Drawing.Point(0, 0);
            this.stringBindingUCtrl.Name = "stringBindingUCtrl";
            this.stringBindingUCtrl.Size = new System.Drawing.Size(547, 198);
            this.stringBindingUCtrl.TabIndex = 0;
            // 
            // btnAddRemQuery
            // 
            this.btnAddRemQuery.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddRemQuery.Location = new System.Drawing.Point(2, 396);
            this.btnAddRemQuery.Name = "btnAddRemQuery";
            this.btnAddRemQuery.Size = new System.Drawing.Size(110, 24);
            this.btnAddRemQuery.TabIndex = 2;
            this.btnAddRemQuery.Text = "Add Query";
            this.btnAddRemQuery.UseVisualStyleBackColor = true;
            this.btnAddRemQuery.Click += new System.EventHandler(this.btnAddRemQuery_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(475, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.splitContainer1.Location = new System.Drawing.Point(2, 1);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.queryEditUserControl1);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.stringBindingUCtrl);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(549, 391);
            this.splitContainer1.SplitterDistance = 187;
            this.splitContainer1.TabIndex = 4;
            // 
            // StringBindingQueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 421);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAddRemQuery);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimumSize = new System.Drawing.Size(457, 410);
            this.Name = "StringBindingQueryForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StringBindingQueryForm_FormClosing);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private QueryEditUserControl queryEditUserControl1;
        private System.Windows.Forms.Button btnOk;
        private StringBindingUCtrl stringBindingUCtrl;
        private System.Windows.Forms.Button btnAddRemQuery;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}