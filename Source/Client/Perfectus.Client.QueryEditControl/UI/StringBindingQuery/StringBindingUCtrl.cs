using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;
using Perfectus.Common;

namespace Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery
{
    public partial class StringBindingUCtrl : UserControl
    {
        internal void EnforceFocusToWebBrowser()
        {
            htmlEditorCtrl.Select();
            htmlEditorCtrl.Focus();
        }

        public Package Package
        {
            set 
            { 
                cpiTreeSearch1.Package = value;
                htmlEditorCtrl.Package = value;
            }
        }

        public StringBindingUCtrl()
        {
            InitializeComponent();
            htmlEditorCtrl.SetFrame(Enabled);
            this.EnabledChanged += new EventHandler(StringBindingUCtrl_EnabledChanged);
        }

        void StringBindingUCtrl_EnabledChanged(object sender, EventArgs e)
        {
            htmlEditorCtrl.SetFrame(Enabled);
        }

        public void SetData(String param_data)
        {
            htmlEditorCtrl.XmlBody = param_data;
        }
        public String GetData()
        {
            return htmlEditorCtrl.XmlBody == null ? null : htmlEditorCtrl.XmlBody.TrimEnd(null);
        }

        private void StringBindingUCtrl_Load(object sender, EventArgs e)
        {
            // Setup the mini controls
            // Supported items...
            this.cpiTreeSearch1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.QUESTION);
            this.cpiTreeSearch1.AddPackageItemType(SelectorGlobal.VisiblePackageItemType.FUNCTION);

            // Supported item types...
            this.cpiTreeSearch1.AddDatatype(PerfectusDataType.Number);
            this.cpiTreeSearch1.AddDatatype(PerfectusDataType.Text);
            this.cpiTreeSearch1.AddDatatype(PerfectusDataType.Date);
            this.cpiTreeSearch1.AddDatatype(PerfectusDataType.YesNo);

            // Supported drag-drop operations
            this.cpiTreeSearch1.AddDragNDropOperation(SelectorGlobal.DragNDropOperation.SOURCE_HTML);
        }
    }
}
