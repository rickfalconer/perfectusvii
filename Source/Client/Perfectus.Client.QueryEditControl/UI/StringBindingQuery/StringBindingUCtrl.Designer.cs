using Perfectus.Client.Studio.UI.PackageObjectEditors.Controls;
namespace Perfectus.Client.Studio.QueryEditControl.UI.StringBindingQuery
{
    partial class StringBindingUCtrl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cpiTreeSearch1 = new Perfectus.Client.Studio.QueryEditControl.CPITreeSearch();
            this.htmlEditorCtrl = new StringBindingEditorControl2();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cpiTreeSearch1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.htmlEditorCtrl);
            this.splitContainer1.Size = new System.Drawing.Size(340, 320);
            this.splitContainer1.SplitterDistance = 113;
            this.splitContainer1.TabIndex = 0;
            // 
            // cpiTreeSearch1
            // 
            this.cpiTreeSearch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cpiTreeSearch1.Location = new System.Drawing.Point(0, 0);
            this.cpiTreeSearch1.Margin = new System.Windows.Forms.Padding(0);
            this.cpiTreeSearch1.Name = "cpiTreeSearch1";
            this.cpiTreeSearch1.Size = new System.Drawing.Size(113, 320);
            this.cpiTreeSearch1.TabIndex = 0;
            // 
            // htmlEditorCtrl
            // 
            this.htmlEditorCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.htmlEditorCtrl.Location = new System.Drawing.Point(0, 0);
            this.htmlEditorCtrl.Name = "htmlEditorCtrl";
            this.htmlEditorCtrl.Size = new System.Drawing.Size(223, 320);
            this.htmlEditorCtrl.TabIndex = 0;
            this.htmlEditorCtrl.Text = "htmlEditorCtrl";
            // 
            // StringBindingUCtrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "StringBindingUCtrl";
            this.Size = new System.Drawing.Size(340, 320);
            this.Load += new System.EventHandler(this.StringBindingUCtrl_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private CPITreeSearch cpiTreeSearch1;
        private StringBindingEditorControl2 htmlEditorCtrl;

    }
}
