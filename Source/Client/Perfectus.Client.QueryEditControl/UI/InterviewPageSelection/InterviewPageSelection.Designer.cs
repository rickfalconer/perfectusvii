namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
    partial class InterviewPageSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container( );
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( InterviewPageSelection ) );
            this.listView1 = new System.Windows.Forms.ListView( );
            this.Page = new System.Windows.Forms.ColumnHeader( );
            this.Note = new System.Windows.Forms.ColumnHeader( );
            this.imageList1 = new System.Windows.Forms.ImageList( this.components );
            this.btnCancel = new System.Windows.Forms.Button( );
            this.btnOk = new System.Windows.Forms.Button( );
            this.checkBox1 = new System.Windows.Forms.CheckBox( );
            this.SuspendLayout( );
            // 
            // listView1
            // 
            this.listView1.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                        | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.listView1.Columns.AddRange( new System.Windows.Forms.ColumnHeader[ ] {
            this.Page,
            this.Note} );
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView1.LargeImageList = this.imageList1;
            this.listView1.Location = new System.Drawing.Point( 2, 12 );
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size( 206, 143 );
            this.listView1.SmallImageList = this.imageList1;
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler( this.listView1_MouseDoubleClick );
            this.listView1.SelectedIndexChanged += new System.EventHandler( this.listView1_SelectedIndexChanged );
            // 
            // Page
            // 
            this.Page.Text = "Page";
            // 
            // Note
            // 
            this.Note.Text = "Note";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ( ( System.Windows.Forms.ImageListStreamer )( resources.GetObject( "imageList1.ImageStream" ) ) );
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName( 0, "form_blue.ico" );
            this.imageList1.Images.SetKeyName( 1, "form_green.ico" );
            this.imageList1.Images.SetKeyName( 2, "form_yellow.ico" );
            this.imageList1.Images.SetKeyName( 3, "window_earth.ico" );
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point( 114, 194 );
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size( 75, 23 );
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point( 24, 194 );
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size( 75, 23 );
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point( 2, 161 );
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size( 103, 17 );
            this.checkBox1.TabIndex = 6;
            this.checkBox1.Text = "End of Interview";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler( this.rdbEndInterview_CheckedChanged );
            // 
            // InterviewPageSelection
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size( 210, 221 );
            this.Controls.Add( this.checkBox1 );
            this.Controls.Add( this.btnOk );
            this.Controls.Add( this.btnCancel );
            this.Controls.Add( this.listView1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ( ( System.Drawing.Icon )( resources.GetObject( "$this.Icon" ) ) );
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size( 216, 245 );
            this.Name = "InterviewPageSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Select Page";
            this.Load += new System.EventHandler( this.InterviewPageSelection_Load );
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler( this.InterviewPageSelection_FormClosed );
            this.ResumeLayout( false );
            this.PerformLayout( );

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Page;
        private System.Windows.Forms.ColumnHeader Note;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}