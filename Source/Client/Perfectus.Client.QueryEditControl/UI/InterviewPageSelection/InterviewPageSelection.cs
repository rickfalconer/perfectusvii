using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.Configuration;

namespace Perfectus.Client.Studio.UI.PackageObjectEditors.QueryDesigner
{
    public partial class InterviewPageSelection : Form
    {
        public InterviewPageSelection()
        {
            InitializeComponent();

            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");
            this.Size = config.PageListFormsize;
            this.Location = config.PageListFormLocation;
        }

        public Object SelectedPage()
        {
            if (listView1.SelectedItems.Count == 1)
                return listView1.SelectedItems[0].Tag;
            return null;
        }

        public void AddPage(String Title, string Note, Object page, int imageIdx)
        {
            ListViewItem item = new ListViewItem(Title);
            item.SubItems.Add(Note);
            item.Tag = page;
            item.ImageIndex = imageIdx;

            if (imageIdx == 3)
            {
                Font f = new Font(item.Font, FontStyle.Bold);
                item.Font = f;
            }
            this.listView1.Items.Add(item);
        }

        private void InterviewPageSelection_Load(object sender, EventArgs e)
        {
            listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.checkBox1.Checked = listView1.Items.Count == 0;
            rdbEndInterview_CheckedChanged(null, EventArgs.Empty);
        
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = listView1.SelectedItems.Count == 1
            || this.checkBox1.Checked;
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btnOk.PerformClick();
        }

        private void InterviewPageSelection_FormClosed(object sender, FormClosedEventArgs e)
        {
            ModuleMain main = new ModuleMain();
            CodeEditControlSection config = (CodeEditControlSection)main.GetConfig("QueryEditorConfigurationSection");
            config.PageListFormsize = this.Size;
            config.PageListFormLocation = this.Location;
        }

        private void rdbEndInterview_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.checkBox1.Checked)
            {
                listView1.Enabled = true;
                this.listView1.Focus();
                this.listView1.Items[0].Selected = true;
            }
            else
            {
                listView1.Enabled = false;
                btnOk.Enabled = true;
                listView1.SelectedItems.Clear();
            }
        }
    }
}