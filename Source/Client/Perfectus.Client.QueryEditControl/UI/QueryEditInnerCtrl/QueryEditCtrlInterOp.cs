using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.InteropServices;


namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class QueryCtrl 
    {
        # region Freezing

        [DllImport("user32", CharSet = CharSet.Auto)]
        private static extern int SendMessageAPI(HandleRef hWnd, int msg,
                                               int wParam, int lParam);


        private const int WM_SETREDRAW                    = 0x000B;
    
        private int freezeCnt = 0;
        private Boolean frozen (  )
        {
            return freezeCnt > 0;
        }

        private int Freezer(Boolean param_freeze)
        {
            if (param_freeze)
            {
                if (freezeCnt == 0)
                {
                    SendMessage(new HandleRef(this, Handle), WM_SETREDRAW, 0, 0);
                }
                freezeCnt++;
            }
            else
                if (!param_freeze && freezeCnt > 0)
                {
                    freezeCnt--;
                    if (freezeCnt == 0)
                    {
                        SendMessage(new HandleRef(this, Handle), WM_SETREDRAW, 1, 0);
                        Invalidate();
                    }
                }
            return freezeCnt;
        }
    }
    #endregion

    
    public partial class QueryCtrl 
    {
        private const int SCF_SELECTION = 0x0001;
        private const int SCF_WORD = 0x0002;
        private const int SCF_ALL = 0x0004;

        private const int CFM_UNDERLINETYPE = 8388608;
        private const int EM_SETCHARFORMAT = 1092;
        private const int EM_GETCHARFORMAT = 1082;

        [StructLayout(LayoutKind.Sequential)]
        private struct CHARFORMAT
        {
            public int cbSize;
            public uint dwMask;
            public uint dwEffects;
            public int yHeight;
            public int yOffset;
            public int crTextColor;
            public byte bCharSet;
            public byte bPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public char[] szFaceName;

            // CHARFORMAT2 from here onwards.
            public short wWeight;
            public short sSpacing;
            public int crBackColor;
            public int LCID;
            public uint dwReserved;
            public short sStyle;
            public short wKerning;
            public byte bUnderlineType;
            public byte bAnimation;
            public byte bRevAuthor;
        }

        [DllImport("user32", CharSet = CharSet.Auto)]
        private static extern int SendMessage(HandleRef hWnd, int msg,
                                               int wParam, ref CHARFORMAT lp);

        [DllImport("user32", CharSet = CharSet.Auto)]
        private static extern int SendMessage(HandleRef hWnd, int msg,
                                               int wParam, int lParam);

        public UnderlineStyle SelectionUnderlineStyle
        {
            get
            {
                CHARFORMAT fmt = new CHARFORMAT();
                fmt.cbSize = Marshal.SizeOf(fmt);

                // Get the underline style.
                SendMessage(new HandleRef(this, Handle), EM_GETCHARFORMAT,
                             SCF_SELECTION, ref fmt);

                // Default to no underline.
                if ((fmt.dwMask & CFM_UNDERLINETYPE) == 0)
                    return UnderlineStyle.None;

                byte style = (byte)(fmt.bUnderlineType & 0x0F);

                return (UnderlineStyle)style;
            }

            set
            {
                // Ensure we don't alter the color by accident.
                UnderlineColor color = SelectionUnderlineColor;

                // Ensure we don't show it if it shouldn't be shown.
                if (value == UnderlineStyle.None)
                    color = UnderlineColor.Black;

                CHARFORMAT fmt = new CHARFORMAT();
                fmt.cbSize = Marshal.SizeOf(fmt);
                fmt.dwMask = CFM_UNDERLINETYPE;
                fmt.bUnderlineType = (byte)((byte)value | (byte)color);

                // Set the underline type.
                SendMessage(new HandleRef(this, Handle), EM_SETCHARFORMAT,
                             SCF_SELECTION, ref fmt);
            }
        }


        public enum UnderlineStyle
        {
            /// <summary>
            /// No underlining.
            /// </summary>
            None = 0,

            /// <summary>
            /// Standard underlining across all words.
            /// </summary>
            Normal = 1,

            /// <summary>
            /// Standard underlining broken between words.
            /// </summary>
            Word = 2,

            /// <summary>
            /// Double line underlining.
            /// </summary>
            Double = 3,

            /// <summary>
            /// Dotted underlining.
            /// </summary>
            Dotted = 4,

            /// <summary>
            /// Dashed underlining.
            /// </summary>
            Dash = 5,

            /// <summary>
            /// Dash-dot ("-.-.") underlining.
            /// </summary>
            DashDot = 6,

            /// <summary>
            /// Dash-dot-dot ("-..-..") underlining.
            /// </summary>
            DashDotDot = 7,

            /// <summary>
            /// Wave underlining (like spelling mistakes in MS Word).
            /// </summary>
            Wave = 8,

            /// <summary>
            /// Extra thick standard underlining.
            /// </summary>
            Thick = 9,

            /// <summary>
            /// Extra thin standard underlining.
            /// </summary>
            HairLine = 10,

            /// <summary>
            /// Double thickness wave underlining.
            /// </summary>
            DoubleWave = 11,

            /// <summary>
            /// Thick wave underlining.
            /// </summary>
            HeavyWave = 12,

            /// <summary>
            /// Extra long dash underlining.
            /// </summary>
            LongDash = 13
        }


        public UnderlineColor SelectionUnderlineColor
        {
            get
            {
                CHARFORMAT fmt = new CHARFORMAT();
                fmt.cbSize = Marshal.SizeOf(fmt);

                // Get the underline color.
                SendMessage(new HandleRef(this, Handle), EM_GETCHARFORMAT,
                             SCF_SELECTION, ref fmt);

                // Default to black.
                if ((fmt.dwMask & CFM_UNDERLINETYPE) == 0)
                    return UnderlineColor.Black;

                byte style = (byte)(fmt.bUnderlineType & 0xF0);

                return (UnderlineColor)style;
            }

            set
            {
                // Ensure we don't alter the style.
                UnderlineStyle style = SelectionUnderlineStyle;

                // Ensure we don't show it if it shouldn't be shown.
                if (style == UnderlineStyle.None)
                    value = UnderlineColor.Black;

                CHARFORMAT fmt = new CHARFORMAT();
                fmt.cbSize = Marshal.SizeOf(fmt);
                fmt.dwMask = CFM_UNDERLINETYPE;
                fmt.bUnderlineType = (byte)((byte)style | (byte)value);

                // Set the underline color.
                SendMessage(new HandleRef(this, Handle), EM_SETCHARFORMAT,
                             SCF_SELECTION, ref fmt);
            }
        }

        public enum UnderlineColor
        {
            /// <summary>Black.</summary>
            Black = 0x00,

            /// <summary>Blue.</summary>
            Blue = 0x10,

            /// <summary>Cyan.</summary>
            Cyan = 0x20,

            /// <summary>Lime green.</summary>
            LimeGreen = 0x30,

            /// <summary>Magenta.</summary>
            Magenta = 0x40,

            /// <summary>Red.</summary>
            Red = 0x50,

            /// <summary>Yellow.</summary>
            Yellow = 0x60,

            /// <summary>White.</summary>
            White = 0x70,

            /// <summary>DarkBlue.</summary>
            DarkBlue = 0x80,

            /// <summary>DarkCyan.</summary>
            DarkCyan = 0x90,

            /// <summary>Green.</summary>
            Green = 0xA0,

            /// <summary>Dark magenta.</summary>
            DarkMagenta = 0xB0,

            /// <summary>Brown.</summary>
            Brown = 0xC0,

            /// <summary>Olive green.</summary>
            OliveGreen = 0xD0,

            /// <summary>Dark gray.</summary>
            DarkGray = 0xE0,

            /// <summary>Gray.</summary>
            Gray = 0xF0
        }
    }
}
