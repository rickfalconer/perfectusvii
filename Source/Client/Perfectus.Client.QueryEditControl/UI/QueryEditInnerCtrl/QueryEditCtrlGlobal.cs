using System;
using System.Collections.Generic;
using System.Text;
using Perfectus.Client.Studio.ExpressionLibrary;

namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class QueryCtrl
    {
        const String PrefixText = "<%";
        const String PostfixText = "%>";

        public enum enumAction { None, LClick, LDblClick, RClick, Hover, SelChg }

        private void SuspendEvents() { Freezer(true);  }
        private void ResumeEvents() { Freezer(false); }

        internal interface IInfoStruc
        {
            enumAction Action { get; set; }
        }

        internal class ObjectInfoStruc : EventArgs, IInfoStruc
        {
            private Boolean _valid;
            public Boolean Valid
            {
                get { return _valid; }
                set { _valid = value; }
            }

            private Guid _id;
            public Guid Id
            {
                get { return _id; }
            }
            private Guid _parentid;
            public Guid ParentId
            {
                get { return _parentid; }
            }
            private String _name;
            public String Name
            {
                get { return _name; }
                set { _name = value; }
            }
            private AtomType _type;
            public AtomType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            internal
                ObjectInfoStruc(String param_name, Guid param_id, Guid param_parentid,
                            AtomType param_type, Boolean param_valid)
                : this(param_name, param_id, param_parentid, param_type )
            {
                _valid = param_valid;
            }

            internal
                ObjectInfoStruc(String param_name, Guid param_id, Guid param_parentid,
                            AtomType param_type)
                : base()
            {
                _name = param_name;
                _id = param_id;
                _parentid = param_parentid;
                _type = param_type;
            }
            internal
                ObjectInfoStruc(CAtom param_atom)
                : this(param_atom.ToString(), param_atom.Id, param_atom.ParentId, param_atom.type)
            { }
            
            public override string ToString()
            {
                return String.Format("{0}{1}", VisibleText(), HiddenText());
            }
            public String HiddenText()
            {
                return String.Format("#{0}#{1}#{2}#{3}", Id.ToString(), ParentId.ToString(), Type, PostfixText);
            }
            public String VisibleText()
            {
                return String.Format("{0}", Name);
            }

            #region IInfoStruc Members

            private enumAction _action;
            public enumAction Action { get { return _action; } set { _action = value; } }

            #endregion
        }

        internal class ObjectsInfoStruc : EventArgs, IInfoStruc
        {
            List<ObjectInfoStruc> _hitObjects;

            internal
                ObjectsInfoStruc()
                : base()
            {
                _hitObjects = new List<ObjectInfoStruc>();
            }
            internal
            void Add(ObjectInfoStruc param_object)
            {
                _hitObjects.Add(param_object);
            }
            internal
            ObjectInfoStruc[] GetObjects()
            {
                return _hitObjects.ToArray();
            }
            #region IInfoStruc Members

            private enumAction _action;
            public enumAction Action { get { return _action; } set { _action = value; } }

            #endregion
        }


        internal class InvalidHitMessage : EventArgs
        {
            private String _message;
            internal String Message
            {
                get { return _message; }
            }

            internal
            InvalidHitMessage(String param_message)
                : base()
            {
                _message = param_message;
            }
            public override string ToString()
            {
                return Message;
            }
        }

        internal delegate void ObjectHit(ObjectInfoStruc param_structure);
        internal delegate void ObjectsHit(ObjectsInfoStruc param_structure);
        internal delegate void InvalidHit(InvalidHitMessage param_message);
        internal delegate void ObjectHover(ObjectInfoStruc param_message);
        internal delegate void QueryRedraw(Boolean param_success, String param_message);

        internal event ObjectHit objectHit;
        internal event ObjectsHit objectsHit;
        internal event InvalidHit invalidHit;
        internal event ObjectHover objectHover;
        internal event QueryRedraw queryRedraw;

        private void OnObjectHit(ObjectInfoStruc param_structure)
        {
            if (objectHit != null && !frozen())
                objectHit(param_structure);
        }
        private void OnObjectsHit(ObjectsInfoStruc param_structure)
        {
            if (objectsHit != null && !frozen())
                objectsHit(param_structure);
        }
        private void OnInvalidHit(String param_message)
        {
            if (invalidHit != null && !frozen())
                invalidHit(new InvalidHitMessage(param_message));
        }
        private void OnObjectHover(ObjectInfoStruc param_structure)
        {
            if (objectHover != null && !frozen())
                objectHover(param_structure);
        }
        private void OnQueryRedraw(Boolean param_success, String param_message)
        {
            if (queryRedraw != null )
                queryRedraw(param_success, param_message);
        }
    }
}
