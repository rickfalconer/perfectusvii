using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Client.Studio.ExpressionLibrary;
using Perfectus.Common;

namespace Perfectus.Client.Studio.QueryEditControl.OperatorSelectorForm
{
    public partial class OperatorSelector : Form
    {
        private Perfectus.Common.PerfectusDataType[] _dataTypes;
        public Perfectus.Common.PerfectusDataType[] DataTypes { set { _dataTypes = value; } }

        private OperatorDisplay _operatorDisplay;
        public OperatorDisplay OperatorDisplay { get { return _operatorDisplay;}}

        public OperatorSelector(OperatorDisplay param_operatorDisplay)
        {
            InitializeComponent();
            _operatorDisplay = param_operatorDisplay;
        }

        private Boolean IsDatatypeSupported(PerfectusDataType param_datatype)
        {
            foreach (PerfectusDataType type in _dataTypes)
                if (param_datatype == type)
                    return true;
            return false;
        }

        private void OperatorSelector_Load(object sender, EventArgs e)
        {
            this.listView1.Items.AddRange(
                new ListViewItem[] {
                new ListViewItem(new String[] { new OperatorDisplay ( Operator.Eq).Display, "Equal to " }),
                new ListViewItem(new String[] {new OperatorDisplay ( Operator.Ne).Display, "Not equal to " })});

            if ( IsDatatypeSupported (PerfectusDataType .Number) ||
                IsDatatypeSupported (PerfectusDataType.Date))
                this.listView1.Items.AddRange(
                    new ListViewItem[] {
                    new ListViewItem(new String[] { new OperatorDisplay ( Operator.Lt).Display, "Less than " }),
                    new ListViewItem(new String[] { new OperatorDisplay ( Operator.Gt).Display, "Greater than " }),
                    new ListViewItem(new String[] { new OperatorDisplay ( Operator.Le).Display, "Less or equal than " }),
                    new ListViewItem(new String[] { new OperatorDisplay ( Operator.Ge).Display, "Greater or equal than " })});
    
            this.listView1.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            int index = System.Convert.ToInt32(_operatorDisplay.Op);
            this.listView1.Items[index].Selected = true;

            int width = listView1.Columns[0].Width + listView1.Columns[1].Width;

            double f = listView1.Items[0].Font.GetHeight() * (listView1.Items.Count + 1);
            int height = System.Convert.ToInt32(f);
            Size = new Size(width,height);
            DesktopLocation = new Point(Cursor.Position.X, Cursor.Position.Y + 8);
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            int index = listView1.SelectedIndices[0];
            // We love C++
            _operatorDisplay.Op = (Operator)Enum.ToObject(typeof(Operator), index);
            this.DialogResult = DialogResult.OK;
            DialogResult = DialogResult.OK;
            Close();
        }

        private void OperatorSelector_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }
    }
}