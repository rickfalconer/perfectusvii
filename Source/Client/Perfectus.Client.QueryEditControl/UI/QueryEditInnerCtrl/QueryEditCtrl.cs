using System;
using System.Collections.Generic;
using System.Text;

using System.Windows.Forms;
using Perfectus.Client.Studio.ExpressionLibrary;
using System.Drawing;
using Perfectus.Client.Studio.ExpressionLibrary.PrintFormatter;
using Perfectus.Client.Configuration;


namespace Perfectus.Client.Studio.QueryEditControl
{
    public partial class QueryCtrl : RichTextBox
    {
        #region Private variables

        private CQuery _query;
        private IPrintFormatter _iPrintFormatter;

        internal Dictionary<String, CTextAttr> attributes;
        
        System.Windows.Forms.Timer _textMarkingTimer = new Timer();
//        System.Windows.Forms.Timer _mouseMoveTimer = new Timer();

        #endregion

        #region Constructor / Dispose
        public
        QueryCtrl()
        {
            ReadOnly = true;
            
            this.AutoWordSelection = false;

            attributes = new Dictionary<String, CTextAttr>();
            _textMarkingTimer.Tick += new EventHandler(CodeEditBox_SelectionChanged_Tick);
            _textMarkingTimer.Interval = System.Windows.Forms.SystemInformation.MouseHoverTime;

            //_mouseMoveTimer.Tick += new EventHandler(CodeEditBox_MouseHover_Tick);
            //_mouseMoveTimer.Interval = System.Windows.Forms.SystemInformation.MouseHoverTime;

            this.MouseEnter += new System.EventHandler(CodeCtrl_MouseEnter);
            this.SelectionChanged += new System.EventHandler(CodeCtrl_SelectionChanged);
            //this.MouseMove += new System.Windows.Forms.MouseEventHandler(CodeCtrl_MouseMove);
            this.MouseLeave += new System.EventHandler(CodeCtrl_MouseLeave);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(CodeCtrl_MouseDown);
        }

        protected override void Dispose(bool disposing)
        {
            _textMarkingTimer.Stop();
            _textMarkingTimer.Tick -= new EventHandler(CodeEditBox_SelectionChanged_Tick);
            this.MouseEnter -= new System.EventHandler(CodeCtrl_MouseEnter);
            this.SelectionChanged -= new System.EventHandler(CodeCtrl_SelectionChanged);
            this.MouseLeave -= new System.EventHandler(CodeCtrl_MouseLeave);
            this.MouseDown -= new System.Windows.Forms.MouseEventHandler(CodeCtrl_MouseDown);
            base.Dispose(disposing);
        }

        #endregion



        #region Initialize query
        internal void SetQuery(IPrintFormatter param_prettyPrinter)
        {
            _iPrintFormatter = param_prettyPrinter;
            _iPrintFormatter.writeText += new WriteText(WriteText);
            _iPrintFormatter.writeElement += new WriteElement(WriteElement);
        }

        internal void SetQuery(IPrintFormatter param_prettyPrinter, CQuery param_query)
        {
            if (param_query == null)
                throw new ExpressionLibraryException("Query must not be null", "CodeCtrl::SetQuery", "Software defect");

            if (param_prettyPrinter == null)
                throw new ExpressionLibraryException("Pretty-printer must not be null", "CodeCtrl::SetQuery", "Software defect");

            _query = param_query;
            _iPrintFormatter = param_prettyPrinter;

            _iPrintFormatter.writeText += new WriteText(WriteText);
            _iPrintFormatter.writeElement += new WriteElement(WriteElement);
        }

        internal void SetQuery(CQuery param_query)
        {
            if (param_query == null)
                throw new ExpressionLibraryException("Query must not be null", "CodeCtrl::SetQuery", "Software defect");
            _query = param_query;
            ReDraw();
        }


        public void ReDraw (){
            if (_iPrintFormatter == null)
                return;
            int curSelStart = this.SelectionStart;

            SuspendEvents();
            float curentZoom = ZoomFactor;
            Clear();
            _query.DisplayYourself(_iPrintFormatter.Print);
            ResumeEvents();
            String msg = "";
            OnQueryRedraw(_query.IsValid(ref msg), msg);
            ZoomFactor = 1.0f;
            ZoomFactor = curentZoom;
            this.SelectionStart = curSelStart;
            this.SelectionLength = 0;
            ScrollToCaret();
        }

        internal CQuery GetQuery()
        {
            return _query;
        }

        #endregion
    }
}