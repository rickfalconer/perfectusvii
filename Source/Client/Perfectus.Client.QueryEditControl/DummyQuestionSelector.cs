using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Perfectus.Common.PackageObjects;

namespace CodeEditControl
{
    public partial class DummyQuestionSelector : Form
    {
        private Package _package;

        private Question _question;

        public Question question
        {
            get { return _question; }
            set { _question = value; }
        }
	

        public DummyQuestionSelector(Package param_package)
        {
            InitializeComponent();

            _package = param_package;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void DummyQuestionSelector_Load(object sender, EventArgs e)
        {
            foreach (Question q in _package.Questions)
                this.listBox1.Items.Add(q);

            if (listBox1.Items.Count == 0)
            {
                MessageBox.Show("No questions in package");
                Close();
            }
            listBox1.SelectedIndex = 0;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _question = (Question)listBox1.SelectedItem;
        }
    }
}